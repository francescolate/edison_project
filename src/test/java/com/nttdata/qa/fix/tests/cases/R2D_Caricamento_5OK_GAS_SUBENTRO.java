package com.nttdata.qa.fix.tests.cases;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_Caricamento_5OK_GAS_SUBENTRO {

	@Step("R2D Caricamento esiti 5OK per SWA - GAS")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				String statopratica=prop.getProperty("STATO_R2D");
				//verifica Stato Pratica in 4OK - FA
		//		if (statopratica.compareToIgnoreCase("FA")==0)  {
				{	// Completamento 5OK - SWA GAS
					R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
					R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
					//Selezione Menu Caricamento Esiti
					logger.write("Code di comunicazione/Caricamento Esiti - Start");
					 menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
					logger.write("Code di comunicazione/Caricamento Esiti - Completed");
					//Selezione tipologia Caricamento
					logger.write("Selezione tipologia Caricamento - Start");
					 caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
					logger.write("Selezione tipologia Caricamento - Completed");
					//Inserimento PDR oppure Id Richiesta
//					if(prop.getProperty("PDR").isEmpty()){
						logger.write("inserisci Id_Richiesta - Start");
//						caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
						if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("ID_ORDINE"));
						} else {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("ID_ORDINE"));
						}
						logger.write("inserisci Id_Richiesta - Completed");
//					}else {
//						logger.write("inserisci Pod - Start");
//						caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPDR, prop.getProperty("PDR"));
//						logger.write("inserisci Pod - Completed");
//					}
//					//Inserisci Pod
//					logger.write("Caricamento valore PDR - Start");
//					 caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPDR, prop.getProperty("PDR"));
//					logger.write("Caricamento valore PDR - Completed");
					//Click Cerca
					logger.write("Selezione pulsante Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione pulsante Cerca - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Click button Azione
					logger.write("Selezione Bottone Azione Pratica - Start");
					 caricamentoEsiti.selezionaTastoAzionePratica();
					logger.write("Selezione Bottone Azione Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Verifica Stato pratica atteso
					logger.write("Verifico Stato Pratica - Start");
					 caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "FA");
					logger.write("Verifico Stato Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Selezione Tipo Esito 
					logger.write("Caricamento Evento FA - Start");
					 caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_5OK_GAS"));
					logger.write("Caricamento Evento - Completed");
					//Inserimento esito
					logger.write("Caricamento Esito 5OK - Start");
					 caricamentoEsiti.selezioneEsito5OKGas("OK");
					logger.write("Caricamento Esito 5OK - Completed");
//					//Calcolo sysdate
					Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String data = simpleDateFormat.format(calendar.getTime()).toString().substring(0,10);
					
					//Calcolo data minima switch, primo giorno del mese successivo
					calendar.add(Calendar.MONTH,1);//Mese successivo
					calendar.set(Calendar.DAY_OF_MONTH, 1);//Primo giorno del mese
					String dataminima = simpleDateFormat.format(calendar.getTime()).toString();
					prop.setProperty("DATA_MINIMA_SW", dataminima);
					logger.write("Calcolo data minima ripensamento");
//					System.out.println("Data minima ripensamento:"+dataminima);
	
					if(prop.getProperty("TIPO_LAVORAZIONE_CRM_GAS").contains("Disdetta standard")) {
						String dataDisdetta = prop.getProperty("DATA_SWITCH_/_DISDETTA");
						logger.write("Caricamento Data Disdetta - Start");
//						 caricamentoEsiti.inserimentoDettaglioEsito5OK_DisdettaStandard_ELE(dataDisdetta, cputente, cpgestore);
						logger.write("Caricamento Data Disdetta - Completed");
					}
					
					else {
						logger.write("Esitazione 5OK - Start");
						String idRich; 
						if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
							idRich = prop.getProperty("ID_RICHIESTA"); 
						} else {
							idRich = prop.getProperty("ID_ORDINE");
						}
						caricamentoEsiti.inserimentoDettaglioEsito5OK_SWA_GAS(idRich, 
								prop.getProperty("LETTURA_CONTATORE"),
								prop.getProperty("TIPO_LETTURA"),
								prop.getProperty("COEFFICIENTE_C"),
								prop.getProperty("DATA_MINIMA_SWITCH"),
								prop.getProperty("PRELIEVO_ANNUO"),
								prop.getProperty("CLASSE_CONTATORE"),
								prop.getProperty("CIFRE_CONTATORE"),
								data,
								prop.getProperty("DATA_MINIMA_SW"));
						prop.setProperty("STATO_R2D", "OK");
						logger.write("Esitazione 5OK - Completed");
						TimeUnit.SECONDS.sleep(5);
					}
				}

				//Salvataggio stato attuale pratica
//				System.out.println("Caricamento 5OK Stato: "+ prop.getProperty("STATO_PRATICA_R2D"));
//				System.out.println("Caricamento 5OK Stato: "+ prop.getProperty("STATO_R2D"));
				prop.setProperty("RETURN_VALUE", "OK");

			}
			//
			prop.setProperty("STATUS", "OK");
		} 		
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}

