package com.nttdata.qa.util.qantt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.JiraTestMap;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class BonificaTOMain
{	
	public static void main(String[] args) throws Exception {


		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		PrintWriter out =null;
//		 HashMap<String, String> vecchi =  new HashMap<String, String>();
//		 
//		 vecchi.put("RECUPERARICHIESTATOUCHPOINT", "x");
//		 vecchi.put("DATACONFIGURATION", "x");
//		 vecchi.put("INSERIMENTOINDIRIZZOESECUZIONELAVORIEVO", "x");
//		 vecchi.put("PROCESSOMODIFICASTATORESIDENZA", "x");
//		 vecchi.put("MODALITAFIRMAECANALEINVIOSUBENTRO", "x");
//		 vecchi.put("VERIFICAQUOTEQUALITYCALLDIGITAL", "x");
//		 vecchi.put("VERIFICAINDIRIZZISEDELEGALE", "x");
//		 vecchi.put("VERIFICAMODELLOOGGETTOEMAILGENERICOCOPIADOCUMENTIWORKBENCH", "x");
//		 vecchi.put("CONFERMAFATTURAZIONEELETTRONICASWAEVO", "x");
//		 vecchi.put("VERIFICASTATOINLAVORAZIONESOTTOSTATOINVIATOCOPIADOCUMENTIWORKBENCH", "x");
//		 vecchi.put("SALVAOIRICERCA", "x");
//		 vecchi.put("VERIFICHERICHIESTAPPDOPOPREVENTIVOEVO", "x");
//		 vecchi.put("CHECKSEZIONESELEZIONEFORNITURARICONTRATTUALIZZAZIONE", "x");
//		 vecchi.put("PROCESSOAVVIOESTRATTOCONTO", "x");
//		 vecchi.put("R2D_FORZATURA_ESITI_2OK_GAS", "x");
//		 vecchi.put("CREAOFFERTAVOLTURASENZAACCOLLO_FINALIZZAZIONE", "x");
//		 vecchi.put("PROCESSOSWITCHATTIVOMULTIEVORESIDENZIALE", "x");
//		 vecchi.put("COMPILADATIFORNITURAGASPRIMAATTIVAZIONE", "x");
//		 vecchi.put("RECUPERALINKCREAZIONEACCOUNT", "x");
//		 vecchi.put("RECUPERASTATUSCASE_AND_ORDER", "x");
//		 vecchi.put("CONTATTIECONSENSIVOLTURASENZAACCOLLORES", "x");
//		 vecchi.put("UPDATESOLLECITORICHIESTATOUCHPOINT", "x");
//		 vecchi.put("AZIONIRIEPILOGOOFFERTAVOLTURA", "x");
//		 vecchi.put("VERIFYPODDETAILS330", "x");
//		 vecchi.put("VERIFYPODDETAILS331", "x");
//		 vecchi.put("RECUPERAATTIVITÀDMSUPLOAD", "x");
//		 vecchi.put("AAA", "x");
//		 vecchi.put("SETUTENZAS2SQUALITY", "x");
//		 vecchi.put("VERIFICASOTTOSTATOCASEOFFERANNULLATO", "x");
//		 vecchi.put("VERIFICAMODELLOOGGETTOPECGENERICOCOPIADOCUMENTIWORKBENCH", "x");
//		 vecchi.put("LEGGIECLICKTP2", "x");
//		 vecchi.put("R2D_CARICAMENTOESITI_3OK_ELE", "x");
//		 vecchi.put("SELEZIONEMERCATOALLACCIOATTIVAZIONEEVO", "x");
//		 vecchi.put("VERIFICASOTTOSTATOCASEORDERINVIATO", "x");
//		 vecchi.put("CONFERMAPREVENTIVOALLACCIOATTIVAZIONEPREDETERMINABILE", "x");
//		 vecchi.put("RECUPERAPODMULTINONESISTENTI", "x");
//		 vecchi.put("VERIFICHERICHIESTADAPOD", "x");
//		 vecchi.put("CONSENSIECONTATTIVOLTURACONACCOLLO", "x");
//		 vecchi.put("ABILITAPRECHECKINVIATA", "x");
//		 vecchi.put("VERIFICABOTTONECONFERMAREFERENTE", "x");
//		 vecchi.put("CARICAVERIFICAVALIDADOCUMENTIMULTIELE", "x");
//		 vecchi.put("PROCESSOPREDISPOSIZIONEPRESAEVOGAS", "x");
//		 vecchi.put("PROCESSOSWITCHATTIVOEVONONRESIDENZIALEDUAL", "x");
//		 vecchi.put("SETPODTORETREIVEELE_DUAL", "x");
//		 vecchi.put("CREAOFFERTAGESTIONEANNULLAVOLTURACONACCOLLO", "x");
//		 vecchi.put("PROCESSOATTIVAZIONEVASBOLLETTAWEB", "x");
//		 vecchi.put("VERIFYREQUESTDETAILS_278", "x");
//		 vecchi.put("CANCELPOD_338", "x");
//		 vecchi.put("CHECKSEZIONECLIENTEUSCENTEANDINSERTPODVOLTURACONACCOLLO", "x");
//		 vecchi.put("PROCESSOSWITCHATTIVOEVONONRESIDENZIALE", "x");
//		 vecchi.put("VERIFICASTATUSUDBCREAZIONEORDINE", "x");
//		 vecchi.put("DISABILITAQUALITYCHECK", "x");
//		 vecchi.put("AZIONISEZIONECARRELLOBUSINESS", "x");
//		 vecchi.put("CHECKSEZIONECONTATTICONSENSI", "x");
//		 vecchi.put("SETCASESTATUSPOSTCHIUSURA", "x");
//		 vecchi.put("R2D_INVIOPSACQUIRENTEUNICO_1OK_GAS", "x");
//		 vecchi.put("VERIFICASTATUSORDINEESPLETATO", "x");
//		 vecchi.put("MODIFICAANAGRAFICABUSINESS", "x");
//		 vecchi.put("CONTATTIECONSENSIVOLTURASENZAACCOLLO", "x");
//		 vecchi.put("RECUPERADATIINNESTATIWORKBENCH", "x");
//		 vecchi.put("PROCESSOSWITCHATTIVOEVORESIDENZIALE", "x");
//		 vecchi.put("ABILITAQUALITYCHECK", "x");
//		 vecchi.put("R2D_CARICAMENTOESITI_3OK_STANDARD_GAS", "x");
//		 vecchi.put("RECUPERAATTIVITÀ", "x");
//		 vecchi.put("CONFERMAMODALITAFIRMADIGITAL", "x");
//		 vecchi.put("VERIFICAQUOTEQUALITYCALL", "x");
//		 vecchi.put("R2D_CARICAMENTOESITI_5OK_SWA_GAS", "x");
//		 vecchi.put("SALVAINBOZZA", "x");
//		 vecchi.put("RECUPERASTATUSASSET", "x");
//		 vecchi.put("LOGINR2D_ELE", "x");

		 
		 
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);		
//			TimeUnit.SECONDS.sleep(1);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			File log = new File("C:\\log.txt");
		       
		       if(log.exists()==false){
		               System.out.println("We had to make a new file.");
		               log.createNewFile();
		       }
		       out = new PrintWriter(log);
		       out.append(" Start " + "\n");
		       
		       util.waitUntilIsDisplayed(By.xpath("//a[contains(text(),'CONFIGURAZIONE TEST')]"));
				//Click su test configuration
				util.objectManager(By.xpath("//a[contains(text(),'CONFIGURAZIONE TEST')]"), util.click);
				
			//ciclo dai moduli estratti
			HashMap<String, String> x = EstrazioneClassi.ritornaModuli();
			for(Entry<String, String> entry : x.entrySet()) {

				//viene letto il test object da caricare
				String attualeTestObject;
				
				String nome = entry.getKey();
				
			    String path = entry.getValue();
			    
			    // codice per bypassare quelli già inseriti altrimenti non ce ne andiamo più
			   try {
//				    
				   if(!nome.startsWith("V")) {
				    	continue;
				    }
//				   if(vecchi.get(nome).equals("x")) {
//				    	System.out.println("esiste: "+nome);
//				    	continue;
//				    }
				    
			   }catch(Exception e) {
				   
			   }
			    
			    System.out.println(nome + " " +path);
				attualeTestObject=nome;
				
				//Click su test objects
				TimeUnit.SECONDS.sleep(1);
				try{
					util.objectManager(By.xpath("//a[contains(text(),'Test Object')]"), util.click);
				}catch(Exception e){
					util.waitUntilIsDisplayed(By.xpath("//a[contains(text(),'Test Object')]"));
				}
				//viene effettuata ricerca per verificare se il test object è stato già censito su qantt
				boolean caricato=false;
				//Viene effettuato il click sul filtro per ricercare il test object
				util.objectManager(By.xpath("//button[@container='body']"), util.click);
				//Viene inserito il nome del test object nel campo name
				util.objectManager(By.xpath("//input[@id='name']"), util.sendKeys,attualeTestObject);
				TimeUnit.SECONDS.sleep(2);
				//click su filter
				util.objectManager(By.xpath("//button[contains(text(),'Filtra')]"), util.click);
	
				//Verifica se il test object è già presente su Qantt
				if(!util.exists(By.xpath("//h6[contains(text(),'Nessuno Elemento corrisponde al filtraggio')]"), 5)){
					System.out.println("Il test Object:"+attualeTestObject+" esiste già");
					out.append("Il test Object:"+attualeTestObject+" esiste già" + "\n");
					caricato=true;
				}
				//hai trovato
				if(caricato==true){
					util.objectManager(By.xpath("//fa-icon[@placement='top' and @tabindex='0']"), util.click);
					util.waitUntilIsDisplayed(By.xpath("//input[@id='testScript']"));
						String x1 = driver.findElement(By.xpath("//input[@id='testScript']")).getAttribute("value");
//						String c =driver.findElement(By.xpath("//select[@id='testExecutor']")).getText();
						Select sel = new Select (driver.findElement(By.xpath("//select[@id='testExecutor']")));
						String c = sel.getFirstSelectedOption().getText();
							System.out.println("Modulo "+x1);
						if((!x1.contains("x"))&&(c.trim().equals("SELENIUM"))){
							TimeUnit.SECONDS.sleep(1);
							util.objectManager(By.xpath("//a[contains(text(),'Test Object')]"), util.click);
							continue;
						}else{
							out.append("Modulo contenente la x o è diverso da selenium" + "\n");
							System.out.println("Tipo diverso sa Selenium "+nome);
							Select sel1 = new Select (driver.findElement(By.xpath("//select[@id='testExecutor']")));
							TimeUnit.SECONDS.sleep(1);
							sel1.selectByIndex(0);
							driver.findElement(By.xpath("//input[@id='testScript']")).clear();
							driver.findElement(By.xpath("//input[@id='testScript']")).sendKeys(x1.replaceAll("x", ""));
							TimeUnit.SECONDS.sleep(1);
							util.objectManager(By.xpath("//button[text()='Salva']"), util.click);
							TimeUnit.SECONDS.sleep(2);
						}
						
					
					
				}
				//Se il test Object non esiste, viene creato e vengono inseriti i campi di completamento
				if(caricato==false){
					//Click su nuovo test Object
					util.objectManager(By.xpath("//button[contains(text(),'Nuovo Test Object')]"), util.click);
	
					String testName,description,testDescription,testExecutor;
					testName=nome;
					description="il modulo "+nome;
					testDescription=path.replace("src\\test\\java\\", "").replace("\\", ".").replace(".java", "");
					testExecutor=" SELENIUM ";
	
					out.append("Modulo :"+nome+" con package: "+testDescription + "\n");
					//Inserimento nome
					util.objectManager(By.xpath("//input[@id='name']"), util.sendKeys,testName);
					//Inserimento descrizione
					util.objectManager(By.xpath("//textarea[@id='description']"), util.sendKeys,description);
					//Inserimento di tutti i paramentri di input
					util.objectManager(By.xpath("//input[@id='testScript']"), util.sendKeys,testDescription);
					
					//Inserimento di tutti i paramentri di output
//					util.objectManager(By.xpath("//select[@id='testExecutor']"), util.select,testExecutor);
					//impiega troppo tempo fino a 2 minuti porkaraund
					
					driver.findElement(By.xpath("//select[@id='testExecutor']")).click();
					
					driver.findElement(By.xpath("//select[@id='testExecutor']")).sendKeys(Keys.ARROW_DOWN);
					
					driver.findElement(By.xpath("//select[@id='testExecutor']")).sendKeys(Keys.ENTER);
					
					//Inserimento di tutti i parametri di input
					
					//raf spa inserisco i parametri in input estratti dalla classe
					ArrayList<String> y = EstrazioneClassi.ritornaPropertyInByModulo(path);
				    for (String parametroInput : y) {
						System.out.println("I: "+parametroInput);
						out.append("I: "+parametroInput + "\n");
//					}
//					int maxi=2;
//					for(int i=0;i<maxi;i++){
//						parametroInput="A"+i;
						util.objectManager(By.xpath("//input[@id='newInput']"), util.sendKeys,parametroInput);
						TimeUnit.SECONDS.sleep(2);
						
						util.objectManager(By.xpath("//input[@id='newInput']/ancestor::div[1]/button"), util.click);
						TimeUnit.SECONDS.sleep(2);
					}
				  
					
				
					//Inserimento di tutti i parametri di output
				    
				    ArrayList<String> z = EstrazioneClassi.ritornaPropertyOutByModulo(path);
				    for (String parametroOutput : z) {
						System.out.println("O: "+parametroOutput);
						out.append("O: "+parametroOutput + "\n");
//					}
//					int maxj=2;
//					for(int j=0;j<maxj;j++){
//						parametroOutput="B"+j;
						util.objectManager(By.xpath("//input[@id='newOutput']"), util.sendKeys,parametroOutput);
						TimeUnit.SECONDS.sleep(2);
//						if(j<=maxj-1){
							util.objectManager(By.xpath("//input[@id='newOutput']/ancestor::div[1]/button"), util.click);
							TimeUnit.SECONDS.sleep(2);
//						}
					}
				    TimeUnit.SECONDS.sleep(2);
					//Crea test object
					util.objectManager(By.xpath("//button[contains(text(),'Crea')]"), util.click);
					 TimeUnit.SECONDS.sleep(10);
				}
				
			
			}
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");		
			out.close();
		}
	}




}
