package com.nttdata.qa.util.qantt;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.util.ReportUtility;

public class CaricaTestObject {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
//			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
//			prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
//			prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
//			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
//			prop.setProperty("CODICE_FISCALE", "TMTLXJ85C04F839P");
//			prop.setProperty("COMMODITY", "ELE");
//			prop.setProperty("TIPO_UTENZA", "PE");
//			prop.setProperty("TIPO_CLIENTE", "Residenziale");
//			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
//			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
//			prop.setProperty("MERCATO", "Libero");
//			prop.setProperty("PROVINCIA", "ROMA");
//			prop.setProperty("CITTA", "ROMA");
//			prop.setProperty("INDIRIZZO", "VIA NIZZA");
//			prop.setProperty("CIVICO", "4");
//			prop.setProperty("USO_FORNITURA", "Uso Abitativo");
//			prop.setProperty("RESIDENTE", "NO");
//			prop.setProperty("USO_ENERGIA", "Ordinaria");
//			prop.setProperty("TITOLARITA", "Uso/Abitazione");
//			prop.setProperty("TENSIONE", "220");
//			prop.setProperty("ASCENSORE", "NO");
//			prop.setProperty("POTENZA", "3");
//			prop.setProperty("DISALIMENTABILITA", "SI");
//			prop.setProperty("TIPO_MISURATORE", "Non Orario");
//			prop.setProperty("CONSUMO_ANNUO", "1000");
//			prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
//			prop.setProperty("PRODOTTO", "Speciale Luce 60");
//			prop.setProperty("TIPO_OI_ORDER", "Commodity");
//			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
//			prop.setProperty("CAP", "00135");
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			LoginQantt.main(args);
			CreaTestObject.main(args);
		};
		
		@After
    public void fineTest() throws Exception{
//            String args[] = {nomeScenario};
//            InputStream in = new FileInputStream(nomeScenario);
//            prop.load(in);
//            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//            ReportUtility.reportToServer(this.prop);
      };

		
		
		
		
		
	

}
