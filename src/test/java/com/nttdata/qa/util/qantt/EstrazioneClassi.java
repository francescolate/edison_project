package com.nttdata.qa.util.qantt;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EstrazioneClassi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			HashMap<String, String> x = ritornaModuli();
			for(Entry<String, String> entry : x.entrySet()) {
			    String key = entry.getKey();
			    String value = entry.getValue();
			    System.out.println(key + " " +value);
			    ArrayList<String> y = ritornaPropertyInByModulo(value);
			    for (String string : y) {
					System.out.println("I: "+string);
				}
			    ArrayList<String> z = ritornaPropertyOutByModulo(value);
			    for (String string : z) {
					System.out.println("O: "+string);
				}
			    
			    // do what you have to do here
			    // In your case, another loop.
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<String> ritornaPropertyInByModulo(String string) {
		ArrayList<String> arr = new ArrayList<String>();
		File file = new File(string);

		try {
		    Scanner scanner = new Scanner(file);

		    //now read the file line by line...
		    int lineNum = 0;
		    String temp ="";
		    while (scanner.hasNextLine()) {
		        String line = scanner.nextLine();
		        lineNum++;
		        Pattern pattern = Pattern.compile("getProperty\\(\"([*A-Za-z[_]0-9]*)\"*");
		        Matcher matcher = pattern.matcher(line);
		        while(matcher.find()) {
		        	temp = matcher.group(1);
		        	if(temp.equals("RUN_LOCALLY"))continue;
		        	if(temp.equals("MODULE_ENABLED"))continue;
		        	if(temp.length()<2)continue;
		        	
		        	
		        	 arr.add(temp.toUpperCase());
		          
		        }
		    }
		    scanner.close();
		} catch(Exception e) { 
		    e.printStackTrace();
		}
	
	return removeDuplicates(arr);
		
	}
	
	
	
	public static ArrayList<String> ritornaPropertyOutByModulo(String string) {
		ArrayList<String> arr = new ArrayList<String>();
		File file = new File(string);

		try {
		    Scanner scanner = new Scanner(file);

		    //now read the file line by line...
		    int lineNum = 0;
		    String temp ="";
		    while (scanner.hasNextLine()) {
		        String line = scanner.nextLine();
		        lineNum++;
		        Pattern pattern = Pattern.compile("setProperty\\(\"([*A-Za-z[_]0-9]*)\"*");
		        Matcher matcher = pattern.matcher(line);
		        while(matcher.find()) {
		        	temp = matcher.group(1);
		        	if(temp.equals("ERROR_DESCRIPTION")||temp.equals("RETURN_VALUE"))continue;
		        	if(temp.length()<2)continue;
		        	
		        	
		        	 arr.add(temp.toUpperCase());
		          
		        }
		    }
		    scanner.close();
		} catch(Exception e) { 
		    e.printStackTrace();
		}
	
	return removeDuplicates(arr);
		
	}
	
	
	public static ArrayList<String> removeDuplicates(ArrayList<String> list) 
    { 
  
        // Create a new ArrayList 
        ArrayList<String> newList = new ArrayList<String>(); 
  
        // Traverse through the first list 
        for (String element : list) { 
  
            // If this element is not present in newList 
            // then add it 
            if (!newList.contains(element)) { 
  
                newList.add(element); 
            } 
        } 
  
        // return the new list 
        return newList; 
    } 

	public static HashMap<String, String> ritornaModuli() throws IOException {
		HashMap<String, String> arr = new HashMap<String, String>();
		
		Files.walk(Paths.get("src/test/java/com/nttdata/qa/enel"))
		.filter(f -> (
//				f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/testqantt/colla"))
				
//				f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/testqantt/r2d"))
				 f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/testqantt"))
//				 f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/udb"))
//				|| f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/esitazionir2d"))
//				|| f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/creazionereferente"))
				)&& (f.toString().contains(".")) 
				)
//        .forEach(entry ->  arr.add(entry.getFileName().toString().replace(".java", "").toUpperCase()));
//		.forEach(entry ->  arr.add(entry.getParent().toString()+"\\"+entry.getFileName()));
		
		.forEach(entry -> arr.put(entry.getFileName().toString().replace(".java", "").toUpperCase(), entry.getParent().toString()+"\\"+entry.getFileName()));
		
		/*-- filtro per i file .avi --*/
//		FileExtFilter fef = new FileExtFilter ("java");
//
//		String[] list1 = dir.list (fef);
//
//		/*-- elenca tutti i file con quella estensione --*/

//		for(Entry<String, String> entry : arr.entrySet()) {
//		    String key = entry.getKey();
//		    String value = entry.getValue();
//		    System.out.println(key + " " +value);
//		    // do what you have to do here
//		    // In your case, another loop.
//		}
		
		return arr;
	}

	public static ArrayList<String> ritornaTutto() throws Exception {
		ArrayList<String> arr = new ArrayList<String>();
		
		Files.walk(Paths.get("src/test/java/com/nttdata/qa/enel"))
		.filter(f -> (
//				f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/testqantt/colla"))
				
				f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/testqantt/r2d"))
				|| f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/testqantt"))
				|| f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/udb"))
//				|| f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/esitazionir2d"))
//				|| f.getParent().equals(Paths.get("src/test/java/com/nttdata/qa/enel/creazionereferente"))
				)&& f.toString().contains(".")
				)
//        .forEach(entry ->  arr.add(entry.getFileName().toString().replace(".java", "").toUpperCase()));
		.forEach(entry ->  arr.add(entry.getParent().toString()+"\\"+entry.getFileName()));
		
		/*-- filtro per i file .avi --*/
//		FileExtFilter fef = new FileExtFilter ("java");
//
//		String[] list1 = dir.list (fef);
//
//		/*-- elenca tutti i file con quella estensione --*/

		for (String string : arr) {
			System.out.println(string);
			
			File file = new File(string);

			try {
			    Scanner scanner = new Scanner(file);

			    //now read the file line by line...
			    int lineNum = 0;
			    String temp ="";
			    while (scanner.hasNextLine()) {
			        String line = scanner.nextLine();
			        lineNum++;
			        Pattern pattern = Pattern.compile("getProperty\\(\"([*A-Z[_]0-9]*)\"*");
			        Matcher matcher = pattern.matcher(line);
			        while(matcher.find()) {
			        	temp = matcher.group(1);
			        	if(temp.equals("RUN_LOCALLY"))continue;
			        	if(temp.length()<2)continue;
			        	
			        	System.out.println("Sottosequenza : "+matcher.group());
			        	 System.out.println("Sottogruppo 1 : "+temp);
			          
			        }
			    }
			    scanner.close();
			} catch(Exception e) { 
			    e.printStackTrace();
			}
		}
		return arr;
		
	}
	
	
}
