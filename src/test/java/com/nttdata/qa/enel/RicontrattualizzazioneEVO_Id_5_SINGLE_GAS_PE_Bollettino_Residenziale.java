package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class RicontrattualizzazioneEVO_Id_5_SINGLE_GAS_PE_Bollettino_Residenziale {
		
    Properties prop;
    String nomeScenario = this.getClass().getSimpleName() + ".properties";
        
    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");
        prop.setProperty("ATTIVABILITA", "Attivabile");
        prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
        prop.setProperty("PRODOTTO", "SICURA GAS");
        prop.setProperty("ASSOCIAZIONE_PRODOTTI", "Y");
        prop.setProperty("CAP", "00184");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
        prop.setProperty("COMMODITY", "GAS");
        prop.setProperty("TIPO_OI_ORDER", "Commodity");
        prop.setProperty("RUN_LOCALLY", "Y");
        //prop.setProperty("VAS_EX_GAS", "SI");
        prop.setProperty("RIGA_DA_ESTRARRE", "12");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

//      prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
/*
        SetQueryRecuperaDatiRicontrattualizzazione119_res_gas.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyNAMECFPOD.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID24.main(args);
        ConfermaCheckList.main(args);
        CheckSezioneSelezioneFornituraRicontrattualizzazione.main(args);
        ConfermaRiepilogoOffertaRicontrattualizzazione.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);
        ConfiguraProdottoSWAEVOGasResidenziale.main(args);
        AssociazioneProdottiServizi.main(args);
        ConfermaFatturazioneElettronicaSWAEVO.main(args);
        IndirizziFatturazione.main(args);
        ConsensiEContatti.main(args);
        ModalitaFirmaAllaccioAttivazione.main(args);
        ConfermaOffertaAllaccioAttivazione.main(args);//preso in presti da allaccio e attivazioni
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaOffertaSubentro.main(args);
        CaricaEValidaDocumenti.main(args);

        //attendere 5 minuti
        Thread.sleep(300000);

        RecuperaStatusOffer.main(args);
        VerificaOffertaInChiusa.main(args);
        RecuperaStatusCase_and_Offer.main(args);
        VerificaOffertaInChiusa.main(args);
        VerificaSottostatoCaseOrderInviato.main(args);

        //query 115
        RecuperaStatusCaseItem.main(args);
        VerificaStatoInAttesa.main(args);

        //query 105
        RecuperaStatusOrderHeader.main(args);
        Thread.currentThread().sleep(6000);

        //query 103
        RecuperaStatusCase_and_Order.main(args);
        VerificaStatusR2DND.main(args);
        VerificaStatusUDBND.main(args);
        VerificaStatoCaseInAttesa.main(args);


        //query 103
        RecuperaStatusCase_and_Order.main(args);
        VerificaStatusSAP_ISUAttivazPod.main(args);//verifica stato OK sap ISU
        VerificaStatusSEMPREInAttesa.main(args);
        VerificaStatusOrdineEspletato.main(args);
        RecuperaItemAsset.main(args);
        VerificaItemAssetRicontrattualizzazione.main(args);
        VerificaAsset.main(args);


        //query 103
        RecuperaStatusCase_and_Order.main(args);
        VerificaStatusOrdineEspletato.main(args);
        VerificaCaseChiuso.main(args);
        VerificaSottoStatoCaseEspletato.main(args);
        VerificaStatusSEMPREAttivazPod.main(args);//sempre OK
*/
        //query 115
        RecuperaStatusCaseItem.main(args);
        VerificaStatoCaseItemEspletato.main(args);

    }

    @After
    public void fineTest() throws Exception {
		prop.load( new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

    }
}