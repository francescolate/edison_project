package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Caricamento_5OK_GAS_SUBENTRO;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SubentroEVO_Id_14 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName()+".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("LOCALITA_ISTAT", "VERONA");
        prop.setProperty("CITTA", "VERONA");
        prop.setProperty("CAP", "37121");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("MATRICOLA_CONTATORE", "1234567890");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA ROMA");
        prop.setProperty("CIVICO", "123");
        prop.setProperty("PROVINCIA_COMUNE", "VERONA");
        prop.setProperty("USO", "Uso Abitativo");
        prop.setProperty("ISTAT", "Y");
        prop.setProperty("COMMODITY", "GAS3");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CLIENTE_BUSINESS", "N");
        prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "NO");
        
        prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
        prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - INVIATA");
        prop.setProperty("STATO_RICHIESTA", "INVIATA");
        prop.setProperty("STATO_R2D", "N.D.");
        prop.setProperty("STATO_SAP", "N.D.");
        prop.setProperty("STATO_SEMPRE", "N.D.");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("RUN_LOCALLY", "Y");
        //prop.setProperty("CODICE_FISCALE", "SDASDA59B25H264B");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO_FIRMA", "POSTA");
        prop.setProperty("RIGA_DA_ESTRARRE", "33");
       
        prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		
		prop.setProperty("PRODOTTO", "SICURA GAS");
        //prop.setProperty("VAS_EX_GAS", "SI");

		// variabili aggiunte 
		
        prop.setProperty("CIFRE_CONTATORE", "5");
        prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
        prop.setProperty("TIPO_PDR", "0");
        prop.setProperty("PRELIEVO_ANNUO", "1000");
        prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
        prop.setProperty("TIPO_LETTURA", "Effettiva");
        prop.setProperty("GESTIONE_APPUNTAMENTO", "Y");
   
        return prop;
    }


    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        RecuperaPodNonEsistente.main(args);
        SetSubentroquery_subentro_id14.main(args);
        RecuperaDatiWorkbench.main(args);       
        SetPropertyCF.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID17.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);

		SelezioneUsoFornitura.main(args);
		CommodityGasResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        ConfermaFatturazioneElettronicaSWAEVO.main(args);
		PagamentoBollettinoPostale.main(args);
		ConfermaScontiBonusEVO.main(args);
        ConfiguraProdottoGasResidenzialeSubentro.main(args);
        GestioneCVPAllaccioAttivazione.main(args);
		ConsensiEContattiSubentro.main(args);
        ModalitaFirmaSubentro.main(args);
		ConfermaOffertaAllaccioAttivazione.main(args);
        
	    LoginSalesForce.main(args);
		RicercaOffertaSubentro.main(args);     
		CaricaEValidaDocumenti.main(args);
        
        SetSubentroProperty.main(args);
        LoginSalesForce.main(args);
        RicercaRichiesta.main(args);
        //VerificheRichiestaDaPod.main(args);
	    RecuperaOrderIDDaPod.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);

        //Dati R2d    
        SetSubentro15R2DGAS.main(args);
        LoginR2D_GAS.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodIniziali_GAS.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_InvioPSPortale_1OK_GAS.main(args);
        R2D_CaricamentoEsiti_3OK_GAS.main(args);
        R2D_CaricamentoEsiti_5OK_Standard_GAS.main(args);     
        R2D_VerifichePodFinali_GAS.main(args);   
        
        //Da eseguire dopo qualche ora        
        
        SetSubentroBatch.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaRichiesta.main(args);
        VerificheRichiestaDaPod.main(args);

    }

    @After
    public void fineTest() throws Exception {
    	
         prop.load(new FileInputStream(nomeScenario));
         this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
         ReportUtility.reportToServer(this.prop);
         
    }

}
