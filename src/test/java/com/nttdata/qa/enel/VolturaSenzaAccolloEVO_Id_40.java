package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaSenzaAccolloRes;
import com.nttdata.qa.enel.testqantt.CompilaIndirizzoFibra;
import com.nttdata.qa.enel.testqantt.CompilaSezioneIndirizziVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ContattiEConsensiVolturaSenzaAccolloRes;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GetNumeroCellulareOTP;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.MetodoDiPagamentoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.ModalitàFirmaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Voltura;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaOtp;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoConfermOffertaWebVSA;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaWebPrivatoVoltura;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaNumeroContratto;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyFor_VSA;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Globale_Richiesta_InLavorazione;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SezioneCVPVoltura;
import com.nttdata.qa.enel.testqantt.SezioneFatturazioneElettronicaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.VerificaAlertRosso;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaStatoFibra;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;

/**

Cliente: Residenziale

Descrizione: Accedere alla piattaforma SFDC ed avviare un processo di Voltura 
Senza Accollo EVO da canale pe su cliente di tipo Residenziale con scelta fornitura ELE.
Verificare la sezione modalità firma selezionando come Modalità "Digital"
Procedere fino al completamento del processo
Risultato Atteso: Corretto espletamento della pratica E2E dopo la selezione della modalità firma "Digital"

 */
public class VolturaSenzaAccolloEVO_Id_40 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();

		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.setProperty("TIPO_UTENZA", "PE");
		
		prop.setProperty("QUERY", SetPropertyFor_VSA.recupera_CF_POD_per_Volture_id40);

		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO"); //selezione processo
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("MERCATO", "Libero"); //Selezione mercato
		prop.setProperty("TIPOCLIENTE","RESIDENZIALE");

		//CLIENTE ENTRANTE
		prop.setProperty("CODICE_FISCALE", "CSTRRT80C07F257Y"); //non modificare CF entrante - registrato per ricevere mail su TP2

		

		//CLIENTE USCENTE e SELEZIONE FORNITURA		
		prop.setProperty("CODICE_FISCALE_CL_USCENTE", "SPNBNC80C68F205M");	 //Lasciare stringa vuota se si vuole che il dato venga estratto con query
		prop.setProperty("POD", "IT002E4871222A"); 





		//IDENTIFICAZIONE INTERLOCUTORE
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");




		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");//Valorizzazione uso fornitura con testing.crm.auto.mation@gmail.com
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE"); 
		prop.setProperty("CONSUMO","3");
		prop.setProperty("FLAG_RESIDENTE","SI");

		//METODO DI PAGAMENTO
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");




		//INDIRIZZO 
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("CAP", "27036");


		//CONFIGURAZIONE PRODOTTO ELETTRICO
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("ELIMINA_VAS", "SI");

		//CONFIGURAZIONE FIBRA
		prop.setProperty("OPZIONE_FIBRA","SI");
		prop.setProperty("INDIRIZZO_FIBRA_OK","SI");
		prop.setProperty("TELEFONO_FIBRA", "06221234567");
		prop.setProperty("REGIONE_FIBRA", "LAZIO");
		prop.setProperty("PROVINCIA_FIBRA", "ROMA");
		prop.setProperty("CODICE_MIGRAZIONE", "XXXXXX");
		prop.setProperty("CITTA_FIBRA", "ROMA");
		prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
		prop.setProperty("CIVICO_FIBRA", "196");
		prop.setProperty("COMPILA_EMAIL_FIBRA", "Y");
		prop.setProperty("EMAIL_FIBRA", "testing.crm.automation1002@gmailEnel.com");
		prop.setProperty("SCELTA_MELITA","NO");
		prop.setProperty("CONSENSO_DATI_FIBRA_OK","SI");
		
		//TODO: selezionare cellulare
		prop.setProperty("CELLULARE","3467656345"); //TODO: FR capire come gestire il cellulare - la properietà vieme usata anche da compila indirizzo fibra



		//MODALITA FIRMA
		prop.setProperty("MODALITA_FIRMA","DIGITAL");
		prop.setProperty("CANALE","EMAIL");
		//prop.setProperty("EMAIL", "testing.crm.auto.mation@gmail.com");
		prop.setProperty("EMAIL","testing.crm.automation@gmail.com");
		prop.setProperty("EFFETTUARE_CONFERMA","Y");
		
		
		prop.setProperty("RECUPERA_OI_RICHIESTA","Y");
		
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Documento di riconoscimento");
		
		//VERIFICA STATO RICHIESTA POST EMISSIONE ORDINE
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "BOZZA");
		prop.setProperty("STATO_RICHIESTA", "BOZZA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - NULL / DRAFT");


		prop.setProperty("DISALIMENTABILITA","SI");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");


		
		
		prop.setProperty("RIGA_DA_ESTRARRE", "6");
		
		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	};



	@Test
	public void eseguiTest() throws Exception{
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		/*** decommentare SetPropertyFor_VSA se  il CF e il POD del  CL uscente non non vengono impostati nelle property*/
		//SetPropertyFor_VSA.main(args);
		
		//AVVIO ORDER ENTRY
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);	
		CheckSezioneSelezioneFornitura.main(args);
		RiepilogoOffertaVoltura.main(args);
		ConfermaRiepilogoOfferta.main(args);
		SelezioneUsoFornitura.main(args);
		CompilaDatiFornituraVolturaSenzaAccolloRes.main(args);
		MetodoDiPagamentoOffertaVoltura.main(args);
		ConfermaScontiBonusEVO.main(args);	
		CompilaSezioneIndirizziVolturaSenzaAccollo.main(args);		
		ConfiguraProdottoElettricoResidenziale.main(args);
		SezioneCVPVoltura.main(args);
		ContattiEConsensiVolturaSenzaAccolloRes.main(args);
		SezioneFatturazioneElettronicaVolturaSenzaAccollo.main(args); 
		CompilaIndirizzoFibra.main(args);
		ModalitàFirmaVolturaSenzaAccollo.main(args);	
		CheckRiepilogoOfferta.main(args);
		VerificheRichiestaDaPod.main(args);//lo stato della pratica risulta in "BOZZA"Lo stato della pratica risulta in "Bozza"
		SalvaNumeroContratto.main(args);

		// RICOMINCIARE DA QUI
		RecuperaLinkTP2Voltura.main(args);
		SetUtenzaMailResidenziale.main(args);
		
		//sostituisce 
		LoginPortalTP2.main(args);  
		//RiepilogoOffertaWebPrivatoVoltura.main(args);    
		RiepilogoConfermOffertaWebVSA.main(args);  
		
		//Quando viene effettuato il login all'area WEB USER e PASSWORD vengono modificate- pertanto per continuare il test è necessario reimpostarle nelle property
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.store(new FileOutputStream(nomeScenario), null);
		
		//VERIFICA LO STATO DELLA RICHIESTA SIA IN  “INVIATA” 
		SetPropertyVSA_Stato_Globale_Richiesta_InLavorazione.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
		
		//CARICAMENTO E VALIDAZIONE DOCUMENTI
		CaricaEValidaDocumentiVoltureSenzaAccollo.main(args); //TODO: è corretto ricaricare i documenti - questi dovrebbero essere già caricati dall'area web
		VerificaDocumento.main(args);


		VerificheRichiestaDaPod.main(args); 
		VerificheStatoRichiesta.main(args);
		RecuperaOrderIDDaPod.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);
		//VERIFICA STATO FIBRA
		VerificaStatoFibra.main(args);
		
		//ESITAZIONE R2D 
		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);
		LoginR2D_ELE.main(args);
		R2D_VerifichePodIniziali_ELE.main(args);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);



		//Da eseguire dopo qualche ora
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args); 
		VerificheStatoRichiesta.main(args);	



	};




	@After
	public void fineTest() throws Exception{
		/*
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);*/

	};
}
