package com.nttdata.qa.enel;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniDettagliSpedizioneCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GestioneReferentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID21;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoRIDEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaLinkItemCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatoDocumentModelloPlicoCopiaDocumentiPostBatch;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusModelloPlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.RecuperoVerificaModelloItemCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaNumeroDocumento;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaSezioneTipologiaDocumenti;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaCasePlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaKitContrattualeCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaKitContrattualePadreCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaModelloCorpoEmailGenericoCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaModelloOggettoEmailGenericoCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaNumeroRichiestaCaseCopiaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaSelezioneFornitureCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoDocumentInviatoModelloPlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoCaseCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoDocumentoWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;

import com.nttdata.qa.enel.testqantt.VerificheOrdinePostQuery;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class CopiaDocumentiEVO_Id_2 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
		prop.setProperty("NOME_UTENTE", "723535469");
		prop.setProperty("ID_DOCUMENTO", "0332118324");
		prop.setProperty("ID_DOCUMENTO_2", "0332118327");
		prop.setProperty("CANALE_INVIO", "EMAIL");		
		prop.setProperty("INDIRIZZO_EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("TIPOLOGIA_DOC", "KIT_CONTRUATTUALE");
		prop.setProperty("RUN_LOCALLY", "Y");

	}

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load( new FileInputStream(nomeScenario));
/*
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID21.main(args);
		VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
		VerificaSelezioneFornitureCopiaDocumenti.main(args);
		SelezionaSezioneTipologiaDocumenti.main(args);
		VerificaKitContrattualePadreCopiaDocumenti.main(args);
		VerificaKitContrattualeCopiaDocumenti.main(args);
		AzioniDettagliSpedizioneCopiaDocumenti.main(args);	
		SalvaNumeroDocumento.main(args);	

		//verifica case: Stato - In Lavorazione, Sottostato - Inviato  
		VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);
		VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench.main(args);		
		//verifica presenza reconds document associati al Case
		RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
		VerificaCasePlicoCopiaDocumenti.main(args);
		//verifica presenza item con campo Link valorizzato
		RecuperaLinkItemCaseCopiaDocumenti.main(args);		
		//verifica presenza item con Modello = Oggetto Email Generic
		VerificaModelloOggettoEmailGenericoCopiaDocumentiWorkbench.main(args);
		//verifica presenza item con Modello = Corpo Email Generic
	    VerificaModelloCorpoEmailGenericoCopiaDocumentiWorkbench.main(args);

		//da eseguire dopo batch
		//verifica avanzamento document con Modello = Plico in Stato=Inviato
		RecuperaStatoDocumentModelloPlicoCopiaDocumentiPostBatch.main(args);
		VerificaStatoDocumentInviatoModelloPlicoCopiaDocumenti.main(args);*/
		//verifica case: Stato - Chiuso, Sottostato - Ricevuto  
		VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);
		VerificaStatoSottostatoDocumentoWorkbench.main(args);
	}

	@After
	public void fineTest() throws Exception {
		prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}
