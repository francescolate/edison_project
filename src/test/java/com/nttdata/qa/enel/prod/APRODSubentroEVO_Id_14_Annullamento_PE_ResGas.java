package com.nttdata.qa.enel.prod;

import java.io.FileInputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullaPredisposizionePresa;

import core.Decrypt;

public class APRODSubentroEVO_Id_14_Annullamento_PE_ResGas {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName()+".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
//		prop.setProperty("USERNAME","ENELINT\\AF09533");
		prop.setProperty("USERNAME","ENELINT\\AE82870");
//		prop.setProperty("PASSWORD",Decrypt.decrypt("ajcqB/3XGY5lIfZXaMs0EA=="));
		prop.setProperty("PASSWORD",Decrypt.decrypt("B2W1HIfYVTPB1jO6OaE1tA=="));
		prop.setProperty("CODICE_FISCALE", "PRKPTR72B03H502P");
		prop.setProperty("TIPO_CLIENTE", "Residenziale");
		prop.setProperty("TIPO_UTENZA","PE");
        prop.setProperty("LOCALITA_ISTAT", "BOLOGNA");
        prop.setProperty("CITTA", "BOLOGNA");
        prop.setProperty("CAP", "40121");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("INDIRIZZO", "VIA FRATELLI CAIROLI");
        prop.setProperty("CIVICO", "4");
        prop.setProperty("PROVINCIA_COMUNE", "BOLOGNA");
        prop.setProperty("USO", "Uso Abitativo");
        prop.setProperty("ISTAT", "Y");
        prop.setProperty("COMMODITY", "GAS");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CLIENTE_BUSINESS", "N");
        prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "NO");
        prop.setProperty("PRODOTTO", "SICURA_GAS");

		prop.setProperty("STATO_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "OFFER - ANNULLATA");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("SEZIONE_ISTAT", "N");
       
        prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");

		// variabili aggiunte 
        prop.setProperty("CIFRE_CONTATORE", "5");
        prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
        prop.setProperty("TIPO_PDR", "0");
        prop.setProperty("PRELIEVO_ANNUO", "1000");
        prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
        prop.setProperty("LETTURA_CONTATORE", "15000");
        prop.setProperty("TIPO_LETTURA", "Effettiva");
        prop.setProperty("COEFFICIENTE_C", "1,02");
        
        prop.setProperty("POD", "03088637915000");
        prop.setProperty("TIPO", "SUBENTRO");
		prop.setProperty("STATO_POD", "N.D.");
        
        
        
        return prop;
    }


    @Test
    public void eseguiTest() throws Exception {

//        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
/*
        LoginSalesForceProduzione.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazionePerReferenteEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID17.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentro.main(args);
		VerificaRiepilogoOffertaVolturaConAccollo.main(args);
		SelezioneUsoFornitura.main(args);
		CommodityGasResidenzialeSubentro.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostale.main(args);
		ConfermaScontiBonusEVO.main(args);
		ConfiguraProdottoSWAEVOGasResidenziale.main(args);
		ConsensiEContattiSubentro.main(args);
		ModalitaFirmaECanaleInvioSubentro.main(args);
		GestioneCVPAllaccioAttivazione.main(args);
		AnnullaOffertaGenerico.main(args);
		
		//startare 1 minuti dopo l'emissione
		Thread.currentThread().sleep(60000);
*/		
		VerificheRichiestaAnnullaPredisposizionePresa.main(args);
    }

    @After
    public void fineTest() throws Exception {
    }

}
