package com.nttdata.qa.enel.prod;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazionePerReferenteEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForceProduzione;
import com.nttdata.qa.enel.testqantt.ProcessoPredisposizionePresaEVOEle;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SceltaProcessoPredisposizionePresaEVOEle;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullaPredisposizionePresa;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import core.Decrypt;


public class APRODPredisposizionePresa_EVO_id01b_Annullamento_S2S_BusEle {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME","ENELINT\\AF45318");
		prop.setProperty("PASSWORD",Decrypt.decrypt("WIpeqMCLDYHSuddkl+P2jw=="));
		prop.setProperty("TIPO_UTENZA","S2S");
		prop.setProperty("ANNULLA_OFFERTA","Y");
		prop.setProperty("STATUS_UBIEST", "ON");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "06796070560");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "PRKPTR90L19B936Z");
		prop.setProperty("SCELTA_TIPO_CLIENTE", "Y");
		prop.setProperty("TIPO_CLIENTE", "Azienda");
//		prop.setProperty("COMPLETE_NAME", "PETER PARKER COMPANY");
		prop.setProperty("COMPLETE_NAME", "PETER PARKER SPA");
		
		
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("CAP", "00136");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA ALFREDO SERRANTI");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "PREDISPOSIZIONE_PRESA");
		
		prop.setProperty("TIPO", "ALLACCIO");
		prop.setProperty("STATO_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_POD", "N.D.");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("POD", "N.D.");

		//nuovi prop
		prop.setProperty("USO_FORNITURA", "Altri usi");
		prop.setProperty("TENSIONE_RICHIESTA", "220");
		prop.setProperty("POTENZA_RICHIESTA", "4.5");
		prop.setProperty("FATTURAZIONE_ANTICIPATA", "NO");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		LoginSalesForceProduzione.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazionePerReferenteEVO.main(args);
		SceltaProcessoPredisposizionePresaEVOEle.main(args);
		ProcessoPredisposizionePresaEVOEle.main(args);
		
		//startare 3 minuti dopo l'emissione
		Thread.currentThread().sleep(180000); 
		
		VerificheRichiestaAnnullaPredisposizionePresa.main(args);
		
		
	}

	@After
	public void tearDown() throws Exception{

	              }


}



