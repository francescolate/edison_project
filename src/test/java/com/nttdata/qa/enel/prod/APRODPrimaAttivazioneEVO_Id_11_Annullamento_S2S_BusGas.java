package com.nttdata.qa.enel.prod;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AnnullaOffertaGenerico;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazionePerReferenteEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.CommodityGasNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraGasPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GestioneReferentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForceProduzione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullata;

import core.Decrypt;

public class APRODPrimaAttivazioneEVO_Id_11_Annullamento_S2S_BusGas {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("STATUS_UBIEST", "ON");
		prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME","ENELINT\\AF45318");
		prop.setProperty("PASSWORD",Decrypt.decrypt("WIpeqMCLDYHSuddkl+P2jw=="));
		prop.setProperty("CODICE_FISCALE", "06796070560");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "PRKPTR90L19B936Z");
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("COMMODITY", "GAS4");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("TIPO_CLIENTE", "Non Residenziale");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("REGIONE", "EMILIA ROMAGNA");
		prop.setProperty("PROVINCIA", "BOLOGNA");
		prop.setProperty("CITTA", "BOLOGNA");
		prop.setProperty("INDIRIZZO", "VIA FRATELLI CAIROLI");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CAP", "40121");
		prop.setProperty("CAP_FORZATURA", "40121");
		prop.setProperty("CITTA_FORZATURA", "BOLOGNA");
		prop.setProperty("MATRICOLA_CONTATORE", "Y");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ACQUA");
		prop.setProperty("CATEGORIA_CONSUMO", "Alberghi");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLASSE_PRELIEVO", "7 giorni");
		prop.setProperty("CATEGORIA_MARKETING", "ALBERGHI");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "PRODUZIONE");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "Y");
		prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
		prop.setProperty("CODICE_UFFICIO", "0000000");
		
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("PROCESSO", "Avvio Prima Attivazione EVO");
        prop.setProperty("PRODOTTO", "NEW_ANNO_SICURO_GAS");
        prop.setProperty("CORPORATE", "N");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("TELEFONO", "3394675542");
		prop.setProperty("CELLULARE", "3394675542");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("RUN_LOCALLY", "Y");	
		prop.setProperty("POD", "03088637914001");
		
		prop.setProperty("TIPO", "PRIMA ATTIVAZIONE");
		prop.setProperty("STATO_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "OFFER - ANNULLATA");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");

	};

	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		LoginSalesForceProduzione.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazionePerReferenteEVO.main(args);
		ProcessoPrimaAttivazioneNonResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID14.main(args);
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);
		CompilaDatiFornituraGasPrimaAttivazione.main(args);
		VerificaCreazioneOffertaPrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		GestioneReferentePrimaAttivazioneEVO.main(args);
		CommodityGasNonResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args); 
		SplitPaymentSwaEVO.main(args);
		CigCugpSwaEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoSWAEVOGasNonResidenziale.main(args);
		GestioneCVPPrimaAttivazione.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);
		AnnullaOffertaGenerico.main(args);
		VerificheRichiestaAnnullata.main(args);
	};

	@After
	public void fineTest() throws Exception {

	};

}
