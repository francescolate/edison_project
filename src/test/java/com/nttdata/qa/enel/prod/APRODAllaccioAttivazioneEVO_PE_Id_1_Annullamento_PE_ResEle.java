package com.nttdata.qa.enel.prod;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AnnullaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraAllaccioAttivazioneUsoAbitativoRes;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForceProduzione;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoAllaccioAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer_senza_POD;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoAllaccioAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.VerificaCaseChiuso;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaSottostatoCaseOfferAnnullato;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullata;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import core.Decrypt;

public class APRODAllaccioAttivazioneEVO_PE_Id_1_Annullamento_PE_ResEle {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
//			prop.setProperty("USERNAME","ENELINT\\AF09533");
			prop.setProperty("USERNAME","ENELINT\\AE82870");
//			prop.setProperty("PASSWORD",Decrypt.decrypt("ajcqB/3XGY5lIfZXaMs0EA=="));
			prop.setProperty("PASSWORD",Decrypt.decrypt("B2W1HIfYVTPB1jO6OaE1tA=="));
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
//			prop.setProperty("CODICE_FISCALE", "PRKPTR72B03H502P");
			prop.setProperty("CODICE_FISCALE", "PRKPTR80R27F839G");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "PE");
			prop.setProperty("TIPO_CLIENTE", "Residenziale");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
			prop.setProperty("MERCATO", "Libero");
			prop.setProperty("CAP", "00136");
			prop.setProperty("REGIONE", "LAZIO");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CITTA", "ROMA");
			prop.setProperty("INDIRIZZO", "VIA ALFREDO SERRANTI");
			prop.setProperty("CIVICO", "4");
			prop.setProperty("USO_FORNITURA", "Uso Abitativo");
			prop.setProperty("RESIDENTE", "NO");
			prop.setProperty("USO_ENERGIA", "Ordinaria");
			prop.setProperty("TITOLARITA", "Uso/Abitazione");
			prop.setProperty("TENSIONE", "220");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "3");
			prop.setProperty("DISALIMENTABILITA", "SI");
			prop.setProperty("TIPO_MISURATORE", "Non Orario");
			prop.setProperty("CONSUMO_ANNUO", "1000");
			prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
//			prop.setProperty("PRODOTTO", "SICURA_GAS");
			prop.setProperty("PRODOTTO", "Scegli Tu");
			prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
			prop.setProperty("TIPO_OI_ORDER", "Commodity");
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("CANALE_INVIO", "POSTA");
			prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
			
			prop.setProperty("TIPO", "ALLACCIO E ATTIVAZIONE");
			prop.setProperty("STATO_RICHIESTA", "CHIUSO");
			prop.setProperty("SOTTO_STATO_RICHIESTA", "OFFER - ANNULLATA");
			prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
			prop.setProperty("STATO_R2D", "N.D.");
			prop.setProperty("STATO_SAP", "N.D.");
			prop.setProperty("STATO_SEMPRE", "N.D.");
			
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);

			LoginSalesForceProduzione.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoAllaccioAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoAllaccioAttivazioneEVO.main(args);
			InserimentoIndirizzoEsecuzioneLavoriEVO.main(args);
			VerificaCreazioneOffertaAllaccioAttivazione.main(args);
			VerificaRiepilogoOffertaAllaccioAttivazione.main(args);
			SelezioneUsoFornitura.main(args);
			CompilaDatiFornituraAllaccioAttivazioneUsoAbitativoRes.main(args);
			ConfermaIndirizziAllaccioAttivazione.main(args);
			ConfermaFatturazioneElettronicaAllaccioAttivazione.main(args);
			PagamentoBollettinoPostaleEVO.main(args);
			ConfermaScontiBonusEVO.main(args);
			ConfiguraProdottoElettricoResidenziale.main(args);
			GestioneCVPAllaccioAttivazione.main(args);
			ConsensiEContattiAllaccioAttivazione.main(args);
			ModalitaFirmaAllaccioAttivazione.main(args);
			AnnullaOffertaAllaccioAttivazione.main(args);
			VerificheRichiestaAnnullata.main(args);

		};
		
//		@After
//	    public void fineTest() throws Exception{
//            String args[] = {nomeScenario};
//            InputStream in = new FileInputStream(nomeScenario);
//            prop.load(in);
//            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//            ReportUtility.reportToServer(this.prop);
//      };

		
		
		
		
		
	

}
