package com.nttdata.qa.enel.prod;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AnnullaOffertaGenerico;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazionePerReferenteEVO;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureSubentro;
import com.nttdata.qa.enel.testqantt.LoginSalesForceProduzione;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaECanaleInvioSubentro;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostale;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SezioneMercatoSubentro;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaVolturaConAccollo;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullaPredisposizionePresa;
import com.nttdata.qa.enel.testqantt.checkSezioneContattiEConsensi;

import core.Decrypt;

public class APRODSubentroEVO_Id_13_Annullamento_S2S_BusEle {
	Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
		prop.setProperty("USERNAME","ENELINT\\AF45318");
		prop.setProperty("PASSWORD",Decrypt.decrypt("WIpeqMCLDYHSuddkl+P2jw=="));
		prop.setProperty("PROCESSO", "Avvio Subentro EVO");
		prop.setProperty("CODICE_FISCALE", "06796070560");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "PRKPTR90L19B936Z");
		prop.setProperty("TIPO_CLIENTE", "Azienda");
		prop.setProperty("COMPLETE_NAME", "PETER PARKER SPA");
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "00136");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ALTRI SERVIZI");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");
        prop.setProperty("CONSUMO_ANNUO", "10");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("INDIRIZZO", "VIA ALFREDO SERRANTI");
		prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("TIPO_OPERAZIONE", "SUBENTRO_EVO");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("POD", "ENERGIA");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CLIENTE_BUSINESS", "Y");
        prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
        prop.setProperty("SEZIONI_TOTALI", "16");
        prop.setProperty("CONTA", "0");
        prop.setProperty("PRODOTTO", "SENZA ORARI LUCE");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("VERDE", "N");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("POD", "IT002E9900100A");
		prop.setProperty("STATO_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "OFFER - ANNULLATA");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{
		
	    prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		
		prop.load(in);

		LoginSalesForceProduzione.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazionePerReferenteEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentro.main(args);

        VerificaRiepilogoOffertaVolturaConAccollo.main(args);
        SelezioneUsoFornitura.main(args);
        CommodityEleNonResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);
        SplitPaymentEVO.main(args);
    	CigCupEVO.main(args);
        ConfiguraProdottoElettricoNonResidenziale.main(args);
        checkSezioneContattiEConsensi.main(args);
        ModalitaFirmaECanaleInvioSubentro.main(args);
        GestioneCVPAllaccioAttivazione.main(args);
		
		AnnullaOffertaGenerico.main(args);
		
		//startare 1 minuti dopo l'emissione
		Thread.currentThread().sleep(60000);
		
		VerificheRichiestaAnnullaPredisposizionePresa.main(args);
	};
	
	@After
    public void fineTest() throws Exception{

	}
}
