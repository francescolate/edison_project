package com.nttdata.qa.enel.prod;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazionePerReferenteEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForceProduzione;
import com.nttdata.qa.enel.testqantt.ProcessoPredisposizionePresaEVOGas;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SceltaProcessoPredisposizionePresaEVOGas;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullaPredisposizionePresa;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaPredisposizionePresaEVO;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import core.Decrypt;


public class APRODPredisposizionePresa_EVO_id07_Annullamento_S2S_BusGas {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME","ENELINT\\AF45318");
		prop.setProperty("PASSWORD",Decrypt.decrypt("WIpeqMCLDYHSuddkl+P2jw=="));
		prop.setProperty("TIPO_UTENZA","S2S");
		prop.setProperty("ANNULLA_OFFERTA","Y");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "06796070560");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "PRKPTR90L19B936Z");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("REGIONE", "EMILIA ROMAGNA");
		prop.setProperty("PROVINCIA", "BOLOGNA");
		prop.setProperty("CITTA", "BOLOGNA");
		prop.setProperty("INDIRIZZO", "VIA FRATELLI CAIROLI");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CAP", "40121");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "POSTA");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "PREDISPOSIZIONE_PRESA");
		
		prop.setProperty("TIPO", "ALLACCIO");
		prop.setProperty("STATO_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_POD", "N.D.");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("POD", "N.D.");

		//nuovi prop
		prop.setProperty("POTENZIALITA_1", "20000");
		prop.setProperty("POTENZIALITA_2", "20000");
		prop.setProperty("POTENZIALITA_3", "20000");
		prop.setProperty("NUMERO_PDR_TIPO_1", "1");
		prop.setProperty("NUMERO_PDR_TIPO_2", "1");
		prop.setProperty("NUMERO_PDR_TIPO_3", "1");

		prop.setProperty("FATTURAZIONE_ANTICIPATA", "NO");
		prop.setProperty("FLAG_136", "NO");
		prop.setProperty("SPLIT_PAYMENT", "NO");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		LoginSalesForceProduzione.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazionePerReferenteEVO.main(args);
		SceltaProcessoPredisposizionePresaEVOGas.main(args);
		ProcessoPredisposizionePresaEVOGas.main(args);
		//startare 1 minuto dopo l'emissione
		Thread.currentThread().sleep(60000); 

		VerificheRichiestaAnnullaPredisposizionePresa.main(args);
		
		
	}

	@After
	public void tearDown() throws Exception{
//	        InputStream in = new FileInputStream(nomeScenario);
//	        prop.load(in);
//	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//	        ReportUtility.reportToServer(this.prop);
	              }


}



