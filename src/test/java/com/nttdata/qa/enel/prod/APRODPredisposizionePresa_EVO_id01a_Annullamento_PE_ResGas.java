package com.nttdata.qa.enel.prod;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForceProduzione;
import com.nttdata.qa.enel.testqantt.ProcessoPredisposizionePresaEVOGas;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SceltaProcessoPredisposizionePresaEVOGas;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullaPredisposizionePresa;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import core.Decrypt;


public class APRODPredisposizionePresa_EVO_id01a_Annullamento_PE_ResGas {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt.my.salesforce.com");
//		prop.setProperty("USERNAME","ENELINT\\AF09533");
		prop.setProperty("USERNAME","ENELINT\\AE82870");
//		prop.setProperty("PASSWORD",Decrypt.decrypt("ajcqB/3XGY5lIfZXaMs0EA=="));
		prop.setProperty("PASSWORD",Decrypt.decrypt("B2W1HIfYVTPB1jO6OaE1tA=="));
		prop.setProperty("TIPO_UTENZA","PE");
		prop.setProperty("ANNULLA_OFFERTA","Y");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "PRKPTR72B03H502P");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("CAP", "40121");
		prop.setProperty("REGIONE", "EMILIA ROMAGNA");
		prop.setProperty("PROVINCIA", "BOLOGNA");
		prop.setProperty("CITTA", "BOLOGNA");
		prop.setProperty("INDIRIZZO", "VIA FRATELLI CAIROLI");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "PREDISPOSIZIONE_PRESA");
		
		prop.setProperty("TIPO", "ALLACCIO");
		prop.setProperty("STATO_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");
		prop.setProperty("STATO_POD", "N.D.");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");

		//nuovi prop
		prop.setProperty("POTENZIALITA_1", "20000");
		prop.setProperty("NUMERO_PDR_TIPO_1", "1");
		prop.setProperty("NUMERO_PDR_TIPO_2", "");
		prop.setProperty("NUMERO_PDR_TIPO_3", "");

		prop.setProperty("FATTURAZIONE_ANTICIPATA", "NO");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
			
		LoginSalesForceProduzione.main(args);
		
//		SbloccaTab.main(args);
//		
//		CercaClientePerNuovaInterazioneEVO.main(args);

		SceltaProcessoPredisposizionePresaEVOGas.main(args);
		ProcessoPredisposizionePresaEVOGas.main(args);
		
		//startare 3 minuti dopo l'emissione
		Thread.currentThread().sleep(180000); 
		
		VerificheRichiestaAnnullaPredisposizionePresa.main(args);
		
		
	}

//    @After
//    public void tearDown() throws Exception{
//            InputStream in = new FileInputStream(nomeScenario);
//            prop.load(in);
//            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//            ReportUtility.reportToServer(this.prop);
//    }

}



