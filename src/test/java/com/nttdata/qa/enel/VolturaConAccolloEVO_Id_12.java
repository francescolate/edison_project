package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class VolturaConAccolloEVO_Id_12 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("CODICE_FISCALE", "00407150788");
		prop.setProperty("CODICE_FISCALE_CL_USCENTE", "86133090919"); //PA 
		prop.setProperty("POD", "IT002E8706710A");
		
		
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("CAUSALE", "Trasformazione/Fusione Azienda");
		prop.setProperty("PROCESSO", "Avvio Voltura con accollo EVO");
		prop.setProperty("CL_USCENTE_DENOMINAZIONE","0");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Illuminazione Pubblica");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("RUN_LOCALLY","Y");
		return prop;
	}


	@Test
    public void eseguiTest() throws Exception{
		
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));

		//SetVolturaConAccolloEVOrecupera_CF_POD_per_Volture12.main(args);
		//RecuperaDatiWorkbench.main(args);
		//SetPropertyVolturaConAccolloEVONOMECFPOD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID23.main(args);
		SelezioneMercatoVolturaConAccolloBSN.main(args);
		CheckSezioneClienteUscenteAndInsertPIVAVolturaConAccollo.main(args);
		CheckSezioneSelezioneFornitura.main(args);
		CreaOffertaVerificaCampiVolturaConAccolloBSN.main(args);
		SceltaReferenteVolturaConAccolloBSN.main(args);
		CheckDatiFornituraVolturaConAccolloIlluminazPubblicaBSN.main(args);
		CreaOffertaGestioneAnnullaVolturaConAccollo.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		Utility.takeSnapShotOnKo(prop, nomeScenario);
		
	};
}
