package com.nttdata.qa.enel;


import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class CopiaDocumentiEVO_Id_25 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void inizioTest() throws Exception {
        this.prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
        prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
        prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
        prop.setProperty("STATO_DOCUMENTO", "CHIUSO");
        prop.setProperty("SOTTOSTATO_DOCUMENTO", "ANNULLATO");
        prop.setProperty("RUN_LOCALLY", "Y");
    }

    ;

    @Test
    public void eseguiTest() throws Exception {

//      prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load( new FileInputStream(nomeScenario));
/*
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID21.main(args);
        GestioneTastoAnnullaSezioneSelezForniture.main(args);
        VerificaStatoSottostatoCaseCopiaDocumenti.main(args);*/
    }


    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
