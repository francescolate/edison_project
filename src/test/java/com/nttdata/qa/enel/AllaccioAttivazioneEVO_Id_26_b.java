package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CaricaDocumentiSenzaValidazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneRes;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoAllaccioAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_senza_POD;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOffertaDaTab;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoAllaccioAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.ValidazionePreventivo;
import com.nttdata.qa.enel.testqantt.VerificaCaseInLavorazione_senza_POD;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOIOdineStatoPreventivoAccettato;
import com.nttdata.qa.enel.testqantt.VerificaOIOdineStatoPreventivoInviato;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInLavorazione_senza_POD;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineRichiestaPreventivo;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DInviatoAlDistributore;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPreventivoDaCompletare;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoPreventivo_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerificaMancataCreazioneRichiesta_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIntermedie_ELE;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DCodDTE01;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DCodDTN02;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DPreventivoSottoSoglia;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class AllaccioAttivazioneEVO_Id_26_b {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "TMTLXJ85C04F839P");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "PENP");
			prop.setProperty("TIPO_CLIENTE", "Residenziale");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
			prop.setProperty("MERCATO", "Salvaguardia");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CITTA", "ROMA");
			prop.setProperty("INDIRIZZO", "VIA NIZZA");
			prop.setProperty("CIVICO", "4");
			prop.setProperty("CAP", "00135");
			prop.setProperty("USO_FORNITURA", "Uso Abitativo");
			prop.setProperty("RESIDENTE", "NO");
			prop.setProperty("USO_ENERGIA", "Ordinaria");
			prop.setProperty("TITOLARITA", "Uso/Abitazione");
			prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
			prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ACQUA");
			prop.setProperty("USO_ENERGIA", "Ordinaria");
			prop.setProperty("TENSIONE", "220");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "3");
			prop.setProperty("DISALIMENTABILITA", "SI");
			prop.setProperty("TIPO_MISURATORE", "Non Orario");
			prop.setProperty("CONSUMO_ANNUO", "1000");
			prop.setProperty("TENSIONE", "220");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "3");
			prop.setProperty("CONSUMO_ANNUO", "1000");
			prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
			//CONFIGURAZIONE PRODOTTO - USO DIVERSO DA ABITAZIONE - serve prodotto Business anche se cliente residenziale
			prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
			prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
			prop.setProperty("ELIMINA_VAS", "Y");		
			//prop.setProperty("SCELTA_ABBONAMENTI", "BASE");
			//
			prop.setProperty("CANALE_INVIO", "POSTA");
			prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
			prop.setProperty("CLASSIFICAZIONE_PREVENTIVO", "NON PREDETERMINABILE");
			prop.setProperty("MODALITA_INVIO_PREVENTIVO", "Posta");
			prop.setProperty("MODALITA_FIRMA_PREVENTIVO", "No Vocal");
			prop.setProperty("TIPO_OI_ORDER", "Commodity");
			prop.setProperty("RIGA_DA_ESTRARRE", "1");
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("CAP", "00135");
			prop.setProperty("RUN_LOCALLY","Y");
			
			//Integrazione R2D ELE
			prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
			prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d);
			prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d);
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoAllaccioAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoAllaccioAttivazioneEVO.main(args);
			InserimentoIndirizzoEsecuzioneLavoriEVO.main(args);
			VerificaCreazioneOffertaAllaccioAttivazione.main(args);
			VerificaRiepilogoOffertaAllaccioAttivazione.main(args);
			SelezioneUsoFornitura.main(args);
			CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneRes.main(args);
			ConfermaIndirizziAllaccioAttivazione.main(args);
			ConfermaFatturazioneElettronicaAllaccioAttivazione.main(args);
			PagamentoBollettinoPostaleEVO.main(args);
			ConfermaScontiBonusEVO.main(args);
			ConfiguraProdottoElettricoNonResidenziale.main(args);
			ConsensiEContattiAllaccioAttivazione.main(args);
			ModalitaFirmaAllaccioAttivazione.main(args);
			ConfermaOffertaAllaccioAttivazione.main(args);
			ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile.main(args);
			
			//Controlli post emissione richiesta
			RecuperaStatusOffer.main(args);
			VerificaOffertaInLavorazione_senza_POD.main(args);
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaCaseInLavorazione_senza_POD.main(args);
			VerificaStatusOrdineRichiestaPreventivo.main(args);
			SalvaIdOrdine.main(args);
			
			//Integrazione con R2D
			SetAttributeR2DCodDTN02.main(args);
			LoginR2D_ELE.main(args);
			R2D_VerifichePodIniziali_ELE.main(args);
			R2D_InvioPSPortale_1OK_ELE.main(args);
			R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
			
			//Verifiche post esitazione R2D
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaStatusR2DInviatoAlDistributore.main(args);
			
			//		Creazione preventivo
			SetAttributeR2DPreventivoSottoSoglia.main(args);
			LoginR2D_ELE.main(args);
			R2D_CaricamentoPreventivo_ELE.main(args);
			R2D_VerifichePodIntermedie_ELE.main(args);
			
			//Verifiche post caricamento preventivo R2D
			RecuperaStatusCase_and_Order_senza_POD.main(args);
//			VerificaOIOdineStatoPreventivoRicevuto.main(args);
			VerificaStatusR2DPreventivoDaCompletare.main(args);
			
			//Verifiche stato OI in Preventivo Inviato-->Aspettare 10 min
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaOIOdineStatoPreventivoInviato.main(args);
			
			//Validazione preventivo su SFDC
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			RicercaOffertaDaTab.main(args);
			ValidazionePreventivo.main(args);
			CaricaDocumentiSenzaValidazione.main(args);
			
			//Verifiche aggiornamento OI con stato 'Preventivo accettato'
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaOIOdineStatoPreventivoAccettato.main(args);
			
			//Verificare mancato aggiornamento su R2D
			SetAttributeR2DCodDTE01.main(args);
			LoginR2D_ELE.main(args);
			R2D_VerificaMancataCreazioneRichiesta_ELE.main(args);
			
			
			

		};
		
		@After
	    public void fineTest() throws Exception{
            String args[] = {nomeScenario};
            InputStream in = new FileInputStream(nomeScenario);
            prop.load(in);
            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
            ReportUtility.reportToServer(this.prop);
      };
		
	

}
