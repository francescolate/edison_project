package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSAP;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_Forzatura;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RicercaAttivita_Forzatura;
import com.nttdata.qa.enel.testqantt.RilavoraScarti;
import com.nttdata.qa.enel.testqantt.SbloccaTab_Forzatura;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SAP;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class Forzatura_esiti_SAP {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_forzatura_salesforce);
			prop.setProperty("PASSWORD",Costanti.password_forzatura_salesforce);
			prop.setProperty("TIPO_OI_ORDER", "Commodity");	
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("POD", "IT004E00000397"); 
			prop.setProperty("NUMERO_RICHIESTA", "215510083");
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			RecuperaStatusCase_and_Order.main(args);
			VerificaSeForzabile_KO_SAP.main(args);
			//I seguenti moduli vengono eseguti nel caso di un KO SAP riprocessabile
			AssegnaAttivitaScartiSAP.main(args);
			LoginSalesForce_Forzatura.main(args);
			SbloccaTab_Forzatura.main(args);
			RicercaAttivita_Forzatura.main(args);
			RilavoraScarti.main(args);

		};
		
		@After
	    public void fineTest() throws Exception{
            String args[] = {nomeScenario};
            InputStream in = new FileInputStream(nomeScenario);
            prop.load(in);
            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
            ReportUtility.reportToServer(this.prop);
      };

		
		
		
		
		
	

}
