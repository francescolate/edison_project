package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class RicontrattualizzazioneEVO_Id_9_MULTI_ELE_PE_Bollettino_Residenziale {

    Properties prop;
    String nomeScenario = this.getClass().getSimpleName() + ".properties";
        
    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");
        prop.setProperty("ATTIVABILITA", "Attivabile");
        prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
        prop.setProperty("PRODOTTO", "SCEGLI OGGI WEB LUCE");
        prop.setProperty("ASSICURAZIONE_SERVIZI", "Y");
        prop.setProperty("CAP", "00184");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("TIPO_OI_ORDER", "Commodity");
        prop.setProperty("RUN_LOCALLY", "Y");
        //prop.setProperty("OPZIONE_KAM_AGCOR", "BASE");
        prop.setProperty("TAB_SGEGLI_TU", "SI");
        prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
        prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

//      prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
/*
        SetQueryRecuperaDatiRicontrattualizzazione119_3.main(args);
        RecuperaDatiRicontrattualizzazioneMULTIELE.main(args);
        //RecuperaDatiWorkbench.main(args);
        //SetPropertyNAMECFPOD.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID22.main(args);
        ConfermaCheckList.main(args);
        CheckSezioneSelezioneFornituraRicontrattualizzazioneMULTIELE.main(args);
        ConfermaRiepilogoOffertaRicontrattualizzazione.main(args);
        PagamentoBollettinoPostaleVolture.main(args);
        ConfermaScontiBonusEVO.main(args);
        //ConfiguraProdottoMULTIELEResidenziale.main(args);
        ConfiguraProdottoElettricoResidenziale.main(args);
        ConfermaFatturazioneElettronicaSWAEVO.main(args);
        IndirizziFatturazione.main(args);
        ConsensiEContatti.main(args);
        ModalitaFirmaAllaccioAttivazione.main(args);
        ConfermaOffertaAllaccioAttivazione.main(args);//preso in presti da allaccio e attivazioni
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaOffertaSubentro.main(args);
        CaricaEValidaDocumenti.main(args);

        //attendere 5 minuti
        Thread.sleep(300000);

        RecuperaStatusOffer.main(args);

        VerificaOffertaInChiusa.main(args);

        //prima Elettrico1

        SetPodElettricoMULTI1.main(args);

        RecuperaStatusCase_and_Offer.main(args);

        VerificaOffertaInChiusa.main(args);

        VerificaSottostatoCaseOrderInviato.main(args);

        //poi  Elettrico2

        SetPodElettricoMULTI2.main(args);

        RecuperaStatusCase_and_Offer.main(args);

        VerificaOffertaInChiusa.main(args);

        VerificaSottostatoCaseOrderInviato.main(args);

        //query 115
        RecuperaStatusCaseItem.main(args);

        VerificaStatoInAttesa.main(args);

        //query 105

        RecuperaStatusOrderHeader.main(args);


        //prima Elettrico1

        SetPodElettricoMULTI1.main(args);
        //query 103
        RecuperaStatusCase_and_Order.main(args);

        VerificaStatusR2DND.main(args);

        VerificaStatusUDBND.main(args);

        VerificaStatoInAttesa.main(args);

        //poi Elettrico2

        SetPodElettricoMULTI2.main(args);
        //query 103
        RecuperaStatusCase_and_Order.main(args);

        VerificaStatusR2DND.main(args);

        VerificaStatusUDBND.main(args);

        VerificaStatoInAttesa.main(args);


        //prima Elettrico

        SetPodElettricoMULTI1.main(args);

        //query 103
        RecuperaStatusCase_and_Order.main(args);

        VerificaStatusSAP_ISUAttivazPod.main(args);//verifica stato OK sap ISU

        VerificaStatusSEMPREInAttesa.main(args);

        VerificaStatusOrdineEspletato.main(args);

        RecuperaItemAsset.main(args);

        VerificaItemAssetRicontrattualizzazione.main(args);
        //step 37
        VerificaAsset.main(args);
        //step 40 i precedenti già fatti prima

        //poi GAS Elettrico

        SetPodElettricoMULTI2.main(args);

        //query 103
        RecuperaStatusCase_and_Order.main(args);

        VerificaStatusSAP_ISUAttivazPod.main(args);//verifica stato OK sap ISU

        VerificaStatusSEMPREInAttesa.main(args);

        VerificaStatusOrdineEspletato.main(args);

        RecuperaItemAsset.main(args);

        VerificaItemAssetRicontrattualizzazione.main(args);
        //step 37
        VerificaAsset.main(args);


        //query 103
        RecuperaStatusCase_and_Order.main(args);

        VerificaStatusOrdineEspletato.main(args);

        VerificaCaseChiuso.main(args);

        VerificaSottoStatoCaseEspletato.main(args);

        VerificaStatusSEMPREAttivazPod.main(args);//sempre OK
*/
        //query 115
        RecuperaStatusCaseItem.main(args);

        VerificaStatoCaseItemEspletato.main(args);

    }

    @After
    public void fineTest() throws Exception {
		prop.load( new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

    }
}