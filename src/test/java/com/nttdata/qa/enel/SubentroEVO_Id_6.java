package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SubentroEVO_Id_6 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {

        Properties prop = new Properties();
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("LINK",Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("TIPO_DELEGA", "Nessuna delega");
        prop.setProperty("CODICE_FISCALE", "SDLSNT24H25H264O");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("CAP", "00178");      
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("TENSIONE_CONSEGNA", "380");
        prop.setProperty("POTENZA_CONTRATTUALE", "6");
        prop.setProperty("POTENZA_FRANCHIGIA", "6,6");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
                   
        prop.setProperty("CAP_GAS", "37121");
        prop.setProperty("SEZIONE_ISTAT_GAS", "N");
        prop.setProperty("PROVINCIA_COMUNE_GAS", "VERONA");
        prop.setProperty("INDIRIZZO_GAS", "CORSO PORTA NUOVA");
        prop.setProperty("MATRICOLA_CONTATORE", "3");
              
        prop.setProperty("USO", "Uso Abitativo");
        
        prop.setProperty("RESIDENTE", "NO");
        prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3297451908");
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        
        prop.setProperty("CITTA", "ROMA");
        
        prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
        prop.setProperty("CATEGORIA_USO", "Riscaldamento + uso cottura cibi e/o produzione di acqua calda sanitaria");    
        prop.setProperty("UNITA_ABITATIVA", "NO");
               
        prop.setProperty("RUN_LOCALLY", "Y");       
        prop.setProperty("PRODOTTO_ELE", "SCEGLI OGGI WEB LUCE");
		prop.setProperty("PIANO_TARIFFARIO_ELE", "Senza Orari");
		prop.setProperty("TAB_SGEGLI_TU_ELE", "SI");
		prop.setProperty("PRODOTTO_TAB_ELE", "Scegli Tu");
		prop.setProperty("PRODOTTO_GAS", "SICURA GAS");
        prop.setProperty("UNITA_ABITATIVA", "SI");
        
        prop.setProperty("MODULE_ENABLED", "Y");
        prop.setProperty("CONTAINER_RIEPILOGO_OFFERTE", "Y");
        
        prop.setProperty("UNITA_ABITATIVA", "SI");
        
        prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO_FIRMA", "POSTA");
        prop.setProperty("GESTIONE_APPUNTAMENTO", "Y");
        return prop;
    }


    @Test
    public void eseguiTest() throws Exception {
    	
       // prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
        /*
        RecuperaPodNonEsistenteDual.main(args);     
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID17.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureDualSubentro.main(args);         
        
        SelezioneUsoFornitura.main(args);    
        CommodityDualResidenzialeSubentroEVO.main(args);     
        ConfermaIndirizziPrimaAttivazione.main(args);
    	ConfermaScontiBonusEVO.main(args); 	
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
        PagamentoBollettinoPostale.main(args);  
        ConfiguraProdottoDualResidenzialeSubentro.main(args);
        GestioneCVPAllaccioAttivazione.main(args);
        SelezioneUnitaAbitativa.main(args);  
        ConsensiEContattiSubentro.main(args); 
        ModalitaFirmaSubentro.main(args);       
    	ConfermaOffertaAllaccioAttivazione.main(args);
        
    	LoginSalesForce.main(args);
		RicercaOffertaSubentro.main(args);
		CaricaEValidaDocumenti.main(args);
        
        
		//recupera orderID ele
        SetSubentroProperty.main(args);
        RecuperaOrderIDDaPodEle.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);
        System.out.println("ID recuperato");
        */
        
        // Dati R2d Ele      
        SetSubentro7R2D.main(args);
        LoginR2D_ELE.main(args);
        TimeUnit.SECONDS.sleep(5);
        R2D_VerifichePodIniziali_ELE.main(args);
        TimeUnit.SECONDS.sleep(5);
        R2D_InvioPSPortale_1OK_ELE.main(args);
        R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
        R2D_CaricamentoEsiti_5OK_ELE.main(args);
        R2D_VerifichePodFinali_ELE.main(args);
        
        //recupera ordereID gas
        
        SetSubentroProperty.main(args);
        LoginSalesForce.main(args);
        RicercaRichiesta.main(args);
        RecuperaOrderIDDaPodGas.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);
        System.out.println("ID recuperato");
        
        // Dati R2d Gas
        
        SetSubentro15R2DGAS.main(args);           
        LoginR2D_GAS.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodIniziali_GAS.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_InvioPSPortale_1OK_GAS.main(args);
        R2D_CaricamentoEsiti_3OK_GAS.main(args);
        R2D_CaricamentoEsiti_5OK_Standard_GAS.main(args);
        R2D_VerifichePodFinali_GAS.main(args);
        
       

        //Da eseguire dopo qualche ora
        prop.store(new FileOutputStream(nomeScenario), null);
        SetSubentroBatch.main(args);      
        prop.load(new FileInputStream(nomeScenario));
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaRichiesta.main(args);
        VerificheRichiestaDaPod.main(args);
    }


    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
