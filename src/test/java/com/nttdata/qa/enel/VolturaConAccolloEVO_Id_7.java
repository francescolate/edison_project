package com.nttdata.qa.enel;



import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


/**

Utente: s2s_manager_cfi_l@crmtenelfreemarket.com.uat
Cliente: PA

@DESCRIZIONE
Accedere alla piattaforma SFDC ed avviare un processo di Voltura Senza Accollo EVO da canale S2S su cliente di tipo PA 
Procedere fino alla verificare della presenza della maschera di CIG e CUP, costituita da picklist di adesione alla opzione Flag 136, e campi di CIG e CUP
Procedere fino al completamento del processo

@RISULTATO_ATTESO
Atteso: Corretto espletamento della pratica E2E

@DP CRM_EVO_SWA_DP_11
 	 R3: id richiesta: 215665953
 */


public class VolturaConAccolloEVO_Id_7 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";
	QANTTLogger logger;

	public static Properties conf() {
		Properties prop = new Properties();

		//UTENZE E LINK
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LINK",Costanti.salesforceLink);  
		prop.setProperty("TIPO_UTENZA", "S2S");

		prop.setProperty("PROCESSO", "Avvio Voltura con accollo EVO");
		prop.setProperty("TIPO_OPERAZIONE", "VOLTURA_CON_ACCOLLO");
		prop.setProperty("CAUSALE", "Trasformazione/Fusione Azienda");
		
		
		
				
		//CLIENTE ENTRANTE
		prop.setProperty("CODICE_FISCALE", "52490720977");
		
		prop.setProperty("CODICE_FISCALE", "80001000860");
		//CLIENTE USCENTE
		prop.setProperty("CODICE_FISCALE_CL_USCENTE", "93919190046");
		prop.setProperty("POD", "IT002E6083210A"); //POD VA BENE
		//CF REFERENTE: GLDKXF85C04F839D
		//CHECK SEZIONE OFFERTE
		prop.setProperty("ATTIVABILITA", "Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");

		//SPLIT PAYMENT - CIG e CUP
		prop.setProperty("POPOLA_SPLIT_PAYMENT","Y");
		prop.setProperty("CIG", "CIG3581103");
		prop.setProperty("CUP", "CUP358111111103");

		
		//MODALITA FIRMA
		prop.setProperty("CANALE_INVIO_FIRMA", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("EMAIL", "d.calabresi@reply.it");
		prop.setProperty("EFFETTUARE_CONFERMA", "Y");
		
		
		//SEZIONE FORNITURA
		prop.setProperty("CATEGORIA", "ALTRI SERVIZI");
		prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ALTRI SERVIZI");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("USO", "Uso Diverso da Abitazione");
		prop.setProperty("DISALIMENTABILITA", "SI");
		
		//PAGAMENTO E FATTURAZIONE
		prop.setProperty("SELZIONA_PRIMO_METODO_PAG_DISPONIBILE", "Y");
		prop.setProperty("IBAN", "IT18Q0617501594000002286990");
		prop.setProperty("CANALE_INVIO", "SDI"); //FATTURTAZIONE ELETTRONICA
		

		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		
	

		//	INDIRIZZO
		prop.setProperty("FLAG_RESIDENTE","NO");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "12");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("CAP", "00159");
		prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa 3F Business");

		
		prop.setProperty("EFFETTUARE_CONFERMA", "Y");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Documento di riconoscimento");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CHECK_LIST_TEXT", "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE:Che il cliente disponga dei dati del cliente uscente (CF, PI, Utenza) e dei dati del sito (POD/PDR)Se il cliente è in Salvaguardia è necessaria l’Istanza di SalvaguardiaINOLTRE VERIFICARE:PER USO ABITATIVO-    Se il cliente vuole attivare il RID/SDD è necessario l’IBANPER USI DIVERSI DALL’ABITAZIONE-    Se il cliente vuole attivare il RID/SDD è necessario l’IBAN-    Se il cliente è una Pubb. Amm. è necessario il Codice Ufficio-    Se il cliente è soggetto a Legge 136 è necessario almeno uno tra CIG e CUP-    Se il cliente è soggetto a Split Payment è necessario indicare Data Inizio e Data Fine validità-    In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)INFORMAZIONI UTILI:In caso di Dual ricordati che le forniture dovranno avere stessi: USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO In caso di Multi ricordati che le forniture dovranno avere stessi: COMMODITY – USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO – PRODOTTO - CODICE UFFICIO (se previsto)Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto.  Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della voltura. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta    In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associataSCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		
		prop.setProperty("EFFETTUARE_CONFERMA", "N");
		
		prop.setProperty("RUN_LOCALLY", "Y");
		return prop;
	}



	@Before
	public void setUp() throws Exception {
		this.prop = conf();
		logger = new QANTTLogger(prop);
	}


	@Test
	public void eseguiTest() throws Exception { 


		String args[] = {nomeScenario};
		prop.store(new FileOutputStream(nomeScenario), null);
		prop.load(new FileInputStream(nomeScenario));
		
		//--------------- Decommentare solo se si vuole recuperare il cliente uscente mediante query	
		SetClUscente_VCA7.main(args);
		
		//---------------Decommentare solo se si vuole recuperare il cliente entrante mediante query	
		//SetClEntrante_VCA7.main(args);
	
	
	
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID23.main(args);
		SelezioneMercatoVolturaConAccolloBSN.main(args);
		CheckSezioneClienteUscenteAndInsertPodVolturaConAccollo.main(args); 
		CheckSezioneSelezioneFornitura.main(args); 
		CreaOffertaVerificaCampiVolturaConAccolloBSN.main(args); 
		SceltaReferenteVolturaConAccolloBSN.main(args);
		CompilaDatiFornituraVolturaConAccolloBSN.main(args);
		CompilaIndirizziVolturaConAccollo.main(args);
		SelezionaMetodoDiPagamentoVolturaConAccollo.main(args); //TODO: FR R3 . Rendere selezione metodo pagamento dinamica 
		
		VerificaFatturazioneElettronicaVolturaConAccolloBSN.main(args);//TODO: FR R3 . Adeguare Fatturazione elettronica per  Pubblica amministrazione
		AzioniSezioneCarrelloVolturaConAccollo.main(args);
		SplitPaymentEVOVolturaConAccollo.main(args);//TODO: FR R3 - Adeguare SplitPayment  - la picklist non è selezionabile mentre lo script si aspetta che lo sia
		
		CigCupEVOVolturaConAccollo.main(args); 
		ConsensiEContattiVolturaConAccollo.main(args);

		ModalitaFirmaVolturaConAccollo.main(args);
		CheckRiepilogoOfferta.main(args);
		CaricaEValidaDocumenti.main(args);
		VerificaDocumento.main(args);
		
		logger.write(">>> Avvio verifiche post emissione ordine <<<");
		Utility.Wait(5);
		
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOfferta.main(args);
		VerificheStatoRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
		SaveOrderItemIdForVca.main(args);//R3
		RecuperaOrderIDDaPod.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);

		logger.write(">>> Avvio Esitazoine R2D <<<");
		SetVolturaConAccolloid6R2D.main(args);
		LoginR2D_ELE.main(args);
		R2D_VerifichePodIniziali_ELE.main(args);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		
		
		// Da eseguire dopo il batch
		logger.write(">>> Verifiche stato cessazione <<<");
        VerificaStatoCessazioneForVca.main(args);
        
       
        
        logger.write(">>> Verifiche Riprocessabilità KO SAP <<<");
        RecuperaStatoAttivazioneForVca.main(args);
  		SetUtenzaBo.main(args);
  		VerificaSeForzabile_KO_SAP.main(args);
  		//I seguenti moduli vengono eseguiti nel caso di un KO SAP riprocessabile
		AssegnaAttivitaScartiSAP.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		
		
		
		logger.write(">>> Verifiche Riprocessabilità KO Sempre <<<");
		RecuperaStatoAttivazioneForVca.main(args);
		VerificaSeForzabile_KO_SEMPRE.main(args);
		AssegnaAttivitaScartiSEMPRE.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		//VERIFICA STATO ATTIVAZIONE
		VerificaStatoAttivazioneForVca.main(args);

		logger.write(">>> Verifiche esiti SAP  - SEMPRE <<<");
		
		SetVolturaConAccollo6Batch.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
		VerificheStatoRichiesta.main(args);


	}

	@After
	public void fineTest() throws Exception {
		
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		Utility.takeSnapShotOnKo(prop, nomeScenario);
		//ReportUtility.reportToServer(this.prop);
		System.out.println("Test concluso");

		


	}
}
