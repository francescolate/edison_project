package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AggiungiFornitura;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckSezioneContattiConsensi;
import com.nttdata.qa.enel.testqantt.CommodityDualResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraDualPrimaAttivazione_ELE;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraDualPrimaAttivazione_GAS;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaInserimentoForniture;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneAppuntamentoPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureDualEVO_ELE;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureDualEVO_GAS;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistenteDualPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOffertaDaTab;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUnitaAbitativa;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Dual;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Dual;
import com.nttdata.qa.enel.testqantt.ValidaDocumentiDelibera40;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOIOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInInviata;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Accertamento_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Istruttoria_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIntermedie_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_8 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("MATRICOLA_CONTATORE", "Y");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP_GAS", "20121");
		prop.setProperty("CAP_ELE", "00198");
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("RESIDENTE", "NO");
		
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
	
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		
		//Carrello
		prop.setProperty("PRODOTTO_ELE", "SCEGLI OGGI LUCE 40");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");


		
		//Carrello
		prop.setProperty("PRODOTTO_GAS","SICURA GAS");
//		prop.setProperty("VAS_EX_GAS","SI");
		
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO", "Y");
		prop.setProperty("UNITA_ABITATIVA","SI");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("TELEFONO", "3394675542");
		prop.setProperty("CELLULARE", "3394675542");	
	
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("CARICA_DOCUMENTO", "Y");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("RUN_LOCALLY", "Y");
		
		// Dati R2d ELE
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("STATO_R2D", "AW");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
		prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
		prop.setProperty("POTENZA_FRANCHIGIA", "220");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");
		
		// Dati R2d GAS
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/217.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d);
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Attivazione in delibera 40");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		prop.setProperty("CP_GESTORE", "12345");
		prop.setProperty("ESITO_DELIBERA40_ISTRUTTORIA", "Istruttoria Positiva");
		prop.setProperty("ESITO_DELIBERA40_ACCERTAMENTO", "Accertamento Positivo");
		prop.setProperty("LETTURA_CONTATORE", "112233");
		prop.setProperty("ANNO_COSTRUZIONE_CONTATORE", "2019");
		prop.setProperty("ACCESSIBILITA_229", "1 (Accessibile)");
		prop.setProperty("COEFFICIENTE_C", "1");
		prop.setProperty("CLASSE_CONTATORE", "G0004");
		
	};

	@Test
	public void eseguiTest() throws Exception {
		
		prop.store(new FileOutputStream(nomeScenario), null);
  		String args[] = {nomeScenario};
  
  		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		//recupero pod ELE e GAS
	    RecuperaPodNonEsistenteDualPrimaAttivazione.main(args);
	    //SFDC
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureDualEVO_GAS.main(args);
		CompilaDatiFornituraDualPrimaAttivazione_GAS.main(args);
		AggiungiFornitura.main(args);
		InserimentoFornitureDualEVO_ELE.main(args);
		CompilaDatiFornituraDualPrimaAttivazione_ELE.main(args);
		ConfermaInserimentoForniture.main(args);
		VerificaCreazioneOffertaPrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		SelezioneUsoFornitura.main(args);
		CommodityDualResidenzialePrimaAttivazione.main(args);
	    ConfermaIndirizziPrimaAttivazione.main(args);
	    GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoSWAEVODUALResidenziale.main(args);
		
	    GestioneCVPPrimaAttivazione.main(args);
	    SelezioneUnitaAbitativa.main(args);
		CheckSezioneContattiConsensi.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);
		ModalitaFirmaPrimaAttivazione.main(args);
		ConfermaPrimaAttivazione.main(args);
		
		//Verifica stato offerta in Bozza
		RecuperaStatusOffer.main(args);
		VerificaOffertaInBozza.main(args);

		//Verifiche POD GAS-delibera 40
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerificaOIOffertaInBozza.main(args);
		SalvaIdOfferta.main(args);	

		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		
		SetOIRicercaDaOIOrdine.main(args);
		
		//Esiti R2D delibera 40 GAS
		LoginR2D_GAS.main(args);
		R2D_VerifichePodIniziali_GAS.main(args);
		R2D_InvioPSPortale_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		R2D_CaricamentoEsitiDelibera40Istruttoria_GAS.main(args);
		R2D_CaricamentoEsitiDelibera40Accertamento_GAS.main(args);
		R2D_VerifichePodIntermedie_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);
		
		//Verifica offerta in stato inviata
		RecuperaStatusOffer.main(args);
		VerificaOffertaInInviata.main(args);
		
		//Accesso a SFDC per caricare file
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOffertaDaTab.main(args);
		ValidaDocumentiDelibera40.main(args);
		CaricaVerificaValidaDocumenti.main(args);
		GestioneAppuntamentoPrimaAttivazione.main(args);
		
		//Verifica offerta in stato chiusa
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args);
		
		//Verifiche POD ELE-->aspettare 3 min
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		VerificaStatusR2DPresaInCarico.main(args);
		
		SetOIRicercaDaOIOrdine.main(args);
		
		//Esiti R2D ELE
		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		
	
		//Lanciare dopo 4 ore
		//Set focus su POD ELE
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//Set focus su POD GAS
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//Verifica finale su chiusura Richiesta
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);
		
		//Verifica finale su chiusura Richiesta
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);

	
	};

	@After
	public void fineTest() throws Exception {
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
