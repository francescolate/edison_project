package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_9 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "TMTLXJ85C04F839P");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "00100");
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("RESIDENTE", "NO");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		//PRODOTTO
		//prop.setProperty("PRODOTTO", "Valore Luce Plus"); R2
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("ELIMINA_VAS", "SI");
		
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "VOCAL ORDER");
		prop.setProperty("REGISTRAZIONE_VOCAL", "N");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("RUN_LOCALLY","Y");
		
		// Dati R2d ELE
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("STATO_R2D", "AW");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
		prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
		prop.setProperty("POTENZA_FRANCHIGIA", "220");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");


	};

	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID17.main(args);
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);
		CompilaDatiFornituraElePrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		CommodityEleResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoElettricoResidenziale.main(args);
		GestioneCVPPrimaAttivazione.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);
		ModalitaFirmaPrimaAttivazione.main(args);
		ConfermaPrimaAttivazione.main(args);
		CaricaEValidaDocumenti.main(args);
		
		//Verifica offerta in stato chiusa
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args);
		
		//Verifiche POD ELE-->aspettare 3 min
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		VerificaStatusR2DPresaInCarico.main(args);
		
		SetOIRicercaDaOIOrdine.main(args);
		
		//Esiti R2D ELE
		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		
		
		//Lanciare dopo 4 ore
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//Verifica finale su chiusura Richiesta
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);
	};

	@After
    public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

}
