package com.nttdata.qa.enel.creazionereferente;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerificaContattiReferente;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Id4R_CreazioneNuovoReferente_ObbligatorietaCampiContatti {
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() throws Exception{
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_REFERENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "GABBINON");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "M");
		prop.setProperty("RUOLO", "Coniuge");
		prop.setProperty("DATA_NASCITA", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA", "Milano");
		prop.setProperty("PROVINCIA", "MILANO");
		prop.setProperty("COMUNE", "MILANO");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "12");
		prop.setProperty("CELLULARE", "3467656379");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("MODULE_ENABLED","Y");
		//Solo per esecuzione locale, NO QANTT
		prop.setProperty("RUN_LOCALLY","Y");
		return prop;
	}


	@After
	public void tearDown() throws Exception{
	       InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
//
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);  
		VerificaContattiReferente.main(args);
//		String filePath = "src/test/resources/nuovireferenti.txt";
//		Files.write(Paths.get(filePath), ("REFERENTE "+"Id4R_Verifica Obbligatorietà Campi Contatti - OK"+"\n").getBytes(), StandardOpenOption.APPEND);

	}


}

