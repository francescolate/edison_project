package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.AnnullaTP2;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaUnitaAbitativaSWAEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfirmationCall;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PagamentoRIDEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVODUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitaPerNome;
import com.nttdata.qa.enel.testqantt.RecuperaAttività;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàDMSUploadPENP;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàQualityCallInterna;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Dual;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistenteDual;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocale;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SelezionaQualityCheck;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Dual;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Dual;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SetUtenzaOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.ValidaModuloAdesione;
import com.nttdata.qa.enel.testqantt.VerificaAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaAttribBollettinoPostaleTemporaleAsset;
import com.nttdata.qa.enel.testqantt.VerificaCancBollettinoPostaleTemporaleAsset;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamentoRIDAsset;
import com.nttdata.qa.enel.testqantt.VerificaQuoteQualityCallDigital;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivita;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivitaFatto;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaChiusaDaConfermare;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaInviata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DBozzaOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDUAL;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.VerificheVBLeBL;
import com.nttdata.qa.enel.testqantt.Verifiche_Order_Creation;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class SWA_EVO_id42_DUAL_PENP_QUALITY_CALL_DIGITAL_ANNULLATA_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}



	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA","PENP");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
//		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		//dati pod ELE
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA_ELE", "Vincolato");
		prop.setProperty("TITOLARITA_ELE","Uso/Abitazione");
		prop.setProperty("RESIDENTE_ELE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO NAZIONALE");
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "servizio elettrico romano");
		prop.setProperty("CAP_ELE", "00198");
		prop.setProperty("REGIONE_ELE", "LAZIO");
		prop.setProperty("PROVINCIA_ELE", "ROMA");
		prop.setProperty("CITTA_ELE", "ROMA");
		prop.setProperty("INDIRIZZO_ELE", "VIA NIZZA");
		prop.setProperty("CIVICO_ELE", "4");
		//Dati pod GAS
		prop.setProperty("CAP_GAS", "00198");
		prop.setProperty("REGIONE_GAS", "LAZIO");
		prop.setProperty("PROVINCIA_GAS", "ROMA");
		prop.setProperty("CITTA_GAS", "ROMA");
		prop.setProperty("INDIRIZZO_GAS", "VIA NIZZA");
		prop.setProperty("CIVICO_GAS", "4");
		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A.");
//		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A. • Libero");
		prop.setProperty("TITOLARITA_GAS", "Uso/Abitazione");
		prop.setProperty("MERCATO_DI_PROVENIENZA_GAS", "Libero");
		
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		//Prodotto ELE
		//prop.setProperty("PRODOTTO_ELE", "Speciale Luce 60");
		prop.setProperty("PRODOTTO", Costanti.prodotto_res_ele_non_integrato);
		//Prodotto GAS
		//prop.setProperty("PRODOTTO_GAS", "SoloXTe Gas");
		prop.setProperty("PRODOTTO", Costanti.prodotto_res_gas_non_integrato);
		
		prop.setProperty("UNITA_ABITATIVA", "NO");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("IBAN","IT59F0529673970CC0000033409");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("OPZIONE_VAS", "");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("ESITO_UDB","OK");
		
		// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Residenziale");
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** AbilitaQualityCallDigital.main(args);
//		*************************************************************************

		//Data Preparation query 98 per abilitare il canale alla quality call
		
		RecuperaPodNonEsistenteDual.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenzialeDUAL.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);

		ConfermaUnitaAbitativaSWAEVOResidenziale.main(args);
	
		ConfiguraProdottoSWAEVODUALResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
			
		GestioneDocumentaleSWAEvo.main(args);
		
		PrecheckSwaEVODUAL.main(args);		

		TimeUnit.SECONDS.sleep(60);
		RecuperaStatusOffer.main(args);
		VerificaStatoOffertaInviata.main(args);
		
		//bisogna aspettare che passi il batch affinchè si generi l'attività che gira ogni ora 
		TimeUnit.SECONDS.sleep(3600);
		
//		'Al passaggio del Document "Local upload" in stato Acquisito, viene creata la relativa attività "DMS Local Upload PENP".
//		'Assegnare l'attività ad un utente SFDC di tipo Operatore Cliente Avanzato.
		SetUtenzaOperatoreAvanzato.main(args);

		RecuperaAttivitàDMSUploadPENP.main(args);
	
		LoginSalesForce.main(args);
		
		RicercaAttività.main(args);
	
		ValidaModuloAdesione.main(args);
		
		//Verifica attività in stato Fatto
    	RecuperaAttivitaPerNome.main(args);
	    	
    	VerificaStatoAttivitaFatto.main(args);
			
		//Verifica offerta in stato sospesa
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaSospesa.main(args);
	
		VerificaQuoteQualityCallDigital.main(args);
		
//		Gestione TP2 		
		SetUtenzaMailResidenziale.main(args); 
			
		RecuperaLinkTP2Dual.main(args);
				
		SetUtenzaMailResidenziale.main(args);
			
		LoginPortalTP2.main(args);
			
		AnnullaTP2.main(args);
			
		RecuperaRichiestaTouchPoint.main(args);
			
		VerificaChiusuraTouchPoint.main(args);
			
		//Dopo 5 minuti
		TimeUnit.SECONDS.sleep(300);
		RecuperaStatusCase_and_Offer.main(args);
		
		VerificaStatoOffertaAnnullata.main(args);
			
//		*************************************************************************
//	    *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** SetUtenzaPENP.main(args);
//		*** DisabilitaQC_CC.main(args);
//		*************************************************************************

	}

}



