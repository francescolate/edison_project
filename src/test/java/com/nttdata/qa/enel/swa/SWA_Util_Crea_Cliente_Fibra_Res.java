package com.nttdata.qa.enel.swa;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_Util_Crea_Cliente_Fibra_Res {
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {

		this.prop = conf();
	}


	public static Properties conf() throws Exception{
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_3);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_3);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
//		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("NOME", "CLAUDIO");
		prop.setProperty("SESSO", "M");
		prop.setProperty("DATA_NASCITA", "04/03/1986");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA ALBERTO ASCARI");
		prop.setProperty("CIVICO", "196");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");
		prop.setProperty("EMAIL_FIBRA", "cli.res112@gmail.com");
		prop.setProperty("DESCRIZIONE_EMAIL", "Privato");
		//Solo per esecuzione locale, NO QANTT
		prop.setProperty("RUN_LOCALLY","Y");
		return prop;
	}



	@After
	public void tearDown() throws Exception{
	       InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
//
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
		CreazioneNuovoCliente2.main(args);
		String filePath = "src/test/resources/nuoviclienti.txt";
		Files.write(Paths.get(filePath), ("RESIDENZIALE;"+prop.getProperty("CODICE_FISCALE")+"\n").getBytes(), StandardOpenOption.APPEND);




	}




}

