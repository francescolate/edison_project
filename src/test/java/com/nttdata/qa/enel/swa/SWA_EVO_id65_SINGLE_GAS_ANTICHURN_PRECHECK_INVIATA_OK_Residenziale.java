package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaPreCheckInviata;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte1;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte2;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeGas;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocaleDigital;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_SimulatoriConfiguratoriEsitiFlussiSII_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id65_SINGLE_GAS_ANTICHURN_PRECHECK_INVIATA_OK_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_antichurn2);
		prop.setProperty("PASSWORD",Costanti.password_antichurn2);
//		prop.setProperty("USERNAME",Costanti.utenza_antichurn2);
//		prop.setProperty("PASSWORD",Costanti.password_antichurn2);
		prop.setProperty("TIPO_UTENZA", "Antichurn");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Residenziale");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Libero");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
//		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A. • Libero");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
        prop.setProperty("PRODOTTO", "SoloXTe Gas");
//        prop.setProperty("PRODOTTO", "GiustaXte Gas");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("OPZIONE_VAS", "");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");

		prop.setProperty("ESITO_UDB","OK");

		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
//		prop.setProperty("DATA_MINIMA_SW", "01/06/2020");
//		prop.setProperty("VERIFICA_AMM", "1");
//		prop.setProperty("TIPO_PDR", "0");

		//Configurazione Flusso SII PKG1
		prop.setProperty("PIVA_UTENTE","06655971007");
		prop.setProperty("PIVA_GESTORE","05877611003"); // --> campo pre-impostato su r2D
		prop.setProperty("CP_UTENTE","23445354325"); // (Questo valore è un identificativo e può essere modificato)
		prop.setProperty("CP_GESTORE","01010101033"); // (Questo valore è un identificativo e deve essere modificato per ogni simulazione)
		prop.setProperty("VERIFICA_AMM","1"); // --> con 1 e '1' in Esito mi da "OK"
		prop.setProperty("ESITO_ATTESO","OK"); // Valorizzare in base allo scenario se KO o OK per verifica finale
		prop.setProperty("ESITO","1");
		prop.setProperty("COD_ESITO","202");
		prop.setProperty("DETTAGLIO_ESITO","Nome e cognome o Ragione sociale corretti e dato aggiornato");
		prop.setProperty("TIPO_PDR","0");
		prop.setProperty("CODICE_REMI","34355800");
		prop.setProperty("CF_STRANIERO","0");
		prop.setProperty("PIVA_CONTR_COMM","00905811006");
		prop.setProperty("RAG_SOC_CONTR_COMM","MARIO ROSSI");
		prop.setProperty("PEC_CONTR_COMM","mariorossi@pec.com");
		prop.setProperty("DENOMINAZIONE","Denominazione");
		prop.setProperty("PIVA_DISTRIBUTORE","00905811006");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*		
		AbilitaPreCheckInviata.main(args);
		
		RecuperaPodNonEsistente.main(args);

		LoginR2D_GAS.main(args);	
		
		TimeUnit.SECONDS.sleep(5);

		R2D_SimulatoriConfiguratoriEsitiFlussiSII_GAS.main(args);
*/
		LoginSalesForce.main(args);
		
//		SbloccaTab.main(args);
//		
//		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenzialeGas.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOGasResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);

		ConfermaModalitaFirmaDigital.main(args);

		ConsensiEContatti.main(args);
		
		RegistrazioneVocaleDigital.main(args);
		
		PrecheckSwaEVOParte1.main(args);
		PrecheckSwaEVOParte2.main(args);
		
	}

}



