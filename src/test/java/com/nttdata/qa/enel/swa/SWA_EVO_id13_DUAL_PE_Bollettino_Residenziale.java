package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSAP;
import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSAP_commodity;
import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSEMPRE;
import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSEMPRE_commodity;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaUnitaAbitativaSWAEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.RilavoraScarti;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_Forzatura;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVODUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistenteDual;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaAttivita_Forzatura;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SbloccaTab_Forzatura;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Dual;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Dual;
import com.nttdata.qa.enel.testqantt.SetUtenzaBo;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.VerificaAttivazioneAsset;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SAP;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SEMPRE;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id13_DUAL_PE_Bollettino_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
		prop.setProperty("TIPO_UTENZA","PE");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
//		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		//dati pod ELE
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA_ELE", "Vincolato");
		prop.setProperty("TITOLARITA_ELE","Uso/Abitazione");
		prop.setProperty("RESIDENTE_ELE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "servizio elettrico romano");
		prop.setProperty("CAP_ELE", "00135");
		prop.setProperty("REGIONE_ELE", "LAZIO");
		prop.setProperty("PROVINCIA_ELE", "ROMA");
		prop.setProperty("CITTA_ELE", "ROMA");
		prop.setProperty("INDIRIZZO_ELE", "VIA NIZZA");
		prop.setProperty("CIVICO_ELE", "4");
		//Dati pod GAS
		prop.setProperty("CAP_GAS", "00135");
		prop.setProperty("REGIONE_GAS", "LAZIO");
		prop.setProperty("PROVINCIA_GAS", "ROMA");
		prop.setProperty("CITTA_GAS", "ROMA");
		prop.setProperty("INDIRIZZO_GAS", "VIA NIZZA");
		prop.setProperty("CIVICO_GAS", "4");
		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A.");
//		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A. • Libero");
		prop.setProperty("TITOLARITA_GAS", "Uso/Abitazione");
		prop.setProperty("MERCATO_DI_PROVENIENZA_GAS", "Libero");
		
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		//Prodotto ELE
		prop.setProperty("PRODOTTO_ELE", "SCEGLI OGGI WEB LUCE");
		prop.setProperty("TAB_SGEGLI_TU_ELE", "SI");
		prop.setProperty("PIANO_TARIFFARIO_ELE", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB_ELE", "Scegli Tu");
		//Prodotto GAS
        prop.setProperty("PRODOTTO_GAS", "SICURA GAS");
//        prop.setProperty("VAS_EX_GAS", "SI");
		
		prop.setProperty("UNITA_ABITATIVA", "NO");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("IBAN","IT59F0529673970CC0000033409");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("OPZIONE_VAS", "");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("ESITO_UDB","OK");
		
		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
		
		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
		
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

 		prop.store(new FileOutputStream(nomeScenario), null);
  		String args[] = {nomeScenario};
  
  		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		RecuperaPodNonEsistenteDual.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenzialeDUAL.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);

		ConfermaUnitaAbitativaSWAEVOResidenziale.main(args);
		
		ConfiguraProdottoSWAEVODUALResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);
		
		PrecheckSwaEVODUAL.main(args);
		
		
		//startare 5 minuti dopo l'emissione
		//Set focus su POD ELE
		SetPODtoRetreiveELE_Dual.main(args);
		//Verifica status case+offer emissione contratto
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);
		UpdateDataRipensamentoSWA.main(args);
		
		//Set focus su POD GAS
		SetPODtoRetreiveGAS_Dual.main(args);
		//Verifica status case+offer emissione contratto
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);
		UpdateDataRipensamentoSWA.main(args);		
		
		//Startare dopo 2 ore
		//Set focus su POD ELE
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerificheScadenzaRipensamento_SWA.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusR2DRicezioneOrdine.main(args);
		//Set focus su POD GAS
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerificheScadenzaRipensamento_SWA.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusR2DRicezioneOrdine.main(args);
		
		//Check status offerta
		RecuperaStatusOffer.main(args);
		VerificaChiusuraOfferta.main(args);
		
		//Esiti POD ELE
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		
		
//		Accesso a R2D
		
		LoginR2D_ELE.main(args);
		
		TimeUnit.SECONDS.sleep(20);
		
		R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_3OK_SWA_ELE.main(args);
		
		R2D_Forzatura_Esiti_3OK_SWA_ELE.main(args); // OK Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_5OK_SWA_ELE.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);

		R2D_VerifichePodFinali_SW_E_DISDETTE_ELE.main(args);
		TimeUnit.SECONDS.sleep(10);

		//Verifiche post esiti R2D, aspettare 2 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);
		
		RecuperaStatusAsset.main(args);
		VerificaAttivazioneAsset.main(args);
		
		//Esiti POD GAS
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		
		//Accesso a R2D
		
		LoginR2D_GAS.main(args);
		
		TimeUnit.SECONDS.sleep(20);
		
		R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_Forzatura_Esiti_3OK_SWA_GAS.main(args); // OK Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_4OK_SWA_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_5OK_SWA_GAS.main(args); //OK Cla
		
		TimeUnit.SECONDS.sleep(5);

		R2D_VerifichePodFinali_SW_E_DISDETTE_GAS.main(args);
		
		//Verifiche post esiti R2D, aspettare 2 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);
		
		RecuperaStatusAsset.main(args);
		VerificaAttivazioneAsset.main(args);
		
		//Lanciare dopo 4 ore
		//Set focus su POD ELE
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		//Verifica se un eventuale KO SAP è riprocessabile
		SetUtenzaBo.main(args);
		VerificaSeForzabile_KO_SAP.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SAP riprocessabile
		AssegnaAttivitaScartiSAP.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		
		//Verifica se un eventuale KO SEMPRE è riprocessabile
		VerificaSeForzabile_KO_SEMPRE.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SEMPRE riprocessabile
		AssegnaAttivitaScartiSEMPRE.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//Set focus su POD GAS
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		//Verifica se un eventuale KO SAP è riprocessabile
		VerificaSeForzabile_KO_SAP.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SAP riprocessabile
		AssegnaAttivitaScartiSAP_commodity.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		
		//Verifica se un eventuale KO SEMPRE è riprocessabile
		VerificaSeForzabile_KO_SEMPRE.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SEMPRE riprocessabile
		AssegnaAttivitaScartiSEMPRE_commodity.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//Verifica finale su chiusura Richiesta
		SetPODtoRetreiveELE_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);
		
		//Verifica finale su chiusura Richiesta
		SetPODtoRetreiveGAS_Dual.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);

	}

}



