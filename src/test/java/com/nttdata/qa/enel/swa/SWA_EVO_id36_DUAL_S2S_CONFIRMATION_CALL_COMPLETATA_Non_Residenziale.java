package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaUnitaAbitativaSWAEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfirmationCall;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PagamentoRIDEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVODUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.RecuperaAttività;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàConfirmationCall;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistenteDual;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocale;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SelezionaQualityCheck;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Dual;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Dual;
import com.nttdata.qa.enel.testqantt.SetUtenzaOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.UpdateDate_QC_CC;
import com.nttdata.qa.enel.testqantt.VerificaAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaAttribBollettinoPostaleTemporaleAsset;
import com.nttdata.qa.enel.testqantt.VerificaCancBollettinoPostaleTemporaleAsset;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamentoRIDAsset;
import com.nttdata.qa.enel.testqantt.VerificaQuoteConfirmationCall;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivita;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaChiusaDaConfermare;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DBozzaOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaChiusaDaConfermare;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDUAL;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.VerificheVBLeBL;
import com.nttdata.qa.enel.testqantt.Verifiche_Order_Creation;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class SWA_EVO_id36_DUAL_S2S_CONFIRMATION_CALL_COMPLETATA_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_attivazioni);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_attivazioni);
		prop.setProperty("TIPO_UTENZA","S2S");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		//prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "90038580313");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "GLSCRS65H08Z115F");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		//dati pod ELE
		prop.setProperty("TIPO_INSTALLAZIONE", "ACCESSIBILE");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ACQUA");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("MERCATO_DI_PROVENIENZA_ELE", "Vincolato");
		prop.setProperty("TITOLARITA_ELE","Uso/Abitazione");
		prop.setProperty("RESIDENTE_ELE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO NAZIONALE");
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "servizio elettrico romano");
		prop.setProperty("CAP_ELE", "00198");
		prop.setProperty("REGIONE_ELE", "LAZIO");
		prop.setProperty("PROVINCIA_ELE", "ROMA");
		prop.setProperty("CITTA_ELE", "ROMA");
		prop.setProperty("INDIRIZZO_ELE", "VIA NIZZA");
		prop.setProperty("CIVICO_ELE", "4");
		//Dati pod GAS
		prop.setProperty("CAP_GAS", "00198");
		prop.setProperty("REGIONE_GAS", "LAZIO");
		prop.setProperty("PROVINCIA_GAS", "ROMA");
		prop.setProperty("CITTA_GAS", "ROMA");
		prop.setProperty("INDIRIZZO_GAS", "VIA NIZZA");
		prop.setProperty("CIVICO_GAS", "4");
		prop.setProperty("MERCATO_DI_PROVENIENZA_GAS", "Libero");
		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A.");
		prop.setProperty("TITOLARITA_GAS", "Uso/Abitazione");
		
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		//Prodotto ELE
        prop.setProperty("PRODOTTO_ELE", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR_ELE", "SUPER");
        prop.setProperty("ELIMINA_VAS_ELE", "Y");
		//Prodotto GAS
		prop.setProperty("PRODOTTO_GAS", "New_Soluzione Gas Impresa Business");
		prop.setProperty("OPZIONE_KAM_AGCOR_GAS", "SUPER");
		
		prop.setProperty("UNITA_ABITATIVA", "NO");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("IBAN","IT59F0529673970CC0000033409");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("ESITO_UDB","OK");
		prop.setProperty("SCENARIO", "CONFIRMATION_CALL");
		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
		
		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
        prop.setProperty("PRODOTTO", "GiustaXte Impresa");
		prop.setProperty("TENSIONE", "220");
		prop.setProperty("CONSUMO_ANNUO_ELE", "1500");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("SCENARIO", "CONFIRMATION_CALL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		prop.setProperty("CATEGORIA_MARKETING", "AGRITURISMI");
		prop.setProperty("CATEGORIA_CONSUMO", "Agriturismo");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "RISCALDAMENTO E PRODUZIONE");
		prop.setProperty("POTENZIALITA", "0");
		prop.setProperty("UTILIZZO", "ARTIGIANALE");
		prop.setProperty("MATRICOLA_CONTATORE", "1234567899");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		//Data Preparation query 98 per abilitare il canale alla confirmation call
		AbilitaConfirmationCall.main(args);

		//Data Preparation query 90 per recuperare pod nuovo		
		RecuperaPodNonEsistenteDual.main(args);

		LoginSalesForce.main(args);

		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVONonResidenzialeDUAL.main(args);

		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfiguraProdottoSWAEVODUALNonResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);
	
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
	
		RegistrazioneVocale.main(args);
	
		// Precheck DUAL aggiunto il 2/8/2021 prima era nella Registrazione Vocale
		PrecheckSwaEVODUAL.main(args);		

		// Attendere alcuni minuti
		TimeUnit.SECONDS.sleep(180);
		
		RecuperaStatusOffer.main(args);
		
		//'Passaggio dell'Offerta in SOSPESA. Viene creata la relativa attività di Confirmation Call.
		// attendere qualche minuto e fino 2 ore
		VerificaStatoOffertaSospesa.main(args);
		//verifico che il Quote sia di confirmation call
		VerificaQuoteConfirmationCall.main(args);
		// Setto la Data indietro di 1 giorno
		UpdateDate_QC_CC.main(args); 
		
		// *** ATTENZIONE *** bisogna aspettare che passi il batch affinchè si generi l'attività
		//  il recupero dell'attività di CC potrebbe impiegare qualche ora.
		TimeUnit.SECONDS.sleep(360);
		
		SetUtenzaOperatoreAvanzato.main(args);
		//Assegnare l'attività ad un utente SFDC di tipo Operatore Cliente Avanzato.
		RecuperaAttivitàConfirmationCall.main(args);
				
		LoginSalesForce.main(args);
				
		//Ricercare l'attività Confirmation Call e premere il pulsante "ESEGUI CONFIRMATION CALL".
		//Premere il pulsante TERMINA CHIAMATA.
		RicercaAttività.main(args);

		ConfirmationCall.main(args);
		
    	VerificaStatoAttivita.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaOfferta.main(args);
		
		SelezionaConfermaOfferta.main(args);
	
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaChiusaDaConfermare.main(args);
		
		//alla fine bisogna sempre disattivare la Confirmation call per il canale utilizzato
		SetUtenzaS2SAttivazioni.main(args);
		DisabilitaQC_CC.main(args);

	}

}



