package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfirmationCall;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàConfirmationCall;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocale;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SetUtenzaOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateDate_QC_CC;
import com.nttdata.qa.enel.testqantt.VerificaQuoteConfirmationCall;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivita;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaChiusaDaConfermare;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaChiusaDaConfermare;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id35_SINGLE_ELE_S2S_CONFIRMATION_CALL_COMPLETA_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_attivazioni);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_attivazioni);
		prop.setProperty("TIPO_UTENZA", "S2S");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "03254490562");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "PPRLVR70L03H501I");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("PRODOTTO", "GiustaXte Impresa");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
//		prop.setProperty("PATH_DOCUMENTO", "C:\\Users\\galdieroro\\Documents\\My Received Files\\a.pdf");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE_DISPONIBILE", "220");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		prop.setProperty("SCENARIO", "CONFIRMATION_CALL");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ACQUA");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
//        prop.setProperty("PRODOTTO", "GiustaXte Impresa");
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
        prop.setProperty("ELIMINA_VAS", "Y");
		prop.setProperty("TENSIONE", "220");
		prop.setProperty("CONSUMO_ANNUO_ELE", "1500");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("OPZIONE_VAS", "");
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		//Data Preparation query 98 per abilitare il canale alla confirmation call
		AbilitaConfirmationCall.main(args);
	
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);
	
		ProcessoSwitchAttivoEVONonResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoNonResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);
	
		RegistrazioneVocale.main(args);
		
		// Precheck aggiunto il 2/8/2021 prima era nella Registrazione Vocale
		PrecheckSwaEVO.main(args);

		// Attendere alcuni minuti
		TimeUnit.SECONDS.sleep(180);
		
		RecuperaStatusOffer.main(args);
		
		//'Passaggio dell'Offerta in SOSPESA. Viene creata la relativa attività di Confirmation Call.
		// attendere qualche minuto e fino 2 ore
		VerificaStatoOffertaSospesa.main(args);
		//verifico che il Quote sia di confirmation call
		VerificaQuoteConfirmationCall.main(args);
		// Setto la Data indietro di 1 giorno
		UpdateDate_QC_CC.main(args); 
		
		// *** ATTENZIONE *** bisogna aspettare che passi il batch affinchè si generi l'attività
		//  il recupero dell'attività di CC potrebbe impiegare qualche ora.
		TimeUnit.SECONDS.sleep(360);
		
		SetUtenzaOperatoreAvanzato.main(args);
		//Assegnare l'attività ad un utente SFDC di tipo Operatore Cliente Avanzato.
		RecuperaAttivitàConfirmationCall.main(args);
				
		LoginSalesForce.main(args);
				
		//Ricercare l'attività Confirmation Call e premere il pulsante "ESEGUI CONFIRMATION CALL".
		//Premere il pulsante TERMINA CHIAMATA.
		RicercaAttività.main(args);

		ConfirmationCall.main(args);
		
    	VerificaStatoAttivita.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaOfferta.main(args);
		
		SelezionaConfermaOfferta.main(args);
	
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaChiusaDaConfermare.main(args);
		
		//alla fine bisogna sempre disattivare la Confirmation call per il canale utilizzato
		SetUtenzaS2SAttivazioni.main(args);
		DisabilitaQC_CC.main(args);


	}

}



