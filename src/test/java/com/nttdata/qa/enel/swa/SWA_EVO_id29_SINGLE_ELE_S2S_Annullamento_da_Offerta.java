package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AnnullaOfferta;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.FirmaDigital;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id29_SINGLE_ELE_S2S_Annullamento_da_Offerta {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
//		prop.setProperty("PRODOTTO", "Speciale Luce 60");
		prop.setProperty("PRODOTTO", Costanti.prodotto_res_ele_non_integrato);
		//prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		//prop.setProperty("TAB_SGEGLI_TU", "SI");
		//prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
        //prop.setProperty("ELIMINA_VAS", "Y");
	// Dettaglio Sito
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE_DISPONIBILE", "220");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		// Motivo Annullamento	
		prop.setProperty("MOTIVO_ANNULLAMENTO","OPERAZIONE ERRATA");
		// * * *	
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("OPZIONE_VAS", "");
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "Chiuso");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Annullata");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Annullata");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "Chiuso");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Annullato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("ESITO_UDB","OK");


		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
			
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		FirmaDigital.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);

		AnnullaOfferta.main(args);

		//startare 5 minuti dopo l'emissione per verificare lo Stato=ANNULLATA
		TimeUnit.SECONDS.sleep(300);
		
		//Verifica status case+offer emissione contratto
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);


	}
	
    @After
 public void fineTest() throws Exception{
          String args[] = {nomeScenario};
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
    }

}



