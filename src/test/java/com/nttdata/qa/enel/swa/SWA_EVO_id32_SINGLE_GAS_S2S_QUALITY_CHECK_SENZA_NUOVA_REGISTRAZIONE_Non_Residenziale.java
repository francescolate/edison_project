package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.ControllaeConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocale;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaQualityCheck;
import com.nttdata.qa.enel.testqantt.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.SetUtenzaS2SQuality;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaInviata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaInviataoSospesa;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificheVBLeBL;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id32_SINGLE_GAS_S2S_QUALITY_CHECK_SENZA_NUOVA_REGISTRAZIONE_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_attivazioni);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_attivazioni);
		prop.setProperty("TIPO_UTENZA", "S2S");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "04922840113");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "GLDGWJ85C04F839A");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Libero");
		//  Dettaglio Sito
		prop.setProperty("MATRICOLA_CONTATORE", "1234567899");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("TIPO_INSTALLAZIONE", "ACCESSIBILE");
//		prop.setProperty("CATEGORIA_CONSUMO", "Industria");
//		Centri di recupero
		prop.setProperty("CATEGORIA_CONSUMO", "Agriturismo");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		prop.setProperty("CATEGORIA_MARKETING", "AGRITURISMI");
		
//		prop.setProperty("ORDINE_GRANDEZZA", "INDUSTRIA");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "RISCALDAMENTO E PRODUZIONE");
		prop.setProperty("POTENZIALITA", "0");
		
//		prop.setProperty("UTILIZZO", "INDUSTRIALE");
		prop.setProperty("UTILIZZO", "ARTIGIANALE");
		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
		prop.setProperty("CONSUMO_ANNUO", "1500");
//		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
        //prop.setProperty("PRODOTTO", "New_Soluzione Gas Impresa Business");
		prop.setProperty("PRODOTTO", Costanti.prodotto_bus_gas_non_integrato);
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "ATTESA QUALITY CHECK");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - NULL / DRAFT");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("SPLIT_PAYMENT", "No");
		prop.setProperty("FLAG_136", "NO");
		prop.setProperty("OPZIONE_VAS", "");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		//Data Preparation query 97 per abilitare il canale alla quality check
		AbilitaQualityCheck.main(args);
		 
		//Data Preparation query 90 per recuperare pod nuovo	
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOGasNonResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);
		
		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ControllaeConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOGasNonResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		ReferenteSwaEVO.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
	
		RegistrazioneVocale.main(args);
		
		// *** 5/10/2021 Mettere Precheck prima non previsto ***
		PrecheckSwaEVO.main(args);
		
		//fine emissione richiesta FE

		RecuperaStatusCase_and_Offer.main(args); // Offerta in "Offer - Null / Draft"
		VerificaStatoOffertaQualityCheck.main(args);
		//'Viene generata l'attivita Quality Check. Recupero l'attività che potrebbe impiegare qualche ora per essere generata.
		// Attendere circa  2 minuti
		TimeUnit.SECONDS.sleep(120);
		
		VerificaAttivitàQualityCheck.main(args);
		//'Effettuare accesso alla piattaforma SFDC con profilo s2squalitycontroller_cfi_l@enelfreemarket.com.uat e Recuperare l'offerta in Stato Attesa Quality Check
		SetUtenzaS2SQuality.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaOfferta.main(args);
		
		//ricerca dell'attività recuperata in precedenza nella home SF				
		SelezionaQualityCheck.main(args);
		//lo step lavora anche l'attività che permetterà di chiudere il processo.

		VerificheVBLeBL.main(args);
		//Dopo 1 minuto 
		TimeUnit.SECONDS.sleep(60);
		
		RecuperaStatusOffer.main(args);
		VerificaStatoOffertaInviataoSospesa.main(args);
		VerificaChiusuraAttivitàQualityCheck.main(args);
		//alla fine bisogna sempre disattivare la quality check per il canale utilizzato
//		SetUtenzaS2SAttivazioni.main(args);
//		DisabilitaQualityCheck.main(args);
		
		
	}

}



