package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte1;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte2;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerificaQuoteQualityCallDigital;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_SimulatoriConfiguratoriEsitiFlussiSII_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id69_SINGLE_ELE_PENP_QUALITY_CALL_DIGITAL_PRECHECK_INVIATA_OK_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA", "PENP");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("PRODOTTO", "Speciale Luce 60");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
//		prop.setProperty("PATH_DOCUMENTO", "C:\\Users\\galdieroro\\Documents\\My Received Files\\a.pdf");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE_DISPONIBILE", "220");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
		prop.setProperty("SCENARIO", "QUALITY_CALL_DIGITAL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Residenziale");
		prop.setProperty("OPZIONE_VAS", "");
		prop.setProperty("ANNULLAMENTO", "SI");
        
//		prop.setProperty("TIPOMERCATO", "Libero");
//
//		  
//		prop.setProperty("NUOVA_POTENZA","3"); 
//		prop.setProperty("DEALERPHONE","3333131311");  
//		 
//		prop.setProperty("COMMVALUE","12");
//		prop.setProperty("POTENZAANNUA","3300");  
//		
//		prop.setProperty("VOCALNOVOCAL","No Vocal");
//		prop.setProperty("MODALITAFIRMA","POSTA");
//		prop.setProperty("MODULE_ENABLED","Y");
//		prop.setProperty("TARIFFA","Senza Orari");
//		prop.setProperty("EXPECTEDSTATUS","Chiusa da confermare");
//		prop.setProperty("FILENAME","src/main/resources/C.T.E.pdf");
//		//Verifiche post emissione ordine
//		prop.setProperty("EXPECTEDSTATUS_OFFERTA_POST_EMISSIONE_SWA", "Chiusa");
//		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
//		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
//		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
//		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
//		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
//		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
//		//Property per verifiche post emissione
//		prop.setProperty("EXPECTEDSTATUS_RICHIESTA_FINALE","Chiuso");
//		prop.setProperty("EXPECTED_SOTTO_STATUS_RICHIESTA_FINALE", "Espletato");
//		prop.setProperty("NUMERO_CONTRATTO", "1234567");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");

		//Configurazione Flusso SII PK1
		prop.setProperty("PIVA_VENDITORE","06655971007");
//		prop.setProperty("PIVA_GESTORE","05779711000"); // --> campo pre-impostato su r2D
		prop.setProperty("CP_VENDITORE","09876543222"); // (Questo valore è un identificativo e può essere modificato)
		prop.setProperty("CP_GESTORE","01010101022"); // (Questo valore è un identificativo e deve essere modificato per ogni simulazione)
		prop.setProperty("VERIFICA_AMM","1"); // --> con 1 e '1' in Esito mi da il OK
		prop.setProperty("ESITO_ATTESO","OK"); // Valorizzare in base allo scenario se KO o OK per verifica finale
		prop.setProperty("ESITO","1");
//		prop.setProperty("COD_ESITO","910");
		prop.setProperty("COD_ESITO","916");
//		prop.setProperty("DETTAGLIO_ESITO","Presenza di dati identificativi del cliente finale non corretti per meno di 2 caratteri alfanumerici");
		prop.setProperty("DETTAGLIO_ESITO","Pod non Attivo");
		prop.setProperty("CF_STRANIERO","0");
		prop.setProperty("PIVA_CONTR_COMM","00905811006");
		prop.setProperty("RAG_SOC_CONTR_COMM","MARIO ROSSI");
		prop.setProperty("EMAIL_CONTR_COMM","mariorossi@mail.com");
		prop.setProperty("POTENZA_MEDIA_ANNUA","3");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** AbilitaQualityCallDigital.main(args);
//		*************************************************************************
/*		
		RecuperaPodNonEsistente.main(args);

		LoginR2D_ELE.main(args);	
		
		TimeUnit.SECONDS.sleep(5);

		R2D_SimulatoriConfiguratoriEsitiFlussiSII_ELE.main(args);

		LoginSalesForce.main(args);

		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoResidenziale.main(args);
		
		//ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);

		PrecheckSwaEVOParte1.main(args);
		
		//		RecuperaStatoPrecheckOK.main(args);
		
		PrecheckSwaEVOParte2.main(args);
		
		//		UpdateStatoPrecheckOK.main(args);
		
		//bisogna aspettare che passi il batch affinchè si generi l'attività e passi in, anche 1 ora di attesa
		TimeUnit.SECONDS.sleep(600);
*/		
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaSospesa.main(args);
    	
		VerificaQuoteQualityCallDigital.main(args);

//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** SetUtenzaPENP.main(args);
//		*** DisabilitaQualityCallDigital.main(args);
//		*************************************************************************
		

	}

}



