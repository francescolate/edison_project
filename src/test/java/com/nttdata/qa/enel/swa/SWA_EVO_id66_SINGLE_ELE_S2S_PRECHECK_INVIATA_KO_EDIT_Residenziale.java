package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaPreCheckInviata;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOKO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOModifica;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte1;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVORichiestaSupporto;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatoPrecheckKO;
import com.nttdata.qa.enel.testqantt.RecuperaStatoPrecheckKOMOGE;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocaleDigital;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.UpdateStatoPrecheckKO;
import com.nttdata.qa.enel.testqantt.UpdateStatoPrecheckKOMOGE;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_SimulatoriConfiguratoriEsitiFlussiSII_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id66_SINGLE_ELE_S2S_PRECHECK_INVIATA_KO_EDIT_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_attivazioni);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_attivazioni);
		prop.setProperty("TIPO_UTENZA", "S2S");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Residenziale");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("PRODOTTO", "Speciale Luce 60");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE_DISPONIBILE", "220");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("OPZIONE_VAS", "");
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");

		prop.setProperty("ESITO_UDB","OK");

		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");

		//Configurazione Flusso SII PK1
		prop.setProperty("PIVA_VENDITORE","06655971007");
//		prop.setProperty("PIVA_GESTORE","05779711000"); // --> campo pre-impostato su r2D
		prop.setProperty("CP_VENDITORE","09876543211"); // (Questo valore è un identificativo e può essere modificato)
		prop.setProperty("CP_GESTORE","01010101012"); // (Questo valore è un identificativo e deve essere modificato per ogni simulazione)
		prop.setProperty("VERIFICA_AMM","1"); // --> con '1' e '0' in Esito mi da il KO
		prop.setProperty("ESITO_ATTESO","KO"); // Valorizzare in base allo scenario se KO o OK per verifica finale
		prop.setProperty("ESITO","0");
		prop.setProperty("COD_ESITO","923");
		prop.setProperty("DETTAGLIO_ESITO","Codice fiscale non coerente");
		prop.setProperty("CF_STRANIERO","0");
		prop.setProperty("PIVA_CONTR_COMM","00905811006");
		prop.setProperty("RAG_SOC_CONTR_COMM","MARIO ROSSI");
		prop.setProperty("EMAIL_CONTR_COMM","mariorossi@mail.com");
		prop.setProperty("POTENZA_MEDIA_ANNUA","3");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		AbilitaPreCheckInviata.main(args);
		
		RecuperaPodNonEsistente.main(args);

		LoginR2D_ELE.main(args);	
		
		TimeUnit.SECONDS.sleep(5);

		R2D_SimulatoriConfiguratoriEsitiFlussiSII_ELE.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenziale.main(args);
	
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
	
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		RegistrazioneVocaleDigital.main(args);

		PrecheckSwaEVOParte1.main(args);
		
		PrecheckSwaEVOModifica.main(args);
			
	}

}



