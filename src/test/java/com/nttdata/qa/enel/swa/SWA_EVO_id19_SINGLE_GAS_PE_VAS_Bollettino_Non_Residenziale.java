package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSAP;
import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSAP_commodity;
import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSEMPRE;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_Forzatura;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaAttivita_Forzatura;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.RilavoraScarti;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdBPMVAS;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SbloccaTab_Forzatura;
import com.nttdata.qa.enel.testqantt.SetOrderLineItemCommoditytoRetreive;
import com.nttdata.qa.enel.testqantt.SetOrderLineItemVAStoRetreive;
import com.nttdata.qa.enel.testqantt.SetUtenzaBo;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.VerificaAttivazioneAsset;
import com.nttdata.qa.enel.testqantt.VerificaAttivazioneAssetVAS;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaScadenzaRipensamentoVAS_SWA;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SAP;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SEMPRE;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissioneVas;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id19_SINGLE_GAS_PE_VAS_Bollettino_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
		prop.setProperty("TIPO_UTENZA","PE");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("CODICE_FISCALE", "04922840113");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "GLDGWJ85C04F839A");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Libero");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		//  Dettaglio Sito
		prop.setProperty("MATRICOLA_CONTATORE", "1234567899");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("TIPO_INSTALLAZIONE", "ACCESSIBILE");
//		prop.setProperty("CATEGORIA_CONSUMO", "Industria");
//		Centri di recupero
		prop.setProperty("CATEGORIA_CONSUMO", "Agriturismo");
//		prop.setProperty("CATEGORIA_MARKETING", "MECCANICA");
		prop.setProperty("CATEGORIA_MARKETING", "AGRITURISMI");
		
//		prop.setProperty("ORDINE_GRANDEZZA", "INDUSTRIA");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "RISCALDAMENTO E PRODUZIONE");
		prop.setProperty("POTENZIALITA", "0");
		
//		prop.setProperty("UTILIZZO", "INDUSTRIALE");
		prop.setProperty("UTILIZZO", "ARTIGIANALE");
//		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A. • Libero");
		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		
		prop.setProperty("CAP", "00135");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
        prop.setProperty("COMMODITY", "GAS");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("PRODOTTO", "New_Soluzione Gas Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "CORPORATE SUPER");
        prop.setProperty("OPZIONE_VAS", "Bolletta Web");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
        prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "Chiusa da confermare");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("TIPO_REFERENTE", "");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE_VAS", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO_VAS", "Chiusa");
		
		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");

		prop.setProperty("RUN_LOCALLY", "Y");

		//
		prop.setProperty("MOD_MAIL_CERTIFICATA", "SI");
		prop.setProperty("MAIL_CERTIFICATA", "testing.crm.automation@gmail.com");
		
		return prop;
	}

	@After
	public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

 		prop.store(new FileOutputStream(nomeScenario), null);
  		String args[] = {nomeScenario};
  
  		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOGasNonResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);
		
		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOGasNonResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);
		
		PrecheckSwaEVO.main(args);
		
		//startare 5 minuti dopo l'emissione
		//Verifica status case+offer emissione contratto

		//Order
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);
		//Verifica Offerta Vas
		SetOrderLineItemVAStoRetreive.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissioneVas.main(args);
		UpdateDataRipensamentoSWA.main(args);
		
		//Startare dopo 1 gg - necessario passaggio BATCH notturno  
		//Order
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerificheScadenzaRipensamento_SWA.main(args);
		//Verifica Offerta Vas
		SetOrderLineItemVAStoRetreive.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerificaScadenzaRipensamentoVAS_SWA.main(args);
		
		//Order
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusR2DRicezioneOrdine.main(args);
		//Verifica Offerta Vas
		SetOrderLineItemVAStoRetreive.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdBPMVAS.main(args);
		
		//Check status offerta
		RecuperaStatusOffer.main(args);
		VerificaChiusuraOfferta.main(args);
		
		//Esiti POD ELE
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		
//		Accesso a R2D
		
		LoginR2D_GAS.main(args);
		
		TimeUnit.SECONDS.sleep(20);
		
		R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_Forzatura_Esiti_3OK_SWA_GAS.main(args); // OK Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_4OK_SWA_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_5OK_SWA_GAS.main(args); //OK Cla
		
		TimeUnit.SECONDS.sleep(5);

		R2D_VerifichePodFinali_SW_E_DISDETTE_GAS.main(args);

		//Verifiche post esiti R2D, aspettare 2 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);
		
		RecuperaStatusAsset.main(args);
		VerificaAttivazioneAsset.main(args);
		
		
		//Lanciare dopo 4 ore
		//Order
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SetUtenzaBo.main(args);
		//Verifica se un eventuale KO SAP è riprocessabile
		VerificaSeForzabile_KO_SAP.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SAP riprocessabile
		AssegnaAttivitaScartiSAP.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		
		//Verifica se un eventuale KO SEMPRE è riprocessabile
		VerificaSeForzabile_KO_SEMPRE.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SEMPRE riprocessabile
		AssegnaAttivitaScartiSEMPRE.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//VAS
		SetOrderLineItemVAStoRetreive.main(args);
		RecuperaStatusCase_and_Order.main(args);
		//Verifica se un eventuale KO SAP è riprocessabile
		VerificaSeForzabile_KO_SAP.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SAP riprocessabile
		AssegnaAttivitaScartiSAP.main(args);
		LoginSalesForce_Forzatura.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScarti.main(args);
		
		VerificaStatusSAP_ISUAttivazPod.main(args);
		
		
		//Verifica finale su chiusura Richiesta
		
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);
		
		

	}

}



