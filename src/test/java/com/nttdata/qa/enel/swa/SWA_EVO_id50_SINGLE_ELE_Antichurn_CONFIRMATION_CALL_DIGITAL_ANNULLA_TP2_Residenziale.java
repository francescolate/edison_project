package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCallDigital;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.AnnullaTP2;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaDigital;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitaPerNome;
import com.nttdata.qa.enel.testqantt.RecuperaAttività;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocale;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocaleDigital;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetUtenzaANTICHURN;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.ValidaModuloAdesione;
import com.nttdata.qa.enel.testqantt.VerificaAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivitaFatto;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaInviata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheVBLeBL;
import com.nttdata.qa.enel.testqantt.colla.LoginHomeBusinessArea;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class SWA_EVO_id50_SINGLE_ELE_Antichurn_CONFIRMATION_CALL_DIGITAL_ANNULLA_TP2_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S o Antichurn
//		prop.setProperty("USERNAME",Costanti.utenza_antichurn);
//		prop.setProperty("PASSWORD",Costanti.password_antichurn);
		prop.setProperty("USERNAME",Costanti.utenza_antichurn2);
		prop.setProperty("PASSWORD",Costanti.password_antichurn2);
		
		prop.setProperty("TIPO_UTENZA","Antichurn");
		prop.setProperty("DIGITAL","Y");
		prop.setProperty("MODALITA_CONFERMA","Conferma Digital");
		prop.setProperty("SCENARIO","CONFIRMATION_CALL_DIGITAL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("ANNULLAMENTO", "SI");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE • Vincolato");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
//		prop.setProperty("PRODOTTO", "Sempre Con Te");
		prop.setProperty("PRODOTTO", "Speciale Luce 60");
//		prop.setProperty("PRODOTTO", Costanti.prodotto_residenziale_ele);
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "SOSPESA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("OPZIONE_VAS", "");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Sospesa");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "SOSPESA");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

    @After
 public void fineTest() throws Exception{
    	
          String args[] = {nomeScenario};
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
          
    }


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*	
		AbilitaConfirmationCallDigital.main(args);
		
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenziale.main(args);
	
		RiepilogoOffertaDigital.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);

		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		RegistrazioneVocaleDigital.main(args);
		
		PrecheckSwaEVO.main(args);

		//startare 3 minuti dopo l'emissione per verificare lo Stato=SOSPESA
		TimeUnit.SECONDS.sleep(180);

		//Verifica offerta in stato sospesa
		RecuperaStatusCase_and_Offer.main(args);
		VerificaStatoOffertaSospesa.main(args);
	
		//	Gestione TP2 
		SetUtenzaMailResidenziale.main(args);

		RecuperaLinkTP2.main(args);
		
		LoginPortalTP2.main(args);

		AnnullaTP2.main(args);
		
		RecuperaRichiestaTouchPoint.main(args);
		
		VerificaChiusuraTouchPoint.main(args);
	
		RecuperaStatusOffer.main(args);

		VerificaStatoOffertaAnnullata.main(args);
	
		SetUtenzaANTICHURN.main(args);

		DisabilitaQC_CC.main(args);//unico in quanto disattiva in base ad id univoco
*/

	}

}



