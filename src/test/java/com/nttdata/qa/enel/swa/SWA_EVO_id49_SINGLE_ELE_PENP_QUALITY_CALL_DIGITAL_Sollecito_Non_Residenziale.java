package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.AnnullaTP2;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaTP2;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitaPerNome;
import com.nttdata.qa.enel.testqantt.RecuperaAttività;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Sollecito;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetScenarioQualityCallInterna;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailNonResidenziale;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenzialexNonRes;
import com.nttdata.qa.enel.testqantt.SetUtenzaOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.SetUtenzaPENP;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.UpdateScadenzaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.UpdateSollecitoRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.ValidaModuloAdesione;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneAttivita;
import com.nttdata.qa.enel.testqantt.VerificaQuoteQualityCallDigital;
import com.nttdata.qa.enel.testqantt.VerificaScadenzaTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivitaFatto;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaInviata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id49_SINGLE_ELE_PENP_QUALITY_CALL_DIGITAL_Sollecito_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA","PENP");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
//		prop.setProperty("CODICE_FISCALE", "02752081204");
		prop.setProperty("CODICE_FISCALE", "69174850078");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "BSNTTN88T10F839T");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Diverso da Abitazione");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ALTRI SERVIZI");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE", "220");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
        prop.setProperty("ELIMINA_VAS", "Y");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
        prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("TIPO_REFERENTE", "");
		prop.setProperty("OPZIONE_VAS", "");
		
		prop.setProperty("SCENARIO", "QUALITY_CALL_DIGITAL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		
		prop.setProperty("TIPO_OI_ORDER", "Commodity");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void fineTest() throws Exception{
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

 		prop.store(new FileOutputStream(nomeScenario), null);
  		String args[] = {nomeScenario};
  		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** AbilitaQualityCallDigital.main(args);
//		*************************************************************************

		//Data Preparation query 98 per abilitare il canale alla quality call
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVONonResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);
		
		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoNonResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);
		
		PrecheckSwaEVO.main(args);
	
		Thread.currentThread().sleep(60000);
		
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaSospesa.main(args);
    	
		VerificaQuoteQualityCallDigital.main(args);
		
		//bisogna aspettare che passi il batch affinchè si generi l'attività, un ora circa
    	SetUtenzaOperatoreAvanzato.main(args);
		
		RecuperaAttività.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaAttività.main(args);
		
		ValidaModuloAdesione.main(args);
		
		//Verifica offerta in stato sospesa
    	RecuperaStatusOffer.main(args);
	
		VerificaStatoOffertaSospesa.main(args);
		
		VerificaQuoteQualityCallDigital.main(args);
		
        //Gestione TP2 e Web  
		// SetUtenzaMailNonResidenziale.main(args); --> solo x questo cliente andiamo sull'email usata per RES
		SetUtenzaMailResidenzialexNonRes.main(args); //Per Cliente "69174850078" che va su altra email
		
		RecuperaLinkTP2.main(args);
	
		UpdateSollecitoRichiestaTouchPoint.main(args);
		
		//Aspettare giorno successivoCOMMODITY
		Thread.currentThread().sleep(60000);
		
		RecuperaLinkTP2Sollecito.main(args);
			
		LoginPortalTP2.main(args);
		
		ConfermaTP2.main(args);
		
		RecuperaRichiestaTouchPoint.main(args);
		
		VerificaChiusuraTouchPoint.main(args);
		
//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** SetUtenzaPENP.main(args);
//		*** DisabilitaQualityCallDigital.main(args);
//		*************************************************************************
		
	}

}



