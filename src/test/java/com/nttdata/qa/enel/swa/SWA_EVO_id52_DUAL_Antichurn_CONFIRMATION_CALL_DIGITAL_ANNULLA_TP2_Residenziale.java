package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCallDigital;
import com.nttdata.qa.enel.testqantt.AnnullaTP2;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaUnitaAbitativaSWAEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaDigital;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVODUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Dual;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistenteDual;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocaleDigital;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetUtenzaANTICHURN;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailTP8Residenziale;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id52_DUAL_Antichurn_CONFIRMATION_CALL_DIGITAL_ANNULLA_TP2_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S o Antichurn
//		prop.setProperty("USERNAME",Costanti.utenza_antichurn);
//		prop.setProperty("PASSWORD",Costanti.password_antichurn);
		prop.setProperty("USERNAME",Costanti.utenza_antichurn2);
		prop.setProperty("PASSWORD",Costanti.password_antichurn2);
		prop.setProperty("TIPO_UTENZA","Antichurn");
		prop.setProperty("DIGITAL","Y");
		prop.setProperty("MODALITA_CONFERMA","Conferma Digital");
		prop.setProperty("SCENARIO","CONFIRMATION_CALL_DIGITAL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COMMODITY", "ELE");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		//dati pod ELE
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA_ELE", "Vincolato");
		prop.setProperty("TITOLARITA_ELE","Uso/Abitazione");
		prop.setProperty("RESIDENTE_ELE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO NAZIONALE");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE • Vincolato");
		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "servizio elettrico romano");
		prop.setProperty("CAP_ELE", "00198");
		prop.setProperty("REGIONE_ELE", "LAZIO");
		prop.setProperty("PROVINCIA_ELE", "ROMA");
		prop.setProperty("CITTA_ELE", "ROMA");
		prop.setProperty("INDIRIZZO_ELE", "VIA NIZZA");
		prop.setProperty("CIVICO_ELE", "4");
		//Dati pod GAS
		prop.setProperty("CAP_GAS", "00198");
		prop.setProperty("REGIONE_GAS", "LAZIO");
		prop.setProperty("PROVINCIA_GAS", "ROMA");
		prop.setProperty("CITTA_GAS", "ROMA");
		prop.setProperty("INDIRIZZO_GAS", "VIA NIZZA");
		prop.setProperty("CIVICO_GAS", "4");
		prop.setProperty("MERCATO_DI_PROVENIENZA_GAS", "Libero");
		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A.");
//		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A. • Libero");
		prop.setProperty("TITOLARITA_GAS", "Uso/Abitazione");
		
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "Vocal order");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		//Prodotto ELE
		prop.setProperty("PRODOTTO_ELE", "Speciale Luce 60");
//		prop.setProperty("PRODOTTO_ELE", Costanti.prodotto_residenziale_ele);
		//Prodotto GAS
		prop.setProperty("PRODOTTO_GAS", "SoloXTe Gas");
//		prop.setProperty("PRODOTTO_GAS", Costanti.prodotto_residenziale_gas);
		
		prop.setProperty("UNITA_ABITATIVA", "NO");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "SOSPESA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("OPZIONE_VAS", "");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Sospesa");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "SOSPESA");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

    @After
 public void fineTest() throws Exception{
    	
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
          
    }


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*
		AbilitaConfirmationCallDigital.main(args);
		
		RecuperaPodNonEsistenteDual.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenzialeDUAL.main(args);
		
		RiepilogoOffertaDigital.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaUnitaAbitativaSWAEVOResidenziale.main(args);
		
		ConfiguraProdottoSWAEVODUALResidenziale.main(args);
		
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);

		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
	
		RegistrazioneVocaleDigital.main(args);
		
		PrecheckSwaEVODUAL.main(args);
		
		//startare 3 minuti dopo l'emissione per verificare lo Stato=SOSPESA
		TimeUnit.SECONDS.sleep(180);

		//Verifica offerta in stato sospesa
    	RecuperaStatusOffer.main(args);
		VerificaStatoOffertaSospesa.main(args);
		
		//	Gestione TP2 
		SetUtenzaMailResidenziale.main(args); 

		RecuperaLinkTP2Dual.main(args);

		LoginPortalTP2.main(args);
		
		AnnullaTP2.main(args);
		
		RecuperaRichiestaTouchPoint.main(args);
		
		VerificaChiusuraTouchPoint.main(args);
	
		TimeUnit.SECONDS.sleep(180);
		RecuperaStatusOffer.main(args);

		//startare 3 minuti dopo l'emissione per verificare lo Stato=SOSPESA
		VerificaStatoOffertaAnnullata.main(args);
	
		SetUtenzaANTICHURN.main(args);

		DisabilitaQC_CC.main(args);
*/		
	}

}




