package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SetDatiAnagraficiSDD;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoRIDEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeGas;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamentoRIDAsset;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id80_SINGLE_GAS_PE_RID_EST_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() throws Exception {
		Properties prop = new Properties();
		
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");


		// Valorizzare anche TIPO_UTENZA con PE 
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
		prop.setProperty("TIPO_UTENZA","PE");

		
		//CREAZIONE CLIENTE
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "M");
		prop.setProperty("DATA_NASCITA", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		
		
		prop.setProperty("CAP", "00135");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("COMUNE", "ROMA");
		
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		


		//DATI CLIENTE
		//prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		//prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega"); 
		
		
		
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		
		
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "GAS");
		
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Libero");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","SI"); 
		
		
	
		

		//IBAN
		prop.setProperty("IS_IBAN_ESTERO","Y");
		prop.setProperty("IBAN","FR2414508000404273857151W16");


		//MODALITA FIRMA
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		
		
		//Prodotto GAS
		prop.setProperty("PRODOTTO", "NEW_Energia Sicura Gas_15 percento");
		prop.setProperty("VAS_EX_GAS", "SI");
		
		
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "Chiusa da confermare");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("COMMODITY", "GAS");

		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE_VAS", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");

		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO_VAS", "Chiusa");

		prop.setProperty("ESITO_UDB","OK");

		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");


		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		//CREAZIONE NUOVO CLIENTE
		LoginSalesForce.main(args);
		CreazioneNuovoCliente.main(args);
		
		
		//START SWA
		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoSwitchAttivoEVOResidenzialeGas.main(args);
		RiepilogoOfferta.main(args);
		ConfermaIndirizziSWAEVO.main(args);
		ConfermaFatturazioneElettronicaSWAEVO.main(args);
		SetDatiAnagraficiSDD.main(args);
		PagamentoRIDEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoSWAEVOGasResidenziale.main(args);
		ConfermaModalitaFirmaDigital.main(args);
		ConsensiEContatti.main(args);
		GestioneDocumentaleSWAEvo.main(args);
		PrecheckSwaEVO.main(args);

		
		//startare 5 minuti dopo l'emissione
		//Verifica status case+offer emissione contratto
		LOGGER.info(">>>>>>>>> START VERIFICHE EMISSIONE ORDINE");
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);
		UpdateDataRipensamentoSWA.main(args);

		
		//startare giorno dopo
		//Verifica status case+order creazione ordine commodity
		LOGGER.info(">>>>>>>>> START VERIFICHE STATO UDB/RD2 CREAZIONE ORDINE");
		RecuperaStatusCase_and_Offer.main(args);
		VerificheScadenzaRipensamento_SWA.main(args);//qui 01.09.2021
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdBPM.main(args);
		SalvaIdOrdine.main(args);
		VerificaStatusUDBCreazioneOrdine.main(args);
		VerificaStatusR2DRicezioneOrdine.main(args);

		LOGGER.info(">>>>>>>>> START VERIFICHE STATO UDB POST ATTIVAZIONE POD");
		InvioEsitoUBD.main(args);//Invio dell'esito UBD
		//Lanciare dopo 5 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusUDBAttivazPod.main(args);

		
		LOGGER.info(">>>>>>>>> START INVIO ESITI R2D");
		//Accesso a R2D
		LoginR2D_GAS.main(args);
		TimeUnit.SECONDS.sleep(20);
		R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		R2D_Forzatura_Esiti_3OK_SWA_GAS.main(args); // OK Cla
		TimeUnit.SECONDS.sleep(5);
		R2D_CaricamentoEsiti_4OK_SWA_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
		R2D_CaricamentoEsiti_5OK_SWA_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodFinali_SW_E_DISDETTE_GAS.main(args);

		//Verifiche post esiti R2D, aspettare 2 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);

		RecuperaStatusAsset.main(args);
		VerificaMetodoPagamentoRIDAsset.main(args);

		//Lanciare dopo 4 ore
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		VerificheAttivazioneRichiesta.main(args);

	}

}



