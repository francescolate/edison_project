package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaPreCheckInviata;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.ControllaeConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte1;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte2;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_SimulatoriConfiguratoriEsitiFlussiSII_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id63_SINGLE_GAS_PENP_Bollettino_PRECHECK_INVIATA_OK_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA", "PENP");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "04922840113");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "GLDGWJ85C04F839A");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPOLOGIA_CLIENTE","Non Residenziale");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Libero");
		//  Dettaglio Sito
		prop.setProperty("MATRICOLA_CONTATORE", "1234567899");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("TIPO_INSTALLAZIONE", "ACCESSIBILE");
//		prop.setProperty("CATEGORIA_CONSUMO", "Industria");
//		Centri di recupero
		prop.setProperty("CATEGORIA_CONSUMO", "Agriturismo");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("CATEGORIA_MARKETING", "AGRITURISMI");
		
//		prop.setProperty("ORDINE_GRANDEZZA", "INDUSTRIA");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "RISCALDAMENTO E PRODUZIONE");
		prop.setProperty("POTENZIALITA", "0");
		
//		prop.setProperty("UTILIZZO", "INDUSTRIALE");
		prop.setProperty("UTILIZZO", "ARTIGIANALE");
//		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
//        prop.setProperty("PRODOTTO", "SoloXTe Gas");
//        prop.setProperty("PRODOTTO", "GiustaXte Gas");
        prop.setProperty("PRODOTTO", "GiustaXte Gas Impresa");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("OPZIONE_VAS", "");
		
		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
//		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_cla);
//		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_cla);
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
//		prop.setProperty("DATA_MINIMA_SW", "01/06/2020");
//		prop.setProperty("VERIFICA_AMM", "1");
//		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
		prop.setProperty("SCENARIO", "QUALITY_CALL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");

		//Configurazione Flusso SII PKG1
		prop.setProperty("PIVA_UTENTE","06655971007");
		prop.setProperty("PIVA_GESTORE","05877611003"); // --> campo pre-impostato su r2D
		prop.setProperty("CP_UTENTE","23445354325"); // (Questo valore è un identificativo e può essere modificato)
		prop.setProperty("CP_GESTORE","01010101033"); // (Questo valore è un identificativo e deve essere modificato per ogni simulazione)
		prop.setProperty("VERIFICA_AMM","1"); // --> con 1 e '1' in Esito mi da "OK"
		prop.setProperty("ESITO_ATTESO","OK"); // Valorizzare in base allo scenario se KO o OK per verifica finale
		prop.setProperty("ESITO","1");
		prop.setProperty("COD_ESITO","202");
		prop.setProperty("DETTAGLIO_ESITO","Nome e cognome o Ragione sociale corretti e dato aggiornato");
		prop.setProperty("TIPO_PDR","0");
		prop.setProperty("CODICE_REMI","34355800");
		prop.setProperty("CF_STRANIERO","0");
		prop.setProperty("PIVA_CONTR_COMM","00905811006");
		prop.setProperty("RAG_SOC_CONTR_COMM","MARIO ROSSI");
		prop.setProperty("PEC_CONTR_COMM","mariorossi@pec.com");
		prop.setProperty("DENOMINAZIONE","Denominazione");
		prop.setProperty("PIVA_DISTRIBUTORE","00905811006");

		prop.setProperty("RUN_LOCALLY", "Y");
        return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {
		
//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*	
		AbilitaPreCheckInviata.main(args);
		
		RecuperaPodNonEsistente.main(args);

		LoginR2D_GAS.main(args);	
		
		TimeUnit.SECONDS.sleep(5);

		R2D_SimulatoriConfiguratoriEsitiFlussiSII_GAS.main(args);

		LoginSalesForce.main(args);
	
//		SbloccaTab.main(args);
//		
//		CercaClientePerNuovaInterazioneEVO.main(args);
*/LoginSalesForce.main(args);
		ProcessoSwitchAttivoEVOGasNonResidenziale.main(args);
	
		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);

		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);

		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ControllaeConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOGasNonResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
	
		GestioneDocumentaleSWAEvo.main(args);
	
		PrecheckSwaEVOParte1.main(args);

		PrecheckSwaEVOParte2.main(args);

//		// le due query sono inutili con la nuova gestione.
//		RecuperaStatoPrecheckOK.main(args);
//		UpdateStatoPrecheckOK.main(args);
		
		
	}

}



