package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCallDigital;
import com.nttdata.qa.enel.testqantt.AbilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaTP2;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfirmationCall;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.DisabilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.DisabilitaQC_CC;
import com.nttdata.qa.enel.testqantt.DisabilitaQualityCheck;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaAttività;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàDMSUploadPENP;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàQualityCallInterna;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Dual;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RegistrazioneVocale;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SelezionaQualityCall;
import com.nttdata.qa.enel.testqantt.SelezionaQualityCheck;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SetUtenzaOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.ValidaModuloAdesione;
import com.nttdata.qa.enel.testqantt.VerificaAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraAttivitàQualityCheck;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaQuoteQualityCallDigital;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivita;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivitaFatto;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaChiusaDaConfermare;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaInviata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheVBLeBL;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id43_SINGLE_ELE_PENP_QUALITY_CALL_DIGITAL_OK_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";
		
	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA", "PENP");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("PRODOTTO", "Speciale Luce 60");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
//		prop.setProperty("PATH_DOCUMENTO", "C:\\Users\\galdieroro\\Documents\\My Received Files\\a.pdf");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE_DISPONIBILE", "220");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
		prop.setProperty("SCENARIO", "QUALITY_CALL_DIGITAL");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Residenziale");
		prop.setProperty("OPZIONE_VAS", "");
		prop.setProperty("ANNULLAMENTO", "SI");
        
//		prop.setProperty("TIPOMERCATO", "Libero");
//
//		  
//		prop.setProperty("NUOVA_POTENZA","3"); 
//		prop.setProperty("DEALERPHONE","3333131311");  
//		 
//		prop.setProperty("COMMVALUE","12");
//		prop.setProperty("POTENZAANNUA","3300");  
//		
//		prop.setProperty("VOCALNOVOCAL","No Vocal");
//		prop.setProperty("MODALITAFIRMA","POSTA");
//		prop.setProperty("MODULE_ENABLED","Y");
//		prop.setProperty("TARIFFA","Senza Orari");
//		prop.setProperty("EXPECTEDSTATUS","Chiusa da confermare");
//		prop.setProperty("FILENAME","src/main/resources/C.T.E.pdf");
//		//Verifiche post emissione ordine
//		prop.setProperty("EXPECTEDSTATUS_OFFERTA_POST_EMISSIONE_SWA", "Chiusa");
//		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
//		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
//		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
//		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
//		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
//		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
//		//Property per verifiche post emissione
//		prop.setProperty("EXPECTEDSTATUS_RICHIESTA_FINALE","Chiuso");
//		prop.setProperty("EXPECTED_SOTTO_STATUS_RICHIESTA_FINALE", "Espletato");
//		prop.setProperty("NUMERO_CONTRATTO", "1234567");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
	
//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** AbilitaQualityCallDigital.main(args);
//		*************************************************************************
/*
		//Data Preparation query 98 per abilitare il canale alla quality call
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);

		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenziale.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoResidenziale.main(args);
		
		//ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);

		PrecheckSwaEVO.main(args);

		RecuperaStatusCase_and_Offer.main(args);
		
		VerificaStatoOffertaInviata.main(args);
		
		//bisogna aspettare che passi il batch affinchè si generi l'attività

		SetUtenzaOperatoreAvanzato.main(args);

		RecuperaAttivitàDMSUploadPENP.main(args);
	
		SetUtenzaPENP.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaAttività.main(args);
			
		ValidaModuloAdesione.main(args);
			
		RecuperaAttivitàDMSUploadPENP.main(args);
		
    	VerificaStatoAttivitaFatto.main(args);
		
		RecuperaStatusCase_and_Offer.main(args);
		
		VerificaStatoOffertaSospesa.main(args);

		RecuperaStatusOffer.main(args);
		
		VerificaQuoteQualityCallDigital.main(args);
*/		
		// TP2
		RecuperaLinkTP2.main(args);
		
		SetUtenzaMailResidenziale.main(args);
		
		LoginPortalTP2.main(args);
		
		ConfermaTP2.main(args);
		
		RecuperaRichiestaTouchPoint.main(args);
		
		VerificaChiusuraTouchPoint.main(args);
	
		//Dopo 5 minuti
		TimeUnit.SECONDS.sleep(300);
		
		RecuperaStatusCase_and_Offer.main(args);
		
		VerificaStatoOffertaChiusaDaConfermare.main(args);

//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** SetUtenzaPENP.main(args);
//		*** DisabilitaQC_CC.main(args);
//		*************************************************************************
		

	}

}



