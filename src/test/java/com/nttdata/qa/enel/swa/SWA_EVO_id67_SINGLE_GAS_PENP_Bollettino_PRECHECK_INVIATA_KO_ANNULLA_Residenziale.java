package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AbilitaConfirmationCall;
import com.nttdata.qa.enel.testqantt.AbilitaPreCheckInviata;
import com.nttdata.qa.enel.testqantt.AnnullaOfferta;
import com.nttdata.qa.enel.testqantt.AnnullaPrecheck;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ClickPrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.ConfirmationCall;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.ControllaeConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.RipristinaStatoPrecheck;
import com.nttdata.qa.enel.testqantt.SalvaPrecheckAndSettaKO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOKO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte1;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOParte2;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenzialeGas;
import com.nttdata.qa.enel.testqantt.RecuperaAttività;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatoPrecheckKO;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SetUtenzaPENP;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateStatoPrecheckKO;
import com.nttdata.qa.enel.testqantt.UpdateStatoPrecheckOK;
import com.nttdata.qa.enel.testqantt.ValidaModuloAdesione;
import com.nttdata.qa.enel.testqantt.VerificaCausaleSospensione;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivita;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.RecuperaStatoPrecheckOK;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_SimulatoriConfiguratoriEsitiFlussiSII_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class SWA_EVO_id67_SINGLE_GAS_PENP_Bollettino_PRECHECK_INVIATA_KO_ANNULLA_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA", "PENP");
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPOLOGIA_CLIENTE","Residenziale");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Libero");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","SI"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A.");
//		prop.setProperty("SOCIETA_DI_VENDITA", "Eni gas e luce S.p.A. • Libero");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
//        prop.setProperty("PRODOTTO", "SoloXTe Gas");
//        prop.setProperty("PRODOTTO", "GiustaXte Gas");
		prop.setProperty("PRODOTTO", Costanti.prodotto_residenziale_gas); // GIUSTAXTE GAS SPECIAL 3
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("VAS_EX_GAS", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "Chiusa da confermare");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("OPZIONE_VAS", "");
		prop.setProperty("RUN_LOCALLY", "Y");

		//Configurazione Flusso SII PKG1
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("PIVA_UTENTE","06655971007");
		prop.setProperty("PIVA_GESTORE","05877611003"); // --> campo pre-impostato su r2D
		prop.setProperty("CP_UTENTE","23445354325"); // (Questo valore è un identificativo e può essere modificato)
		prop.setProperty("CP_GESTORE","01010101033"); // (Questo valore è un identificativo e deve essere modificato per ogni simulazione)
		prop.setProperty("VERIFICA_AMM","1"); // --> con 1 e '0' in Esito mi da "KO"
		prop.setProperty("ESITO_ATTESO","KO"); // Valorizzare in base allo scenario se KO o OK per verifica finale
		prop.setProperty("ESITO","0");
		prop.setProperty("COD_ESITO","021");
		prop.setProperty("DETTAGLIO_ESITO","PDR già attivo con l'utente richiedente");
		prop.setProperty("TIPO_PDR","0");
		prop.setProperty("CODICE_REMI","34355800");
		prop.setProperty("CF_STRANIERO","0");
		prop.setProperty("PIVA_CONTR_COMM","00905811006");
		prop.setProperty("RAG_SOC_CONTR_COMM","MARIO ROSSI");
		prop.setProperty("PEC_CONTR_COMM","mariorossi@pec.com");
		prop.setProperty("DENOMINAZIONE","Denominazione");
		prop.setProperty("PIVA_DISTRIBUTORE","00905811006");
		
		
		return prop;
	}

    @After
 public void fineTest() throws Exception{
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
    }


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {
		
//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*
		AbilitaPreCheckInviata.main(args);
		
		RecuperaPodNonEsistente.main(args);

		LoginR2D_GAS.main(args);	
		
		TimeUnit.SECONDS.sleep(5);

		R2D_SimulatoriConfiguratoriEsitiFlussiSII_GAS.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoEVOResidenzialeGas.main(args);
	
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOGasResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
	
		GestioneDocumentaleSWAEvo.main(args);
		
		// Imposta i flag del "KO" ad "Y" per ottenere il KO da SFDC
		SalvaPrecheckAndSettaKO.main(args);

		PrecheckSwaEVOParte1.main(args);

		AnnullaPrecheck.main(args);
*/
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaAnnullata.main(args);
		
//		moduli non necessari dopo modifica gestione Precheck
//		RipristinaStatoPrecheck.main(args);
	
	}

}



