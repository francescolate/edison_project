package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVODUALNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfirmationCall;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVODUAL;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenzialeDUAL;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàDMSUploadPENP;
import com.nttdata.qa.enel.testqantt.RecuperaAttivitàQualityCallInterna;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistenteDual;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaAttività;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SetUtenzaOperatoreAvanzato;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.ValidaDocumentiAttività;
import com.nttdata.qa.enel.testqantt.ValidaDocumentiAttivitàFast;
import com.nttdata.qa.enel.testqantt.ValidaModuloAdesione;
import com.nttdata.qa.enel.testqantt.VerificaQuoteQualityCall;
import com.nttdata.qa.enel.testqantt.VerificaStatoAttivitaFatto;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaChiusaDaConfermare;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaSospesa;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaChiusaDaConfermare;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SWA_EVO_id39_DUAL_PENP_QUALITY_CALL_COMPLETATA_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;

	String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_penp_mag);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_penp_mag);
		prop.setProperty("TIPO_UTENZA","PENP");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "26761360796");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "LVENDR53T08L736K");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		//dati pod ELE
		prop.setProperty("TIPO_INSTALLAZIONE", "ACCESSIBILE");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ACQUA");
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("MERCATO_DI_PROVENIENZA_ELE", "Vincolato");
		prop.setProperty("TITOLARITA_ELE","Uso/Abitazione");
		prop.setProperty("RESIDENTE_ELE","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO NAZIONALE");
//		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA_ELE", "servizio elettrico romano");
		prop.setProperty("CAP_ELE", "00198");
		prop.setProperty("REGIONE_ELE", "LAZIO");
		prop.setProperty("PROVINCIA_ELE", "ROMA");
		prop.setProperty("CITTA_ELE", "ROMA");
		prop.setProperty("INDIRIZZO_ELE", "VIA NIZZA");
		prop.setProperty("CIVICO_ELE", "4");
		//Dati pod GAS
		prop.setProperty("CAP_GAS", "00198");
		prop.setProperty("REGIONE_GAS", "LAZIO");
		prop.setProperty("PROVINCIA_GAS", "ROMA");
		prop.setProperty("CITTA_GAS", "ROMA");
		prop.setProperty("INDIRIZZO_GAS", "VIA NIZZA");
		prop.setProperty("CIVICO_GAS", "4");
		prop.setProperty("SOCIETA_DI_VENDITA_GAS", "Eni gas e luce S.p.A.");
		prop.setProperty("MERCATO_DI_PROVENIENZA_GAS", "Libero");
		prop.setProperty("TITOLARITA_GAS", "Uso/Abitazione");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		//Prodotto ELE
        prop.setProperty("PRODOTTO_ELE", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "CORPORATE SUPER");
        prop.setProperty("ELIMINA_VAS", "Y");
		//Prodotto GAS
		prop.setProperty("PRODOTTO_GAS", "New_Soluzione Gas Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "CORPORATE SUPER");
		
		prop.setProperty("UNITA_ABITATIVA", "NO");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("IBAN","IT59F0529673970CC0000033409");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
//// variabili aggiunte da Claudio
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
		prop.setProperty("TENSIONE", "220");
		prop.setProperty("CONSUMO_ANNUO_ELE", "1500");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TIPOLOGIA_CLIENTE", "Non Residenziale");
		prop.setProperty("CATEGORIA_MARKETING", "AGRITURISMI");
		prop.setProperty("CATEGORIA_CONSUMO", "Agriturismo");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "RISCALDAMENTO E PRODUZIONE");
		prop.setProperty("POTENZIALITA", "0");
		prop.setProperty("UTILIZZO", "ARTIGIANALE");
		prop.setProperty("MATRICOLA_CONTATORE", "1234567899");
		prop.setProperty("SCENARIO", "QUALITY_CALL");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("CARICA_DOCUMENTO", "SI");

		prop.setProperty("RUN_LOCALLY", "Y");
        return prop;
	}

	@After
	public void tearDown() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** AbilitaQualityCall.main(args);
//		*************************************************************************

		//Data Preparation query 98 per abilitare il canale alla quality call
		RecuperaPodNonEsistenteDual.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazione.main(args);

		ProcessoSwitchAttivoEVONonResidenzialeDUAL.main(args);

		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);

		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);

		PagamentoBollettinoPostaleEVO.main(args);

		ConfiguraProdottoSWAEVODUALNonResidenziale.main(args);

		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);

		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
	
		GestioneDocumentaleSWAEvo.main(args);
	
		PrecheckSwaEVODUAL.main(args);
		
//		*** Solo se la sessione è scaduta - Inizio *** 	
//		LoginSalesForce.main(args);
//		RicercaOfferta.main(args);
//	*** Solo se la sessione è scaduta - Fine ***	
		
		TimeUnit.SECONDS.sleep(60);
		RecuperaStatusOffer.main(args);
		VerificaQuoteQualityCall.main(args);

		//aspettare batch che gira ogni ora 
		TimeUnit.SECONDS.sleep(3600);
		
		// Al passaggio del Document "Local upload" in stato Acquisito, viene creata la relativa attività "DMS Local Upload PENP".
		// Assegnare l'attività ad un utente SFDC di tipo Operatore Cliente Avanzato.
		SetUtenzaOperatoreAvanzato.main(args);
	
		RecuperaAttivitàDMSUploadPENP.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaAttività.main(args);
		
		ValidaModuloAdesione.main(args);
		
		//RicercaOfferta.main(args); --> è inutile!
		
//		Al passaggio dell'Offerta in SOSPESA viene creata la relativa attività "Quality Call Interna"
		//RecuperaStatusCase_and_Offer.main(args);
		RecuperaStatusOffer.main(args);
		
		VerificaStatoOffertaSospesa.main(args);

		RecuperaAttivitàQualityCallInterna.main(args);
		
		LoginSalesForce.main(args);
		
		RicercaAttività.main(args);
		
		// Si aprirà pop-up da cui l'operatore potrà selezionare l'esito "CHIAMATA COMPLETATA". Premere Conferma.		
		ConfirmationCall.main(args); // anche per Quality Call
		
//		 Lo stato dell'attività  di Quality Call avanzerà in stato FATTO.
		RecuperaAttivitàQualityCallInterna.main(args);
		VerificaStatoAttivitaFatto.main(args);
		
		LoginSalesForce.main(args);

		RicercaOfferta.main(args);

		SelezionaConfermaOfferta.main(args);
		
		//Dopo 1 minuto 
		TimeUnit.SECONDS.sleep(60);
		RecuperaStatusOffer.main(args);
		VerificaStatoOffertaChiusaDaConfermare.main(args);
	
//		*************************************************************************
//      *** DP non più necessaria perchè flag sempre Attivo dal 02/08/2021
//		*** SetUtenzaPENP.main(args);
//		*** DisabilitaQC_CC.main(args);
//		*************************************************************************


	}

}



