package com.nttdata.qa.enel.swa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SetDatiAnagraficiSDD;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizzoSpedizionePlicoSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PagamentoRIDEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamentoRIDAsset;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DBozzaOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.PartitaIva;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

/**
 * SWA SINGLE ELE 
 * CLIENTE NON RESIDENZIALE 
 * Canale S2S 
 * Metodo di pgamento RID
 * IBAN ESTERO 
 ********************************* 
 * Risultato atteso: Corretta
 * creazione dell'offerta. Corretto creazione dell'ordine alla chiusura
 * dell'offerta. Corretto invio dell'ordine verso il BPM, fino a UDB in attesa.
 *
 */

//Preso a riferimento SWA EVO 22 - 
public class SWA_EVO_id81_SINGLE_ELE_S2S_RID_EST_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() throws Exception {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		
		//CONFIGURAZIONE UTENZA
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_attivazioni);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_attivazioni);
		prop.setProperty("TIPO_UTENZA", "S2S");
		
		
	
		
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
		prop.setProperty("COMMODITY", "ELE");
		
		
		//DATI CLIENTE
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		
		
		//DATI FORNITURA
		prop.setProperty("USO","Uso Diverso da Abitazione");
		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("POTENZA", "3");
		prop.setProperty("TENSIONE", "220");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("CONSUMO_ANNUO", "1500");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ACQUA");
		//prop.setProperty("CATEGORIA_MERCEOLOGICA", "CONSUMI RELATIVI AL POMPAGGIO");
		
		//INVIO CONTRATTO E MODALITA FIRMA
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		
		
		
		
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
	
		//Property creazione cliente
		prop.setProperty("TIPOLOGIA_CLIENTE", "BUSINESS");
		String piva = PartitaIva.getPartitaIVA();
		prop.setProperty("CODICE_FISCALE", piva);
		prop.setProperty("PARTITA_IVA", piva);
		prop.setProperty("TIPO_FORMA_GIURIDICA", "s.p.a");
		prop.setProperty("RAGIONE_SOCIALE", "AUTOMATION TEAM");
		prop.setProperty("COGNOME_REFERENTE", "GALDIERO");
		prop.setProperty("NOME_REFERENTE", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO_REFERENTE", "M");
		prop.setProperty("DATA_NASCITA_REFERENTE", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA_REFERENTE", "Napoli");
		prop.setProperty("RUOLO_REFERENTE", "Titolare");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TELEFONO_CLIENTE", "0819987645");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");
		
		//INDIRIZZO
		prop.setProperty("CAP", "00135");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		
		//DATI FATTURAZIONE
		prop.setProperty("CANALE_INVIO_FATTURAZIONE","SDI");
		prop.setProperty("CODICE_UFFICIO", "0000000");
		
		//CIG-CUP e SPLIT PAYMENT
		prop.setProperty("SPLIT_PAYMENT", "No");
	    prop.setProperty("FLAG_136", "NO");
		
        //CONFIGURAZIONE PRODOTTO
		prop.setProperty("PRODOTTO", "New_Trend Sicuro Energia Impresa Business");
		prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
		
		
		
		////////////////////////
	    
	    
		
       
		
		prop.setProperty("IS_IBAN_ESTERO","Y");
        prop.setProperty("IBAN","FR2414508000404273857151W16");
        

		
		
//        prop.setProperty("SCELTA_ABBONAMENTI", "MONO");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
        prop.setProperty("CARICA_DOCUMENTO", "NO");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("OPZIONE_VAS", "");


		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("ESITO_UDB","OK");
		

		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
		
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

 		prop.store(new FileOutputStream(nomeScenario), null);
  		String args[] = {nomeScenario};
  
  		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
	
		LoginSalesForce.main(args);
		CreazioneNuovoCliente.main(args);
		
		//Recupero POD
		RecuperaPodNonEsistente.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		
		//selezione processo, identificazione interolocutore - compilazione indirizzo
		ProcessoSwitchAttivoEVONonResidenziale.main(args);
		
		
		
		RiepilogoOfferta.main(args);
		
		ReferenteSwaEVO.main(args);

		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);
		
		SplitPaymentSwaEVO.main(args);
		
		CigCugpSwaEVO.main(args);
		
		
		SetDatiAnagraficiSDD.main(args);
		PagamentoRIDEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args); 
		
		
		
		ConfiguraProdottoSWAEVOElettricoNonResidenziale.main(args);
				
		ConfermaIndirizzoSpedizionePlicoSWAEVO.main(args);
	
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
			
		GestioneDocumentaleSWAEvo.main(args);
			
        VerificheRichiesta.main(args);
       
		CaricaEValidaDocumenti.main(args);

		 
		//startare 5 minuti dopo l'emissione
		//Verifica status case+offer emissione contratto
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);

		//startare giorno dopo
		//Verifica status case+order creazione ordine commodity
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusUDBCreazioneOrdine.main(args);
		
		//Effettuare update su data ripensamento
		UpdateDataRipensamentoSWA.main(args);
		
		//Startare dopo 2 ore
		RecuperaStatusCase_and_Offer.main(args);
		VerificheScadenzaRipensamento_SWA.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusR2DRicezioneOrdine.main(args);
		
		//Invio dell'esito UBD
		InvioEsitoUBD.main(args);
		//Lanciare dopo 5 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusUDBAttivazPod.main(args);
		
//		Accesso a R2D
		
		LoginR2D_ELE.main(args);
		
		TimeUnit.SECONDS.sleep(20);
		
		R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_3OK_SWA_ELE.main(args);
		
		R2D_Forzatura_Esiti_3OK_SWA_ELE.main(args); // OK Cla
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_5OK_SWA_ELE.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);

		R2D_VerifichePodFinali_SW_E_DISDETTE_ELE.main(args);
		TimeUnit.SECONDS.sleep(10);

		//Verifiche post esiti R2D, aspettare 2 minuti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);
		
		RecuperaStatusAsset.main(args);
		VerificaMetodoPagamentoRIDAsset.main(args);
		
		//Lanciare dopo 4 ore
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		VerificheAttivazioneRichiesta.main(args);



	}

}



