 package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class SubentroEVO_Id_4 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();

        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("TIPO_DELEGA", "Nessuna delega");
        //prop.setProperty("CODICE_FISCALE", "04922840113");
        prop.setProperty("COMMODITY", "GAS3");
        prop.setProperty("CAP", "37121");
        prop.setProperty("CITTA", "VERONA");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("PROVINCIA_COMUNE", "VERONA");
        prop.setProperty("INDIRIZZO", "VIA ROMA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("RIGA_DA_ESTRARRE", "8");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("MERCATO", "Libero");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
             
        RecuperaPodNonEsistente.main(args);
        SetSubentroquery_subentro_id4.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyCF.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentro.main(args);        
        InserimentoFornitureSubentroSingolaEleGas.main(args);       
        BozzaRiprendiDettaglioSubentro.main(args);
    }

    @After
    public void fineTest() throws Exception {
    	
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
    }
}
