package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.util.Costanti;

import com.nttdata.qa.enel.testqantt.*;

public class SubentroEVO_Id_36 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}
	
	public static Properties conf() {
		Properties prop = new Properties();

		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("CODICE_FISCALE", "SDLSNT24H25H264O");
		prop.setProperty("MODULE_ENABLED", "Y");
		
		prop.setProperty("PROCESSO", "Avvio Subentro EVO");
		
		prop.setProperty("TIPO_DOCUMENTO", "Patente");
		prop.setProperty("NUMERO_DOCUMENTO", "1231");
		prop.setProperty("RILASCIATO_DA", "ABCD");
		prop.setProperty("RILASCIATO_IL", "01/01/2020");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("SEZIONE_ISTAT", "N");
		prop.setProperty("LOCALITA_ISTAT", "ROMA");
		prop.setProperty("CAP", "20121");
		prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "4");
		prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("CLIENTE_BUSINESS", "N");
		
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("CAP_GAS", "37121");
		prop.setProperty("SEZIONE_ISTAT_GAS", "N");
		prop.setProperty("PROVINCIA_COMUNE_GAS", "VERONA");
		prop.setProperty("INDIRIZZO_GAS", "CORSO PORTA NUOVA");
		
		prop.setProperty("RESIDENTE", "SI");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
		prop.setProperty("CONSUMO_ANNUO", "1250");
		
		prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Riscaldamento + uso cottura cibi e/o produzione di acqua calda sanitaria");
		
		
		return prop;
	}
	
	
	@Test
	public void eseguiTest() throws Exception {
		//prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		prop.load(new FileInputStream(nomeScenario));
		/*
		RecuperaPodNonEsistenteDual.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args); // inserimento CF
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		SezioneMercatoSubentro.main(args);
		InserimentoFornitureDualSubentro.main(args);
		SelezioneUsoFornitura.main(args);   */
        CommodityDualResidenzialeSubentroEVO.main(args);    
        IndirizzoDiResidenzaSubentro.main(args);
      
	}

	@After
	public void fineTest() throws Exception {
		/*
		 * prop.load(new FileInputStream(nomeScenario));
		 * this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 * ReportUtility.reportToServer(this.prop);
		 */
	}
}
