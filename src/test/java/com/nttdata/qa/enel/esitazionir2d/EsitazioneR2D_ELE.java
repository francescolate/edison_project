package com.nttdata.qa.enel.esitazionir2d;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class EsitazioneR2D_ELE {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
//		prop.setProperty("USERNAME",Costanti.utenza_salesforce_cla_pe);
//		prop.setProperty("PASSWORD",Costanti.password_salesforce_cla_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("SWITCH_ATTIVO", "Y");
		
		// da valorizzare prima di lanciare lo script - Start
		prop.setProperty("POD", "");
		prop.setProperty("ID_RICHIESTA", "OI-0249059113");
		prop.setProperty("DATA_MINIMA_SW", "01/08/2020");
		// da valorizzare prima di lanciare lo script - Completed

		prop.setProperty("USO","Uso Abitativo");
//		prop.setProperty("COMMODITY", "ELE");
//		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
//		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
//		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
//		prop.setProperty("RILASCIATO_DA", "MCTCNA");
//		prop.setProperty("RILASCIATO_IL", "04/03/2015");
//		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
//		prop.setProperty("USO","Uso Abitativo");
//		prop.setProperty("POTENZA", "3");
//		prop.setProperty("TENSIONE_DISPONIBILE", "220");
//		prop.setProperty("TIPO_MISURATORE", "Non Orario");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO ROMANO");
//		prop.setProperty("TIPOMERCATO", "Libero");
//		prop.setProperty("CAP", "00135");
//		prop.setProperty("REGIONE", "LAZIO");
//		prop.setProperty("PROVINCIA", "ROMA");
//		prop.setProperty("CITTA", "ROMA");
//		prop.setProperty("INDIRIZZO", "VIA NIZZA");
//		prop.setProperty("CIVICO", "4");
//		prop.setProperty("TITOLARITA","Uso/Abitazione");  
//		prop.setProperty("NUOVA_POTENZA","3"); 
//		prop.setProperty("DEALERPHONE","3333131311");  
//		prop.setProperty("RESIDENTE","NO");  
//		prop.setProperty("COMMVALUE","12");
//		prop.setProperty("POTENZAANNUA","3300");  
//		prop.setProperty("VOCALNOVOCAL","No Vocal");
//		prop.setProperty("MODALITAFIRMA","POSTA");
//		prop.setProperty("MODULE_ENABLED","Y");
//		prop.setProperty("TARIFFA","Senza Orari");
//		prop.setProperty("EXPECTEDSTATUS","Chiusa da confermare");
//		prop.setProperty("FILENAME","src/main/resources/C.T.E.pdf");
		//Verifiche post emissione ordine
		prop.setProperty("EXPECTEDSTATUS_OFFERTA_POST_EMISSIONE_SWA", "Chiusa");
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
		//Integrazione R2D ELE
//		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("LINK_R2D_ELE","r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
		//Property per verifiche post emissione
		prop.setProperty("EXPECTEDSTATUS_RICHIESTA_FINALE","Chiuso");
		prop.setProperty("EXPECTED_SOTTO_STATUS_RICHIESTA_FINALE", "Espletato");
		prop.setProperty("NUMERO_CONTRATTO", "1234567");
		prop.setProperty("STATO", "");
		prop.setProperty("STATO_R2D", "");
		prop.setProperty("DATA_RECESSO", "");
		prop.setProperty("INDICE_POD", "1");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream("prop.properties"), null);
		String args[] = {"prop.properties"};

		InputStream in = new FileInputStream("prop.properties");
		prop.load(in);

		LoginR2D_ELE.main(args);
		
		TimeUnit.SECONDS.sleep(5);
		
		R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		
//		R2D_InvioRichiesteSWA_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_SWA_ELE.main(args);
		
		R2D_Forzatura_Esiti_3OK_SWA_ELE.main(args); // OK Cla
//		System.out.println("Forzatura 3OK Stato: "+ prop.getProperty("STATO_R2D"));
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_5OK_SWA_ELE.main(args); //OK Cla
//		System.out.println("Verifica Esito 5OK Stato: "+ prop.getProperty("STATO_R2D"));
		TimeUnit.SECONDS.sleep(5);

		R2D_VerifichePodFinali_SW_E_DISDETTE_ELE.main(args);
//		System.out.println("Verifica Finale Stato: "+ prop.getProperty("STATO_R2D"));
//		TimeUnit.SECONDS.sleep(10);


//FINE R2D **************************************************************************************
//
//		//		TERZO ACCESSO A SALESFORCE PER VERIFICARE ESITI INTEGRAZIONE
//		ManageDriver.closeDriver(this.driver);
//		//Apertura driver
//		this.driver=ManageDriver.startDriver();//startare giorno successivo dopo le 12
//		//				prop.setProperty("NUMERO_RICHIESTA", "116033894");
//		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
//		RicercaVerificaRichiestaPostEsitiSWA.main(this.args);    



	}

}

