package com.nttdata.qa.enel.esitazionir2d;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class EsitazioneR2D_GAS_ROB {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		
		
		//Dato ereditato dalla parte salesforce
		prop.setProperty("POD", "54534648418942");
		prop.setProperty("TIPO_OPERAZIONE", "VARIAZIONE_INDIRIZZO_FORNITURA");
		prop.setProperty("OI_RICHIESTA", "OI-200904957");
		
		//Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS","MEGARETI SPA");
		prop.setProperty("STATO_R2D", "AW");
		
		//Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Variazione Indirizzo Fornitura SII");
		
	
		//Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("CP_GESTORE", "12345");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		


		return prop;
	}

	@After
	public void tearDown() throws Exception{
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream("prop.properties"), null);
		String args[] = {"prop.properties"};
		
		LoginR2D_GAS.main(args);
		TimeUnit.SECONDS.sleep(60);
		R2D_VerifichePodIniziali_GAS.main(args);
        TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);




	}

}

