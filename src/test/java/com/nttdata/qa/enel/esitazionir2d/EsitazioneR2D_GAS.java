package com.nttdata.qa.enel.esitazionir2d;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class EsitazioneR2D_GAS {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
//		prop.setProperty("SWITCH_ATTIVO", "Y");
		
		// da valorizzare prima di lanciare lo script - Start
		prop.setProperty("PDR", "");
		prop.setProperty("ID_RICHIESTA", "OI-0249081405");
		prop.setProperty("DATA_MINIMA_SW", "01/08/2020");
		// da valorizzare prima di lanciare lo script - Completed
		
		prop.setProperty("USO","Uso Abitativo");
//		prop.setProperty("CODICE_FISCALE", "TMTGZM85C04F839L");
//		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
//		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
//		prop.setProperty("RILASCIATO_DA", "MCTCNA");
//		prop.setProperty("RILASCIATO_IL", "04/03/2015");'
//		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
//		prop.setProperty("TIPOMERCATO", "Libero");
//		prop.setProperty("CAP", "37121");
//		prop.setProperty("PROVINCIA", "VERONA");
//		prop.setProperty("CITTA", "VERONA");
//		prop.setProperty("INDIRIZZO", "VIA ROMA");
//		prop.setProperty("CIVICO", "2");
//		prop.setProperty("CATEGORIA_CONSUMO", "Uso Cucina");
//		prop.setProperty("CATEGORIA_MARKETING", "RESIDENZIALE");
//		prop.setProperty("PROFILO_CONSUMO", "COTTURA PRODUZIONE ACQUA CALDA");
//		prop.setProperty("TIPO_INSTALLAZIONE", "ACCESSIBILE");
//		prop.setProperty("ORDINE_GRANDEZZA","USO CUCINA");
//		prop.setProperty("UTILIZZO","CIVILE");
//		prop.setProperty("POTENZIALITA", "5000");
//		prop.setProperty("SOCIETA_DI_VENDITA", "AGESP ENERGIA S.R.L.");
//		prop.setProperty("TELEFONO_DISTRIBUTORE", "3926676198");
//		prop.setProperty("TITOLARITA", "Uso/Abitazione");
//		prop.setProperty("ORDINE_FITTIZIO", "NO");
//		prop.setProperty("UTILIZZO", "CIVILE");
//		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
//		prop.setProperty("TELEFONO_SMS", "3926676198");
//		prop.setProperty("CLASSE_PRELIEVO", "7 GIORNI");
//		prop.setProperty("PRELIEVI_CONSUMI_ANNO", "1030");
//		prop.setProperty("VOCALNOVOCAL","No vocal");
//		prop.setProperty("MODALITAFIRMA","STAMPA LOCALE");
//		prop.setProperty("CANALE_INVIO","STAMPA LOCALE");
//		prop.setProperty("PRODOTTO", "GiustaXte Gas");
//		prop.setProperty("MODULE_ENABLED","Y");
//		prop.setProperty("COMMODITY", "GAS");
//		prop.setProperty("EXPECTEDSTATUS_EMISSIONE_OFFERTA","Chiusa da confermare");
//		prop.setProperty("FILENAME","src/main/resources/C.T.E.pdf");
		//Verifiche post emissione ordine
		prop.setProperty("EXPECTEDSTATUS_OFFERTA_POST_EMISSIONE_SWA", "Chiusa");
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
		//Integrazione R2D ELE
//		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
		prop.setProperty("LINK_R2D_GAS","r2d-coll.awselb.enelint.global/r2d/gas/home.do");
//		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
//		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("USER_R2D",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_GAS","EIN");
		prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
//		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta - Esiti SWA");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		//Property per verifiche post emissione
		prop.setProperty("EXPECTEDSTATUS_RICHIESTA_FINALE","Chiuso");
		prop.setProperty("EXPECTED_SOTTO_STATUS_RICHIESTA_FINALE", "Espletato");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("TIPO_UTENZA","PE");
// variabili aggiunte da Claudio per Esitazione
//		prop.setProperty("STATO", "");
		prop.setProperty("STATO_R2D", "");
		prop.setProperty("DATA_RECESSO", "");
		prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
		prop.setProperty("VERIFICA_AMM", "1");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("INDICE_POD", "1");
		
		return prop;
	}

	@After
	public void tearDown() throws Exception{
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream("prop.properties"), null);
		String args[] = {"prop.properties"};

		InputStream in = new FileInputStream("prop.properties");
		prop.load(in);

//		Impostazione Data Minima Switch
//		prop.setProperty("DATA_MINIMA_SW", "01/05/2020");

		LoginR2D_GAS.main(args);
		
		TimeUnit.SECONDS.sleep(5);
		
		R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS.main(args);  // OK-Cla
		TimeUnit.SECONDS.sleep(5);
		
//		R2D_InvioRichiesteSWA_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_SWA_GAS.main(args);
		
		R2D_Forzatura_Esiti_3OK_SWA_GAS.main(args); // OK Cla
		TimeUnit.SECONDS.sleep(5);
//		
		R2D_CaricamentoEsiti_4OK_SWA_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
//		
		R2D_CaricamentoEsiti_5OK_SWA_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
//
		R2D_VerifichePodFinali_SW_E_DISDETTE_GAS.main(args);
		TimeUnit.SECONDS.sleep(3);


//FINE R2D **************************************************************************************
//
//		//		ACCESSO A SALESFORCE PER VERIFICARE ESITI INTEGRAZIONE
//		ManageDriver.closeDriver(this.driver);
//		//Apertura driver
//		this.driver=ManageDriver.startDriver();//startare giorno successivo dopo le 12
//		//				prop.setProperty("NUMERO_RICHIESTA", "116033894");
//		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
//		RicercaVerificaRichiestaPostEsitiSWA.main(this.args);    
//		UpdateDatiWorkbench.main(args);



	}

}


