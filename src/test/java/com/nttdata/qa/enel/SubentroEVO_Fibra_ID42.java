package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.components.lightning.SezioneFibraComponent;
import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;



public class SubentroEVO_Fibra_ID42 {


	Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }
    
    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_penp_pag);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_penp_pag);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "PENP");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("MODULE_ENABLED", "Y");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
        
        prop.setProperty("TIPO_DOCUMENTO", "Patente");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("CAP", "00178");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        
        prop.setProperty("TIPO_MISURATORE", "Non Orario");       
        prop.setProperty("TENSIONE_CONSEGNA", "380");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        
        prop.setProperty("USO", "Uso Abitativo");
        prop.setProperty("RESIDENTE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");   
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("ORDINE_FITTIZIO", "NO");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ABITAZIONI PRIVATE");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
        
        prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
        
        prop.setProperty("PRODOTTO", "SCEGLI OGGI WEB LUCE");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
	    prop.setProperty("OPZIONE_FIBRA", "Y");
        
        prop.setProperty("CANALE_INVIO_FIRMA", "STAMPA LOCALE");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        
        prop.setProperty("INSERISCI_FIBRA", "OK");
        prop.setProperty("PROVINCIA_FIBRA", "ROMA");
        prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
        prop.setProperty("CIVICO_FIBRA", "196");
        prop.setProperty("CELLULARE_FIBRA", "3929926614");
        prop.setProperty("BANNER_FIBRA", "Indirizzo coperto dalla fibra");
        prop.setProperty("RINUNCIA_FIBRA", "Y");

        return prop;
        
    }
    
    @Test
    public void eseguiTest() throws Exception {
        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
        
        RecuperaPodNonEsistente.main(args);        
        LoginSalesForcePE.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args); //inserimento CF
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID15.main(args);
        SezioneMercatoSubentro.main(args);  
        InserimentoFornitureSubentroSingolaEleGas.main(args);      
        SelezioneUsoFornitura.main(args);
        CommodityEleNonResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);  
        ConfiguraProdottoElettricoResidenziale.main(args);  
        ConsensiEContattiSubentro.main(args);
        ModalitaFirmaSubentro.main(args);
        CompilaFibraSubentro.main(args);
        ModalitaFirmaSubentro.main(args); //modalita firma va eseguita una seconda volta dopo rinuncia a fibra
        GestioneCVPAllaccioAttivazione.main(args);
        ConfermaOffertaAllaccioAttivazione.main(args);
        RicercaOfferta.main(args);
        
      
    }
    
    @After
    public void fineTest() throws Exception {
    	/*
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);*/
    }
}


