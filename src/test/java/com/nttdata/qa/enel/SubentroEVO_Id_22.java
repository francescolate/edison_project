package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class SubentroEVO_Id_22 {

	final String nomeScenario = this.getClass().getSimpleName() + ".properties";
	Properties prop;

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("LOCALITA_ISTAT", "ROMA");
		prop.setProperty("CAP", "00178");
		prop.setProperty("PROCESSO", "Avvio Subentro EVO");
		prop.setProperty("NUMERO_DOCUMENTO", "1231");
		prop.setProperty("RILASCIATO_DA", "ABCD");
		prop.setProperty("RILASCIATO_IL", "01/01/2020");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "4");
		prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("SEZIONE_ISTAT", "Y");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("CODICE_FISCALE", "CLNBCK59B25H264M");
		prop.setProperty("KO_BL", "Y");
		return prop;
	}

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	@Test
	public void eseguiTest() throws Exception {

		String args[] = { nomeScenario };
		//prop.store(new FileOutputStream(nomeScenario), null);
		prop.load(new FileInputStream(nomeScenario));
/*
		RecuperaPodNonEsistente.main(args);*/
		LoginSalesForce.main(args);/*
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID17.main(args);
		SezioneMercatoSubentro.main(args);*/
		//System.out.println("Fine test");
	}

	@After
	public void fineTest() throws Exception {
		 /*
		  InputStream in = new FileInputStream(nomeScenario); prop.load(in);
		  this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		  ReportUtility.reportToServer(this.prop);
		 */
	}
}
