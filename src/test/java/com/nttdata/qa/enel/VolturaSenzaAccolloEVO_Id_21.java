package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura2;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVolturaErroreSAP;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaIlluminazionePubblicaErroreBSN;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura3_Finalizzazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.NuovoMetodoDiPagamento;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaCF_POD_per_Volture_2;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_21 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);
		prop.setProperty("CODICE_FISCALE", "52490720977");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "BSNTTN88T10F839T");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Diverso da Abitazione");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");
		prop.setProperty("TIPOCLIENTE","BUSINESS");
		
		prop.setProperty("FLAG_RESIDENTE","NO");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		
		
		prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_Volture2);
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("RUN_LOCALLY","Y");
		
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		SetQueryRecuperaCF_POD_per_Volture_2.main(args);
		RecuperaDatiWorkbench.main(args);
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("NOME", prop.getProperty("QUERY_ACCOUNT.NAME"));
//		prop.setProperty("CODICE_FISCALE_CL_USCENTE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
//		prop.store(new FileOutputStream(nomeScenario), null);
		
		SetPropertyVSA_Data_Query.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID14.main(args);
		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);
		CheckSezioneSelezioneFornitura.main(args); 
		// aggiungere controllo se esiste referente! 
		ConfermaRiepilogoOfferta.main(args);
		SelezioneUsoFornitura.main(args);
		ReferenteSwaEVO.main(args); 
		CompilaDatiFornituraVolturaIlluminazionePubblicaErroreBSN.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
}
