package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.github.javafaker.Faker;
import com.nttdata.qa.enel.testqantt.AnnulloVoltura;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura2;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziVolture;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura3_Finalizzazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.NuovoMetodoDiPagamento;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_CF_Uscente_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetProperty_VSA_R2D_Parameters;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaCF_POD_per_Volture;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheOrdinePostQuery;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_CambioUso_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_3 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("LINK_UDB","http://dtcmmind-bw-01.risorse.enel:8887");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("CODICE_FISCALE", "CRSFNC89M49H769B");
		prop.setProperty("NOME", "FRANCESCA CORSI");
		
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");
		//prop.setProperty("IBAN", "IT57D0760112700000012285433");
		prop.setProperty("FLAG_RESIDENTE","SI");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("CAP", "00198");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("PRODOTTO","Valore Luce Plus");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Documento di riconoscimento");
	//	prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_Volture);
		prop.setProperty("RIGA_DA_ESTRARRE", "4");
		prop.setProperty("ESITO_UDB","OK");
		
			
		prop.setProperty("RUN_LOCALLY","Y");
		
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{
		prop.store(new FileOutputStream(nomeScenario), null);	
		String args[] = {nomeScenario};		
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
				
			SetQueryRecuperaCF_POD_per_Volture.main(args);
		RecuperaDatiWorkbench.main(args);
		//in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("NOME", prop.getProperty("QUERY_ACCOUNT.NAME"));
//		prop.setProperty("CODICE_FISCALE_CL_USCENTE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
//		prop.setProperty("RIGA_DA_ESTRARRE", "5");
//		prop.store(new FileOutputStream(nomeScenario), null);
//		
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
			
		SetPropertyVSA_Data_Query.main(args);
			LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args); 
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID17.main(args);
		SelezioneMercatoVolturaSenzaAccollo.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);
		
		CheckSezioneSelezioneFornitura.main(args);
		RiepilogoOffertaVoltura.main(args);
		AzioniRiepilogoOffertaVoltura.main(args);
		
		NuovoMetodoDiPagamento.main(args); 
		ConfermaIndirizziVolture.main(args);
		AzioniRiepilogoOffertaVoltura2.main(args);
		
		AzioniSezioneCarrello.main(args); 
	
		CreaOffertaVoltura3_Finalizzazione.main(args);
		CheckRiepilogoOfferta.main(args); 
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOfferta.main(args);
		CaricaEValidaDocumenti.main(args);
		VerificaDocumento.main(args);
	
		VerificheStatoRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
		
		RecuperaOrderIDDaPod.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusUDBCreazioneOrdine.main(args);
		InvioEsitoUBD.main(args);
			
		// Dati R2d
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
		//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));

		SetOIRicercaDaOIOrdine.main(args);
		SetProperty_VSA_R2D_Parameters.main(args);
					
		//prop.store(new FileOutputStream(nomeScenario), null);
		LoginR2D_ELE.main(args); 
		//TimeUnit.SECONDS.sleep(5); 
		R2D_VerifichePodIniziali_ELE.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args); 
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		

		//Da eseguire dopo qualche ora
		
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);
//		prop.store(new FileOutputStream(nomeScenario), null);
		
		
	
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args); 

	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
