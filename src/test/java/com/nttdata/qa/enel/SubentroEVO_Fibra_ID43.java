package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AnnullaOffertaGenerico;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.CompilaFibraSubentro;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiSubentro;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureSubentroSingolaEleGas;
import com.nttdata.qa.enel.testqantt.LoginSalesForcePE;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaSubentro;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostale;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SezioneMercatoSubentro;
import com.nttdata.qa.enel.util.Costanti;


public class SubentroEVO_Fibra_ID43 {

	Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }
    
    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "PE");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("MODULE_ENABLED", "Y");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
        
        prop.setProperty("TIPO_DOCUMENTO", "Patente");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("CAP", "00178");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        
        prop.setProperty("TIPO_MISURATORE", "Non Orario");       
        prop.setProperty("TENSIONE_CONSEGNA", "380");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        
        prop.setProperty("USO", "Uso Abitativo");
        prop.setProperty("RESIDENTE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");   
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("ORDINE_FITTIZIO", "NO");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ABITAZIONI PRIVATE");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
        
        prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
        
        prop.setProperty("PRODOTTO", "SCEGLI OGGI WEB LUCE");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
	    prop.setProperty("OPZIONE_FIBRA", "Y");
        
        prop.setProperty("CANALE_INVIO_FIRMA", "POSTA");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        
        prop.setProperty("INSERISCI_FIBRA", "OK");
        prop.setProperty("PROVINCIA_FIBRA", "ROMA");
        prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
        prop.setProperty("CIVICO_FIBRA", "196");
        prop.setProperty("CELLULARE_FIBRA", "3929926614");
        prop.setProperty("BANNER_FIBRA", "Indirizzo coperto dalla fibra");
        prop.setProperty("VERIFICA_ERRORE_SE_SEP", "Y");

        return prop;
        
    }
    
    @Test
    public void eseguiTest() throws Exception {
        //prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
         
        //RecuperaPodNonEsistente.main(args);        
        LoginSalesForcePE.main(args);/*
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args); //inserimento CF
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID15.main(args);
        SezioneMercatoSubentro.main(args);  
        InserimentoFornitureSubentroSingolaEleGas.main(args);       
        SelezioneUsoFornitura.main(args);
        CommodityEleResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);  
        ConfiguraProdottoElettricoResidenziale.main(args);  
        ConsensiEContattiSubentro.main(args);
        ModalitaFirmaSubentro.main(args);
        AnnullaOffertaGenerico.main(args);*/
    }
    
    @After
    public void fineTest() throws Exception {
    	/*
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);*/
    }
}
