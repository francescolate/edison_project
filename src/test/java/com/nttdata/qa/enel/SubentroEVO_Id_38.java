package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


public class SubentroEVO_Id_38 {

	Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }
    
    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("MODULE_ENABLED", "Y");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        
        prop.setProperty("TIPO_DOCUMENTO", "Patente");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("CAP", "00178");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");       
        prop.setProperty("TENSIONE_CONSEGNA", "380");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("USO", "Uso Abitativo");
        
        prop.setProperty("RESIDENTE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");   
        prop.setProperty("ASCENSORE", "SI");
        prop.setProperty("ORDINE_FITTIZIO", "NO");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ABITAZIONI PRIVATE");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
        prop.setProperty("TIPOLOGIA_ASCENSORE", "Oleodinamico");
        prop.setProperty("CORRENTE_DI_REGIME", "123");
        
        prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
        prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
        
        prop.setProperty("PRODOTTO", "SCEGLI OGGI WEB LUCE");
        prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
        prop.setProperty("TAB_SGEGLI_TU", "SI" );
        prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
        
        prop.setProperty("CANALE_INVIO_FIRMA", "EMAIL");
        prop.setProperty("MODALITA_FIRMA", "DIGITAL");
        prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
        
        prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
        prop.setProperty("NUMERO_CONTRATTO", "");
        

        return prop;
        
    }
    
    @Test
    public void eseguiTest() throws Exception {
        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
        
        RecuperaPodNonEsistente.main(args);        
        LoginSalesForcePE.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args); //inserimento CF*
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID15.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);     
        
        SelezioneUsoFornitura.main(args);
        CommodityEleResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
        PagamentoBollettinoPostale.main(args);         
        ConfermaScontiBonusEVO.main(args);  
        ConfiguraProdottoElettricoResidenziale.main(args);  
        GestioneCVPAllaccioAttivazione.main(args);
        ConsensiEContattiSubentro.main(args);
        ModalitaFirmaSubentro.main(args);          
        ConfermaOffertaAllaccioAttivazione.main(args);    
        
        /*- inizio TP2 -  */
		RecuperaNumeroContratto.main(args);
		SetUtenzaMailResidenziale.main(args);
		RecuperaLinkTP2Subentro.main(args);
		LoginPortalTP2.main(args);
		ConfermaTP2SubentroNew.main(args);
		RecuperaRichiestaTouchPoint.main(args);
		VerificaChiusuraTouchPoint.main(args);
		    
        LoginSalesForcePE.main(args);
        RicercaOffertaSubentro.main(args); 
	    CaricaEValidaDocumenti.main(args);
            
        SetSubentroProperty.main(args);
        LoginSalesForcePE.main(args);
        RicercaRichiesta.main(args);
        RecuperaOrderIDDaPod.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);

        // Dati R2d
        SetSubentro7R2D.main(args);
        LoginR2D_ELE.main(args);
        TimeUnit.SECONDS.sleep(5);
        R2D_VerifichePodIniziali_ELE.main(args);
        TimeUnit.SECONDS.sleep(5);
        R2D_InvioPSPortale_1OK_ELE.main(args);
        R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
        R2D_CaricamentoEsiti_5OK_ELE.main(args);
        R2D_VerifichePodFinali_ELE.main(args);

        //Da eseguire dopo qualche ora       
        SetSubentroBatch.main(args);
        LoginSalesForcePE.main(args);
        SbloccaTab.main(args);
        RicercaRichiesta.main(args);
        VerificheRichiestaDaPod.main(args);
       
       
       
    }
    
    @After
    public void fineTest() throws Exception {
    	
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
    }
}
