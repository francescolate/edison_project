package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaIlluminazionePubblicaErroreRES;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaSenzaAccolloRes;
import com.nttdata.qa.enel.testqantt.CompilaIndirizzoFibra;
import com.nttdata.qa.enel.testqantt.CompilaSezioneIndirizziVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.ContattiEConsensiVolturaSenzaAccolloRes;
import com.nttdata.qa.enel.testqantt.GetNumeroCellulareOTP;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.MetodoDiPagamentoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.ModalitàFirmaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Voltura;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaOtp;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaWebPrivatoVoltura;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaNumeroContratto;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyFor_VSA;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Globale_Richiesta_InLavorazione;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID20;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID8;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SezioneCVPVoltura;
import com.nttdata.qa.enel.testqantt.SezioneFatturazioneElettronicaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.VerificaAlertRosso;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaMessaggioAlertBlocco;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

/**
S2S
Cliente: Residenziale

Descrizione: Accedere alla piattaforma SFDC ed avviare un processo di Voltura Senza Accollo 
EVO da canale S2S su cliente di tipo Residenziale con scelta fornitura ELE.
Verificare il comportamento della sezione modalità firma selezionando 
come Modalità "NO VOCAL" in presenza di un porodotto fibra selezioanto nel carrello
Risultato Atteso: Modalità non cosentita

 */
public class VolturaSenzaAccolloEVO_Id_39 {


	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("QUERY", SetPropertyFor_VSA.recupera_CF_POD_per_Volture_id39);
		
		
		
		//AVVIO PROCESSO
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		
		//CLIENTE ENTRANTE
		prop.setProperty("CODICE_FISCALE", ""); //Lasciare stringa vuota se si vuole che il dato venga estratto con query
		
		//CLIENTE USCENTE e SELEZIONE FORNITURA		
		prop.setProperty("CODICE_FISCALE_CL_USCENTE", "");	//Lasciare stringa vuota se si vuole che il dato venga estratto con query
		prop.setProperty("POD", "");
	    

		
		//IDENTIFICAZIONE INTERLOCUTORE
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		//SELEZIONE MERCATO
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("MERCATO", "Libero");

		
		
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE"); 
		prop.setProperty("CONSUMO","3");
		prop.setProperty("FLAG_RESIDENTE","SI");
		
		//METODO DI PAGAMENTO
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");
		
		
		

		//INDIRIZZO 
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("CAP", "27036");
		
		
		//CONFIGURAZIONE PRODOTTO ELETTRICO
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("ELIMINA_VAS", "SI");
		
		//CONFIGURAZIONE FIBRA
		prop.setProperty("OPZIONE_FIBRA","SI");
		prop.setProperty("INDIRIZZO_FIBRA_OK","SI");
		prop.setProperty("TELEFONO_FIBRA", "06221234567");
		
		
		prop.setProperty("REGIONE_FIBRA", "LAZIO");
		prop.setProperty("PROVINCIA_FIBRA", "ROMA");
		prop.setProperty("CODICE_MIGRAZIONE", "XXXXXX");
		prop.setProperty("CITTA_FIBRA", "ROMA");
		prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
		prop.setProperty("CIVICO_FIBRA", "196");
		prop.setProperty("COMPILA_EMAIL_FIBRA", "Y");
		prop.setProperty("EMAIL_FIBRA", "cli.res111@gmail.com");
		prop.setProperty("SCELTA_MELITA","NO");
		prop.setProperty("CONSENSO_DATI_FIBRA_OK","SI");
		//TODO: selezionare cellulare
		
		
		
		
		//MODALITA FIRMA
		prop.setProperty("MODALITA_FIRMA","NO VOCAL");
		prop.setProperty("CANALE","EMAIL");
		prop.setProperty("EMAIL", "cli.res111@gmail.com");
		prop.setProperty("VERIFICA_SEZIONI_CONFERMATE","N");
		prop.setProperty("CELLULARE", "3467656345");
		
		
		
		
		prop.setProperty("DISALIMENTABILITA","SI");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		
		prop.setProperty("CHECK_RED_ALERT_MESSAGE","Y");
		prop.setProperty("RED_ALERT_MESSAGE","La modalità di firma NO VOCAL è compatibile con il prodotto Fibra solo per canali SE e SEP e canale invio contratto STAMPA LOCALE");
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("RIGA_DA_ESTRARRE", "4");
		
		return prop;
	};
	
	

	@Test
    public void eseguiTest() throws Exception{
	    prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		

	
		SetPropertyFor_VSA.main(args);		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID17.main(args);
		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);	
		CheckSezioneSelezioneFornitura.main(args);
		RiepilogoOffertaVoltura.main(args);
		ConfermaRiepilogoOfferta.main(args);
		SelezioneUsoFornitura.main(args);
		CompilaDatiFornituraVolturaSenzaAccolloRes.main(args);
		MetodoDiPagamentoOffertaVoltura.main(args);
		ConfermaScontiBonusEVO.main(args);	
		System.out.println("Fatturazione Elettronica");
		SezioneFatturazioneElettronicaVolturaSenzaAccollo.main(args);
		CompilaSezioneIndirizziVolturaSenzaAccollo.main(args);//Indirizzo BUC: gestito nel modulo CompilaSezioneIndirizziVolturaSenzaAccollo
		ConfiguraProdottoElettricoResidenziale.main(args);
		SezioneCVPVoltura.main(args);
		ContattiEConsensiVolturaSenzaAccolloRes.main(args);
		//SezioneFatturazioneElettronicaVolturaSenzaAccollo.main(args);
		CompilaIndirizzoFibra.main(args); 
		ModalitàFirmaVolturaSenzaAccollo.main(args);	
		VerificaAlertRosso.main(args);

		
	
		
		

	
	};


	
	
	@After
    public void fineTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
	
}
