package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SubentroEVO_Id_9 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("RIGA_DA_ESTRARRE", "1");
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "00178");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
     
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("LIST_TEXT", "Y");
        prop.setProperty("CHECK_LIST_TEXT", "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- Che il contatore sia cessato ed in passato abbia erogato Luce/gas- Che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:POD/PDR - Matr/EneltelINFORMAZIONI UTILI- In caso di Dual ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO- In caso di Multi ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO – PRODOTTO – STESSA COMMODITY- In caso di Cliente Pubblica Amministrazione sarà necessario il Codice Ufficio- In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)- In caso di Duale/Multi ricordati che non è possibile eseguire la modifica potenza e/o tensione- Se il cliente attiva metodo di pagamento SDD e Bolletta Web riceverà uno sconto\\bonus in fatturaFibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento del subentro. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta  In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente:-   numero di cellulare -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associata  SERVIZIO DI PRENOTAZIONEIl Servizio di Prenotazione consente al Cliente di concludere il contratto di fornitura oggi, scegliendo una data desiderata di attivazione del servizio. Il Cliente potrà modificare la data di attivazione successivamente, attraverso un link che riceverà nella propria casella email.ATTENZIONE: è necessario che il Cliente restituisca tutta la documentazione necessaria richiesta,  prima della data indicata, altrimenti non sarà possibile procedere con l’operazione.Quando è possibile richiedere il Servizio di Prenotazione:al momento il servizio è disponibile solo per operazioni di :-          riattivazione del contatore elettrico su fornitura singola (subentro single ele)-          operazioni a parità di condizioni tecniche  (senza modifica potenza e/o tensione)-          rete application to application  ( E- Distribuzione )-          comprese tra 20gg dalla data odierna e non oltre i 90gg (giorni solari) Cosa serve:E’ necessario fornire un:-          indirizzo EMAIL-          numero di telefono mobileSe i dati sono già presenti a sistema, chiedere conferma al cliente prima di procedere.  SCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"");
        prop.setProperty("CLIENTE_BUSINESS", "Y");
        prop.setProperty("CANALE_INVIO_FIRMA", "STAMPA LOCALE");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("CODICE_UFFICIO_ERRATO", "00000");
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
        prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - INVIATA");
        prop.setProperty("STATO_RICHIESTA", "INVIATA");
        prop.setProperty("STATO_R2D", "N.D.");
        prop.setProperty("STATO_SAP", "N.D.");
        prop.setProperty("STATO_SEMPRE", "N.D.");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ALTRI SERVIZI");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3464536453");
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
        prop.setProperty("RUN_LOCALLY", "Y");
        
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ALTRI SERVIZI");
        prop.setProperty("DISALIMENTABILITA", "SI");   
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("ORDINE_FITTIZIO", "NO");
        prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("TITOLARITA", "");
        prop.setProperty("SELEZIONA_CANALE_INVIO", "Y");
        prop.setProperty("COMMODITY", "ELE");
        
        prop.setProperty("SPLIT_PAYMENT", "No");
        
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO_FIRMA", "STAMPA LOCALE");
        return prop;
    }


    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        RecuperaPodNonEsistente.main(args);        
        SetSubentroquery_subentro_id8.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyCF.main(args);     
        LoginSalesForce.main(args);           
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);     
        Identificazione_Interlocutore_ID16.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);            
		SelezioneReferente.main(args);
		SelezioneUsoFornitura.main(args);
		CommodityEleNonResidenzialeSubentro.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostale.main(args);
		ConfermaScontiBonusEVO.main(args);
		SplitPaymentEVO.main(args);
		CigCupEVO.main(args);
		ConfiguraProdottoElettricoNonResidenziale.main(args);
		GestioneCVPAllaccioAttivazione.main(args);
		ConsensiEContattiSubentro.main(args);           
		ModalitaFirmaSubentro.main(args);
		ConfermaOffertaAllaccioAttivazione.main(args);
		
	    LoginSalesForce.main(args);
		RicercaOffertaSubentro.main(args);          
		CaricaEValidaDocumenti.main(args);
        SetSubentroProperty.main(args);
        LoginSalesForce.main(args);
        RicercaOffertaSubentro.main(args);
        RecuperaOrderIDDaPod.main(args);
      
        //Check status offerta
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);

        //Dati R2D
        SetSubentro7R2D.main(args);
        LoginR2D_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodIniziali_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_InvioPSPortale_1OK_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_CaricamentoEsiti_5OK_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodFinali_ELE.main(args);

        //Da eseguire dopo qualche ora
		SetSubentroBatch.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaRichiesta.main(args);
        VerificheRichiestaDaPod.main(args);

    }


    @After
    public void fineTest() throws Exception {
    	
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
    }
}
