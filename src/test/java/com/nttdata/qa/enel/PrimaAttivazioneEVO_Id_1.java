package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_1 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "04922840113");
			prop.setProperty("CODICE_FISCALE_REFERENTE", "GLDGWJ85C04F839A");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "S2S");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("MERCATO", "Salvaguardia");
			prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "Y");
			prop.setProperty("TIPOLOGIA_PRODUTTORE", "SERVIZI AUSILIARI DI CENTRALE");
			prop.setProperty("RUN_LOCALLY","Y");

		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};
			
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
            
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);			
			ProcessoPrimaAttivazioneNonResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoPrimaAttivazioneEVO.main(args);
		};
		
		@After
	    public void fineTest() throws Exception{
            String args[] = {nomeScenario};
            InputStream in = new FileInputStream(nomeScenario);
            prop.load(in);
            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
            ReportUtility.reportToServer(this.prop);
      };
}
