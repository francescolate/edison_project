package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class SubentroEVO_Id_2 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("TIPO_DELEGA", "Nessuna delega");
        prop.setProperty("RIGA_DA_ESTRARRE", "2");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("RIGA_DA_ESTRARRE", "1");
        prop.setProperty("CAP", "00178");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("ESITO_OFFERTABILITA", "KO");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("MERCATO", "Libero");

        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        
        SetSubentroquery_subentro_id2.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyPODCF.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID17.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);
        System.out.println("fine test");
    }

    @After
    public void fineTest() throws Exception {
    	/*
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);*/
    }
}
