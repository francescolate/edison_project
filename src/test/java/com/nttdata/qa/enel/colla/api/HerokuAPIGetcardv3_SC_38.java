// HerokuAPIGetcardv3_SC_38
// Solo per APP  - TOken in input expire 31/12/2021
/*"userUPN": "fef896fe-7b6d-4028-8664-a93fb8e15b15",
"cf": "DRADRA60D24A944Z",
"enelId": "bd33b186-fae7-4519-a430-14f42c9cfa00",
"userEmail": "r32019p109statocontiwe.b@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar DRADRA60D24A944Z r32019p109statocontiwe.b@gmail.com fef896fe-7b6d-4028-8664-a93fb8e15b15 bd33b186-fae7-4519-a430-14f42c9cfa00
*/
// KO - No Channel 

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_2;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetcardv3_SC_38 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CHANNELKEY","");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CF","DRADRA60D24A944Z");
		prop.setProperty("USERNAME","r32019p109statocontiwe.b@gmail.com");
		prop.setProperty("USERUPN","fef896fe-7b6d-4028-8664-a93fb8e15b15");
		prop.setProperty("ENELID","bd33b186-fae7-4519-a430-14f42c9cfa00");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \"" + prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		prop.setProperty("JSON_DATA_OUTPUT", "{\"result\":\"AUT02\",\"code\":\"-1\",\"description\":\"Client non autorizzato\"}");
		prop.setProperty("SET_AUTH", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - No Channel 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
