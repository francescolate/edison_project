package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.CaratteristicheDettaglioGiustaPerTeImpresaGas;
import com.nttdata.qa.enel.testqantt.colla.DocumentiGiustaPerTeImpresaGas;
import com.nttdata.qa.enel.testqantt.colla.GiustaPerTeImpresaGas;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.Sub_Bsn_Gas;
import com.nttdata.qa.enel.util.ReportUtility;



public class WEB_Pubblico_Motorino_ricerca_2 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	//	prop.setProperty("LINK", "https://www-colla.enel.it/");
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
	   // prop.setProperty("LINK_IMPRESE", "https://www-colla.enel.it/it/imprese");
	   // prop.setProperty("LINK_LUCE_E_GAS", "https://www-colla.enel.it/it/luce-e-gas");
	    prop.setProperty("TYPE_OF_CONTRACT", "Gas");
		prop.setProperty("PLACE", "Negozio - Ufficio");
		prop.setProperty("NEED", "SUBENTRO");
		//prop.setProperty("LINK_LUCE_E_GAS_CON_FILTRO","https://www-colla.enel.it/it/luce-e-gas/gas/negozio-ufficio?nec=Subentro");
		//prop.setProperty("LINK_GIUSTA_PER_TE", "https://www-colla.enel.it/it/luce-e-gas/gas/offerte/giustaperte-impresa-gas");
		//prop.setProperty("LINK_PLUS_IMAGE", "https://www-colla.enel.it/etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/plus.png");
		//prop.setProperty("LINK_MINUS_IMAGE", "https://www-colla.enel.it/etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/minus.png");
		//prop.setProperty("LINK_DOWNLOAD1", "https://www-colla.enel.it/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-offerta/giustaxte/gas/GiustaPerTeGasImpresa_cte.pdf");
		//prop.setProperty("LINK_DOWNLOAD2", "https://www-colla.enel.it/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-offerta/giustaxte/gas/Modulo_adesione_business.PDF");
		//prop.setProperty("LINK_DOWNLOAD3", "https://www-colla.enel.it/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-offerta/giustaxte/gas/giusta-x-te-gas-impresa-cte.pdf");
		//prop.setProperty("LINK_DOWNLOAD4", "https://www-colla.enel.it/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-offerta/giustaxte/gas/condizioni_generali_fornitura_bus");   
		
		
		
	    prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	    
	};
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		
		Login_Public_Area.main(args);
		Sub_Bsn_Gas.main(args);
		GiustaPerTeImpresaGas.main(args);
//		CaratteristicheDettaglioGiustaPerTeImpresaGas.main(args);
//		DocumentiGiustaPerTeImpresaGas.main(args);
	};
	
	@After
	  public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
	
}
