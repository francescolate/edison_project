package com.nttdata.qa.enel.colla;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_108;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_PaginaBollette_DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_108 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "marco.campanale@enelsaa3001.enel.com");
		//prop.setProperty("WP_USERNAME", "testimpresa@yopmail.com");
		//prop.setProperty("WP_PASSWORD", "Eneltest2017");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");
	    prop.setProperty("PDF_FILE", "Fattura_0000004000008281.pdf");
	  //  prop.setProperty("DIR_PATH", System.getProperty("user.home")+File.separator+"Downloads");
		
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.store(new FileOutputStream(nomeScenario), null);		
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//Login_Private_Area.main(args);
		DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_108.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
