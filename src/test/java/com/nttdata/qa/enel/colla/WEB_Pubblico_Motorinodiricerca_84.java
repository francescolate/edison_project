package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.FooterPageEng;
import com.nttdata.qa.enel.testqantt.colla.HeaderLoginEng;
import com.nttdata.qa.enel.testqantt.colla.HeaderLuceGasEng;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area_ENG;
import com.nttdata.qa.enel.testqantt.colla.MotorinoDiRicerca;
import com.nttdata.qa.enel.testqantt.colla.OreFreeID35;
import com.nttdata.qa.enel.testqantt.colla.PowerAndGasInfo;
import com.nttdata.qa.enel.testqantt.colla.PublicAreaBusinessEngBodyChecker;
import com.nttdata.qa.enel.testqantt.colla.PublicAreaEngBodyChecker;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Costanti;

public class WEB_Pubblico_Motorinodiricerca_84 {
	
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
//		prop.setProperty("LINK1", "https://www-coll2.enel.it/en/luce-e-gas/luce-e-gas/negozio-ufficio?nec=non_specificato&sortType=in-promozione");
//		prop.setProperty("LINK", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll2.enel.it/en/luce-e-gas/luce-e-gas/negozio-ufficio?nec=non_specificato&sortType=in-promozione");
		prop.setProperty("LINK_POWER", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/en/luce-e-gas/luce/negozio-ufficio");
		prop.setProperty("LINK_GAS", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/en/luce-e-gas/gas/negozio-ufficio");
		prop.setProperty("LINK_CONTACT_CONSULTANT", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/en/form-ricontatto-commerciale");
		prop.setProperty("LINK_BACK", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/en/luce-e-gas/luce-e-gas/negozio-ufficio?nec=non_specificato&sortType=in-promozione");
		prop.setProperty("LINK_SOLUTION", "https://www-coll1.enel.it/en/imprese?zoneid=search-quick_link");
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/en/luce-e-gas/luce-e-gas/negozio-ufficio?nec=non_specificato&sortType=in-promozione");
//		prop.setProperty("LINK_POWER", "https://www-coll1.enel.it/en/luce-e-gas/luce/negozio-ufficio");
//		prop.setProperty("LINK_GAS", "https://www-coll1.enel.it/en/luce-e-gas/gas/negozio-ufficio");
//		prop.setProperty("LINK_CONTACT_CONSULTANT", "https://www-coll1.enel.it/en/form-ricontatto-commerciale");
//		prop.setProperty("LINK_BACK", "https://www-coll1.enel.it/en/luce-e-gas/luce-e-gas/negozio-ufficio?nec=non_specificato&sortType=in-promozione");
//		prop.setProperty("LINK_SOLUTION", "https://www-coll1.enel.it/en/imprese?zoneid=search-quick_link");

		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};

		Login_Public_Area_ENG.main(args);
		PublicAreaBusinessEngBodyChecker.main(args);
		HeaderLoginEng.main(args);	
		FooterPageEng.main(args);
		PowerAndGasInfo.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};

}
