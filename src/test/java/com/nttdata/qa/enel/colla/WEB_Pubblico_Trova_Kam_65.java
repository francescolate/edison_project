package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.Publico_Trova_Kam_65;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Trova_Kam_65 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
//		prop.setProperty("USERNAME", "r32019p109statocontiwe.b@gmail.com");
//		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("URL", "https://www-coll1.enel.it/it/imprese");
		prop.setProperty("URL2", "https://www-coll1.enel.it/it/imprese/grandi-aziende?zoneid=imprese-tile_small");
		prop.setProperty("ERROR_NOINPUT", "Campo Obbligatorio");	
		prop.setProperty("INPUT", "33333");
		prop.setProperty("INPUTVALID", "00163)");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Publico_Trova_Kam_65.main(args);
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
