/* HerokuAPICoperturaComune_SC_183 
** Case OK - WEB - NO TOKEN IN HEADER 
* need token (cliente qualsiasi)
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaComuneIndirizzo_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaComuneIndirizzo_2;
import com.nttdata.qa.enel.util.ReportUtility;
// import com.nttdata.jwt.gen.*;


public class HerokuAPICoperturaComune_SC_183 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
        prop.setProperty("Authorization", "");
        
		prop.setProperty("SEARCH","rom");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");		
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/order-fibra/v1/coperturacomune?name="+ prop.getProperty("SEARCH"));
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"GEN10\",\"code\":\"-4\",\"description\":\"Sessione non valida, si prega di effettuare nuovamente l'accesso\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Case OK - WEB - NO TOKEN IN HEADER
     * Need Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaComuneIndirizzo_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
