package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaFibraMelita;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaFibraMelitaInformativa;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMarketingConsensus;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_FIBRA_ProcessoAcquistoFibra_Fibra_in_Espletato_115 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    //prop.setProperty("WP_LINK", "https://www-colla.enel.it/");
	    prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	   // prop.setProperty("WP_USERNAME", "raffae.llaquom.o@gmail.com");
	   prop.setProperty("WP_USERNAME", "gabriele.colazzo@enelsaa0118.enel.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    //raffae.llaquom.o@gmail.com
	    prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		
	 //   prop.setProperty("ACCOUNT_TYPE", "RES");
	    prop.setProperty("CONSENSUS_EMAIL", "TRUE");
	    prop.setProperty("CONSENSUS_MOBILE", "TRUE");
	    
//	    prop.setProperty("QUERY","SELECT Account.NE__Fiscal_code__c, count(id)\r\n" + 
//	    		"FROM Asset\r\n" + 
//	    		"WHERE Account.NE__Fiscal_code__c !=''\r\n" + 
//	    		" and Account.RecordType.Name ='Residenziale'\r\n" + 
//	    		" and NE__Status__c = 'Active'\r\n" + 
//	    		" and RecordType.Name = 'Commodity'\r\n" + 
//	    		" and CreatedDate > 2019-12-01T00:00:00Z\r\n" + 
//	    		" and RecordType.Name NOT IN ('VAS')\r\n" + 
//	    		"GROUP BY Account.NE__Fiscal_code__c\r\n" + 
//	    		"HAVING count(Id) > 1\r\n" + 
//	    		"LIMIT 150\r\n" + 
//	    		"");
	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);	
		PrivateAreaMarketingConsensus.main(args);
		//PrivateAreaFibraMelita.main(args);
		PrivateAreaFibraMelitaInformativa.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
