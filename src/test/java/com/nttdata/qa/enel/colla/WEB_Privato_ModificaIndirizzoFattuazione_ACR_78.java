package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.Privato_ACR_78;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_ModificaIndirizzoFattuazione_ACR_78 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r32019p109statocontiwe.b@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");

		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		
		Login_Private_Area.main(args);
		
		//LoginEnel.main(args);
		//PrivateAreaMenu.main(args);
		Privato_ACR_78.main(args);
	};

	@After
	public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}