package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Pubblico_ModuloReclami_69;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_ModuloReclami_69 {
	

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Pubblico_ModuloReclami_69.main(args);
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
