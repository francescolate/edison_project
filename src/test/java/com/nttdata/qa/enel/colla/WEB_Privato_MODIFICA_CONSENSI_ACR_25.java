package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.GetAccountEmailNoChange;
import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyRichestaDetails;
import com.nttdata.qa.enel.testqantt.colla.Privato_ACR_25;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MODIFICA_CONSENSI_ACR_25 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("LINK", Costanti.salesforceLink);
	    prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
	    prop.setProperty("WP_USERNAME", "r.affael.laquomo@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
//	    prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
//		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
	   // prop.setProperty("PROFILE_INFO_TEXT", "Titolare Mancuso Salvatore Codice FiscaleMNCSVT58H26H325L Telefono Fisso0818142364 Numero di Cellulare+39 3669047153 Email$email$ PECNon presente Canale di contatto preferito Telefono Cellulare");
	    prop.setProperty("ACTION_TYPE", "EMAIL");
	    prop.setProperty("CF", "MNCSVT58H26H325L");
	    prop.setProperty("ACCOUNT_ID", "0010Y00000G8JwnQAF");
	    prop.setProperty("STATO", "CHIUSO");
	    prop.setProperty("SOTTOSTATO", "INVIATO");
	    prop.setProperty("TITOLARE","MANCUSO SALVATORE");
	    
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		GetAccountEmailNoChange.main(args);
		Privato_ACR_25.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		GetAccountFromCF.main(args);
		VerifyRichestaDetails.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		   InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	};

}
