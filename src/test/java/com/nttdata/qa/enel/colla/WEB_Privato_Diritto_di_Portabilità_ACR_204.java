package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.AccountRegistration;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration_204;
import com.nttdata.qa.enel.testqantt.colla.GetDataRegistration_204;
import com.nttdata.qa.enel.testqantt.colla.Privato_203_Diritto_Portabilità;
import com.nttdata.qa.enel.testqantt.colla.Privato_204_ACR_Diritto_di_Portabilità;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_Privato_Diritto_di_Portabilità_ACR_204 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		//prop.setProperty("LINK", "https://www-coll1.enel.it/");
		//prop.setProperty("USERNAME", "r.32019p109statocont.iweb@gmail.com");
		//prop.setProperty("USERNAME", "r.32019p109statocontiwe.b@gmail.com");
		//prop.setProperty("PASSWORD", "Password01");
		//r.32019p109stato.contiweb@gmail.com
		//r.32019p109statocontiw.eb@gmail.com
		//r.32019p109statocontiwe.b@gmail.com
		
		prop.setProperty("WP_LINK", "https://www-coll2.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
	//	prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "");
		//prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		GetDataRegistration_204.main(args);
		AccountRegistration.main(args);
		SetUtenzaMailResidenziale.main(args);
        RecuperaLinkCreazioneAccount.main(args);
        ContinueWithRegistration_204.main(args);
		Privato_204_ACR_Diritto_di_Portabilità.main(args);	
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
