package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID321;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Bolletta_WEB_ACR_321 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "mobiletes.tingaut.o.m.a.t.i.o.n@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SERVIZI_HOMEPAGE", "Servizi per le forniture");
		prop.setProperty("BOLLETTA_WEBPAGE_TITLE", "Bolletta Web");
		prop.setProperty("SERVIZI_HOMEPAGE_SECTIONTITLE", "Servizi per le bollette");
		prop.setProperty("MODIFICA_BOLLETTA_WEBPAGE_TITLE", "Modifica Bolletta Web");
		prop.setProperty("MODIFICA_BOLLETTA_WEBPAGE_TITLE_SUBTEXT", "Seleziona una o più forniture su cui intendi modificare il servizio di Bolletta Web.");
		prop.setProperty("EMAIL", "mobiletes.tingaut.o.m.a.t.i.o.n@gmail.com");
		prop.setProperty("CELLULARE", "3310101003");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_Bolletta_Web_ID321.main(args);
		
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}