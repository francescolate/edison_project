package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.VerifyMotorinoScegliTu100x100;
import com.nttdata.qa.enel.testqantt.colla.VerifyMotorinoScegliTu100x100_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_MOTORINO_OFFERTA_SCEGLI_TU_100X100_15 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		//TODO Change colla to colla1 before commit
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("LINK100", "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/scegli-tu-100x100-notte-festivi");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		//VerifyMotorinoScegliTu100x100.main(args);
		VerifyMotorinoScegliTu100x100_2.main(args);

	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
	
}
