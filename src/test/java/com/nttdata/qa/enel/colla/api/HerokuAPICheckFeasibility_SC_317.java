/*HerokuAPICheckOKSAP_SC_317
** Chiamata API checkfeasibility - KO No "channel"  - APP - RES	
***/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPISendPost;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaCivico;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPICheckFeasibility_SC_317 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("Authorization", "Token");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		prop.setProperty("CF_PIVA","SCGCLD97H41F839F");
		prop.setProperty("USERNAME","collaudo07@yopmail.com");
		prop.setProperty("USERUPN","559e25ee-1ca9-4882-ad2e-cd9b9a2dc426");
		prop.setProperty("ENELID","4edf0bb4-3fe5-4004-9748-4f5f2f741b46");
		prop.setProperty("EXPIRATION","2021-07-30");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/2g-ind/api/query/v1/checkfeasibility");
        prop.setProperty("JSON_INPUT", "{\"data\":{\"idAsset\":\"02i1l0000037lW9AAI\",\"channel\":\"\",\"processName\":\"CAMBIO_FASCIA\"}}");
		prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"OK\",\"code\":\"0\",\"description\":\"SUCCESS\"}");
		prop.setProperty("JSON_DATA_OUTPUT_2", "\"data\":{\"sourceSystem\":null,\"description\":null,\"idAsset\":null,\"outcomeCode\":\"0000\",\"outcome\":\"OK\"}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input
     * Verificare l'output
	 * API call for Tokens
     * Chiamata API checkfeasibility - KO No "channel"  - APP - RES	
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPISendPost.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
