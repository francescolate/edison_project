package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_74_Modulo_Reclami;
import com.nttdata.qa.enel.testqantt.colla.LaunchModuloReclami;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_ModuloReclami_74 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("LUCE_E_GAS", "Luce e gas");
		prop.setProperty("IMPRESA", "Imprese");
		prop.setProperty("INSIEME A TE", "Insieme a te");
		prop.setProperty("STORIE", "Storie");
		prop.setProperty("FUTUR-E", "Futur-e");
		prop.setProperty("MEDIA", "Media");
		prop.setProperty("SUPPORTO", "Supporto");
		prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
		prop.setProperty("CREDITS", "Credits");
		prop.setProperty("PRIVACY", "Privacy");
		prop.setProperty("COOKIE_POLICY", "Cookie Policy");
		prop.setProperty("REMIT", "Remit");
		prop.setProperty("PREFERENZE_COOKIE", "Preferenze Cookie");
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LaunchModuloReclami.main(args);
		ID_74_Modulo_Reclami.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
