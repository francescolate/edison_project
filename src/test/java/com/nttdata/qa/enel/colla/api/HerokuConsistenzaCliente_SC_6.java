package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
//import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_6 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";

	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"\",   \"piva\": \"\",   \"podPdr\": \"\",   \"numeroutente\": \"\",   \"uniqueid\": \"901f595c-79c8-4311-bc21-66dbc236d817\"  } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"617338018\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA LICIA 18 - 00183 ROMA RM\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"Y\",\"podPdr\":\"IT002E3305071A\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"Bollettino Postale\",\"dataAttivazioneFornitura\":\"2018-02-28T23:00:00.000Z\",\"dataAttivazioneProdotto\":\"2019-03-01\",\"prodotto\":\"Sempre Con Te\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con il seguente TAG in Input UniqueId
     * Verificare vi sia risposta corretta e non vi sia errore
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
         //String args[] = {nomeScenario};
         InputStream in = new FileInputStream(nomeScenario);
         prop.load(in);
         this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
         ReportUtility.reportToServer(this.prop);
       };

	

}
