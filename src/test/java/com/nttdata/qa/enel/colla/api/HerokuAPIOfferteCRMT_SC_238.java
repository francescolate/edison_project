/* HerokuAPICoperturaIndirizzo_SC_238 
 * Need Token  - NEED VPN ENEL
** OK - RES SWITCH ATTIVO - Annullata - WEB

"userUPN": "7ccb61f5-32be-4663-9ba8-b45cbefa141b",
"cf": "BGLGPP32L14E506L",
"enelId": "2feb6bb1-88b3-44d7-8419-d2e6788728f0",
"userEmail": "r.3201.9p109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_238 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","BGLGPP32L14E506L");
		prop.setProperty("USERNAME","r.3201.9p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","7ccb61f5-32be-4663-9ba8-b45cbefa141b");
		prop.setProperty("ENELID","2feb6bb1-88b3-44d7-8419-d2e6788728f0");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
////		prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJFRTAwMDAwMTgxNTJAQk9YLkVORUwuQ09NIiwiYXVkIjoiQXBwTW9iaWxlIiwiY29kaWNlZmlzY2FsZWNvdW50cnkiOiJJVCIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZW5lbGlkIjoiZTY0NzE0NTktZDY4Zi00OTUyLTg0ZjQtNTQ5MzFiOGY2ODczIiwiZXhwIjoxNjA3Njk0MDE2LCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZDQyNDcwOGEtMjcwNS00NTE2LWFmYjAtOTgzODQ4ZmJkZmRjIiwic2lkIjoiN2FiOTRlMGItODA0Mi00ZjA5LTg1NTAtYjM0ODRiMWNmOGMyIn0.1jyqWzsW80_DxuiAfoliy0ZhSEwIA_qMP1W-0EWkvyY");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
	           prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"PRE-CHECK KO DEFINITIVO\",\"numeroOre\":null,\"statoCaseItem\":\"Annullata\",\"statoUdb\":\"IN ATTESA\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT002E5596452A\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3106728\",\"flagStd\":0,\"namingInFattura\":\"Sempre Con Te\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5001l000003n8phAAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OPnFAAW\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000UnnMEAS\",\"dataRichiesta\":\"2020-03-31T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwBHUAY\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA ALO GIOVANNOLI 22 - 00176 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * OK - RES SWITCH ATTIVO - Annullata - WEB
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
