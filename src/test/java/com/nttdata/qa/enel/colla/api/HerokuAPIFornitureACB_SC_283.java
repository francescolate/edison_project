/*HerokuAPIFornitureACB_SC_283 
** API call for Tokens - Need VPN ENEL
* Fornitura ACB - OK - WEB - BSN Stato Fornitura CESSATA  
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 00113800312 r32019p109.statocontiweb@gmail.com 93ce26ed-04b6-4c33-866f-bedb2d7e4a00 5b4bb143-eaee-421d-b3f1-93edce3e89fa 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIFornitureACB;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIFornitureACB_SC_283 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		//prop.setProperty("PIVA","00113800312");
		//prop.setProperty("USERNAME","r32019p109.statocontiweb@gmail.com");
		//prop.setProperty("USERUPN","93ce26ed-04b6-4c33-866f-bedb2d7e4a00");
		//prop.setProperty("ENELID","5b4bb143-eaee-421d-b3f1-93edce3e89fa");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiOTNjZTI2ZWQtMDRiNi00YzMzLTg2NmYtYmVkYjJkN2U0YTAwIiwiY29kaWNlZmlzY2FsZSI6IjAwMTEzODAwMzEyIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMTA3MDA0LCJlbmVsaWQiOiI1YjRiYjE0My1lYWVlLTQyMWQtYjNmMS05M2VkY2UzZTg5ZmEiLCJ1c2VyaWQiOiJyMzIwMTlwMTA5LnN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiMmM2MTJiMTEtNGNhZi00MGVlLWE4ZTItZmVhMjA5ZTA4MTdmIn0.fPtffm8EheE__FcfiwnGyhpH9Bg3ElLi3_QdzMkQUDs");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", " http://10.151.58.208:82/enel-web-hp-extra-stage/v1/info/ricercaFornitureAcb?cf=" + prop.getProperty("CF_PIVA") + "&numMesiMaxCess=12");
		//prop.setProperty("JSON_DATA_OUTPUT","{\"data\":{\"forniture\":[{\"idAccount\":\"0010Y00000G2C23QAF\",\"numeroUtente\":\"639391728\",\"listinoInFattura\":\"Anno Sicuro\",\"partitaIvaCliente\":\"00113800312\",\"idAsset\":\"02i0Y000000kw54QAA\",\"dataAttivazione\":\"2007-01-31T23:00:00.000Z\",\"contrattoSAP\":\"0009039617\",\"codiceFiscale\":\"00113800312\",\"podPdr\":\"IT001E04665371\",\"stato\":\"Disattivato\",\"tipologiaCommodity\":\"Elettrico\",\"aliasAccount\":null,\"indirizzoFornitura\":\"VIA BUGATTO GIUSEPP 4 - 34077 RONCHI DEI LEGIONARI GO\",\"alias\":null,\"bpAccountSAP\":\"0001238391\",\"contractAccountSAP\":\"001000393231\",\"citta\":\"RONCHI DEI LEGIONARI\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}"); 

        prop.setProperty("PIVA","00518860168");
        prop.setProperty("USERNAME","r.3.2.0.19p109.statocontiweb@gmail.com");
        prop.setProperty("USERUPN","c7ebe69d-12ff-4e36-a8ce-80dd2846170f");
        prop.setProperty("ENELID","c38ff79a-1d05-408c-b0e4-1e07f33ca10f");	
        prop.setProperty("JSON_DATA_OUTPUT","idAccount\":\"0010Y00000FWtlSQAT\",\"numeroUtente\":\"635602821\",\"listinoInFattura\":\"Anno Sicuro\",\"partitaIvaCliente\":\"00518860168\",\"idAsset\":\"02i0Y000002M5FnQAK\",\"dataAttivazione\":\"2007-06-30T22:00:00.000Z\",\"contrattoSAP\":\"0023857156\",\"codiceFiscale\":\"00518860168\",\"podPdr\":\"IT001E14312849\",\"stato\":\"Disattivato\",\"tipologiaCommodity\":\"Elettrico\",\"aliasAccount\":null,\"indirizzoFornitura\":\"VIALE S.LUCIO 59A - 24023 CLUSONE BG\",\"alias\":null,\"bpAccountSAP\":\"0001468845\",\"contractAccountSAP\":\"001000687835\",\"citta\":\"CLUSONE\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}"); 
        	
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens - Need VPN ENEL
     * Fornitura ACB - OK - WEB - BSN Stato Fornitura CESSATA  
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIFornitureACB.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
