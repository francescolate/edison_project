package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.OreFreeDettaglioFornitura;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;

public class SanityCheck_Login {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	private Properties prop;
	final String nomeScenario="SanityCheck_Login.properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-colla.enel.it/");
	    prop.setProperty("WP_USERNAME", "annamaria.cecili@enelsaa3010.enel.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "RES_LIKE");
	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");
	    prop.setProperty("ORE_FREE_ORARIO_DI_START", "20:00");
	    
	    if(System.getProperty("WP_LINK")!=null){
	    	prop.setProperty("WP_LINK", System.getProperty("WP_LINK"));
	    	prop.setProperty("WP_USERNAME", System.getProperty("WP_USERNAME"));
	    	prop.setProperty("WP_PASSWORD", System.getProperty("WP_PASSWORD"));
	    }
	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		
	};
}