package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.VerifyBsnSelfCareHome;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.ModifyInfoEnelEnergia_Bsn;
import com.nttdata.qa.enel.testqantt.colla.ModifyInfoEnelEnergia_Bsn_Exit;

public class WEB_Privato_INFO_ENEL_ENERGIA_ACB_23 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
		//prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com");
		//prop.setProperty("WP_USERNAME", "collaudoloyaltycolazzo.20.20@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaudolo.yaltycolazzo20.20@gmail.com");
		//prop.setProperty("WP_USERNAME", "t.estingcrma.u.t.omation@gmail.com");
		//prop.setProperty("WP_USERNAME", "r32019p109statocontiw.e.b@gmail.com");
		//prop.setProperty("WP_USERNAME", "r.32019p109statoco.ntiweb@gmail.com");
		prop.setProperty("WP_USERNAME", "affiddaqe-8312@yopmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		
	//	:r.32019p109statoco.ntiweb@gmail.com or collaudoloyaltycolazzo.20.20@gmail.com affiddaqe-8312@yopmail.com
		
		prop.setProperty("VERIFY_BSN_SC_TITLE", "false");
		prop.setProperty("SHOULD_LOGOUT", "false");
		prop.setProperty("BSN_LEFT_MENU_ITEM_LOGOUT", "");
		
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//VerifyBsnSelfCareHome.main(args);
		ModifyInfoEnelEnergia_Bsn.main(args);
		ModifyInfoEnelEnergia_Bsn_Exit.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		   InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	};

}
