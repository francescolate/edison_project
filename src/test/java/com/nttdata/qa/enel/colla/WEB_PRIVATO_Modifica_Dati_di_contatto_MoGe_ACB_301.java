package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyRequestDetails_ACB_301;
import com.nttdata.qa.enel.testqantt.colla.ACB_301_Modifica_DatiDiContato;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Dati_di_contatto_MoGe_ACB_301 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		
		prop.setProperty("CF", "03430350177");
		prop.setProperty("TITOLARE", "IDEAL COLOR SNC");
		prop.setProperty("CF", "03430350177");
		prop.setProperty("TELEFONO", "+390818890127");
		prop.setProperty("EMAILPEC", "idealcolorsnc@comunicapec.it");
		prop.setProperty("EMAIL", "fabiana.manzo@nttdata.com");
		
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);
		ACB_301_Modifica_DatiDiContato.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		GetAccountFromCF.main(args);
		VerifyRequestDetails_ACB_301.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
