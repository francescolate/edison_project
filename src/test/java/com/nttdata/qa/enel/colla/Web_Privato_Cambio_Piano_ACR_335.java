
package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.Privato_Cambio_Piano_ACR_335;
import com.nttdata.qa.enel.util.ReportUtility;


public class Web_Privato_Cambio_Piano_ACR_335 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		// By Fab
		prop.setProperty("WP_USERNAME", "dan01@yopmail.com"); 
		//By VG 
//		prop.setProperty("WP_USERNAME", "zerocalcaretest@yopmail.com"); //CLCZRE90L21H501B
		//310521838
		//310522248 -- Current one 
		//310522233
		prop.setProperty("WP_PASSWORD", "Password01"); 
		//prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Private_Area.main(args);
		//LoginEnel.main(args);
		Privato_Cambio_Piano_ACR_335.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
