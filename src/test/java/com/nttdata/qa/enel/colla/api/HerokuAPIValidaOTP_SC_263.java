// HerokuAPIValidaOTP_SC_263
// Solo per WEB  - TOken generate
/* Chiamata API per Token
Chiamata API valida OTP ( email di certificazione). Codice errato o scaduto.
Messaggio ricevuto "Invalid Validation Code"
 */

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIValidaOTP_1;

public class HerokuAPIValidaOTP_SC_263 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CODE","18330ab3-4eee-4505-95e2-a09af070118c");
		prop.setProperty("Authorization", "Bearer " + APIService.getToken());
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/provisioning/validation-code/validate");
		prop.setProperty("JSON_INPUT", "{    \"code\": \"" + prop.getProperty("CODE") + "\",    \"metadata\": {        \"language\": \"it_IT\",        \"processChannel\": \"ENEL_ENERGIA\"    }}"); 
		prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":20001,\"data\":{\"success\":false,\"enelId\":null},\"message\":\"Invalid Validation Code\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Call API Delete Account with non-existing enelid
     * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};

		HerokuAPIValidaOTP_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };


	

}
