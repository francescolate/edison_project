/* HerokuAPIAccountProfileByID_SC_136 
** AccountProfileByID - caso KO - APP - NO CHANNEL
*PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_1;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIAccountProfileByID_SC_136 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","");
		prop.setProperty("CF","PAXNDR93H23F205I");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("TID", UUID.randomUUID().toString());
		prop.setProperty("SID", UUID.randomUUID().toString()); 
		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		
		//prop.setProperty("Authorization", "1");
		prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"AUT02\",\"code\":\"-1\",\"description\":\"Client non autorizzato\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * AccountProfileByID - caso KO - APP - NO CHANNEL
  	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccountProfileByID_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
