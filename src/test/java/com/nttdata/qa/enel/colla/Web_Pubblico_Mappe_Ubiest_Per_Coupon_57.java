package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Mappe_Ubiest_Coupon_57;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Pubblico_Mappe_Ubiest_Per_Coupon_57 {
	
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "r32019p109statocontiwe.b@gmail.com");		
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("LUCELINK", "https://www-coll1.enel.it/it/luce-e-gas/enelpremia?zoneid=luce_e_gas-tile_small");
		prop.setProperty("LUCEURL", "https://www-coll1.enel.it/it/luce-e-gas");
		prop.setProperty("ALLAPPURL", "https://apps.apple.com/it/app/enel-energia/id767368903");
		prop.setProperty("GOOGLE_PLAYSTORE", "https://play.google.com/store/apps/details?id=it.enel&hl=it");
		prop.setProperty("COMEFUNZIONA_URL", "https://www-coll1.enel.it/it/luce-e-gas/enelpremia/come-funziona");
		prop.setProperty("ENELPREMIA", "https://www-coll1.enel.it/it/luce-e-gas/enelpremia.html");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Mappe_Ubiest_Coupon_57.main(args);
		
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};


}
