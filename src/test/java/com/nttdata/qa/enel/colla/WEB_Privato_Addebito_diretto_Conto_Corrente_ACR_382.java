package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_382_ACR_Addebito_Diretto;
//import com.nttdata.qa.enel.testqantt.colla.Privato_328_ACR_Addebito_Diretto;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Addebito_diretto_Conto_Corrente_ACR_382 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	@Before
	public void startTest() throws Exception{
	this.prop=new Properties();
	prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	prop.setProperty("WP_USERNAME", "silvia.test2020@gmail.com");		
	prop.setProperty("WP_PASSWORD", "Password01");
	prop.setProperty("AREA_CLIENTI", "");
	prop.setProperty("ACCOUNT_TYPE", "");
	prop.setProperty("IBAN1", "DE45234523452345234234523423423423423");
	prop.setProperty("IBAN2", "IT");
	prop.setProperty("IBAN3", "DE!?ì");
	prop.setProperty("IBAN4", "GB()/&%$£");
	prop.setProperty("IBAN5", "DF");
	prop.setProperty("IBAN6", "1254");
    prop.setProperty("IBAN7", "MC2254128987568565651542125");
    prop.setProperty("RUN_LOCALLY","Y");
	prop.setProperty("RETURN_VALUE", "OK");
	prop.store(new FileOutputStream(nomeScenario), null);	
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_382_ACR_Addebito_Diretto.main(args);
		
	
	};
	
	@After
    public void endTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
	};

}
