/* HerokuAPIFreeSlot_SC_148 
** Case KO - NO Data End - APP
* need token (cliente qualsiasi)
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIFreeSlot_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIFreeSlot_SC_148 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("POD_PDR","IT022E25779970");
		prop.setProperty("DATA_START","20190801");
		prop.setProperty("DATA_END","");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		prop.setProperty("CF_PIVA","PAXNDR93H23F205I");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("EXPIRATION","2021-12-31");
		
		
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZGNjZDdmMWEtNmZiOS00NjJkLWEyZTYtNzNjNTZiZDMzZWZlIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJleHAiOjE2MTIxMDEwNTQsImVuZWxpZCI6ImY3MDA4MTVmLTFhYzAtNDQzOS05OWFjLWI0N2U3YTUzZmU0ZiIsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiNWE1MGVlODgtYmI3Yy00YWY4LWI3MjktMjM1NTQ4NzdhMDc1In0.qEoLRcNU0PHYdVJv-urPqgrFds64bsLyouZF0g4WVkY");
//		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/2g-ind/api/query/v1/freeslot/"+ prop.getProperty("POD_PDR") + "?startdate="+ prop.getProperty("DATA_START") + "&enddate="+ prop.getProperty("DATA_END"));
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"GEN01\",\"code\":\"-1\",\"description\":\"Errore parametri di input\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Case KO - NO Data End - APP
     * Need Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIFreeSlot_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
