package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Diritto_di_Portabilita_ACR_197;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Diritto_di_Portabilità_ACR_197 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "annamaria.cecili@enelsaa3005.enel.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("URL", "https://www-coll1.enel.it/it/supporto/faq/privacy");
		
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Private_Area.main(args);
		Diritto_di_Portabilita_ACR_197.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}





















