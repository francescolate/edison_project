package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID306;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Revoca_Bolletta_WEB_ACB_306 {

	Properties  prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "co.llaudoloyaltycolazzo.2020@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_PATH", "AREA RISERVATA HOMEPAGE");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area dedicata Business");
		prop.setProperty("SERVIZI_PATH", "AREA RISERVATA SERVIZI");
		prop.setProperty("SERVIZI_PAGETITLE", "Servizi");
		prop.setProperty("SERVIZI_PAGETITLE_SUBTEXT", "In questa pagina trovi tutti i servizi.");
		prop.setProperty("BOLLETTA_WEB_PAGETITLE", "Bolletta Web");
		prop.setProperty("BOLLETTA_WEB_PAGETITLE_SUBTEXT", "Il servizio bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica tramite SMS.");
		prop.setProperty("ATTIVAZIONE", "Attivazione Vuoi attivare Bolletta Web sulle tue forniture? SCOPRI DI PIÙ");
		prop.setProperty("MODIFICA", "Modifica Vuoi modificare Bolletta Web? SCOPRI DI PIÙ");
		prop.setProperty("REVOCA", "Revoca Vuoi disattivare Bolletta Web su alcune delle tue forniture? SCOPRI DI PIÙ");
		prop.setProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE", "Disattivazione Bolletta Web");
		prop.setProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE_SUBTEXT", "Seleziona la fornitura per la quale vuoi disattivare Bolletta Web.");
		prop.setProperty("CURRENTSTEP1", "Riepilogo e conferma dati");
		prop.setProperty("SUPPLY_DEACTIVE_MESSAGE", "Stai per disattivare il servizio su questa fornitura.");
		prop.setProperty("SUPPLY_DEACTIVE_CONFIRMING_MESSAGE", "Sei sicuro di voler rinunciare ai vantaggi di Bolletta Web?");
		prop.setProperty("DAILOG_TITLE", "Attenzione");
		prop.setProperty("DAILOG_TEXT", "Se uscirai dal processo perderai le modifiche effettuate.");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_Bolletta_Web_ID306.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}
