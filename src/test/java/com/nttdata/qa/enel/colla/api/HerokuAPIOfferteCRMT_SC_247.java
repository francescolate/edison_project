/* HerokuAPICoperturaIndirizzo_SC_247 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - WEB - BSN - SWA Sospeso
* 
java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 00086480118 r.32019p109statoco.ntiweb@gmail.com 30a1fe52-f3e7-4ca5-a592-c30a26890c2c 0418d8e5-b56c-455c-812a-89ef40baf1f3 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_247 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","00086480118");
		prop.setProperty("USERNAME","r.32019p109statoco.ntiweb@gmail.com");
		prop.setProperty("USERUPN","30a1fe52-f3e7-4ca5-a592-c30a26890c2c");
		prop.setProperty("ENELID","0418d8e5-b56c-455c-812a-89ef40baf1f3");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkY2NkN2YxYS02ZmI5LTQ2MmQtYTJlNi03M2M1NmJkMzNlZmUiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJmNzAwODE1Zi0xYWMwLTQ0MzktOTlhYy1iNDdlN2E1M2ZlNGYiLCJleHAiOjE2MDc2OTI5MDEsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZmRkZmY1NjUtNzVlYS00MzEyLTg5NjMtZTY5NzM0ZTAyMmIzIiwic2lkIjoiNjJhNjJkYWYtZWMxNS00MTdhLTlmNjEtZTVkNGFjYTFmZTdjIn0.tjo0jJ5Tvijg-9Wwnk98VgM4xm0RoB8bVsn9F1SMHJM");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//      prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Sospesa\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E00095843\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3080723\",\"flagStd\":1,\"namingInFattura\":\"Soluzione Energia Impresa Mono\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5001l000003KwVEAA0\",\"idListino\":\"a1l1l000000OPD3AAO\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000J7uvEAC\",\"dataRichiesta\":\"2019-12-03T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AyF3EAK\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"LOC LIVELLI SNC - 19020 BEVERINO SP\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Sospesa\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E00095843\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3080723\",\"flagStd\":1,\"namingInFattura\":\"Soluzione Energia Impresa Mono\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5001l000003KwVEAA0\",\"tipologiaProdotto\":\"BSN\",\"idListino\":\"a1l1l000000OPD3AAO\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000J7uvEAC\",\"dataRichiesta\":\"2019-12-03T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AyF3EAK\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"LOC LIVELLI SNC - 19020 BEVERINO SP\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
        
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - WEB - BSN - SWA Sospeso
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
