/* HerokuAPICoperturaIndirizzo_SC_214 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - SUBENTRO - Annullato

"userUPN": "EE0000013353@BOX.ENEL.COM",
"cf": "FBBCRL64P12C632K",
"enelId": "8156efdf-4d63-40f0-8b4d-cac5208045c5",
"userEmail": "sara.proiettitozzi@enelsaa0205.enel.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_214 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","FBBCRL64P12C632K");
		prop.setProperty("USERNAME","sara.proiettitozzi@enelsaa0205.enel.com");
		prop.setProperty("USERUPN","EE0000013353@BOX.ENEL.COM");
		prop.setProperty("ENELID","8156efdf-4d63-40f0-8b4d-cac5208045c5");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Rinuncia cliente\",\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"01613703023126\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3056682\",\"flagStd\":0,\"namingInFattura\":\"SoloXTe Gas\",\"statoR2d\":\"Rilavorazione\",\"numeroUtente\":\"310491341\",\"processoInCorso\":\"SUBENTRO\",\"idCase\":\"5001l000003IHJcAAO\",\"idListino\":\"a1l1l000000ORSQAA4\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000HyB4EAK\",\"dataRichiesta\":\"2019-11-05T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000nbUHUAY\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA PALENA 18 - 66100 CHIETI CH\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Rinuncia cliente\",\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"01613703023126\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3056682\",\"flagStd\":0,\"namingInFattura\":\"SoloXTe Gas\",\"statoR2d\":\"Rilavorazione\",\"numeroUtente\":\"310491341\",\"processoInCorso\":\"SUBENTRO\",\"idCase\":\"5001l000003IHJcAAO\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000ORSQAA4\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000HyB4EAK\",\"dataRichiesta\":\"2019-11-05T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000nbUHUAY\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA PALENA 18 - 66100 CHIETI CH\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
       

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - SUBENTRO - Annullato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
