package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_114_RES_SWA_ELE_OCR;
import com.nttdata.qa.enel.testqantt.colla.LaunchOfferta;
//import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetails_98;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_ProcessiAcquisitivi_114 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("NOME", "PETER");
	    prop.setProperty("COGNOME", "PARKER");
	    prop.setProperty("CELLULARE", "7874637282");
	    prop.setProperty("CELLULARECHANGE", "7874637283");
	    prop.setProperty("CF", "PRKPTR72B03H502P");
	    prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
	   	   
		
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LaunchOfferta.main(args);
		ID_114_RES_SWA_ELE_OCR.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
