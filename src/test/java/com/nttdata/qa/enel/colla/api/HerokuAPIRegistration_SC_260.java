/*HerokuAPIRegistration_SC_260
** API call for Tokens
API call Registration with RES customer, Active Supply. CUSTOMER already registered with another EMAIL.
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRegistration_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIRegistration_SC_260 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account");
		prop.setProperty("FIRSTNAME","PAOLA MARIA TERESE");
		prop.setProperty("LASTNAME","BORTONE");
		prop.setProperty("CODICEFISCALE","BRTPMR42R55I452P");
		prop.setProperty("EMAIL","r.32019p1.0.9statocontiweb@gmail.com");
        prop.setProperty("PHONENUMBER","+393312206210");
        // OK prop.setProperty("JSON_INPUT", "{     \"email\": \"" + prop.getProperty("email") + "\",     \"phoneNumber\": \"" + prop.getProperty("phoneNumber") + "\",     \"password\": \"Password01\",     \"firstName\": \"" + prop.getProperty("firstName") + "\",     \"lastName\": \"" + prop.getProperty("lastName") + "\",     \"countryCode\": \"IT\",     \"companyName\":\"\",     \"vatCode\":\"\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"personalId\",                     \"value\": \"" + prop.getProperty("CodiceFiscale") + "\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");
//        prop.setProperty("JSON_INPUT", "{     \"email\": \"" + prop.getProperty("EMAIL") + "\",     \"phoneNumber\": \"" + prop.getProperty("PHONENUMBER") + "\",     \"password\": \"Password01\",     \"firstName\": \"" + prop.getProperty("FIRSTNAME") + "\",     \"lastName\": \"" + prop.getProperty("LASTNAME") + "\",     \"countryCode\": \"IT\",     \"companyName\":\"\",     \"vatCode\":\"\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"personalId\",                     \"value\": \"" + prop.getProperty("CODICEFISCALE") + "\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"personalId\":\"personalId must be unique for 'IT', '" + prop.getProperty("CODICEFISCALE") + "' already exists\"}");		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * API call Registration with RES customer, Active Supply. CUSTOMER already registered with another EMAIL.
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRegistration_2.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
