package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_97_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent_97;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_FormScrivici_97 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("NOME_ASSOCIAZIONE", "CODICI");
		prop.setProperty("CF_DELEGATO", "MCRVCN78C22D662D");
		prop.setProperty("NOMINATIVO_DELEGATO", "VINCENZO MACARO");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("TELEPONE", "3333323231");
		prop.setProperty("NOME", "ALBUS");
		prop.setProperty("COGNOME", "SILENTE");
		prop.setProperty("CF", "SLNLBS68M28H501Y");
		prop.setProperty("ARGOMENTO", "SWA");
		prop.setProperty("MESSAGGIO", "PROVA TEST AUTOMATION ID 97");
	    prop.setProperty("TIPOCLIENTE", "BUSINESS");	
	    prop.setProperty("SEI GIÀ CLIENTE?", "NO");	
	    prop.setProperty("DI COSA HAI BISOGNO?", "Informazioni");	
	    prop.setProperty("VUOI INSERIRE ALLEGATI?", "NO");	
	    prop.setProperty("Privacy", "ACCETTATO");	
	    

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};		
		ID_97_Form_Scrivici.main(args);
		VerifyEmailSubjectAndContent_97.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
