package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.ACB_263_InfoEnergia;
import com.nttdata.qa.enel.testqantt.colla.InfoEnelEnergia_ACR_259;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetails_263;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_INFO_ENEL_ENERGIA_ACR_263 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "collaudoloyaltycolazz.o202.0@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
        prop.setProperty("ACCOUNT_ID", "0010Y00000FuY1HQAV");
        prop.setProperty("CELLULARE", "3459089078");
		prop.setProperty("CONFERMA_CELLULARE", "3459089078");
	    
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.store(new FileOutputStream(nomeScenario), null);
	    
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		ACB_263_InfoEnergia.main(args);
		VerifyRequestDetails_263.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
	};

}
