/* HerokuAPICoperturaIndirizzo_SC_224 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - VOLTURA SENZA ACCOLLO - GAS - ESPLETATA
* "userUPN": "dccd7f1a-6fb9-462d-a2e6-73c56bd33efe",
"cf": "PAXNDR93H23F205I",
"enelId": "f700815f-1ac0-4439-99ac-b47e7a53fe4f",
"userEmail": "r.32019p109stat.ocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_224 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","PAXNDR93H23F205I");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":null,\"podPdr\":\"00880000859175\",\"statoSap\":\"KO\",\"statoSempre\":\"BOZZA\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":null,\"flagStd\":0,\"namingInFattura\":\"GIUSTAXTE GAS SPECIAL 3\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"310505686\",\"processoInCorso\":\"VOLTURA SENZA ACCOLLO\",\"idCase\":\"5001l000004dHssAAE\",\"idListino\":\"a1l1l000000S7SEAA0\",\"dataAttivazione\":\"2020-05-27T00:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000Y9o7EAC\",\"dataRichiesta\":null,\"idProdotto\":\"a1Y1l000001oPCTEA2\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"Via GOFFREDO MAMELI SNC - 00047 MARINO MI\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Cessazione\",\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":null,\"podPdr\":\"00880000859175\",\"statoSap\":\"KO\",\"statoSempre\":\"BOZZA\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":null,\"flagStd\":0,\"namingInFattura\":\"GIUSTAXTE GAS SPECIAL 3\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"310505686\",\"processoInCorso\":\"VOLTURA SENZA ACCOLLO\",\"idCase\":\"5001l000004dHssAAE\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000S7SEAA0\",\"dataAttivazione\":\"2020-05-27T00:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000Y9o7EAC\",\"dataRichiesta\":null,\"idProdotto\":\"a1Y1l000001oPCTEA2\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"Via GOFFREDO MAMELI SNC - 00047 MARINO MI\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Cessazione\",\"processoCombinato\":false}");
        

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - VOLTURA SENZA ACCOLLO - GAS - ESPLETATA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
