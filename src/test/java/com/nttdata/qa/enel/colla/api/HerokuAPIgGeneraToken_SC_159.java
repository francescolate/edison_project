/*HerokuAPIgGeneraToken_SC_159 
** KO - No SOURCE - WEB 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIUserByEnelID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIgGeneraToken_SC_159 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
		@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ACCOUNT_ID","0010Y00000FTxw7QAD");
		prop.setProperty("SOURCECHANNEL","");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/auth-fibra/v1/generaToken?idAccount=");
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"KO\",\"code\":\"-1\",\"description\":\"Client non autorizzato\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - No SOURCE - WEB   
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIUserByEnelID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
