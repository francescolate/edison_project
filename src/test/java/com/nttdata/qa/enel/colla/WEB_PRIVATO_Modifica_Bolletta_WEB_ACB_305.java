package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID305;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Bolletta_WEB_ACB_305 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "collaudoloyalt.ycolazzo20.2.0@gmail.com");//r32019p109st.atocontiwe.b@gmail.com
		prop.setProperty("WP_PASSWORD", "pASSWORD01");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "IMPRESA");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_PATH", "AREA RISERVATA HOMEPAGE");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area dedicata Business");
		prop.setProperty("SERVIZI_PATH", "AREA RISERVATA SERVIZI");
		prop.setProperty("ACTIVESUPPLYSTATUS", "Il servizio Bolletta Web è attivo sulla fornitura:");
		prop.setProperty("MODIFICA_BOLLETTA_WEB_PAGETITLE", "Modifica Bolletta Web");
		prop.setProperty("MODIFICA_BOLLETTA_WEB_PAGETITLE_SUBTEXT", "Completa i campi sottostanti e clicca sul tasto prosegui.");
		prop.setProperty("DAILOG_TITLE", "Attenzione");
		prop.setProperty("DAILOG_TEXT", "Se uscirai dal processo perderai le modifiche effettuate.");
		prop.setProperty("FORNITURE_SUBTEXT", "Tutto quello che c'è da sapere sulla tua fornitura");
		prop.setProperty("SUBTITLE_FORNITURA", "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_Bolletta_Web_ID305.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}



