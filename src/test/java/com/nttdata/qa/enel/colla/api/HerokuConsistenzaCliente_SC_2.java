package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_2 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"DRADRA60D24A944Z\",   \"piva\": \"\",   \"podPdr\": \"\",   \"numeroutente\": \"\",   \"uniqueid\": \"\"  } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"300001594\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA VIRGINIA 12/A - 40017 SAN GIOVANNI IN PERSICETO BO\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"Y\",\"podPdr\":\"IT001E04392213\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2018-03-01T00:00:00.000Z\",\"dataAttivazioneProdotto\":\"2019-03-01\",\"prodotto\":\"Luce 30\"}]}");//,{\"numeroUtente\":\"300001597\",\"tipologiaFornitura\":\"Gas\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA VIRGINIA 12/A - 40017 SAN GIOVANNI IN PERSICETO BO\",\"statoServizio\":\"Attivo\",\"flagResidente\":null,\"podPdr\":\"03081000468030\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"Bollettino Postale\",\"dataAttivazioneFornitura\":\"2018-03-01T00:00:00.000Z\",\"dataAttivazioneProdotto\":\"2018-03-01\",\"prodotto\":\"Gas 20\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con il seguente TAG in Input Codice Fiscale
     * Verificare vi sia risposta corretta e non vi sia errore
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
