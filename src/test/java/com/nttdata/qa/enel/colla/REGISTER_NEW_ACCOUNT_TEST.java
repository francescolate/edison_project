package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.AccountRegistration;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration;
import com.nttdata.qa.enel.util.Utility;

public class REGISTER_NEW_ACCOUNT_TEST {
	
	Properties prop;
	final String nomeScenario="TEST.properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "CASA");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		//NOT UNCOMMENT THE NEXT LINE OR COPY WITHIN YOUR SCRIPTS
		//CreateEmailAddressesList.main(args);
		GetDataForRegistration.main(args);
		AccountRegistration.main(args);
		SetUtenzaMailResidenziale.main(args);
        RecuperaLinkCreazioneAccount.main(args);
        ContinueWithRegistration.main(args);
        
        //YourOwnModuleForKeepGoingWithTheRequestedProcessToBeAutomated
	};
	
	@After
    public void fineTest() throws Exception{
		
	};
}
