/*HerokuAPIRicercaForniture_SC_111 
** API call for Tokens
* Case OK - WEB - API Anagrafica - RES
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PPLLGU63M24E023Z r.32019p109stato.contiweb@gmail.com cfebb068-6eb5-4108-af1e-ec93c764ba34 0d009471-5a94-4e29-9ff1-97be10da0507 2020-12-31

"userUPN": "cfebb068-6eb5-4108-af1e-ec93c764ba34",
"cf": "PPLLGU63M24E023Z",
"enelId": "0d009471-5a94-4e29-9ff1-97be10da0507",
"userEmail": "r.32019p109stato.contiweb@gmail.com"
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIAnagrafica_SC_111 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","PPLLGU63M24E023Z");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUMERO_MESI","0");
		prop.setProperty("USERNAME","r.32019p109stato.contiweb@gmail.com");
		prop.setProperty("USERUPN","cfebb068-6eb5-4108-af1e-ec93c764ba34");
		prop.setProperty("ENELID","0d009471-5a94-4e29-9ff1-97be10da0507");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiY2ZlYmIwNjgtNmViNS00MTA4LWFmMWUtZWM5M2M3NjRiYTM0IiwiY29kaWNlZmlzY2FsZSI6IlBQTExHVTYzTTI0RTAyM1oiLCJpc3MiOiJhcHAiLCJleHAiOjE2MTIwODg1MTQsImVuZWxpZCI6IjBkMDA5NDcxLTVhOTQtNGUyOS05ZmYxLTk3YmUxMGRhMDUwNyIsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdG8uY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZDYwNDMyNTItYzU0Mi00YmVmLWJlOTYtMjQzYTdiN2RiM2I2In0.2KOBJsUxSF8yoYMgqyg5v7veeiWKuEhA-1HznexIKVY");
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/anagrafica?canale=" + prop.getProperty("CANALE") + "&cf=" + prop.getProperty("CF_PIVA") + "&numMesiMaxCess="+ prop.getProperty("NUMERO_MESI"));
		prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"numeroAssetELE\":1,\"tipoAccount\":\"RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"NON RICON.\",\"flagDipendenteEnel\":false,\"totaleUsoNonAbitativo\":0,\"codiceFormaGiuridica\":\"20\",\"numeroPartiteIVA\":0,\"numeroAssetGAS\":0,\"indicatoriSufornitureInCorsoAttivazione\":0,\"account\":[{\"idAccount\":\"0010Y00000FMWwtQAH\",\"tipoAccount\":\"RESIDENZIALE\",\"cellulareSenzaPrefisso\":null,\"indirizzo\":null,\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":null,\"contact\":{\"dataCreazione\":\"2017-02-11T09:33:26.000Z\",\"cognome\":\"IPPOLITO\",\"cellulareSenzaPrefisso\":null,\"accountSocialTwitter\":null,\"codiceFiscale\":\"PPLLGU63M24E023Z\",\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":\"TELEFONO\",\"altroTelefonoSenzaPrefisso\":null,\"telefono\":\"0818142364\",\"fax\":null,\"accountSocialFB\":null,\"altroTelefono\":\"004335411559\",\"indirizzoDomicilio\":\"VIA PONTORME 95     50053 EMPOLI FI <regione vuota>\",\"indirizzoEmail\":\"fabiana.manzo@nttdata.com\",\"indirizzoResidenza\":null,\"descrizioneAltroTelefono\":\"Other\",\"cellulare\":\"004335411559\",\"cellulareCertificato\":false,\"nome\":\"LUIGI\",\"descrizioneTelefono\":\"-\",\"emailCertificata\":true,\"prefissoAltroTelefono\":null,\"prefissoCellulare\":null,\"idContact\":\"0030Y00000Bj75nQAB\",\"descrizioneEmail\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null},\"ragioneSociale\":null,\"telefono\":\"004335411559\",\"fax\":null,\"citta\":null,\"descrizioneAltroTelefono\":null,\"toponomasticaViaCivico\":null,\"flagDipendenteEnel\":false,\"accountType\":null,\"codiceFormaGiuridica\":null,\"nome\":\"LUIGI\",\"descrizioneTelefono\":null,\"civico\":null,\"telefonoValido\":true,\"emailCertificata\":false,\"chiaveAlReferenteTitolare\":\"0030Y00000Bj75nQAB\",\"prefissoCellulare\":null,\"statoCliente\":\"Cliente\",\"descrizioneEmail\":null,\"mercatoRiferimentoCliente\":\"Maggior Tutela\",\"flagPersonaFisicaGiuridica\":\"N\",\"cognome\":\"IPPOLITO\",\"descrizioneFormaGiuridica\":null,\"sedeLegale\":null,\"accountSocialTwitter\":null,\"tipologiaCliente\":\"Casa\",\"provincia\":null,\"codiceFiscale\":\"PPLLGU63M24E023Z\",\"bpsap\":\"0033189871\",\"cap\":null,\"cognomeNome\":\"IPPOLITO LUIGI\",\"accountSocialFB\":null,\"altroTelefono\":null,\"idMasterAccountSiebel\":\"2-96MNZO1\",\"indirizzoEmail\":null,\"cellulare\":null,\"cellulareCertificato\":true,\"flagSintesi\":false,\"partitaIVA\":null,\"toponomastica\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Case OK - WEB - API Anagrafica - RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
