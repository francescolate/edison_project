/*HerokuAPIUserProfileold_SC_286 
** API call for Tokens
* Case OK - User/Profile/old BSN with Supply "Cessata"
* Userupn 93ce26ed-04b6-4c33-866f-bedb2d7e4a00
PIVA 00113800312
 "enelId": "5b4bb143-eaee-421d-b3f1-93edce3e89fa",
"email": "r32019p109.statocontiweb@gmail.com"
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIUserProfileold_SC_286 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiOTNjZTI2ZWQtMDRiNi00YzMzLTg2NmYtYmVkYjJkN2U0YTAwIiwiY29kaWNlZmlzY2FsZSI6IjAwMTEzODAwMzEyIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMTA3MDA0LCJlbmVsaWQiOiI1YjRiYjE0My1lYWVlLTQyMWQtYjNmMS05M2VkY2UzZTg5ZmEiLCJ1c2VyaWQiOiJyMzIwMTlwMTA5LnN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiMmM2MTJiMTEtNGNhZi00MGVlLWE4ZTItZmVhMjA5ZTA4MTdmIn0.fPtffm8EheE__FcfiwnGyhpH9Bg3ElLi3_QdzMkQUDs");
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-stage/v1/user/profile/old?isRetPag=true&isRetAddebitoCC=true&nuPagaButton=");
//		prop.setProperty("JSON_DATA_OUTPUT","\"asset\":{\"asset\":[{\"idAccount\":\"0010Y00000G2C23QAF\",\"livellodiTensione\":\"Bassa Tensione\",\"idServicePoint\":\"a1f0Y000002d2EPQAY\",\"soggettoALegge136\":\"false\",\"dataCessazione\":\"2020-04-01T00:00:00.000Z\",\"dataAdesioneEnelmia\":null,\"podPdr\":\"IT001E04665371\",\"tipologiaServizio\":\"NEW_AE__B\",\"tipologiaEnelmia\":null,\"tipologiaBolletta\":\"BS\",\"tipologiaSmart\":\"N\",\"bpAccountSAP\":\"0001238391\",\"potenzaContrattuale\":\"62.5\",\"descrizioneTipologiaEnelmia\":null,\"numeroCartaEnelPremia\":null,\"indirizzoBanca\":null,\"tipologiaFornitura\":\"Elettrico\",\"dataRecessoEnelmia\":null,\"dataInizioFormaContrattuale\":\"2020-04-02T00:00:00.000Z\",\"partitaIvaCliente\":\"00113800312\",\"flagLoyaltyEnelPremia\":false,\"dataAttivazione\":\"2007-01-31T23:00:00.000Z\",\"indirizzoDiFornitura\":\"VIA BUGATTO GIUSEPP 4 - 34077 RONCHI DEI LEGIONARI GO\",\"potenzaDisponibile\":\"62.5\",\"nomeBanca\":null,\"tipologiaAsset\":\"Commodity\",\"iban\":null,\"codiceContratto\":\"05451258\",\"dataAttivazioneEnelPremia\":null,\"codiceFiscaleCliente\":\"00113800312\",\"tensioneDisponibile\":\"380\",\"cognomeIntestatario\":null,\"idAsset\":\"02i0Y000000kw54QAA\",\"pianoTariffario\":null,\"idContratto\":\"8000Y0000017L4HQAU\",\"rowIdSiebelContratto\":\"2-1HTC-8817\",\"nomeIntestatario\":null,\"ultimoProcessoModificaAsset\":\"Voltura con Accollo\",\"listino\":\"Anno Sicuro\",\"categoriaUso\":null,\"tipoProdotto\":\"Bus\",\"codiceFiscaleIntestatario\":null,\"codiceEnelmia\":null,\"alias\":\"639391728\",\"potenzaInFranchigia\":\"62.5\",\"numeroUtente\":\"639391728\",\"statoFormaContrattuale\":\"ATTIVA\",\"regime\":\"Mercato Libero\",\"flag136\":false,\"assetFiglio\":[],\"flagAdesioneEnelmia\":false,\"indirizzoDiFatturazione\":\"VIA BUGATTI 4, 34077, RONCHI DEI LEGIONARI, GO\",\"idListino\":\"a1l0Y000000XTXIQA4\",\"codiceOfferta\":\"3-E0033846\",\"attributoAsset\":[],\"dataFineFormaContrattuale\":\"2020-01-31T00:00:00.000Z\",\"contrattoSAP\":\"0009039617\",\"idProdotto\":\"a1Y2400000BFIpmEAH\",\"statoServizio\":\"Disattivato\",\"usoFornitura\":\"Uso Diverso da Abitazione\",\"metodoDiPagamento\":\"Bollettino Postale\",\"contractAccountSAP\":\"001000393231\"}]},\"dashboard\":null,\"eeCommodity\":null},\"status\":{\"descrizione\":\"Success - Unica is null\",\"esito\":\"OK\",\"codice\":\"000\""); 
		prop.setProperty("JSON_DATA_OUTPUT","{\"asset\":[{\"idAccount\":\"0010Y00000G2C23QAF\",\"livellodiTensione\":\"Bassa Tensione\",\"idServicePoint\":\"a1f0Y000002d2EPQAY\",\"soggettoALegge136\":\"false\",\"dataCessazione\":\"2020-04-01T00:00:00.000Z\",\"dataAdesioneEnelmia\":null,\"podPdr\":\"IT001E04665371\",\"tipologiaServizio\":\"NEW_AE__B\",\"tipologiaEnelmia\":null,\"tipologiaBolletta\":\"BS\",\"tipologiaSmart\":\"N\",\"bpAccountSAP\":\"0001238391\",\"potenzaContrattuale\":\"62.5\",\"descrizioneTipologiaEnelmia\":null,\"numeroCartaEnelPremia\":null,\"indirizzoBanca\":null,\"tipologiaFornitura\":\"Elettrico\",\"dataRecessoEnelmia\":null,\"dataInizioFormaContrattuale\":\"2020-04-02T00:00:00.000Z\",\"partitaIvaCliente\":\"00113800312\",\"flagLoyaltyEnelPremia\":false,\"dataAttivazione\":\"2007-01-31T23:00:00.000Z\",\"indirizzoDiFornitura\":\"VIA BUGATTO GIUSEPP 4 - 34077 RONCHI DEI LEGIONARI GO\",\"potenzaDisponibile\":\"62.5\",\"nomeBanca\":null,\"tipologiaAsset\":\"Commodity\",\"iban\":null,\"codiceContratto\":\"05451258\",\"dataAttivazioneEnelPremia\":null,\"codiceFiscaleCliente\":\"00113800312\",\"tensioneDisponibile\":\"380\",\"cognomeIntestatario\":null,\"idAsset\":\"02i0Y000000kw54QAA\",\"pianoTariffario\":null,\"idContratto\":\"8000Y0000017L4HQAU\",\"rowIdSiebelContratto\":\"2-1HTC-8817\",\"nomeIntestatario\":null,\"ultimoProcessoModificaAsset\":\"Voltura con Accollo\",\"listino\":\"Anno Sicuro\",\"categoriaUso\":null,\"tipoProdotto\":\"Bus\",\"codiceFiscaleIntestatario\":null,\"codiceEnelmia\":null,\"alias\":\"639391728\",\"potenzaInFranchigia\":\"62.5\",\"numeroUtente\":\"639391728\",\"statoFormaContrattuale\":\"ATTIVA\",\"regime\":\"Mercato Libero\",\"flag136\":false,\"assetFiglio\":[],\"flagAdesioneEnelmia\":false,\"indirizzoDiFatturazione\":\"VIA BUGATTI 4, 34077, RONCHI DEI LEGIONARI, GO\",\"idListino\":\"a1l0Y000000XTXIQA4\",\"codiceOfferta\":\"3-E0033846\",\"attributoAsset\":[],\"dataFineFormaContrattuale\":\"2020-01-31T00:00:00.000Z\",\"contrattoSAP\":\"0009039617\",\"idProdotto\":\"a1Y2400000BFIpmEAH\",\"statoServizio\":\"Disattivato\",\"usoFornitura\":\"Uso Diverso da Abitazione\",\"metodoDiPagamento\":\"Bollettino Postale\",\"contractAccountSAP\":\"001000393231\"}]},\"dashboard\":null,\"eeCommodity\":null},\"status\":{\"descrizione\":\"Success\"");
		prop.setProperty("CF_PIVA","00518860168");
		prop.setProperty("USERNAME","r.3.2.0.19p109.statocontiweb@gmail.com");
		prop.setProperty("USERUPN","c7ebe69d-12ff-4e36-a8ce-80dd2846170f");
		prop.setProperty("ENELID","c38ff79a-1d05-408c-b0e4-1e07f33ca10f");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("JSON_DATA_OUTPUT","{\"asset\":[{\"idAccount\":\"0010Y00000FWtlSQAT\",\"livellodiTensione\":\"Bassa Tensione\",\"idServicePoint\":\"a1f0Y000002ugCMQAY\",\"soggettoALegge136\":\"false\",\"codiceDistributore\":null,\"dataCessazione\":\"2021-04-07T00:00:00.000Z\",\"dataAdesioneEnelmia\":null,\"podPdr\":\"IT001E14312849\",\"tipologiaServizio\":\"NEW_AE__B\",\"tipologiaEnelmia\":null,\"tipologiaBolletta\":\"BS\",\"tipologiaSmart\":\"N\",\"bpAccountSAP\":\"0001468845\",\"potenzaContrattuale\":\"15\",\"descrizioneTipologiaEnelmia\":null,\"numeroCartaEnelPremia\":null,\"indirizzoBanca\":\"CLUSONE\",\"tipologiaFornitura\":\"Elettrico\",\"dataRecessoEnelmia\":null,\"dataInizioFormaContrattuale\":\"2021-04-08T00:00:00.000Z\",\"partitaIvaCliente\":\"00518860168\",\"codiceConcessione\":null,\"flagLoyaltyEnelPremia\":false,\"dataAttivazione\":\"2007-06-30T22:00:00.000Z\",\"indirizzoDiFornitura\":\"VIALE S.LUCIO 59A - 24023 CLUSONE BG\",\"potenzaDisponibile\":\"16.5\",\"nomeBanca\":\"INTESA SANPAOLO SPA\",\"tipologiaAsset\":\"Commodity\",\"iban\":\"IT07N0306952910000002102113\",\"codiceContratto\":\"11296620\",\"dataAttivazioneEnelPremia\":null,\"codiceFiscaleCliente\":\"00518860168\",\"tensioneDisponibile\":\"380\",\"cognomeIntestatario\":null,\"idAsset\":\"02i0Y000002M5FnQAK\",\"pianoTariffario\":null,\"idContratto\":\"8000Y000001VryRQAS\",\"rowIdSiebelContratto\":\"2-1J3T-5000\",\"nomeIntestatario\":null,\"ultimoProcessoModificaAsset\":\"Voltura con Accollo\",\"listino\":\"Anno Sicuro\",\"categoriaUso\":null,\"tipoProdotto\":\"BSN\",\"codiceFiscaleIntestatario\":null,\"codiceEnelmia\":null,\"alias\":\"635602821\",\"potenzaInFranchigia\":\"16.5\",\"numeroUtente\":\"635602821\",\"statoFormaContrattuale\":\"ATTIVA\",\"regime\":\"Mercato Libero\",\"flag136\":false,\"assetFiglio\":[],\"flagAdesioneEnelmia\":false,\"indirizzoDiFatturazione\":\"VIALE SAN LUCIO 59 A, 24023, CLUSONE, BG\",\"idListino\":\"a1l1n000000ehKBAAY\",\"codiceOfferta\":\"3-E0041949\",\"attributoAsset\":[{\"valore\":\"N\",\"nomeAttributo\":\"Opzione Agevolata Anno Sicuro\",\"assetAssociato\":\"02i0Y000002M5FnQAK\"},{\"valore\":\"N\",\"nomeAttributo\":\"Corporate\",\"assetAssociato\":\"02i0Y000002M5FnQAK\"},{\"valore\":\"N\",\"nomeAttributo\":\"DLB 153\",\"assetAssociato\":\"02i0Y000002M5FnQAK\"},{\"valore\":\"N\",\"nomeAttributo\":\"Opzione Verde\",\"assetAssociato\":\"02i0Y000002M5FnQAK\"}],\"dataFineFormaContrattuale\":\"2020-04-30T00:00:00.000Z\",\"contrattoSAP\":\"0023857156\",\"idProdotto\":\"a1Y2400000BFIpmEAH\",\"statoServizio\":\"Disattivato\",\"usoFornitura\":\"Uso Diverso da Abitazione\",\"metodoDiPagamento\":\"RID\",\"canaleDiPagamento\":\"conto corrente\",\"contractAccountSAP\":\"001000687835\"}]},\"dashboard\":null,\"eeCommodity\":null},\"status\":{\"descrizione\":\"Success\"");

		prop.store(new FileOutputStream(nomeScenario), null);	
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Case OK - User/Profile/old BSN with Supply "Cessata"
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
