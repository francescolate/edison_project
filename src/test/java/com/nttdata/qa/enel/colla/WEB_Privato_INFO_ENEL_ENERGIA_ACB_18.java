package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu42;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.DataConfiguration;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.colla.ActivateInfoEnelEnergia_Bsn;
import com.nttdata.qa.enel.testqantt.colla.ActivateInfoEnelEnergia_Bsn_Continue;

public class WEB_Privato_INFO_ENEL_ENERGIA_ACB_18 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
		
		//***BSN
	//	prop.setProperty("WP_USERNAME", "r.32019p109.statocontiweb@gmail.com"); //// No attiva //		
	//	prop.setProperty("WP_USERNAME", "testImpresa@yopmail.com"); // No attiva // 07351980565
      prop.setProperty("WP_USERNAME", "affiddaqe-8312@yopmail.com"); //BSN
		
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "IMPRESE");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		
		prop.setProperty("SAVE_CLIENT_NUMBER", "true");
		prop.setProperty("QUERY", "");
		prop.setProperty("QUERY_PROPERTIES", "QUERY");
		prop.setProperty("QUERY_REFERENCES", "query_ACB_18");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		DataConfiguration.main(args);
		LoginEnel.main(args);
		//PrivateAreaMenu42.main(args);
		ActivateInfoEnelEnergia_Bsn.main(args);
		ActivateInfoEnelEnergia_Bsn_Continue.main(args);
		//RecuperaDatiWorkbench.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		   InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	};
}
