package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACB_287;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubject_260;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailUpdateFromSF_287;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Info_Enel_Energia_ACB_287 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r32019p109st.atocontiwe.b@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
	//	prop.setProperty("EMAIL", "mobile.testing.automation@gmail.com"); 
	//	prop.setProperty("CONFERMA_EMAIL", "mobile.testing.automation@gmail.com"); //Enel$001
		prop.setProperty("GMAIL_ADDRESS_INPUT", "mobile.testing.automation@gmail.com");
		prop.setProperty("EMAIL","fabiana.manzo32@nttdata.com");
		prop.setProperty("CONFERMA_EMAIL","fabiana.manzo32@nttdata.com");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.setProperty("CF", "03430350177"); //immi
		
		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Info_Enel_Energia_ACB_287.main(args);
		//VerifyEmailSubject_260.main(args);
		LoginSalesForce.main(args);
        //SbloccaTab.main(args);
        VerifyEmailUpdateFromSF_287.main(args);	
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
