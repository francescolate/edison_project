/*HerokuAPIDelete_SC_277
** API call for Tokens  KO da capire!!!!
API call registration and OTP validation for RES customer
Call API Delete Account with newly registered email RES (validated with OTP of the certification email)
**
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.RecuperaCodiceDaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIDelete_1;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class HerokuAPIDelete_SC_277 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		//prop.setProperty("ENELID","ff942baa-ea6b-48af-a52d-85392dea3405");
		prop.setProperty("Authorization", "Bearer " + APIService.getToken()); 
		// prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account/"+ prop.getProperty("ENELID"));
		prop.setProperty("FIRSTNAME","GIUSEPPE");
		prop.setProperty("LASTNAME","LISO");
		prop.setProperty("CF_PIVA","LSIGPP75P19A285I");
		prop.setProperty("EMAIL","mobiletest.i.n.g.a.u.tomation@gmail.com");
        prop.setProperty("PHONENUMBER",Utility.getMobileNumber());
        prop.setProperty("TIPOCLIENTE", "REG_FROM_MOBILE");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":200,\"data\":{\"success\":true},\"message\":\"OK\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * API call Registration with RES customer, Active Supply, telephone number already used in another registration
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};	
		HerokuAPIDelete_1.main(args); 
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
