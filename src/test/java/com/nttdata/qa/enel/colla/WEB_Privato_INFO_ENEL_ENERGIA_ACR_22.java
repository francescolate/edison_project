package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.VerifyResSelfCareHome;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.ModifyInfoEnelEnergia;
import com.nttdata.qa.enel.testqantt.colla.ModifyInfoEnelEnergia_Continue;

public class WEB_Privato_INFO_ENEL_ENERGIA_ACR_22 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
	//	prop.setProperty("WP_USERNAME", "r32019p109statocontiwe.b@gmail.com");
		prop.setProperty("WP_USERNAME", "testin.gc.rmautomation@gmail.com");
	//	prop.setProperty("WP_USERNAME", "silvia.test77@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "BSN_LIKE");
		
	//	prop.setProperty("AREA_CLIENTI", "");
	//	prop.setProperty("ACCOUNT_TYPE", "");
		
		prop.setProperty("VERIFY_SC_SELECTION_PAGE", "false");
		prop.setProperty("LOAD_BSN_SC_PAGE", "false");
		prop.setProperty("RES_LEFT_MENU_BSN", "");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//VerifyResSelfCareHome.main(args);
		ModifyInfoEnelEnergia.main(args);
		ModifyInfoEnelEnergia_Continue.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		   InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	};
}
