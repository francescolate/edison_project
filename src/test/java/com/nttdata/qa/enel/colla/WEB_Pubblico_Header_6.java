package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.HeaderLogin;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.Support;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Header_6 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("USERNAME_RES", "r32019p109statocontiwe.b@gmail.com");
	    prop.setProperty("PASSWORD_RES", "Password01");
	    prop.setProperty("LINK_SUPPORTO", "https://www-coll1.enel.it/it/supporto");
	    prop.setProperty("LINK_CAMBIO_POTENZA", "https://www-colla.enel.it/it/supporto/faq/cambio-potenza-contatore");
	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		HeaderLogin.main(args);
		Support.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
	       //String args[] = {nomeScenario};
     InputStream in = new FileInputStream(nomeScenario);
     prop.load(in);
     this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
     ReportUtility.reportToServer(this.prop);
		
	};
}
