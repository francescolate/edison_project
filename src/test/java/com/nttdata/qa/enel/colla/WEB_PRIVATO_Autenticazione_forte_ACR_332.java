package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.ACR_332_Autenticazione_Forte;
import com.nttdata.qa.enel.testqantt.colla.AccountRegistration;
import com.nttdata.qa.enel.testqantt.colla.DeleteRegisteredAccount;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_PRIVATO_Autenticazione_forte_ACR_332 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();

		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
		// prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("SUPPLY_NUMBER", "817163263");
		prop.setProperty("CF", "NVKRFL49H07F839C");
		prop.setProperty("COMPANY_NAME", "SINED SPA");
		prop.setProperty("ACCOUNT_FIRSTNAME", "RAFFAELE");
		prop.setProperty("ACCOUNT_LASTNAME", "NOVIKON");
		prop.setProperty("WP_USERNAME", "testin.gcrmautoma.t.ion@gmail.com");

		prop.store(new FileOutputStream(nomeScenario), null);

	};

	@Test
	public void eseguiTest() throws Exception {
		String args[] = { nomeScenario };
		// NOT UNCOMMENT THE NEXT LINE OR COPY WITHIN YOUR SCRIPTS
		// CreateEmailAddressesList.main(args);
		// GetDataForRegistration.main(args);
		DeleteRegisteredAccount.main(args);
		AccountRegistration.main(args);
		SetUtenzaMailResidenziale.main(args);
		RecuperaLinkCreazioneAccount.main(args);
		ACR_332_Autenticazione_Forte.main(args);
		// YourOwnModuleForKeepGoingWithTheRequestedProcessToBeAutomated
	};

	@After
	public void fineTest() throws Exception {

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}