package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.SelezionaConfermaOfferta;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.Email_Res_Swa_Pubblico_60;
import com.nttdata.qa.enel.testqantt.colla.Pubblico_ID_61_Processo_A_RES_SWA_ELE;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubject_Publico60;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Pubblico_Pagina_Atterraggio_Post_Certificazione_Email_60 {
Properties prop;
	
	final String nomeScenario="Web_Pubblico_Pagina_Atterraggio_Post_Certificazione_Email_60.properties";
	final String BASE_LINK = "https://www-coll2.enel.it/it/luce-e-gas/luce/offerte/speciale-luce-60";
	final String HOMEPAGE_LINK = "https://www-coll2.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/speciale-luce-60");
		prop.setProperty("HOMEPAGE", "https://www-coll1.enel.it/");
		prop.setProperty("LUCEGAS", "luce-e-gas");
		prop.setProperty("ADISIONE", "https://www-coll1.enel.it/it/adesione");
		prop.setProperty("NOME", "Farfalla");
		prop.setProperty("COGNOME", "Farfallin");
		prop.setProperty("CODICE_FISCALE", "FRFFFL73A41H501N");
		prop.setProperty("TELEFONO", "3434343434");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CAP", "60027-OSIMO");
		prop.setProperty("CITTA", "OSIMO");
		prop.setProperty("CAP", "60027");
		prop.setProperty("CITY", "OSIMO");
		prop.setProperty("CITY_FULL", "OSIMO, ANCONA, MARCHE");
		prop.setProperty("ADDRESS", "VIA ABBADIA");
		prop.setProperty("INDIRIZZO", "VIA ABBADIA");
		prop.setProperty("CIVIC", "3");
		prop.setProperty("NUMERICO_CIVICO", "33");
		prop.setProperty("ATTUALE_FORNITORE", "ACEA ENERGIA SPA");
		prop.setProperty("SUPPLIER_LUCE", "ACEA ENERGIA SPA");
		prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		//prop.setProperty("LINK", Costanti.salesforceLink);
		// prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test	
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Email_Res_Swa_Pubblico_60.main(args);
		//VerifyAccountStatusType.main(args);
		VerifyEmailSubject_Publico60.main(args);
		SelezionaConfermaOfferta.main(args); 
		//add salesforce
	};	
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};

}
