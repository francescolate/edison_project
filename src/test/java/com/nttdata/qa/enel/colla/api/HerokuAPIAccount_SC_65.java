// HerokuAPIAccount_SC_65
// Solo per WEB  - TOken in input expire 31/12/2020
/*"userUpn": "b208d852-0d32-42d6-a369-5988d8b5bb20",
"userEmail": "r.32019.p109statocontiweb@gmail.com",
"fiscalCode": "01308620564",
"enelId":"a2d8bf0b-19ac-4a65-a613-cf73d3f3e2f7"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar 01308620564 r.32019.p109statocontiweb@gmail.com b208d852-0d32-42d6-a369-5988d8b5bb20 a2d8bf0b-19ac-4a65-a613-cf73d3f3e2f7 2021-12-31
*/
// OK - Canale WEB - BSN

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccount_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIAccount_SC_65 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","01308620564");
		prop.setProperty("USERNAME","r.32019.p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","b208d852-0d32-42d6-a369-5988d8b5bb20");
		prop.setProperty("ENELID","a2d8bf0b-19ac-4a65-a613-cf73d3f3e2f7");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiYjIwOGQ4NTItMGQzMi00MmQ2LWEzNjktNTk4OGQ4YjViYjIwIiwiY29kaWNlZmlzY2FsZSI6IjAxMzA4NjIwNTY0IiwiaXNzIjoiYXBwIiwiZXhwIjoxNjQzNjQwMTkzLCJlbmVsaWQiOiJhMmQ4YmYwYi0xOWFjLTRhNjUtYTYxMy1jZjczZDNmM2UyZjciLCJ1c2VyaWQiOiJyLjMyMDE5LnAxMDlzdGF0b2NvbnRpd2ViQGdtYWlsLmNvbSIsImp0aSI6IjNlMzQzNTQxLTNkNzctNDUyMC1iZDQ2LWRkMTgzYTE5ZTAyZiJ9.WR1HwXLh572aKUZ7UGBHkyJTlQ4-ou2HHhle7jEnSpA");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/authentication/v2/account");
		prop.setProperty("JSON_INPUT", "{   \"data\":{      \"enelId\":\"" + prop.getProperty("ENELID") + "\"  }}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"userUPN\":\"b208d852-0d32-42d6-a369-5988d8b5bb20\",\"countryCfa\":\"IT\",\"phoneNumberSkipped\":false,\"cfa\":\"01308620564\"}");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - Canale WEB - BSN
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccount_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
