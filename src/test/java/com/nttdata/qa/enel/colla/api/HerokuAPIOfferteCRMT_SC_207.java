/* HerokuAPIOfferteCRMT_SC_207 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - PRIMA ATTIVAZIONE RES
* java -jar C:\Users\manzofa\Desktop\token\MobileTokenJWT.jar ZMBGFR38C21A944X francesco.lucia@nttdata.com 049fe692-392b-44a5-bbeb-d80b689db837 c027675e-53d2-4943-a097-3ed45a92be01 2020-12-31
"userUPN": "049fe692-392b-44a5-bbeb-d80b689db837",
"cf": "ZMBGFR38C21A944X",
"enelId": "c027675e-53d2-4943-a097-3ed45a92be01",
"userEmail": "francesco.lucia@nttdata.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_207 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","ZMBGFR38C21A944X");
		prop.setProperty("USERNAME","francesco.lucia@nttdata.com");
		prop.setProperty("USERUPN","049fe692-392b-44a5-bbeb-d80b689db837");
		prop.setProperty("ENELID","c027675e-53d2-4943-a097-3ed45a92be01");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
        //prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Preventivo ricevuto\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT002E3146891A\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3089414\",\"flagStd\":0,\"namingInFattura\":\"Sempre Con Te\",\"statoR2d\":\"Da completare\",\"numeroUtente\":\"310494790\",\"processoInCorso\":\"PRIMA ATTIVAZIONE\",\"idCase\":\"5001l000003l712AAA\",\"idListino\":\"a1l1l000000OPnFAAW\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000JtkQEAS\",\"dataRichiesta\":\"2020-02-19T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwBHUAY\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA ABANO TERME 34611 - 00135 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":true}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Preventivo ricevuto\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT002E3146891A\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3089414\",\"flagStd\":0,\"namingInFattura\":\"Sempre Con Te\",\"statoR2d\":\"Da completare\",\"numeroUtente\":\"310494790\",\"processoInCorso\":\"PRIMA ATTIVAZIONE\",\"idCase\":\"5001l000003l712AAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OPnFAAW\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000JtkQEAS\",\"dataRichiesta\":\"2020-02-19T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwBHUAY\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA ABANO TERME 34611 - 00135 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":true}");
        
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - PRIMA ATTIVAZIONE RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
