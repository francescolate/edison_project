/*HerokuAPICoperturaCivico_SC_290
** Chiamata API CoperturaCivico - KO No Token - WEB - RES
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaCivico;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPICoperturaCivico_SC_290 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("Authorization", "Y");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("TOKEN","");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/order-fibra/v1/coperturacivico?civico=1&idstrada=38000079839");
		prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"KO\",\"code\":\"-1\",\"description\":\"No data retrieved\"}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
	 * API call for Tokens
     * Chiamata API CoperturaCivico - KO No Token - WEB - RES					
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaCivico.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
