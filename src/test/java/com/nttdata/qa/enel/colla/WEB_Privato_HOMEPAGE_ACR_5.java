package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.HOMEPAGE_ACR_5;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_HOMEPAGE_ACR_5 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "raf.faellaquom.o@gmail.com"); //r.32019p109statocontiweb@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");
	    prop.setProperty("NUMEROCLIENTE", "889918382");
		prop.setProperty("FORNITURAFIELD", "Ridotta");
		prop.setProperty("STATOPAGAMENTO", "Non Regolare");
		prop.setProperty("STATO_PAGAMENTO", "Da Pagare");
		prop.setProperty("TIPO_DI_DOCUMENTO", "Bolletta");
		prop.setProperty("ANNO", "2020 2019 2018");
		prop.setProperty("FORNITURA", "Luce - VIA GIUSEPPE GARIBALDI 242 - 57122 LIVORNO LI"); //Luce - VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT Gas - VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		HOMEPAGE_ACR_5.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
	    InputStream in = new FileInputStream(nomeScenario);
	     prop.load(in);
	     this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	     ReportUtility.reportToServer(this.prop);
		
	};

}
