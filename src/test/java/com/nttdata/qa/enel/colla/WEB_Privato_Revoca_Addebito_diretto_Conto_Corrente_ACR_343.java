package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.CancelItem;
import com.nttdata.qa.enel.testqantt.CancelPOD;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_343_ACR_Addebito_Diretto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
public class WEB_Privato_Revoca_Addebito_diretto_Conto_Corrente_ACR_343 {
	
	Properties prop;
	final String nomeScenario="WEB_Privato_Revoca_Addebito_diretto_Conto_Corrente_ACR_343.properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll2.enel.it/");
		prop.setProperty("WP_USERNAME", "yxavepydd-2370@yopmail.com");		
		prop.setProperty("WP_PASSWORD", "Password01");
		
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		
		prop.setProperty("IBAN", "IT58U0200803292000104571038");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("POD", "IT002E8969300A");
		prop.setProperty("CF", "CSSCLL90T41H501H");
		prop.setProperty("dtcmmind_URL", "http://10.151.55.144:8887/");
		prop.setProperty("COGNOME", "GIOVANNETTI");
		prop.setProperty("NOME", "GIULIANO");
		prop.setProperty("CODICE", "GVNGLN87A22H501D");
		
		prop.setProperty("INSERTIMOCOLOR", "Crimson");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_343_ACR_Addebito_Diretto.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CancelItem.main(args);
		//CancelPOD.main(args);
	
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
		
	};


}
