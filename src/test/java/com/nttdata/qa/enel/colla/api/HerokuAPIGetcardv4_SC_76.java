// HerokuAPIGetcardv4_SC_76
// Solo per WEB  - TOken in input expire 31/12/2021
/*"userUPN": "81542e4d-b642-41c8-8938-4087e6ce4207",
"cf": "SCHPRZ63S54L295F",
"enelId": "c616046a-7af9-4794-b8fa-cdd6bdd05650",
"userEmail": "raf.faellaquo.mo@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar SCHPRZ63S54L295F raf.faellaquo.mo@gmail.com 81542e4d-b642-41c8-8938-4087e6ce4207 c616046a-7af9-4794-b8fa-cdd6bdd05650 2021-12-31
*/
// OK - RES - Check supply status RIDOTTA

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv4_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIGetcardv4_SC_76 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	

		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","SCHPRZ63S54L295F");
		prop.setProperty("USERNAME","raf.faellaquo.mo@gmail.com");
		prop.setProperty("USERUPN","81542e4d-b642-41c8-8938-4087e6ce4207");
		prop.setProperty("ENELID","c616046a-7af9-4794-b8fa-cdd6bdd05650");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v4/getcards");
//		prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",      \"username\":\""+prop.getProperty("USERNAME")+"\",       \"keys\":[           {              \"key\":\"APP_VER\",             \"value\":\"9.0.1.0\"          },          {              \"key\":\"OS\",             \"value\":\"IOS\"          },          {              \"key\":\"OS_VER\",             \"value\":\"10.2.1\"          },          {              \"key\":\"DEVICE\",             \"value\":\"iPhone5\"          },          {              \"key\":\"ID_DISPOSITIVO\",             \"value\":\"ABAB288B-1C8D-4C88-95FE-2E9966AE32B3\"          }       ]    } }");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("JSON_DATA_OUTPUT", "\"offertaAttiva\":\"ENERGIAPURA CASA\",\"stato\":\"Ridotta\",\"isSubscription\":false,\"alias\":\"Luce\",\"isOreFree\":false,\"isInfoEnelEnergiaActivated\":\"non attivo\",\"rvc\":false,\"oreFree\":false,\"isBollettaWebActivated\":\"non attivo\",\"dataInizioCalendario\":");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check supply status RIDOTTA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv4_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
