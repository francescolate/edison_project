package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.testqantt.DataConfiguration;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.ActivateInfoEE_Res;
import com.nttdata.qa.enel.testqantt.colla.Add_Credentials;
import com.nttdata.qa.enel.testqantt.colla.Add_Query_17_Credentials;
import com.nttdata.qa.enel.testqantt.colla.SfdcVerifyInfoEEActivation;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_INFO_ENEL_ENERGIA_ACR_17 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
	//	prop.setProperty("WP_USERNAME", "silvia.test77@gmail.com");
	//  prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com"); //Error as RES
		prop.setProperty("WP_USERNAME", "dan04@yopmail.com"); //RES
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("CF", "RCCFNN41S16A258Q");
		prop.setProperty("CLIENT_NUMBER", "");
		
		prop.setProperty("MAIL_CERTIFIED", "");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("EMAIL_SUBJECT", "Conferma Indirizzo Email");
		prop.setProperty("VERIFICATION_STRING", "Enel Energia ti semplifica la vita con Info Enel Energia");
		prop.setProperty("CERTIFICATION_LINK", "");
		
		prop.setProperty("LINK", "");
		prop.setProperty("USERNAME", "");
		prop.setProperty("PASSWORD", "");
		
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		
		prop.setProperty("QUERY", "");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);
		Add_Query_17_Credentials.main(args);
		DataConfiguration.main(args);
		ActivateInfoEE_Res.main(args);
		Add_Credentials.main(args);
		DataConfiguration.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args);
		//SfdcVerifyInfoEEActivation.main(args);
		//RecuperaDatiWorkbench.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
