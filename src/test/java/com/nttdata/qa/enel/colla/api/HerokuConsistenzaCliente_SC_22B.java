package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_22B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"\",   \"piva\": \"\",   \"podPdr\": \"IT002E5647767A\",   \"numeroutente\": \"\",   \"uniqueid\": \"\"  } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"310509096\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA NIZZA 152 - 00198 ROMA RM\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"N\",\"podPdr\":\"IT002E5647767A\",\"usoFornitura\":\"Uso Diverso da Abitazione\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2020-06-10T00:00:00.000Z\",\"dataAttivazioneProdotto\":null,\"prodotto\":\"GiustaXTe Impresa\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API per verifica TAG di output
- prodotto
- Piano tariffario 
- dataAttivazioneProdotto
se e Solo se statoServizio = ATTIVO
CLIENTE  BSN con offerta GIUSTAXTE
	 * @throws Exception
	 */	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
