package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_279_ACR;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Info_Enel_Energia_ACR_279 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("LINK", BASE_LINK);
		prop.setProperty("USERNAME", "r.3.2.0.19p109statocontiweb@gmail.com");	//r32019p109statocontiwe.b@gmail.com
		prop.setProperty("CONFERMAEMAIL", "r.3.2.0.19p109statocontiweb@gmail.com"); //r32019p109statocontiwe.b@gmail.com
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("CELLULARE", "3895114091");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Privato_Info_Enel_Energia_279_ACR.main(args);	
		
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
	

}
