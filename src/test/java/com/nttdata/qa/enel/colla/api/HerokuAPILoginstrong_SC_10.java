//HerokuAPILoginstrong_SC_10
// Solo per APP , senza autorization e Token in input 
// KO mandatory Field osVer

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPILoginstrong_2;
import com.nttdata.qa.enel.util.ReportUtility;
import java.util.UUID;

public class HerokuAPILoginstrong_SC_10 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/authentication/v2/loginstrong");
	    prop.setProperty("USERNAME","r32019p109statocontiweb@gmail.com");
	    prop.setProperty("PASSWORD","Password01");
	    prop.setProperty("OSVER","");
	    prop.setProperty("IDDISPOSITIVO","MWS0216808002415");
	    prop.setProperty("DEVICE","HUAWEI EVA-L09");
	    prop.setProperty("CONTEXT","app_ee");
	    prop.setProperty("RESOLUTION","1080x1794");
	    //prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",         \"username\": \"r32019p109statocontiweb@gmail.com\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     },     \"data\": {         \"username\": \"r32019p109statocontiweb@gmail.com\",         \"password\": \"Password01\",         \"migrationToken\": \"\",         \"oidcToken\": \"\",         \"pwd_crypt\": \"\",         \"resolution\": \"1080x1794\",         \"osVer\": \"\",         \"idDispositivo\": \"MWS0216808002415\",         \"appVer\": \"10.0.1\",         \"os\": \"ANDROID\",         \"context\": \"app_ee\",         \"device\": \"HUAWEI EVA-L09\"     } }");
//	    prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",         \"username\": \""+ prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     },     \"data\": {         \"username\": \""+ prop.getProperty("USERNAME") + "\",         \"password\": \""+ prop.getProperty("PASSWORD") + "\",         \"migrationToken\": \"\",         \"oidcToken\": \"\",         \"pwd_crypt\": \"\",         \"resolution\": \"1080x1794\",         \"osVer\": \"\",         \"idDispositivo\": \"MWS0216808002415\",         \"appVer\": \"10.0.1\",         \"os\": \"ANDROID\",         \"context\": \"app_ee\",         \"device\": \"HUAWEI EVA-L09\"     } }");
//	    prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",   \"sid\": \""+ prop.getProperty("SID") + "\",         \"username\": \""+ prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \""+prop.getProperty("IDDISPOSITIVO") + "\"             }         ]     },     \"data\": {         \"username\": \""+ prop.getProperty("USERNAME") + "\",         \"password\": \""+ prop.getProperty("PASSWORD") + "\",         \"migrationToken\": \"\",         \"oidcToken\": \"\",         \"pwd_crypt\": \"\",         \"resolution\": \""+prop.getProperty("RESOLUTION") + "\",         \"osVer\": \""+prop.getProperty("OSVER") + "\",         \"idDispositivo\": \""+prop.getProperty("IDDISPOSITIVO") + "\",         \"appVer\": \"10.0.1\",         \"os\": \"ANDROID\",         \"context\": \""+prop.getProperty("CONTEXT") + "\",         \"device\": \""+prop.getProperty("DEVICE") + "\"     } }");
	    prop.setProperty("JSON_DATA_OUTPUT", "{\"result\":\"GEN01\",\"code\":\"-1\",\"description\":\"Servizio temporaneamente non disponibile, si prega di riprovare piu' tardi\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO mandatory Field osVer
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPILoginstrong_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
