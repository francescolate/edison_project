package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_75_Modulo_Reclami;
import com.nttdata.qa.enel.testqantt.colla.LaunchModuloReclami;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent_75;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_ModuloReclami_75 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("NOME", "ALBUS");
	    prop.setProperty("COGNOME", "SILENTE");
	    prop.setProperty("CELLULARE", "7874637282");
	    prop.setProperty("TIME", "8:00 - 12:00");
	    prop.setProperty("CF", "SLNLBS68M28H501Y");
	    prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
	    prop.setProperty("NUMEROCLIENTE", "310521194");
	    prop.setProperty("POD", "IT002E8877878A");
	    prop.setProperty("ADDRESS", "VIA VAI 2");
	    prop.setProperty("DESCRIZIONE", "PROVA TEST AUTOMATION AREA PUBBLICA ID 76");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");		
		prop.setProperty("AUTOLETTURA", "12345");
		prop.setProperty("DATE", "01/02/2021");
			
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LaunchModuloReclami.main(args);
		ID_75_Modulo_Reclami.main(args);
		//VerifyEmailSubjectAndContent_75.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
