package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_329_ACR_Addebito_Diretto;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_89_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_90_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_92_Form_Scrivici;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_FormScrivici_89 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
	
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("SUPPORTOLINK", "https://www-coll1.enel.it/it/supporto/faq/contattaci");
		prop.setProperty("QUI_LINK", "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON");
		prop.setProperty("NOME_DROPDOWN", "ALTROCONSUMO");
		prop.setProperty("CFD", "MCRVCN78C22D662D");
		prop.setProperty("CFD1", "1234");
		prop.setProperty("CFD2", "90049060594");
		prop.setProperty("ND", "VINCENZO MACARO");
		prop.setProperty("EMAIL", "testingcrmautomation@gmail.com");
		prop.setProperty("EMAIL1", "@@");
		prop.setProperty("TELEPHONO", "3434343434");
		prop.setProperty("TELEPHONO1", "abc");
		prop.setProperty("NOME", "ALBUS");
		prop.setProperty("COGNOME", "SILENTE");
		prop.setProperty("CF", "SLNLBS68M28H501Y");
		prop.setProperty("CF1", "SLNLBS68M28H501Y");
		prop.setProperty("ARGOMENTO", "Rateizza la tua bolletta");
		prop.setProperty("MESSAGIO", "PROVA TEST AUTOMATION ID 92");
		
		prop.setProperty("LUCE_E_GAS", "Luce e gas");
		prop.setProperty("IMPRESA", "Imprese");
		prop.setProperty("INSIEME A TE", "Insieme a te");
		prop.setProperty("STORIE", "Storie");
		prop.setProperty("FUTUR-E", "Futur-e");
		prop.setProperty("MEDIA", "Media");
		prop.setProperty("SUPPORTO", "Supporto");
		prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
		prop.setProperty("CREDITS", "Credits");
		prop.setProperty("PRIVACY", "Privacy");
		prop.setProperty("COOKIE_POLICY", "Cookie Policy");
		prop.setProperty("REMIT", "Remit");
		prop.setProperty("PREFERENZE_COOKIE", "Preferenze Cookie");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Publicco_ID_89_Form_Scrivici.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}



