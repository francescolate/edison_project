package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID322;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Revoca_Bolletta_WEB_ACR_322 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.3.2.0.19.p109statocontiweb@gmail.com");//collaudoloyaltycolazzo.2.020@gmail.com //collaudoloyalty.colazzo2.020@gmail.com
	//	prop.setProperty("WP_USERNAME", "col.laudoloyaltycolazzo.2020@gmail.com"); //SQTMNL75E46C352Y
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    	    		
		
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	Login_Private_Area.main(args);
		LoginEnel.main(args);
		Privato_Bolletta_Web_ID322.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}




















