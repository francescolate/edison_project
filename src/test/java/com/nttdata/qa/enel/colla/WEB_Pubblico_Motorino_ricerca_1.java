package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.CaratteristicheDettaglioLuce30Spring;
import com.nttdata.qa.enel.testqantt.colla.DocumentiLuce30Spring;
import com.nttdata.qa.enel.testqantt.colla.FinePaginaLuce30Spring;
import com.nttdata.qa.enel.testqantt.colla.FooterPage;
import com.nttdata.qa.enel.testqantt.colla.HeaderLogin;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.PLACETFissaLuceConsumer;
import com.nttdata.qa.enel.testqantt.colla.HeaderOffertaLuce30Spring;
import com.nttdata.qa.enel.testqantt.colla.Swa_Res_Ele;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Pubblico_Motorino_ricerca_1 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	    prop.setProperty("LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("LINK_PROMO_LUCE", "https://www-coll1.enel.it/it/luce-e-gas/luce/casa?nec=non_specificato&sortType=in-promozione");
	    prop.setProperty("LINK_LUCE_CASA_TUTTE", "https://www-coll1.enel.it/it/luce-e-gas/luce/casa");
	    prop.setProperty("LINK_PLACET_FISSA_LUCE", "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/placet-consumer-luce-fissa");
	    prop.setProperty("LINK_GIUSTAPERTE_BIORARIA", "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/giustaperte-bioraria");
	    prop.setProperty("TYPE_OF_CONTRACT", "Luce");
		prop.setProperty("PLACE", "Casa");
		prop.setProperty("NEED", "CAMBIO FORNITORE");
		prop.setProperty("NEED_PRECEDENTE", "Cambio Fornitore");
		prop.setProperty("ORDINA_PER_1", "In Promozione");
		prop.setProperty("ORDINA_PER_2", "Monoraria");
		prop.setProperty("ORDINA_PER_3", "Bioraria");
		//--FooterPage--
		prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Italia S.p.a.");
	    prop.setProperty("AFTER_FOOTER_TEXT", "© Enel Energia S.p.a. - Gruppo Iva P.IVA 12345678901");
	    prop.setProperty("SEQUENZA_OFFERTE_BIO", "Ore Free;GiustaPerTe Bioraria;E-Light Bioraria;Enel One;Scegli Tu 100x100;Enel Energia PLACET Variabile Luce Consumer;Enel Energia PLACET Fissa Luce Consumer;Pagina TEST Luce 60;Remote control;Open Energy Green;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light;Valore Luce Plus;Luce 30 Spring;GiustaPerTe;EnergiaX65");

	 	prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	    
	};
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		
		Login_Public_Area.main(args);
		Swa_Res_Ele.main(args);
		//HeaderOffertaLuce30Spring.main(args);
		//HeaderLogin.main(args);
		//FooterPage.main(args);
		//CaratteristicheDettaglioLuce30Spring.main(args);
	    //DocumentiLuce30Spring.main(args);
		//FinePaginaLuce30Spring.main(args);
		//PLACETFissaLuceConsumer.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);	

	};
	
}
