package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_24B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"\",   \"piva\": \"\",   \"podPdr\": \"IT001E19391741\",   \"numeroutente\": \"\",   \"uniqueid\": \"\"  } }");
		//prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"783390834\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA PICOCO 38 - 27010 BASCAPE' PV\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"Y\",\"podPdr\":\"IT001E19391741\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"Bonifico\",\"dataAttivazioneFornitura\":\"2015-02-28T23:00:00.000Z\",\"dataAttivazioneProdotto\":null,\"prodotto\":\"SEMPLICE LUCE\"}]}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"783390834\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA ADA NEGRI 1 - 88900 CROTONE KR\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"Y\",\"podPdr\":\"IT001E19391741\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"Bonifico\",\"dataAttivazioneFornitura\":\"2015-02-28T23:00:00.000Z\",\"dataAttivazioneProdotto\":null,\"prodotto\":\"SEMPLICE LUCE\"}]}");
		
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API per verifica TAG di output
- metodoDiPagamento
se e Solo se statoServizio = ATTIVO
Cliente RES con Bonifico
	 * @throws Exception
	 */	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
