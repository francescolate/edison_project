package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_91_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_95_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_96_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent_96;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_FormScrivici_96 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
	
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("SUPPORTOLINK", "https://www-coll1.enel.it/it/supporto/faq/contattaci");
		prop.setProperty("QUI_LINK", "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON");
		prop.setProperty("NOME_DROPDOWN", "ALTROCONSUMO");
		prop.setProperty("CFD", "MCRVCN78C22D662D");
		prop.setProperty("ND", "VINCENZO MACARO");
		prop.setProperty("EMAIL", "testingcrmautomation@gmail.com");
		prop.setProperty("TELEPHONO", "3434343434");
		prop.setProperty("NOME", "ALBUS");
		prop.setProperty("COGNOME", "SILENTE");
		prop.setProperty("CF", "SLNLBS68M28H501Y");
		prop.setProperty("ARGOMENTO", "Attiva Un Nuovo Contratto");
		prop.setProperty("MESSAGGIO", "PROVA TEST AUTOMATION ID 96");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Publicco_ID_96_Form_Scrivici.main(args);
		VerifyEmailSubjectAndContent_96.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
