package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ACR_346_Novita_Non_Iscritto_EPW;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Sezione_Novità_ACR_346 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r3.2019p109statocontiweb@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);
		ACR_346_Novita_Non_Iscritto_EPW.main(args);
};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
