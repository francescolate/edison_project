// HerokuAPIAccount_SC_64
// Solo per WEB  - TOken in input expire 31/12/2020
/*"userUPN": "5318676e-3037-4eeb-afaf-0a069a0dc6a6",
**"cf": "NCLGPP87E48F112P",
**"enelId": "18003079-add1-42de-b644-a3be38364804",
**"userEmail": "r.32.019p109statocontiweb@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar NCLGPP87E48F112P r.32.019p109statocontiweb@gmail.com 5318676e-3037-4eeb-afaf-0a069a0dc6a6 18003079-add1-42de-b644-a3be38364804 2021-12-31
*/
// OK - Canale WEB - RES

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccount_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIAccount_SC_64 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","NCLGPP87E48F112P");
		prop.setProperty("USERNAME","r.32.019p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","5318676e-3037-4eeb-afaf-0a069a0dc6a6");
		prop.setProperty("ENELID","18003079-add1-42de-b644-a3be38364804");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
     	//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiNTMxODY3NmUtMzAzNy00ZWViLWFmYWYtMGEwNjlhMGRjNmE2IiwiY29kaWNlZmlzY2FsZSI6Ik5DTEdQUDg3RTQ4RjExMlAiLCJpc3MiOiJhcHAiLCJleHAiOjE2NDM2Mzk3MzksImVuZWxpZCI6IjE4MDAzMDc5LWFkZDEtNDJkZS1iNjQ0LWEzYmUzODM2NDgwNCIsInVzZXJpZCI6InIuMzIuMDE5cDEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiYzdiMTNhMmQtOWM4MS00NjZhLWFkNTAtNjYyNGY0MWE3NTgxIn0.xHGo2HQbD8OGhm0naO4ZvlYue7x_UJoF37N-F8TGlb4");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/authentication/v2/account");
		prop.setProperty("JSON_INPUT", "{   \"data\":{      \"enelId\":\"" + prop.getProperty("ENELID") + "\"  }}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"userUPN\":\"5318676e-3037-4eeb-afaf-0a069a0dc6a6\",\"cf\":\"NCLGPP87E48F112P\",\"countryCf\":\"IT\",\"phoneNumberSkipped\":false}");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     *  OK - Canale WEB - RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccount_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
