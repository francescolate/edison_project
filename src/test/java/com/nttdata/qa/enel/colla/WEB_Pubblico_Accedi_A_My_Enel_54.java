package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.ContinueChangePassword_42;
import com.nttdata.qa.enel.testqantt.colla.Privato_ChangePassword_42;
import com.nttdata.qa.enel.testqantt.colla.Recupera_Password_BSN_54;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Accedi_A_My_Enel_54 {
	
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
							   
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "testing.crm.automation@gmail.com");
		//prop.setProperty("USERNAME", "m.ob.ile.te.sti.ng.auto.ma.t.ion@gmail.com");	
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("INVALIDUSER", "ABJ@@@");
		prop.setProperty("INVALIDPASSWD", "123");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		//prop.setProperty("EMAIL", "m.ob.ile.te.sti.ng.auto.ma.t.ion@gmail.com");
		prop.setProperty("NUOVA_PASSWD", "Password01");
		prop.setProperty("NUOVACONFERMA_PASSWD", "Password01");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Recupera_Password_BSN_54.main(args);
		SetUtenzaMailResidenziale.main(args);
		Privato_ChangePassword_42.main(args);
		ContinueChangePassword_42.main(args);
		
		
		
	};
	
	@After
	 public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

}
