package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.ID_66_Spazio_Enel;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PUBBLICO_TROVA_SPAZIO_ENEL_66 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
				
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");		
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		ID_66_Spazio_Enel.main(args);
	};
	
	@After
	  public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}








