package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Operatore_TLM_Pubblico_50;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Cruscotto_Di_Telemarketing_Delle_Offerte_50 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/it/login-telemarketing.html";
	
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/it/login-telemarketing.html");
		prop.setProperty("EMAIL_LABEL", "Email");
		prop.setProperty("USERNAME", "stefano.lombardi3@enel.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("INVALIDUSER", "ABJ@@@");
		prop.setProperty("INVALIDPASSWORD", "Invalid");
		prop.setProperty("TELEURL", "https://www-coll1.enel.it/ool/telemarketing/telemarketing.jsp");
		prop.setProperty("PODINVALIDINPUT", "AAAAA");
		prop.setProperty("PODINPUT", "IT002E9473893A");
		prop.setProperty("COMMUNE", "ROMA");
		prop.setProperty("INDRIZZO", "VIA IGNAZIO VIAN");
		prop.setProperty("NUMERO", "3");
					
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Operatore_TLM_Pubblico_50.main(args);
		
	};
	
	@After
	  public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);	
		};


	
	
}
