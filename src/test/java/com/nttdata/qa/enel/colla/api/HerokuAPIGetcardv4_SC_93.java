// HerokuAPIGetcardv4_SC_93
// Solo per WEB  - TOken in input expire 31/12/2021
/*"userUPN": "EE0000018072@BOX.ENEL.COM",
"cf": "FRTFRC85B18G388N",
"enelId": "e884e754-3add-4cdb-9ad4-73fa005eb55e",
"userEmail": "annamaria.cecili@enelsaa3010.enel.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar FRTFRC85B18G388N annamaria.cecili@enelsaa3010.enel.com EE0000018072@BOX.ENEL.COM e884e754-3add-4cdb-9ad4-73fa005eb55e 2021-12-31
**/
// OK - RES - No Channel

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.jwt.gen.*;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv4_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetcardv4_SC_93 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","FRTFRC85B18G388N");
		prop.setProperty("USERNAME","annamaria.cecili@enelsaa3010.enel.com");
		prop.setProperty("USERUPN","EE0000018072@BOX.ENEL.COM");
		prop.setProperty("ENELID","e884e754-3add-4cdb-9ad4-73fa005eb55e");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("CHANNELKEY","");
		prop.setProperty("SOURCECHANNEL","WEB");
		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiRUUwMDAwMDE4MDcyQEJPWC5FTkVMLkNPTSIsImNvZGljZWZpc2NhbGUiOiJGUlRGUkM4NUIxOEczODhOIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjQzNjI2NjE5LCJlbmVsaWQiOiJlODg0ZTc1NC0zYWRkLTRjZGItOWFkNC03M2ZhMDA1ZWI1NWUiLCJ1c2VyaWQiOiJhbm5hbWFyaWEuY2VjaWxpQGVuZWxzYWEzMDEwLmVuZWwuY29tIiwianRpIjoiMDM1YTJiYzAtNmQxMy00MDE1LWJjYTQtODhiYzExOWU0MzVkIn0.guNG-U0aOkB5EMffpBbqq8OhnBLPVmUHUsopj5IPHfg");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v4/getcards");
//		prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",      \"username\":\""+prop.getProperty("USERUPN")+"\",       \"keys\":[           {              \"key\":\"APP_VER\",             \"value\":\"9.0.1.0\"          },          {              \"key\":\"OS\",             \"value\":\"IOS\"          },          {              \"key\":\"OS_VER\",             \"value\":\"10.2.1\"          },          {              \"key\":\"DEVICE\",             \"value\":\"iPhone5\"          },          {              \"key\":\"ID_DISPOSITIVO\",             \"value\":\"ABAB288B-1C8D-4C88-95FE-2E9966AE32B3\"          }       ]    } }");
		prop.setProperty("JSON_DATA_OUTPUT", "\"result\":\"AUT02\",\"code\":\"-1\",\"description\":\"Client non autorizzato\"");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - No Channel
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv4_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
