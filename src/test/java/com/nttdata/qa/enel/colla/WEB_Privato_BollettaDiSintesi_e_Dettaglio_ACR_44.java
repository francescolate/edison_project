package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.jwt.gen.JWToken;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaBolletteLuce;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Privato_BollettaDiSintesi_e_Dettaglio_ACR_44 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "raffaellaquomo@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("BOLLETTE_HEADER_TEXT", "Qui potrai visualizzare bollette e/o rate delle tue forniture.");
	    prop.setProperty("SUPPLY_ADDRESS", "VIA PASQUALE LEONE 4 - 73015 SALICE SALENTINO LE");
	    prop.setProperty("CUSTOMER_CODE", "843517154");
	    prop.setProperty("TIPOLOGIA_BOLLETTA_BUTTON", "SCEGLI BOLLETTA DI SINTESI");
	    prop.setProperty("ACCOUNT_TYPE", "");
   
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);
		PrivateAreaBolletteLuce.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
