/*HerokuAPIUserByEnelID_SC_51 
** KO - No EnelID - APP
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIUserByEnelID_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIUserByEnelID_SC_51 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
//		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/");// + prop.getProperty("ENELID"));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"path\":\"/user/v1/userbyenelid/\",\"error\":\"Not Found\",\"message\":\"Not Found\"");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - No EnelID - APP 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIUserByEnelID_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
