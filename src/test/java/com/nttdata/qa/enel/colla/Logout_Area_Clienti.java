package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.LogoutEnelColla;

public class Logout_Area_Clienti {
	    private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
		Properties prop;
		final String nomeScenario="Logout_Area_Clienti.properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			
			prop.setProperty("LINK", "https://www-colla.enel.it/");
			prop.setProperty("USERNAME", "r32019p109statocontiweb@gmail.com");
			prop.setProperty("PASSWORD", "Password01");
			prop.setProperty("RUN_LOCALLY","Y");
			prop.store(new FileOutputStream(nomeScenario), null);
		
		};
		
		@Test
        public void eseguiTest() throws Exception{
			String args[]= {nomeScenario};
			LoginEnel.main(args);
			LogoutEnelColla.main(args);
		};
		
		@After
        public void fineTest() throws Exception{
			
		};
		
		
		
		
		
	

}
