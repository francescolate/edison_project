package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration;
import com.nttdata.qa.enel.testqantt.colla.DeleteRegisteredAccount;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration;
import com.nttdata.qa.enel.testqantt.colla.ID_37_RegistrazioneRES;
import com.nttdata.qa.enel.testqantt.colla.ID_40_AccediGoogle;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_PUBBLICO_ACCEDI_A_MY_ENEL_37 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "CASA");
		
//		prop.setProperty("ACCOUNT_FIRSTNAME","FARJANA");
//		prop.setProperty("ACCOUNT_LASTNAME", "BEGAM");
//		prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());		

		
//		New strategy for registration - 2021-02-18
		prop.setProperty("ACCOUNT_FIRSTNAME", "ROSA");
		prop.setProperty("ACCOUNT_LASTNAME", "SALSA");
		prop.setProperty("CF", "SLSRSO85H45H501A");
		prop.setProperty("SUPPLY_NUMBER", "310603403");
		prop.setProperty("WP_USERNAME", "t.es.t.i.n.g.c.rm.au.t.o.m.a.t.i.o.n@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		//prop.setProperty("WP_USERNAME", "m.ob.ile.te.sti.ng.auto.ma.t.ion@gmail.com");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		//GetDataForRegistration.main(args);
		DeleteRegisteredAccount.main(args);
		ID_37_RegistrazioneRES.main(args);
		SetUtenzaMailResidenziale.main(args);
        RecuperaLinkCreazioneAccount.main(args);
        ContinueWithRegistration.main(args);
	};
	
	@After
	 public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

}


