package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.MTP_RES_132;
import com.nttdata.qa.enel.testqantt.colla.MTP_RES_133;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MPT_RES_133 {
	
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "raffaellaquomo@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("ADDRESS", "Via Pasquale Leone 4 73015 Salice Salentino Salice Salentino Le");
		prop.setProperty("POD", "IT001E73277736");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("OLEODINAMICO", "Oleodinamico");
		prop.setProperty("OPERATING_CURRENT_INPUT", "20");
		prop.setProperty("ARGANO", "Argano");
		prop.setProperty("SUGGESTED_POWER", "4.5");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		MTP_RES_133.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
