package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.LoginEnelLikeRES;
import com.nttdata.qa.enel.testqantt.colla.LoginEnelNew;
import com.nttdata.qa.enel.testqantt.colla.ModificaIndirizzoFattuazione_81;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenuBSNNew;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenuLikeRES;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenuNew;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_ModificaIndirizzoFattuazione_ACB_81 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.32019p109.statocontiweb@gmail.com");
	    //prop.setProperty("WP_USERNAME", "collaudoloyaltycolaz.zo202.0@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("CLIENT_NUMBER", "310504613");
	    prop.setProperty("AREA_CLIENTI", "IMPRESA");
	    	    	    	   	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		//LoginEnelLikeRES.main(args);
		LoginEnel.main(args);
//		PrivateAreaMenuBSNNew.main(args);
		ModificaIndirizzoFattuazione_81.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}



