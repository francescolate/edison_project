/* HerokuAPICoperturaIndirizzo_SC_221 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RICONTRATTUALIZZAZIONE - Annullata

"userUPN": "cfebb068-6eb5-4108-af1e-ec93c764ba34",
"cf": "PPLLGU63M24E023Z",
"enelId": "0d009471-5a94-4e29-9ff1-97be10da0507",
"userEmail": "r.32019p109stato.contiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_221 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","PPLLGU63M24E023Z");
		prop.setProperty("USERNAME","r.32019p109stato.contiweb@gmail.com");
		prop.setProperty("USERUPN","cfebb068-6eb5-4108-af1e-ec93c764ba34");
		prop.setProperty("ENELID","0d009471-5a94-4e29-9ff1-97be10da0507");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Annullamento da Operatore\",\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E40794726\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3114897\",\"flagStd\":0,\"namingInFattura\":\"BIORARIA\",\"statoR2d\":null,\"numeroUtente\":\"823604484\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001l0000048j62AAA\",\"idListino\":\"a1l1l000000OPnFAAW\",\"dataAttivazione\":\"2014-05-31T22:00:00.000Z\",\"idCaseItem\":\"a381l000000BxO2AAK\",\"dataRichiesta\":\"2020-05-06T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwBHUAY\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA NIZZA 152 - 00198 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Annullamento da Operatore\",\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E40794726\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3114897\",\"flagStd\":0,\"namingInFattura\":\"BIORARIA\",\"statoR2d\":null,\"numeroUtente\":\"823604484\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001l0000048j62AAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OPnFAAW\",\"dataAttivazione\":\"2014-05-31T22:00:00.000Z\",\"idCaseItem\":\"a381l000000BxO2AAK\",\"dataRichiesta\":\"2020-05-06T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwBHUAY\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA NIZZA 152 - 00198 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
       

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RICONTRATTUALIZZAZIONE - Annullata
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
