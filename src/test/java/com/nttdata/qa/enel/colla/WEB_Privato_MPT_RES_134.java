package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.GetAccountEmail;
import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyRicheste;
import com.nttdata.qa.enel.testqantt.colla.ChangePowerAndVoltage;
import com.nttdata.qa.enel.testqantt.colla.ChangePowerAndVoltagePreventivo;

public class WEB_Privato_MPT_RES_134 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "raffaellaquomo@gmail.com");//PTRSLL45R62F101X
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
	    prop.setProperty("CF", "PTRSLL45R62F101X");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("ADDRESS", "Via Pasquale Leone 4 73015 Salice Salentino Salice Salentino Le");
		prop.setProperty("POD", "IT001E73277736");
		prop.setProperty("PVVALUE", "3 kW / 220 V");
		prop.setProperty("POWER_INPUT", "3.5 kW");
		prop.setProperty("TENZION", "220 V (tensione attuale)");
		prop.setProperty("CELLULAR", "3333323231");
	    prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
		prop.setProperty("COSTOFLABOUR_VALUE", "87,35 €+ iva*");
		prop.setProperty("COSTOFLABOUR_VALUE_2", "53,87 €+ iva*");													//Marco
		prop.setProperty("STATO_ER", "PREVENTIVO INVIATO");
		prop.setProperty("NUMERO_UTENZA", "843517154");
		prop.setProperty("STATO_R2D", "DA COMPLETARE");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("STATO_SDSAP", "OK");
		prop.setProperty("STATO_UDB", "NON PREVISTO");

		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		ChangePowerAndVoltage.main(args);
		ChangePowerAndVoltagePreventivo.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args);
		//GetAccountFromCF.main(args);
		//VerifyRicheste.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
