/*HerokuAPIRegistration_SC_261B
* API call for Tokens - RES
* API call Registration with RES customer, Active Supply.
* Wrong value with Input parameter PersonalID in the case of BSN, businessID in the case of RES
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRegistration_2;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRegistration_4;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIRegistration_SC_261B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account");
		prop.setProperty("FIRSTNAME","NOEMI");
		prop.setProperty("LASTNAME","LUNA");
		prop.setProperty("CODICEFISCALE","LNUNMO80H41F839K");
		prop.setProperty("EMAIL","r32019p109statocontiw.eb@gmail.com");
        prop.setProperty("PHONENUMBER","+393312206205");
		//prop.setProperty("JSON_INPUT", "{     \"data\": {         \"email\": \"" + prop.getProperty("email") + "\",         \"phoneNumber\": \"" + prop.getProperty("phoneNumber") + "\",         \"password\": \"Password01\",         \"firstName\": \"" + prop.getProperty("firstName") + "\",         \"lastName\": \"" + prop.getProperty("lastName") + "\",         \"countryCode\": \"IT\",         \"companyName\": \"\",         \"vatCode\": \"\",         \"attributes\": [             {                 \"country\": \"IT\",                 \"claims\": [                     {                         \"name\": \"businessId\",                         \"value\": \"" + prop.getProperty("CodiceFiscale") + "\"                     }                 ]             }         ],         \"consents\": [             {                 \"id\": \"11\",                 \"group\": \"12\",                 \"language\": \"it_IT\",                 \"channel\": \"ENEL_ENERGIA\",                 \"given\": true             },             {                 \"id\": \"10\",                 \"group\": \"12\",                 \"language\": \"it_IT\",                 \"channel\": \"ENEL_ENERGIA\",                 \"given\": true             }         ],         \"metadata\": {             \"language\": \"it_IT\",             \"processChannel\": \"ENEL_ENERGIA\"         }     } }");
        prop.setProperty("JSON_INPUT", "{     \"email\": \"" + prop.getProperty("EMAIL") + "\",     \"phoneNumber\": \"" + prop.getProperty("PHONENUMBER") + "\",     \"password\": \"Password01\",     \"firstName\": \"" + prop.getProperty("FIRSTNAME") + "\",     \"lastName\": \"" + prop.getProperty("LASTNAME") + "\",     \"countryCode\": \"IT\",     \"companyName\":\"\",     \"vatCode\":\"\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"businessId\",                     \"value\": \"" + prop.getProperty("CODICEFISCALE") + "\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"attributes[0].claims[0].value\":\"Field 'attributes[0].claims[0].value' must be a valid businessId, '" + prop.getProperty("CODICEFISCALE") + "' given\"}");		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens - RES
	 * API call Registration with RES customer, Active Supply.
	 * Wrong value with Input parameter PersonalID in the case of BSN, businessID in the case of RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRegistration_4.main(args); // per gestire errore ricevuto
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
