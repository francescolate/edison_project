package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_348_ACR_Tracking_RVC;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_PRIVATO_Tracking_RVC_ACR_348 {


	Properties  prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "colla3@yopmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SUPPLY_HEADING", "Luce");
		prop.setProperty("SUPPLY_DETAILS", "L’indirizzo della tua fornitura luce è: VIA ANGELO SECCHI 2 - 42021 BIBBIANO RE");
		prop.setProperty("NUMEROCLIENTE", "Numero Cliente:");
		prop.setProperty("NUMEROCLIENTE_NUMBER", "310488559");
		prop.setProperty("SUPPLY_STATUS_HEADING", "Stato Fornitura");
		prop.setProperty("SUPPLY_STATUS", "Attiva");
		prop.setProperty("SECTION_1_HEADING", "Cambio Offerta");
		prop.setProperty("SECTION_1_HEADING_SUBTEXT", "Visualizza lo stato di attivazione della nuova offerta");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_348_ACR_Tracking_RVC.main(args);
		
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}