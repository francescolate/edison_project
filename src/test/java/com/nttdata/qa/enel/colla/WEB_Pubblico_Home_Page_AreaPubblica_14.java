package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.CookiePolicy;
import com.nttdata.qa.enel.testqantt.colla.CookiePolicy_14;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Home_Page_AreaPubblica_14 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
//TODO Change colla to colla1 before commit
//	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("BANNER_DESCRIPTION", "Utilizziamo i cookie, anche di terze parti, per assicurarti la migliore esperienza possibile sul nostro sito e inviarti pubblicità e servizi in linea con le tue preferenze. Continuando a navigare acconsenti all’uso dei Cookie. Per maggiori informazioni o per modificare le impostazioni consulta la nostra Cookie Policy.");
		prop.setProperty("ENT_PAGE_TITLE", "Vicini alle piccole imprese con un mese gratis di Open Energy");
		prop.setProperty("BANNER_TITLE", "Cookie Policy");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		//Login_Public_Area.main(args);;
		//CookiePolicy.main(args);
		CookiePolicy_14.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
	
}
