package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.CheckHamburgerRightMenu;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.OpenHambugerAndCheckMenu;
import com.nttdata.qa.enel.testqantt.colla.ServiziOnlineCaricaDocumenti;
import com.nttdata.qa.enel.testqantt.colla.ServiziOnlineCaricaDocumentiWithData;
import com.nttdata.qa.enel.testqantt.colla.SupportoToContattaci;
import com.nttdata.qa.enel.testqantt.colla.SupportoToContattaciHeaderAndFooter;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PUBBLICO_FormScrivici_88 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("LINK", "https://www-coll1.enel.it");
	    prop.setProperty("LINK_CONTATTACI", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/it/supporto/faq/contattaci");
	    prop.setProperty("LINK_DOCUMENTI", "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON");
	    prop.setProperty("PATH_CONTATTACI", "Home/supporto/Come entrare in contatto con Enel Energia");
	    prop.setProperty("TITLE_CONTATTACI", "Tanti modi per entrare in contatto con noi");
	    prop.setProperty("SUBTITLE_CONTATTACI", "Enel Energia è sempre in contatto con te, se sei cliente e se vuoi diventarlo.");
	  
		prop.setProperty("LUCE_E_GAS", "Luce e gas");
		prop.setProperty("IMPRESA", "Imprese");
		prop.setProperty("INSIEME A TE", "Insieme a te");
		prop.setProperty("STORIE", "Storie");
		prop.setProperty("FUTUR-E", "Futur-e");
		prop.setProperty("MEDIA", "Media");
		prop.setProperty("SUPPORTO", "Supporto");
		prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
		prop.setProperty("CREDITS", "Credits");
		prop.setProperty("PRIVACY", "Privacy");
		prop.setProperty("COOKIE_POLICY", "Cookie Policy");
		prop.setProperty("REMIT", "Remit");
		prop.setProperty("PREFERENZE_COOKIE", "Preferenze Cookie");
	    
	    prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		SupportoToContattaci.main(args);
		SupportoToContattaciHeaderAndFooter.main(args);
		OpenHambugerAndCheckMenu.main(args);
		CheckHamburgerRightMenu.main(args);
	};
	
	@After
    public void fineTest() throws Exception{  
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
