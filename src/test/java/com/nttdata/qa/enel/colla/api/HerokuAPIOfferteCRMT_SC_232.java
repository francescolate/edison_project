/* HerokuAPICoperturaIndirizzo_SC_232 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - WEB - RES - ALLACCIO E ATTIVAZIONE - Espletato
"userUPN": "714e38b2-b7a3-4dcc-a5fa-7e5643e89a3b",
"cf": "SLNLBS68M28H501Y",
"enelId": "de8f7bc2-3c7d-4784-9009-00e58d97546c",
"userEmail": "r.32019p109statoconti.web@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_232 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","SLNLBS68M28H501Y");
		prop.setProperty("USERNAME","r.32019p109statoconti.web@gmail.com");
		prop.setProperty("USERUPN","714e38b2-b7a3-4dcc-a5fa-7e5643e89a3b");
		prop.setProperty("ENELID","de8f7bc2-3c7d-4784-9009-00e58d97546c");
		prop.setProperty("EXPIRATION","2021-12-31");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
////		prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwNDlmZTY5Mi0zOTJiLTQ0YTUtYmJlYi1kODBiNjg5ZGI4MzciLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlpNQkdGUjM4QzIxQTk0NFgiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJjMDI3Njc1ZS01M2QyLTQ5NDMtYTA5Ny0zZWQ0NWE5MmJlMDEiLCJleHAiOjE2MDc2OTE1OTcsInVzZXJpZCI6ImZyYW5jZXNjby5sdWNpYUBudHRkYXRhLmNvbSIsImp0aSI6ImUwN2Y0YzU5LWZiMWQtNDc3YS04OGRhLThhYTc4ZDUyM2Y3MCIsInNpZCI6IjAwMGQ3NTY3LTVmMjMtNDgyZC04MTllLTUxNjczNWUyYWRlYyJ9.wFN-dHREgRIoffPyK8I9ykWp9fzjEkICuwvlTx5ZXug");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT002E3009090A\",\"statoSap\":\"OK\",\"statoSempre\":\"IN ATTESA\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3124844\",\"flagStd\":1,\"namingInFattura\":\"Speciale Luce 360\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"310513349\",\"processoInCorso\":\"ALLACCIO E ATTIVAZIONE\",\"idCase\":\"5001l000004vODiAAM\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000P9yHAAS\",\"dataAttivazione\":\"2020-07-04T00:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000ZpFHEA0\",\"dataRichiesta\":\"2020-07-04T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000BmzREAS\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA NIZZA 152 - 00198 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - WEB - RES - ALLACCIO E ATTIVAZIONE - Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
