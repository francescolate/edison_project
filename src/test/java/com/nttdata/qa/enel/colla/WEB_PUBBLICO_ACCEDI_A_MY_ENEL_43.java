package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.ID_43_RecuperaUsername;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PUBBLICO_ACCEDI_A_MY_ENEL_43 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "r32019p109statocontiweb@gmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("FISCALE", "GRTMNG51S44B113C");
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		ID_43_RecuperaUsername.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);	
	};

}


