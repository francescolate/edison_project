package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.DataConfiguration;
import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.ChangeVoltageNoEstimate;
import com.nttdata.qa.enel.testqantt.colla.ChangeVoltageNoEstimateSFDCVerification;
import com.nttdata.qa.enel.testqantt.colla.ChangeVoltageNoEstimateSFDCVerification_Part2;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.ChangeVoltageGetMail;
import com.nttdata.qa.enel.testqantt.colla.ChangeVoltageNoEstimateR2D;

public class WEB_Privato_MPT_RES_135 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
	//	prop.setProperty("WP_USERNAME", "r.32019p.109statocontiweb@gmail.com");
		prop.setProperty("WP_USERNAME", "r.32.019p109statocontiweb@gmail.com");
		//r.32.019p109statocontiweb@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("CONTACT_EMAIL", "testing.crm.automation@gmail.com");
//		prop.setProperty("POD", "IT002E4614105A");
		prop.setProperty("POD", "");
		prop.setProperty("CF", "MGLNLS88C41B872B");
		prop.setProperty("NEW_VOLTAGE", "380 V");
		prop.setProperty("REFERENCE_DATE", "14/07/2020");
		prop.setProperty("DISTRIBUTOR_SHORT", "");
		prop.setProperty("DISTRIBUTOR", "");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Modifica Tensione");
		prop.setProperty("CODICE_RICHIESTA_CRM", "");
		
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("EMAIL_SUBJECT", "Enel Energia – Richiesta Modifica Tensione della fornitura");
		prop.setProperty("VERIFICATION_STRING", "abbiamo ricevuto la richiesta per la Modifica Tensione");
		
		prop.setProperty("LINK", "");
		prop.setProperty("USERNAME", "");
		prop.setProperty("PASSWORD", "");
		prop.setProperty("USER_R2D", "");
		prop.setProperty("PASSWORD_R2D", "");
		
		prop.setProperty("QUERY_PROPERTIES", "LINK;USERNAME;PASSWORD;USER_R2D;PASSWORD_R2D");
		prop.setProperty("QUERY_REFERENCES", "salesforceLink;utenza_admin_salesforce;password_admin_salesforce;utenza_r2d;password_r2d");
		
		prop.setProperty("ACCOUNT_TYPE", "RES");
		
		prop.setProperty("SUPPLYSERVICES", "");
		
		prop.setProperty("MODULE_ENABLED", "Y");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		DataConfiguration.main(args);
		LoginEnel.main(args);
		//PrivateAreaMenu.main(args);
		ChangeVoltageNoEstimate.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args);
		GetAccountFromCF.main(args);
		//ChangeVoltageNoEstimateSFDCVerification.main(args);
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		prop.setProperty("QUERY_PROPERTIES", "USERNAME;PASSWORD");
		prop.setProperty("QUERY_REFERENCES", "utenza_Tp2_crm;password_Tp2_crm");
		prop.store(new FileOutputStream(nomeScenario), null);
		DataConfiguration.main(args);
		//ChangeVoltageGetMail.main(args);
		LoginR2D_ELE.main(args);
		ChangeVoltageNoEstimateR2D.main(args);
		in = new FileInputStream(nomeScenario);
		prop.load(in);
		prop.setProperty("QUERY_PROPERTIES", "USERNAME;PASSWORD");
		prop.setProperty("QUERY_REFERENCES", "utenza_admin_salesforce;password_admin_salesforce");
		prop.store(new FileOutputStream(nomeScenario), null);
		DataConfiguration.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args);
		GetAccountFromCF.main(args);
		ChangeVoltageNoEstimateSFDCVerification_Part2.main(args);
		in = new FileInputStream(nomeScenario);
		prop.load(in);
		prop.setProperty("QUERY_PROPERTIES", "USERNAME;PASSWORD");
		prop.setProperty("QUERY_REFERENCES", "utenza_Tp2_crm;password_Tp2_crm");
		prop.store(new FileOutputStream(nomeScenario), null);
		DataConfiguration.main(args);
	};
	
	@After
	  public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
