package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenuSospesaDaPagare;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_DETTAGLIO_FORNITURA_ACR_9b {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "r32019p109statocontiweb@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("SUPPLY_ADDRESS", "VIA CHIESA 3 - 24010 BRACCA BG");
	    prop.setProperty("CUSTOMER_CODE", "855343208");
	    prop.setProperty("STATO_FORNITURA_FEILD", "Stato FornituraSospesa");
	    prop.setProperty("STATO_PAGAMENTO_FEILD", "Stato pagamentoDa pagare (9)");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "");
    	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		PrivateAreaMenu.main(args);
		PrivateAreaMenuSospesaDaPagare.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}

