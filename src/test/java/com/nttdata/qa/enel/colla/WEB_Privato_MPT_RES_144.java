package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.MTP_RES_144;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MPT_RES_144 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "r.aff.aellaquomo@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("ADDRESS", "Via Ostiense 45 00154 Roma Roma Rm");
		prop.setProperty("POD", "IT002E7489376A");
		prop.setProperty("ARGANO","Argano");
		prop.setProperty("RUNNINGCURRENTINPUT", "2");
		prop.setProperty("STARTINGCURRENTINPUT", "2");
		prop.setProperty("SUGGESTEDPOWER", "1.5");
		prop.setProperty("PVVALUE", "3 kW / 220 V");
		prop.setProperty("PLACEHOLDER", "Inserisci il valore in Ampere");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);		
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		MTP_RES_144.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
