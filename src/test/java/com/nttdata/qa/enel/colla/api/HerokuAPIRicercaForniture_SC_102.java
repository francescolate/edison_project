/*HerokuAPIRicercaForniture_SC_102 
** API call for Tokens
* RicercaForniture - OK - BSN At least one supply ELE in "Attiva" state
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 01533540447 r.32019p109statoc.ontiweb@gmail.com e66bc31c-44a5-4ed6-a76b-39a5808567da 998777cd-ccbc-45e8-b013-2578bc623fff 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.jwt.gen.JWToken;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica_CF_PIVA;
import com.nttdata.qa.enel.util.ReportUtility;
//import com.nttdata.jwt.gen.*;


public class HerokuAPIRicercaForniture_SC_102 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","01533540447");
		prop.setProperty("USERNAME","r.32019p109statoc.ontiweb@gmail.com");
		prop.setProperty("USERUPN","e66bc31c-44a5-4ed6-a76b-39a5808567da");
		prop.setProperty("ENELID","998777cd-ccbc-45e8-b013-2578bc623fff");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZTY2YmMzMWMtNDRhNS00ZWQ2LWE3NmItMzlhNTgwODU2N2RhIiwiY29kaWNlZmlzY2FsZSI6IjAxNTMzNTQwNDQ3IiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMTE1ODc1LCJlbmVsaWQiOiI5OTg3NzdjZC1jY2JjLTQ1ZTgtYjAxMy0yNTc4YmM2MjNmZmYiLCJ1c2VyaWQiOiJyLjMyMDE5cDEwOXN0YXRvYy5vbnRpd2ViQGdtYWlsLmNvbSIsImp0aSI6Ijc4ZWQ3ZDU0LThkNTUtNDY1Yy04OWM1LTQ4MTlhY2RjYjVmMyJ9.T4HO0o1oRIV0qTGO8Jm7OwSJkrhJL-5ggA6qv54sv08");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-extra-stage/v1/info/ricerca/forn?cf=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"fornituraSummaryList\":[{\"stato\":\"Attivo\",\"listinoInFattura\":\"Senza Orari Luce\",\"fornKey\":\"0010Y00000OiiyAQAR|02i0Y000004Y3ZOQA0|654995134\",\"codice\":\"654995134\",\"indirizzo\":\"VIA CALATAFIMI 86 - 63074 SAN BENEDETTO DEL TRONTO AP\",\"name\":\"654995134\",\"partitaIvaCliente\":\"01533540447\",\"tipoForn\":\"Elettrico\",\"flagDefault\":\"Y\",\"contrattoSAP\":\"0031771873\",\"bpAccountSAP\":\"0036920807\",\"contractAccountSAP\":\"001026135098\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * RicercaForniture - OK - BSN At least one supply ELE in "Attiva" state
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica_CF_PIVA.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
