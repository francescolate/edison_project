package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CancelItem;
import com.nttdata.qa.enel.testqantt.CancelPOD;
import com.nttdata.qa.enel.testqantt.CancelPOD_338;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyRequestDetails_338;
import com.nttdata.qa.enel.testqantt.colla.ACR_338_AddebitoContoCorrente;
import com.nttdata.qa.enel.testqantt.colla.GetUserDetailsFromWorkBench;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.testqantt.colla.VerifyemailSubject_338;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Addebito_diretto_Conto_Corrente_ACR_338 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "raffae.ll.aquomo@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "");
	    
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    
	    prop.setProperty("LINK", Costanti.salesforceLink);
	    prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
	    
	    prop.setProperty("POD", "01613459005869");
	    prop.setProperty("CF", "HRNMNK46H63Z112Q");
		prop.setProperty("EMAIL_INPUT", "testing.crm.automation@gmail.com");
		prop.setProperty("ACCOUNT_ID", "0010Y00000F8ifXQAR");
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("TIPOCLIENTE", "BUSINESS");		
		
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		GetUserDetailsFromWorkBench.main(args);
		VerifyLoginEnelArea.main(args);
		ACR_338_AddebitoContoCorrente.main(args);
		VerifyemailSubject_338.main(args);
		VerifyRequestDetails_338.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CancelPOD_338.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
