package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_ORE_FREE_60;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_ORE_FREE_DettaglioFornituraOreFree_CambioFascia_60 {
	
	private Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "r.3.2.0.19p109statoc.ontiweb@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");
	    prop.setProperty("TIME", "Ore Free: 11:00 - 14:00");
	    
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_ORE_FREE_60.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
