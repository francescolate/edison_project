package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACR_282;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailUpdateFromSF_282;
import com.nttdata.qa.enel.testqantt.colla.VerifyemailSubject_282;
import com.nttdata.qa.enel.testqantt.colla.VerifyemailSubject_338;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Info_Enel_Energia_ACR_282 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//prop.setProperty("WP_USERNAME", "mobiletesting.nttdata@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaudoloyaltyc.olazzo2.020@gmail.com"); 
		//prop.setProperty("WP_USERNAME", "collaudoloyaltycolazz.o202.0@gmail.com"); //CLZGRL86R14D883O
		//prop.setProperty("WP_USERNAME", "testi.ngcrma.u.t.o.m.a.t.i.on@gmail.com"); //MTTFNC33L21F890Y
		prop.setProperty("WP_USERNAME", "testin.gc.rmautomation@gmail.com"); //TMSLGU45B09A766B
		
		prop.setProperty("WP_PASSWORD", "Password01"); 
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", ""); 
		
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
	//	prop.setProperty("EMAIL","g.colazzo@lutech.it");
	//	prop.setProperty("CONFERMA_EMAIL","g.colazzo@lutech.it");
	//	prop.setProperty("EMAIL","m_angelacaponetto@tiscali.it");
	//	prop.setProperty("CONFERMA_EMAIL","m_angelacaponetto@tiscali.it");
		prop.setProperty("EMAIL","fabiana.monza@nttdata.com");
		prop.setProperty("CONFERMA_EMAIL","fabiana.monza@nttdata.com");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Info_Enel_Energia_ACR_282.main(args);
		LoginSalesForce.main(args);
        //SbloccaTab.main(args);
        //VerifyEmailUpdateFromSF_282.main(args);
        
		
      /*  String args[]= {nomeScenario};
		LoginSalesForce.main(args);*/
        
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
