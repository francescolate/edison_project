package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_21A {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"\",   \"piva\": \"\",   \"podPdr\": \"\",   \"numeroutente\": \"310509504\",   \"uniqueid\": \"\"  } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"310509504\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA NINO BIXIO 1 - 60027 OSIMO AN\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"N\",\"podPdr\":\"IT004E00000191\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"Bollettino Postale\",\"dataAttivazioneFornitura\":\"2020-07-01T00:00:00.000Z\",\"dataAttivazioneProdotto\":null,\"prodotto\":\"Scegli Tu Ore Free\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API per verifica TAG di output
- prodotto
- Piano tariffario 
- dataAttivazioneProdotto
se e Solo se statoServizio = ATTIVO
CLIENTE  RES con Scegi Tu
	 * @throws Exception
	 */	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
