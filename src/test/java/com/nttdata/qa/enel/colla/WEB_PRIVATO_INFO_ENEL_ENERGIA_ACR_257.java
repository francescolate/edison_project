package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_257_INFO_ENEL_ENERGIA_ACR;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_INFO_ENEL_ENERGIA_ACR_257 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "teleauto@yopmail.com");//collaudoloyalt.ycolazzo2.020@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SERVIZI_PER_LE_FORNITURE", "Servizi per le forniture");
		prop.setProperty("INFOENEL_ENERIGIA_BUTTON_STATUS", "Non Attivo");
		prop.setProperty("INFOENEL_ENERIGIA_HEADING", "InfoEnelEnergia");
		prop.setProperty("INFOENEL_ENERGIA_NON_E_ATTIVO_STATUS", "Il servizio InfoEnelEnergia non è attivo sulla fornitura:");
		prop.setProperty("ATTIVA_INFOENEL_ENERGIA_PAGETITLE", "Attiva InfoEnelEnergia");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_257_INFO_ENEL_ENERGIA_ACR.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}