/* HerokuAPICoperturaIndirizzo_SC_223 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - ALLACCIO E ATTIVAZIONE  - Annullata

"userUPN": "714e38b2-b7a3-4dcc-a5fa-7e5643e89a3b",
"cf": "SLNLBS68M28H501Y",
"enelId": "de8f7bc2-3c7d-4784-9009-00e58d97546c",
"userEmail": "r.32019p109statoconti.web@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_223 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","SLNLBS68M28H501Y");
		prop.setProperty("USERNAME","r.32019p109statoconti.web@gmail.com");
		prop.setProperty("USERUPN","714e38b2-b7a3-4dcc-a5fa-7e5643e89a3b");
		prop.setProperty("ENELID","de8f7bc2-3c7d-4784-9009-00e58d97546c");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Annullato da Operatore\",\"numeroOre\":null,\"statoCaseItem\":\"Annullata\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":null,\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3125108\",\"flagStd\":0,\"namingInFattura\":\"Speciale Luce 360\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"ALLACCIO E ATTIVAZIONE\",\"idCase\":\"5001l000004vTITAA2\",\"idListino\":\"a1l1l000000P9yHAAS\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000ZptTEAS\",\"dataRichiesta\":\"2020-07-06T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000BmzREAS\",\"commodityCaseItem\":null,\"indirizzoFornitura\":null,\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Annullato da Operatore\",\"numeroOre\":null,\"statoCaseItem\":\"Annullata\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":null,\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3125108\",\"flagStd\":0,\"namingInFattura\":\"Speciale Luce 360\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"ALLACCIO E ATTIVAZIONE\",\"idCase\":\"5001l000004vTITAA2\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000P9yHAAS\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000ZptTEAS\",\"dataRichiesta\":\"2020-07-06T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000BmzREAS\",\"commodityCaseItem\":null,\"indirizzoFornitura\":null,\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
       

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - ALLACCIO E ATTIVAZIONE  - Annullata
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
