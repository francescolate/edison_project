/* HerokuAPICoperturaComune_SC_166 
** Case OK - Search "AVERSA" - WEB 
* need token (cliente qualsiasi)
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaComuneIndirizzo_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPICoperturaComune_SC_166 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","PAXNDR93H23F205I");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));

		prop.setProperty("SEARCH","AVERSA");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJOT1NVQkpFQ1QiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlIjoiMTIzNDVGSVNDQUxfQ09ERSIsImlzcyI6ImFwcCIsImV4cCI6MTYwODgwMzY5MCwiZW5lbGlkIjoiMTIzNDUiLCJ1c2VyaWQiOiJOT1VTRVJJRCIsImp0aSI6ImJiN2YxMjIzLWE3NDEtNDMzZS05YmQ5LTU1MmJkZjUxMTM0ZiIsInNpZCI6ImYxYTFiZmY0LWVlMDEtNGU5My04MjdjLWZiMTEzZTRmZDE4YiJ9.2pug6-_b913G5P_RUzn081ix0T3u6OFiGe34_6umi3o");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/order-fibra/v1/coperturacomune?name="+ prop.getProperty("SEARCH"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"data\":{\"communes\":[{\"province\":{\"name\":\"Caserta\",\"id\":\"061\",\"region\":{\"name\":\"Campania\",\"id\":\"15\"}},\"cadastralCode\":\"A512\",\"name\":\"Aversa\",\"id\":\"61005\"},{\"province\":{\"name\":\"Bari\",\"id\":\"072\",\"region\":{\"name\":\"Puglia\",\"id\":\"16\"}},\"cadastralCode\":\"C975\",\"name\":\"Conversano\",\"id\":\"72019\"},{\"province\":{\"name\":\"Catanzaro\",\"id\":\"079\",\"region\":{\"name\":\"Calabria\",\"id\":\"18\"}},\"cadastralCode\":\"I671\",\"name\":\"Sersale\",\"id\":\"79130\"},{\"province\":{\"name\":\"Caserta\",\"id\":\"061\",\"region\":{\"name\":\"Campania\",\"id\":\"15\"}},\"cadastralCode\":\"E173\",\"name\":\"Gricignano di Aversa\",\"id\":\"61043\"},{\"province\":{\"name\":\"Torino\",\"id\":\"001\",\"region\":{\"name\":\"Piemonte\",\"id\":\"01\"}},\"cadastralCode\":\"L345\",\"name\":\"Traversella\",\"id\":\"1278\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Case OK - Search "AVERSA" - WEB
     * Need Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaComuneIndirizzo_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
