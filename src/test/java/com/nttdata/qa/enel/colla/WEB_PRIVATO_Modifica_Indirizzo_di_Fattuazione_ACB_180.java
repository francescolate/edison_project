package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_180_Mod_Ind_Fatturazione_ACB;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Indirizzo_di_Fattuazione_ACB_180 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com");//collaudol.oyaltycolazzo20.20@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGEPATH","AREA RISERVATA HOMEPAGE");
		prop.setProperty("HOMEPAGETITLE","Benvenuto nella tua area dedicata Business");
		prop.setProperty("SERVIZI_PAGEPATH","AREA RISERVATA SERVIZI");
		prop.setProperty("SERVIZI_PAGETITLE","Servizi");
		prop.setProperty("SERVIZI_PAGE_SUBTEXT","In questa pagina trovi tutti i servizi.");
		prop.setProperty("INDIRIZZO_DI_FATTURAZIONE_PAGEPATH","AREA RISERVATA FORNITURA INDIRIZZO DI FATTURAZIONE");
		prop.setProperty("INDIRIZZO_DI_FATTURAZIONE_PAGETITLE","L'indirizzo di fatturazione della tua fornitura");
		prop.setProperty("MODIFICA_INDIRIZZO_DI_FATTURAZIONE_PAGEPATH","AREA RISERVATAFORNITURAMODIFICA INDIRIZZO DI FATTURAZIONE");
		prop.setProperty("MODIFICA_INDIRIZZO_DI_FATTURAZIONE_PAGETITLE","Modifica Indirizzo di Fatturazione");
		prop.setProperty("STEP1","I tuoi dati");
		prop.setProperty("STEP2","Riepilogo e conferma dati");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_180_Mod_Ind_Fatturazione_ACB.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}