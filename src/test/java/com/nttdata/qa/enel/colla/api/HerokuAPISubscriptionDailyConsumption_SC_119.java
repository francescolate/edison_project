/* HerokuAPISubscriptionDailyConsumption_SC_119 
** KO - Token - APP
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPISubscription_1;
import com.nttdata.qa.enel.util.ReportUtility;


public class HerokuAPISubscriptionDailyConsumption_SC_119 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		prop.setProperty("Authorization", "N");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/2g-ind/api/query/v1/subscription/dailyconsumption/627301545/IT001E77670065?startdate=20200803");
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"GEN10\",\"code\":\"-4\",\"description\":\"Sessione non valida, si prega di effettuare nuovamente l'accesso\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - Token - APP
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPISubscription_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
