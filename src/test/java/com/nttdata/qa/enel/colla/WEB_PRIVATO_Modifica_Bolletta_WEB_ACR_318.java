package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.Cancel_Item_CF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.PRIVATO_Modifica_Bolletta_WEB_ACR_318;
import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetails_Annullato_318;
import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetails_Inlavorazione_318;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Bolletta_WEB_ACR_318 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "lucia@yopmail.com");//collaudoloyaltycolazzo.2.020@gmail.com //CNCRRT77C66E463N
		//prop.setProperty("WP_USERNAME", "col.laudoloyaltycolazzo2.020@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaudoloyaltycolazz.o202.0@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("POD","03130000853653");//IT002E8779012A//IT001E10002525
		prop.setProperty("CF","GHNLCU37A47F032Y");//CNCRRT77C66E463N
		prop.setProperty("STATUS1","In attesa");
		prop.setProperty("STATUS2","Annullato");
		
		prop.setProperty("EMAIL", "fabiana.monza@nttdata.com");
		prop.setProperty("CELLULARE", "3669047153");
		
		prop.setProperty("LINK", Costanti.salesforceLink);
	    prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
	    
	    prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//Login_Private_Area.main(args);
		PRIVATO_Modifica_Bolletta_WEB_ACR_318.main(args);
		VerifyRequestDetails_Inlavorazione_318.main(args);
		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
		Cancel_Item_CF.main(args);
		VerifyRequestDetails_Annullato_318.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
