package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.ID_71_ModuloReclami;

public class WEB_Pubblico_ModuloReclami_71 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
		prop.setProperty("NOME", "Fabiana");
		prop.setProperty("COGNOME", "Rossi");
		prop.setProperty("INSERISCI_IL_TUO_NOME", "Inserisci il tuo nome");
		prop.setProperty("SELIZIONA_IL_SESSO", "SessoSeleziona il sesso");
		prop.setProperty("SELIZIONA_IL_PROV", "Seleziona la provincia");
		prop.setProperty("DATE_FORMAT", "GG/MM/AAAA");
		prop.setProperty("DATE_ALPHA", "AA/BB/CC");
		prop.setProperty("AGRIGENTO", "Agrigento");
		prop.setProperty("ACERRA", "Acerra");
		prop.setProperty("SESSO", "Femmina");
		prop.setProperty("DATE", "01011980");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		ID_71_ModuloReclami.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		
	};
}



