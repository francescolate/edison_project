package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.colla.ACB_302_Attiva_Bolletta_WEB;
import com.nttdata.qa.enel.testqantt.colla.ACR_345_Novita_Iscritto_EPW;
import com.nttdata.qa.enel.testqantt.colla.AnnullaCaseItem;
import com.nttdata.qa.enel.testqantt.colla.VerifyDataAfterAnnullament_302;
import com.nttdata.qa.enel.testqantt.colla.VerifyData_302;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.testqantt.colla.VerifyServiceRequestAfterAnnullament_302;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Attiva_Bolletta_WEB_ACB_302 {
		
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "mobil.etes.ting.automation@gmail.com"); //r32019p109st.atocontiweb@gmail.com r.32019p109s.tatocontiweb@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01");								//Password01
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("AREA_CLIENTI", "IMPRESA");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("EMAIL", "r.32019p109s.tatocontiweb@gmail.com");
		prop.setProperty("CONFERMA_EMAIL", "r.32019p109s.tatocontiweb@gmail.com");
		prop.setProperty("NUMERO", "702392838");
		prop.setProperty("CELLULARE", "3408976766");
		prop.setProperty("CONFERMA_NUMERO", "702392838");
		prop.setProperty("QUERY", "select id, Name, ITA_IFM_Subject__c,CreatedDate , ITA_IFM_Status__c,ITA_IFM_POD_PDR__c, ITA_IFM_R2D_Status__c, "
						+ "ITA_IFM_SAP_Status__c, ITA_IFM_SEMPRE_Status__c, ITA_IFM_SAPSD_Status__c, ITA_IFM_UDB_Status__c "
						+ "from ITA_IFM_Case_Items__c "
						+ "where ITA_IFM_Account_ID__c = '0011l00000hpsP6AAI' and ITA_IFM_Subject__c = 'ATTIVAZIONE VAS' and CreatedDate = TODAY ORDER BY createddate desc");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("LINK_SALESFORCE", "https://enelcrmt--uat.lightning.force.com/");
		
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);
		ACB_302_Attiva_Bolletta_WEB.main(args);
		RecuperaDatiWorkbench.main(args);
		VerifyData_302.main(args);
		AnnullaCaseItem.main(args);
		RecuperaDatiWorkbench.main(args);
		VerifyDataAfterAnnullament_302.main(args);
		prop.setProperty("QUERY", "select CreatedDate , ITA_IFM_CancelStatus__c,ITA_IFM_OperationStatus__c, ITA_IFM_Status__c, RecordType.Name "
						+ "from ITA_IFM_ServiceRequest__c "
						+ "where ITA_IFM_Account__c = '0011l00000hpsP6AAI' and CreatedDate = TODAY and RecordType.Name = 'Attivazione VAS - Modifica' ORDER BY createddate desc");
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		RecuperaDatiWorkbench.main(args);
		VerifyServiceRequestAfterAnnullament_302.main(args);
};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
