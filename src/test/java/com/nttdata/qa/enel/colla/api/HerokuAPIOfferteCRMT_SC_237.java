/* HerokuAPICoperturaIndirizzo_SC_237 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - WEB - RES - Subentro Espletato

"userUPN": "7ccb61f5-32be-4663-9ba8-b45cbefa141b",
"cf": "BGLGPP32L14E506L",
"enelId": "2feb6bb1-88b3-44d7-8419-d2e6788728f0",
"userEmail": "r.3201.9p109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_237 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","BGLGPP32L14E506L");
		prop.setProperty("USERNAME","r.3201.9p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","7ccb61f5-32be-4663-9ba8-b45cbefa141b");
		prop.setProperty("ENELID","2feb6bb1-88b3-44d7-8419-d2e6788728f0");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//	//	prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJFRTAwMDAwMTgxNTJAQk9YLkVORUwuQ09NIiwiYXVkIjoiQXBwTW9iaWxlIiwiY29kaWNlZmlzY2FsZWNvdW50cnkiOiJJVCIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZW5lbGlkIjoiZTY0NzE0NTktZDY4Zi00OTUyLTg0ZjQtNTQ5MzFiOGY2ODczIiwiZXhwIjoxNjA3Njk0MDE2LCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZDQyNDcwOGEtMjcwNS00NTE2LWFmYjAtOTgzODQ4ZmJkZmRjIiwic2lkIjoiN2FiOTRlMGItODA0Mi00ZjA5LTg1NTAtYjM0ODRiMWNmOGMyIn0.1jyqWzsW80_DxuiAfoliy0ZhSEwIA_qMP1W-0EWkvyY");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
       prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"05262321548562\",\"statoSap\":\"KO\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3100223\",\"flagStd\":1,\"namingInFattura\":\"GiustaXTe Gas\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"310499861\",\"processoInCorso\":\"SUBENTRO\",\"idCase\":\"5001l000003my0RAAQ\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OPSHAA4\",\"dataAttivazione\":\"2020-03-30T00:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000TRJXEA4\",\"dataRichiesta\":\"2020-03-28T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwB4UAI\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA ACHILLE BIZZONI 4 - 20125 MILANO MI\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - WEB - RES - Subentro Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
