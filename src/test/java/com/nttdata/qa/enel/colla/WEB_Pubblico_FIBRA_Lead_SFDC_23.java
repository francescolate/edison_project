package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.CheckCampiFibraDiMelitaRestiamoInContatto;
import com.nttdata.qa.enel.testqantt.colla.HeaderLogin;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.OpenHambugerAndCheckMenu;
import com.nttdata.qa.enel.testqantt.colla.VerificaCoperturaKOFibraDiMelita;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Pubblico_FIBRA_Lead_SFDC_23 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("LINK", "https://www-coll1.enel.it");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		HeaderLogin.main(args);
		OpenHambugerAndCheckMenu.main(args);
		CheckCampiFibraDiMelitaRestiamoInContatto.main(args);

		
		
	};
	
	@After
	  public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
		};
}
