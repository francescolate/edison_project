package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.GetCfForAttivaBollettaWeb;
import com.nttdata.qa.enel.testqantt.colla.GetCfForModificaBollettaWeb;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.ModificaBollettaWeb95;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_ModificaBollettaWEB_ACR_95 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	//	prop.setProperty("WP_USERNAME", "solehog812@tmail2.com"); //CLDSVR45L30A522G
		prop.setProperty("WP_USERNAME", "lucia@yopmail.com");
	
	//	prop.setProperty("WP_USERNAME", "r.3.2.0.19.p109statocontiweb@gmail.com");
		
	//  prop.setProperty("WP_USERNAME", "r.3201.9p109statocontiweb@gmail.com");
	// 	prop.setProperty("WP_USERNAME", "dan01@yopmail.com");
		
	//	prop.setProperty("WP_USERNAME", "col.laudoloyaltycolazzo.2020@gmail.com");	
	//	prop.setProperty("WP_USERNAME", "r.affaellaquom.o@gmail.com");
		
	    prop.setProperty("WP_PASSWORD", "Password01");	  
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("MOBILE_NUMBER", "3291929449");
	    prop.setProperty("CUSTOMER_NUMBER", "310492575");
	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		//GetCfForModificaBollettaWeb.main(args);
		//prop.load(new FileInputStream(nomeScenario));
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);
		ModificaBollettaWeb95.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
	