package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.Pubblico_Motorinodiricerca_79;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Motorinodiricerca_79 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	// TODO Change colla to colla1 before commit
	// final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception {
		this.prop = new Properties();

		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("MAIN_LINK",
				"https://www-coll1.enel.it/it/luce-e-gas/luce-e-gas/negozio-ufficio?nec=voltura&sortType=in-promozione");
		prop.setProperty("VISUALIZZA_OFFERTA_LUCE_LINK",
				"https://www-coll1.enel.it/it/luce-e-gas/luce/negozio-ufficio");
		prop.setProperty("VISUALIZZA_OFFERTA_GAS_LINK", "https://www-coll1.enel.it/it/luce-e-gas/gas/negozio-ufficio");
		prop.setProperty("CONTATTA_LINK", "https://www-coll1.enel.it/it/form-ricontatto-commerciale");
		prop.setProperty("INFORMAZIONI_LEGALI_LINK", "https://www-coll1.enel.it/it/supporto/faq/info-legali");
		prop.setProperty("LUCE_E_GAS", "Luce e gas");
		prop.setProperty("IMPRESA", "Imprese");
		prop.setProperty("INSIEME A TE", "Insieme a te");
		prop.setProperty("STORIE", "Storie");
		prop.setProperty("FUTUR-E", "Futur-e");
		prop.setProperty("MEDIA", "Media");
		prop.setProperty("SUPPORTO", "Supporto");
		prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
		prop.setProperty("CREDITS", "Credits");
		prop.setProperty("PRIVACY", "Privacy");
		prop.setProperty("COOKIE_POLICY", "Cookie Policy");
		prop.setProperty("REMIT", "Remit");
		prop.setProperty("PREFERENZE_COOKIE", "Preferenze Cookie");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void runTest() throws Exception {
		String args[] = { nomeScenario };
		Pubblico_Motorinodiricerca_79.main(args);
	};

	@After
	public void endTest() throws Exception {	
		  InputStream in = new FileInputStream(nomeScenario); prop.load(in);
		  this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		  ReportUtility.reportToServer(this.prop);
	};
}
