/* HerokuAPIAccountProfileByID_SC_139 
** AccountProfileByID - caso OK - WEB - RES
* java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\Genera_Token_API_WEB\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31
* No Token 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIAccountProfileByID_SC_139 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
        prop.setProperty("Authorization", "1");
		prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"code\":200,\"data\":{\"firstName\":\"Andrea\",\"lastName\":\"Pa\",\"phoneNumber\":\"+393312206204\",\"countryCode\":\"IT\",\"companyName\":\"\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"personalId\",\"value\":\"PAXNDR93H23F205I\"}]}],\"enelId\":\"f700815f-1ac0-4439-99ac-b47e7a53fe4f\",\"email\":\"r.32019p109stat.ocontiweb@gmail.com\",\"enabled\":true,\"vatCode\":\"\"},\"message\":\"OK\"},\"meta\":{\"path\":\"/profile/v1/account/{id}\",\"method\":\"account-profile-by-id\",\"dataAggiornamento\"");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * AccountProfileByID - caso OK - APP - RES 
     * No Token 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccountProfileByID_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
