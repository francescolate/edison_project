package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.Header;
import com.nttdata.qa.enel.testqantt.colla.HeaderLogin;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.Support;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Header_9 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
//	    prop.setProperty("LINK_FACEBOOK", "https://www.facebook.com/messages/t/enel.energia.10");
//	    prop.setProperty("LINK_TWITTER", "https://twitter.com/EnelEnergiaHelp");
//	    prop.setProperty("LINK_TELEGRAM", "https://telegram.org/");
//	    prop.setProperty("LINK_PEDIUS", "https://corporate-qual.enel.it/it/megamenu/media/news/2018/04/enel-lancia-app-pedius-per-comunicazione-non-udenti");
	   	
	    prop.setProperty("LINK_FACEBOOK", "https://www.facebook.com/login.php?next=https%3A%2F%2Fwww.facebook.com%2Fmessages%2Ft%2Fenel.energia.10");
	    prop.setProperty("LINK_TWITTER", "https://twitter.com/EnelEnergiaHelp");
	    prop.setProperty("LINK_TELEGRAM", "https://telegram.org/");
	    prop.setProperty("LINK_PEDIUS", "https://corporate-qual.enel.it/it/megamenu/media/news/2018/04/enel-lancia-app-pedius-per-comunicazione-non-udenti");
	    
	    prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		HeaderLogin.main(args);
		Header.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
	       //String args[] = {nomeScenario};
  InputStream in = new FileInputStream(nomeScenario);
  prop.load(in);
  this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
  ReportUtility.reportToServer(this.prop);

	};
}
