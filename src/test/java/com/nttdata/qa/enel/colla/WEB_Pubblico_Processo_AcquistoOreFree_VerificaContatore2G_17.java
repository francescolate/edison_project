package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.OreFree_Verify_Cont_2G;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Processo_AcquistoOreFree_VerificaContatore2G_17 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	//TODO Change colla to colla1 before commit
//	final String BASE_LINK = "https://www-coll2.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "marco.campanale@enelsaa3331.enel.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("POD_TEST_STRING_1", "IT");
		prop.setProperty("POD_TEST_STRING_2", "099393932");
		prop.setProperty("POD_TEST_STRING_3", "&%$G");
		prop.setProperty("POD_TEST_STRING_4", "IT012E3939393A");
		prop.setProperty("POD_TEST_ERROR_STRING", "Formato inserito non valido");
		prop.setProperty("ACCEPTANCE_PAGE_TITLE", "Scegli Tu Ore Free");
		prop.setProperty("CONTRACT_TYPE", "Luce");
		prop.setProperty("PLACE", "Casa");
		prop.setProperty("REQUIREMENTS", "Visualizza tutte");
		prop.setProperty("POPUP_TITLE", "Inserisci il numero del tuo POD*");
		prop.setProperty("POPUP_SUBTITLE", "Verifica che la tua fornitura abbia i requisiti per poter proseguire con l'adesione.");
		prop.setProperty("INFO_LINK_STRING", "Che cos'è il POD e dove lo trovo?");
		prop.setProperty("ERROR_LABEL_STRING", "Questo campo è obbligatorio");
		prop.setProperty("FAQ_LINK", "https://www-coll1.enel.it/it/supporto/faq/subentro");
		prop.setProperty("FAQ_TITLE", "Come fare un subentro");
		prop.setProperty("FAQ_SUBTITLE", "Riattivare un contatore in semplici passaggi");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		OreFree_Verify_Cont_2G.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
	
}
