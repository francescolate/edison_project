package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaBolletteGas;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu42;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_BollettaDiSintesi_e_Dettaglio_ACR_42 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		
	   // prop.setProperty("WP_USERNAME", "r32019p109statocontiwe.b@gmail.com");
	   prop.setProperty("WP_USERNAME", "ezio.galloni@enelsaa0003.accenture.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("SUPPLY_ADDRESS", "VIA VIRGINIA 12/A - 40017 SAN GIOVANNI IN PERSICETO BO");
	    prop.setProperty("CUSTOMER_CODE", "300001597");
	    prop.setProperty("TIPOLOGIA_BOLLETTA_BUTTON", "SCEGLI BOLLETTA DI DETTAGLIO");
	   // prop.setProperty("ACTION_TYPE", "DETTAGLIO_FORNITURA_BUTTON");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//PrivateAreaMenu42.main(args);
		PrivateAreaBolletteGas.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
