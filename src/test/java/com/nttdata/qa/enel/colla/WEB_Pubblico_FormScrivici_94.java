package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LaunchCaricaDocumenti;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_94_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent_94;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_FormScrivici_94 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
	this.prop=new Properties();
	
	prop.setProperty("WP_LINK", "https://www-coll1.enel.it/it/login");
	
	prop.setProperty("LUCE_E_GAS", "Luce e gas");
	prop.setProperty("IMPRESA", "Imprese");
	prop.setProperty("INSIEME A TE", "Insieme a te");
	prop.setProperty("STORIE", "Storie");
	prop.setProperty("FUTUR-E", "Futur-e");
	prop.setProperty("MEDIA", "Media");
	prop.setProperty("SUPPORTO", "Supporto");
	prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
	prop.setProperty("CREDITS", "Credits");
	prop.setProperty("PRIVACY", "Privacy");
	prop.setProperty("COOKIE_POLICY", "Cookie Policy");
	prop.setProperty("REMIT", "Remit");
	prop.setProperty("PREFERENZE_COOKIE", "Preferenze Cookie");
	
	prop.setProperty("RUN_LOCALLY","Y");
	prop.store(new FileOutputStream(nomeScenario), null);
  };

    @Test
    public void runTest() throws Exception{
    	String args[]= {nomeScenario};
		LaunchCaricaDocumenti.main(args);
		Publicco_ID_94_Form_Scrivici.main(args);
		VerifyEmailSubjectAndContent_94.main(args);
  };

   @After
    public void endTest() throws Exception{
	InputStream in = new FileInputStream(nomeScenario);
    prop.load(in);
    this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
    ReportUtility.reportToServer(this.prop);
  };

 }
