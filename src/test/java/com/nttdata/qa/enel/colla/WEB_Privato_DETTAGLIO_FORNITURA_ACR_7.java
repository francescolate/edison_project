package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.Home_Page_Residential_Menu;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.Supply_Detail;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_DETTAGLIO_FORNITURA_ACR_7 {

	private RemoteWebDriver driver;  
	Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "r.32019p109statocontiweb@gmail.com"); //raffaella.q.uomo@gmail.com
	    prop.setProperty("WP_PASSWORD", "Password01");
//	    prop.setProperty("LOGIN_POPUP_TEXT", "Gentile cliente, dalle ore 22:00 di Venerdì 24 Aprile alle ore 07:00 di lunedì 27 Aprile i servizi digitali dell’area clienti web e della app saranno in manutenzione. Lavoreremo per migliorarne la qualità. Anticipa in questi giorni le operazioni di gestione della tua fornitura come il pagamento della bolletta, il download del PDF della bolletta o l’attivazione di uno dei nostri servizi.Il servizio di modifica fascia base o giornaliera e verifica dei consumi per i clienti con offerta ORE FREE, sarà sempre disponibile dalla App di Enel Energia. Se non l’hai ancora fatto, scarica la nostra App!");
	    prop.setProperty("ACCOUNT_TYPE","");
	    prop.setProperty("AREA_CLIENTI", "");
	   	       
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Private_Area.main(args);
		Home_Page_Residential_Menu.main(args);
		Supply_Detail.main(args);
				
	};
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}	

