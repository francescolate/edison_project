/* HerokuAPICoperturaIndirizzo_SC_206 
** Case KO APP - No Token
* need token (cliente qualsiasi)
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaComuneIndirizzo_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaComuneIndirizzo_4;
import com.nttdata.qa.enel.util.ReportUtility;
// import com.nttdata.jwt.gen.*;


public class HerokuAPICoperturaIndirizzo_SC_206 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ID_HOUSENUMBER","380100004236133");
		prop.setProperty("ID_STRADA","38000051841");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		prop.setProperty("Authorization", "");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/order-fibra/v1/indirizzo?idhousenumber="+ prop.getProperty("ID_HOUSENUMBER")+ "&idstrada="+ prop.getProperty("ID_STRADA"));
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"GEN10\",\"code\":\"-4\",\"description\":\"Sessione non valida, si prega di effettuare nuovamente l'accesso\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Case KO APP - No Token
     * Need Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaComuneIndirizzo_4.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
