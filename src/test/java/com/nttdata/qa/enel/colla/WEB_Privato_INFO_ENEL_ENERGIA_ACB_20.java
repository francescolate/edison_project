package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu42;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.ActivateInfoEnelEnergia_Bsn;
import com.nttdata.qa.enel.testqantt.colla.ActivateInfoEnelEnergia_Bsn_Exit;

public class WEB_Privato_INFO_ENEL_ENERGIA_ACB_20 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	//final String BASE_LINK = "https://www-colla.enel.it/";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
		prop.setProperty("WP_USERNAME", "testImpresa@yopmail.com"); //07351980565
		prop.setProperty("WP_PASSWORD", "Password01");
		
		prop.setProperty("AREA_CLIENTI", "IMPRESE");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		
		prop.setProperty("SAVE_CLIENT_NUMBER", "false");
		prop.setProperty("QUERY", "");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//PrivateAreaMenu42.main(args);
		ActivateInfoEnelEnergia_Bsn.main(args);
		ActivateInfoEnelEnergia_Bsn_Exit.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		   InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	};
	
}
