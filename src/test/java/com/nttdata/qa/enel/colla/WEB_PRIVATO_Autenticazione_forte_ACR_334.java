package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.ACR_334_Autenticazione_Forte;
import com.nttdata.qa.enel.testqantt.colla.AccountRegistration;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration;
import com.nttdata.qa.enel.testqantt.colla.CreateEmailAddressesList;
import com.nttdata.qa.enel.testqantt.colla.DeleteRegisteredAccount;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_PRIVATO_Autenticazione_forte_ACR_334 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();

		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
		// prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "");
		// New strategy for registration - 2021-02-18
		prop.setProperty("ACCOUNT_FIRSTNAME", "MITICA");
		prop.setProperty("ACCOUNT_LASTNAME", "COMAN");
		prop.setProperty("CF", "CMNMTC68B14Z129Q");
		prop.setProperty("SUPPLY_NUMBER", "739997012");
		prop.setProperty("WP_USERNAME", "t.es.t.i.n.g.c.rm.a.u.t.o.m.a.t.i.o.n@gmail.com");

		prop.store(new FileOutputStream(nomeScenario), null);

	};

	@Test
	public void eseguiTest() throws Exception {
		String args[] = { nomeScenario };
		// NOT UNCOMMENT THE NEXT LINE OR COPY WITHIN YOUR SCRIPTS
		// CreateEmailAddressesList.main(args);
		// GetDataForRegistration.main(args);
		DeleteRegisteredAccount.main(args);
		AccountRegistration.main(args);
		SetUtenzaMailResidenziale.main(args);
		RecuperaLinkCreazioneAccount.main(args);
		ACR_334_Autenticazione_Forte.main(args);
	};
	
	@After
	public void fineTest() throws Exception {
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
