package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration;
import com.nttdata.qa.enel.testqantt.colla.DeleteRegisteredAccount;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration_333;
import com.nttdata.qa.enel.testqantt.colla.GetUserForRegistration_38;
import com.nttdata.qa.enel.testqantt.colla.Id_38_Registrazione_BSN;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_PUBBLICO_ACCEDI_A_MY_ENEL_38 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		//prop.setProperty("WP_USERNAME", "te.s.t.i.n.g.c.r.mautomation@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("AREA_CLIENTI", "IMPRESA");
		prop.setProperty("SUPPLY_NUMBER", "631279708");
		prop.setProperty("PIVA", "02105380980");
		prop.setProperty("COMPANY_NAME", "SINED SPA");
		prop.setProperty("ACCOUNT_FIRSTNAME", "Stefano");
		prop.setProperty("ACCOUNT_LASTNAME", "Pietro");
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "testin.gcrmautoma.t.ion@gmail.com");
		//prop.setProperty("WP_USERNAME", "m.o.bi.letesti.ngauto.ma.t.ion@gmail.com");
        
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	GetDataForRegistration_333.main(args);
		DeleteRegisteredAccount.main(args);
		Id_38_Registrazione_BSN.main(args);	
		SetUtenzaMailResidenziale.main(args);
        RecuperaLinkCreazioneAccount.main(args);
        ContinueWithRegistration.main(args);
	};
	
	@After
	 public void fineTest() throws Exception{
		String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };
}
