package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.BSN_Customer_Supply_47;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_RINOMINA_FORNITURA_ACB_47 {
	
	Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "r.3.2.0.19p109statocont.iweb@gmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("CUSTOM_NUM", "636727539");
//	    prop.setProperty("AREA_CLIENTI", "");
//	    prop.setProperty("ACCOUNT_TYPE", "");
//	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");

  	       
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		BSN_Customer_Supply_47.main(args);
		
						
	};
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
	
	

}
