package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyUserDetails_303;
import com.nttdata.qa.enel.testqantt.colla.ACB_303_Attiva_Bolletta_WEB;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Attiva_Bolletta_WEB_ACB_303 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "m.obiletesti.ngautomat.ion@gmail.com"); //r.32019p109sta.tocontiweb@gmail.com //collaudoloyaltycolazz.o2.020@gmail.com //col.laudoloyaltycolazzo20.2.0@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("EMAIL", "collaudolo.yaltycolazzo20.20@gmail.com");
		prop.setProperty("CONFERMA_EMAIL", "collaudolo.yaltycolazzo20.20@gmail.com");
		prop.setProperty("NUMERO", "702392838");
		prop.setProperty("CONFERMA_NUMERO", "702392838");
	    prop.setProperty("LINK", Costanti.salesforceLink);
	  //  prop.setProperty("NUMERO_CLIENTE", "888895230");
	    prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
	 
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);	
		ACB_303_Attiva_Bolletta_WEB.main(args);
		/*
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		VerifyUserDetails_303.main(args);
		*/
};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
