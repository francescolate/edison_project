/*HerokuAPIRicercaForniture_SC_103 
** API call for Tokens
* RicercaForniture - OK - BSN At least one supply GAS in "Attiva" state
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 00086480118 r.32019p109statoco.ntiweb@gmail.com 30a1fe52-f3e7-4ca5-a592-c30a26890c2c 0418d8e5-b56c-455c-812a-89ef40baf1f3 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica_CF_PIVA;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIRicercaForniture_SC_103 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","00086480118");
		prop.setProperty("USERNAME","r.32019p109statoc.ontiweb@gmail.com");
		prop.setProperty("USERUPN","30a1fe52-f3e7-4ca5-a592-c30a26890c2c");
		prop.setProperty("ENELID","0418d8e5-b56c-455c-812a-89ef40baf1f3");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiMzBhMWZlNTItZjNlNy00Y2E1LWE1OTItYzMwYTI2ODkwYzJjIiwiY29kaWNlZmlzY2FsZSI6IjAwMDg2NDgwMTE4IiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMTE1OTY3LCJlbmVsaWQiOiIwNDE4ZDhlNS1iNTZjLTQ1NWMtODEyYS04OWVmNDBiYWYxZjMiLCJ1c2VyaWQiOiJyLjMyMDE5cDEwOXN0YXRvY28ubnRpd2ViQGdtYWlsLmNvbSIsImp0aSI6IjdjMTAyYmQxLTNiNzAtNGVhNC04ZDc4LWUzMjhmOGJhOTE1ZiJ9.fefnHpEJnTZoFAuDco8IW1XHdW4251Pq0DvpRslv2DI");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-extra-stage/v1/info/ricerca/forn?cf=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"stato\":\"Attivo\",\"listinoInFattura\":\"Soluzione Gas Impresa Extra\",\"fornKey\":\"0011l00000XXGo7AAH|02i1l0000044nzWAAQ|310494050\",\"codice\":\"310494050\",\"indirizzo\":\"VIA ADDA 12 - 20124 MILANO MI\",\"name\":\"310494050\",\"partitaIvaCliente\":\"00086480118\",\"tipoForn\":\"Gas\",\"flagDefault\":\"Y\",\"contrattoSAP\":\"0039257545\",\"bpAccountSAP\":\"0039637336\",\"contractAccountSAP\":\"001032754767\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * RicercaForniture - OK - BSN At least one supply GAS in "Attiva" state
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica_CF_PIVA.main(args); 
				
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
