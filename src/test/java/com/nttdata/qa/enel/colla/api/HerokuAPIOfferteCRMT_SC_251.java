/* HerokuAPICoperturaIndirizzo_SC_251 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - BSN - WEB - VOLTURA CON ACCOLLO - Espletato
java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 00113800312 r32019p109.statocontiweb@gmail.com 93ce26ed-04b6-4c33-866f-bedb2d7e4a00 5b4bb143-eaee-421d-b3f1-93edce3e89fa 2020-12-31
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_251 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","00113800312");
		prop.setProperty("USERNAME","r32019p109.statocontiweb@gmail.com");
		prop.setProperty("USERUPN","93ce26ed-04b6-4c33-866f-bedb2d7e4a00");
		prop.setProperty("ENELID","5b4bb143-eaee-421d-b3f1-93edce3e89fa");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkY2NkN2YxYS02ZmI5LTQ2MmQtYTJlNi03M2M1NmJkMzNlZmUiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJmNzAwODE1Zi0xYWMwLTQ0MzktOTlhYy1iNDdlN2E1M2ZlNGYiLCJleHAiOjE2MDc2OTI5MDEsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZmRkZmY1NjUtNzVlYS00MzEyLTg5NjMtZTY5NzM0ZTAyMmIzIiwic2lkIjoiNjJhNjJkYWYtZWMxNS00MTdhLTlmNjEtZTVkNGFjYTFmZTdjIn0.tjo0jJ5Tvijg-9Wwnk98VgM4xm0RoB8bVsn9F1SMHJM");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//      prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":null,\"podPdr\":\"IT001E04665371\",\"statoSap\":\"OK\",\"statoSempre\":\"OK\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":null,\"flagStd\":0,\"namingInFattura\":\"Anno Sicuro\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"639391728\",\"processoInCorso\":\"VOLTURA CON ACCOLLO\",\"idCase\":\"5001l000003nCj0AAE\",\"idListino\":\"a1l0Y000000XTXIQA4\",\"dataAttivazione\":\"2007-01-31T23:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000UwiyEAC\",\"dataRichiesta\":null,\"idProdotto\":\"a1Y2400000BFIpmEAH\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA BUGATTO GIUSEPP 4 - 34077 RONCHI DEI LEGIONARI GO\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Cessazione\",\"processoCombinato\":false}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":null,\"podPdr\":\"IT001E04665371\",\"statoSap\":\"OK\",\"statoSempre\":\"OK\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":null,\"flagStd\":0,\"namingInFattura\":\"Anno Sicuro\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"639391728\",\"processoInCorso\":\"VOLTURA CON ACCOLLO\",\"idCase\":\"5001l000003nCj0AAE\",\"tipologiaProdotto\":\"BSN\",\"idListino\":\"a1l0Y000000XTXIQA4\",\"dataAttivazione\":\"2007-01-31T23:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000UwiyEAC\",\"dataRichiesta\":null,\"idProdotto\":\"a1Y2400000BFIpmEAH\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA BUGATTO GIUSEPP 4 - 34077 RONCHI DEI LEGIONARI GO\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Cessazione\",\"processoCombinato\":false}");
        
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - BSN - WEB - VOLTURA CON ACCOLLO - Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
