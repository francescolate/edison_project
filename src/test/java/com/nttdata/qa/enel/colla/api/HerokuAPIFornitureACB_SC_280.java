/*HerokuAPIFornitureACB_SC_280 
** API call for Tokens - Need VPN ENEL
* Fornitura ACB - KO - WEB - No CF/PIVA in input 
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 01533540447 r.32019p109statoc.ontiweb@gmail.com e66bc31c-44a5-4ed6-a76b-39a5808567da 998777cd-ccbc-45e8-b013-2578bc623fff 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIFornitureACB;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIFornitureACB_SC_280 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("PIVA","");
		prop.setProperty("CF_PIVA","01533540447");
		prop.setProperty("USERNAME","r.32019p109statoc.ontiweb@gmail.com");
		prop.setProperty("USERUPN","e66bc31c-44a5-4ed6-a76b-39a5808567da");
		prop.setProperty("ENELID","998777cd-ccbc-45e8-b013-2578bc623fff");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZTY2YmMzMWMtNDRhNS00ZWQ2LWE3NmItMzlhNTgwODU2N2RhIiwiY29kaWNlZmlzY2FsZSI6IjAxNTMzNTQwNDQ3IiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMDg5ODk1LCJlbmVsaWQiOiI5OTg3NzdjZC1jY2JjLTQ1ZTgtYjAxMy0yNTc4YmM2MjNmZmYiLCJ1c2VyaWQiOiJyLjMyMDE5cDEwOXN0YXRvYy5vbnRpd2ViQGdtYWlsLmNvbSIsImp0aSI6IjRlMTc2OTJmLTc5ZjctNGRmMy1hMTU5LTgzNzYzZDFhYTMwMSJ9.-RtqOBIpfQ5cK2XYBxKiKf5zfXc2xzMFF-jssgdghm0");
		prop.setProperty("JSON_DATA_OUTPUT","{\"data\":{\"forniture\":[]},\"status\":{\"descrizione\":\"Chiamata Anagrafica - Codice Esito: ResponseStatus [codice=null, descrizione=null, esito=null]\",\"esito\":\"KO\",\"codice\":\"005\"}}"); 
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens - Need VPN ENEL
     * Fornitura ACB - KO - WEB - No CF/PIVA in input 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIFornitureACB.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
