/*HerokuAPIRegistration_SC_288
** API call for Tokens
** Chiamata API per Token
Chiamata API Registrazione con cliente BSN , Fornitura Attiva. TAG PIVA VUOTO 
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRegistration_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIRegistration_SC_288 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account");
		prop.setProperty("FIRSTNAME","ALBERTO");
		prop.setProperty("LASTNAME","CORRADINI");
		prop.setProperty("CODICEFISCALE","");
		prop.setProperty("EMAIL","r.3.2.0.1.9.p.109statocontiweb@gmail.com");
        prop.setProperty("PHONENUMBER","+393208800123");
        prop.setProperty("DENOMINAZIONE","CLIENTE AZIENDA AGRICOLA CORRADINI ALBERTO EGIDIO");
		//prop.setProperty("JSON_INPUT", "{     \"data\": {         \"email\": \"" + prop.getProperty("email") + "\",         \"phoneNumber\": \"" + prop.getProperty("phoneNumber") + "\",         \"password\": \"Password01\",         \"firstName\": \"" + prop.getProperty("firstName") + "\",         \"lastName\": \"" + prop.getProperty("lastName") + "\",         \"countryCode\": \"IT\",         \"companyName\": \"" + prop.getProperty("Denominazione") + "\",         \"vatCode\": \"" + prop.getProperty("CodiceFiscale") + "\",         \"attributes\": [             {                 \"country\": \"IT\",                 \"claims\": [                     {                         \"name\": \"businessId\",                         \"value\": \"" + prop.getProperty("CodiceFiscale") + "\"                     }                 ]             }         ],         \"consents\": [             {                 \"id\": \"11\",                 \"group\": \"12\",                 \"language\": \"it_IT\",                 \"channel\": \"ENEL_ENERGIA\",                 \"given\": true             },             {                 \"id\": \"10\",                 \"group\": \"12\",                 \"language\": \"it_IT\",                 \"channel\": \"ENEL_ENERGIA\",                 \"given\": true             }         ],         \"metadata\": {             \"language\": \"it_IT\",             \"processChannel\": \"ENEL_ENERGIA\"         }     } }");
// MODULO        prop.setProperty("JSON_INPUT", "{     \"email\": \"" + prop.getProperty("EMAIL") + "\",     \"phoneNumber\": \"" + prop.getProperty("PHONENUMBER") + "\",     \"password\": \"Password01\",     \"firstName\": \"" + prop.getProperty("FIRSTNAME") + "\",     \"lastName\": \"" + prop.getProperty("LASTNAME") + "\",     \"countryCode\": \"IT\",     \"companyName\":\"" + prop.getProperty("DENOMINAZIONE") + "\",     \"vatCode\":\"" + prop.getProperty("CODICEFISCALE") + "\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"businessId\",                     \"value\": \"" + prop.getProperty("CODICEFISCALE") + "\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");prop.setProperty("JSON_DATA_OUTPUT", "{     \"data\": {         \"message\": \"409 Conflict\"     },     \"status\": {         \"code\": \"-1\",         \"result\": \"KO\",         \"description\": \"FAILED\"     }");		
        prop.setProperty("JSON_DATA_OUTPUT", "{\"attributes[0].claims[0].value\":\"Field 'attributes[0].claims[0].value' must be not empty, '' given\"}");
        prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
	 * API call for Tokens
     * Chiamata API Registrazione con cliente BSN , Fornitura Attiva. TAG PIVA VUOTO 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRegistration_2.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
