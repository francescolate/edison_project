/*HerokuAPIGetUUID_Email_SC_268A 
** API call for Tokens
** Chiamata API GET UDID by Email con email esistente e cliente già registrato RES
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID_MAIL_1;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetUUID_Email_SC_268A {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("EMAIL","r.affaellaquomo@gmail.com");
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?email=");
//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?email=" + prop.getProperty("EMAIL"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":200,\"data\":{\"accounts\":[{\"firstName\":\"Agata\",\"lastName\":\"Russo\",\"phoneNumber\":\"+39064745721\",\"countryCode\":\"IT\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"personalId\",\"value\":\"RSSGTA62E53I035R\"}]}],\"enelId\":\"3964ca58-0858-4a8b-b7b9-27eaa88130e6\",\"email\":\"r.affaellaquomo@gmail.com\",\"enabled\":true}]},\"message\":\"OK\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Chiamata API GET UDID by Email con email esistente e cliente già registrato RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID_MAIL_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
