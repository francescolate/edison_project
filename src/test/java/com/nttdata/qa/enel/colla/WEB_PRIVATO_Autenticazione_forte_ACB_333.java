package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.ACB_333_AutenticazioneForte;
import com.nttdata.qa.enel.testqantt.colla.AccountRegistration;
import com.nttdata.qa.enel.testqantt.colla.DeleteRegisteredAccount;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration_333;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_PRIVATO_Autenticazione_forte_ACB_333 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
	//	prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("AREA_CLIENTI", "Impressa");
		prop.setProperty("SUPPLY_NUMBER", "631279708");
		prop.setProperty("CF", "02105380980");
		prop.setProperty("COMPANY_NAME", "SINED SPA");
		prop.setProperty("ACCOUNT_FIRSTNAME", "Stefano ");
		prop.setProperty("ACCOUNT_LASTNAME", "Pietro");
		prop.setProperty("WP_USERNAME", "testin.gcrmautoma.t.ion@gmail.com");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		//NOT UNCOMMENT THE NEXT LINE OR COPY WITHIN YOUR SCRIPTS
		
		//CreateEmailAddressesList.main(args);
	//	GetDataForRegistration_333.main(args);
		DeleteRegisteredAccount.main(args);
		AccountRegistration.main(args);
		SetUtenzaMailResidenziale.main(args);
        RecuperaLinkCreazioneAccount.main(args);
        ACB_333_AutenticazioneForte.main(args);
        
	};
	
	@After
    public void fineTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
	};
}
