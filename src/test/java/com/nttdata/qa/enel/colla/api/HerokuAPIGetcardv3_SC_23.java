// HerokuAPIGetcardv3_SC_23
// Solo per APP  - TOken in input expire 31/12/2021
/**"userUPN": "3e2f1455-1a19-4e05-94c6-ea3b7c793256",
"cf": "PRLVCN51C01L134B"
"enelId": "edfa1604-94c6-4a7e-914a-681881fdf221",
"userEmail": "raf.faellaqu.omo@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar PRLVCN51C01L134B raf.faellaqu.omo@gmail.com 3e2f1455-1a19-4e05-94c6-ea3b7c793256 edfa1604-94c6-4a7e-914a-681881fdf221 2021-12-31**/
// OK - RES - Check stato Fornitura SOSPESA

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_23 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","PRLVCN51C01L134B");
		prop.setProperty("USERNAME","raf.faellaqu.omo@gmail.com");
		prop.setProperty("USERUPN","3e2f1455-1a19-4e05-94c6-ea3b7c793256");
		prop.setProperty("ENELID","edfa1604-94c6-4a7e-914a-681881fdf221");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \""+prop.getProperty("USERNAME")+"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		prop.setProperty("JSON_DATA_OUTPUT", "\"descrizione\":\"La tua fornitura gas è stata sospesa. Regolarizza i pagamenti per riattivarla ora\"");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check stato Fornitura SOSPESA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
