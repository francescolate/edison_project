// HerokuAPIValidaOTP_SC_264
// Solo per WEB  - TOken generate
/* Valida OTP - KO 
API call for Tokens
Valid API call OTP (certification email) TOKEN NOT valid
 */

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//import com.nttdata.qa.enel.util.APIService_2;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIValidaOTP_2;

public class HerokuAPIValidaOTP_SC_264 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CODE","18330ab3-4eee-4505-95e2-a09af070118c");
		prop.setProperty("Authorization", "Bearer 584061f1-0f15-3ffe-a2db-03e8b55b4e21");
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/provisioning/validation-code/validate");
		prop.setProperty("JSON_INPUT", "{    \"code\": \"" + prop.getProperty("CODE") + "\",    \"metadata\": {        \"language\": \"it_IT\",        \"processChannel\": \"ENEL_ENERGIA\"    }}"); 
		prop.setProperty("JSON_DATA_OUTPUT", "{}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Valida OTP - KO 
     * API call for Tokens
     * Valid API call OTP (certification email) TOKEN NOT valid
     * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};

		HerokuAPIValidaOTP_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };


	

}
