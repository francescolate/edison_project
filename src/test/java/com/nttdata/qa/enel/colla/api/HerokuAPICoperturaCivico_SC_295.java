/*HerokuAPICoperturaCivico_SC_295
** Chiamata API CoperturaCivico - Case OK - WEB - RES	
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaCivico;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPICoperturaCivico_SC_295 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("Authorization", "Token");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("CF_PIVA","ZMBGFR38C21A944X");
		prop.setProperty("USERNAME","nttcosenzateam@gmail.com");
		prop.setProperty("USERUPN","ab55160d-e3de-411c-a458-81ec880a322f");
		prop.setProperty("ENELID","f87ecdc9-69c0-4a17-9596-e4f2ca7010f9");
		prop.setProperty("EXPIRATION","2022-07-30");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/order-fibra/v1/coperturacivico?civico=1&idstrada=38000079839");
		prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"OK\",\"code\":\"0\",\"description\":\"SUCCESS\"}");
		prop.setProperty("JSON_DATA_OUTPUT_2", "{\"houseNumbers\":[{\"id\":\"380100009786979\",\"value\":\"164\"},{\"id\":\"380100009787105\",\"value\":\"180\"},{\"id\":\"380100009787201\",\"value\":\"194\"},{\"id\":\"380100009787171\",\"value\":\"190\"},{\"id\":\"380100009786817\",\"value\":\"10\"}]}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
	 * API call for Tokens
     * Chiamata API CoperturaCivico - Case OK - WEB - RES							
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaCivico.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
