package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.ModificaOpzioniBollettaDiSintesi;
import com.nttdata.qa.enel.testqantt.colla.ModificaOpzioniBollettaDiSintesiDettaglioInterruzioneDelProcesso;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Bolletta_di_Sintesi_2_0_ACB_112 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	 //   prop.setProperty("WP_USERNAME", "r.32019p109.statocontiweb@gmail.com");
	    
	    prop.setProperty("WP_USERNAME", "collaudol.oyaltycolazzo20.20@gmail.com"); 
	    prop.setProperty("WP_PASSWORD", "Password01");
//	    prop.setProperty("SUPPLY_ADDRESS", "VIA VIRGINIA 12/A - 40017 SAN GIOVANNI IN PERSICETO BO");
//	    prop.setProperty("CUSTOMER_CODE", "300001594");
//	    prop.setProperty("TIPOLOGIA_BOLLETTA_BUTTON", "SCEGLI BOLLETTA DI DETTAGLIO");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");
	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		ModificaOpzioniBollettaDiSintesi.main(args);
		ModificaOpzioniBollettaDiSintesiDettaglioInterruzioneDelProcesso.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
        
	};
}
