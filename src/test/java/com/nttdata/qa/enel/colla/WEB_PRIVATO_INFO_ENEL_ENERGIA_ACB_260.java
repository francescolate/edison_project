package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.VerifyRequestDetail_260;
import com.nttdata.qa.enel.testqantt.colla.ACB_260_InfoEnergia;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubject_260;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_INFO_ENEL_ENERGIA_ACB_260 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r32019p109st.atocontiwe.b@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("AREA_CLIENTI", "Impresse");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");	    
	  //  prop.setProperty("USERNAME",prop.getProperty("GMAIL_ADDRESS_INPUT"));
	    prop.setProperty("USERNAME", "testing.crm.automation@gmail.com");
		prop.setProperty("PASSWORD",Costanti.password_Tp2_mobile);	    
	    prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");       
	    prop.setProperty("ACCOUNT_ID", "0010Y00000E9SHQQA3");
	    prop.setProperty("CF", "00627400872");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");		
	//	prop.setProperty("EMAIL_INPUT", "mobile.testing.automatio@gmail.com");
		prop.setProperty("EMAIL_INPUT", "testing.crm.automation@gmail.com");
		//prop.setProperty("GMAIL_ADDRESS_INPUT", "testing.crm.automation@gmail.com");
		
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);
		ACB_260_InfoEnergia.main(args);
        VerifyEmailSubject_260.main(args);
		VerifyRequestDetail_260.main(args);
};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
