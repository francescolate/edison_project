/* HerokuAPICoperturaIndirizzo_SC_213 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - Attivazione Fibra - Annullata

"userUPN": "EE0000018152@BOX.ENEL.COM",
"cf": "TNTFNC84M57L736V",
"enelId": "e6471459-d68f-4952-84f4-54931b8f6873",
"userEmail": "r32019p.109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_213 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","TNTFNC84M57L736V");
		prop.setProperty("USERNAME","r32019p.109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","EE0000018152@BOX.ENEL.COM");
		prop.setProperty("ENELID","e6471459-d68f-4952-84f4-54931b8f6873");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));

///		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":null,\"tipoOfferta\":null,\"podPdr\":\"01611289013987\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3012167\",\"flagStd\":0,\"namingInFattura\":null,\"statoR2d\":null,\"numeroUtente\":\"788072768\",\"processoInCorso\":\"Attivazione Fibra\",\"idCase\":\"5001l000002tulFAAQ\",\"idListino\":null,\"dataAttivazione\":\"2015-05-05T22:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000HPioEAG\",\"dataRichiesta\":\"2019-09-26T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AxqEEAS\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA VERDI 8 - 35028 PIOVE DI SACCO PD\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione Fibra\",\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":null,\"tipoOfferta\":null,\"podPdr\":\"01611289013987\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3015703\",\"flagStd\":0,\"namingInFattura\":null,\"statoR2d\":null,\"numeroUtente\":\"788072768\",\"processoInCorso\":\"Attivazione Fibra\",\"idCase\":\"5001l000002v1X0AAI\",\"tipologiaProdotto\":\"RES\",\"idListino\":null,\"dataAttivazione\":\"2015-05-05T22:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000HT4qEAG\",\"dataRichiesta\":\"2019-10-02T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AxqEEAS\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA VERDI 8 - 35028 PIOVE DI SACCO PD\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione Fibra\",\"processoCombinato\":false}");

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - Attivazione Fibra - Annullata
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
