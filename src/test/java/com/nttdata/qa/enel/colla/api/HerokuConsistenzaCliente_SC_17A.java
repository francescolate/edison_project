package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_17A {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		//prop.setProperty("JSON_INPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"711958577\",\"tipologiaFornitura\":\"Gas\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA VIA SPOLETO 4 - 37121 VERONA LE\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"Y\",\"podPdr\":\"61493840052412\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2016-06-13T22:00:00.000Z\",\"dataAttivazioneProdotto\":null,\"prodotto\":\"Energiasicura gas\"}]}");
		prop.setProperty("JSON_INPUT", "{\"data\": {  \"codiceFiscale\": \"\",  \"piva\": \"\",  \"podPdr\": \"61493840052412\",  \"numeroutente\": \"711958577\",  \"uniqueid\": \"\"}}");

		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"711958577\",\"tipologiaFornitura\":\"Gas\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA VIA SPOLETO 4 - 37121 VERONA LE\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"Y\",\"podPdr\":\"61493840052412\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2016-06-13T22:00:00.000Z\",\"dataAttivazioneProdotto\":null,\"prodotto\":\"Energiasicura gas\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API per verifica TAG di output
Numeroutente = Valorizzato se e Solo se statoServizio = ATTIVO
RES 
	 * @throws Exception
	 */	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
