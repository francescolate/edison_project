package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_8B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"\",   \"piva\": \"\",   \"podPdr\": \"IT001E89774779\",   \"numeroutente\": \"\",   \"uniqueid\": \"\"  } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"301856148\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA GIUSEPPE D'ABORMIDA 14 - 70038 TERLIZZI BA\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"N\",\"podPdr\":\"IT001E89774779\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2019-02-01T00:00:00.000Z\",\"dataAttivazioneProdotto\":\"2019-02-01\",\"prodotto\":\"GiustaXTe\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con il seguente TAG in Input podPdr
	 * cliente = Y per RES con fornitura IN ATTIVAZIO
     * Verificare vi sia risposta corretta e non vi sia errore
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
