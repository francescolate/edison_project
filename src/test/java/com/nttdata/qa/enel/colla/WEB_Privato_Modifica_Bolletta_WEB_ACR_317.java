package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.Cancel_Item_CF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID317;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Privato_Modifica_Bolletta_WEB_ACR_317 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "lucia@yopmail.com");//co.llaudoloyaltycolazzo.2020@gmail.com//collaudoloyaltycolazzo.2.020@gmail.com		
	//	prop.setProperty("WP_USERNAME", "collaudoloyaltycolazzo.2.020@gmail.com");	

		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("EMAIL", "fabiana.monza@nttdata.com");
		prop.setProperty("CONFERMA_EMAIL", "fabiana.monza@nttdata.com");
		prop.setProperty("NUMERO", "3669047153");
		prop.setProperty("CONFERMA_NUMERO", "3669047153");//3669047153
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("CF", "CNCRRT77C66E463N");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Bolletta_Web_ID317.main(args);
		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
		Cancel_Item_CF.main(args);
		
	
	};
	
	@After
    public void endTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	
		
	};


}
