package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.VerifyRequest_ACR293;
import com.nttdata.qa.enel.testqantt.colla.ACR_293_MoGe;
import com.nttdata.qa.enel.testqantt.colla.ACR_296_MoGe_E2E;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Dati_di_contatto_MoGe_ACR_293 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.32019p1.09statocontiweb@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);
		ACR_293_MoGe.main(args);
		VerifyRequest_ACR293.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
