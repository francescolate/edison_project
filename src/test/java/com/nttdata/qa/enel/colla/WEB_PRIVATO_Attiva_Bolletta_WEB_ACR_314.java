package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID_314;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubject_314;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Attiva_Bolletta_WEB_ACR_314 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll2.enel.it/");
		
		prop.setProperty("WP_USERNAME", "mobiletestin.ga.ut.omation@gmail.com"); //collaudoloyaltycol.azzo.20.2.0@gmail.com  
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CONFERMA_EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("NUMERO", "3687152807");
		prop.setProperty("CONFERMA_NUMERO", "3687152807");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("CF", "LLTGSN75T53L874Y");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
	
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	}
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		//LoginEnel.main(args);
		VerifyLoginEnelArea.main(args);
		Privato_Bolletta_Web_ID_314.main(args);
		VerifyEmailSubject_314.main(args);
		
		
	};
	
	@After
		public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
				
	};

}


