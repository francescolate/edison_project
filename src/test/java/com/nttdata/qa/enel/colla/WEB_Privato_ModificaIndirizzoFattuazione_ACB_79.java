package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.GetAccount;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.LoginEnelNew;
import com.nttdata.qa.enel.testqantt.colla.ModificaIndirizzoFattuazione_79;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenuNew;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_ModificaIndirizzoFattuazione_ACB_79 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "collaudolo.yaltycolazzo20.20@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("CLIENT_NUMBER", "888895230");
	    prop.setProperty("INDIRIZZI_GIA_VARIFICAT", "VIA GRAMSCI, 12 - 13040 - VERCELLI (VC)");
	    prop.setProperty("PROVINCE", "VERCELLI");
	    prop.setProperty("COMUNE", "PALAZZOLO VERCELLESE");
	    prop.setProperty("INDIRIZZO", "VIA ANTONIO GRAMSCI");
	    prop.setProperty("NUMERO_CIVICO", "12");
	    prop.setProperty("LOCALITA", "PALAZZOLO VERCELLESE");
	    prop.setProperty("CAP", "13040");
	    prop.setProperty("REGIONE", "PIEMONTE");
	    prop.setProperty("COLOR", "RED");
	    prop.setProperty("OBJECT_NAME", "MADATORYFIELD");
	    prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
        prop.setProperty("AREA_CLIENTI", "");
	    	    	   	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
//		PrivateAreaMenu.main(args);
		ModificaIndirizzoFattuazione_79.main(args);
		GetAccount.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}



