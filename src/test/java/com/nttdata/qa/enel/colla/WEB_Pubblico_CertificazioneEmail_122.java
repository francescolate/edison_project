package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.CaratteristicheDettaglioGiustaPerTeImpresaGas;
import com.nttdata.qa.enel.testqantt.colla.DocumentiGiustaPerTeImpresaGas;
import com.nttdata.qa.enel.testqantt.colla.GiustaPerTeImpresaGas;
import com.nttdata.qa.enel.testqantt.colla.ID_121_RES_SWA_ELE_OCR_DUAL_Est_Module;
import com.nttdata.qa.enel.testqantt.colla.ID_122_SWA_ELE_BSN_Estero;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.Sub_Bsn_Gas;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubject_Publico60;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmail_121;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;



public class WEB_Pubblico_CertificazioneEmail_122 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("OFFER", "Open Energy");
		prop.setProperty("OFFER_FULL", "Open Energy Digital");
		prop.setProperty("OCR_OFFER_LINK", "/it/luce-e-gas/luce-e-gas/offerte/elight-gas-elight-bioraria?formOCR=detail");
		prop.setProperty("TYPE_OF_CONTRACT", "Luce");
		prop.setProperty("PLACE", "Negozio - Ufficio");
		prop.setProperty("NEED", "CAMBIO FORNITORE");

		prop.setProperty("FIRSTNAME", "PETER");
		prop.setProperty("LASTNAME", "PARKER");
		prop.setProperty("CF", "PRKPTR72B03H502P");
		prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CAP", "60027");
		prop.setProperty("CITY", "OSIMO");
		prop.setProperty("CITY_FULL", "OSIMO, ANCONA, MARCHE");
		prop.setProperty("REGION", "MARCHE");
		prop.setProperty("ADDRESS", "VIA MONTECERNO");
		prop.setProperty("CIVIC", "3");
		prop.setProperty("SUPPLIER_LUCE", "ACEA ENERGIA SPA");
		prop.setProperty("SUPPLIER_GAS", "Eni gas e luce S.p.A.");
		prop.setProperty("POD", "IT004E86659896");
		prop.setProperty("PDR", "11690000009896");
		prop.setProperty("IBAN", "DE90602500100000691214");
		prop.setProperty("COMPLETE_ADDRESS", "VIA MONTECERNO 3, 60027, ANCONA, OSIMO, AN");


		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
	    prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	    
	};
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		
		Login_Public_Area.main(args);
		ID_122_SWA_ELE_BSN_Estero.main(args);
		//VerifyEmail_121.main(args);

	};
	
	@After
	  public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
	
}
