package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AccountAttiva;
import com.nttdata.qa.enel.testqantt.LoginSF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_326_ACR_Addebito_Diretto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Addebito_diretto_Carta_di_Credito_ACR_326 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME","wayon35550@go4mail.net");//r32019p109statocontiwe.b@gmail.com		
		prop.setProperty("WP_PASSWORD","Password01");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		
		prop.setProperty("EMAILID","wayon35550@go4mail.net");//r32019p109statocontiwe.b@gmail.com
		prop.setProperty("NUMERO","5255900260000031");
		prop.setProperty("CIRCUITO","Mastercard");
		prop.setProperty("CVV","562");
		prop.setProperty("SCANDENZAMONTH","09");
		prop.setProperty("SCANDENZAYEAR","2021");
		
		prop.setProperty("SFLINK", Costanti.salesforceLink);
		prop.setProperty("SFUSERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("SFPASSWORD",Costanti.password_salesforce_s2s_manager);
		
		prop.setProperty("CF", "SVSGNR88C01B872W");
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		
	/*	Privato_326_ACR_Addebito_Diretto.main(args);
		LoginSF.main(args);
		SbloccaTab.main(args);
		AccountAttiva.main(args);*/
		
		LoginEnel.main(args);
		Privato_326_ACR_Addebito_Diretto.main(args);
		LoginSalesForce.main(args);
//        SbloccaTab.main(args);
		AccountAttiva.main(args);
		
	};
	
	@After
    public void endTest() throws Exception{
	
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
	};

}
