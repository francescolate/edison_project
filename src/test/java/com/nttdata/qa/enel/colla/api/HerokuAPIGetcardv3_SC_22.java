// HerokuAPIGetcardv3_SC_22
// Solo per APP  - TOken in input expire 31/12/2021
/**"userUPN": "81542e4d-b642-41c8-8938-4087e6ce4207",
"cf": "SCHPRZ63S54L295F",
"enelId": "c616046a-7af9-4794-b8fa-cdd6bdd05650",
"userEmail": "raf.faellaquo.mo@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar SCHPRZ63S54L295F raffae.llaquom.o@gmail.com 81542e4d-b642-41c8-8938-4087e6ce4207 c616046a-7af9-4794-b8fa-cdd6bdd05650 2021-12-31**/
// OK _RES - Check stato Fornitura RIDOTTA

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_22 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","SCHPRZ63S54L295F");
		prop.setProperty("USERNAME","raf.faellaquo.mo@gmail.com");
		prop.setProperty("USERUPN","81542e4d-b642-41c8-8938-4087e6ce4207");
		prop.setProperty("ENELID","c616046a-7af9-4794-b8fa-cdd6bdd05650");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \""+prop.getProperty("USERNAME")+"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		
		prop.setProperty("JSON_DATA_OUTPUT", "\"commodity\":\"Elettrico\",\"pod\":\"IT001E68866428\",\"isScegliTu\":false,\"businessPartner\":\"0005010750\",\"idAsset\":\"02i0Y000000lgY8QAI\",\"subscription\":false,\"title\":\"Ridotta\",\"for_met_pag\":\"Bollettino Postale\",\"contoContrattuale\":\"001007947054\",\"offertaAttiva\":\"ENERGIAPURA CASA\",\"stato\":\"Ridotta\"");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check Stato Fornitura CESSATA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
