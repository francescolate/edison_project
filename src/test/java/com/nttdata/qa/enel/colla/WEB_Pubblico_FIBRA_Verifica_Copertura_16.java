package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.VerifyFibreAvailability;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_FIBRA_Verifica_Copertura_16 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
//	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("LINK_FIBRE_LEAD_STEP_2", "https://www-coll1.enel.it/it/fibralead_step2");
		prop.setProperty("INVALID_ADDRESS_CITY","Milano");
		prop.setProperty("INVALID_ADDRESS_STREET", "VIA KOCH ROBERT");
		prop.setProperty("INVALID_ADDRESS_STREET_NUMBER", "1/4");
		prop.setProperty("VALID_ADDRESS_CITY","Roma");
		prop.setProperty("VALID_ADDRESS_STREET", "VIA LI VIA ORESTILLA");
		prop.setProperty("VALID_ADDRESS_STREET_NUMBER", "10");
		prop.setProperty("LINK_FIBRE_OK", "https://www-coll1.enel.it/it/fibra_ok");
		prop.setProperty("LINK_LOGIN_PAGE", "https://www-coll1.enel.it/it/login?zoneid=fibra_copertura_confermata-link_contextual_accedi");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyFibreAvailability.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	
	};
	
}
