package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.RenameVerificationNegoziEnelInSpazioEnel;

public class AO007709_Cambio_Naming_Punto_Enel {

	Properties prop;
	final String nomeScenario="AO007709_Cambio_Naming_Punto_Enel.properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-colla.enel.it/");
		prop.setProperty("DESCRIPTION_UBIEST1", "SPAZIO ENEL");
		prop.setProperty("DESCRIPTION_UBIEST2", "SPAZIO ENEL PARTNER");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CHECK_LABEL","VAI ALLO SPAZIO ENEL");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	
	};
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		RenameVerificationNegoziEnelInSpazioEnel.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		
	};

}
