//HerokuAPIAuthenticate_SC_45
// Solo per APP , senza autorization , tid , sid e TOken in input 
// KO - Username not present in the database 

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAuthenticate_3;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIAuthenticate_SC_45 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	

		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/enelid/identity/v1/auth/authenticate");
		prop.setProperty("USERNAME","fa.ma@hotmail.it");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("JSON_INPUT", "{     \"data\": {         \"username\": \""+ prop.getProperty("USERNAME") +"\",         \"password\": \""+ prop.getProperty("PASSWORD") +"\"     } }");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"result\":\"KO\",\"code\":\"-3\",\"description\":\"No data retrieved\"}");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - Username not present in the database  
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAuthenticate_3.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
