/* HerokuAPIFreeSlot_SC_150 
** Case OK WEB 
* need token (cliente qualsiasi)
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIFreeSlot_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIFreeSlot_SC_150 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("POD_PDR","IT022E25779970");
		prop.setProperty("DATA_START","20190801");
		prop.setProperty("DATA_END","20190831");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");		
		prop.setProperty("CF_PIVA","PAXNDR93H23F205I");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("EXPIRATION","2021-12-31");
		
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZmVmODk2ZmUtN2I2ZC00MDI4LTg2NjQtYTkzZmI4ZTE1YjE1IiwiY29kaWNlZmlzY2FsZSI6IkRSQURSQTYwRDI0QTk0NFoiLCJpc3MiOiJhcHAiLCJleHAiOjE2MTIwODM5MjUsImVuZWxpZCI6ImJkMzNiMTg2LWZhZTctNDUxOS1hNDMwLTE0ZjQyYzljZmEwMCIsInVzZXJpZCI6InIzMjAxOXAxMDlzdGF0b2NvbnRpd2UuYkBnbWFpbC5jb20iLCJqdGkiOiI1NDI4OWRjMy03Mzg4LTRhYmQtYjdhNi02ZjlkMjQxMDdlMTYifQ.ZhhHfuMgHNcaBwq9YQlBco8XMBKmOwDhSlx8dhkgD8k");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/2g-ind/api/query/v1/freeslot/"+ prop.getProperty("POD_PDR") + "?startdate="+ prop.getProperty("DATA_START") + "&enddate="+ prop.getProperty("DATA_END"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"freeSlotList\":[{\"date\":\"20190808\",\"tipoFascia\":\"PERS\",\"slot\":\"00\"}]},\"meta\":{\"path\":\"/query/v1/freeslot/{pod}\",\"method\":\"get-free-slot\",\"dataAggiornamento\"");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Case OK WEB 
     *  need token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		// HerokuAPISubscription_Account_1.main(args); 
		HerokuAPIFreeSlot_1.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
