package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.OreFreeDettaglioFornituraFasceGiornaliere;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_ORE_FREE_DettaglioFornituraOreFree_CambioFascia_OK_67 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		//prop.setProperty("WP_LINK", "https://www-colla.enel.it");
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.3.2.0.19p109statoc.ontiweb@gmail.com");//r.3.2.0.19p109statoc.ontiweb@gmail.com//annamaria.cecili@enelsaa3010.enel.com
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("ORE_FREE_ORARIO_DI_START", "20:00");
	    prop.setProperty("CONFERMA_ANNULLA", "CONFERMA");
	   // prop.setProperty("DAY_TO_BE_ADDED", "2");
	    prop.setProperty("DAY_TO_BE_ADDED", "1");
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//DetaglioFornitura_GestiscileOreFree67.main(args);
		OreFreeDettaglioFornituraFasceGiornaliere.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};

}
