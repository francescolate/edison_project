/*HerokuAPIGetUUID_CFPIVA_SC_276B
** Chiamata API per Token
**Chiamata API GET UDID con cliente già registrato 
** Token Non valido
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID_3;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetUUID_CFPIVA_SC_276B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","01533540447");
		prop.setProperty("IDCLIENTE","businessId");		
		prop.setProperty("COUNTRY", "IT");
       	prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?");
       	prop.setProperty("TIPOCLIENTE", "BSN");
       	
//		prop.setProperty("Authorization", "Bearer af7ceced-e28a-3b0f-a408-f2f267b30e9f");
//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?" + prop.getProperty("IDCLIENTE") + "=" + prop.getProperty("CF_PIVA") + "&" + prop.getProperty("IDCLIENTE") + "Country=IT");
//		prop.setProperty("JSON_DATA_OUTPUT", "{protocol=h2, code=401, message=, url=https://uniqueid-coll.enel.com:8243/profile/account?businessId=" + prop.getProperty("CF_PIVA")+"&businessIdCountry=IT}");
		prop.setProperty("JSON_DATA_OUTPUT", "{protocol=h2, code=401, message=, url=https://uniqueid-coll.enel.com:8243/profile/account?businessId=");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Chiamata API per Token
     * Chiamata API GET UDID con cliente già registrato 
     * Token Non valido
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID_3.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
