package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.ACR_MPT_380;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MPT_RES_380 
{
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{ 
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "r.a.ff.aellaquomo@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("OPTIONS", "Inserimento dati|Preventivo|Esito ");
		prop.setProperty("POTENZA", "4.5 kW"); 
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("POWER_INPUT", "4.5 kW");
		prop.setProperty("TENZION", "220 V (tensione attuale)");
		prop.setProperty("POD", "IT002E9512155A"); 
		prop.setProperty("CELLULAR", "3339284380");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		};
		
		@Test
	    public void eseguiTest() throws Exception
		{
			String args[]= {nomeScenario};
			LoginEnel.main(args);
			ACR_MPT_380.main(args);
		};
		
		@After
	    public void fineTest() throws Exception
		{
			String args[] = {nomeScenario};
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
			ReportUtility.reportToServer(this.prop);
		};
}
