/* HerokuAPICoperturaIndirizzo_SC_216 
 * Need Token  - NEED VPN ENEL
** OK - RES - RICONTRATTUALIZZAZIONE - Espletata - APP

"userUPN": "5318676e-3037-4eeb-afaf-0a069a0dc6a6",
"cf": "NCLGPP87E48F112P",
"enelId": "18003079-add1-42de-b644-a3be38364804",
"userEmail": "r.32.019p109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_216 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","NCLGPP87E48F112P");
		prop.setProperty("USERNAME","");
		prop.setProperty("USERUPN","");
		prop.setProperty("ENELID","");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//      prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT002E4614105A\",\"statoSap\":\"KO\",\"statoSempre\":\"OK\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ2303636\",\"flagStd\":0,\"namingInFattura\":\"Energia Pura Casa Extra\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"614979268\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001n000017HRnrAAG\",\"idListino\":\"a1l1n000000s2rtAAA\",\"dataAttivazione\":\"2018-02-28T23:00:00.000Z\",\"idCaseItem\":\"a381n00000L0xnuAAB\",\"dataRichiesta\":\"2019-05-15T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y0000027yZfUAI\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA IACOPO SUIGO 33 - 00128 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT002E4614105A\",\"statoSap\":\"KO\",\"statoSempre\":\"OK\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ2303636\",\"flagStd\":0,\"namingInFattura\":\"Energia Pura Casa Extra\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"614979268\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001n000017HRnrAAG\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1n000000s2rtAAA\",\"dataAttivazione\":\"2018-02-28T23:00:00.000Z\",\"idCaseItem\":\"a381n00000L0xnuAAB\",\"dataRichiesta\":\"2019-05-15T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y0000027yZfUAI\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA IACOPO SUIGO 33 - 00128 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * OK - RES - RICONTRATTUALIZZAZIONE - Espletata - APP
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
