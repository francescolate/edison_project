/*HerokuAPIUserByEnelID_SC_50
** OK LIKE_RES - APP
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIUserByEnelID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIUserByEnelID_SC_50 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","c88b0269-daa8-4c1e-bb56-5233da6328df");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
//		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/" + prop.getProperty("ENELID"));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"user\":{\"googleId\":null,\"lastName\":\"ANDRIULO\",\"sfCreateCounter\":5,\"lingua\":null,\"companyTelNum\":null,\"personalCity\":null,\"personalAddressNumber\":null,\"companyName\":\"BE SMART DI VITO ANDRIULO\",\"personalCap\":null,\"companyAdressLine\":null,\"companyFiscalCode\":\"NDRVTI70A16B180O\",\"twitterId\":null,\"personalMobileNumber\":\"+393332192111\",\"userUpn\":\"d5f8114b-177e-4ab4-be28-9685110c4dff\",\"personalStudy\":null,");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK LIKE_RES - APP 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIUserByEnelID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
