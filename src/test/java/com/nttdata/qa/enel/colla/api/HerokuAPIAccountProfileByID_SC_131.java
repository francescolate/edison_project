/* HerokuAPIAccountProfileByID_SC_131 
** AccountProfileByID - caso OK - APP - RES
* No Token 
*PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIAccountProfileByID_SC_131 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		// prop.setProperty("Authorization", "Bearer 1");
		prop.setProperty("TID", UUID.randomUUID().toString());
		prop.setProperty("SID", UUID.randomUUID().toString()); 
//		prop.setProperty("CF","PAXNDR93H23F205I");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
			
		prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"code\":200,\"data\":{\"firstName\":\"Andrea\",\"lastName\":\"Pa\",\"phoneNumber\":\"+393312206204\",\"countryCode\":\"IT\",\"companyName\":\"\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"personalId\",\"value\":\"PAXNDR93H23F205I\"}]}],\"enelId\":\"f700815f-1ac0-4439-99ac-b47e7a53fe4f\",\"email\":\"r.32019p109stat.ocontiweb@gmail.com\",\"enabled\":true,\"vatCode\":\"\"},\"message\":\"OK\"},\"meta\":{\"path\":\"/profile/v1/account/{id}\",\"method\":\"account-profile-by-id\",\"dataAggiornamento\":");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * AccountProfileByID - caso OK - APP - RES 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		// HerokuAPISubscription_Account_1.main(args); 
		HerokuAPIAccountProfileByID_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
