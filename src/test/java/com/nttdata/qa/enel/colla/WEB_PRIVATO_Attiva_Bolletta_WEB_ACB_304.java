package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PRIVATO_Attiva_Bolletta_WEB_ACB_304;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Attiva_Bolletta_WEB_ACB_304 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK","https://www-coll2.enel.it/");
		prop.setProperty("WP_LINK","https://www-coll2.enel.it/");
		//prop.setProperty("USERNAME", "r32019p109st.atocontiwe.b@gmail.com");
		//prop.setProperty("USERNAME", "collaudoloyalt.ycolazzo20.20@gmail.com");
		
		prop.setProperty("WP_USERNAME", "m.o.bilete.st.ingautoma.tio.n@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("USERNAME", "collaudoloyaltycolazz.o2.020@gmail.com");
		prop.setProperty("PASSWORD", "Password01");
//		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//prop.setProperty("USERNAME", "r32019p109st.atocontiwe.b@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaudoloyalt.ycolazzo20.20@gmail.com");
		//prop.setProperty("WP_USERNAME", "col.laudoloyaltycolazzo2.020@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaudoloyaltycolazz.o202.0@gmail.com");
		//prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		PRIVATO_Attiva_Bolletta_WEB_ACB_304.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
