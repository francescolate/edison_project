package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.VerifyLoginPage;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.testqantt.colla.VerifyBsnSelfCareHome;

public class WEB_Pubblico_Accedi_a_MyEnel_19 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
//	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		//prop.setProperty("USERNAME", "immilezell-1212@yopmail.com");
		prop.setProperty("USERNAME", "marco.campanale@enelsaa3331.enel.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("ACCESS_PROBLEMS_STRING", "Se hai problemi di accesso clicca qui");
		prop.setProperty("LINK_FAQ", "https://www-coll1.enel.it/it/supporto/faq/faq-area-clienti");
		prop.setProperty("TITLE_FAQ", "Le FAQ di Enel Energia");
		prop.setProperty("RECOVERY_PWD_STRING_1", "Recupera password");
		prop.setProperty("RECOVERY_PWD_STRING_2", "Inserisci la username che hai usato per registrarti a Enel Energia per procedere con l'impostazione di una nuova password.");
		prop.setProperty("RECOVERY_USRNM_STRING_1", "Recupera Username");
		prop.setProperty("RECOVERY_USRNM_STRING_2", "Inserisci il tuo codice fiscale per poter recuperare il tuo username");
		prop.setProperty("REGISTER_STRING_1", "SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE");
		prop.setProperty("REGISTER_STRING_2", "Registrati nell'area riservata");
		prop.setProperty("ACCESS_ERR_STRING_1", "Username obbligatoria");
		prop.setProperty("ACCESS_ERR_STRING_2", "Password obbligatoria");
		//VerifyBsnSelfCareHome
		prop.setProperty("VERIFY_BSN_SC_TITLE", "false");
		prop.setProperty("BSN_LEFT_MENU_ITEM_LOGOUT", "LOGOUT");		
		prop.setProperty("SHOULD_LOGOUT", "true");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginPage.main(args);
		VerifyBsnSelfCareHome.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	
	};
}
