/* HerokuAPICoperturaComune_SC_165 
** Case OK - Search "rom" - WEB 
* need token (cliente qualsiasi)
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar PAXNDR93H23F205I r.32019p109stat.ocontiweb@gmail.com dccd7f1a-6fb9-462d-a2e6-73c56bd33efe f700815f-1ac0-4439-99ac-b47e7a53fe4f 2020-12-31 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaComuneIndirizzo_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPICoperturaComune_SC_165 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","PAXNDR93H23F205I");
		prop.setProperty("USERNAME","r.32019p109stat.ocontiweb@gmail.com");
		prop.setProperty("USERUPN","dccd7f1a-6fb9-462d-a2e6-73c56bd33efe");
		prop.setProperty("ENELID","f700815f-1ac0-4439-99ac-b47e7a53fe4f");
		prop.setProperty("EXPIRATION","2021-12-31");

		prop.setProperty("SEARCH","rom");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZGNjZDdmMWEtNmZiOS00NjJkLWEyZTYtNzNjNTZiZDMzZWZlIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJleHAiOjE2MTIxMDEwNTQsImVuZWxpZCI6ImY3MDA4MTVmLTFhYzAtNDQzOS05OWFjLWI0N2U3YTUzZmU0ZiIsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiNWE1MGVlODgtYmI3Yy00YWY4LWI3MjktMjM1NTQ4NzdhMDc1In0.qEoLRcNU0PHYdVJv-urPqgrFds64bsLyouZF0g4WVkY");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"data\":{\"communes\":[{\"province\":{\"name\":\"Roma\",\"id\":\"058\",\"region\":{\"name\":\"Lazio\",\"id\":\"12\"}},\"cadastralCode\":\"H501\",\"name\":\"Roma\",\"id\":\"58091\"},{\"province\":{\"name\":\"Bergamo\",\"id\":\"016\",\"region\":{\"name\":\"Lombardia\",\"id\":\"03\"}},\"cadastralCode\":\"E189\",\"name\":\"Gromo\",\"id\":\"16118\"},{\"province\":{\"name\":\"Trento\",\"id\":\"022\",\"region\":{\"name\":\"Trentino-Alto Adige/Südtirol\",\"id\":\"04\"}},\"cadastralCode\":\"H517\",\"name\":\"Romeno\",\"id\":\"22155\"},{\"province\":{\"name\":\"Sassari\",\"id\":\"090\",\"region\":{\"name\":\"Sardegna\",\"id\":\"20\"}},\"cadastralCode\":\"H507\",\"name\":\"Romana\",\"id\":\"90061\"},{\"province\":{\"name\":\"Trento\",\"id\":\"022\",\"region\":{\"name\":\"Trentino-Alto Adige/Südtirol\",\"id\":\"04\"}},\"cadastralCode\":\"H506\",\"name\":\"Romallo\",\"id\":\"22154\"}]}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Case OK - Search "rom" - WEB
     * Need Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaComuneIndirizzo_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
