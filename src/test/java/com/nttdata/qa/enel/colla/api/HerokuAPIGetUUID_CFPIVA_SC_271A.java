/*HerokuAPIGetUUID_CFPIVA_SC_271A 
** API call for Tokens
** API GET UDID call with customer already registered with CF RES 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetUUID_CFPIVA_SC_271A {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","SLNLBS68M28H501Y");
		prop.setProperty("IDCLIENTE","personalId");
		prop.setProperty("COUNTRY", "IT");
//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?" + prop.getProperty("IDCLIENTE") + "=" + prop.getProperty("CF_PIVA") + "&" + prop.getProperty("IDCLIENTE") + "Country=IT");
       	prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":200,\"data\":{\"accounts\":[{\"firstName\":\"Albus\",\"lastName\":\"Silente\",\"phoneNumber\":\"+393312206203\",\"countryCode\":\"IT\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"personalId\",\"value\":\"SLNLBS68M28H501Y\"}]}],\"enelId\":\"de8f7bc2-3c7d-4784-9009-00e58d97546c\",\"email\":\"r.32019p109statoconti.web@gmail.com\",\"enabled\":true}]},\"message\":\"OK\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Chiamata API GET UDID by Email con email esistente e cliente già registrato RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
