package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Modifica_Password_ACR_379;
import com.nttdata.qa.enel.testqantt.colla.Privato_Modifica_Password_ACR_379_PwdReset;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Modifica_Password_ACR_379 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	//	prop.setProperty("WP_USERNAME", "r.3.2.0.19p109statocontiw.eb@gmail.com"); //yxavepydd-2370@yopmail.com
		prop.setProperty("WP_USERNAME", "yxavepydd-2370@yopmail.com");
		prop.setProperty("WP_PASSWORD", "Password01"); 
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("NuovaValue1", "Password02");
		prop.setProperty("ConfermaValue1", "Password02");
		prop.setProperty("NuovaValue2", "Password01");
		prop.setProperty("ConfermaValue2", "Password01");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Modifica_Password_ACR_379.main(args);
		Privato_Modifica_Password_ACR_379_PwdReset.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);  
	};
}