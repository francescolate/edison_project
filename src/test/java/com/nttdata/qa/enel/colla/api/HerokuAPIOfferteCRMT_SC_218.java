/* HerokuAPIOfferteCRMT_SC_218 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - Subentro Espletato

"userUPN": "7ccb61f5-32be-4663-9ba8-b45cbefa141b",
"cf": "BGLGPP32L14E506L",
"enelId": "2feb6bb1-88b3-44d7-8419-d2e6788728f0",
"userEmail": "r.3201.9p109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_218 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","BGLGPP32L14E506L");
		prop.setProperty("USERNAME","r.3201.9p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","7ccb61f5-32be-4663-9ba8-b45cbefa141b");
		prop.setProperty("ENELID","2feb6bb1-88b3-44d7-8419-d2e6788728f0");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//      prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"05262321548562\",\"statoSap\":\"KO\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3100223\",\"flagStd\":1,\"namingInFattura\":\"GiustaXTe Gas\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"310499861\",\"processoInCorso\":\"SUBENTRO\",\"idCase\":\"5001l000003my0RAAQ\",\"idListino\":\"a1l1l000000OPSHAA4\",\"dataAttivazione\":\"2020-03-30T00:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000TRJXEA4\",\"dataRichiesta\":\"2020-03-28T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwB4UAI\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA ACHILLE BIZZONI 4 - 20125 MILANO MI\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"05262321548562\",\"statoSap\":\"KO\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3100223\",\"flagStd\":1,\"namingInFattura\":\"GiustaXTe Gas\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"310499861\",\"processoInCorso\":\"SUBENTRO\",\"idCase\":\"5001l000003my0RAAQ\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OPSHAA4\",\"dataAttivazione\":\"2020-03-30T00:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000TRJXEA4\",\"dataRichiesta\":\"2020-03-28T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwB4UAI\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA ACHILLE BIZZONI 4 - 20125 MILANO MI\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false},{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"In attesa\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Dual Energy\",\"podPdr\":\"00885252470215\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3100218\",\"flagStd\":1,\"namingInFattura\":\"GiustaXTe Gas\",\"statoR2d\":\"Verificato\",\"numeroUtente\":\"310499837\",\"processoInCorso\":\"PRIMA ATTIVAZIONE\",\"idCase\":\"5001l000003mxzxAAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OPSHAA4\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000TREZEA4\",\"dataRichiesta\":\"2020-03-28T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwB4UAI\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA ACHILLE LORIA 4 - 10128 TORINO TO\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false},{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E59228758\",\"statoSap\":\"KO\",\"statoSempre\":\"BOZZA\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3089930\",\"flagStd\":0,\"namingInFattura\":\"BIORARIA\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"310494968\",\"processoInCorso\":\"VOLTURA CON ACCOLLO\",\"idCase\":\"5001l000003lMapAAE\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l0Y000000XB5TQAW\",\"dataAttivazione\":\"2012-10-31T23:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000KA66EAG\",\"dataRichiesta\":\"2020-02-25T00:00:00.000Z\",\"idProdotto\":\"a1Y2400000BFIoIEAX\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA POGGIO 292 - 47862 MAIOLO RN\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false},{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":null,\"podPdr\":\"IT001E73469088\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":null,\"flagStd\":0,\"namingInFattura\":\"SEMPLICE LUCE\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"771462202\",\"processoInCorso\":\"VOLTURA CON ACCOLLO\",\"idCase\":\"5000Y00000N6I8JQAV\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l0Y000000HH42QAG\",\"dataAttivazione\":\"2015-05-31T22:00:00.000Z\",\"idCaseItem\":\"a1Q0Y00000GPqxrUAD\",\"dataRichiesta\":null,\"idProdotto\":\"a1Y2400000BFIpwEAH\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA FORTORE 3 - 73100 LECCE LE\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Cessazione\",\"processoCombinato\":false},{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":null,\"podPdr\":\"IT001E67352667\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":null,\"flagStd\":0,\"namingInFattura\":\"ENERGIAPURA CASA\",\"statoR2d\":\"NON PREVISTO\",\"numeroUtente\":\"625950899\",\"processoInCorso\":\"VOLTURA SENZA ACCOLLO\",\"idCase\":\"5000Y000008ho0cQAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l24000001KwsLAAS\",\"dataAttivazione\":\"2012-02-16T23:00:00.000Z\",\"idCaseItem\":\"a1Q0Y000008KPiTUAW\",\"dataRichiesta\":null,\"idProdotto\":\"a1Y2400000BFIgYEAX\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA V AMENDOLA G. 41 - 67051 AVEZZANO AQ\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Cessazione\",\"processoCombinato\":false},{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E73469088\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"029368748\",\"flagStd\":0,\"namingInFattura\":\"Semplice Luce\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"771462202\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5000Y000004VMBjQAO\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l24000001KzhpAAC\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q0Y000006IXvAUAW\",\"dataRichiesta\":\"2015-03-13T00:00:00.000Z\",\"idProdotto\":\"a1Y2400000BFKfwEAH\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA FORTORE 3 - 73100 LECCE LE\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false},{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Ok\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E67352667\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"006569865\",\"flagStd\":0,\"namingInFattura\":\"ENERGIAPURA CASA\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"625950899\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5000Y000004VMBlQAO\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l24000001KutRAAS\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q0Y00000A1aVXUAZ\",\"dataRichiesta\":\"2007-12-28T00:00:00.000Z\",\"idProdotto\":\"a1Y2400000BFKZaEAP\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA V AMENDOLA G. 41 - 67051 AVEZZANO AQ\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
        
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - Subentro Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
