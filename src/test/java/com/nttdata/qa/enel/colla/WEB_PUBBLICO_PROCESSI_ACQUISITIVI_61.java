package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.VerifyAccountStatusType;
import com.nttdata.qa.enel.testqantt.colla.Pubblico_ID_61_Processo_A_RES_SWA_ELE;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PUBBLICO_PROCESSI_ACQUISITIVI_61 {

	Properties prop;
	
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
//	final String BASE_LINK = "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/speciale-luce-60";
//	final String HOMEPAGE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/speciale-luce-60");
		prop.setProperty("HOMEPAGE", "https://www-coll1.enel.it/");
		prop.setProperty("ADISIONE", "https://www-coll1.enel.it/it/adesione");
		prop.setProperty("NOME", "Farfalla");
		prop.setProperty("COGNOME", "Farfallin");
		prop.setProperty("CODICE_FISCALE", "FRFFFL73A41H501N");
		prop.setProperty("TELEFONO", "3434343434");
		prop.setProperty("EMAIL", "sampletestingcrmautomation123@gmail.com");
		prop.setProperty("CAP", "60027 OSIMO");
		prop.setProperty("CITTA", "OSIMO (ANCONA) MARCHE");
		prop.setProperty("INDIRIZZO", "Via abbadia");
		prop.setProperty("NUMERICO_CIVICO", "33");
		prop.setProperty("ATTUALE_FORNITORE", "ACEA ENERGIA SPA - Libero");
		prop.setProperty("INFORMAZIONI_FORNITURA_HEADING", "Informazioni fornitura");
		prop.setProperty("INFORMAZIONI_FORNITURA_HEADING_SUBTEXT", "Di cosa hai bisogno ?");
		prop.setProperty("IF_RADIO_BUTTON_1", "CAMBIO FORNITORE");
		prop.setProperty("IF_RADIO_BUTTON_1_DESCRIPTION", "Ho gia un contratto con un altro fornitore ma vorrei passare ad Enel");
		prop.setProperty("IF_RADIO_BUTTON_2", "PRIMA ATTIVAZIONE");
		prop.setProperty("IF_RADIO_BUTTON_2_DESCRIPTION", "Voglio attivare un nuovo contatore");
		prop.setProperty("IF_RADIO_BUTTON_3", "SUBENTRO");
		prop.setProperty("IF_RADIO_BUTTON_3_DESCRIPTION", "Voglio attivare un contatore disattivato");
		prop.setProperty("IF_RADIO_BUTTON_4", "VOLTURA");
		prop.setProperty("IF_RADIO_BUTTON_4_DESCRIPTION", "Voglio cambiare intestatario a un contratto di fornitura");
		prop.setProperty("POD_HEADING", "POD*");
		prop.setProperty("CAP_HEADING", "CAP*");
		prop.setProperty("ITUOI_DATI_HEADING", "I tuoi dati");
		prop.setProperty("RECAPITI_HEADING", "Recapiti");
		prop.setProperty("RECAPITI_QUESTION_1", "Sei residente presso la fornitura?");
		prop.setProperty("RECAPITI_QUESTION_2", "Dove vuoi ricevere le comunicazioni relative alla tua fornitura?");
		prop.setProperty("PAGAMENTI_HEADING", "Pagamenti e Bollette");
		prop.setProperty("METODO_DI_PAGAMENTO", "Metodo di Pagamento*");
		prop.setProperty("CODICE_IBAN", "Codice IBAN*");
		prop.setProperty("MODALITA_DI_RICEZIONE", "Modalità di ricezione della bolletta*");
		prop.setProperty("CODICI_PROMOZIONALI_HEADING", "Codici promozionali");
		prop.setProperty("CODICI_PROMOZIONALI_SUBTEXT", "Se sei in possesso di un codice promozionale inseriscilo nel campo seguente");
		prop.setProperty("MANDATI_E_CONSENSI_HEADING", "Mandati e Consensi");
		prop.setProperty("CONSENSI_MARKETING_ENEL_ENERGIA_HEADING", "Consenso Marketing Enel Energia");
		prop.setProperty("CONSENSI_MARKETING_TERZI_HEADING", "Consenso marketing terzi");
		prop.setProperty("CONSENSI_PROFILAZIONE_ENEL_ENERGIA_HEADING", "Consenso profilazione Enel Energia");
		prop.setProperty("ACCETTO", "Accetto");
		prop.setProperty("NON_ACCETTO", "Non accetto");
		prop.setProperty("TORNA_ALLA_HOMEPAGE_TITLE", "Sei ad un passo dal completare l'adesione!");
		prop.setProperty("TORNA_ALLA_HOMEPAGE_MESSAGE2", "Se non dovessi riceverla, verifica la casella della posta indesiderata");
		prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Pubblico_ID_61_Processo_A_RES_SWA_ELE.main(args);
		VerifyAccountStatusType.main(args);
		
	};
	
	@After
    public void endTest() throws Exception{
		String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
