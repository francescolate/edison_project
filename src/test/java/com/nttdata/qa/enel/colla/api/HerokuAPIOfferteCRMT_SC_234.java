/* HerokuAPICoperturaIndirizzo_SC_234 
 * Need Token  - NEED VPN ENEL
** Case KO - Offers CRMT - WEB - RES - Prospect Senza Offerte

CF GRZRNN34L52A182N
 "enelId": "242cab5f-dc89-46dc-8f74-05fd01185d34",
"email": "r.32019p109statocontiwe.b@gmail.com",
   "userUpn": "b610522f-b559-421f-8d77-92c358136a81",
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_234 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
//		prop.setProperty("CF_PIVA","GRZRNN34L52A182N");
//		prop.setProperty("USERNAME","r.32019p109statocontiwe.b@gmail.com");
//		prop.setProperty("USERUPN","b610522f-b559-421f-8d77-92c358136a81");
//		prop.setProperty("ENELID","242cab5f-dc89-46dc-8f74-05fd01185d34");
		prop.setProperty("EXPIRATION","2021-12-31");
		
        prop.setProperty("CF_PIVA","CSCBMN65L28F839X");
        prop.setProperty("USERNAME","r32019p109.statocontiwe.b@gmail.com");
        prop.setProperty("USERUPN","ca1dd1ff-5efa-4daf-99f9-5b235596aa1f");
        prop.setProperty("ENELID","f613ea45-3f3b-4514-b765-878f978fdbb4");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJFRTAwMDAwMTgxNTJAQk9YLkVORUwuQ09NIiwiYXVkIjoiQXBwTW9iaWxlIiwiY29kaWNlZmlzY2FsZWNvdW50cnkiOiJJVCIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZW5lbGlkIjoiZTY0NzE0NTktZDY4Zi00OTUyLTg0ZjQtNTQ5MzFiOGY2ODczIiwiZXhwIjoxNjA3Njk0MDE2LCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZDQyNDcwOGEtMjcwNS00NTE2LWFmYjAtOTgzODQ4ZmJkZmRjIiwic2lkIjoiN2FiOTRlMGItODA0Mi00ZjA5LTg1NTAtYjM0ODRiMWNmOGMyIn0.1jyqWzsW80_DxuiAfoliy0ZhSEwIA_qMP1W-0EWkvyY");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
       prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"offerte\":[],\"totaleStd\":0},\"status\":{\"descrizione\":\"Not found\",\"esito\":\"KO\",\"codice\":\"001\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case KO - Offers CRMT - WEB - RES - Prospect Senza Offerte
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
