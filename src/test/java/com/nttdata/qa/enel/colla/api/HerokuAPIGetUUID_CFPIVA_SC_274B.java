/*HerokuAPIGetUUID_CFPIVA_SC_274B
** API call for Tokens
** API GET UDID call with businessId empy 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID_2;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetUUID_CFPIVA_SC_274B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","");
		prop.setProperty("IDCLIENTE","businessId");
		prop.setProperty("COUNTRY", "IT");
       	prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?");

//		prop.setProperty("Authorization", "Bearer " + APIService.getToken());
//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?" + prop.getProperty("IDCLIENTE") + "=" + prop.getProperty("CF_PIVA") + "&" + prop.getProperty("IDCLIENTE") + "Country=IT");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":400,\"message\":\"Bad Request\",\"errors\":{\"businessId\":\"Field 'businessId' must be a valid 'businessId', '' given\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * API GET UDID call with businessId empy
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
