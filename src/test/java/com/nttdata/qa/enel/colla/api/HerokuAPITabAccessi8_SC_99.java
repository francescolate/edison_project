/*HerokuAPITabAccessi8_SC_99 
** API call for Tokens
* TabAccessi8 - KO UserUPN null
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar LNERFL38B08H643E r32.019p109statocontiweb@gmail.com fede4574-670a-4668-a0a8-1990f0032768 6b789a66-ec0c-4f05-b51d-0d89b98f5f4f 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPITabAccessi8;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPITabAccessi8_SC_99 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","LNERFL38B08H643E");
		prop.setProperty("USERNAME","r32.019p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","");
		prop.setProperty("ENELID","6b789a66-ec0c-4f05-b51d-0d89b98f5f4f");
		prop.setProperty("EXPIRATION","2021-12-31");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZWRlNDU3NC02NzBhLTQ2NjgtYTBhOC0xOTkwZjAwMzI3NjgiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IkxORVJGTDM4QjA4SDY0M0UiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiI2Yjc4OWE2Ni1lYzBjLTRmMDUtYjUxZC0wZDg5Yjk4ZjVmNGYiLCJleHAiOjE2MDc3MDc3NTIsInVzZXJpZCI6InIzMi4wMTlwMTA5c3RhdG9jb250aXdlYkBnbWFpbC5jb20iLCJqdGkiOiI3YWJkYWE1Yy0yNzk5LTQ0YTgtOThkNC1iM2I4NzY1OTNmYjMiLCJzaWQiOiI5YjQ1ZWI0Ni1mMDFkLTQzNTEtODhiYS1kOGQzYjU4MzUwYjkifQ.G43VNmd6j8b4pKHuRIv69DvVdNxrdxDN8zdhi1Wosj8");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-webws1-stage/v1/accessi/tab-accessi8?userUPN=" + prop.getProperty("USERUPN"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-webws1-stage/v1/accessi/tab-accessi8?userUPN=");
       prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":null,\"status\":{\"descrizione\":\"UserUpn mancante\",\"esito\":1,\"codice\":\"WS1-01\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * TabAccessi8 - KO UserUPN null 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPITabAccessi8.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
