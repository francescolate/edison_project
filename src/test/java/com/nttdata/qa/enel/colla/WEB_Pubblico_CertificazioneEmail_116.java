package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_116_RES_SUB_GAS_OCR;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent116;
import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetails_116;
import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetailsEmail_116;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;



public class WEB_Pubblico_CertificazioneEmail_116 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("NOME", "PETER");
	    prop.setProperty("COGNOME", "PARKER");
	    prop.setProperty("CELLULARE", "7874637282");
	    prop.setProperty("CF", "PRKPTR72B03H502P");
	    prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
	    prop.setProperty("CAP", "60027-OSIMO");
	    prop.setProperty("PDR", "11670000000000");
	    prop.setProperty("CITTA", "OSIMO,ANCORA, MARCHE");
	    prop.setProperty("INDRIZZO", "VIA MONTECERNO");
	    //prop.setProperty("Codici_Promozionali',23455");
		prop.setProperty("NUMEROCIVICO", "3");		
		prop.setProperty("ATTUALE_FORNITORE", "ACEA ENERGIA SPA");
		prop.setProperty("USERNAME", "testing.crm.automation@gmail.com");
		prop.setProperty("PASSWORD",Costanti.password_Tp2_mobile);	    
	    prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");       
	    prop.setProperty("ACCOUNT_ID", "0010Y00000E9SHQQA3");

			
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		ID_116_RES_SUB_GAS_OCR.main(args);
		VerifyRequestDetails_116.main(args);
		VerifyEmailSubjectAndContent116.main(args);
		VerifyRequestDetailsEmail_116.main(args);
		
	
	};
	
	@After
   
		public void fineTest() throws Exception{
			InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
		};
	
}
