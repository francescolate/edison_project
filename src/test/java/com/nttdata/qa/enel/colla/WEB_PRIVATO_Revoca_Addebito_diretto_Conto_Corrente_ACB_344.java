package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Privato_344_ACB_Addebito_Diretto;

public class WEB_PRIVATO_Revoca_Addebito_diretto_Conto_Corrente_ACB_344 {

	Properties  prop;
	final String nomeScenario="WEB_PRIVATO_Revoca_Addebito_diretto_Conto_Corrente_ACB_344.properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("LINK","https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "testimpresa@yopmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		Privato_344_ACB_Addebito_Diretto.main(args);
		
	}
	
	@After
	public void fineTest() throws Exception
	{
		
	}

}