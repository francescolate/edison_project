package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginValidate_Pubblico_39;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Accedi_a_My_Enel_39 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "r32019p109statocontiwe.b@gmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("INVALIDUSER", "ABJ@@@");
		prop.setProperty("INVALIDPASSWD", "123");
		//String BASE_LINK = prop.getProperty("LINK");
		prop.setProperty("URL", "https://www-coll1.enel.it/it");
		prop.setProperty("ACCESS_PROBLEMS_STRING", "Se non riesci ad accedere oppure vuoi ricevere informazioni sul Profilo Unico clicca qui");
//		prop.setProperty("LINK_FAQ", "https://www-coll1.enel.it/" + "it/supporto/faq/faq-area-clienti");
//		prop.setProperty("TITLE_FAQ", "Le FAQ di Enel Energia - -coll1.enel.it");
		prop.setProperty("HOMETITLE", "Entra nel Mercato Libero: Offerte Luce e Gas | Enel Energia");
		//prop.setProperty("HOMETITLE", "Entra nel Mercato Libero: Offerte Luce e Gas | Enel Energia - -coll1.enel.it");
		prop.setProperty("USERNAME_VALIDATE", "Username obbligatoria");
		prop.setProperty("PASSWD_VALIDATE", "Password obbligatoria");
		prop.setProperty("USERNAME_INVALID", "Username non valida");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginValidate_Pubblico_39.main(args);
		
	};
	
	@After
	 public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };
}
