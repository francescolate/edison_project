/* HerokuAPIAccountProfileByID_SC_132 
** AccountProfileByID - caso OK - APP - BSN
01533540447 r.32019p109statoc.ontiweb@gmail.com e66bc31c-44a5-4ed6-a76b-39a5808567da 998777cd-ccbc-45e8-b013-2578bc623fff 2020-12-31
* No Token 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_2;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIAccountProfileByID_SC_132 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","998777cd-ccbc-45e8-b013-2578bc623fff");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		prop.setProperty("TID", UUID.randomUUID().toString());
		prop.setProperty("SID", UUID.randomUUID().toString()); 
		//prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));

		prop.setProperty("Authorization", "1");
		prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"code\":200,\"data\":{\"firstName\":\"BRUNA\",\"lastName\":\"MASSACCI\",\"phoneNumber\":\"+393209090191\",\"countryCode\":\"IT\",\"companyName\":\"ASSIMETA SAS\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"businessId\",\"value\":\"01533540447\"}]}],\"enelId\":\"998777cd-ccbc-45e8-b013-2578bc623fff\",\"email\":\"r.32019p109statoc.ontiweb@gmail.com\",\"enabled\":true,\"vatCode\":\"01533540447\"},\"message\":\"OK\"},\"meta\":{\"path\":\"/profile/v1/account/{id}\",\"method\":\"account-profile-by-id\",\"dataAggiornamento\":");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * AccountProfileByID - caso OK - APP - BSN 
     * No Token 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccountProfileByID_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
