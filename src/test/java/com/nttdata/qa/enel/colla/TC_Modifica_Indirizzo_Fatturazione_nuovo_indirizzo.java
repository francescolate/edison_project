package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.ModificaIndirizzoFatturazione;
import com.nttdata.qa.enel.testqantt.colla.RenameVerificationNegoziEnelInSpazioEnel;

public class TC_Modifica_Indirizzo_Fatturazione_nuovo_indirizzo {

	Properties prop;
	final String nomeScenario="ScenarioRenameVerificationNegoziInSpazio.properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-colla.enel.it/");
		prop.setProperty("USERNAME", "marco.campanale@enelsaa3331.enel.com");
		prop.setProperty("PASSWORD", "Password01");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	
	};
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		ModificaIndirizzoFatturazione.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		
	};

}

