/*HerokuAPIUserByEnelID_SC_61
** OK LIKE_RES - WEB
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIUserByEnelID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIUserByEnelID_SC_61 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","c88b0269-daa8-4c1e-bb56-5233da6328df");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
//		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/" + prop.getProperty("ENELID"));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/");
        prop.setProperty("JSON_DATA_OUTPUT", "\"fiscalCode\":\"NDRVTI70A16B180O\",\"companyCountry\":null,\"userEmail\":\"affiddaqe-8312@yopmail.com\",\"enelId\":\"c88b0269-daa8-4c1e-bb56-5233da6328df\",\"disattivazione\":null,\"companyCity\":null,\"sendEnelIdToSf\":true,\"personalJob\":null,\"facebookId\":null,\"isAttivo\":1,\"companyVatCode\":\"02475860744\",\"companyAdressNum\":null,\"personalCountry\":null,\"companyCap\":null,\"emailPreRegistrazione\":null,\"birthDate\":null,\"personalProvince\":null,\"personalTelephoneNumber\":\"+393332192111\",\"firstName\":\"VITO\",\"personalAddressLine\":null,\"companyFaxNum\":\"NDRVTI70A16B180O\",\"dataCreation\":1574726400000,\"sesso\":null}},\"meta\":{\"path\":\"/user/v1/userbyenelid/{enelId}\",\"method\":\"get-user-by-enel-id\",\"dataAggiornamento\"");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK LIKE_RES - WEB 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIUserByEnelID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
