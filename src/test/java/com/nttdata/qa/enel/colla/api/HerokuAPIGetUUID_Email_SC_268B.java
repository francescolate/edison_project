/*HerokuAPIGetUUID_Email_SC_268B 
** API call for Tokens
** Chiamata API GET UDID by Email con email esistente e cliente già registrato BSN
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID_MAIL_1;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetUUID_Email_SC_268B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("EMAIL","r.32019p109statoc.ontiweb@gmail.com");
//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?email=" + prop.getProperty("EMAIL"));
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?email=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":200,\"data\":{\"accounts\":[{\"firstName\":\"BRUNA\",\"lastName\":\"MASSACCI\",\"phoneNumber\":\"+393209090191\",\"countryCode\":\"IT\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"businessId\",\"value\":\"01533540447\"}]}],\"enelId\":\"998777cd-ccbc-45e8-b013-2578bc623fff\",\"email\":\"r.32019p109statoc.ontiweb@gmail.com\",\"enabled\":true}]},\"message\":\"OK\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Chiamata API GET UDID by Email con email esistente e cliente già registrato BSN
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID_MAIL_1.main(args); 
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
