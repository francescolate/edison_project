/*HerokuAPIFornitureACB_SC_284 
** API call for Tokens - Need VPN ENEL
* Fornitura ACB - OK - WEB - BSN Stato Fornitura RIDOTTA  
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 93069180755 r.32019p109s.tatocontiweb@gmail.com 13367ddb-3f59-471e-b720-6f99b871a408 a21ed51c-f996-4e3c-a122-ecaf85bff252 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIFornitureACB;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIFornitureACB_SC_284 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("PIVA","93069180755");
		prop.setProperty("USERNAME","r.32019p109s.tatocontiweb@gmail.com");
		prop.setProperty("USERUPN","13367ddb-3f59-471e-b720-6f99b871a408");
		prop.setProperty("ENELID","a21ed51c-f996-4e3c-a122-ecaf85bff252");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiMTMzNjdkZGItM2Y1OS00NzFlLWI3MjAtNmY5OWI4NzFhNDA4IiwiY29kaWNlZmlzY2FsZSI6IjkzMDY5MTgwNzU1IiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMDg5NDc4LCJlbmVsaWQiOiJhMjFlZDUxYy1mOTk2LTRlM2MtYTEyMi1lY2FmODViZmYyNTIiLCJ1c2VyaWQiOiJyLjMyMDE5cDEwOXMudGF0b2NvbnRpd2ViQGdtYWlsLmNvbSIsImp0aSI6IjNiMTMzNjc3LWQwZGEtNDEzOS1hODBiLTc3ZDA0NzAwMGU2ZiJ9.-JQeYjQd3uBK21V6cmLKq5SgAzt7Cp3TyQNOKyc7__U");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-extra-stage/v1/info/ricercaFornitureAcb?cf=" + prop.getProperty("CF_PIVA") + "&numMesiMaxCess=12");
		prop.setProperty("JSON_DATA_OUTPUT","{\"data\":{\"forniture\":[{\"idAccount\":\"0010Y00000FS0G3QAL\",\"numeroUtente\":\"744841161\",\"listinoInFattura\":\"Senza Orari Luce\",\"partitaIvaCliente\":\"93069180755\",\"idAsset\":\"02i0Y000000l3jhQAA\",\"dataAttivazione\":\"2015-12-31T23:00:00.000Z\",\"contrattoSAP\":\"0026150028\",\"codiceFiscale\":\"93069180755\",\"podPdr\":\"IT001E73436801\",\"stato\":\"Ridotto\",\"tipologiaCommodity\":\"Elettrico\",\"aliasAccount\":null,\"indirizzoFornitura\":\"VIA LEUCA 14 - 73020 CAVALLINO LE\",\"alias\":null,\"bpAccountSAP\":\"0030781229\",\"contractAccountSAP\":\"001021655337\",\"citta\":\"CAVALLINO\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}"); 
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens - Need VPN ENEL
     * Fornitura ACB - OK - WEB - BSN Stato Fornitura RIDOTTA  
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIFornitureACB.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
