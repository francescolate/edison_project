package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_67_Verifica_Consumi_Concord;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PUBBLICO_VERIFICA_CONSUMI_67 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("INVALID_EMAIL", "abcd@@@@");
		prop.setProperty("PARTITA_IVA", "DASFASFASFA");
		prop.setProperty("PARTITA_IVA", "A1a32323151");
		prop.setProperty("INVALID_TELEFONO", "Ayrssdgdfg");
		prop.setProperty("NUMERO_CLIENTE", "abcdfgdg");
		prop.setProperty("MISURATORE", "abcdfgdg");
		prop.setProperty("POD", "1111");
		prop.setProperty("LETTURA", "aaaa");
		prop.setProperty("AMMINISTRATORE_DI_CONDOMINIO", "GIOCONDA BALZO");
		prop.setProperty("CODICE_FISCALE_IP", "BLZGND65A46L049K");
		prop.setProperty("PARTITA_IVA_IP", "90097060736");
		prop.setProperty("DENOMINAZIONE_CONDOMINIO_IP", "CONDOMINIO PRESSO CONDOMINIO VIA BRUNELL");
		prop.setProperty("NUMERO_CLIENTE_IP", "777825292");
		prop.setProperty("EMAIL", "raffaellaquomo@gmail.com");
		prop.setProperty("TELEFONO_IP", "333323231");
		prop.setProperty("POD_IP", "IT001E74380467");
		prop.setProperty("LETTURA_IP", "1");

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		ID_67_Verifica_Consumi_Concord.main(args);		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
