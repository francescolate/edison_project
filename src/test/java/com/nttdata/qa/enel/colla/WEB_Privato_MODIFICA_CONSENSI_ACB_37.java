package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.DataConfiguration;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyRequestDetails_277;
import com.nttdata.qa.enel.testqantt.colla.Add_Query_34_36_37;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.ModifyConsents_Bsn;
import com.nttdata.qa.enel.testqantt.colla.ModifyConsents_Bsn_ModAll;
import com.nttdata.qa.enel.testqantt.colla.ModifyConsents_Bsn_SF_reset;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu42;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MODIFICA_CONSENSI_ACB_37 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", BASE_LINK);
		//prop.setProperty("WP_USERNAME", "r.32019p109statoc.ontiweb@gmail.com");
		prop.setProperty("WP_USERNAME", "affiddaqe-8312@yopmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("CF", "01533540447");
		
		prop.setProperty("AREA_CLIENTI", "BSN");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		
		prop.setProperty("ALL_CONSENTS", "NO");
		
		prop.setProperty("QUERY", "");
		prop.setProperty("QUERY_PROPERTIES", "QUERY");
		prop.setProperty("QUERY_REFERENCES", "query1_ACB_32_to_37");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("LINK", Costanti.salesforceLink);
	    prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		/*DataConfiguration.main(args);
		LoginEnel.main(args);
		//PrivateAreaMenu42.main(args);
		ModifyConsents_Bsn.main(args);
		ModifyConsents_Bsn_ModAll.main(args);
		//RecuperaDatiWorkbench.main(args);
		//Let's change the value of the "QUERY" property, so that we can execute the second query
		Add_Query_34_36_37.main(args);
		DataConfiguration.main(args);
		//RecuperaDatiWorkbench.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args); 
		ModifyConsents_Bsn_SF_reset.main(args);*/
		
		
	//Testing samples
		
		//LoginSalesForce.main(args);
		VerifyRequestDetails_277.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		   /* InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);*/
	
	};
}
