//HerokuAPILoginstrong_SC_3
// Solo per APP , senza autorization e Token in input  
// KO - No Username 

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPILoginstrong_2;
import com.nttdata.qa.enel.util.ReportUtility;
import java.util.UUID;

public class HerokuAPILoginstrong_SC_3 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/authentication/v3/loginstrong");
	    prop.setProperty("USERNAME"," ");
	    prop.setProperty("PASSWORD","Password01");
		prop.setProperty("OSVER","7.0");
		prop.setProperty("IDDISPOSITIVO","MWS0216808002415");
		prop.setProperty("DEVICE","HUAWEI EVA-L09");
		prop.setProperty("CONTEXT","app_ee");
		prop.setProperty("RESOLUTION","1080x1794");	  
		//prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",\"username\": \"\", \"keys\": [ { \"key\": \"APP_VER\", \"value\": \"10.0.1\" }, { \"key\": \"OS\", \"value\": \"ANDROID\" }, { \"key\": \"APP\", \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\" }, { \"key\": \"ID_DISPOSITIVO\", \"value\": \"MWS0216808002415\" } ] }, \"data\": { \"username\": \"\", \"password\": \"Password01\", \"migrationToken\": \"\", \"oidcToken\": \"\", \"pwd_crypt\": \"\", \"resolution\": \"1080x1794\", \"osVer\": \"7.0\", \"idDispositivo\": \"MWS0216808002415\", \"appVer\": \"10.0.1\", \"os\": \"ANDROID\", \"context\": \"app_ee\", \"device\": \"HUAWEI EVA-L09\" }}");
	    //prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",          \"username\": \""+ prop.getProperty("USERNAME") +"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     },     \"data\": {         \"username\": \""+ prop.getProperty("USERNAME") +"\",         \"password\": \""+ prop.getProperty("PASSWORD") +"\",         \"migrationToken\": \"\",         \"oidcToken\": \"\",         \"pwd_crypt\": \"\",         \"resolution\": \"1080x1794\",         \"osVer\": \"7.0\",         \"idDispositivo\": \"MWS0216808002415\",         \"appVer\": \"10.0.1\",         \"os\": \"ANDROID\",         \"context\": \"app_ee\",         \"device\": \"HUAWEI EVA-L09\"     } }");
	    //prop.setProperty("JSON_DATA_OUTPUT", "{\"result\":\"AUT01\",\"code\":\"-1\",\"description\":\"Username o password non riconosciute. Ti ricordiamo che per poter accedere alla App e' necessario aver completato la registrazione sul portale enelenergia.it\"}");
	    prop.setProperty("JSON_DATA_OUTPUT", "{\"result\":\"GEN10\",\"code\":\"-1\",\"description\":\"Gentile cliente, i nostri sistemi sono in manutenzione per migliorare la qualità dei servizi. Ti invitiamo ad accedere più tardi. Ci scusiamo per il disagio.\"}");

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - No Username 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPILoginstrong_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
