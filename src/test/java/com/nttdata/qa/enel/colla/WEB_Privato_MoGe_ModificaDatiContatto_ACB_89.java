package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.GetBSNInformation;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaAccountBSN;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MoGe_ModificaDatiContatto_ACB_89 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	  //  prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com");
	    prop.setProperty("WP_USERNAME", "collaudoloyaltycolazzo.20.20@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("TITOLARE", "IDEAL COLOR SNC");
	    prop.setProperty("CF", "03430350177");
	    prop.setProperty("TELEFONO", "+390818890127");
	    prop.setProperty("EMAIL", "fabiana.manzo31@nttdata.com");
	    prop.setProperty("PEC", "idealcolorsnc@comunicapec.it");
	    prop.setProperty("EMPTY_EMAIL", "");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
	    prop.setProperty("ACCOUNT_ID", "0010Y00000FqA99QAF");
	    prop.setProperty("CHANGE_EMAIL_AND_TEL", "081924489:automation@yopmail.com");
	    prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		GetBSNInformation.main(args);
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);
		PrivateAreaAccountBSN.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
