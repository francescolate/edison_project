package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.MotorinoDiRicerca;
import com.nttdata.qa.enel.testqantt.colla.ScegliPrimaDomandaOffertaScegliTu100x100;
import com.nttdata.qa.enel.testqantt.colla.ScegliTerzaDomandaOffertaScegliTu100x100;
import com.nttdata.qa.enel.testqantt.colla.ScegliTu100x100;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Pubblico_MOTORINO_OFFERTA_SCEGLI_TU_100X100_26 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("TYPE_OF_CONTRACT", "Luce");
		prop.setProperty("PLACE", "Casa");
		prop.setProperty("NEED", "VISUALIZZA TUTTE");
		prop.setProperty("RISPOSTA_A1", "Solitamente durante tutto il giorno");
		prop.setProperty("RISPOSTA_A2", "Durante tutto il giorno indistintamente");
		prop.setProperty("RISPOSTA_A3", "No, Voglio consumare indistintamente tutto il giorno");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		    
		};
		
		@Test
	    public void eseguiTest() throws Exception{
			String args[]= {nomeScenario};
			
			Login_Public_Area.main(args);
			MotorinoDiRicerca.main(args);
			ScegliTu100x100.main(args);
			ScegliPrimaDomandaOffertaScegliTu100x100.main(args);
		};
		
		@After
		  public void fineTest() throws Exception{
			String args[] = {nomeScenario};
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
			ReportUtility.reportToServer(this.prop);	
		};
		
}
