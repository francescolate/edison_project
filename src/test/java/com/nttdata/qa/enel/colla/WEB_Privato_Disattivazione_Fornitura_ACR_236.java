package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.colla.AccountRegistration;
import com.nttdata.qa.enel.testqantt.colla.ContinueWithRegistration;
import com.nttdata.qa.enel.testqantt.colla.DeleteRegisteredAccount;
import com.nttdata.qa.enel.testqantt.colla.GetDataForRegistration;
import com.nttdata.qa.enel.testqantt.colla.Privato_235_ACR_Disattivazione;
import com.nttdata.qa.enel.testqantt.colla.Privato_236_ACR_Disattivazione;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;

public class WEB_Privato_Disattivazione_Fornitura_ACR_236 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_PASSWORD", "Password01");
		//prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("AREA_CLIENTI", "CASA");
		
		//New strategy for registration - 2021-02-18
		prop.setProperty("ACCOUNT_FIRSTNAME", "HANANE");
		prop.setProperty("ACCOUNT_LASTNAME", "EL GUESSE");
		prop.setProperty("CF", "LGSHNN80P63Z330D");
		prop.setProperty("SUPPLY_NUMBER", "698614898");
		//prop.setProperty("WP_USERNAME", "t.es.t.i.n.g.c.r.m.a.u.t.o.m.a.t.i.o.n@gmail.com");
		prop.setProperty("WP_USERNAME", "r320.19p109statocontiwe.b@gmail.com");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
//		GetDataForRegistration.main(args);
		//DeleteRegisteredAccount.main(args);
		//AccountRegistration.main(args);
		//SetUtenzaMailResidenziale.main(args);
		//RecuperaLinkCreazioneAccount.main(args);
		//ContinueWithRegistration.main(args);
		Privato_236_ACR_Disattivazione.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
