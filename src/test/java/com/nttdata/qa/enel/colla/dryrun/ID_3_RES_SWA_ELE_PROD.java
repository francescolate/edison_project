package com.nttdata.qa.enel.colla.dryrun;

import java.io.FileOutputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.ID_3_RES_SWA_ELE_PROD_Module;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area_Prod;
import com.nttdata.qa.enel.testqantt.colla.OCR_Module;
import com.nttdata.qa.enel.util.Utility;
import java.io.FileInputStream;
import java.io.InputStream;
import com.nttdata.qa.enel.util.ReportUtility;

public class ID_3_RES_SWA_ELE_PROD {
	
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", "https://www.enel.it/it/luce-e-gas/luce/casa?nec=swa&sortType=in-promozione");
//		prop.setProperty("OCR_OFFER_LINK", "https://enelsite:FrUkoq+swIpl!O0aKeB_@www.enel.it/it/luce-e-gas/luce/offerte/e-light-bioraria?swaOCR=detail");
		prop.setProperty("OCR_OFFER_LINK", "https://www.enel.it/it/luce-e-gas/luce/offerte/e-light-bioraria?swaOCR=detail");
		prop.setProperty("FIRSTNAME", "PETER");
		prop.setProperty("LASTNAME", "PARKER");
		prop.setProperty("CF", "PRKPTR72B03H502P");
		prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CAP", "00136");
		prop.setProperty("POD_PDR", "IT002E1231230A");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
		
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		
		Login_Public_Area_Prod.main(args);
		ID_3_RES_SWA_ELE_PROD_Module.main(args);
	};
		
	@After
    public void fineTest() throws Exception{
//		InputStream in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//		ReportUtility.reportToServer(this.prop);
	};
}
