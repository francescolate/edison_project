package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.ServizioTutelaGas;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Servizio_Tutela_Gas_11 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("LINK", BASE_LINK);
		
		prop.setProperty("LOC_ROMA", "Roma");
		prop.setProperty("LOC_MILANO", "Milano");
		prop.setProperty("LOC_NAPOLI", "Napoli");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		ServizioTutelaGas.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
