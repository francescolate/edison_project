package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.Cancel_Item_CF;
import com.nttdata.qa.enel.testqantt.Cancel_Item_CF_339;
import com.nttdata.qa.enel.testqantt.GetIdBpmValue;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.UpdateIdBpmValue;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACB_284;
import com.nttdata.qa.enel.testqantt.colla.Privato_Modifica_Addebito_diretto_Conto_Corrente_ACB_339;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Modifica_Addebito_diretto_Conto_Corrente_ACB_339 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		
	 //	prop.setProperty("WP_USERNAME", "testimpresa@yopmail.com"); 
    // 	prop.setProperty("WP_PASSWORD", "Password01"); 
		prop.setProperty("WP_USERNAME", "web.test.ing.automation@gmail.com"); 
		prop.setProperty("WP_PASSWORD", "Enel$001"); 
		
		//310492564 - Supply //01249760560 - CF // POD IT012E00369689 // //IT38P0303240021000000000418 - IBAN
		prop.setProperty("CF", "01249760560"); 
		
		prop.setProperty("AREA_CLIENTI", "IMPRESA"); 
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("RUN_LOCALLY","Y");  
		
	    prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
			
		prop.setProperty("LINK_UDB", "http://dtcmmind-bw-01.risorse.enel:8887");
		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//Login_Private_Area.main(args);
		Privato_Modifica_Addebito_diretto_Conto_Corrente_ACB_339.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		GetIdBpmValue.main(args);
		UpdateIdBpmValue.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		Cancel_Item_CF_339.main(args);	
	};
	
	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}