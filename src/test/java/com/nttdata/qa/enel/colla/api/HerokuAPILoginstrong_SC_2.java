//HerokuAPILoginstrong_SC_2
// Solo per APP , senza autorization e Token in input 
// OK - iOS - RES Check EnelID, CF, CountryFiscalCountry - login e verifica della response

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPILoginstrong_1;
import com.nttdata.qa.enel.util.ReportUtility;
import java.util.UUID;

public class HerokuAPILoginstrong_SC_2 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/authentication/v2/loginstrong");
	    prop.setProperty("USERNAME","raff.aellaq.uomo@gmail.com");
	    prop.setProperty("PASSWORD","Password01");
//	    prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",          \"username\": \""+ prop.getProperty("USERNAME") +"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"IOS\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     },     \"data\": {         \"username\": \""+ prop.getProperty("USERNAME") +"\",         \"password\": \""+ prop.getProperty("PASSWORD") +"\",         \"migrationToken\": \"\",         \"oidcToken\": \"\",         \"pwd_crypt\": \"\",         \"resolution\": \"1080x1794\",         \"osVer\": \"7.0\",         \"idDispositivo\": \"MWS0216808002415\",         \"appVer\": \"10.0.1\",         \"os\": \"IOS\",         \"context\": \"app_ee\",         \"device\": \"HUAWEI EVA-L09\"     } }");
	    prop.setProperty("JSON_DATA_OUTPUT", "\"userUPN\":\"021bc291-c6bc-4ae7-aa67-dee6e6fc3c5c\",\"isProspect\":false,\"isStrongAuthenticated\":true,\"cf\":\"DMTTZN69H67C765I\",\"countryFiscalCode\":\"IT\",\"active\":false,\"userEmail\":\"r.affaellaquom.o@gmail.com\",\"userType\":\"UserCommodityInLavorazioneToo\",\"enelId\":\"95c8a66b-7004-4098-97d8-786a4da0d802\",\"token\":\"");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - iOS - RES Check EnelID, CF, CountryFiscalCountry - login e verifica della response
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPILoginstrong_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
