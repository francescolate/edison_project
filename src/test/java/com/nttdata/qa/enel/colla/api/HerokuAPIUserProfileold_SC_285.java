/*HerokuAPIUserProfileold_SC_285 
** API call for Tokens
* Case OK - User/Profile/old RES 
* Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJFbmVsU2VsZkNhcmUiLCJzdWIiOiJFRTAwMDAwMDk4MTNAQk9YLkVORUwuQ09NIiwiaXNzIjoiV0VCIiwiZXhwIjoxNjA5MzI5MzI3LCJpYXQiOjE1OTMxNjg5MjcsInVzZXJpZCI6IkVFMDAwMDAwOTgxM0BCT1guRU5FTC5DT00iLCJqdGkiOiI0ZDhkOTU1OC0zOGY4LTQwNjctYmI2Ny01ZTZlN2ZmNDFjY2EifQ.VbOVKyACUUbToV_9yMwwmlCAKQcffbLPtccnPM-lAlA
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIUserProfileold_SC_285 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","MSNSRA56E17D612W");
		prop.setProperty("USERNAME","morris.mastrolia@enelsaa0005.enel.com");
		prop.setProperty("USERUPN","EE0000009813@BOX.ENEL.COM");
		prop.setProperty("ENELID","7f05d8f1-70cd-47da-9fdf-5e13c882d832");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJFbmVsU2VsZkNhcmUiLCJzdWIiOiJFRTAwMDAwMDk4MTNAQk9YLkVORUwuQ09NIiwiaXNzIjoiV0VCIiwiZXhwIjoxNjA5MzI5MzI3LCJpYXQiOjE1OTMxNjg5MjcsInVzZXJpZCI6IkVFMDAwMDAwOTgxM0BCT1guRU5FTC5DT00iLCJqdGkiOiI0ZDhkOTU1OC0zOGY4LTQwNjctYmI2Ny01ZTZlN2ZmNDFjY2EifQ.VbOVKyACUUbToV_9yMwwmlCAKQcffbLPtccnPM-lAlA");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-stage/v1/user/profile/old?isRetPag=true&isRetAddebitoCC=true&nuPagaButton=");
		prop.setProperty("JSON_DATA_OUTPUT","{\"numeroAssetELE\":1,\"tipoAccount\":\"RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"NON RICON.\",\"flagDipendenteEnel\":false,\"totaleUsoNonAbitativo\":0,\"codiceFormaGiuridica\":\"20\",\"numeroPartiteIVA\":0,\"numeroAssetGAS\":1,\"indicatoriSufornitureInCorsoAttivazione\":0,\"account\":[{\"idAccount\":\"0010Y00000F2pn7QAB\",\"tipoAccount\":\"RESIDENZIALE\",\"cellulareSenzaPrefisso\":null,\"indirizzo\":null,\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":null,\"contact\":{\"dataCreazione\":\"2017-02-11T09:03:34.000Z\",\"cognome\":\"MASINI\",\"cellulareSenzaPrefisso\":null,\"accountSocialTwitter\":null,\"codiceFiscale\":\"MSNSRA56E17D612W\",\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":\"TELEFONO\",\"altroTelefonoSenzaPrefisso\":null,\"telefono\":\"0571920601\",\"fax\":null,\"accountSocialFB\":null,\"altroTelefono\":\"3492235141\",\"indirizzoDomicilio\":\"VIA PONZANO 460     50053 EMPOLI FI <regione vuota>\",\"indirizzoEmail\":\"morris.mastrolia@enelsaa0005.enel.com\",\"indirizzoResidenza\":null,\"descrizioneAltroTelefono\":null,\"cellulare\":\"3492235141\",\"cellulareCertificato\":false,\"nome\":\"SAURO\",\"descrizioneTelefono\":\"FISSO\",\"emailCertificata\":false,\"prefissoAltroTelefono\":null,\"prefissoCellulare\":null,\"idContact\":\"0030Y00000AmqUgQAJ\",\"descrizioneEmail\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null},\"ragioneSociale\":null,\"telefono\":null,\"fax\":null,\"citta\":null,\"descrizioneAltroTelefono\":null,\"toponomasticaViaCivico\":null,\"flagDipendenteEnel\":false,\"accountType\":null,\"codiceFormaGiuridica\":null,\"nome\":\"SAURO\",\"descrizioneTelefono\":null,\"civico\":null,\"telefonoValido\":false,\"emailCertificata\":false,\"chiaveAlReferenteTitolare\":\"0030Y00000AmqUgQAJ\",\"prefissoCellulare\":null,\"statoCliente\":\"Cliente\",\"descrizioneEmail\":null,\"mercatoRiferimentoCliente\":\"Maggior Tutela\",\"flagPersonaFisicaGiuridica\":\"N\",\"cognome\":\"MASINI\",\"descrizioneFormaGiuridica\":null,\"sedeLegale\":null,\"accountSocialTwitter\":null,\"tipologiaCliente\":\"Casa\",\"provincia\":null,\"codiceFiscale\":\"MSNSRA56E17D612W\",\"bpsap\":\"0030276459\",\"cap\":null,\"cognomeNome\":\"MASINI SAURO\",\"accountSocialFB\":null,\"altroTelefono\":null,\"idMasterAccountSiebel\":\"2-2HTQ0SV\",\"indirizzoEmail\":\"visosauro@gmail.com\",\"cellulare\":null,\"cellulareCertificato\":false,\"flagSintesi\":false,\"partitaIVA\":null,\"toponomastica\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null}]},\"asset\":{\"asset\":[{\"idAccount\":\"0010Y00000F2pn7QAB\",\"livellodiTensione\":null,\"idServicePoint\":\"a1f0Y000004eYROQA2\",\"soggettoALegge136\":\"false\",\"codiceDistributore\":null,\"dataCessazione\":null,\"dataAdesioneEnelmia\":null,\"podPdr\":\"15104203627694\",\"tipologiaServizio\":\"NEW_Energia Sicura Gas_New\",\"tipologiaEnelmia\":null,\"tipologiaBolletta\":\"BS\",\"tipologiaSmart\":\"N\",\"bpAccountSAP\":\"0030276459\",\"potenzaContrattuale\":null,\"descrizioneTipologiaEnelmia\":null,\"numeroCartaEnelPremia\":null,\"indirizzoBanca\":\"AGENZIA DI EMPOLI\",\"tipologiaFornitura\":\"Gas\",\"dataRecessoEnelmia\":null,\"dataInizioFormaContrattuale\":\"2018-12-11T00:00:00.000Z\",\"partitaIvaCliente\":null,\"codiceConcessione\":null,\"flagLoyaltyEnelPremia\":false,\"dataAttivazione\":\"2016-04-30T20:00:00.000Z\",\"indirizzoDiFornitura\":\"VIA PONZANO 460 - 50053 EMPOLI FI\",\"potenzaDisponibile\":null,\"nomeBanca\":\"BANCA DI CREDITO COOPERATIVO DI CAMBIANO\",\"tipologiaAsset\":\"Commodity\",\"iban\":\"IT28X0842537830000030123327\",\"codiceContratto\":\"09854457\",\"dataAttivazioneEnelPremia\":null,\"codiceFiscaleCliente\":\"MSNSRA56E17D612W\",\"tensioneDisponibile\":null,\"cognomeIntestatario\":\"SCARTONI ANTONELLA E          MASIN\",\"idAsset\":\"02i0Y000001oe8vQAA\",\"pianoTariffario\":null,\"idContratto\":\"8000Y000001PohIQAS\",\"rowIdSiebelContratto\":\"2-IKZHF7R\",\"nomeIntestatario\":null,\"ultimoProcessoModificaAsset\":\"Switch Attivo\",\"listino\":\"Energiasicura gas\",\"categoriaUso\":\"Riscald.to appartamento <100mq\",\"tipoProdotto\":\"RES\",\"codiceFiscaleIntestatario\":\"MSNSRA56E17D612W\",\"codiceEnelmia\":null,\"alias\":\"715345782\",\"potenzaInFranchigia\":null,\"numeroUtente\":\"715345782\",\"statoFormaContrattuale\":\"ATTIVA\",\"regime\":\" \",\"flag136\":false,\"assetFiglio\":[],\"flagAdesioneEnelmia\":false,\"indirizzoDiFatturazione\":\"VIA PONZANO 460, 50053, EMPOLI, FI\",\"idListino\":\"a1l1n000000XTiOAAW\",\"codiceOfferta\":\"3-E0041202\",\"attributoAsset\":[],\"dataFineFormaContrattuale\":\"2020-04-30T00:00:00.000Z\",\"contrattoSAP\":\"0027670093\",\"idProdotto\":\"a1Y2400000BFIoiEAH\",\"statoServizio\":\"Attivo\",\"usoFornitura\":\"Uso Abitativo\",\"metodoDiPagamento\":\"RID\",\"canaleDiPagamento\":\"conto corrente\",\"contractAccountSAP\":\"001022840303\"}]},\"dashboard\":{\"enelpremiaUrl\":null,\"eBillingUnread\":null,\"enelTuttoOk\":null,\"gasOk\":null,\"type\":null,\"luceOk\":null,\"urlEnelTuttoOk\":null,\"consensi\":null,\"infoEnergia\":\"DISATTIVATO\",\"eBilling\":null,\"urleasyclick\":null,\"domiciliazione\":\"Attivo\",\"easyclick\":null,\"ltyKeys\":null,\"rem\":null,\"statoPagHP\":null,\"codiceUnicaBw\":null,\"azUpsellEnelTuttoOk\":null,\"riepilogo\":null,\"bollettaWeb\":\"DISATTIVATO\",\"urlRem\":null,\"enelmiaUrl\":null,\"enelmia\":null,\"enelpremiaUrlp3\":null,\"azUpsellDomiciliazione\":null,\"bollettaPec\":null,\"enelpremiaName\":null,\"enelpremia\":null,\"azUpsellBollettaweb\":null},\"eeCommodity\":\"DUAL\"},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}"); 
        prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Case OK - User/Profile/old RES 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
