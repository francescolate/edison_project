/* HerokuAPIAccountProfileByID_SC_141 
** AccountProfileByID - caso OK - WEB - LIKE_RES
* No Token
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_2;
import com.nttdata.qa.enel.util.ReportUtility;
// import com.nttdata.jwt.gen.*;


public class HerokuAPIAccountProfileByID_SC_141 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","ee67a106-47a9-4337-9061-83dfeb0b5dbb");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("Authorization", "1");
		prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"code\":200,\"data\":{\"firstName\":\"NINO\",\"lastName\":\"FRASSICA\",\"phoneNumber\":\"+393809090001\",\"countryCode\":\"IT\",\"companyName\":\"MONTAGGI FN\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"businessId\",\"value\":\"04107310569\"}]}],\"enelId\":\"ee67a106-47a9-4337-9061-83dfeb0b5dbb\",\"email\":\"r32019p10.9statocontiweb@gmail.com\",\"enabled\":true,\"vatCode\":\"04107310569\"},\"message\":\"OK\"},\"meta\":{\"path\":\"/profile/v1/account/{id}\",\"method\":\"account-profile-by-id\",\"dataAggiornamento\":");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * AccountProfileByID - caso OK - WEB - LIKE_RES 
     * No Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccountProfileByID_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
