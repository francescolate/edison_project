package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_ACR_101;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_PaginaBollette_DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_101 {

	
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	

	@Before 
	public void inizioTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "silviatestenel@yahoo.com");
		prop.setProperty("WP_PASSWORD", "Eneltest2017");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("VERIFY_SC_SELECTION_PAGE","");			
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Bolletta_ACR_101.main(args);
	};

	@After
	public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
