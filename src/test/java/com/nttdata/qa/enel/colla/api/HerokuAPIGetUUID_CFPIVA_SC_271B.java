/*HerokuAPIGetUUID_CFPIVA_SC_271B 
** API call for Tokens
** API GET UDID call with customer already registered with PIVA BSN 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetUUID_CFPIVA_SC_271B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","01533540447");
		prop.setProperty("IDCLIENTE","businessId");
		prop.setProperty("COUNTRY", "IT");
       	prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?");

//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?" + prop.getProperty("IDCLIENTE") + "=" + prop.getProperty("CF_PIVA") + "&" + prop.getProperty("IDCLIENTE") + "Country=IT");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":200,\"data\":{\"accounts\":[{\"firstName\":\"BRUNA\",\"lastName\":\"MASSACCI\",\"phoneNumber\":\"+393209090191\",\"countryCode\":\"IT\",\"attributes\":[{\"country\":\"IT\",\"claims\":[{\"name\":\"businessId\",\"value\":\"01533540447\"}]}],\"enelId\":\"998777cd-ccbc-45e8-b013-2578bc623fff\",\"email\":\"r.32019p109statoc.ontiweb@gmail.com\",\"enabled\":true}]},\"message\":\"OK\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * API GET UDID call with customer already registered with PIVA BSN 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
