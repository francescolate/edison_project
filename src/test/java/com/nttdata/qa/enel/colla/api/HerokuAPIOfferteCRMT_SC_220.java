/* HerokuAPICoperturaIndirizzo_SC_220 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - ALLACCIO E ATTIVAZIONE  - In lavorazione

"userUPN": "ec3de713-1459-4aa7-8fd9-1dcd4cc9303e",
"cf": "MGLNLS88C41B872B",
"enelId": "40fba534-9423-42a0-a3fa-eeed49b6553a",
"userEmail": "r.32019p.109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_220 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","MGLNLS88C41B872B");
		prop.setProperty("USERNAME","r.32019p.109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","ec3de713-1459-4aa7-8fd9-1dcd4cc9303e");
		prop.setProperty("ENELID","40fba534-9423-42a0-a3fa-eeed49b6553a");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"IN LAVORAZIONE\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":null,\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3115126\",\"flagStd\":0,\"namingInFattura\":\"Scegli Tu\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"ALLACCIO E ATTIVAZIONE\",\"idCase\":\"5001l00000494bsAAA\",\"idListino\":\"a1l1l000000OP4oAAG\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000NRlTEAW\",\"dataRichiesta\":\"2020-05-11T00:00:00.000Z\",\"idProdotto\":\"a1Y1n000002ITnHEAW\",\"commodityCaseItem\":null,\"indirizzoFornitura\":null,\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"IN LAVORAZIONE\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":null,\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3115126\",\"flagStd\":0,\"namingInFattura\":\"Scegli Tu\",\"statoR2d\":null,\"numeroUtente\":null,\"processoInCorso\":\"ALLACCIO E ATTIVAZIONE\",\"idCase\":\"5001l00000494bsAAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OP4oAAG\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000NRlTEAW\",\"dataRichiesta\":\"2020-05-11T00:00:00.000Z\",\"idProdotto\":\"a1Y1n000002ITnHEAW\",\"commodityCaseItem\":null,\"indirizzoFornitura\":null,\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
        

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - ALLACCIO E ATTIVAZIONE  - In lavorazione
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
