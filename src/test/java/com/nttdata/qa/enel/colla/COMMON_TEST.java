package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.CheckEmailWithAttachment;
import com.nttdata.qa.enel.testqantt.colla.TEST_MODULE;

public class COMMON_TEST {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
//		CheckEmailWithAttachment.main(args);
		TEST_MODULE.main(args);
	}
	
	@After
    public void fineTest() throws Exception{ };
}
