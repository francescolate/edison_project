package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaDettaglioForniture;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaPopupLogin;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Dettaglio_Fornitura_In_Attivazione_127C {
	
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "collaudoloyaltycolazz.o202.0@gmail.com");  //sil.via.test77@gmail.com
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACTIVATION_STATUS", "richiesta di attivazione in lavorazione");
//	    prop.setProperty("QUERY", "SELECT ITA_IFM_Account_ID__c, id, ITA_IFM_Account_ID__r.NE__Fiscal_code__c, ITA_IFM_POD__c, ITA_IFM_Status__c\r\n" + 
//	    		"FROM ITA_IFM_Case_Items__c\r\n" + 
//	    		"where ITA_IFM_IsCombinedCalc__c = 'SI'\r\n" + 
//	    		"AND ITA_IFM_Account_ID__r.RecordType.Name = 'Residenziale'\r\n" + 
//	    		"AND ITA_IFM_Status__c = 'In attesa cvp'\r\n" + 
//	    		"AND ITA_IFM_Subject__c = 'SUBENTRO'\r\n" + 
//	    		"AND CreatedDate > 2020-01-01T00:00:00.000+0000\r\n" + 
//	    		"Limit 10");
	    prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "RES_LIKE");
	    prop.setProperty("VERIFY_SC_SELECTION_PAGE", "");
	    prop.setProperty("ITA_IFM_POD__c", "IT002E3810990A");			//IT004E09238423
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("STATO_FORNITURA","In Lavorazione");
		prop.setProperty("TIPO_ATTIVAZIONE","SUBENTRO");
		prop.setProperty("DATA_RICHIESTA","18/12/2020");
		prop.setProperty("POD","IT002E3810990A");
		prop.setProperty("OFFERTA_ATTIVA","GiustaXTe Impresa");
		prop.setProperty("NUMERO_OFFERTA","SG3181540");
		prop.setProperty("SOTTOSCRIZIONE","Cartacea");
		prop.setProperty("CONTRATTO","PDF non disponibile");
		
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
//		RecuperaDatiWorkbench.main(args);
		LoginEnel.main(args);
		PrivateAreaPopupLogin.main(args);
		PrivateAreaMenu.main(args);
		PrivateAreaDettaglioForniture.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
