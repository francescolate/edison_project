package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.testqantt.colla.WebPrivatoDettaglioFornituraLavoriSulContatore;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_DettaglioFornitura_In_Attivazione_126 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		  prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		 
		/*  prop.setProperty("WP_USERNAME", "silvia.test2020@gmail.com");
		  prop.setProperty("CUSTOMER_CODE", "310497376");
		  prop.setProperty("WP_PASSWORD", "Eneltest2017");	*/
		
	    prop.setProperty("WP_USERNAME", "silvia.test77@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");	  
	    prop.setProperty("CUSTOMER_CODE", "310497349");
	  
	  //  prop.setProperty("CUSTOMER_CODE", "310497357");
	 //   prop.setProperty("CUSTOMER_CODE", "310507246");
	    
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "CASA");//IMPRESA
	    prop.setProperty("SUPPLY_ADDRESS", "VIA ACHILLE FUNI 9, 20152, MILANO, MILANO, MI");
	    prop.setProperty("SUPPLY_TYPE", "luce");
	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);
		WebPrivatoDettaglioFornituraLavoriSulContatore.main(args);	
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
