/*HerokuAPIUserProfileold_SC_287 
** API call for Tokens
* Case OK - User/Profile/old - KO CRMT - NON TOCCARE IL TOKEN
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIUserProfileold_SC_287 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
        prop.setProperty("CF_PIVA","LNERFL39B08H643E");
        prop.setProperty("USERNAME","r32.019p109statocontiweb@gmail.com");
        prop.setProperty("USERUPN","fede4574-670a-4668-a0a8-1990f0032768");
        prop.setProperty("ENELID","6b789a66-ec0c-4f05-b51d-0d89b98f5f4f");
        prop.setProperty("EXPIRATION","2021-12-31");
        prop.setProperty("SOURCECHANNEL","WEB");
        prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
        prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-stage/v1/user/profile/old?isRetPag=false&isRetAddebitoCC=false&nuPagaButton=false");
        prop.setProperty("JSON_DATA_OUTPUT","{\"data\":{\"datiWDM\":null,\"eeEfficienzaEnergetica\":null,\"segmentoWeb\":null,\"eeTipologiaCliente\":null,\"mapMenuToDelete\":\"|HOME|UPLOAD|MODIFICA_DATI|GDPR|REM |EASYCLICK|ENELMIA|ENELPREMIA|LE_BOLLETTE|LA_FORNITURA|LE_ATTIVITA|GLI_ACQUISTI|MYENERGY\",\"unica\":null,\"typeData\":\"SERV\",\"userProfile\":{\"idAccount\":\"0010Y00000FusQxQAJ\",\"profili\":\"N\",\"facebook\":false,\"presenzaDatoFiscale\":\"S\",\"google\":false,\"codiceFiscale\":\"LNERFL38B08H643E\",\"cognomeCliente\":\"LEONE\",\"nomeAzienda\":null,\"twitter\":false,\"idContact\":\"\",\"partitaIva\":\"\",\"servizio\":\"NOSERVIZIO\",\"autenticazioneForteBSN\":\"N\",\"nomeCliente\":\"RAFFAELE\",\"emailUtente\":\"fede4574-670a-4668-a0a8-1990f0032768\"},\"tceInfo\":null,\"popupLogin\":null,\"anagraficaData\":null,\"asset\":null,\"dashboard\":null,\"eeCommodity\":null},\"status\":{\"descrizione\":\"Ko No dato fiscale - Aggiornare profilo\",\"esito\":\"KO\",\"codice\":\"004\"}}");//{\"data\":null,\"status\":{\"descrizione\":\"Errore servizio CRMT\",\"esito\":\"KO\",\"codice\":\"005\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Case OK - User/Profile/old - KO CRMT
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
         //String args[] = {nomeScenario};
         InputStream in = new FileInputStream(nomeScenario);
         prop.load(in);
         this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
         ReportUtility.reportToServer(this.prop);
   };
}
