/*HerokuAPICheckOKSAP_SC_301
** Chiamata API checkoksap - KO No Token - WEB - RES															
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPISendPost;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPICheckOKSAP_SC_301 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("Authorization", "");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("CF_PIVA","ZMBGFR38C21A944X");
		prop.setProperty("USERNAME","francesco.lucia@nttdata.com");
		prop.setProperty("USERUPN","049fe692-392b-44a5-bbeb-d80b689db837");
		prop.setProperty("ENELID","c027675e-53d2-4943-a097-3ed45a92be01");
		prop.setProperty("EXPIRATION","2021-07-30");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/2g-ind/api/query/v1/checkoksap/IT002E3311145A?canale=WEB&data=24.06.2021&cf=SCGCLD97H41F839F");
		prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"GEN10\",\"code\":\"-4\",\"description\":\"Sessione non valida, si prega di effettuare nuovamente l'accesso\"}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
	 * API call for Tokens
     * Chiamata API checkoksap - KO No Token - WEB - RES																								
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPISendPost.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
