/*HerokuAPIChangePassword_SC_266
** Solo per APP , senza autorization e Token in input  
** API call for Tokens 
** Change Password OTP - Caso KO
** API call Change Password for EnelID does not exist
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIChangePassword_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIChangePassword_SC_266 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID", "f613ea45-3f3b-1111-b765-878f978fdbb4");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"code\":404,\"message\":\"Not Found\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens 
	 * Change Password OTP - Caso KO
	 * API call Change Password for EnelID does not exist
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIChangePassword_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
