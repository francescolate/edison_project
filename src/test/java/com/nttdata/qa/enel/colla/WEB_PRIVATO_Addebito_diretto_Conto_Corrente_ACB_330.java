package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyPodDetails330;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_330_ACB_Addebito_Diretto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Addebito_diretto_Conto_Corrente_ACB_330 {

	Properties  prop;
	final String nomeScenario="WEB_PRIVATO_Addebito_diretto_Conto_Corrente_ACB_330.properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "web.testing.automat.ion@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");  
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_PATH", "AREA RISERVATA HOMEPAGE");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area dedicata Business");
		prop.setProperty("SECTION_HEADING", "Vantaggi per il tuo business");
		prop.setProperty("ADDEBITO_DIRETTO_PATH", "AREA RISERVATA SERVIZI ADDEBITO DIRETTO");
		prop.setProperty("ADDEBITO_DIRETTO_TITLE", "Addebito Diretto");
		prop.setProperty("ADDEBITO_DIRETTO_SUBTEXT", "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sul Tuo Conto Corrente");
		prop.setProperty("ADDEBITO_DIRETTO_NONACTIVE_STATUS", "Il servizio Addebito Diretto non è attivo sulla fornitura:");
		prop.setProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_PATH", "AREA RISERVATAFORNITURAATTIVAZIONE ADDEBITO DIRETTO");
		prop.setProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT", "Compila i campi sottostanti con i tuoi dati e poi prosegui");
		prop.setProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT2", "Verifica i campi sottostanti e clicca sul tasto conferma");
		prop.setProperty("SUPPLY_INFO", "Ecco le forniture su cui modifichermo Addebito Diretto");
		prop.setProperty("EMAIL_INFO", "Inserisci la tua mail per ricevere aggiornamenti sulla richiesta.");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("POD", "02660026599153");
		prop.setProperty("LINK_UDB", "http://dtcmmind-bw-01.risorse.enel:8887");
		prop.setProperty("SERVICEREQUEST_LINK","https://workbench.developerforce.com/login.php");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_330_ACB_Addebito_Diretto.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		VerifyPodDetails330.main(args);
		
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}