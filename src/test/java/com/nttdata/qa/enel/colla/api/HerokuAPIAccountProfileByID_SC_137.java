/* HerokuAPIAccountProfileByID_SC_137 
** AccountProfileByID - caso KO - WEB - NO CHANNEL
* No Token
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccountProfileByID_2;
import com.nttdata.qa.enel.util.ReportUtility;
// import com.nttdata.jwt.gen.*;


public class HerokuAPIAccountProfileByID_SC_137 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","c027675e-53d2-4943-a097-3ed45a92be01");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","");
		prop.setProperty("Authorization", "1");
		prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"AUT02\",\"code\":\"-1\",\"description\":\"Client non autorizzato\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * AccountProfileByID - caso KO - WEB - NO CHANNEL
     * No Token
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccountProfileByID_2.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
