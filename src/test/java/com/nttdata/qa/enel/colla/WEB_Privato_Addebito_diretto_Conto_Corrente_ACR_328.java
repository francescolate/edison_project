package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CancelPOD;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_327_ACR_Addebito_Diretto;
import com.nttdata.qa.enel.testqantt.colla.Privato_328_ACR_Addebito_Diretto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Addebito_diretto_Conto_Corrente_ACR_328 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "silvia.test2020@gmail.com");		
		prop.setProperty("WP_PASSWORD", "Eneltest2017");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		
		prop.setProperty("IBAN", "IT30P0301503200000000246802");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("POD", "IT002E6365412A");
		prop.setProperty("CF", "RNDLCR55L07Z601K");
		prop.setProperty("dtcmmind_URL", "http://10.151.55.144:8887/");
		
		prop.setProperty("INSERTIMOCOLOR", "Crimson");
		
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.setProperty("RETURN_VALUE", "OK");
		prop.store(new FileOutputStream(nomeScenario), null);	
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_328_ACR_Addebito_Diretto.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CancelPOD.main(args);
	
	};
	
	@After
    public void endTest() throws Exception{
		
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
	};

}
