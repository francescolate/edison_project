/*HerokuAPIGetUUID_CFPIVA_SC_276A
** Chiamata API per Token
Chiamata API GET UDID con cliente già registrato 
Token Non valido
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetUUID_3;
// import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetUUID_CFPIVA_SC_276A {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","SLNLBS68M28H501Y");
		prop.setProperty("IDCLIENTE","personalId");
		prop.setProperty("COUNTRY", "IT");
       	prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?");

//		prop.setProperty("Authorization", "Bearer af7ceced-e28a-3b0f-a408-f2f267b30e9f");
//		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/profile/account?" + prop.getProperty("IDCLIENTE") + "=" + prop.getProperty("CF_PIVA") + "&" + prop.getProperty("IDCLIENTE") + "Country=IT");
//		prop.setProperty("JSON_DATA_OUTPUT", "{protocol=h2, code=401, message=, url=https://uniqueid-coll.enel.com:8243/profile/account?personalId=" + prop.getProperty("CF_PIVA")+"&personalIdCountry=IT}");
		prop.setProperty("JSON_DATA_OUTPUT", "{protocol=h2, code=401, message=, url=https://uniqueid-coll.enel.com:8243/profile/account?personalId=");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Chiamata API per Token
     * Chiamata API GET UDID con cliente già registrato 
     * Token Non valido
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetUUID_3.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
