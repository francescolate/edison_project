package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ID_106_SWA_ELE_BSN;
import com.nttdata.qa.enel.testqantt.colla.LaunchOpenEnergyDigitalOffer;

import com.nttdata.qa.enel.testqantt.colla.OfferOpenEnerygyDigital_106;


public class WEB_Pubblico_ProcessiAcquisitivi_106 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("CODICE_FISCALE_SOCIETA", "01533540447");
	    prop.setProperty("PIVA", "01533540447");
	    prop.setProperty("RAGIONE_SOCIALE", "ASSIMETA SAS");
	    prop.setProperty("NOME_REFERENTE", "BRUNA");
	    prop.setProperty("COGNOME_REFERENTE", "MASSACCI");
	    prop.setProperty("CODICE_FISCALE_REFERENTE", "MSSBRN64T43G005E");
	    prop.setProperty("TIPO_REFERENTE", "Rappresentante Legale");
	    prop.setProperty("ATTUALE_FORNITORE", "ACEA ENERGIA SPA - Libero");
	    prop.setProperty("CAP", "60027 - OSIMO");
	    prop.setProperty("ATTUALE_FORNITORE", "ACEA ENERGIA SPA - Libero");
		
		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LaunchOpenEnergyDigitalOffer.main(args);
		ID_106_SWA_ELE_BSN.main(args);
		OfferOpenEnerygyDigital_106.main(args);
	
		
	};
	
	@After
    public void fineTest() throws Exception{
//		InputStream in = new FileInputStream(nomeScenario);
//        prop.load(in);
//        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//        ReportUtility.reportToServer(this.prop);
	};
}
