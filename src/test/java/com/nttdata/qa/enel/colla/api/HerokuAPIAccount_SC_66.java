// HerokuAPIAccount_SC_66
// Solo per WEB  - TOken in input expire 31/12/2020
/*"userUPN": "d5f8114b-177e-4ab4-be28-9685110c4dff",
"cf": "NDRVTI70A16B180O",
"enelId": "c88b0269-daa8-4c1e-bb56-5233da6328df",
"userEmail": "affiddaqe-8312@yopmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar NDRVTI70A16B180O affiddaqe-8312@yopmail.com d5f8114b-177e-4ab4-be28-9685110c4dff c88b0269-daa8-4c1e-bb56-5233da6328df 2021-12-31
*/
// OK - Canale WEB - LIKE_RES

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.jwt.gen.*;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccount_1;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIAccount_SC_66 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","NDRVTI70A16B180O");
		prop.setProperty("USERNAME","affiddaqe-8312@yopmail.com");
		prop.setProperty("USERUPN","d5f8114b-177e-4ab4-be28-9685110c4dff");
		prop.setProperty("ENELID","c88b0269-daa8-4c1e-bb56-5233da6328df");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZDVmODExNGItMTc3ZS00YWI0LWJlMjgtOTY4NTExMGM0ZGZmIiwiY29kaWNlZmlzY2FsZSI6Ik5EUlZUSTcwQTE2QjE4ME8iLCJpc3MiOiJhcHAiLCJleHAiOjE2NDM2NDEzMTEsImVuZWxpZCI6ImM4OGIwMjY5LWRhYTgtNGMxZS1iYjU2LTUyMzNkYTYzMjhkZiIsInVzZXJpZCI6ImFmZmlkZGFxZS04MzEyQHlvcG1haWwuY29tIiwianRpIjoiNTJhNDBlZTYtNzQ2ZC00MTZmLWJmMTYtYTE0YTY2NzVlMmU3In0.ZQwTff7MZDdZxApBNJT1e2KF6A3Xryz7m59t6POoiM8");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/authentication/v2/account");
		prop.setProperty("JSON_INPUT", "{   \"data\":{      \"enelId\":\"" + prop.getProperty("ENELID") + "\"  }}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"userUPN\":\"d5f8114b-177e-4ab4-be28-9685110c4dff\",\"cf\":\"NDRVTI70A16B180O\",\"countryCf\":\"IT\",\"phoneNumberSkipped\":false}");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - Canale WEB - LIKE_RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccount_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
