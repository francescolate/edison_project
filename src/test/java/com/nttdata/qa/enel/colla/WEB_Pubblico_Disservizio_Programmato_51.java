package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.Pubblico_Disservizio_Programmato_51;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Pubblico_Disservizio_Programmato_51 {
	
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	final String BASE_LINK = "https://www-coll1.enel.it/it/disservizio-programmato.html";
	
	@Before
	public void startTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/it/disservizio-programmato.html");
		prop.setProperty("URL", "https://www-coll1.enel.it/it/disservizio-programmato.html");
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Pubblico_Disservizio_Programmato_51.main(args);
		
	};
	
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
