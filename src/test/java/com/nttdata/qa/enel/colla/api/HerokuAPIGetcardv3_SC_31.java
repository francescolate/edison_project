// HerokuAPIGetcardv3_SC_31
// Solo per APP  - TOken in input expire 31/12/2021
/*"userUPN": "d7f4cce7-fe5b-4639-a3dc-b75daea644ba",
"cf": "SCRGPP60E27M088Z",
"enelId": "9e2c5364-f359-47c7-86ed-b5fd701b87a4",
"userEmail": "co.llaudoloyalty.colazzo2020@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar SCRGPP60E27M088Z co.llaudoloyalty.colazzo2020@gmail.com d7f4cce7-fe5b-4639-a3dc-b75daea644ba 9e2c5364-f359-47c7-86ed-b5fd701b87a4
*/
// OK - RES - Check Bolletta WEB - Non Attiva

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_31 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","SCRGPP60E27M088Z");
		//prop.setProperty("USERNAME","co.llaudoloyalty.colazzo2020@gmail.com");
		prop.setProperty("USERNAME","r3.2019p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","d7f4cce7-fe5b-4639-a3dc-b75daea644ba");
		prop.setProperty("ENELID","9e2c5364-f359-47c7-86ed-b5fd701b87a4");
		prop.setProperty("EXPIRATION","2022-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \"" + prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		prop.setProperty("JSON_DATA_OUTPUT", "\"isBollettaWebActivated\":false,");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check Bolletta WEB - Non Attiva
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
