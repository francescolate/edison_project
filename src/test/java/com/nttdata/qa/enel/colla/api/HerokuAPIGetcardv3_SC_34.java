// HerokuAPIGetcardv3_SC_34
// Solo per APP  - TOken in input expire 31/12/2021
/*"userUPN": "EE0000018072@BOX.ENEL.COM",
"cf": "FRTFRC85B18G388N",
"enelId": "e884e754-3add-4cdb-9ad4-73fa005eb55e",
"userEmail": "annamaria.cecili@enelsaa3010.enel.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar FRTFRC85B18G388N annamaria.cecili@enelsaa3010.enel.com EE0000018072@BOX.ENEL.COM e884e754-3add-4cdb-9ad4-73fa005eb55e 2021-12-31
**/
// OK - RES - Check Fornitura CON ORE FREE

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_34 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","FRTFRC85B18G388N");
		prop.setProperty("USERNAME","annamaria.cecili@enelsaa3010.enel.com");
		prop.setProperty("USERUPN","EE0000018072@BOX.ENEL.COM");
		prop.setProperty("ENELID","e884e754-3add-4cdb-9ad4-73fa005eb55e");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
        prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \"" + prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
        prop.setProperty("JSON_DATA_OUTPUT", "\"commodity\":\"Elettrico\",\"fasciaOdierna\":\"11\",\"pod\":\"IT001E17747291\",\"isScegliTu\":false,\"businessPartner\":\"0032435555\",\"idAsset\":\"02i1l0000037lS7AAI\",\"numeroOreFree\":\"3\",\"subscription\":false,\"title\":\"Attiva\",\"for_met_pag\":\"Bollettino Postale\",\"contoContrattuale\":\"001032752798\",\"offertaAttiva\":\"Scegli Tu Ore Free\",\"stato\":\"Attiva\",\"isSubscription\":false,\"alias\":\"Luce\",\"isOreFree\":true,\"rvc\":false,\"oreFree\":true,\"isBollettaWebActivated\":true,\"categoriaFornituraCommodity\":\"-\"");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check Fornitura CON ORE FREE
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
