/* HerokuAPICoperturaIndirizzo_SC_236 
 * Need Token  - NEED VPN ENEL
** OK - RES - SWITCH ATTIVO - Dual Energy WEB

"enelId": "93204662-f75b-41a2-a197-0a2e9714073a",
"userEmail": "r.320.19p109statocontiweb@gmail.com"
"userUPN": "06f1a1ca-2138-4f08-beac-edeb3b39a8ad",
"cf": "VNZCML82H21H501H"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_236 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","VNZCML82H21H501H");
		prop.setProperty("USERNAME","r.320.19p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","06f1a1ca-2138-4f08-beac-edeb3b39a8ad");
		prop.setProperty("ENELID","93204662-f75b-41a2-a197-0a2e9714073a");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJFRTAwMDAwMTgxNTJAQk9YLkVORUwuQ09NIiwiYXVkIjoiQXBwTW9iaWxlIiwiY29kaWNlZmlzY2FsZWNvdW50cnkiOiJJVCIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZW5lbGlkIjoiZTY0NzE0NTktZDY4Zi00OTUyLTg0ZjQtNTQ5MzFiOGY2ODczIiwiZXhwIjoxNjA3Njk0MDE2LCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZDQyNDcwOGEtMjcwNS00NTE2LWFmYjAtOTgzODQ4ZmJkZmRjIiwic2lkIjoiN2FiOTRlMGItODA0Mi00ZjA5LTg1NTAtYjM0ODRiMWNmOGMyIn0.1jyqWzsW80_DxuiAfoliy0ZhSEwIA_qMP1W-0EWkvyY");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
      prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Ok\",\"tipoOfferta\":\"Dual Energy\",\"podPdr\":\"00881106009671\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"046253083\",\"flagStd\":0,\"namingInFattura\":\"Gas 20\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"603470134\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5000Y00000TKJkBQAX\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l0Y000000tm5tQAA\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q0Y00000I6BVQUA3\",\"dataRichiesta\":\"2018-03-29T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000nQwXUAU\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA SEBASTIANO ZIANI 7 - 00136 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * OK - RES - SWITCH ATTIVO - Dual Energy WEB
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
