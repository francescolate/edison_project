/* HerokuAPICoperturaIndirizzo_SC_254 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - WEB -  CF/PIVA NON PRESENTE IN CRMT

**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_254 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","0011380031A");
		prop.setProperty("USERNAME","");
		prop.setProperty("USERUPN","");
		prop.setProperty("ENELID","");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
////		prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkY2NkN2YxYS02ZmI5LTQ2MmQtYTJlNi03M2M1NmJkMzNlZmUiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJmNzAwODE1Zi0xYWMwLTQ0MzktOTlhYy1iNDdlN2E1M2ZlNGYiLCJleHAiOjE2MDc2OTI5MDEsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZmRkZmY1NjUtNzVlYS00MzEyLTg5NjMtZTY5NzM0ZTAyMmIzIiwic2lkIjoiNjJhNjJkYWYtZWMxNS00MTdhLTlmNjEtZTVkNGFjYTFmZTdjIn0.tjo0jJ5Tvijg-9Wwnk98VgM4xm0RoB8bVsn9F1SMHJM");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
       prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"offerte\":[],\"totaleStd\":0},\"status\":{\"descrizione\":\"Not found\",\"esito\":\"KO\",\"codice\":\"001\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - WEB -  CF/PIVA NON PRESENTE IN CRMT
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
