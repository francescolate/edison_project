/* HerokuAPIOfferteCRMT_SC_245 
 * Need Token  - NEED VPN ENEL
** OK - RES - RICONTRATTUALIZZAZIONE - ORE FREE In corso - WEB
* "userUPN": "EE0000018072@BOX.ENEL.COM",
"cf": "FRTFRC85B18G388N",
"enelId": "e884e754-3add-4cdb-9ad4-73fa005eb55e",
"userEmail": "annamaria.cecili@enelsaa3010.enel.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_245 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","GLINXT01S28H501B");
		prop.setProperty("USERNAME","r.3.2019p109.statocontiweb@gmail.com");
		prop.setProperty("USERUPN","d3371a31-6421-480f-8090-668876af3341");
		prop.setProperty("ENELID","0df8f3c9-60a7-4c3b-b04d-c2961ebd8305");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkY2NkN2YxYS02ZmI5LTQ2MmQtYTJlNi03M2M1NmJkMzNlZmUiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJmNzAwODE1Zi0xYWMwLTQ0MzktOTlhYy1iNDdlN2E1M2ZlNGYiLCJleHAiOjE2MDc2OTI5MDEsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZmRkZmY1NjUtNzVlYS00MzEyLTg5NjMtZTY5NzM0ZTAyMmIzIiwic2lkIjoiNjJhNjJkYWYtZWMxNS00MTdhLTlmNjEtZTVkNGFjYTFmZTdjIn0.tjo0jJ5Tvijg-9Wwnk98VgM4xm0RoB8bVsn9F1SMHJM");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//	    prop.setProperty("JSON_DATA_OUTPUT", "\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Inviata\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT004E00000166\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3148109\",\"flagStd\":0,\"namingInFattura\":\"Scegli Tu Ore Free\",\"statoR2d\":null,\"numeroUtente\":\"310509789\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001l000005WVrvAAG\",\"idListino\":\"a1l1l000000F0RFAA0\",\"dataAttivazione\":\"2020-03-01T00:00:00.000Z\",\"idCaseItem\":\"a381l000000JM76AAG\",\"dataRichiesta\":\"2020-09-23T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000002NP68EAG\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA NINO BIXIO 3 - 60027 OSIMO AN\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false");
	    prop.setProperty("JSON_DATA_OUTPUT", "\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Inviata\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT004E00000166\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3148109\",\"flagStd\":0,\"namingInFattura\":\"Scegli Tu Ore Free\",\"statoR2d\":null,\"numeroUtente\":\"310509789\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001l000005WVrvAAG\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000F0RFAA0\",\"dataAttivazione\":\"2020-03-01T00:00:00.000Z\",\"idCaseItem\":\"a381l000000JM76AAG\",\"dataRichiesta\":\"2020-09-23T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000002NP68EAG\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA NINO BIXIO 3 - 60027 OSIMO AN\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false");
	    
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * OK - RES - RICONTRATTUALIZZAZIONE - ORE FREE In corso - WEB
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
