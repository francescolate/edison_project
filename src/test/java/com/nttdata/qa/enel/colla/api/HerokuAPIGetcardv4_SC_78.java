// HerokuAPIGetcardv4_SC_78
// Solo per WEB  - TOken in input expire 31/12/2021
/*  "userUPN": "EE0000018152@BOX.ENEL.COM",
        "cf": "TNTFNC84M57L736V"
        "enelId": "e6471459-d68f-4952-84f4-54931b8f6873",
        "userEmail": "r32019p.109statocontiweb@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar TNTFNC84M57L736V r32019p.109statocontiweb@gmail.com EE0000018152@BOX.ENEL.COM e6471459-d68f-4952-84f4-54931b8f6873 2021-12-31
* STESSA UTENZA DELLO SCENARIO 73 e 79*/
// OK - RES - Check Commodity ELE

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.jwt.gen.JWToken;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv4_1;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIGetcardv4_SC_78 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	

		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","TNTFNC84M57L736V");
        prop.setProperty("USERNAME","r32019p.109statocontiweb@gmail.com");
        prop.setProperty("USERUPN","7992b42a-e4d8-4551-bf26-14121001141a");
        prop.setProperty("ENELID","663158be-386c-4b4f-825d-5936ca0083e4");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v4/getcards");
//		prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",      \"username\":\""+prop.getProperty("USERUPN")+"\",       \"keys\":[           {              \"key\":\"APP_VER\",             \"value\":\"9.0.1.0\"          },          {              \"key\":\"OS\",             \"value\":\"IOS\"          },          {              \"key\":\"OS_VER\",             \"value\":\"10.2.1\"          },          {              \"key\":\"DEVICE\",             \"value\":\"iPhone5\"          },          {              \"key\":\"ID_DISPOSITIVO\",             \"value\":\"ABAB288B-1C8D-4C88-95FE-2E9966AE32B3\"          }       ]    } }");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("JSON_DATA_OUTPUT", "\"commodity\":\"Elettrico\",\"pod\":");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check Commodity ELE
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv4_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
