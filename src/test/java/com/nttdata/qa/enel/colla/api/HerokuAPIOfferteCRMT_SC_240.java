/* HerokuAPICoperturaIndirizzo_SC_240 
 * Need Token  - NEED VPN ENEL
** OK - RES - VOLTURA SENZA ACCOLLO - GAS - Annullata - WEB

"userUPN": "41eb30eb-50e8-4851-826f-db85138f647f",
"cf": "HRDNWL85L51Z330A",
"enelId": "dc390e0d-f76d-4c3e-97ed-881e8a992257",
"userEmail": "r.32019p109statocont.iweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_240 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","HRDNWL85L51Z330A");
		prop.setProperty("USERNAME","");
		prop.setProperty("USERUPN","");
		prop.setProperty("ENELID","");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJFRTAwMDAwMTgxNTJAQk9YLkVORUwuQ09NIiwiYXVkIjoiQXBwTW9iaWxlIiwiY29kaWNlZmlzY2FsZWNvdW50cnkiOiJJVCIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZW5lbGlkIjoiZTY0NzE0NTktZDY4Zi00OTUyLTg0ZjQtNTQ5MzFiOGY2ODczIiwiZXhwIjoxNjA3Njk0MDE2LCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZDQyNDcwOGEtMjcwNS00NTE2LWFmYjAtOTgzODQ4ZmJkZmRjIiwic2lkIjoiN2FiOTRlMGItODA0Mi00ZjA5LTg1NTAtYjM0ODRiMWNmOGMyIn0.1jyqWzsW80_DxuiAfoliy0ZhSEwIA_qMP1W-0EWkvyY");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Mancato rientro Piano Casa\",\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"00352800082285\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"2-F7TM59B\",\"flagStd\":0,\"namingInFattura\":\"Energiasicura gas\",\"statoR2d\":\"Da inviare\",\"numeroUtente\":\"752989088\",\"processoInCorso\":\"VOLTURA SENZA ACCOLLO\",\"idCase\":\"5000Y000003hZZxQAM\",\"idListino\":\"a1l24000001KykMAAS\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q0Y00000AOm0RUAT\",\"dataRichiesta\":\"2015-08-03T00:00:00.000Z\",\"idProdotto\":\"a1Y2400000BFIoiEAH\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIALE DEL TORO 24 - 80014 GIUGLIANO IN CAMPANIA NA\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Mancato rientro Piano Casa\",\"numeroOre\":null,\"statoCaseItem\":\"Annullato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":\"Gas\",\"podPdr\":\"00352800082285\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"Bozza\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"2-F7TM59B\",\"flagStd\":0,\"namingInFattura\":\"Energiasicura gas\",\"statoR2d\":\"Da inviare\",\"numeroUtente\":\"752989088\",\"processoInCorso\":\"VOLTURA SENZA ACCOLLO\",\"idCase\":\"5000Y000003hZZxQAM\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l24000001KykMAAS\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q0Y00000AOm0RUAT\",\"dataRichiesta\":\"2015-08-03T00:00:00.000Z\",\"idProdotto\":\"a1Y2400000BFIoiEAH\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIALE DEL TORO 24 - 80014 GIUGLIANO IN CAMPANIA NA\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
       

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * OK - RES - VOLTURA SENZA ACCOLLO - GAS - Annullata - WEB
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
