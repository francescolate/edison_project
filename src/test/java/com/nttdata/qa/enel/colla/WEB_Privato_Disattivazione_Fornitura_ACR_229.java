package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_229_ACR_Disattivazione;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Disattivazione_Fornitura_ACR_229 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll2.enel.it/");
		//prop.setProperty("USERNAME", "collaudo10@yopmail.com");	
		prop.setProperty("WP_USERNAME", "r32019p109s.tatocontiweb@gmail.com");	
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("EMAIL", "collaudo10@yopmail.com");
		prop.setProperty("CONFERMA_EMAIL","collaudo10@yopmail.com");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_229_ACR_Disattivazione.main(args);
		
		
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
	

}
