package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Bolletta_Web_ID307;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Attiva_Bolletta_WEB_ACR_307 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.a.f.faellaquomo@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SERVIZI_PER_LE_BOLLETTE", "Servizi per le bollette");
		prop.setProperty("BOLLETTA_WEB_TITLE", "Bolletta Web");
		prop.setProperty("BOLLETTA_WEB_TITLE_SUBTEXT", "Il servizio Bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica di emissione tramite SMS.");
		prop.setProperty("NONACTIVESUPPLYSTATUS", "Il servizio Bolletta Web non è attivo sulla fornitura:");
		prop.setProperty("ATTIVAZIONE_BOLLETTA_WEB_TITLE", "Attivazione Bolletta Web");
		prop.setProperty("ATTIVAZIONE_BOLLETTA_WEB_TITLE_SUBTEXT", "Seleziona una o più forniture su cui intendi attivare il servizio di Bolletta Web.");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_Bolletta_Web_ID307.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}


