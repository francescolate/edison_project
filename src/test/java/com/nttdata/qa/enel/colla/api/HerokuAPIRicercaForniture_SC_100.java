/*HerokuAPIRicercaForniture_SC_100 
** API call for Tokens
* RicercaForniture - OK - RES At least one supply ELE in "Attivo" state
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar TNTFNC84M57L736V r32019p.109statocontiweb@gmail.com EE0000018152@BOX.ENEL.COM e6471459-d68f-4952-84f4-54931b8f6873 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica_CF_PIVA;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIRicercaForniture_SC_100 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","TNTFNC84M57L736V");
		prop.setProperty("USERNAME","r32019p.109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","EE0000018152@BOX.ENEL.COM");
		prop.setProperty("ENELID","e6471459-d68f-4952-84f4-54931b8f6873");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiRUUwMDAwMDE4MTUyQEJPWC5FTkVMLkNPTSIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMTE1MTA4LCJlbmVsaWQiOiJlNjQ3MTQ1OS1kNjhmLTQ5NTItODRmNC01NDkzMWI4ZjY4NzMiLCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiYWJhMDMwMDctZGM3My00NjJiLTkxNzktMDVhNzg0YTJlMWFmIn0.KiRSwOP4IC9t8QbkhzCAFitYYURQkTvZGXd_tEitAvA");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-web-hp-extra-stage/v1/info/ricerca/forn?cf=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"stato\":\"Attivo\",\"listinoInFattura\":\"BIORARIA\",\"fornKey\":\"0010Y00000FO1tpQAD|02i0Y000001p7a0QAA|838415696\",\"codice\":\"838415696\",\"indirizzo\":\"VIA VERDI 8 - 35028 PIOVE DI SACCO PD\",\"name\":null,\"partitaIvaCliente\":null,\"tipoForn\":\"Elettrico\",\"flagDefault\":null,\"contrattoSAP\":\"0019377822\",\"bpAccountSAP\":\"0032872111\",\"contractAccountSAP\":\"001016400496\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * RicercaForniture - OK - RES At least one supply ELE in "Attivo" state 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica_CF_PIVA.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
