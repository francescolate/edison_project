/* HerokuAPIOfferteCRMT_SC_231 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - WEB - RES - VOLTURA SENZA ACCOLLO - ELE - ESPLETATA
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar DDNCDM67C10H355I r.3.201.9p109statocontiweb@gmail.com 6f2dcab3-e3c8-4b44-ad93-994da9cdc01b 243953a4-e180-4c91-bed4-18f1e4c1a67b 2020-12-31
"userUPN": "6f2dcab3-e3c8-4b44-ad93-994da9cdc01b",
"cf": "DDNCDM67C10H355I",
"enelId": "243953a4-e180-4c91-bed4-18f1e4c1a67b",
"userEmail": "r.3.201.9p109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIOfferteCRMT_SC_231 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","DDNCDM67C10H355I");
		prop.setProperty("USERNAME","r.3.201.9p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","6f2dcab3-e3c8-4b44-ad93-994da9cdc01b");
		prop.setProperty("ENELID","243953a4-e180-4c91-bed4-18f1e4c1a67b");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"NON PREVISTO\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E01572668\",\"statoSap\":\"OK\",\"statoSempre\":\"OK\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ1210031\",\"flagStd\":0,\"namingInFattura\":\"GiustaXTe\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"305082285\",\"processoInCorso\":\"VOLTURA SENZA ACCOLLO\",\"idCase\":\"5001n000012SVkGAAW\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1n000000ehb6AAA\",\"dataAttivazione\":\"2019-04-04T00:00:00.000Z\",\"idCaseItem\":\"a1Q1n00000Pj5G2EAJ\",\"dataRichiesta\":\"2019-02-28T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000cwB7UAI\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"CORSO SUSA 86 - 10098 RIVOLI TO\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - WEB - RES - SWITCH ATTIVO GAS - Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
