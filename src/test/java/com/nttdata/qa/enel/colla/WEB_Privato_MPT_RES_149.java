package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModifyPowerAndVoltage;
import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.GetAccountFromEmail;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyRequestDetails;
import com.nttdata.qa.enel.testqantt.VerifyRicheste;
import com.nttdata.qa.enel.testqantt.colla.ChangePowerAndVoltage;
import com.nttdata.qa.enel.testqantt.colla.ChangePowerAndVoltagePreventivo;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MPT_RES_149 {
	
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "web.t.esting.automation@gmail.com");
		prop.setProperty("WP_PASSWORD", "Enel$001");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("ADDRESS", "Via Abetone 44 00141 Roma Roma Rm");
		prop.setProperty("POD", "IT002E7347634A");
		prop.setProperty("POWERDEFAULT", "3 kW(potenza attuale)");
		prop.setProperty("VOLTAGEFAULT", "220 V(tensione attuale)");
		prop.setProperty("POWER_INPUT", "15 kW");
		prop.setProperty("ARGANO", "Argano");
		prop.setProperty("STARTINGCURRENTINPUT", "60");
		prop.setProperty("RUNNINGCURRENTINPUT", "20");
		prop.setProperty("CELLULAR", "3371501035");
		prop.setProperty("POTENZA", "15 kW");
		prop.setProperty("TENSIONE", "380 V");

		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		ModifyPowerAndVoltage.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args);
		//GetAccountFromEmail.main(args);
		//VerifyRequestDetails.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
