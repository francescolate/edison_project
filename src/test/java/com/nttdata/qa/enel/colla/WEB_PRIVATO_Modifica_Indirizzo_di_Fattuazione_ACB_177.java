package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.ModificaIndirizzoFattuazione_177;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Indirizzo_di_Fattuazione_ACB_177 {

	//private RemoteWebDriver driver; 
	//Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("WP_USERNAME", "r.32019p109statoc.ontiweb@gmail.com");
//	    prop.setProperty("WP_USERNAME", "r32019p109st.atocontiweb@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "BSN");
	    prop.setProperty("CLIENT_NUMBER", "654995134");
//	    prop.setProperty("CLIENT_NUMBER", "975417086");
	    prop.setProperty("AREA_CLIENTI", "BUSINESS");
	    	    	    	   	    
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	Login_Private_Area.main(args);
		LoginEnel.main(args);
	//	PrivateAreaMenu.main(args);
		ModificaIndirizzoFattuazione_177.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}




