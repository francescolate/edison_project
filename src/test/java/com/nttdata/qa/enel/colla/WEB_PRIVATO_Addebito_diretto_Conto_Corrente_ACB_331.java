package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerifyPodDetails331;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_331_ACB_Addebito_Diretto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_PRIVATO_Addebito_diretto_Conto_Corrente_ACB_331 {

	Properties  prop;
	final String nomeScenario="WEB_PRIVATO_Addebito_diretto_Conto_Corrente_ACB_331.properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "testimpresa@yopmail.com"); 
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "IMPRESA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_PATH", "AREA RISERVATA HOMEPAGE");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area dedicata Business");
		prop.setProperty("FORNITURE_PATH", "AREA RISERVATA FORNITURE");
		prop.setProperty("FORNITURE_HEADING", "Tutte le tue forniture");
		prop.setProperty("SERVIZI_PATH", "AREA RISERVATA SERVIZI");
		prop.setProperty("SERVIZI_SUBTEXT", "In questa pagina trovi tutti i servizi.");
		prop.setProperty("ADDEBITO_DIRETTO_PATH", "AREA RISERVATA SERVIZI ADDEBITO DIRETTO");
		prop.setProperty("ADDEBITO_DIRETTO_TITLE", "Addebito Diretto");
		prop.setProperty("ADDEBITO_DIRETTO_SUBTEXT", "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sul Tuo Conto Corrente");
		prop.setProperty("ADDEBITO_DIRETTO_NONACTIVE_STATUS", "Il servizio Addebito Diretto non è attivo sulla fornitura:");
		prop.setProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_PATH", "AREA RISERVATAFORNITUREATTIVAZIONE ADDEBITO DIRETTO");
		prop.setProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT", "Il servizio automatico che paga le tue bollette.");
		prop.setProperty("CHECOS", "Che cos'è?");
		prop.setProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT2", "Verifica i campi sottostanti e clicca su conferma");
		prop.setProperty("SUPPLY_INFO", "Ecco le forniture su cui modifichermo Addebito Diretto");
		prop.setProperty("EMAIL_INFO", "Inserisci la tua mail per ricevere aggiornamenti sulla richiesta.");
		prop.setProperty("NUMEROCLIENTE1", "310512806");
		prop.setProperty("NUMEROCLIENTE2", "310514133");
		
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		
		prop.setProperty("POD1_IT002E5928147A", "IT002E5928147A");
		prop.setProperty("POD2_IT002E3401567A", "IT002E3401567A");
		prop.setProperty("LINK_UDB", "http://dtcmmind-bw-01.risorse.enel:8887");
		prop.setProperty("PROCESSO", "ATTIVAZIONE SDD");
		prop.setProperty("STATO", "In attesa");
		prop.setProperty("METODO_DI_PAGAMENTO_HEADING", "Metodo di pagamento");
		prop.setProperty("NUOVO_METODO_DI_PAGAMENTO", "SDD");
		prop.setProperty("NUOVO_TITOLARE_DEL_CONTO_BANCARIO", "IMPRESA MAIRA OPEN ENERGY");
		prop.setProperty("CF_DEL_NUOVO_TITOLARE_DEL_CONTO_BANCARIO", "07351980565");
		prop.setProperty("IBAN_VALUE", "IT30P0301503200000000246802");
		prop.setProperty("SERVICEREQUEST_LINK","https://workbench.developerforce.com/login.php");
		
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_331_ACB_Addebito_Diretto.main(args);
		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
		VerifyPodDetails331.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}