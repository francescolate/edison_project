package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SearchAccountEmail;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_225_ACR_Disattivazione;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Disattivazione_Fornitura_ACR_225 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll2.enel.it/");
		prop.setProperty("WP_USERNAME", "r32019p109s.tatocontiweb@gmail.com");		
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("EMAIL", "r32019p109s.tatocontiweb@gmail.com");
		prop.setProperty("CONFERMA_EMAIL", "r32019p109s.tatocontiweb@gmail.com");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.setProperty("CF", "MCCFRC91P54H501V");
		prop.setProperty("AREA_CLIENTI", "CASA");
	    prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
//		LoginEnel.main(args);
//		Privato_225_ACR_Disattivazione.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		SearchAccountEmail.main(args);
		
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
			
	};

}
