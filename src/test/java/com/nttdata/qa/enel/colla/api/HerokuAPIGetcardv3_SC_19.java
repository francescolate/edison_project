// HerokuAPIGetcardv3_SC_19
// Solo per APP  - TOken in input expire 31/12/2021
/*"userUPN": "d7f4cce7-fe5b-4639-a3dc-b75daea644ba",
"cf": "SCRGPP60E27M088Z",
"enelId": "9e2c5364-f359-47c7-86ed-b5fd701b87a4",
"userEmail": "co.llaudoloyalty.colazzo2020@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\Genera_Token_API_WEB\MobileTokenJWT.jar SCRGPP60E27M088Z co.llaudoloyalty.colazzo2020@gmail.com d7f4cce7-fe5b-4639-a3dc-b75daea644ba 9e2c5364-f359-47c7-86ed-b5fd701b87a4
*/
// OK - RES - Check "Stato Fornitura" = ATTIVA

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_19 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	

		prop.setProperty("RUN_LOCALLY","Y");
		//prop.setProperty("CF","SCRGPP60E27M088Z");
		prop.setProperty("CF","LVIVGR69C06Z104T");
		prop.setProperty("USERNAME","r3.2019p109statocontiweb@gmail.com");
		//prop.setProperty("USERUPN","d7f4cce7-fe5b-4639-a3dc-b75daea644ba");
		prop.setProperty("USERUPN","5c4913e0-642b-4a0a-849f-80f78aac8696");
		//prop.setProperty("ENELID","9e2c5364-f359-47c7-86ed-b5fd701b87a4");
		prop.setProperty("ENELID","58c8a7bd-3d74-47d5-a051-bb009cf4d489");
		prop.setProperty("EXPIRATION","2022-12-31");
		prop.setProperty("PASSWORD","Password01");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \""+prop.getProperty("USERNAME")+"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
		prop.setProperty("JSON_DATA_OUTPUT", "\"Cards\":[{\"commodity\":\"ELETTRICO\",\"codiceStato\":\"FT_18\",\"pod\":\"IT017E06001203\",\"isScegliTu\":false,\"subscription\":false,\"title\":\"In lavorazione\",\"offertaAttiva\":\"GiustaXTe\",\"stato\":\"In lavorazione\",\"tipoAttivazione\":\"Subentro\",\"isSubscription\":false,\"alias\":\"Luce\",\"isOreFree\":false,\"rvc\":false,\"oreFree\":false,\"categoriaFornituraCommodity\":\"ELETTRICO\",\"Color\":{\"titleRightColor\":\"#FF5A0F\",\"backgroundColor\":\"#FFFFFF\",\"textRightFooterColor\":\"#B2B2B2\",\"textFooterColor\":\"\",\"barFooterColor\":\"#000000\",\"lineColor\":\"#ECECEC\",\"subBarFooterColor\":\"#FF5A0F\",\"subTitleLeftColor\":\"#7F7F7F\",\"titleLeftColor\":\"#000000\",\"textLeftFooterColor\":\"#FF5A0F\"},");				
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check "Stato Fornitura" = ATTIVA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
