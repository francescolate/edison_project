package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.RinominaFornitura;

public class TC_AO006537_AREA_CLIENTI_RES_Rinomina_Fornitura {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario="TC_AO006537_AREA_CLIENTI_RES_Rinomina_Fornitura.properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-colla.enel.it/");
		prop.setProperty("USERNAME", "r32019p109statocontiweb@gmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("STATO", "Attiva");
		prop.setProperty("NUM_UTENTE", "648767961");
		prop.setProperty("DETTAGLIO_FORNITURA", "VIA CHIESA 3 - 24010 BRACCA BG");
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	
	};
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		RinominaFornitura.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		
	};
}
