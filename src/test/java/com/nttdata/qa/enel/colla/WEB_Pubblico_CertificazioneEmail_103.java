package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.ID_102_RES_FREE_SWA_ELE_OCR;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent_103;
import com.nttdata.qa.enel.testqantt.colla.VerifyRequestDetails_103;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_CertificazioneEmail_103 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");		
	    prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
	    prop.setProperty("NOME", "PETER");
	    prop.setProperty("COGNOME", "PARKER");
	    prop.setProperty("CELLULARE", "7874637282");
	    prop.setProperty("CF", "PRKPTR72B03H502P");
	    prop.setProperty("CAP", "60027-OSIMO");
	    prop.setProperty("POD", "116900000000");
	    prop.setProperty("CITTA", "OSIMO,ANCORA, MARCHE");
	    prop.setProperty("INDRIZZO", "VIA MONTECERNO");
		prop.setProperty("NUMEROCIVICO", "4");		
		prop.setProperty("ATTUALE_FORNITORE", "ACEA ENERGIA SPA");
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.setProperty("USERNAME",Costanti.utenza_admin_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_admin_salesforce);
		prop.store(new FileOutputStream(nomeScenario), null);	

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};	
		
		VerifyEmailSubjectAndContent_103.main(args);
		VerifyRequestDetails_103.main(args);
		ID_102_RES_FREE_SWA_ELE_OCR.main(args);
		 
	};
	
	@After
    public void fineTest() throws Exception{

	};
}
