package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.VerifyRequestDetails_275;
import com.nttdata.qa.enel.testqantt.colla.ACR_275_InfoEnelEnegia;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_INFO_ENEL_ENERGIA_ACR_275 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//prop.setProperty("WP_USERNAME", "r32019p109statocontiwe.b@gmail.com"); 
		// new users
			prop.setProperty("WP_USERNAME", "testin.gc.rmautomation@gmail.com"); // FN team checked //TMSLGU45B09A766B  --647326902
		//prop.setProperty("WP_USERNAME", "collaud.oloyaltycolazzo2.020@gmail.com");
		//prop.setProperty("WP_USERNAME", "r32019p109statocont.iwe.b@gmail.com");
		//prop.setProperty("WP_USERNAME", "testin.g.c.r.m.a.u.tomation@gmail.com");//FN team checked 
		//prop.setProperty("WP_USERNAME", "t.est.i.n.g.c.r.m.a.u.t.o.mation@gmail.com");
		
		//collaud.oloyaltycolazzo2.020@gmail.com - DNGVCN34M01A662Z
		//r32019p109statocont.iwe.b@gmail.com - RZZGPP48M15B538J
		
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("MODULE_ENABLED", "Y");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
	    
		prop.store(new FileOutputStream(nomeScenario), null);			
    };
    
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		ACR_275_InfoEnelEnegia.main(args);
		VerifyRequestDetails_275.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
	    InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}