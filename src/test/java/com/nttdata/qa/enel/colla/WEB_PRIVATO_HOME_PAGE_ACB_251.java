package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.ACB_251_Menu_Lateral;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_HOME_PAGE_ACB_251 {
	
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";

		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			
			prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
			prop.setProperty("WP_USERNAME", "r.3.2.0.19p109.statocontiweb@gmail.com");
			prop.setProperty("WP_PASSWORD", "Password01");
		    prop.setProperty("RUN_LOCALLY","Y");
		    prop.setProperty("MODULE_ENABLED", "Y");
		    prop.setProperty("AREA_CLIENTI", "");
		    prop.setProperty("ACCOUNT_TYPE", "BSN");

			prop.store(new FileOutputStream(nomeScenario), null);		

	    };
		
		@Test
	    public void eseguiTest() throws Exception{
			String args[]= {nomeScenario};
			VerifyLoginEnelArea.main(args);
			ACB_251_Menu_Lateral.main(args);
			
		};
		
		@After
	    public void fineTest() throws Exception{
			InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
		};
}
