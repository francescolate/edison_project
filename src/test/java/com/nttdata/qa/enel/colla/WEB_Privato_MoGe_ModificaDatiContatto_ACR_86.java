package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.testqantt.GetAccountEmail;
import com.nttdata.qa.enel.testqantt.GetAccountFromCF;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaAccount;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu;
import com.nttdata.qa.enel.testqantt.colla.PrivateAreaMenu41;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_MoGe_ModificaDatiContatto_ACR_86 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
	    prop.setProperty("LINK", Costanti.salesforceLink);
	    prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
	    prop.setProperty("WP_USERNAME", "r.affael.laquomo@gmail.com");
	    prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
	    prop.setProperty("ACTION_TYPE", "EMAIL");
	    prop.setProperty("CF", "MNCSVT58H26H325L");
	    prop.setProperty("ACCOUNT_ID", "0010Y00000G8JwnQAF");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("TITOLARE", "IDEAL COLOR SNC");
	    prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		GetAccountEmail.main(args);
		LoginEnel.main(args);
		//PrivateAreaMenu41.main(args);
		PrivateAreaAccount.main(args);
		LoginSalesForce.main(args);
		//SbloccaTab.main(args);
		//GetAccountFromCF.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
