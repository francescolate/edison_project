package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Privato_206_ACR_Menù_Laterale;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Homepage_Menù_laterale_sinsitro_206 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("LINK", BASE_LINK);
		prop.setProperty("USERNAME", "collaudoloyaltycolazzo.202.0@gmail.com");		
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("FORNITURE_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale");
		prop.setProperty("NOVITA_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale/novita");
		prop.setProperty("NOVITA_SECTION1", "https://www.enel.it/it/contatti/ac-blackfriday");
		prop.setProperty("NOVITA_SECTION2", "https://www-coll1.enel.it/it/area-clienti/residenziale/novita#");
		prop.setProperty("NOVITA_SECTION3", "https://www-coll1.enel.it/it/supporto/faq/app-enel");
		prop.setProperty("NOVITA_SECTION4", "https://www.ufirst.com/country-selection");
		prop.setProperty("SUPPORTO_URL", "https://www-coll1.enel.it/it/supporto");
		prop.setProperty("SUPPORTO_FAQ", "https://www-coll1.enel.it/it/supporto/faq/faq-enel-energia?zoneid=supporto-hero");
		prop.setProperty("TRAVOSPAZIO_URL", "https://www-coll1.enel.it/spazio-enel");

		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null); 
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Privato_206_ACR_Menù_Laterale.main(args);
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
