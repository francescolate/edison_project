package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Pubblico_ID_80_Motorino_Ricerca;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Motorinodiricerca_80 {

	Properties prop;
	
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/it/luce-e-gas/luce-e-gas/negozio-ufficio?nec=pa&sortType=in-promozione");
		prop.setProperty("HOMEPAGE", "https://www-coll1.enel.it/");
		prop.setProperty("VISUALIZZA_OFFERTA_LUCE_LINK", "https://www-coll1.enel.it/it/luce-e-gas/luce/negozio-ufficio");
		prop.setProperty("VISUALIZZA_OFFERTA_GAS_LINK", "https://www-coll1.enel.it/it/luce-e-gas/gas/negozio-ufficio");
		prop.setProperty("CONTATTA_LINK", "https://www-coll1.enel.it/it/form-ricontatto-commerciale");
		prop.setProperty("LUCE_E_GAS_VOLTURA_LINK", "https://www-coll1.enel.it/it/luce-e-gas/luce-e-gas/negozio-ufficio?nec=voltura&sortType=in-promozione");
		prop.setProperty("CREDITS_FOOTER_LINK", "https://www-coll1.enel.it/it/supporto/faq/credits");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		Pubblico_ID_80_Motorino_Ricerca.main(args);
	};
	
	@After
    public void endTest() throws Exception{
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}