package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACR_281;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Info_Enel_Energia_ACR_281 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//Given 
		//prop.setProperty("WP_USERNAME", "collaud.oloyaltycolazzo2.020@gmail.com");
		//Other user which has active supply 
		//prop.setProperty("WP_USERNAME", "testingcr.m.a.u.t.o.m.a.tion@gmail.com"); //GRGFLV74T27L219N
		prop.setProperty("WP_USERNAME", "testin.gc.rmautomation@gmail.com"); 
		//prop.setProperty("WP_USERNAME", "testin.gc.rmautomation@gmail.com"); //TMSLGU45B09A766B
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Info_Enel_Energia_ACR_281.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}