package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LaunchCaricaDocumenti;
import com.nttdata.qa.enel.testqantt.colla.Publicco_ID_95_Form_Scrivici;
import com.nttdata.qa.enel.testqantt.colla.VerifyEmailSubjectAndContent_95;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_FormScrivici_95 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
	this.prop=new Properties();
	
	prop.setProperty("WP_LINK", "https://www-coll1.enel.it/it/login");
	
	prop.setProperty("RUN_LOCALLY","Y");
	prop.store(new FileOutputStream(nomeScenario), null);
  };

    @Test
    public void runTest() throws Exception{
    	String args[]= {nomeScenario};
		LaunchCaricaDocumenti.main(args);
		Publicco_ID_95_Form_Scrivici.main(args);
		VerifyEmailSubjectAndContent_95.main(args);
  };

   @After
    public void endTest() throws Exception{
	   	InputStream in = new FileInputStream(nomeScenario);
	    this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	    ReportUtility.reportToServer(this.prop);
  };

}
