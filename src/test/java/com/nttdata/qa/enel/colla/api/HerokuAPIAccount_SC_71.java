// HerokuAPIAccount_SC_71
// Solo per WEB  - TOken in input expire 31/12/2020
/*"userUPN": "5318676e-3037-4eeb-afaf-0a069a0dc6a6",
**"cf": "NCLGPP87E48F112P",
**"enelId": "18003079-add1-42de-b644-a3be38364804",
**"userEmail": "r.32.019p109statocontiweb@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar NCLGPP87E48F112P r.32.019p109statocontiweb@gmail.com 5318676e-3037-4eeb-afaf-0a069a0dc6a6 18003079-add1-42de-b644-a3be38364804 2021-12-31
*/
// KO - Token errato (troncato) 

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIAccount_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIAccount_SC_71 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	

		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","18003079-add1-42de-b644-a3be38364804");
		prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1Qi");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/authentication/v2/account");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("JSON_INPUT", "{   \"data\":{      \"enelId\":\"" + prop.getProperty("ENELID") + "\"  }}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"result\":\"GEN10\",\"code\":\"-4\",\"description\":\"Sessione non valida, si prega di effettuare nuovamente l'accesso\"}");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - Token errato (troncato) 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIAccount_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
