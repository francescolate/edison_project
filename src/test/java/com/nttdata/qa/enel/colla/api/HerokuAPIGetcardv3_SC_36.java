// HerokuAPIGetcardv3_SC_36
// Solo per APP  - TOken in input expire 31/12/2021
/*"userUPN": "d5f8114b-177e-4ab4-be28-9685110c4dff",
"cf": "NDRVTI70A16B180O",
"enelId": "c88b0269-daa8-4c1e-bb56-5233da6328df",
"userEmail": "affiddaqe-8312@yopmail.com"
*/
// OK - Check with user LIKE-RES

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_36 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","NDRVTI70A16B180O");
		prop.setProperty("USERNAME","affiddaqe-8312@yopmail.com");
		prop.setProperty("USERUPN","d5f8114b-177e-4ab4-be28-9685110c4dff");
		prop.setProperty("ENELID","c88b0269-daa8-4c1e-bb56-5233da6328df");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
        prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \"" + prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
        prop.setProperty("JSON_DATA_OUTPUT", "\"commodity\":\"GAS\",\"codiceStato\":\"FT_16\",\"pod\":\"00887777777777\",\"isScegliTu\":false,\"subscription\":false,\"title\":\"In lavorazione\",\"offertaAttiva\":\"Solo XTe Gas Impresa\",\"stato\":\"In lavorazione\",\"tipoAttivazione\":\"Switch attivo\",\"isSubscription\":false,\"alias\":\"Gas\",\"isOreFree\":false,\"rvc\":false,\"oreFree\":false,\"categoriaFornituraCommodity\":\"GAS\",\"Color\":{\"titleRightColor\":\"#FF5A0F\",\"backgroundColor\":\"#FFFFFF\",\"textRightFooterColor\":\"#B2B2B2\",\"textFooterColor\":\"\",\"barFooterColor\":\"#000000\",\"lineColor\":\"#ECECEC\",\"subBarFooterColor\":\"#FF5A0F\",\"subTitleLeftColor\":\"#7F7F7F\",\"titleLeftColor\":\"#000000\",\"textLeftFooterColor\":\"#FF5A0F\"},\"okSap\":false,\"isRvc\":false,\"idListino\":\"a1l1l000000ORZuAAO\",\"dataAttivazione\":\"07.12.2019\",\"codiceFornitura\":\"310493216\",\"idProdotto\":\"\"");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - Check with user LIKE-RES
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
