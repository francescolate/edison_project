// HerokuAPIGetcardv3_SC_27
// Solo per APP  - TOken in input expire 31/12/2021
/*   "userUPN": "EE0000018152@BOX.ENEL.COM",
        "cf": "TNTFNC84M57L736V"
        "enelId": "e6471459-d68f-4952-84f4-54931b8f6873",
        "userEmail": "r32019p.109statocontiweb@gmail.com"
java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar TNTFNC84M57L736V r32019p.109statocontiweb@gmail.com EE0000018152@BOX.ENEL.COM e6471459-d68f-4952-84f4-54931b8f6873 2021-12-31
*/
// OK - RES - Check Metodo di Pagamento - RID

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_27 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		
		
		prop.setProperty("CF","TNTFNC84M57L736V");
		prop.setProperty("USERNAME","r32019p.109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","7992b42a-e4d8-4551-bf26-14121001141a");
		prop.setProperty("ENELID","663158be-386c-4b4f-825d-5936ca0083e4");
		
//		prop.setProperty("USERUPN","EE0000018152@BOX.ENEL.COM");
//		prop.setProperty("ENELID","e6471459-d68f-4952-84f4-54931b8f6873");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
//		prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \""+prop.getProperty("USERNAME")+"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
//		prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		prop.setProperty("JSON_DATA_OUTPUT", "\"commodity\":\"Elettrico\",\"pod\":\"IT001E31809747\",\"isScegliTu\":false,\"businessPartner\":\"0032872111\",\"idAsset\":\"02i0Y000001p7a0QAA\",\"subscription\":false,\"title\":\"Attiva\",\"for_met_pag\":\"RID\",\"contoContrattuale\":\"001016400496\",\"offertaAttiva\":\"BIORARIA\",\"stato\":\"Attiva\",\"isSubscription\":false,\"for_met_pag_iban\":\"IT07R0306234210000001766191\"");
				
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check Metodo di Pagamento - RID
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
