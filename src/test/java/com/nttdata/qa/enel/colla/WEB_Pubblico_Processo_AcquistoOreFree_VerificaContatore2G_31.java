package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.MotorinoDiRicerca;
import com.nttdata.qa.enel.testqantt.colla.OreFreeID31;
import com.nttdata.qa.enel.testqantt.colla.PaginaAdesioneFornitura;
import com.nttdata.qa.enel.testqantt.colla.PaginaAdesioneInserisciDati;
import com.nttdata.qa.enel.util.ReportUtility;





public class WEB_Pubblico_Processo_AcquistoOreFree_VerificaContatore2G_31 {
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
		prop.setProperty("TYPE_OF_CONTRACT", "Luce");
		prop.setProperty("PLACE", "Casa");
		prop.setProperty("NEED", "VISUALIZZA TUTTE");
		prop.setProperty("CARATTERI_SPECIALI", "%&!");
		prop.setProperty("NOME", "MARIO");
		prop.setProperty("COGNOME", "ROSSI");
		prop.setProperty("DATA_NASCITA", "02-3-2002");
		prop.setProperty("CITTA", "COSENZA (CS)");
		prop.setProperty("SESSO","UOMO");
		prop.setProperty("NAZIONE","ITALIA");
		prop.setProperty("CELLULARE", "3201234567");
		prop.setProperty("CELLULARE_VUOTO", "");
		prop.setProperty("CELLULARE_8_CARATTERI", "32012345");
		prop.setProperty("CELLULARE_CARATTERI", "aA");
		prop.setProperty("EMAIL", "g.colazzo@lutech.it");
		prop.setProperty("EMAIL_SENZA_@", "g.colazzolutech.it");
		prop.setProperty("EMAIL_SENZA_IT", "g.colazzo@lutech");
		prop.setProperty("EMAIL_VUOTA", "");
		prop.setProperty("CAP", "87100 - COSENZA");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
		    
		};
		
		@Test
	    public void eseguiTest() throws Exception{
			String args[]= {nomeScenario};
			
			Login_Public_Area.main(args);
			MotorinoDiRicerca.main(args);
			OreFreeID31.main(args);
			//PaginaAdesioneInserisciDati.main(args);
			//PaginaAdesioneFornitura.main(args);
		};
		
		@After
	    public void fineTest() throws Exception{
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
			ReportUtility.reportToServer(this.prop);
			
		};
		
}
