/* HerokuAPICoperturaIndirizzo_SC_212 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - SWA MULTI GAS
"enelId": "24eae849-ec8d-4a10-a0d4-d06769274d38",
"email": "testing.crm.automation@gmail.com",
CF TMTRDN88T10F839M 
"userUpn": "4c78e304-25e4-4fc5-a83a-3266354bb337",
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIOfferteCRMT_SC_212 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","TMTRDN88T10F839M");
		prop.setProperty("USERNAME","testing.crm.automation@gmail.com");
		prop.setProperty("USERUPN","4c78e304-25e4-4fc5-a83a-3266354bb337");
		prop.setProperty("ENELID","24eae849-ec8d-4a10-a0d4-d06769274d38");
		prop.setProperty("EXPIRATION","2021-12-31");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//      prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"In attesa\",\"statoUdb\":null,\"tipoOfferta\":\"Multi Gas\",\"podPdr\":\"05269993590189\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"BOZZA\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3152750\",\"flagStd\":1,\"namingInFattura\":\"SoloXTe Gas\",\"statoR2d\":\"Verificato\",\"numeroUtente\":\"310524292\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5001l000005fCJ4AAM\",\"idListino\":\"a1l1n0000011ovnAAA\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000d7nyEAA\",\"dataRichiesta\":\"2020-10-05T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000nbUHUAY\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA NIZZA 4 - 00198 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"In attesa\",\"statoUdb\":null,\"tipoOfferta\":\"Multi Gas\",\"podPdr\":\"05269993590189\",\"statoSap\":\"BOZZA\",\"statoSempre\":\"BOZZA\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3152750\",\"flagStd\":1,\"namingInFattura\":\"SoloXTe Gas\",\"statoR2d\":\"Verificato\",\"numeroUtente\":\"310524292\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5001l000005fCJ4AAM\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1n0000011ovnAAA\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q1l000000d7nyEAA\",\"dataRichiesta\":\"2020-10-05T00:00:00.000Z\",\"idProdotto\":\"a1Y0Y000000nbUHUAY\",\"commodityCaseItem\":\"GAS\",\"indirizzoFornitura\":\"VIA NIZZA 4 - 00198 ROMA RM\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
        
        
        
        
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - SWA MULTI GAS
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
