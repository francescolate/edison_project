/*HerokuAPIUserByEnelID_SC_63
** KO - No user - WEB (not exist) 
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIUserByEnelID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIUserByEnelID_SC_63 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ENELID","de8f7bc2-3c7d-4784-9009-00e58d975468");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
//		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/" + prop.getProperty("ENELID"));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/enelid/user/v1/userbyenelid/");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"user\":null},\"meta\":{\"path\":\"/user/v1/userbyenelid/{enelId}\",\"method\":\"get-user-by-enel-id\",\"dataAggiornamento\":");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * KO - No user - WEB (not exist)  
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIUserByEnelID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
