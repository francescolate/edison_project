/*HerokuAPIFornitureACB_SC_282 
** API call for Tokens - Need VPN ENEL
* Fornitura ACB - OK - WEB - BSN Stato Fornitura SOSPESA 
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 02109250692 r32019p109statocontiw.e.b@gmail.com 11e9cf36-f8f8-4067-ad14-5672f06281b8 2d7ab8a9-32e4-4ffc-91cd-9a716944910d 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIFornitureACB;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIFornitureACB_SC_282 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("PIVA","02109250692");
		prop.setProperty("USERNAME","r32019p109statocontiw.e.b@gmail.com");
		prop.setProperty("USERUPN","11e9cf36-f8f8-4067-ad14-5672f06281b8");
		prop.setProperty("ENELID","2d7ab8a9-32e4-4ffc-91cd-9a716944910d");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiMTFlOWNmMzYtZjhmOC00MDY3LWFkMTQtNTY3MmYwNjI4MWI4IiwiY29kaWNlZmlzY2FsZSI6IjAyMTA5MjUwNjkyIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMDkwNjcxLCJlbmVsaWQiOiIyZDdhYjhhOS0zMmU0LTRmZmMtOTFjZC05YTcxNjk0NDkxMGQiLCJ1c2VyaWQiOiJyMzIwMTlwMTA5c3RhdG9jb250aXcuZS5iQGdtYWlsLmNvbSIsImp0aSI6ImI4MTY2OTMzLTU2YjMtNDg3OC05N2I4LWIyMzg0MTdhODE1YyJ9.sasWr-E0icZrXyCOOJQr9a2ngLLkbAUTGxUbHMLZicQ");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", " http://10.151.58.208:82/enel-web-hp-extra-stage/v1/info/ricercaFornitureAcb?cf=" + prop.getProperty("CF_PIVA") + "&numMesiMaxCess=12");
		prop.setProperty("JSON_DATA_OUTPUT","{\"data\":{\"forniture\":[{\"idAccount\":\"0010Y00000F6pbGQAR\",\"numeroUtente\":\"784802249\",\"listinoInFattura\":\" Solo Condominio Gas\",\"partitaIvaCliente\":\"02109250692\",\"idAsset\":\"02i0Y000000l97eQAA\",\"dataAttivazione\":\"2014-09-30T22:00:00.000Z\",\"contrattoSAP\":\"0023031671\",\"codiceFiscale\":\"02109250692\",\"podPdr\":\"11750002170044\",\"stato\":\"Sospeso\",\"tipologiaCommodity\":\"Gas\",\"aliasAccount\":null,\"indirizzoFornitura\":\"CDA RIPARI SNC - 66026 ORTONA CH\",\"alias\":null,\"bpAccountSAP\":\"0034137207\",\"contractAccountSAP\":\"001019188081\",\"citta\":\"ORTONA\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}"); 
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens - Need VPN ENEL
     * Fornitura ACB - OK - WEB - BSN Stato Fornitura ATTIVA 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIFornitureACB.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
