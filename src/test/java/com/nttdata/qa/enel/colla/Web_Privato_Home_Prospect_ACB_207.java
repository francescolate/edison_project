package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Home_Prospect_ACB_207;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Home_Prospect_ACB_207 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "c.ollaudoloyaltycolazzo20.20@gmail.com"); 
		//c.ollaudoloyaltycolazzo20.20@gmail.com
		prop.setProperty("WP_PASSWORD", "Password01"); 
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.setProperty("FORNITURE_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale");
		prop.setProperty("ENELPREMIA_URL", "https://www-coll1.enel.it/it/luce-e-gas/enelpremia");
		prop.setProperty("NOVITA_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale/novita");
		prop.setProperty("CHIAMACI_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale/chiamaci");
		prop.setProperty("ACCOUNT_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale/account");
		prop.setProperty("ITUOI_URL", "https://www-coll1.enel.it/it/area-clienti/residenziale/i-tuoi-diritti");
		prop.setProperty("NOVITA_SECTION1", "https://www.enelxstore.com/it/it");
		prop.setProperty("NOVITA_SECTION2", "https://www-coll1.enel.it/it/area-clienti/residenziale/novita#");
		prop.setProperty("NOVITA_SECTION3", "https://www-coll1.enel.it/it/supporto/faq/app-enel");
		prop.setProperty("NOVITA_SECTION4", "https://www.ufirst.com/country-selection");
		prop.setProperty("SUPPORTO_URL", "https://www-coll1.enel.it/it/supporto"); 
		prop.setProperty("SUPPORTO_FAQ", "https://www-coll1.enel.it/it/supporto/faq/faq-enel-energia?zoneid=supporto-hero");
		prop.setProperty("TRAVOSPAZIO_URL", "https://www-coll1.enel.it/spazio-enel"); 
		

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Home_Prospect_ACB_207.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
