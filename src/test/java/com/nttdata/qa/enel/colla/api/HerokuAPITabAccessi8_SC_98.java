/*HerokuAPITabAccessi8_SC_98 
** API call for Tokens
* TabAccessi8 - OK Not exit UserUPN
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 00086480118 r.32019p109statoco.ntiweb@gmail.com 30a1fe52-f3e7-4ca5-a592-c30a26890c2c 0418d8e5-b56c-455c-812a-89ef40baf1f3 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPITabAccessi8;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPITabAccessi8_SC_98 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","00086480118");
		prop.setProperty("USERNAME","r.32019p109statoco.ntiweb@gmail.com");
		prop.setProperty("USERUPN","30a1fe52-f3e7-4ca5-a592");
		prop.setProperty("ENELID","0418d8e5-b56c-455c-812a-89ef40baf1f3");
		prop.setProperty("EXPIRATION","2021-12-31");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmZWRlNDU3NC02NzBhLTQ2NjgtYTBhOC0xOTkwZjAwMzI3NjgiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IkxORVJGTDM4QjA4SDY0M0UiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiI2Yjc4OWE2Ni1lYzBjLTRmMDUtYjUxZC0wZDg5Yjk4ZjVmNGYiLCJleHAiOjE2MDc3MDc3NTIsInVzZXJpZCI6InIzMi4wMTlwMTA5c3RhdG9jb250aXdlYkBnbWFpbC5jb20iLCJqdGkiOiI3YWJkYWE1Yy0yNzk5LTQ0YTgtOThkNC1iM2I4NzY1OTNmYjMiLCJzaWQiOiI5YjQ1ZWI0Ni1mMDFkLTQzNTEtODhiYS1kOGQzYjU4MzUwYjkifQ.G43VNmd6j8b4pKHuRIv69DvVdNxrdxDN8zdhi1Wosj8");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-webws1-stage/v1/accessi/tab-accessi8?userUPN=" + prop.getProperty("USERUPN"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-webws1-stage/v1/accessi/tab-accessi8?userUPN=");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"piva\":null,\"tipoCliente\":null,\"sessionData\":null,\"cf\":null,\"isAutenticato\":0,\"tipoServizio\":null,\"sessionId\":null,\"effEnergetica\":\"N\"},\"status\":{\"descrizione\":null,\"esito\":0,\"codice\":\"WS1-00\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * TabAccessi8 - OK Userupn Valido BSN - WEB 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPITabAccessi8.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
