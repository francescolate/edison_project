package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.Privato_208_ACR_Menù_Laterale;
import com.nttdata.qa.enel.testqantt.colla.VerifyLoginEnelArea;
import com.nttdata.qa.enel.util.ReportUtility;
public class WEB_Privato_Homepage_Menu_Laterale_Sinsitro__208 {
	
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	final String BASE_LINK = "https://www-coll1.enel.it/";
	
	@Before
	public void startTest() throws Exception{
		
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.32019p109statocont.iweb@gmail.com");	//r.32019p109statocont.iweb@gmail.com"	
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("SUPPORTO_URL", "https://www-coll1.enel.it/it/supporto");
		prop.setProperty("TRAVOSPAZIO_URL", "https://www-coll1.enel.it/spazio-enel");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");

		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
	};
	
	@Test
    public void runTest() throws Exception{
		String args[]= {nomeScenario};
		VerifyLoginEnelArea.main(args);
		Privato_208_ACR_Menù_Laterale.main(args);
		
		
	};
	
	@After
    public void endTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
