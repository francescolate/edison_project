package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.FooterPageEng;
import com.nttdata.qa.enel.testqantt.colla.HeaderLoginEng;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.PublicAreaEngBodyChecker;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Motorino_di_ricerca_83 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
	    prop.setProperty("LINK", "https://www-coll1.enel.it/en/luce-e-gas/luce-e-gas/casa?nec=pa&sortType=in-promozione");
	    
	    prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		PublicAreaEngBodyChecker.main(args);
		HeaderLoginEng.main(args);	
		FooterPageEng.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
