/* HerokuAPICoperturaIndirizzo_SC_255 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - CF/PIVA NON PRESENTE IN CRMT
java -jar C:\Users\manzofa\Desktop\token\WebTokenJWT.jar FRTFRC85B18G388N annamaria.cecili@enelsaa3010.enel.com EE0000018072@BOX.ENEL.COM e884e754-3add-4cdb-9ad4-73fa005eb55e 2020-12-31

"userUPN": "EE0000018072@BOX.ENEL.COM",
"cf": "FRTFRC85B18G388N",
"enelId": "e884e754-3add-4cdb-9ad4-73fa005eb55e",
"userEmail": "annamaria.cecili@enelsaa3010.enel.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIOfferteCRMT_SC_255 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","FRTFRC85B18G388T");
		prop.setProperty("USERNAME","annamaria.cecili@enelsaa3010.enel.com");
		prop.setProperty("USERUPN","EE0000018072@BOX.ENEL.COM");
		prop.setProperty("ENELID","e884e754-3add-4cdb-9ad4-73fa005eb55e");
		prop.setProperty("EXPIRATION","2021-12-31");

//	    prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiRUUwMDAwMDE4MDcyQEJPWC5FTkVMLkNPTSIsImNvZGljZWZpc2NhbGUiOiJGUlRGUkM4NUIxOEczODhOIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMDg5MjUyLCJlbmVsaWQiOiJlODg0ZTc1NC0zYWRkLTRjZGItOWFkNC03M2ZhMDA1ZWI1NWUiLCJ1c2VyaWQiOiJhbm5hbWFyaWEuY2VjaWxpQGVuZWxzYWEzMDEwLmVuZWwuY29tIiwianRpIjoiMzY0Yzc4N2MtMGMxYy00ZWE2LWE3ZWItYjE4ZTcyOTUxMDdhIn0.WadPH72KbDDL-3X8TtCRbGVawm65_eRSMM5nyfURxWc");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
       prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"offerte\":[],\"totaleStd\":0},\"status\":{\"descrizione\":\"Not found\",\"esito\":\"KO\",\"codice\":\"001\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - CF/PIVA NON PRESENTE IN CRMT
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
