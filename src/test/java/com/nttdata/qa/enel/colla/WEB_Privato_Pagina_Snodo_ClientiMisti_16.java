package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginHomeBusinessArea;
import com.nttdata.qa.enel.testqantt.colla.LoginPageOthers;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Pagina_Snodo_ClientiMisti_16 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("LINK", "https://www-coll1.enel.it/");
//		prop.setProperty("USERNAME", "co.llaudoloyalty.colazzo2020@gmail.com");
		prop.setProperty("USERNAME", "r32019p1.09statocontiweb@gmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.store(new FileOutputStream(nomeScenario), null);
		
	};
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginHomeBusinessArea.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
	       InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	};

}
