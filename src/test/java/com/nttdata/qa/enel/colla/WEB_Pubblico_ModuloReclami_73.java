package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.ID_73_ModuloReclami;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_ModuloReclami_73 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
		prop.setProperty("NOME", "ALBUS");
		prop.setProperty("COGNOME", "SILENTE");
		prop.setProperty("TELEFONOCELLULARE", "3245678987");
		prop.setProperty("CODICE_FISCALE", "SLNLBS68M28H501Y");
		prop.setProperty("INDIRIZZO_DELLA_FORNITURE", "VIA VAI 2");
		prop.setProperty("EMAIL", "fabiana11.manzo@nttdata.com");
		prop.setProperty("NUMERO_CLIENTE", "310521194");
		prop.setProperty("CODICE_POD", "IT002E8877878A");
		prop.setProperty("DESCRIZIONE", "PROVA TEST ID 73");
	   
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		ID_73_ModuloReclami.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}


