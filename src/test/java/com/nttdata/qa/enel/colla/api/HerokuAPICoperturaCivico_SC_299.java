/*HerokuAPICoperturaCivico_SC_299
** Chiamata API CoperturaCivico - Case OK - WEB - RES															
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaCivico;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPICoperturaCivico_SC_299 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("Authorization", "Token");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("CF_PIVA","ZMBGFR38C21A944X");
		prop.setProperty("USERNAME","francesco.lucia@nttdata.com");
		prop.setProperty("USERUPN","049fe692-392b-44a5-bbeb-d80b689db837");
		prop.setProperty("ENELID","c027675e-53d2-4943-a097-3ed45a92be01");
		prop.setProperty("EXPIRATION","2022-07-30");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/order-fibra/v1/coperturacivico?civico=32&idstrada=38000079839");
		prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"OK\",\"code\":\"0\",\"description\":\"SUCCESS\"}");
		prop.setProperty("JSON_DATA_OUTPUT_2", "{\"houseNumbers\":[{\"id\":\"380100009787855\",\"value\":\"32\"}]}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
	 * API call for Tokens
     * Chiamata API CoperturaCivico - Case OK - WEB - RES																				
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaCivico.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
