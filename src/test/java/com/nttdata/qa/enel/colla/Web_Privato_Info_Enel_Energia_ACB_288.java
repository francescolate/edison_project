package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACB_288;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Info_Enel_Energia_ACB_288 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/"); 
		prop.setProperty("WP_USERNAME", "collaudoloyaltyco.lazzo.2020@gmail.com");
		prop.setProperty("WP_PASSWORD", "pASSWORD01");
		//prop.setProperty("WP_USERNAME", "coll.audoloyaltycolazzo2.020@gmail.com");
		//prop.setProperty("WP_USERNAME", "immilezell-1212@yopmail.com");
		//prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.setProperty("EMAIL", "t.estingcrma.u.t.omation@gmail.com");
		prop.setProperty("CONFERMA_EMAIL", "t.estingcrma.u.t.omation@gmail.com");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Info_Enel_Energia_ACB_288.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}