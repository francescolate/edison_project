package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.Pubblico_ID_83_Motorino_Ricerca_ENG;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Pubblico_Motorinodiricerca_83 {

	Properties prop;
	
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void startTest() throws Exception{
			
			this.prop=new Properties();
			prop.setProperty("LINK", "https://www-coll1.enel.it/en/luce-e-gas/luce-e-gas/casa?nec=pa&sortType=in-promozione");
			prop.setProperty("HOMEPAGE", "https://www-coll2.enel.it/");
			prop.setProperty("RUN_LOCALLY","Y");
			
			prop.store(new FileOutputStream(nomeScenario), null);
		};
		
		@Test
	    public void runTest() throws Exception{
			String args[]= {nomeScenario};
			Pubblico_ID_83_Motorino_Ricerca_ENG.main(args);
			
		};
		
		@After
	    public void endTest() throws Exception{
			
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
		};

	}
