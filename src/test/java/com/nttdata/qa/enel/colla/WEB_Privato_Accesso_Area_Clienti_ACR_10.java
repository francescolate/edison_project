package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.Home_page_Resedential_ACR_10;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_Accesso_Area_Clienti_ACR_10 {

		private RemoteWebDriver driver;  
		Logger LOGGER = Logger.getLogger("");
		Properties prop;
		final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			
		    prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		    prop.setProperty("WP_USERNAME", "raf.faellaq.uomo@gmail.com");
		    prop.setProperty("WP_PASSWORD", "Password01");
		    prop.setProperty("ACCOUNT_TYPE", "");
			prop.setProperty("RUN_LOCALLY","Y");
			
			prop.store(new FileOutputStream(nomeScenario), null);
	    };
		@Test
	    public void eseguiTest() throws Exception{
			String args[]= {nomeScenario};
			Login_Private_Area.main(args);
			Home_page_Resedential_ACR_10.main(args);
			
					
		};
		@After
	    public void fineTest() throws Exception{
			InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
		};

	}	

