package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.colla.BSN_Customer_Supply_48_PAVAN;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_RINOMINA_FORNITURA_ACB_48_PAVAN {
	
	private RemoteWebDriver driver;
	Logger logger = Logger.getLogger("");
	Properties  prop;
	final String nomeScenario = "WEB_Privato_RINOMINA_FORNITURA_ACB_48_PAVAN";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		
		prop.setProperty("LINK","https://www-coll1.enel.it/");
		prop.setProperty("USERNAME", "immilezell-1212@yopmail.com");
		prop.setProperty("PASSWORD", "Password01");
		prop.setProperty("CUST_NUM", "636727539");
		
		prop.setProperty("RUN LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}
	
	@Test
	public void eseguiTest() throws Exception
	{
		String args[] = {nomeScenario};
		
		BSN_Customer_Supply_48_PAVAN.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		 	InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	}
	

}
