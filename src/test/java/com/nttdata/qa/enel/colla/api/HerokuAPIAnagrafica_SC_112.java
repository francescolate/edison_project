/*HerokuAPIRicercaForniture_SC_112 
** API call for Tokens
* Case OK - WEB - API Anagrafica - BSN
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar 01533540447 r.32019p109statoc.ontiweb@gmail.com e66bc31c-44a5-4ed6-a76b-39a5808567da 998777cd-ccbc-45e8-b013-2578bc623fff 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIAnagrafica_SC_112 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","01533540447");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUMERO_MESI","0");
		prop.setProperty("USERNAME","r.32019p109statoc.ontiweb@gmail.com");
		prop.setProperty("USERUPN","e66bc31c-44a5-4ed6-a76b-39a5808567da");
		prop.setProperty("ENELID","998777cd-ccbc-45e8-b013-2578bc623fff");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		// prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiZTY2YmMzMWMtNDRhNS00ZWQ2LWE3NmItMzlhNTgwODU2N2RhIiwiY29kaWNlZmlzY2FsZSI6IjAxNTMzNTQwNDQ3IiwiaXNzIjoiYXBwIiwiZXhwIjoxNjEyMDg5ODk1LCJlbmVsaWQiOiI5OTg3NzdjZC1jY2JjLTQ1ZTgtYjAxMy0yNTc4YmM2MjNmZmYiLCJ1c2VyaWQiOiJyLjMyMDE5cDEwOXN0YXRvYy5vbnRpd2ViQGdtYWlsLmNvbSIsImp0aSI6IjRlMTc2OTJmLTc5ZjctNGRmMy1hMTU5LTgzNzYzZDFhYTMwMSJ9.-RtqOBIpfQ5cK2XYBxKiKf5zfXc2xzMFF-jssgdghm0");
		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/anagrafica?canale=" + prop.getProperty("CANALE") + "&cf=" + prop.getProperty("CF_PIVA") + "&numMesiMaxCess="+ prop.getProperty("NUMERO_MESI"));
		//prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"numeroAssetELE\":1,\"tipoAccount\":\"NON RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"Imp. Ind.\",\"flagDipendenteEnel\":false,\"totaleUsoNonAbitativo\":1,\"codiceFormaGiuridica\":\"7\",\"numeroPartiteIVA\":1,\"numeroAssetGAS\":0,\"indicatoriSufornitureInCorsoAttivazione\":0,\"account\":[{\"idAccount\":\"0010Y00000OiiyAQAR\",\"cognome\":\"ASSIMETA SAS\",\"tipoAccount\":\"NON RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"s.a.s.\",\"sedeLegale\":\"VIA COLA DI RIENZO COLA DI RIENZO 11     63074 SAN BENEDETTO DEL TRONTO AP <regione vuota>\",\"cellulareSenzaPrefisso\":null,\"indirizzo\":\"COLA DI RIENZO\",\"accountSocialTwitter\":null,\"tipologiaCliente\":\"Azienda\",\"provincia\":\"ASCOLI PICENO\",\"codiceFiscale\":\"01533540447\",\"descrizioneCellulare\":null,\"bpsap\":\"0036920807\",\"pecCertificata\":false,\"cap\":\"63074\",\"canaleContattoPreferito\":null,\"cognomeNome\":\"ASSIMETA SAS\",\"contact\":{\"dataCreazione\":\"2017-05-04T16:09:40.000Z\",\"cognome\":\"MASSACCI\",\"cellulareSenzaPrefisso\":null,\"accountSocialTwitter\":null,\"codiceFiscale\":\"MSSBRN64T43G005E\",\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":\"TELEFONO\",\"altroTelefonoSenzaPrefisso\":null,\"telefono\":null,\"fax\":null,\"accountSocialFB\":null,\"altroTelefono\":\"0735593877\",\"indirizzoDomicilio\":null,\"indirizzoEmail\":null,\"indirizzoResidenza\":null,\"descrizioneAltroTelefono\":null,\"cellulare\":null,\"cellulareCertificato\":false,\"nome\":\"BRUNA\",\"descrizioneTelefono\":null,\"emailCertificata\":false,\"prefissoAltroTelefono\":null,\"prefissoCellulare\":null,\"idContact\":\"0030Y00000L33nwQAB\",\"descrizioneEmail\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null},\"ragioneSociale\":\"ASSIMETA SAS\",\"telefono\":\"0735593867\",\"fax\":null,\"accountSocialFB\":null,\"citta\":\"SAN BENEDETTO DEL TRONTO\",\"altroTelefono\":null,\"idMasterAccountSiebel\":\"2-OIZCDKW\",\"indirizzoEmail\":null,\"descrizioneAltroTelefono\":\"FISSO\",\"toponomasticaViaCivico\":\"VIA COLA DI RIENZO 11\",\"cellulare\":\"3473891917\",\"cellulareCertificato\":false,\"flagDipendenteEnel\":false,\"codiceFormaGiuridica\":\"4\",\"nome\":null,\"descrizioneTelefono\":\"FISSO\",\"civico\":\"11\",\"telefonoValido\":false,\"emailCertificata\":false,\"chiaveAlReferenteTitolare\":null,\"prefissoCellulare\":null,\"partitaIVA\":\"01533540447\",\"statoCliente\":\"Cliente\",\"descrizioneEmail\":null,\"toponomastica\":\"VIA\",\"mercatoRiferimentoCliente\":\"Maggior Tutela\",\"indirizzoEmailPEC\":\"ergosbt@pec.it\",\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null,\"flagPersonaFisicaGiuridica\":\"N\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}");
		//prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"numeroAssetELE\":1,\"tipoAccount\":\"NON RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"Imp. Ind.\",\"flagDipendenteEnel\":false,\"totaleUsoNonAbitativo\":1,\"codiceFormaGiuridica\":\"7\",\"numeroPartiteIVA\":1,\"numeroAssetGAS\":0,\"indicatoriSufornitureInCorsoAttivazione\":10,\"account\":[{\"idAccount\":\"0010Y00000OiiyAQAR\",\"cognome\":\"ASSIMETA SAS\",\"tipoAccount\":\"NON RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"s.a.s.\",\"sedeLegale\":\"VIA MONTECERNO MONTECERNO 33 OSIMO    60027 OSIMO AP <regione vuota>\",\"cellulareSenzaPrefisso\":null,\"indirizzo\":\"MONTECERNO\",\"accountSocialTwitter\":null,\"tipologiaCliente\":\"Azienda\",\"provincia\":\"ANCONA\",\"codiceFiscale\":\"01533540447\",\"descrizioneCellulare\":\"Company\",\"bpsap\":\"0036920807\",\"pecCertificata\":false,\"cap\":\"60027\",\"canaleContattoPreferito\":null,\"cognomeNome\":\"ASSIMETA SAS\",\"contact\":{\"dataCreazione\":\"2017-05-04T16:09:40.000Z\",\"cognome\":\"MASSACCI\",\"cellulareSenzaPrefisso\":null,\"accountSocialTwitter\":null,\"codiceFiscale\":\"MSSBRN64T43G005E\",\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":\"TELEFONO\",\"altroTelefonoSenzaPrefisso\":null,\"telefono\":null,\"fax\":null,\"accountSocialFB\":null,\"altroTelefono\":\"0735593877\",\"indirizzoDomicilio\":null,\"indirizzoEmail\":null,\"indirizzoResidenza\":null,\"descrizioneAltroTelefono\":null,\"cellulare\":\"0039\",\"cellulareCertificato\":false,\"nome\":\"BRUNA\",\"descrizioneTelefono\":null,\"emailCertificata\":false,\"prefissoAltroTelefono\":null,\"prefissoCellulare\":\"0039\",\"idContact\":\"0030Y00000L33nwQAB\",\"descrizioneEmail\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null},\"ragioneSociale\":\"ASSIMETA SAS\",\"telefono\":\"0039\",\"fax\":null,\"accountSocialFB\":null,\"citta\":\"OSIMO\",\"altroTelefono\":null,\"idMasterAccountSiebel\":\"2-OIZCDKW\",\"indirizzoEmail\":\"paolo.resinosi@nttdata.com\",\"descrizioneAltroTelefono\":\"Company\",\"toponomasticaViaCivico\":\"VIA MONTECERNO 33\",\"cellulare\":\"3473891917\",\"cellulareCertificato\":false,\"flagDipendenteEnel\":false,\"codiceFormaGiuridica\":\"4\",\"nome\":null,\"descrizioneTelefono\":\"Company\",\"civico\":\"33\",\"telefonoValido\":false,\"emailCertificata\":false,\"chiaveAlReferenteTitolare\":\"0030Y00000L33nwQAB\",\"prefissoCellulare\":\"0039\",\"partitaIVA\":\"01533540447\",\"statoCliente\":\"Cliente\",\"descrizioneEmail\":null,\"toponomastica\":\"VIA\",\"mercatoRiferimentoCliente\":\"Maggior Tutela\",\"indirizzoEmailPEC\":\"ergosbt@pec.it\",\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null,\"flagPersonaFisicaGiuridica\":\"N\"}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"numeroAssetELE\":1,\"tipoAccount\":\"NON RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"Imp. Ind.\",\"flagDipendenteEnel\":false,\"totaleUsoNonAbitativo\":1,\"codiceFormaGiuridica\":\"7\",\"numeroPartiteIVA\":1,\"numeroAssetGAS\":0,\"indicatoriSufornitureInCorsoAttivazione\":30,\"account\":[{\"idAccount\":\"0010Y00000OiiyAQAR\",\"tipoAccount\":\"NON RESIDENZIALE\",\"cellulareSenzaPrefisso\":null,\"indirizzo\":\"MONTECERNO\",\"descrizioneCellulare\":\"Company\",\"pecCertificata\":false,\"canaleContattoPreferito\":null,\"contact\":{\"dataCreazione\":\"2017-05-04T16:09:40.000Z\",\"cognome\":\"MASSACCI\",\"cellulareSenzaPrefisso\":null,\"accountSocialTwitter\":null,\"codiceFiscale\":\"MSSBRN64T43G005E\",\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":\"TELEFONO\",\"altroTelefonoSenzaPrefisso\":null,\"telefono\":null,\"fax\":null,\"accountSocialFB\":null,\"altroTelefono\":\"0735593877\",\"indirizzoDomicilio\":null,\"indirizzoEmail\":null,\"indirizzoResidenza\":null,\"descrizioneAltroTelefono\":null,\"cellulare\":\"0039\",\"cellulareCertificato\":false,\"nome\":\"BRUNA\",\"descrizioneTelefono\":null,\"emailCertificata\":false,\"prefissoAltroTelefono\":null,\"prefissoCellulare\":\"0039\",\"idContact\":\"0030Y00000L33nwQAB\",\"descrizioneEmail\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null},\"ragioneSociale\":\"ASSIMETA SAS\",\"telefono\":\"00393669047153\",\"fax\":null,\"citta\":\"OSIMO\",\"descrizioneAltroTelefono\":\"Company\",\"toponomasticaViaCivico\":\"VIA MONTECERNO 3\",\"flagDipendenteEnel\":false,\"accountType\":null,\"codiceFormaGiuridica\":\"4\",\"nome\":null,\"descrizioneTelefono\":\"Company\",\"civico\":\"3\",\"telefonoValido\":false,\"emailCertificata\":false,\"chiaveAlReferenteTitolare\":\"0030Y00000L33nwQAB\",\"prefissoCellulare\":\"0039\",\"statoCliente\":\"Cliente\",\"descrizioneEmail\":null,\"mercatoRiferimentoCliente\":\"Maggior Tutela\",\"flagPersonaFisicaGiuridica\":\"N\",\"cognome\":\"ASSIMETA SAS\",\"descrizioneFormaGiuridica\":\"s.a.s.\",\"sedeLegale\":\"VIA MONTECERNO MONTECERNO 3     60027 OSIMO AN MARCHE\",\"accountSocialTwitter\":null,\"tipologiaCliente\":\"Azienda\",\"provincia\":\"ANCONA\",\"codiceFiscale\":\"01533540447\",\"bpsap\":\"0036920807\",\"cap\":\"60027\",\"cognomeNome\":\"ASSIMETA SAS\",\"accountSocialFB\":null,\"altroTelefono\":null,\"idMasterAccountSiebel\":\"2-OIZCDKW\",\"indirizzoEmail\":\"fabiana.manzo@nttdata.com\",\"cellulare\":\"3473891917\",\"cellulareCertificato\":false,\"flagSintesi\":false,\"partitaIVA\":\"01533540447\",\"toponomastica\":\"VIA\",\"indirizzoEmailPEC\":\"ergosbt@pec.it\",\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Case OK - WEB - API Anagrafica - BSN
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
