/*HerokuAPIgGeneraToken_SC_161 
** OK - APP
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIUserByEnelID;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIgGeneraToken_SC_161 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
		@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("ACCOUNT_ID","0010Y00000FTxw7QAD");
		prop.setProperty("SOURCECHANNEL","APP");
		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/auth-fibra/v1/generaToken?idAccount=" + prop.getProperty("ACCOUNT_ID"));
        prop.setProperty("JSON_DATA_OUTPUT", "\"status\":{\"result\":\"OK\",\"code\":\"0\",\"description\":\"SUCCESS\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK RES - APP 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIUserByEnelID.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
