package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.CheckHamburgerRightMenu;
import com.nttdata.qa.enel.testqantt.colla.FrequentQuestions;
import com.nttdata.qa.enel.testqantt.colla.HeaderLogin;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.OpenHambugerAndCheckMenu;
import com.nttdata.qa.enel.util.ReportUtility;


public class WEB_Pubblico_Header_10 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		//prop.setProperty("BROWSER", "IE");
		
	    prop.setProperty("LINK", "https://www-coll1.enel.it/");
	 //   prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Italia S.p.a.");
	 //   prop.setProperty("AFTER_FOOTER_TEXT", "© Enel Energia S.p.a. - Gruppo Iva P.IVA 12345678901");
	   
	    prop.setProperty("BEFORE_FOOTER_TEXT", "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.");
	    prop.setProperty("AFTER_FOOTER_TEXT", "Gruppo IVA Enel P.IVA 15844561009");
	   
	    
	    prop.setProperty("DOMANDE_FREQUENTI_URL", prop.getProperty("LINK")+"it/supporto/faq-index");
	    prop.setProperty("CONTATTACI_URL", prop.getProperty("LINK")+"it/contattaci?zoneid=domande_frequenti-box_stripe");
	    prop.setProperty("LEGGERE_BOLLETTA_URL", prop.getProperty("LINK")+"it/supporto/faq/come-leggere-bolletta-luce");
	    prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		Login_Public_Area.main(args);
		HeaderLogin.main(args);	
		OpenHambugerAndCheckMenu.main(args);
		CheckHamburgerRightMenu.main(args);
		FrequentQuestions.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
	       //String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
