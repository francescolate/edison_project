/*HerokuAPIRegistration_SC_258B
** API call for Tokens
** API call Registration with BSN customer, Active Supply, Unique telephone number.
** EMAIL already used for another customer.*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRegistration_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuAPIRegistration_SC_258B {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account");
		prop.setProperty("FIRSTNAME","PROVA");
		prop.setProperty("LASTNAME","PROVA");
		prop.setProperty("CODICEFISCALE","GTAVCN85B17I483S");
		prop.setProperty("EMAIL","r.32019p109s.tatocontiweb@gmail.com");
        prop.setProperty("PHONENUMBER","+393312206210");
        prop.setProperty("DENOMINAZIONE","VILLA MAGIA");
		// ok -- prop.setProperty("JSON_INPUT", "{     \"email\": \"r.32019p109s.tatocontiweb@gmail.com\",     \"phoneNumber\": \"+393312206210\",     \"password\": \"Password01\",     \"firstName\": \"PROVA\",     \"lastName\": \"PROVA\",     \"countryCode\": \"IT\",     \"companyName\":\"VILLA MAGIA\",     \"vatCode\":\"93069180755\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"businessId\",                     \"value\": \"93069180755\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");
// MODULO        prop.setProperty("JSON_INPUT", "{     \"email\": \""+ prop.getProperty("EMAIL") + "\",     \"phoneNumber\": \""+ prop.getProperty("PHONENUMBER") +"\",     \"password\": \"Password01\",     \"firstName\": \""+ prop.getProperty("FIRSTNAME") +"\",     \"lastName\": \""+ prop.getProperty("LASTNAME") +"\",     \"countryCode\": \"IT\",     \"companyName\":\""+ prop.getProperty("DENOMINAZIONE") +"\",     \"vatCode\":\""+ prop.getProperty("CODICEFISCALE") +"\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"businessId\",                     \"value\": \""+ prop.getProperty("CODICEFISCALE") + "\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"email\":\"email must be unique, '"+prop.getProperty("EMAIL")+"' already exists\"}");		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
	 * API call for Tokens
     * API call Registration with BSN customer, Active Supply, Unique telephone number.
     * EMAIL already used for another customer.
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRegistration_2.main(args); // per gestire errore ricevuto
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
