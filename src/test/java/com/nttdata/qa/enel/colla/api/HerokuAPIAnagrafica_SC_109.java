/*HerokuAPIRicercaForniture_SC_109 
** API call for Tokens
* Case OK - WEB - API Anagrafica - RES with at least one supply in "IN LAVORAZIONE" status
* java -jar C:\Users\manzofa\Desktop\token\WEBTokenJWT.jar ZMBGFR38C21A944X francesco.lucia@nttdata.com 049fe692-392b-44a5-bbeb-d80b689db837 c027675e-53d2-4943-a097-3ed45a92be01 2020-12-31
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIRicercaForn_Anagrafica;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIAnagrafica_SC_109 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF_PIVA","ZMBGFR38C21A944X");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUMERO_MESI","0");
		prop.setProperty("USERNAME","francesco.lucia@nttdata.com");
		prop.setProperty("USERUPN","049fe692-392b-44a5-bbeb-d80b689db837");
		prop.setProperty("ENELID","c027675e-53d2-4943-a097-3ed45a92be01");
		prop.setProperty("EXPIRATION","2021-12-31");
		/*prop.setProperty("USERNAME","r32019p.109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","EE0000018152@BOX.ENEL.COM");
		prop.setProperty("ENELID","e6471459-d68f-4952-84f4-54931b8f6873");
		prop.setProperty("EXPIRATION","2021-12-31");*/
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiMDQ5ZmU2OTItMzkyYi00NGE1LWJiZWItZDgwYjY4OWRiODM3IiwiY29kaWNlZmlzY2FsZSI6IlpNQkdGUjM4QzIxQTk0NFgiLCJpc3MiOiJhcHAiLCJleHAiOjE2MTIwODc1MjksImVuZWxpZCI6ImMwMjc2NzVlLTUzZDItNDk0My1hMDk3LTNlZDQ1YTkyYmUwMSIsInVzZXJpZCI6ImZyYW5jZXNjby5sdWNpYUBudHRkYXRhLmNvbSIsImp0aSI6IjZkMmI0MGU1LWU0YmMtNGFjNy04Yjc0LTU3NTYxN2YwYWI2OCJ9.d0tYi_YywnBFRUN7y3xHNKIdUhc9313n-4e0dKClIMs");
        prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/anagrafica?canale=" + prop.getProperty("CANALE") + "&cf=" + prop.getProperty("CF_PIVA") + "&numMesiMaxCess="+ prop.getProperty("NUMERO_MESI"));
		prop.setProperty("JSON_DATA_OUTPUT", "{\"data\":{\"numeroAssetELE\":2,\"tipoAccount\":\"RESIDENZIALE\",\"descrizioneFormaGiuridica\":\"NON RICON.\",\"flagDipendenteEnel\":false,\"totaleUsoNonAbitativo\":0,\"codiceFormaGiuridica\":\"20\",\"numeroPartiteIVA\":0,\"numeroAssetGAS\":1,\"indicatoriSufornitureInCorsoAttivazione\":3,\"account\":[{\"idAccount\":\"0010Y00000G410FQAR\",\"tipoAccount\":\"RESIDENZIALE\",\"cellulareSenzaPrefisso\":null,\"indirizzo\":null,\"descrizioneCellulare\":null,\"pecCertificata\":false,\"canaleContattoPreferito\":null,\"contact\":{\"dataCreazione\":\"2017-02-11T10:35:19.000Z\",\"cognome\":\"ZAMBRINI\",\"cellulareSenzaPrefisso\":null,\"accountSocialTwitter\":null,\"codiceFiscale\":\"ZMBGFR38C21A944X\",\"descrizioneCellulare\":\"Corporate\",\"pecCertificata\":false,\"canaleContattoPreferito\":\"TELEFONO\",\"altroTelefonoSenzaPrefisso\":null,\"telefono\":\"035806650\",\"fax\":null,\"accountSocialFB\":null,\"altroTelefono\":\"3498425220\",\"indirizzoDomicilio\":\"VIA PAGANINI 2     24040 BOLTIERE BG <regione vuota>\",\"indirizzoEmail\":\"gfzambrin@gmail.com\",\"indirizzoResidenza\":null,\"descrizioneAltroTelefono\":\"Other\",\"cellulare\":\"3401431666\",\"cellulareCertificato\":false,\"nome\":\"GIANFRANCO\",\"descrizioneTelefono\":\"F\",\"emailCertificata\":false,\"prefissoAltroTelefono\":null,\"prefissoCellulare\":null,\"idContact\":\"0030Y00000CW7ypQAD\",\"descrizioneEmail\":\"Company\",\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null},\"ragioneSociale\":null,\"telefono\":null,\"fax\":null,\"citta\":null,\"descrizioneAltroTelefono\":null,\"toponomasticaViaCivico\":null,\"flagDipendenteEnel\":false,\"accountType\":null,\"codiceFormaGiuridica\":null,\"nome\":\"GIANFRANCO\",\"descrizioneTelefono\":null,\"civico\":null,\"telefonoValido\":false,\"emailCertificata\":false,\"chiaveAlReferenteTitolare\":\"0030Y00000CW7ypQAD\",\"prefissoCellulare\":null,\"statoCliente\":\"Cliente\",\"descrizioneEmail\":null,\"mercatoRiferimentoCliente\":\"Maggior Tutela\",\"flagPersonaFisicaGiuridica\":\"N\",\"cognome\":\"ZAMBRINI\",\"descrizioneFormaGiuridica\":null,\"sedeLegale\":null,\"accountSocialTwitter\":null,\"tipologiaCliente\":\"Casa\",\"provincia\":null,\"codiceFiscale\":\"ZMBGFR38C21A944X\",\"bpsap\":\"0040444393\",\"cap\":null,\"cognomeNome\":\"ZAMBRINI GIANFRANCO\",\"accountSocialFB\":null,\"altroTelefono\":null,\"idMasterAccountSiebel\":\"1-6SGW6R_MA\",\"indirizzoEmail\":null,\"cellulare\":null,\"cellulareCertificato\":false,\"flagSintesi\":false,\"partitaIVA\":null,\"toponomastica\":null,\"indirizzoEmailPEC\":null,\"accountSocialLinkedin\":null,\"descrizioneEmailPEC\":null}]},\"status\":{\"descrizione\":\"Success\",\"esito\":\"OK\",\"codice\":\"000\"}}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * Case OK - WEB - API Anagrafica - RES with at least one supply in "IN LAVORAZIONE" status
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIRicercaForn_Anagrafica.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
