package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.DATI_DI_REGISTRAZIONE_ACR_56;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_Privato_DATI_DI_REGISTRAZIONE_ACR_56 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it");
		prop.setProperty("WP_USERNAME", "r.affaellaquom.o@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("AREA_CLIENTI", "");
	    prop.setProperty("ACCOUNT_TYPE", "");
	    prop.setProperty("NOME", "TIZIANA");
		prop.setProperty("COGNOME", "DI MATTEO");
		prop.setProperty("CELLULARE", "+39064745721");
		prop.setProperty("CODICE_FISCALE", "DMTTZN69H67C765I");
//		prop.setProperty("FORNITURA", "Luce - VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT Gas - VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT");

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		DATI_DI_REGISTRAZIONE_ACR_56.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
