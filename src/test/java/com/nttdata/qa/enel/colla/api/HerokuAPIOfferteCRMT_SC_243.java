/* HerokuAPICoperturaIndirizzo_SC_243 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - WEB - RES - Attivazione Fibra - Espletato
* "userUPN": "fede4574-670a-4668-a0a8-1990f0032768",
"cf": "LNERFL38B08H643E",
"enelId": "6b789a66-ec0c-4f05-b51d-0d89b98f5f4f",
"userEmail": "r32.019p109statocontiweb@gmail.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_243 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","W");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","LNERFL38B08H643E");
		prop.setProperty("USERNAME","r32.019p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","fede4574-670a-4668-a0a8-1990f0032768");
		prop.setProperty("ENELID","6b789a66-ec0c-4f05-b51d-0d89b98f5f4f");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//
//		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkY2NkN2YxYS02ZmI5LTQ2MmQtYTJlNi03M2M1NmJkMzNlZmUiLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlBBWE5EUjkzSDIzRjIwNUkiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJmNzAwODE1Zi0xYWMwLTQ0MzktOTlhYy1iNDdlN2E1M2ZlNGYiLCJleHAiOjE2MDc2OTI5MDEsInVzZXJpZCI6InIuMzIwMTlwMTA5c3RhdC5vY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZmRkZmY1NjUtNzVlYS00MzEyLTg5NjMtZTY5NzM0ZTAyMmIzIiwic2lkIjoiNjJhNjJkYWYtZWMxNS00MTdhLTlmNjEtZTVkNGFjYTFmZTdjIn0.tjo0jJ5Tvijg-9Wwnk98VgM4xm0RoB8bVsn9F1SMHJM");
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
//		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":null,\"tipoOfferta\":null,\"podPdr\":\"IT001E89005953\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3030996\",\"flagStd\":0,\"namingInFattura\":null,\"statoR2d\":null,\"numeroUtente\":\"706830652\",\"processoInCorso\":\"Attivazione Fibra\",\"idCase\":\"5001l000002vj2yAAA\",\"idListino\":null,\"dataAttivazione\":\"2016-06-30T22:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000Hd5LEAS\",\"dataRichiesta\":\"2019-10-08T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AxqEEAS\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA MORO ALDO 54 - 70018 RUTIGLIANO BA\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione Fibra\",\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":null,\"tipoOfferta\":null,\"podPdr\":\"IT001E89005953\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SZ3030996\",\"flagStd\":0,\"namingInFattura\":null,\"statoR2d\":null,\"numeroUtente\":\"706830652\",\"processoInCorso\":\"Attivazione Fibra\",\"idCase\":\"5001l000002vj2yAAA\",\"tipologiaProdotto\":\"RES\",\"idListino\":null,\"dataAttivazione\":\"2016-06-30T22:00:00.000Z\",\"idCaseItem\":\"a1Q1l000000Hd5LEAS\",\"dataRichiesta\":\"2019-10-08T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AxqEEAS\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA MORO ALDO 54 - 70018 RUTIGLIANO BA\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione Fibra\",\"processoCombinato\":false}");
       

		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - WEB - RES - Attivazione Fibra - Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
