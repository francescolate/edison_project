/* HerokuAPICoperturaIndirizzo_SC_208 
 * Need Token  - NEED VPN ENEL
** Case OK - Offers CRMT - APP - RES - SWITCH ATTIVO ELE - Espletato
* java -jar C:\Users\manzofa\Desktop\token\MobileTokenJWT.jar ZMBGFR38C21A944X francesco.lucia@nttdata.com 049fe692-392b-44a5-bbeb-d80b689db837 c027675e-53d2-4943-a097-3ed45a92be01 2020-12-31
"userUPN": "049fe692-392b-44a5-bbeb-d80b689db837",
"cf": "ZMBGFR38C21A944X",
"enelId": "c027675e-53d2-4943-a097-3ed45a92be01",
"userEmail": "francesco.lucia@nttdata.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_208 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","ZMBGFR38C21A944X");
		prop.setProperty("USERNAME","francesco.lucia@nttdata.com");
		prop.setProperty("USERUPN","049fe692-392b-44a5-bbeb-d80b689db837");
		prop.setProperty("ENELID","c027675e-53d2-4943-a097-3ed45a92be01");
		prop.setProperty("EXPIRATION","2021-12-31");


		//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwNDlmZTY5Mi0zOTJiLTQ0YTUtYmJlYi1kODBiNjg5ZGI4MzciLCJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwiY29kaWNlZmlzY2FsZSI6IlpNQkdGUjM4QzIxQTk0NFgiLCJpc3MiOiJhcHAiLCJlbmVsaWQiOiJjMDI3Njc1ZS01M2QyLTQ5NDMtYTA5Ny0zZWQ0NWE5MmJlMDEiLCJleHAiOjE2MDc2OTE1OTcsInVzZXJpZCI6ImZyYW5jZXNjby5sdWNpYUBudHRkYXRhLmNvbSIsImp0aSI6ImUwN2Y0YzU5LWZiMWQtNDc3YS04OGRhLThhYTc4ZDUyM2Y3MCIsInNpZCI6IjAwMGQ3NTY3LTVmMjMtNDgyZC04MTllLTUxNjczNWUyYWRlYyJ9.wFN-dHREgRIoffPyK8I9ykWp9fzjEkICuwvlTx5ZXug");
//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
        //prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E33254238\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"TS7618121\",\"flagStd\":0,\"namingInFattura\":\"BIORARIA\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"945295572\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5000Y0000042oGdQAI\",\"idListino\":\"a1l24000001Kuo0AAC\",\"dataAttivazione\":null,\"idCaseItem\":\"a1Q0Y000006LtmOUAS\",\"dataRichiesta\":\"2011-05-11T00:00:00.000Z\",\"idProdotto\":\"a1Y2400000BFKZREA5\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA DEI GERANEI 25 - 30020 SAN MICHELE AL TAGLIAMENTO VE\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":null,\"processoCombinato\":false}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":null,\"numeroOre\":null,\"statoCaseItem\":\"Espletato\",\"statoUdb\":\"Non Previsto\",\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E33254238\",\"statoSap\":\"OK\",\"statoSempre\":\"Ok\",\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"TS7618121\",\"flagStd\":0,\"namingInFattura\":\"BIORARIA\",\"statoR2d\":\"Ok\",\"numeroUtente\":\"945295572\",\"processoInCorso\":\"SWITCH ATTIVO\",\"idCase\":\"5000Y0000042oGdQAI\"");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * Case OK - Offers CRMT - APP - RES - SWITCH ATTIVO ELE - Espletato
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
