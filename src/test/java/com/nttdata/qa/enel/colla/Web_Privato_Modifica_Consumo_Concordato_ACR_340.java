package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Modifica_Consumo_Concordato_ACR_340;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Modifica_Consumo_Concordato_ACR_340 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//No supply in the given login
		//prop.setProperty("WP_USERNAME", "web.testing.aut.omation@gmail.com"); 
		//prop.setProperty("WP_PASSWORD", "Password01"); 
		//other
		//prop.setProperty("WP_USERNAME", "t.estingcrma.u.t.omation@gmail.com"); 
		
		// Functional team credentials
		prop.setProperty("WP_USERNAME", "web.testing.a.u.tomation@gmail.com");
		prop.setProperty("WP_PASSWORD", "Enel$001");
		prop.setProperty("VALUE", "15");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Modifica_Consumo_Concordato_ACR_340.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
