package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_1;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIconsistenzaCliente_2;
import com.nttdata.qa.enel.util.ReportUtility;

public class HerokuConsistenzaCliente_SC_8C {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("API_URL", "https://msa-stage.enel.it/msa/app/feintegration/v1/consistenzaCliente");
		prop.setProperty("JSON_INPUT", "{  \"data\": {   \"codiceFiscale\": \"\",   \"piva\": \"01533540447\",   \"podPdr\": \"\",   \"numeroutente\": \"\",   \"uniqueid\": \"\"  } }");
		//prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"654995134\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA CALATAFIMI 86 - 63074 SAN BENEDETTO DEL TRONTO AP\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"N\",\"podPdr\":\"IT001E57842112\",\"usoFornitura\":\"Uso Diverso da Abitazione\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2017-05-31T22:00:00.000Z\",\"dataAttivazioneProdotto\":\"2019-06-01\",\"prodotto\":\"Senza Orari Luce\"}]}");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"cliente\":\"Y\",\"forniture\":[{\"numeroUtente\":\"654995134\",\"tipologiaFornitura\":\"Elettrico\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA CALATAFIMI 86 - 63074 SAN BENEDETTO DEL TRONTO AP\",\"statoServizio\":\"Attivo\",\"flagResidente\":\"N\",\"podPdr\":\"IT001E57842112\",\"usoFornitura\":\"Uso Diverso da Abitazione\",\"metodoDiPagamento\":\"RID\",\"dataAttivazioneFornitura\":\"2017-05-31T22:00:00.000Z\",\"dataAttivazioneProdotto\":\"2019-06-01\",\"prodotto\":\"Senza Orari Luce\"},{\"numeroUtente\":\"310606562\",\"tipologiaFornitura\":\"GAS\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 11 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"11690000001114\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Enel Energia PLACET Fissa Gas Business\"},{\"numeroUtente\":\"310606561\",\"tipologiaFornitura\":\"ELETTRICO\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA ABBADIA 1 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"IT004E17173087\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Soluzione Energia Impresa -20%\"},{\"numeroUtente\":\"310606460\",\"tipologiaFornitura\":\"GAS\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 11 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"11690000002018\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Enel Energia PLACET Fissa Gas Business\"},{\"numeroUtente\":\"310605236\",\"tipologiaFornitura\":\"ELETTRICO\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 4 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"IT004E22711819\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Open_Energy Digital\"},{\"numeroUtente\":\"310604410\",\"tipologiaFornitura\":\"ELETTRICO\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA ABBADIA 4 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"IT004E49439299\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Open Energy Digital\"},{\"numeroUtente\":\"310604409\",\"tipologiaFornitura\":\"GAS\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 5 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"11690000006668\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Enel Energia PLACET Fissa Gas Business\"},{\"numeroUtente\":\"310604202\",\"tipologiaFornitura\":\"GAS\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 33 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"11690000009876\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Enel Energia PLACET Fissa Gas Business\"},{\"numeroUtente\":\"310603735\",\"tipologiaFornitura\":\"ELETTRICO\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 4 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"IT004E39284719\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Open Energy Digital\"},{\"numeroUtente\":\"310602837\",\"tipologiaFornitura\":\"ELETTRICO\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 33 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"IT004E33990010\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Open Energy\"},{\"numeroUtente\":\"310602832\",\"tipologiaFornitura\":\"GAS\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 33 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"11690000002097\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Enel Energia PLACET Variabile Gas Business\"},{\"numeroUtente\":\"310602830\",\"tipologiaFornitura\":\"GAS\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 33 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"11690000004237\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Enel Energia PLACET Variabile Gas Business\"},{\"numeroUtente\":\"310602825\",\"tipologiaFornitura\":\"ELETTRICO\",\"evidenzeCredito\":null,\"pianoTariffario\":null,\"indirizzoDiFornitura\":\"VIA MONTECERNO 33 - 60027 OSIMO AN\",\"statoServizio\":\"In attivazione\",\"flagResidente\":null,\"podPdr\":\"IT004E21212323\",\"usoFornitura\":null,\"metodoDiPagamento\":null,\"dataAttivazioneFornitura\":null,\"dataAttivazioneProdotto\":null,\"prodotto\":\"Open Energy\"}]}");
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con il seguente TAG in Input PIVA
	 * cliente = Y per BSN con fornitura ATTIVA
     * Verificare vi sia risposta corretta e non vi sia errore
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIconsistenzaCliente_2.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
