package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Login_Private_Area;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACR_290;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Info_Enel_Energia_ACR_290 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//No supply in the given login
		//Other login
		
		//silvia.test77@gmail.com
		prop.setProperty("WP_USERNAME", "silvia.test77@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaudoloya.ltycolazzo20.2.0@gmail.com");
		//prop.setProperty("WP_USERNAME", "collaud.oloyaltycolazzo2.020@gmail.com"); //BSN
		//prop.setProperty("WP_USERNAME", "t.estingcrma.u.t.omation@gmail.com"); 
		//prop.setProperty("WP_USERNAME", "testi.ngcrma.u.t.o.m.a.t.i.on@gmail.com"); //MTTFNC33L21F890Y
		prop.setProperty("WP_PASSWORD", "Password01"); 
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		//LoginEnel.main(args);
		Login_Private_Area.main(args);
		Privato_Info_Enel_Energia_ACR_290.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
