package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.VerifyBillingAddress;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_172_Mod_ind_Fatturazione_ACR;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_Modifica_Indirizzo_di_Fattuazione_ACR_172 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r32019p109statocontiwe.b@gmail.com");//olettyzyrra-2468@yopmail.com
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("CONSUMPTIONNUMBER_COLUMNHEADING", "ITA_IFM_ConsumptionNumber__c");
		prop.setProperty("BILLINGADDRESS_COLUMNHEADING", "ITA_IFM_BillingAddress__c");
		prop.setProperty("WB_LINK","https://workbench.developerforce.com/query.php");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SERVIZI_HOMEPAGE", "Servizi per le forniture");
		prop.setProperty("SERVIZI_HOMEPAGE_SUBTEXT", "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture");
		prop.setProperty("MODIFICA_INDIRIZZO_FATTURAZIONE","Modifica Indirizzo Fatturazione");
		prop.setProperty("MODIFICA_INDIRIZZO_FATTURAZIONE_TITLE", "Modifica Indirizzo di Fatturazione");
		prop.setProperty("STEP1","Inserimento dati");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_172_Mod_ind_Fatturazione_ACR.main(args);
		VerifyBillingAddress.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}