package com.nttdata.qa.enel.colla;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.POE2_Component;
import com.nttdata.qa.enel.util.WebDriverManager;

public class TEST_BASIC_AUTH {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/it/search?search=Modifica+Potenza");
		prop.setProperty("WP_LINK_COLL2", "https://www-coll2.enel.it");
		prop.setProperty("WP_LINK_CORPORATE", "https://corporate-qual.enel.it");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.store(new FileOutputStream(nomeScenario), null);
    };
	
	@Test
    public void eseguiTest() throws Exception{
//		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
//		LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
//		log.launchCorporateLink(prop.getProperty("WP_LINK_CORPORATE"));
//		Thread.sleep(3000);
//		log.launchLink(prop.getProperty("WP_LINK_COLL2"));
//		Thread.sleep(3000);
//		log.launchLink(prop.getProperty("WP_LINK"));
//		Thread.sleep(3000);
		
		//your steps
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
		log.launchLink("https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON#");
		POE2_Component com = new POE2_Component(driver);
		com.verifyComponentExistence(com.cookieAcceptance);
		com.clickComponent(com.cookieAcceptance);
		com.verifyComponentExistence(com.inviaMessButton);
		Thread.sleep(15000);
		com.clickComponent(com.inviaMessButton);
		Thread.sleep(300);
		com.insertText(com.nome, "FARFALLA");
		Thread.sleep(300);
		com.insertText(com.cognome, "FARFALLINA");
		Thread.sleep(300);
		com.insertText(com.cellulare, "3269102881");
		Thread.sleep(300);
		com.insertText(com.indirizzo, "Via Nizza 134");
		Thread.sleep(300);
		com.insertText(com.email, "vinc@yopmail.it");
		Thread.sleep(300);
		com.insertText(com.emailConf, "vinc@yopmail.it");
		Thread.sleep(300);
		com.insertText(com.username, "FRFFFL73A41H501N");
		Thread.sleep(300);
		com.clickComponent(com.no);
		com.scrollComponent(com.allegati);
		Thread.sleep(5000);
		com.clickComponent(com.argomento);
		Thread.sleep(3000);
		com.jsClickComponent(com.option);
		Thread.sleep(300);
		com.insertText(com.note, "AAA BB CCC");
		Thread.sleep(300);
		com.scrollComponent(com.privacy);
		com.clickComponent(com.allegati);
		Thread.sleep(20000);
		com.clickComponent(com.privacy);
		Thread.sleep(300);
		com.clickComponent(com.accediBtn);
		
		com.verifyComponentExistence(com.uploadButton);
		com.SendKeys(com.uploadButton, "C:\\Users\\gaetavi\\Desktop\\bolletta.pdf");
	};
	
	@After
    public void fineTest() throws Exception{ };
}
