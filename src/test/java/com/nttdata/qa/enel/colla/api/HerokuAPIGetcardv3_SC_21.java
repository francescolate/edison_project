// HerokuAPIGetcardv3_SC_21
// Solo per APP  - TOken in input expire 31/12/2021
/**"userUPN": "021bc291-c6bc-4ae7-aa67-dee6e6fc3c5c",
"cf": "BYDJME69R47Z330V",
"enelId": "95c8a66b-7004-4098-97d8-786a4da0d802",
"userEmail": "raffae.llaquom.o@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\MobileTokenJWT.jar BYDJME69R47Z330V raffae.llaquom.o@gmail.com 021bc291-c6bc-4ae7-aa67-dee6e6fc3c5c 95c8a66b-7004-4098-97d8-786a4da0d802 2021-12-31**/
// OK - RES - Check Stato Fornitura CESSATA

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv3_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.APIService;

public class HerokuAPIGetcardv3_SC_21 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CF","BYDJME69R47Z330V");
		//prop.setProperty("USERNAME","raffae.llaquom.o@gmail.com");
		//prop.setProperty("USERUPN","021bc291-c6bc-4ae7-aa67-dee6e6fc3c5c");
		//prop.setProperty("ENELID","95c8a66b-7004-4098-97d8-786a4da0d802");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("PASSWORD","Password01");
		//prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v3/getcards");
		//prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \""+prop.getProperty("USERNAME")+"\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");
		//prop.setProperty("JSON_DATA_OUTPUT", "commodity\":\"Gas\",\"pod\":\"05260000037612\",\"isScegliTu\":false,\"businessPartner\":\"0031747885\",\"idAsset\":\"02i0Y000000nUGHQA2\",\"subscription\":false,\"title\":\"Cessata\",\"for_met_pag\":\"Bollettino Postale\",\"contoContrattuale\":\"001023940919\",\"offertaAttiva\":\"City Gas\",\"stato\":\"Cessata\",\"isSubscription\":false,\"alias\":\"Gas\",\"isOreFree\":false,\"rvc\":false,\"oreFree\":false,\"isBollettaWebActivated\":false,\"categoriaFornituraCommodity\":\"-\",\"dataInizioCalendario\":\"20200617\",\"Color\":{\"titleRightColor\":\"#7F7F7F\",\"backgroundColor\":\"#FFFFFF\",\"textRightFooterColor\":\"\",\"textFooterColor\":\"#7F7F7F\",\"barFooterColor\":\"\",\"lineColor\":\"#ECECEC\",\"subBarFooterColor\":\"\",\"subTitleLeftColor\":\"#7F7F7F\",\"titleLeftColor\":\"#000000\",\"textLeftFooterColor\":\"\"},\"dataInizioFormaContrattuale\":\"17.06.2020\",\"okSap\":false,\"isRvc\":false,\"idListino\":\"a1l0Y000000mK7FQAU\",\"flagElight\":\"N\",\"dataAttivazione\":\"20.09.2016\",\"codiceFornitura\":\"687993374\",\"idProdotto\":\"a1Y2400000BFIq4EAH\",\"dataFineCalendario\":\"20200531\",\"indFornitura\":\"VIA G. DON BARTOLOMEO 80 - 20161 MILANO MI\",\"canalePag\":\"bollettino postale\",\"descrizione\":\"La tua fornitura gas è cessata. Hai comunque accesso allo storico delle tue bollette\",\"scegliTu\":false");
		
		prop.setProperty("CF_PIVA","DRADRA60D24A944Z");
	    prop.setProperty("USERNAME","r32019p109statocontiwe.b@gmail.com");
	    prop.setProperty("USERUPN","fef896fe-7b6d-4028-8664-a93fb8e15b15");
	    prop.setProperty("ENELID","bd33b186-fae7-4519-a430-14f42c9cfa00");
	    prop.setProperty("JSON_DATA_OUTPUT", "{\"commodity\":\"Gas\",\"pod\":\"03081000468030\",\"isScegliTu\":false,\"businessPartner\":\"0037774929\",\"idAsset\":\"02i0Y000007d3ohQAA\",\"subscription\":false,\"title\":\"Cessata\",\"for_met_pag\":\"Bollettino Postale\",\"contoContrattuale\":\"001028394984\",\"offertaAttiva\":\"Gas 20\",\"stato\":\"Cessata\",\"isSubscription\":false,\"alias\":\"Gas\",\"isOreFree\":false,\"rvc\":false,\"oreFree\":false,\"isBollettaWebActivated\":false,\"categoriaFornituraCommodity\":\"-\",\"dataInizioCalendario\":\"20210404\",\"Color\":{\"titleRightColor\":\"#7F7F7F\",\"backgroundColor\":\"#FFFFFF\",\"textRightFooterColor\":\"\",\"textFooterColor\":\"#7F7F7F\",\"barFooterColor\":\"\",\"lineColor\":\"#ECECEC\",\"subBarFooterColor\":\"\",\"subTitleLeftColor\":\"#7F7F7F\",\"titleLeftColor\":\"#000000\",\"textLeftFooterColor\":\"\"},\"dataInizioFormaContrattuale\":\"01.02.2018\",\"okSap\":false,\"isRvc\":false,\"idListino\":\"a1l0Y000000SrQLQA0\",\"flagElight\":\"N\",\"dataAttivazione\":\"01.03.2018\",\"codiceFornitura\":\"300001597\",\"idProdotto\":\"a1Y1l000002PWtvEAG\",\"dataFineCalendario\":\"20211003\",\"indFornitura\":\"VIA VIRGINIA 12/A - 40017 SAN GIOVANNI IN PERSICETO BO\",\"canalePag\":\"bollettino postale\",\"descrizione\":\"La tua fornitura gas è cessata. Hai comunque accesso allo storico delle tue bollette\",\"scegliTu\":false}]}");
		
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check Stato Fornitura CESSATA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv3_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
