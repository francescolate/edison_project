package com.nttdata.qa.enel.colla;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_Info_Enel_Energia_ACR_289;
import com.nttdata.qa.enel.util.ReportUtility;

public class Web_Privato_Info_Enel_Energia_ACR_289 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		//No supply in the given login
		//other login
//		prop.setProperty("WP_USERNAME", "tes.tin.g.c.r.m.a.u.t.o.m.a.tion@gmail.com");
		prop.setProperty("WP_USERNAME", "mobiletesting.nttdata@gmail.com");

		
		//prop.setProperty("WP_USERNAME", "collaudoloyalty.colazzo2.020@gmail.com");//DLBSFN61D02C662O
		//prop.setProperty("WP_USERNAME", "collaud.oloyaltycolazzo2.020@gmail.com"); 
		//prop.setProperty("WP_USERNAME", "t.estingcrma.u.t.omation@gmail.com"); 
		//prop.setProperty("WP_USERNAME", "testi.ngcrma.u.t.o.m.a.t.i.on@gmail.com"); //MTTFNC33L21F890Y
		//prop.setProperty("WP_USERNAME", "collaudo.loyaltycolazzo2.020@gmail.com"); //PTTGNI44S03L669Z
		//prop.setProperty("WP_USERNAME", "collaudoloya.ltycolazzo20.2.0@gmail.com"); //LRAFNC94R61B428P
		prop.setProperty("WP_PASSWORD", "Password01"); 
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("RUN_LOCALLY","Y");

		prop.store(new FileOutputStream(nomeScenario), null);
	};

	@Test
	public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		LoginEnel.main(args);
		Privato_Info_Enel_Energia_ACR_289.main(args);
	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
