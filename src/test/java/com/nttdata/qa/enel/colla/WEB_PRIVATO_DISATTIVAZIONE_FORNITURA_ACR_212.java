package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.testqantt.colla.Privato_212_ACR_Disattivazione;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PRIVATO_DISATTIVAZIONE_FORNITURA_ACR_212 {

	Properties  prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception
	{
		this.prop = new Properties();
		prop.setProperty("WP_LINK","https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "annamaria.cecili@enelsaa3006.enel.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "CASA");
		prop.setProperty("ACCOUNT_TYPE", "");
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");			
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SERVIZI_HOMEPAGE", "Servizi per le forniture");
		prop.setProperty("SERVIZI_HOMEPAGE_SECTIONTITLE", "Servizi per il contratto");
		prop.setProperty("DISATTIVAZIONE_FORNITURA_PAGE_HEADING", "Disattivazione Fornitura");
		prop.setProperty("DISATTIVAZIONE_FORNITURA_PAGE_TITLE", "Disattivazione Fornitura");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.store(new FileOutputStream(nomeScenario), null);
		
	}

	
	@Test
	public void eseguiTest() throws Exception
	{
		
		String args[] = {nomeScenario};
		LoginEnel.main(args);
		Privato_212_ACR_Disattivazione.main(args);
	}
	
	@After
	public void fineTest() throws Exception
	{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}

}
