// HerokuAPIGetcardv4_SC_75
// Solo per WEB  - TOken in input expire 31/12/2021
/*"userUPN": "9598e546-a467-4f91-b817-8aad04dff4d1",
"cf": "BYDJME69R47Z330V",
"enelId": "11e45824-e440-45bb-af81-ac427547d78c",
"userEmail": "raffae.llaquom.o@gmail.com"
**java -jar C:\Users\manzofa\Desktop\ENEL_AUTOMAZIONE\R3_2020_WEB\Progettazione_API_HEROKU_WEB\Genera_Token_API_WEB\WEBTokenJWT.jar BYDJME69R47Z330V raffae.llaquom.o@gmail.com 9598e546-a467-4f91-b817-8aad04dff4d1 11e45824-e440-45bb-af81-ac427547d78c 2021-12-31
*/
// OK - RES - Check supply status CESSATA

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIGetcardv4_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;

public class HerokuAPIGetcardv4_SC_75 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
	

		prop.setProperty("RUN_LOCALLY","Y");
		//prop.setProperty("CF_PIVA","BYDJME69R47Z330V");
		//prop.setProperty("USERNAME","raffae.llaquom.o@gmail.com");
		//prop.setProperty("USERUPN","9598e546-a467-4f91-b817-8aad04dff4d1");
		//prop.setProperty("ENELID","11e45824-e440-45bb-af81-ac427547d78c");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("API_URL", "https://msa-stage.enel.it:443/msa/app/supply/v4/getcards");
		//prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + " \",   \"sid\": \""+ prop.getProperty("SID") + "\",      \"username\":\""+prop.getProperty("USERNAME")+"\",       \"keys\":[           {              \"key\":\"APP_VER\",             \"value\":\"20.3.0\"          },          {              \"key\":\"OS\",             \"value\":\"IOS\"          },          {              \"key\":\"OS_VER\",             \"value\":\"10.2.1\"          },          {              \"key\":\"DEVICE\",             \"value\":\"iPhone5\"          },          {              \"key\":\"ID_DISPOSITIVO\",             \"value\":\"ABAB288B-1C8D-4C88-95FE-2E9966AE32B3\"          }       ]    } }");
		//prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
		//prop.setProperty("JSON_DATA_OUTPUT", "\"commodity\":\"Gas\",\"pod\":\"05260000037612\",\"businessPartner\":\"0031747885\",\"idAsset\":\"02i0Y000000nUGHQA2\",\"subscription\":false,\"for_met_pag\":\"Bollettino Postale\",\"contoContrattuale\":\"001023940919\",\"offertaAttiva\":\"City Gas\",\"stato\":\"Cessata\",\"isSubscription\":false,\"alias\":\"Gas\",\"isOreFree\":false,\"isInfoEnelEnergiaActivated\":\"non attivo\",\"rvc\":false,\"oreFree\":false,\"isBollettaWebActivated\":\"non attivo\",\"dataInizioCalendario\":\"20200617\",\"dataInizioFormaContrattuale\":\"17.06.2020\",\"okSap\":false,\"isRvc\":false,\"idListino\":\"a1l0Y000000mK7FQAU\",\"dataAttivazione\":\"20.09.2016\",\"codiceFornitura\":\"687993374\",\"idProdotto\":\"a1Y2400000BFIq4EAH\",\"dataFineCalendario\":\"20200531\",\"indFornitura\":\"VIA G. DON BARTOLOMEO 80 - 20161 MILANO MI\",\"usoFornitura\":\"Uso Abitativo\"");
				
		prop.setProperty("CF_PIVA","DRADRA60D24A944Z");
	    prop.setProperty("USERNAME","r32019p109statocontiwe.b@gmail.com");
	    prop.setProperty("USERUPN","fef896fe-7b6d-4028-8664-a93fb8e15b15");
	    prop.setProperty("ENELID","bd33b186-fae7-4519-a430-14f42c9cfa00");
	    prop.setProperty("JSON_DATA_OUTPUT", "{\"commodity\":\"Gas\",\"pod\":\"03081000468030\",\"isScegliTu\":false,\"businessPartner\":\"0037774929\",\"idAsset\":\"02i0Y000007d3ohQAA\",\"subscription\":false,\"for_met_pag\":\"Bollettino Postale\",\"contoContrattuale\":\"001028394984\",\"offertaAttiva\":\"Gas 20\",\"stato\":\"Cessata\",\"isSubscription\":false,\"alias\":\"Gas\",\"isOreFree\":false,\"isInfoEnelEnergiaActivated\":\"Attivo\",\"rvc\":false,\"oreFree\":false,\"isBollettaWebActivated\":\"non attivo\"");

		
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * OK - RES - Check supply status CESSATA
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIGetcardv4_1.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
