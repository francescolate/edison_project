/* HerokuAPICoperturaStrada_SC_104 
** API call for Tokens
* CoperturaStrada - OK - WEB - RES
*/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPICoperturaStrada_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPICoperturaStrada_SC_104 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("STRADA","via ascari");
		prop.setProperty("ID_COMUNE","58091");
		prop.setProperty("CF_PIVA","LNERFL38B08H643E");
		prop.setProperty("USERNAME","r32.019p109statocontiweb@gmail.com");
		prop.setProperty("USERUPN","fede4574-670a-4668-a0a8-1990f0032768");
		prop.setProperty("ENELID","6b789a66-ec0c-4f05-b51d-0d89b98f5f4f");
		prop.setProperty("EXPIRATION","2021-12-31");
		prop.setProperty("SOURCECHANNEL","WEB");
		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
		prop.setProperty("SET_AUTH", "Y");
//		prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJFbmVsU2VsZkNhcmUiLCJzdWIiOiJFRTAwMDE5OTI5MjhAQk9YLkVORUwuQ09NIiwiaXNzIjoiV0VCIiwiZXhwIjoxNjA5MzIyNzQ1LCJpYXQiOjE1OTMwNzU5NDUsInVzZXJpZCI6IkVFMDAwMDAwMjcyOEBCT1guRU5FTC5DT00iLCJqdGkiOiI3ZGJmY2VhYy1lMTllLTRjYWEtYjYwMy0wOTBjMmUwODE4YWQifQ.PbJspaRkNt6jq6xINp53v-F7Xri5EUHrdBW-x7BV2Mg");
        prop.setProperty("JSON_DATA_OUTPUT", "{\"streets\":[{\"commune\":{\"province\":{\"name\":\"Roma\",\"id\":\"058\",\"region\":{\"name\":\"Lazio\",\"id\":\"12\"}},\"cadastralCode\":\"H501\",\"name\":\"Roma\",\"id\":\"58091\"},\"name\":\"ALBERTO ASCARI\",\"id\":\"38000079839\",\"toponymy\":\"VIA\"}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * API call for Tokens
     * CoperturaStrada - OK - WEB - RES 
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPICoperturaStrada_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
