/* HerokuAPICoperturaIndirizzo_SC_226 
 * Need Token  - NEED VPN ENEL
** OK - RES - RICONTRATTUALIZZAZIONE - ORE FREE Annullata
* "userUPN": "EE0000018072@BOX.ENEL.COM",
"cf": "FRTFRC85B18G388N",
"enelId": "e884e754-3add-4cdb-9ad4-73fa005eb55e",
"userEmail": "annamaria.cecili@enelsaa3010.enel.com"
**/

package com.nttdata.qa.enel.colla.api;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.api.HerokuAPIOfferteCRMT_1;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.jwt.gen.*;


public class HerokuAPIOfferteCRMT_SC_226 {
	
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
		
	@Before 
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("CANALE","A");
		prop.setProperty("NUM_MESI","0");
		prop.setProperty("CF_PIVA","FRTFRC85B18G388N");
		prop.setProperty("USERNAME","annamaria.cecili@enelsaa3010.enel.com");
		prop.setProperty("USERUPN","EE0000018072@BOX.ENEL.COM");
		prop.setProperty("ENELID","e884e754-3add-4cdb-9ad4-73fa005eb55e");
		prop.setProperty("EXPIRATION","2021-12-31");

//		prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
//		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=" + prop.getProperty("CANALE") + "&numMesiMaxAnnullamento=" + prop.getProperty("NUM_MESI") + "&cf=" + prop.getProperty("CF_PIVA"));
		prop.setProperty("API_URL", "http://10.151.58.208:82/enel-crmt-anagrafica-stage/v1/fibra/offerte?canale=");
		prop.setProperty("JSON_DATA_OUTPUT", "{\"causaleAnnullamento\":\"Annullamento da Operatore\",\"numeroOre\":\"3\",\"statoCaseItem\":\"Annullato\",\"statoUdb\":null,\"tipoOfferta\":\"Elettrico\",\"podPdr\":\"IT001E17747291\",\"statoSap\":null,\"statoSempre\":null,\"oraFineFasciaDefault\":null,\"numeroOfferta\":\"SG3124491\",\"flagStd\":0,\"namingInFattura\":\"Scegli Tu Ore Free\",\"statoR2d\":null,\"numeroUtente\":\"310488453\",\"processoInCorso\":\"RICONTRATTUALIZZAZIONE\",\"idCase\":\"5001l000004vKgAAAU\",\"tipologiaProdotto\":\"RES\",\"idListino\":\"a1l1l000000OP8qAAG\",\"dataAttivazione\":\"2019-09-01T00:00:00.000Z\",\"idCaseItem\":\"a381l000000MBIIAA4\",\"dataRichiesta\":\"2020-01-01T00:00:00.000Z\",\"idProdotto\":\"a1Y1l000000AxqDEAS\",\"commodityCaseItem\":\"ELETTRICO\",\"indirizzoFornitura\":\"VIA GIACOMO MATTEOTTI SNC - 27010 MARZANO PV\",\"oraInizioFasciaDefault\":null,\"tipoOperazione\":\"Attivazione\",\"processoCombinato\":false}");
		
		prop.store(new FileOutputStream(nomeScenario), null);	
		
	};
	/**
	 * Effettuare la chiamata API con i vari tag in input 
     * Verificare l'output
     * Need Token  - NEED VPN ENEL
     * OK - RES - RICONTRATTUALIZZAZIONE - ORE FREE Annullata
	 * @throws Exception
	 */
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
	//	requestObject 
		HerokuAPIOfferteCRMT_1.main(args); 
		
	};
	
	@After
    public void fineTest() throws Exception{
             //String args[] = {nomeScenario};
             InputStream in = new FileInputStream(nomeScenario);
             prop.load(in);
             this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
             ReportUtility.reportToServer(this.prop);
       };

	

}
