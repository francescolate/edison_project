package com.nttdata.qa.enel.colla;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.colla.PubblicoGreen_Kit53;
import com.nttdata.qa.enel.util.ReportUtility;

public class WEB_PUBBLICO_GREEN_KIT_53 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{
		this.prop=new Properties();
		
		prop.setProperty("WP_LINK", "https://www-coll1.enel.it/");
		prop.setProperty("WP_USERNAME", "r.32019p109.statocontiweb@gmail.com");
		prop.setProperty("CODICE_CLIENTE", "310492518");
	    prop.setProperty("RUN_LOCALLY","Y");
	    prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("LUCE_E_GAS", "Luce e gas");
		prop.setProperty("IMPRESA", "Imprese");
		prop.setProperty("INSIEME A TE", "Insieme a te");
		prop.setProperty("STORIE", "Storie");
		prop.setProperty("FUTUR-E", "Futur-e");
		prop.setProperty("MEDIA", "Media");
		prop.setProperty("SUPPORTO", "Supporto");
		prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
		prop.setProperty("CREDITS", "Credits");
		prop.setProperty("PRIVACY", "Privacy");
		prop.setProperty("COOKIE_POLICY", "Cookie Policy");
		prop.setProperty("REMIT", "Remit");

		prop.store(new FileOutputStream(nomeScenario), null);		

    };
	
	@Test
    public void eseguiTest() throws Exception{
		String args[]= {nomeScenario};
		PubblicoGreen_Kit53.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
