package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.ConfermaCheckListAttivazionSDD;
import com.nttdata.qa.enel.testqantt.CreaNuovoSdd;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID12;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoGestioneSDD;
import com.nttdata.qa.enel.testqantt.ReSetIban_AttivazioneSDD11;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaFornituraAttivazioneSDD;
import com.nttdata.qa.enel.testqantt.SetCfForGestioneSdd;
import com.nttdata.qa.enel.testqantt.AnnullaCaseCreazioneSdd;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.ReportUtility;



/**
 * 'Profilo: PE Cliente: Residenziale Commodity: ELE
 * 
 * Descrizione scenario Attivazione SDD ELE da canale PE, cliente Residenziale,
 * IBAN estero, controlli formali obbligatorietà campi e validità IBAN estero
 * 
 * Risultato atteso Corretta verifica controlli formali.
 */
public class Gestione_Attivazione_SSD_EVO_id11 {


    
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";
	Logger LOGGER = Logger.getLogger("");
	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "");
		prop.setProperty("POD", "");

		prop.setProperty("ESITO_COMPATIBILITA", "Attivabile");

		// IBAN
		prop.setProperty("ATT_SDD_IBAN", "FR911450800030");
		prop.setProperty("ATT_SDD_IBAN_ESTERO", "Y");
		
		//gestione flusso creazione nuovo rid
		prop.setProperty("ATT_SDD_CLICK_SALVA_NUOVO_RID", "NO"); //Valorizzo a N in quanto il test verifica messaggio errore su validazione rid
		prop.setProperty("ATT_SDD_CLICK_CONFERMA_NUOVO_RID", "NO"); //Valorizzo a N in quanto il test verifica messaggio errore su validazione rid
		prop.setProperty("ATT_SDD_VERIFICA_CREAZIONE_NUOVO_RID", "NO"); //Valorizzo a N in quanto il test verifica messaggio errore su validazione rid
		prop.setProperty("ATT_SDD_CHECK_OBBLIGATORIETA_CAMPI", "Y");
		prop.setProperty("ATT_SSD_MSG_VALIDAZIONE_IBAN", "Attenzione l'Iban estero deve avere una lunghezza minima di 15 caratteri");
		//prop.setProperty("ATT_SSD_MSG_VALIDAZIONE_IBAN", "Il valore inserito è troppo corto");
		
		
		
		prop.setProperty("ATT_SDD_BANCA", "FINECOBANK SPA");
		prop.setProperty("ATT_SDD_AGENZIA", "SEDE DI ROMA");
		prop.setProperty("ATT_SDD_STATO", "ITALIA");


		



		prop.setProperty("RUN_LOCALLY", "Y");

	};

	@Test
	public void eseguiTest() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		SetCfForGestioneSdd.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoGestioneSDD.main(args);
		Identificazione_Interlocutore_ID12.main(args);
		ConfermaCheckListAttivazionSDD.main(args);
		SelezionaFornituraAttivazioneSDD.main(args);
		CreaNuovoSdd.main(args);
		ReSetIban_AttivazioneSDD11.main(args);
		CreaNuovoSdd.main(args);
		LOGGER.info(nomeScenario+" concluso con successo");
		AnnullaCaseCreazioneSdd.main(args);

		
	};

	@After
	public void fineTest() throws Exception {
		 String args[] = {nomeScenario};
		 InputStream in = new FileInputStream(nomeScenario);
		 prop.load(in);
		 this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 ReportUtility.reportToServer(this.prop);
	};

}
