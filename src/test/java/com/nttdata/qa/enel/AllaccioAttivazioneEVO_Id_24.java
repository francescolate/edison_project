package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneBSN;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaAllaccioAttivazioneNoRes;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoAllaccioAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoAllaccioAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.util.Costanti;

public class AllaccioAttivazioneEVO_Id_24 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "13771801001");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "PE");
			prop.setProperty("TIPO_CLIENTE", "Business");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
			prop.setProperty("MERCATO", "Salvaguardia");
			prop.setProperty("PROVINCIA", "FROSINONE");
			prop.setProperty("CITTA", "ANAGNI");
			prop.setProperty("INDIRIZZO", "VIA CALZATORA");
			prop.setProperty("CIVICO", "5");
			prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
			prop.setProperty("RESIDENTE", "NO");
			prop.setProperty("USO_ENERGIA", "Impianto di risalita e simili");
			prop.setProperty("TITOLARITA", "Uso/Abitazione");
			prop.setProperty("TENSIONE", "380");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "4");
			prop.setProperty("DISALIMENTABILITA", "SI");
			prop.setProperty("TIPO_MISURATORE", "Non Orario");
			prop.setProperty("CONSUMO_ANNUO", "23123");
			prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ALTRE PRODUZIONI DELLA CHIMICA SECONDARI");
			prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
	        prop.setProperty("SPLIT_PAYMENT", "No");
	        prop.setProperty("FLAG_136", "NO");
			prop.setProperty("CANALE_INVIO", "EMAIL");
			prop.setProperty("MODALITA_FIRMA", "DIGITAL");
			prop.setProperty("CLASSIFICAZIONE_PREVENTIVO", "NON PREDETERMINABILE");
			prop.setProperty("MODALITA_INVIO_PREVENTIVO", "Posta");
			prop.setProperty("MODALITA_FIRMA_PREVENTIVO", "No Vocal");
			prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
			prop.setProperty("PRODOTTO", "NEW_MS__B");
			prop.setProperty("TIPO_OI_ORDER", "Commodity");
			prop.setProperty("RUN_LOCALLY","Y");
			
			
			
			
//			// Dati R2d
//			prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
//			prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
////			prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
//			// Dati Verifiche pod iniziali
//			prop.setProperty("SKIP_POD", "Y");
//			prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
////			prop.setProperty("STATO_R2D", "AW");
//			// Dati 1OK
//			prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
//			prop.setProperty("TIPO_CONTATORE", "GME - Contatore Elettronico Non Orario");
//			prop.setProperty("POTENZA_FRANCHIGIA", "220");
//			// Dati 3OK
//			prop.setProperty("INDICE_POD", "1");
//			prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
//			prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");
			
			
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoAllaccioAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoAllaccioAttivazioneEVO.main(args);
			InserimentoIndirizzoEsecuzioneLavoriEVO.main(args);
			VerificaCreazioneOffertaAllaccioAttivazione.main(args);
			VerificaRiepilogoOffertaAllaccioAttivazione.main(args);
			SelezioneUsoFornitura.main(args);
			CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneBSN.main(args);
			ConfermaIndirizziAllaccioAttivazione.main(args);
			ConfermaFatturazioneElettronicaAllaccioAttivazioneNoRes.main(args);
			SplitPaymentEVO.main(args);
			CigCupEVO.main(args);
			PagamentoBollettinoPostaleEVO.main(args);
			ConfermaScontiBonusEVO.main(args);
			ConfiguraProdottoElettricoNonResidenziale.main(args);
			GestioneCVPAllaccioAttivazione.main(args);
			ConsensiEContattiAllaccioAttivazione.main(args);
			ModalitaFirmaAllaccioAttivazione.main(args);
			ConfermaOffertaAllaccioAttivazione.main(args);
			ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile.main(args);
			
			//Da impletementare: integrazione a valle risoluzione cvp e touch point(non si sa il flusso)

		};
		
		@After
        public void fineTest() throws Exception{
		
			
		};
		
		
		
		
		
	

}
