package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_APPEZZOTTA;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetPodPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_pod_acea_asset_attivi;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_2 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "S2S");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("MERCATO", "Libero");
			prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
			prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
			prop.setProperty("CAP", "00100");
			prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
			prop.setProperty("ESITO_OFFERTABILITA", "KO");
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		@Test
        public void eseguiTest() throws Exception{
			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};
			
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			SetQueryRecupera_pod_acea_asset_attivi.main(args);
			RecuperaDatiWorkbench.main(args);
			SetPodPrimaAttivazione.main(args);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			
			ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID17.main(args);
			SelezioneMercatoPrimaAttivazioneEVO.main(args);
			InserimentoFornitureEVO.main(args);
		};
		
		@After
        public void fineTest() throws Exception{
			String args[] = {nomeScenario};
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
			
		};
		
		
		
		
		
	

}
