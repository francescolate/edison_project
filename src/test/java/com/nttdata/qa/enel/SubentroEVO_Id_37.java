package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.util.Costanti;


public class SubentroEVO_Id_37 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();

		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("RIGA_DA_ESTRARRE", "6");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("PROCESSO", "Avvio Subentro EVO");
		prop.setProperty("TIPO_DOCUMENTO", "Patente");
		prop.setProperty("NUMERO_DOCUMENTO", "1231");
		prop.setProperty("RILASCIATO_DA", "ABCD");
		prop.setProperty("RILASCIATO_IL", "01/01/2020");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("SEZIONE_ISTAT", "N");
		prop.setProperty("LOCALITA_ISTAT", "ROMA");
		prop.setProperty("CAP", "20121");
		prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("TIPO_MISURATORE", "Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "4");
		prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("CLIENTE_BUSINESS", "N");
		prop.setProperty("LIST_TEXT", "N");
		prop.setProperty("CHECK_LIST_TEXT",
				"ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- Che il contatore sia cessato ed in passato abbia erogato Luce/gas- Che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:POD/PDR - Matr/EneltelINFORMAZIONI UTILI- In caso di Dual ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO- In caso di Multi ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO – PRODOTTO – STESSA COMMODITY- In caso di Cliente Pubblica Amministrazione sarà necessario il Codice Ufficio- In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)- In caso di Duale/Multi ricordati che non è possibile eseguire la modifica potenza e/o tensione- Se il cliente attiva metodo di pagamento SDD e Bolletta Web riceverà uno sconto\\bonus in fatturaFibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento del subentro. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta  In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente:-   numero di cellulare -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associata  SERVIZIO DI PRENOTAZIONEIl Servizio di Prenotazione consente al Cliente di concludere il contratto di fornitura oggi, scegliendo una data desiderata di attivazione del servizio. Il Cliente potrà modificare la data di attivazione successivamente, attraverso un link che riceverà nella propria casella email.ATTENZIONE: è necessario che il Cliente restituisca tutta la documentazione necessaria richiesta,  prima della data indicata, altrimenti non sarà possibile procedere con l’operazione.Quando è possibile richiedere il Servizio di Prenotazione:al momento il servizio è disponibile solo per operazioni di :-          riattivazione del contatore elettrico su fornitura singola (subentro single ele)-          operazioni a parità di condizioni tecniche  (senza modifica potenza e/o tensione)-          rete application to application  ( E- Distribuzione )-          comprese tra 20gg dalla data odierna e non oltre i 90gg (giorni solari) Cosa serve:E’ necessario fornire un:-          indirizzo EMAIL-          numero di telefono mobileSe i dati sono già presenti a sistema, chiedere conferma al cliente prima di procedere.  SCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("CAP_GAS", "37121");
		prop.setProperty("SEZIONE_ISTAT_GAS", "N");
		prop.setProperty("PROVINCIA_COMUNE_GAS", "VERONA");
		prop.setProperty("INDIRIZZO_GAS", "CORSO PORTA NUOVA");

		prop.setProperty("RESIDENTE", "SI");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
		prop.setProperty("CONSUMO_ANNUO", "1250");
		prop.setProperty("VALORE_MESI", "100");
		prop.setProperty("ERRORE_MESI",
				"I seguenti campi sono obbligatori o non correttamente compilati:Gennaio; Febbraio; Marzo; Aprile; Maggio; Giugno; Luglio; Agosto; Settembre; Ottobre; Novembre; Dicembre;");

		prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Riscaldamento + uso cottura cibi e/o produzione di acqua calda sanitaria");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO_FIRMA", "POSTA");
		
		prop.setProperty("RUN_LOCALLY", "Y");       

		prop.setProperty("PRODOTTO_ELE", "SCEGLI OGGI WEB LUCE");
		prop.setProperty("PIANO_TARIFFARIO_ELE", "Senza Orari");
		prop.setProperty("TAB_SGEGLI_TU_ELE", "SI");
		prop.setProperty("PRODOTTO_TAB_ELE", "Scegli Tu");
		prop.setProperty("PRODOTTO_GAS", "SICURA GAS");
		// prop.setProperty("VAS_EX_GAS", "SI");
		prop.setProperty("GESTIONE_APPUNTAMENTO", "Y");

        prop.setProperty("UNITA_ABITATIVA", "SI");

		return prop;
	}

	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		prop.load(new FileInputStream(nomeScenario));

		RecuperaPodNonEsistenteDual.main(args);

		LoginSalesForcePE.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args); // inserimento CF
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		SezioneMercatoSubentro.main(args);
		InserimentoFornitureDualSubentro.main(args);

		SelezioneUsoFornitura.main(args);      
        CommodityDualResidenzialeSubentroEVO.main(args);     
        ConfermaIndirizziPrimaAttivazione.main(args);
    	ConfermaScontiBonusEVO.main(args); 	
        GestioneFatturazioneElettronicaSubentro.main(args);
        PagamentoBollettinoPostale.main(args);     
        ConfiguraProdottoDualResidenzialeSubentro.main(args);
        GestioneCVPAllaccioAttivazione.main(args);
        SelezioneUnitaAbitativa.main(args);  
        ConsensiEContattiSubentro.main(args);     
        ModalitaFirmaSubentro.main(args);      
    	ConfermaOffertaAllaccioAttivazione.main(args);
  
    	LoginSalesForcePE.main(args);
        RicercaOffertaSubentro.main(args);
		CaricaEValidaDocumenti.main(args);
        System.out.println("Documenti caricati e validati");      
		
        //recupera orderID ele
        SetSubentroProperty.main(args);
        RecuperaOrderIDDaPodEle.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);
        System.out.println("ID recuperato");
       
        // Dati R2d Ele      		
        SetSubentroEleOrarioR2D.main(args);
        LoginR2D_ELE.main(args);
        TimeUnit.SECONDS.sleep(5); 
        R2D_VerifichePodIniziali_ELE.main(args);
        TimeUnit.SECONDS.sleep(5);
        R2D_InvioPSPortale_1OK_ELE.main(args);
        R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
        R2D_CaricamentoEsiti_5OK_ELE.main(args);
        R2D_VerifichePodFinali_ELE.main(args);
        
        //recupera ordereID gas 
		
        SetSubentroProperty.main(args);
        LoginSalesForce.main(args);
        RicercaOffertaSubentro.main(args);
        RecuperaOrderIDDaPodGas.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);
        System.out.println("ID recuperato");
        
        // Dati R2d Gas
        
        SetSubentro15R2DGAS.main(args);           
        LoginR2D_GAS.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodIniziali_GAS.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_InvioPSPortale_1OK_GAS.main(args);
        R2D_CaricamentoEsiti_3OK_GAS.main(args);
        R2D_CaricamentoEsiti_5OK_Standard_GAS.main(args);
        R2D_VerifichePodFinali_GAS.main(args);
        
        
/*
        //Da eseguire dopo qualche ora
        prop.store(new FileOutputStream(nomeScenario), null);
        SetSubentroBatch.main(args);      
        prop.load(new FileInputStream(nomeScenario));
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaRichiesta.main(args);
        //VerificheRichiestaDaPod.main(args);*/




	}

	@After
	public void fineTest() throws Exception {
		/*
		 * prop.load(new FileInputStream(nomeScenario));
		 * this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 * ReportUtility.reportToServer(this.prop);
		 */
	}
}
