package com.nttdata.qa.enel;


import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniDettagliSpedizioneCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID21;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaLinkItemCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.RecuperaStatusModelloPlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.SalvaNumeroDocumento;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaSezioneTipologiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaCasePlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaKitContrattualeCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaKitContrattualePadreCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaNumeroRichiestaCaseCopiaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaSelezioneFornitureCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoCaseCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoDocumentoWorkbench;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class CopiaDocumentiEVO_Id_1 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void inizioTest() throws Exception {
        this.prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
        prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
        prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
        prop.setProperty("NOME_UTENTE", "723535469");
        prop.setProperty("TIPOLOGIA_DOC", "KIT_CONTRUATTUALE");
        prop.setProperty("ID_DOCUMENTO", "0332118324");
        prop.setProperty("ID_DOCUMENTO_2", "0332118327");
        prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("STATO_DOCUMENTO", "CHIUSO");
        prop.setProperty("SOTTOSTATO_DOCUMENTO", "RICEVUTO");
    }

    @Test
    public void eseguiTest() throws Exception {

 //       prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load( new FileInputStream(nomeScenario));
/*
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID21.main(args);
        VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
        VerificaSelezioneFornitureCopiaDocumenti.main(args);
        SelezionaSezioneTipologiaDocumenti.main(args);
        VerificaKitContrattualePadreCopiaDocumenti.main(args);
        VerificaKitContrattualeCopiaDocumenti.main(args);
        AzioniDettagliSpedizioneCopiaDocumenti.main(args);
        SalvaNumeroDocumento.main(args);

        //presenza problematica segnalata: case rimane in stato IN LAVORAZIONE (modulo gui)
        VerificaStatoSottostatoCaseCopiaDocumenti.main(args);
        //verifica presenza reconds document associati al Case
        RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
        VerificaCasePlicoCopiaDocumenti.main(args);
        //verifica presenza item con campo Link valorizzato
        RecuperaLinkItemCaseCopiaDocumenti.main(args);
        //presenza problematica segnalata: case rimane in stato IN LAVORAZIONE (modulo Workbench)
        //verifica case: Stato - Chiuso, Sottostato - Ricevuto
        VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);*/
        VerificaStatoSottostatoDocumentoWorkbench.main(args);
    }


    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }

}
