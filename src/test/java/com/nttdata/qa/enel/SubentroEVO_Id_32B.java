package com.nttdata.qa.enel;

import com.nttdata.qa.enel.components.lightning.FatturazioneElettronicaComponent;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CommodityGasResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSubentroEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSubentro;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;

import com.nttdata.qa.enel.testqantt.InserimentoFornitureSubentroSingolaEleGas;
import com.nttdata.qa.enel.testqantt.LoginSalesForcePE;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostale;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetPropertyCF;
import com.nttdata.qa.enel.testqantt.SetPropertyPOD;
import com.nttdata.qa.enel.testqantt.SetSubentroqueryCF_subentro_id32B;
import com.nttdata.qa.enel.testqantt.SetSubentroqueryPod_subentro_id32B;
import com.nttdata.qa.enel.testqantt.SezioneMercatoSubentro;
import com.nttdata.qa.enel.testqantt.SezioneMercatoSubentroLiberoAggiungiFornituraOffertabilitàKo;
import com.nttdata.qa.enel.util.Costanti;



import com.nttdata.qa.enel.util.ReportUtility;
import org.apache.poi.util.SystemOutLogger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class SubentroEVO_Id_32B {

    Properties prop;
    String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "STS");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "20121");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("TIPO_DOCUMENTO", "Patente");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("USO", "Uso Abitativo");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("LIST_TEXT", "N");

        prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "NO");
         
        prop.setProperty("RIGA_DA_ESTRARRE", "7");
        prop.setProperty("RUN_LOCALLY", "Y");

        prop.setProperty("CODICE_FISCALE","CLZGRL86R14D883O");
        prop.setProperty("COMMODITY", "GAS");
    	
        prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
        prop.setProperty("CATEGORIA_USO", "Riscaldamento + uso cottura cibi e/o produzione di acqua calda sanitaria");
        prop.setProperty("TITOLARITA", "Uso/Abitazione");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");

        prop.setProperty("INSERISCI_INDIRIZZO_FATTURAZIONE", "Y");
        prop.setProperty("PROVINCIA_FATTURA", "Lecce");
        prop.setProperty("LOCALITA_FATTURA", "Lecce");
        prop.setProperty("INDIRIZZO_FATTURA", "Via asdasda");
        prop.setProperty("CIVICO_FATTURA", "1");
        prop.setProperty("COMUNE_FATTURA", "Lecce");
        prop.setProperty("CAP_FATTURA", "73100");
        
        prop.setProperty("CHECK_BANNER_ERRORE", "Y");
        prop.setProperty("CODICE_AMICO", "123abc456");
        
        
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {
       // prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
  /*
        RecuperaPodNonEsistente.main(args);
        LoginSalesForcePE.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID15.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);
        SelezioneUsoFornitura.main(args);
        CommodityGasResidenzialeSubentro.main(args);
        ConfermaIndirizziSubentroEVO.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusSubentro.main(args);
        System.out.println("fine");
  */
    }

    @After
    public void fineTest() throws Exception {
    	
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}