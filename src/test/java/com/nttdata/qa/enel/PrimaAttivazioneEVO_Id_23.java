package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEleGasPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetPropertyCFPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.SetPropertyPODELE_PA23;
import com.nttdata.qa.enel.testqantt.SetPropertyPODGAS_PA23;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_recupera_CF_PrimaAttivazioneId11;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_23 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("MATRICOLA_CONTATORE", "Y");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("TIPO_CLIENTE", "Non Residenziale");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "20121");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("RIGA_DA_ESTRARRE", "2");

	};

	@Test
	public void eseguiTest() throws Exception {
	
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		SetQueryRecupera_recupera_CF_PrimaAttivazioneId11.main(args);
		
		RecuperaDatiWorkbench.main(args);
		SetPropertyCFPrimaAttivazione.main(args);
		
		//recupero pod gas non esistente
		RecuperaPodNonEsistente.main(args);
		SetPropertyPODGAS_PA23.main(args);
		
		//recupero pod ele non esistente
		RecuperaPodNonEsistente.main(args);
		SetPropertyPODELE_PA23.main(args);
		
		//recupero secondo pod ele non esistente
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
	//	CercaClientePerNuovaInterazioneEVO.main(args);
		CercaClientePerNuovaInterazione.main(args);
		ProcessoPrimaAttivazioneNonResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID14.main(args);
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEleGasPrimaAttivazione.main(args);
	};

	@After
	public void fineTest() throws Exception {
		String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
