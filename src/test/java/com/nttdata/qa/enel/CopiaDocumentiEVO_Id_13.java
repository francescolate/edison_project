package com.nttdata.qa.enel;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;


import com.nttdata.qa.enel.testqantt.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class CopiaDocumentiEVO_Id_13 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME", "bo_rcd_super_user_cfi_l@enelfreemarket.com.uat");
		prop.setProperty("PASSWORD", "UATSFDc_2028");
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		//prop.setProperty("CODICE_FISCALE", "80004990687");
		prop.setProperty("CODICE_FISCALE", "97081650588");
		prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
		//prop.setProperty("NOME_UTENTE", "476347769");
		prop.setProperty("NOME_UTENTE", "266395370");
		//prop.setProperty("NUMERO_FATTURA", "0000003022764194");
		prop.setProperty("NUMERO_FATTURA", "0000002414984315");
		prop.setProperty("TIPOLOGIA_DOC", "FATTURE");
		prop.setProperty("TIPOLOGIA_DMS", "Tutte");
		prop.setProperty("TIPO_UTENZA", "BO");
		//prop.setProperty("ID_DOCUMENTO", "a3W1l000001CXNpEAO");	
	    prop.setProperty("ID_DOCUMENTO", "a3W1l000001CXNpEAO");
		prop.setProperty("CANALE_INVIO", "");
		prop.setProperty("MODALITA_DUPLICATO","Copia Originale");
		prop.setProperty("RUN_LOCALLY", "Y");
	}

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load( new FileInputStream(nomeScenario));
	/*
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneCustomEVO.main(args);
		AvvioProcesso.main(args);
		VerificaNumeroRichiestaCaseCopiaDocumento.main(args);*/
		VerificaSelezioneFornitureCopiaDocumenti.main(args);
		SelezionaSezioneTipologiaDocumenti.main(args);
		VerificaSelezioneFattureCopiaDocumenti.main(args);
		SalvaNumeroDocumento.main(args);
		RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
		VerificaCasePlicoCopiaDocumenti.main(args);
		RecuperaLinkItemCaseCopiaDocumenti.main(args);
	}

	@After
	public void fineTest() throws Exception {
		prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}
}
