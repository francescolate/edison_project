package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.RESBollettaWebComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_323_Revoca_Bolletta_WEB {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String currentHandle ="";

		try {		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);		
			RESBollettaWebComponent rbwc = new RESBollettaWebComponent(driver);
			
			prop.setProperty("SUPPLY_SERVICE", "Autolettura InfoEnelEnergia Modifica Indirizzo Fatturazione Modifica Potenza e/o tensione Bolletta Web Pagamento Online Addebito Diretto Rettifica Bolletta Rateizzazione Bolletta di Sintesi o di Dettaglio Duplicato Bolletta Invio Documenti Cambio Piano Modifica Consumo Concordato Disattivazione Fornitura");
			
			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
	            rbwc.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
			
			logger.write("Verifi home page title, text etc -- Start");			
			rbwc.comprareText(rbwc.homePageTitle, rbwc.HomePageTitle, false);
			rbwc.comprareText(rbwc.homePageText, rbwc.HomePageText, true);	
			rbwc.comprareText(rbwc.homePageSubTitle1, rbwc.HomePageSubTitle1, true);	
			rbwc.comprareText(rbwc.homePageSubText1, rbwc.HomePageSubText1, true);	
			rbwc.comprareText(rbwc.homePageSubTitle2, rbwc.HomePageSubTitle2, true);	
			rbwc.comprareText(rbwc.homePageSubText2, rbwc.HomePageSubText2, true);			
			logger.write("Verifi home page title, text etc -- Completed");
			
			logger.write("Click on Servizi -- Start");
			rbwc.verifyComponentExistence(rbwc.servizi);
			rbwc.clickComponent(rbwc.servizi);
			logger.write("Click on Servizi -- Completed");
			
			
			logger.write("Verifi Servizi page Path, title and Text -- Start");
			rbwc.comprareText(rbwc.serviziPageTitle, rbwc.ServiziPageTitle, false);
			rbwc.comprareText(rbwc.serviziPageSubTitle1, rbwc.ServiziPageSubTitle1, false);
			rbwc.comprareText(rbwc.serviziPageSubTitle2, rbwc.ServiziPageSubTitle2, false);
			rbwc.checkForSupplyServices(prop.getProperty("SUPPLY_SERVICE"));
			logger.write("Verifi Servizi page Path, title and Text -- Completed");
			
			logger.write("Click on Bolletta Web-- Start");
			rbwc.verifyComponentExistence(rbwc.bollettaWeb);
			rbwc.clickComponent(rbwc.bollettaWeb);
			logger.write("Click on Bolletta Web-- Completed");
			
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Start");
			rbwc.comprareText(rbwc.pageTitleBW, rbwc.PageTitleBW, false);
			rbwc.comprareText(rbwc.pageTextBW, rbwc.PageTextBW, false);
			rbwc.comprareText(rbwc.pageSubTextBW, rbwc.PageSubTextBW, false);
			rbwc.comprareText(rbwc.pageFooterTextBW, rbwc.PageFooterTextBW, false);
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Completed");

			logger.write("Click on avvia la Richiesta-- Start");
			rbwc.clickComponent(rbwc.avvialaRichiesta);	
			logger.write("Click on avvia la Richiesta-- Completed");
			
			logger.write("Click on Disattiva Bolletta Web-- Start");
			rbwc.comprareText(rbwc.pageTitleDisattivaBW, rbwc.PageTitleDisattivaBW, false);
			rbwc.comprareText(rbwc.pageTextDisattivaBW, rbwc.PageTextDisattivaBW, true);
		//	rbwc.comprareText(rbwc.pageSubTextDisattivaBW, rbwc.PageSubTextDisattivaBW, false);
			rbwc.verifyComponentExistence(rbwc.checkSupply);
			rbwc.clickComponent(rbwc.checkSupply);	
			rbwc.verifyComponentExistence(rbwc.attenzione);
			rbwc.comprareText(rbwc.attenzione, rbwc.Attenzione, true);
			logger.write("Click on Disattiva Bolletta Web-- Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
