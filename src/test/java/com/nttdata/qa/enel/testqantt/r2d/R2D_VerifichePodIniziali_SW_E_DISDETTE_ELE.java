package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerificaPodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE {

	@Step("R2D Verifiche POD Iniziali Switch - ELE")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		Boolean praticaTrovata = false;

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			String statopratica="vuota";
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				logger.write("Menu Report Switch e Disdette - Start");
				 menuBox.selezionaVoceMenuBox("Interrogazione","Report Switch e Disdette");
				logger.write("Menu Report Switch e Disdette - Completed");
				//Indice POD
//				int indice=0;
//				if(!prop.containsKey("INDICE_POD")){
//					indice=1;
//					prop.setProperty("INDICE_POD", String.valueOf(indice));
//				}
//				else{
//					indice=Integer.parseInt(prop.getProperty("INDICE_POD"));
//				}
				//Selezione tipologia
				//Inserimento POD
				R2D_VerificaPodComponent verifichePod = new R2D_VerificaPodComponent(driver);
//				if(prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")).isEmpty()){
//					logger.write("inserisci Pod - Start");
//					 verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD"));
//					logger.write("inserisci Pod - Completed");
//				} else {
					logger.write("inserisci Id_Richiesta - Start");
//					System.out.println(prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
//					verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
					if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
						verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA"));
					} else {
						verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_ORDINE"));
					}
					logger.write("inserisci Id_Richiesta - Completed");
//				}
//				verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD_ELE_"+indice,prop.getProperty("POD_ELE")));
				//Inserimento ID richiesta CRM
//				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA_CRM_ELE_"+indice,prop.getProperty("ID_RICHIESTA_CRM_ELE")));
//				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA"));
				//Cerca
				logger.write("Pressione bottone Cerca - Start");
				verifichePod.cercaPod(verifichePod.buttonCerca);
				logger.write("Pressione bottone Cerca - Completed");
				TimeUnit.SECONDS.sleep(5);
//				// Verificare se la pratica è stata trovata
//				verifichePod.esistePratica();
				//Click Dettaglio Pratiche
				logger.write("Bottone Dettaglio Pratica - Start");
				praticaTrovata = false;
				verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				praticaTrovata = true;
				logger.write("Bottone Dettaglio Pratica - Completed");
				TimeUnit.SECONDS.sleep(5);
				//Click su Pratica
				logger.write("Apri Pratica - Start");
				verifichePod.selezionaPratica(verifichePod.rigaPratica);
				logger.write("Apri Pratica - Completed");
				TimeUnit.SECONDS.sleep(5);
				//Verifica Campi
				//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
				//				campiDaVerificare.add("Esito MUTI;"+"OK");
				//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
				campiDaSalvare.add("Dispacciamento");
				campiDaSalvare.add("Stato Pratica R2D");
				campiDaSalvare.add("Codice Pratica Venditore");
				campiDaSalvare.add("Data Switch / Disdetta");
				campiDaSalvare.add("Data invio recesso"); //new Cla
				campiDaSalvare.add("Codice ROW-ID CRM"); //new Cla		
				campiDaSalvare.add("Distributore");

//				campiDaSalvare.add("Numero Ordine CRM"); //new Cla
				
				//Salvare eventualmente stato indicizzato
				logger.write("Verifiche e Salvataggio campi Pratica - Start");
				verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);
				logger.write("Verifiche e Salvataggio campi Pratica - Completed");
				//Verifico lo stato iniziale della pratica deve essere AW o AA
				logger.write("Verifiche Iniziali Stato Pratica - Start");
				statopratica=prop.getProperty("STATO_R2D");
				if(statopratica.compareToIgnoreCase("CI")==0 || statopratica.compareToIgnoreCase("OK")==0){
					logger.write("La pratica è già in Stato  (CI/OK)");
					} else if(statopratica.compareToIgnoreCase("AW")!=0 && statopratica.compareToIgnoreCase("AA")!=0){
						throw new Exception("Lo stato iniziale della pratica non è corretto:"+statopratica+". Stato pratica atteso AW o AA");
					}
					logger.write("Verifiche Iniziali Stato Pratica - Completed");
				}
			
//			prop.setProperty("STATO_R2D", statopratica);

			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			if(!praticaTrovata) {
//				String messaggio = "Nessuna Pratica Trovata";
//				System.out.println(messaggio);
//				System.exit(1);
			}

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");	
		}
	}
}
