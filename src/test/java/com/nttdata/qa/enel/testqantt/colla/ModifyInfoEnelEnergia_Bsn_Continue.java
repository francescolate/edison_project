package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEESteps_BsnComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyInfoEnelEnergia_Bsn_Continue {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			//System.out.println("Phone data input verification");
			logger.write("Phone data input verification - Start");
			InfoEESteps_BsnComponent steps = new InfoEESteps_BsnComponent(driver);
			steps.clickComponent(steps.consumiSmsChk);
			steps.clickComponent(steps.proseguiBtn);
			Thread.sleep(10000);
			//steps.checkPhoneRequiredFields();
			steps.insertText(steps.phoneField, prop.getProperty("PHONE_NUMBER"));
			steps.insertText(steps.confirmPhoneField, prop.getProperty("PHONE_NUMBER"));
			steps.clickComponent(steps.consumiMailChk);
			steps.clickComponent(steps.proseguiBtn);
			logger.write("Phone data input verification - Completed");
			
			//System.out.println("Mail data input verification");
			logger.write("Mail data input verification - Start");
			Thread.sleep(10000);
			//steps.checkMailRequiredFields();
			steps.insertText(steps.emailField, prop.getProperty("EMAIL"));
			steps.insertText(steps.confirmEmailField, prop.getProperty("EMAIL"));
			steps.clickComponent(steps.proseguiBtn);
			Thread.sleep(10000);
			logger.write("Mail data input verification - Completed");
			
			//System.out.println("Step 2 verification");
			logger.write("Step 2 verification - Start");
			steps.checkStep2Elements();
			logger.write("Step 2 verification - Completed");
			
			//System.out.println("Back/continue verification");
			logger.write("Back/continue verification - Start");
			steps.clickComponent(steps.backBtn);
			steps.checkStep1Elements();
			steps.clickComponent(steps.proseguiBtn);
			steps.checkStep2Elements();
			logger.write("Back/continue verification - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		
	}

}
