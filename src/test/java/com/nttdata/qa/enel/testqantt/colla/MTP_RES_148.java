package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_148 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
											
			home.verifyComponentExistence(home.enelLogo);
								
			home.clickComponent(home.detaglioFurnitura_310519430New);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			
			logger.write("Check For page title-- start");
			
			Thread.currentThread().sleep(5000);
			dc.verifyComponentExistence(dc.pageTitle);
			
			logger.write("Check For page title-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			dc.verifyComponentExistence(dc.serviziPerleforniture);
			
			Thread.currentThread().sleep(5000);
			Thread.sleep(10000);
			
			dc.verifyComponentExistence(dc.modificaPotenzo);
			
			dc.jsClickObject(dc.modificaPotenzo);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			Thread.sleep(10000);
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.ModificaSubText, true);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");
			
			changePV.clickComponent(changePV.radioButton);
			
			changePV.checkButtonIsEnable(changePV.changePowerOrVoltageButton);
		
			changePV.clickComponent(changePV.radioButton1);
			
			logger.write("Check Change Power And Voltage button status -- Start");

			changePV.checkButtonIsEnable(changePV.changePowerOrVoltageButton);

			logger.write("Check Change Power And Voltage button status -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
