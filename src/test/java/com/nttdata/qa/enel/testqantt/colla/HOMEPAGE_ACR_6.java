package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HOMEPAGE_ACR_6 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			HomeComponent home = new HomeComponent(driver);
			
		    prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
		    prop.setProperty("LETUEATTIVITÀ", "Qui troverai tutte le attività per poter gestire le tue forniture e scoprire i vantaggi.");
			//prop.setProperty("FORNITURARIDOTTASOSPESA", "Provvedi subito al pagamento dell’intera morosità indicata nella lettera di diffida e la tua fornitura verrà riattivata");
			//prop.setProperty("BOLLETTETEXT", "Qui potrai visualizzare bollette e/o rate delle tue forniture.");

			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
						
			logger.write("Check for Residential Menu - Start");
			
			home.verifyComponentExistence(home.supplyLabelLuceAttiva);
			
			logger.write("Check for Supply label - Completed");
			
			logger.write("Check for View your bills - Start");

			home.verifyComponentExistence(home.viewYourBillsLuceAttiva);
			
			logger.write("Check for View your bills - Completed");

			logger.write("Check for Supply Details - Start");

			home.verifyComponentExistence(home.supplyDetailsLuceAttiva);
						
			logger.write("Check for Supply Details - Completed");
			
			logger.write("Check for Supply status - Start");

		    prop.setProperty("NUMEROCLIENTE", "728775892");

			home.xpathAttivaStatus = home.xpathAttivaStatus.replace("@numeroCliente@", prop.getProperty("NUMEROCLIENTE"));
			home.checkSupplyStatus(By.xpath(home.xpathAttivaStatus), prop.getProperty("FORNITURA"));
			
			logger.write("Check for Supply status - Completed");

			home.verifyComponentExistence(home.yourActivity);
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			
			log.VerifyText(home.yourActivitySubText, prop.getProperty("LETUEATTIVITÀ"));
			
			logger.write("Check for Field da Pagare - Start");

			home.checkFieldDisplay(home.daPagare);
			
			logger.write("Check for Field da Pagare - Completed");

			prop.setProperty("RETURN_VALUE", "OK");			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
