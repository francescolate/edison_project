package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.components.colla.ModConsensiACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModConsensiACR165 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		ModConsensiACRComponent mod = new ModConsensiACRComponent(driver);
		AccountComponent acc = new AccountComponent(driver);
		
		logger.write("click on Account menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.account);
		mod.comprareText(mod.accountHeading, ModConsensiACRComponent.ACCOUNT_HEADING, true);
		mod.comprareText(mod.accountParagraph, ModConsensiACRComponent.ACCOUNT_PARAGRAPH, true);
		logger.write("click on Account menu item and verify the section heading and paragraph- End");
		
		logger.write("Verify the Consesi section heading and paragraph- started");
		mod.comprareText(mod.consesiHeading, ModConsensiACRComponent.CONSESI_HEADING, true);
		mod.comprareText(mod.consesiParagraph, ModConsensiACRComponent.CONSESI_PARAGRAPH, true);
		logger.write("Verify the Consesi section heading and paragraph- Ends");
		
		logger.write("Click on Modifica Consensi Button and verify the data- Starts");
		mod.clickComponent(mod.modificaConsensiButton);
		mod.comprareText(mod.consensiSectionHeading, ModConsensiACRComponent.CONSENSI_SECTION_HEADING, true);
		logger.write("Click on Modifica Consensi Button and verify the data- Ends");
		
		logger.write("Change the Consenso marketing Enel Energia (from SI to NO or from No to SI) for the fields Email and Telefono Fisso.-Starts");
		ModConsensiACRComponent.list.clear();
		mod.radiobuttonStatus(mod.telefonoFissoConsensi1SIRadio, mod.telefonoFissoConsensi1NORadio, mod.telefonoFissoConsensi1SIRadioLabel,mod.telefonoFissoConsensi1NORadioLabel);
		mod.radiobuttonStatus(mod.telefonoFissoConsensi2SIRadio, mod.telefonoFissoConsensi2NORadio, mod.telefonoFissoConsensi2SIRadioLabel,mod.telefonoFissoConsensi2NORadioLabel);
		mod.radiobuttonStatus(mod.emailConsensi1SIRadio, mod.emailConsensi1NORadio, mod.emailConsensi1SIRadioLabel, mod.emailConsensi1NORadioLabel);
		mod.radiobuttonStatus(mod.emailConsensi2SIRadio, mod.emailConsensi2NORadio, mod.emailConsensi2SIRadioLabel, mod.emailConsensi2NORadioLabel);
		mod.clickComponent(mod.salvaModificheButton);
		mod.comprareText(mod.confirmationHeading, ModConsensiACRComponent.CONFIRMATION_HEADING, true);
		mod.comprareText(mod.confirmationParagraph, ModConsensiACRComponent.CONFIRMATION_PARAGRAPH, true);
		logger.write("Verification of consents for each contact- End ");
		
		
		logger.write("Click on Fine Button and verification of Data- Starts");
		mod.clickComponent(mod.fineButton);
		mod.comprareText(mod.homePageHeading1, ModConsensiACRComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1, ModConsensiACRComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2, ModConsensiACRComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2, ModConsensiACRComponent.HOME_PAGE_PARAGRAPH2, true);
		logger.write("Click on Fine Button and verification of Data- Ends");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
