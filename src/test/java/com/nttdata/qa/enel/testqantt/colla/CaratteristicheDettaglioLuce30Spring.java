package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.Luce30SpringComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CaratteristicheDettaglioLuce30Spring {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Luce30SpringComponent dettaglio =new Luce30SpringComponent(driver);
			
			logger.write("Verifica testo nella sezione Caratteristiche per l'offerta Luce 30 Spring - Start");
			By titolo_Caratteristiche=dettaglio.titoloCaratteristiche;
			dettaglio.verificaTesto(titolo_Caratteristiche, dettaglio.titoloSezioneCaratteristiche);
			
			By lista_Sottotitoli=dettaglio.listaSottotitoli;
			dettaglio.verificaSottotitoli(lista_Sottotitoli);
			
			By lista_Testi=dettaglio.listaTesti;
			dettaglio.verificaTesti(lista_Testi);
			logger.write("Verifica testo nella sezione Caratteristiche per l'offerta Luce 30 Spring - Completed");
			
			logger.write("Verifica testo domande nella sezione Dettagli per l'offerta Luce 30 Spring e relativa risposta ad esse - Start");
			By tab_Dettagli=dettaglio.tabDettagli;
			dettaglio.verifyComponentExistence(tab_Dettagli);
			dettaglio.clickComponent(tab_Dettagli);
			TimeUnit.SECONDS.sleep(2);
			By sezione_Dettagli=dettaglio.sezioneDettagliLabel;
			dettaglio.verifyComponentExistence(sezione_Dettagli);
			
			
			By plus_image=dettaglio.dettagli;
//			dettaglio.checkPlusImageDettagli(plus_image,prop.getProperty("LINK")+dettaglio.linkPLUS_IMAGE);
//			dettaglio.checkQuestionsDetails(plus_image,prop.getProperty("LINK")+dettaglio.linkMINUS_IMAGE);
			dettaglio.checkPlusImageDettagli(plus_image,dettaglio.linkPLUS_IMAGE);
			dettaglio.checkQuestionsDetails(plus_image,dettaglio.linkMINUS_IMAGE);
		
			
			String winHandleBefore = driver.getWindowHandle();
			winHandleBefore = driver.getWindowHandle();
			dettaglio.clickAttualiCondizioni(plus_image);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			dettaglio.checkUrl(prop.getProperty("LINK")+dettaglio.linkPDF_ATTUALI_CONDIZIONI_FORNITURA_GENERALE);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			logger.write("Verifica testo domande nella sezione Dettagli per l'offerta Luce 30 Spring e relativa risposta ad esse - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
