package com.nttdata.qa.enel.testqantt;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CompletaTP8Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CompletaTP8 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			CompletaTP8Component CompletaTP8Component = new CompletaTP8Component(driver);
			logger.write("Compilazione Dati Catastali Proprietario da portale per TP8 - Start");
//			TimeUnit.SECONDS.sleep(60);
			TimeUnit.SECONDS.sleep(6);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			try {
				//Click su Accetta Cookie
				WebElement button=driver.findElement(CompletaTP8Component.buttonAccetta);
				js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", button);
			}catch(Exception e) {}
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath("//body")).sendKeys(Keys.END);
			//CompletaTP8Component.ControllaOps();
			// PROVA NUOVA ISTRUZIONE **************************************
			String frameiframeSF = "iframeSF";
			driver.switchTo().frame(frameiframeSF);
//			util.getFrameActive();
			// FINE PROVA NUOVA ISTRUZIONE *********************************
			WebElement button;
			try {
			logger.write("Selezione Checkbox Proprietario dati da portale per TP8 - Start");
			button=driver.findElement(CompletaTP8Component.checkboxProprietario);
			js.executeScript("arguments[0].click();", button);
			}catch(Exception e) { 
				  logger.write("Bottone CheckBox Proprietario non trovato!");
				  System.out.println("Bottone CheckBox Proprietario non trovato!");
			}
			logger.write("Selezione Checkbox Trattamenti dati da portale per TP8 - Completed");
			CompletaTP8Component.inserisciValore(CompletaTP8Component.comuneAmministrativo, prop.getProperty("COMUNE_AMMINISTRATIVO", ""));			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.comuneCatastale, prop.getProperty("COMUNE_CATASTALE", ""));			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.codice_comune_catastale, prop.getProperty("CODICE_COMUNE_CATASTALE", ""));			
//			Seleziona il TIPO_UNITA			
			String valoreMenu = prop.getProperty("TIPO_UNITA", "");
			button=driver.findElement(CompletaTP8Component.menuTipoUnita);
			js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", button);
			Thread.currentThread().sleep(2000);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_DOWN);        
			Thread.currentThread().sleep(2000);
			r.keyRelease(KeyEvent.VK_DOWN);
			Thread.currentThread().sleep(2000);

			r.keyPress(KeyEvent.VK_ENTER);       
			Thread.currentThread().sleep(2000);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.currentThread().sleep(2000);
			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.sezione, prop.getProperty("SEZIONE", ""));			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.foglio, prop.getProperty("FOGLIO", ""));			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.subalterno, prop.getProperty("SUBALTERNO", ""));			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.particella, prop.getProperty("PARTICELLA", ""));			
			CompletaTP8Component.inserisciValore(CompletaTP8Component.estensione_particella, prop.getProperty("ESTENSIONE_PARTICELLA", ""));			
//			Seleziona il TIPO_PARTICELLA			
			valoreMenu = prop.getProperty("TIPO_PARTICELLA", "");
			button=driver.findElement(CompletaTP8Component.menuTipoParticella);
			js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", button);
			Thread.currentThread().sleep(2000);
			Robot r1 = new Robot();
			r1.keyPress(KeyEvent.VK_DOWN);        
			Thread.currentThread().sleep(2000);
			r1.keyPress(KeyEvent.VK_DOWN);        
			Thread.currentThread().sleep(2000);
			r1.keyRelease(KeyEvent.VK_DOWN);
			Thread.currentThread().sleep(2000);
			r1.keyPress(KeyEvent.VK_ENTER);       
			Thread.currentThread().sleep(2000);
			r1.keyRelease(KeyEvent.VK_ENTER);
			Thread.currentThread().sleep(2000);
			
			//Click su completa
			button=driver.findElement(CompletaTP8Component.prosegui);
			js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", button);
			TimeUnit.SECONDS.sleep(30);
			driver.switchTo().defaultContent();
			//Click su bottone SI
			button=driver.findElement(CompletaTP8Component.si);
			js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", button);
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(30);
			
			//Click su bottone CONFERMA FINALE
			driver.findElement(By.xpath("//body")).sendKeys(Keys.END);
			driver.switchTo().frame(frameiframeSF);
//			util.getFrameActive();
			try {
			WebElement btnConferma=driver.findElement(CompletaTP8Component.conferma);
			}catch(Exception e) {}

			WebElement btnConferma=driver.findElement(CompletaTP8Component.conferma);
			js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", btnConferma);
			TimeUnit.SECONDS.sleep(30);
			driver.switchTo().defaultContent();

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}

