package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GDPRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class GDPR_ACB_ID_182 {

	public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		GDPRComponent gdpr = new GDPRComponent(driver);
		String FAQPageURL = prop.getProperty("WP_LINK")+"it/supporto/faq/privacy";
		
		logger.write("Verification of Home Page Data  - Start ");
		gdpr.comprareText(gdpr.homePagePath1, GDPRComponent.HOME_PAGE_PATH1, true);
		gdpr.comprareText(gdpr.homePagePath2, GDPRComponent.HOME_PAGE_PATH2, true);
		gdpr.comprareText(gdpr.homePageHeading, GDPRComponent.HOME_PAGE_HEADING, true);
		logger.write("Verification of Home Page Data  - End ");
		
		logger.write("Clicking on I Tuoi Dirtti and Verification of Data  - Start ");
		gdpr.clickComponent(gdpr.iTuoiDirtti);
		gdpr.comprareText(gdpr.iTuoiDirttiPath1, GDPRComponent.PATH1_ITUOIDIRTTI, true);
		gdpr.comprareText(gdpr.iTuoiDirttiPath2, GDPRComponent.PATH2_ITUOIDIRTTI, true);
		logger.write("Clicking on I Tuoi Dirtti and Verification of Data  - End ");
		
		logger.write(" Verification of Data Anagrafici details- Start ");
		gdpr.comprareText(gdpr.datiAnagraficiHeading, GDPRComponent.DATA_ANAGRAFICI_HEADING, true);
		/*gdpr.verifyComponentExistence(gdpr.titolare);
		gdpr.verifyComponentExistence(gdpr.codiFiscale);
		gdpr.verifyComponentExistence(gdpr.email);*/
		logger.write(" Verification of Data Anagrafici details- End ");
		
		logger.write(" Verification of Diritto Di Informazione details by clicking + symbol - Start ");
		gdpr.comprareText(gdpr.dirittoDiInformazione, GDPRComponent.DIRITTO_DI_INFORMAZIONE, true);
		gdpr.clickComponent(gdpr.collapseIconDirittoDiInformazione);
		gdpr.comprareText(gdpr.dirittiDiInformazioneText, GDPRComponent.DIRITTO_DI_INFORMAZIONE_TEXT, true);
		gdpr.clickComponent(gdpr.collapseIconDirittoDiInformazione);
		gdpr.isElementNotPresent(gdpr.dirittiDiInformazioneText);
		logger.write(" Verification of Diritto Di Informazione details by clicking + symbol - End ");
		
		logger.write(" Verification of page navigation after clicking clicca qui link- Start ");
		gdpr.clickComponent(gdpr.collapseIconDirittoDiInformazione);
		gdpr.clickComponent(gdpr.cliccaQuiDirittiDiInformazioneText);
		gdpr.checkURLAfterRedirection(FAQPageURL);
		logger.write(" Verification of page navigation after clicking clicca qui link- End ");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
