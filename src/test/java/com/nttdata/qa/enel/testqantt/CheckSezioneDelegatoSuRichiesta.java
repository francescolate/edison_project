package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DelegatoComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.VerificaDocumentoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;



public class CheckSezioneDelegatoSuRichiesta {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			VerificaRichiestaComponent contratto = new VerificaRichiestaComponent(driver);
			
			logger.write("verifica che nella richiesta nel tab dettagli sia presente la sezione delegato con i dati relativi al cliente delegato scelto in fase di identificazione interlocutore - Start ");
			contratto.pressJavascript(contratto.richiesta);
			TimeUnit.SECONDS.sleep(15);
			VerificaDocumentoComponent doc=new VerificaDocumentoComponent(driver);
		
			doc.clickComponentWithJse(doc.tabDettagli);
			TimeUnit.SECONDS.sleep(10);
			DelegatoComponent delegato=new DelegatoComponent(driver);
			delegato.checkSezioneDelegato(prop.getProperty("NOME_DELEGATO"), prop.getProperty("COGNOME_DELEGATO"), prop.getProperty("CF_DELEGATO"), prop.getProperty("NUMERO_DOCUMENTO"));
			logger.write("verifica che nella richiesta nel tab dettagli sia presente la sezione delegato con i dati relativi al cliente delegato scelto in fase di identificazione interlocutore - Completed ");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
