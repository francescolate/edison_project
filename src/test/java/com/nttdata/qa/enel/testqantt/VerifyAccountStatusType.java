package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerifyAccountStatusType {

	@Step("Validate Account Status Type")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			PubblicoID61ProcessoAResSwaEleComponent prs = new PubblicoID61ProcessoAResSwaEleComponent(driver);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.get_AccountStatus_171);
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");

			prs.VerifyText(prs.Status, PubblicoID61ProcessoAResSwaEleComponent.Status_ColumnHeading);
			prs.VerifyText(prs.Status_Value, PubblicoID61ProcessoAResSwaEleComponent.Status_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_Process__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_Process__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_Process__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_Process__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_ProdName__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_ProdName__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_ProdName__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_ProdName__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_AccountFirstName__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_AccountFirstName__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_AccountFirstName__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_AccountFirstName__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_AccountLastName__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_AccountLastName__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_AccountLastName__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_AccountLastName__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_CommunicationEmailAddress__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_CommunicationEmailAddress__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_CommunicationEmailAddress__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_CommunicationEmailAddress__c_ColumnValue);
			
		
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.get_AccountType_171);
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			
			prs.VerifyText(prs.ITA_IFM_AccountType__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_AccountType__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_AccountType__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_AccountType__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_Commodity__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_Commodity__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_Commodity__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_Commodity__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_FiscalCode__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_FiscalCode__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_FiscalCode__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_FiscalCode__c_ColumnValue);
			
			prs.VerifyText(prs.ITA_IFM_NewOrderItem_Status__c, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_NewOrderItem_Status__c_ColumnHeading);
			prs.VerifyText(prs.ITA_IFM_NewOrderItem_Status__c_Value, PubblicoID61ProcessoAResSwaEleComponent.ITA_IFM_NewOrderItem_Status__c_ColumnValue);
			
			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
