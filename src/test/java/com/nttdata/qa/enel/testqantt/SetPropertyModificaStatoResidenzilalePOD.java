package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class SetPropertyModificaStatoResidenzilalePOD {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			

			

					String nomeScenario=args[0].replace(".properties","" );
					logger.write("Start Settaggio CF e POD per "+nomeScenario);
					logger.write("SetPropertyModificaStatoResidenzilalePOD ");



					String queryListaClienti="";
					String secondaQuery="";
					String getPOdCf="";



					switch(nomeScenario) 
					{
					//Cliente Residenziale, con solo una fornitura ELE attiva con Uso Abitativo e Flag Residente a NO, su distributore configurato a portale e senza SR in corso sul cliente
					case "CRM_T_REVAMPING_Modifica_Stato_Residente_12": 
						
						//La prima query serve a recuperare un cliente con solo un pod attivo, non ci sono condizioni su fornitore o stato residenza
						queryListaClienti=
								"SELECT Account.NE__Fiscal_code__c Codice_Fiscale, count(id) N_Siti\r\n"
								+ "FROM Asset\r\n"
								+ "WHERE RecordType.Name = 'Commodity'\r\n"
								+ "and CreatedDate > 2020-01-01T00:00:00Z\r\n"
								+ "and NE__Status__c = 'Active'\r\n"
								+ "and ITA_IFM_Commodity__c = 'ELE'\r\n"
								+ "and Account.NE__Fiscal_code__c !=''\r\n"
								+ "and Account.RecordType.Name = 'Residenziale'\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\r\n"
								+ "and Account.NE__Fiscal_code__c not in ('BRDMRT80L43A471J','LTTCLT72C43A704T')\r\n"
								+ "GROUP BY Account.NE__Fiscal_code__c\r\n"
								+ "HAVING count(Id)=1\r\n"
								+ "LIMIT 100";
						
						//La seconda query utilizza i risultati della prima query per trovare un cliente con 1 solo pod attivo che sia con stato residenza a false e fotnitura acea
						secondaQuery=
								"SELECT  Account.NE__Fiscal_code__c , Account.NE__E_mail__c, Account.ITA_IFM_HolderContactEmail__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n"
								+ "FROM Asset\r\n"
								+ "WHERE RecordType.Name = 'Commodity'\r\n"
								+ "and NE__Status__c = 'Active'\r\n"
								+ "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\r\n"
								+ "and ITA_IFM_Commodity__c = 'ELE'\r\n"
								+ "and Account.NE__Fiscal_code__c !=''\r\n"
								+ "and Account.RecordType.Name = 'Residenziale'\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\r\n"
								+ "and NE__Service_Point__r.ITA_IFM_FlagResidence__c = false \r\n"
								+ "AND  Account.NE__Fiscal_code__c IN(@ListaCfConUnaSolaFornitura@)";
						

						break;
					case "CRM_T_REVAMPING_Modifica_Stato_Residente_13":
						//La prima query serve a recuperare un cliente con solo un pod attivo, non ci sono condizioni su fornitore o stato residenza
						queryListaClienti=
								"SELECT  Account.NE__Fiscal_code__c Codice_Fiscale,count(id) N_Siti \r\n"
								+ "FROM Asset\r\n"
								+ "WHERE RecordType.Name = 'Commodity'\r\n"
								+ "and CreatedDate > 2020-01-01T00:00:00Z\r\n"
								+ "and NE__Status__c = 'Active'\r\n"
								+ "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\r\n"
								+ "and ITA_IFM_Commodity__c = 'ELE'\r\n"
								+ "and Account.NE__Fiscal_code__c !=''\r\n"
								+ "and Account.RecordType.Name = 'Residenziale'\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\r\n"
								+ "GROUP BY Account.NE__Fiscal_code__c\r\n"
								+ "HAVING count(Id)=1\r\n"
								+ "LIMIT 100\r\n";
						
						//La seconda query utilizza i risultati della prima query per trovare un cliente con 1 solo pod attivo che sia con stato residenza a true e fotnitura acea
						secondaQuery=
								" SELECT Account.Id, Account.NE__Fiscal_code__c, Account.NE__E_mail__c, Account.ITA_IFM_HolderContactEmail__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n"
								+ "FROM Asset\r\n"
								+ "WHERE RecordType.Name = 'Commodity'\r\n"
								+ "and NE__Status__c = 'Active'\r\n"
								+ "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\r\n"
								+ "and ITA_IFM_Commodity__c = 'ELE'\r\n"
								+ "and Account.NE__Fiscal_code__c !=''\r\n"
								+ "and Account.RecordType.Name = 'Residenziale'\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\r\n"
								+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\r\n"
								+ "and NE__Service_Point__r.ITA_IFM_FlagResidence__c = TRUE\r\n"
								+ "AND Account.NE__Fiscal_code__c IN(@ListaCfConUnaSolaFornitura@)";
					
						break;
					case "CRM_T_REVAMPING_Modifica_Stato_Residente_14":
						queryListaClienti=
								"SELECT Account.NE__Fiscal_code__c Codice_Fiscale, count(id) N_Siti\r\n" + 
							    		"FROM Asset\r\n" + 
							    		"WHERE CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
							    		"AND NE__Status__c = 'Active'\r\n" + 
							    		"AND RecordType.Name = 'Commodity'\r\n" + 
							    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
							    		"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
							    		"AND ITA_IFM_Sap_Contractual_Account__c != null\r\n" + 
							    		"AND Account.RecordType.Name = 'Residenziale'\r\n" + 
							    		"AND AccountId IN (SELECT NE__Account__c FROM NE__Service_Point__c WHERE ITA_IFM_Commodity_Use__c = 'Uso Abitativo' "+
							    		"and ITA_IFM_FlagResidence__c = true)\r\n" + 
							    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n" + 
							    		"AND Account.NE__Fiscal_code__c NOT IN ('FRNGLI99A70A354Y') "+
							    		"GROUP BY Account.NE__Fiscal_code__c\r\n" + 
							    		"HAVING count(Id) > 1\r\n" + 
							    		"LIMIT 100";

						secondaQuery=
								"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.NE__E_mail__c, Account.ITA_IFM_HolderContactEmail__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\n" +
							    		"FROM Asset\r\n" + 
							    		"WHERE CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
							    		"AND NE__Status__c = 'Active'\r\n" + 
							    		"AND RecordType.Name = 'Commodity'\r\n" + 
							    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
							    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\r\n" + 
							    		"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
							    		"AND ITA_IFM_Sap_Contractual_Account__c != null\r\n" + 
							    		"AND Account.RecordType.Name = 'Residenziale'\r\n" + 
							    		"AND AccountId IN (SELECT NE__Account__c FROM NE__Service_Point__c WHERE ITA_IFM_Commodity_Use__c = 'Uso Abitativo' and ITA_IFM_FlagResidence__c = true)\r\n" + 
							    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n" + 
							    		"AND Account.NE__Fiscal_code__c NOT IN ('FRNGLI99A70A354Y') "+
							    		"AND Account.NE__Fiscal_code__c  IN(@ListaCfConUnaSolaFornitura@)";


						break;
					case "CRM_T_REVAMPING_Modifica_Stato_Residente_15":
						//query recuperata da msr_id4 in costanti
						queryListaClienti=
					    		"SELECT Account.NE__Fiscal_code__c Codice_Fiscale,count(id) N_Siti \r\n" +
					                    "FROM Asset\n" +
					                    "WHERE RecordType.Name = 'Commodity'\n" +
					                    "and NE__Status__c = 'Active'\n" +
					                    "and ITA_IFM_Commodity__c = 'ELE'\n" +
					                    "and Account.RecordType.Name = 'Residenziale'\n" +
					                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\n" +
					                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\n" +
					                    "GROUP BY Account.NE__Fiscal_code__c\r\n"+
										"HAVING count(Id)>1\r\n"+
										"LIMIT 100\r\n";

						secondaQuery=
								"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.NE__E_mail__c, Account.ITA_IFM_HolderContactEmail__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\n" +
					                    "FROM Asset\n" +
					                    "WHERE RecordType.Name = 'Commodity'\n" +
					                    "and NE__Status__c = 'Active'\n" +
					                    "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\n" +
					                    "and ITA_IFM_Commodity__c = 'ELE'\n" +
					                    "and Account.NE__Fiscal_code__c !=''\n" +
					                    "and Account.RecordType.Name = 'Residenziale'\n" +
					                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\n" +
					                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\n" +
					                    "and NE__Service_Point__r.ITA_IFM_FlagResidence__c = false\n" +
				                    	"AND Account.NE__Fiscal_code__c  IN(@ListaCfConUnaSolaFornitura@)";

						break;
					default:
						throw new Exception("Nessun nome scenario corrispondente");


					}
					/*
                prop.setProperty("CODICE_FISCALE","GNNLGU65A01F205I" );
                prop.setProperty("POD", "IT012E01098881");
					 */



					WorkbenchQuery wb=new WorkbenchQuery();
					wb.Login(args[0]);



					Map<Integer,Map> clientiConUnaSolaFornitura=wb.EseguiQuery(queryListaClienti);

					//GENERO IN PER DA INSERIRE IN SECONDA QUERY
					logger.write("Generazione In Condition");
					String inCondition=WorkbenchQuery.GeneraInClause(clientiConUnaSolaFornitura,"Codice_Fiscale");


					clientiConUnaSolaFornitura=wb.EseguiQuery(secondaQuery.replace("@ListaCfConUnaSolaFornitura@", inCondition));

					String cf=clientiConUnaSolaFornitura.get(1).get("Account.NE__Fiscal_code__c").toString();
					String Pod=clientiConUnaSolaFornitura.get(1).get("NE__Service_Point__c.ITA_IFM_POD__c").toString();



					prop.setProperty("CODICE_FISCALE", cf);
					prop.setProperty("POD",Pod );
					logger.write("CODICE_FISCALE: "+cf);
					logger.write("POD: "+Pod);
					logger.write("Fine Settaggio CF e POD per "+nomeScenario);


				


			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
	
	
	public static String GeneraInClause(Map<Integer,Map> rows, String colName) throws Exception
	{
		//Da utilizzare dopo aver chiamato il metodo RecuperaTuttiIRisultati
		if(rows.isEmpty()) 	throw new Exception("Nessun record da estrarre");

		String inCondition="";
		//GENERO IN PER DA INSERIRE IN SECONDA QUERY
		for(Integer r :rows.keySet())
		{
			if(!rows.get(r).containsKey(colName)) throw new Exception("Impossibile trovare la colonna "+colName);
			inCondition+=(inCondition.equals("")?"":",")+"'"+rows.get(r).get(colName)+"'";
		}


		return inCondition;
	}
	

}
