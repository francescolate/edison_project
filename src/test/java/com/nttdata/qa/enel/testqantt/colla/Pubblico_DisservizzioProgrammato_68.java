package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_DisservizzioProgrammato_68 {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ImpressComponent ic = new ImpressComponent(driver);
			
			logger.write("Accessing Disservizio page - Start");
			ic.launchLink(prop.getProperty("LINK"));
			ic.verifyComponentExistence(ic.logoEnel);
			ic.verifyComponentExistence(ic.buttonAccetta);
			ic.clickComponent(ic.buttonAccetta);
			logger.write("Accessing Disservizio page - Complete");
			
			ic.verifyComponentExistence(ic.buttonAccept);
			ic.clickComponent(ic.buttonAccept);
			
			logger.write("Verify the Disservizio page contents - Start");
			ic.verifyComponentExistence(ic.disservizioContent1);
			ic.comprareText(ic.disservizioContent1, ImpressComponent.DISSERVIZIO_VALUE, true);
			ic.verifyComponentExistence(ic.disservizioContent2);
			ic.comprareText(ic.disservizioContent2, ImpressComponent.DISSERVIZIO_VALUE1, true);
			logger.write("Verify the Disservizio page contents - Start");

			logger.write("Verify the Disservizio page header tab and label - Start");
			ic.verifyHeaderValue();
			logger.write("Verify the Disservizio page header and label - Complete");

			logger.write("Verify the Disservizio page footer and label - Start");
			ic.verifyFooterValue();
			logger.write("Verify the Disservizio page footer and label - Complete");

			logger.write("Click on Home link - Start");
			ic.verifyComponentExistence(ic.HomeLink);
			ic.clickComponent(ic.HomeLink);
			logger.write("Click on Home link - Complete");

			logger.write("Verify the header values - Start");
			ic.verifyHeaderValue();
			logger.write("Verify the header values - Complete");

			logger.write("Verify the first list question - Start");
			ic.verifyComponentExistence(ic.HomePageQues1);
			ic.comprareText(ic.HomePageQues1, ImpressComponent.HOME_QUES1, true);
			logger.write("Verify the first list question - Complete");

			logger.write("Verify the second list question - Start");
			ic.verifyComponentExistence(ic.homePageQues2);
			ic.comprareText(ic.homePageQues2, ImpressComponent.HOME_QUES2, true);
			logger.write("Verify the second list question - Complete");

			logger.write("Verify the third list question - Start");
			ic.verifyComponentExistence(ic.homePageQues3);
			ic.comprareText(ic.homePageQues3, ImpressComponent.HOME_QUES3, true);
			logger.write("Verify the third list question - Complete");
			
					
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
