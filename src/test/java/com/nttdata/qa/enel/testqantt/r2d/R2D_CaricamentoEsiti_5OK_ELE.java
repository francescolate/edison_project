package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_5OK_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {
			
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Menu Interrogazione
				menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
				R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
				//Selezione tipologia Caricamento
				caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
				//Inserisci Pod
				if(prop.getProperty("SKIP_POD","N").equals("N")){
					caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD_ELE",prop.getProperty("POD")));
					//Inserisci ID Richiesta CRM
				}
				caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA",prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA"))));
				//Cerca
				caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
				//Click button Azione
				caricamentoEsiti.selezionaTastoAzionePratica();
				//Verifica Stato pratica atteso
				caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "CI");
				//Selezione esito 5OK
				caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_5OK_ELE"));
				//Inserimento esito
				caricamentoEsiti.selezioneEsito("OK");
				//Calcolo sysdate
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
//				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String data = simpleDateFormat.format(calendar.getTime()).toString();
				String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE");
				calendar.add(Calendar.MONTH, 1);
				String dataScadenza = simpleDateFormat.format(calendar.getTime()).toString();
				calendar.add(Calendar.MONTH, -1); // ripristino data odierna

				
				//Inserimento dettaglio esito 5OK in base alla tipologia processo
				//if (prop.getProperty("PRIMA_ATTIVAZIONE","N").equals("Y"))
				if (tipoOperazione.equals("PRIMA_ATTIVAZIONE_EVO")){
					//attribuzione opzione tariffaria
					String opzionetariffaria="";
					if (prop.getProperty("USO_FORNITURA").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="Tariffa D3_2008 (Con Potenza)";
					if(prop.getProperty("USO_FORNITURA").compareToIgnoreCase("Uso Diverso da Abitazione")==0)
						opzionetariffaria="Tariffa B1_2008 (Cottimo)";
					caricamentoEsiti.inserimentoDettaglioEsito5OKPrimaAttivazioneELE(prop.getProperty("OI_RICERCA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),opzionetariffaria,prop.getProperty("TENSIONE_CONSEGNA"),prop.getProperty("POTENZA_CONTRATTUALE"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("POTENZA_FRANCHIGIA"));
				}
				//if (prop.getProperty("VOLTURA_SENZA_ACCOLLO","N").equals("Y")){
				if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKVolturaSenzaAccolloELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"));
				}
				
				//if (prop.getProperty("SUBENTRO_R2D","N").equals("Y")){
				if (tipoOperazione.equals("SUBENTRO_R2D")){
					//attribuzione opzione tariffaria
					String opzionetariffaria="";
					if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="ETAD3M00A1";
					if(prop.getProperty("USO").compareToIgnoreCase("Uso Diverso da Abitazione")==0)
						opzionetariffaria="ETAB1C00C1";
					caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),opzionetariffaria,prop.getProperty("VOLTAGGIO"),prop.getProperty("POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("FRANCHIGIA"));
				}
				
				if (tipoOperazione.equals("SUBENTRO_EVO")){
					//attribuzione opzione tariffaria
					String opzionetariffaria = "";
					if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo") == 0) {
						opzionetariffaria = "Tariffa D3_2008 (Con Potenza)";
						caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroELE4(prop.getProperty("OI_RICHIESTA"),
								data, prop.getProperty("CODICE_CAUSALE_5OK_ELE"), opzionetariffaria,
								prop.getProperty("VOLTAGGIO"), prop.getProperty("POTENZA"),
								prop.getProperty("TIPO_CONTATORE"), prop.getProperty("POTENZA_IMPEGNATA"));
					} else if (prop.getProperty("USO").compareToIgnoreCase("Uso Diverso da Abitazione") == 0) {
						opzionetariffaria = "Tariffa B1_2008 (Cottimo)";
						caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroELE4(prop.getProperty("OI_RICHIESTA"),
								data, prop.getProperty("CODICE_CAUSALE_5OK_ELE"), opzionetariffaria,
								prop.getProperty("VOLTAGGIO"), prop.getProperty("POTENZA"),
								prop.getProperty("TIPO_CONTATORE"), prop.getProperty("POTENZA_IMPEGNATA"));
					} else if (prop.getProperty("USO").compareToIgnoreCase("Illuminazione Pubblica") == 0) {
						opzionetariffaria = "Tariffa BTIP_2008";
						caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroELE4(prop.getProperty("OI_RICHIESTA"),
								data, prop.getProperty("CODICE_CAUSALE_5OK_ELE"), opzionetariffaria,
								prop.getProperty("VOLTAGGIO"), prop.getProperty("POTENZA"),
								prop.getProperty("TIPO_CONTATORE"), prop.getProperty("POTENZA_IMPEGNATA"));
					}
				}
				
				if (tipoOperazione.equals("SUBENTRO_EVO_DP_10")){
					//attribuzione opzione tariffaria
					String opzionetariffaria="";
					if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="Tariffa D3_2008 (Con Potenza)";
					if(prop.getProperty("USO").compareToIgnoreCase("Uso Diverso da Abitazione")==0)
						opzionetariffaria="Tariffa B1_2008 (Cottimo)";
					caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroELE3(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),opzionetariffaria,prop.getProperty("VOLTAGGIO"),prop.getProperty("POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("FRANCHIGIA"));
				}
				//if (prop.getProperty("ALLACCIO","N").equals("Y")){
				if (tipoOperazione.equals("ALLACCIO_EVO")){
					String opzionetariffaria="";
					if (prop.getProperty("USO_FORNITURA").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="Tariffa D3_2008 (Con Potenza)";
					if(prop.getProperty("USO_FORNITURA").compareToIgnoreCase("Uso Diverso da Abitazione")==0)
						opzionetariffaria="Tariffa B1_2008 (Cottimo)";
					String tipoCliente=prop.getProperty("TIPO_CLIENTE");
					String cf_pi=prop.getProperty("CODICE_FISCALE");
//					caricamentoEsiti.inserimentoDettaglioEsito5OKAllaccioELE(prop.getProperty("OI_ORDINE"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),tipoCliente,cf_pi,prop.getProperty("POD"),prop.getProperty("VOLTAGGIO"),prop.getProperty("POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("FRANCHIGIA"),opzionetariffaria);
					caricamentoEsiti.inserimentoDettaglioEsito5OKAllaccioELE(prop.getProperty("OI_ORDINE"), data,tipoCliente,prop.getProperty("POD"),prop.getProperty("TENSIONE"),prop.getProperty("POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("POTENZA_FRANCHIGIA"),opzionetariffaria);
				}
				//if (prop.getProperty("SUBENTRO_COMBINATO_R2D","N").equals("Y")){
//				if (tipoOperazione.equals("SUBENTRO_COMBINATO_R2D")){
				if (tipoOperazione.equals("SUBENTRO_COMBINATO")){
					String opzionetariffaria="";
					if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="ETAD3M00A1";
					else
						opzionetariffaria="ETAB1C00C1";
					String cf_pi=prop.getProperty("CODICE_FISCALE");
					String classificazionePreventivo=prop.getProperty("CLASSIFICAZIONE_PREVENTIVO");
					caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroCombinatoELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),cf_pi,prop.getProperty("VOLTAGGIO"),prop.getProperty("NUOVA_POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("FRANCHIGIA"),opzionetariffaria,classificazionePreventivo,prop.getProperty("POD_ELE"));
				}
				//if (prop.getProperty("VOLTURA_CON_ACCOLLO","N").equals("Y")){
				if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")){	
					caricamentoEsiti.inserimentoDettaglioEsito5OKVolturaConAccolloELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"));
				}
				//if (prop.getProperty("MODIFICA_POTENZA_TENSIONE","N").equals("Y")){
				if (tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")){
					String opzionetariffaria="";
					if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="ETAD3M00A1";
					else
						opzionetariffaria="ETAB1C00C1";
					String cf_pi=prop.getProperty("CODICE_FISCALE");
					String classificazionePreventivo=prop.getProperty("CLASSIFICAZIONE_PREVENTIVO");
					if(classificazionePreventivo.compareTo("PREDETERMINABILE")==0){
					caricamentoEsiti.inserimentoDettaglioEsito5OKModificaPotenzaTensionELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),prop.getProperty("VOLTAGGIO"),prop.getProperty("POTENZA"),opzionetariffaria);
					}
					if(classificazionePreventivo.compareTo("NON PREDETERMINABILE")==0){
						caricamentoEsiti.inserimentoDettaglioEsito5OKSubentroCombinatoELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),cf_pi,prop.getProperty("VOLTAGGIO"),prop.getProperty("POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("FRANCHIGIA"),opzionetariffaria,classificazionePreventivo,prop.getProperty("POD_ELE"));
					}
				}
				//if (prop.getProperty("VARIAZIONE_INDIRIZZO_FORNITURA","N").equals("Y")){
				if (tipoOperazione.equals("VARIAZIONE_INDIRIZZO_FORNITURA")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKVariazioneIndirizzoFornituraELE(prop.getProperty("OI_RICHIESTA"),data);
				}
				//if (prop.getProperty("RETTIFICA_MERCATO","N").equals("Y")){
				if (tipoOperazione.equals("RETTIFICA_MERCATO")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKVariazioneIndirizzoFornituraELE(prop.getProperty("OI_RICHIESTA"),data);
				}
				
				//if (prop.getProperty("MOGE","N").equals("Y")){
				if (tipoOperazione.equals("MOGE")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKMogeAnagraficaELE(prop.getProperty("OI_RICHIESTA"),data);
				}
				
				if (tipoOperazione.equals("CAMBIO_USO")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKCambioUsoEle(prop.getProperty("OI_RICHIESTA"),data,"CAMBIOUSO");
				}
				//if (prop.getProperty("MODIFICA_STATO_RESIDENTE","N").equals("Y")){
				if (tipoOperazione.equals("MODIFICA_STATO_RESIDENTE")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKVariazioneIndirizzoFornituraELE(prop.getProperty("OI_RICHIESTA",prop.getProperty("OI_ORDINE")),data);
				}
				//if (prop.getProperty("VARIAZIONE_INDIRIZZO_FATTURAZIONE","N").equals("Y")){
				if (tipoOperazione.equals("VARIAZIONE_INDIRIZZO_FATTURAZIONE")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKVariazioneIndirizzoFornituraELE(prop.getProperty("OI_RICHIESTA"),data);
				}
				//if (prop.getProperty("VERIFICA_CONTATORE","N").equals("Y")){
				if (tipoOperazione.equals("VERIFICA_CONTATORE")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKVerificaContatoreELE(prop.getProperty("OI_RICHIESTA"), data, prop.getProperty("TIPOLOGIA_ESITO"), prop.getProperty("FILENAME"), prop.getProperty("TIPOLOGIA_DOCUMENTO"));
				}
				
				if (tipoOperazione.equals("DISDETTA_CON_SUGGELLO")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKDisdettaconsuggello(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"));
				}
				//if (prop.getProperty("SPOSTAMENTO_CONTATORE_ENTRO_10M","N").equals("Y")){
				if (tipoOperazione.equals("SPOSTAMENTO_CONTATORE_ENTRO_10M")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKSpostamentoContatoreEntro10MELE(prop.getProperty("OI_RICHIESTA"),data);
				}
				//if (prop.getProperty("SPOSTAMENTO_CONTATORE_OLTRE_10M","N").equals("Y")){
				if (tipoOperazione.equals("SPOSTAMENTO_CONTATORE_OLTRE_10M")){
					String opzionetariffaria="";
					if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo")==0)
						opzionetariffaria="ETAD3M00A1";
					else
						opzionetariffaria="ETAB1C00C1";
					String cf_pi=prop.getProperty("CODICE_FISCALE");
					caricamentoEsiti.inserimentoDettaglioEsito5OKSpostamentoContatoreOltre10MELE(prop.getProperty("OI_RICHIESTA"), data,prop.getProperty("CODICE_CAUSALE_5OK_ELE"),cf_pi,prop.getProperty("VOLTAGGIO"),prop.getProperty("NUOVA_POTENZA"),prop.getProperty("TIPO_CONTATORE"),prop.getProperty("FRANCHIGIA"),opzionetariffaria);
				}

				if (tipoOperazione.equals("PREDISPOSIZIONE_PRESA")){
					if (!prop.getProperty("TIPO_LAVORAZIONE_CRM_ELE","N").equals("REL N01 Allaccio")) {
							caricamentoEsiti.inserimentoDettaglioEsitoPreventivoPPELE(prop.getProperty("OI_RICERCA"),data, 
							prop.getProperty("IMPORTO_PREVENTIVO"), dataScadenza, 
							prop.getProperty("CODICE_PREVENTIVO"), prop.getProperty("TIPO_CARICAMENTO"),
							prop.getProperty("LIVELLO_TENSIONE"), prop.getProperty("TEMPO_MAX_LAVORI"),
							prop.getProperty("REFERENTE"), prop.getProperty("TEL_REFERENTE"),
							prop.getProperty("VOCE_DI_COSTO"));
							logger.write("Caricamento Preventivo - Richiesta N01 Allaccio");
					} else {
						caricamentoEsiti.inserimentoDettaglioEsito5OKPPELE(prop.getProperty("OI_RICERCA"), 
								data, prop.getProperty("TENSIONE_RICHIESTA"),
								prop.getProperty("POTENZA_RICHIESTA"), prop.getProperty("POTENZA_FRANCHIGIA"));
						logger.write("Caricamento 5 OK - Richiesta N01 Allaccio");
					}
				}

				//Salvataggio stato attuale pratica
				prop.setProperty("STATO_R2D", "OK");
				logger.write("Stato Pratica = 'OK'");

			}
			//
			prop.setProperty("RETURN_VALUE", "OK");
		}catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}	
	}
}
