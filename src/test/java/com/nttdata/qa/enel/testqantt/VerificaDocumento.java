package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.VerificaDocumentoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class VerificaDocumento {
    @Step("Verifica Documenti")
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);
        RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try {
            System.out.println("entra in verifica documento");
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            VerificaDocumentoComponent doc = new VerificaDocumentoComponent(driver);
               
            doc.clickComponentIfExist(doc.tabCorrelato);
            
            js.executeScript("window.scrollBy(0,2000)");
            
            doc.caricaSezioneDocumenti();

            logger.write("click su 'visualizza tutto' nella sezione documenti e verifica che in tabella siano presenti nella colonna Modello le seguenti voci: Plico, Oggetto generico EMAIL, ecc - Start");
            doc.verificaSezioneDocumentiNonVuota(doc.sezioneDocumenti, "0");
            doc.scrollComponent(doc.sezioneDocumenti);
            doc.clickComponentWithJse(doc.linkVisualizzaTutto);
            gestione.checkSpinnersSFDC();
            
            if (prop.getProperty("PROCESSO").contentEquals("Avvio Subentro EVO")) {
                doc.verificaValoreInTabella("Plico");
                doc.verificaValoreInTabella("ML_KitStdLettSubentro");
                if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y") && prop.getProperty("MERCATO").contentEquals("Salvaguardia")) {
                   if (doc.verificaValoreInTabella("ML_MdaSingleBus")||doc.verificaValoreInTabella("ML_KitSwaBusCteGl")||doc.verificaValoreInTabella("ML_KitStdBusCGFDual")|| doc.verificaValoreInTabella("Salvaguardia")) {
                	   System.out.println("Documenti verificati");
                   } else {
                	   throw new Exception("Documenti non presenti");
                   }
                    
                    
                   
                } else if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y") && prop.getProperty("MERCATO").contentEquals("Libero")) {
                   if( doc.verificaValoreInTabella("ML_MdaSingleBus")|| doc.verificaValoreInTabella("ML_KitSwaBusCteGl")||doc.verificaValoreInTabella("ML_KitStdBusCGFDual")||doc.verificaValoreInTabella("Plico")) {
                	   System.out.println("Documenti verificati");
                   } else {
                	   throw new Exception("Documenti non presenti");
                   }
                   
                    
                }
            } else if (prop.getProperty("TIPO_OPERAZIONE").contentEquals("PRIMA_ATTIVAZIONE_EVO")) {
                doc.verificaValoreInTabella("ML_ECConfermaClt");
                doc.verificaValoreInTabella("ML_EOConfermaClt");
                doc.verificaValoreInTabella("ML_KitResInfoPreContr");
                doc.verificaValoreInTabella("ML_MdaSingleRes");
                doc.verificaValoreInTabella("ML_KitSwaResCteSpl");
                doc.verificaValoreInTabella("ML_KitSwaResConfrSpl60");
                doc.verificaValoreInTabella("ML_KitStdResCGFDual");
                doc.verificaValoreInTabella("ML_EsercDirRipensamento");
            }else if(prop.getProperty("TIPO_OPERAZIONE").contentEquals("VOLTURA_CON_ACCOLLO")){
            	//FR 26.08.2021
            	 doc.verificaValoreInTabella("Plico");
                 doc.verificaValoreInTabella("Oggetto generico EMAIL");
                 doc.verificaValoreInTabella("Corpo generico EMAIL");
            
            } else {
                doc.verificaValoreInTabella("Plico");
                doc.verificaValoreInTabella("Oggetto generico EMAIL");
                doc.verificaValoreInTabella("Corpo generico EMAIL");
                doc.verificaValoreInTabella("ML_LettEntranteOkVoltura");
            }

            doc.clickComponentIfExist(doc.buttonChiudiDocumenti);
            logger.write("click su 'visualizza tutto' nella sezione documenti e verifica che in tabella siano presenti nella colonna Modello le seguenti voci: Plico, Oggetto generico EMAIL, ecc - Completed");
            gestione.checkSpinnersSFDC();

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
