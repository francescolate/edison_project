package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaSezioneInboundCopiaDocumenti {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				DettagliSpedizioneCopiaDocComponent forn=new DettagliSpedizioneCopiaDocComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver); 
				SezioneSelezioneFornituraComponent aut=new SezioneSelezioneFornituraComponent(driver);
				logger.write("Valorizzazione campo 'Tipologia DMS' - Start'");						      
            	//By tip_DMS=forn.tipologia_DMS;
                forn.enterDMSType(prop.getProperty("TIPOLOGIA_DMS"));     
                
				logger.write("Valorizzazione campo 'Tipologia DMS' - Completed'");
				
				logger.write("Seleziona button 'Cerca' - Start'");				
				By button_cerca=forn.buttonCercaInbound;
				aut.verifyComponentExistence(button_cerca);
				aut.clickComponent(button_cerca);
				logger.write("Seleziona button 'Cerca' - Completed'");
				TimeUnit.SECONDS.sleep(2);
				
				logger.write("check esistenza campo 'Ricerca Inbound' - Start'");				              
                By campoRicercaInbound=forn.buttonRicercaInbound;
                forn.verifyComponentExistence(campoRicercaInbound);              
                TimeUnit.SECONDS.sleep(1);                
                logger.write("check esistenza campo 'Ricerca Inbound' - Completed");			   
				
				logger.write("inserire nel campo 'Ricerca Inbound' l'ID_DOCUMENTO - Start");
				TimeUnit.SECONDS.sleep(1);
				aut.enterPodValue(campoRicercaInbound, prop.getProperty("ID_DOCUMENTO"));
				TimeUnit.SECONDS.sleep(3);
				logger.write("inserire nel campo 'Ricerca Inbound' l'ID_DOCUMENTO - Completed");
				
				logger.write("verifica che nella tabella della sezione Inbound sono riportate le seguenti informazioni: Id Documento, Numero Attività, Tipologia DMS, Data Ricezione, Commento e selezionare l'Id documento di interesse - Start");
				aut.verificaColonneTabellaSezioneInbound(forn.colonneSezioneInbound);
				TimeUnit.SECONDS.sleep(5);
				forn.clickWithJS(forn.radioButtonSezioneInbound);
				logger.write("verifica che nella tabella della sezione Inbound sono riportate le seguenti informazioni: Id Documento, Numero Attività, Tipologia DMS, Data Ricezione, Commento e selezionare l'Id documento di interesse - Completed");
				
				TimeUnit.SECONDS.sleep(5);
										
				logger.write("verifica che il pulsante 'Conferma Inbound' sia abilitato e click su questo - Start");
				forn.verifyComponentExistence(forn.buttonConfermaSezioneInbound);
				forn.clickComponent(forn.buttonConfermaSezioneInbound);
				logger.write("verifica che il pulsante 'Conferma Inbound' sia abilitato e click su questo - Completed");	
					
				gestione.checkSpinnersSFDC();
					
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
