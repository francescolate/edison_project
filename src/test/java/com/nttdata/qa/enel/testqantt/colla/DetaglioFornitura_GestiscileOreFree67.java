package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DetaglioFornitura_GestiscileOreFree67_Component;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DetaglioFornitura_GestiscileOreFree67 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent home = new HomeComponent(driver);
			home.verifyComponentExistence(home.enelLogo);
			home.verifyComponentExistence(home.gestisciLeOreFree);
			home.clickComponent(home.gestisciLeOreFree);
			DetaglioFornitura_GestiscileOreFree67_Component dc = new DetaglioFornitura_GestiscileOreFree67_Component(driver);
			dc.verifyComponentExistence(dc.oreFreeTitle);
		//	dc.checkForDate();
			dc.scrolltoElement(dc.modifica);
			dc.clickComponent(dc.modifica);
			dc.verifyComponentExistence(dc.setYourBand);
			dc.comprareText(dc.setBandSubText, dc.setYourBandText, true);
			dc.verifyComponentExistence(dc.alleOre);
			dc.verifyComponentExistence(dc.startTime);
			dc.verifyComponentExistence(dc.dalleOre);
			dc.verifyComponentExistence(dc.endTime);
			dc.verifyComponentExistence(dc.haiImpostatoLafascia);
			dc.checkFieldStatus(dc.endTime);
			dc.clickComponent(dc.startTime);
			dc.checkFieldValue();
			dc.changeDropDownValue(dc.startTimeList, prop.getProperty("DALLE_ORE"));
			Thread.sleep(5000);
			dc.verifyComponentExistence(dc.haiImpostatoLafascia);
			dc.switchToDefault();
			dc.clickComponent(dc.conferma);
			dc.comprareText(dc.popUpText, dc.confermaPopupText, true);
			dc.clickComponent(dc.chiudi);
						
			prop.setProperty("RETURN_VALUE", "OK");

			
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
