package com.nttdata.qa.enel.testqantt;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.RichiestaCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class CancelPOD {
	
	@Step("Cancel POD")
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			GetAccountFromComponent gafc = new GetAccountFromComponent(driver);
			
			logger.write("typing the user CF - START");	
			gafc.verifyComponentExistence(gafc.searchBar);
			//gafc.searchAccount(gafc.searchBar, prop.getProperty("EMAIL"), true);
			gafc.searchAccount(gafc.searchBar, prop.getProperty("CF"), true);
			logger.write("typing the user CF - COMPLETED");
			
			/*
			logger.write("checking the data existence - START");	
			gafc.verifyComponentExistence(gafc.accountLink);
			gafc.clickComponent(gafc.accountLink);
			logger.write("checking the data existence - COMPLETE");
			
			*/
			gafc.verifyComponentExistence(gafc.clientiNome);
			gafc.clickComponent(gafc.clientiNome);
			
			RichiestaCollaComponent rcc = new RichiestaCollaComponent(driver);
			
			Thread.sleep(5000);
			//rcc.scrollComponent(rcc.richestaHeader1);
			//rcc.scrollDown();
			rcc.jsScroll();
			rcc.verifyComponentExistence(rcc.richestaHeader1);
			rcc.pressJavascript(rcc.RichestaHeader);
			Thread.sleep(10000);
					
			CaseItemDetailsComponent cdc = new CaseItemDetailsComponent(driver);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDateTime now = LocalDateTime.now();
            rcc.verifyComponentExistence(By.xpath(rcc.richestaAttivazioneToday1.replace("$DATE$", dtf.format(now))));
            rcc.pressJavascript(By.xpath(rcc.richestaAttivazioneToday1.replace("$DATE$", dtf.format(now))));
            
            Thread.sleep(5000);
			rcc.verifyComponentExistence(rcc.itemName);
			rcc.clickComponent(rcc.itemName);
			
			Thread.sleep(5000);
			//Enabling cancel reason and button
			//String bpmId = rcc.getBPMId(rcc.bpmId);
		
			//rcc.CancelActivate(bpmId, prop.getProperty("dtcmmind_URL"));
		
			Thread.sleep(10000);
			cdc.verifyComponentExistence(cdc.annulmentButton);
			cdc.jsClickObject(cdc.annulmentButton);
			Thread.currentThread().sleep(30000);
			
			cdc.selectRinunciaCliente(cdc.rinunciaCliente);
						
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
}
