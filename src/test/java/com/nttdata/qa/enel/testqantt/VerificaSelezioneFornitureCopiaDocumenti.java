package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaSelezioneFornitureCopiaDocumenti {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				logger.write("check esistenza messaggio 'Nella sezione Selezione Fornitura è presente una tabella con i campi'");

				util.getFrameActive();

                By campoRicerca=forn.campoRicerca1;
                forn.verifyComponentExistence(campoRicerca);

                TimeUnit.SECONDS.sleep(5);
                logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Completed");

				logger.write("inserire nel campo 'Cerca Fornitura' il NOME_UTENTE - Start");
				TimeUnit.SECONDS.sleep(5);
				forn.enterPodValue(campoRicerca, prop.getProperty("NOME_UTENTE"));
				TimeUnit.SECONDS.sleep(5);
				logger.write("inserire nel campo 'Cerca Fornitura' il NOME_UTENTE - Completed");


				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: Commodity, Stato, Numero Cliente, POD/PDR, Indirizzo fornitura e selezionare la fornitura di interesse - Start");
				forn.verificaColonneTabellaSelezioneFornitura(forn.colonne_copia_doc);
				forn.clickWithJS(forn.radioButtonCopiaDocumento);
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: Commodity, Stato, Numero Cliente, POD/PDR, Indirizzo fornitura e selezionare la fornitura di interesse - Completed");
				

				logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Start");
				try {
					forn.verifyComponentExistence(forn.buttonConfermaSelezioneFornituraAbilitato);
					forn.clickComponent(forn.buttonConfermaSelezioneFornituraAbilitato);
	            } catch (Exception e) {
	            	System.out.println("button conferma selezione fornitura non abilitato. Continuing execution flow");
	            }
				logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Completed");
					
				gestione.checkSpinnersSFDC();
					
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
