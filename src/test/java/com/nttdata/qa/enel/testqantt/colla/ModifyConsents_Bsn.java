package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModifyConsents_BsnComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyConsents_Bsn {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			//System.out.println("Verifying popup elements and accessing data modification page");
			logger.write("Verifying popup elements and accessing data modification page - Start");
			ModifyConsents_BsnComponent modCon = new ModifyConsents_BsnComponent(driver);
			modCon.verifyVisibilityThenClick(modCon.userLogo);
			//modCon.verifyComponentVisibility(modCon.popupAvatar);
			//modCon.verifyComponentVisibility(modCon.yourProfileBtn);
			modCon.verifyVisibilityThenClick(modCon.contactDataBtn);
			logger.write("Verifying popup elements and accessing data modification page - Completed");
			
			//System.out.println("Verifying modification page strings");
			logger.write("Verifying modification page strings - Start");
			if (modCon.checkGroupSelectionExistance()) {
				modCon.clickComponent(modCon.supplyGroupSelectionBtn);
			}
			modCon.checkModPageStrings();
			logger.write("Verifying modification page strings - Completed");
			
			//System.out.println("Accessing consents page");
			logger.write("Accessing consents page - Start");
			//modCon.verifyVisibilityThenClick(modCon.giveConsentBtn);
			modCon.verifyVisibilityThenClick(modCon.giveConsentByNameBtn);
			String anUrl = prop.getProperty("WP_LINK") + "it/area-clienti/imprese/modifica_dati/privacy";
			driver.get(anUrl);
			logger.write("Accessing consents page - Completed");
			
			//System.out.println("Verifying consents modification page url");
			logger.write("Verifying consents modification page url - Start");
			String theUrl = prop.getProperty("WP_LINK") + modCon.consentsPageUrl;
			modCon.confirmUrl(theUrl);
			Thread.sleep(50000);
			logger.write("Verifying consents modification page url - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
