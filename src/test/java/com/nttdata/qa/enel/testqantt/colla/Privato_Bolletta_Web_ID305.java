package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID308Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID305 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivatoBollettaWebID308Component atti = new PrivatoBollettaWebID308Component(driver);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if path and heading of homePage is displayed - Start");
			By homePagePath = atti.areaRiservata;
			atti.verifyComponentExistence(homePagePath);
			atti.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			By homepageSubheading = atti.pageHeading;
			atti.verifyComponentExistence(homepageSubheading);
			atti.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Click on 'serviziSelect' menu item - Start");
			By servizi = atti.serviziSelectMenu;
			atti.verifyComponentExistence(servizi);
			atti.clickComponent(servizi);
			logger.write("Click on 'serviziSelect' menu item - Completed");
			
			logger.write("Verify SERVIZI Path - Start");
			By serviziPath = atti.areaRiservata;
			atti.verifyComponentExistence(serviziPath);
			atti.VerifyText(serviziPath, prop.getProperty("SERVIZI_PATH"));
			logger.write("Verify SERVIZI Path - Completed");
			
			logger.write("Click on Bolletta WEBBOX Tile - Start");
			By Bolletta_Web_box = atti.Bolletta_Web_box;
			atti.verifyComponentExistence(Bolletta_Web_box);
			atti.clickComponent(Bolletta_Web_box);
			logger.write("Click on Bolletta WEBBOX Tile - Completed");
			Thread.sleep(5000);
			
			logger.write("Validate the supply is in ACTIVE status - Start");
			By supplyActiveStatus = atti.supplyActiveStatus;
			atti.verifyComponentExistence(supplyActiveStatus);
			atti.VerifyText(supplyActiveStatus, prop.getProperty("ACTIVESUPPLYSTATUS"));
			logger.write("Validate the supply is in ACTIVE status - Completed");
			
			logger.write("Click on MODIFICA Bolletta WEB BUTTON - Start");
			By modificaBollettaWeb_button = atti.modificaBollettaWeb_button;
			atti.verifyComponentExistence(modificaBollettaWeb_button);
			atti.clickComponent(modificaBollettaWeb_button);
			logger.write("Click on MODIFICA Bolletta WEB BUTTON - Completed");
			Thread.sleep(45000);
			
			logger.write("Validate for modificaBollettaWeb_Title, modificaBollettaWeb_TitleSubtext- Start");
			By modificaBollettaWeb_Title = atti.modificaBollettaWeb_Title;
			atti.verifyComponentExistence(modificaBollettaWeb_Title);
			atti.VerifyText(modificaBollettaWeb_Title, prop.getProperty("MODIFICA_BOLLETTA_WEB_PAGETITLE"));
			
			By modificaBollettaWeb_TitleSubtext = atti.modificaBollettaWeb_TitleSubtext;
			atti.verifyComponentExistence(modificaBollettaWeb_TitleSubtext);
			atti.VerifyText(modificaBollettaWeb_TitleSubtext, prop.getProperty("MODIFICA_BOLLETTA_WEB_PAGETITLE_SUBTEXT"));
			logger.write("Validate for modificaBollettaWeb_Title, modificaBollettaWeb_TitleSubtext- Completed");
			
			logger.write("Click on 'fornitureSelect' menu item - Start");
			By fornitureSelect = atti.fornitureSelect;
			atti.verifyComponentExistence(fornitureSelect);
			atti.clickComponent(fornitureSelect);
			logger.write("Click on 'fornitureSelect' menu item - Completed");
			Thread.sleep(8000);
			
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Start");
			atti.verifyComponentExistence(atti.dailogTitle);
			atti.VerifyText(atti.dailogTitle, prop.getProperty("DAILOG_TITLE"));
			
			atti.verifyComponentExistence(atti.dailogText);
			atti.VerifyText(atti.dailogText, prop.getProperty("DAILOG_TEXT"));
			
			atti.verifyComponentExistence(atti.Torna_al_processo_Button);
			atti.verifyComponentExistence(atti.Esci_Button);
			
			atti.clickComponent(atti.Torna_al_processo_Button);
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Completed");
			
			logger.write("Check back if  modificaBollettaWeb_Title is displayed - Start");
			atti.verifyComponentExistence(modificaBollettaWeb_Title);
			atti.VerifyText(modificaBollettaWeb_Title, prop.getProperty("MODIFICA_BOLLETTA_WEB_PAGETITLE"));
			logger.write("Check back if  modificaBollettaWeb_Title is displayed - Completed");
			
			logger.write("Click on 'fornitureSelect' menu item - Start");
			atti.verifyComponentExistence(fornitureSelect);
			atti.clickComponent(fornitureSelect);
			logger.write("Click on 'fornitureSelect' menu item - Completed");
			
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Start");
			atti.verifyComponentExistence(atti.dailogTitle);
			atti.verifyComponentExistence(atti.Torna_al_processo_Button);
			atti.verifyComponentExistence(atti.Esci_Button);
			
			atti.clickComponent(atti.Esci_Button);
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Completed");
			
			Thread.sleep(40000);
			
			logger.write("Checking if FORNITURE_SUBTEXT and SUBTITLE_FORNITURA is displayed - Start");
			
			atti.verifyComponentExistence(atti.fornitureSubText);
			atti.VerifyText(atti.fornitureSubText, prop.getProperty("FORNITURE_SUBTEXT"));
			
			atti.verifyComponentExistence(atti.subtitleFornitura);
			atti.VerifyText(atti.subtitleFornitura, prop.getProperty("SUBTITLE_FORNITURA"));
			
			logger.write("Checking if FORNITURE_SUBTEXT and SUBTITLE_FORNITURA is displayed - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			
			
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}	
