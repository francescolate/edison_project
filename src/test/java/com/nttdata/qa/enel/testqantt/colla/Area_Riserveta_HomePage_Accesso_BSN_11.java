package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AreaRiservataHomePageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Area_Riserveta_HomePage_Accesso_BSN_11 {

	public static void main(String[] args) throws Exception{
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			AreaRiservataHomePageComponent home = new AreaRiservataHomePageComponent(driver);
						
		    prop.setProperty("SUPPLY_DETAIL_HEADING", "Dettagli fornitura");
		    prop.setProperty("BILL_DETAILS", "Le tue bollette");      
		    prop.setProperty("HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("HOMEPAGE_TITLE2", "HOMEPAGE");
		    prop.setProperty("HOMEPAGE_TITLE_DESCRIPTION", "Benvenuto nella tua area dedicata Business");
		    prop.setProperty("SUPPLY_BILL_TITLE", "Forniture e bollette");

			logger.write("Home Page Logo, Title and Description Verification- Starts");
			home.verifyComponentExistence(home.homePageLogo);
			home.VerifyText(home.homePageTitle1,prop.getProperty("HOMEPAGE_TITLE1"));
			home.VerifyText(home.homePageTitle2,prop.getProperty("HOMEPAGE_TITLE2"));
			home.VerifyText(home.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"));
			logger.write("Home Page Log,Title and Description Verification- Ends");
			
			logger.write("Home Page Left Menu Items Verification- Starts");
			home.leftMenuItemsDisplay(home.leftMenuItems);
			logger.write("Home Page Left Menu Items Verification- Ends");
			
			logger.write("Home Page Supply Detail Verification- Starts");
			home.VerifyText(home.supplyBillTitle,prop.getProperty("SUPPLY_BILL_TITLE"));
			//home.VerifyText(home.customerLightSupply, "Prova BSN\nFornitura Luce Cliente N °: "+prop.getProperty("CUSTOMER_LIGHT_SUPPLY"));
//			home.VerifyText(home.supplyHeading, prop.getProperty("SUPPLY_DETAIL_HEADING"));
			home.supplyDetailsDisplay(home.supplyDetails);
		    logger.write("Home Page Supply Detail Verification- Ends");	
		    
		    logger.write("Bill Details Verification- Starts");
		    home.VerifyText(home.billDetails, prop.getProperty("BILL_DETAILS"));
		    home.supplyBillDetailsDisplay(home.billTable);
		    logger.write("Bill Details Verification- Ends");
		    
		    prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}

			
			
