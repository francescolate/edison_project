package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CompilaDatiContatto;
import com.nttdata.qa.enel.components.lightning.ConfermaButtonComponent;
import com.nttdata.qa.enel.components.lightning.DettagliComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaCommodityComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaServizioVASComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;
import javassist.bytecode.analysis.Util;

public class SelezionaServizioVasBolletta {

	@Step("Processo Attivazione Servizio VAS")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				
				logger.write("Scelta processo Attivazione Vas - Start");
				SceltaProcessoComponent sceltaProcesso = new SceltaProcessoComponent(driver);
				
				logger.write("Selezione servizio Vas e compilazione campi - Start");
				SelezionaServizioVASComponent servizioVAS = new SelezionaServizioVASComponent(driver);

				// servizioVAS.fillServizioVas(prop.getProperty("SERVIZIO_VAS"));

				// creato nuova funzione che include il controllo del testo della popup
				servizioVAS.fillServizioVas2(prop.getProperty("SERVIZIO_VAS"));
				TimeUnit.SECONDS.sleep(15);
				SelezionaCommodityComponent selectCommodity = new SelezionaCommodityComponent(driver);
				//selectCommodity.setCommodity(prop.getProperty("COMMODITY"));
 				// clicca solo sulla prima check-box
				selectCommodity.selectCommodityCheckbox1(selectCommodity.checkBox);   
				

				CompilaDatiContatto datiContratto = new CompilaDatiContatto(driver);

				if (prop.getProperty("SERVIZIO_VAS").equals("Bolletta Web")) {
					datiContratto.fillDati(prop.getProperty("INDIRIZZO_EMAIL"), prop.getProperty("CELLULARE"));

				}

				if (prop.getProperty("SERVIZIO_VAS").equals("Bolletta PEC")) {
					datiContratto.fillDatiPEC(prop.getProperty("INDIRIZZO_PEC"), prop.getProperty("CELLULARE"));
				}

				ConfermaButtonComponent conferma = new ConfermaButtonComponent(driver);
				// Conferma su pagina dati cliente
				conferma.press(conferma.confermaSave);

				driver.switchTo().defaultContent();
//				driver.switchTo().frame(frameToSw);

				// Click su PopUp Operazione Conclusa con successo
				conferma.verifyComponentExistence(conferma.titoloPopup);
				// Click su bottone NO della Popup
				if (!conferma.verifyExistence(conferma.ricordaNO))
					throw new Exception(
							"Dopo aver completato l'operazione non viene mostrata la popup che ricorda al cliente che ricevera' una mail per conferma");
				conferma.press(conferma.ricordaNO);
				driver.switchTo().defaultContent();
				
				logger.write("Selezione servizio Vas e compilazione campi - Completed");

				// Forse la parte sotto non serve più è cambiato lo scenario
//				logger.write("Verifica flag certificazione obbligatoria nei dati di processo - Start");
//				DettagliComponent dettagli = new DettagliComponent(driver);
//				dettagli.verificaObbligatorietaBollettaWebPec();
//				logger.write("Verifica flag certificazione obbligatoria nei dati di processo - Completed");

			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
