package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;
import org.junit.Assert;

public class CommodityEleNonResidenzialeSubentro {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			
			//conferma.press(conferma.selezionaPrimaFornitura);
			
			conferma.confermaCommodity(prop.getProperty("POD"));
			conferma.verificaCampiSezioneCommodityEle(prop.getProperty("POD"));
			if(!prop.getProperty("CATEGORIA_MERCEOLOGICA","").contentEquals("")) {
	            if(prop.getProperty("CATEGORIA_MERCEOLOGICA","").contentEquals("SERVIZI PER LA RETE STRADALE")) {
					conferma.verificaValoreCategoriaMerceologica(prop.getProperty("CATEGORIA_MERCEOLOGICA"));
				}
	            else {
	            	if(driver.findElement(conferma.inputCategoria).isEnabled()) {
	            	conferma.inserisciCategoriaMerceologica(prop.getProperty("CATEGORIA_MERCEOLOGICA"));
	            	}
				}
            }

			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
			conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
			
	
			
			if(driver.findElement(conferma.inputTitolarita).isEnabled()) {
			conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
			}
			if(!prop.getProperty("ASCENSORE").equals("SI")) {
			conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
			} else {
				conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
				conferma.verifyComponentExistence(conferma.alertAscensore);
				Assert.assertEquals("Ricorda che per l’inserimento dell’ascensore è necessario il calcolo della potenza richiesta.", driver.findElement(conferma.alertAscensore).getText());
				conferma.clickComponent(conferma.alertAscensoreOk);
				conferma.verifyCampiAscensore();
				conferma.inserisciTipologiaAscensore(prop.getProperty("TIPOLOGIA_ASCENSORE"));
				conferma.inserisciCorrenteDiRegime(prop.getProperty("CORRENTE_DI_REGIME"));
				conferma.clickComponent(conferma.calcolaAscensore);
				conferma.verifyComponentExistence(conferma.alertAscensore2);
				Assert.assertEquals("La potenza richiesta/disponibile non è sufficiente rispetto ai dati ascensore. La potenza suggerita è 75 vuole continuare?", driver.findElement(conferma.alertAscensore2).getText());				
				conferma.clickComponent(conferma.alertAscensoreSi);
				
			}
					
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
