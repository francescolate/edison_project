package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.nttdata.qa.enel.components.colla.ContinueWithRegistrationComponent;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ContinueWithRegistration {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		try {
			By accessLoginPage = null;
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            Thread.sleep(10000);
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			Thread.sleep(10000);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			Thread.sleep(10000);
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));
			Thread.sleep(10000);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));
			Thread.sleep(10000);
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			
			Thread.sleep(10000);
			
			ContinueWithRegistrationComponent cwrc = new ContinueWithRegistrationComponent(driver);
			cwrc.firstAccess(prop.getProperty("SUPPLY_NUMBER"));
			
			try{
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Start");
				log.verifyComponentExistence(log.areaClientiCondizioni);
				log.clickComponent(log.areaClientiCondizioni);
				log.verifyComponentExistence(log.areaClientiPrivacy);
				log.clickComponent(log.areaClientiPrivacy);
				log.verifyComponentExistence(log.terminiECondizioniButton);
				log.clickComponent(log.terminiECondizioniButton);
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Completed");
			}catch(Exception e){
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Pagina inesistente. Proseguo.");
			}
			Thread.sleep(5000);

			try{
				if (!prop.getProperty("AREA_CLIENTI").equals(""))
				{
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
						log.verifyComponentExistence(log.areaClientiCasa);
						log.clickComponent(log.areaClientiCasa);
					}
					else
					{
						log.verifyComponentExistence(log.areaClientiImpresa);
						log.clickComponent(log.areaClientiImpresa);
					}
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
				} else {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
				}
			}catch(Exception e){
				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
			}
			Thread.sleep(10000);
			//ContinueWithRegistrationComponent cwrc = new ContinueWithRegistrationComponent(driver);
			try{
			cwrc.verifyComponentExistence(cwrc.primoAccessoHeader);
			cwrc.verifyComponentExistence(cwrc.numeroClienteInputField);
			cwrc.fillInputField(cwrc.numeroClienteInputField, prop.getProperty("SUPPLY_NUMBER"));
			cwrc.verifyComponentExistence(cwrc.numeroClienteContinuaButton);
			cwrc.clickComponent(cwrc.numeroClienteContinuaButton);
			cwrc.verifyComponentExistence(cwrc.numeroClienteConfermaButton);
			cwrc.clickComponent(cwrc.numeroClienteConfermaButton);
			cwrc.verifyComponentExistence(cwrc.numeroClienteFineButton);
			cwrc.clickComponent(cwrc.numeroClienteFineButton);
			}catch(Exception e){
				logger.write("check sull'esistenza della pagina Conferma Codice Cliente - PAGINA INESISTENTE. PROSEGUO.");
			}
			try{
				if (!prop.getProperty("AREA_CLIENTI").equals(""))
				{
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
						log.verifyComponentExistence(log.areaClientiCasa);
						log.clickComponent(log.areaClientiCasa);
					}
					else
					{
						log.verifyComponentExistence(log.areaClientiImpresa);
						log.clickComponent(log.areaClientiImpresa);
					}
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
				} else {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
				}
			}catch(Exception e){
				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
			}
			
			if(!prop.getProperty("ACCOUNT_TYPE").equals(""))
				if(prop.getProperty("ACCOUNT_TYPE").contains("BSN"))
					if(!prop.getProperty("AREA_CLIENTI").equals(""))
						if(prop.getProperty("AREA_CLIENTI").equals("CASA"))
							accessLoginPage=log.loginSuccessful;
						else
							accessLoginPage=log.loginBSNSuccessful;
					else
						accessLoginPage=log.loginBSNSuccessful;
				else
					accessLoginPage=log.loginSuccessful;
			else
				accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage);
			
//			if(prop.getProperty("ACCOUNT_TYPE").contains("LIKE_RES") || prop.getProperty("ACCOUNT_TYPE").contains("LIKE_BSN"))
//			{
//				boolean shouldVerifySelfCareSelectionPage = Boolean.parseBoolean(prop.getProperty("VERIFY_SC_SELECTION_PAGE", "false"));
//				if (!shouldVerifySelfCareSelectionPage) {
//					accessLoginPage=log.loginSuccessful;
//					log.verifyComponentExistence(accessLoginPage); 
//				}
//			}
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
		
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			cealc.restoreAvailableEmailAddresses(cealc.getListOfAvailableEmailAddresses(), prop.getProperty("WP_USERNAME"));

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
