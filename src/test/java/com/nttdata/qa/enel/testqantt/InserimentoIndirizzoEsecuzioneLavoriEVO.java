package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.util.Costanti;

import io.qameta.allure.Step;

public class InserimentoIndirizzoEsecuzioneLavoriEVO {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			CompilaIndirizziComponent fornitura = new CompilaIndirizziComponent(driver);
			// Inserimento e Validazione Indirizzo
			String statoUbiest=Costanti.statusUbiest;
			if(statoUbiest.compareTo("ON")==0){
				logger.write("Inserimento e Validazione Indirizzo - Start");
				fornitura.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				fornitura.verificaIndirizzo(fornitura.buttonVerifica);
				logger.write("Inserimento e Validazione Indirizzo - Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			else if(statoUbiest.compareTo("OFF")==0){
				logger.write("Inserimento Indirizzo Forzato - Start");
				fornitura.inserisciNonValidato(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				fornitura.verificaIndirizzo(fornitura.buttonVerifica);
				logger.write("Inserimento Indirizzo Forzato  - Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			//Compare in caso di CAP comune in più zone, seleziona il primo. Gestito solo nel caso in cui è presente. 
			fornitura.selezionaIstat(fornitura.tabellaIstat, fornitura.primoIstat, fornitura.confermaIstat);
			logger.write("Inserimento e Validazione Indirizzo Esecuzione Lavori - Completed");
			Thread.currentThread().sleep(5000);
			
			TimeUnit.SECONDS.sleep(5);
			logger.write("Conferma inserimento indirizzo - Start");
			fornitura.pressButton(fornitura.confermaIndirizzoEsecuzioneLavori);
			logger.write("Conferma inserimento indirizzo - Completed");
			
			GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
					driver);
			logger.write("Conferma creazione offerta - Start");
			compila.clickComponentWithJseAndCheckSpinners(compila.creaOffertaMainButton);
			logger.write("Conferma creazione offerta - Completed");
			TimeUnit.SECONDS.sleep(5);
			

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
