package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.OffertaOpenEneryDigitalComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_106_SWA_ELE_BSN {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			prop.setProperty("CIGINVALID_INPUT", "2352352352");
			prop.setProperty("CUPINVALID_INPUT", "adas");
			prop.setProperty("CF_NUMERIC", "123456781234567");
			prop.setProperty("COMUNE_DI_NASCITA_ROMA_INPUT", "XXI");
			prop.setProperty("STATOESTERODINASCITA_INPUT", "Rom");
			
			
			OffertaOpenEneryDigitalComponent oc = new OffertaOpenEneryDigitalComponent(driver);
			Thread.sleep(10000);
			oc.verifyComponentExistence(oc.pageTitle);
	//		oc.comprareText(oc.titleSubText, oc.TitlleSubText, true);
			oc.clickComponent(oc.podInfoIcon);
			oc.comprareText(oc.COSEILPODTitle, oc.COSEILPOD_Title, true);
			oc.comprareText(oc.COSEILPODBody, oc.COSEILPOD_Body, true);
			oc.clickComponent(oc.COSEILPODClose);
			oc.verifyComponentExistence(oc.pageTitle);
			
			logger.write("Verify I Tuoi Dati Content -- Start");
			oc.verifyComponentExistence(oc.ITuoiDati);
			oc.verifyComponentExistence(oc.timeLeft);
			oc.comprareText(oc.timeLeft, oc.TimeLeft, true);
			oc.comprareText(oc.CONTRATTO, oc.CONTRATTO_TEXT, true);
			oc.verifyComponentExistence(oc.OpenEnergyDigitalSideMenu);
			oc.verifyComponentExistence(oc.Abbonamento);
			oc.verifyComponentExistence(oc.abbonamentoValue);
			oc.verifyComponentExistence(oc.SERVIZIAGGIUNTIVI);
			oc.verifyComponentExistence(oc.bolletaWeb);
			oc.comprareText(oc.bolletaWebSideMenuText, oc.BolletaWebSideMenuText, true);
			logger.write("Verify I Tuoi Dati Content -- Completed");

			
			logger.write("Verify Inserisci I Tuoi Dati input fields -- Start");
			Thread.sleep(5000);
			oc.switchFrame(oc.Iframe);
			oc.verifyComponentExistence(oc.inserisciITuoiDati);
			oc.verifyComponentExistence(oc.forma_Giuridica);
			oc.verifyComponentExistence(oc.Codice_Fiscale_Societa);
			oc.verifyComponentExistence(oc.Ragione_Sociale);
			oc.verifyComponentExistence(oc.Partita_IVA);
			oc.verifyComponentExistence(oc.Prefisso);
			oc.verifyComponentExistence(oc.Telefono_Societa);
			oc.verifyComponentExistence(oc.Tipo_Referente);
			oc.verifyComponentExistence(oc.Nome_Referente);
			oc.verifyComponentExistence(oc.Cognome_Referente);
			oc.verifyComponentExistence(oc.Email_Societa);
			oc.verifyComponentExistence(oc.Email_Societa_I_Icon);
			oc.verifyComponentExistence(oc.PEC);
			oc.verifyComponentExistence(oc.PEC_I_Icon);
			oc.verifyComponentExistence(oc.CalcolaloOra);
			logger.write("Verify Inserisci I Tuoi Dati input fields -- Completed");
			
			logger.write("Click on CalcolaloOra and Verify pop up details -- Strart");
			oc.clickComponent(oc.CalcolaloOra);
			driver.switchTo().defaultContent();
			oc.verifyComponentExistence(oc.calcolaloOraPopupHeader);
			oc.comprareText(oc.calcolaloOraPopupHeader, oc.CalcolaloOraPopupHeader, true);
			oc.verifyComponentExistence(oc.calcolaloOraPopupClose);
			oc.verifyComponentExistence(oc.calcolaloOraPopupNome);
			oc.verifyComponentExistence(oc.calcolaloOraPopupCogNome);
			oc.verifyComponentExistence(oc.calcolaloOraPopupDataDiNascita);
			oc.verifyComponentExistence(oc.calcolaloOraPopupGiorno);
			oc.verifyComponentExistence(oc.calcolaloOraPopupMese);
			oc.verifyComponentExistence(oc.calcolaloOraPopupAnno);
			oc.verifyComponentExistence(oc.calcolaloOraPopupSesso);
			oc.verifyComponentExistence(oc.calcolaloOraPopupDonna);
			oc.verifyComponentExistence(oc.calcolaloOraPopupSeiNatoInItalia);
			oc.verifyComponentExistence(oc.calcolaloOraPopupSi);
			oc.verifyComponentExistence(oc.calcolaloOraPopupNo);
			oc.verifyComponentExistence(oc.calcolaloOraPopupCalcoraButton);
			oc.verifyComponentExistence(oc.calcolaloOraPopupText);
			oc.comprareText(oc.calcolaloOraPopupText, oc.CalcolaloOraPopupText, true);
			oc.clickComponent(oc.calcolaloOraPopupClose);
			logger.write("Click on CalcolaloOra and Verify pop up details -- Completed");

		//	oc.switchFrame(oc.Iframe);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.comprareText(oc.titleSubText, oc.TitlleSubText, true);
		//	oc.comprareText(oc.laziendaESoggetta, oc.LaziendaESoggetta, true);
			oc.switchFrame(oc.Iframe);
			oc.verifyComponentExistence(oc.laziendaESoggettaSi);
			oc.verifyComponentExistence(oc.laziendaESoggettaNo);
			
			oc.clickComponent(oc.laziendaESoggettaSi);
			oc.verifyComponentExistence(oc.CIG);
			oc.verifyComponentExistence(oc.CUP);
			oc.enterInputParameters(oc.CIG, prop.getProperty("CIGINVALID_INPUT"));
			oc.enterInputParameters(oc.CUP, prop.getProperty("CIGINVALID_INPUT"));
			oc.clickComponent(oc.CIG);
			oc.comprareText(oc.CIGErrorMsg, oc.CIG_CUPErrorMsg, true);
			oc.comprareText(oc.CUPErrorMsg, oc.CIG_CUPErrorMsg, true);
			oc.enterInputParameters(oc.CIG, prop.getProperty("CUPINVALID_INPUT"));
			oc.enterInputParameters(oc.CUP, prop.getProperty("CUPINVALID_INPUT"));
			oc.clickComponent(oc.CIG);
			oc.comprareText(oc.CIGErrorMsg, oc.CIG_CUPErrorMsg, true);
			oc.comprareText(oc.CUPErrorMsg, oc.CIG_CUPErrorMsg, true);
			oc.clickComponent(oc.laziendaESoggettaNo);
			oc.comprareText(oc.informativaText1, oc.InformativaText1, true);
			oc.comprareText(oc.informativaText2, oc.InformativaText2, true);
			oc.comprareText(oc.checkBoxText, oc.CheckBoxText, true);
			
			logger.write("Click on informativa privacy and verify the content -- Start");
			oc.clickComponent(oc.informativaPrivacy);
			driver.switchTo().defaultContent();
			oc.comprareText(oc.informativaPrivacyPopupTitle, oc.InformativaPrivacyPopupTitle, true);
			oc.comprareText(oc.informativaPrivacyPopupBody, oc.InformativaPrivacyPopupBody, true);
			oc.clickComponent(oc.informativaPrivacyButton);
			logger.write("Click on informativa privacy and verify the content -- Completed");
			oc.switchFrame(oc.Iframe);
			oc.verifyComponentExistence(oc.ProseguiButton);
			oc.verifyComponentExistence(oc.oppure);
			oc.verifyComponentExistence(oc.SALVAECONTINUADOPO);
			logger.write("Verify Mandatory field Error Messages -- Start");
			oc.clickComponent(oc.laziendaESoggettaSi);
			oc.clickComponent(oc.ProseguiButton);
			oc.comprareText(oc.forma_GiuridicaErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Codice_Fiscale_SocietaErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Ragione_SocialeErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.emailErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Codice_Fiscale_ReferenteErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Tipo_ReferenteErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Nome_ReferenteErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Cognome_ReferenteErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Telefono_CellulareReferenteErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.CIGErrorMsg1, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.CUPErrorMsg1, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.partiaIVAErrorMsg, oc.PartiaIVAErrorMsg, true);
			logger.write("Verify Mandatory field Error Messages -- Completed");
			
			oc.enterInputParameters(oc.Codice_Fiscale_Societa, prop.getProperty("CODICE_FISCALE_SOCIETA"));
			oc.enterInputParameters(oc.Ragione_Sociale, prop.getProperty("RAGIONE_SOCIALE"));
			oc.enterInputParameters(oc.Partita_IVA, prop.getProperty("PIVA"));
			oc.enterInputParameters(oc.Nome_Referente, prop.getProperty("NOME_REFERENTE"));
			oc.enterInputParameters(oc.Cognome_Referente, prop.getProperty("COGNOME_REFERENTE"));
			oc.enterInputParameters(oc.Codice_Fiscale_Referente, prop.getProperty("CODICE_FISCALE_REFERENTE"));
			Thread.sleep(30000);
			oc.verifyComponentExistence(oc.laziendaESoggettaNo);
			oc.clickComponent(oc.laziendaESoggettaNo);
			oc.clickComponent(oc.ProseguiButton);
			oc.comprareText(oc.emailErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.Telefono_CellulareReferenteErrorMsg, oc.MandatoryErrorMsg, true);
			oc.enterInputParameters(oc.Codice_Fiscale_Societa, prop.getProperty("CF_NUMERIC"));
			oc.clickComponent(oc.ProseguiButton);
			oc.verifyComponentExistence(oc.Codice_Fiscale_ReferenteErrorMsg);
			
			oc.enterInputParameters(oc.Codice_Fiscale_Societa, prop.getProperty("CODICE_FISCALE_SOCIETA"));
			Thread.sleep(5000);
			oc.clickComponent(oc.CalcolaloOra);
			oc.clickComponent(oc.CalcolaloOra);
			driver.switchTo().defaultContent();
			oc.verifyComponentExistence(oc.calcolaloOraPopupHeader);
			oc.comprareText(oc.calcolaloOraPopupHeader, oc.CalcolaloOraPopupHeader, true);
			oc.checkForPrepopulatedText(oc.calcolaloOraPopupNomeString, prop.getProperty("NOME_REFERENTE"));
			oc.checkForPrepopulatedText(oc.calcolaloOraPopupCogNomeString, prop.getProperty("COGNOME_REFERENTE"));
			oc.clickComponent(oc.Calcola);
			oc.comprareText(oc.giornaErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.meseErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.annoErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.sessoErrorMsg, oc.MandatoryErrorMsg, true);
			oc.comprareText(oc.SeiNatoInItaliaErrorMsg, oc.MandatoryErrorMsg, true);
			
			oc.clickComponent(oc.calcolaloOraPopupDonna);
			oc.isElementSelected(oc.calcolaloOraPopupUomo);
			oc.clickComponent(oc.calcolaloOraPopupUomo);
			oc.isElementSelected(oc.calcolaloOraPopupDonna);
			oc.clickComponent(oc.calcolaloOraPopupSi);
			oc.clickComponent(oc.Calcola);
			oc.comprareText(oc.calcolaloOraPopupComuneDiNascitaErrorMsg, oc.MandatoryErrorMsg, true);
			oc.enterInputParameters(oc.calcolaloOraPopupComuneDiNascita, prop.getProperty("COMUNE_DI_NASCITA_ROMA_INPUT"));
			oc.verifyComponentExistence(oc.calcolaloOraPopupComuneDiNascita_Suggestion);
			oc.clickComponent(oc.calcolaloOraPopupNo);
			oc.verifyComponentExistence(oc.calcolaloOraPopupStatoEsteroDiNascita);
			Thread.sleep(3000);
			oc.clickComponent(oc.Calcola);
			oc.comprareText(oc.calcolaloOraPopupStatoEsteroDiNascitaErrorMsg, oc.MandatoryErrorMsg, true);
			oc.enterInputParameters(oc.calcolaloOraPopupStatoEsteroDiNascita, prop.getProperty("STATOESTERODINASCITA_INPUT"));
			oc.verifyComponentExistence(oc.calcolaloOraPopupComuneDiNascita_Suggestion);
			
			oc.checkForDropDownValues(By.xpath("//select[@id='days']"), oc.day);
			oc.checkForDropDownValues(By.xpath("//select[@id='months']"), oc.month);
			oc.clickComponent(oc.calcolaloOraPopupGiorno);
			oc.clickComponent(oc.calcolaloOraPopupGiorno_Input);
			oc.clickComponent(oc.calcolaloOraPopupMese);
			oc.clickComponent(oc.calcolaloOraPopupMese_Input);
			oc.clickComponent(oc.calcolaloOraPopupAnno);
			oc.clickComponent(oc.calcolaloOraPopupAnno_Input);
			oc.clickComponent(oc.calcolaloOraPopupDonna);
			oc.clickComponent(oc.calcolaloOraPopupSi);
			oc.enterInputParameters(oc.calcolaloOraPopupComuneDiNascita, prop.getProperty("COMUNE_DI_NASCITA_ROMA_INPUT"));
			oc.clickComponent(oc.calcolaloOraPopupComuneDiNascita_Suggestion);
			oc.clickComponent(oc.Calcola);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.switchFrame(oc.Iframe);
			oc.clickComponent(oc.Cognome_Referente);
			oc.comprareText(oc.Codice_Fiscale_ReferenteErrorMsg, oc.CF_ErrorMsg, true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
