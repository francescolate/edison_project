package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConfiguraProdottoSWAEVOGasResidenziale {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {


			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Start");
			TimeUnit.SECONDS.sleep(10);
			//			configura.press(configura.linkGasResidenziale);
			////			if (prop.containsKey("OPZIONE_VAS")){
			//			if (!prop.getProperty("OPZIONE_VAS").equals("")){
			//				configura.configuraProdottoConVas(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_VAS"));
			//				logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+" con Vas: "+prop.getProperty("OPZIONE_VAS")+"- Completed");
			//			}
			//			else{
			//			configura.configuraProdotto(prop.getProperty("PRODOTTO"));
			//			}

			configura.press(configura.linkGasResidenziale);
			configura.selezionaProdotto(prop.getProperty("PRODOTTO"));

			logger.write("Selezione del prodotto");

			if (!prop.getProperty("OPZIONE_KAM_AGCOR","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR");
			}

			if (!prop.getProperty("SCELTA_ABBONAMENTI","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO"),prop.getProperty("SCELTA_ABBONAMENTI"));
				logger.write("Selezione Scelta Abbonamenti");
			}

			if (!prop.getProperty("OPZIONE_VAS","").equals("")){
				configura.configuraVas(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_VAS"));
				logger.write("Selezione VAS");
			}
			if (!prop.getProperty("SCONTO","").equals("")){
				configura.configuraSconto(prop.getProperty("PRODOTTO"),prop.getProperty("SCONTO"));
				logger.write("Selezione Sconto");
			}
			if (!prop.getProperty("OPZIONE_FIBRA","").equals("")){
				logger.write("Selezione Fibra - Start");
				configura.ConfiguraFibra(prop.getProperty("OPZIONE_FIBRA"));
				logger.write("Selezione Fibra - Completed");
			}
			if (!prop.getProperty("VAS_EX_GAS","").equals("")){
				configura.configuraVasEx(prop.getProperty("VAS_EX_GAS"));
				logger.write("Selezione VAS Gas");
			}

			//GESTIONE MAIL CERTIFICATA
			if (!prop.getProperty("MOD_MAIL_CERTIFICATA","").equals("")){
				configura.configuraVasMailCertificata(prop.getProperty("PRODOTTO"),prop.getProperty("MAIL_CERTIFICATA"));
				logger.write("Mail Certificata VAS Modificata");
			}

			try {
				configura.salvaConfigurazione();
				logger.write("Salva Configurazione");
			}
			catch(Exception ex)
			{
				//in alcuni casi il bottone salva, anche se non visibile, 
				//viene trovato dall'xpath ma non risulta cliccabile - per gestire la situazione  è stato  inserito il catch
				logger.write("Bottone Salva configurazione carrello non trovato - Gestito mediante catch");

			}
			configura.checkOut();
			logger.write("Check out");

			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Completed");
			TimeUnit.SECONDS.sleep(10);

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
