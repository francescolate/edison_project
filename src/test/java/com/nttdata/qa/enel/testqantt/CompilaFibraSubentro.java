package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ChiudiCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.SezioneFibraComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;
import org.junit.Assert;

public class CompilaFibraSubentro {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		SeleniumUtilities util;

		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			util = new SeleniumUtilities(driver);
			CompilaIndirizziComponent indir = new CompilaIndirizziComponent(driver);
			SezioneFibraComponent fibra = new SezioneFibraComponent(driver);

			TimeUnit.SECONDS.sleep(5);

			if (prop.getProperty("INSERISCI_FIBRA").equals("OK")) {
				logger.write("Inserimento dati FIBRA OK - Start");
				fibra.popolareCampo(prop.getProperty("PROVINCIA_FIBRA"), fibra.ProvinciaSezioneFibra);
				fibra.popolareCampo(prop.getProperty("PROVINCIA_FIBRA"), fibra.ComuneSezioneFibra);
				fibra.popolareCampo(prop.getProperty("INDIRIZZO_FIBRA"), fibra.IndirizzoSezioneFibra);
				fibra.popolareCampo(prop.getProperty("CIVICO_FIBRA"), fibra.CivicoSezioneFibra);
				fibra.clickComponent(fibra.VerificaButtonSezioneFibra);
				fibra.verificaBanner(fibra.bannerCopertura, prop.getProperty("BANNER_FIBRA"));
				fibra.clickComponent(fibra.consensiSezioneFibraNo);
				if(driver.findElement(fibra.ConfermaCellulareSezioneFibra).isDisplayed()) {
				fibra.clearAndSendKeys(fibra.ConfermaCellulareSezioneFibra, prop.getProperty("CELLULARE_FIBRA"));
				} else {
					 System.out.println("Campo Conferma Celllulare non presente!");
				}
				fibra.clickComponentIfExist(fibra.ConsensTrattDatiPersonSezFibra);
			
				if(!prop.getProperty("RINUNCIA_FIBRA", "").equals("")) {
				fibra.clickComponentWithJse(fibra.rinunciaFibra);
			   }
				
				fibra.clickComponentIfExist(fibra.ConfermaButtonSezioneFibra);
				logger.write("Inserimento dati FIBRA OK - Completed");
			} else if (prop.getProperty("INSERISCI_FIBRA").equals("KO")) {
				logger.write("Inserimento dati FIBRA KO - Start");
				
				fibra.popolareCampo(prop.getProperty("PROVINCIA_FIBRA"), fibra.ProvinciaSezioneFibra);
				fibra.popolareCampo(prop.getProperty("PROVINCIA_FIBRA"), fibra.ComuneSezioneFibra);
				fibra.popolareCampo(prop.getProperty("INDIRIZZO_FIBRA"), fibra.IndirizzoSezioneFibra);
				fibra.popolareCampo(prop.getProperty("CIVICO_FIBRA"), fibra.CivicoSezioneFibra);
				fibra.clickComponent(fibra.VerificaButtonSezioneFibra);
				
				fibra.verificaBanner(fibra.bannerCopertura, prop.getProperty("BANNER_FIBRA"));
				logger.write("OK - Copertura Fibra non presente!");
				logger.write(" *** FINE TEST ***");
				System.out.println("OK - Copertura Fibra non presente! *** FINE TEST ***");
				logger.write("Inserimento dati FIBRA KO - Completed");
			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
