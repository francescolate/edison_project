// Package HerokuAPIAccountProfileByID_1

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
//import java.util.HashMap;
import java.util.Properties;
//import java.util.UUID;
import java.util.UUID;

//import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIAccountProfileByID_Component;
import com.nttdata.qa.enel.util.APIService;
//import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIAccountProfileByID_2 {

	/**  
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku TAB Accessi Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			//Da configurare come OutPut
			prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString()); //"8b8fdc68-ee96-11e9-81b4-2a2ae2mano18")
    		prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString()); //"8b8fdc68-ee96-11e9-81b4-2a2ae2mano19")
			prop.setProperty("API_URL", "http://msa-stage.enel.it/msa/enelid/profile/v1/account/"+ prop.getProperty("ENELID"));

    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIAccountProfileByID_Component HRKComponent = new HerokuAPIAccountProfileByID_Component();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


