package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BollettaWebComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID308Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PRIVATO_Attiva_Bolletta_WEB_ACB_304 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a Enel Energia con il tuo Profilo Unico");
			prop.setProperty("HOMEPAGE_PATH", "AREA RISERVATA HOMEPAGE");
			prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area dedicata Business");
			
			
			PrivatoBollettaWebID308Component atti = new PrivatoBollettaWebID308Component(driver);
			
			
			logger.write("Checking if path and heading of homePage is displayed - Start");
			By homePagePath = atti.areaRiservata;
			atti.verifyComponentExistence(homePagePath);
			atti.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			By homepageSubheading = atti.pageHeading;
			atti.verifyComponentExistence(homepageSubheading);
			atti.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Click on 'serviziSelect' menu item - Start");
			By servizi = atti.serviziSelectMenu;
			atti.verifyComponentExistence(servizi);
			atti.clickComponent(servizi);
			logger.write("Click on 'serviziSelect' menu item - Completed");
			
			prop.setProperty("SERVIZI_PATH", "AREA RISERVATA SERVIZI");
			
			logger.write("Verify SERVIZI Path - Start");
			By serviziPath = atti.areaRiservata;
			atti.verifyComponentExistence(serviziPath);
			atti.VerifyText(serviziPath, prop.getProperty("SERVIZI_PATH"));
			logger.write("Verify SERVIZI Path - Completed");
			
			logger.write("Click on Bolletta WEBBOX Tile - Start");
			By Bolletta_Web_box = atti.Bolletta_Web_box;
			atti.verifyComponentExistence(Bolletta_Web_box);
			atti.clickComponent(Bolletta_Web_box);
			Thread.sleep(5000);
			logger.write("Click on Bolletta WEBBOX Tile - Completed");
			
			//prop.setProperty("ActiveSupplyStatus", "Il servizio Bolletta Web è attivo sulla fornitura:");
			
			
			BollettaWebComponent bwc = new BollettaWebComponent(driver);
			driver.switchTo().defaultContent();
			
			logger.write("Validate the supply is in ACTIVE status - Start");
			System.out.println("bwc.supplyActiveStatus -->" + bwc.supplyActiveStatus);
			bwc.verifyComponentExistence(bwc.supplyActiveStatus);
			bwc.compareText(bwc.supplyActiveStatus, bwc.PageSubTextBW, true);
			logger.write("Validate the supply is in ACTIVE status - Completed");
			
			/*
			logger.write("Click on MODIFICA Bolletta WEB BUTTON - Start");
			By modificaBollettaWeb_button = atti.modificaBollettaWeb_button;
			atti.verifyComponentExistence(modificaBollettaWeb_button);
			atti.clickComponent(modificaBollettaWeb_button);
			Thread.sleep(15000);
			logger.write("Click on MODIFICA Bolletta WEB BUTTON - Completed");
			*/
			
			logger.write("Click on MODIFICA INDIRIZZO EMAIL BUTTON - Start");
			bwc.verifyComponentExistence(bwc.modificaIndirizzoEmailButton);
			bwc.clickComponent(bwc.modificaIndirizzoEmailButton);
			logger.write("Click on MODIFICA INDIRIZZO EMAIL BUTTON - Completed");
			
			logger.write("Checking if path and heading of homePage is displayed - Start");
			atti.clickComponent(atti.Esci_ButtonNew);
			atti.clickComponent(atti.EsciSicuro_ButtonNew);
			Thread.sleep(10000);
			atti.verifyComponentExistence(atti.areaRiservata);
			//atti.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
			atti.compareText(atti.areaRiservata, atti.pathHome, true);
			atti.verifyComponentExistence(homepageSubheading);
			atti.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}	
