package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.ContactComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetails_102 {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			prop.setProperty("PROFILE_INFO_TEXT", "La tua richiesta di cambio fornitore sulla fornitura in Indirizzo inserito nel form con l'offerta Scegli Tu Ore Free");
			prop.setProperty("WB_STATUS", "IN LAVORAZIONE");		
			prop.setProperty("WB_PROCESS", "Switch Attivo");		
			prop.setProperty("WB_PRODUCTNAME", "Offer Scegli Tu Ore Free");		
			prop.setProperty("WB_ORIGIN", "Web");		
			prop.setProperty("WB_USERCHANNEL", "C-000516");		
			prop.setProperty("WB_USERCHANNELCODE", "018");		
			prop.setProperty("WB_SUBUSERCHANNEL", "ORDINI ON LINE");		
			prop.setProperty("WB_SUBUSERCHANNELCODE", "0QJ");		
			prop.setProperty("WB_ACCOUNTTYPE", "RESIDENZIALE");		
			prop.setProperty("WB_COMMODITY", "ELETTRICO");		
			prop.setProperty("WB_NEWORDERITEMSTATUS", "Attesa di Certificazione");		
			prop.setProperty("WB_ACTIVATIONREASON", "SWITCH ATTIVO");		
			prop.setProperty("WB_STATUS_C", "Attesa di Certificazione");		
			prop.setProperty("WB_OFFERTYPE", "Electricity");		
			prop.setProperty("WB_OPERATIONTYPE", "Attivazione");		
			prop.setProperty("WB_BILLINGPROFILEPAYMENT", "Bollettino Postale");				

			AccountComponent ac = new AccountComponent(driver);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			ContactComponent cn = new ContactComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
						
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.getQuery_102a);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			
			cn.comprareText(cn.wbStatus, prop.getProperty("WB_STATUS"), true);
			cn.comprareText(cn.wbProcess, prop.getProperty("WB_PROCESS"), true);
			cn.comprareText(cn.wbProductName, prop.getProperty("WB_PRODUCTNAME"), true);
			cn.comprareText(cn.wbCommunicationEmail, prop.getProperty("EMAIL"), true);
			cn.comprareText(cn.wbFirstName, prop.getProperty("NOME"), true);
			cn.comprareText(cn.wbLastName, prop.getProperty("COGNOME"), true);
			cn.comprareText(cn.wbOrigin, prop.getProperty("WB_ORIGIN"), true);
			cn.comprareText(cn.wbChannel, prop.getProperty("WB_USERCHANNEL"), true);
			cn.comprareText(cn.wbChannelCode, prop.getProperty("WB_USERCHANNELCODE"), true);
			cn.comprareText(cn.wbSubChannel, prop.getProperty("WB_SUBUSERCHANNEL"), true);
			cn.comprareText(cn.wbSubChannelCode, prop.getProperty("WB_SUBUSERCHANNELCODE"), true);
			
			wbc.insertQuery(Costanti.getQuery_102b);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			cn.comprareText(cn.wbAccountType, prop.getProperty("WB_ACCOUNTTYPE"), true);
			cn.comprareText(cn.wbCommodity, prop.getProperty("WB_COMMODITY"), true);
			cn.comprareText(cn.wbNewOrderItemStatus, prop.getProperty("WB_NEWORDERITEMSTATUS"), true);
		
			wbc.insertQuery(Costanti.getQuery_102c);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			cn.comprareText(cn.wbActivationReason, prop.getProperty("WB_ACTIVATIONREASON"), true);
			cn.comprareText(cn.wbNEStatus, prop.getProperty("WB_STATUS_C"), true);
			cn.comprareText(cn.wbOffertype, prop.getProperty("WB_OFFERTYPE"), true);
			cn.comprareText(cn.wbOperationType, prop.getProperty("WB_OPERATIONTYPE"), true);
			cn.comprareText(cn.wbAccountType1, prop.getProperty("WB_ACCOUNTTYPE"), true);
			cn.comprareText(cn.wbBillingProfilePayment, prop.getProperty("WB_BILLINGPROFILEPAYMENT"), true);
			cn.verifyFieldNotNull(cn.wbName);
			cn.verifyFieldNotNull(cn.wbQuoteNumber);

			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
