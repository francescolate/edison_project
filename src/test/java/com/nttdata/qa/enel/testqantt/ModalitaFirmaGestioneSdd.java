package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.nttdata.qa.enel.components.lightning.AttivazioneSDDCreaNuovoMetodoDiPagamento;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModalitaFirmaGestioneSdd {

	static QANTTLogger logger=null;

	static Properties prop=null;

	public static void main(String[] args) throws Exception {
		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			logger = new QANTTLogger(prop);
			logger.write("Avvio gestione modalità firma gestione SDD");
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				AttivazioneSDDCreaNuovoMetodoDiPagamento nuovoPag=new AttivazioneSDDCreaNuovoMetodoDiPagamento(WebDriverManager.getDriverInstance(prop));


				String modalitaFirma=prop.getProperty("ATT_SDD_MOD_FIRMA");
				
				String canaleFirma=prop.getProperty("ATT_SDD_CANALE_FIRMA");

				nuovoPag.CompilaModalitaFirma(modalitaFirma,canaleFirma );
				nuovoPag.ConfermaOperazioneAttivazioneSdd();
				logger.write("Fine gestione modalità firma gestione SDD");


			}
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}

	}
}
