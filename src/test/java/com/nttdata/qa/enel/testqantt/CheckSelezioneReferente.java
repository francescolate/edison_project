package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CheckSelezioneReferente {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			logger.write("Box Referente Presente");
			logger.write("Box Referente controllo di almeno un referente preselezionato - Started");
			List<WebElement> listaRadioInput = driver.findElements(By.xpath("//*[text()='Referente']/ancestor::div[@role='region']//input[@type='radio']"));
			for(WebElement radio : listaRadioInput) {
				int i = 0;
				if(radio.getAttribute("aria-checked").equals("true")) {
					System.out.println("Referente Preselezionato esistente");		
					logger.write("Risulta un Referente Preselezionato");
				}
			}		
			logger.write("Box Referente controllo di almeno un referente preselezionato - Ended");
			
			logger.write("Box Referente seleziona nuovo referente - Started");
			conferma.press(conferma.selezionaReferente);
			logger.write("Box Referente nuovo referente selezionato");
			logger.write("Box Referente seleziona nuovo referente - Ended");
			
			conferma.clickComponent(conferma.button_conferma_in_referente_container);

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
	

}
