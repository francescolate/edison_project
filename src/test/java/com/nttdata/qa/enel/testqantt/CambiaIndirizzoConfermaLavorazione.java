package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ChiudiCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;



public class CambiaIndirizzoConfermaLavorazione {

	public static void main (String[] args) throws Exception{
		Properties prop = null;
		SeleniumUtilities util;

		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			String statoUbiest=Costanti.statusUbiest;
			util = new SeleniumUtilities(driver);
			CompilaIndirizziComponent indir = new CompilaIndirizziComponent(driver);
	
			TimeUnit.SECONDS.sleep(10);
			if(statoUbiest.compareTo("ON")==0){
			indir.compileAddressInfo(prop.getProperty("PROVINCIA"), indir.inputProvincia,
					prop.getProperty("CITTA"), indir.inputComune, prop.getProperty("INDIRIZZO"),
					indir.inputIndirizzo, prop.getProperty("CIVICO"), indir.inputCivico,
					indir.buttonVerifica);
			}
			else if(statoUbiest.compareTo("OFF")==0){
				logger.write("Inserimento Indirizzo Forzato - Start");
				indir.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				indir.verificaIndirizzo(indir.buttonVerifica);
				indir.forzaIndirizzo(prop.getProperty("CAP"));
				logger.write("Inserimento Indirizzo Forzato  - Completed");
				TimeUnit.SECONDS.sleep(5);
			}

			TimeUnit.SECONDS.sleep(2);

			ChiudiCambioIndirizzoComponent chiusuraCambio = new ChiudiCambioIndirizzoComponent(driver);
			if(util.exists(chiusuraCambio.popUpOperazioneOK,20)){
				chiusuraCambio.verificaPopUpconTesto(chiusuraCambio.popUpOperazioneOK, chiusuraCambio.popUpOperazioneOKTesto, chiusuraCambio.buttonOKpopUpOperazioneOK, chiusuraCambio.testopopUpOperazioneOK);
			}
			else{
			chiusuraCambio.confermaCambioIndirizzoFornitura();
			chiusuraCambio.confermaAnnullaVas(chiusuraCambio.buttonNo);
			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}


}