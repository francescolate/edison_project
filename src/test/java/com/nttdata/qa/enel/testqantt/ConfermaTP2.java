package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CompletaTP2Component;
import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class  ConfermaTP2 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			CompletaTP2Component CompletaTP2Component = new CompletaTP2Component(driver);
			JavascriptExecutor js = (JavascriptExecutor)driver;

			TimeUnit.SECONDS.sleep(60);
			driver.switchTo().defaultContent();
			try {
				WebElement button=driver.findElement(CompletaTP2Component.buttonAccetta);
				js.executeScript("arguments[0].click();", button);
			}catch(Exception e) {}

			
			logger.write("Selezione Checkbox Trattamenti dati da portale per TP2 - Start");
			TimeUnit.SECONDS.sleep(60);
			driver.switchTo().defaultContent();
			//driver.switchTo().frame(0);
			driver.findElement(By.xpath("//body")).sendKeys(Keys.END);
//			List<WebElement> iframe = driver.findElements(By.xpath("//iframe"));
//			for (WebElement webElement : iframe) {
				String frameFlag = "iframeSF";
				driver.switchTo().frame(frameFlag);
				try {
					WebElement button=driver.findElement(CompletaTP2Component.checkboxTrattamenti);
					js.executeScript("arguments[0].click();", button);
//				break;
				}catch(Exception e) {}
				
//			}
			logger.write("Selezione Checkbox Trattamenti dati da portale per TP2 - Completed");
			
			//se esiste sezione modalità di sottoscrizione, selezionare si
			if(util.exists(CompletaTP2Component.sezioneModalitaSottoscrizione, 15)){
				WebElement radio=driver.findElement(CompletaTP2Component.modalitaSottoscrizioneSi);
				js.executeScript("arguments[0].click();", radio);
//				util.objectManager(CompletaTP2Component.modalitaSottoscrizioneSi, util.scrollAndClick);
				logger.write("Selezione valore Si Modalità di Sottoscrizione");
				//Selezione Presso Spazio Enel Partner
				TimeUnit.SECONDS.sleep(3);
				radio=driver.findElement(CompletaTP2Component.pressoSpazioEnelPartner);
				js.executeScript("arguments[0].click();", radio);
				logger.write("Selezione valore Presso Spazio Enel Partner");
			}
			
			
			//Click su completa
			
			CompletaTP2Component.jsClickObject(CompletaTP2Component.conferma);
			TimeUnit.SECONDS.sleep(30);
			driver.switchTo().defaultContent();
//			button=util.waitAndGetElement(CompletaTP2Component.menuTendina);
//			js = (JavascriptExecutor)driver;
//			js.executeScript("arguments[0].click();", button);
//			TimeUnit.SECONDS.sleep(3);
//
//			button=util.waitAndGetElement(CompletaTP2Component.valoreMenu);
//			js = (JavascriptExecutor)driver;
//			js.executeScript("arguments[0].click();", button);
//
//			TimeUnit.SECONDS.sleep(3);
//			button=util.waitAndGetElement(CompletaTP2Component.ConfAnnulla);
//			js = (JavascriptExecutor)driver;
//			js.executeScript("arguments[0].click();", button);


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}

