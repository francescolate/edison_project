package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class VerificaSelezioneFattureCopiaDocumenti {

    public static void main(String[] args) throws Exception {
        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            DettagliSpedizioneCopiaDocComponent forn = new DettagliSpedizioneCopiaDocComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            SezioneSelezioneFornituraComponent aut = new SezioneSelezioneFornituraComponent(driver);

            if (prop.getProperty("TIPO_UTENZA", "").contentEquals("BO"))
                forn.checkModalitàDuplicato(prop.getProperty("MODALITA_DUPLICATO"));
            else {
                logger.write("Seleziona Invio Dettaglio Fattura  in Modalità Duplicato - Start");
                Thread.sleep(15000);
                forn.selezionaLigtheningValue("Modalità Duplicato", prop.getProperty("MODALITA_DUPLICATO"));
                logger.write("Seleziona Invio Dettaglio Fattura  in Modalità Duplicato - Completed");
            }

            logger.write("check esistenza campo 'Ricerca Fatture' - Start'");
            By campoRicercaFatture = forn.buttonRicercaFatture;
            forn.verifyComponentExistence(campoRicercaFatture);
            logger.write("check esistenza campo 'Ricerca Fatture' - Completed");

            logger.write("inserire nel campo 'Ricerca Fatture' il numero fattura - Start");
            aut.enterPodValue(campoRicercaFatture, prop.getProperty("NUMERO_FATTURA"));
            logger.write("inserire nel campo 'Ricerca Fatture' il numero fattura- Completed");

            logger.write("verifica che nella tabella della sezione Fatture sono riportate le seguenti informazioni: Id Documento, Data Pagamento, Modalità Pagamento, Importo Pagato, Numero Fattura e selezionare l'Id documento di interesse - Start");
            aut.verificaColonneTabellaSezioneFatture(forn.colonneSezioneFatture);
            TimeUnit.SECONDS.sleep(60);
            forn.clickWithJS(forn.radioButtonSezioneFatture);
            logger.write("verifica che nella tabella della sezione Fatture sono riportate le seguenti informazioni: Id Documento, Data Pagamento, Modalità Pagamento, Importo Pagato, Numero Fattura e selezionare l'Id documento di interesse - Completed");


            logger.write("verifica che il pulsante 'Conferma Fatture' sia abilitato e click su questo - Start");
            forn.verifyComponentExistence(forn.buttonConfermaSezioneFatture);
            forn.clickComponent(forn.buttonConfermaSezioneFatture);
            logger.write("verifica che il pulsante 'Conferma Fatture' sia abilitato e click su questo - Completed");

            gestione.checkSpinnersSFDC();

            if (prop.getProperty("TIPO_UTENZA", "").contentEquals("BO")) {
                logger.write("verifica che il pulsante 'Conferma' sia abilitato e click su questo - Start");
                forn.verifyComponentExistence(forn.buttonConferma);
                forn.clickComponent(forn.buttonConferma);
                logger.write("verifica che il pulsante 'Conferma' sia abilitato e click su questo - Completed");

            }

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }


    }

}
