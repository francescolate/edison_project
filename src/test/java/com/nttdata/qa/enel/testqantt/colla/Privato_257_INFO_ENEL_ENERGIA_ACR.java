package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato253InfoEnelEnergiaAcrComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_257_INFO_ENEL_ENERGIA_ACR {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato253InfoEnelEnergiaAcrComponent ieeac = new Privato253InfoEnelEnergiaAcrComponent(driver);
			
			ieeac.verifyComponentVisibility(ieeac.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = ieeac.homePageCentralText;
			ieeac.verifyComponentExistence(homePage);
			ieeac.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			By homepageSubheading = ieeac.homePageCentralSubText;
			ieeac.verifyComponentExistence(homepageSubheading);
			ieeac.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = ieeac.sectionTitle;
			ieeac.verifyComponentExistence(sectionTitle);
			ieeac.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = ieeac.sectionTitleSubText;
			ieeac.verifyComponentExistence(sectionTitleSubText);
			ieeac.VerifyText(sectionTitleSubText, ieeac.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Clicking on dettaglioFornitura button - Start");
			By dettaglioFornitura = ieeac.dettaglioFornituraGas;
			ieeac.verifyComponentExistence(dettaglioFornitura);
			ieeac.clickComponent(dettaglioFornitura);
			logger.write("Clicking on dettaglioFornitura button - Start");
			Thread.sleep(35000);
			
			logger.write("Checking if user navigated to Dettaglio Fornitura Home Page - Start");
			By serviziPerLeForniture = ieeac.serviziPerLeForniture;
			ieeac.verifyComponentExistence(serviziPerLeForniture);
			ieeac.VerifyText(serviziPerLeForniture, prop.getProperty("SERVIZI_PER_LE_FORNITURE"));
			logger.write("Checking if user navigated to Dettaglio Fornitura Home Page - Completed");
						
			logger.write("Validate and click on infoEnelEnergia_button - Start");
			By infoEnelEnergia_button = ieeac.infoEnelEnergia_button;
			ieeac.verifyComponentExistence(infoEnelEnergia_button);
			
			By infoEnelEnergia_button_status = ieeac.infoEnelEnergia_button_status;
			ieeac.verifyComponentExistence(infoEnelEnergia_button_status);
			ieeac.VerifyText(infoEnelEnergia_button_status, prop.getProperty("INFOENEL_ENERIGIA_BUTTON_STATUS"));
			
			ieeac.clickComponent(infoEnelEnergia_button);
			logger.write("Validate and click on infoEnelEnergia_button - Completed");
			Thread.sleep(45000);
						
			ieeac.verifyComponentVisibility(ieeac.infoEnelEnergia_Heading);
			
			logger.write("Verifying Info Enel Energia Page Heading,Subtext and ESCI_button- Start");
			By infoEnelEnergia_Heading = ieeac.infoEnelEnergia_Heading;
			ieeac.verifyComponentExistence(infoEnelEnergia_Heading);
			ieeac.VerifyText(infoEnelEnergia_Heading, prop.getProperty("INFOENEL_ENERIGIA_HEADING"));
			
			By infoEnelEnergia_Subtext = ieeac.infoEnelEnergia_Subtext;
			ieeac.verifyComponentExistence(infoEnelEnergia_Subtext);
			ieeac.VerifyText(infoEnelEnergia_Subtext, ieeac.infoEnelEnergiaSubtext);
					
			By infoEnelEnergia_non_e_attivo_status = ieeac.infoEnelEnergia_non_e_attivo_status;
			ieeac.verifyComponentExistence(infoEnelEnergia_non_e_attivo_status);
			ieeac.VerifyText(infoEnelEnergia_non_e_attivo_status, prop.getProperty("INFOENEL_ENERGIA_NON_E_ATTIVO_STATUS"));
			
			By ESCI_button = ieeac.ESCI_button;
			ieeac.verifyComponentExistence(ESCI_button);
			logger.write("Verifying Info Enel Energia Page Heading,Subtext and ESCI_button- Completed");
			
			logger.write("Validate and click AttivaInfoEnelEnergia_button - Start");
			By AttivaInfoEnelEnergia_button = ieeac.AttivaInfoEnelEnergia_button;
			ieeac.verifyComponentExistence(AttivaInfoEnelEnergia_button);
			ieeac.clickComponent(AttivaInfoEnelEnergia_button);
			logger.write("Validate and click AttivaInfoEnelEnergia_button - Completed");
			Thread.sleep(25000);

			ieeac.verifyComponentVisibility(ieeac.AttivaInfoEnelEnergia_PageTitle);
			
			logger.write("Verifying Attiva Info Enel Energia PageTitle, Description,Subtext, commodities and ESCI_button- Start");
			By AttivaInfoEnelEnergia_PageTitle = ieeac.AttivaInfoEnelEnergia_PageTitle;
			ieeac.verifyComponentExistence(AttivaInfoEnelEnergia_PageTitle);
			ieeac.VerifyText(AttivaInfoEnelEnergia_PageTitle, prop.getProperty("ATTIVA_INFOENEL_ENERGIA_PAGETITLE"));
			
			By AttivaInfoEnelEnergia_Description = ieeac.AttivaInfoEnelEnergia_Description;
			ieeac.verifyComponentExistence(AttivaInfoEnelEnergia_Description);
			ieeac.VerifyText(AttivaInfoEnelEnergia_Description, ieeac.attivaInfoEnelEnergia_Description);
			
			ieeac.selectAttivaInfoEnelEnergiaCheckbox(ieeac.selectAttivaInfoEnelEnergiaCheckbox);
			
			By AttivaInfoEnelEnergia_ESCI_button = ieeac.AttivaInfoEnelEnergia_ESCI_button;
			ieeac.verifyComponentExistence(AttivaInfoEnelEnergia_ESCI_button);
			logger.write("Verifying Attiva Info Enel Energia PageTitle, Description,Subtext, commodities and ESCI_button- Completed");
			
			logger.write("Validate AttivaInfoEnelEnergia_CONTINUA_button - Start");
			By AttivaInfoEnelEnergia_CONTINUA_button = ieeac.AttivaInfoEnelEnergia_CONTINUA_button;
			ieeac.verifyComponentExistence(AttivaInfoEnelEnergia_CONTINUA_button);
			logger.write("Validate AttivaInfoEnelEnergia_CONTINUA_button - Start");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
}