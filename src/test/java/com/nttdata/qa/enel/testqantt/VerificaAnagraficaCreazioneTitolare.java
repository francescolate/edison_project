package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.PartitaIva;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class VerificaAnagraficaCreazioneTitolare {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				AccediTabClientiComponent tabClienti = new AccediTabClientiComponent(driver);
				tabClienti.accediTabClienti();
				CreaNuovoClienteComponent crea = new CreaNuovoClienteComponent(driver);
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				logger.write("Selezione pulsante Nuovo per creazione cliente - Start");
				crea.nuovoCliente();
				logger.write("Selezione pulsante Nuovo per creazione cliente - Completed");
				String frameName;
				
				String partitaIva =prop.getProperty("PARTITA_IVA", "").trim(); 
				if (partitaIva.equalsIgnoreCase("RANDOM"))
				{
					partitaIva = PartitaIva.getPartitaIVA();
					prop.setProperty("PARTITA_IVA", partitaIva);
				}
				
				String nomeReferente =prop.getProperty("NOME_REFERENTE", "").trim(); 
				if (nomeReferente.equalsIgnoreCase("RANDOM"))
				{
					nomeReferente = SeleniumUtilities.randomAlphaNumeric(9);
					prop.setProperty("NOME_REFERENTE", nomeReferente);
				}
				
				switch (prop.getProperty("TIPOLOGIA_CLIENTE")) {
				case "RESIDENZIALE":
					logger.write("Selezione tipologia Residenziale - Start");
					TimeUnit.SECONDS.sleep(10);
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonResidenziale);
					TimeUnit.SECONDS.sleep(10);
					logger.write("Selezione tipologia Residenziale - Completed");
					logger.write("Verifica schermata creazione cliente Residenziale - Start");
					crea.verificaTipologiaCliente("Residenziale");
					logger.write("Verifica schermata creazione cliente Residenziale - Completed");
					logger.write("Verifica obbligatorietà campi anagrafica - Start");
					crea.verificaObbligatorietaCampiAnagraficaResidenziale();
					logger.write("Verifica obbligatorietà campi anagrafica - Completed");
					
					
					break;
					
				case "IMPRESA":
					logger.write("Selezione tipologia Impresa Individuale - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonImpresaIndividuale);
					logger.write("Selezione tipologia Impresa Individuale - Completed");
					logger.write("Verifica schermata creazione nuovo cliente Impresa Individuale - Start");
					crea.verificaTipologiaCliente("Impresa Individuale");
					logger.write("Verifica schermata creazione nuovo cliente Impresa Individuale - Completed");
					logger.write("Compila campi anagrafica Impresa Individuale - Start");
					crea.compilaAnagraficaImpresaIndividuale(partitaIva, prop.getProperty("RAGIONE_SOCIALE"));
					logger.write("Compila campi anagrafica Impresa Individuale - Completed");
//					logger.write("Compila angrafica referente - Start");
//					crea.compilaAnagraficaReferente(nomeReferente,prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"));
//					logger.write("Compila angrafica referente - Completed");
					logger.write("Compila indirizzo sede legale - Start");
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
						indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}	
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					logger.write("Compila indirizzo sede legale - Completed");
					logger.write("Compila contatti cliente - Start");
					crea.compilaContatti(frameName, prop.getProperty("CELLULARE"),
							prop.getProperty("DESCRIZIONE_CELLULARE"));
					logger.write("Compila contatti cliente - Completed");
					logger.write("Verifica obbligatorietà campi anagrafica Titolare Impresa Individuale - Start");
					crea.verificaObbligatorietaCampiAnagraficaResidenziale();
					logger.write("Verifica obbligatorietà campi anagrafica Titolare Impresa Individuale - Completed");
					break;
					
				case "CONDOMINIO":
					logger.write("Selezione tipologia Condominio - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonCondominio);
					logger.write("Selezione tipologia Condominio - Completed");
					logger.write("Verifica schermata creazione nuovo cliente Condominio - Start");
					crea.verificaTipologiaCliente("Condominio");
					logger.write("Verifica schermata creazione nuovo cliente Condominio - Completed");
					logger.write("Compila campi anagrafica Condominio - Start");
					crea.compilaAnagraficaImpresaIndividuale(partitaIva, prop.getProperty("RAGIONE_SOCIALE"));
					logger.write("Compila campi anagrafica Condominio - Completed");
					logger.write("Compila indirizzo sede legale - Start");
					logger.write("Compila indirizzo sede legale - Start");
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
					indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}

					logger.write("Compila indirizzo sede legale - Completed");
					logger.write("Compila contatti cliente - Start");
					crea.compilaContatti(frameName, prop.getProperty("CELLULARE"),
							prop.getProperty("DESCRIZIONE_CELLULARE"));
					logger.write("Compila contatti cliente - Completed");
					logger.write("Verifica obbligatorietà campi anagrafica Titolare Condominio - Start");
					crea.verificaObbligatorietaCampiAnagraficaResidenziale();
					logger.write("Verifica obbligatorietà campi anagrafica Titolare Condominio - Completed");
					break;
					
				case "BUSINESS":
					logger.write("Selezione tipologia Business - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonBusiness);
					logger.write("Selezione tipologia Business - Completed");
					logger.write("Verifica schermata creazione nuovo cliente Business - Start");
					crea.verificaTipologiaCliente("Business");
					logger.write("Verifica schermata creazione nuovo cliente Business - Completed");
					logger.write("Compila campi anagrafica Business - Start");
					String codiceFiscale = partitaIva;
					prop.setProperty("CODICE_FISCALE",codiceFiscale);
					crea.compilaAnagraficaBusiness(codiceFiscale,partitaIva, prop.getProperty("RAGIONE_SOCIALE"),prop.getProperty("TIPO_FORMA_GIURIDICA"));
					logger.write("Compila campi anagrafica Business - Completed");
					logger.write("Compila indirizzo sede legale - Start");
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
					indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					logger.write("Compila indirizzo sede legale - Completed");
					logger.write("Compila contatti cliente - Start");
					crea.compilaContatti(frameName, prop.getProperty("CELLULARE"),
							prop.getProperty("DESCRIZIONE_CELLULARE"));
					logger.write("Compila contatti cliente - Completed");
					logger.write("Verifica obbligatorietà campi anagrafica Titolare Business - Start");
					crea.verificaObbligatorietaCampiAnagraficaResidenziale();
					logger.write("Verifica obbligatorietà campi anagrafica Titolare Business - Completed");
					break;

				default:
					throw new Exception(
							"Tipologia cliente diversa da RESIDENZIALE/IMPRESA/CONDOMINIO/BUSINESS. Impossibile proseguire");
				}


			}

			

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
