package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.SupplyExcelDownload_BsnComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SupplyExcelDownload_Bsn {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Accessing supplies section");
			logger.write("Accessing supplies section - Start");
			SupplyExcelDownload_BsnComponent xlDownloadPage = new SupplyExcelDownload_BsnComponent(driver);
			xlDownloadPage.verifyComponentVisibility(xlDownloadPage.fornitureMenuItem);
			xlDownloadPage.clickComponent(xlDownloadPage.fornitureMenuItem);
			logger.write("Accessing supplies section - Completed");
			
			System.out.println("Downloading file");
			logger.write("Downloading file - Start");
			xlDownloadPage.verifyComponentVisibility(xlDownloadPage.xlExportBtn);
			xlDownloadPage.clickComponent(xlDownloadPage.xlExportBtn);
			logger.write("Downloading file - Completed");
			
			/*
			 * To verify the file has been downloaded, we need to get the download folder path.
			 * Since the test will be executed on Chrome, we can open its settings page and read
			 * the information we need from there.
			 */
			/*System.out.println("Verifying file download");
			logger.write("Verifying file download - Start");
			xlDownloadPage.checkFileDidDownload();
			logger.write("Verifying file download - Completed");*/
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
