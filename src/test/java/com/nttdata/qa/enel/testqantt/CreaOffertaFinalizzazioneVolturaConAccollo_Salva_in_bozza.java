package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CreaOffertaFinalizzazioneVolturaConAccollo_Salva_in_bozza {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			CarrelloComponent carrello = new CarrelloComponent(driver);
			SalvaEditaOffertaComponent edita = new SalvaEditaOffertaComponent(driver);
/*			logger.write("verifica che tutte le sezioni sono state confermate  e click su 'Salva in bozza' - Start");
			offer.verifyComponentExistence(offer.sezioniConfermate);
			offer.clickComponentIfExist(offer.salvaInBozza);*/
			logger.write("Click su Modifica Offerta dopo salvataggio in bozza - Start");
			driver.switchTo().defaultContent();
			
			boolean modificaClicked=false; int counter=0;
			while(!modificaClicked)
			{
				try {
					counter++;				
					util.getFrameActive(offer.modifica);
					Thread.sleep(5*1000);
					//offer.clickComponentWithJse(offer.modifica);
					//offer.clickComponent(offer.modifica);
					offer.click(offer.modifica);
					modificaClicked=true;
				}
				catch(Exception exeption){
					if(counter>2) throw exeption;
				}
			}
			logger.write("Click su Modifica Offerta dopo salvataggio in bozza - Completed");
			driver.switchTo().defaultContent();
			Thread.sleep(3*1000);
			logger.write("Riconferma sezioni-start");

			if (prop.getProperty("COMMODITY").compareTo("ELE")==0) {
				gestione.selezionaFornitura(gestione.fornitura_ele_allaccio);
			}
			else
			{  
				gestione.selezionaFornitura(gestione.fornitura_gas_voltura);
			}


			// TODO: 03/03/2021 Fare codice per scegliere la categoria do consumo
			conferma.inserisciCategoriaConsumo(prop.getProperty("CATEGORIA_CONSUMO"));
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);

			gestione.clickComponentIfExist(offer.buttonConfermaIndirizzoFat);
			gestione.clickComponentIfExist(offer.buttonConfermaIndirizzoRes);
			gestione.clickComponentIfExist(offer.buttonConfermaIndUltimaFat);// TODO: 03/03/2021 Controllare

			gestione.clickComponentIfExist(offer.buttonConfermaMetodoPagamento);
			gestione.clickComponentIfExist(gestione.confermaFatturazioneElettronicaSWAEVO);

			carrello.clickWithJS(carrello.buttonCheckout);

			gestione.checkSpinnersSFDC();

			carrello.checkPopupCheckoutAndClickConferma();

			gestione.checkSpinnersSFDC();

			carrello.checkoutEffettuato();

			gestione.clickComponentIfExist(gestione.confermaButtonContattiEConsensi);
			gestione.clickComponentIfExist(gestione.modalitàFirmaConfermaButton);
			logger.write("Riconferma sezioni-completed");

			logger.write("verifica che tutte le sezioni sono state confermate  e click su 'Conferma' - Start");
			offer.verifyComponentExistence(offer.sezioniConfermate);
			offer.clickComponentIfExist(offer.buttonConferma);
			gestione.checkSpinnersSFDC();
			logger.write("verifica che tutte le sezioni sono state confermate  e click su 'Conferma' - Completed");
			logger.write("verifica che tutte le sezioni sono state confermate  e click su 'Salva in bozza' - Completed");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
