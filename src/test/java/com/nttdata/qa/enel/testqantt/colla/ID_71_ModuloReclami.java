package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_71_ModuloReclami {

			public static void main(String[] args) throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				WebDriver driver = WebDriverManager.getNewWebDriver(prop);
				ModuloReclamiComponent mrc = new ModuloReclamiComponent(driver);
				LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
				mrc.launchLink(prop.getProperty("WP_LINK"));
				
				By accettaCookie = log.buttonAccetta;
				log.verifyComponentExistence(accettaCookie);
				log.clickComponent(accettaCookie);
				
	 			mrc.comprareText(mrc.home, mrc.HOME, true);
				mrc.comprareText(mrc.formReclami, mrc.FORM_RECLAMI, true);
				mrc.comprareText(mrc.reclamiHeading, mrc.FORM_RECLAMI_HEADING, true);
				
				
				mrc.verifyComponentExistence(mrc.nome);
				mrc.verifyComponentExistence(mrc.cognome);
				mrc.verifyComponentExistence(mrc.telefonoCellulare);
				mrc.verifyComponentExistence(mrc.fasceOrarieDiReperibilità);
				mrc.verifyComponentExistence(mrc.indirizzoDellaForniture);
				mrc.verifyComponentExistence(mrc.email);
				mrc.verifyComponentExistence(mrc.confermaEmail);
				mrc.verifyComponentExistence(mrc.numeroCliente);
				mrc.verifyComponentExistence(mrc.codicePOD);
				
				mrc.clickComponent(mrc.calcolaOra);
				mrc.verifyComponentExistence(mrc.calcolaCodiceFiscalePopup);
				
				mrc.verifyComponentExistence(mrc.name);
				mrc.verifyFeildAttribute(mrc.name, prop.getProperty("INSERISCI_IL_TUO_NOME"));
				mrc.verifyComponentExistence(mrc.surName);
				mrc.verifyFeildAttribute(mrc.name, prop.getProperty("INSERISCI_IL_TUO_NOME"));
				mrc.verifyComponentExistence(mrc.gender);
				mrc.verifyTex(mrc.gender, prop.getProperty("SELIZIONA_IL_SESSO"));
				mrc.verifyComponentExistence(mrc.date);
				mrc.verifyFeildAttribute(mrc.date, prop.getProperty("DATE_FORMAT"));
				mrc.verifyComponentExistence(mrc.si);
				mrc.verifyComponentExistence(mrc.no);
				mrc.verifyComponentExistence(mrc.provincia);
				mrc.verifyTex(mrc.provincia, prop.getProperty("SELIZIONA_IL_PROV"));
				mrc.verifyComponentExistence(mrc.commune);
				
				mrc.checkForSessoValue(mrc.gender, mrc.SESSO_VALUE);
				
//				mrc.checkForProvinciaValue(mrc.provincia, mrc.PROVINCIA_VALUE);
				
				mrc.enterInputParameters(mrc.date, prop.getProperty("DATE_ALPHA"));
				
				mrc.clickComponent(mrc.popUpClose);
				
				mrc.enterInputParameters(mrc.nome, prop.getProperty("NOME"));
				mrc.enterInputParameters(mrc.cognome, prop.getProperty("COGNOME"));
				mrc.clickComponent(mrc.calcolaOra);
//				mrc.verifyFeildAttribute(mrc.name, prop.getProperty("NOME"));
//				mrc.verifyFeildAttribute(mrc.surName, prop.getProperty("COGNOME"));
				
				mrc.clickComponent(mrc.provincia);
				mrc.clickComponent(mrc.provinciaAgrigento);
//				mrc.getAttributeInnerText(mrc.commune, prop.getProperty("AGRIGENTO"));
				
				mrc.clickComponent(mrc.provincia);
				mrc.clickComponent(mrc.provinciaNapoli);
//				mrc.getAttributeInnerText(mrc.commune, prop.getProperty("ACERRA"));
				
				mrc.checkForCommuneValue(mrc.commune, mrc.COMMUNE_VALUE);
				
				mrc.clickComponent(mrc.popUpClose);
				mrc.comprareText(mrc.home, mrc.HOME, true);
				mrc.comprareText(mrc.formReclami, mrc.FORM_RECLAMI, true);
				mrc.comprareText(mrc.reclamiHeading, mrc.FORM_RECLAMI_HEADING, true);
				
				mrc.clickComponent(mrc.calcolaOra);
//				mrc.clickComponent(mrc.name);
//				mrc.comprareText(mrc.errorMessageName, mrc.ERROR_MESSAGE, true);
//				mrc.clickComponent(mrc.surName);
//				mrc.comprareText(mrc.errorMessagesurName, mrc.ERROR_MESSAGE, true);
				mrc.clickComponent(mrc.date);
				mrc.comprareText(mrc.errorMessageBirthDay, mrc.ERROR_MESSAGE, true);
				mrc.clickComponent(mrc.gender);
//				mrc.comprareText(mrc.gender, mrc.EFFTTUAUNASELEZIONE, true);
				
				mrc.clickComponent(mrc.popUpClose);
				mrc.comprareText(mrc.home, mrc.HOME, true);
				mrc.comprareText(mrc.formReclami, mrc.FORM_RECLAMI, true);
				mrc.comprareText(mrc.reclamiHeading, mrc.FORM_RECLAMI_HEADING, true);
				
				mrc.clickComponent(mrc.calcolaOra);
				mrc.enterInputParameters(mrc.name, prop.getProperty("NOME"));
				mrc.enterInputParameters(mrc.surName, prop.getProperty("COGNOME"));
				mrc.enterInputParameters(mrc.date, "07-02-2021");
				mrc.clickComponent(mrc.gender);
				mrc.clickComponent(mrc.sessoFemmina);
//				mrc.clickComponent(mrc.si);
				mrc.clickComponent(mrc.provincia);
				mrc.clickComponent(mrc.provinciaAgrigento);
				mrc.clickComponent(mrc.calcolaOraButton);
				
				prop.setProperty("RETURN_VALUE", "OK");
				
			} catch (Throwable e) {
				
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

			} finally {
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}

		}
	}
