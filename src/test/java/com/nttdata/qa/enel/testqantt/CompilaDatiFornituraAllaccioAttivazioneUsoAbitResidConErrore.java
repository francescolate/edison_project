package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CompilaDatiFornituraAllaccioAttivazioneUsoAbitResidConErrore {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
					driver);
			logger.write("Selezione commodity - Start");
			compila.selezionaFornitura(compila.fornitura_ele_allaccio);
			logger.write("Selezione commodity - Completed");
		
			logger.write("Inserimento Dati Fornitura - Start");
			//selezione Residente
			compila.selezionaLigtheningValue("Residente", prop.getProperty("RESIDENTE"));
			logger.write("Selezione Campo Residente");
			
			//selezione Uso Energia
			compila.selezionaLigtheningValue("Uso Energia", prop.getProperty("USO_ENERGIA"));
			logger.write("Selezione Campo Uso Energia");
			
			//selezione Titolarita’
			compila.selezionaLigtheningValue("Titolarita’", prop.getProperty("TITOLARITA"));
			logger.write("Selezione Campo Titolarita’");
			
			//selezione Tensione Richiesta
			compila.selezionaLigtheningValueInSrollView("Tensione Richiesta", prop.getProperty("TENSIONE"));
			logger.write("Selezione Campo Tensione Richiesta");
			
			//selezione Ascensore/Impianto di Sollevamento
			compila.selezionaLigtheningValue("Ascensore/Impianto di Sollevamento", prop.getProperty("ASCENSORE"));
			logger.write("Selezione Campo Ascensore/Impianto di Sollevamento");
			
			//selezione Potenza Richiesta 
			compila.selezionaLigtheningValueInSrollView("Potenza Richiesta", prop.getProperty("POTENZA"));
			logger.write("Selezione Campo Potenza Richiesta");
			
			//selezione Disalimentabilita’
			compila.selezionaLigtheningValue("Disalimentabilita’", prop.getProperty("DISALIMENTABILITA"));
			logger.write("Selezione Campo Disalimentabilita’");
			
			//selezione Tipo Misuratore
			compila.selezionaLigtheningValue("Tipo Misuratore", prop.getProperty("TIPO_MISURATORE"));
			logger.write("Selezione Campo Tipo Misuratore");
			
			//Inserimento consumo Annuo
			compila.insertTextByChar(compila.consumo_annuo_allaccio, prop.getProperty("CONSUMO_ANNUO"));
			logger.write("Selezione Campo Consumo Annuo");
			
			logger.write("Inserimento Dati Fornitura - Completed");
			
			logger.write("Conferma inserimento fornitura - Start");
			compila.pressAndCheckSpinners(compila.confermaFornitura);
			logger.write("Conferma inserimento fornitura - Completed");
			
			logger.write("Visualizzazione pop-up Errore Potenza - Start");
			compila.verificaPopUpconTesto(compila.popUpErrorePotenzaTitle, compila.popUpErrorePotenzaTesto,compila.buttonPopUpChiudiErrorePotenza, compila.popUpErrorePotenzaTestoAtteso);
			logger.write("Visualizzazione pop-up Errore Potenza - Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
	
	public static String checklistMessage="ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:  POD/PDR/numero utenza.INFORMAZIONI UTILI - Il Cambio Uso della fornitura di energia elettrica ha un costo pari a: 0,00 €.- Il Cambio Uso della fornitura di gas ha un costo pari a: 0,00 €.- Contestualmente alla variazione uso è possibile attivare/modificare/revocare i Servizi VASe il Metodo di pagamento - Per la variazione uso gas, nel caso di modifica dell'uso fornitura, è necessario eseguire il cambio prodotto.Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto."; 
}
