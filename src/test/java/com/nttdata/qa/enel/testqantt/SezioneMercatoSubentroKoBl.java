package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SezioneMercatoSubentroKoBl {

    public static void main(String[] args) throws Exception {
        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            logger.write("Verifica checklist e conferma - Start");
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);


            Thread.sleep(5000);

            checkListModalComponent.clickConferma();


            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");
            SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
            merc.verifyComponentExistence(merc.tabSubentro);
            merc.verifyComponentExistence(merc.labelSubentro);

            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            merc.verifyComponentExistence(insert.sezioneInserimentoDati);

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");

            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
            merc.verifyComponentExistence(merc.checkContatto);
            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");


            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
            merc.verifyComponentExistence(merc.checkCf);
            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");

            Thread.sleep(5000);

            merc.clickComponentIfExist(merc.buttonAnnullaSezioneMercato);

            util.getFrameActive();

            merc.verifyComponentExistence(merc.TextPopupAnnullaSezioneMercato);

            Thread.sleep(5000);

            if (merc.getElementTextString(merc.paragrafoPopupAnnullaSezioneMercato).equals("Tutte le modifiche non salvate andranno perse. Continuare?"))
                logger.write("Il testo dell'alet è corretto");
            else
                throw new Exception("Il testo dell'alet non è corretto");

            merc.clickComponentIfExist(merc.popupButtonConferma);

            util.getFrameActive();

            Thread.sleep(5000);

            merc.checkSpinnersSFDC();

            Thread.sleep(5000);

            merc.verifyComponentExistence(merc.articleNavigazioneProcesso);

           if(merc.getElementTextString(merc.statoNavigazioneProcesso).equals("CHIUSO") && merc.getElementTextString(merc.sottostatoNavigazioneProcesso).equals("ANNULLATO"))
               logger.write("Ok");
           else
               throw new Exception("KO");




            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }

    public static String checkListText = "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- Che il contatore sia cessato ed in passato abbia erogato Luce/gas- Che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:POD/PDR - Matr/Eneltel- Che il cliente non sia in SALVAGUARDIA, altrimenti invitalo a recarsi al PEINFORMAZIONI UTILI- In caso di Dual ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO- In caso di Multi ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO – PRODOTTO – STESSA COMMODITY- In caso di Cliente Pubblica Amministrazione sarà necessario il Codice Ufficio- In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)- In caso di Duale/Multi ricordati che non è possibile eseguire la modifica potenza e/o tensione- Se il cliente attiva metodo di pagamento SDD e Bolletta Web riceverà uno sconto\\bonus in fatturaFibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento del subentro. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta  In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente:-   numero di cellulare -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associata  SERVIZIO DI PRENOTAZIONEIl Servizio di Prenotazione consente al Cliente di concludere il contratto di fornitura oggi, scegliendo una data desiderata di attivazione del servizio. Il Cliente potrà modificare la data di attivazione successivamente, attraverso un link che riceverà nella propria casella email.ATTENZIONE: è necessario che il Cliente restituisca tutta la documentazione necessaria richiesta,  prima della data indicata, altrimenti non sarà possibile procedere con l’operazione.Quando è possibile richiedere il Servizio di Prenotazione:al momento il servizio è disponibile solo per operazioni di :-          riattivazione del contatore elettrico su fornitura singola (subentro single ele)-          operazioni a parità di condizioni tecniche  (senza modifica potenza e/o tensione)-          rete application to application  ( E- Distribuzione )-          comprese tra 20gg dalla data odierna e non oltre i 90gg (giorni solari) Cosa serve:E’ necessario fornire un:-          indirizzo EMAIL-          numero di telefono mobileSe i dati sono già presenti a sistema, chiedere conferma al cliente prima di procedere.  SCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"";

}
