package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Login_Public_Area {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			logger.write("apertura del portale web Enel di test - Start");
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);
			logger.write("check sulla presenza del logo Enel - Completed");
			
			log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));
			
			By buttonCookiesAccept = log.buttonAccetta;
			log.verifyComponentExistence(buttonCookiesAccept);
			log.jsClickComponent(buttonCookiesAccept);
			
			//log.verifyComponentExistence(log.cookiePolicyAcceptButton);
			//log.jsClickComponent(log.cookiePolicyAcceptButton);
			
			logger.write("click su button accept cookies - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");
	
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
	
	//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
