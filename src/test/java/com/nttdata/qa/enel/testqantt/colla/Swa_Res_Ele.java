package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ELightBiorariaComponent;
import com.nttdata.qa.enel.components.colla.GiustaPerTeBiorariaComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.OrdinaPerMenuComponent;
import com.nttdata.qa.enel.components.colla.PLACETFissaLuceConsumerComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Swa_Res_Ele {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);			
			Thread.currentThread().sleep(3000);
			
			logger.write("click sul tab 'Luce e Gas' - Start");
			
			TopMenuComponent tab=new TopMenuComponent(driver);
			tab.sceltaMenu("Luce e gas");
			
			logger.write("click sul tab 'Luce e Gas' - Completed");
			
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);			
			/*
			logger.write("check sul titolo della pagina 'Trova la soluzione giusta per te' nella sezione 'Luce e Gas' - Start");
			By solutionLabel=headerMenu.rightSolutionLabel;
			headerMenu.verifyComponentExistence(solutionLabel);
			logger.write("check sul titolo della pagina 'Trova la soluzione giusta per te' nella sezione 'Luce e Gas' - Completed");
			*/
					
			headerMenu.scrollComponent(headerMenu.labelLuceandGas);
			
			logger.write("visualizzazione dell domande 'Che contratto vuoi attivare';'Dove?';'Per quale necessità?' relative al motorino di ricerca e del pulsante 'INIZIA ORA' - Start");
			By iniziaOraPulsating=headerMenu.iniziaOraButton;
			headerMenu.verifyComponentExistence(iniziaOraPulsating);
			logger.write("check existence button 'INIZIA ORA' - Completed");
			
			
			By searchMenu=headerMenu.searchSection;
			headerMenu.verifyQuestionsExistence(searchMenu);
			logger.write("visualizzazione dell domande 'Che contratto vuoi attivare';'Dove?';'Per quale necessità?' relative al motorino di ricerca e del pulsante 'INIZIA ORA' - Completed");
			
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Che contratto vuoi attivare': Luce; Gas; Luce e Gas - Start");
			By optionsContractList=headerMenu.contractList;
			headerMenu.verifyFirstListBox(optionsContractList);
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Che contratto vuoi attivare': Luce; Gas; Luce e Gas - Completed");
			
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Dove': Casa; Negozio - Ufficio; Sei una start up, una PMI o una grande azienda? SCOPRI LE OFFERTE PER LE IMPRESE - Start");
			By optionsPlaceList=headerMenu.placeList;
			headerMenu.verifySecondListBox(optionsPlaceList);
			
			
			By linkOnPlaceList=headerMenu.thirdOptionLinkOnPlaceList;
			headerMenu.verifyComponentExistence(linkOnPlaceList);
			By scopri_le_offerte=headerMenu.scoprileofferte;
			headerMenu.checkColor(scopri_le_offerte, "DeepPink", "'SCOPRI LE OFFERTE'");
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Dove': Casa; Negozio - Ufficio; Sei una start up, una PMI o una grande azienda? SCOPRI LE OFFERTE PER LE IMPRESE - Completed");
			
			logger.write("accesso alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkIMPRESE+"' - Start");
			headerMenu.clickComponent(linkOnPlaceList);
			Thread.currentThread().sleep(3000);
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkIMPRESE);
			By impresePageAccess=headerMenu.impresePage;
			headerMenu.verifyComponentExistence(impresePageAccess);
			logger.write("accesso alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkIMPRESE+"' - Completed");
			
			logger.write("back alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS+"' - Start");
			driver.navigate().back();
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS);
			headerMenu.verifyComponentExistence(iniziaOraPulsating);
			logger.write("back alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS+"' - Completed");
			
			logger.write("verifica presenza processi con relativa descrizione sulla pagina web 'Luce e Gas' per la listbox 'Per quale necessita':CAMBIO FORNITORE; PRIMA ATTIVAZIONE; SUBENTRO; VOLTURA; VISUALIZZA TUTTE  - Start");
			By optionsNeedList=headerMenu.needList;
			headerMenu.verifyThirdListBox(optionsNeedList);
			logger.write("verifica presenza processi con relativa descrizione sulla pagina web 'Luce e Gas' per la listbox 'Per quale necessita':CAMBIO FORNITORE; PRIMA ATTIVAZIONE; SUBENTRO; VOLTURA; VISUALIZZA TUTTE  - Completed");
			
			logger.write("effettuare ricerca con il filtro: Luce;Casa;Cambio fornitore' e check sulla pagina alla quale si accede - Start");
			headerMenu.selectionOptionsFromListbox(optionsContractList,prop.getProperty("TYPE_OF_CONTRACT"),optionsPlaceList,prop.getProperty("PLACE"),optionsNeedList,prop.getProperty("NEED"));		
			headerMenu.clickComponent(iniziaOraPulsating);
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS_CON_FILTRO);
			headerMenu.verifyQuestionsExistence(searchMenu);
			
			OrdinaPerMenuComponent ordinaperMenu= new OrdinaPerMenuComponent(driver);			
			By ordinaLabel=ordinaperMenu.labelOrdinaPer;
			ordinaperMenu.verifyComponentExistence(ordinaLabel);
						
			By inpromozioneLabel=ordinaperMenu.defaultLabel;
			ordinaperMenu.verifyComponentExistence(inpromozioneLabel);
						
			By perlacasalabel=headerMenu.labelperlacasa;
			//headerMenu.verifyComponentExistence(perlacasalabel);
			//headerMenu.checkColor(perlacasalabel,"LightGray","label PER LA CASA");
						
			OffersLuceComponent offer= new OffersLuceComponent(driver);	
			By elightLabel=offer.labelElight;
			offer.verifyComponentExistence(elightLabel);
		
			
			By searchofferButton=offer.discovertheofferButton;
			offer.scrollComponent(searchofferButton);
			offer.verifyTextButton(searchofferButton);
			logger.write("effettuare ricerca con il filtro: Luce;Casa;Cambio fornitore' e check sulla pagina alla quale si accede - Completed");
			
			logger.write("check sul testo sotto la sezione dell'offerta in evidenza - Start");
			By PromotionalmessageLink=offer.vediTutteLuce;
			offer.scrollComponent(PromotionalmessageLink);
			offer.checkColor(PromotionalmessageLink, "DeepPink", "VEDI TUTTE LUCE");
				
			By luceperlacasaLabel=offer.labelLuceperlacasa_2;
			offer.checkColor(luceperlacasaLabel, "Black", "LUCE PER LA CASA");
									
			By titleLabel=offer.labelTitle;
			offer.verifyTextLabel(titleLabel,offer.daiPiuLuce);
						
			By descriptionLabel=offer.labelDescription;
			offer.verifyTextLabel(descriptionLabel,offer.descrizione);
						
			offer.scrollComponent(PromotionalmessageLink);
			logger.write("check sul testo sotto la sezione dell'offerta in evidenza - Completed");
			
			logger.write("click sul button 'VEDI TUTTE LUCE' e apertura pagina 'LUCE E GAS' senza filtro impostato nel motorino di ricerca - Start");
			offer.clickComponent(PromotionalmessageLink);
						
			headerMenu.verifyQuestionsExistence(searchMenu);
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS_SENZA_FILTRO);
						
			By defaultcontract=headerMenu.defaultcontractListvalue;
			headerMenu.verifyComponentExistence(defaultcontract);
			
			By defaultplace=headerMenu.defaultplaceListvalue;
			headerMenu.verifyComponentExistence(defaultplace);
			
			By defaultneed=headerMenu.defaultneedListvalue;
			headerMenu.verifyComponentExistence(defaultneed);
			logger.write("click sul button 'VEDI TUTTE LUCE' e apertura pagina 'LUCE E GAS' senza filtro impostato nel motorino di ricerca - Completed");
			
			logger.write("back alla pagina precedente  'LUCE E GAS' con il filtro nel motorino di ricerca impostato - Start");
			
			driver.navigate().back();
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS_CON_FILTRO);
			headerMenu.verifyQuestionsExistence(searchMenu);
			By precedentcontract=headerMenu.contractListvalue;
			headerMenu.verifyTextSearchMenu(precedentcontract,prop.getProperty("TYPE_OF_CONTRACT"));
			
			By precedentplace=headerMenu.placeListvalue;
			headerMenu.verifyTextSearchMenu(precedentplace,prop.getProperty("PLACE"));
			
			By precedentneed=headerMenu.Listvalue;
			headerMenu.verifyTextSearchMenu(precedentneed,prop.getProperty("NEED_PRECEDENTE"));
			logger.write("back alla pagina precedente  'LUCE E GAS' con il filtro nel motorino di ricerca impostato - Completed");
			
			logger.write("check della lista offerte per il filtro 'Ordina Per:' impostato a 'IN PROMOZIONE'- Start");
			By titoli_listOfOffers=offer.titoliListaOfferte;
	//		offer.verifyOfferList(titoli_listOfOffers, prop.getProperty("ORDINA_PER_1"), offer.SEQUENZA_OFFERTE_IN_PROMOZIONE);
			logger.write("le offerte sono le seguenti: "+offer.SEQUENZA_OFFERTE_IN_PROMOZIONE);
			logger.write("check della lista offerte per il filtro 'Ordina Per:' impostato a 'IN PROMOZIONE'- Completed");	
			
			logger.write("check sulla presenza di tre possibili valori per il filtro 'Ordina Per:' : In Promozione,Bioraria,Monoraria - Start");
			headerMenu.scrollComponent(ordinaLabel);
			headerMenu.verifyComponentExistence(ordinaLabel);
						
			ordinaperMenu.verifyComponentExistence(inpromozioneLabel);
						
			By menuclickOrdinaper=ordinaperMenu.OrdinaperMenu;
			ordinaperMenu.scrollComponent(menuclickOrdinaper);
			ordinaperMenu.checkOptionsFromListbox(menuclickOrdinaper);
			logger.write("check sulla presenza di tre possibili valori per il filtro 'Ordina Per:' : In Promozione,Bioraria,Monoraria - Completed");
			
			logger.write("sulla pagina web 'Luce e Gas' dal menu 'ORDINA PER:' viene selezionata l'opzione: Monoraria : - Start");
			ordinaperMenu.selectionOptionsFromListbox(menuclickOrdinaper,prop.getProperty("ORDINA_PER_2"));
			logger.write("sulla pagina web 'Luce e Gas' dal menu 'ORDINA PER:' viene selezionata l'opzione: Monoraria : - Completed");
			
			By featuredofferlabel=offer.labelInEvidenza;
			offer.verifyComponentExistence(featuredofferlabel);
			logger.write("check existence label 'IN EVIDENZA': - Completed");
				
	    	offer.verifyComponentExistence(elightLabel);
			logger.write("check existence offer 'E-Light' - Completed");
			
			offer.scrollComponent(searchofferButton);
			offer.verifyTextButton(searchofferButton);
			logger.write("check existence button 'SCOPRI L'OFFERTA' for E-light offer - Completed");
			
			logger.write("verifica la lista delle offerte sulla pagina web 'Luce e Gas' con filtro 'ORDINA PER:' impostato a Monoraria : - Start");
		//	offer.verifyOfferList(titoli_listOfOffers,prop.getProperty("ORDINA_PER_2"),offer.SEQUENZA_OFFERTE_MONO);
			logger.write("verifica la lista delle offerte sulla pagina web 'Luce e Gas' con filtro 'ORDINA PER:' impostato a Monoraria : - Completed");	
			
			Thread.sleep(5000);
			
			logger.write("sulla pagina web 'Luce e Gas' dal menu 'ORDINA PER:' vine selezionata l'opzione: Bioraria : - Start");
			ordinaperMenu.selectionOptionsFromListbox(menuclickOrdinaper,prop.getProperty("ORDINA_PER_3"));
			logger.write("sulla pagina web 'Luce e Gas' dal menu 'ORDINA PER:' vine selezionata l'opzione: Bioraria : - Completed");
			
			offer.verifyComponentExistence(featuredofferlabel);
			logger.write("check existence label 'IN EVIDENZA': - Completed");
				
	    	offer.verifyComponentExistence(elightLabel);
			logger.write("check existence offer 'E-Light' - Completed");
			
			Thread.sleep(5000);
			
			logger.write("verifica la lista delle offerte sulla pagina web 'Luce e Gas' con filtro 'ORDINA PER:' impostato a Bioraria : - Start");
			offer.verifyOfferList2(titoli_listOfOffers, offer.offerteBio);
			//offer.verifyOfferList(titoli_listOfOffers,prop.getProperty("ORDINA_PER_3"),prop.getProperty("SEQUENZA_OFFERTE_BIO"));
			logger.write("verifica la lista delle offerte sulla pagina web 'Luce e Gas' con filtro 'ORDINA PER:' impostato a Bioraria : - Completed");
			

			logger.write("verifica offerta GiustaPerTe Bioraria and click on: - Start");
			offer.verifyComponentExistence(offer.bannerGiustaPerTeBioraria);
			offer.containsText(offer.bannerGiustaPerTeBioraria, "LUCE");
			offer.containsText(offer.bannerGiustaPerTeBioraria, "GiustaPerTe Bioraria");
			offer.containsText(offer.bannerGiustaPerTeBioraria, "Risparmi di sera, nei weekend e nei festivi con uno sconto del 100% della componente energia il 1° mese");
			offer.containsText(offer.bannerGiustaPerTeBioraria, "DETTAGLIO OFFERTA");
			//offer.containsText(offer.bannerGiustaPerTeBioraria, "Sconto 100% primo mese BIO");
			offer.containsText(offer.bannerGiustaPerTeBioraria, "*prezzo per kWh nel weekend, festivi e tutti i giorni dalle 19 alle 8");
			offer.containsText(offer.bannerGiustaPerTeBioraria, "**prezzo per kWh da lunedì a venerdì, dalle 8 alle 19");
			offer.containsText(offer.bannerGiustaPerTeBioraria, "Il prezzo si riferisce alla sola componente energia ed è bloccato per un anno (IVA e imposte escluse)");
			offer.verifyComponentExistence(offer.attivaOraGiustaPerTeBioraria);
			offer.clickComponent(offer.bannerGiustaPerTeBioraria);		
			logger.write("verifica offerta GiustaPerTe Bioraria and click on: - Completed");
			
			GiustaPerTeBiorariaComponent gptc= new GiustaPerTeBiorariaComponent(driver);
			
			logger.write("verifica pagina offerta GiustaPerTe Bioraria: - Start");		
			gptc.checkQuestionsDetails();
			gptc.checkDocumentsDetails();
			gptc.checkDocumentiContratto();
			gptc.checkDocumentiGenerali();
			//gptc.containsAllText(gptc.promo, gptc.promoText);
			//gptc.containsText(gptc.offer1, "GiustaPerTe");
			//gptc.containsText(gptc.offer2, "E-Light Bioraria");
			logger.write("verifica pagina offerta GiustaPerTe Bioraria: - Completed");
			
			logger.write("Back del browser: - Start");
			driver.navigate().back();
			Thread.sleep(5000);
			logger.write("Back del browser: - Completed");
			
			logger.write("verifica pagina Vedi tutte promo luce: - Start");
			gptc.clickComponent(gptc.vediTutteLuce);
			Thread.sleep(5000);
			headerMenu.checkUrl(prop.getProperty("LINK_LUCE_CASA_TUTTE"));
//			headerMenu.containsAllText(headerMenu.contractContainer, "Luce");
//			headerMenu.containsAllText(headerMenu.placeContainer, "Casa");
//			headerMenu.containsAllText(headerMenu.optionContainer, "Visualizza tutte");
			logger.write("verifica pagina Vedi tutte promo luce: - Completed");
			
			Thread.sleep(5000);
			
//			logger.write("Back del browser: - Start");
//			driver.navigate().back();
//			headerMenu.checkUrl(prop.getProperty("LINK_GIUSTAPERTE_BIORARIA"));
//			logger.write("Back del browser: - Completed");
			
//			logger.write("Click su Luce e Gas sull'header e torno in HOME / LUCE E GAS - Start");
//			gptc.clickComponent(gptc.headerLuceGas);
//			logger.write("Click su Luce e Gas sull'header e torno in HOME / LUCE E GAS - Completed");
			
			logger.write("verifica pagina Luce-Casa-Cambio fornitore: - Start");		
			headerMenu.selectionOptionsFromListbox("Luce", "Casa", "Cambio Fornitore");
			headerMenu.clickComponent(headerMenu.iniziaOraButton);
			Thread.sleep(5000);
			logger.write("verifica pagina Luce-Casa-Cambio fornitore: - Completed");
			
			logger.write("verifica pagina attivazione Placet Fissa Luce: - Start");	
			headerMenu.clickComponent(headerMenu.placetFissaLuce);
			//headerMenu.clickComponent(headerMenu.attivaOraPlacetFissaLuce);
			Thread.sleep(5000);
			PLACETFissaLuceConsumerComponent placet = new PLACETFissaLuceConsumerComponent(driver);
			placet.clickComponent(placet.attivaOraButton);
			OCR_Module_Component ocr = new OCR_Module_Component(driver);
			Thread.sleep(5000);
			ocr.checkHeader("Enel Energia PLACET Fissa Luce Consumer", driver);
			ocr.clickComponent(ocr.chiudiLogo);
			ocr.jsClickComponent(ocr.esciSenzaSalvare);
			Thread.sleep(5000);
			logger.write("verifica pagina attivazione Placet Fissa Luce: - Completed");	
						
			headerMenu.selectionOptionsFromListbox("Luce", "Casa", "Cambio Fornitore");
			headerMenu.clickComponent(headerMenu.iniziaOraButton);
			
			logger.write("Click dettagli su Placet Fissa Luce - Start");
			headerMenu.clickComponent(headerMenu.placetFissaLuce);
			logger.write("Click dettagli su Placet Fissa Luce - Completed");
						
			logger.write("verifica presenza della spalletta - Start");
			placet.checkUrl(prop.getProperty("LINK_PLACET_FISSA_LUCE"));
			placet.verifyComponentExistence(placet.dettagliSplattetta);
			placet.clickComponent(placet.attivaOraButton);
			//placet.containsAllText(placet.dettagliSplattetta, placet.dettagli_Spalletta);
			//placet.containsAllText(placet.scrittaPiccolaSpalletta, placet.scritta_in_piccolo);
			logger.write("verifica presenza della spalletta - Completed");
			
			ocr.checkHeader("Enel Energia PLACET Fissa Luce Consumer", driver);
			ocr.verifyComponentExistence(ocr.chiudiLogo);
			ocr.clickComponent(ocr.chiudiLogo);
			ocr.verifyComponentExistence(ocr.esciSenzaSalvare);
			ocr.jsClickComponent(ocr.esciSenzaSalvare);
			
			
			placet.checkUrl(prop.getProperty("LINK"));
			
			
//			
//			
//			logger.write("verifica presenza della spalletta dopo lo scroll della pagina- Start");
//			placet.scrollComponent(placet.documentigenerali);
//			placet.verifyComponentExistence(placet.dettagliSplattetta);
//			logger.write("verifica presenza della spalletta dopo lo scroll della pagina- Completed");
//			TimeUnit.SECONDS.sleep(1);
//			
//			logger.write("Click su Attiva ora Placet fissa luce e verifica pagina - Start");
//			placet.clickComponent(placet.attivaoraspalletta);
//			
//			Thread.sleep(5000);
//			
//			ocr.checkHeader("Enel Energia PLACET Fissa Luce Consumer", driver);
//			ocr.clickComponent(ocr.chiudiLogo);
//			ocr.jsClickComponent(ocr.esciSenzaSalvare);
//			placet.checkUrl(prop.getProperty("LINK"));
//			logger.write("Click su Attiva ora Placet fissa luce e verifica pagina - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
