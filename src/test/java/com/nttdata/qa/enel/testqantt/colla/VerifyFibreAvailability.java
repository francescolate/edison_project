package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.HomePageHeaderComponent;
import com.nttdata.qa.enel.components.colla.FibreVerificationComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.FibraDiMelitaComponent;
import com.nttdata.qa.enel.components.colla.FibreLeadStep2Component;
import com.nttdata.qa.enel.components.colla.FibreOkComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyFibreAvailability {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			//System.out.println("Getting driver instance: " + driver);
			//** Start Set dati da verificare
			String BASE_LINK = prop.getProperty("LINK");
			
			//**END Set Dati da verificare
			//System.out.println("Accessing home page");
			logger.write("Accessing home page - Start");
			LoginLogoutEnelCollaComponent homePage = new LoginLogoutEnelCollaComponent(driver);
			homePage.launchLink(prop.getProperty("LINK"));
			By logo = homePage.logoEnel;
			homePage.verifyComponentExistence(logo);
			
			//Click chusura bunner pubblicitario se esistente
			homePage.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

			
			By acceptCookieBtn = homePage.buttonAccetta;
			homePage.verifyComponentExistence(acceptCookieBtn);
			homePage.clickComponent(acceptCookieBtn);
			logger.write("Accessing home page - Completed");
			
			//System.out.println("Verifying home page header");
			logger.write("Verifying home page header - Start");
			HomePageHeaderComponent header = new HomePageHeaderComponent(driver);
			header.verifyComponentExistence(header.luceGasBtn);
			header.verifyComponentExistence(header.enterpriseBtn);
			//header.verifyComponentExistence(header.loyaltyBtn);
			header.verifyComponentExistence(header.storiesBtn);
			header.verifyComponentExistence(header.futurE_Btn);
			header.verifyComponentExistence(header.mediaBtn);
			header.verifyComponentExistence(header.supportBtn);
			header.verifyComponentExistence(header.loginMenuBtn);
			header.verifyComponentExistence(header.searchMenuBtn);
			header.verifyComponentExistence(header.hamburgerMenu);
			logger.write("Verifying home page header - Completed");
			
			System.out.println("Accessing fibre verification page");
			logger.write("Accessing fibre verification page - Start");
			Thread.sleep(5000);
			header.clickComponent(header.hamburgerMenu);
			header.verifyComponentExistence(header.fibreSelectionBtn);
			header.clickComponent(header.fibreSelectionBtn);
			logger.write("Accessing fibre verification page - Completed");
			
			System.out.println("Verifying verification page text");
			logger.write("Verifying verification page text - Start");
			FibreVerificationComponent verificationPage = new FibreVerificationComponent(driver);
			verificationPage.verifyPageStrings();
			logger.write("Verifying verification page text - Completed");
			
			System.out.println("Verifying address coverage section");
			logger.write("Verifying address coverage section - Start");
			verificationPage.verifyComponentVisibility(verificationPage.sectionHeader);
			verificationPage.verifyComponentVisibility(verificationPage.cityField);
			verificationPage.verifyComponentVisibility(verificationPage.addressField);
			verificationPage.verifyComponentVisibility(verificationPage.streetNumberField);
			verificationPage.verifyComponentVisibility(verificationPage.coverageVerificationBtn);
			verificationPage.verifyComponentVisibility(verificationPage.findAddressLink);
			logger.write("Verifying address coverage section - Completed");
			
			System.out.println("Verifying empty input fields error");
			logger.write("Verifying empty input fields error - Start");
			verificationPage.clickComponent(verificationPage.coverageVerificationBtn);
			verificationPage.checkEmptyFieldsError();
			logger.write("Verifying empty input fields error - Completed");
			
			System.out.println("Step2 text verification");
			logger.write("Step2 text verification - Start");
			verificationPage.clickComponent(verificationPage.findAddressLink);
			FibreLeadStep2Component step2 = new FibreLeadStep2Component(driver);
			step2.confirmUrl(prop.getProperty("LINK_FIBRE_LEAD_STEP_2"));
			step2.checkPageElements();
			logger.write("Step2 text verification - Completed");
			
			System.out.println("Invalid address test");
			logger.write("Invalid address test - Start");
			driver.navigate().back();
			verificationPage.insertAddress(
					prop.getProperty("INVALID_ADDRESS_CITY"),
					prop.getProperty("INVALID_ADDRESS_STREET"),
					prop.getProperty("INVALID_ADDRESS_STREET_NUMBER"));
			verificationPage.clickComponent(verificationPage.coverageVerificationBtn);
			step2.confirmUrl(prop.getProperty("LINK_FIBRE_LEAD_STEP_2"));
			String completeAddress = prop.getProperty("INVALID_ADDRESS_STREET") + ", " + prop.getProperty("INVALID_ADDRESS_STREET_NUMBER") + " " + prop.getProperty("INVALID_ADDRESS_CITY");
			step2.checkPageElementsForInvalidAddress(completeAddress);
			driver.navigate().back();
			logger.write("Invalid address test - Start");
			
			System.out.println("Valid address test");
			logger.write("Valid address test - Start");
			verificationPage.insertAddress(
					prop.getProperty("VALID_ADDRESS_CITY"),
					prop.getProperty("VALID_ADDRESS_STREET"),
					prop.getProperty("VALID_ADDRESS_STREET_NUMBER"));
			verificationPage.clickComponent(verificationPage.coverageVerificationBtn);
			FibreOkComponent okPage = new FibreOkComponent(driver);
			okPage.confirmUrl(prop.getProperty("LINK_FIBRE_OK"));
			String validAddress = prop.getProperty("VALID_ADDRESS_STREET") + ", " + prop.getProperty("VALID_ADDRESS_STREET_NUMBER") + " " + prop.getProperty("VALID_ADDRESS_CITY");
			okPage.checkPageElements(validAddress);
			logger.write("Valid address test - Coompleted");
			
			logger.write("Verify Footer - Start");
			FooterPageComponent fpc = new FooterPageComponent(driver);
			fpc.verifyFooter();
			logger.write("Verify Footer - Completed");
			
			logger.write("Verify page Melita - Start");
			Thread.sleep(10000);
			FibraDiMelitaComponent fmc = new FibraDiMelitaComponent(driver);
			fmc.checkHeaderPage();
			fmc.checkTextPageOK();
			fmc.checkTextOperatore();
			logger.write("Verify page Melita - Completed");
			
			
			
			
			/*
			System.out.println("Accessing login page");
			logger.write("Accessing login page - Start");
			okPage.clickComponent(okPage.loginAccessBtn);
			//Even if okPage is not the currently displayed page, as a BaseComponent it's still able to confirm the required url
			okPage.confirmUrl(prop.getProperty("LINK_LOGIN_PAGE"));
			homePage.verifyComponentExistence(homePage.username);
			homePage.verifyComponentExistence(homePage.password);
			homePage.verifyComponentExistence(homePage.buttonLoginAccedi);
			logger.write("Accessing login page - Completed");
			*/
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		
	}
	
}
