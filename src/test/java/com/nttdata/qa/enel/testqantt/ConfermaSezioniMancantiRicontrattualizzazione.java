package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.FatturazioneElettronicaComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ConfermaSezioniMancantiRicontrattualizzazione extends BaseComponent {
	SeleniumUtilities util;
	SpinnerManager spinner;
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			//confermo le 4 sezioni mancanti dopo aver fatto salva in bozza e modifica offerta
			
			logger.write("Conferma Fatturazione Elettronica - Start");
            FatturazioneElettronicaComponent fatturazione = new FatturazioneElettronicaComponent(driver);
            fatturazione.pressButton(fatturazione.confermaFatturazioneElettronicaSWAEVO);
            logger.write("Conferma Fatturazione Elettronica - Completed");

            CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
        	logger.write("Selezione Indirizzo di Fatturazione - Start");
			compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Fatturazione");
			logger.write("Selezione Indirizzo di Fatturazione - Completed");
			
			ContattiConsensiComponent contatti = new ContattiConsensiComponent(driver);
			contatti.press(contatti.confermaButton);
			
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			modalita.clickComponentIfExist(modalita.confermaButton);
			
			//verifica presenza conferma globale abilitato
			SceltaProcessoComponent confermaOfferta = new SceltaProcessoComponent(driver);
			confermaOfferta.scrollComponent(confermaOfferta.confermaButton);
            
            
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}

