package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerificaMancataCreazioneRichiesta_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			String statopratica="vuota";

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Interrogazione","Report Post Sales");
				//Inserimento POD
				R2D_VerifichePodComponent verifichePod = new R2D_VerifichePodComponent(driver);
				if(prop.getProperty("SKIP_POD","N").equals("N")){
					verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD_ELE",prop.getProperty("POD")));
				}
				//Inserimento ID richiesta CRM
				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA",prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA"))));
				//Se Allaccio o Subentro combinato viene inserito Codice classe servizio DT
				//////
				String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE","");
				//if (prop.getProperty("ALLACCIO","N").equals("Y")||prop.getProperty("SUBENTRO_COMBINATO_R2D","N").equals("Y")||prop.getProperty("MODIFICA_POTENZA_TENSIONE","N").equals("Y")||prop.getProperty("SPOSTAMENTO_CONTATORE_OLTRE_10M","N").equals("Y")||prop.getProperty("VERIFICA_CONTATORE","N").equals("Y")){
//				if (tipoOperazione.equals("ALLACCIO")||tipoOperazione.equals("SUBENTRO_COMBINATO_R2D")||tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")||tipoOperazione.equals("SPOSTAMENTO_CONTATORE_OLTRE_10M")||tipoOperazione.equals("VERIFICA_CONTATORE")){	
				if (tipoOperazione.equals("ALLACCIO_EVO")||tipoOperazione.equals("SUBENTRO_COMBINATO")||tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")||tipoOperazione.equals("SPOSTAMENTO_CONTATORE_OLTRE_10M")||tipoOperazione.equals("VERIFICA_CONTATORE")){	
					verifichePod.selezionaCodiceServizioDt(verifichePod.selectCodiceServizioDt,prop.getProperty("CODICE_SERVIZIO_DT_R2D"));
					if(prop.getProperty("ACCETTAZIONE_PREVENTIVO_R2D","N").equals("Y")){
						verifichePod.inserisciAccettazionePreventivo(verifichePod.inputValidazionePreventivo, "Y");
					}
				}
				
				//flag fittizio a si
//				if (prop.getProperty("FLAG_FITTIZIO","").contentEquals("Y")) {
				if (prop.getProperty("FLAG_FITTIZIO","N").equals("Y")) {
					verifichePod.selezionaPratica(verifichePod.flagFittizioSi);
				}
				//Cerca
				verifichePod.cercaPod(verifichePod.buttonCerca);
				//Click Dettaglio Pratiche
				logger.write("Verifica mancata creazione richiesta su R2D - Start");
				//Se la ricerca non trova nessun record, significa che come ci si aspettava, non è stata creata alcuna richiesta su R2D
				//e il test termina correttamente
				if(verifichePod.verificaSeEsiste(verifichePod.buttonDettaglioPratiche)){
					throw new Exception("La pratica è erroneamente stata creata su R2D");
				}
				logger.write("Verifica mancata creazione richiesta su R2D - Completed");
				}

			prop.setProperty("RETURN_VALUE", "OK");
			
		}catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
