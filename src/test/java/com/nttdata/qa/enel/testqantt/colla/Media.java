package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Storie_FuturE_MediaSectionsComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Media {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				
			logger.write("sull' Home page al link '"+prop.getProperty("LINK")+"' click sul tab 'Media' nell' Header e check del link della pagina Media in apertura - Start ");
			Thread.sleep(10000);
			TopMenuComponent menu=new TopMenuComponent(driver);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			//menu.sceltaMenu("Media");	
			log.launchCorporateLink(prop.getProperty("LINK_MEDIA"));
			Storie_FuturE_MediaSectionsComponent media = new Storie_FuturE_MediaSectionsComponent(driver);
			media.checkUrl(prop.getProperty("LINK_MEDIA"));
			logger.write("sull' Home page al link '"+prop.getProperty("LINK")+"' click sul tab 'Media' nell' Header e check del link della pagina Media in apertura - Completed ");
			
			/*logger.write("sulla pagina web con link '"+prop.getProperty("LINK_MEDIA")+"' e' presente l' Header - Start ");
			By enelLogo=media.logoEnel;
			media.verifyComponentExistence(enelLogo);
						
			By electricityandgasStorieLabel=media.luceAndGasTab;
			media.verifyComponentExistence(electricityandgasStorieLabel);
			
			By companiesStorieLabel=media.impreseTab;
			media.verifyComponentExistence(companiesStorieLabel);
			
			By withyouStorieLabel=media.insiemeateTab;
			media.verifyComponentExistence(withyouStorieLabel);
		    
			By storieStorieLabel=media.storieTab;
			media.verifyComponentExistence(storieStorieLabel);
		    
			By futur_eStorieLabel=media.futureTab;
			media.verifyComponentExistence(futur_eStorieLabel);
			
			By mediaStorieLabel=media.mediaTab;
			media.verifyComponentExistence(mediaStorieLabel);
			
			By supportStorieLabel=media.supportoTab;
			media.verifyComponentExistence(supportStorieLabel);	
			
			HeaderComponent login = new HeaderComponent(driver);
			By userIcon=login.iconUser;
			login.verifyComponentExistence(userIcon);
			By glassIcon=login.magnifyingglassIcon;
			login.verifyComponentExistence(glassIcon);
			
			HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
			By menuIcon=hamburger.iconMenu;
			hamburger.verifyComponentExistence(menuIcon);
			logger.write("sulla pagina web con link '"+prop.getProperty("LINK_MEDIA")+"' e' presente l' Header - Completed ");
			
			logger.write("check sulla presenza del testo, links e icone social sul footer della pagina web '"+prop.getProperty("LINK_MEDIA")+"' - Start ");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
				
			By linkInformazioniLegali=media.informazioniLegaliLink;
			media.verifyComponentExistence(linkInformazioniLegali);
			media.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=media.creditsLink;
			media.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=media.privacyLink;
			media.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=media.cookieLink;
			media.verifyComponentExistence(linkCookie);
			
			By linkRemit=media.remitLink;
			media.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=media.twitterIcon;
			media.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=media.facebookIcon;
			media.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=media.youtubeIcon;
			media.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=media.linkedinIcon;
			media.verifyComponentExistence(iconLinkedin);
			logger.write("check sulla presenza del testo, links e icone social sul footer della pagina web '"+prop.getProperty("LINK_MEDIA")+"' - Completed ");
			
			logger.write("click sul link 'Privacy' e check del suo link - Start");
			media.clickComponent(linkPrivacy);
			TimeUnit.SECONDS.sleep(1);
			By privacyAccess=media.privacyPage;
			media.verifyComponentExistence(privacyAccess);
			media.checkUrl(prop.getProperty("LINK_PRIVACY"));
			logger.write("click sul link 'Privacy' e check del suo link - Completed");
			
			logger.write("back alla pagina web con url '"+prop.getProperty("LINK_MEDIA")+"' - Start");
			driver.navigate().back();
			
			menu.checkUrl(prop.getProperty("LINK_MEDIA"));
			media.verifyComponentExistence(enelLogo);
			logger.write("back alla pagina web con url '"+prop.getProperty("LINK_MEDIA")+"' - Completed");
			
			
		 	logger.write("click sul tab 'SUPPORTO' nell' Header della pagina web con url '"+prop.getProperty("LINK_MEDIA")+"' e check del suo link - Start");
			 menu.sceltaMenu("SUPPORTO");
				
			 menu.checkUrl(prop.getProperty("LINK_SUPPORTO"));
			 By supporto_Title=media.supportoTitle;
			 media.verifyComponentExistence(supporto_Title);
			 logger.write("click sul tab 'SUPPORTO' nell' Header della pagina web con url '"+prop.getProperty("LINK_MEDIA")+"' e check del suo link - Completed");
			*/
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
