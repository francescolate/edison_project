package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.ServiziComponent ;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Autolettura_ACR_353 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ServiziComponent  sz = new ServiziComponent (driver);
			ServicesComponent sc = new ServicesComponent(driver);
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			sz.comprareText(sz.homepageDetail1, sz .HOMEPAGEDETAIL, true);
			sz.comprareText(sz.homepageheadingdetail, sz .HOMEPAGEHEADINGDETAIL, true);
			sz.comprareText(sz.homepageheadingdetail2, sz .HOMEPAGEHEADINGDETAIL2, true);
			sz.comprareText(sz.homepageParagraphtext, sz .HOMEPAGE_PARAGRAPHTEXT, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");			
			logger.write("Click on service and verify the Supply service section -- Starts");
			sz.clickComponent(sz.servicestext);
			sz.verifyComponentExistence(sz.serviceTitle);	
			Thread.sleep(20000);
			logger.write("Click on service and verify the Supply service section -- Ends");

			logger.write("Click on autolettura on service section -- Starts");
			sz.verifyComponentExistence(sz. AUTOLETTURA);
			sz.clickComponent(sz. AUTOLETTURA);
			Thread.sleep(20000);
			logger.write("Click on autolettura on service section -- Ends");

			logger.write("Verify autolettura page title and content -- Starts");
			sz.verifyComponentExistence(sz.headingautolettura);
			sz.comprareText(sz.headingautolettura, sz.HEADING_autolettura, true);
			sz.comprareText(sz.subtitleautolettura, sz.SUBTITLEautolettura, true);
			sz.comprareText(sz.titleautolettura, sz.TITLE_autolettura, true); 
			logger.write("Verify information display and contents -- start");
			sz.clickComponent(sz.informationX);
			sz.verifyComponentExistence(sz.popUp);
			sz.clickComponent(sz.popUptext);
			sz.clickComponent(sz.closePopUp);
			logger.write("Verify data visible and contents -- start");
			sz.verifyComponentExistence(sz.nome);
			logger.write("Verify data visible and contents -- end");
			logger.write("Check presence of Fotter Section -- Start");
			//sz.verifyFooterVisibility();
			logger.write("Check presence of Fotter Section -- Completed");		
			logger.write("Verify supply details - Start");
			sc.verifyComponentExistence(sc.Indirizzodellafornitura);
			sc.verifyComponentExistence(sc.NumeroCliente);
			sc.verifyComponentExistence(sc.linkStoricoLetture);
			sc.verifyComponentExistence(sc.esciButton);
			sc.verifyComponentExistence(sc.comminica);
			logger.write("Verify supply details - Complete");
						
			sz.verifyComponentExistence(sz.esci);
			sz.verifyComponentExistence(sz.comunicalettura);
            logger.write("Verify contents in page  - Complete");

			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
			finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
