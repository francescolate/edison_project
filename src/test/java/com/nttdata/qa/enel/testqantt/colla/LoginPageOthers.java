package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LoginPageOthers {
	

		public static void main(String[] args) throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
				prop.setProperty("LINK_FAQ", prop.getProperty("LINK")+"it/supporto/faq/faq-area-clienti");
				prop.setProperty("LINK_FORGOT_PASSWD", prop.getProperty("LINK")+"it/recupera-password");
				prop.setProperty("LINK_FORGOT_USR", prop.getProperty("LINK")+"it/recupera-username");
				prop.setProperty("SIGNIN", prop.getProperty("LINK")+"it/registrazione");

				logger.write("apertura del portale web Enel di test - Start");
				//Login Page
				LoginPageComponent log = new LoginPageComponent(driver);
				
				log.launchLink(prop.getProperty("LINK"));
				logger.write("apertura del portale web Enel di test - Completed");
				
				logger.write("check sulla presenza del logo Enel - Start");
				By logo = log.logoEnel;
				log.verifyComponentExistence(logo);// verifica esistenza logo enel
				logger.write("check sulla presenza del logo Enel - Completed");
				
				//Click chusura bunner pubblicitario se esistente
				log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

				
				By accettaCookie = log.buttonAccetta;
				log.verifyComponentExistence(accettaCookie);
				log.clickComponent(accettaCookie);
	            
				logger.write("click su icona utente - Start");
				By icon = log.iconUser;
				log.verifyComponentExistence(icon);
				log.clickComponent(icon); 
				logger.write("click su icona utente - Completed");
				
				logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
				By pageLogin = log.loginPage;
				log.verifyComponentExistence(pageLogin);
				logger.write("Check login page - completed");
				
				logger.write("Check the user name and password fields - start");
				By user = log.username;
				log.verifyComponentExistence(user);
				By pw = log.password;
				log.verifyComponentExistence(pw);
				logger.write("Check the user name and password fields - completed");
				
				logger.write("Check the Login button - start");
				By accedi = log.buttonLoginAccedi;
				log.verifyComponentExistence(accedi);
				logger.write("Check the Login button - completed");
  
				By recName = log.RecUserName;
		        log.verifyComponentExistence(recName);
		        By RecPasswd=log.RecUserPasswd;
		        log.verifyComponentExistence(RecPasswd);
		        
		       // By probLogin =log.LoginProb;
		        //log.verifyComponentExistence(probLogin);
		
		        By linkProbLogin=log.LoginProbLink;
		        log.verifyComponentExistence(linkProbLogin);
		        log.clickComponent(linkProbLogin);
		        
		     //TopMenuComponent
		        TopMenuComponent menu=new TopMenuComponent(driver);
		        menu.checkUrl(prop.getProperty("LINK_FAQ"));
		        
		        
		        Thread.sleep(5000);
		        log.backBrowser();
		        
		        logger.write("Check the google and facebook  - start");
		        By signInFacebook=log.FacebookSignIn;
		        log.verifyComponentExistence(signInFacebook);
		        By signInGoogle=log.GoogleSignIn;
		        log.verifyComponentExistence(signInGoogle);
		        logger.write("Check the google and facebook - completed");
		        
		        logger.write("Check the forgot Password and back to login page  - start"); 
		        log.clickComponent(RecPasswd);
		       
		       menu.checkUrl(prop.getProperty("LINK_FORGOT_PASSWD"));
		        Thread.sleep(4000);
		        log.backBrowser();
		        logger.write("Check the forgot Password and back to login page - completed"); 
		        
		        log.clickComponent(recName);
		        
		        menu.checkUrl(prop.getProperty("LINK_FORGOT_USR"));
		        log.backBrowser();
		        
		        logger.write("Clicking on the register button and back to login page - start"); 
		        By register=log.SignIn;
		        log.verifyComponentExistence(register);
		        log.clickComponent(register);	
		        
		        menu.checkUrl(prop.getProperty("SIGNIN"));
		        log.backBrowser();
		        logger.write("Clicking on the register button and back to login page - completed"); 
		        
		        
		        
		  /*   logger.write("click on login button without username and password - start");
		        log.clickComponent(accedi);
		        logger.write("clicked on login button without username and password - completed");
		        By validateUser=log.UsernameValidation;
		        log.verifyComponentExistence(validateUser);
		        
		        By validatePasswd=log.PasswdValidation;
		        log.verifyComponentExistence(validatePasswd);
		        driver.navigate().refresh();
		        thread.sleep(5000);
		        log.clickComponent(accedi);
		    */  
		        logger.write("Enter the user name and password in  login page - start"); 
		     	log.enterLoginParameters(user, prop.getProperty("USERNAME")); 
		
				log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
				
				log.verifyComponentExistence(accedi);
				log.clickComponentIfExist(accedi); 
				logger.write("Enter the user name and password in  login page - completed"); 
				
				logger.write("Verify the Private and Business buttons - start"); 
				By privateRegister=log.RegisterPrivate;
		        log.verifyComponentExistence(privateRegister);
		        
		        By businessRegister=log.RegisterBusiness;
		        log.verifyComponentExistence(businessRegister);
		        logger.write("Verify the Private and Business buttons - Completed"); 
		        
		        logger.write("Click on Private area button - start"); 
		        By PrivateBtn = log.RegisterPrivateBtn;				
				log.clickComponent(PrivateBtn);
				logger.write("Click on Private area button - completed");
				
				if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
					log.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
		       
				//Home Page 

				logger.write("Verify the left menu list  - Start");
				log.homepageRESIDENTIALMenu(log.LeftMenuList);
				logger.write("Verify the left menu list  - Completed");
								  
				prop.setProperty("RETURN_VALUE", "OK");
				
			} catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}

	}

}
