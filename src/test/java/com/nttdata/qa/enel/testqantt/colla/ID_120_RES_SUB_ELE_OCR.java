package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertagasComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_120_RES_SUB_ELE_OCR {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			prop.setProperty("LUCEEGASLINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			OffertagasComponent oc =new OffertagasComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);

			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);

			PublicAreaComponent pa = new PublicAreaComponent(driver);
			Thread.sleep(5000);
			logger.write("select drop down value - Start");
			pa.changeDropDownValue(pa.productDropDown, pa.luce);
			pa.changeDropDownValue(pa.placeDropDown, pa.casa);
			pa.changeDropDownValue(pa.myselectionDD, pa. visualiza);
			logger.write("Click on inizia ora - Start");
			pa.clickComponent(pa.iniziaOra);
			Thread.sleep(3000);
			logger.write("Click on inizia ora - Completed");
			pa.clickComponent(pa.dettaglioOfferta);

			oc.clickComponent(oc.attivaSubito);
			logger.write("Verify page navigation to attiva Offerta page and page details -- start");
			oc.verifyComponentExistence(oc.compilaManualmente);
			oc.clickComponent(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");

			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Start");
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(2000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(25000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");

			logger.write("Verify and Enter the values in informazioniFornitura section-- Strat");
			oc.clickComponent(oc.subEntro);
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);
			oc.enterInputParameters(oc.pod, "IT004E" + PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			oc.enterInputParameters(oc.capNew, prop.getProperty("CAP"));
			Thread.sleep(10000);
			oc.clickComponent(oc.capdropdown);
			//oc.clickComponent(oc.capdropdownNewUpdated);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.clickComponent(oc.prosegui1);			
			
			
			prop.setProperty("RETURN_VALUE", "OK");

		} 
		catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} 
		finally
		{
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
