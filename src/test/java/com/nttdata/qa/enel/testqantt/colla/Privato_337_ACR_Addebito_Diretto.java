package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_337_ACR_Addebito_Diretto {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			/*WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
				
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
	
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
												
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
		
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			*/
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			df.verifyComponentExistence(df.fornitureHeader);
			df.comprareText(df.fornitureHeader, DettaglioFornitureComponent.FORNITURE_HEADER, true);
			df.verifyComponentExistence(df.fornitureContent);
			df.comprareText(df.fornitureContent, DettaglioFornitureComponent.FORNITURE_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
			
			logger.write("Click on Dettaglio fornitura button  - Start");
			df.verifyComponentExistence(df.dettaglioForniuturaButton);
			df.clickComponent(df.dettaglioForniuturaButton);
			logger.write("Click on Dettaglio fornitura button  - Complete");
			
			logger.write("Click on Addebitto  button  - Start");
			df.verifyComponentExistence(df.addebitoDirettoLink);
			df.clickComponent(df.addebitoDirettoLink);
			logger.write("Click on Addebitto  button button  - Complete");
			
			logger.write("Click on Addebitto header and contents  - Start");
			df.verifyComponentExistence(df.addebitoDirettoHeader);
			df.comprareText(df.addebitoDirettoHeader, DettaglioFornitureComponent.ADDEBITTODIRECTO_HEADER, true);
			df.verifyComponentExistence(df.addebitoDirettoContent);
			df.comprareText(df.addebitoDirettoContent, DettaglioFornitureComponent.ADDEBITTODIRECTO_CONTENT, true);
			df.verifyComponentExistence(df.activePaymentContent);
			df.comprareText(df.activePaymentContent, DettaglioFornitureComponent.ACTIVEPAYMENT_CONTENT, true);
			logger.write("Click on Addebitto header and contents  - Complete");
			
			logger.write("Verify the supply and click on Modifica carta button  - Start");
			df.verifyComponentExistence(df.indrizzo_50053);
			df.verifyComponentExistence(df.modificaCartaButton);
			df.clickComponent(df.modificaCartaButton);
			logger.write("Verify the supply and click on Modifica carta button  - Complete");
			
			logger.write("Enter the inputs and select the dropdowns  - Start");			
			df.verifyComponentExistence(df.emailInput_337);
			df.enterInput(df.emailInput_337, prop.getProperty("EMAILID"));
			df.verifyComponentExistence(df.circuitoDropdown);
			df.selectOptions(prop.getProperty("CIRCUITO"), df.circuitoDropdown);
			df.verifyComponentExistence(df.numerodiCarta);
			df.enterInput(df.numerodiCarta, prop.getProperty("NUMERO"));
			df.verifyComponentExistence(df.scandenzaDropDownMonth);
			df.selectOptions(prop.getProperty("SCANDENZAMONTH"), df.scandenzaDropDownMonth);
			df.verifyComponentExistence(df.scandenzaDropDrownYear);
			df.selectOptions(prop.getProperty("SCANDENZAYEAR"), df.scandenzaDropDrownYear);
			df.verifyComponentExistence(df.cvvInput);
			df.enterInput(df.cvvInput, prop.getProperty("CVV"));
			logger.write("Enter the inputs and select the dropdowns  - Start");
			
			logger.write("Click on privacy checkbox and prosegui button  - Start");
			df.verifyComponentExistence(df.privacyCheckbox);
			df.clickComponent(df.privacyCheckbox);
			df.verifyComponentExistence(df.proseguibutton);
			df.clickComponent(df.proseguibutton);
			logger.write("Click on privacy checkbox and prosegui button  - Start");
			
			
			logger.write("Verify the Operazion contents and status  - Start");
			df.verifyAddebitoStatus();
			df.verifyComponentExistence(df.operazioneContent);
			df.comprareText(df.operazioneContent, DettaglioFornitureComponent.OPERAZIONE_CONTENT, true);
			//df.verifyComponentExistence(df.larichiestaContent);
			//df.comprareText(df.larichiestaContent, DettaglioFornitureComponent.LARICHIESTA_CONTENT, true);
			logger.write("Verify the Operazion contents and status  - Start");
			
			logger.write("Click on Fine button  - Start");
			df.verifyComponentExistence(df.addebitoFineButton);
			df.clickComponent(df.addebitoFineButton);
			logger.write("Click on Fine button  - Start");
			
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
	

}
