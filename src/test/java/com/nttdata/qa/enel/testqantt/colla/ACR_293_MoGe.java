package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_293_MoGe {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			hm.checkForErrorPopup();
			logger.write("Click on Account from residential menu -- Strat");
			hm.clickComponent(hm.account);
			logger.write("Click on Account from residential menu -- Completed");

			AccountComponent ac = new AccountComponent(driver);
			logger.write("Verify page navigation and details in Account page -- Strat");
			ac.verifyComponentExistence(ac.pageTitle);
			ac.comprareText(ac.pageTitle, ac.PAGE_TITLE, true);
			ac.comprareText(ac.titleSubText, ac.TITLE_SUBTEXT, true);
			logger.write("Verify page navigation and details in Account page -- Completed");
			logger.write("Click on Modifica Contatti button-- Strat");
			ac.clickComponent(ac.modificaContatti);
			Thread.sleep(7000);
			logger.write("Click on Modifica Contatti button-- Completed");

			logger.write("Verify Dati e contatti title and subtext-- Strat");
			ac.verifyComponentExistence(ac.datiEContattiTitle);
			ac.comprareText(ac.datiEContattiTitle, ac.PAGE_TITLE, true);
			ac.comprareText(ac.datiEContattiSubText, ac.DATI_E_CONTATTI_SUBTEXT, true);
			logger.write("Verify Dati e contatti title and subtext-- Completed");

			ac.modifyExistingEmail(ac.inputEmail);
			
		    JavascriptExecutor js = (JavascriptExecutor) driver;
		    String email = (String)js.executeScript("return (document.evaluate(\""+"//input[@id='cntEmailFieldInput']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			
			//System.out.println(email);
			prop.setProperty("MODIFIED_EMAIL", email);
			String tele = (String)js.executeScript("return (document.evaluate(\""+"//input[@id='cntPhoneFieldInput']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");

			//System.out.println("Tele "+tele);
			prop.setProperty("TELEFONO_INPUT", tele);
			String CellularePrefix = (String)js.executeScript("return (document.evaluate(\""+"//select[@name='areaCodeMobilePhone']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			String cell =CellularePrefix+ (String)js.executeScript("return (document.evaluate(\""+"//input[@id='cntMobileFieldInput']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");

			prop.setProperty("CELLULAR_INPUT", cell);
			System.out.println(prop.getProperty("CELLULAR_INPUT"));
			ac.clickComponent(ac.SalvaModifiBtn);
			
			Thread.sleep(15000);
			ac.verifyComponentExistence(ac.OperazioneHeading);
			ac.comprareText(ac.OperazioneHeading, ac.OPERAZIONE_HEADING, true);
			ac.comprareText(ac.OperazioneValue, ac.OPERAZIONE_VALUE, true);
			logger.write("Click on Fine button -- Start");
			ac.clickComponent(ac.FineBtn);
			logger.write("Click on Fine button -- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
