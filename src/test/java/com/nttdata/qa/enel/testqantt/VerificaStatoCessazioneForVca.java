package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

/**
 @INFO Verifiche stato cessazione
 @PROPERTY POD   OI__ITEM_CESSAZIONE
 */
public class VerificaStatoCessazioneForVca {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			logger.write("Avvio verifiche finali per cessazione VCA  - Start");
			Map<Integer,Map> queryResult=null;
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			
			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);
			
			
			String pod=prop.getProperty("POD");
			String oiCessazione=prop.getProperty("OI_ITEM_CESSAZIONE");
			
			
			String query=Costanti.query_VCA_OrderItemdaOrderItemId;
			
			String stato,idBpm,R2D_status,SAP_status, SAP_Status_code, SEMPRE_status,SEMPRE_status_code,udb_status;
			
			
			//VERIFICA CESSAZIONE
			logger.write("Esecuzione query");
		
			queryResult=wb.EseguiQuery(query.replace("@ORDER_ITEM_ID@", oiCessazione).replace("@POD@", pod));
			
			logger.write("Estrazione risultati");
			Map row=queryResult.get(1);
			;
			 stato=row.get("NE__Status__c").toString();
			 idBpm=row.get("ITA_IFM_IDBPM__c").toString();
			 pod=row.get("ITA_IFM_POD__c").toString();
			 R2D_status=row.get("ITA_IFM_R2D_Status__c").toString();
			 SAP_status=row.get("ITA_IFM_SAP_Status__c").toString();
			 SAP_Status_code=row.get("ITA_IFM_SAP_StatusCode__c").toString();
			 SEMPRE_status=row.get("ITA_IFM_SEMPRE_Status__c").toString();
			 SEMPRE_status_code=row.get("ITA_IFM_SEMPRE_StatusCode__c").toString();;
			 udb_status=row.get("ITA_IFM_UDB_Status__c").toString();
			 
			 String cess_status_exp_value=prop.getProperty("STATO_CESS");
			 String cess_R2D_status_exp_value=prop.getProperty("STATO_R2D_CESS");
			 String cess_SAP_status_exp_value=prop.getProperty("STATO_SAP_CESS");
			 String cess_SEMPRE_status_exp_value=prop.getProperty("STATO_SEMPRE_CESS");
			 String cess_udb_status_exp_value=prop.getProperty("STATO_UDB_CESS");
			
			 
			 logger.write(String.format("Stato Item - Atteso: %s  -> Effettivo: %s",stato,cess_status_exp_value));
			 logger.write(String.format("Stato R2D - Atteso: %s  -> Effettivo: %s",R2D_status,cess_R2D_status_exp_value));
			 logger.write(String.format("Stato SAP - Atteso: %s  -> Effettivo: %s",SAP_status,cess_SAP_status_exp_value));
			 logger.write(String.format("Stato SEMPRE - Atteso: %s  -> Effettivo: %s",SEMPRE_status,cess_SEMPRE_status_exp_value));
			 logger.write(String.format("Stato UDB - Atteso: %s  -> Effettivo: %s",udb_status,cess_udb_status_exp_value));
			 
			 	if(!stato.toUpperCase().equals(cess_status_exp_value)) {

					throw new Exception(String.format("Cessazione VCA  - Valore  stato non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", stato,cess_status_exp_value));
				}
				if(!R2D_status.toUpperCase().equals(cess_R2D_status_exp_value)) {
					throw new Exception(String.format("Cessazione VCA  - Stati R2D non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", stato,cess_status_exp_value));
				}
				if(!SAP_status.toUpperCase().equals(cess_SAP_status_exp_value)) {
					throw new Exception(String.format("Cessazione VCA  - Stato SAP   non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", SAP_status,cess_SAP_status_exp_value));
				}
				if(!SEMPRE_status.toUpperCase().equals(cess_SEMPRE_status_exp_value)) {
					throw new Exception(String.format("Cessazione VCA  - Stato SEMPRE non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", SEMPRE_status,cess_SEMPRE_status_exp_value));
				}
				if(!udb_status.toUpperCase().equals(cess_udb_status_exp_value)) {
					//throw new Exception(String.format("Cessazione VCA  - Stato UDB non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", udb_status,cess_udb_status_exp_value));
				}
			
			
				logger.write("Cessazione  chiusa con successo");
				logger.write("Avvio verifiche finali per cessazione VCA  - End");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
