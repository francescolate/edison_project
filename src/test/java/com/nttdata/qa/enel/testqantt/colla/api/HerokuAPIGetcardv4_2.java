// HerokuAPIGetcardv4_2 package WEB - COMPARE per gestire errori

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
// import java.util.UUID;
import java.util.UUID;

import com.nttdata.qa.enel.components.colla.api.HerokuAPIGetcardv4Component_Err;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIGetcardv4_2 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Authenticate Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString());
			prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString());
			
			if(prop.getProperty("SET_AUTH")==null)
				prop.setProperty("Authorization", "Bearer ");
			else if(prop.getProperty("SET_AUTH").equals("Y"))
				prop.setProperty("Authorization", "Bearer " + APIService.APILoginStrong(prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"), prop.getProperty("TID"), prop.getProperty("SID")));

			prop.setProperty("JSON_INPUT", "{   \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",         \"sid\": \"" + prop.getProperty("SID") + "\",         \"username\": \"" + prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \"MWS0216808002415\"             }         ]     } }");

			//Da configurare come OutPut
    		// prop.setProperty("Authorization", "Basic QXZ2MGYyRjA6ZjBlWERzSHc=");
    		//prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
    		//prop.setProperty("SOURCECHANNEL","WEB");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIGetcardv4Component_Err HRKComponent = new HerokuAPIGetcardv4Component_Err();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
