

	package com.nttdata.qa.enel.testqantt;

	import java.io.FileOutputStream;
	import java.io.PrintWriter;
	import java.io.StringWriter;
	import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
	import com.nttdata.qa.enel.util.QANTTLogger;
	import com.nttdata.qa.enel.util.WebDriverManager;

	import io.qameta.allure.Step;

	public class PagamentoBollettinoPostaleVolture {

		@Step("Pagamento Bollettino Postale")
		public static void main(String[] args) throws Exception {

			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
					GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
					logger.write("seleziona il primo 'Metodo di pagamento' nella relativa sezione e click su CONFERMA - Start");
					offer.checkValoreInTable2AndClosePopup("Bollettino Postale");
					TimeUnit.SECONDS.sleep(1);
					offer.clickComponent(offer.buttonConfermaMetodoPagamento);
					gestione.checkSpinnersSFDC();
					logger.write("seleziona il primo 'Metodo di pagamento' nella relativa sezione e click su CONFERMA - Completed");
			
				}
				prop.setProperty("RETURN_VALUE", "OK");
			}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}
	}
