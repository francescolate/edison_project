package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class ConfermaIndirizziVolture {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			String statoUbiest=Costanti.statusUbiest;
//INDIRIZZO DI FATTURAZIONE
			if(statoUbiest.compareTo("ON")==0){
				logger.write("Selezione Indirizzo di Fatturazione - Start");
				compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Fatturazione");
				logger.write("Selezione Indirizzo di Fatturazione - Completed");
			}
			else if (statoUbiest.compareTo("OFF")==0){
				logger.write("Selezione Indirizzo di Fatturazione Forzato - Start");
				compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Indirizzo di Fatturazione",prop.getProperty("CAP"),prop.getProperty("CITTA"));
				logger.write("Selezione Indirizzo di Fatturazione Forzato - Completed");
			}

			
//INDIRIZZO DI RESIDENZA			
			if(statoUbiest.compareTo("ON")==0){
				logger.write("Selezione Indirizzo di Residenza/Sede Legale - Start");
				compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
				logger.write("Selezione Indirizzo di Residenza/Sede Legale - Completed");
			}
			else if (statoUbiest.compareTo("OFF")==0){
				logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Start");
				compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Indirizzo di Residenza/Sede Legale",prop.getProperty("CAP"),prop.getProperty("CITTA"));
				logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Completed");
			}

			
			logger.write("Conferma Indirizzo Ultima Fattura - Start");
			compila.pressButton(compila.buttonConfermaIndUltimaFat);
			logger.write("Conferma Indirizzo Ultima Fattura - Completed");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
