package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ForniturePerCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CercaFornituraPerCambioIndirizzoFatturazione {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			SpinnerManager spinner = new SpinnerManager(driver);
			spinner.checkSpinners();
			Thread.currentThread().sleep(10000);
			IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
			String numerorichiesta = nuovaRichiesta
			.salvaNumeroRichiesta2(nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
         	prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());
		
			ForniturePerCambioIndirizzoComponent selectFornitura = new ForniturePerCambioIndirizzoComponent(driver);
			selectFornitura.cercaECliccaFornitura(prop.getProperty("POD"),
					selectFornitura.checkboxPrimaFornituraInTabellaFatturazione);
			spinner.waitForSpinnerBySldsHide(
					By.xpath("//lightning-spinner[@id='spinner' and contains(@class,'slds-is-fixed')]"));
			selectFornitura.clickConfermaFornitura();
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
		
	}
}