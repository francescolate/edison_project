package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.CustomerDetailsPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetails {

	  public static void main(String[] args) throws Exception {

          
          Properties prop = null;
          prop = WebDriverManager.getPropertiesIstance(args[0]);
          QANTTLogger logger = new QANTTLogger(prop);
          RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
         
          try
          {
        	  
        	CustomerDetailsPageComponent cc = new CustomerDetailsPageComponent(driver);
        	Thread.sleep(10000);
        	logger.write("Click on case order Id  -- Start");	
        	cc.verifyComponentExistence(cc.orderId);
  			cc.jsClickObject(cc.orderId);
  			logger.write("Click on case order Id  -- Completed");	
  			Thread.sleep(10000);
        	CaseItemDetailsComponent cd = new CaseItemDetailsComponent(driver);
        	prop.setProperty("POTENZA", "15,0");
        	cd.checkData(By.xpath(cd.datadiprocessoNumber.replace("$VALUE$", prop.getProperty("POTENZA"))), prop.getProperty("POTENZA"));
        	prop.setProperty("TENSIONE", "380");
        	cd.checkData(By.xpath(cd.datadiprocessoText.replace("$VALUE$", prop.getProperty("TENSIONE"))), prop.getProperty("TENSIONE"));
        	cd.clickComponent(cd.annulmentButton);
        	Thread.sleep(40000);
        	cd.selectRinunciaCliente(cd.rinunciaCliente);
        	driver.navigate().refresh();
			Thread.currentThread().sleep(5000);
		//	driver.navigate().refresh();
			Thread.currentThread().sleep(5000);
			prop.setProperty("RETURN_VALUE", "OK");
  		
          } catch (Exception e) {
  			prop.setProperty("RETURN_VALUE", "KO");
  			StringWriter errors = new StringWriter();
  			e.printStackTrace(new PrintWriter(errors));
  			errors.toString();
  			logger.write("ERROR_DESCRIPTION: " + errors.toString());

  			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
  			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
  				throw e;

  		} finally {
  			// Store WebDriver Info in properties file
  			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
  		}
     }
}
