package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_Forzatura_Esiti_3OK_SWA_ELE {

	@Step("R2D Forzature esiti 3OK PER SWA - ELE")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//				String statopratica1=prop.getProperty("STATO_PRATICA_R2D").substring(0, 2);
				String statopratica=prop.getProperty("STATO_R2D").substring(0, 2);
				//Cambio stato in CI solamente se Stato Pratica è AA o AW
				if ((statopratica.compareToIgnoreCase("AA")==0 || (statopratica.compareToIgnoreCase("AW"))==0)) {
					R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
					//Forzatura puntuale pratica in stato CI
					logger.write("Menu Forzatura Puntuale - Start");
					 menuBox.selezionaVoceMenuBox("Forzature","Forzatura Puntuale");
					logger.write("Menu Forzatura Puntuale - Completed");
					TimeUnit.SECONDS.sleep(5);
					R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
					//Selezione tipologia Caricamento
//					logger.write("Selezione tipologia Caricamento - Start");
//					 caricamentoEsiti.selezionaTipoCaricamento("SWA");
//					logger.write("Selezione tipologia Caricamento - Completed");
//					TimeUnit.SECONDS.sleep(5);
					//Click bottone Cerca
					logger.write("Selezione pulsante Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione pulsante Cerca - Completed");
					TimeUnit.SECONDS.sleep(5);
					//int indice=Integer.parseInt(prop.getProperty("INDICE_POD"));
//					if(prop.getProperty("ID_RICHIESTA").isEmpty()){
//						//Inserisci Pod
//						logger.write("Caricamento valore POD - Start");
//						 caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD"));
//						logger.write("Caricamento valore POD - Completed");
//					} else {
						logger.write("inserisci Id_Richiesta - Start");
//						caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.idCRM, prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
						if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.idCRM, prop.getProperty("ID_RICHIESTA"));
						} else {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.idCRM, prop.getProperty("ID_ORDINE"));
						}
						logger.write("inserisci Id_Richiesta - Completed");
//					}
					TimeUnit.SECONDS.sleep(5);
					//Cerca
					logger.write("Selezione pulsante Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione pulsante Cerca - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Seleziona Flag Cambia Stato
					logger.write("Caricamento valore Check Cambia Stato - Start");
					caricamentoEsiti.checkCambiaStato(caricamentoEsiti.inputForzaStato);
					logger.write("Caricamento valore Check Cambia Stato - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Click Seleziona Cambia Stato in CI
					logger.write("Caricamento valore Stato Pratica - Start");
					caricamentoEsiti.selezionaStatoPratica(caricamentoEsiti.inputStato, "CI");
					logger.write("Caricamento valore Stato Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Click Conferma Forzatura
					logger.write("Selezione pulsante Conferma - Start");
					caricamentoEsiti.confermaForzatura(caricamentoEsiti.buttonConfermaForzatura);
					logger.write("Selezione pulsante Conferma - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Salvataggio stato attuale pratica
					logger.write("Aggiornamento Stato Pratica su prop - Start");
					prop.setProperty("STATO_R2D", "CI");
					logger.write("Aggiornamento Stato Pratica su prop - Completed");
				}
			}
			//
//			System.out.println("Forzatura 3OK STATO_PRATICA_R2D: "+ prop.getProperty("STATO_PRATICA_R2D"));
//			System.out.println("Forzatura 3OK STATO: "+ prop.getProperty("STATO_R2D"));
			
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");		
		}
	}
}
