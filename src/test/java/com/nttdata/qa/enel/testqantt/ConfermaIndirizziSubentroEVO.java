package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SFDCBoxSubentroComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class ConfermaIndirizziSubentroEVO {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		QANTTLogger logger = new QANTTLogger(prop);
		SFDCBoxSubentroComponent boxSubentro = new SFDCBoxSubentroComponent(driver);
		RiepilogoOffertaComponent checkCampiSezioneIndirizzi=new RiepilogoOffertaComponent(driver);

		try {
			
			logger.write("check dei campi nella sezione 'Indirizzo Residenza/Sede Legale' e 'Indirizzo di Fatturazione' - Start");
			checkCampiSezioneIndirizzi.verificaCampiIndirizzoResidenza();
			checkCampiSezioneIndirizzi.verificaCampiIndirizzoFatturazione();
			logger.write("check dei campi nella sezione 'Indirizzo Residenza/Sede Legale' e 'Indirizzo di Fatturazione' - Completed");
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			String tipoCliente = prop.getProperty("TIPO_CLIENTE","").toLowerCase();
			SeleniumUtilities util = new SeleniumUtilities(driver);
			String statoUbiest=Costanti.statusUbiest;

			//Se il tasto conferma è abilitato, viene confermata l'intera sezione senza inserire indirizzo
			if(!util.exists(compila.buttonConfermaIndizzoResidenzaDisabilitato,5)){
				compila.pressButton(compila.buttonConfermaIndizzoResidenza);
			}
			else{
				String fornitura_indirizzo=prop.getProperty("CLIENTE_FORNISCE_INDIRIZZO");
				if(prop.getProperty("TIPO_CLIENTE").compareTo("Residenziale")==0 || prop.getProperty("TIPO_CLIENTE").compareTo("Casa")==0){
				//Se il campo 'fornisce indirizzo' è abilitato viene popolato
				if(!util.exists(compila.clienteFornisceIndirizzoSelectDisabilitato,5)){
					logger.write("Selezione campo 'cliente fornisce indirizzo?- Start");
					compila.clienteFornisceIndirizzo(fornitura_indirizzo);
					logger.write("Selezione campo 'cliente fornisce indirizzo?- Completed");
				}
					}
						if(statoUbiest.compareTo("ON")==0){
							logger.write("Selezione Indirizzo di Fatturazione - Start");
							compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
							logger.write("Selezione Indirizzo di Residenza/Sede Legalee - Completed");
						}
						else if (statoUbiest.compareTo("OFF")==0){
							logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Start");
							compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Indirizzo di Residenza/Sede Legale",prop.getProperty("CAP_FORZATURA"),prop.getProperty("CITTA_FORZATURA"));
							logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Completed");
						}
			}

			if (prop.getProperty("INDIRIZZO_FATTURAZIONE", "").contentEquals("NO")) {
				compila.clickCheckIndirizzoFatturazione(compila.checkBoxIndirizzoFatturazione,"Indirizzo di Fatturazione");	
			}
			else if (!prop.getProperty("INSERISCI_INDIRIZZO_FATTURAZIONE", "").equals("")){
	            By container_idf = boxSubentro.container_idf;
	            boxSubentro.verifyComponentExistence(container_idf);
	            boxSubentro.clearAndSendKeys(boxSubentro.idf_input_localita, prop.getProperty("LOCALITA_FATTURA"));
	            boxSubentro.clearAndSendKeys(boxSubentro.idf_input_comune, prop.getProperty("COMUNE_FATTURA"));
	            boxSubentro.clearAndSendKeys(boxSubentro.idf_input_ncivico, prop.getProperty("CIVICO_FATTURA"));
	            boxSubentro.clearAndSendKeys(boxSubentro.idf_input_provincia, prop.getProperty("PROVINCIA_FATTURA"));
	            boxSubentro.clearAndSendKeys(boxSubentro.idf_input_indirizzo, prop.getProperty("INDIRIZZO_FATTURA"));

	            
	            //BUTTON VERIFICA
	            logger.write("Verifica esistenza banner \"Indirizzo non trovato. CANDIDATO ESTRATTO CON CHIAVI FONETICHE.\" - Start");
	            boxSubentro.clickComponentIfExist(boxSubentro.idf_button_verifica);          
	            if(boxSubentro.getElementExistance(boxSubentro.idf_bannerChiaviFonetiche)) {
	            	logger.write("Il banner \"Indirizzo non trovato. CANDIDATO ESTRATTO CON CHIAVI FONETICHE.\"  È presente ed è visibile");
	            } else {
	            	logger.write("Il banner \"Indirizzo non trovato. CANDIDATO ESTRATTO CON CHIAVI FONETICHE.\" Non è presente");
	            }
	            logger.write("Verifica esistenza banner \"Indirizzo non trovato. CANDIDATO ESTRATTO CON CHIAVI FONETICHE.\" - Completed");
	            
	            
	            driver.findElement(boxSubentro.spanForzaIndirizzo).click();
				    
	            WebElement capFattura = driver.findElement(boxSubentro.idf_input_cap);
	            capFattura.sendKeys(prop.getProperty("CAP_FATTURA"));
	            driver.findElement(By.xpath("//*[@id=\"DivmodalModifyAddressABC\"]/form/div/div[2]/div/div[8]/div/div[2]/ul/li[1]/span/div/span/div/span")).click();

	            
	            boxSubentro.verifyClickability(boxSubentro.idf_button_forza);
	            boxSubentro.clickComponent(boxSubentro.idf_button_forza);
	            
	            WebElement indirizzoVerificato = driver.findElement(boxSubentro.idf_indirizzo_verificato);
	                      
	            if(indirizzoVerificato.isDisplayed()) {
	            boxSubentro.clickComponentIfExist(boxSubentro.idf_button_conferma);
	            }
			}
			else {
				if(statoUbiest.compareTo("ON")==0){
					logger.write("Selezione Indirizzo di Fatturazione - Start");
					compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Fatturazione");
					logger.write("Selezione Indirizzo di Fatturazione - Completed");
				}
				else if (statoUbiest.compareTo("OFF")==0){
					logger.write("Selezione Indirizzo di Fatturazione Forzato - Start");
					compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Indirizzo di Fatturazione",prop.getProperty("CAP_FORZATURA",prop.getProperty("CAP")),prop.getProperty("CITTA_FORZATURA",prop.getProperty("CITTA")));
					logger.write("Selezione Indirizzo di Fatturazione Forzato - Completed");
				}
				
			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}

