package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;


public class VerificaRichiestaVas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    driver.switchTo().defaultContent();
            driver.navigate().refresh();
  		    TimeUnit.SECONDS.sleep(7);
		    VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
		    logger.write("Recupero valore id richiesta - Start");
//			System.out.println("Recupero valore id richiesta");
		    verifica.pressJavascript(verifica.elementiRichiestaSection);
//			System.out.println("Recupero OI richiesta");
		    String id = verifica.recuperaOIRichiesta();
	        prop.setProperty("OI_RICHIESTA", id);
//			System.out.println("Valore OI richiesta: "+prop.getProperty("OI_RICHIESTA"));
	        logger.write("Recupero valore id richiesta - Completed");
	        logger.write("Verifica Stati Richiesta - Start");
//			System.out.println("Verifica Stati Richiesta - Start");
//			System.out.println("Stato Richiesta Aspettato: " + prop.getProperty("STATO_RICHIESTA"));
	        verifica.verificaStatiRichiesta(prop.getProperty("STATO_RICHIESTA"), prop.getProperty("STATO_R2D"), prop.getProperty("STATO_SAP"), prop.getProperty("STATO_SEMPRE"));
			verifica.verificaSottoStatiRichiesta(prop.getProperty("SOTTOSTATO_RICHIESTA"));
//			System.out.println("Verifica Stati Richiesta - Completed");
			logger.write("Verifica Stati Richiesta - Completed");
			
	        
	        prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
