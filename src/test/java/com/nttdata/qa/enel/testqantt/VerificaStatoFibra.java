package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaStatoFibra {

	public static void main(String[] args) throws Exception {
		// TODO FR 21.07.2021 Inserire verifica stato fibra in bozza
				/*
				RecuperaStatusCase_and_Order usare per verificare stato fibra
				
				
				
				 	ELEMENTI DELL'OFFERTTA -> //div[contains(text(),"Elementi dell'Offerta")]
					STATO FIBRA -> //strong[text()='Fibra']/ancestor::ul/li/a[contains(text(),'OI')]
					POD FIBRA -> //strong[text()='Fibra']/ancestor::ul/li[span[text()='POD']]/strong
				*/
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
//			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				
			WorkbenchQuery wb=new WorkbenchQuery();	
			wb.Login(args[0]);
			String idRichiest=prop.getProperty("NUMERO_RICHIESTA");
			String statoR2D_expected="";
			String statoSAP_expected="";
			//String statoItem_Expected="BOZZA"; //R3 - vedere mail "R3 - Prima Attivazione id 39  - Stato Fibra dopo emissione ordine - 215678815"
			String statoItem_Expected=prop.getProperty("STATO_ATTESO_FIBRA","IN ATTESA");
			
			String query= "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber,ITA_IFM_Case__c, ITA_IFM_Case__r.Status,ITA_IFM_Case__r.ITA_IFM_SubStatus__c, Name, \n" +
		            "NE__OrderId__r.RecordType.Name,NE__Status__c, ITA_IFM_IDBPM__c, ITA_IFM_POD__c, ITA_IFM_R2D_Status__c,ITA_IFM_R2DOutcomeStatus__c,ITA_IFM_SAP_Status__c,ITA_IFM_SAP_StatusCode__c,ITA_IFM_SAP_StatusDescription__c, ITA_IFM_SEMPRE_Status__c,ITA_IFM_SEMPRE_StatusCode__c,ITA_IFM_SEMPRE_StatusDescription__c, ITA_IFM_UDB_Status__c, \n" +
		            "ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c,ITA_IFM_RecordTypeName__c, NE__BillingProfId__r.NE__Payment__c,ITA_IFM_Temporary_Billing__c, CreatedDate \n" +
		            "FROM NE__OrderItem__c \n" +
		            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' \n"+
		            "and NE__OrderId__r.RecordType.Name = 'Order'\n"+
		            "and ITA_IFM_RecordTypeName__c='Fibra' ";
			query=query.replaceAll("@RICHIESTA@", idRichiest);
			Map<Integer,Map> result=wb.EseguiQuery(query);
			
			String statoItem_Actual=result.get(1).get("NE__Status__c").toString();
			
			//Verifica stato offerta
			logger.write("Verifica stato Fibra");
			if(statoItem_Actual.compareToIgnoreCase(statoItem_Expected)!=0){
				throw new Exception("Lo stato dell'offerta non corrisponde a quello atteso. Valore attuale:"+statoItem_Actual+" valore atteso: "+statoItem_Expected);
				
			}
		}

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
		
		
	}

}
