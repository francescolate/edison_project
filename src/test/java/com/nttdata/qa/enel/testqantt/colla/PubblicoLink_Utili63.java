package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.DiventaNostroPartnerComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PubblicoLink_Utili63 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			log.clickComponent(log.buttonAccetta);

			PublicAreaComponent pc = new PublicAreaComponent(driver);
			pc.waitForLinkUtili(pc.linkutili);
			pc.verifyComponentExistence(pc.linkutili);
			Thread.sleep(5000);
			pc.comprareText(pc.utiliLinks, pc.Text, true);
			logger.write("Click on  diventa nostro partner- Start");
			Thread.sleep(5000);
			pc.clickComponent(pc.diventanostropartner);
			logger.write("Click on  diventa nostro partner- Completed");

			DiventaNostroPartnerComponent dc = new DiventaNostroPartnerComponent(driver);
			logger.write("Verify page title and sub text on diventa nostro partner- Start");
			dc.checkURLAfterRedirection(dc.url);
			pc.comprareText(dc.pageTitle, dc.PageTitle, true);
			pc.comprareText(dc.titleSubText, dc.SubText, true);
			logger.write("Verify page title and sub text on diventa nostro partner- Completed");
			dc.verifyComponentExistence(dc.agenzie);
			pc.comprareText(dc.agenzieText, dc.AgenzieText, true);
			dc.verifyComponentExistence(dc.scopridipiuAgenzie);
			logger.write("Click on scopri di piu- Start");
			dc.clickComponent(dc.scopridipiuAgenzie);
			dc.checkURLAfterRedirection(dc.scopridipiuAgenzieUrl);
			logger.write("Click on scopri di piu- Completed");

			pc.comprareText(dc.offerteTitle, dc.OfferteTitle, true);
			pc.comprareText(dc.offerteSubText, dc.OfferteSubText, true);			
			driver.navigate().back();
			dc.checkURLAfterRedirection(dc.url);
			logger.write("Verify page title and sub text on diventa nostro partner- Start");
			pc.comprareText(dc.pageTitle, dc.PageTitle, true);
			pc.comprareText(dc.titleSubText, dc.SubText, true);
			logger.write("Verify page title and sub text on diventa nostro partner- Completed");

			dc.verifyComponentExistence(dc.smartAgent);
			pc.comprareText(dc.smartAgentSubText, dc.SmartAgentSubText, true);
			dc.verifyComponentExistence(dc.scopridipiuSmartagent);
			logger.write("Click on scopri di piu- Start");
			dc.clickComponent(dc.scopridipiuSmartagent);
			logger.write("Click on scopri di piu- Completed");
			Thread.sleep(5000);
			dc.checkURLAfterRedirection(dc.SmartagentUrl);
			pc.comprareText(dc.smartOffereteTitle, dc.SmartAgent_OfferteTitle, true);
			pc.comprareText(dc.smartAgentSubText, dc.SmartAgent_OfferteSubText, true);
			driver.navigate().back();
			Thread.sleep(5000);
			dc.checkURLAfterRedirection(dc.url);
			logger.write("Verify page title and sub text on diventa nostro partner- Start");
			pc.comprareText(dc.pageTitle, dc.PageTitle, true);
			pc.comprareText(dc.titleSubText, dc.SubText, true);
			logger.write("Verify page title and sub text on diventa nostro partner- Completed");

			dc.verifyComponentExistence(dc.puntoEnel);
			pc.comprareText(dc.puntoEnelSubText, dc.PuntoEnelSubText, true);
			logger.write("Click on scopri di piu- Start");
			Thread.sleep(5000);
			pc.clickComponent(dc.scopridipiuPuntoenel);
			logger.write("Click on scopri di piu- Completed");
			Thread.sleep(5000);
			dc.checkURLAfterRedirection(dc.PuntoenelUrl);
			pc.comprareText(dc.negozioTitle, dc.NegozioTitle, true);
			pc.comprareText(dc.negozioSubText, dc.NegozioSubText, true);
			driver.navigate().back();
			Thread.sleep(5000);
			dc.checkURLAfterRedirection(dc.url);
			logger.write("Verify page title and sub text on diventa nostro partner- Start");
			pc.comprareText(dc.pageTitle, dc.PageTitle, true);
			pc.comprareText(dc.titleSubText, dc.SubText, true);
			logger.write("Verify page title and sub text on diventa nostro partner- Completed");
			
			logger.write("Verify header section - Start");
		//	prop.setProperty("LUCE_E_GAS", "Luce e gas");
			dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("LUCE_E_GAS"));
		//	prop.setProperty("IMPRESA", "Imprese");
			dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("IMPRESA"));
		//	prop.setProperty("INSIEME A TE", "Insieme a te");
		//	dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("INSIEME A TE"));
		//	prop.setProperty("STORIE", "Storie");
			dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("STORIE"));
		//	prop.setProperty("FUTUR-E", "Futur-e");
			dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("FUTUR-E"));
		//	prop.setProperty("MEDIA", "Media");
			dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("MEDIA"));
		//	prop.setProperty("SUPPORTO", "Supporto");
			dc.verifyHeaderVoice(dc.headerVoice,prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");
			
			pc.comprareText(dc.enelItalia, dc.EnelItalia, true);
			pc.comprareText(dc.enelEnergia, dc.EnelEnergia, true);	
			logger.write("Verify footer section - Start");
		//	prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
			dc.verifyFooterLink(dc.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
		//	prop.setProperty("CREDITS", "Credits");
			dc.verifyFooterLink(dc.footerLink, prop.getProperty("CREDITS"));
		//	prop.setProperty("PRIVACY", "Privacy");
			dc.verifyFooterLink(dc.footerLink, prop.getProperty("PRIVACY"));
		//	prop.setProperty("COOKIE_POLICY", "Cookie Policy");
			dc.verifyFooterLink(dc.footerLink, prop.getProperty("COOKIE_POLICY"));
		//	prop.setProperty("REMIT", "Remit");
			dc.verifyFooterLink(dc.footerLink, prop.getProperty("REMIT"));
			logger.write("Verify footer section - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
	
}
