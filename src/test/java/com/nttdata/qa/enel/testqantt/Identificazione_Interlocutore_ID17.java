package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Identificazione_Interlocutore_ID17 {

    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {
        	RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				logger.write("Identificazione interlocutore EVO_v1.0 - ID17 - Start");
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
				logger.write("i campi nome, cognome e CF sono disabilitati - Start");
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoNome);
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoCognome);
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoCF);
				logger.write("i campi nome, cognome e CF sono disabilitati - Completed");
				logger.write("Selezione tipo delega e click su pulsante Conferma e poi di nuovo su Conferma - Start");

				DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
				TimeUnit.SECONDS.sleep(2);
				/*delega.selezionaDelega("Delega light");
				TimeUnit.SECONDS.sleep(1);
				delega.selezionaDelega(prop.getProperty("TIPO_DELEGA"));
				TimeUnit.SECONDS.sleep(1);
				delega.conferma(delega.confermaButton);*/
				delega.verifyComponentExistence(delega.inputTipoDelega);
				if(!prop.getProperty("TIPO_DELEGA", "").equals("")) {
				delega.verificaValoreTipoDelega(prop.getProperty("TIPO_DELEGA"));
				}
				identificaInterlocutore.pressButton(identificaInterlocutore.confermaFinaleInBasso);
				logger.write("Selezione tipo delega e click su pulsante Conferma e poi di nuovo su Conferma - Completed");
				//GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				//gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(3);
				logger.write("Identificazione interlocutore EVO_v1.0 - ID17 - Completed");
				
				
				prop.setProperty("RETURN_VALUE", "OK");
			} catch (Exception e) {
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
