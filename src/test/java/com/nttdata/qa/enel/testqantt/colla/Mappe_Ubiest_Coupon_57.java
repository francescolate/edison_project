package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LuceEGasPubComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Mappe_Ubiest_Coupon_57 {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			LuceEGasPubComponent lpc = new LuceEGasPubComponent(driver);
		
			logger.write("Accessing the home page - Start");
			lpc.launchLinkBasicAuth(prop.getProperty("LINK"));
			lpc.clickComponent(lpc.homePageClose);
			lpc.verifyComponentExistence(lpc.logoEnel);
			lpc.verifyComponentExistence(lpc.buttonAccetta);
			lpc.clickComponent(lpc.buttonAccetta);
			logger.write("Accessing the home page - Complete");
				Thread.sleep(5000);
			logger.write("Click on Luce e gas header  - Start");
			lpc.verifyComponentExistence(lpc.luceGasHeader);
			lpc.clickComponent(lpc.luceGasHeader);
			logger.write("Click on Luce e gas header  - Complete");
			
			logger.write("Verify the luce home path and  title  - Start");
			lpc.verifyComponentExistence(lpc.luceHomePath);
			lpc.containsText(lpc.luceHomePath, lpc.LUCE_HOMEPATH,true);
			lpc.verifyComponentExistence(lpc.luceTitle);
			lpc.containsText(lpc.luceTitle, lpc.LUCE_TITLE);
			logger.write("Verify the luce home path and  title  - Start");
			
			logger.write("Verify and click on community banner Enel premia - Start");
			lpc.verifyComponentExistence(lpc.bannerEnelPremia);
			lpc.clickComponent(lpc.bannerEnelPremia);	
			logger.write("Verify and click on community banner Enel premia - Start");
			
			logger.write("Verify the  community banner Enel premia URL - Start");
			lpc.checkUrl(prop.getProperty("LUCELINK"));
			logger.write("Verify the  community banner Enel premia URL - Complete");
			
			logger.write("Verify the  community banner Enel premia home path and title - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaPath);
			lpc.containsText(lpc.enelPremiaPath, lpc.ENELPREMIA_PATH,true);
			//lpc.verifyComponentExistence(lpc.enelPremiaTitle);
			lpc.verifyComponentExistence(lpc.enelPremiaHeader1);
			//lpc.containsText(lpc.enelPremiaHeader1, LuceEGasPubComponent.ENELPREMIA_HEADER1,true);
			lpc.verifyComponentExistence(lpc.enelPremiaHeader2);
			//lpc.containsText(lpc.enelPremiaHeader2, LuceEGasPubComponent.ENELPREMIA_HEADER2,true);
			logger.write("Verify the  community banner Enel premia home path and title - Complete");
		/*
			logger.write("Verify and click Enel premia Accedi button - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaButton);
			lpc.clickComponent(lpc.enelPremiaButton);
			logger.write("Verify and click Enel premia Accedi button - Complete");
						
					
			logger.write("Click on back browser button- Start");
			lpc.backBrowser(lpc.enelPremiaButton);
			logger.write("Click on back browser button- Complete");
			
			//Step 5
			logger.write("Verify the  community banner Enel premia URL - Start");
			lpc.checkUrl(prop.getProperty("LUCELINK"));
			logger.write("Verify the  community banner Enel premia URL - Complete");
			
			logger.write("Verify the  community banner Enel premia home path and title - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaPath);
			lpc.containsText(lpc.enelPremiaPath, LuceEGasPubComponent.ENELPREMIA_PATH,true);
			logger.write("Verify the  community banner Enel premia home path and title - Complete");
			*/
			
			//Step 7
			logger.write("Verify and click the diventa link - Start");
			lpc.verifyComponentExistence(lpc.diventaCliente);
			lpc.verifyComponentExistence(lpc.Scarica);
			Thread.sleep(3000);
			lpc.clickComponent(lpc.diventaCliente);
			logger.write("Verify and click the diventa link - Complete");
			
			logger.write("Verify LUCE E GAS URL - Start");
			lpc.checkUrl(prop.getProperty("LUCEURL"));
			logger.write("VerifyLUCE E GAS URL - Complete");
			
			logger.write("Verify the luce home path and  title  - Start");
			lpc.verifyComponentExistence(lpc.luceHomePath);
			lpc.containsText(lpc.luceHomePath, lpc.LUCE_HOMEPATH,true);
			lpc.verifyComponentExistence(lpc.luceTitle);
			lpc.containsText(lpc.luceTitle, lpc.LUCE_TITLE);
			logger.write("Verify the luce home path and  title  - Start");
			
			//Step 8

			logger.write("Click on back browser button- Start");
			lpc.backBrowser(lpc.enelPremiaButton);
			logger.write("Click on back browser button- Complete");
			
			logger.write("Verify the  community banner Enel premia URL - Start");
			lpc.checkUrl(prop.getProperty("LUCELINK"));
			logger.write("Verify the  community banner Enel premia URL - Complete");
			
			logger.write("Verify the  community banner Enel premia home path and title - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaPath);
			lpc.containsText(lpc.enelPremiaPath, lpc.ENELPREMIA_PATH,true);
			
			logger.write("Verify the  community banner Enel premia home path and title - Complete");
			
			//Step 9
			logger.write("Verify and click Scarica link - Start");
			lpc.verifyComponentExistence(lpc.Scarica);
			lpc.clickComponent(lpc.Scarica);
			logger.write("Verify and click Scarica link - Complete");
			
			//lpc.verifyPDFUrl(url);
			lpc.SwitchToWindow_2();
			lpc.checkUrl(lpc.PDFLINK);
			lpc.SwitchToWindow();
			//lpc.verifyPDFUrl(lpc.PDFLINK);
			
						
			//Step10
			logger.write("Verify Scarica section contents - Start");
			lpc.verifyComponentExistence(lpc.comeFunziona);
			lpc.containsText(lpc.comeFunziona, lpc.COME_FUNZIONA,true);
			lpc.verifyComponentExistence(lpc.scaricaApp);
			lpc.containsText(lpc.scaricaApp, lpc.SCARICA_APP);
			lpc.verifyComponentExistence(lpc.scaricaAppContent);
			lpc.containsText(lpc.scaricaAppContent, lpc.SCARICAAPP_CONTENT);
			lpc.verifyComponentExistence(lpc.iscriviti);
			lpc.containsText(lpc.iscriviti,lpc.ISCRIVITI);
			lpc.verifyComponentExistence(lpc.iScrivitContent);
			lpc.containsText(lpc.iScrivitContent, lpc.ISCRIVITI_CONTENT);
			logger.write("Verify Scarica section contents - Complete");
			
			//Step11
			logger.write("Verify and click on VAI ALL'APPSTORE link	 - Start");
			lpc.verifyComponentExistence(lpc.allAppStore);
			lpc.waitForElementToDisplay(lpc.allAppStore);
			Thread.sleep(3000);
			lpc.clickComponent(lpc.allAppStore);
			logger.write("Verify and click on VAI ALL'APPSTORE link	 - Complete");
			
			//Steps 12 
			Thread.sleep(4000);
			logger.write("Click on back browser button	 - Start");
			lpc.backBrowser(lpc.enelPremiaButton);
			logger.write("Click on back browser button	 - Complete");
			
			logger.write("Verify the  community banner Enel premia URL - Start");
			lpc.checkUrl(prop.getProperty("LUCELINK"));
			logger.write("Verify the  community banner Enel premia URL - Complete");
			
			logger.write("Verify the  community banner Enel premia home path and title - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaPath);
			lpc.containsText(lpc.enelPremiaPath, lpc.ENELPREMIA_PATH,true);
			lpc.verifyComponentExistence(lpc.enelPremiaHeader1);
			//lpc.containsText(lpc.enelPremiaHeader1, LuceEGasPubComponent.ENELPREMIA_HEADER1,true);
			lpc.verifyComponentExistence(lpc.enelPremiaHeader2);
			//lpc.containsText(lpc.enelPremiaHeader2, LuceEGasPubComponent.ENELPREMIA_HEADER2,true);
			logger.write("Verify the  community banner Enel premia home path and title - Complete");
			
			//Step 13 
				
			logger.write("Verify and click on Google play store link - Start");
			lpc.verifyComponentExistence(lpc.googlePlayStore);
			Thread.sleep(3000);
			lpc.clickComponent(lpc.googlePlayStore);
			
			Thread.sleep(3000);
			logger.write("Verify  Google play store URL - Start");
			lpc.checkUrl(prop.getProperty("GOOGLE_PLAYSTORE"));
			logger.write("Verify  Google play store URL - Complete");
					
			//Step 14			
			
			logger.write("Click on back browser button	 - Start");
			lpc.backBrowser(lpc.enelPremiaButton);
			logger.write("Click on back browser button	 - Complete");
			
			logger.write("Verify the  community banner Enel premia URL - Start");
			lpc.checkUrl(prop.getProperty("LUCELINK"));
			logger.write("Verify the  community banner Enel premia URL - Complete");
			
			logger.write("Verify the  community banner Enel premia home path and title - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaPath);
			lpc.containsText(lpc.enelPremiaPath, lpc.ENELPREMIA_PATH,true);
			
			logger.write("Verify the  community banner Enel premia home path and title - Complete");
						
			//Step 15 
			
			logger.write("Click on GuardaILRegolamento link - Start");
			lpc.verifyComponentExistence(lpc.guardaIlRegolamento);
			lpc.clickComponent(lpc.guardaIlRegolamento);
			logger.write("Click on GuardaILRegolamento - Complete");
			//pdf page validation 
			
			//Step 16
			logger.write("Click on ScopriComeFunziona link - Start");
			lpc.verifyComponentExistence(lpc.scopriComeFunziona);
			lpc.clickComponent(lpc.scopriComeFunziona);
			logger.write("Click on ScopriComeFunziona link - Complete");
			
			//lpc.verifyPDFUrl(LuceEGasPubComponent.PDFLINK1);
			
			lpc.checkUrl(prop.getProperty("COMEFUNZIONA_URL"));
			
			lpc.SwitchToWindow();
			Thread.sleep(2000);
			
			//Step 17 
			logger.write("Click on EnelPremia contents link - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaContents);
			lpc.containsText(lpc.enelPremiaContents, lpc.ENEL_PREMIA_CONTENTS, true);
			logger.write("Click on EnelPremia contents link - Start");
			
			//Step 18 
			logger.write("Click on Regolamento e Dwonload link - Start");
			lpc.verifyComponentExistence(lpc.regolamento);
			lpc.verifyComponentExistence(lpc.download);
			logger.write("Click on Regolamento e Dwonload link - Complete");
			
			
			//Step19 
			logger.write("Click on back browser button	 - Start");
			lpc.backBrowser(lpc.enelPremiaButton);
			logger.write("Click on back browser button	 - Complete");
			
			logger.write("Verify the  community banner Enel premia URL - Start");
			lpc.checkUrl(prop.getProperty("LUCELINK"));
			logger.write("Verify the  community banner Enel premia URL - Complete");
			
			logger.write("Verify the  community banner Enel premia home path and title - Start");
			lpc.verifyComponentExistence(lpc.enelPremiaPath);
			lpc.containsText(lpc.enelPremiaPath, lpc.ENELPREMIA_PATH,true);
			
			logger.write("Verify the  community banner Enel premia home path and title - Complete");
			
			
			//step 20
			logger.write("Verify the Sola App header and contents - Start");
			lpc.verifyComponentExistence(lpc.solaAppHeader);
			lpc.containsText(lpc.solaAppHeader, lpc.SOLAAPP_HEADER);
			lpc.verifyComponentExistence(lpc.solaAppContents);
			lpc.containsText(lpc.solaAppContents, lpc.SOLAAPP_CONTENTS, true);
			logger.write("Verify the Sola App header and contents - Complete");
			
			//step21
			logger.write("Verify the Scegli header and contents - Start");
			lpc.verifyComponentExistence(lpc.scegliHeader);
			lpc.containsText(lpc.scegliHeader, lpc.SCEGLI_HEADER, true);
			lpc.containsText(lpc.scegliContents, lpc.SCEGLI_CONTENTS, true);
			logger.write("Verify the Scegli header and contents - Complete");
			//step 22
			
			
		    lpc.verifyComponentExistence(lpc.guardaIlRegolamento);
		    lpc.clickComponent(lpc.guardaIlRegolamento);
			
			//GAURDALINK
			lpc.SwitchToWindow_2();
			lpc.checkUrl(lpc.GAURDALINK);
			lpc.SwitchToWindow();
			//GAURDALINK
			
			lpc.verifyComponentExistence(lpc.scopriComeFunziona);
			lpc.clickComponent(lpc.scopriComeFunziona);
			
			lpc.backBrowser(lpc.enelPremiaButton);
			
						
			logger.write("Switch to frame  - Start");
			lpc.switchToFrame("full-frame");
			logger.write("Switch to frame  - Complete");
			
			
			Thread.sleep(3000);
			logger.write("Click on MostraFiltri button  - Start");
			lpc.verifyComponentExistence(lpc.mostraFiltributton);
			lpc.jsClickComponent(lpc.mostraFiltributton);
			logger.write("Click on MostraFiltri button  - Complete");
			Thread.sleep(4000);
			
			prop.setProperty("RETURN_VALUE", "OK");
						
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

	
	
}
