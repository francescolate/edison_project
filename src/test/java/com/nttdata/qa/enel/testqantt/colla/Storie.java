package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Storie_FuturE_MediaSectionsComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Storie {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            
			logger.write("nell' Home page al link '"+prop.getProperty("LINK")+"' click sul tab 'Storie' presente sul suo Header e check del link della pagina Storie in apertura - Start ");
			TopMenuComponent menu=new TopMenuComponent(driver);
			menu.sceltaMenu("Storie");		
			Storie_FuturE_MediaSectionsComponent storie = new Storie_FuturE_MediaSectionsComponent(driver);
			//storie.checkUrl(prop.getProperty("LINK_STORIE"));
			logger.write("nell' Home page al link '"+prop.getProperty("LINK")+"' click sul tab 'Storie' presente sul suo Header e check del link della pagina Storie in apertura - Completed ");
			
			/*
			logger.write("sulla pagina web con link "+prop.getProperty("LINK_STORIE")+" e' presente l' Header - Start ");
			By enelLogo=storie.logoEnel;
			storie.verifyComponentExistence(enelLogo);
						
			By electricityandgasStorieLabel=storie.luceAndGasTab;
			storie.verifyComponentExistence(electricityandgasStorieLabel);
			
			By companiesStorieLabel=storie.impreseTab;
			storie.verifyComponentExistence(companiesStorieLabel);
			
			By withyouStorieLabel=storie.insiemeateTab;
			storie.verifyComponentExistence(withyouStorieLabel);
		    
			By storieStorieLabel=storie.storieTab;
			storie.verifyComponentExistence(storieStorieLabel);
		    
			By futur_eStorieLabel=storie.futureTab;
			storie.verifyComponentExistence(futur_eStorieLabel);
			
			By mediaStorieLabel=storie.mediaTab;
			storie.verifyComponentExistence(mediaStorieLabel);
			
			By supportStorieLabel=storie.supportoTab;
			storie.verifyComponentExistence(supportStorieLabel);	
			
			HeaderComponent login = new HeaderComponent(driver);
			By userIcon=login.iconUser;	
			login.verifyComponentExistence(userIcon);
			By glassIcon=login.magnifyingglassIcon;
			login.verifyComponentExistence(glassIcon);
			
			HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
			By menuIcon=hamburger.iconMenu;
			hamburger.verifyComponentExistence(menuIcon);
			logger.write("sulla pagina web con link "+prop.getProperty("LINK_STORIE")+" e' presente l' Header - Completed ");
			
			logger.write("check presenza del testo, dei links e delle icone social sul footer della pagina web '"+prop.getProperty("LINK_STORIE")+"' - Start ");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
						
			By linkInformazioniLegali=storie.informazioniLegaliLink;
			storie.verifyComponentExistence(linkInformazioniLegali);
			storie.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=storie.creditsLink;
			storie.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=storie.privacyLink;
			storie.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=storie.cookieLink;
			storie.verifyComponentExistence(linkCookie);
			
			By linkRemit=storie.remitLink;
			storie.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=storie.twitterIcon;
			storie.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=storie.facebookIcon;
			storie.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=storie.youtubeIcon;
			storie.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=storie.linkedinIcon;
			storie.verifyComponentExistence(iconLinkedin);
			logger.write("check presenza del testo, dei links e delle icone social sul footer della pagina web '"+prop.getProperty("LINK_STORIE")+"' - Completed ");
			
			logger.write("click sul link 'Informazioni legali' e check del suo link - Start");
			storie.clickComponent(linkInformazioniLegali);
			
			By legalInfo=storie.legalInfoPage;
			storie.verifyComponentExistence(legalInfo);
			storie.checkUrl(prop.getProperty("LINK_INFO_LEGALI"));
			logger.write("click sul link 'Informazioni legali' e check del suo link - Completed");
			
			logger.write("back alla pagina web con url '"+prop.getProperty("LINK_STORIE")+"' - Start");
			driver.navigate().back();
			
			menu.checkUrl(prop.getProperty("LINK_STORIE"));
			storie.verifyComponentExistence(enelLogo);
			logger.write("back alla pagina web con url '"+prop.getProperty("LINK_STORIE")+"' - Completed");
			
			logger.write("click sul tab 'LUCE E GAS' nell' Header della pagina con url '"+prop.getProperty("LINK_STORIE")+"' e check del suo link - Start");
            menu.sceltaMenu("LUCE E GAS");
			
			menu.checkUrl(prop.getProperty("LINK_LUCE_E_GAS"));
			
			By luceAndGas_Title=storie.luceAndGasTitle;
			storie.verifyComponentExistence(luceAndGas_Title);
			logger.write("click sul tab 'LUCE E GAS' nell' Header della pagina con url '"+prop.getProperty("LINK_STORIE")+"' e check del suo link - Completed");
			*/
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
