package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class classe4366 {


	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("Verificare che creata l'offerta, sia possibile salvare in bozza. - Start");	
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			BaseComponent base = new BaseComponent(driver);
			
			base.baseTimeoutInterval = 1;
			
			By button_conferma = gestione.button_conferma;			
			gestione.verifyComponentExistence(button_conferma);
			gestione.clickComponent(button_conferma);
			
			By button_crea_offerta = gestione.button_crea_offerta;			
			gestione.verifyComponentExistence(button_crea_offerta);
			gestione.clickComponent(button_crea_offerta);
						
//			base.checkSpinnersSFDC2();
			
			By button_popup_ko = gestione.button_popup_ko;
			gestione.verifyComponentInexistence(button_popup_ko);
			
//			By container_referente = gestione.container_referente;			
//			gestione.verifyComponentExistence(container_referente);
			
//			By container_referente_loaded = By.xpath("name((//h2//*[text()='Referente']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]/@*)[3])");
//			gestione.verifyComponentText(container_referente_loaded, "'host'");
//			driver.findElement(container_referente_loaded).getText();
			
//			base.checkSpinnersSFDC2();
			
			By popup_vbl_ko = gestione.popup_vbl_ko;
			gestione.verifyComponentInexistence(popup_vbl_ko);
			
			By radio_first_in_referente_table = gestione.radio_first_in_referente_table;			
			gestione.verifyComponentExistence(radio_first_in_referente_table);
			gestione.clickComponent(radio_first_in_referente_table);
			
			By button_conferma_in_referente_container = gestione.button_conferma_in_referente_container;		
			gestione.verifyComponentExistence(button_conferma_in_referente_container);
			gestione.clickComponent(button_conferma_in_referente_container);
			
			By pickuplist_in_container_suf = gestione.pickuplist_in_container_suf;			
			gestione.verifyComponentExistence(pickuplist_in_container_suf);
			gestione.clickComponent(pickuplist_in_container_suf);
			
			By pickuplist_ua = By.xpath(gestione.pickuplist_by_text.replaceFirst("##", "Uso Abitativo"));			
			gestione.verifyComponentExistence(pickuplist_ua);
			gestione.clickComponent(pickuplist_ua);
			
			By button_conferma_in_suf_container = gestione.button_conferma_in_suf_container;			
			gestione.verifyComponentExistence(button_conferma_in_suf_container);
			gestione.clickComponent(button_conferma_in_suf_container);
			
//			base.checkSpinnersSFDC2();
			
			By popup_prodotto_non_valido = gestione.popup_prodotto_non_valido;
			gestione.verifyComponentInexistence(popup_prodotto_non_valido);
			
			By container_commodity = gestione.container_commodity;			
			gestione.verifyComponentExistence(container_commodity);
			
//			By container_commodity_loaded = By.xpath("contains(name((//h2//*[text()='Commodity']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]/@*)[3]) , 'host')");
//			gestione.verifyComponentText(container_commodity_loaded, "true");
			
			By radio_elettrico = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//div[text()='ELETTRICO']/ancestor::tbody//label");
			gestione.verifyComponentExistence(radio_elettrico);
			gestione.clickComponent(radio_elettrico);
			
//			By pickuplist_cmsap = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/parent::div//input");
//			gestione.verifyComponentExistence(pickuplist_cmsap);
//			gestione.clickComponent(pickuplist_cmsap);
//			
//			By pickuplist_altri_servizi = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/parent::div//*[text()='ALTRI SERVIZI']");
//			gestione.verifyComponentExistence(pickuplist_altri_servizi);
//			gestione.clickComponent(pickuplist_altri_servizi);
			
			By pickuplist_residente = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Residente']/parent::div//input");
			gestione.verifyComponentExistence(pickuplist_residente);
			gestione.verifyAttributeValue(pickuplist_residente, "value", "NO");
			
			By pickuplist_ordine_fittizio = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Ordine Fittizio']/parent::div//input");
			gestione.verifyComponentExistence(pickuplist_ordine_fittizio);
			gestione.clickComponent(pickuplist_ordine_fittizio);
			
			By pickuplist_no = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Ordine Fittizio']/parent::div//*[text()='NO']");
			gestione.verifyComponentExistence(pickuplist_no);
			gestione.clickComponent(pickuplist_no);
			
			
			By input_tele_dist = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Telefono Distributore']/parent::div//input");
			gestione.verifyComponentExistence(input_tele_dist);
			gestione.clickComponent(input_tele_dist);
			driver.findElement(input_tele_dist).sendKeys(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			
			
			By pickuplist_ascensore = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Ascensore']/parent::div//input");
			gestione.verifyComponentExistence(pickuplist_ascensore);
			gestione.clickComponent(pickuplist_ascensore);
			
			By pickuplist_no_2 = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Ascensore']/parent::div//*[text()='NO']");
			gestione.verifyComponentExistence(pickuplist_no_2);
			gestione.clickComponent(pickuplist_no_2);
			
			By pickuplist_disalimentabilita = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Disalimentabilita’']/parent::div//input");
			gestione.verifyComponentExistence(pickuplist_disalimentabilita);
			gestione.clickComponent(pickuplist_disalimentabilita);
			
			By pickuplist_si = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Disalimentabilita’']/parent::div//*[text()='SI']");
			gestione.verifyComponentExistence(pickuplist_si);
			gestione.clickComponent(pickuplist_si);
			
			
			By input_consumo_annuo = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//label[text()='Consumo Annuo']/parent::div//input");
			gestione.verifyComponentExistence(input_consumo_annuo);
			gestione.clickComponent(input_consumo_annuo);
			driver.findElement(input_consumo_annuo).sendKeys(prop.getProperty("CONSUMO_ANNUO"));
		
			By button_conferma_fornitura = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//button[text()='Conferma Fornitura']");
			gestione.verifyComponentExistence(button_conferma_fornitura);
			gestione.clickComponent(button_conferma_fornitura);
			
			By cell_confermata_si = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//div[text()='ELETTRICO']/ancestor::tbody//div[text()='SI']");
			gestione.verifyComponentExistence(cell_confermata_si);
//			gestione.clickComponent(cell_confermata_si)
			
			By span_sezioni_non_confermate = By.xpath("//span[contains(text(),'sezioni non confermate')]");/*/ancestor::div[contains(@class,'header')]");*/
			gestione.verifyComponentExistence(span_sezioni_non_confermate);
			
			int n_sezioni;
			if(gestione.getElementTextString(span_sezioni_non_confermate).matches("^\\d.*$")) {
				n_sezioni = Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString());
			}else{
				throw new Exception("Numero delle sezione da confermare non presente.");
			};
					
			By button_conferma_fornitura_next = By.xpath("//div[@aria-label='Commodity']/ancestor::div[@role='region']//button[text()='Conferma']");
			gestione.verifyComponentExistence(button_conferma_fornitura_next);
			gestione.clickComponent(button_conferma_fornitura_next);
			
//			System.out.println(n_sezioni);
			
//			if(!(n_sezioni < (Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString())))) {
//				throw new Exception("Numero delle sezione da confermare uguale o maggiore a prima del click del tasto Conferma.");
//			};
			
			n_sezioni = Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString());
			
			System.out.println(n_sezioni);
			
			By container_irsl = By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']");		
			gestione.verifyComponentExistence(container_irsl);
			
//			By container_irsl_loaded = By.xpath("contains(name((//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]/@*)[3]) , 'host')");
//			gestione.verifyComponentText(container_irsl_loaded, "true");
			
			By container_irsl_indirizzo_verificato = By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//*[text()='Indirizzo verificato']");
			
			gestione.verifyComponentExistence(container_irsl_indirizzo_verificato);
			
			By container_irsl_button_conferma = By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Conferma']");
			
			gestione.verifyComponentExistence(container_irsl_button_conferma);
			gestione.clickComponent(container_irsl_button_conferma);

			//BOX INDIRIZZO DI FATTURAZIONE 
			By container_idf = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']");		
			gestione.verifyComponentExistence(container_idf);
			
//			By container_idf_loaded = By.xpath("contains(name((//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]/@*)[3]) , 'host')");
//			gestione.verifyComponentText(container_idf_loaded, "true");
			
			By idf_banner_indirizzo = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//*[text()='Indirizzo di Residenza/Sede Legale']");
			
			By idf_button_conferma = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Conferma']");
			if(gestione.getElementExistance(idf_banner_indirizzo)) {
				
				gestione.verifyComponentExistence(idf_button_conferma);
				gestione.clickComponent(idf_button_conferma);
				
			};
			
			By idf_tab_verifica = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//*[text()='Verifica Indirizzi Esistenti']");
			
			gestione.verifyComponentExistence(idf_tab_verifica);
			gestione.clickComponent(idf_tab_verifica);
			
			By idf_first_checkbox_showed = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//input[@type='checkbox' and ancestor::div[contains(@class , 'show')]]/..");
			
			gestione.verifyComponentExistence(idf_first_checkbox_showed);
			gestione.clickComponent(idf_first_checkbox_showed);
			
			By idf_button_importa = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Importa']");
			
			gestione.verifyComponentExistence(idf_button_importa);
			gestione.clickComponent(idf_button_importa);
			
			String idf_label = "//label[text()='##']";
			
			String parent_input = "/parent::*//input";
			
			By idf_input_regione = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Regione") + parent_input);
			
			gestione.verifyComponentExistence(idf_input_regione);
			
			By idf_input_regione_disabled =  By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Regione") + parent_input + "[@disabled]");
			
			gestione.verifyComponentExistence(idf_input_regione_disabled);
			
			By idf_input_provincia = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "*Provincia") + parent_input);
			
			gestione.verifyComponentExistence(idf_input_provincia);
			
			By idf_input_comune = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "*Comune") + parent_input);
			
			gestione.verifyComponentExistence(idf_input_comune);
			
			By idf_input_localita = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Località") +  parent_input);
			
			gestione.verifyComponentExistence(idf_input_localita);
			
			By idf_input_indirizzo = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "*Indirizzo") + parent_input);
			
			gestione.verifyComponentExistence(idf_input_indirizzo);
			
			By idf_input_ncivico = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "*Numero Civico") + parent_input);
			
			gestione.verifyComponentExistence(idf_input_ncivico);
			
			By idf_input_cap = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "CAP") + parent_input);
			
			gestione.verifyComponentExistence(idf_input_cap);		
			
			By idf_input_cap_disabled = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "CAP") + parent_input + "[@disabled]");
			
			gestione.verifyComponentExistence(idf_input_cap_disabled);
			
			By idf_button_verifica = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Verifica']");
			
			Thread.sleep(2000);
			gestione.verifyComponentExistence(idf_button_verifica);
			gestione.clickComponent(idf_button_verifica);
			
			By idf_indirizzo_verificato = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//*[text()='Indirizzo verificato']");
			
			gestione.verifyComponentExistence(idf_indirizzo_verificato);
			
			gestione.verifyComponentExistence(idf_button_conferma);
			gestione.clickComponent(idf_button_conferma);
			
//			if(!(n_sezioni < Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString()))) {
//				throw new Exception("Numero delle sezione da confermare uguale o maggiore a prima del click del tasto Conferma.");
//			};
			n_sezioni = Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString());
			
//			System.out.println(n_sezioni);
			
			//BOX FATTURAZIONE ELETTRONICA
			By container_fe = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']");		
			gestione.verifyComponentExistence(container_fe);
			
			String container_fe_invoicing_section = "//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]";
			
			System.out.println(gestione.getAttributesName(container_fe_invoicing_section)[2]);
			
//			if(!gestione.getAttributesName(container_fe_loaded_b)[2].contains("host"));
			
//			gestione.verifyComponentText(container_fe_loaded, "host");
			
//			driver.findElement(container_fe_loaded).getAttribute("");
//			driver.findElement(container_fe_loaded);
//			JavascriptExecutor js = (JavascriptExecutor)driver;
//			Object message = js.executeScript("document.evaluate( xyxyxy ,document, null, XPathResult.STRING_TYPE, null ).stringValue;");
//			System.out.println(message.toString().trim());
			
			By fe_pickuplist_canale_invio = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Canale Invio") + parent_input + "/..");		
			gestione.verifyComponentExistence(fe_pickuplist_canale_invio);
			gestione.clickWithJS(fe_pickuplist_canale_invio);
			
			String parent_element_by_text = "/parent::*//*[text()='##']";
			
			By fe_pickuplist_canale_invio_element_by_text = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + parent_element_by_text.replaceFirst("##", "SDI"));			
			gestione.verifyComponentExistence(fe_pickuplist_canale_invio_element_by_text);
			gestione.clickWithJS(fe_pickuplist_canale_invio_element_by_text);
			
			By fe_input_codice_ufficio = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Codice Ufficio") + parent_input);		
			gestione.verifyComponentExistence(fe_input_codice_ufficio);
			gestione.clickWithJS(fe_input_codice_ufficio);
			
			driver.findElement(fe_input_codice_ufficio).clear();
			
			driver.findElement(fe_input_codice_ufficio).sendKeys("0000");
			
			By fe_valore_corto = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']//*[text()='Il valore inserito è troppo corto']");
			
			gestione.verifyComponentExistence(fe_valore_corto);
			
			driver.findElement(fe_input_codice_ufficio).clear();
			
			driver.findElement(fe_input_codice_ufficio).sendKeys("0000000");
			
			By fe_button_conferma = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']//button[text()='Conferma']");
			gestione.verifyComponentExistence(fe_button_conferma);
			gestione.clickWithJS(fe_button_conferma);
			
			By fe_input_data_inizio_validita = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Data Inizio Validità") + parent_input);		
			gestione.verifyComponentExistence(fe_input_data_inizio_validita);
			
			By fe_input_data_inizio_validita_disabled = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Data Inizio Validità") + parent_input+ "[@disabled]");
			gestione.verifyComponentExistence(fe_input_data_inizio_validita_disabled);
			//TODO CHECK SYSDATE VALUE
			
			By fe_input_data_fine_validita = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Data Fine Validità") + parent_input);		
			gestione.verifyComponentExistence(fe_input_data_fine_validita);
			gestione.verifyComponentExistence(fe_input_data_fine_validita);
			String fe_input_data_fine_validita_s = gestione.xpathToString(fe_input_data_fine_validita);
			System.out.println(gestione.getValue(fe_input_data_fine_validita_s));
			
			By fe_input_data_fine_validita_disabled =  By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Data Fine Validità") + parent_input + "[@disabled]");
			gestione.verifyComponentExistence(fe_input_data_fine_validita_disabled);
			
//			if(!(n_sezioni < Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString()))) {
//			throw new Exception("Numero delle sezione da confermare uguale o maggiore a prima del click del tasto Conferma.");
//			};
			n_sezioni = Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString());
		
//			System.out.println(n_sezioni);
			
			//TODO CHECK SYSDATE VALUE
			
//			By fe_input_data_fine_validita_value = new ByChained(fe_input_data_fine_validita , status_disabled);
			
//			gestione.clickComponent(span_sezioni_non_confermate);
			
			//BOX METODO DI PAGAMENTO
			By container_mdp = By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div[@role='region']");		
			gestione.verifyComponentExistence(container_mdp);
			
			String container_mdp_loaded = "//h2//*[text()='Metodo di pagamento']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]";
//			gestione.verifyComponentText(container_mdp_loaded, "host");
			
			System.out.println(gestione.getAttributesName(container_mdp_loaded)[2]);
			
			String mdp_radio_button_by_text = "//div[text()='##']/ancestor::tr//input";
			
			String any_parent = "/parent::*";
			
			By mdp_radio_data_fine_validita = By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div[@role='region']" + mdp_radio_button_by_text.replaceFirst("##", "Bollettino Postale") + any_parent);		
			gestione.verifyComponentExistence(mdp_radio_data_fine_validita);
			String mdp_radio_data_fine_validita_s = gestione.xpathToString(mdp_radio_data_fine_validita);
			System.out.println(gestione.getValue(mdp_radio_data_fine_validita_s));
			
			By pop_up_button_ok = By.xpath("/button[text()='Ok']/ancestor::slot[@name='footer']");		
			gestione.verifyComponentExistence(pop_up_button_ok);
			gestione.clickWithJS(pop_up_button_ok);
			
//			if(!(n_sezioni < Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString()))) {
//			throw new Exception("Numero delle sezione da confermare uguale o maggiore a prima del click del tasto Conferma.");
//			};
			n_sezioni = Integer.parseInt(gestione.getElementTextString(span_sezioni_non_confermate).split(" ")[0].toString());
		
//			System.out.println(n_sezioni);
			
			//BOX SCONTI E BONUS
			By container_seb = By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[@role='region']");		
			gestione.verifyComponentExistence(container_seb);
			
			String container_seb_loaded = "//h2//*[text()='Sconti e Bonus']/ancestor::div[@role='region']/ancestor::*[contains(local-name(), 'invoicing_section')]";
//			gestione.verifyComponentText(container_seb_loaded, "host");
			
			System.out.println(gestione.getAttributesName(container_seb_loaded)[2]);
			
			By seb_input_codice_compagnia = By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Codice Campagna") + parent_input);		
			gestione.verifyComponentExistence(seb_input_codice_compagnia);
			
			By seb_input_codice_compagnia_populated = By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[@role='region']" + idf_label.replaceFirst("##", "Codice Campagna") + parent_input);		
			gestione.verifyComponentExistence(seb_input_codice_compagnia_populated);
			
			
//			By button_salva_in_bozza = gestione.button_salva_in_bozza;			
//			gestione.verifyComponentExistence(button_salva_in_bozza);
//			gestione.clickComponent(button_salva_in_bozza);
//			
//			base.checkSpinnersSFDC2();
//			
//			By check_stato_by_text = By.xpath(gestione.check_stato_by_text.replaceFirst("##", "Bozza"));			
//			gestione.verifyComponentExistence(check_stato_by_text);
//			
//			By check_statodc_by_text = By.xpath(gestione.check_statodc_by_text.replaceFirst("##", "IN LAVORAZIONE"));			
//			gestione.verifyComponentExistence(check_statodc_by_text);
//			
//			By check_sstatodc_by_text = By.xpath(gestione.check_sstatodc_by_text.replaceFirst("##", "Offer - Null / Draft"));			
//			gestione.verifyComponentExistence(check_sstatodc_by_text);
//			
//			By button_ssdc_by_text = gestione.button_ssdc_by_text;
//			
//			By frame_ssdc = gestione.frame_ssdc;
//			
//			gestione.verifyFrameAvailableAndSwitchToIt(frame_ssdc);
//			gestione.verifyComponentExistence(button_ssdc_by_text);
//			gestione.clickComponent(button_ssdc_by_text);
		
			logger.write("Verifica esito Offertabilita - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
