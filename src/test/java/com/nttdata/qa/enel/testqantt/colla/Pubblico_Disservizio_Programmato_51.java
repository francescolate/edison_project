package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_Disservizio_Programmato_51 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ImpressComponent ic = new ImpressComponent(driver);
			
			logger.write("Accessing Disservizio page - Start");
			ic.launchLink(prop.getProperty("LINK"));
			ic.verifyComponentExistence(ic.buttonAccetta);
			ic.clickComponent(ic.buttonAccetta);
			ic.verifyComponentExistence(ic.logoEnel);
			logger.write("Accessing Disservizio page - Complete");
			
			logger.write("Verify the Disservizio page home - Start");
			ic.verifyComponentExistence(ic.disservizioHome);
			ic.comprareText(ic.disservizioHome, ImpressComponent.DISSERVIZIO_HOME, true);
			logger.write("Verify the Disservizio page home - Complete");
			
			logger.write("Verify the Disservizio page contents - Start");
			ic.verifyComponentExistence(ic.disservizioContent1);
			ic.comprareText(ic.disservizioContent1, ImpressComponent.DISERVIZIO_CONTENT1, true);
			//ic.verifyComponentExistence(ic.disservizioContent2);
			//ic.comprareText(ic.disservizioContent2, ImpressComponent.DISERVIZIO_CONTENT2, true);
			logger.write("Verify the Disservizio page contents - Start");
			
			logger.write("Verify the Disservizio page header tab and label - Start");
			ic.verifyHeaderVisibility();
			logger.write("Verify the Disservizio page header and label - Complete");
			
			logger.write("Verify the Disservizio page footer and label - Start");		
			ic.verifyFooterVisibility();
			logger.write("Verify the Disservizio page footer and label - Complete");
			
			logger.write("Verify the Disservizio page footer copyrights - Start");
			ic.verifyComponentExistence(ic.copyRight1);
			ic.verifyComponentExistence(ic.copyRight2);
			logger.write("Verify the Disservizio page footer copyrights - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
