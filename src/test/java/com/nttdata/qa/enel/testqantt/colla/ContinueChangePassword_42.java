package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ContinueChangePassword_42 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getDriverInstance(prop);

		try {
			
			LoginPageValidateComponent lp = new LoginPageValidateComponent(driver);
			
			
			logger.write("Verify and enter the OTP  - Start");
			lp.verifyComponentExistence(lp.otpInput);
			lp.insertText(lp.otpInput, prop.getProperty("OTP"));
			logger.write("Verify and enter the OTP  - Complete");
			
			logger.write("Click on Continua button  - Start");
			lp.verifyComponentExistence(lp.otpcontinuaButton);
			lp.clickComponent(lp.otpcontinuaButton);
			logger.write("Click on Continua button  - Start");
			
			logger.write("Click on Accedi button  - Start");
			lp.verifyComponentExistence(lp.accediButton);
			lp.clickComponent(lp.accediButton);
			logger.write("Click on Accedi button  - Start");
			
			logger.write("Enter the user and Password - Start");
			lp.insertText(lp.username, prop.getProperty("USERNAME"));
			lp.insertText(lp.password, prop.getProperty("PASSWORD"));
			lp.clickComponent(lp.buttonLoginAccedi);
			logger.write("Enter the user and Password - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	


}
