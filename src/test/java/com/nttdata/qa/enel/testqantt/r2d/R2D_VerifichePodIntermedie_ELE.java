package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerifichePodIntermedie_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Menu Interrogazione
				menuBox.selezionaVoceMenuBox("Interrogazione","Report Post Sales");
				R2D_VerifichePodComponent verifichePod = new R2D_VerifichePodComponent(driver);
				//Inserimento POD
				if(prop.getProperty("SKIP_POD","N").equals("N")){
				verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD_ELE",prop.getProperty("POD")));
				}
				//Inserimento ID richiesta CRM
				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA",prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA"))));
				String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE");
				//if (prop.getProperty("ALLACCIO","N").equals("Y")||prop.getProperty("SUBENTRO_COMBINATO_R2D","N").equals("Y")){
//				if (tipoOperazione.equals("ALLACCIO")||tipoOperazione.equals("SUBENTRO_COMBINATO_R2D")){
//					verifichePod.selezionaCodiceServizioDt(verifichePod.selectCodiceServizioDt,prop.getProperty("CODICE_SERVIZIO_DT_R2D"));
//				}
				if (tipoOperazione.equals("ALLACCIO_EVO")){
					verifichePod.selezionaCodiceServizioDt(verifichePod.selectCodiceServizioDt,prop.getProperty("CODICE_SERVIZIO_DT_R2D"));
				}
				//Cerca POD
				verifichePod.cercaPod(verifichePod.buttonCerca);
				//Click Dettaglio Pratiche
				verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				//Click su Pratica
				verifichePod.selezionaPratica(verifichePod.rigaPratica);
				//Verifica Campi
					//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
				campiDaVerificare.add("Stato R2D;"+prop.getProperty("STATO_R2D"));
				campiDaVerificare.add("Esito MUTI;"+"OK");
				//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
				verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);
				
			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
