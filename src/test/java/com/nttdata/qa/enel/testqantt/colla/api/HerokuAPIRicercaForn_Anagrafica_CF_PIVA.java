// Package HerokuAPIUserByEnelID

package com.nttdata.qa.enel.testqantt.colla.api;
// Utilizzato per API RicercaFornitura , User/Profile/old , FornitureACB e Anagrafica

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
// import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

import com.nttdata.jwt.gen.JWToken;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIRicercaForn_Anagrafica_Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class HerokuAPIRicercaForn_Anagrafica_CF_PIVA {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku getuuid eMAIL Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
			prop.setProperty("API_URL", prop.getProperty("API_URL") + prop.getProperty("CF_PIVA"));

			//Da configurare come OutPut
			prop.setProperty("TID", UUID.randomUUID().toString());
    		prop.setProperty("SID", UUID.randomUUID().toString()); 
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIRicercaForn_Anagrafica_Component HRKComponent = new HerokuAPIRicercaForn_Anagrafica_Component();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


