package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.Precheck2GComponents;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


import io.qameta.allure.Step;

public class ConfiguraProdottoElettricoResidenzialeSubentro {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			Precheck2GComponents precheck = new Precheck2GComponents(driver);
	
			logger.write("Configura prodotto elettrico "+prop.getProperty("PRODOTTO")+"- Start");
			TimeUnit.SECONDS.sleep(10);

			configura.press(configura.linkElettricoResidenziale);
			configura.selezionaProdotto(prop.getProperty("PRODOTTO"));
			
			logger.write("Selezione del prodotto");
			if (!prop.getProperty("OPZIONE_KAM_AGCOR","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR");
			}
			if (!prop.getProperty("SCELTA_ABBONAMENTI","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO"),prop.getProperty("SCELTA_ABBONAMENTI"));
				logger.write("Selezione Scelta Abbonamenti");
			}
			
			if (!prop.getProperty("OPZIONE_VAS","").equals("")){
				configura.configuraVas(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_VAS"));
				logger.write("Selezione VAS");
			}else if (!prop.getProperty("ELIMINA_VAS","").equals("")){
				configura.eliminaVas();
				logger.write("Elimina VAS obbligatorio");
			}
			if (!prop.getProperty("SCONTO","").equals("")){
				configura.configuraSconto(prop.getProperty("PRODOTTO"),prop.getProperty("SCONTO"));
				logger.write("Selezione Sconto");
			}
			if (!prop.getProperty("OPZIONE_FIBRA","").equals("")){
				logger.write("Selezione Fibra - Start");
				configura.ConfiguraFibra(prop.getProperty("OPZIONE_FIBRA"));
				logger.write("Selezione Fibra - Completed");
			}
			// Selezione del prodotto nel TAB del Carrello
			if (prop.getProperty("TAB_SGEGLI_TU","").equals("SI")){
				configura.ConfiguraProdottoTab(prop.getProperty("PRODOTTO_TAB"),prop.getProperty("PIANO_TARIFFARIO"));
				logger.write("Selezione il Prodotto nel TAB del carrello");
			}
			if (!prop.getProperty("PIANO_TARIFFARIO","").equals("")){
				driver.findElement(By.xpath("//div[text()='Scegli Tu']")).click();
				configura.ConfiguraPianoTariffario(prop.getProperty("PIANO_TARIFFARIO"));
				logger.write("Selezione Piano Tariffario");
			}
			if (!prop.getProperty("ORE_FREE","").equals("")){
				configura.configuraOrarioFree(prop.getProperty("ORE_FREE"));
				logger.write("Selezione Ore Free");
			}		
			configura.salvaConfigurazione();
			logger.write("Salva Configurazione");

			configura.checkOut();
			logger.write("Check out");
			
			if (!prop.getProperty("ASSICURAZIONE_SERVIZI","").equals("")){
				configura.press(configura.ConfermaAssicurazioneServizi);
				logger.write("Conferma Assicurazione e Servizi");
			}
			
			logger.write("Configura prodotto ele "+prop.getProperty("PRODOTTO")+"- Completed");
			
			TimeUnit.SECONDS.sleep(5);
			
			/* Verifica Precheck 2G */
			
			if(prop.getProperty("PRECHECK2G","").equals("Y")) {
				precheck.verificaPrecheckOK();
			} else {
				System.out.println("Precheck2G non verificato");
			}


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}

