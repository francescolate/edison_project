package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.PrivateAreaAccountBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaAccountBSN {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaAccountBSNComponent paac = new PrivateAreaAccountBSNComponent(driver);
			Thread.sleep(5000);
			logger.write("cliccando sul logo dell'account BSN - START");
			paac.clickComponent(paac.accountLogo);
			logger.write("cliccando sul logo dell'account BSN - COMPLETED");
			
			logger.write("cliccando sul pulsante DATI DI CONTATTO - START");
			paac.verifyComponentExistence(paac.modificaProfiloLink);
			paac.clickComponent(paac.modificaProfiloLink);
			logger.write("cliccando sul pulsante DATI DI CONTATTO - COMPLETED");
			
			logger.write("verificando lo header della pagina Modifica Dati di Contatto - START");
			paac.checkDocumentReadyState();
			paac.verifyComponentExistence(paac.modificaDatiContattoHeader);
			paac.compareText(paac.modificaDatiContattoHeader, paac.modificaDatiDiContattoHeaderText, true);
			logger.write("verificando lo header della pagina Modifica Dati di Contatto - COMPLETED");
			
			logger.write("verificando il testo sotto lo header della pagina Modifica Dati di Contatto - START");
			paac.verifyComponentExistence(paac.modificaDatiContattoHeaderText);
			//paac.compareText(paac.modificaDatiContattoHeaderText, paac.modificaDatiDiContattoHeaderSubText, true);
			logger.write("verificando il testo sotto lo header della pagina Modifica Dati di Contatto - COMPLETED");
			
			/*logger.write("cliccando sul prosegui - START");
			Thread.sleep(5000);
			paac.verifyComponentExistence(paac.prosegui);
			paac.clickComponent(paac.prosegui);
			Thread.sleep(5000);
			logger.write("cliccando sul prosegui - COMPLETED");*/
			
			
			logger.write("verificando il nome del titolare - START");
			paac.verifyComponentExistence(paac.inputTitolare);
			//paac.compareTextDisabledInput(paac.inputTitolareString, prop.getProperty("TITOLARE"));
			//paac.compareTextDisabledInput(paac.inputTitolareString, prop.getProperty("NOME"));
			logger.write("verificando il nome del titolare - COMPLETED");
			
			logger.write("verificando il cf del titolare - START");
			paac.verifyComponentExistence(paac.inputCf);
			//paac.compareTextDisabledInput(paac.inputCfString, prop.getProperty("CF"));
			logger.write("verificando il cf del titolare - COMPLETED");
			
			logger.write("verificando il testo del paragrafo precedente i valore TELEFONO,EMAIL,PEC - START");
			paac.verifyComponentExistence(paac.telMailPec);
			//paac.compareText(paac.telMailPec, paac.telMailPecText, true);
			logger.write("verificando il testo del paragrafo precedente i valore TELEFONO,EMAIL,PEC - COMPLETED");
			
			logger.write("verificando il telefono del titolare - START");
			paac.verifyComponentExistence(paac.inputTelefono);
		//	paac.compareTextDisabledInput(paac.inputTelefonoString, prop.getProperty("TELEFONO"));
//			paac.compareTextDisabledInput(paac.inputTelefonoString, prop.getProperty("PHONE"));
			logger.write("verificando il telefono del titolare - COMPLETED");
			
			logger.write("verificando la mail del titolare - START");
			paac.verifyComponentExistence(paac.inputEmail);
			//paac.compareTextDisabledInput(paac.inputEmailString, prop.getProperty("EMAIL"));
			logger.write("verificando la mail del titolare - COMPLETED");
			
			logger.write("verificando la pec del titolare - START");
			paac.verifyComponentExistence(paac.inputPec);
			//paac.compareTextDisabledInput(paac.inputPecString, prop.getProperty("PEC"));
			logger.write("verificando la pec del titolare - COMPLETED");
			
			logger.write("cliccando su Modifica - START");
			paac.verifyComponentExistence(paac.modificaButton);
			//paac.clickComponent(paac.modificaButton);
			paac.clickeaspetta(paac.modificaButton);
			logger.write("cliccando su Modifica - COMPLETED");
			Thread.sleep(30000);
			logger.write("verificando lo header della pagina Modifica Dati di Contatto - START");
			paac.checkDocumentReadyState();
			paac.verifyComponentExistence(paac.modificaDatiContattoHeader);
			paac.compareText(paac.modificaDatiContattoHeader, paac.modificaDatiDiContattoHeaderText, true);
			logger.write("verificando lo header della pagina Modifica Dati di Contatto - COMPLETED");
			
			logger.write("verificando il testo sotto lo header della pagina Modifica Dati di Contatto - START");
			paac.verifyComponentExistence(paac.modificaDatiContattoCompilazione);
			paac.compareText(paac.modificaDatiContattoCompilazione, paac.modificaDatiDiContattoCompilazioneText, true);
			logger.write("verificando il testo sotto lo header della pagina Modifica Dati di Contatto - COMPLETED");
			
			logger.write("verificando il telefono del titolare - START");
			paac.verifyComponentExistence(paac.inputTelefonoNew);
//			paac.compareTextDisabledInput(paac.inputTelefonoString, prop.getProperty("TELEFONO"));
//			paac.compareTextDisabledInput(paac.inputTelefonoStringNew, prop.getProperty("PHONE"));
			logger.write("verificando il telefono del titolare - COMPLETED");
			
			logger.write("verificando la mail del titolare - START");
			paac.verifyComponentExistence(paac.inputEmailStar);
			//paac.compareTextDisabledInput(paac.inputEmailStarString, prop.getProperty("EMAIL"));
			logger.write("verificando la mail del titolare - COMPLETED");
			
			logger.write("verificando la pec del titolare - START");
			paac.verifyComponentExistence(paac.inputPec);
			//paac.compareTextDisabledInput(paac.inputPecString, prop.getProperty("PEC"));
			logger.write("verificando la pec del titolare - COMPLETED");
			
			if(prop.containsKey("EMPTY_EMAIL"))
			{
				logger.write("svuotando il campo email - START");
				paac.clickComponent(paac.inputEmailStar);
				paac.changeInputText(paac.inputEmailStar, "");
				//paac.clickComponent(paac.inputTelefono);
				logger.write("svuotando il campo email - COMPLETED");
				
				logger.write("controllando l'errore per campo email mandatorio vuoto - START");
				//paac.verifyComponentExistence(paac.mandatoryEmailErrorText);
				logger.write("controllando l'errore per campo email mandatorio vuoto - COMPLETED");
			}
			
			if(prop.containsKey("CHANGE_EMAIL_AND_TEL")){
				String[] newValues = prop.getProperty("CHANGE_EMAIL_AND_TEL").split(":");
				logger.write("cambiando il valore del campo telefono - START");
				//paac.clickComponent(paac.inputTelefono);
				//paac.changeInputText(paac.inputTelefono, newValues[0]);
				logger.write("cambiando il valore del campo telefono - COMPLETED");
				logger.write("cambiando il valore del campo email - START");
				paac.clickComponent(paac.inputEmailStar);
				paac.changeInputText(paac.inputEmailStar, newValues[1]);
				logger.write("cambiando il valore del campo email - COMPLETED");
				
				logger.write("cliccando sul prosegui - START");
				paac.verifyComponentExistence(paac.proseguiButton);
				paac.clickComponent(paac.proseguiButton);
				logger.write("cliccando sul prosegui - COMPLETED");
				
				logger.write("verificando il telefono del titolare - START");
				paac.verifyComponentExistence(paac.inputTelefono);
				//paac.compareTextDisabledInput(paac.inputTelefonoString, newValues[0]);
				logger.write("verificando il telefono del titolare - COMPLETED");
				
				logger.write("verificando la mail del titolare - START");
				paac.verifyComponentExistence(paac.inputEmail);
				paac.compareTextDisabledInput(paac.inputEmailString, newValues[1]);
				logger.write("verificando la mail del titolare - COMPLETED");
				
				logger.write("verificando la pec del titolare - START");
				paac.verifyComponentExistence(paac.inputPec);
				//paac.compareTextDisabledInput(paac.inputPecString, prop.getProperty("PEC"));
				logger.write("verificando la pec del titolare - COMPLETED");
				
				logger.write("controllando la presenza del pulsante esci - START");
				paac.verifyComponentExistence(paac.esciButton);
				logger.write("controllando la presenza del pulsante esci - COMPLETED");
				
				logger.write("controllando la presenza del pulsante conferma - START");
				paac.verifyComponentExistence(paac.confermaButton);
				logger.write("controllando la presenza del pulsante conferma - COMPLETED");
				
				logger.write("controllando la presenza del pulsante indietro - START");
				paac.verifyComponentExistence(paac.indietroButton);
				paac.clickComponent(paac.indietroButton);
				logger.write("controllando la presenza del pulsante indietro - COMPLETED");
				
				logger.write("verificando il telefono del titolare - START");
				//paac.verifyComponentExistence(paac.inputTelefono);
				//paac.compareTextDisabledInput(paac.inputTelefonoString, newValues[0]);
				logger.write("verificando il telefono del titolare - COMPLETED");
				
				logger.write("verificando la mail del titolare - START");
				paac.verifyComponentExistence(paac.inputEmailStar);
				//paac.compareTextDisabledInput(paac.inputEmailStarString, newValues[1]);
				logger.write("verificando la mail del titolare - COMPLETED");
				
				logger.write("verificando la pec del titolare - START");
				paac.verifyComponentExistence(paac.inputPec);
				//paac.compareTextDisabledInput(paac.inputPecString, prop.getProperty("PEC"));
				logger.write("verificando la pec del titolare - COMPLETED");
				
			}
			
			
			logger.write("controllando la presenza del pulsante esci - START");
			paac.verifyComponentExistence(paac.esciButton);
			logger.write("controllando la presenza del pulsante esci - COMPLETED");
			
			logger.write("controllando la presenza del pulsante prosegui - START");
			paac.verifyComponentExistence(paac.proseguiButton);
			logger.write("controllando la presenza del pulsante prosegui - COMPLETED");
			
			//driver.close();

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
