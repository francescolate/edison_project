package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.HomePageResidentialComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Autolettura_ACR_357_B {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			HomeComponent hc = new HomeComponent(driver);
			ServicesComponent sc = new ServicesComponent(driver);
			HomePageResidentialComponent hpr = new HomePageResidentialComponent(driver);
			
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");			
			
			logger.write("Click on service and verify the Supply service section -- Starts");
			hc.clickComponent(hc.services);
			sc.verifyComponentExistence(sc.serviceTitle);
			Thread.sleep(20000);
			logger.write("Click on service and verify the Supply service section -- Ends");

			logger.write("Click on autolettura on service section -- Starts");
			sc.verifyComponentExistence(sc.autolettura);
			sc.clickComponent(sc.autolettura);
			Thread.sleep(20000);
			logger.write("Click on autolettura on service section -- Ends");

			logger.write("Verify autolettura page title and content -- Starts");
			sc.verifyComponentExistence(sc.headingautolettura);
			sc.comprareText(sc.headingautolettura, ServicesComponent.HEADING_autolettura, true);
			sc.comprareText(sc.subtitleautolettura, ServicesComponent.SUBTITLEautolettura, true);
			sc.comprareText(sc.titleautolettura, ServicesComponent.TITLE_autolettura, true); 
			logger.write("Verify autolettura page title and content -- Ends");

			logger.write("Verify supply details - Start");
			sc.verifyComponentExistence(sc.Indirizzodellafornitura);
			sc.verifyComponentExistence(sc.NumeroCliente);
			sc.verifyComponentExistence(sc.linkStoricoLetture);
			sc.verifyComponentExistence(sc.esciButton);
			sc.verifyComponentExistence(sc.comminica);
			logger.write("Verify supply details - Complete");
			
			logger.write("Click on No and verify the page - Start");
			hpr.clickComponent(hpr.Forniture);
			sc.verifyComponentExistence(sc.popUpHeading);
			sc.verifyComponentExistence(sc.popUpInnerText);
			sc.verifyComponentExistence(sc.no);
			sc.verifyComponentExistence(sc.siButton);
			sc.clickComponent(sc.siButton);
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Click on No and verify the page - Complete");

			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
