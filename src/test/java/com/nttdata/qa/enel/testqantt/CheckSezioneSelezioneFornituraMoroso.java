package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CheckSezioneSelezioneFornituraMoroso {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
				logger.write("check messaggio d'errore - Start");
				forn.verifyComponentExistence(forn.messaggioMoroso);
				logger.write("check messaggio d'errore - Completed");
				logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Start");
				
                By sezione_fornitura=forn.sezioneSelezioneFornitura;
                forn.verifyComponentExistence(sezione_fornitura);
                TimeUnit.SECONDS.sleep(10);
                By campo_CercaFornitura=forn.campoCercaFornitura;
                forn.verifyComponentExistence(campo_CercaFornitura);
                forn.scrollComponent(forn.pagina);
              
                TimeUnit.SECONDS.sleep(1);
                logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Completed");
			   
            	logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: Commodity, Stato, Numero Cliente, POD/PDR, Indirizzo fornitura, Attivabilità, Offertabilità, POD Moroso e seleziona tutte le forniture - Start");
				forn.verificaColonneTabella(forn.colonne);
				forn.scrollComponent(forn.pagina);
				TimeUnit.SECONDS.sleep(5);
				forn.selezionaTuttiRecordInTabellaMoroso();
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: Commodity, Stato, Numero Cliente, POD/PDR, Indirizzo fornitura, Attivabilità, Offertabilità, POD Moroso e seleziona tutte le forniture - Completed");
				
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna POD MOROSO sia presente il valore 'SI' - Start");
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(2);
				forn.checkValoreInTableMoroso("SI", 2);
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna POD MOROSO sia presente il valore 'SI' - Completed");
								
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna POD MOROSO sia presente il valore 'NO' - Start");
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(2);
				forn.checkValoreInTableMoroso("NO", 1);
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna POD MOROSO sia presente il valore 'NO' - Completed");
	
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
