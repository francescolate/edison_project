package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ContactComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertaComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LaunchOfferta {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);			
			prop.setProperty("MODULORECLAMILINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			Thread.sleep(10000);
			logger.write("select drop down value - Start");
			pc.changeDropDownValue(pc.productDropDown, pc.luce);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, pc.cambio);
			logger.write("select drop down value - Completed");
			
			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			pc.checkURLAfterRedirection(pc.detaglioOffertaUrl);
			pc.clickComponent(pc.dettaglioOfferta);
			
			OffertaComponent oc = new OffertaComponent(driver);
			oc.clickComponent(oc.attivaSubito);
	
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
