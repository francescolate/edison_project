

	package com.nttdata.qa.enel.testqantt;

	import java.io.FileOutputStream;
	import java.io.PrintWriter;
	import java.io.StringWriter;
	import java.util.Properties;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.remote.RemoteWebDriver;

	import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
	import com.nttdata.qa.enel.util.QANTTLogger;
	import com.nttdata.qa.enel.util.WebDriverManager;

	import io.qameta.allure.Step;

	public class PagamentoRIDEVO {

		@Step("PagamentoRIDEVO")
		public static void main(String[] args) throws Exception {

			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					SceltaMetodoPagamentoComponentEVO sceltaMetodoPagamentoComponentEVO = new SceltaMetodoPagamentoComponentEVO(
							driver);
					String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE","");		
					
					if (tipoOperazione.equals("ALLACCIO_EVO")) {
					    sceltaMetodoPagamentoComponentEVO.selezionaNuovoMetodoPagamentoNoSearch(prop.getProperty("IBAN"),
					    		sceltaMetodoPagamentoComponentEVO.nuovoMetodoPagamento2,
								sceltaMetodoPagamentoComponentEVO.copiaDaIntestatario2,
								sceltaMetodoPagamentoComponentEVO.ibanField2, sceltaMetodoPagamentoComponentEVO.saveMetod2,
								sceltaMetodoPagamentoComponentEVO.buttonConferma2,
								sceltaMetodoPagamentoComponentEVO.nomeIntestatarioCC, "AUTOMATION",
								sceltaMetodoPagamentoComponentEVO.ragioneSocialeCC, "AUTOMATION",
								sceltaMetodoPagamentoComponentEVO.codiceFiscaleCC, "CCCCCC85T15H703W");
					}
					else if(tipoOperazione.equals("SWITCH_ATTIVO_EVO")) {
						if(prop.getProperty("IS_IBAN_ESTERO","").equals("Y"))
						{
							  sceltaMetodoPagamentoComponentEVO.SelezionaNuovoMetodoPagamentoIbanEstero(prop.getProperty("IBAN"),sceltaMetodoPagamentoComponentEVO.campoRicerca,
							    		sceltaMetodoPagamentoComponentEVO.nuovoMetodoPagamento2,
										sceltaMetodoPagamentoComponentEVO.copiaDaIntestatario2,
										sceltaMetodoPagamentoComponentEVO.ibanField3, sceltaMetodoPagamentoComponentEVO.saveMetod3,
										sceltaMetodoPagamentoComponentEVO.buttonConferma2,
										sceltaMetodoPagamentoComponentEVO.nomeIntestatarioCC, prop.getProperty("NOME_NUOVO_SDD","AUTOMATION"),
										sceltaMetodoPagamentoComponentEVO.ragioneSocialeCC, prop.getProperty("COGNOME_NUOVO_SDD","AUTOMATION"),
										sceltaMetodoPagamentoComponentEVO.codiceFiscaleCC, prop.getProperty("CF_NUOVO_SDD","CCCCCC85T15H703W"),
										sceltaMetodoPagamentoComponentEVO.ibanEsteroCheckBox,true);
						}
						else
						{
						    sceltaMetodoPagamentoComponentEVO.selezionaNuovoMetodoPagamentoWithSearch(prop.getProperty("IBAN"),sceltaMetodoPagamentoComponentEVO.campoRicerca,
						    		sceltaMetodoPagamentoComponentEVO.nuovoMetodoPagamento2,
									sceltaMetodoPagamentoComponentEVO.copiaDaIntestatario2,
									sceltaMetodoPagamentoComponentEVO.ibanField3, sceltaMetodoPagamentoComponentEVO.saveMetod3,
									sceltaMetodoPagamentoComponentEVO.buttonConferma2,
									sceltaMetodoPagamentoComponentEVO.nomeIntestatarioCC, "AUTOMATION",
									sceltaMetodoPagamentoComponentEVO.ragioneSocialeCC, "AUTOMATION",
									sceltaMetodoPagamentoComponentEVO.codiceFiscaleCC, "CCCCCC85T15H703W");
						}
					}
					else if(tipoOperazione.equals("RICONTRATTUALIZZAZIONE")) {
						    sceltaMetodoPagamentoComponentEVO.selezionaNuovoMetodoPagamentoWithSearch(prop.getProperty("IBAN"),sceltaMetodoPagamentoComponentEVO.campoRicerca,
						    		sceltaMetodoPagamentoComponentEVO.nuovoMetodoPagamento2,
									sceltaMetodoPagamentoComponentEVO.copiaDaIntestatario2,
									sceltaMetodoPagamentoComponentEVO.ibanField3, sceltaMetodoPagamentoComponentEVO.saveMetod3,
									sceltaMetodoPagamentoComponentEVO.buttonConferma2,
									sceltaMetodoPagamentoComponentEVO.nomeIntestatarioCC, "AUTOMATION",
									sceltaMetodoPagamentoComponentEVO.ragioneSocialeCC, "AUTOMATION",
									sceltaMetodoPagamentoComponentEVO.codiceFiscaleCC, prop.getProperty("CODICE_FISCALE"));	    
				}
					else if(tipoOperazione.contentEquals("PRIMA_ATTIVAZIONE_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaSDD();
					}
					else if(tipoOperazione.contentEquals("RICONTRATTUALIZZAZIONE_RID")) {
						sceltaMetodoPagamentoComponentEVO.selezionaIBANSearch(prop.getProperty("IBAN"),sceltaMetodoPagamentoComponentEVO.campoRicerca );
						sceltaMetodoPagamentoComponentEVO.selezionaSDD();
					}
					else if(tipoOperazione.contentEquals("SUBENTRO_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaNuovoMetodoPagamentoSub(prop.getProperty("IBAN"),
					    		sceltaMetodoPagamentoComponentEVO.nuovoMetodoPagamento2,
								sceltaMetodoPagamentoComponentEVO.copiaDaIntestatario2,
								sceltaMetodoPagamentoComponentEVO.ibanField5, sceltaMetodoPagamentoComponentEVO.saveMetod2,
								sceltaMetodoPagamentoComponentEVO.buttonConferma2,
								sceltaMetodoPagamentoComponentEVO.nomeIntestatarioCC, "AUTOMATION",
								sceltaMetodoPagamentoComponentEVO.ragioneSocialeCC, "AUTOMATION",
								sceltaMetodoPagamentoComponentEVO.codiceFiscaleCC, "CCCCCC85T15H703W");
					}
						else {
							sceltaMetodoPagamentoComponentEVO.selezionaNuovoMetodoPagamento(prop.getProperty("interactionFrame"),
								prop.getProperty("IBAN"), sceltaMetodoPagamentoComponentEVO.rid,
								sceltaMetodoPagamentoComponentEVO.nuovoMetodoPagamento2,
								sceltaMetodoPagamentoComponentEVO.copiaDaIntestatario2,
								sceltaMetodoPagamentoComponentEVO.ibanField5, sceltaMetodoPagamentoComponentEVO.saveMetod2,
								sceltaMetodoPagamentoComponentEVO.checkPayMetod);
						}
					logger.write("Selezione RID");
				}
				prop.setProperty("RETURN_VALUE", "OK");
				}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}
	}
