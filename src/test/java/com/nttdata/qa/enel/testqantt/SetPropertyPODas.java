package com.nttdata.qa.enel.testqantt; 
 
import com.nttdata.qa.enel.util.QANTTLogger; 
import com.nttdata.qa.enel.util.WebDriverManager; 
 
import java.io.FileOutputStream; 
import java.io.PrintWriter; 
import java.io.StringWriter; 
import java.util.Properties; 
 

/*
 * Classe creata da M. Lombardi 
 * Nel caso si lavori con pod multipli ma biosgna prendere uno dei pod da una query specifica,
 * possiamo assegnare qui un nome speicifico.
 * Es. POD_3 o POD_GAS
 * 
 * 
 */
public class SetPropertyPODas { 
    public static void main(String[] args) throws Exception { 
 
        Properties prop; 
        prop = WebDriverManager.getPropertiesIstance(args[0]); 
        QANTTLogger logger = new QANTTLogger(prop); 
 
        try { 
 
            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
                prop.setProperty("POD_"+prop.getProperty("SET_POD_AS"), prop.getProperty("QUERY_NE__SERVICE_POINT__C.ITA_IFM_POD__C"));
                System.out.println(prop.getProperty("POD_"+prop.getProperty("SET_POD_AS")));
            }
      
 
            prop.setProperty("RETURN_VALUE", "OK"); 
        } catch (Exception e) { 
            prop.setProperty("RETURN_VALUE", "KO"); 
            StringWriter errors = new StringWriter(); 
            e.printStackTrace(new PrintWriter(errors)); 
            errors.toString(); 
            logger.write("ERROR_DESCRIPTION: " + errors.toString()); 
 
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString()); 
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e; 
 
        } finally { 
            //Store WebDriver Info in properties file 
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info"); 
        } 
    } 
}

