package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SetClUscente_VCA7 {


	public static void main(String[] args) throws Exception
	{
		Properties prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {


			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);
			logger.write("Settaggio cliente ucente mediante query - Start");

			int rigaDaEstrarre=Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE", "1"));
			//SET CLIENTE USCENTE
			prop.setProperty("QUERY",Costanti.recupera_CF_POD_per_VoltureConAccolloid7);
			Map<Integer,Map> lista_Cl_Uscente=wb.EseguiQuery(Costanti.recupera_CF_POD_per_VoltureConAccolloid7);

			Map row=lista_Cl_Uscente.get(rigaDaEstrarre);

			prop.setProperty("CODICE_FISCALE_CL_USCENTE", row.get("Account.NE__Fiscal_code__c").toString());
			prop.setProperty("POD", row.get("NE__Service_Point__c.ITA_IFM_POD__c").toString());

			logger.write("CF CL Uscente: "+prop.getProperty("CODICE_FISCALE_CL_USCENTE"));
			logger.write("POD CL Uscente: "+prop.getProperty("POD"));

			logger.write("Settaggio cliente ucente mediante query - End");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}


