package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaOffertaInviataComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaOffertaInviataVoltureConAccollo {
	private final static int SEC = 120;
	
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			VerificaOffertaInviataComponent offer=new VerificaOffertaInviataComponent(driver);
			
			// Controlla le seguenti informazioni: -Tipo =  Voltura Con Accollo, -Creata il, -Stato = Chiusa
			offer.verificaElementExist(offer.sezioneNavigazioneProcesso);
			offer.verificaElementExist(offer.sezioneNavigazioneProcessoTipo);
			offer.verificaElement(offer.sezioneNavigazioneProcessoTipoValue, "VOLTURA CON ACCOLLO");
			offer.verificaElementExist(offer.sezioneNavigazioneProcessoCreatoIl);
			offer.verificaElementExist(offer.sezioneNavigazioneProcessoStato);
			offer.verificaElement(offer.sezioneNavigazioneProcessoStatoValue, "CHIUSO");

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
