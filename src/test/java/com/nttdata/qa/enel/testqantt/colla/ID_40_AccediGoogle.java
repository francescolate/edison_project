package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_40_AccediGoogle {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			//Login Page
			LoginPageComponent log = new LoginPageComponent(driver);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
			
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
//			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
//			if(log.checkComponentExistence(log.homeFullscreenAlertCloseButton))
//			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
//			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
//			By accettaCookie = log.buttonAccetta;
//			log.verifyComponentExistence(accettaCookie);
//			Thread.sleep(5000);
//			log.clickComponent(accettaCookie);
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			Thread.sleep(5000);
			log.clickComponentIfExist(log.homeFullscreenAlertCloseButton);
			log.clickComponentIfExist(log.buttonAccetta);
			Thread.sleep(5000);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
//			By accettaCookie = log.buttonAccetta;
//			log.verifyComponentExistence(accettaCookie);
//			log.clickComponent(accettaCookie);
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("Check login page - completed");
			
			logger.write("Check the user name and password fields - start");
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			logger.write("Check the user name and password fields - completed");
			
			logger.write("Check the Login button - start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			logger.write("Check the Login button - completed");

			By recName = log.RecUserName;
	        log.verifyComponentExistence(recName);
	        By RecPasswd=log.RecUserPasswd;
	        log.verifyComponentExistence(RecPasswd);
	        
	        log.verifyComponentExistence(log.haiProblemiText);
	        log.comprareText(log.haiProblemiText, LoginPageComponent.HAI_PROBLEMI_TEXT, true);
        
	        logger.write("Check the google and facebook  - start");
	        By signInFacebook=log.FacebookSignIn;
	        log.verifyComponentExistence(signInFacebook);
	        By signInGoogle=log.GoogleSignIn;
	        log.verifyComponentExistence(signInGoogle);
	        logger.write("Check the google and facebook - completed");
	        
	        log.verifyComponentExistence(log.nonHaiAccount);
	        log.comprareText(log.nonHaiAccount, LoginPageComponent.NON_HAI_ACCOUNT, true);
	        
	        logger.write("Check the registrati button  - start");
	        log.verifyComponentExistence(log.registratiButton);
	        logger.write("Check the registrati button  - ends");
	        
	        logger.write("Check the accedi google  - start"); 
	        log.clickComponent(signInGoogle);
	        logger.write("Check the accedi google  - Ends");
	        
	        if(!log.checkComponentExistence(By.xpath(log.accountItem.replace("$ACCOUNT_ITEM$", prop.getProperty("USERNAME"))))){
		        log.verifyComponentExistence(log.googleUserId);
		        log.enterLoginParameters(log.googleUserId, prop.getProperty("USERNAME"));
		        
		        log.verifyComponentExistence(log.nextButton);
		        log.clickComponent(log.nextButton);
		        
		        log.verifyComponentExistence(log.googlePassword);
		        log.enterLoginParameters(log.googlePassword, prop.getProperty("PASSWORD"));
		        
		        log.verifyComponentExistence(log.nextButtonPassword);
		        log.clickComponent(log.nextButtonPassword);
	        }else
	        	log.clickComponent(By.xpath(log.accountItem.replace("$ACCOUNT_ITEM$", prop.getProperty("USERNAME"))));
		        
	        Thread.sleep(15000);
	        iee.comprareText(iee.homepageHeadingRES, InfoEnelEnergiaACRComponent.HOMEPAGE_HEADING_RES, true);
			iee.comprareText(iee.homepageParagraphRES, InfoEnelEnergiaACRComponent.HOMEPAGE_PARAGRAPH_RES, true);	      	
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
