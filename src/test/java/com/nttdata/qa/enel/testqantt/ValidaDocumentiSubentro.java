package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ValidaDocumentiSubentro {
	@Step("Valida Documenti")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			driver.switchTo().defaultContent();
			DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
			DocumentValidatorComponent validator = new DocumentValidatorComponent(driver);

			logger.write("Visualizza Documento - Start");
			TimeUnit.SECONDS.sleep(10);
			doc.visualizzaDocumenti();
			System.out.println("Visualizza Documento");
			logger.write("Visualizza Documento - Completed");

			
			
			logger.write("Valida Documento - Start");
			TimeUnit.SECONDS.sleep(10);
			
			validator.validaDocSubentro(util.getFrameByIndex(1));
			System.out.println("Validazione completata");
			logger.write("Valida Documento - Completed");
			TimeUnit.SECONDS.sleep(10);

			logger.write("Chiudi Validator Documento - Start");
			validator.chiudi();

			logger.write("Chiudi Validator Documento - Completed");
			driver.navigate().refresh();
			validator.verifyComponentExistence(validator.statoChiusaOfferta);
			prop.setProperty("RETURN_VALUE", "OK");
			System.out.println("test ended");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
