package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SfdcVerifyInfoEEActivationComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SfdcVerifyInfoEEActivation {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				
				SfdcVerifyInfoEEActivationComponent sfdcVer = new SfdcVerifyInfoEEActivationComponent(driver);
				
				System.out.println("Searching for CF/IVA");
				logger.write("Searching for CF/IVA - Start");
				driver.manage().window().maximize();
				sfdcVer.searchCfIva(prop.getProperty("CF"));
				logger.write("Searching for CF/IVA - Completed");
				
				System.out.println("Accessing page for client");
				logger.write("Accessing page for client - Start");
				sfdcVer.verifyVisibilityThenClick(sfdcVer.clientNameLink);
				logger.write("Accessing page for client - Completed");
				
				System.out.println("Accessing richieste and selecting latest AttivazioneVas");
				logger.write("Accessing richieste and selecting latest AttivazioneVas - Start");
				sfdcVer.accessRichieste();
				sfdcVer.selectLatestAttivazioneVas();
				logger.write("Accessing richieste and selecting latest AttivazioneVas - Completed");
				
				System.out.println("Checking activation status");
				logger.write("Checking activation status - Start");
				sfdcVer.checkActivationStatus();
				logger.write("Checking activation status - Completed");
				
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
