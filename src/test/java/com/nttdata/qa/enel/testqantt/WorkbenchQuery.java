package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class WorkbenchQuery {
    public boolean throwExceptionIfThreAreNoresult=true;
    public int tentativi = 5;
	private final static int SEC = 120;
	private  RemoteWebDriver driver;
	private  WorkbenchComponent a;
	private  QANTTLogger logger;
	
	
	public  void Login(String properties) throws Exception
	{
		Properties prop = new Properties();
		prop.load(new FileInputStream(properties));
		RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
		logger = new QANTTLogger(prop);
		
		
		a = new WorkbenchComponent(driver);
		prop = WebDriverManager.getPropertiesIstance(properties);
		
	

		if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {


			page.navigate(Costanti.salesforceLink);
			page.enterUsername(Costanti.workbenchUser);
			page.enterPassword(Costanti.workbenchPwd);
			page.submitLogin();
			TimeUnit.SECONDS.sleep(10);
			page.navigate(Costanti.workbenchLink);
			WorkbenchComponent a = new WorkbenchComponent(driver);
			a.selezionaEnvironment(Costanti.workbenchSelectionEnv);
			a.pressButton(a.checkAgree);
			a.pressButton(a.buttonLogin);
			TimeUnit.SECONDS.sleep(5);

			//non può essere infinito un ciclo !!!!
			//dopo 3 tentativi l'utenza si locka
			int i = 0;

			while ((!driver.getCurrentUrl().startsWith(Costanti.workbenchLink)) && i < 2) {
				//TODO COUNT TENTATIVI DI LOGIN
				page.enterUsername(Costanti.workbenchUser);
				page.enterPassword(Costanti.workbenchPwd);
				page.submitLogin();
				TimeUnit.SECONDS.sleep(2);
				i += 1;
			}

			a.AbilitaJoin();
		}//
	}//Fine metodo Login

	public void SaveRowToProperties(Map row, Properties prop)
	{
		for(Object key: row.keySet())
		{
			prop.setProperty(key.toString().toUpperCase(), row.get(key).toString());
		}
	}
	
	public   Map<Integer,Map>  EseguiQuery(String query) throws Exception
	{
		
		
			a.inserisciNuovaQuery();
			Thread.sleep(10000);
			a.insertQuery(query);
			boolean condition = false;
			//int tentativi = 5;
			while (!condition && tentativi-- > 0) {
				a.pressButton(a.submitQuery);
				condition = a.aspettaRisultati(SEC);
			}
			if (condition) {
				TimeUnit.SECONDS.sleep(3);
				 return a.recuperaTuttiIRisultati();

			} else
			{
				if(throwExceptionIfThreAreNoresult)
					throw new Exception("Impossibile recuperare i risultati dalla query workbench. Sono stati fatti : " + tentativi + "tentativi di recupero");
				else
					return new HashMap<Integer, Map>();
			}
			
				
		
			
		
}

	public static String GeneraInClause(Map<Integer,Map> rows, String colName) throws Exception
	{
		//Da utilizzare dopo aver chiamato il metodo RecuperaTuttiIRisultati
		if(rows.isEmpty()) 	throw new Exception("Nessun record da estrarre");

		String inCondition="";
		//GENERO IN PER DA INSERIRE IN SECONDA QUERY
		for(Integer r :rows.keySet())
		{
			if(!rows.get(r).containsKey(colName)) throw new Exception("Impossibile trovare la colonna "+colName);
			inCondition+=(inCondition.equals("")?"":",")+"'"+rows.get(r).get(colName)+"'";
		}


		return inCondition;
	}

}