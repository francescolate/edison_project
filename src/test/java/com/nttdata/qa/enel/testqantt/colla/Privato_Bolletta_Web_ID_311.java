package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID_311 {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
	
	PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
	DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);

	dfc.verifyComponentExistence(dfc.BenvenutoTitle);
	dfc.comprareText(dfc.BenvenutoTitle, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_TITLE, true);
	dfc.verifyComponentExistence(dfc.BenvenutoContent);
	dfc.comprareText(dfc.BenvenutoContent, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_CONTENT, true);
	dfc.verifyComponentExistence(dfc.fornitureHeader);
	dfc.comprareText(dfc.fornitureHeader, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_HEADER, true);
	dfc.verifyComponentExistence(dfc.fornitureContent);
	dfc.comprareText(dfc.fornitureContent, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_CONTENT, true);
	
	logger.write("Verify and click on Servizi link  - Start");
	dfc.verifyComponentExistence(dfc.serviziLink);
	dfc.clickComponentIfExist(dfc.serviziLink);
	logger.write("Verify and click on Servizi link  - Complete");
	
	logger.write("Verify and click on Servizi title and contents  - Start");
	dfc.verifyComponentExistence(dfc.serviziFornitureTitle);
	dfc.comprareText(dfc.serviziFornitureTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_TITLE, true);
	dfc.verifyComponentExistence(dfc.serviziFornitureContent);
	dfc.comprareText(dfc.serviziFornitureContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_CONTENT, true);
	dfc.verifyComponentExistence(dfc.serviziBolletteTitle);
	dfc.comprareText(dfc.serviziBolletteTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_TITLE, true);
	dfc.verifyComponentExistence(dfc.serviziBolletteContent);
	dfc.comprareText(dfc.serviziBolletteContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_CONTENT, true);
	dfc.verifyComponentExistence(dfc.serviziContratoTitle);
	dfc.comprareText(dfc.serviziContratoTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_TITLE, true);
	dfc.verifyComponentExistence(dfc.serviziContratoContent);
	dfc.comprareText(dfc.serviziContratoContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_CONTENT, true);
	logger.write("Verify and click on Servizi title and contents  - Complete");
	
	logger.write("Verify and click on BollettaWeb Link  - Start");
	dfc.verifyComponentExistence(dfc.bollettaWebLink);
	dfc.clickComponent(dfc.bollettaWebLink);
	logger.write("Verify and click on BollettaWeb Link  - Complete");
	
	logger.write("Verify the Bolletta web header and contents  - Start");
	dfc.verifyComponentExistence(dfc.bollettaWebHeader);
	dfc.comprareText(dfc.bollettaWebHeader, PrivateAreaDisattivazioneFornituraComponent.BOLLETTAWEB_HEADER, true);
	dfc.verifyComponentExistence(dfc.bollettaWebContent);
	dfc.comprareText(dfc.bollettaWebContent, PrivateAreaDisattivazioneFornituraComponent.BOLLETTAWEB_CONTENT, true);
	logger.write("Verify the Bolletta web header and contents  - Complete");
	
	logger.write("Click on Attiva Bolletaweb button  - Start");
	dfc.verifyComponentExistence(dfc.attivaBollettaWebButton);
	dfc.clickComponent(dfc.attivaBollettaWebButton);
	logger.write("Click on Attiva Bolletaweb button  - Complete");
	
	logger.write("Select the supply by clicking on the checkbox  - Start");
	dfc.verifyComponentExistence(dfc.checkbox1);
	dfc.clickComponent(dfc.checkbox1);
	logger.write("Select the supply by clicking on the checkbox  - Complete");
	
	logger.write("Verify the Attivazion header and contents  - Start");
	dfc.verifyComponentExistence(dfc.attivazioneHeader);
	dfc.comprareText(dfc.attivazioneHeader, PrivateAreaDisattivazioneFornituraComponent.ATTIVAZIONE_HEADER, true);
	dfc.verifyComponentExistence(dfc.attivazioneContent);
	dfc.comprareText(dfc.attivazioneContent, PrivateAreaDisattivazioneFornituraComponent.ATTIVAZIONE_CONTENT1, true);
	logger.write("Verify the Attivazion header and contents  - Complete");
	
	logger.write("Click on Continua button - Start");
	dfc.verifyComponentExistence(dfc.supplyContinuaButton);
	dfc.clickComponent(dfc.supplyContinuaButton);
	logger.write("Click on Continua button - Complete");
	
	logger.write("Verify the continua and Indietro buttons - Start");
	dfc.waitForElementToDisplay(dfc.statusContinuaButton);
	dfc.verifyComponentExistence(dfc.statusIndietroButton);
	logger.write("Verify the continua and Indietro buttons - Complete");
	
	dfc.clickComponent(dfc.statusContinuaButton);
	
	dfc.verifyComponentExistence(dfc.modificaBollettaEmailInput);
	dfc.enterInput(dfc.modificaBollettaEmailInput, prop.getProperty("EMAIL"));
	
	dfc.verifyComponentExistence(dfc.modificaBollettaConfermaEmail);
	dfc.enterInput(dfc.modificaBollettaConfermaEmail, prop.getProperty("CONFERMA_EMAIL"));
	
	dfc.verifyComponentExistence(dfc.modificaBollettaNumeroInput);
	
	dfc.enterInput(dfc.modificaBollettaNumeroInput, prop.getProperty("NUMERO"));
	dfc.verifyComponentExistence(dfc.modificaBollettaNumeroConferma);
	dfc.enterInput(dfc.modificaBollettaNumeroConferma, prop.getProperty("CONFERMA_NUMERO"));
	
	dfc.verifyComponentExistence(dfc.modificaBollettaCheckboxInput);
	dfc.verifyComponentExistence(dfc.modificaBollettaCheckboxLabel);
	dfc.clickComponent(dfc.modificaBollettaCheckboxLabel);
	
	dfc.verifyComponentExistence(dfc.statusContinuaButton);
	dfc.clickComponent(dfc.statusContinuaButton);
	
	dfc.verifyComponentExistence(dfc.BollettanumerodicellulareLabel);
	dfc.comprareText(dfc.BollettanumerodicellulareLabel, PrivateAreaDisattivazioneFornituraComponent.BOLLETTA_CELLULARELABEL, true);
	dfc.verifyComponentExistence(dfc.bollettaNumeroCellulareValue);
	dfc.comprareText(dfc.bollettaNumeroCellulareValue, PrivateAreaDisattivazioneFornituraComponent.BOLLETTA_CELLULAREVALUE, true);
	
	dfc.verifyComponentExistence(dfc.bollettaStatusContent);
	dfc.comprareText(dfc.bollettaStatusContent, PrivateAreaDisattivazioneFornituraComponent.BOLLETTASTATUS_CONTENT, true);
	
	
	logger.write("Click on Forniture link - Start");
	dfc.verifyComponentExistence(dfc.fornitureLink);
	dfc.clickComponent(dfc.fornitureLink);
	logger.write("Click on Forniture link - Complete");
	
	logger.write("Verify the Popup header and buttons - Start");
	dfc.verifyComponentExistence(dfc.popupHeader);
	dfc.comprareText(dfc.popupHeader, PrivateAreaDisattivazioneFornituraComponent.POPUPHEADER, true);
	dfc.verifyComponentExistence(dfc.popupContent1);
	dfc.comprareText(dfc.popupContent1, PrivateAreaDisattivazioneFornituraComponent.POPUPCONTENT1, true);
	dfc.verifyComponentExistence(dfc.popupSIButton);
	dfc.verifyComponentExistence(dfc.popupNoButton);
	logger.write("Verify the Popup header and buttons - Complete");
	

	logger.write("Click on Popup SI button - Start");
	dfc.clickComponent(dfc.popupSIButton);
	logger.write("Click on Popup SI button - Complete");
	
	
	logger.write("Verify the home page title and contents - Start");
	dfc.verifyComponentExistence(dfc.BenvenutoTitle);
	dfc.comprareText(dfc.BenvenutoTitle, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_TITLE, true);
	dfc.verifyComponentExistence(dfc.BenvenutoContent);
	dfc.comprareText(dfc.BenvenutoContent, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_CONTENT, true);
	dfc.verifyComponentExistence(dfc.fornitureHeader);
	logger.write("Verify the home page title and contents - Complete");
	
	
	
	prop.setProperty("RETURN_VALUE", "OK");
	
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	




}
