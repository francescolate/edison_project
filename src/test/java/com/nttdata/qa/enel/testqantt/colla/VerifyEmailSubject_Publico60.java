package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.BSN_AttivazioneInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerifyEmailSubject_Publico60 {

	@Step("Leggi email e clicca sul Touch Point di Attivazione")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String link = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
					prop.setProperty("USERNAME",Costanti.utenza_Tp2_crm);
					prop.setProperty("PASSWORD",Costanti.password_Tp2_crm);
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            link = gm.recuperaLink(prop, "CONFERMA_OFFERTA", false);
		            Thread.sleep(5000);
		            System.out.println(link);
		            
		            LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
		            log.launchLink(prop.getProperty("HOMEPAGE"));
		            Thread.sleep(10000);
		            log.launchLink(link);
		            Thread.sleep(10000);
		           
		            logger.write("Recupera Link - Completed");
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
