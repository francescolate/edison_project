package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Info_Enel_Energia_ACB_285 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);	
			
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Correct visualization of the Home Page with text- Ends");

			logger.write("Click on servizi and verify the data- Starts");
			iee.clickComponent(iee.servizi);
			iee.comprareText(iee.serviziHomepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.serviziHomepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_SERVIZI_PATH2, true);
			iee.comprareText(iee.serviziText, InfoEnelEnergiaACRComponent.SERVIZI_TEXT, true);
			logger.write("Click on servizi and verify the data- Ends");

			logger.write("Verify Info EnelEnergia page title and content -- Starts");
			iee.clickComponent(iee.infoEnelEnergia);
			Thread.sleep(20000);
			iee.comprareText(iee.infoEnelEnergiaTitle, InfoEnelEnergiaACRComponent.INFO_ENEL_ENERGIA_TITLE, true);
			iee.comprareText(iee.infoEnelEnergiaText, InfoEnelEnergiaACRComponent.INFO_ENEL_ENERGIA_TEXT, true);
			iee.comprareText(iee.modifica, InfoEnelEnergiaACRComponent.MODIFICA, true);
			iee.comprareText(iee.modificaText, InfoEnelEnergiaACRComponent.MODIFICA_TEXT, true);
			iee.clickComponent(iee.scopriDiPIUModifica);
			Thread.sleep(20000);
			logger.write("Verify Info EnelEnergia page title and content -- Ends");
			
			logger.write("Verify Info EnelEnergia modifica page title and content -- Starts");
			Thread.sleep(20000);
			BSN_ModificaInfoEnelEnergiaComponent bmc = new BSN_ModificaInfoEnelEnergiaComponent(driver);
			bmc.comprareText(bmc.pageTitleNew, BSN_ModificaInfoEnelEnergiaComponent.PageTitle, true);
			bmc.comprareText(bmc.pageSubText, BSN_ModificaInfoEnelEnergiaComponent.PageSubText, true);
			bmc.comprareText(bmc.menuOneDescription, BSN_ModificaInfoEnelEnergiaComponent.MenuOneDescription, true);
			logger.write("Verify Info EnelEnergia modifica page title and content -- Ends");
			
			logger.write("Verify Info EnelEnergia modifica page title check boxes and content -- Starts");
			bmc.comprareText(bmc.avvisoComunicaLettura, BSN_ModificaInfoEnelEnergiaComponent.AvvisoComunicaLettura, true);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaSMS);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaEmail);
			
			bmc.comprareText(bmc.avvisoEmissioneBolletta, BSN_ModificaInfoEnelEnergiaComponent.AvvisoEmissioneBolletta, true);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaSMS);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaEmail);
			
			bmc.comprareText(bmc.avvenutoPagamento, BSN_ModificaInfoEnelEnergiaComponent.AvvenutoPagamento, true);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoEmail);
			
			bmc.comprareText(bmc.avvisoProssimaScadenza, BSN_ModificaInfoEnelEnergiaComponent.AvvisoProssimaScadenza, true);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaSMS);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaEmail);
			
			bmc.comprareText(bmc.avvisoSollecitoPagamento, BSN_ModificaInfoEnelEnergiaComponent.AvvisoSollecitoPagamento, true);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoEmail);
			
			bmc.comprareText(bmc.avvisoConsumi, BSN_ModificaInfoEnelEnergiaComponent.AvvisoConsumi, true);
			bmc.verifyComponentExistence(bmc.avvisoConsumiSMS);
			bmc.verifyComponentExistence(bmc.avvisoConsumiEmail);
			logger.write("Verify Info EnelEnergia modifica page title check boxes and content -- Ends");
			
			logger.write("Verify the Email and Phone fields -- Starts");
			bmc.verifyComponentExistence(bmc.email);
			bmc.verifyComponentExistence(bmc.confirmEmail);
			bmc.verifyComponentExistence(bmc.numeroDiCellulare);
			bmc.verifyComponentExistence(bmc.confermaCellulare);
			logger.write("Verify the Email and Phone fields -- Ends");
			
			logger.write("Verify the AggiornaIDatiDiContatto check box -- Starts");
			bmc.verifyComponentExistence(bmc.aggiornaIDatiDiContatto);
			logger.write("Verify the AggiornaIDatiDiContatto check box -- Ends");
			
			logger.write("Click on INDIETRO button and verify it goes to previous page -- Starts");
			bmc.clickComponent(bmc.indietro);
			iee.clickComponent(iee.scopriDiPIUModifica);
			logger.write("Click on INDIETRO button and verify it goes to previous page -- Ends");

			logger.write("Input to the Email fields -- Starts");
			bmc.clickComponent(bmc.avvisoComunicaLetturaEmail);
			bmc.enterInputParameters(bmc.email, prop.getProperty("EMAIL"));
			bmc.enterInputParameters(bmc.confirmEmail, prop.getProperty("CONFERMA_EMAIL"));
			bmc.clickComponent(bmc.prosegui);
			logger.write("Input to the Email fields -- Ends");
			
			/*logger.write("Verify the data in step2 and confirm -- Starts");
			bmc.comprareText(bmc.supplyDataText, BSN_ModificaInfoEnelEnergiaComponent.SupplyDataText, true);
			bmc.comprareText(bmc.tipo, BSN_ModificaInfoEnelEnergiaComponent.Tipo, true);
			bmc.comprareText(bmc.numeroCliente, BSN_ModificaInfoEnelEnergiaComponent.NumeroClienteAlias, true);
			bmc.comprareText(bmc.tipoValue, BSN_ModificaInfoEnelEnergiaComponent.TipoValue, true);
			bmc.comprareText(bmc.numeroClientiValue, BSN_ModificaInfoEnelEnergiaComponent.NumeroClientiValue1, true);
			bmc.comprareText(bmc.addressValue, BSN_ModificaInfoEnelEnergiaComponent.AddressValue, true);
			logger.write("Verify the data in step2 and confirm -- Ends");*/
			
		    logger.write("Verify the data in step2 and confirm -- Starts");
		    bmc.verifyComponentExistence(bmc.prosegui);
			bmc.clickComponent(bmc.prosegui);
			//bmc.comprareText(bmc.step3Text, BSN_ModificaInfoEnelEnergiaComponent.Step3Text, true);
			bmc.verifyComponentExistence(bmc.fine);
			bmc.clickComponent(bmc.fine);
			logger.write("Verify the data in step2 and confirm -- Ends");
			
		/*	logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Correct visualization of the Home Page with text- Ends");
			*/
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
