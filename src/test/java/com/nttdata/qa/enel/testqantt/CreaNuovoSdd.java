package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.nttdata.qa.enel.components.lightning.AttivazioneSDDCreaNuovoMetodoDiPagamento;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
/**
 * creazione nuovo metodo di pagamento e relative verifiche
 * @param ATT_SDD_NOME proprietà settata automaticamente da class SetCfForGestioneSdd 
 * @param ATT_SDD_COGNOME proprietà settata automaticamente da class SetCfForGestioneSdd 
 * @param ATT_SDD_RAGIONE_SOCIALE proprietà settata automaticamente da class SetCfForGestioneSdd 
 * @param ATT_SDD_CF proprietà settata automaticamente da class SetCfForGestioneSdd 
 * @param ATT_SDD_IBAN da settare manualmente nella configurazione dello scenario
 * @param ATT_SDD_IBAN_ESTERO da settare manualmente nella configurazione dello scenario (Y/N)
 * @param TIPOLOGIA_CLIENTE da settare manualmente nella configurazione dello scenario (BUSINESS o RESIDENZIALE)
 * @param ATT_SDD_BANCA  da settare manualmente nella configurazione dello scenario
 * @param ATT_SDD_AGENZIA da settare manualmente nella configurazione dello scenario
 * @param ATT_SDD_STATO da settare manualmente nella configurazione dello scenario
 * @param ATT_SSD_MSG_VALIDAZIONE_IBAN da settare manualmente nella configurazione dello scenario - 
 * 								valorizzare solo se è necessario effettuare controlli sulla validazione dell'IBAN
 * @param ATT_SDD_VERIFICA_DEPOSITO_CAUZUIONALE valore di default Y - valorizzare a N se non si vuole verificare la tabella del deposito cauzionale 
 * @param ATT_SDD_CLICK_SALVA_NUOVO_RID valore di default Y - valorizzare a N se non si vuole cliccare su SALVA quando si crea nuovo metodo pagamento
 * @param ATT_SDD_VERIFICA_CREAZIONE_NUOVO_RID valore di default Y - valorizzare a N se non si è cliccato su salva e quindi non si vogliono effettuare le verifiche 
 * @param ATT_SDD_CLICK_CONFERMA_NUOVO_RID valore di default Y - valorizzare a N se non si è cliccato su salva e quindi non si vogliono effettuare le verifiche 
 * @throws Exception
 */
public class CreaNuovoSdd {


	static QANTTLogger logger=null;
 
	static Properties prop=null;


	public static void main(String[] args) throws Exception {
		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				
				AttivazioneSDDCreaNuovoMetodoDiPagamento nuovoPag=new AttivazioneSDDCreaNuovoMetodoDiPagamento(WebDriverManager.getDriverInstance(prop));
				
				String nome=prop.getProperty("ATT_SDD_NOME");
				String cognome=prop.getProperty("ATT_SDD_COGNOME",prop.getProperty("ATT_SDD_RAGIONE_SOCIALE"));
				String cf=prop.getProperty("ATT_SDD_CF");
				String IBAN=prop.getProperty("ATT_SDD_IBAN");
				boolean isIbanEstero=prop.getProperty("ATT_SDD_IBAN_ESTERO","NO").contentEquals("Y");
				boolean isClienteBusiness=prop.getProperty("TIPOLOGIA_CLIENTE").contentEquals("BUSINESS");
				 
				String banca=prop.getProperty("ATT_SDD_BANCA");
				String agenzia=prop.getProperty("ATT_SDD_AGENZIA");
				String stato=prop.getProperty("ATT_SDD_STATO");
				
				
				
				String msg_validazione_iban=prop.getProperty("ATT_SSD_MSG_VALIDAZIONE_IBAN","");
				
				if(prop.getProperty("ATT_SDD_CHECK_OBBLIGATORIETA_CAMPI","N").equals("Y"))
				{
					nuovoPag.VerificaObbligatorietaCampi(isClienteBusiness);
				}
				
				if(isClienteBusiness)
				{
					nuovoPag.CompilaFormNuovoRID_Bsn(nome, cognome, cf, IBAN, isIbanEstero);
					
				}
				else
				{
					nuovoPag.CompilaFormNuovoRID_Res(nome, cognome, cf, IBAN, isIbanEstero);
				}
				if(prop.getProperty("ATT_SDD_CLICK_SALVA_NUOVO_RID","Y").equals("Y"))
				{
					//CLICK SALVATAGGIO BOTTONE NUOV RID
					nuovoPag.SalvaNuovoRid();
				}
				
				if(prop.getProperty("ATT_SDD_VERIFICA_CREAZIONE_NUOVO_RID","Y").equals("Y"))
				{
					//VERIFICA TABELLA METODO DI PAGMENTO E CREAZIONE NUOVO METODO DI PAGAMENTO
					logger.write("Verifica Creazione nuovo metodo di pagamento start");
					nuovoPag.VerificaCreazioneNuovoMetodoDiPagamento(IBAN, nome,cognome,cf,banca,agenzia,stato,isClienteBusiness);
					logger.write("Verifica Creazione nuovo metodo di pagamento completed");
//					In alcuni scenari di Variazione SDD non è previsto il controllo sulla tabella del deposito cauzionale
					if(prop.getProperty("ATT_SDD_VERIFICA_DEPOSITO_CAUZUIONALE","Y").equals("Y"))
					{		
						//TABELLA DEPOSITO CAUZIONALE
						logger.write("Verifica Tabella deposito Cauzionale start");
						nuovoPag.VerificaTabellaDepositoCauzionale();
						logger.write("Verifica Tabella deposito Cauzionale Completed");
					}

				}
				if(prop.getProperty("ATT_SDD_CLICK_CONFERMA_NUOVO_RID","Y").equals("Y"))
				{
					logger.write("Conferma nuovo Rid - Start");
					nuovoPag.CliccaConferma();
					logger.write("Conferma nuovo Rid - Completed");
				}
				if(!msg_validazione_iban.contentEquals(""))
				{
					logger.write("Verifica Messaggio validazione - start");
					nuovoPag.VerificaMessaggioValidazioneIban(msg_validazione_iban);
					TimeUnit.SECONDS.sleep(15);
					logger.write("Verifica Messaggio validazione - completed");
					nuovoPag.AnnullaCreazioneNuovoRid();

					
					
					
				}
				
				
			}
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}

	}

}
