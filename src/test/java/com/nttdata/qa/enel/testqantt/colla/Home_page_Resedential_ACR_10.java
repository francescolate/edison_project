package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomePageResidentialComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Home_page_Resedential_ACR_10 {

	public static void main(String[] args) throws Exception  {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
//		    prop.setProperty("LOGIN_POPUP_TEXT", "Gentile cliente, dalle ore 22:00 di Venerdì 24 Aprile alle ore 07:00 di lunedì 27 Aprile i servizi digitali dell’area clienti web e della app saranno in manutenzione. Lavoreremo per migliorarne la qualità. Anticipa in questi giorni le operazioni di gestione della tua fornitura come il pagamento della bolletta, il download del PDF della bolletta o l’attivazione di uno dei nostri servizi.Il servizio di modifica fascia base o giornaliera e verifica dei consumi per i clienti con offerta ORE FREE, sarà sempre disponibile dalla App di Enel Energia. Se non l’hai ancora fatto, scarica la nostra App!");
//		    prop.setProperty("POPUP_TITLE", "Lo sapevi che puoi addebitare l’importo della tua bolletta sulla tua carta di credito o sul tuo conto corrente?");
//		    prop.setProperty("INNER_TEXT", "Scegli la strada che non costa fatica, passa all’addebito diretto e scopri tutti i vantaggi di questo metodo di pagamento.");
		    prop.setProperty("WELCOME_TEXT", "Benvenuto nella tua area privata");
		    prop.setProperty("WELCOMEPAGE_HEADING", "In questa sezione potrai gestire le tue forniture");
		    prop.setProperty("YOUR_SUPPLIES", "Le tue forniture");
		    prop.setProperty("VIEW_INFO_SUPPLY_BILL", "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.");
		    prop.setProperty("YOUR_ACTIVITIES", "Le tue attività");
		    prop.setProperty("VIEW_ACTIVITIES_MANAGE_SUPPLY", "Qui troverai tutte le attività per poter gestire le tue forniture e scoprire i vantaggi.");

			HomePageResidentialComponent home = new HomePageResidentialComponent(driver);
			By leftMenu=home.leftMenu;
			home.homepageRESIDENTIALMenu(leftMenu);
			Thread.currentThread().sleep(12000);
			logger.write("Display of Welcome message and section verification- Starts");
			home.VerifyText(home.welcomeText, prop.getProperty("WELCOME_TEXT","Benvenuto nella tua area privata"));
			home.VerifyText(home.welcomePageHeading, prop.getProperty("WELCOMEPAGE_HEADING","In questa sezione potrai gestire le tue forniture"));
			home.VerifyText(home.yourSupplies, prop.getProperty("YOUR_SUPPLIES","Le tue forniture"));
			home.VerifyText(home.viewInfoSupplyBill, prop.getProperty("VIEW_INFO_SUPPLY_BILL"));
			home.VerifyText(home.yourActivities, prop.getProperty("YOUR_ACTIVITIES","Le tue attività"));
			home.VerifyText(home.viewActivitiesManageSupply, prop.getProperty("VIEW_ACTIVITIES_MANAGE_SUPPLY","Qui troverai tutte le attività per poter gestire le tue forniture e scoprire i vantaggi."));
			logger.write("Display of Welcome message and section verification- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}

			
			