package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class InfoEEAbbandonoACR19 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaMyComponent ieec = new InfoEnelEnergiaMyComponent(driver);
		    prop.setProperty("SupplyServices", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
		    prop.setProperty("SERVICE_ADDRESS", "VIA CETTEO CIGLIA 52 - 65128 PESCARA PE");
		    prop.setProperty("CLIENT_NUMBER", "303103573");

			logger.write("Verifying the menu on the left side after clicking on Servizi  STARTS");
			ieec.clickComponent(ieec.serviziItem);
			ieec.comprareText(ieec.serviziFornitureLbl, InfoEnelEnergiaMyComponent.SERVIZI_FORNTURE_LABEL, true);
			ieec.checkForSupplyServices(prop.getProperty("SupplyServices"));
			ieec.clickComponent(ieec.infoEnergiaIcon);
			logger.write("Verifying the menu on the left side after clicking on Servizi  ENDS");
			
			logger.write("Verifying correct display of the page with Title InfoEnelEnergia -STARTS");
			ieec.comprareText(ieec.headingInfoEnergia, InfoEnelEnergiaMyComponent.HEADING_INFOENERGIA, true);
			ieec.comprareText(ieec.headingInfoEnergia, InfoEnelEnergiaMyComponent.HEADING_INFOENERGIA, true);
			ieec.comprareText(ieec.subtitleInfoEnergia, InfoEnelEnergiaMyComponent.SUBTITLE_INFOENERGIA, true);
			logger.write("Verifying correct display of the page with Title InfoEnelEnergia -ENDS");
			
			logger.write("Verifying correct display of the Service Address and Client Number -STARTS");
			ieec.comprareText(ieec.addressInfoEEservice, prop.getProperty("SERVICE_ADDRESS"), true);
			ieec.comprareText(ieec.clientNumber, "Numero cliente "+prop.getProperty("CLIENT_NUMBER"), true);
			logger.write("Verifying correct display of the Service Address and Client Number -ENDS");
			
			logger.write("Verifying correct display of the AttivaInfoEnergia and Esci Buttons -STARTS");
			ieec.verifyComponentExistence(ieec.attivaInfoEnergiaButton);
			ieec.verifyComponentExistence(ieec.esciButton);
			logger.write("Verifying correct display of the AttivaInfoEnergia and Esci Buttons -ENDS");
			
			logger.write("Verifying the functionality of No -STARTS");
			ieec.clickComponent(ieec.serviziItem);
			ieec.comprareText(ieec.popUpHeading, InfoEnelEnergiaMyComponent.POPUP_HEADING, true);
			ieec.comprareText(ieec.popUpInnerText, InfoEnelEnergiaMyComponent.POPUP_INNERTEXT, true);
			ieec.verifyComponentExistence(ieec.noButton);
			ieec.verifyComponentExistence(ieec.siButton);
			ieec.verifyComponentExistence(ieec.closePopUp);
			Thread.sleep(3000);
			ieec.clickComponent(ieec.noButton);
			Thread.sleep(3000);
			logger.write("Verifying the functionality of No -ENDS");
			
			logger.write("Verifying the functionality of popclose -STARTS");
			ieec.clickComponent(ieec.ituoidirtti);
			ieec.comprareText(ieec.popUpHeading, InfoEnelEnergiaMyComponent.POPUP_HEADING, true);
			ieec.comprareText(ieec.popUpInnerText, InfoEnelEnergiaMyComponent.POPUP_INNERTEXT, true);
			ieec.verifyComponentExistence(ieec.noButton);
			ieec.verifyComponentExistence(ieec.siButton);
			ieec.verifyComponentExistence(ieec.closePopUp);
			Thread.sleep(3000);
			ieec.clickComponent(ieec.closePopUp);
			Thread.sleep(3000);
			logger.write("Verifying the functionality of popup close -ENDS");
			
			logger.write("Verifying the functionality of SI Button -STARTS");
			ieec.clickComponent(ieec.account);
			ieec.comprareText(ieec.popUpHeading, InfoEnelEnergiaMyComponent.POPUP_HEADING, true);
			ieec.comprareText(ieec.popUpInnerText, InfoEnelEnergiaMyComponent.POPUP_INNERTEXT, true);
			ieec.verifyComponentExistence(ieec.noButton);
			ieec.verifyComponentExistence(ieec.siButton);
			Thread.sleep(3000);
			ieec.verifyComponentExistence(ieec.closePopUp);
			ieec.clickComponent(ieec.siButton);
			Thread.sleep(3000);
			logger.write("Verifying the functionality of SI Button -ENDS");
			
			logger.write("Verifying the data and contacts- STARTS");
			ieec.comprareText(ieec.accountTitle, InfoEnelEnergiaMyComponent.ACCOUNT_TITLE, true);
			ieec.comprareText(ieec.accountSubTitle, InfoEnelEnergiaMyComponent.ACCOUNT_SUBTITLE, true);
			ieec.verifyComponentExistence(ieec.modificaContattiButton);
			ieec.comprareText(ieec.consensiTitle, InfoEnelEnergiaMyComponent.CONSENSI_TITLE, true);
			ieec.comprareText(ieec.consensiSubTitle, InfoEnelEnergiaMyComponent.CONSENSI_SUBTITLE, true);
			ieec.verifyComponentExistence(ieec.modificaConsensiButton);
			ieec.comprareText(ieec.datiRegistrazioneTitle, InfoEnelEnergiaMyComponent.DATI_REG_TITLE, true);
			ieec.comprareText(ieec.datiRegistrazionesubTitle, InfoEnelEnergiaMyComponent.DATI_REG_SUBTITLE, true);
			ieec.verifyComponentExistence(ieec.modificaDatiButton);
			logger.write("Verifying the data and contacts- ENDS");
					
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}

