package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_291_MoGe {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			logger.write("Click on Account from Residential menu -- Start");
			hm.clickComponent(hm.account);
			Thread.sleep(20000);
			logger.write("Click on Account from Residential menu -- Completed");

			AccountComponent ac = new AccountComponent(driver);
			logger.write("Verifi Account page title and subtext -- Start");
			ac.verifyComponentExistence(ac.pageTitle);
			hm.comprareText(ac.pageTitle, ac.PAGE_TITLE, true);
			hm.comprareText(ac.titleSubText, ac.TITLE_SUBTEXT, true);
			logger.write("Verifi Account page title and subtext -- Completed");

			logger.write("Click on i icon ans verify the pop up details -- Start");
			ac.clickComponent(ac.iIcon);
			ac.verifyComponentExistence(ac.popUpTitle);
			hm.comprareText(ac.popUpTitle, ac.POPUP_TITLE, true);
			hm.comprareText(ac.popUpInnerText, ac.POPUP_INNERTEXT, true);
			ac.verifyComponentExistence(ac.popUpCloseButton);
			logger.write("Click on i icon ans verify the pop up details -- Start");
			ac.clickComponent(ac.popUpCloseButton);
			Thread.sleep(3000);
			
			logger.write("Verify Dati E Contatti details -- Start");
			ac.verifyComponentExistence(ac.Titolare);
			prop.setProperty("TITOLAREVALUE", "Mancuso Salvatore");
			ac.VerifyText(ac.TitolareValue, prop.getProperty("TITOLAREVALUE"));
			
			ac.verifyComponentExistence(ac.Codice);
			prop.setProperty("CODICEVALUE", "MNCSVT58H26H325L");
			ac.VerifyText(ac.CodiceValue, prop.getProperty("CODICEVALUE"));

			ac.verifyComponentExistence(ac.Telefono);
			prop.setProperty("TELEFONOVALUE", "0818142364");
			ac.VerifyText(ac.TelefonoValue, prop.getProperty("TELEFONOVALUE"));

			ac.verifyComponentExistence(ac.Numero);
			prop.setProperty("NUMEROVALUE", "+39 00393339090890");
			ac.VerifyText(ac.NumeroValue, prop.getProperty("NUMEROVALUE"));

			ac.verifyComponentExistence(ac.Email);
			prop.setProperty("EMAILVALUE", "fabiana.manzo1@yopmail.com");
			ac.VerifyText(ac.EmailValue, prop.getProperty("EMAILVALUE"));

			ac.verifyComponentExistence(ac.PEC);
			prop.setProperty("PECVALUE", "Non presente");
			ac.VerifyText(ac.PECValue, prop.getProperty("PECVALUE"));

			ac.verifyComponentExistence(ac.Canale);
			prop.setProperty("CANALEVALUE", "Cellulare");
			ac.VerifyText(ac.CanaleValue, prop.getProperty("CANALEVALUE"));
			logger.write("Verify Dati E Contatti details -- Completed");
			logger.write("Click on modifica contatti button -- Start");
			ac.clickComponent(ac.modificaContatti);
			logger.write("Click on modifica contatti button -- Completed");
			Thread.sleep(15000);
			
			logger.write("Verify page navigation and page title -- Start");
			Thread.sleep(5000);
			ac.verifyComponentExistence(ac.datiEContattiTitle);
			hm.comprareText(ac.datiEContattiTitle, ac.PAGE_TITLE, true);
			hm.comprareText(ac.datiEContattiSubText, ac.DATI_E_CONTATTI_SUBTEXT, true);
			logger.write("Verify page navigation and page title -- Completed");

			ac.selectRadioButton();
			logger.write("Click on salva modifi button -- Start");
			ac.clickComponent(ac.SalvaModifiBtn);
			logger.write("Click on salva modifi button -- Completed");

			Thread.sleep(10000);
			ac.verifyComponentExistence(ac.OperazioneHeading_1);
			hm.comprareText(ac.OperazioneHeading, ac.OPERAZIONE_HEADING, true);
			hm.comprareText(ac.OperazioneValue, ac.OPERAZIONE_VALUE, true);
			logger.write("Click on Fine button -- Start");
			ac.clickComponent(ac.FineBtn);
			logger.write("Click on Fine button -- Completed");
			Thread.sleep(10000);
			
			hm.verifyComponentExistence(hm.pageTitleRes);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
