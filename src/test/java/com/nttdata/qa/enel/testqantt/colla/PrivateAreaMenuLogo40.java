package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.EditProfileComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteMyComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaMenuLogoComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaMenuLogoComponentConstant;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaMenuLogo40 {

		public static void main(String[] args)throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				PrivateAreaMenuLogoComponent pabc = new PrivateAreaMenuLogoComponent(driver);
																
				logger.write("checking and clicking on the Logo - START");
				pabc.verifyComponentExistence(pabc.homePageLogo);
				pabc.clickComponent(pabc.homePageLogo);
				logger.write("checking and clicking on the Logo - ENDS");
				
				logger.write("checking the Menu Display - START");
				pabc.verifyComponentExistence(pabc.modificaLink);
				pabc.verifyComponentExistence(pabc.datiDiContatto);
//				pabc.verifyComponentExistence(pabc.ilTuoProfilo);
//				logger.write("checking the Menu Display - ENDS");
//				
//				logger.write("checking the Data Display by clicking on IL TUO PROFILO- START");
//				pabc.clickComponent(pabc.ilTuoProfilo);
				pabc.comprareText(pabc.credenzialiDiAccessoHeading, PrivateAreaBolletteMyComponent.ACCESSO_HEADING, true);
				pabc.comprareText(pabc.email, pabc.EMAIL, true);
				pabc.comprareText(pabc.emailValue, pabc.EMAIL_VALUE, true);
					
				EditProfileComponent ed = new EditProfileComponent(driver);
				
				logger.write("Check For name - Start");

				ed.verifyComponentExistence(ed.namelabel);
				
				ed.checkFieldValue(ed.name, prop.getProperty("NOME"));
				
				logger.write("Check For name - Completed");
				
				logger.write("Check For cognome - Start");

				ed.verifyComponentExistence(ed.cogNomeLabel);
				
				ed.checkFieldValue(ed.cogNome, prop.getProperty("COGNOME"));
			
				logger.write("Check For cognome - Completed");

				logger.write("Check For phone number - Start");

				ed.verifyComponentExistence(ed.phoneNumLabel);
				
				ed.checkFieldValue(ed.phoneNum, prop.getProperty("CELLULARE"));

				logger.write("Check For phone number - Completed");

				logger.write("Check For codice fiscale - Start");

				ed.verifyComponentExistence(ed.codiceFiscaleLabel);
				
						
				logger.write("Check For codice fiscale - Completed");

			/*	pabc.verifyCustomerData(pabc.customerDataFields, pabc.XPATH_CUSTOMER_DATA_VALUES, PrivateAreaBolletteMyComponent.expCustDataFeild, PrivateAreaBolletteMyComponent.expCustDataValue);
				pabc.verifyComponentExistence(pabc.infoIconCircleCellulare);
				logger.write("checking the Data Display by clicking on IL TUO PROFILO- ENDS");
				
				logger.write("checking the pop text after clicking on 'i' icon- START");
				pabc.clickComponent(pabc.infoIconCircleCellulare);
				pabc.comprareText(pabc.popUpHeader, PrivateAreaBolletteMyComponent.POPUP_HEADING, true);
				pabc.comprareText(pabc.popUpTitle, PrivateAreaBolletteMyComponent.POPUP_TITLE, true);
				pabc.clickComponent(pabc.popUpclose);
				logger.write("checking the pop text after clicking on 'i' icon- ENDS");
			
				logger.write("checking the Data Display in Anagrafica azienda section- START");
				pabc.verifyCompanyData(pabc.companyDataFields, pabc.XPATH_COMPANY_DATA_VALUES, PrivateAreaBolletteMyComponent.anagraficaExpCustDataField, PrivateAreaBolletteMyComponent.anagraficaExpCustDataValue);
				logger.write("checking the Data Display in Anagrafica azienda section- ENDS"); */
				
				logger.write("Verifying the check is checked and the text associated with it START");
				pabc.verifyCheckBoxIsChecked(pabc.acceptTerm, PrivateAreaBolletteMyComponent.ACCEPT_TERMS_TEXT, pabc.XAPTH_CHKBOX1);
				pabc.verifyCheckBoxIsChecked(pabc.acceptTerm2, PrivateAreaBolletteMyComponent.ACCEPT_TERM_TEXT2, pabc.XPATH_CHKBOX2);
				logger.write("Verifying the check is checked and the text associated with it ENDS");
				
				logger.write("Verifying the pop and the text associated with after clicking Informativa Privacy  START");
				pabc.clickComponent(pabc.informativaPrivacyLink);
				pabc.comprareText(pabc.infopriPopupHeading, PrivateAreaMenuLogoComponentConstant.INFOPRIV_POPUP_HEADING, true);
				pabc.comprareText(pabc.largerText, PrivateAreaMenuLogoComponentConstant.INFOPRIV_POPUP_LARGER_TEXT, true);
				pabc.clickComponent(pabc.infoPriPopupClose);
				pabc.clickComponent(pabc.condizioniLink);
				pabc.comprareText(pabc.condizionPopupHeading,PrivateAreaMenuLogoComponentConstant.CONDIZION_POPUP_HEADING, true);
				pabc.comprareText(pabc.largerTextCondizion, PrivateAreaMenuLogoComponentConstant.CONDIZION_POPUP_LARGER_TEXT, true);
				pabc.clickComponent(pabc.condizionPopupClose);
				logger.write("Verifying the pop and the text associated with after clicking Informativa Privacy  ENDS");
							
				prop.setProperty("RETURN_VALUE", "OK");
				} catch (Throwable e) {

				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}

		}

	}
