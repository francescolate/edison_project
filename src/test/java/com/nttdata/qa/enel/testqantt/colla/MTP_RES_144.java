package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_144 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
											
			home.verifyComponentExistence(home.enelLogo);
								
			home.clickComponent(home.detaglioFurnitura);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			
			logger.write("Check For page title-- start");
			
			dc.verifyComponentExistence(dc.pageTitle);
			
			logger.write("Check For page title-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			dc.verifyComponentExistence(dc.serviziPerleforniture);
			
			Thread.sleep(10000);
			
			dc.verifyComponentExistence(dc.modificaPotenzo);
			
			dc.jsClickObject(dc.modificaPotenzo);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.ModificaSubText, true);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");

			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");
			
			changePV.verifyComponentExistence(changePV.radioButton);
			
			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on change power or voltage button -- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on change power or voltage button -- Completed");

			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.TitleSubText, true);			
			
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");

			logger.write("Click on radio button Yes  -- Start");
			
			changePV.clickComponent(changePV.radioYes);
			
			logger.write("Click on radio button Yes  -- Completed");
			
			logger.write("Check for lift type lable  -- Start");

			changePV.verifyComponentExistence(changePV.liftTypeLabel);
			
			logger.write("Check for lift type lable  -- Completed");

			changePV.verifyComponentExistence(changePV.runningCurrentLable);
			
			changePV.verifyComponentExistence(changePV.suggestedPowerLable);

			logger.write("Check for Placeholder text  -- Start");

			changePV.checkForPlaceholderText(changePV.runningCurrentInput,prop.getProperty("PLACEHOLDER"));
			
			logger.write("Check for Placeholder text  -- Completed");
			
			changePV.changeDropDownValue(changePV.liftTypeDropdown,prop.getProperty("ARGANO"));
			
			logger.write("Check For starting current label  -- Start");
			
			changePV.verifyComponentExistence(changePV.startingCurrentLable);
			
			logger.write("Check For starting current label  -- Completed");
			
			changePV.checkSuggestPowerButtonStatus();
			
			logger.write("Enter input to running current and starting current  -- Start");

			changePV.enterInputtoField(changePV.runningCurrentInput,prop.getProperty("RUNNINGCURRENTINPUT"));
			
			changePV.enterInputtoField(changePV.startingCurrentInput,prop.getProperty("STARTINGCURRENTINPUT"));

			logger.write("Enter input to running current and starting current  -- Completed");
			
			changePV.clickComponent(changePV.runningCurrentInput);

			logger.write("Check For Warning  -- Start");

			changePV.checkForWarning(changePV.agroWarning);
			
			logger.write("Check For Warning  -- Completed");	
			
			logger.write("Check For Suggested Power Value  -- Start");
			
			changePV.checkSuggestedPowerValue(changePV.suggestedPowerFieldString, prop.getProperty("SUGGESTEDPOWER"));

			logger.write("Check For Suggested Power Value  -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
