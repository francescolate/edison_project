package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CompilaDatiFornituraVolturaBusinessGas {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
					driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);

			logger.write("Selezione commodity - Start");
			compila.selezionaFornitura(compila.fornitura_gas_allaccio);
			logger.write("Selezione commodity - Completed");
		
			logger.write("Inserimento Dati Fornitura - Start");
		
			offer.popolareCampiDatiFornituraBSNGas("ABITAZIONI PRIVATE", "Riscald.to appartamento <100mq", "RESIDENZIALE", "SI", "Riscald.to appartamento <100mq".toUpperCase(),
					"20000", "CIVILE", "Uso condizionamento", "7 giorni", "Proprietà o Usufrutto","RISCALDAMENTO");
			logger.write("valorizzare i campi Categoria Merciologica SAP con 'Abitazioni Private'; Titolarietà con 'Proprità o Usufrutto'; Consumo Annuo con un valore a piacere; Flag Residente con '"+prop.getProperty("FLAG_RESIDENTE")+"' - Completed");
			
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);

			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);
			
			logger.write("Inserimento Dati Fornitura - Completed");			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
	
}
