package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CarrelloComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class checkSezioneContattiEConsensiAccettazioneCondizioniContrattuali {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			//voltura
			CarrelloComponent carrello=new CarrelloComponent(driver);
			
			
			offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
			logger.write("Esistenza nella sezione contatti e consensi Accettazione Condizioni Contrattuali - Start");
			offer.verifyComponentValue(offer.checkboxContattiConsensi, "true");
			offer.scrollComponent(offer.sezioneContattiEConsensi);
			logger.write("Esistenza nella sezione contatti e consensi Accettazione Condizioni Contrattuali - Completed");
			TimeUnit.SECONDS.sleep(1);
			logger.write("Deselezione Accettazione Condizioni Contrattuali - Start");
			offer.clickComponent(offer.checkboxContattiConsensiClick);
			TimeUnit.SECONDS.sleep(1);
			logger.write("Deselezione Accettazione Condizioni Contrattuali - Completed");
			logger.write("Messaggio di campo obbligatorio Accettazione Condizioni Contrattuali - Start");
			offer.verifyComponentExistence(offer.messaggioErroreCampoObbl);
			TimeUnit.SECONDS.sleep(1);
			logger.write("Messaggio di campo obbligatorio Accettazione Condizioni Contrattuali - Completed");
			offer.clickComponent(offer.checkboxContattiConsensiClick);
			TimeUnit.SECONDS.sleep(1);
			logger.write("Navigare fino alla sezione contatti e consensi e click su 'Conferma' - Start");
			offer.clickComponent(offer.buttonConfermaContattiConsensi);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			logger.write("Navigare fino alla sezione contatti e consensi e click su 'Conferma' - Completed");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
