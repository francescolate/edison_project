package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ContattaciComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PediusComponent;
import com.nttdata.qa.enel.components.colla.SpazioEnelComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.components.colla.UploadDocumentsComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Header {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			TopMenuComponent menu=new TopMenuComponent(driver);
			SupportoSectionComponent supporto=new SupportoSectionComponent(driver);
			
		    logger.write("click sul tab 'SUPPORTO' nell'header della pagina di login e check sull'accesso alla pagina web con link '"+prop.getProperty("LINK")+supporto.linkSUPPORTO+"'  - Start");
			//menu.sceltaMenu("Supporto");
		    Thread.sleep(5000);
			supporto.clickComponent(By.xpath("//li//a[text()='Supporto']"));
			
			supporto.checkUrl(prop.getProperty("LINK")+supporto.linkSUPPORTO);
			By headerDescription_Supporto=supporto.headerDescriptionSupporto;
			By homeSupporto_Path=supporto.homeSupportoPath;
			By supporto_Title=supporto.supportoTitle;
			By supporto_Subtitle=supporto.supportoSubtitle;
			
			supporto.checkHeaderPageSupporto(headerDescription_Supporto,homeSupporto_Path,supporto_Title,supporto_Subtitle);
			logger.write("click sul tab 'SUPPORTO' nell'header della pagina di login e check sull'accesso alla pagina web con link '"+prop.getProperty("LINK")+supporto.linkSUPPORTO+"'  - Completed");
              
			logger.write("verifica presenza sezioni 'OPERAZIONI VELOCI'- Start");
			By leftOperazioniVeloci_Label=supporto.leftOperazioniVelociLabel;
			By leftVisualizzaBolletta_Label=supporto.leftVisualizzaBollettaLabel;
			By guardaSubito_Button=supporto.guardaSubitoButton;
			By centralOperazioniVeloci_Label=supporto.centralOperazioniVelociLabel;
			By centralPagaBolletta_Label=supporto.centralPagaBollettaLabel;
			By pagaSubito_Button=supporto.pagaSubitoButton;
			By rightOperazioniVeloci_Label=supporto.rightOperazioniVelociLabel;
			By rightComunica_Label=supporto.rightComunicaLabel;
			By comunica_Button=supporto.comunicaButton;
			
			supporto.checkOperazioniVelociSections(leftOperazioniVeloci_Label, leftVisualizzaBolletta_Label, guardaSubito_Button,"Visualizza la tua bolletta"); 
			supporto.checkOperazioniVelociSections(centralOperazioniVeloci_Label,centralPagaBolletta_Label,pagaSubito_Button,"Paga la tua bolletta");
			supporto.checkOperazioniVelociSections(rightOperazioniVeloci_Label,rightComunica_Label,comunica_Button,"Comunica l'autolettura");
			logger.write("verifica presenza sezioni 'OPERAZIONI VELOCI'- Completed");
			
			logger.write("verifica presenza sezione 'domande piu' frequenti' - Start");
			By domandefrequenti_Label=supporto.domandefrequentiLabel;
			supporto.verifyComponentExistence(domandefrequenti_Label);
			supporto.scrollComponent(domandefrequenti_Label);
			
			By questions_List=supporto.questionsList;
			supporto.checkFrequentQuestions(supporto.domande,questions_List);
			By leggiFaq_Button=supporto.leggiFaqButton;
			supporto.verifyComponentExistence(leggiFaq_Button);
			supporto.scrollComponent(leggiFaq_Button);
			logger.write("verifica presenza sezione 'domande piu' frequenti' - Completed");
			
			logger.write("verifica presenza sezione 'SERVIZI' - Start");
			By servizi_Section=supporto.serviziSectionTitle;
			supporto.verifyComponentExistence(servizi_Section);
			supporto.scrollComponent(servizi_Section);
			
			By servizi_List=supporto.serviziList;
			supporto.scrollComponent(servizi_List);
			supporto.checkServiziList(supporto.servizi, supporto.serviziList);
			logger.write("verifica presenza sezione 'SERVIZI' - Completed");
			
			/*
			logger.write("verifica presenza sezione 'GESTIONE ENEL' - Start");
			By gestioneEnel_Section=supporto.gestioneEnelSection;
			supporto.checkgestioneEnelSection(gestioneEnel_Section);
			By gestioneEnel_image=supporto.gestioneEnelimage;
			supporto.verifyComponentExistence(gestioneEnel_image);
			logger.write("verifica presenza sezione 'GESTIONE ENEL' - Completed");
			*/
			
			logger.write("verifica presenza sezione 'STORIE DI SUCCESSO' - Start");
			By storiedisuccesso_Section=supporto.storiedisuccessoSection;
			
		    supporto.checkstorieDiSuccessoSection(storiedisuccesso_Section, supporto.storie);
			
			logger.write("verifica presenza sezione 'STORIE DI SUCCESSO' - Completed");
			
			logger.write("Click sulla voce 'Contratti e Modulistica' - Start");
			By contrattiAndModulistica_Link=supporto.ContrattiAndModulisticaLink;
			supporto.clickComponent(contrattiAndModulistica_Link);
			
			
			By contrattiAndModulistica_Links=supporto.linksOfContrattiAndModulistica;
			supporto.checklinksOnContrattiAndModulisticaSection(contrattiAndModulistica_Links);
			logger.write("Click sulla voce 'Contratti e Modulistica' - Completed");
			ContattaciComponent contact=new ContattaciComponent(driver);
			logger.write("Click sulla voce 'Contattaci' e apertura pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			By contattaci_Link=contact.contattaciLink;
			contact.clickComponent(contattaci_Link);
			contact.checkUrl(prop.getProperty("LINK")+contact.linkCONTATTACI);
			
			By contattaci_Path=contact.contattaciPath;
			By contattaci_Title=contact.contattaciTitle;
			By contattaciSub_title=contact.contattaciSubtitle;
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("Click sulla voce 'Contattaci' e apertura pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			logger.write("check presenza testo: 'Per le nostre offerte' con descrizione '"+contact.text1+"' - Start");
			By contattacitest1_Title=contact.contattacitest1Title;
			contact.verifyComponentExistence(contattacitest1_Title);
			contact.scrollComponent(contattacitest1_Title);
			
			By text_Section1=contact.textSection;
			contact.checkTextPageContattaci(text_Section1,contact.text1);
			logger.write("check presenza testo: 'Per le nostre offerte' con descrizione '"+contact.text1+"' - Completed");
			
			SpazioEnelComponent spazio= new SpazioEnelComponent(driver);
			logger.write("Click sulla voce 'qui' presente nel testo '"+contact.text1+"' e apertura pagina web con url '"+prop.getProperty("LINK")+spazio.linkSPAZIO_ENEL+"' - Start");
			contact.replaceLinkCollaInXpathEClickObject(prop.getProperty("LINK"));
		
			spazio.checkUrl(prop.getProperty("LINK")+spazio.linkSPAZIO_ENEL);
			By error_Message=spazio.errorMessage;
			spazio.checkError(error_Message);
			By spazioEnel_Title=spazio.spazioEnelTitle;
			By spazioEnel_Path=spazio.spazioEnelPath;
			spazio.checkHeaderPageSpazioEnel(spazioEnel_Path, spazioEnel_Title);
			
			
			logger.write("Click sulla voce 'qui' presente nel testo '"+contact.text1+"' e apertura pagina web con url '"+prop.getProperty("LINK")+spazio.linkSPAZIO_ENEL+"' - Completed");
			
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			driver.navigate().back();
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			logger.write("check presenza testo: 'Per i nostri clienti' con descrizione '"+contact.text2+"' - Start");
			By contattacitest2_Title=contact.contattacitest2Title;
			contact.verifyComponentExistence(contattacitest2_Title);
			contact.scrollComponent(contattacitest2_Title);
			
			By text_Section2=contact.textSection;
			contact.checkTextPageContattaci(text_Section2,contact.text2);
			logger.write("check presenza testo: 'Per i nostri clienti' con descrizione '"+contact.text2+"' - Completed");
				
			logger.write("Click sul link 'Area Clienti' nel testo 'Vuoi parlarci via web? Entra in Area Clienti e contattaci in chat.' e accesso all'area clienti - Start");
			By areaClientiLink=contact.paragraph;
			contact.clickLinkParagraph(areaClientiLink,contact.paragrafotesto);
			
			LoginLogoutEnelCollaComponent login=new LoginLogoutEnelCollaComponent(driver);
			By loginLabel=login.accediAMyEnelLabel;
		    login.verifyComponentExistence(loginLabel);
		    
		    By user = login.username;
		    login.verifyComponentExistence(user);
		    
		    By pw = login.password;
			login.verifyComponentExistence(pw);
			
			By accedi = login.buttonLoginAccedi;
			login.verifyComponentExistence(accedi);
			logger.write("Click sul link 'Area Clienti' nel testo 'Vuoi parlarci via web? Entra in Area Clienti e contattaci in chat.' e accesso all'area clienti - Completed");
			
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			driver.navigate().back();
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			
			UploadDocumentsComponent document=new UploadDocumentsComponent(driver);
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto2+" ' e accesso alla pagina web "+prop.getProperty("LINK")+document.linkCARICA_DOCUMENTI+"' - Start");
			By quiLink=contact.paragraph;
			
			contact.clickLinkParagraphViaWeb(quiLink,contact.paragrafotesto2);
			document.checkUrl(prop.getProperty("LINK")+document.linkCARICA_DOCUMENTI);
			By textPage=document.text;
			
//			document.checkTest(textPage, document.paragrafiditesto);
			
			By inviaMessaggio_Button=document.inviaMessaggioButton;
			document.verifyComponentExistence(inviaMessaggio_Button);
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto2+" ' e accesso alla pagina web "+prop.getProperty("LINK")+document.linkCARICA_DOCUMENTI+"' - Completed");
			
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			driver.navigate().back();
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto3+"' e accesso/chiusura della pagina web di accesso al social facebook - Start");
			String winHandleBefore = driver.getWindowHandle();
			Thread.sleep(3000);
		//	contact.clickLinkParagraph(quiLink,contact.paragrafotesto3);
			contact.clickComponent(By.xpath("//span[text()='qui']"));
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
//			contact.checkUrl(prop.getProperty("LINK_FACEBOOK"));
			contact.checkUrl("https://www.facebook.com/messages/t/enel.energia.10");
			By facebook_Login=contact.facebookLogin;
			contact.verifyComponentExistence(facebook_Login);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto3+"' e accesso/chiusura della pagina web di accesso al social facebook - Completed");
			
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto4+"' e accesso/chiusura della pagina web di accesso al social twitter - Start");
			winHandleBefore = driver.getWindowHandle();
		//	contact.clickLinkParagraph(quiLink,contact.paragrafotesto4);
			contact.clickComponent(By.xpath("(//span//a[text()='qui'])[2]"));
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			contact.checkUrl(prop.getProperty("LINK_TWITTER"));
			//By twitter_Search=contact.twitterSearch;
			//contact.verifyComponentExistence(twitter_Search);
			driver.close();
			driver.switchTo().window(winHandleBefore);			
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto4+"' e accesso/chiusura della pagina web di accesso al social twitter - Completed");
			
			
			logger.write("check presenza testo '"+contact.text3+"' - Start");
			By text_Section3=contact.textSection;
		//	contact.checkTextPageContattaci(text_Section3,contact.text3);
			logger.write("check presenza testo '"+contact.text3+"' - Completed");
			
			logger.write("Click sul link 'Telegram' nel testo '"+contact.paragrafotesto5+"' e accesso/chiusura della pagina web di accesso al social telegram - Start");
			winHandleBefore = driver.getWindowHandle();
			contact.clickComponent(By.xpath("//a[@title='Collegamento a telegram']"));
//			contact.clickLinkParagraph(quiLink,contact.paragrafotesto5);
//			for(String winHandle : driver.getWindowHandles()){
//			    driver.switchTo().window(winHandle);
//			}
			
			contact.checkUrl(prop.getProperty("LINK_TELEGRAM"));
			By telegram_Image=contact.telegramImage;
			contact.verifyComponentExistence(telegram_Image);
			driver.navigate().back();;
	//		driver.switchTo().window(winHandleBefore);
			logger.write("Click sul link 'Telegram' nel testo '"+contact.paragrafotesto5+"' e accesso/chiusura della pagina web di accesso al social telegram - Completed");

			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto6+"' e accesso alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCARICA_DOCUMENTI2+"' - Start");
			contact.clickLinkParagraph(quiLink,contact.paragrafotesto6);
			contact.checkUrl(prop.getProperty("LINK")+contact.linkCARICA_DOCUMENTI2);
			By documenti_Title=contact.documentiTitle;
			contact.verifyComponentExistence(documenti_Title);
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto6+"' e accesso alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCARICA_DOCUMENTI2+"' - Completed");
			
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			driver.navigate().back();
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto7+"' e accesso alla pagina web con url '"+prop.getProperty("LINK")+spazio.linkSPAZIO_ENEL+"' - Start");
			contact.clickLinkParagraph(quiLink,contact.paragrafotesto7);
			spazio.checkUrl(prop.getProperty("LINK")+spazio.linkSPAZIO_ENEL);
			
			spazio.checkHeaderPageSpazioEnel(spazioEnel_Path, spazioEnel_Title);
			
			logger.write("Click sul link 'qui' nel testo '"+contact.paragrafotesto7+"' e accesso alla pagina web con url '"+prop.getProperty("LINK")+spazio.linkSPAZIO_ENEL+"' - Completed");
			
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			driver.navigate().back();
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			logger.write("Click sul link 'Leggi come funziona' nel testo '"+contact.textpedius+" e accesso alla pagina Pedius - Start");
			By leggicomefunziona_Link=contact.leggicomefunzionaLink;
			By text_Section=contact.textSection;
			contact.clickComeFunzionaLink(text_Section, leggicomefunziona_Link,contact.textpedius);
			
			PediusComponent pedius=new PediusComponent(driver);
			pedius.checkUrl(prop.getProperty("LINK_PEDIUS"));
		/*	By pedius_Path=pedius.pediusPath;
			By pedius_Title=pedius.pediusTitle;
			By pedius_Subtitle=pedius.pediusSubtitle;
			pedius.checkHeaderPagePedius(pedius_Path, pedius_Title, pedius_Subtitle);*/
			logger.write("Click sul link 'Leggi come funziona' nel testo '"+contact.textpedius+" e accesso alla pagina Pedius - Completed");
			
			
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Start");
			driver.navigate().back();
			contact.checkHeaderPageContattaci(contattaci_Path, contattaci_Title, contattaciSub_title);
			logger.write("back del browser alla pagina web con url '"+prop.getProperty("LINK")+contact.linkCONTATTACI+"' - Completed");
			
			logger.write("Click sul link '"+contact.paragrafotesto8+"' e apertura della pagina dell'elenco numeri di fax - Start");
			winHandleBefore = driver.getWindowHandle();
			contact.clickLinkParagraph(quiLink,contact.paragrafotesto8);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			By pdf_Page=contact.pdfPage;
			contact.verifyComponentExistence(pdf_Page);
			logger.write("Click sul link '"+contact.paragrafotesto8+"' e apertura della pagina dell'elenco numeri di fax - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
