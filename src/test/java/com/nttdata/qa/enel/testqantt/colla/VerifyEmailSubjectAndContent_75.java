package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyEmailSubjectAndContent_75 {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String link = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            link = gm.recuperaLink(prop, "RECLAMO LUCE 75", false);
		            Thread.sleep(5000);
		            System.out.println(link);
		            ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
		            mod.verifyEmailContent(link, mod.EmailContentLine1);
		            mod.verifyEmailContent(link, mod.EmailContentLine2);
		            mod.verifyEmailContent(link, mod.EmailContentLine3);
		            mod.verifyEmailContent(link, mod.EmailContentLine4);
		            mod.verifyEmailContent(link, mod.EmailContentLine5);
		            mod.verifyEmailContent(link, mod.EmailContentLine6);
		            mod.verifyEmailContent(link, mod.EmailContentLine7);
		            mod.verifyEmailContent(link, prop.getProperty("NOME"));
		            mod.verifyEmailContent(link, prop.getProperty("COGNOME"));
		            mod.verifyEmailContent(link, prop.getProperty("CELLULARE"));
		            mod.verifyEmailContent(link, prop.getProperty("TIME"));
		            mod.verifyEmailContent(link, prop.getProperty("CF"));
		            mod.verifyEmailContent(link, prop.getProperty("EMAIL"));
		            mod.verifyEmailContent(link, prop.getProperty("NUMEROCLIENTE"));
		            mod.verifyEmailContent(link, prop.getProperty("POD"));
		            mod.verifyEmailContent(link, prop.getProperty("ADDRESS"));
		            mod.verifyEmailContent(link, prop.getProperty("DESCRIZIONE"));
		            
		            logger.write("Recupera Link - Completed");
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
          			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
