package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RecuperaCodiceDaLinkCreazioneAccount {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String link = null;			
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
		            GmailQuickStart gm = new GmailQuickStart();
		    		link = gm.recuperaLink(prop, "REG_FROM_MOBILE", false);
		    		Thread.sleep(10000);
		    		link = link.substring(link.indexOf("confirmation="));
		    		String code = link.substring(link.indexOf("=")+1);
		    		if(!APIService.confirmAccount(code))
		    			throw new Exception("Impossibile confermare l'account"); 
		    		Thread.sleep(11000);
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) 
				throw e;

		}finally
		{
			driver.close();
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
