package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SFDCBoxSubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class CreaOffertaSubentroId14 {

    public static void main(String[] args) throws Exception {

        Properties prop = null;
       // QANTTLogger logger = new QANTTLogger(prop);
        int n_sezioni_non_confermate_text;
        int boxStepId;
        String boxErrorMsg;
        String genericErrorText;

        try {

            prop = WebDriverManager.getPropertiesIstance(args[0]);
            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            SFDCBoxSubentroComponent boxSubentro = new SFDCBoxSubentroComponent(driver);

            By sezioni_confermate = boxSubentro.sezioni_confermate;
            boxSubentro.verifyComponentExistence(sezioni_confermate);

            By sezioni_non_confermate = boxSubentro.sezioni_non_confermate;
            boxSubentro.verifyComponentExistence(sezioni_non_confermate);

            n_sezioni_non_confermate_text = (Integer.parseInt(boxSubentro.getElementTextString(sezioni_non_confermate).substring(0, 2)));

            boxStepId = 0;
            boxErrorMsg = null;
            if (prop.getProperty("CONTAINER_RIEPILOGO_OFFERTE").contains("Y")) {

                try {
                    //logger.write("BOX Riepilogo offerta");

                    //STEP 1 CHECK BOX EXISTENCE
                    boxStepId++;
                    boxErrorMsg = "CHECK BOX EXISTENCE";
                    By container_rieoff = boxSubentro.container_rieoff;
                    boxSubentro.verifyComponentExistence(container_rieoff);

                    //STEP 2 CHECK BOX ERROR
                    boxStepId++;
                    boxErrorMsg = "CHECK BOX ERROR";
                    By rieoff_error_banner = boxSubentro.rieoff_error_banner;
                    boxSubentro.verifyComponentInvisibility(rieoff_error_banner);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    throw new Exception("error in box Riepilogo offerta at step N : " + boxStepId + " , Msg : " + boxErrorMsg, e.fillInStackTrace());
                }
            }

            try {
                //logger.write("BOX Riepilogo offerta");
                if (prop.getProperty("CONTAINER_REFERENTE").contains("Y")) {
                    By container_referente = boxSubentro.container_referente;
                    boxSubentro.verifyComponentExistence(container_referente);

                    By popup_cvp_light_ko = gestione.popup_cvp_light_ko;
                    boxSubentro.verifyComponentInexistence(popup_cvp_light_ko);

                    By popup_vbl_ko = gestione.popup_vbl_ko;
                    boxSubentro.verifyComponentInexistence(popup_vbl_ko);

                    By radio_first_in_referente_table = gestione.radio_first_in_referente_table;
                    boxSubentro.verifyComponentExistence(radio_first_in_referente_table);
                    boxSubentro.clickComponent(radio_first_in_referente_table);

                    By container_referente_button_conferma = boxSubentro.container_referente_button_conferma;
                    boxSubentro.verifyComponentExistence(container_referente_button_conferma);
                    boxSubentro.clickComponent(container_referente_button_conferma);
                    n_sezioni_non_confermate_text = n_sezioni_non_confermate_text - 1;
                }

            } catch (Exception e1) {
                // TODO Auto-generated catch block
                throw new Exception("error in box REFERENTE", e1.fillInStackTrace());
            }


            try {

                //logger.write("BOX Seleziona Uso Forniture");

                By container_suf = boxSubentro.container_suf;
                boxSubentro.verifyComponentExistence(container_suf);

                By suf_pickuplist_uf = boxSubentro.suf_pickuplist_uf;
                boxSubentro.verifyComponentExistence(suf_pickuplist_uf);

                boxSubentro.clickAncestorClickable(boxSubentro.xpathToString(suf_pickuplist_uf), 7);

                By suf_pickuplist_uf_uda = boxSubentro.suf_pickuplist_uf_uda;
                boxSubentro.verifyComponentExistence(suf_pickuplist_uf_uda);

                boxSubentro.clickComponent(suf_pickuplist_uf_uda);

                By suf_button_conferma = boxSubentro.suf_button_conferma;
                boxSubentro.verifyComponentExistence(suf_button_conferma);

                boxSubentro.clickComponent(suf_button_conferma);

                By popup_prodotto_non_valido = gestione.popup_prodotto_non_valido;
                boxSubentro.verifyComponentInexistence(popup_prodotto_non_valido);

                n_sezioni_non_confermate_text = n_sezioni_non_confermate_text - 1;

            } catch (Exception e1) {
                // TODO Auto-generated catch block
                throw new Exception("error in box 'Seleziona Uso Forniture'", e1.fillInStackTrace());
            }


            try {

                //logger.write("BOX Commodity");

                By container_comm = boxSubentro.container_comm;
                boxSubentro.verifyComponentExistence(container_comm);

                By comm_radio_elettrico = boxSubentro.comm_radio_elettrico;
                boxSubentro.verifyComponentExistence(comm_radio_elettrico);
                boxSubentro.clickComponentIfExist(comm_radio_elettrico);

                By comm_pickuplist_tit = boxSubentro.comm_pickuplist_tit;
                boxSubentro.verifyComponentExistence(comm_pickuplist_tit);

                if (!boxSubentro.checkDisabled(comm_pickuplist_tit))
                    boxSubentro.clickComponentIfExist(comm_pickuplist_tit);

                By comm_pickuplist_cms = boxSubentro.comm_pickuplist_cms;
                boxSubentro.verifyComponentExistence(comm_pickuplist_cms);
                boxSubentro.clickComponentIfExist(comm_pickuplist_cms);

                By comm_pickuplist_cms_al_se_pickup = boxSubentro.comm_pickuplist_cms_al_se;

                boxSubentro.clickComponentIfExist(comm_pickuplist_cms_al_se_pickup);

                By comm_pickuplist_residente = boxSubentro.comm_pickuplist_residente;
                boxSubentro.verifyComponentExistence(comm_pickuplist_residente);

                boxSubentro.verifyAttributeValue(comm_pickuplist_residente, "value",
                        "NO");

                By comm_pickuplist_ofi = boxSubentro.comm_pickuplist_ofi;

                if (boxSubentro.clickComponentIfNotDisabled(comm_pickuplist_ofi)) {
                    By comm_pickuplist_ofi_no = boxSubentro.comm_pickuplist_ofi_no;
                    boxSubentro.verifyComponentExistence(comm_pickuplist_ofi_no);
                    boxSubentro.clickComponentIfExist(comm_pickuplist_ofi_no);
                }

                By comm_input_tdi_dist = boxSubentro.comm_input_tdi_dist;
                boxSubentro.verifyComponentExistence(comm_input_tdi_dist);
                boxSubentro.clickComponentIfExist(comm_input_tdi_dist);

                boxSubentro.clearAndSendKeys(comm_input_tdi_dist, prop.getProperty("TELEFONO_DISTRIBUTORE"));

                By comm_pickuplist_asc = boxSubentro.comm_pickuplist_asc;
                boxSubentro.verifyComponentExistence(comm_pickuplist_asc);
                boxSubentro.clickComponentIfExist(comm_pickuplist_asc);

                By comm_asc_pickuplist_no = boxSubentro.comm_asc_pickuplist_no;
                boxSubentro.verifyComponentExistence(comm_asc_pickuplist_no);
                boxSubentro.clickComponentIfExist(comm_asc_pickuplist_no);

                By comm_pickuplist_dis = boxSubentro.comm_pickuplist_dis;
                boxSubentro.verifyComponentExistence(comm_pickuplist_dis);
                boxSubentro.clickComponent(comm_pickuplist_dis);

                By comm_dis_pickuplist_si = boxSubentro.comm_dis_pickuplist_si;
                boxSubentro.verifyComponentExistence(comm_dis_pickuplist_si);
                boxSubentro.clickComponent(comm_dis_pickuplist_si);

                By comm_input_consumo_annuo = boxSubentro.comm_input_consumo_annuo;
                boxSubentro.verifyComponentExistence(comm_input_consumo_annuo);
                boxSubentro.clickComponent(comm_input_consumo_annuo);

                boxSubentro.clearAndSendKeys(comm_input_consumo_annuo, prop.getProperty("CONSUMO_ANNUO"));

                By comm_button_conferma_fornitura = boxSubentro.comm_button_conferma_fornitura;
                boxSubentro.verifyComponentExistence(comm_button_conferma_fornitura);
                boxSubentro.clickComponent(comm_button_conferma_fornitura);

                By comm_cell_confermata_si = boxSubentro.comm_cell_confermata_si;
                boxSubentro.verifyComponentExistence(comm_cell_confermata_si);

                By comm_button_conferma = boxSubentro.comm_button_conferma;
                boxSubentro.verifyComponentExistence(comm_button_conferma);
                boxSubentro.clickComponent(comm_button_conferma);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                throw new Exception("error in box 'Seleziona Uso Forniture'", e1.fillInStackTrace());
            }


            //BOX Indirizzo di Residenza/Sede Legale
            System.out.println("BOX Indirizzo di Residenza/Sede Legale");
            By container_irsl = boxSubentro.container_irsl;
            boxSubentro.verifyComponentExistence(container_irsl);
            By irsl_indirizzo_verificato = boxSubentro.irsl_indirizzo_verificato;

            try {
                boxSubentro.verifyComponentExistence(irsl_indirizzo_verificato);
            } catch (Exception e) {
                throw new Exception("Error : banner \"Indirizzo verificato\"  not present in container \"Indirizzo di Residenza/Sede Legale\" .");
            }

            By irsl_button_conferma = boxSubentro.irsl_button_conferma;
            boxSubentro.verifyComponentExistence(irsl_button_conferma);
            boxSubentro.clickComponent(irsl_button_conferma);


            //BOX INDIRIZZO DI FATTURAZIONE
            System.out.println("BOX INDIRIZZO DI FATTURAZIONE");
            System.out.println("Box worck in progress");

            By container_idf = boxSubentro.container_idf;
            boxSubentro.verifyComponentExistence(container_idf);
            By idf_banner_indirizzo = boxSubentro.idf_banner_indirizzo;
            By idf_button_conferma = boxSubentro.idf_button_conferma;

            if (boxSubentro.getElementExistance(idf_banner_indirizzo)) {
                boxSubentro.verifyComponentExistence(idf_button_conferma);
                boxSubentro.clickComponent(idf_button_conferma);
            }

            By idf_tab_verifica = boxSubentro.idf_tab_verifica;
            boxSubentro.verifyComponentExistence(idf_tab_verifica);
            boxSubentro.clickComponent(idf_tab_verifica);
            By idf_first_checkbox_showed = boxSubentro.idf_first_checkbox_showed;
            boxSubentro.verifyComponentExistence(idf_first_checkbox_showed);
            boxSubentro.clickComponent(idf_first_checkbox_showed);
            By idf_button_importa = boxSubentro.idf_button_importa;
            boxSubentro.verifyComponentExistence(idf_button_importa);
            boxSubentro.clickComponent(idf_button_importa);
            By idf_input_regione = boxSubentro.idf_input_regione;
            boxSubentro.verifyComponentExistence(idf_input_regione);
            By idf_input_regione_disabled = boxSubentro.idf_input_regione_disabled;
            boxSubentro.verifyComponentExistence(idf_input_regione_disabled);
            By idf_input_provincia = boxSubentro.idf_input_provincia;
            boxSubentro.verifyComponentExistence(idf_input_provincia);
            By idf_input_comune = boxSubentro.idf_input_comune;
            boxSubentro.verifyComponentExistence(idf_input_comune);
            By idf_input_localita = boxSubentro.idf_input_localita;
            boxSubentro.verifyComponentExistence(idf_input_localita);
            By idf_input_indirizzo = boxSubentro.idf_input_indirizzo;
            boxSubentro.verifyComponentExistence(idf_input_indirizzo);
            By idf_input_ncivico = boxSubentro.idf_input_ncivico;
            boxSubentro.verifyComponentExistence(idf_input_ncivico);
            By idf_input_cap = boxSubentro.idf_input_cap;
            boxSubentro.verifyComponentExistence(idf_input_cap);
            By idf_input_cap_disabled = boxSubentro.idf_input_cap_disabled;
            boxSubentro.verifyComponentExistence(idf_input_cap_disabled);
            By idf_button_verifica = boxSubentro.idf_button_verifica;
            boxSubentro.verifyComponentExistence(idf_button_verifica);
            boxSubentro.clickComponent(idf_button_verifica);

            try {
                By idf_indirizzo_non_verificato_generic = boxSubentro.idf_indirizzo_non_verificato_generic;
                boxSubentro.verifyComponentInvisibility(idf_indirizzo_non_verificato_generic);
            } catch (Exception e) {
                genericErrorText = boxSubentro.getElementTextString(boxSubentro.idf_indirizzo_non_verificato_generic);
                throw new Exception("Error : '" + genericErrorText + "'", e.fillInStackTrace());
            }

            By idf_indirizzo_verificato = boxSubentro.idf_indirizzo_verificato;
            boxSubentro.verifyComponentExistence(idf_indirizzo_verificato);

            boxSubentro.verifyComponentExistence(idf_button_conferma);
            boxSubentro.clickComponent(idf_button_conferma);

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            //logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());

            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
