// Package HerokuAPIRegistration_1

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;

import com.nttdata.qa.enel.components.colla.api.HerokuAPIRegistrationComponent_Err2;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class HerokuAPIRegistration_3 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Authenticate Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			prop.setProperty("JSON_INPUT", "{     \"email\": \"" + prop.getProperty("EMAIL") + "\",     \"phoneNumber\": \"" + prop.getProperty("PHONENUMBER") + "\",     \"password\": \"Password01\",     \"firstName\": \"" + prop.getProperty("FIRSTNAME") + "\",     \"lastName\": \"" + prop.getProperty("LASTNAME") + "\",     \"countryCode\": \"IT\",     \"companyName\":\"\",     \"vatCode\":\"\",      \"attributes\": [         {             \"country\": \"IT\",             \"claims\": [                 {                     \"name\": \"personalId\",                     \"value\": \"" + prop.getProperty("CODICEFISCALE") + "\"                 }             ]         }     ],     \"consents\": [         {             \"id\": \"11\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         },         {             \"id\": \"10\",             \"group\": \"12\",             \"language\": \"it_IT\",             \"channel\": \"ENEL_ENERGIA\",             \"given\": true         }     ],     \"metadata\": {         \"language\": \"it_IT\",         \"processChannel\": \"ENEL_ENERGIA\"     } }");
			//Da configurare come OutPut
			prop.setProperty("Authorization", "Bearer ");
			//prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
    		prop.setProperty("TID", UUID.randomUUID().toString());
    		prop.setProperty("SID", UUID.randomUUID().toString()); 
    		//prop.setProperty("SOURCECHANNEL","WEB");
    		prop.setProperty("SOURCE_CHANNEL","EE");
    		prop.setProperty("TOUCHPOINT","WEB");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIRegistrationComponent_Err2 HRKComponent = new HerokuAPIRegistrationComponent_Err2();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


