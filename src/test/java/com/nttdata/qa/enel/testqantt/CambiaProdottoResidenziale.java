package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CambiaProdottoResidenziale {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		//COMMODITY;PRODOTTO
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			
			if(prop.getProperty("COMMODITY").contentEquals("GAS")) {
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Start");
			driver.switchTo().defaultContent();
			String frame = configura.switchToFrame();
			configura.clickWithJS(configura.cambiaProdotto);
		    //configura.press(configura.cambiaProdotto);
		    driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(30);
			//configura.switchToFrame2();
			configura.switchToFrame();
		//modifica codice data 15/10/2020 per gestione cliente gas business
			
			configura.pressResidenziale(configura.linkGasResidenziale);
			
		
			configura.modificaProdotto(prop.getProperty("PRODOTTO"));	
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(30);
			configura.avantiProdotto();
			driver.switchTo().defaultContent();
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Completed");
			
			}
			if(prop.getProperty("COMMODITY").contentEquals("ELE")) {
				logger.write("Configura prodotto ele "+prop.getProperty("PRODOTTO")+"- Start");
				driver.switchTo().defaultContent();
				String frame = configura.switchToFrame();
			    configura.press(configura.cambiaProdotto);
			    driver.switchTo().defaultContent();
				TimeUnit.SECONDS.sleep(30);
				configura.switchToFrame();
				configura.press(configura.linkElettricoResidenziale);
				configura.modificaProdotto(prop.getProperty("PRODOTTO"));
				driver.switchTo().defaultContent();
				TimeUnit.SECONDS.sleep(60);
				configura.avantiProdotto();
				driver.switchTo().defaultContent();
				logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Completed");
				
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
