package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class InserimentoFornitureEVO {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			PrecheckComponent forniture = new PrecheckComponent(driver);
			forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD"));
			forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
			forniture.pressAndCheckSpinners(forniture.precheckButton2);
			
			//Compare in caso di CAP comune in più zone, seleziona il primo. Gestito solo nel caso in cui è presente. 
			forniture.selezionaIstat(forniture.popUpIstat, forniture.primoIstat, forniture.confermaIstat);
			logger.write("Inserimento e Validazione Indirizzo Esecuzione Lavori - Completed");
			Thread.currentThread().sleep(5000);
			
			if(prop.getProperty("VERIFICA_ESITO_OFFERTABILITA").contentEquals("Y")) {
			logger.write("Verifica esito Offertabilita - Start");
			forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
			logger.write("Verifica esito Offertabilita - Completed");
			}
			//Inserisce il campo "Modifica Impianto" se indicato nel property
			if (!prop.getProperty("MODIFICA_IMPIANTO","").equals("")){
				Thread.currentThread().sleep(5000);
				forniture.selezionaLigtheningValue("Modifica impianto", prop.getProperty("MODIFICA_IMPIANTO"));
				logger.write("Selezione Modifica Impianto");
			}
			
//			if(prop.getProperty("VERIFICA_ESITO_PRECHECK","N").contentEquals("Y")) {
//				logger.write("Verifica descrizione esito Precheck - Start");
//				String descEsito = forniture.recuperaDescrizionePrecheck();
//				if(!prop.getProperty("DESCRIZIONE_ESITO_PRECHECK").toLowerCase().contentEquals(descEsito.toLowerCase())) {
//					
//					throw new Exception("La descrizione esito precheck attesa era : '"+prop.getProperty("DESCRIZIONE_ESITO_PRECHECK")+"'. Quella riscontrata : '"+descEsito+"'");
//					
//				}
//				logger.write("Verifica descrizione esito Precheck - Completed");
//				
//			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
