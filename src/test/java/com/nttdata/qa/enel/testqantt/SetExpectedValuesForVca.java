package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SetExpectedValuesForVca {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {


				//STATI RICHIESTA
				prop.setProperty("STATO_CASE", "CHIUSO");
				prop.setProperty("SOTTOSTATO_CASE", "RICEVUTO");
				prop.setProperty("STATO_ORDINE", "ESPLETATO");
				prop.setProperty("STATO_GLOBALE_RICHIESTA", "CHIUSO");
				prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ESPLETATO");
				prop.setProperty("STATO_RICHIESTA", "ESPLETATO");
				prop.setProperty("STATO_R2D", "OK");
				prop.setProperty("STATO_SAP", "OK");
				prop.setProperty("STATO_SEMPRE", "OK");


				//NECESSARI PER VERIFICHE SU ORDER ITEM CESSAZIONE
				prop.setProperty("STATO_CESS","ESPLETATO");
				prop.setProperty("STATO_R2D_CESS","NON PREVISTO");
				prop.setProperty("STATO_SAP_CESS","OK");
				prop.setProperty("STATO_SEMPRE_CESS","OK");
				prop.setProperty("STATO_UDB_CESS","NON PREVISTO");

				//NECESSARI PER VERIFICHE SU ORDER ITEM ATTIVAZIONE
				prop.setProperty("STATO_ATT","ESPLETATO");
				prop.setProperty("STATO_R2D_ATT","OK");
				prop.setProperty("STATO_SAP_ATT","OK");
				prop.setProperty("STATO_SEMPRE_ATT","OK");
				if(prop.getProperty("STATO_UDB_ATT","").equals(null))
						prop.setProperty("STATO_UDB_ATT","NON PREVISTO");



			}


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}