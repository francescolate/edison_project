package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaSezionePreventiviCopiaDocumenti {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				DettagliSpedizioneCopiaDocComponent forn=new DettagliSpedizioneCopiaDocComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver); 
				SezioneSelezioneFornituraComponent aut=new SezioneSelezioneFornituraComponent(driver);
							
				logger.write("check esistenza campo 'Ricerca preventivi' - Start'");				              
                By campoRicercaPreventivi=forn.buttonRicercaPreventivi;
                forn.verifyComponentExistence(campoRicercaPreventivi);              
                TimeUnit.SECONDS.sleep(1);                
                logger.write("check esistenza campo 'Ricerca preventivi' - Completed");			   
				
				logger.write("inserire nel campo 'Ricerca preventivi' il Numero Pdr - Start");
				TimeUnit.SECONDS.sleep(1);
				aut.enterPodValue(campoRicercaPreventivi, prop.getProperty("NUMERO_PDR"));
				TimeUnit.SECONDS.sleep(3);
				logger.write("inserire nel campo 'Ricerca preventivi' il Numero Pdr - Completed");
				
				logger.write("verifica che nella tabella della sezione preventivi sono riportate le seguenti informazioni: Id protocollo, Numero Attività, causale trasporrti, Importo, Data Richiesta e selezionare l'Id documento di interesse - Start");
				aut.verificaColonneTabellaSezionePianoDiRientro(forn.colonneSezionePreventivi);
				TimeUnit.SECONDS.sleep(5);
				forn.clickWithJS(forn.radioButtonSezionePreventivi);
				logger.write("verifica che nella tabella della sezione preventivi sono riportate le seguenti informazioni: Id protocollo, Numero Attività, causale trasporrti, Importo, Data Richiesta e selezionare l'Id documento di interesse - Completed");
				
				TimeUnit.SECONDS.sleep(5);
										
				logger.write("verifica che il pulsante 'Conferma preventivi' sia abilitato e click su questo - Start");
				forn.verifyComponentExistence(forn.buttonConfermaPreventivi);
				forn.clickComponent(forn.buttonConfermaPreventivi);
				logger.write("verifica che il pulsante 'Conferma preventivi' sia abilitato e click su questo - Completed");	
					
				gestione.checkSpinnersSFDC();
					
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
