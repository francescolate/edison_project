package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_Forzatura_Esiti_3OK_GAS {

	@Step("R2D Forzature esiti 3OK PER GAS")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//				String statopratica1=prop.getProperty("STATO_PRATICA_R2D").substring(0, 2);
				String statopratica=prop.getProperty("STATO_PRATICA").substring(0, 2);
				//Cambio stato in CI solamente se Stato Pratica è AA o AW
				if ((statopratica.compareToIgnoreCase("AA")==0 || (statopratica.compareToIgnoreCase("AW"))==0)) {
					R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
					//Forzatura puntuale pratica in stato CI
					logger.write("Menu Forzatura Puntuale - Start");
					 menuBox.selezionaVoceMenuBoxF("Forzature","Forzatura");
					logger.write("Menu Forzatura Puntuale - Completed");
					TimeUnit.SECONDS.sleep(3);
					R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
					//Selezione tipologia Caricamento
					logger.write("Selezione tipologia Caricamento - Start");
					 caricamentoEsiti.selezionaPuntualePS();
					logger.write("Selezione tipologia Caricamento - Completed");
					//Click bottone Cerca
					//Forzara Puntuale e click bottone Cerca
					logger.write("Selezione Forzatura Puntuale e Bottone Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione Forzatura Puntuale e Bottone Cerca - Completed");
					TimeUnit.SECONDS.sleep(3);
					//Inserimento PDR oppure OI_RICERCA
						logger.write("inserisci OI_RICERCA - Start");
						if (!prop.getProperty("OI_RICERCA","").equals("")) {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.idCRM, prop.getProperty("OI_RICERCA"));
						} else {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.idCRM, prop.getProperty("ID_ORDINE"));
						}
						logger.write("inserisci OI_RICERCA - Completed");
					TimeUnit.SECONDS.sleep(3);
					//Cerca
					logger.write("Selezione pulsante Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione pulsante Cerca - Completed");
					TimeUnit.SECONDS.sleep(3);
					//Seleziona Flag Cambia Stato
					logger.write("Caricamento valore Check Cambia Stato - Start");
					caricamentoEsiti.checkCambiaStato(caricamentoEsiti.inputForzaStato);
					logger.write("Caricamento valore Check Cambia Stato - Completed");
					TimeUnit.SECONDS.sleep(3);
					//Click Seleziona Cambia Stato in CI
					logger.write("Caricamento valore Stato Pratica - Start");
					caricamentoEsiti.selezionaStatoPratica(caricamentoEsiti.inputStato, "CI");
					logger.write("Caricamento valore Stato Pratica - Completed");
					TimeUnit.SECONDS.sleep(3);
					//Click Conferma Forzatura
					logger.write("Selezione pulsante Conferma - Start");
					caricamentoEsiti.confermaForzatura(caricamentoEsiti.buttonConfermaForzaturaGas);
					logger.write("Selezione pulsante Conferma - Completed");
					TimeUnit.SECONDS.sleep(3);
					//Salvataggio stato attuale pratica
					logger.write("Aggiornamento Stato Pratica su prop - Start");
					prop.setProperty("STATO_PRATICA", "CI");
					logger.write("Aggiornamento Stato Pratica su prop - Completed");
				}
			}
			//
//			System.out.println("Forzatura 3OK STATO_R2D: "+ prop.getProperty("STATO_R2D"));
//			System.out.println("Forzatura 3OK STATO_PRATICA_R2D: "+ prop.getProperty("STATO_PRATICA_R2D"));
			
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");		
		}
	}
}
