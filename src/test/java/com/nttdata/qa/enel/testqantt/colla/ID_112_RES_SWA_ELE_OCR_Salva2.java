package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.OffertaComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_112_RES_SWA_ELE_OCR_Salva2 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			OffertaComponent oc = new OffertaComponent(driver);

			// Step 2 verification

			logger.write("Verify page navigation to Offerta page and page details -- Strat");
			driver.navigate().to(oc.offertaUrl);
			//oc.verifyComponentExistence(oc.aderisci);
			oc.checkURLAfterRedirection(oc.offertaUrl);
			// oc.verifyComponentExistence(oc.pageTitle);
			oc.comprareText(oc.pageText, oc.PageText, true);
			oc.verifyComponentExistence(oc.inserisciITuoiDati);
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.verifyComponentExistence(oc.pagamentiEBollette);
			oc.verifyComponentExistence(oc.consensi);
			oc.verifyComponentExistence(oc.caricaBolletta);
			oc.verifyComponentExistence(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");

			// Step 3

			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Strat");
			oc.clickComponent(oc.compilaManualmente);
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));

			// oc.enterInputParameters(oc.cellulareprefix,
			
			/*
			 * Robot robot = new Robot(); oc.clickComponent(oc.cellulareprefix);
			 * robot.keyPress(KeyEvent.VK_DOWN); robot.delay(50);
			 * robot.keyRelease(KeyEvent.VK_DOWN); robot.delay(50);
			 * robot.keyPress(KeyEvent.VK_ENTER); robot.delay(50);
			 * robot.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(500);
			 */

			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(30000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");

			// Step 3 - Verification

			logger.write("Verify and Enter the values in informazioniFornitura section-- Strat");
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);

			// Step 4

			oc.enterInputParameters(oc.pod, "IT004E" + PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			//oc.enterInputParameters(oc.podNew, "IT004E" + PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
			//oc.enterInputParameters(oc.capNew, prop.getProperty("CAP"));
			oc.clickComponent(oc.capdropdown);
			oc.comprareText(oc.privacyText, oc.PrivacyText, true);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.verifyComponentExistence(oc.inseriscilodopo);
			oc.clickComponent(oc.prosegui1);

			// Step 4 Verification

			oc.comprareText(oc.indrizzoDiFornituraText, oc.IndrizzoDiFornituraText, true);
			oc.verifyComponentExistence(oc.citta);
			oc.verifyComponentExistence(oc.indrizzo);
			oc.verifyComponentExistence(oc.numeroCivico);
			oc.verifyComponentExistence(oc.indrizzoDiFornituraCAP);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText, oc.IndrizzoDiFornituraPrivacyText, true);
			oc.verifyComponentExistence(oc.attuleFornitore);
			oc.verifyComponentExistence(oc.recapiti);
			oc.comprareText(oc.recapitiqst1, oc.Recapitiqst1, true);
			oc.verifyComponentExistence(oc.recapitians1);
			oc.verifyComponentExistence(oc.recapitians2);
			oc.comprareText(oc.recapitiqst2, oc.Recapitiqst2, true);
			oc.comprareText(oc.recapitiansOne, oc.RecapitiansOne, true);
			oc.comprareText(oc.recapitiansTwo, oc.RecapitiansTwo, true);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText2, oc.IndrizzoDiFornituraPrivacyText2, true);

			// Step 5

			oc.enterInputParameters(oc.citta, prop.getProperty("CITTA"));
			oc.enterInputParameters(oc.indrizzo, prop.getProperty("INDRIZZO"));
			oc.enterInputParameters(oc.numeroCivico, prop.getProperty("NUMEROCIVICO"));
			oc.clickComponent(oc.indrizzo);
			Thread.sleep(3000);
			oc.verifyComponentExistence(oc.insertManually);
			oc.clickComponent(oc.insertManually);
			Thread.sleep(3000);
			oc.enterInputParameters(oc.indrizzo, prop.getProperty("INDRIZZO"));
			oc.enterInputParameters(oc.indrizzoDiFornituraCAP, prop.getProperty("CAP"));
			oc.clickComponent(oc.indrizzoDiFornituraCapDropdown);
			oc.enterInputParameters(oc.attuleFornitore, prop.getProperty("ATTUALE_FORNITORE"));
			Thread.sleep(2000);
			oc.clickComponent(oc.Attuale_Fornitore_InputSelect);
			oc.clickComponent(oc.recapitians1);
			oc.clickComponent(oc.recapitiansOne);
			Thread.sleep(5000);
			oc.clickComponent(oc.salvaEContinuaDopo);
			logger.write("Verify and Enter the values in informazioniFornitura section-- Completed");

			// Step 6

			oc.clickComponent(oc.salvaOraContinuaDopoPopupRadioBtn1);
			oc.clickComponent(oc.salvaOraContinuaDopoPopupSalvaedesci);

			// Step 7 Email Verification

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
