package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ArticlePageComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SOSLuceEGas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			
			ArticlePageComponent article  = new ArticlePageComponent(driver);
			
			//Start set Dati da verificare
			String separator = "<;>";
			prop.setProperty("SOS_PATH_MENU_TEXT" , "HOME/ INTERRUZIONE ENERGIA ELETTRICA O GAS");
			prop.setProperty("SOS_TITLE_MENU_TEXT" , "Manca la corrente o il gas?");
			prop.setProperty("SOS_DETAIL_MENU_TEXT" , "Seleziona la tipologia di fornitura dove riscontri il problema");
			
			String [] sos_article_texts = {
					  "Questo servizio, aperto a tutti, ti fornisce il numero di pronto intervento del distributore"
					, "di energia elettrica o gas per segnalare un guasto o ricevere informazioni."
					, "Inoltre, nel caso di mancanza di corrente, riceverai subito informazioni su eventuali guasti diffusi o interruzioni programmate che interessano la fornitura e verrai guidato da un supporto interattivo nella verifica di potenziali problemi sull'impianto."};
			
			prop.setProperty("SOS_ARTICLE_TEXTS", convertArrayToStringWhitSeparator(sos_article_texts, separator));
			
			String [] luce_article_texts = {
					  "Per accedere al servizio ti basterà inserire l’indirizzo della fornitura in cui manca la corrente per ricevere il numero di pronto intervento del distributore. Se sei cliente Enel Energia ed il tuo distributore è e-Distribuzione, inserendo indirizzo e codice fiscale, riceverai subito informazioni su eventuali guasti diffusi o interruzioni programmate che interessano la tua fornitura."
					, "Indirizzo"
					, "Accedendo con l’indirizzo potrai verificare in maniera interattiva lo stato del tuo impianto e visualizzare il numero di telefono del tuo distributore di energia elettrica"
					, "Inserisci l'indirizzo"};
			
			prop.setProperty("LUCE_ARTICLE_TEXTS", convertArrayToStringWhitSeparator(luce_article_texts, separator));
			
			String [] gas_article_texts = {
					  "Questo servizio ti fornisce il numero di pronto intervento del distributore gas per ricevere informazioni o segnalare un guasto."
					, "Indirizzo"
					, "Potrai ricevere il numero di pronto intervento inserendo semplicemente l'indirizzo della fornitura interessata dal disservizio"
					, "Inserisci l'indirizzo"};
			
			prop.setProperty("GAS_ARTICLE_TEXTS", convertArrayToStringWhitSeparator(gas_article_texts, separator));
			
			prop.setProperty("LEGAL_NOTE_TEXT", "Le informazioni in merito ad eventuali guasti o interruzioni programmate, che troverete all’interno del presente sito, sono rese pubbliche dal distributore di zona competente.");
			
			
			
			String [] sos_questions_text = {"Perché manca la corrente?"
					, "Interruzioni della fornitura di energia elettrica: da cosa possono dipendere?"
					, "Come segnalare un guasto?"
					, "Sono previsti indennizzi per la mancanza di corrente?"
					, "A chi segnalare il guasto del contatore gas?"
					, "Cosa fare in caso di contatore del gas bloccato?"
					, "Interruzione della fornitura di gas: cosa fare?"
					};

			String [] luce_questions_text = {"Perché manca la corrente?"
					, "Interruzioni della fornitura di energia elettrica: da cosa possono dipendere?"
					, "Come segnalare un guasto?"
					, "Sono previsti indennizzi per la mancanza di corrente?"};
			
			String [] gas_questions_text = {"A chi segnalare il guasto del contatore gas?"
					, "Cosa fare in caso di contatore del gas bloccato?"
					, "Interruzione della fornitura di gas: cosa fare?"};

			prop.setProperty("SOS_QUESTIONS_TEXT" , convertArrayToStringWhitSeparator(sos_questions_text, separator));
			prop.setProperty("LUCE_QUESTIONS_TEXT" , convertArrayToStringWhitSeparator(luce_questions_text, separator));
			prop.setProperty("GAS_QUESTIONS_TEXT" , convertArrayToStringWhitSeparator(gas_questions_text, separator));
			
			String [] sos_answers_text = {
					"Esiste un problema sulla tua fornitura di energia elettrica, che può essere dovuto a:"
							+ "troppi elettrodomestici usati contemporaneamente"
							+ "si è verificato un guasto sul tuo impianto"
							+ "distacco a seguito di mancato pagamento"
							+ "Lavori programmati dal distributore nella tua zona che riguardano la fornitura"
							+ "Guasto diffuso sulla rete elettrica della tua zona che riguarda la fornitura"
					, "Guasti sulla rete elettrica di trasmissione nazionale"
							+ "Guasti sulla rete elettrica di distribuzione"
					, "Verifica con l’aiuto di questo servizio se l’impianto è interessato da un guasto."
							+ "Se al termine della procedura risulterà un guasto di competenza del distributore , ti forniremo il suo numero per fare la segnalazione." 
					, "L’Autorità di Regolazione per Energia Reti e Ambiente prevede indennizzi per determinate casistiche, verificale sul sito www.arera.it"
					, "Questo servizio ti fornisce il numero verde per la segnalazione guasti gas. Il distributore effettuerà tutte le verifiche necessarie sul contatore per ripristinare il servizio."
					, "Questo servizio ti fornisce il numero verde per contattare il distributore gas e attivare tutte le verifiche per la risoluzione del problema."
					, "Questo servizio ti fornisce il numero verde per contattare il pronto intervento gas in caso di sospensione della fornitura per guasto."
					};
			
			String [] luce_answers_text = {
					"Esiste un problema sulla tua fornitura di energia elettrica, che può essere dovuto a:"
							+ "troppi elettrodomestici usati contemporaneamente"
							+ "si è verificato un guasto sul tuo impianto"
							+ "distacco a seguito di mancato pagamento"
							+ "Lavori programmati dal distributore nella tua zona che riguardano la fornitura"
							+ "Guasto diffuso sulla rete elettrica della tua zona che riguarda la fornitura"
					, "Guasti sulla rete elettrica di trasmissione nazionale"
							+ "Guasti sulla rete elettrica di distribuzione"
					, "Verifica con l’aiuto di questo servizio se l’impianto è interessato da un guasto."
							+ "Se al termine della procedura risulterà un guasto di competenza del distributore , ti forniremo il suo numero per fare la segnalazione"
					, "L’Autorità di Regolazione per Energia Reti e Ambiente prevede indennizzi per determinate casistiche, verificale sul sito www.arera.it"};
			
			String [] gas_answers_text = {
					"Questo servizio ti fornisce il numero verde per la segnalazione guasti gas. Il distributore effettuerà tutte le verifiche necessarie sul contatore per ripristinare il servizio."
					, "Questo servizio ti fornisce il numero verde per contattare il distributore gas e attivare tutte le verifiche per la risoluzione del problema."
					, "Questo servizio ti fornisce il numero verde per contattare il pronto intervento gas in caso di sospensione della fornitura per guasto."};
			
			prop.setProperty("SOS_ANSWERS_TEXT" , convertArrayToStringWhitSeparator(sos_answers_text, separator));
			prop.setProperty("LUCE_ANSWERS_TEXT" , convertArrayToStringWhitSeparator(luce_answers_text, separator));
			prop.setProperty("GAS_ANSWERS_TEXT" , convertArrayToStringWhitSeparator(gas_answers_text, separator));

			// END Set Dati 
			//TO DO CREARE UNA CLASSE PER LE COSTANTI
			
			String[][] links = {{"SOS luce e gas" , "https://www-coll1.enel.it/it/interruzione-energia-elettrica-gas"} , 
					{"Mix Combustibili" , "https://www-coll1.enel.it/it/supporto/faq/mercato-libero-luce"}  , 
					{"Piano salva Black out (PESSE)" , "https://www-coll1.enel.it/it/supporto/faq/salvablackout"}   , 
					{"Servizio di salvaguardia" , "https://www-coll1.enel.it/it/supporto/faq/servizio-salvaguardia"}    , 
					{"Servizio default di distribuzione" , "https://www-coll1.enel.it/it/supporto/faq/servizio-di-default-distribuzione"}    , 
					{"Conciliazioni e risoluzione delle controversie" , "/it/supporto/faq/conciliazione-adr"} , 
					{"Informazioni utili" , "/it/informazioni-utili/informazioni-utili-index"} , 
					{"Nuove regole europee per la protezione dei dati" , "/it/informazioni-utili/informazioni-utili/in-arrivo-nuove-regole-europee-per-la-protezione-dei-dati"} , 
					{"Contattaci" , "https://www-coll1.enel.it/it/contattaci"} , 
					{"Diventa nostro partner" , "https://www-coll1.enel.it/it/imprese/diventa-nostro-partner"} , 
					{"Accedi alle sponsorizzazioni" , "https://sponsorship.enel.it"} , 
					{"Guida alla bolletta luce e gas" , "/it/supporto/faq/guida-bolletta-luce-gas"} , 
					//{"Prescrizione degli importi fatturati per la fornitura di energia elettrica e gas" , ""} , 
					{"Modulistica" , "https://www-coll1.enel.it/it/supporto/faq/modulistica"} , 
					{"Modulistica reclami" , "/it/supporto/faq/come-fare-un-reclamo-enel-energia"} , 
					{"Remit" , "https://corporate-qual.enel.it/it/remit"} , 
					{"Negoziazione paritetica" , "https://www.pariteticaenel-associazioni.it/"} , 
					{"Evoluzione mercati al dettaglio" , "https://www.autorita.energia.it/it/190701"} , 
					{"Offerta Servizio Tutela Gas" , "https://www-coll1.enel.it/it/supporto/faq/offerta-servizio-tutela-gas"}};
			
			article.checkLinks(links);
			Thread.sleep(5000);
			article.clickPageLinkByTextAndHref(links[0][0], links[0][1]);

			TopMenuComponent topMenu = new TopMenuComponent(driver);

			topMenu.checkMenuPath(prop.getProperty("SOS_PATH_MENU_TEXT"));
			topMenu.checkTitle(prop.getProperty("SOS_TITLE_MENU_TEXT"));
			topMenu.checkDetailById(prop.getProperty("SOS_DETAIL_MENU_TEXT") , "descriptionHP");
			
			By info_icon=article.info_icon;
			article.clickComponent(info_icon);
//			String[] popup_text = {
//					 "Chi è il Distributore"
//					,"In Italia esistono diversi distributori regolarizzati dall'ARERA per pertinenza geografica."
//					,"Attraverso la gestione delle reti di distribuzione dell'energia elettrica in media e bassa tensione, e del gas attraverso le reti cittadine, il distributore è l'incaricato alla consegna e trasporto."
//					,"Si occupa inoltre della lettura dei consumi, essendo il proprietario dei contatori."
//					,"Il distributore è anche l'incaricato ad intervenire quando si verificano guasti sulle reti di sua competenza, problemi con il contatore o per l’allacciamento di una nuova fornitura."
//			};
			
			String popup_text = "Chi è il DistributoreIn Italia esistono diversi distributori regolarizzati dall'ARERA per pertinenza geografica.Attraverso la gestione delle reti di distribuzione dell'energia elettrica in media e bassa tensione, e del gas attraverso le reti cittadine, il distributore è l'incaricato alla consegna e trasporto.Si occupa inoltre della lettura dei consumi, essendo il proprietario dei contatori.Il distributore è anche l'incaricato ad intervenire quando si verificano guasti sulle reti di sua competenza, problemi con il contatore o per l’allacciamento di una nuova fornitura.";
			
			article.compareText(article.remodalContainer, popup_text, true);
//			article.checkTextsByInnerAndClass(popup_text, "remodal remodal-is-initialized remodal-is-opened");
			
			By close_popup_button=article.close_popup_button;
			article.clickComponent(close_popup_button);
			
			//article.checkTextsByInnerAndId(prop.getProperty("SOS_ARTICLE_TEXTS").split(separator) , "landing");		
			article.checkQuestionsBySection(prop.getProperty("SOS_QUESTIONS_TEXT").split(separator) , "details orange");
			article.checkAnswersByQuestionsTextAndSection(prop.getProperty("SOS_ANSWERS_TEXT").split(separator)
					, prop.getProperty("SOS_QUESTIONS_TEXT").split(separator)
					, "details orange");
		
			By luce_button=article.luce_button;
			article.clickComponent(luce_button);
			String[] bottom_legal_info = {
					"Le informazioni in merito ad eventuali guasti o interruzioni programmate, che troverete all’interno del presente sito, sono rese pubbliche dal distributore di zona competente."
			};
			
			article.checkTextsByTextAndClass(bottom_legal_info, "notaLegale");
		//	article.checkTextsByTextAndId(prop.getProperty("LUCE_ARTICLE_TEXTS").split(separator) , "luceSection");
			article.checkQuestionsBySection(prop.getProperty("LUCE_QUESTIONS_TEXT").split(separator) , "details luce");			
			article.checkAnswersByQuestionsTextAndSection(prop.getProperty("LUCE_ANSWERS_TEXT").split(separator)
					, prop.getProperty("LUCE_QUESTIONS_TEXT").split(separator)
					, "details luce");
			
//			String[][] link = {{"Inserisci l'indirizzo" , "/it/inserimento-informazioni?indirizzo"},{"/it/inserimento-informazioni?indirizzo"}};
//			article.clickPageLinkByInnerAndHref(link[0][0], link[0][1]);
			By luce_indirizzo_link = article.luce_indirizzo_link;
			article.clickComponent(luce_indirizzo_link);
			
			topMenu.checkMenuPath("HOME/ INSERIMENTO INFORMAZIONI");
			topMenu.checkTitle("Inserisci le informazioni");
			topMenu.checkDetailByClass("Inserisci l'indirizzo in cui manca la corrente per ricevere il numero di pronto intervento del distributore da contattare" , "luceLS");
			
			String[] indirizzo_text = {
					"Inserendo l'indirizzo della fornitura in cui manca la corrente riceverai dapprima un supporto interattivo per verificare lo stato del tuo impianto e, successivamente, il numero di pronto intervento del distributore di energia elettrica."
					};

			//article.checkTextsByInnerAndClass(indirizzo_text, "descriptionFormIndirizzo");
			By citta_input = article.citta_input;
			article.verifyComponentExistence(citta_input);
			By indirizzo_input = article.indirizzo_input;
			article.verifyComponentExistence(indirizzo_input);
			By civico_input = article.civico_input;
			article.verifyComponentExistence(civico_input);
			By privicy_textarea = article.privicy_textarea;
			article.verifyComponentExistence(privicy_textarea);
			article.verifyComponentText(privicy_textarea, "INFORMATIVA PRIVACY ai sensi dell’art. 13 del Decreto Legislativo 30 giugno 2003, n. 196. Enel Energia S.p.A. ti informa che utilizzerà i dati da te rilasciati esclusivamente al fine di interrogare i sistemi messi a disposizione dal Distributore territorialmente competente per fornirti le informazioni da te richieste. Il conferimento dei dati da te rilasciati nel presente form è facoltativo e volto unicamente a consentire ad EE di inviarti le informazioni da te richieste, restando pertanto esclusa qualsiasi ulteriore finalità. Il mancato rilascio dei dati suddetti non consentirà tuttavia ad EE di procedere a tale fine. I tuoi dati personali saranno trattati anche con l’ausilio di strumenti informatici in modo da garantirne la sicurezza e la riservatezza, ed automaticamente cancellati dopo la chiusura della pagina. Titolare del trattamento dei dati è Enel Energia, con sede in Viale Regina Margherita, 125, 00198 Roma.” Informativa privacy completa sul sito www-coll1.enel.it");
			By esc_button = article.esc_button;
			article.verifyComponentExistence(esc_button);
			By confirm_button = article.confirm_button;
			article.verifyComponentExistence(confirm_button);
			article.clickComponent(confirm_button);
			By citta_error = article.citta_error;
			article.verifyComponentExistence(citta_error);
			By indirizzo_error = article.indirizzo_error;
			article.verifyComponentExistence(indirizzo_error);
			By civico_error = article.civico_error;
			article.verifyComponentExistence(civico_error);
			article.clickComponent(esc_button);
//			article.verifyComponentText(citta_input, "Inserisci la città");

			
			By gas_button=article.gas_button;
			article.clickComponent(gas_button);
			
			//article.checkTextsByTextAndId(prop.getProperty("GAS_ARTICLE_TEXTS").split(separator) , "gasSection");
			article.checkQuestionsBySection(prop.getProperty("GAS_QUESTIONS_TEXT").split(separator) , "details gas");
			article.checkAnswersByQuestionsTextAndSection(prop.getProperty("GAS_ANSWERS_TEXT").split(separator)
					, prop.getProperty("GAS_QUESTIONS_TEXT").split(separator)
					, "details gas");
			
			By gas_indirizzo_link = article.gas_indirizzo_link;
			article.clickComponent(gas_indirizzo_link);
			
			topMenu.checkMenuPath("HOME/ INSERIMENTO INFORMAZIONI");
			topMenu.checkTitle("Inserisci le informazioni");
			topMenu.checkDetailByClass("Ti forniamo il numero di telefono per chiamare il tuo distributore gas" , "gasLS");
			
			String[] gas_indirizzo_text = {
					"Inserisci l'indirizzo della fornitura in cui manca il gas e riceverai informazioni utili per contattare il tuo distributore di zona"
					};

			//article.checkTextsByInnerAndClass(gas_indirizzo_text, "descriptionFormIndirizzo");

			article.verifyComponentExistence(citta_input);

			article.verifyComponentExistence(indirizzo_input);

			article.verifyComponentExistence(civico_input);

			article.verifyComponentExistence(privicy_textarea);
			article.verifyComponentText(privicy_textarea, "INFORMATIVA PRIVACY ai sensi dell’art. 13 del Decreto Legislativo 30 giugno 2003, n. 196. Enel Energia S.p.A. ti informa che utilizzerà i dati da te rilasciati esclusivamente al fine di interrogare i sistemi messi a disposizione dal Distributore territorialmente competente per fornirti le informazioni da te richieste. Il conferimento dei dati da te rilasciati nel presente form è facoltativo e volto unicamente a consentire ad EE di inviarti le informazioni da te richieste, restando pertanto esclusa qualsiasi ulteriore finalità. Il mancato rilascio dei dati suddetti non consentirà tuttavia ad EE di procedere a tale fine. I tuoi dati personali saranno trattati anche con l’ausilio di strumenti informatici in modo da garantirne la sicurezza e la riservatezza, ed automaticamente cancellati dopo la chiusura della pagina. Titolare del trattamento dei dati è Enel Energia, con sede in Viale Regina Margherita, 125, 00198 Roma.” Informativa privacy completa sul sito www-coll1.enel.it");

			article.verifyComponentExistence(esc_button);

			article.verifyComponentExistence(confirm_button);
			article.clickComponent(confirm_button);

			article.verifyComponentExistence(citta_error);

			article.verifyComponentExistence(indirizzo_error);

			article.verifyComponentExistence(civico_error);
			article.clickComponent(esc_button);
			
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

			//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

	public static String convertArrayToStringWhitSeparator(String[] array , String separator) {
		String prop = "";
		for(String el : array) {
			prop = prop.concat(el).concat(separator);
		}
		prop = prop.substring(0, prop.length() - separator.length());
		return prop;
	}
}
