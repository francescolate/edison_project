package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ContactComponent;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyEmailSubjectAndContent_94 {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String content = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            content = gm.recuperaLink(prop, "FORMSCRIVICI 94", false);
		            Thread.sleep(5000);
		            System.out.println(content);
		            
		            ContactComponent con = new ContactComponent(driver);
		            con.verifyEmailContent(content, con.EmailContentLine1);
		            con.verifyEmailContent(content, con.EmailContentLine2);
		            con.verifyEmailContent(content, con.EmailContentLine3);
		            con.verifyEmailContent(content, con.EmailContentLine4);
		            con.verifyEmailContent(content, con.EmailContentLine5);
		            con.verifyEmailContent(content, con.EmailContentLine6);
		            //con.verifyEmailContent(content, prop.getProperty("NOME_ASSOCIAZIONE"));
		            //con.verifyEmailContent(content, prop.getProperty("CFASSOCIAZIONE"));
		            //con.verifyEmailContent(content, prop.getProperty("CF_DELEGATO"));
		            //con.verifyEmailContent(content, prop.getProperty("NOMINATIVO_DELEGATO"));
		            con.verifyEmailContent(content, prop.getProperty("EMAIL"));
		            con.verifyEmailContent(content, prop.getProperty("TELEPONE"));
		            con.verifyEmailContent(content, prop.getProperty("NOME"));
		            con.verifyEmailContent(content, prop.getProperty("COGNOME"));
		            con.verifyEmailContent(content, prop.getProperty("CF"));
		            //con.verifyEmailContent(content, prop.getProperty("SEI GIÀ CLIENTE?"));
		            //con.verifyEmailContent(content, prop.getProperty("DI COSA HAI BISOGNO?"));
		            //con.verifyEmailContent(content, prop.getProperty("VUOI INSERIRE ALLEGATI?"));
		            con.verifyEmailContent(content, prop.getProperty("Privacy"));
		            con.verifyEmailContent(content, prop.getProperty("ARGOMENTO"));
		            con.verifyEmailContent(content, prop.getProperty("MESSAGGIO"));
		            //con.verifyEmailContent(content, prop.getProperty("PREFIX"));

		            logger.write("Recupera Link - Completed");
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
          			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
