package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_128 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			prop.setProperty("PVVALUE", "3 kW / 220 V");
			prop.setProperty("MENUITEM", "Inserimento dati Preventivo Esito");		
			prop.setProperty("PLACEHOLDER", "Inserisci il valore in Ampere");
			prop.setProperty("WARNING", "Attenzione La potenza inserita non è sufficiente per il sollevamento");
			prop.setProperty("POWERDEFAULT", "3 kW(potenza attuale)");
			prop.setProperty("VOLTAGEFAULT", "220 V(tensione attuale)");
			prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
		    prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
			prop.setProperty("BILLSERVICES", "Bolletta Web Pagamento Online Addebito Diretto Rettifica Bolletta Rateizzazione Bolletta di Sintesi o di Dettaglio Duplicato Bolletta");
			prop.setProperty("FAQ", "Cosa si intende per potenza e tensione? Quando è necessario modificare la potenza? Quali sono i tempi per l’esecuzione della modifica di potenza o tensione? Cosa si intende per sopralluogo tecnico? Quali sono i costi dell’operazione? Come posso pagare il preventivo?");
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
			
			home.clickComponent(home.services);
			
			ServicesComponent services = new ServicesComponent(driver);
			
			logger.write("Check For supply services-- start");
			
			services.checkForSupplyServices(prop.getProperty("SUPPLYSERVICES"));
			
			logger.write("Check For supply services-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			services.waitForElementToDisplay(services.modificaPotenzaeoTensione);
			
			//services.clickComponent(services.modificaPotenzaeoTensione);
			
			services.jsClick(services.modificaPotenzaeoTensione);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Check Power And Voltage Content-- Start");

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageContent);
			
			changePV.checkForPageContent();
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageButton);
			
			logger.write("Check Power And Voltage Content-- Start");
			
			logger.write("Check Faq -- Start");
			
			changePV.checkForFaqQst(prop.getProperty("FAQ"));
			
			logger.write("Check Faq -- Completed");
			
			logger.write("Check Faq plus Icon -- Start");
			
			changePV.checkForPlusIcon();
			
			logger.write("Check Faq plus Icon -- Completed");

			logger.write("Check Faq one details -- Start");
			
			changePV.checkFaqOneDetails();
			
			logger.write("Check Faq one details -- Completed");

			logger.write("Check Faq details -- Start");

			changePV.checkFaqDetails();
			
			logger.write("Check Faq details -- Completed");
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			Thread.sleep(10000);
			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText);

			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");
			
			logger.write("Check for Exit And Change Power Voltage Button -- Start");
			
			changePV.verifyComponentExistence(changePV.exitButton);
			
			changePV.verifyComponentExistence(changePV.changePowerOrVoltageButton);

			logger.write("Check for Exit And Change Power Voltage Button -- Completed");
			
			logger.write("Check for Exit And Change Power Voltage Button Status-- Start");

			changePV.checkForExitButtonStatus(changePV.exitButton);
			
			changePV.checkForChangePowerOrVoltageButtonStatus(changePV.changePowerOrVoltageButton);
			
			logger.write("Check for Exit And Change Power Voltage Button Status-- Completed");
			
			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");

			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");

			changePV.checkForChangePowerOrVoltageButtonStatus(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on services -- Start");
			
			home.clickComponent(home.services);
			
			logger.write("Click on services -- Completed");
			
			changePV.checkForPopUp();
			
			logger.write("Click on No option in pop up  -- Start");
			
			changePV.clickComponent(changePV.popupNo);
			
			logger.write("Click on No option in pop up  -- Completed");

			logger.write("Check page title and subtext  -- Start");

			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText);
			
			logger.write("Check page title and subtext  -- Completed");

			logger.write("Click on services -- Start");

			home.clickComponent(home.services);
			
			logger.write("Click on services -- Completed");

			changePV.checkForPopUp();
			
			logger.write("Click on close mark in pop up -- Start");

			changePV.clickComponent(changePV.attenzionepopupClose);
			
			logger.write("Click on close mark in pop up -- Completed");
			
			logger.write("Check page title and subtext  -- Start");

			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText);
			
			logger.write("Check page title and subtext  -- Completed");

			home.clickComponent(home.services);
			
			changePV.checkForPopUp();
			
			logger.write("Click on Yes option in pop up -- Start");

			changePV.clickComponent(changePV.popupYes);
			
			logger.write("Click on Yes option in pop up -- Completed");
			
			logger.write("Check services page title and subtext -- Start");
			
			services.verifyComponentExistence(services.serviceTitle);
			
			services.verifyComponentExistence(services.serviceSubText);
			
			logger.write("Check services page title and subtext -- Completed");

			logger.write("Check for supply services -- Start");

			services.checkForSupplyServices(prop.getProperty("SUPPLYSERVICES"));
			
			logger.write("Check for supply services -- Completed");
			
			services.verifyComponentExistence(services.billServices);
			
			services.verifyComponentExistence(services.billServicesSubText);
			
			logger.write("Check for bill services -- Start");
			
			services.checkForBillServices(prop.getProperty("BILLSERVICES"));
			
			logger.write("Check for bill services -- Completed");

			services.verifyComponentExistence(services.contractServices);
			
			services.verifyComponentExistence(services.contractServicesSubText);
			
			logger.write("Check for Contract services -- Start");

			services.checkForContractServices(prop.getProperty("CONTRACTSERVICES"));
			
			logger.write("Check for Contract services -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
				
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
