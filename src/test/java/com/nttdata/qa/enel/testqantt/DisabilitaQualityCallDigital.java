package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class DisabilitaQualityCallDigital {

	@Step("Disabilita Confirmation Call")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				// System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}

				SeleniumUtilities util = new SeleniumUtilities(driver);
									
				
				String	query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_TypeCall__c, ITA_IFM_SubChannel__c, ITA_IFM_ProcessType__c, ITA_IFM_CustomerType__c, ITA_IFM_Residential_Use__c, ITA_IFM_Alphanum_CF__c, " + 
							"ITA_IFM_CustomerChoice__c, ITA_IFM_CallDirection__c, ITA_IFM_TypeLegalForm__c, ITA_IFM_Offer_From_Tablet__c, ITA_IFM_Offer_With_Delegation__c, ITA_IFM_Mobile_Phone__c, ITA_IFM_Email__c, " +
							"LastModifiedById, LastModifiedDate " + 
							" FROM " +
							"ITA_IFM_Attivazione_QC_CC__c " + 
							" where id = '"+prop.getProperty("ID_ATTIVAZIONE")+"'";
				
					
				a.insertQuery(query);
				a.pressButton(a.submitQuery);
				TimeUnit.SECONDS.sleep(3);
				a.recuperaRisultati(1, prop);
				//se era già attivo non faccio nulla
 				if (prop.getProperty("ITA_IFM_Status__c".toUpperCase()).equals("Attivo")) {
					a.updateRow("ITA_IFM_Status__c", "Inattivo");
					TimeUnit.SECONDS.sleep(10);
				}
				TimeUnit.SECONDS.sleep(3);

				a.logoutWorkbench();

			}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
