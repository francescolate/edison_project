package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNServiziComponent;
import com.nttdata.qa.enel.components.colla.BolleteBSNComponent;
import com.nttdata.qa.enel.components.colla.BolleteWebBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_302_AttivaBollettaWeb {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			pa.verifyComponentExistence(pa.pageTitle);
			pa.comprareText(pa.pageTitle, pa.PageTitle, true);
			pa.verifyComponentExistence(pa.servizi);
			pa.clickComponent(pa.servizi);
			
			BSNServiziComponent sc = new BSNServiziComponent(driver);
			sc.verifyComponentExistence(sc.pageTitle);
			sc.comprareText(sc.pagePath, sc.Path, true);
			sc.comprareText(sc.pageText, sc.PageText, true);
			sc.clickComponent(sc.bollettaWeb);
			
			BolleteWebBSNComponent bc = new BolleteWebBSNComponent(driver);
			bc.verifyComponentExistence(bc.pageTitle);
			bc.verifyComponentExistence(bc.path);
			bc.comprareText(bc.titleSubtext, bc.TitleSubText, true);
			bc.comprareText(bc.pageText, bc.PageText, true);
			bc.comprareText(bc.attivazioneRow, bc.AttivazioneRow, true);
			bc.comprareText(bc.modificaRow, bc.ModificaRow, true);
			bc.comprareText(bc.revocaRow, bc.RevocaRow, true);
			bc.verifyComponentExistence(bc.attivazioneScopriDiPiu);
			bc.clickComponent(bc.attivazioneScopriDiPiu);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
