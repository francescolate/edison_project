package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_73_ModuloReclami {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ModuloReclamiComponent mrc = new ModuloReclamiComponent(driver);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			mrc.launchLink(prop.getProperty("WP_LINK"));
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
 			mrc.comprareText(mrc.home, mrc.HOME, true);
			mrc.comprareText(mrc.formReclami, mrc.FORM_RECLAMI, true);
			mrc.comprareText(mrc.reclamiHeading, mrc.FORM_RECLAMI_HEADING, true);
			
			mrc.clickComponent(mrc.tipologiaDelReclamo);
			mrc.clickComponent(mrc.optionReclamoLuce);
			mrc.comprareText(mrc.inserisciITuoiDati, mrc.INSERICI_I_TUOI_DATI, true);
			mrc.enterInputParameters(mrc.nome, prop.getProperty("NOME"));
			mrc.enterInputParameters(mrc.cognome, prop.getProperty("COGNOME"));
			mrc.enterInputParameters(mrc.telefonoCellulare, prop.getProperty("TELEFONOCELLULARE"));
			mrc.clickComponent(mrc.fasceOrarieDiReperibilità);
			mrc.clickComponent(mrc.option8to12);
			mrc.enterInputParameters(mrc.codiceFiscale, prop.getProperty("CODICE_FISCALE"));
			mrc.enterInputParameters(mrc.indirizzoDellaForniture,prop.getProperty("INDIRIZZO_DELLA_FORNITURE"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("EMAIL"));
			mrc.enterInputParameters(mrc.confermaEmail, prop.getProperty("EMAIL"));
			mrc.enterInputParameters(mrc.numeroCliente, prop.getProperty("NUMERO_CLIENTE"));
			mrc.enterInputParameters(mrc.codicePOD, prop.getProperty("CODICE_POD"));
			mrc.clickComponent(mrc.confermaButton);
			
			mrc.comprareText(mrc.argomentoeDescrizioneDelReclamo, mrc.ARGOMENTO_DESCRIZIONE_DEL_RECLAMO, true);
			mrc.comprareText(mrc.effttuaUnaSelezione, mrc.EFFTTUAUNASELEZIONE, true);
			
			mrc.checkForArgomentoValue(mrc.argomentDropdown, mrc.VALUE);
			
			mrc.scrollComponent(mrc.argomentDropdown);
			mrc.jsClickComponent(mrc.argomentDropdown);
			mrc.jsClickComponent(mrc.contatti);
			
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.descrizioneErrorMessage, mrc.ERROR_MESSAGE_DESCRIZIONE, true);
			mrc.enterInputParameters(mrc.descrizioneRichiesta, prop.getProperty("DESCRIZIONE"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.casellaNonSelezionata, mrc.CASELLA_NON_SELEZIONATA, true);
			
		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
