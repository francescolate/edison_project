package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.InfoEESteps_BsnComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ActivateInfoEnelEnergia_Bsn_Continue {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Selecting avviso consumi SMS check");
			logger.write("Selecting avviso consumi SMS check - Start");
			InfoEESteps_BsnComponent steps = new InfoEESteps_BsnComponent(driver);
			
			//steps.clickComponent(steps.attivabutton);
			 prop.setProperty("MOBILE_NUMBER", "3291929449");
			 InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);	
				BSN_ModificaInfoEnelEnergiaComponent bmc = new BSN_ModificaInfoEnelEnergiaComponent(driver);
				bmc.clickComponent(bmc.avvisoEmissioneBollettaSMS);
			 bmc.enterInputParameters(bmc.numeroDiCellulare, prop.getProperty("MOBILE_NUMBER"));
			 steps.enterInputParameters(steps.numeroConfirmInputNew, prop.getProperty("MOBILE_NUMBER"));
			steps.clickComponent(steps.proseguiBtn);
			logger.write("Selecting avviso consumi SMS check - Completed");
			
		    
			System.out.println("Step 2 verification and confirm");
			logger.write("Step 2 verification and confirm - Start");
			//steps.checkStep2Elements();
			steps.clickComponent(steps.confermaBtn);
			logger.write("Step 2 verification and confirm - Completed");
			
			System.out.println("Step 3 verification and completing");
			logger.write("Step 3 verification and completing - Start");
			//steps.verifyComponentVisibility(steps.step3OkMessage);
			//steps.verifyVisibilityThenClick(steps.step3EndBtn);
			logger.write("Step 3 verification and completing - Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
