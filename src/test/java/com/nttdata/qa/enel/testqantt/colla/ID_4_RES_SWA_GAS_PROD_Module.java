package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_4_RES_SWA_GAS_PROD_Module {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		int counter = 0; boolean exitCondition;
		QANTTLogger logger = new QANTTLogger(prop);
		try{
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OCR_Module_Component omc = new OCR_Module_Component(driver);
			Thread.sleep(3000);
			
//			do{
//				if(!omc.getElementHref(omc.offerDetailsButtonGas).equals(prop.get("OCR_OFFER_LINK"))){
//					counter++;
//					Thread.sleep(5000);
//				}else
//					counter=10;
//			}while(counter<10);
//			
//			omc.verifyComponentExistence(omc.offerDetailsButtonGas);
//			omc.clickComponent(omc.offerDetailsButtonGas);
			
			omc.verifyComponentExistence(omc.offerActivationButton);
			omc.clickComponent(omc.offerActivationButton);
			
			omc.verifyComponentExistence(omc.compilaManualmenteButton);
			omc.clickComponent(omc.compilaManualmenteButton);
			
			omc.verifyComponentExistence(omc.firstnameInputField);
			omc.clearAndSendKeys(omc.firstnameInputField, prop.getProperty("FIRSTNAME"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.lastnameInputField);
			omc.clearAndSendKeys(omc.lastnameInputField, prop.getProperty("LASTNAME"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.fiscalCodeInputField);
			omc.clearAndSendKeys(omc.fiscalCodeInputField, prop.getProperty("CF"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.mobileInputField);
			omc.clearAndSendKeys(omc.mobileInputField, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.emailInputField);
			omc.clearAndSendKeys(omc.emailInputField, prop.getProperty("EMAIL"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.emailConfirmationInputField);
			omc.clearAndSendKeys(omc.emailConfirmationInputField, prop.getProperty("EMAIL"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.privacyCheckbox);
			omc.clickComponent(omc.privacyCheckbox);
			
			Thread.sleep(1000);
			
			omc.verifyComponentExistence(omc.salvaContinuaDopoButton);
			omc.clickComponent(omc.salvaContinuaDopoButton);
			
			Thread.sleep(1000);
			
			System.out.println("Check dialog - Start");
			omc.checkSalvaOraContinuaDopo(prop.getProperty("FIRSTNAME"), prop.getProperty("LASTNAME"), prop.getProperty("EMAIL"), prop.getProperty("MOBILE_NUMBER"), prop.getProperty("CF"), driver);
			omc.clickComponent(omc.esciButton);
			System.out.println("Check dialog - Completed");

			System.out.println("Click Altro dialog - Start");
			omc.verifyComponentVisibility(omc.salvaContinuaDopoButton);
			omc.clickComponent(omc.salvaContinuaDopoButton);
			omc.verifyComponentExistence(omc.checkAltro);
			omc.clickComponent(omc.checkAltro);
			omc.verifyComponentVisibility(omc.areaAltro);
			System.out.println("Click Altro dialog - Completed");
			
			System.out.println("Click Non ho le informazioni richieste dialog - Start");
			omc.verifyComponentExistence(omc.checkInfo);
			omc.clickComponent(omc.checkInfo);
			omc.verifyComponentInexistence(omc.areaAltro);
			System.out.println("Click Non ho le informazioni richieste dialog - Completed");
			
			System.out.println("Click Salva ed Esci dialog - Start");
			omc.verifyComponentVisibility(omc.salvaEsciButton);
			omc.clickComponent(omc.salvaEsciButton);
			System.out.println("Click Salva ed Esci dialog - Completed");
			
			System.out.println("Check last page - Start");
			omc.verifyComponentVisibility(omc.finalText);
			omc.containsText(omc.finalText, omc.finalText1);
			omc.containsText(omc.finalText, omc.finalText2);
			omc.verifyComponentExistence(omc.backToHomeButton);
			omc.clickComponent(omc.backToHomeButton);
			System.out.println("Click last page - Completed");
			

			
			prop.setProperty("RETURN_VALUE", "OK");

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
	public static void closeChat(OCR_Module_Component omc) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, 1))
			omc.jsClickComponent(omc.chatCloseButton);
	}
	
	public static void closeChat(OCR_Module_Component omc, int timeout) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, timeout))
			omc.jsClickComponent(omc.chatCloseButton);
	}
}
