package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_76_Modulo_Reclami {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
			
			mod.verifyComponentExistence(mod.tipologia_reclamoLabel);
			mod.clickComponent(mod.tipologia_reclamoDropdown);
			mod.clickComponent(mod.reclamoLuce);
			
			mod.enterInputParameters(mod.nome, prop.getProperty("NOME"));
			mod.enterInputParameters(mod.cognome, prop.getProperty("COGNOME"));
			mod.enterInputParameters(mod.cellulare, prop.getProperty("CELLULARE"));
			mod.clickComponent(mod.fasceorarie);
			mod.clickComponent(mod.fasceorarieValue);
			mod.enterInputParameters(mod.cf, prop.getProperty("CF"));
			mod.enterInputParameters(mod.indrizzo, prop.getProperty("ADDRESS"));
			mod.enterInputParameters(mod.email, prop.getProperty("EMAIL"));
			mod.enterInputParameters(mod.confermaEmail, prop.getProperty("EMAIL"));
			mod.enterInputParameters(mod.numeroClienti, prop.getProperty("NUMEROCLIENTE"));
			mod.enterInputParameters(mod.pod, prop.getProperty("POD"));
			
			mod.clickComponent(mod.argumentoDropdown);
			mod.clickComponent(mod.morosita);
			mod.clickComponent(mod.argumentoDropdown);
			mod.clickComponent(mod.morosita);
			mod.enterInputParameters(mod.descrizione, prop.getProperty("DESCRIZIONE"));
			mod.enterInputParameters(mod.autolettura, prop.getProperty("AUTOLETTURA"));
			mod.enterInputParameters(mod.dateAutolettura, prop.getProperty("DATE"));
			
			mod.clickComponent(mod.checkBox);
			mod.clickComponent(mod.conferma);
			
			mod.verifyComponentExistence(mod.moduloReclamiTitle);
			mod.verifyComponentExistence(mod.moduloReclaimMsgTitle);
			Thread.sleep(5000);
			mod.comprareText(mod.moduloReclaimMsg, mod.MODULORECLAIMMSG, true);
			mod.clickComponent(mod.indietro);
			
			mod.verifyComponentExistence(mod.supportoTitle);
			mod.comprareText(mod.supportoTitle, mod.SUPPORTOTITLE, true);
	//		mod.comprareText(mod.supportoPath, mod.SUPPORTOPATH, true);
			
			Thread.sleep(12000);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
