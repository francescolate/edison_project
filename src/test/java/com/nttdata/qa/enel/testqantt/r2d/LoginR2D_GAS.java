package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.r2d.LoginR2DComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class LoginR2D_GAS {

	@Step("Login RD2 GAS")
	public static void main(String[] args) throws Exception {

		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
//			prop.setProperty("INCOGNITO_MODE", "Y");
			WebDriver driver = WebDriverManager.getNewWebDriver(prop); 
			QANTTLogger logger = new QANTTLogger(prop);

				LoginR2DComponent page = new LoginR2DComponent(driver);
//				System.out.println("http://"+prop.getProperty("USER_R2D",prop.getProperty("USERNAME_R2D_GAS"))+":"+prop.getProperty("PASSWORD_R2D",prop.getProperty("PASSWORD_R2D_GAS"))+"@"+"r2d-coll.awselb.enelint.global/r2d/gas/home.do");
				page.navigate("http://"+prop.getProperty("USER_R2D",prop.getProperty("USERNAME_R2D_GAS"))+":"+prop.getProperty("PASSWORD_R2D",prop.getProperty("PASSWORD_R2D_GAS"))+"@"+"r2d-coll.awselb.enelint.global/r2d/gas/home.do");
//				String Link = "http://AF09527@enelint.global:Enel$R32020@r2d-coll.awselb.enelint.global/r2d/gas/login.do";
//				page.navigate(Link);
				
				TimeUnit.SECONDS.sleep(5);
//			prop.setProperty("INCOGNITO_MODE", "N");
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
