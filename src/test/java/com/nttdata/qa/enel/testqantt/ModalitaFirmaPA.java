package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModalitaFirmaPA {

	

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			String statoUbiest=Costanti.statusUbiest;
			logger.write("Selezione Modalità Firma - Start ");
			
			String modalitaFirma=prop.getProperty("MODALITA_FIRMA");
			
			
			//SELEZIONO MODALITA FIRMA
			
			modalita.selezionaLigtheningValue("Modalità Firma", modalitaFirma);
			
			switch(modalitaFirma.toLowerCase()) 
			{
				case "vocal order":
					String canaleInvio=prop.getProperty("CANALE_INVIO");
					String email=prop.getProperty("EMAIL");
					modalita.selezionaLigtheningValue("Canale Invio", "Canale Invio",canaleInvio );
					modalita.sendText("Indirizzo Email", email);
					modalita.clickComponentWithJseAndCheckSpinners(modalita.confermaButton);
						
					break;
				default:
					throw new Exception("");
					
			
			
			}
			
			
			logger.write("Selezione Modalità Firma - Completed ");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
}
