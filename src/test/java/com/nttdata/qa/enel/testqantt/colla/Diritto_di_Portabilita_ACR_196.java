package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Diritto_di_Portabilita_ACR_196 {

	public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		
		logger.write("Correct visualization of the Home Page with central text- Starts");		
		dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
		dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
		dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
		dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
		logger.write("Correct visualization of the Home Page with central text- Ends");
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Section view Diritto di Portabilità - Starts");
		dpc.comprareText(dpc.dirittoDiPortabilitaTitle1, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_TITLE1, true);
		dpc.comprareText(dpc.dirittoDiPortabilitaTitle2, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_TITLE2, true);
		dpc.comprareText(dpc.dirittoDiPortabilitaText, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_TEXT, true);
		logger.write("Section view Diritto di Portabilità - Ends");
		
		logger.write("Click on ACCEDI AL SERVIZIO and verify Correct display of the page with Diritto di Portabilità- Starts");
		dpc.clickComponent(dpc.accediAlServizoButton);
		dpc.comprareText(dpc.dirittoDiPortabilityPageTitle, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_PAGE_TITLE, true);
		dpc.comprareText(dpc.dirittoDiPortabilityPageText, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_PAGE_TEXT, true);
		dpc.verifyComponentExistence(dpc.indirizzoDellaFornitura);
		dpc.verifyComponentExistence(dpc.numeroCliente);
		dpc.verifyComponentExistence(dpc.checkbox1);
		dpc.verifyComponentExistence(dpc.scaricaFile);
		dpc.isElementEnabled(dpc.scaricaFile);
		dpc.verifyComponentExistence(dpc.buttonEsci);
		dpc.verifyComponentExistence(dpc.buttonRichediITuoiDati);
		dpc.isElementDisabled(dpc.buttonRichediITuoiDati);
		logger.write("Click on ACCEDI AL SERVIZIO and verify Correct display of the page with Diritto di Portabilità- Ends");
		
		logger.write("Select a supply and verify the RICHIEDI I TUOI DATI  button is enabled- Starts");
		dpc.clickComponent(dpc.checkbox1);
		dpc.isElementEnabled(dpc.buttonRichediITuoiDati);
		logger.write("Select a supply and verify the RICHIEDI I TUOI DATI  button is enabled- Ends");
		
		logger.write("Click on RICHIEDI I TUOI DATI and Correct page display - Starts");
		dpc.clickComponent(dpc.buttonRichediITuoiDati);
		dpc.comprareText(dpc.sectionText, DirittodiPortabilitaComponent.SECTION_TEXT, true);
		logger.write("Click on RICHIEDI I TUOI DATI and Correct page display - Ends");
		
		logger.write("Click on FINE and verify Correct visualization of the Home Page- Starts");
		dpc.clickComponent(dpc.buttonFine);
		dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
		dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
		dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
		dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
		logger.write("Click on FINE and verify Correct visualization of the Home Page- Ends");
		
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
