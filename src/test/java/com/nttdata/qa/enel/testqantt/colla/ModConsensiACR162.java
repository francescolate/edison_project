package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModConsensiACR162 {

	public static void main(String[] args) throws Exception {
	
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		ModConsensiACR162Component mod = new ModConsensiACR162Component(driver);
				
		mod.comprareText(mod.homePageHeading1, ModConsensiACR162Component.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1, ModConsensiACR162Component.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2, ModConsensiACR162Component.HOME_PAGE_HEADING2, true);
//		mod.comprareText(mod.homePageParagraph2, ModConsensiACR162Component.HOME_PAGE_PARAGRAPH2, true);
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
//		mod.comprareText(mod.sectionHeading1, ModConsensiACR162Component.SECTION_PAGE_HEADING1, true);
//		mod.comprareText(mod.sectionParagraph1, ModConsensiACR162Component.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		logger.write("click on modificaContattieConsensi tile item and verify the section heading and paragraph- started");
		Thread.sleep(20000);
		mod.clickComponent(mod.modificaContattieConsensi);
		Thread.sleep(10000);
		mod.comprareText(mod.modificaSectionHeading, ModConsensiACR162Component.MODIFICA_SECTION_HEADING, true);
		mod.comprareText(mod.modificaSectionParagraph, ModConsensiACR162Component.MODIFICA_SECTION_PARAGRAPH, true);
		
		mod.comprareText(mod.modificaConsesiHeading, ModConsensiACR162Component.MODIFICA_CONSENSI_HEADING, true);
		mod.comprareText(mod.modificaConsensiParagraph, ModConsensiACR162Component.MODIFICA_CONSENSI_PARAGRAPH, true);
		mod.verifyComponentExistence(mod.modificaConsensiButton);
		Thread.sleep(5000);
		
		mod.comprareText(mod.datiRegHeading, ModConsensiACR162Component.DATI_REG_HEADING, true);
		mod.comprareText(mod.datiRegParagraph, ModConsensiACR162Component.DATI_REG_PARAGRAPH, true);
		mod.verifyComponentExistence(mod.modificaDatiRegButton);
		Thread.sleep(5000);
		mod.clickComponent(mod.modificaConsensiButton);
		Thread.sleep(5000);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		logger.write("Verification of Header - Start ");
		mod.isElementNotPresent(mod.header);
		logger.write("Verification of Header - End ");
		
		
		logger.write("Verification of Footer - Start ");
				
		prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Energia S.p.a.");
		mod.comprareText(mod.footerBeforeText, prop.getProperty("BEFORE_FOOTER_TEXT"), true);
		
		prop.setProperty("TUTTI_DIRITTI_RISERVATI", "Tutti i Diritti Riservati");
		mod.comprareText(mod.tuttiDiritti, prop.getProperty("TUTTI_DIRITTI_RISERVATI"), true);
		
		prop.setProperty("PIVA", "P. IVA 06655971007");
		//mod.comprareText(mod.pIVA, prop.getProperty("PIVA"), true);
		mod.verifyComponentExistence(mod.pIVA);
		
		mod.verifyComponentExistence(mod.informazionLegali);
		mod.verifyComponentExistence(mod.privacy);
		mod.verifyComponentExistence(mod.credits);
		mod.verifyComponentExistence(mod.contattacci);
		logger.write("Verification of Footer - End ");
		
		logger.write("Verification of Informativa Sulla Privacy - Start ");
		mod.clickComponent(mod.informativaSullaPrivacy);
		mod.comprareText(mod.informativaSullarPrivacyContent, ModConsensiACR162Component.INFORMATIVA_CONTENT, true);
		mod.clickComponent(mod.closeButton);
		logger.write("Verification of Informativa Sulla Privacy - End ");
		
		logger.write("Verification of consents for each contact- Start ");
		mod.comprareText(mod.numeroDiCellulare, ModConsensiACR162Component.NUMERO_DI_CELLULARE, true);
		mod.verifyComponentExistence(mod.cellulareCensensi1);
		mod.verifyComponentExistence(mod.cellulareConsensi1SIRadio);
		mod.verifyComponentExistence(mod.cellulareConsensi1NORadio);
		mod.verifyComponentExistence(mod.cellulareConsensi2);
		mod.verifyComponentExistence(mod.cellulareConsensi2SIRadio);
		mod.verifyComponentExistence(mod.cellulareConsensi2NORadio);
		
		mod.comprareText(mod.telefonoFisso, ModConsensiACR162Component.TELEFONO_FISSO, true);
		mod.verifyComponentExistence(mod.telefonoFissoCensensi1);
		mod.verifyComponentExistence(mod.telefonoFissoConsensi1SIRadio);
		mod.verifyComponentExistence(mod.telefonoFissoConsensi1NORadio);
		mod.verifyComponentExistence(mod.telefonoFissoConsensi2);
		mod.verifyComponentExistence(mod.telefonoFissoConsensi2SIRadio);
		mod.verifyComponentExistence(mod.telefonoFissoConsensi2NORadio);
		
		mod.comprareText(mod.email, ModConsensiACR162Component.EMAIL, true);
		mod.verifyComponentExistence(mod.emailCensensi1);
		mod.verifyComponentExistence(mod.emailConsensi1SIRadio);
		mod.verifyComponentExistence(mod.emailConsensi1NORadio);
		mod.verifyComponentExistence(mod.emailConsensi2);
		mod.verifyComponentExistence(mod.emailConsensi2SIRadio);
		mod.verifyComponentExistence(mod.emailConsensi2NORadio);
		logger.write("Verification of consents for each contact- End ");
		
		logger.write("Verification of consents for Profilazione- Start ");
		mod.comprareText(mod.consensoProfilazione, ModConsensiACR162Component.CONSENSO_PROFILAZIONE, true);
		mod.verifyComponentExistence(mod.consensoProfilazioneSIRadio);
		mod.verifyComponentExistence(mod.consensoProfilazioneNORadio);
		logger.write("Verification of consents for Profilazione- End ");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
