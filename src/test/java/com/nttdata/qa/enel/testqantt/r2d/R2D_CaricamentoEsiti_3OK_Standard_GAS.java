package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_3OK_Standard_GAS {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				// Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Code di comunicazione", "Caricamento Esiti");
				R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
				// Selezione tipologia Caricamento
				caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
				// Inserisci Pdr
				caricamentoEsiti.inserisciPdr(caricamentoEsiti.inputPDR,
						prop.getProperty("POD_GAS", prop.getProperty("POD")));
				// Inserisci ID Richiesta CRM
				caricamentoEsiti.inserisciNumeroOrdineCRM(caricamentoEsiti.inputNumeroOrdineCRM,
						prop.getProperty("OI_RICERCA"));
				// Cerca
				caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
				// Click button Azione
				caricamentoEsiti.selezionaTastoAzionePratica();
				// Verifica Stato pratica atteso
				caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "AA");
				// Selezione avento 3OK
				caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_3OK_GAS"));
				// Inserimento esito
				caricamentoEsiti.selezioneEsito("OK");
				// Calcolo sysdate
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String data = simpleDateFormat.format(calendar.getTime()).toString();

					caricamentoEsiti.inserimentoDettaglioEsito3OKconGestoreGAS(data,
							prop.getProperty("OI_RICERCA"));
			
				// Salvo valore stato pratica attuale
				prop.setProperty("STATO_PRATICA", "CI");

			
			//
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
