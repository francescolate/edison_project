package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaPreventivoComponent;
import com.nttdata.qa.enel.components.lightning.GestionePreventivoComponent;
import com.nttdata.qa.enel.components.lightning.OfferStatusVerifierComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class PagamentoPreventivo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				OfferStatusVerifierComponent offerVal = new OfferStatusVerifierComponent(driver);
				//Click su tab Dettaglio
				offerVal.clickTabDettagli(offerVal.tabDettagli);
				//Click su button Riepilogo
				offerVal.clickRiepilogo(offerVal.buttonRiepilogo);
				GestionePreventivoComponent preventivo = new GestionePreventivoComponent(driver);
				//Click su gestione preventivo
				preventivo.apriGestionePreventivoDaRiepilogoOrdini(preventivo.tabellaRiepilogoOrdini,preventivo.linkGestionePreventivo,0);
				//Verifica classificazione preventivo
				String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE");
//				if (prop.getProperty("ALLACCIO","N").equals("Y")){
				if (tipoOperazione.compareTo("ALLACCIO_EVO")==0){
				String tipopreventivo = prop.getProperty("CLASSIFICAZIONE_PREVENTIVO");
				preventivo.accettaInformativa(preventivo.popInformativaPreventivo, preventivo.buttonInformativaPreventivoOK);
				preventivo.verificaClassificazionePreventivo(tipopreventivo, preventivo.inputClassificazionePreventivo);
				//Selezione Stato Richiesta Preventivo
				preventivo.selezionaRichiestaPreventivo(preventivo.selectStatoRichiestaPreventivo, "Preventivo pagato");
				// Avanti Modalità Firma Preventivo
				preventivo.press(preventivo.confermaPreventivo);

				}
			}
            prop.setProperty("RETURN_VALUE", "OK");
        }
        catch (Exception e) 
        {
                       prop.setProperty("RETURN_VALUE", "KO");
                       StringWriter errors = new StringWriter();
                       e.printStackTrace(new PrintWriter(errors));
                       errors.toString();

                       prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
                      if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
                       //Store WebDriver Info in properties file
                       prop.store(new FileOutputStream(args[0]), "Set TestObject Info");                               
        }
	}

}
