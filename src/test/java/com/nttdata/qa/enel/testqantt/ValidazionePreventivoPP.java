package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaPreventivoComponent;
import com.nttdata.qa.enel.components.lightning.GestionePreventivoComponent;
import com.nttdata.qa.enel.components.lightning.OfferStatusVerifierComponent;
import com.nttdata.qa.enel.components.lightning.OffertaStatusVerificaPPComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ValidazionePreventivoPP {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				// Gestione preventivo su SFDC
				OffertaStatusVerificaPPComponent offerVal = new OffertaStatusVerificaPPComponent(driver);
				if (prop.getProperty("COMMODITY").equals("ELE")) {
					offerVal.gestionePreventivo();
				} else {
					offerVal.gestionePreventivoGAS(prop.getProperty("IMPORTO_PREVENTIVO"), prop.getProperty("STEP_PP"));
				}
				
			}
            prop.setProperty("RETURN_VALUE", "OK");
        }
        catch (Exception e) 
        {
                       prop.setProperty("RETURN_VALUE", "KO");
                       StringWriter errors = new StringWriter();
                       e.printStackTrace(new PrintWriter(errors));
                       errors.toString();

                       prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
                      if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
                       //Store WebDriver Info in properties file
                       prop.store(new FileOutputStream(args[0]), "Set TestObject Info");                               
        }
	}

}

