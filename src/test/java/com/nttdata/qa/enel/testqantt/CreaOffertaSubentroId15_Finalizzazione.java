package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CreaOffertaSubentroId15_Finalizzazione {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
	    	
	    	String conta=prop.getProperty("CONTA");
	    	String totale_sezioni_da_confermare=prop.getProperty("SEZIONI_TOTALI");
			int count=Integer.parseInt(conta)+1;
			int sezioni_da_confermare=Integer.parseInt(totale_sezioni_da_confermare)-count;
			
			int n_sezioni_daConfermare_rispetto_numero_totale = Integer.parseInt(gestione.getElementTextString(offer.span_sezioni_nonConfermate2).split("/")[0].toString());
			
			int n_sezioni_totale = Integer.parseInt(gestione.getElementTextString(offer.span_sezioni_nonConfermate2).split("/")[1].toString().substring(0, 2));
			
			if(sezioni_da_confermare!=(n_sezioni_totale-n_sezioni_daConfermare_rispetto_numero_totale))throw new Exception("il numero delle sezione da confermare è diverso da quello atteso");
			offer.scrollComponent(offer.buttonConfermaContattiConsensi);
			TimeUnit.SECONDS.sleep(3);
			if (prop.getProperty("CLIENTE_BUSINESS","").contentEquals("Y")) {
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Profilazione");
		    	offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Telefonico");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Cellulare");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Email");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Telefonico");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Cellulare");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Email");
			}
			else {
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Start");
			offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
			offer.scrollComponent(offer.sezioneContattiEConsensi);
			offer.clickComponent(offer.checkBoxSiContattiConsensi);
			TimeUnit.SECONDS.sleep(1);
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Completed");
			}
			
			offer.clickComponent(offer.buttonConfermaContattiConsensi);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			count=count+1;
			sezioni_da_confermare=Integer.parseInt(totale_sezioni_da_confermare)-count;
			System.out.println(sezioni_da_confermare);
			n_sezioni_daConfermare_rispetto_numero_totale = Integer.parseInt(gestione.getElementTextString(offer.span_sezioni_nonConfermate2).split("/")[0].toString());
			System.out.println(n_sezioni_daConfermare_rispetto_numero_totale);
			n_sezioni_totale = Integer.parseInt(gestione.getElementTextString(offer.span_sezioni_nonConfermate2).split("/")[1].toString().substring(0, 2));
			System.out.println(n_sezioni_totale);
			if(sezioni_da_confermare!=(n_sezioni_totale-n_sezioni_daConfermare_rispetto_numero_totale))throw new Exception("il numero delle sezione da confermare è diverso da quello atteso");
			
			
			if (prop.getProperty("CLIENTE_BUSINESS","").contentEquals("Y")) {
				offer.popolareCampiModFirma("NO VOCAL","STAMPA LOCALE");
			}
			else {
			    offer.popolareCampiModFirma("NO VOCAL","POSTA");}
			TimeUnit.SECONDS.sleep(2);
			offer.clickComponent(offer.buttonConfermaModFirmaCanale);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			if (prop.getProperty("CLIENTE_BUSINESS","").contentEquals("Y")) {
			    if (prop.getProperty("MERCATO","").contentEquals("Libero")) {
			    	offer.clickComponent(offer.buttonConfermaCvp);
					gestione.checkSpinnersSFDC();
					offer.verificaCvp();
			    }
				offer.verifyComponentExistence(offer.sezioniConfermate);
				offer.clickComponent(offer.buttonConferma);
				gestione.checkSpinnersSFDC();
			}
			else {
				count=count+1;
				sezioni_da_confermare=Integer.parseInt(totale_sezioni_da_confermare)-count;
				System.out.println(sezioni_da_confermare);
				n_sezioni_daConfermare_rispetto_numero_totale = Integer.parseInt(gestione.getElementTextString(offer.span_sezioni_nonConfermate2).split("/")[0].toString());
				System.out.println(n_sezioni_daConfermare_rispetto_numero_totale);
				n_sezioni_totale = Integer.parseInt(gestione.getElementTextString(offer.span_sezioni_nonConfermate2).split("/")[1].toString().substring(0, 2));
				System.out.println(n_sezioni_totale);
				if(sezioni_da_confermare!=(n_sezioni_totale-n_sezioni_daConfermare_rispetto_numero_totale))throw new Exception("il numero delle sezione da confermare è diverso da quello atteso");
				
				offer.clickComponent(offer.buttonConfermaCvp);
				gestione.checkSpinnersSFDC();
				offer.verificaCvp();
				offer.verifyComponentExistence(offer.sezioniConfermate);
				offer.clickComponent(offer.buttonConferma);
				gestione.checkSpinnersSFDC();
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
