package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaSeForzabile_KO_SAP {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				if(prop.getProperty("ITA_IFM_SAP_STATUS__C").compareTo("KO")!=0){
					prop.setProperty("KO_RIPROCESSABILE", "KO");
				}

				else if(prop.getProperty("ITA_IFM_SAP_STATUS__C").compareTo("KO")==0 && Costanti.lista_KO_SAP.contains(prop.getProperty("ITA_IFM_SAP_STATUSCODE__C"))){
					prop.setProperty("KO_RIPROCESSABILE", "OK");
					System.out.println("Il valore è contenuto"+ prop.getProperty("ITA_IFM_SAP_STATUSCODE__C")+ " è contenuto nella lista:"+Costanti.lista_KO_SAP.contains(prop.getProperty("ITA_IFM_SAP_STATUSCODE__C")));
				}
				else{ 
					prop.setProperty("KO_RIPROCESSABILE", "KO");
					System.out.println("Il valore non è contenuto"+ prop.getProperty("ITA_IFM_SAP_STATUSCODE__C")+ " è contenuto nella lista:"+Costanti.lista_KO_SAP.contains(prop.getProperty("ITA_IFM_SAP_STATUSCODE__C")));
					throw new Exception ("Il codice KO SAP:"+prop.getProperty("ITA_IFM_SAP_STATUSCODE__C")+" non è riprocessabile");
				}
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
