package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SelezioneUsoFornituraComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CreaOffertaSubentroId7Cla_Finalizzazione {

    public static void main(String[] args) throws Exception {
        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
            RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);

            //per commodity DUAL
            if (prop.getProperty("DUAL").contentEquals("Y")) {
                SelezioneUsoFornituraComponentEVO uso = new SelezioneUsoFornituraComponentEVO(driver);
                logger.write("Selezione 'Unita' abitativa' - Start");
                uso.selezionaLigtheningValue("Unità abitativa", prop.getProperty("UNITA_ABITATIVA"));
                logger.write("Selezione 'Unita' abitativa' - Completed");
                uso.clickComponent(gestione.selezionaUnitaAbitativaConferma);
                uso.checkSpinnersSFDC();
                TimeUnit.SECONDS.sleep(3);
            }

            //BOX Contatti e Consensi
            offer.scrollComponent(offer.buttonConfermaContattiConsensi);
            TimeUnit.SECONDS.sleep(3);
            if (prop.getProperty("CLIENTE_BUSINESS").contentEquals("Y")) {
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Profilazione");
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Telefonico");
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Cellulare");
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Email");
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Telefonico");
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Cellulare");
//                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Email");
            } else {
                logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Start");
                offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
                offer.scrollComponent(offer.sezioneContattiEConsensi);
                offer.clickComponent(offer.checkBoxSiContattiConsensi);
                TimeUnit.SECONDS.sleep(1);
                logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Completed");
            }
            offer.clickComponent(offer.buttonConfermaContattiConsensi);
            TimeUnit.SECONDS.sleep(5);

            //BOX Modalità firma e Canale Invio
//            if (prop.getProperty("CLIENTE_BUSINESS").contentEquals("Y")) {
//                offer.popolareCampiModFirma("NO VOCAL", "STAMPA LOCALE");
//            } else {
//                offer.popolareCampiModFirma("NO VOCAL", "POSTA");
//            }
            // Per lo scenario DP_15 mettere Canale POSTA
            offer.popolareCampiModFirma("NO VOCAL", "POSTA");
            offer.clickComponent(offer.buttonConfermaModFirmaCanale);
            TimeUnit.SECONDS.sleep(5);
            
            // Conferma Fatturazione Elettronica
            offer.clickComponent(offer.buttonConfermaFatElettronica);
            TimeUnit.SECONDS.sleep(5);
            

            if (prop.getProperty("CLIENTE_BUSINESS").contentEquals("Y")) {
                if (prop.getProperty("MERCATO").contentEquals("Libero")) {
                    offer.clickComponent(offer.buttonConfermaCvp);
                    gestione.checkSpinnersSFDC();
                    offer.verificaCvp();
                }
                offer.verifyComponentExistence(offer.sezioniConfermate);
                offer.clickComponent(offer.buttonConferma);
                gestione.checkSpinnersSFDC();
            } else {
                offer.clickComponent(offer.buttonConfermaCvp);
                gestione.checkSpinnersSFDC();
                offer.verificaCvp();
                offer.verifyComponentExistence(offer.sezioniConfermate);
                offer.clickComponent(offer.buttonConferma);
                gestione.checkSpinnersSFDC();
            }

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
