package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CreaOffertaSubentroId11_Finalizzazione {

    public static void main(String[] args) throws Exception {
        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);

            offer.scrollComponent(offer.buttonConfermaContattiConsensi);

            TimeUnit.SECONDS.sleep(3);
            if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y")) {
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Profilazione");
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Telefonico");
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Cellulare");
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Email");
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Telefonico");
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Cellulare");
                offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Email");
            } else {
                logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Start");
                offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
                offer.scrollComponent(offer.sezioneContattiEConsensi);
                offer.clickComponent(offer.checkBoxSiContattiConsensi);
                TimeUnit.SECONDS.sleep(1);
                logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Completed");
            }

            offer.clickComponent(offer.buttonConfermaContattiConsensi);
            gestione.checkSpinnersSFDC();
            TimeUnit.SECONDS.sleep(3);

            if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y")) {
                offer.popolareCampiModFirma("NO VOCAL", "STAMPA LOCALE");
            } else {
                offer.popolareCampiModFirma("NO VOCAL", "POSTA");
            }
            TimeUnit.SECONDS.sleep(2);
            offer.clickComponent(offer.buttonConfermaModFirmaCanale);
            gestione.checkSpinnersSFDC();
            TimeUnit.SECONDS.sleep(3);
            if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y")) {
                if (prop.getProperty("MERCATO", "").contentEquals("Libero")) {
                    offer.clickComponent(offer.buttonConfermaCvp);
                    gestione.checkSpinnersSFDC();
                    offer.verificaCvp();
                }
                offer.verifyComponentExistence(offer.sezioniConfermate);
                offer.clickComponent(offer.buttonConferma);
                gestione.checkSpinnersSFDC();
            } else {
                offer.clickComponent(offer.buttonConfermaCvp);
                gestione.checkSpinnersSFDC();
                offer.verificaCvp();
                offer.verifyComponentExistence(offer.sezioniConfermate);
                offer.clickComponent(offer.buttonConferma);
                gestione.checkSpinnersSFDC();
            }

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
