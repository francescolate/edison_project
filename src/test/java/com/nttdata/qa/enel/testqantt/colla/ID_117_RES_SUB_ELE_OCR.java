package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertagasocrComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_117_RES_SUB_ELE_OCR {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		

		try {
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			prop.setProperty("LUCEEGASLINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			OffertagasocrComponent oc =new OffertagasocrComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			PublicAreaComponent pa = new PublicAreaComponent(driver);
			Thread.sleep(5000);
			logger.write("select drop down value - Start");
			pa.changeDropDownValue(pa.productDropDown, pa.gas);
			pa.changeDropDownValue(pa.placeDropDown, pa.casa);
			pa.changeDropDownValue(pa.myselectionDD, pa. visualiza);
					logger.write("Click on inizia ora - Start");
			pa.clickComponent(pa.iniziaOra);
			Thread.sleep(3000);
			logger.write("Click on inizia ora - Completed");
			pa.checkURLAfterRedirection(pa.detaglioOffertgasaUrl);
			pa.clickComponent(pa.dettaglioOfferta);
			
			oc.clickComponent(oc.attivaSubito);
			logger.write("Verify page navigation to attiva Offerta page and page details -- start");
			driver.navigate().to(oc.offertaUrlgas);
						
			oc.verifyComponentExistence(oc.aderisci);
			oc.checkURLAfterRedirection(oc.offertaUrlgas);
						
			oc.verifyComponentExistence(oc.compilaManualmente);
			oc.clickComponent(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Start");
							
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(2000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(25000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");
			oc.clickComponent(oc.subentro);
			oc.comprareText(oc.subentrotext1, oc.SUBENTROTEXT1, true);
			oc.comprareText(oc.subentrotext2, oc.SUBENTROTEXT2, true);
			logger.write("select the values in SUBENTRO and validate the information is displayed correctly-- Start");
			oc.verifyComponentExistence(oc.pdr);
			oc.verifyComponentExistence(oc.iban);
			oc.verifyComponentExistence(oc.Datidellimmobile);
			oc.verifyComponentExistence(oc.Cartadidentità);
			oc.enterInputParameters(oc.pdr, prop.getProperty("PDR"));
			oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
			oc.clickComponent(oc.popup);
					
			oc.verifyComponentExistence(oc.popupcosetext);
			oc.comprareText(oc.pdrtext, oc.PDRTEXT, true);
			oc.comprareText(oc.popuptext1, oc.POPUPTEXT1, true);
			oc.comprareText(oc.popuptext2, oc.POPUPTEXT2, true);
			oc.comprareText(oc.popuptext3, oc.POPUPTEXT3, true);
			oc.comprareText(oc.popuptext3, oc.POPUPTEXT4, true);
			oc.clickComponent(oc.popupclose);
			oc.verifyComponentExistence(oc.Datidellimmobile);
					
			oc.comprareText(oc.pdrtext, oc.PDRTEXT, true);
			oc.comprareText(oc.popupclosetext1, oc.POPUPCLOSETEXT1, true);
			oc.comprareText(oc.popupclosetext2, oc.POPUPCLOSETEXT2, true);
			oc.comprareText(oc.popupclosetext3, oc.POPUPCLOSETEXT3, true);
			oc.comprareText(oc.popupclosetext4, oc.POPUPCLOSETEXT4, true);
			oc.comprareText(oc.popupclosetext5, oc.POPUPCLOSETEXT5, true);
			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);
			oc.enterInputParameters(oc.pdr, prop.getProperty("PDR"));
		    oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
		    oc.clickComponent(oc.capdropdown);
		    oc.clickComponent(oc.popupclose);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.clickComponent(oc.prosegui1);
		
			oc.verifyComponentExistence(oc.citta);
			oc.verifyComponentExistence(oc.indrizzo);
			oc.verifyComponentExistence(oc.numeroCivico);
			oc.verifyComponentExistence(oc.indrizzoDiFornituraCAP);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText, oc.IndrizzoDiFornituraPrivacyText, true);
			oc.verifyComponentExistence(oc.Fornitore);
		 
			Thread.sleep(5000);
			
			oc.verifyComponentExistence(oc.recapiti);

			oc.verifyComponentExistence(oc.recapitians1);
			oc.verifyComponentExistence(oc.recapitians2);
			oc.comprareText(oc.recapitiqst2, oc.Recapitiqst2, true);
			oc.comprareText(oc.recapitiansOne, oc.RecapitiansOne, true);
			oc.comprareText(oc.recapitiansTwo, oc.RecapitiansTwo, true);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.verifyComponentExistence(oc.salvaEContinuaDopo);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText2, oc.IndrizzoDiFornituraPrivacyText2, true);
			oc.enterInputParameters(oc.citta, prop.getProperty("CITTA"));
			oc.enterInputParameters(oc.indrizzo, prop.getProperty("INDRIZZO"));
			oc.enterInputParameters(oc.numeroCivico, prop.getProperty("NUMEROCIVICO"));
			oc.clickComponent(oc.indrizzo);
			Thread.sleep(3000);
			oc.verifyComponentExistence(oc.insertManually);
			oc.clickComponent(oc.insertManually);
			Thread.sleep(3000);
			oc.enterInputParameters(oc.indrizzoDiFornituraCAP, prop.getProperty("CAP"));
			oc.clickComponent(oc.indrizzoDiFornituraCapDropdown);
			oc.verifyComponentExistence(oc.Codici_Promozionali_Heading);
			oc.verifyComponentExistence(oc.Codici_Promozionali_Subtext);
			oc.enterInputParameters(oc.Codici_Promozionalitest, prop.getProperty("Codici_Promozionali"));
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(5000);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Completed");
			oc.clickComponent(oc.CONTINUA);
			oc.verifyComponentExistence(oc.Utilizzogas);
			oc.verifyComponentExistence(oc.Cottura);
			oc.verifyComponentExistence(oc.AcquaCaldaeRiscaldamento);
			oc.verifyComponentExistence(oc.Tipologiaabitazion);
			oc.verifyComponentExistence(oc.Appartamento);
			oc.verifyComponentExistence(oc.Numeropersone);
			logger.write("Verify and Enter the values in informazioniFornitura section-- Completed");
			oc.verifyComponentExistence(oc.Metodo_di_Pagamento_Field);
			oc.verifyDefaultValue(oc.Metodo_di_Pagamento_Bolletino, oc.Bolletino_selection);
			oc.verifyComponentExistence(oc.Modalità_di_ricezione_Heading);
			oc.checkPrePopulatedValueAndCompare(oc.ModalitaricezioneEmailbollete, prop.getProperty("EMAIL"));
			oc.checkPrePopulatedValueAndCompare(oc.ModalitaricezioneTelefonobollete, prop.getProperty("CELLULARE"));
			oc.verifyComponentExistence(oc.Consensi_Heading);
			oc.comprareText(oc.Consensi_chkbx1_Text, oc.Consensi_chkbx1Text, true);
			oc.comprareText(oc.Consensi_chkbx2_Text, oc.chkbx2Text, true);
			oc.comprareText(oc.Mandati_e_Consensi_chkbx3_Text, oc.Consensi_chkbx3Text, true);
			oc.clickComponent(oc.Consensi_chkbx1_Text);
			oc.clickComponent(oc.Consensi_chkbx2_Text);
			oc.clickComponent(oc.Mandati_e_Consensi_chkbx3_Text);
			oc.verifyComponentExistence(oc.Richiesta_Heading);
			oc.verifyComponentExistence(oc.Richiesta_LinkText);
			oc.comprareText(oc.Richiesta_TextInBox, oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text, true);			
			oc.verifyComponentExistence(oc.Richiesta_SI);
			oc.verifyComponentExistence(oc.Richiesta_Di_NO);
			oc.clickComponent(oc.Richiesta_LinkText);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_POPUP_close);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Marketing_Enel_Energia_TextInBox, oc.Consenso_Marketing_Enel_Energia_Text, true);
			oc.verifyComponentExistence(oc.Enel_Energia_Accetto);
			oc.verifyComponentExistence(oc.Enel_Energia_NonAccetto);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_Heading);
			oc.comprareText(oc.Consenso_Marketing_Terzi_TextInBox, oc.Consenso_Marketing_Terzi_Text, true);
			oc.verifyComponentExistence(oc.Terzi_Accetto);
			oc.verifyComponentExistence(oc.Terzi_NonAccetto);
			oc.verifyComponentExistence(oc.Consenso_Profilazione_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Profilazione_Enel_Energia_TextInBox, oc.Consenso_Profilazione_Enel_Energia_Text, true);
			oc.verifyComponentExistence(oc.Terzi_NonAccetto);
			oc.clickComponent(oc.Enel_Energia_Accetto);
			oc.clickComponent(oc.Terzi_Accetto);
			oc.clickComponent(oc.Richiesta_Di_NO);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupContent, oc.Richiesta_Di_Esecuzione_POPUP_Content, true);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupClose);
			oc.clickComponent(oc.COMPLETA_ADESIONE_Button);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Start");
			Thread.sleep(10000);
			oc.comprareText(oc.tornaAllaHomePageTitle, oc.TornaAllaHomePageTitle, true);
			oc.provideEmail(prop.getProperty("EMAIL"));
			oc.clickComponent(oc.TornaAllaHomeButton);
			Thread.sleep(5000);
			oc.checkURLAfterRedirection(prop.getProperty("WP_LINK"));
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
