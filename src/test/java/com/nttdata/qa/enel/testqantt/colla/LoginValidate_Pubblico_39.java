package com.nttdata.qa.enel.testqantt.colla;
import java.awt.Color;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LoginValidate_Pubblico_39 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			LoginPageValidateComponent loginPage = new LoginPageValidateComponent(driver);
			
			logger.write("Accessing login page - Start");
			loginPage.launchLinkBasicAuth(prop.getProperty("LINK"));
			loginPage.clickComponent(loginPage.homePageClose);
			loginPage.verifyComponentExistence(loginPage.logoEnel);
		
			
			loginPage.verifyComponentExistence(loginPage.buttonAccetta);
			loginPage.clickComponent(loginPage.buttonAccetta);
			
			Thread.sleep(5000);
			loginPage.verifyComponentExistence(loginPage.iconUser);
			loginPage.clickComponent(loginPage.iconUser);
			logger.write("Accessing login page - Complete");
			Thread.sleep(5000);

			logger.write("Verifying username and password fields - Start");
			loginPage.verifyComponentVisibility(loginPage.username);
			loginPage.verifyComponentVisibility(loginPage.password);
			logger.write("Verifying username and password fields - Completed");
			
			logger.write("Verifying access button - Start");
			loginPage.verifyComponentVisibility(loginPage.buttonLoginAccedi);
			logger.write("Verifying access button - Completed");
			
			logger.write("Verifying recovery password and username links - Start");
			loginPage.verifyComponentVisibility(loginPage.recoverPasswordBtn);
			loginPage.verifyComponentVisibility(loginPage.recoverUsernameBtn);
			logger.write("Verifying recovery password and username links - Completed");
			
			logger.write("Verifying access problems link - Start");
			loginPage.verifyComponentVisibility(loginPage.accessProblemsLabel);
			loginPage.verifyComponentText(loginPage.accessProblemsLabel, prop.getProperty("ACCESS_PROBLEMS_STRING"));
			loginPage.verifyComponentVisibility(loginPage.accessProblemsBtn);
			
			logger.write("Verifying social access buttons - Start");
			loginPage.verifyComponentVisibility(loginPage.googleAccessBtn);
			loginPage.verifyComponentVisibility(loginPage.facebookAccessBtn);
			logger.write("Verifying social access buttons - Completed");
			
			logger.write("Verifying registration elements - Start");
			loginPage.verifyComponentVisibility(loginPage.noAccountLabel);
			loginPage.verifyComponentVisibility(loginPage.registerBtn);
			//loginPage.verifyComponentVisibility(loginPage.accessProblemsLink);
			logger.write("Verifying registration elements - Completed");
			
			logger.write("Enter Accedi without entering value in username and password  - Start");
			loginPage.clickComponent(loginPage.buttonLoginAccedi);
			loginPage.verifyComponentExistence(loginPage.usernameValidate);
			loginPage.verifyComponentText(loginPage.usernameValidate, prop.getProperty("USERNAME_VALIDATE"));
			loginPage.verifyComponentExistence(loginPage.passwordValidate);
			loginPage.verifyComponentText(loginPage.passwordValidate, prop.getProperty("PASSWD_VALIDATE"));
			logger.write("Enter Accedi without entering value in username and password  - Complete");
			
			logger.write("Enter invalid user name  - Start");
			loginPage.insertText(loginPage.username, prop.getProperty("INVALIDUSER"));
			//loginPage.enterInput(loginPage.password, prop.getProperty("PASSWORD"));
			loginPage.clickComponent(loginPage.buttonLoginAccedi);
			loginPage.verifyComponentExistence(loginPage.usernameInvalid);
			loginPage.verifyComponentText(loginPage.usernameInvalid, prop.getProperty("USERNAME_INVALID"));
			logger.write("Enter invalid user name - Completed");
			
			logger.write("Enter invalid password   - Start");
			loginPage.insertText(loginPage.password, prop.getProperty("INVALIDPASSWD"));
			loginPage.insertText(loginPage.username, prop.getProperty("USERNAME"));
			loginPage.verifyComponentExistence(loginPage.buttonLoginAccedi);
			loginPage.clickComponent(loginPage.buttonLoginAccedi);
			Thread.sleep(5000);
			loginPage.compareText(loginPage.loginError, LoginPageValidateComponent.INVALID, true);
			logger.write("Enter invalid password - Completed");
			
			logger.write("Enter valid username and password   - Start");
			loginPage.insertText(loginPage.username, prop.getProperty("USERNAME"));
			loginPage.insertText(loginPage.password, prop.getProperty("PASSWORD"));
			loginPage.clickComponent(loginPage.buttonLoginAccedi);
			logger.write("Enter valid username and password - Completed");
			
			Thread.sleep(2000);
			if (util.verifyExistence(By.xpath("//a[@href='/it/area-clienti/residenziale']"), 60))
				loginPage.clickComponent(By.xpath("//a[@href='/it/area-clienti/residenziale']"));
			Thread.sleep(2000);

			logger.write("Verify the header and title in the home page   - Start");
			loginPage.compareText(loginPage.header1, LoginPageValidateComponent.HOMEPAGE_HEADER1, true);
			loginPage.compareText(loginPage.header2,LoginPageValidateComponent.HOMEPAGE_HEADER2, true);
			loginPage.compareText(loginPage.title1, LoginPageValidateComponent.HOMEPAGE_TITLE1, true);
			loginPage.compareText(loginPage.title2, LoginPageValidateComponent.HOMEPAGE_TITLE2, true);
			logger.write("Verify the header and title in the home page   - Complete");
			
			logger.write("Click on logout link   - Start");
			loginPage.verifyComponentExistence(loginPage.logoutLink);
			loginPage.clickComponent(loginPage.logoutLink);
			loginPage.verifyComponentExistence(loginPage.esci);
			loginPage.clickComponent(loginPage.esci);
			logger.write("Click on logout link   - Complete");
			Thread.sleep(5000);
			
			logger.write("Verify the url and title   - Start");
			loginPage.verifyLandingPage(driver, prop.getProperty("URL"), prop.getProperty("HOMETITLE"));
			logger.write("Verify the url and title   - Complete");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
