package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class GetBSNInformation {

	@Step("Recupero Pod da Workbench")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			wbc.launchLink(prop.getProperty("WB_LINK"));
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			wbc.insertQuery(Costanti.get_business_account_info.replace("@ACCOUNTID@", prop.getProperty("ACCOUNT_ID")));
			wbc.pressButton(wbc.submitQuery);
			wbc.aspettaRisultati(60);
			List<String> res = wbc.recuperaRisultatiLista(1);
			prop.setProperty("NOME", res.get(0));
			prop.setProperty("EMAIL", res.get(1));
			prop.setProperty("PEC", res.get(2));
			prop.setProperty("PHONE", res.get(3));
			prop.setProperty("CF", res.get(4));
			prop.store(new FileOutputStream(args[0]), null);			
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
