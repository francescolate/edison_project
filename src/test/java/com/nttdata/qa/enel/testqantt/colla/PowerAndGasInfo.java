package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BusinessComponent;
import com.nttdata.qa.enel.components.colla.FooterPageEngComponent;
import com.nttdata.qa.enel.components.colla.HeaderEngComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PowerAndGasComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID80MotorinoRicercaComponent;
import com.nttdata.qa.enel.components.colla.SearchEngComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;



public class PowerAndGasInfo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String url;
		WebDriverWait wait;

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			wait = new WebDriverWait(driver, 10);
			Thread.sleep(5000);
			PowerAndGasComponent pg = new PowerAndGasComponent(driver);
			HeaderEngComponent hec = new HeaderEngComponent(driver);
			FooterPageEngComponent fpec = new FooterPageEngComponent(driver);
			PubblicoID80MotorinoRicercaComponent mrc = new PubblicoID80MotorinoRicercaComponent(driver);
			System.out.println("START");
			Thread.sleep(5000);
					
			
			logger.write("Click See All Power - START");
			
			pg.clickComponent(pg.seeAllPower);
			
			Thread.sleep(5000);
			
			logger.write("Verify page See All Power - START");
			
			pg.checkURLAfterRedirection(prop.get("LINK_POWER").toString());
			
			hec.verifyHeaderItems();
			
			Thread.sleep(10000);
			prop.setProperty("QUERY1", "What kind of contract?");
			prop.setProperty("QUERY2", "Where?");
			prop.setProperty("QUERY3", "What for?");
			
			mrc.verifyComponentExistence(mrc.WhatkindOfContract_Query);
			mrc.comprareText(mrc.WhatkindOfContract_Query, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.Where_Query);
			mrc.comprareText(mrc.Where_Query, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.WhatFor_Query);
			mrc.comprareText(mrc.WhatFor_Query, prop.getProperty("QUERY3"), true);
			
			logger.write("Verify page See All Power - END");
			
			logger.write("Click See All Power - END");
			
			driver.navigate().back();
			
			Thread.sleep(5000);
			
			pg.checkURLAfterRedirection(prop.get("LINK_BACK").toString());
			
			hec.verifyHeaderItems();
				
			url = driver.getCurrentUrl();
			
			mrc.checkURLAfterRedirection(prop.getProperty("LINK"));
			
			Thread.sleep(10000);
			prop.setProperty("QUERY1", "What kind of contract?");
			prop.setProperty("QUERY2", "Where?");
			prop.setProperty("QUERY3", "What for?");
			
			mrc.verifyComponentExistence(mrc.WhatkindOfContract_Query);
			mrc.comprareText(mrc.WhatkindOfContract_Query, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.Where_Query);
			mrc.comprareText(mrc.Where_Query, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.WhatFor_Query);
			mrc.comprareText(mrc.WhatFor_Query, prop.getProperty("QUERY3"), true);
			
			logger.write("Click See All Gas - START");
			
			pg.clickComponent(pg.seeAllGas);
			
			Thread.sleep(5000);
			
			pg.checkURLAfterRedirection(prop.get("LINK_GAS").toString());
			
			hec.verifyHeaderItems();
			
			Thread.sleep(10000);
			prop.setProperty("QUERY1", "What kind of contract?");
			prop.setProperty("QUERY2", "Where?");
			prop.setProperty("QUERY3", "What for?");
			
			mrc.verifyComponentExistence(mrc.WhatkindOfContract_Query);
			mrc.comprareText(mrc.WhatkindOfContract_Query, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.Where_Query);
			mrc.comprareText(mrc.Where_Query, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.WhatFor_Query);
			mrc.comprareText(mrc.WhatFor_Query, prop.getProperty("QUERY3"), true);
			
			logger.write("Click See All Gas - END");
			
			driver.navigate().back();
			
			Thread.sleep(5000);
			
			pg.checkURLAfterRedirection(prop.get("LINK_BACK").toString());
			
			hec.verifyHeaderItems();
			
			url = driver.getCurrentUrl();
			
			Thread.sleep(10000);
			prop.setProperty("QUERY1", "What kind of contract?");
			prop.setProperty("QUERY2", "Where?");
			prop.setProperty("QUERY3", "What for?");
			
			mrc.verifyComponentExistence(mrc.WhatkindOfContract_Query);
			mrc.comprareText(mrc.WhatkindOfContract_Query, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.Where_Query);
			mrc.comprareText(mrc.Where_Query, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.WhatFor_Query);
			mrc.comprareText(mrc.WhatFor_Query, prop.getProperty("QUERY3"), true);
			
			logger.write("Click Contract - START");
			
			pg.clickComponent(pg.contactConsultant);
			
			Thread.sleep(5000);
			
			pg.checkURLAfterRedirection(prop.get("LINK_CONTACT_CONSULTANT").toString());
			
			hec.verifyHeaderItems();
			
			fpec.verifyFooterItemsExistence();
			
			logger.write("Click Contract - END");
			
			driver.navigate().back();
			
			Thread.sleep(5000);
			
			pg.checkURLAfterRedirection(prop.get("LINK_BACK").toString());
			
			hec.verifyHeaderItems();
			
			url = driver.getCurrentUrl();
			
			Thread.sleep(10000);
			prop.setProperty("QUERY1", "What kind of contract?");
			prop.setProperty("QUERY2", "Where?");
			prop.setProperty("QUERY3", "What for?");
			
						
			mrc.verifyComponentExistence(mrc.WhatkindOfContract_Query);
			mrc.comprareText(mrc.WhatkindOfContract_Query, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.Where_Query);
			mrc.comprareText(mrc.Where_Query, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.WhatFor_Query);
			mrc.comprareText(mrc.WhatFor_Query, prop.getProperty("QUERY3"), true);
			
			logger.write("Click Search - START");
			
			hec.verifyHeaderItems();
			
			fpec.verifyFooterItemsExistence();
			
			pg.clickComponent(pg.search);
			
			SearchEngComponent sec = new SearchEngComponent(driver);
			
			sec.verifyMenuItems();
			
			Thread.sleep(5000);
			
			logger.write("Click Search - END");
			
			logger.write("Click Solution - START");
			
			sec.clickComponent(sec.solution);
			
			sec.checkURLAfterRedirection(prop.get("LINK_SOLUTION").toString());
			
			BusinessComponent bc = new BusinessComponent(driver);
			
			bc.verifyMenuItems();
			
			logger.write("Click Solution - END");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");
	
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
	
	//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
}
