package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class InserimentoFornitureEleGasPrimaAttivazione {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CompilaIndirizziComponent pod = new CompilaIndirizziComponent(driver);
			
			SeleniumUtilities util = new SeleniumUtilities(driver);
			PrecheckComponent forniture = new PrecheckComponent(driver);
			String statoUbiest=Costanti.statusUbiest;
			logger.write("inserire una prima fornitura di tipo ELE con cap e premere su 'Esegui precheck' - Start");
			forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD_ELE"));
			forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
			forniture.pressAndCheckSpinners(forniture.precheckButton2);
			logger.write("inserire una prima fornitura di tipo ELE con cap e premere su 'Esegui precheck' - Completed");
			
			//Compare in caso di CAP comune in più zone, seleziona il primo. Gestito solo nel caso in cui è presente. 
			forniture.selezionaIstat(forniture.popUpIstat, forniture.primoIstat, forniture.confermaIstat);
			logger.write("Inserimento e Validazione Indirizzo Esecuzione Lavori - Completed");
			Thread.currentThread().sleep(5000);
			
			if(prop.getProperty("VERIFICA_ESITO_OFFERTABILITA").contentEquals("Y")) {
			logger.write("Verifica esito Offertabilita - Start");
			forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
			logger.write("Verifica esito Offertabilita - Completed");
			}
			
			
			logger.write("Inserimento e Validazione Indirizzo POD ELE - Start");
			if(statoUbiest.compareTo("ON")==0){
				logger.write("Inserimento e Validazione Indirizzo - Start");
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				logger.write("Inserimento e Validazione Indirizzo POD ELE- Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			else if(statoUbiest.compareTo("OFF")==0){
				logger.write("Inserimento Indirizzo Forzato POD ELE- Start");
				pod.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				pod.forzaIndirizzoPrimaAttivazione(prop.getProperty("CAP_FORZATURA"),prop.getProperty("CITTA_FORZATURA"));
				logger.write("Inserimento Indirizzo Forzato  POD ELE- Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			
            
			logger.write("Inserimento dati misuratore - Start");
			GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
					driver);
			
			compila.compileVoltageInfoEVO(prop.getProperty("TIPO_MISURATORE"), prop.getProperty("TENSIONE_CONSEGNA"), prop.getProperty("POTENZA_CONTRATTUALE"));
			
			logger.write("Inserimento dati misuratore - Completed");
			
			logger.write("click su pulsante AGGIUNGI FORNITURA - Start");
			
			compila.clickComponentWithJseAndCheckSpinners(compila.button_aggiungi_fornitura);
			logger.write("click su pulsante AGGIUNGI FORNITURA - Completed");
			
			
			logger.write("inserire una seconda fornitura di tipo GAS con cap e premere su 'Esegui precheck' - Start");
			forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD_GAS"));
			forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
			forniture.pressAndCheckSpinners(forniture.precheckButton2);
			logger.write("inserire una seconda fornitura di tipo GAS con cap e premere su 'Esegui precheck' - Completed");
			TimeUnit.SECONDS.sleep(5);
			
			if(statoUbiest.compareTo("ON")==0){
				logger.write("Inserimento e Validazione Indirizzo POD GAS- Start");
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				logger.write("Inserimento e Validazione Indirizzo POD GAS- Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			else if(statoUbiest.compareTo("OFF")==0){
				logger.write("Inserimento Indirizzo Forzato POD GAS- Start");
				pod.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				pod.forzaIndirizzoPrimaAttivazione(prop.getProperty("CAP_FORZATURA"),prop.getProperty("CITTA_FORZATURA"));
				logger.write("Inserimento Indirizzo Forzato POD GAS - Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			
			
			logger.write("verifica pulsante AGGIUNGI FORNITURA disabilitato - Start");
			
			compila.verifyButtonIsNotEnabled("Aggiungi fornitura");
			
			logger.write("verifica pulsante AGGIUNGI FORNITURA disabilitato - Completed");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
	
	public static String checklistMessage="ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:  POD/PDR/numero utenza.INFORMAZIONI UTILI - Il Cambio Uso della fornitura di energia elettrica ha un costo pari a: 0,00 €.- Il Cambio Uso della fornitura di gas ha un costo pari a: 0,00 €.- Contestualmente alla variazione uso è possibile attivare/modificare/revocare i Servizi VASe il Metodo di pagamento - Per la variazione uso gas, nel caso di modifica dell'uso fornitura, è necessario eseguire il cambio prodotto.Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto."; 
}
