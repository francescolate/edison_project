package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.components.colla.ReportBillingComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_342_RBM_NonIscritto {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			pa.verifyComponentExistence(pa.reportBillingManagement);
			//pa.checkForErrorPopup();
			logger.write("Click on Report Billing Management menu item -- Start");
			pa.clickComponent(pa.reportBillingManagement);
			logger.write("Click on Report Billing Management menu item -- Completed");

			ReportBillingComponent rc =new ReportBillingComponent(driver);
			Thread.sleep(10000);
			logger.write("Verify Report Billing page title and content -- Start");
			rc.verifyComponentExistence(rc.pageTitle);
			rc.comprareText(rc.path, rc.Path, true);
			rc.comprareText(rc.checose, rc.Checose, true);
			rc.comprareText(rc.pageContent, rc.PageContent, true);
			rc.verifyComponentExistence(rc.iscrivitiBtn);
			logger.write("Verify Report Billing page title and content -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
