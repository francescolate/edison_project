package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import javassist.compiler.ast.Visitor;

public class PrivateAreaCardFibraOfferDetails {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);
			
			By card_fibra_active = paf.card_fibra_active;

			paf.verifyComponentExistence(card_fibra_active);
			paf.verifyComponentVisibility(card_fibra_active);
			
//			logger.write("check the text number in the card with FIBRA offer - START");
//			
//			By text_number = By.xpath("./div[1]/div[3]/span[4]/div[2]");
//			
//			By text_number_card_fibra = new ByChained(card_fibra_active, text_number);
//
//			paf.verifyComponentExistence(text_number_card_fibra);
//			paf.verifyComponentVisibility(text_number_card_fibra);
//			paf.verifyComponentText(text_number_card_fibra, "883569408");
			
//			logger.write("check the text number in the card with FIBRA offer - COMPLETED");
			
			//STEP 7
			logger.write("click the button for offer details in the card with FIBRA offer - START");
			
			//da risolvere
//			By button_show_details = By.xpath(".//*[contains(@class,'button-container')]//*[contains(text(),\"Dettaglio Offerta\")]"); 

			By button_show_details_card_fibra = paf.button_show_details_card_fibra;
			
			paf.verifyComponentExistence(button_show_details_card_fibra);
			paf.clickWithJS(button_show_details_card_fibra);
			
			logger.write("click the button for offer details in the card with FIBRA offer - COMPLETED");
			
			By main = paf.main;
			
			paf.verifyComponentExistence(main);
			
			logger.write("check the supply type text - START");
			
			By main_type_type = paf.main_type_type;
			
			paf.verifyComponentExistence(main_type_type);
			paf.verifyComponentVisibility(main_type_type);
			paf.verifyComponentText(main_type_type, "Fibra");
			
			logger.write("check the supply type text - COMPLETED");			
			
			logger.write("check the supply desc text - START");
			
			By main_supply_desc = paf.main_supply_desc;
			
			paf.verifyComponentTextContainsSubstrings(main_supply_desc, new String[]{"Via PRIMO SANT'ALFONSO 22 - 70056 MOLFETTA" , "474092143"});
			
			logger.write("check the supply desc text - COMPLETED");
			
			logger.write("check the supply desc status - START");
			
			By supply_desc_status = paf.supply_desc_status;
			
			paf.verifyComponentExistence(supply_desc_status);
			paf.verifyComponentVisibility(supply_desc_status);
			paf.verifyComponentText(supply_desc_status, "Attiva");
			
			logger.write("check the supply desc status - COMPLETED");
			
			logger.write("check the supply desc date - START");
			
			By supply_desc_date = paf.supply_desc_date;
			
			paf.verifyComponentExistence(supply_desc_date);
			paf.verifyComponentVisibility(supply_desc_date);
			paf.verifyComponentText(supply_desc_date, "/");
			
			logger.write("check the supply desc date - COMPLETED");
			
			logger.write("check the supply desc payment type - START");
			
			By supply_desc_payment_type = paf.supply_desc_payment_type;
			
			paf.verifyComponentExistence(supply_desc_payment_type);
			paf.verifyComponentVisibility(supply_desc_payment_type);
			paf.verifyComponentText(supply_desc_payment_type, "Bollettino Postale");
			
			logger.write("check the supply desc payment type - COMPLETED");
			
			logger.write("check the supply desc reception mode - START");
			
			By supply_desc_reception_mode = paf.supply_desc_reception_mode;
			
			paf.verifyComponentExistence(supply_desc_reception_mode);
			paf.verifyComponentVisibility(supply_desc_reception_mode);
			paf.verifyComponentText(supply_desc_reception_mode, "Cartacea");
			
			logger.write("check the supply desc reception mode - COMPLETED");
			
			//STEP 8
			logger.write("click the payment type icon - START");
			
			By supply_desc_info_icon = paf.supply_desc_info_icon;
			
			paf.verifyComponentExistence(supply_desc_info_icon);
			paf.clickComponentWithJse(supply_desc_info_icon);
			
			logger.write("click the payment type icon - COMPLETED");
			
			By button_more_information = paf.button_more_information;
			
			logger.write("click the button more information - START");
			/*
			paf.verifyComponentExistence(button_more_information);
			paf.clickComponentWithJse(button_more_information);
			
			logger.write("click the button more information - COMPLETED");
			
			logger.write("click the exit button - START");
			
			By button_esci = paf.button_esci;
			
			paf.verifyComponentExistence(button_esci);
			paf.clickComponentWithJse(button_esci);
			
			logger.write("click the exit button - COMPLETED");
			
			logger.write("click the button for showing bill in the card with FIBRA offer - START");
			
			paf.verifyComponentExistence(button_show_details_card_fibra);
			paf.clickComponent(button_show_details_card_fibra);
			
			logger.write("click the button for showing bill in the card with FIBRA offer - COMPLETED");
			
			By supply_desc_info_icon_2 = paf.supply_desc_info_icon_2;
			
			logger.write("click the reception mode icon - START");
			
			paf.verifyComponentExistence(supply_desc_info_icon_2);
			paf.clickComponentWithJse(supply_desc_info_icon_2);
			
			//STEP 9
			logger.write("click the reception mode icon - COMPLETED");
			
			logger.write("click the button more information - START");
			
			paf.verifyComponentExistence(button_more_information);
			paf.clickComponentWithJse(button_more_information);
			
			logger.write("click the button more information - COMPLETED");
			
			logger.write("click the exit button - START");
			
			paf.verifyComponentExistence(button_esci);
			paf.clickComponentWithJse(button_esci);
			
			logger.write("click the exit button - COMPLETED");
			
			logger.write("check the card with FIBRA offer state active - COMPLETED");
*/
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

			//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
