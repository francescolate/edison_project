package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Storie_FuturE_MediaSectionsComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Futur_E {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
	
			logger.write("click sul tab 'Futur-e' presente sull'header dell' Home page con link "+prop.getProperty("LINK")+" e check del link della pagina Futur-E di apertura - Start ");
			TopMenuComponent menu=new TopMenuComponent(driver);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			//menu.sceltaMenu("Futur-e");
			Thread.sleep(5000);
			log.launchCorporateLink(prop.getProperty("LINK_FUTUR_E"));
			Storie_FuturE_MediaSectionsComponent futur_e = new Storie_FuturE_MediaSectionsComponent(driver);
			futur_e.checkUrl(prop.getProperty("LINK_FUTUR_E"));
			logger.write("click sul tab 'Futur-e' presente sull'header dell' Home page con link "+prop.getProperty("LINK")+" e check del link della pagina Futur-E di apertura - Completed ");		
			
			
		/*	logger.write("sulla pagina web al link"+ prop.getProperty("LINK_FUTUR_E")+" e' presente l'header - Start ");
			By enelLogo=futur_e.logoEnel;
			futur_e.verifyComponentExistence(enelLogo);
						
			By electricityandgasStorieLabel=futur_e.luceAndGasTab;
			futur_e.verifyComponentExistence(electricityandgasStorieLabel);
			
			By companiesStorieLabel=futur_e.impreseTab;
			futur_e.verifyComponentExistence(companiesStorieLabel);
			
			By withyouStorieLabel=futur_e.insiemeateTab;
			futur_e.verifyComponentExistence(withyouStorieLabel);
		    
			By storieStorieLabel=futur_e.storieTab;
			futur_e.verifyComponentExistence(storieStorieLabel);
		    
			By futur_eStorieLabel=futur_e.futureTab;
			futur_e.verifyComponentExistence(futur_eStorieLabel);
			
			By mediaStorieLabel=futur_e.mediaTab;
			futur_e.verifyComponentExistence(mediaStorieLabel);
			
			By supportStorieLabel=futur_e.supportoTab;
			futur_e.verifyComponentExistence(supportStorieLabel);	
			
			HeaderComponent login = new HeaderComponent(driver);
			By userIcon=login.iconUser;		
			login.verifyComponentExistence(userIcon);
			By glassIcon=login.magnifyingglassIcon;
			login.verifyComponentExistence(glassIcon);
			
			HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
			By menuIcon=hamburger.iconMenu;
			hamburger.verifyComponentExistence(menuIcon);
			logger.write("sulla pagina web al link"+ prop.getProperty("LINK_FUTUR_E")+" e' presente l'header - Completed ");
			
			logger.write("check sulla presenza del testo, dei links e delle icone social sul footer della pagina web '"+ prop.getProperty("LINK_FUTUR_E")+"' - Start ");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
						
			By linkInformazioniLegali=futur_e.informazioniLegaliLink;
			futur_e.verifyComponentExistence(linkInformazioniLegali);
			futur_e.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=futur_e.creditsLink;
			futur_e.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=futur_e.privacyLink;
			futur_e.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=futur_e.cookieLink;
			futur_e.verifyComponentExistence(linkCookie);
			
			By linkRemit=futur_e.remitLink;
			futur_e.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=futur_e.twitterIcon;
			futur_e.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=futur_e.facebookIcon;
			futur_e.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=futur_e.youtubeIcon;
			futur_e.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=futur_e.linkedinIcon;
			futur_e.verifyComponentExistence(iconLinkedin);
			logger.write("check sulla presenza del testo, dei links e delle icone social sul footer della pagina web '"+ prop.getProperty("LINK_FUTUR_E")+"' - Completed ");
			
			logger.write("click sul link 'Credits' e check sul suo link - Start");
			futur_e.clickComponent(linkCredits);
			
			By creditsAccess=futur_e.creditsPage;
			futur_e.verifyComponentExistence(creditsAccess);
			futur_e.checkUrl(prop.getProperty("LINK_CREDITS"));
			logger.write("click on link 'Credits' e check sul suo link - Completed");
			
			logger.write("back alla pagina web con url "+ prop.getProperty("LINK_FUTUR_E")+" - Start");
			driver.navigate().back();
			
			menu.checkUrl(prop.getProperty("LINK_FUTUR_E"));
			futur_e.verifyComponentExistence(enelLogo);
			logger.write("back alla pagina web con url "+ prop.getProperty("LINK_FUTUR_E")+" - Completed");
			
			logger.write("click sul tab 'IMPRESE' presente nell' Header della pagina con url "+ prop.getProperty("LINK_FUTUR_E")+" e check del suo link - Start");
			 menu.sceltaMenu("IMPRESE");
				
			 menu.checkUrl(prop.getProperty("LINK_IMPRESE"));
			 By imprese_Title=futur_e.impreseTitle;
			 futur_e.verifyComponentExistence(imprese_Title);
			 
			 logger.write("click sul tab 'IMPRESE' presente nell' Header della pagina con url "+ prop.getProperty("LINK_FUTUR_E")+" e check del suo link - Completed");
			 */
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
