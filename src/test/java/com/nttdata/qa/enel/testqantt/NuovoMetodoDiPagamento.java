package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PopupNuovoMetodoPagamentoComponent;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class NuovoMetodoDiPagamento {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			PopupNuovoMetodoPagamentoComponent popup=new PopupNuovoMetodoPagamentoComponent(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			logger.write("'inserimento dei dati di riferimento per il nuovo metodo  di pagamento - Start");
			if(prop.getProperty("NOME_COGNOME_IBAN","").equals(""))
			{
				
				String nome=prop.getProperty("NOME_COGNOME_IBAN");
				String ragioneSociale=prop.getProperty("RAGIONE_SOCIALE_IBAN");
				String cf=prop.getProperty("CODICE_FISCALE_IBAN");
				
				popup.popolaNomeCognome(nome);
				popup.popolareCampi(popup.ragioneSociale, ragioneSociale);
				popup.popolareCampi(popup.campoCF, cf);
			}
			else
			{
				
			
				if (prop.getProperty("NOME")!="") {
					popup.popolaNomeCognome(prop.getProperty("NOME"));	
				} 			
				else
				{ popup.popolareCampi(popup.ragioneSociale, "Data");
				} 
				popup.popolareCampi(popup.campoCF, prop.getProperty("CODICE_FISCALE"));
			    //popup.popolareCampi(popup.campoIban, prop.getProperty("IBAN"));		     
			}
			
			//Popolamento campo IBAN
			popup.popolareCampoIbanRandomico(popup.campoIban);
		    logger.write("'inserimento dei dati di riferimento per il nuovo metodo  di pagamento - Completed");
		    logger.write("click su pulsante 'Salva' e il sistema ritorna sulla pagina di selezione dei metodi di pagamento - Start");
		    	
		    //Click BOttone Salva
		    popup.clickComponent(popup.buttonSalva);
			
			gestione.checkSpinnersSFDC();
			
			
			
			offer.verifyComponentExistence(offer.sezioneMetodoPagamento);
			logger.write("click su pulsante 'Salva' e il sistema ritorna sulla pagina di selezione dei metodi di pagamento - Completed");
			logger.write("click sul pulsante 'Conferma' - Start");
			//offer.clickComponent(offer.primoMetodoPagamento);
			offer.clickComponent(offer.buttonConfermaMetodoPagamento);
			gestione.checkSpinnersSFDC();
			logger.write("click sul pulsante 'Conferma' - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
