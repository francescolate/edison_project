package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.colla.ModificaOpzioniBollettaDiSintesiComponent;

public class ModificaOpzioniBollettaDiSintesiDettaglioInterruzioneDelProcesso {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModificaOpzioniBollettaDiSintesiComponent mocc = new ModificaOpzioniBollettaDiSintesiComponent(driver);
			
			//Click homepage button
			By button_homepage = mocc.button_homepage;
			mocc.verifyComponentExistence(button_homepage);
			mocc.clickComponentWithJse(button_homepage);
			
			//Check popup
			
			By popup_title = mocc.popup_title;
			By popup_desc = mocc.popup_desc;
			
			mocc.verifyComponentText(popup_title, "Attenzione");
			mocc.verifyComponentText(popup_desc, "Se uscirai dal processo perderai le modifiche effettuate.");
			
			By button_popup_torna = mocc.button_popup_torna;
			mocc.verifyComponentExistence(button_popup_torna);
			
			//Click esci
			By button_popup_esci = mocc.button_popup_esci;
			mocc.verifyComponentExistence(button_popup_esci);
		//	mocc.clickComponentWithJse(button_popup_esci);	
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
