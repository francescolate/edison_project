package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.CheckVariazioneAnagraficaComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class Identificazione_Interlocutore_ID14 {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		
			IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
		
			try {
			logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
		
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
			
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
			
			logger.write("Conferma Identificazione - Start");
//			identificaInterlocutore.insertDocumento("b");
//			identificaInterlocutore.press(identificaInterlocutore.confirmButton);
			
		    	
			identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
			logger.write("Conferma Identificazione - Completed");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
