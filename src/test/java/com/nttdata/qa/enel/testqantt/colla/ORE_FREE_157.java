package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFree157Component;
import com.nttdata.qa.enel.components.colla.OreFreecardComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ORE_FREE_157 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreecardComponent ofcc = new OreFreecardComponent(driver);
			OreFree157Component ofc = new OreFree157Component(driver);
			
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Start");
//			ofcc.verifyComponentExistence(ofcc.datiAggiornati);
			ofcc.verifyComponentExistence(ofcc.homePageHeading1);
			ofcc.comprareText(ofcc.homePageHeading1, OreFreecardComponent.HOMEPAGE_HEADING1, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle);
			ofcc.comprareText(ofcc.homePageTitle, OreFreecardComponent.HOMEPAGE_TITLE, true);
			ofcc.verifyComponentExistence(ofcc.homePageHeading2);
			ofcc.comprareText(ofcc.homePageHeading2, OreFreecardComponent.HOMEPAGE_HEADING2, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle2);
			ofcc.comprareText(ofcc.homePageTitle2, OreFreecardComponent.HOMEPAGE_TITLE2, true);
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Ends");
			logger.write("Click on Gestisci Le Ore Free button- Start");
			ofcc.verifyComponentExistence(ofcc.gestisciOreFreeButton);
			ofcc.clickComponent(ofcc.gestisciOreFreeButton);
			Thread.sleep(5000);
			logger.write("Click on Gestisci Le Ore Free button- Ends"); 
			
			
			logger.write("Verify the Correct display of Frequently asked Questions - Start");
			ofc.verifyComponentExistence(ofc.haiBisognoDiAiutoHeading);
			ofc.comprareText(ofc.haiBisognoDiAiutoHeading, OreFree157Component.HAIBISOGNODIAIUTO_HEADING, true);
			ofc.clickComponent(ofc.haiBisognoDiAiutoLink); 
			Thread.sleep(10000);
			ofc.verifyComponentExistence(ofc.domandeFrequentiHeading);
			ofc.comprareText(ofc.domandeFrequentiHeading, OreFree157Component.Domandefrequenti_Heading, true);
			logger.write("Verify the Correct display of Frequently asked Questions - Ends"); 
			
			logger.write("Verify the Correct display of Question 1 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionOne);
			ofc.comprareText(ofc.questionOne, OreFree157Component.QUESTION_ONE, true);
			ofc.clickComponent(ofc.plusOne);
			ofc.verifyComponentExistence(ofc.answerOne);
			ofc.comprareText(ofc.answerOne, OreFree157Component.ANSWER_ONE, true);
			logger.write("Verify the Correct display of Question 1 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 2 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionTwo);
			ofc.comprareText(ofc.questionTwo, OreFree157Component.QUESTION_TWO, true);
			ofc.clickComponent(ofc.plusTwo);
			ofc.verifyComponentExistence(ofc.answerTwo);
			ofc.comprareText(ofc.answerTwo, OreFree157Component.ANSWER_TWO, true);
			logger.write("Verify the Correct display of Question 2 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 3 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionThree);
			ofc.comprareText(ofc.questionThree, OreFree157Component.QUESTION_THREE, true);
			ofc.clickComponent(ofc.plusThree);
			ofc.verifyComponentExistence(ofc.answerThree);
			ofc.comprareText(ofc.answerThree, OreFree157Component.ANSWER_THREE, true);
			logger.write("Verify the Correct display of Question 3 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 4 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionFour);
			ofc.comprareText(ofc.questionFour, OreFree157Component.QUESTION_FOUR, true);
			ofc.clickComponent(ofc.plusFour);
			ofc.verifyComponentExistence(ofc.answerFour); 
			ofc.comprareText(ofc.answerFour, OreFree157Component.ANSWER_FOUR, true);
			logger.write("Verify the Correct display of Question4 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 5 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionFive);
			ofc.comprareText(ofc.questionFive, OreFree157Component.QUESTION_FIVE, true);
			ofc.clickComponent(ofc.plusFive);
			ofc.verifyComponentExistence(ofc.answerFive); 
			ofc.comprareText(ofc.answerFive, OreFree157Component.ANSWER_FIVE, true);
			logger.write("Verify the Correct display of Question 5 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 6 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionSix);
			ofc.comprareText(ofc.questionSix, OreFree157Component.QUESTION_SIX, true);
			ofc.clickComponent(ofc.plusSix);
			ofc.verifyComponentExistence(ofc.answerSix); 
			ofc.comprareText(ofc.answerSix, OreFree157Component.ANSWER_SIX, true);
			logger.write("Verify the Correct display of Question 6 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 7 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionSeven);
			ofc.comprareText(ofc.questionSeven, OreFree157Component.QUESTION_SEVEN, true);
			ofc.clickComponent(ofc.plusSeven);
			ofc.verifyComponentExistence(ofc.answerSeven); 
			ofc.comprareText(ofc.answerSeven, OreFree157Component.ANSWER_SEVEN, true);
			logger.write("Verify the Correct display of Question 7 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 8 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionEight);
			ofc.comprareText(ofc.questionEight, OreFree157Component.QUESTION_EIGHT, true);
			ofc.clickComponent(ofc.plusEight);
			ofc.verifyComponentExistence(ofc.answerEight); 
			ofc.comprareText(ofc.answerEight, OreFree157Component.ANSWER_EIGHT, true);
			logger.write("Verify the Correct display of Question 8 and corresponding answer- End");
			
			logger.write("Verify the Correct display of Question 9 and corresponding answer- Start");
			ofc.verifyComponentExistence(ofc.questionNine);
			ofc.comprareText(ofc.questionNine, OreFree157Component.QUESTION_NINE, true);
			ofc.clickComponent(ofc.plusNine);
			ofc.verifyComponentExistence(ofc.answerNine);
			ofc.comprareText(ofc.answerNine, OreFree157Component.ANSWER_NINE, true);
			logger.write("Verify the Correct display of Question 9 and corresponding answer- End");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
