package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SezioneMercatoSubentroLiberoAggiungiFornitura {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            PrecheckComponent forniture = new PrecheckComponent(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            int numberOfIteration = 6, iterator;
            /*
            util.getFrameActive();

            checkListModalComponent.clickConferma();

            gestione.checkSpinnersSFDC();
*/
           
            logger.write("Verifica checklist e conferma - Start");
            checkListModalComponent.clickConferma();
            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");
            merc.verifyComponentExistence(merc.tabSubentro);
            merc.verifyComponentExistence(merc.labelSubentro);
            merc.verifyComponentExistence(insert.sezioneInserimentoDati);
            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");

            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
            merc.verifyComponentExistence(merc.checkContatto);
            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");

            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
            merc.verifyComponentExistence(merc.checkCf);
            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");

            merc.clickComponentIfExist(merc.inputSezioneMercato);
            merc.clickComponentIfExist(merc.buttonConfermaSezioneMercato);

            if (prop.getProperty("TIPO_FORNITURA").equals("ELETTRICO")) {
                prop.setProperty("POD", forniture.generateRandomPodNumberEnergia());
                merc.clearAndSendKeys(merc.inputPodPdr, prop.getProperty("POD"));
            } else {
                prop.setProperty("POD", forniture.generateRandomPodNumberGas());
                merc.clearAndSendKeys(merc.inputPodPdr, prop.getProperty("POD"));
            }

            merc.clearAndSendKeys(merc.inputCap, prop.getProperty("CAP"));

            merc.clickComponentIfExist(merc.buttonEseguiPrecheck);
           

            merc.checkSpinnersSFDC();

            if (!merc.verifyExistenceComponent(merc.popupCodiceIstat))
                logger.write("modale con la selezion del codice Istat non presente , probabile causa servizio precheck");


            if (prop.getProperty("TIPO_FORNITURA").equals("ELETTRICO")) {
              //  if (!insert.Existence(insert.campoTipoMisuratore))
                insert.selezionaTipoMisuratore(prop.getProperty("TIPO_MISURATORE"));
                insert.verifyComponentExistence(insert.campoTensioneConsegna);
                insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
                insert.verifyComponentExistence(insert.campoTensioneConsegna);
                insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
                insert.verifyComponentExistence(insert.campoTensioneConsegna);
                insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
            }

            if (merc.getElementTextString(merc.offertabilità).equals("OK"))
                logger.write("Offertabilità OK");
            else
                throw new Exception("Offertabilità KO");

            

            for (iterator = 0; iterator <= numberOfIteration; iterator++) {
 
         
                if (iterator == 4) {
                    merc.clickComponentIfExist(merc.rimuoviFornitura);
                    merc.verifyComponentExistence(merc.popupEliminaFornitura);
                    if (merc.getElementTextString(merc.textpopupEliminaFornitura).contains("Stai annullando la fornitura relativa al POD/PDR") && merc.getElementTextString(merc.textpopupEliminaFornitura).contains("Vuoi procedere?")) {
                        logger.write("Testo popup di elimina fornitura corretto");                        
                        merc.clickComponentIfExist(merc.buttonEliminaFornitura);    
                        Thread.sleep(2000);
                    } else {
                        logger.write("Testo popup di elimina fornitura errato");                    
                    }
                   
                }
                
            

                merc.clickComponentIfExist(merc.aggiungiFornitura);

                if (prop.getProperty("TIPO_FORNITURA").equals("ELETTRICO")) {
                    prop.setProperty("POD", forniture.generateRandomPodNumberEnergia());
                    merc.clearAndSendKeys(merc.inputPodPdr, prop.getProperty("POD"));
                } else {
                    prop.setProperty("POD", forniture.generateRandomPodNumberGas());
                    merc.clearAndSendKeys(merc.inputPodPdr, prop.getProperty("POD"));
                }

                merc.clearAndSendKeys(merc.inputCap, prop.getProperty("CAP"));

                merc.clickComponentIfExist(merc.buttonEseguiPrecheck);
                

                if (merc.verifyExistenceComponent(merc.popupLimiteFornitura)) {
                    logger.write("popup di limite aggiunta fornitura esistente");
                    if (merc.getElementTextString(merc.testoPopupLimiteFornitura).equals("Hai raggiunto il numero massimo di linee di offerta inseribili")) {
                        logger.write("Testo popup di limite aggiunta fornitura corretto");
                        break;
                    } else {
                        logger.write("Testo popup di limite aggiunta fornitura errato");
                    }
                }

                merc.checkSpinnersSFDC();

                if (!merc.verifyExistenceComponent(merc.popupCodiceIstat))
                    logger.write("modale con la selezion del codice Istat non presente , probabile causa servizio precheck");


                if (prop.getProperty("TIPO_FORNITURA").equals("ELETTRICO")) {
                  //  if (!insert.Existence(insert.campoTipoMisuratore))    questa riga blocca la corretta esecuzione del codice
                    insert.selezionaTipoMisuratore(prop.getProperty("TIPO_MISURATORE"));
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
                }
                
     
            }

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (
                Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }

}
