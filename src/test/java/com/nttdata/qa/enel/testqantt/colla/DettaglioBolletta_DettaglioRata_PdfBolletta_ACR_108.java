package com.nttdata.qa.enel.testqantt.colla;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BillsComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_108 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
					
		HomeComponent home = new HomeComponent(driver);
		
		logger.write("Check for Residential Menu - Start");
		
		home.checkForResidentialMenu(home.RESIDENTIALMENU);
					
		logger.write("Check for Residential Menu - Completed");
		
		logger.write("Click on Bills from Residential Menu - Start");

		home.clickComponent(home.bills);
		
		logger.write("Click on Bills from Residential Menu - Completed");

		BillsComponent bs = new BillsComponent(driver);
		
		bs.verifyComponentExistence(bs.bolletteHeader);
		
		bs.waitForElementToDisplay(bs.yourFilter);
		
		logger.write("Click on Mostra Filtri - Start");

		bs.clickComponent(bs.yourFilter);
		
		logger.write("Click on Mostra Filtri - Completed");

		logger.write("Change filter value from bolleta to rata- Start");

		bs.clickComponent(bs.rataFilter);
		
		bs.clickComponent(bs.bolletaFilter);
		
		bs.clickComponent(bs.applicaFiltri);
		
		logger.write("Change filter value from bolleta to rata- Completed");

		logger.write("Click on more - Start");

		bs.clickComponent(bs.more);
		
		logger.write("Click on more - Completed");

		bs.verifyComponentExistence(bs.pagaOnline);		
		bs.verifyComponentExistence(bs.dettaglioRata);
		bs.verifyComponentExistence(bs.dettaglioPiano);
		bs.verifyComponentExistence(bs.scaricaPDF);
		Thread.currentThread().sleep(30000);
		
		/*logger.write("Click on scarica PDF - Start");
		bs.clickComponent(By.xpath("(//a[text()='Scarica PDF Bolletta'])[1]"));
		logger.write("Click on scarica PDF - Completed");

		Thread.currentThread().sleep(30000);
		
		String DIR_PATH = System.getProperty("user.home")+File.separator+"Downloads";
		
		Thread.currentThread().sleep(20000);

		if(bs.isFileDownloaded_Ext(DIR_PATH, prop.getProperty("PDF_FILE")))
			bs.deleteDownloadedFile(DIR_PATH, prop.getProperty("PDF_FILE"));*/
		
		prop.setProperty("RETURN_VALUE","OK");
				
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
