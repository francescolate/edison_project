package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_274_InfoEnelEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
//			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			logger.write("Click on services -- Start");
			hm.clickComponent(hm.services);
			logger.write("Click on services -- Completed");

			ServicesComponent sc = new ServicesComponent(driver);
			sc.verifyComponentExistence(sc.serviceTitle);
			sc.verifyComponentExistence(sc.infoEnelEnergia);
			sc.checkForSupplyServices(sc.SupplyServices);
			logger.write("Click on Info EnelEnergia on services -- Start");
			sc.clickComponent(sc.infoEnelEnergia);
			Thread.sleep(10000);
			logger.write("Click on Info EnelEnergia on services -- Completed");

			InfoEnelEnergiaMyComponent ic =new InfoEnelEnergiaMyComponent(driver);
			logger.write("Verify Info EnelEnergia page title and content -- Start");
			ic.verifyComponentExistence(ic.headingInfoEnergia);
			ic.comprareText(ic.headingInfoEnergia, ic.HEADING_INFOENERGIA, true);
			ic.comprareText(ic.titleInfoEnergia, ic.TITLE_INFOENERGIA, true);
			logger.write("Verify Info EnelEnergia page title and content -- Completed");

			logger.write("Click on Attiva Info Energia Button -- Start");
			ic.clickComponent(ic.attivaInfoEnergiaButton);
			logger.write("Click on Attiva Info Energia Button -- Completed");
			Thread.sleep(10000);
			ic.verifyComponentExistence(ic.attivaInfoEnelTitle);
			Thread.sleep(10000);
			ic.comprareText(ic.attivaInfoEnelTitle, ic.AttivaInfoEnelEnergiaTitle, true);
			ic.comprareText(ic.attivaSubText, ic.TITLE_INFOENERGIA, true);
			logger.write("Click on i icon next to supply -- Start");
			ic.clickComponent(ic.Iicon274);
			Thread.sleep(5000);
			logger.write("Click on i icon next to supply -- Completed");

			logger.write("Verify pop up details -- Start");
			ic.verifyComponentExistence(ic.popUpTitle);
			ic.comprareText(ic.popUpHeading, ic.POPUP_HEADING, true);
			ic.comprareText(ic.popupContent, ic.PopUpContent, true);
			ic.verifyComponentExistence(ic.popUpClose);
			logger.write("Verify pop up details -- Completed");
			logger.write("Click on pop up close -- Start");
			ic.clickComponent(ic.popUpClose);
			Thread.sleep(10000);
			ic.isPopUpDisplayed(ic.popUpClose);
			logger.write("Click on pop up close -- Completed");

			ic.verifyComponentExistence(ic.attivaContinueButton);
			ic.verifyComponentExistence(ic.attivaEsciButton);
			logger.write("Click on ESCI button -- Start");
			ic.clickComponent(ic.attivaEsciButton);
			logger.write("Click on ESCI button -- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
