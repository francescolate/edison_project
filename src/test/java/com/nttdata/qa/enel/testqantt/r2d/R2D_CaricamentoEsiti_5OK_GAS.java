package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_5OK_GAS {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				// Selezione Menu Interrogazione
				menuBox.selezionaVoceMenuBox("Code di comunicazione", "Caricamento Esiti");
				R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
				// Selezione tipologia Caricamento
				caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
				// Inserisci Pdr
				if (!prop.getProperty("POD_GAS","").equals("")) {
					caricamentoEsiti.inserisciPdr(caricamentoEsiti.inputPDR,
							prop.getProperty("POD_GAS", prop.getProperty("POD")));
				}
				// Inserisci ID Richiesta CRM
				caricamentoEsiti.inserisciNumeroOrdineCRM(caricamentoEsiti.inputNumeroOrdineCRM,
						prop.getProperty("OI_RICERCA"));
				// Cerca
				caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
				// Click button Azione
				caricamentoEsiti.selezionaTastoAzionePratica();
				// Verifica Stato pratica atteso
				caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "CI");
				// Selezione esito 5OK
				caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_5OK_GAS"));
				// Inserimento esito
				caricamentoEsiti.selezioneEsito("OK");
				// Calcolo sysdate
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String data = simpleDateFormat.format(calendar.getTime()).toString();
				String tipoOperazione = prop.getProperty("TIPO_OPERAZIONE");

				// Inserimento dettaglio esito 5OK in base alla tipologia processo
				if (tipoOperazione.equals("PRIMA_ATTIVAZIONE_EVO")) {
					// if (prop.getProperty("PRIMA_ATTIVAZIONE","N").equals("Y")){
					caricamentoEsiti.inserimentoDettaglioEsito5OKPrimaAttivazioneGAS(data,
							prop.getProperty("MATRICOLA_CONTATORE"), prop.getProperty("LETTURA_CONTATORE"),
							prop.getProperty("ANNO_COSTRUZIONE_CONTATORE"), prop.getProperty("ACCESSIBILITA_229"),
							prop.getProperty("COEFFICIENTE_C"), prop.getProperty("CLASSE_CONTATORE"),
							prop.getProperty("STATO_CONTATORE"));
				}
				// if (prop.getProperty("VOLTURA_SENZA_ACCOLLO","N").equals("Y")){
				if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKVolturaGAS(prop.getProperty("CP_GESTORE"), data);
				}
				// if (prop.getProperty("SUBENTRO_R2D","N").equals("Y")){
				if (tipoOperazione.equals("SUBENTRO_R2D")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKPrimaAttivazioneGAS(data,
							prop.getProperty("MATRICOLA_CONTATORE"), prop.getProperty("LETTURA_CONTATORE"),
							prop.getProperty("ANNO_COSTRUZIONE_CONTATORE"), prop.getProperty("ACCESSIBILITA_229"),
							prop.getProperty("COEFFICIENTE_C"), prop.getProperty("CLASSE_CONTATORE"),
							prop.getProperty("STATO_CONTATORE"));
				}
				// if (prop.getProperty("VOLTURA_CON_ACCOLLO","N").equals("Y")){
				if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKVolturaGAS(prop.getProperty("CP_GESTORE"), data);
				}
				if (tipoOperazione.equals("DISDETTA_CON_SUGGELLO")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKDisdettaconsuggelloGAS(data,
							prop.getProperty("CP_GESTORE"), "124587", prop.getProperty("STATO_CONTATORE"));
				}
				if (tipoOperazione.equals("MOGE")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKMogeGas(prop.getProperty("CP_GESTORE"), data);
				}
				
				if (tipoOperazione.equals("CAMBIO_USO")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKconGestoreGAS(prop.getProperty("CP_GESTORE"), data);
				}

				// if (prop.getProperty("VARIAZIONE_INDIRIZZO_FORNITURA","N").equals("Y")){
				if (tipoOperazione.equals("VARIAZIONE_INDIRIZZO_FORNITURA")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKVolturaGAS(prop.getProperty("CP_GESTORE"), data);
				}
				// if (prop.getProperty("VERIFICA_CONTATORE","N").equals("Y")){
				if (tipoOperazione.equals("VERIFICA_CONTATORE")) {
					caricamentoEsiti.inserimentoDettaglioEsito5OKVerificaContatoreGAS(data,
							prop.getProperty("CP_GESTORE"), prop.getProperty("TIPOLOGIA_ESITO"),
							prop.getProperty("FILENAME"), prop.getProperty("TIPOLOGIA_DOCUMENTO"));
				}
				// if (prop.getProperty("MODIFICA_IMPIANTO","N").equals("Y")){
				if (tipoOperazione.equals("MODIFICA_IMPIANTO")) {
					// Calcolo data sysdata+15 gg
					Calendar calendar15 = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
					calendar15.setTime(simpleDateFormat.parse(data));
					calendar15.add(Calendar.DATE, 15); // number of days to add
					String data15 = simpleDateFormat.format(calendar15.getTime());
					caricamentoEsiti.inserimentoDettaglioEsito5OKModificaImpiantoGAS(prop.getProperty("CP_GESTORE"),
							data, prop.getProperty("IMPORTO_PREVENTIVO"), data15, prop.getProperty("CODICE_PREVENTIVO"),
							prop.getProperty("FILENAME"));
				}
				
				if (tipoOperazione.equals("PREDISPOSIZIONE_PRESA")) {
					// Calcolo data sysdata+15 gg
					Calendar calendar15 = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
					calendar15.setTime(simpleDateFormat.parse(data));
					calendar15.add(Calendar.DATE, 15); // number of days to add
					String data15 = simpleDateFormat.format(calendar15.getTime());
					// Preventivo
					if (prop.getProperty("PREVENTIVO_GAS","N").equals("Y")) {
						caricamentoEsiti.inserimentoDettaglioEsito5OKModificaImpiantoGAS(prop.getProperty("OI_RICERCA"),
								data, prop.getProperty("IMPORTO_PREVENTIVO"), data15, prop.getProperty("CODICE_PREVENTIVO"),
								prop.getProperty("PATH_DOCUMENTO"));
						logger.write("Caricamento Preventivo - Richiesta N01 Allaccio");
					} else  {
						// 5 OK su Flusso E01 ALLACCIO
						logger.write("Caricamento 5 OK - Richiesta E01 Allaccio");
						caricamentoEsiti.inserimentoDettaglioEsito5OKAllaccioGAS(prop.getProperty("OI_RICERCA"),
								data, prop.getProperty("PDR"));
					}
				}
//				if (tipoOperazione.equals("Richiesta N01 Allaccio")){
//					if (!prop.getProperty("TIPO_LAVORAZIONE_CRM_ELE","N").equals("REL N01 Allaccio")) {
//							caricamentoEsiti.inserimentoDettaglioEsitoPreventivoPPELE(prop.getProperty("OI_RICERCA"),data, 
//							prop.getProperty("IMPORTO_PREVENTIVO"), dataScadenza, 
//							prop.getProperty("CODICE_PREVENTIVO"), prop.getProperty("TIPO_CARICAMENTO"),
//							prop.getProperty("LIVELLO_TENSIONE"), prop.getProperty("TEMPO_MAX_LAVORI"),
//							prop.getProperty("REFERENTE"), prop.getProperty("TEL_REFERENTE"),
//							prop.getProperty("VOCE_DI_COSTO"));
//							logger.write("Caricamento Preventivo - Richiesta N01 Allaccio");
//					} else {
//						caricamentoEsiti.inserimentoDettaglioEsito5OKPPELE(prop.getProperty("OI_RICERCA"), 
//								data, prop.getProperty("TENSIONE_RICHIESTA"),
//								prop.getProperty("POTENZA_RICHIESTA"), prop.getProperty("POTENZA_FRANCHIGIA"));
//						logger.write("Caricamento 5 OK - Richiesta N01 Allaccio");
//					}
//				}
				// Salvo valore stato pratica attuale
				prop.setProperty("STATO_PRATICA", "OK");
			
			//
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
