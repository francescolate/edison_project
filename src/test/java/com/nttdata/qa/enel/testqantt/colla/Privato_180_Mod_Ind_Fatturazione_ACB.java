package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato180ModIndFatturazioneACBComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_180_Mod_Ind_Fatturazione_ACB {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato180ModIndFatturazioneACBComponent mod = new Privato180ModIndFatturazioneACBComponent(driver);
			
			logger.write("Validate HOMEPAGE, Title and click on SERVIZI left menu item - Start");
			By areaRiservata = mod.areaRiservata;
			mod.verifyComponentExistence(areaRiservata);
			mod.VerifyText(areaRiservata, prop.getProperty("HOMEPAGEPATH"));
			
			By pageHeading = mod.pageHeading;
			mod.verifyComponentExistence(pageHeading);
			mod.VerifyText(pageHeading, prop.getProperty("HOMEPAGETITLE"));
			
			By serviziSelect = mod.serviziSelect;
			mod.verifyComponentExistence(serviziSelect);
			mod.clickComponent(serviziSelect);
			logger.write("Validate HOMEPAGE, Title and click on SERVIZI left menu item - Completed");
			
			logger.write("Validate SERVIZI HOMEPAGE, Title, SubText and click on 'modificaIndirizzoDiFatturazione_button' - Start");
			By serviziPagePath = mod.areaRiservata;
			mod.verifyComponentExistence(serviziPagePath);
			mod.VerifyText(serviziPagePath, prop.getProperty("SERVIZI_PAGEPATH"));
			
			By serviziHeading = mod.serviziHeading;
			mod.verifyComponentExistence(serviziHeading);
			mod.VerifyText(serviziHeading, prop.getProperty("SERVIZI_PAGETITLE"));
			
			By serviziPageSubText = mod.serviziPageSubText;
			mod.verifyComponentExistence(serviziPageSubText);
			mod.VerifyText(serviziPageSubText, prop.getProperty("SERVIZI_PAGE_SUBTEXT"));
			
			By modificaIndirizzoDiFatturazione_button = mod.modificaIndirizzoDiFatturazione_button;
			mod.verifyComponentExistence(modificaIndirizzoDiFatturazione_button);
			mod.clickComponent(modificaIndirizzoDiFatturazione_button);
			logger.write("Validate SERVIZI HOMEPAGE, Title, SubText and click on 'modificaIndirizzoDiFatturazione_button' - Completed");
			Thread.sleep(10000);
			
			logger.write("Validate IndirizzoDiFatturazione HOMEPAGE, Title and click on 'modificaButton' - Start");
			By indirizzoDiFatturazionePagePath = mod.areaRiservata;
			mod.verifyComponentExistence(indirizzoDiFatturazionePagePath);
			mod.VerifyText(indirizzoDiFatturazionePagePath, prop.getProperty("INDIRIZZO_DI_FATTURAZIONE_PAGEPATH"));
			
			By indirizzoDiFatturazionePageTitle = mod.indirizzoDiFatturazionePageTitle;
			mod.verifyComponentExistence(indirizzoDiFatturazionePageTitle);
			mod.VerifyText(indirizzoDiFatturazionePageTitle, prop.getProperty("INDIRIZZO_DI_FATTURAZIONE_PAGETITLE"));
			
			mod.validateIndirizzoDiFatturazionePageSections(mod.indirizzoDiFatturazionePageSubSectionHeadings);
			
			By modificaButton = mod.modificaButton;
			mod.verifyComponentExistence(modificaButton);
			mod.clickComponent(modificaButton);
			logger.write("Validate IndirizzoDiFatturazione HOMEPAGE, Title and click on 'modificaButton' - Completed");
			Thread.sleep(45000);
			
			logger.write("Validate ModificaIndirizzoDiFatturazione HOMEPAGE, Heading - Start");
			By modificaIndirizzoDiFatturazionePagePath = mod.areaRiservata;
			mod.verifyComponentExistence(modificaIndirizzoDiFatturazionePagePath);
			mod.VerifyText(modificaIndirizzoDiFatturazionePagePath, prop.getProperty("MODIFICA_INDIRIZZO_DI_FATTURAZIONE_PAGEPATH"));
			
			By modificaIndirizzoDiFatturazioneHeading = mod.ModificaIndirizzoDiFatturazioneHeading;
			mod.verifyComponentExistence(modificaIndirizzoDiFatturazioneHeading);
			mod.VerifyText(modificaIndirizzoDiFatturazioneHeading, prop.getProperty("MODIFICA_INDIRIZZO_DI_FATTURAZIONE_PAGETITLE"));
			logger.write("Validate ModificaIndirizzoDiFatturazione HOMEPAGE, Heading - Completed");
			
			logger.write("Check presence of Step 1 is active - Start");
			By inserimentoDati = mod.inserimentoDati;
			mod.verifyComponentExistence(inserimentoDati);
			mod.VerifyText(inserimentoDati, prop.getProperty("STEP1"));
			logger.write("Check presence of Step 1 is active - Completed");
			
			logger.write("Verify the fields for step1 - Start");
			mod.validateFields();
			logger.write("Verify the fields for step1 - Completed");
			
			logger.write("Entering invalid values to validate error - Start");
			mod.enterInput(mod.provinciaFieldValue, "SALERNO");
			mod.enterInput(mod.comuneFieldValue, "ANGRI");
			mod.enterInput(mod.indirizzoFieldValue, "Via Umberto Smaila");
			mod.enterInput(mod.numeroCivicoFieldValue, "1");
			logger.write("Entering invalid values to validate error - Completed");

			logger.write("Validate and click on proseguiButton - Start");
			By proseguiButton = mod.proseguiButton;
			mod.verifyComponentExistence(proseguiButton);
			mod.clickComponent(proseguiButton);
			logger.write("Validate and click on proseguiButton - Completed");
			Thread.sleep(15000);
			
			logger.write("validate error messgae displayed, click on error link - Start");
			mod.verifyComponentExistence(mod.errorMessage);
			mod.VerifyText(mod.errorMessage, mod.errorMessageText);
			
			mod.verifyComponentExistence(mod.errorLink);
			mod.VerifyText(mod.errorLink, mod.errorMessageLink);
			mod.clickComponent(mod.errorLink);
			logger.write("validate error messgae displayed, click on error link - Completed");
			
			/*logger.write("Select values from customer select drop down - Start");
			By customerSelect = mod.customerSelect;
			mod.verifyComponentExistence(customerSelect);
			mod.clickComponent(customerSelect);
			
			By customerSelectDropdownFeild = mod.customerSelectDropdownFeild;
			mod.verifyComponentExistence(customerSelectDropdownFeild);
			mod.clickComponent(customerSelectDropdownFeild);
			logger.write("Select values from customer select drop down - Completed");*/
			
			By ToponomasticaFieldSelect = mod.ToponomasticaFieldSelect;
			mod.verifyComponentExistence(ToponomasticaFieldSelect);
			mod.selectDropDownByValue(ToponomasticaFieldSelect, "VIA");
			
			mod.enterInput(mod.CAPFieldValue, "00198");
			mod.clickComponent(mod.CAPFieldSelect);
			
			logger.write("After customer select, validate the prepopulated field values - Start");
			mod.ValidatePrepopulatedFields(mod.numeroCivicoFieldValue,mod.IndirizzoFieldValue,mod.populatedNumeroCivicoFieldValue);
			logger.write("After customer select, validate the prepopulated field values - Completed");
			
			logger.write("Validate presence of 'esciButton','indietroButton' and click on 'proseguiButton' button - Start");
			By esciButton = mod.esciButton;
			mod.verifyComponentExistence(esciButton);
			
			By indietroButton = mod.indietroButton;
			mod.verifyComponentExistence(indietroButton);
			
			mod.verifyComponentExistence(proseguiButton);
			mod.clickComponent(proseguiButton);
			logger.write("Validate presence of 'esciButton','indietroButton' and click on 'proseguiButton' button - Completed");
			Thread.sleep(8000);
			
			logger.write("Check presence of Step 2 is active - Start");
			By riepilogo_e_ConfermaDati = mod.riepilogo_e_ConfermaDati;
			mod.verifyComponentExistence(riepilogo_e_ConfermaDati);
			mod.VerifyText(riepilogo_e_ConfermaDati, prop.getProperty("STEP2"));
			logger.write("Check presence of Step 2 is active - Completed");
			
			logger.write("Check presence of prepopulated field values in step 2, verify the supplies selected - Start");
			mod.ValidatePrepopulatedFields(mod.numeroCivicoExpectedFieldValue,mod.IndirizzoExpectedFieldValue,mod.populatedNumeroCivicoExpectedFieldValue);
			
			mod.ConfirmSupply(mod.ConfirmSupply);
			logger.write("Check presence of prepopulated field values in step 2, verify the supplies selected - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}	
