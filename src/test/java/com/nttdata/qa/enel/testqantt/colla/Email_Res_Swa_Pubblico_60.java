package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LuceComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Email_Res_Swa_Pubblico_60 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			SpinnerManager sm = new SpinnerManager(driver);
		    SeleniumUtilities util = new SeleniumUtilities(driver);

			
			PubblicoID61ProcessoAResSwaEleComponent prs = new PubblicoID61ProcessoAResSwaEleComponent(driver);
			
			logger.write("Accessing the home page - Start");
			prs.launchLink(prop.getProperty("HOMEPAGE"));
			Thread.sleep(6000);
			prs.launchLink(prop.getProperty("LINK"));
			Thread.sleep(6000);
			//if(prs.verifyExistence(prs.buttonAccetta,10));
			if (util.verifyVisibility(prs.buttonAccetta,10)) 
				prs.clickComponent(prs.buttonAccetta);
			logger.write("Accessing the home page - Complete");

			Thread.sleep(6000);
			
			logger.write("Click on attivaLoffertaButton - Start");
			By attivaLoffertaButton = prs.attivaLoffertaButton;
			prs.verifyComponentExistence(attivaLoffertaButton);
			prs.clickComponent(attivaLoffertaButton);
			logger.write("Click on attivaLoffertaButton - Complete");
			
			Thread.sleep(10000);
			
			logger.write("Check Adesione Contratto Speciale Luce 60 - Start");
			OCR_Module_Component ocr = new OCR_Module_Component(driver);
			ocr.compareText(ocr.header, "Speciale Luce 60", true);
			ocr.checkPage();
			ocr.clickComponent(ocr.compilaManualmenteButton);
			logger.write("Check Adesione Contratto Speciale Luce 60 - Completed");
			
			
			logger.write("Inserisci i tuoi dati - Start");
			ocr.inserisciDati(prop.getProperty("NOME"), prop.getProperty("COGNOME"), prop.getProperty("CODICE_FISCALE"), prop.getProperty("TELEFONO"), prop.getProperty("EMAIL"));
			sm.checkCollaSpinner4();
			logger.write("Inserisci i tuoi dati - Completed");
			
			logger.write("Informazioni fornitura - Start");
			ocr.informazioniFornituraLuce();
			Thread.sleep(5000);
			ocr.insertInformazioniFornitura("CAMBIO FORNITORE", "IT004E"+ocr.generateRandomPOD(8), prop.getProperty("CAP"), prop.getProperty("CITY"), prop.getProperty("CITY_FULL"), prop.getProperty("ADDRESS"), prop.getProperty("CIVIC"), "", prop.getProperty("SUPPLIER_LUCE"), true, true);
			Thread.sleep(10000);
			ocr.insertPagamentiBollette("Bollettino Postale", false, "");
			Thread.sleep(5000);
			logger.write("Informazioni fornitura - Completed");
			
			logger.write("Check Consensi - Start");
			//ocr.checkMandatiConsensi();
			ocr.insertMandatiConsensiLuce();
			//ocr.clickComponent(ocr.completaAdesione);
			logger.write("Check Consensi - Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

}
	
	
}
