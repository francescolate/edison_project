package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.WebPrivatoDettaglioFornituraInAttivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class WebPrivatoDettaglioForniturainattivazonebollete {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			WebPrivatoDettaglioFornituraInAttivazioneComponent wpdfia = new WebPrivatoDettaglioFornituraInAttivazioneComponent(driver);
			wpdfia.checkDocumentReadyState();
			
			logger.write("Visualizza le informazioni principali delle tue forniture e consulta le tue bollette");
			wpdfia.verifyComponentExistence(By.xpath(wpdfia.Visualizzalestato.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			wpdfia.clickComponent(By.xpath(wpdfia.Visualizzalestato.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			logger.write("Controllando se la fornitura esiste e clicca sul button Visualizza Stato - COMPLETED");
			logger.write("Click on No and verify the page - Start");
			//wpdfia.clickComponent(wpdfia.Forniture);
			wpdfia.verifyComponentExistence(wpdfia.popUpHeading);
			wpdfia.verifyComponentExistence(wpdfia.popUpInnerText);
			wpdfia.verifyComponentExistence(wpdfia.no);
			
			wpdfia.verifyComponentExistence(wpdfia.siButton);
			wpdfia.clickComponent(wpdfia.siButton);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
