package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.OffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyEmailSubjectAndContent_99 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String content = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            content = gm.recuperaLink(prop, "OFFERTA VALORE LUCE PLUS", false);
		            Thread.sleep(5000);
		            System.out.println(content);
		            OffertaComponent oc =new OffertaComponent(driver);
		            oc.verifyEmailContent(content, oc.EmailContentLine1);
		            oc.verifyEmailContent(content, oc.EmailContentLine2);
		            oc.verifyEmailContent(content, oc.EmailContentLine3);
		            oc.verifyEmailContent(content, oc.EmailContentLine4);
		            oc.verifyEmailContent(content, oc.EmailContentLine5);
		            oc.verifyEmailContent(content, oc.EmailContentLine6);
		            oc.verifyEmailContent(content, oc.EmailContentLine7);
		            oc.verifyEmailContent(content, oc.EmailContentLine8);
		            oc.verifyEmailContent(content, oc.EmailContentLine9);
		            
		            String endingLimit = "\" aria-label=\"Clicca per confermare la tua offerta:";
			        String startingLimit = "<a href=\"";
			       
		            String link = content.substring((content.indexOf(startingLimit)+9),content.indexOf( endingLimit)+0);
		            
		            System.out.println(link);
		            logger.write("Recupera Link - Completed");		            
		            
				}
			
				prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
          	StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
