package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ContattaciBannerComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LiveChat49 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ContattaciBannerComponent cbc = new ContattaciBannerComponent(driver);
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			log.clickComponent(log.buttonAccetta);
			Thread.sleep(10000);
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			logger.write("select drop down value - Start");
			pc.changeDropDownValue(pc.productDropDown, pc.luce);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, pc.visualizza);
			logger.write("select drop down value - Completed");

			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			pc.waitForElementToDisplay(pc.dettaglioOfferta);
			pc.jsClickObject(pc.dettaglioOfferta);
			Thread.sleep(30000);			
			//pc.clickComponent(pc.closeBtn);
			
			cbc.clickComponent(cbc.contattaci);
			cbc.clickComponent(cbc.boxOffer);
			cbc.clickComponent(cbc.boxMessage);
			
			pc.verifyComponentExistence(pc.telefono);
			pc.verifyComponentExistence(pc.fasciaOraria);

			
			
			
			
			
			
			/*
			pc.waitForElementToDisplay(pc.bannerPhrase);			
			pc.verifyComponentExistence(pc.banner);
			pc.comprareText(pc.bannerPhrase, pc.Banner, true);
			
			logger.write("Click on Banner and verify the message - Start");
			pc.clickComponent(pc.banner);			
			Thread.sleep(10000);
			pc.comprareText(pc.bannerText1, pc.BannerTitle, true);
			pc.verifyComponentExistence(pc.telefono);
			pc.verifyComponentExistence(pc.fasciaOraria);
			logger.write("Click on Banner and verify the message - Completed");

			pc.clickComponent(pc.livechat);
			pc.comprareText(pc.liveChatText, pc.LiveChat, true);
			pc.comprareText(pc.bannerText2, pc.BannerText, true);
			*/

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
