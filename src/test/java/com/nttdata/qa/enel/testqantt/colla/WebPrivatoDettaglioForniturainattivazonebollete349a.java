package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.WebPrivatoDettaglioFornituraInAttivazionebolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class WebPrivatoDettaglioForniturainattivazonebollete349a {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			WebPrivatoDettaglioFornituraInAttivazionebolletteComponent wpdfia = new WebPrivatoDettaglioFornituraInAttivazionebolletteComponent(driver);
			wpdfia.checkDocumentReadyState();
			
			logger.write("Visualizza le informazioni principali delle tue forniture e consulta le tue bollette");
			wpdfia.verifyComponentExistence(By.xpath(wpdfia.Visualizzalestato.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			wpdfia.clickComponent(By.xpath(wpdfia.Visualizzalestato.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			logger.write("Controllando se la fornitura esiste e clicca sul button Visualizza Stato - COMPLETED");
            wpdfia.verifyComponentExistence(wpdfia.NumeroCliente);
			wpdfia.verifyComponentExistence(wpdfia.Statodiattivazione);
			wpdfia.verifyComponentExistence(wpdfia.visualizetext);
			wpdfia.verifyComponentExistence(wpdfia.AttivitàConcluse);
			wpdfia.compareText(wpdfia.visualizetext, wpdfia.VISUALIZETEXT, true);
	        wpdfia.verifyComponentExistence(wpdfia.ServiziRichiesti);
	        logger.write("Verify left menu items -- Start");
	        wpdfia.leftMenuItemsDisplay(wpdfia.leftMenuItems);
			logger.write("Verify left menu items -- Completed");
			logger.write("Click on left menu item Bollete and verify page navigation -- Start");
			wpdfia.verifyComponentExistence(wpdfia.Diseguito);
			wpdfia.compareText(wpdfia.Diseguito, wpdfia.DISEGUITO, true);
			wpdfia.clickComponent(wpdfia.bollette);
			wpdfia.verifyComponentExistence(wpdfia.Attenzione);
			wpdfia.verifyComponentExistence(wpdfia.Seisicurotext);
			wpdfia.compareText(wpdfia.Seisicurotext, wpdfia.	SEISICUROTEXT, true);
			wpdfia.verifyComponentExistence(wpdfia.popUpInnerText);
			wpdfia.verifyComponentExistence(wpdfia.popUpHeading);
			wpdfia.verifyComponentExistence(wpdfia.no);
			wpdfia.verifyComponentExistence(wpdfia.si);
			wpdfia.clickComponent(wpdfia.si);
			wpdfia.verifyComponentExistence(wpdfia.Letuebollette);
			wpdfia.compareText(wpdfia.Letuebollette, wpdfia.LETUEBOLLETTE, true);
					
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
