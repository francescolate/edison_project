package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.DuplicatoBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Rateizzazione_ACR_371 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DuplicatoBolletteComponent mod = new DuplicatoBolletteComponent(driver);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		
		mod.comprareText(mod.homePageHeading1,  DuplicatoBolletteComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2,  DuplicatoBolletteComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH2, true);
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
		mod.comprareText(mod.sectionHeading1, DuplicatoBolletteComponent.SECTION_PAGE_HEADING1, true);
		mod.comprareText(mod.sectionParagraph1, DuplicatoBolletteComponent.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		 mod.clickComponent(mod.rateizzazione);
		 mod.comprareText(mod.rateizzazioneTitle, mod.RATEIZZAZIONE, true);
		 mod.comprareText(mod.rateizzazioneText, mod.RATEIZZAZIONE_TEXT, true);
		 
		 mod.verifyComponentExistence(mod.supplyList);
		 mod.comprareText(mod.indirizzoDellaFornitura, mod.INDIRIZZO_DELLA_FORNITURA, true);
		 mod.comprareText(mod.numeroCliente, mod.NUMERO_CLIENTE, true);
		 mod.comprareText(mod.modalitaDiPagamento, mod.MODALITO_PAGAMENTO, true);
		 mod.comprareText(mod.addebitoDiretto, mod.ADDEBITO_DIRETTO, true);
		 
		 mod.verifyComponentExistence(mod.supplyList);
		 
		 mod.clickComponent(mod.supplyList);
		 
		 mod.comprareText(mod.attenzione, mod.ATTENZIONE, true);
		 mod.comprareText(mod.popupText, mod.POPUP_TEXT, true);
		 
		 mod.clickComponent(mod.popupClose);
		 
		 mod.comprareText(mod.enelEnergiaSPA, mod.ENEL_ENERGIA_SPA, true);
		 mod.comprareText(mod.tuttiDiDirittiRiservati, mod.TUTTI_I_DIRITTI, true);
		 mod.comprareText(mod.gruppoIVA, mod.GRUPO_IVA, true);
		 mod.comprareText(mod.informazioniLegali, mod.INFORMAZIONI_LEGALI, true);
		 mod.comprareText(mod.privacy, mod.PRIVACY, true);
		 mod.comprareText(mod.credits, mod.CREDITS, true);
		 mod.comprareText(mod.contattaci, mod.CONTATTACI, true);
			 
		 		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
