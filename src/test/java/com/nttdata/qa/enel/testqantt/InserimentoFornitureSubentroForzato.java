package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class InserimentoFornitureSubentroForzato {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			logger.write("popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Start");
			PrecheckComponent forniture = new PrecheckComponent(driver);
			prop.setProperty("POD", forniture.generateRandomPodNumberEnergia());
			forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD"));
			forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
			forniture.press(forniture.precheckButton2);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			gestione.checkSpinnersSFDC();
			logger.write("popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Completed");
			
			logger.write("selezionare il codice istat e premere conferma - Start");
			forniture.verifyComponentExistence(forniture.tabellaSelezioneIstat);
		    forniture.verifyComponentExistence(forniture.codiceIstat);
		    forniture.clickComponent(forniture.codiceIstat);
		    forniture.clickComponent(forniture.buttonConfermaIstat);
			gestione.checkSpinnersSFDC();
			logger.write("selezionare il codice istat e premere conferma - Completed");
			
/*cambiarelog*/
			logger.write("verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Start");
			InserimentoFornitureSubentroComponent insert=new InserimentoFornitureSubentroComponent(driver);
			insert.verifyComponentExistence(insert.tabIndirizzo);
			insert.verifyComponentExistence(insert.buttonVerifica);
			insert.clickComponent(insert.buttonVerifica);
			TimeUnit.SECONDS.sleep(3);
			insert.verifyComponentInexistence(insert.labelIndirizzzoVerificato);
			
/*cambiarelog*/
			logger.write("verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Completed");
			
			//FORZATURA INDIRIZZO
			
			By indirizzo_forzato_flag = forniture.indirizzo_forzato_flag;
					
			forniture.verifyComponentExistence(indirizzo_forzato_flag);
			forniture.clickComponent(indirizzo_forzato_flag);
			
			forniture.insertTextByChar(forniture.insertCAP_2, prop.getProperty("CAP"));
			
			By first_address_in_list_cap = forniture.first_address_in_list_cap;
			
			forniture.verifyComponentExistence(first_address_in_list_cap);
			forniture.clickComponent(first_address_in_list_cap);
			
			insert.verifyComponentExistence(insert.buttonForzaIndirizzo);
			insert.clickComponent(insert.buttonForzaIndirizzo);
			
			insert.verifyComponentExistence(insert.labelIndirizzzoVerificato);
			
			//
			
			insert.verifyComponentExistence(insert.campoTensioneConsegna);
			insert.verifyComponentExistence(insert.campoPotenzaContattuale);
			insert.verifyComponentExistence(insert.campoPotenzaFranchigia);
		    insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
			insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
			insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
			if(prop.getProperty("VERIFICA_ESITO_OFFERTABILITA").contentEquals("Y")) {
			logger.write("Verifica esito Offertabilita - Start");
			forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
			logger.write("Verifica esito Offertabilita - Completed");
			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
