package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
/**
	@SCOPO Verifica lo stato dell'order item di attivazione per la VCA
	@PROPERTY_RICHIESTE
		STATO_ATT; STATO_R2D_ATT; STATO_SAP_ATT; STATO_SEMPRE_ATT; STATO_UDB_ATT
 */
public class VerificaStatoAttivazioneForVca {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			logger.write("Verifica stato attivazione  - Start");
			
			logger.write("Recupero valori attesi da property ");
			String att_status_exp_value=prop.getProperty("STATO_ATT");
			String att_R2D_status_exp_value=prop.getProperty("STATO_R2D_ATT");
			String att_SAP_status_exp_value=prop.getProperty("STATO_SAP_ATT");
			String att_SEMPRE_status_exp_value=prop.getProperty("STATO_SEMPRE_ATT");
			String att_udb_status_exp_value=prop.getProperty("STATO_UDB_ATT");
			

		

			Map<Integer,Map> queryResult=null;
			Map row=null;
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);

			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);


			String oiAttivazione=prop.getProperty("OI_ITEM_ATTIVAZIONE");
			String pod=prop.getProperty("POD");



			String stato,idBpm,R2D_status,SAP_status, SAP_Status_code, SEMPRE_status,SEMPRE_status_code,udb_status;






			//VERIFICA ATTIVAZIONE
			logger.write("Esecuzione Query ");
			String query=Costanti.query_VCA_OrderItemdaOrderItemId;
			queryResult=wb.EseguiQuery(query.replace("@ORDER_ITEM_ID@", oiAttivazione).replace("@POD@", pod));
			row=queryResult.get(1);
			logger.write("ID ORDER ITEM: "  + oiAttivazione);
			
			logger.write("Salvataggio risultati Query ");

			stato=row.get("NE__Status__c").toString();
			idBpm=row.get("ITA_IFM_IDBPM__c").toString();
			

			
			
			R2D_status=row.get("ITA_IFM_R2D_Status__c").toString();
			SAP_status=row.get("ITA_IFM_SAP_Status__c").toString();
			SAP_Status_code=row.get("ITA_IFM_SAP_StatusCode__c").toString();
			SEMPRE_status=row.get("ITA_IFM_SEMPRE_Status__c").toString();
			SEMPRE_status_code=row.get("ITA_IFM_SEMPRE_StatusCode__c").toString();;
			udb_status=row.get("ITA_IFM_UDB_Status__c").toString();
			
			logger.write("ID BPM: "+ idBpm);
			
			 logger.write(String.format("Stato Item - Atteso: %s  -> Effettivo: %s",stato,att_status_exp_value));
			 logger.write(String.format("Stato R2D - Atteso: %s  -> Effettivo: %s",R2D_status,att_R2D_status_exp_value));
			 logger.write(String.format("Stato SAP - Atteso: %s  -> Effettivo: %s",SAP_status,att_SAP_status_exp_value));
			 logger.write(String.format("Stato SEMPRE - Atteso: %s  -> Effettivo: %s",SEMPRE_status,att_SEMPRE_status_exp_value));
			 logger.write(String.format("Stato UDB - Atteso: %s  -> Effettivo: %s",udb_status,att_udb_status_exp_value));

			if(!stato.toUpperCase().equals(att_status_exp_value)) {

				throw new Exception(String.format("Attivazione - Valore  stato non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", stato,att_status_exp_value));
			}
			if(!R2D_status.toUpperCase().equals(att_R2D_status_exp_value)) {
				throw new Exception(String.format("Attivazione - Stati R2D non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", stato,att_status_exp_value));
			}
			if(!SAP_status.toUpperCase().equals(att_SAP_status_exp_value)) {
				throw new Exception(String.format("Attivazione - Stato SAP   non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", SAP_status,att_SAP_status_exp_value));
			}
			if(!SEMPRE_status.toUpperCase().equals(att_SEMPRE_status_exp_value)) {
				throw new Exception(String.format("Attivazione - Stato SEMPRE non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", SEMPRE_status,att_SEMPRE_status_exp_value));
			}
			if(!udb_status.toUpperCase().equals(att_udb_status_exp_value)) {
				throw new Exception(String.format("Attivazione - Stato UDB non corrispondente con valore atteso valore atteso: %s - Valore riscontrato: %s", udb_status,att_udb_status_exp_value));
			}


			prop.setProperty("RETURN_VALUE", "OK");
			logger.write("Stato attivazione OK");
			logger.write("Verifica stato attivazione  - End");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
