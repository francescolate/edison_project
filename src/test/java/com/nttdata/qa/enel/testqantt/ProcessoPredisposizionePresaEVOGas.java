package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.GestionePredisposizionePresaComponentEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoPredisposizionePresaEVOGas {

	@Step("Processo Predisposizione Presa GAS EVO")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//				SeleniumUtilities util = new SeleniumUtilities(driver);
//
//				SceltaProcessoComponent predisposizionePresa = new SceltaProcessoComponent(driver);
//				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
//				DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
//				IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(
//						driver);
//				CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
				GestionePredisposizionePresaComponentEVO presa = new GestionePredisposizionePresaComponentEVO(driver);
//				String statoUbiest = Costanti.statusUbiest;
		
//				logger.write("Selezione processo Predisposizione Presa Evo - Start");
//				TimeUnit.SECONDS.sleep(10);
//				predisposizionePresa.clickAllProcess();
//				TimeUnit.SECONDS.sleep(2);
//				predisposizionePresa.chooseProcessToStart(predisposizionePresa.predisposizionePresaEVOGas);
//				logger.write("Selezione processo Predisposizione Presa Evo - Completed");
//
//				logger.write("Identificazione interlocutore - Start");
//				identificaInterlocutore.insertDocumento("b");
//				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
//				logger.write("Identificazione interlocutore - Completed");
//
//				logger.write("Conferma Interlocutore - Start");
//				identificaInterlocutore.pressButton(identificaInterlocutore.confermaPagina);
//				logger.write("Conferma Interlocutore - Completed");
//
//				logger.write("Inserimento documenti identita cliente - Start");
//
//				// CHECKLIST
////				TimeUnit.SECONDS.sleep(50);
//				logger.write("Attendo checklist - Start");
//				checkListModalComponent.attendiChecklist();
//				logger.write("Attendo checklist - Completed");
//			
//				logger.write("Verifica checklist e conferma - Start");
////				String interactionFrame = checkListModalComponent.clickOk();
//				checkListModalComponent.clickConferma();
//				logger.write("Verifica checklist e conferma - Completed");
//
//				// Salva NUMERO_RICHIESTA
////				prop.setProperty("NUMERO_RICHIESTA",
////						intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
//				SetPropertyRichiesta6.main(args);
//				logger.write("NUMERO_RICHIESTA="+prop.getProperty("NUMERO_RICHIESTA"));
	
//				// Inserimento e Validazione Indirizzo Esecuzione Lavori
				logger.write("Inserimento e Validazione Indirizzo Esecuzione lavori - Start");
				presa.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				//*** qui Ubiest FUNZIONA
//				presa.verificaIndirizzo(presa.verificaIndirizzoButton);
				presa.verificaIndirizzo(prop.getProperty("CAP"), presa.verificaIndirizzoButton);
				presa.press(presa.confermaIndirizzo);
				logger.write("Inserimento e Validazione Indirizzo Esecuzione lavori - Completed");
				TimeUnit.SECONDS.sleep(5);

				// Dati Fornitura
				TimeUnit.SECONDS.sleep(5);
				logger.write("Seleziona Commodity Fornitura - Start");
				presa.press(presa.radioCommodity);
				logger.write("Seleziona Commodity Fornitura - Completed");
				TimeUnit.SECONDS.sleep(5);	
				logger.write("Inserimento e Validazione Dati Fornitura - Start");
				if (!prop.getProperty("NUMERO_PDR_TIPO_1").isEmpty()) {
					presa.inserisciDatiFornitura1(prop.getProperty("NUMERO_PDR_TIPO_1"), prop.getProperty("POTENZIALITA_1"));
					}
				if (!prop.getProperty("NUMERO_PDR_TIPO_2").isEmpty()) {
					presa.inserisciDatiFornitura2(prop.getProperty("NUMERO_PDR_TIPO_2"), prop.getProperty("POTENZIALITA_2"));
					}
				if (!prop.getProperty("NUMERO_PDR_TIPO_3").isEmpty()) {
					presa.inserisciDatiFornitura3(prop.getProperty("NUMERO_PDR_TIPO_3"), prop.getProperty("POTENZIALITA_3"));
					}
				presa.inserisciTelefono(presa.telefono); //inserisce numero telefono se il campo è vuoto
				// Emissione Fattura Anticipata
				if (prop.getProperty("FATTURAZIONE_ANTICIPATA").contentEquals("SI")) {
					presa.FatturaAnticipata(prop.getProperty("FATTURAZIONE_ANTICIPATA"));
					}
				presa.press(presa.confermaFornitura);
				logger.write("Inserimento e Validazione Dati Fornitura - Start");
				presa.press(presa.confermaDatiFornitura);
				logger.write("Inserimento e Validazione Dati Fornitura - Completed");

				// Annullamento-Crea Offerta in fase di Data Entry
				if(prop.getProperty("ANNULLA_OFFERTA","").contentEquals("Y")){
					logger.write("Annullamento Ordine - Start");
					presa.press(presa.annullaOrdine);
					presa.press(presa.confermaAnnullaOrdine);
					logger.write("Annullamento Ordine - Completed");
				} else {
					
//					//logger.write("Compila Campi Sezione Modalità firma e Canale Invio - Start");
//					//presa.compilaCampiModalitaFirma(prop.getProperty("CANALE_INVIO_CONTRATTO"));			
//					//logger.write("Compila Campi Sezione Modalità firma e Canale Invio - Completed");

						logger.write("Compila Campi Sezione Modalità firma e Canale Invio - Indirizzo Forzato - Start");
						presa.compilaCampiModalitaFirmaIndirizzoForzato(prop.getProperty("CAP"), prop.getProperty("CANALE_INVIO_CONTRATTO"));			
						logger.write("Compila Campi Sezione Modalità firma e Canale Invio - Indirizzo Forzato - Completed");

					//Calcolo sysdate
					Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String data = simpleDateFormat.format(calendar.getTime()).toString();

					if(prop.getProperty("TIPOCLIENTE","").contentEquals("BUSINESS") || (prop.getProperty("TIPOCLIENTE","").contentEquals("PA"))){
						
						// Sezione CIG e CUP
						logger.write("Compila Campi Sezione CIG e CUP - Start");
						presa.compilaCampiCigCup(prop.getProperty("FLAG_136"),prop.getProperty("CIG",""),prop.getProperty("CUP",""));			
						logger.write("Compila Campi Sezione CIG e CUP - Completed");
						// Sezione Split Payment
						logger.write("Compila Campi Sezione Split Payment - Start");
						presa.compilaSplitPayment(prop.getProperty("SPLIT_PAYMENT"), data);			
						logger.write("Compila Campi Sezione Split Payment - Completed");
						
						if(prop.getProperty("TIPOCLIENTE","").contentEquals("PA")){
							
							// Sezione Fatturazione Elettronica
							logger.write("Compila Campi Sezione Fatturazione Elettronica - Start");
							presa.compilaFatturazioneElettronica(prop.getProperty("TIPOLOGIA_PUBBL_AMMIN"));			
							logger.write("Compila Campi Sezione Fatturazione Elettronica - Completed");
							
						}
					}


					// CREA ORDINE
					TimeUnit.SECONDS.sleep(5);
					logger.write("Bottone Crea Ordine - Start");
					presa.press(presa.creaOrdine);
					logger.write("Bottone Crea Ordine - Completed");
				}

			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
