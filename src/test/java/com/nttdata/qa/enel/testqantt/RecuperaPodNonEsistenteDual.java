package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Step;
import io.qameta.allure.aspects.StepsAspects;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;

public class RecuperaPodNonEsistenteDual {

	@Step("Recupero Pod da Workbench")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}

				boolean flagEle = false;
				boolean flagGas = false;

				SeleniumUtilities util = new SeleniumUtilities(driver);
				String pod = "";
				//recupero POD per ELE
				while(!flagEle) {
					pod = util.generateRandomPodNumberEnergia();
					String query = "SELECT ITA_IFM_POD__c FROM NE__Service_Point__c where ITA_IFM_POD__c='"+pod+"'";
					a.insertQuery(query);
					a.pressButton(a.submitQuery);
					if(a.verificaNoRecordFound()) {
						flagEle = true;
						prop.setProperty("POD_ELE", pod);	
						System.out.println(prop.getProperty("POD_ELE"));
						logger.write("Recupero POD ELE");
					}
				}
				//recupero POD per GAS
				while(!flagGas) {
					pod = util.generateRandomPodNumberGAS3();
					String query = "SELECT ITA_IFM_POD__c FROM NE__Service_Point__c where ITA_IFM_POD__c='"+pod+"'";
					a.insertQuery(query);
					a.pressButton(a.submitQuery);
					if(a.verificaNoRecordFound()) {
						flagGas = true;
						prop.setProperty("POD_GAS", pod);	
						System.out.println(prop.getProperty("POD_GAS"));
						logger.write("Recupero POD GAS");
					}
				}
				a.logoutWorkbench();



			}
			prop.setProperty("RETURN_VALUE", "OK");
		}	
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
