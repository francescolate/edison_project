package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_HomePage_ACB_ID_244 {
	
	public static void main(String[] args) throws Exception {
	
	Properties prop = null;
	
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
		
		logger.write("Launch the page and verify the fields and button  - Start");
		lpv.launchLink(prop.getProperty("LINK"));
		lpv.verifyComponentExistence(lpv.homePageClose);
		lpv.clickComponent(lpv.homePageClose);
		lpv.verifyComponentExistence(lpv.logoEnel);
		lpv.verifyComponentExistence(lpv.buttonAccetta);
		lpv.clickComponent(lpv.buttonAccetta);
		lpv.verifyComponentExistence(lpv.iconUser);
		lpv.clickComponent(lpv.iconUser);
		lpv.verifyComponentExistence(lpv.loginPage);
		lpv.verifyComponentExistence(lpv.username);
		lpv.verifyComponentExistence(lpv.password);
		lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
		logger.write("Launch the page and verify the fields and button  - Complete");
	
							
		PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
					
		logger.write("Enter login button without input  - Start");
		dfc.verifyComponentExistence(dfc.username);
		dfc.verifyComponentExistence(dfc.password);
		dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
		logger.write("Enter login button without input  - Complete");
		
		logger.write("Enter the username and password  - Start");
		dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
		dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
		logger.write("Enter the username and password  - Complete");
		
		logger.write("Click on login button  - Start");
		dfc.clickComponent(dfc.buttonLoginAccedi);
		logger.write("Click on login button  - Complete");
		
		dfc.verifyComponentExistence(dfc.homePageBSNPath);
		dfc.comprareText(dfc.homePageBSNPath, PrivateAreaDisattivazioneFornituraComponent.HOMEPAGE_BSNPATH, true);
		dfc.verifyComponentExistence(dfc.headerBSN);
		dfc.comprareText(dfc.headerBSN, PrivateAreaDisattivazioneFornituraComponent.HEADER_BSN, true);
		dfc.verifyComponentExistence(dfc.fornitureHeaderBSN);
		dfc.comprareText(dfc.fornitureHeaderBSN, PrivateAreaDisattivazioneFornituraComponent.FORNITUREHEADER_BSN, true);
		dfc.verifyComponentExistence(dfc.fornitureNumber);
		dfc.comprareText(dfc.fornitureNumber, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_NUMBER, true);
		dfc.verifyComponentExistence(dfc.fornitureLuce);
		dfc.comprareText(dfc.fornitureLuce, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_LUCE, true);
		
		
		dfc.verifyComponentExistence(dfc.fornituraDettaglio);
		dfc.comprareText(dfc.fornituraDettaglio, PrivateAreaDisattivazioneFornituraComponent.FORNITURA_DETTAGLIO, true);
		dfc.verifyComponentExistence(dfc.luogo);
		dfc.comprareText(dfc.luogo, PrivateAreaDisattivazioneFornituraComponent.LUOGO, true);
		dfc.verifyComponentExistence(dfc.luogoValue);
		dfc.comprareText(dfc.luogoValue, PrivateAreaDisattivazioneFornituraComponent.LUOGO_VALUE, true);
		dfc.verifyComponentExistence(dfc.tipologia);
		dfc.comprareText(dfc.tipologia, PrivateAreaDisattivazioneFornituraComponent.TIPOLOGIA, true);
		
		dfc.verifyComponentExistence(dfc.offerta);
		dfc.comprareText(dfc.offerta, PrivateAreaDisattivazioneFornituraComponent.OFFERTA, true);
		dfc.verifyComponentExistence(dfc.offeraValue);
		dfc.comprareText(dfc.offeraValue, PrivateAreaDisattivazioneFornituraComponent.OFFERTA_VALUE, true);
		
		dfc.verifyComponentExistence(dfc.stato);
		dfc.comprareText(dfc.stato, PrivateAreaDisattivazioneFornituraComponent.STATO, true);
		dfc.verifyComponentExistence(dfc.statoValue);
		dfc.comprareText(dfc.statoValue, PrivateAreaDisattivazioneFornituraComponent.STATO_VALUE, true);
		
		prop.setProperty("RETURN_VALUE", "OK");
		
	} catch (Exception e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
}	
		

}
