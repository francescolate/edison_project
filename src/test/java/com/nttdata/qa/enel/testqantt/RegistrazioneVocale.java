package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.RecordingObjectRuntime;
import com.nttdata.qa.enel.components.lightning.RegistrazioneVocaleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

//Il seguente modulo si occupa della creazione di un nuovo Cliente accedendo prima alla sezione Clienti dal menu a tendina del primo tab disponibile 
//Poi cliccando su Nuovo inserirà la tipologia di cliente , e compilerà tutti i campi necessari al completamento della creazione quali nomi, indirizzi, referenti etc..
public class RegistrazioneVocale {

	@Step("Creazione Nuovo Cliente")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
//			'Premere il pulsante "Registrazione Vocale" per navigare nella pagina di esecuzione della registrazione vocale; - avviare la registrazione utilizzando il pulsante "Avvia registrazione" e premere "Termina registrazione" per concluderla. Per proseguire senza integrazione con la barra telefonica, premere il tasto "Forza Vocal Order"
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			RegistrazioneVocaleComponent regVoc = new RegistrazioneVocaleComponent(driver);
			//driver.switchTo().parentFrame();
			//Premere il pulsante "Registrazione Vocale" per navigare nella pagina di esecuzione della registrazione vocale; 
			logger.write("Pulsante Registrazione Vocale - Start");
			regVoc.selezionaRegistrazioneVocale();	
			logger.write("Pulsante Registrazione Vocale - Stop");
			
			  //- avviare la registrazione utilizzando il pulsante "Avvia registrazione" e premere "Termina registrazione" per concluderla. Per proseguire senza integrazione con la barra telefonica, premere il tasto "Forza Vocal Order"	CRMT - AP05113
			logger.write("Pulsante Avvia Registrazione - Start");
			regVoc.selezionaAvviaRegistrazione();
			logger.write("Pulsante Avvia Registrazione - Completed");
			
			logger.write("Pulsante Termina Registrazione - Start");
			regVoc.selezionaTerminaRegistrazione();
			logger.write("Pulsante Termina Registrazione - Completed");
			
			logger.write("Pulsante Forza vocal order - Start");
			regVoc.Forzavocalorder();
			logger.write("Pulsante Forza vocal order - Completed");
			//	"Il sistema naviga nella vista di riepilogo dell'offerta. Vengono effettuate le seguenti valorizzazioni:  Stato Secondario (Case): ""Offer - Null / Draft""


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
