package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AttivazioneSDD_VerificaStatoCase {
	/**
	 *  verifica lo stato del case creato a valle del completamento del processo di ATTIVAZIONE SDD
	 *  @param NUMERO_RICHIESTA
	 *  @param ATT_SDD_STATO_ATTESO_CASE
	 *  @param ATT_SDD_SOTTOSTATO_ATTESO_CASE
	 *	@param ATT_SDD_STATO_ATTESO_SAP
	 *	@param	ATT_SDD_STATO_ATTESO_UDB"

	 */
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			String query=Costanti.Query_id_132;
			
			String idRichiesta=prop.getProperty("NUMERO_RICHIESTA");
			query=query.replace("@ID_RICHIESTA@", idRichiesta);



			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);
		
			Map<String,String> result=wb.EseguiQuery(query).get(1);
			

			String ITA_IFM_Subject__c=result.get("ITA_IFM_Subject__c");
			String ITA_IFM_POD__c=result.get("ITA_IFM_POD__c");	
			String Name=result.get("Name");	
			String ITA_IFM_Status__c=result.get("ITA_IFM_Status__c");	
			String ITA_IFM_BPM_ID__c=result.get("ITA_IFM_BPM_ID__c");	
			String ITA_IFM_SAP_Status__c=result.get("ITA_IFM_SAP_Status__c");	
			String ITA_IFM_UDB_Status__c=result.get("ITA_IFM_UDB_Status__c");	
			String Case_CaseNumber	=result.get("Case.CaseNumber");
			String Case_Status	=result.get("Case.Status");
			String Case_ITA_IFM_SubStatus__c=result.get("Case.ITA_IFM_SubStatus__c");


			prop.setProperty("ATT_SDD_ID_BPM",ITA_IFM_BPM_ID__c );
			prop.setProperty("ID_BPM",ITA_IFM_BPM_ID__c );
			prop.setProperty("ATT_SDD_OI_ID",Name );

			logger.write("Start Verifica Stato Case");
			String statoAttesoCase=prop.getProperty("ATT_SDD_STATO_ATTESO_CASE") ;
			String sottoStatoAttesoCase=prop.getProperty("ATT_SDD_SOTTOSTATO_ATTESO_CASE") ;
			String statoAttesoSap=prop.getProperty("ATT_SDD_STATO_ATTESO_SAP");
			String statoUDBAtteso=prop.getProperty("ATT_SDD_STATO_ATTESO_UDB");
			String subjectCaseAtteso="ATTIVAZIONE SDD";

			if(ITA_IFM_BPM_ID__c.contentEquals(""))
			{
				throw new Exception("Attezione, non è stato staccato l'ID BPM ");
			}
			
			if(!ITA_IFM_Subject__c.contentEquals(subjectCaseAtteso))
			{
				throw new Exception("Valore non corrispondente con valore atteso ");
			}

			if(!statoAttesoCase.contentEquals(Case_Status))
			{

				throw new Exception(String.format("Stato Case - Valore: %s - Valore atteso: %s",ITA_IFM_Status__c,statoAttesoCase));
			}
			if(!sottoStatoAttesoCase.contentEquals(Case_ITA_IFM_SubStatus__c))
			{
				throw new Exception(String.format("SottoStato Case - Valore: %s - Valore atteso: %s",Case_ITA_IFM_SubStatus__c,sottoStatoAttesoCase));

			}
			if(!statoAttesoSap.contentEquals(ITA_IFM_SAP_Status__c))
			{
				throw new Exception(String.format("Stato SAP - Valore: %s - Valore atteso: %s",ITA_IFM_SAP_Status__c,statoAttesoSap));
			}
			if(!statoUDBAtteso.contentEquals(ITA_IFM_UDB_Status__c))
			{
				throw new Exception(String.format("Stato UDB - Valore: %s - Valore atteso: %s",ITA_IFM_UDB_Status__c,statoUDBAtteso));
			}




			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}

}
