package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangeVoltageNoEstimate {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ServicesComponent services = new ServicesComponent(driver);
			
			System.out.println("Accessing servizi forniture page");
			logger.write("Accessing servizi forniture page - Start");
			services.clickComponent(services.servicesLeftMenuItem);
			logger.write("Accessing servizi forniture page - Completed");
			
			System.out.println("Checking supply services");
			logger.write("Checking supply services - Start");
			prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
			services.checkForSupplyServices(prop.getProperty("SUPPLYSERVICES"));
			logger.write("Checking supply services - Completed");
			
			System.out.println("Accessing voltage variation page");
			logger.write("Accessing voltage variation page - Start");
			services.waitForElementToDisplay(services.modificaPotenzaeoTensione);			
			services.clickComponent(services.modificaPotenzaeoTensione);
			logger.write("Accessing voltage variation page - Completed");
			
			System.out.println("Verifying voltage variation page text");
			logger.write("Verifying voltage variation page text - Start");
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);
			logger.write("Verifying voltage variation page text - Completed");
			
			System.out.println("Voltage variation access button click");
			logger.write("Voltage variation access button click - Start");
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageButton);
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			logger.write("Voltage variation access button click - Completed");
			
			System.out.println("Supply selection page verification");
			logger.write("Supply selection page verification - Start");
			Thread.sleep(5000);
			changePV.verifyComponentExistence(changePV.pageTitle, 180);
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText, 180);
			changePV.verifyComponentText(changePV.pageTitle, changePV.supplySelectionTitle);
			changePV.verifyComponentText(changePV.changePowerAndVoltageSubText, changePV.supplySelectionSubtitle);
			logger.write("Supply selection page verification - Completed");
			
			System.out.println("Selecting first possible supply");
			logger.write("Selecting first possible supply - Start");
			changePV.verifyComponentExistence(changePV.firstSupplyCheckBox);
			changePV.clickComponent(changePV.firstSupplyCheckBox);
			changePV.verifyComponentExistence(changePV.firstSupplyPod);
			String selectedPOD = driver.findElement(changePV.firstSupplyPod).getText();
			prop.setProperty("POD", selectedPOD);
			changePV.verifyComponentExistence(changePV.modifyTensionBtn);
			changePV.clickComponent(changePV.modifyTensionBtn);
			logger.write("Selecting first possible supply - Completed");
			
			System.out.println("Modifying voltage value");
			logger.write("Modifying voltage value - Start");
			changePV.changeDropDownValue(changePV.voltageValue, prop.getProperty("NEW_VOLTAGE"));
			changePV.verifyComponentExistence(changePV.voltageUpdateLbl);
			changePV.verifyComponentText(changePV.voltageUpdateLbl, changePV.voltageUpdateString);
			changePV.clickComponent(changePV.email);
			changePV.enterInputtoField(changePV.email, prop.getProperty("CONTACT_EMAIL"));
			changePV.clickComponent(changePV.voltageUpdateLbl);
			changePV.verifyComponentExistence(changePV.confirmEmail);
			changePV.clickComponent(changePV.confirmEmail);
			changePV.enterInputtoField(changePV.confirmEmail, prop.getProperty("CONTACT_EMAIL"));
			changePV.verifyComponentExistence(changePV.calculateQuote);
			changePV.clickComponent(changePV.calculateQuote);
			changePV.verifyComponentExistence(changePV.attendiPopUpTitle);
			changePV.verifyComponentText(changePV.attendiPopUp, ChangePowerAndVoltageComponent.Attendiqualchesecondo);
			logger.write("Modifying voltage value - Completed");
			
			System.out.println("Checking message");
			logger.write("Checking message - Start");
			changePV.verifyComponentExistence(changePV.messageBodyPart1, 90);
			changePV.verifyComponentText(changePV.messageBodyPart1, changePV.messagePart1);
			changePV.verifyComponentText(changePV.messageBodyPart2, changePV.messagePart2);
			changePV.verifyComponentText(changePV.messageBodyPart3, changePV.messagePart3);
			changePV.clickComponent(changePV.confermaBtn);
			logger.write("Checking message - Completed");
			
			System.out.println("Checking popup");
			logger.write("Checking popup - Start");
			changePV.verifyComponentExistence(changePV.inspectionPopupTitle);
			changePV.verifyComponentExistence(changePV.inspectionPopupBody);
			changePV.verifyComponentText(changePV.inspectionPopupTitle, changePV.inspectionPopupTitleText);
			changePV.verifyComponentText(changePV.inspectionPopupBody, changePV.inspectionPopupBodyText);
			logger.write("Checking popup - Completed");
			
			System.out.println("Checking results page");
			logger.write("Checking results page - Start");
			Thread.sleep(60000);
			changePV.verifyComponentExistence(changePV.resultsPageTitle);
			changePV.verifyComponentExistence(changePV.resultsPageBody);
			changePV.verifyComponentText(changePV.resultsPageTitle, changePV.resultsPageTitleText);
			changePV.verifyComponentText(changePV.resultsPageBody, changePV.resultsPageBodyText);
			changePV.verifyElementColor(changePV.esitoStepLbl, changePV.greenColor);
			changePV.verifyComponentExistence(changePV.fineBtn);
			changePV.clickComponent(changePV.fineBtn);
			logger.write("Checking results page - Completed");
			
			System.out.println("Checking return to  homepage");
			logger.write("Checking return to  homepage - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			By accessLoginPage = log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage);
			logger.write("Checking return to  homepage - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
