package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoSwitchAttivoEVONonResidenziale {

	@Step("Processo Switch Attivo EVO")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				SeleniumUtilities util = new SeleniumUtilities(driver);
				SceltaProcessoComponent switchAttivo = new SceltaProcessoComponent(driver);
				GestionePODperSWAEVO pod = new GestionePODperSWAEVO(driver);
				IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
				logger.write("Selezione processo Switch Attivo Evo - Start");
				
				TimeUnit.SECONDS.sleep(10);
//				String tipoReferente=driver.findElement(switchAttivo.tipologiaReferente).getText();
//				prop.setProperty("TIPO_REFERENTE", tipoReferente);
				switchAttivo.clickAllProcess();
				TimeUnit.SECONDS.sleep(2);
				switchAttivo.chooseProcessToStart(switchAttivo.switchAttivoEVO);

				logger.write("Selezione processo Switch Attivo Evo - Completed");

				logger.write("Identificazione interlocutore - Start");
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(
						driver);
				//identificaInterlocutore.insertDocumento("b");
				identificaInterlocutore.insertDocumentoNew("b");
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				logger.write("Identificazione interlocutore - Completed");

				// Dopo Identifica interlocutore Numero Documento --> passa direttamente al
				// Checkpoint senza richiedere i dati Documenti
				DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
				logger.write("Inserimento documenti identita cliente - Start");
				if(!prop.getProperty("TIPO_UTENZA").contentEquals("S2S")) {
					GestioneDocumentiIdentitaComponentEVO documenti = new GestioneDocumentiIdentitaComponentEVO(driver);
					
					documenti.inserisciDocumento(prop.getProperty("TIPO_DOCUMENTO"), prop.getProperty("NUMERO_DOCUMENTO"),
							prop.getProperty("RILASCIATO_DA"), prop.getProperty("RILASCIATO_IL"));
					
					documenti.conferma(documenti.confermaButton);
					logger.write("Inserimento documenti identita cliente - Completed");
					
				}
				//per il non residenziale in generale non si deve settare il delegato
				//if(!prop.getProperty("TIPO_UTENZA").contentEquals("S2S")) {
				if(util.exists(delega.confermaButton, 5)) {
					throw new Exception("La sezione del delegato non dovrebbe essere visibile per uno scenario non residenziale.");
				}
				// Aggiunto Bottone Conferma 6 luglio
				logger.write("Conferma Interlocutore - Start");
				delega.conferma(delega.confermaPagina);
				logger.write("Conferma Interlocutore - Completed");
				
				TimeUnit.SECONDS.sleep(3);
				CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
				logger.write("Attendo checklist - Start");
				checkListModalComponent.attendiChecklist();
				logger.write("Attendo checklist - Completed");
				TimeUnit.SECONDS.sleep(5);
				logger.write("Verifica checklist e conferma - Start");
				checkListModalComponent.clickConferma();
				logger.write("Verifica checklist e conferma - Completed");
				
				prop.setProperty("NUMERO_RICHIESTA",
						intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
				logger.write("NUMERO_RICHIESTA=" + prop.getProperty("NUMERO_RICHIESTA"));

				logger.write("Verifica pulsanti aggiungi e rimuovi fornitura disabilitati - Start");
				pod.verificaPulsantiFornituraDisabilitati();
				logger.write("Verifica pulsanti aggiungi e rimuovi fornitura disabilitati - Completed");
				logger.write("Inserimento Pod per verifica - Start");
				pod.inserisciPOD(prop.getProperty("POD"));
				logger.write("Inserimento Pod per verifica - Completed");
				logger.write("Inserimento e Validazione Indirizzo - Start");
				// Inserimento e Validazione Indirizzo
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
	
				// *** GESTIONE UBIEST ***************************************************************
				//pod.verificaIndirizzo(pod.verificaIndirizzoButton);
				pod.verificaIndirizzoNew(prop.getProperty("CAP"));
				logger.write("Inserimento e Validazione Indirizzo - Completed");
				TimeUnit.SECONDS.sleep(10);
				logger.write("Conferma Pod - Start");
				pod.confermaPod(pod.confermaPodButton);
				logger.write("Conferma Pod - Completed");
				TimeUnit.SECONDS.sleep(20);
				
				logger.write("Verifica popolamento tabella Forniture Inserite - Start");
				pod.verificaTabellaFornitureInserite("ELETTRICO", prop.getProperty("POD"),
						prop.getProperty("INDIRIZZO"), "OK", "OK");
				logger.write("Verifica popolamento tabella Forniture Inserite - Completed");

				// Inserimento Dettaglio Sito
				TimeUnit.SECONDS.sleep(10);
				logger.write("Valorizzazione e verifica dei campi Dettaglio Sito - Start");
				// Inserimento e Validazione Indirizzo
				pod.inserisciDettaglioSitoNonResidenziale(prop.getProperty("USO"), 
						prop.getProperty("SOCIETA_DI_VENDITA"), 
						prop.getProperty("MERCATO_DI_PROVENIENZA"), 
						prop.getProperty("CATEGORIA_MERCEOLOGICA"), 
						prop.getProperty("POTENZA"), 
						prop.getProperty("TENSIONE"), 
						prop.getProperty("TIPO_MISURATORE"), 
						prop.getProperty("CONSUMO_ANNUO"));
				
				// Click Conferma Dati Sito
//				TimeUnit.SECONDS.sleep(10);
//				
				pod.confermaDatiSito(pod.confermaDatiSitoButton);

				logger.write("Valorizzazione e verifica dei campi Dettaglio Sito - Completed");

				TimeUnit.SECONDS.sleep(5);
				logger.write("Conferma Fornitura - Start");
				pod.confermaForniture(pod.confermaFornitureButton);
				logger.write("Conferma Fornitura - Completed");

				TimeUnit.SECONDS.sleep(5);
				logger.write("Crea Offerta - Start");

				pod.creaOfferta(pod.creaOffertaButton);
				logger.write("Crea Offerta - Completed");

				TimeUnit.SECONDS.sleep(20);

			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
