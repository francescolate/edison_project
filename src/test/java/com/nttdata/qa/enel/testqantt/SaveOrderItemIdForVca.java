package com.nttdata.qa.enel.testqantt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SaveOrderItemIdForVca {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {



			WebDriver driver=WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util=new SeleniumUtilities(driver);
			
			
			//Navigo Fino a elementi dell'ordine
			util.scrollToElement(driver.findElement(By.xpath("//div[contains(text(),\"Elementi dell'Ordine\")]")));

			String idRichiesta=prop.getProperty("NUMERO_RICHIESTA");
			String xpath=String.format("//span[text()='Navigazione processo']/ancestor::article[//a[text()='%s']]",idRichiesta);
			
			List<WebElement> containers=driver.findElements(By.xpath(xpath));
			WebElement container=null;
			for (WebElement webElement : containers) {
				//System.out.println("IS VISIBLE: " + webElement.isDisplayed());
				if(webElement.isDisplayed())
				{
					container=webElement;

					Actions builder = new Actions(driver);
					Action mouseOverHome = builder.moveToElement(
							container.findElement(By.xpath("//div[contains(text(),\"Elementi dell'Ordine\")]"))

							).click().build();
					mouseOverHome.perform();

				}
			}
			//div/parent::div//ul/li[strong[text()='Attivazione']]/parent::ul/li[span[contains(text(),'SAP-ISU')]]/strong";
			//xpath="//div/parent::div//ul/li[strong[text()='@ATTIVITA@']]/parent::ul/li[span[contains(text(),'@CAMPO')]]/strong";
			xpath="//div/parent::div//ul/li[strong[text()='@ATTIVITA@']]/parent::ul/li/a";
			WebElement we=container.findElement(By.xpath(xpath.replace("@ATTIVITA@", "Attivazione")));

			
			String oi_Attivazione=we.getText();
			prop.setProperty("OI_ITEM_ATTIVAZIONE", oi_Attivazione);

			we=container.findElement(By.xpath(xpath.replace("@ATTIVITA@", "Cessazione")));
			String oi_Cessazione=we.getText();
			
			prop.setProperty("OI_ITEM_CESSAZIONE", oi_Cessazione);

			logger.write("OI_ITEM_ATTIVAZIONE: " + prop.getProperty("OI_ITEM_ATTIVAZIONE"));
			logger.write("OI_ITEM_CESSAZIONE: " + prop.getProperty("OI_ITEM_CESSAZIONE"));

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
