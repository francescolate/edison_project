package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SelezioneMercatoVolturaConAccolloBSN {
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            SezioneMercatoComponent mercato = new SezioneMercatoComponent(driver);


            logger.write("Verifica checklist e conferma - Start");
            gestione.checkSpinnersSFDC();
            checkListModalComponent.clickConferma();

            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Numero Richiesta :" + prop.getProperty("NUMERO_RICHIESTA"));
			logger.write("Verifica checklist e conferma - Completed");

            logger.write("check esistenza sezione 'Causale', selezione mercato 'Trasformazione/Fusione Azienda' e click su Conferma - Start");
            By label_SelezioneCausale = mercato.labelSelezioneCausale;
            mercato.verifyComponentExistence(label_SelezioneCausale);
            mercato.verificaSezioneCausaleBSN();
            mercato.selezionaCusale(prop.getProperty("CAUSALE"));
            By conferma = mercato.buttonConfermaSezioneCausale;
            mercato.clickComponentIfExist(conferma);
            gestione.checkSpinnersSFDC();
            logger.write("check esistenza sezione 'Causale', selezione mercato 'Trasformazione/Fusione Azienda' e click su Conferma - Completed");


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

    public static String checkListText = "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE:Che il cliente disponga dei dati del cliente uscente (CF, PI, Utenza) e dei dati del sito (POD/PDR)INOLTRE VERIFICARE:PER USO ABITATIVO-    Se il cliente vuole attivare il RID/SDD è necessario l’IBANPER USI DIVERSI DALL’ABITAZIONE-    Se il cliente vuole attivare il RID/SDD è necessario l’IBAN-    Se il cliente è una Pubb. Amm. è necessario il Codice Ufficio-    Se il cliente è soggetto a Legge 136 è necessario almeno uno tra CIG e CUP-    Se il cliente è soggetto a Split Payment è necessario indicare Data Inizio e Data Fine validità-    In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)INFORMAZIONI UTILI:In caso di Dual ricordati che le forniture dovranno avere stessi: USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO In caso di Multi ricordati che le forniture dovranno avere stessi: COMMODITY – USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO – PRODOTTO - CODICE UFFICIO (se previsto)Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto.  Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della voltura. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta    In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associataSCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"";
    public static String checkListTextSalvaguardia = "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE:Che il cliente disponga dei dati del cliente uscente (CF, PI, Utenza) e dei dati del sito (POD/PDR)Se il cliente è in Salvaguardia è necessaria l’Istanza di SalvaguardiaINOLTRE VERIFICARE:PER USO ABITATIVO-    Se il cliente vuole attivare il RID/SDD è necessario l’IBANPER USI DIVERSI DALL’ABITAZIONE-    Se il cliente vuole attivare il RID/SDD è necessario l’IBAN-    Se il cliente è una Pubb. Amm. è necessario il Codice Ufficio-    Se il cliente è soggetto a Legge 136 è necessario almeno uno tra CIG e CUP-    Se il cliente è soggetto a Split Payment è necessario indicare Data Inizio e Data Fine validità-    In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)INFORMAZIONI UTILI:In caso di Dual ricordati che le forniture dovranno avere stessi: USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO In caso di Multi ricordati che le forniture dovranno avere stessi: COMMODITY – USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO – PRODOTTO - CODICE UFFICIO (se previsto)Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto.  Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della voltura. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta    In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associataSCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"";
}
