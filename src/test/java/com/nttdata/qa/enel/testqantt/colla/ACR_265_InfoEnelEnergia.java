package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_265_InfoEnelEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitle, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			logger.write("Click on services -- Start");
			hm.clickComponent(hm.services);
			logger.write("Click on services -- Completed");

			ServicesComponent sc = new ServicesComponent(driver);
			sc.verifyComponentExistence(sc.serviceTitle);
			sc.verifyComponentExistence(sc.infoEnelEnergia);
			logger.write("Click on Info EnelEnergia on services -- Start");
			sc.clickComponent(sc.infoEnelEnergia);
			Thread.sleep(10000);
			logger.write("Click on Info EnelEnergia on services -- Completed");

			InfoEnelEnergiaMyComponent ic =new InfoEnelEnergiaMyComponent(driver);
			logger.write("Verify Info EnelEnergia page title and content -- Start");
			ic.verifyComponentExistence(ic.headingInfoEnergia);
			ic.comprareText(ic.headingInfoEnergia, ic.HEADING_INFOENERGIA, true);
			ic.comprareText(ic.titleInfoEnergia, ic.TITLE_INFOENERGIA, true);
			/*ic.comprareText(ic.subtitleInfoEnergia, ic.SUBTITLE_INFOENERGIA, true);
			ic.verifyComponentExistence(ic.attivaInfoEnergiaButton);*/
			
			ic.verifyComponentExistence(ic.esciButton);
			ic.comprareText(ic.subTitle2InfoEnergia, ic.SUBTITLE2INFOENERGIA, true);
			ic.verifyComponentExistence(ic.modificaInfoEnergiaButton);
			ic.comprareText(ic.seVoiDisattivareilServizioText, ic.SEVOIDISATTIVAREILSERVIZIOTEXT, true);
			logger.write("Verify Info EnelEnergia page title and content -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
