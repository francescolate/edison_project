package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ModificaOpzioniBollettaDiSintesiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class classe2343253 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModificaOpzioniBollettaDiSintesiComponent mobc = new ModificaOpzioniBollettaDiSintesiComponent(driver);	
					
			//STEP 6 Check step in page
			
			mobc.clickComponent(mobc.button_prosegui);
			
			Thread.sleep(2000);
			
			By current_step = mobc.current_step;
			
			mobc.verifyComponentExistence(current_step);

			By is_current_step_2 = mobc.is_current_step_2;
			
			mobc.baseTimeoutInterval = 3;
			if(!mobc.getElementExistance(is_current_step_2)) {
				throw new Exception("The process isn't at step 2.");
			};
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
