package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.Luce30SpringComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FinePaginaLuce30Spring {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Luce30SpringComponent fine =new Luce30SpringComponent(driver);
			
			logger.write("verifica presenza sezione di testo in basso alla pagina offerta luce 30 Spring con link VEDI TUTTE GAS - Start");
			By titolo_BannerFinePagina=fine.titoloBannerFinePagina;
			fine.verifyComponentExistence(titolo_BannerFinePagina);
			
			By sottotitolo_FinePagina=fine.sottotitoloFinePagina;
			fine.verifyComponentExistence(sottotitolo_FinePagina);
			
			By link_VediTuttoGas=fine.linkVediTuttoGas;
			fine.verifyComponentExistence(link_VediTuttoGas);
			
			By labelgas=fine.gas;
			fine.checkColor(labelgas, "White", "label gas");
			By testoinAlto_2=fine.testoinAlto2;
			fine.verificaTesto(testoinAlto_2,fine.testo_in_alto);
		
			By buttonDettaglioOfferta_2=fine.buttonDettaglioOfferta2;
			fine.verifyComponentExistence(buttonDettaglioOfferta_2);
			
			By labelSconto_2=fine.labelSconto2;
			fine.verifyComponentExistence(labelSconto_2);
			
			By testoinBasso_2=fine.testoinBasso2;
			fine.verificaTesto(testoinBasso_2,fine.testo_in_basso);
			
			By linkAttivaOra_2=fine.linkAttivaOra2;
			fine.verifyComponentExistence(linkAttivaOra_2);
			logger.write("verifica presenza sezione di testo in basso alla pagina offerta luce 30 Spring con link VEDI TUTTE GAS - Completed");
			
			logger.write("click sul link VEDI TUTTE GAS  e check sul motorino di ricerca - Start");
			fine.click(link_VediTuttoGas);
			fine.checkUrl(prop.getProperty("LINK")+fine.linkGAS_CASA);
			TimeUnit.SECONDS.sleep(2);
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);			
			By searchMenu=headerMenu.searchSection;
			headerMenu.verifyQuestionsExistence(searchMenu);
			By precedentcontract=headerMenu.contractListvalue;
			headerMenu.verifyTextSearchMenu(precedentcontract,"Gas");
			
			By precedentplace=headerMenu.placeListvalue;
			headerMenu.verifyTextSearchMenu(precedentplace,"Casa");
			
			By precedentneed=headerMenu.Listvalue;
			headerMenu.verifyTextSearchMenu(precedentneed,"Visualizza tutte");
			logger.write("click sul link VEDI TUTTE GAS  e check sul motorino di ricerca - Completd");
			
			logger.write("ritorno del browser alla pagina LUCE E GAS e check sul motorino di ricerca - Start");
			driver.navigate().back();
			TimeUnit.SECONDS.sleep(2);
			driver.navigate().back();
			TimeUnit.SECONDS.sleep(2);
			headerMenu.verifyQuestionsExistence(searchMenu);
		
			headerMenu.verifyTextSearchMenu(precedentcontract,prop.getProperty("TYPE_OF_CONTRACT"));
			
			headerMenu.verifyTextSearchMenu(precedentplace,prop.getProperty("PLACE"));
		
			headerMenu.verifyTextSearchMenu(precedentneed,prop.getProperty("NEED_PRECEDENTE"));
			
			logger.write("ritorno del browser alla pagina LUCE E GAS e check sul motorino di ricerca - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
