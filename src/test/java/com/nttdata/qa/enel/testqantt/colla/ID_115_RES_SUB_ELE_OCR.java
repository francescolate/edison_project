package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertagasocrComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_115_RES_SUB_ELE_OCR {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		

		try {
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			prop.setProperty("LUCEEGASLINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			OffertagasocrComponent oc =new OffertagasocrComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			PublicAreaComponent pa = new PublicAreaComponent(driver);
			Thread.sleep(5000);
			logger.write("select drop down value - Start");
			pa.changeDropDownValue(pa.productDropDown, pa.gas);
			pa.changeDropDownValue(pa.placeDropDown, pa.casa);
			pa.changeDropDownValue(pa.myselectionDD, pa. visualiza);
			logger.write("Click on inizia ora - Start");
			pa.clickComponent(pa.iniziaOra);
			Thread.sleep(3000);
			logger.write("Click on inizia ora - Completed");
			pa.checkURLAfterRedirection(pa.detaglioOffertgasaUrl);
			log.launchLink("https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/valore-luce-plus");
			
			oc.clickComponent(oc.attivaSubito);
			logger.write("Verify page navigation to attiva Offerta page and page details -- start");
			//driver.navigate().to(oc.offertaUrlgas);
						
			oc.verifyComponentExistence(oc.aderisci);
			//oc.checkURLAfterRedirection(oc.offerUrlgas115);
			oc.comprareText(oc.offerHeading, oc.OFFER_HEADING, true);
			oc.comprareText(oc.offerParagraph, oc.OFFER_PARAGRAPH, true);
			oc.verifyComponentExistence(oc.compilaManualmente);
			oc.comprareText(oc.parapetText, oc.PARAPET_TEXT, true);
			oc.comprareText(oc.offerta, oc.OFFERTA, true);
			oc.comprareText(oc.ValoreLucePlus, oc.VALORE_LUCE_PLUS, true);
			oc.comprareText(oc.contratto, oc.CONTRATTO, true);
			oc.comprareText(oc.prezzoLuce, oc.PREZZO_LUCE, true);
			oc.comprareText(oc.bolletteWebText, oc.BOLLETTEWEB_TEXT, true);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");
			
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Start");
			oc.clickComponent(oc.compilaManualmente);
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(2000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(25000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");
			
			logger.write("Enter the values in informazioniFornitura section and click on continue-- Started");
			oc.comprareText(oc.informazioniFornitura116, oc.INFORMAZIONI_FORNITURA, true);
			oc.comprareText(oc.cambioFornitura, oc.CAMBIO_FORNITORE, true);
			oc.comprareText(oc.cambioFornituraText, oc.CAMBIO_FORNITORE_TEXT, true);
			oc.comprareText(oc.subentro115, oc.SUBENTRO, true);
			oc.comprareText(oc.subentroText, oc.SUBENTRO_TEXT, true);
			oc.comprareText(oc.primaAttivazione, oc.PRIMA_ATTIVAZIONE, true);
			oc.comprareText(oc.primaAttivazionetext, oc.PRIMA_ATTIVAZIONE_TEXT,true);
			oc.comprareText(oc.volutra, oc.VOLTURA, true);
			oc.comprareText(oc.volutraText, oc.VOLTURA_TeXT, true);
			oc.comprareText(oc.confermailCap, oc.CONFERMAILCAP, true);
			oc.verifyComponentExistence(oc.pod);
			oc.verifyComponentExistence(oc.cap115);
			oc.comprareText(oc.privacyText, oc.PrivacyText, true);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.verifyComponentExistence(oc.inseriscilodopo);
			logger.write("Enter the values in informazioniFornitura section and click on continue-- Completed");
			
			logger.write("select the values in SUBENTRO and validate the information is displayed correctly-- Start");
			oc.clickComponent(oc.subentro115);
			
			oc.verifyComponentExistence(oc.pdr115);
			oc.verifyComponentExistence(oc.iban115);
			oc.verifyComponentExistence(oc.Datidellimmobile115);
			oc.verifyComponentExistence(oc.Cartadidentità115);
			oc.enterInputParameters(oc.pod, prop.getProperty("PDR"));
			oc.enterInputParameters(oc.cap115, prop.getProperty("CAP"));
			oc.clickComponent(oc.cap115);
			//oc.clickComponent(oc.capdropdown);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.clickComponent(oc.prosegui1);
			logger.write("select the values in SUBENTRO and validate the information is displayed correctly-- Completed");
			
			logger.write("Verify the values in indirizzo Di Fornitura section-Starts");
			oc.comprareText(oc.indirizzoDiFornitura, oc.INDIRIZZO_DI_FORNITURA, true);
			oc.verifyComponentExistence(oc.citta115);
			oc.verifyComponentExistence(oc.indrizzo115);
			oc.verifyComponentExistence(oc.numeroCivico115);
			oc.verifyComponentExistence(oc.indrizzoDiFornituraCAP115);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText, oc.IndrizzoDiFornituraPrivacyText, true);
					 
			Thread.sleep(5000);
			
			oc.verifyComponentExistence(oc.recapti115);

			oc.verifyComponentExistence(oc.recapitians1);
			oc.verifyComponentExistence(oc.recapitians2);
			oc.comprareText(oc.recapti115Text, oc.RECAPTI_TEXT, true);
			oc.comprareText(oc.doveVuoi, oc.Recapitiqst2, true);
			oc.comprareText(oc.recapitiansOne, oc.RecapitiansOne, true);
			oc.comprareText(oc.recapitiansTwo, oc.RecapitiansTwo, true);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.verifyComponentExistence(oc.salvaEContinuaDopo);
			logger.write("Verify the values in indirizzo Di Fornitura section-Completed");
			
			logger.write("Enter the values in indirizzo Di Fornitura section-Starts");
			oc.enterInputParameters(oc.citta115, prop.getProperty("CITTA"));
			driver.findElement(oc.citta115).sendKeys(Keys.TAB);
			oc.enterInputParameters(oc.indrizzo115, prop.getProperty("INDRIZZO"));
			driver.findElement(oc.indrizzo115).sendKeys(Keys.TAB);
			oc.enterInputParameters(oc.numeroCivico115, prop.getProperty("NUMEROCIVICO"));
			driver.findElement(oc.numeroCivico115).sendKeys(Keys.TAB);
			oc.clickComponent(oc.prosegui1);
			
			
			oc.verifyComponentExistence(oc.pagamentiEBollette);
			oc.verifyComponentExistence(oc.Metodo_di_Pagamento_Field);
			oc.verifyComponentExistence(oc.Codice_IBAN);
			oc.verifyDefaultValue(oc.Metodo_di_Pagamento_Bolletino, oc.Bolletino_selection115);
			logger.write("Enter the values in indirizzo Di Fornitura section-Completed");
			
			oc.verifyComponentExistence(oc.Modalità_di_ricezione_Heading);
			oc.checkPrePopulatedValueAndCompare(oc.ModalitaricezioneEmailbollete, prop.getProperty("EMAIL"));
			oc.checkPrePopulatedValueAndCompare(oc.ModalitaricezioneTelefonobollete, prop.getProperty("CELLULARE"));
			oc.verifyComponentExistence(oc.selectedCheckbox);
			oc.verifyComponentExistence(oc.unselectedCheckbox);
			oc.verifyComponentExistence(oc.Codici_Promozionali_DiscountCode);
			oc.comprareText(oc.codicePromoText, oc.CODICE_PROMO_TEXT, true);
			
			oc.clickComponent(oc.choosePayment);
			oc.clickComponent(oc.Metodo_di_Pagamento_Bolletino);
			oc.isElementPresent(oc.Codice_IBAN);
			oc.isElementPresent(oc.sonoRadioButton1);
			oc.isElementPresent(oc.sonoRadioButton2);
			oc.comprareText(oc.ricordaText, oc.RICORDA_TEXT, true);
			oc.clickComponent(oc.prosegui1);
			
			oc.comprareText(oc.Consensi_chkbx1_Text, oc.Consensi_chkbx1Text, true);
			oc.comprareText(oc.Consensi_chkbx2_Text, oc.chkbx2Text, true);
			oc.verifyComponentExistence(oc.Richiesta_Heading);
			oc.verifyComponentExistence(oc.Richiesta_LinkText);
			oc.comprareText(oc.Richiesta_TextInBox, oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text, true);			
			oc.verifyComponentExistence(oc.Richiesta_SI);
			oc.verifyComponentExistence(oc.Richiesta_Di_NO);
			oc.clickComponent(oc.Richiesta_LinkText);
			
			oc.clickComponent(oc.Consensi_chkbx1_Text);
			oc.clickComponent(oc.Consensi_chkbx2_Text);
					
			
			oc.verifyComponentExistence(oc.Enel_Energia_Accetto);
			oc.verifyComponentExistence(oc.Enel_Energia_NonAccetto);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_Heading);
			oc.comprareText(oc.Consenso_Marketing_Terzi_TextInBox, oc.Consenso_Marketing_Terzi_Text, true);
			oc.verifyComponentExistence(oc.Terzi_Accetto);
			oc.verifyComponentExistence(oc.Terzi_NonAccetto);
			oc.verifyComponentExistence(oc.Consenso_Profilazione_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Profilazione_Enel_Energia_TextInBox, oc.Consenso_Profilazione_Enel_Energia_Text, true);
			oc.verifyComponentExistence(oc.Terzi_NonAccetto);
			oc.clickComponent(oc.Enel_Energia_Accetto);
			oc.clickComponent(oc.Terzi_Accetto);
			oc.clickComponent(oc.Energia_Accetto);
			
			oc.clickComponent(oc.Richiesta_Di_NO);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupContent, oc.Richiesta_Di_Esecuzione_POPUP_Content, true);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupClose);
			
			oc.clickComponent(oc.COMPLETA_ADESIONE_Button);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Start");
			Thread.sleep(10000);
			oc.comprareText(oc.tornaAllaHomePageTitle, oc.TornaAllaHomePageTitle, true);
			oc.provideEmail(prop.getProperty("EMAIL"));
			oc.clickComponent(oc.TornaAllaHomeButton);
			Thread.sleep(5000);
			oc.checkURLAfterRedirection(prop.getProperty("WP_LINK"));
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
