package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.WebPrivatoDettaglioFornituraInAttivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class WebPrivatoDettaglioFornituraLavoriSulContatore {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			WebPrivatoDettaglioFornituraInAttivazioneComponent wpdfia = new WebPrivatoDettaglioFornituraInAttivazioneComponent(driver);
			wpdfia.checkDocumentReadyState();
			
			logger.write("Controllando se la fornitura esiste e clicca sul button Visualizza Stato - START");
			wpdfia.verifyComponentExistence(By.xpath(wpdfia.Visualizzalebollette.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			wpdfia.clickComponent(By.xpath(wpdfia.Visualizzalebollette.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			logger.write("Controllando se la fornitura esiste e clicca sul button Visualizza Stato - COMPLETED");
			
		/*	By track_item_active = new ByChained(By.xpath("//div[@id='sc']") , By.xpath("//li"));
			
			wpdfia.verifyComponentExistence(track_item_active);
			
			By title = By.xpath("//h3");
			
			By track_item_active_title = new ByChained(track_item_active , title);
			
			wpdfia.verifyComponentExistence(track_item_active_title);
			
			By desc = By.xpath("//p");
			
			By track_item_active_desc = new ByChained(track_item_active , desc);
			
			wpdfia.verifyComponentExistence(track_item_active_desc);*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
