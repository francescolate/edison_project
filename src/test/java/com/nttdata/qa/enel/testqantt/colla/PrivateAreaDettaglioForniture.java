package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.WebPrivatoDettaglioFornituraInAttivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaDettaglioForniture {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			prop.setProperty("FORNITURE_HEADER", "Le tue forniture");
		    prop.setProperty("FORNITURE_HEADER_TEXT", "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette");
			PrivateAreaDettaglioFornitureComponent padf = new PrivateAreaDettaglioFornitureComponent(driver);
			
//			logger.write("checking the menu item Bolletta is present within the page - START");	
//			padf.verifyComponentExistence(padf.fornitureMenuItem);
//			padf.clickComponent(padf.fornitureMenuItem);
//			logger.write("Bolletta menu item click  - COMPLETED");
			
			logger.write("checking the text contained within the header of the page - START");

			By header = By.xpath(padf.element_by_text_and_parent_class.replace("#2", padf.forniture_header_class).replace("#1", prop.getProperty("FORNITURE_HEADER")));
			
			padf.verifyComponentExistence(header);
			logger.write("header text checking - COMPLETED");
			
			logger.write("checking the text contained below the header of the page - START");
			
			By headerText = By.xpath(padf.element_by_text_and_parent_class.replace("#2", padf.forniture_header_class).replace("#1", prop.getProperty("FORNITURE_HEADER_TEXT")));
			
			padf.verifyComponentExistence(headerText);
			
			logger.write("below header text checking - COMPLETED");
			
			logger.write("clicking the card details button by the POD - START");
						
			//padf.clickComponent(padf.pod);
			padf.clickComponent(By.xpath("//*[text()='In Lavorazione']//ancestor::div[@class='card forniture regolare']//div[@class='button-container']"));

			
			WebPrivatoDettaglioFornituraInAttivazioneComponent wpdfia = new WebPrivatoDettaglioFornituraInAttivazioneComponent(driver);
			
			logger.write("controllando la presenza dell'icona della fornitura associata al test - START");
			wpdfia.verifyComponentExistence(wpdfia.supplyIcon);
			logger.write("controllando la presenza dell'icona della fornitura associata al test - COMPLETED");
			
			logger.write("confrontando i dettagli della fornitura - START");
			wpdfia.containsText(wpdfia.supplyDetailsHeading, wpdfia.supplyAddressLuce);
			wpdfia.containsText(wpdfia.supplyDetailsHeading, wpdfia.customerNumber);
			logger.write("confrontando i dettagli della fornitura - COMPLETED");
			
			logger.write("verificando tutti i dettagli della fornitura - START");
			wpdfia.clickComponent(wpdfia.mostraDiPiu);
			wpdfia.compareText(wpdfia.supplyDetailContainer, wpdfia.statoFornitura + prop.getProperty("STATO_FORNITURA") 
										+ wpdfia.tipoAttivazione + prop.getProperty("TIPO_ATTIVAZIONE") 
										+ wpdfia.dataRichiesta + prop.getProperty("DATA_RICHIESTA") 
										+ wpdfia.pod + prop.getProperty("POD") 
										+ wpdfia.offertaAttiva + prop.getProperty("OFFERTA_ATTIVA") 
										+ wpdfia.numeroOfferta + prop.getProperty("NUMERO_OFFERTA") 
										+ wpdfia.sottoscrizione + prop.getProperty("SOTTOSCRIZIONE") 
										+ wpdfia.contratto + prop.getProperty("CONTRATTO") , true);				
			//wpdfia.compareText(wpdfia.lavoriSulContatoreHeading, wpdfia.lavoriSulContatoreText, true);
			//wpdfia.compareText(wpdfia.preventivoContainer, wpdfia.preventivoText, true);
			wpdfia.compareText(wpdfia.statoDiAtticazioneContainer, wpdfia.statoDiAttivazioneText, true);
			logger.write("verificando tutti i dettagli della fornitura - COMPLETED");
			
			logger.write("verificando stato attivazione della fornitura - START");
			wpdfia.containsText(wpdfia.tracking, wpdfia.statoRichiestaAttivazione_2);
			logger.write("verificando stato attivazione della fornitura - COMPLETED");
			
		
			/*
			
			By button_by_pod = By.xpath(padf.button_by_pod.replace("#1", prop.getProperty("ITA_IFM_POD__c")));
			
			padf.verifyComponentExistence(button_by_pod);
			padf.clickComponent(button_by_pod);
			
			logger.write("clicking the card details button by the POD - COMPLETED");
//			By link_open_supply = padf.link_open_supply;
//			
//			padf.verifyComponentExistence(link_open_supply);
//			padf.clickComponent(link_open_supply);
			
			logger.write("check current activation status text - START");
			
			By text_status_active = padf.text_status_active;
			
			padf.verifyComponentExistence(text_status_active);
			padf.verifyComponentText(text_status_active, prop.getProperty("ACTIVATION_STATUS"));		
			
			logger.write("check current activation status text - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			*/
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
