package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;


public class RecuperaDatiRicontrattualizzazioneDUAL {
	
	/*
	 * 
	 * Il seguent modulo si occupa di effettuare una prima query nel workbench
	 * Salvando i dati della prima (ed unica) colonna, intervallati da virgola
	 * Da dare in pasto come condizione In('valore','valore','valore') 
	 * ed estrarne poi uno risultato da salvare nel properties
	 * 
	 * 
	 */

	private final static int SEC = 120;
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate(Costanti.workbenchLink);
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				a.pressButton(a.checkAgree);
				a.pressButton(a.buttonLogin);
				TimeUnit.SECONDS.sleep(2);
				//System.out.println(driver.getCurrentUrl());
				while (!driver.getCurrentUrl().startsWith(Costanti.workbenchLink)) {
					page.enterUsername(Costanti.workbenchUser);
					page.enterPassword(Costanti.workbenchPwd);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}
				
				a.AbilitaJoin();
				
				a.inserisciNuovaQuery();
				//Conto il numero di query da effettuare
				int numQuery = 0;
				for(int x = 1;prop.containsKey("QUERY_"+x); x++) {
					numQuery = x;
				}
				
				boolean condition = false;
				int tentativi = 2;
				
				a.insertQuery(prop.getProperty("QUERY_1"));
				condition = false;

				while (!condition && tentativi-- > 0) {
					a.pressButton(a.submitQuery);
					condition = a.aspettaRisultati(SEC);
				}
				if (condition) {
					String inCondition = a.costruisciStringaRisultati();
					String newQuery = String.format(prop.getProperty("QUERY_2"), inCondition);
					a.recuperaRisultati(Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE","20")), prop);
					prop.setProperty("QUERY_2", newQuery);
				}else
					throw new Exception("Impossibile recuperare i risultati dalla query workbench "+prop.getProperty("QUERY_1"));
		
		
				a.insertQuery(prop.getProperty("QUERY_2"));
				condition = false;
				tentativi = 2;
				while (!condition && tentativi-- > 0) {
					a.pressButton(a.submitQuery);
					condition = a.aspettaRisultati(SEC);
				}
				if (condition) {
					String inCondition = a.costruisciStringaRisultati();
					String newQuery = String.format(prop.getProperty("QUERY_2"), inCondition);
					a.recuperaRisultati(Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE","9")), prop);

				}else
					throw new Exception("Impossibile recuperare i risultati dalla query workbench "+prop.getProperty("QUERY_2"));
		
					prop.setProperty("NOME", prop.getProperty("ACCOUNT.NAME"));
					prop.setProperty("CODICE_FISCALE", prop.getProperty("ACCOUNT.NE__FISCAL_CODE__C"));
					prop.setProperty("POD_GAS", prop.getProperty("QUERY_NE__SERVICE_POINT__C.ITA_IFM_POD__C"));
					prop.setProperty("NUMERO_UTENZA_GAS", prop.getProperty("ITA_IFM_ConsumptionNumber__c".toUpperCase()));
					prop.setProperty("PRODOTTO_GAS_ESISTENTE", prop.getProperty("NE__Product__c.Name".toUpperCase()));
					prop.setProperty("MERCATO_GAS", prop.getProperty("NE__Product__c.ITA_IFM_Regime__c".toUpperCase()));
					prop.setProperty("INDIRIZZO_GAS", prop.getProperty("NE__Service_Point__c.ITA_IFM_Concatenated_Supply_Address__c".toUpperCase()));
					
				
					a.insertQuery(prop.getProperty("QUERY_3").replaceAll("%s", prop.getProperty("ACCOUNT.ID")));
					condition = false;
					tentativi = 2;
					while (!condition && tentativi-- > 0) {
						a.pressButton(a.submitQuery);
						condition = a.aspettaRisultati(SEC);
					}
					if(condition) {
						a.recuperaRisultati(Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE","1")), prop);
						a.logoutWorkbench();
					}else
					throw new Exception("Impossibile recuperare i risultati dalla query workbench "+prop.getProperty("QUERY_"+numQuery));

					prop.setProperty("POD_ELE", prop.getProperty("QUERY_NE__SERVICE_POINT__C.ITA_IFM_POD__C"));
					prop.setProperty("NUMERO_UTENZA_ELE", prop.getProperty("ITA_IFM_ConsumptionNumber__c".toUpperCase()));
					prop.setProperty("PRODOTTO_ELE_ESISTENTE", prop.getProperty("NE__Product__c.Name".toUpperCase()));
					prop.setProperty("MERCATO_ELE", prop.getProperty("NE__Product__c.ITA_IFM_Regime__c".toUpperCase()));
					prop.setProperty("INDIRIZZO_ELE", prop.getProperty("NE__Service_Point__c.ITA_IFM_Concatenated_Supply_Address__c".toUpperCase()));

						
			
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}