package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CompletaTP2Component;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.VerificaContrattoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ConfermaTP2SubentroNew {

	public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            VerificaContrattoComponent offer=new VerificaContrattoComponent(driver);
            CompletaTP2Component CompletaTP2Component = new CompletaTP2Component(driver);
            logger.write("Processo TP2 - conferma sezioni");
            /*
            CompletaTP2Component.clickComponentIfExist(CompletaTP2Component.acceptCookie);
            
            logger.write("Conferma Sezione Riepilogo - Started");
            driver.switchTo().defaultContent();
            CompletaTP2Component.switchFrameEClicca(CompletaTP2Component.confermaButtonTp2old);
            logger.write("Conferma Sezione Riepilogo - Completed");
            
            if(!prop.getProperty("OPZIONE_FIBRA","").equals("")) {
            CompletaTP2Component.switchFrameEClicca(CompletaTP2Component.inviaRichiestaFibra);
            }
            // clicca prosegui al banner "Ci siamo quasi!"
            
            driver.switchTo().defaultContent();
            CompletaTP2Component.clickComponent(CompletaTP2Component.proseguiButtonTp2);

            
            //inserimento sezione "COMPILAZIONE DOCUMENTI"
            
            logger.write("Sezione Compilazione Documenti - Start");
            CompletaTP2Component.jsClickComponentNew(CompletaTP2Component.labelAbitazione);          
           
            CompletaTP2Component.jsClickComponentNew(CompletaTP2Component.labelProprieta);
            
            CompletaTP2Component.switchFrameEClicca(CompletaTP2Component.confermaButtonTp2);*/
            logger.write("Sezione Compilazione Documenti - Completed");		
            logger.write("corretto accesso alla pagina di carica documenti - Completed");
			DocumentiUploadComponent doc=new DocumentiUploadComponent(driver);
		  
			
			CompletaTP2Component.jsClickComponentNew(CompletaTP2Component.buttonCaricaDocumentoRiconoscimento);
		    offer.verifyComponentExistence(doc.fileUpload2);
		   
		 
		    logger.write("Upload Documento di riconoscimento - Start");
		    TimeUnit.SECONDS.sleep(15);
			doc.uploadGenericFile(
					Utility.getDocumentPdf(prop),doc.fileUpload2
					);
			
			logger.write("Upload Documento di riconoscimento - Completed");

			logger.write("Invio Documento di Riconoscimento - Start");
			CompletaTP2Component.switchFrameEClicca(CompletaTP2Component.invioDocumenti);
			logger.write("Invio Documento di Riconoscimento - Completed");
                             

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
