package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class LoginSalesForceProduzione {

	@Step("Login Salesforce Produzione")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		try {
			
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				
				Date d = new Date();
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	            String s = fmt.format(d);
	            prop.setProperty("START_TIME", s);
				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate(prop.getProperty("LINK"));
				//ReportUtility.log(prop.getProperty("LOG_PATH"), "Navigazione portale Salesforce", prop.getProperty("LINK"), "PASSED", " ");
				logger.write("Naviga a Link");
				page.enterField(By.id("userNameInput"),prop.getProperty("USERNAME"));
				//ReportUtility.log(prop.getProperty("LOG_PATH"), "Inserimento username", prop.getProperty("USERNAME"), "PASSED", " ");
				logger.write("Inserimento Username");
				
				page.enterField(By.id("passwordInput"),prop.getProperty("PASSWORD"));
				//ReportUtility.log(prop.getProperty("LOG_PATH"), "Inserimento password", prop.getProperty("PASSWORD"), "PASSED", " ");
				logger.write("Inserimento Password");
				
				page.pressButton(By.id("submitButton"));
				//ReportUtility.log(prop.getProperty("LOG_PATH"), "Accedi al portale", " ", "PASSED", " ");
				logger.write("Button Accedi al portale");
				
				// System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
            if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
}
