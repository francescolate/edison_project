package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GetAccountDetailsComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class GetAccountDetails163 {

	@Step("Recupero Pod da Workbench")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			prop.setProperty("ACCOUNT_ID", "0010Y00000FMWwtQAH");
			
			GetAccountDetailsComponent gadc = new GetAccountDetailsComponent(driver);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
			String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            System.out.println(date);
            date = date+"T00:00:00.00Z";
           	wbc.insertQuery(Costanti.get_query_163a.replace("$TODAY$",date ));
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			
//			gadc.verifyAccountDetails(gadc.queryResultFields, GetAccountDetailsComponent.QUERY_FIELDS_163a,gadc.QUERY_VALUES_163a);
			Thread.sleep(5000);
			
			gadc.clearText(gadc.queryInputField);
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.get_query_163b.replace("$ACCOUNT_ID$", prop.getProperty("ACCOUNT_ID")));
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			
//			gadc.verifyAccountDetails(gadc.queryResultFields, GetAccountDetailsComponent.QUERY_FIELDS_163b, gadc.QUERY_VALUES_163b);
			Thread.sleep(5000);
			
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
