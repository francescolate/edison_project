package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.FibraNoCover_ResComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FibraNoCover_Res {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("Verifying Fibra activation in progress - Start");
			FibraNoCover_ResComponent noCover = new FibraNoCover_ResComponent(driver);
			//noCover.checkFiberCardPresence();
			logger.write("Verifying Fibra activation in progress - Completed");
			
			logger.write("Show offer - Start");
			noCover.verifyVisibilityThenClick(noCover.showOfferBtn);
			logger.write("Show offer - Completed");
			
			/*logger.write("Verifying info page - Start");
			noCover.checkInfoPageStrings();
			logger.write("Verifying info page - Completed");*/
			
			logger.write("Verifying impossibility to continue - Start");
			//noCover.verifyVisibilityThenClick(noCover.continueBtn);
			Thread.currentThread().sleep(15000);
			noCover.switchFrame(noCover.melitaFrame);
		//	noCover.comprareText(noCover.popupHeader, noCover.popupHeaderText, true);
		//	noCover.comprareText(noCover.popupBody, noCover.popupBodyText, true);
			logger.write("Verifying impossibility to continue - Completed");
			
		
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
