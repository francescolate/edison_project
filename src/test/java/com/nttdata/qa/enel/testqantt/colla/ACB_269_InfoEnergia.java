package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_269_InfoEnergia {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
					
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepageHeadingRES, InfoEnelEnergiaACRComponent.HOMEPAGE_HEADING_RES, true);
			iee.comprareText(iee.homepageParagraphRES, InfoEnelEnergiaACRComponent.HOMEPAGE_PARAGRAPH_RES, true);
			iee.comprareText(iee.homepageSectionHeadingRES, InfoEnelEnergiaACRComponent.HOMEPAGE_SECTION_HEADING_RES, true);
			iee.comprareText(iee.homepageSectionParagraphRES, InfoEnelEnergiaACRComponent.HOMEPAGE_SECTION_PARAGRAPH_RES, true);
			logger.write("Correct visualization of the Home Page with text- Ends");
			
			logger.write("Click on dettaglio Fornitura and verify the data- Starts");
			iee.clickComponent(iee.gestisciLeOreFree);
			Thread.sleep(5000);
			iee.comprareText(iee.serviziPerLeFornitura, InfoEnelEnergiaACRComponent.SERVIZI_PER_LE_FORNITURE, true);
			logger.write("Click on dettaglio Fornitura and verify the data- Ends");
			
			logger.write(" Click on Info Enel Energia and verify Correct display error page- Starts");
			iee.clickComponent(iee.infoEnelEnergiaTile);
			Thread.sleep(5000);
			iee.comprareText(iee.errorPageHeading, InfoEnelEnergiaACRComponent.ERROR_PAGE_HEADING, true);
			logger.write(" Click on Info Enel Energia and verify Correct display error page- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
