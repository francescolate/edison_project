package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.List;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CambiaProdottoNonResidenziale {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			
			
			if(prop.getProperty("COMMODITY").contentEquals("GAS")) {
				logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Start");
				driver.switchTo().defaultContent();
				String frame = configura.switchToFrame();
				configura.clickWithJS(configura.cambiaProdotto);
			    //configura.press(configura.cambiaProdotto);
			    driver.switchTo().defaultContent();
				TimeUnit.SECONDS.sleep(30);
				//configura.switchToFrame2();
				configura.switchToFrame();
			//modifica codice data 15/10/2020 per gestione cliente gas business
				
				configura.pressResidenziale(configura.linkGasNonResidenziale);
				
						
				// da modificare per adattare prodotto non residenziale
				configura.modificaProdotto(prop.getProperty("PRODOTTO"));	
				driver.switchTo().defaultContent();
				TimeUnit.SECONDS.sleep(30);
				configura.avantiProdotto();
				driver.switchTo().defaultContent();
				logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Completed");
				
				}
			
			if(prop.getProperty("COMMODITY").contentEquals("ELE")) {
	
				logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Start");
			driver.switchTo().defaultContent();
			String frame = configura.switchToFrame();
		    configura.clickComponentWithJseAndCheckSpinners(configura.cambiaProdotto);
		    driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(30);
					
			//configura.switchToFrame();
			configura.press(configura.linkElettricoNonResidenziale);
			configura.modificaProdottoOpzioneKAMAGCOR(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_KAM"));
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(30);
			configura.switchToFrame();
			configura.avantiProdotto();
			driver.switchTo().defaultContent();
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Completed");
			
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
