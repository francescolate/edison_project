package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;



public class CheckSezioneContattiConsensi {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			RiepilogoOffertaComponent contatti = new RiepilogoOffertaComponent(driver);
			
			logger.write("Check sezione Contatti e Consensi - Start ");
			contatti.verifyComponentExistence(contatti.checkboxContattiConsensi);
			contatti.verifyComponentExistence(contatti.buttonInformativa);
			contatti.verifyComponentExistence(contatti.testo1ContattiConsensi);
			contatti.verifyComponentExistence(contatti.testo2ContattiConsensi);
			contatti.verifyComponentExistence(contatti.testo3ContattiConsensi);
		//	contatti.verifyComponentExistence(contatti.testo4ContattiConsensi);
			logger.write("Check sezione Contatti e Consensi - Completed ");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
