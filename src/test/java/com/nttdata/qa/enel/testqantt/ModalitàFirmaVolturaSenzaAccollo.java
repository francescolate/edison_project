package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CarrelloComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CigCupComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FatturazioneElettronicaComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModalitàFirmaVolturaSenzaAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
		
			if (prop.getProperty("CANALE","").contentEquals("PEC")) {
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, verifica corretta esistenza dei campi modalita' firma, canale, indirizzo Pec o Indirizzo email a seconda della scelta, popolare campo Modalita' firma, canale  e click su 'Conferma' - Start");
				offer.verifyComponentExistence(offer.campoModalitaFirma);
				offer.verifyComponentExistence(offer.campoCanale);
				offer.popolareCampo(prop.getProperty("MODALITA_FIRMA"), offer.campoModalitaFirma);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo("EMAIL", offer.campoCanale);
				TimeUnit.SECONDS.sleep(1);
				offer.verifyComponentExistence(offer.campoEmail);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("CANALE"), offer.campoCanale);
				TimeUnit.SECONDS.sleep(1);
				offer.verifyComponentExistence(offer.campoPEC);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("EMAIL"), offer.campoPEC);
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, verifica corretta esistenza dei campi modalita' firma, canale, indirizzo Pec o Indirizzo email a seconda della scelta, popolare campo Modalita' firma, canale  e click su 'Conferma' - Completed");
			} else if (prop.getProperty("MODALITA_FIRMA","").contentEquals("DIGITAL")) {
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DIGITAL', canale con 'EMAIL', popolare i campi email, cellulare e click su 'Conferma' - Start");
				offer.popolareCampo("DIGITAL", offer.campoModalitaFirma);
				offer.checkCampiModFirma();	
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("EMAIL"), offer.campoEmail);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("CELLULARE"), offer.campoCellulare);
				TimeUnit.SECONDS.sleep(1);
				//offer.clickComponent(offer.buttonConfermaModFirmaCanale); //FR 25.08.2021 - commentato perchè la stessa istruzione viene eseguita alla riga 99
				//gestione.checkSpinnersSFDC();
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DIGITAL', canale con 'EMAIL', popolare i campi email, cellulare e click su 'Conferma' - Completed");
			}
			else {
			logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma, canale  e click su 'Conferma' - Start");
			offer.popolareCampo(prop.getProperty("MODALITA_FIRMA"), offer.campoModalitaFirma);
			TimeUnit.SECONDS.sleep(1);
			offer.popolareCampo(prop.getProperty("CANALE"), offer.campoCanale);
			TimeUnit.SECONDS.sleep(1);
			offer.popolareCampo(prop.getProperty("EMAIL"), offer.campoEmail);
			TimeUnit.SECONDS.sleep(1);
			
			
			}
			
			offer.clickComponent(offer.buttonConfermaModFirmaCanale);
			gestione.checkSpinnersSFDC();
			logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma, canale  e click su 'Conferma' - Completed");
			
			
			if (prop.getProperty("EFFETTUARE_CONFERMA","").contentEquals("Y")) {
				offer.clickComponent(offer.buttonConferma);
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(2);
			}
			if(!prop.getProperty("VERIFICA_SEZIONI_CONFERMATE", "Y").contentEquals("N"))
			{
				offer.verifyComponentExistence(offer.sezioniConfermate);
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
