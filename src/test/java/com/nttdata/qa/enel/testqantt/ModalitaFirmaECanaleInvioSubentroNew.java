/**
 * @author Claudio.Cavalleri
 *
 */
package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneAppuntamentoComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModalitaFirmaECanaleInvioSubentroNew {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		SeleniumUtilities util;

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			
			Thread.sleep(5000);
			// imposto Modalità Firma su No Vocal
			logger.write("imposto Modalità Firma su No Vocal - Start ");
			modalita.impostaModalitàFirma(prop.getProperty("MODALITA_FIRMA", "NO VOCAL"));
			logger.write("imposto Modalità Firma su No Vocal - Completed ");

			logger.write("Selezione canale Invio - Start ");
			modalita.selezionaCanaleInvio(prop.getProperty("CANALE_INVIO_FIRMA"));
			modalita.pressButton(modalita.confermaButton);
			logger.write("Selezione canale Invio - Completed");			
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
/*			
			if (prop.getProperty("CANALE_INVIO_FIRMA").equals("POSTA")) {
				if (Costanti.statusUbiest.contentEquals("OFF")) {
					compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzatoModFirma("Modalità firma e Canale Invio",
							"00198", "ROMA");
				} else {
					compila.selezionaIndirizzoEsistenteSeNonSelezionato("Modalità firma e Canale Invio");
				}
			}
			
			driver.findElement(modalita.confermaButton).click();
*/			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
