package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltagePreventintoComponent;
import com.nttdata.qa.enel.components.colla.DettagliocostietempiComponent;
import com.nttdata.qa.enel.components.colla.PowerAndVoltageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_MPT_147 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("apertura del portale web Enel di test - Start");
			PowerAndVoltageComponent pavc = new PowerAndVoltageComponent(driver);
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			DettagliocostietempiComponent dc = new DettagliocostietempiComponent(driver);
			String electricitySupplyDetailPageUrl = prop.getProperty("WP_LINK")+"/it/area-clienti/residenziale/dettaglio_fornitura";
			
			logger.write("Verifying the customer Page-- Start");
			pavc.comprareText(pavc.areaPrivataHeading, PowerAndVoltageComponent.AREA_PRAIVATA_HEADING, true);
			pavc.comprareText(pavc.leFornitureHeading, PowerAndVoltageComponent.LE_FORNITURE_HEADING, true);
			logger.write("Verifying the customer Page-- Ends");
			
			logger.write("Clicking on the Dettaglio Fornitura button in electrical supply card & verifying the page navigation-- Start");
			dc.clickComponent(dc.detaglioFurnitura_310511532New);
			
			pavc.checkURLAfterRedirection(electricitySupplyDetailPageUrl);
			logger.write("Clicking on the Dettaglio Fornitura button in electrical supply card & verifying the page navigation-- Ends");
			
			
			logger.write("Clicking on the Modifica potenca e/o Tenzione & verifying the page navigation-- Start");
			pavc.verifyComponentExistence(pavc.serviziPerLeFornitureHeading);
			Thread.sleep(5000);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTensioneTile);
			pavc.jsClickElement(pavc.modificaPotenzaTensioneTile);
			
			pavc.verifyComponentExistence(pavc.modificaPotenzaPage);
			pavc.verifyComponentExistence(pavc.modificaPotenzaButton);
			pavc.clickComponent(pavc.modificaPotenzaButton);
			Thread.sleep(15000);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTensioneTileHeading);
			pavc.comprareText(pavc.modificaPotenzaTensioneTileHeading, PowerAndVoltageComponent.MODIFICA_POTENZA_TENZIONE_TILE_HEADING, true);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTenzioneTitle);
			pavc.comprareText(pavc.modificaPotenzaTenzioneTitle, PowerAndVoltageComponent.MODIFICA_POTENZA_TENZIONE_TITLE, true);
			logger.write("Clicking on the Modifica potenca e/o Tenzione & verifying the page navigation-- Ends");
			
			logger.write("Clicking on the Radio Button-- Start");
//			dc.clickComponent(dc.radioIT002E9834235A); 
			dc.clickComponent(dc.radioIT002E5415888A); //Pod - IT002E6754098A //IT001E64378944
			logger.write("Clicking on the Radio Button-- Ends");
			
			logger.write("Clicking on the Modifica Potenza e/o Tenzione-- Start");
			pavc.verifyComponentExistence(pavc.modificaPotenzaTenzioneButton2);
			pavc.clickComponent(pavc.modificaPotenzaTenzioneButton2);
			logger.write("Clicking on the Modifica Potenza e/o Tenzione-- Ends");
			
			logger.write("Verifying the data on clicking Modifica Potenza e/o Tenzione button-- Start");
			pavc.verifyComponentExistence(pavc.titleMPT);
			pavc.comprareText(pavc.titleMPT, PowerAndVoltageComponent.TITLE_MPT, true);
			dc.comprareText(dc.subTitleMPT, DettagliocostietempiComponent.SUBTITLE_MPT, true);
			pavc.checkForoptionsAvailable(prop.getProperty("OPTIONS"));
			pavc.verifyComponentExistence(pavc.powerandVoltageDataEntryHeading);
			pavc.comprareText(pavc.powerandVoltageDataEntryHeading, PowerAndVoltageComponent.POVER_VOLTAGE_DATAENTRY_HEADING, true);
			
			pavc.comprareText(pavc.potenzalabel, PowerAndVoltageComponent.POTENZA_LABEL, true);
			pavc.validateDropDownValue(pavc.potenzaValue, PowerAndVoltageComponent.POTENZA_VALUE);
			pavc.changeDropDownValue(pavc.potenzaValue,pavc.setPotenzaValue);
			pavc.comprareText(pavc.tensionelabel, PowerAndVoltageComponent.TENZIONE_LABEL, true);
			pavc.validateDropDownValue(pavc.tensioneValue, PowerAndVoltageComponent.TENZIONE_VALUE);
			pavc.verifyComponentExistence(pavc.textNextToTenzione);
			pavc.comprareText(pavc.textNextToTenzione, PowerAndVoltageComponent.TEXT_NEXT_TENZIONE, true);
			
			pavc.verifyComponentExistence(pavc.noRadioButton);
			pavc.isRadioButtonEnabled(pavc.noRadioButton);
			
			pavc.verifyComponentExistence(pavc.modificaPotenzaTensioneTileHeading3);
			pavc.comprareText(pavc.modificaPotenzaTensioneTileHeading3, PowerAndVoltageComponent.MODIFICA_TENZIONE_HEADING3, true);
			
			pavc.verifyComponentExistence(pavc.emailLabel);
			pavc.comprareText(pavc.emailLabel, PowerAndVoltageComponent.EMAIL_LABEL, true);
			pavc.verifyFeildAttribute(pavc.emailValue, prop.getProperty("WP_USERNAME"), pavc.xpathEmail);
			pavc.enterInputtoField(changePV.cellulare, prop.getProperty("CELLULAR"));
			
			pavc.verifyComponentExistence(pavc.cellulareLabel);
			pavc.comprareText(pavc.cellulareLabel, PowerAndVoltageComponent.CELLULARE_LABEL, true);
			
			logger.write("Verifying the data on clicking Modifica Potenza e/o Tenzione button-- Ends");
			
			logger.write("Modify the dropdown value of Potenza*  field with Inserimento manuale -- Start");
			pavc.changeDropDownValue(pavc.potenzaValue, prop.getProperty("POTENZA"));
	
			changePV.clickComponent(changePV.calculateQuote);
			Thread.sleep(5000);
			pavc.enterInputtoField(changePV.confirmCellulare, prop.getProperty("CELLULAR"));logger.write("Modify the dropdown value of Potenza*  field with Inserimento manuale -- Ends");
			changePV.clickComponent(changePV.calculateQuote);
			Thread.sleep(5000);
			
			logger.write("Verify Pop up -- Start");
			changePV.verifyComponentExistence(changePV.attendiPopUpTitle);
			changePV.comprareText(changePV.attendiPopUp, ChangePowerAndVoltageComponent.Attendiqualchesecondo, true);
			logger.write("Verify Pop up -- Completed");
			
			ChangePowerAndVoltagePreventintoComponent cp = new ChangePowerAndVoltagePreventintoComponent(driver);
			Thread.currentThread();
			Thread.sleep(30000);
			
			/*logger.write("Verify Preventito Page Content -- Start");
			changePV.checkForMenuItem(ChangePowerAndVoltageComponent.menuItem);
			cp.waitForElementToDisplay(cp.preventintoText);
			cp.verifyComponentExistence(cp.preventintoText);
//			cp.comprareText(cp.preventintoText, ChangePowerAndVoltagePreventintoComponent.provinica_text, true);
			cp.verifyComponentExistence(cp.potenzoLabel);
			cp.checkData(cp.potenzoValue, prop.getProperty("POWER_INPUT"));
			cp.verifyComponentExistence(cp.tensionLabel);
			cp.checkData(cp.tensionValue, prop.getProperty("TENZION"));
			cp.comprareText(cp.detagliocostiTitle, ChangePowerAndVoltagePreventintoComponent.dettaglio_costi, true);
			cp.comprareText(cp.modalitàdiPagamentoText, ChangePowerAndVoltagePreventintoComponent.ModalitadiPagamento, true);
			cp.verifyComponentExistence(cp.emailInput);
			cp.verifyComponentExistence(cp.cellularInput);
			cp.verifyComponentExistence(cp.supplySection);
			cp.comprareText(cp.preventivoText3, ChangePowerAndVoltagePreventintoComponent.PreventivoText3, true);
//				
			cp.comprareText(cp.detagliocostiTitle, ChangePowerAndVoltagePreventintoComponent.dettaglio_costi, true);
			dc.comprareText(dc.lavoroCost, DettagliocostietempiComponent.LAVARO_COSTO_TEXT, true);
			dc.comprareText(dc.sommaSeguenti, DettagliocostietempiComponent.SOMMA_SEGUENTI_TEXT, true);
			dc.comprareText(dc.item1, DettagliocostietempiComponent.ITEM_ONE_TEXT, true);
			dc.comprareText(dc.item2, DettagliocostietempiComponent.ITEM_TWO_TEXT, true);
			dc.comprareText(dc.item3, DettagliocostietempiComponent.ITEM_THREE_TEXT, true);
			dc.comprareText(dc.tempiDiEsecuzioneHeading, DettagliocostietempiComponent.TEMPIDIESECUZIONE_HEADING, true);
			dc.comprareText(dc.tempiDiEsecuzioneParagraph1, DettagliocostietempiComponent.TEMPIDIESECUZIONE_PARAGRAPH1, true);
			dc.comprareText(dc.tempiDiEsecuzioneParagraph2, DettagliocostietempiComponent.TEMPIDIESECUZIONE_PARAGRAPH2, true);
			dc.comprareText(dc.tempiDiEsecuzioneParagraph3, DettagliocostietempiComponent.TEMPIDIESECUZIONE_PARAGRAPH3, true);
//						
//			
			cp.comprareText(cp.modalitàdiPagamentoText, ChangePowerAndVoltagePreventintoComponent.ModalitadiPagamento, true);
			dc.verifyComponentExistence(dc.radioAddebitoinBolletta);
			dc.comprareText(dc.radioAddebitoinBolletta, DettagliocostietempiComponent.RADIO_ADDEBITOINBOLLETTA, true);
			driver.findElement(dc.radioAddebitoinBolletta).isSelected();
			dc.verifyComponentExistence(dc.radioVoglioPagareOnline);
			dc.comprareText(dc.radioVoglioPagareOnline, DettagliocostietempiComponent.RADIO_VOGLIOPAGAREONLINE, true);
			dc.comprareText(dc.checkBoxDichiaro, DettagliocostietempiComponent.CHECKBOX_DICHIARO, true);
//			
			cp.verifyComponentExistence(cp.emailInput);
			cp.verifyComponentExistence(cp.cellularInput);
			cp.verifyComponentExistence(cp.supplySection);
			
			dc.clickComponent(dc.checkBoxDichiaro);
			dc.clickComponent(dc.confermaButton);
			
			logger.write("Verify Pop up -- Start");
			changePV.verifyComponentExistence(changePV.attendiPopUpTitle);
			dc.comprareText(dc.attendiPopUp, DettagliocostietempiComponent.Attendiqualchesecondo, true);
			logger.write("Verify Pop up -- Completed");
			
			Thread.sleep(40000);
			
			dc.verifyComponentExistence(dc.attizoneHeading);
//			dc.comprareText(dc.attizoneHeading, DettagliocostietempiComponent.ATTIZONE_HEADING, true);
//			dc.comprareText(dc.attizoneTitle, DettagliocostietempiComponent.ATTIZONE_TITLE, true);
			dc.verifyComponentExistence(dc.noButton);
			dc.verifyComponentExistence(dc.yesButton);
			dc.clickComponent(dc.yesButton);
			logger.write("Verify Preventito Page Content -- Completed");
			
			logger.write("Verifying the data on clicking on SI Button-- Start");
			pavc.verifyComponentExistence(pavc.titleMPT);
			pavc.comprareText(pavc.titleMPT, PowerAndVoltageComponent.TITLE_MPT, true);
			dc.comprareText(dc.subTitleMPT, DettagliocostietempiComponent.SUBTITLE_MPT, true);
//			pavc.checkForoptionsAvailable(prop.getProperty("OPTIONS"));
			logger.write("Verifying the data on clicking on SI Button-- Ends");
			
			Thread.sleep(5000);
			dc.clickComponent(dc.fineButton);
			
			*/
			prop.setProperty("RETURN_VALUE", "OK");
		}catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

	StringWriter errors = new StringWriter();
	e.printStackTrace(new PrintWriter(errors));
	errors.toString();
	logger.write("ERROR_DESCRIPTION: " + errors.toString());

	prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
	if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
		throw e;

//	prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
	// Store WebDriver Info in properties file
	prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
}

	}

}
