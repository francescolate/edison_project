package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LuceEGasComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HOMEPAGE_ACR_4 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			HomeComponent home = new HomeComponent(driver);
			
		    prop.setProperty("ACTIVESUPPLY", "Vuoi attivare una nuova fornitura Scopri le offerte per la tua casa");
		    prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
			prop.setProperty("VISUALIZZALEBOLLETTETEXT", "Le tue bollette Qui potrai visualizzare bollette e/o rate delle tue forniture.");
			prop.setProperty("QST1", "Che contratto vuoi attivare?");
			prop.setProperty("ANS1", "Luce e Gas");
			prop.setProperty("QST2", "Dove?");
			prop.setProperty("ANS2", "Casa");
			prop.setProperty("QST3", "Per quale necessità?");
			prop.setProperty("ANS3", "Visualizza tutte");
			prop.setProperty("PATH", "HOME / LUCE E GAS");

			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
						
			logger.write("Check for Residential Menu - Start");
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);

			log.VerifyText(home.customerNumberGas, prop.getProperty("NUMEROCLIENTE"));

			home.checkForContent(home.activateSupplyText1, home.activeSupplyText2, prop.getProperty("ACTIVESUPPLY"));
			
			logger.write("Check for See offers button - Start");

			home.verifyComponentExistence(home.seeOffers);
			
			logger.write("Check for See offers button - Completed");

			logger.write("Click on see offers - Start");

			home.clickComponent(home.seeOffers);
			
			logger.write("Click on see offers - Completed");

			LuceEGasComponent lg = new LuceEGasComponent(driver);
			
		//	lg.verifyComponentExistence(lg.pageTitle);
			
			logger.write("Check for selections - Start");

			log.VerifyText(lg.qst1, prop.getProperty("QST1"));
			
			log.VerifyText(lg.qst2, prop.getProperty("QST2"));

			log.VerifyText(lg.qst3, prop.getProperty("QST3"));

			logger.write("Check for selections - Completed");

			logger.write("Check selected filter values - Start");

			lg.checkQuestionAndFilter(prop.getProperty("ANS1"), prop.getProperty("ANS2"), prop.getProperty("ANS3"));
			
			logger.write("Check selected filter values - Start");

	//		lg.checkMenuPath(lg.luceegas,prop.getProperty("PATH"));
			
			logger.write("Click on login icon - Start");

			lg.clickComponent(lg.loginIcon);
			
			logger.write("Click on login icon - Completed");

			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}


}
