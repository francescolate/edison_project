package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BollettaWebComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_303_Attiva_Bolletta_WEB {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String currentHandle ="";

		try {		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);	
			WebDriverWait wait = new WebDriverWait(driver, 10);
			
			ServiziComponent sc = new ServiziComponent(driver);
			
			BollettaWebComponent bwc = new BollettaWebComponent(driver);
			
			logger.write("Verify home page title, Logo and details -- Start");
			sc.verifyComponentExistence(sc.homePageLogoServizi);			
			sc.comprareText(sc.homePagePathServizi, sc.HomePagePathSer, true);							//false
			sc.comprareText(sc.homePageTitleServizi, sc.HomePageTitleSer, true);	
			logger.write("Verify home page title, Logo and details -- Completed");
			
			sc.verifyComponentExistence(sc.services);
			logger.write("Click on Servizi -- Start");
			sc.clickComponent(sc.services);
			logger.write("Click on Servizi -- Completed");
			
			
			logger.write("Verifi Servizi page Path, title and Text -- Start");
			sc.comprareText(sc.pagePathServizi, sc.PAGEPATH, true);										//false
			sc.verifyComponentExistence(sc.pageTitleServizi);
			sc.comprareText(sc.pagetextServizi, sc.PAGETEXT, true);
			logger.write("Verifi Servizi page Path, title and Text -- Completed");
			
			
			logger.write("Click on Bolletta Web-- Start");
			sc.verifyComponentExistence(sc.bollettaWeb);
			sc.clickComponent(sc.bollettaWeb);
			logger.write("Click on Bolletta Web-- Completed");
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(sc.homePagePathServizi));
			Thread.sleep(10000);
			
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathBW, true);					//false
			sc.comprareText(sc.pageTitleBW, sc.PageTitleBW, true);
			sc.comprareText(sc.textBollettaWebBy, sc.textBollettaWeb, true);
			//sc.comprareText(sc.servizioBollettaWebBy, sc.servizioBollettaWeb, true);
			sc.containsText(sc.webNonAtt, sc.servizioBollettaWeb);
			sc.verifyComponentExistence(sc.attivaBollettaWebButton);
			//bwc.comprareText(bwc.clientNumberText, bwc.ClientNumberText, true);
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Completed");
			
			logger.write("Click on Attiva Bolletta Web - Start");
			sc.clickComponent(sc.attivaBollettaWebButton);
			logger.write("Click on Attiva Bolletta Web - Completed");
			
			Thread.sleep(20000);
			
			logger.write("Verify Bolletta Web Error -- Start");
			sc.containsText(sc.homePagePathServizi, "Area riservata");
			sc.containsText(sc.homePagePathServizi, "Bolletta Web");
			sc.comprareText(sc.pageTitleBW, sc.PageTitleBW, true);
			sc.verifyComponentExistence(sc.operazioneNonRiuscita);
			sc.comprareText(sc.steps, sc.stepsValue, true);
			sc.verifyComponentExistence(sc.erroreGenerico);
			logger.write("Verify Bolletta Web Error -- Completed");
			
			
			logger.write("Click on Esci and return to homepage - Start");
			sc.clickComponent(sc.esciButton);
			logger.write("Click on Esci and return to homepage - Completed");
			
			logger.write("Verify home page title, Logo and details -- Start");
			sc.verifyComponentExistence(sc.homePageLogoServizi);			
			sc.comprareText(sc.homePagePathServizi, sc.HomePagePathSer, true);							//false
			sc.comprareText(sc.homePageTitleServizi, sc.HomePageTitleSer, true);	
			logger.write("Verify home page title, Logo and details -- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

	}


