package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertaEneloneComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LaunchOffertaEnelOne {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);			
			prop.setProperty("MODULORECLAMILINK", "https://www-coll1.enel.it/it/luce-e-gas/luce/offerte/enel-one?zoneid=luce_e_gas-box_offerta");
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			OffertaEneloneComponent oc = new OffertaEneloneComponent(driver);
			Thread.sleep(10000);
			logger.write("Verify the Enel One path, Ttile and Text - Start");
			//oc.compareText(oc.home, oc.Home, true);
			oc.compareText(oc.sceglipiano, oc.SCEGLIPIANO, true);
			oc.compareText(oc.enelone, oc.EnelOne, true);
			oc.compareText(oc.enelOneTitle, oc.EnelOneTitle, true);
			oc.compareText(oc.enelOneText, oc.EnelOneText, true);
			logger.write("Verify the Enel One,Scegli Piano path, Ttile and Text - Completed");
			logger.write("Click on Verifica E Aderisci - Start");
			oc.clickComponent(oc.VERIFICAEADERISCI);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.compareText(oc.pageText, oc.PageText, true);
			oc.verifyComponentExistence(oc.CONFERMA);
			oc.verifyComponentExistence(oc.popuoclose);
			oc.verifyComponentExistence(oc.popupLink);
			logger.write("Click on Verifica E Aderisci - Completed");
			oc.enterInputParameters(oc.pod, "09439342342342");
			oc.clickComponent(oc.CONFERMA);
			oc.compareText(oc.errorMessages, oc.ERROR_MESSAGE, true);
			oc.enterInputParameters(oc.pod, "AS2");
			oc.clickComponent(oc.CONFERMA);
			oc.compareText(oc.errorMessages, oc.ERROR_MESSAGE, true);
			oc.enterInputParameters(oc.pod, "IT004%");
			oc.clickComponent(oc.CONFERMA);
			oc.compareText(oc.errorMessages, oc.ERROR_MESSAGE, true);
			oc.enterInputParameters(oc.pod, "IT004E"+PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			
			prop.setProperty("RETURN_VALUE", "OK"); 
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
