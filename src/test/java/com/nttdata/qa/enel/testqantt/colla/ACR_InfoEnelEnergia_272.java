package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_InfoEnelEnergia_272 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			logger.write("Verify dettaglioFurniture and click on dettaglioFurniture -- Start");
			hm.verifyComponentExistence(hm.dettaglioFurniture_272);
			hm.clickComponent(hm.dettaglioFurniture_272);
			logger.write("Verify dettaglioFurniture and click on dettaglioFurniture -- Completed");

			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			logger.write("Verify services available for the user -- Start");
			dc.verifyServiceList(dc.infoEnelEnergiaTile, dc.services);
			dc.verifyServiceList(dc.autolettura, dc.services);
			dc.verifyServiceList(dc.modificaIndirizzoFatturazione, dc.services);
			dc.verifyServiceList(dc.modificaPotenzaeoTensione, dc.services);
			dc.verifyServiceList(dc.modificaContattieConsensi, dc.services);
			dc.verifyServiceList(dc.webBill, dc.services);
			dc.verifyServiceList(dc.onlinePayment, dc.services);
			dc.verifyServiceList(dc.directDebit, dc.services);
			dc.verifyServiceList(dc.billCorrection, dc.services);
			dc.verifyServiceList(dc.installments, dc.services);
			dc.verifyServiceList(dc.summary, dc.services);
			dc.verifyServiceList(dc.duplicateUtilityBill, dc.services);
			dc.verifyServiceList(dc.sendingDoc, dc.services);
			dc.verifyServiceList(dc.supplyDeactivation, dc.services);
			logger.write("Verify services available for the user -- Completed");


			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
