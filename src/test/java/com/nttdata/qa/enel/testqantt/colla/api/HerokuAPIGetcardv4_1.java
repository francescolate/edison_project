// HerokuAPIGetcardv4_1 package WEB - COMPARE 

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
// import java.util.UUID;
import java.util.UUID;

import com.nttdata.jwt.gen.JWToken;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIGetcardv4Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIGetcardv4_1 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Authenticate Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			//Da configurare come OutPut
    		prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130");
    		prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6131");
			prop.setProperty("JSON_INPUT", "{  \"gen\": {        \"tid\":\"" + prop.getProperty("TID") + "\" ,   \"sid\":\""+ prop.getProperty("SID")+"\",        \"username\": \""+prop.getProperty("USERNAME")+"\",        \"keys\": [            {                \"key\": \"APP_VER\",                \"value\": \"20.3.0\"            },            {                \"key\": \"OS\",                \"value\": \"ANDROID\"            },            {                \"key\": \"APP\",                \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"            },            {                \"key\": \"ID_DISPOSITIVO\",                \"value\": \"MWS0216808002415\"            }        ]    }}");
			//prop.setProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJBcHBNb2JpbGUiLCJjb2RpY2VmaXNjYWxlY291bnRyeSI6IklUIiwic3ViIjoiRUUwMDAwMDE4MTUyQEJPWC5FTkVMLkNPTSIsImNvZGljZWZpc2NhbGUiOiJUTlRGTkM4NE01N0w3MzZWIiwiaXNzIjoiYXBwIiwiZXhwIjoxNjQzNjI2MDQ5LCJlbmVsaWQiOiJlNjQ3MTQ1OS1kNjhmLTQ5NTItODRmNC01NDkzMWI4ZjY4NzMiLCJ1c2VyaWQiOiJyMzIwMTlwLjEwOXN0YXRvY29udGl3ZWJAZ21haWwuY29tIiwianRpIjoiZmFjOTQ3MDEtN2IwMS00MDdkLWI2MDgtZTZlODhlZGEzM2U1In0.ew4lPe9amhlyaEFNfYx5H9-2OZZrPU18P2mqE_5BaZg");
			prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));

    		// prop.setProperty("Authorization", "Basic QXZ2MGYyRjA6ZjBlWERzSHc=");
    		prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
    		prop.setProperty("SOURCECHANNEL","WEB");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIGetcardv4Component HRKComponent = new HerokuAPIGetcardv4Component();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
