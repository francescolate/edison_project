package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.ImpreseComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LuceEGasComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.components.colla.SearchResultComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_62_Ricerca {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchCorporateLink(prop.getProperty("WP_LINK_CORPORATE"));
			Thread.sleep(3000);
			log.launchLink(prop.getProperty("WP_LINK_COLL2"));
			Thread.sleep(3000);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			log.clickComponent(log.buttonAccetta);
			
			Thread.sleep(30000);
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			pc.verifyComponentExistence(pc.searchIcon);
			pc.clickComponent(pc.searchIcon);
			pc.comprareText(pc.quickLinks, pc.QuickLinks, true);
			pc.clickComponent(pc.lemigliori);
			
			LuceEGasComponent lc =new LuceEGasComponent(driver);
			Thread.sleep(10000);
			lc.verifyComponentExistence(lc.pageTitle);
			pc.comprareText(lc.pageTitle, lc.PageTitle, true);
			lc.checkURLAfterRedirection(lc.pageUrl);
			Thread.sleep(10000);
			lc.verifyComponentExistence(lc.searchIcon);
			Thread.sleep(10000);
			lc.clickComponent(lc.searchIcon);
			pc.comprareText(pc.quickLinks, pc.QuickLinks, true);
			lc.clickComponent(lc.lesoluzioni);
			
			ImpreseComponent ic = new ImpreseComponent(driver);
			ic.comprareText(ic.impreseTitle, ic.ImpresePageTitle, true);
			ic.comprareText(ic.impreseSubText, ic.ImpresePageSubText, true);
			ic.clickComponent(ic.searchIcon);
			pc.comprareText(pc.quickLinks, pc.QuickLinks, true);
			ic.clickComponent(ic.gestisci);
			
			SupportoSectionComponent sc = new SupportoSectionComponent(driver);
			ic.comprareText(sc.pageTitle, sc.PageTitle, true);
			ic.comprareText(sc.titleSubText, sc.PageTitleSubText, true);
			Thread.sleep(3000);
			sc.verifyComponentExistence(sc.searchIcon);
			Thread.sleep(10000);
			sc.jsClickObject(sc.searchIcon);
			log.enterLoginParameters(sc.searchEntry, prop.getProperty("SEARCH_INPUT"));
			sc.clickComponent(sc.submit);
			
			SearchResultComponent src = new SearchResultComponent(driver);
			
			Thread.sleep(10000);
			ic.comprareText(src.potenza, src.Potenza, true);
			ic.comprareText(src.date, src.Date, true);
			/*ic.comprareText(src.modificareText, src.Text, true);
			ic.comprareText(src.subText, src.SubText, true);
*/			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
