package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaAccountComponent;
import com.nttdata.qa.enel.components.colla.Privato170ModIndFatturazioneACRComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerifyBillingAddress {
	
	@Step("Validate billing address mismatch")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			Privato170ModIndFatturazioneACRComponent MIF = new Privato170ModIndFatturazioneACRComponent(driver);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
//			wbc.insertQuery(Costanti.get_BillingAddress_171.replace("$CUSTOMER_NUMBER$", Privato170ModIndFatturazioneACRComponent.numeroClienti_310491181_Value));
//			wbc.insertQuery(Costanti.get_BillingAddress_171.replace("$CUSTOMER_NUMBER$", Privato170ModIndFatturazioneACRComponent.numeroClienti_300001597_Value));
			wbc.insertQuery(Costanti.get_BillingAddress_171.replace("$CUSTOMER_NUMBER$", Privato170ModIndFatturazioneACRComponent.numeroClienti_300001594_Value));
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");

			MIF.VerifyText(MIF.ITA_IFM_ConsumptionNumber__c_Column, prop.getProperty("CONSUMPTIONNUMBER_COLUMNHEADING"));
//			MIF.VerifyText(MIF.ITA_IFM_ConsumptionNumber__c_Value, Privato170ModIndFatturazioneACRComponent.numeroClienti_310491181_Value);
//			MIF.VerifyText(MIF.ITA_IFM_ConsumptionNumber__c_Value, Privato170ModIndFatturazioneACRComponent.numeroClienti_300001597_Value);
			MIF.VerifyText(MIF.ITA_IFM_ConsumptionNumber__c_Value, Privato170ModIndFatturazioneACRComponent.numeroClienti_300001594_Value);
			
			MIF.VerifyText(MIF.ITA_IFM_BillingAddress__c_Column, prop.getProperty("BILLINGADDRESS_COLUMNHEADING"));
			MIF.VerifyTextNotMatch(MIF.ITA_IFM_BillingAddress__c_Value, Privato170ModIndFatturazioneACRComponent.ITA_IFM_BillingAddress__c);
			
			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}