package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerifyRequest_ACR293 {

	@Step("Recupero Pod da Workbench")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			prop.setProperty("PROFILE_INFO_TEXT", "Titolare Mancuso Salvatore Codice FiscaleMNCSVT58H26H325L Telefono Fisso0818142364 Numero di Cellulare+39 3339090890 Email$email$ PECNon presente Canale di contatto preferito Telefono Cellulare");
			
			AccountComponent ac = new AccountComponent(driver);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.get_query_293);
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			
			ac.comprareText(ac.wbStatus, ac.Status, true);
			ac.comprareText(ac.wbEmail, prop.getProperty("MODIFIED_EMAIL"), true);
			ac.comprareText(ac.wbReason, ac.Reason, true);
			ac.comprareText(ac.wbOperationType, ac.OperationType, true);
			ac.comprareText(ac.wbContactPhone, prop.getProperty("TELEFONO_INPUT"), true);
			ac.comprareText(ac.wbContactMobile, prop.getProperty("CELLULAR_INPUT"), true);
			ac.comprareText(ac.wbOrigin, ac.Origin, true);
			
			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
