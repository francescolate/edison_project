package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class PopolaCampiSezioneFornituraGasVolture {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			
			logger.write("valorizzare i campi Categoria Merciologica SAP con 'Abitazioni Private'; Titolarietà con 'Proprità o Usufrutto'; Consumo Annuo con un valore a piacere; Flag Residente con '"+prop.getProperty("FLAG_RESIDENTE")+"' - Start");
		/*	Posizionarsi nella sezione "Dati Formitura".
			I campi che vengono richiesti da valorizzare sono:
			
			
			
			
			
			- Categoria d'uso ( Tipo Combo ) scegliere "Riscaldamento+uso cotturacibie/o pro...."
		
			- Lettrua misuratore valorizzaare con un valore a piacere

	


			Viene abilitato il tasto <Conferma Fornitura>
			Successivamente premere il tasto <Conferma>*/ 
			offer.popolareCampiDatiFornituraGas(prop.getProperty("CATEGORIA_MERCEOLOGICA_SAP"), prop.getProperty("CATEGORIA_CONSUMO"), prop.getProperty("CATEGORIA_MARKETING"), prop.getProperty("FLAG_RESIDENTE"), prop.getProperty("ORDINE_GRANDEZZA").toUpperCase(),
					prop.getProperty("POTENZIALITA"), prop.getProperty("UTILIZZO"), "Uso condizionamento", prop.getProperty("CLASSE_PRELIEVO"), prop.getProperty("TITOLARITA"),prop.getProperty("PROFILO_CONSUMO"));
			
			
			logger.write("valorizzare i campi Categoria Merciologica SAP con 'Abitazioni Private'; Titolarietà con 'Proprità o Usufrutto'; Consumo Annuo con un valore a piacere; Flag Residente con '"+prop.getProperty("FLAG_RESIDENTE")+"' - Completed");
			
			logger.write("click sul pulsante 'Conferma fornitura' - Start");
			offer.clickComponent(offer.buttonConfermaFornitura);
			
			gestione.checkSpinnersSFDC();
			logger.write("click sul pulsante 'Conferma fornitura' - Completed");
			
			logger.write("click sul pulsante 'Conferma' - Start");
			offer.clickComponent(offer.buttonConfermaDatiForn);
			gestione.checkSpinnersSFDC();
			logger.write("click sul pulsante 'Conferma' - Completed");
//			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
