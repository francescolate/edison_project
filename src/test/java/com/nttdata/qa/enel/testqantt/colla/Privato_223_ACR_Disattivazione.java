package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_223_ACR_Disattivazione {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			
			PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
			/*
			logger.write("Accessing login page - Start");
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			logger.write("Accessing login page - Complete");
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			/*lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
								
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			//dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
						
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			//dfc.verifyComponentVisibility(dfc.buttonLoginAccedi);
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			logger.write("Verify and click on Residential customer button  - Start");
			dfc.verifyComponentExistence(dfc.PrivatoResCustomerButton);
			dfc.clickComponent(dfc.PrivatoResCustomerButton);
			logger.write("Verify and click on Residential customer button  - Complete");
			*/
			dfc.verifyComponentExistence(dfc.BenvenutoTitle);
			dfc.comprareText(dfc.BenvenutoTitle, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_TITLE, true);
			dfc.verifyComponentExistence(dfc.BenvenutoContent);
			dfc.comprareText(dfc.BenvenutoContent, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_CONTENT, true);
			dfc.verifyComponentExistence(dfc.fornitureHeader);
			dfc.comprareText(dfc.fornitureHeader, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_HEADER, true);
			dfc.verifyComponentExistence(dfc.fornitureContent);
			dfc.comprareText(dfc.fornitureContent, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_CONTENT, true);
			
			logger.write("Verify and click on Servizi link  - Start");
			dfc.verifyComponentExistence(dfc.serviziLink);
			dfc.clickComponentIfExist(dfc.serviziLink);
			logger.write("Verify and click on Servizi link  - Complete");
			
			logger.write("Verify and click on Servizi title and contents  - Start");
//			dfc.verifyComponentExistence(dfc.serviziFornitureTitle);
//			dfc.comprareText(dfc.serviziFornitureTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_TITLE, true);
//			dfc.verifyComponentExistence(dfc.serviziFornitureContent);
//			dfc.comprareText(dfc.serviziFornitureContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteTitle);
			dfc.comprareText(dfc.serviziBolletteTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteContent);
			dfc.comprareText(dfc.serviziBolletteContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziContratoTitle);
			dfc.comprareText(dfc.serviziContratoTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziContratoContent);
			dfc.comprareText(dfc.serviziContratoContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_CONTENT, true);
			logger.write("Verify and click on Servizi title and contents  - Complete");
			
			
			logger.write("Verify and click on disattivazioneFornitura link  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneFornitura);
			Thread.sleep(2000);
			dfc.clickComponent(dfc.disattivazioneFornitura);
			logger.write("Verify and click on disattivazioneFornitura link  - Complete");
			
			logger.write("Verify disattivazione header and contents  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneHeader);
			dfc.comprareText(dfc.disattivazioneHeader, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_HEADER, true);
			dfc.verifyComponentExistence(dfc.disattivazioneContent1);
			dfc.comprareText(dfc.disattivazioneContent1, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_CONTENT1, true);
			dfc.verifyComponentExistence(dfc.disattivazioneContent2);
			dfc.comprareText(dfc.disattivazioneContent2, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_CONTENT2, true);
			dfc.verifyComponentExistence(dfc.disattivazioneContent3);
			dfc.comprareText(dfc.disattivazioneContent3, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_CONTENT3, true);
			logger.write("Verify disattivazione header and contents  - Complete");
			
			logger.write("Verify domandeFrequenti header  - Start");
			dfc.verifyComponentExistence(dfc.domandeFrequenti);
			dfc.comprareText(dfc.domandeFrequenti, PrivateAreaDisattivazioneFornituraComponent.DOMANDE_HEADER, true);
			logger.write("Verify domandeFrequenti header  - Start");
			
			logger.write("Click on prosegui button  - Start");
			dfc.verifyComponentExistence(dfc.proseguiButton);
			dfc.clickComponent(dfc.proseguiButton);
			logger.write("Click on prosegui button  - Complete");
			
			logger.write("Verify the disattivazione button is disabled  - Start");
			dfc.waitForElementToDisplay(dfc.buttonEsci);
			dfc.verifyComponentExistence(dfc.disattivazioneButtonDisabled);
			logger.write("Verify the disattivazione button is disabled  - Complete");
						
			logger.write("Selecting 5 ELE supplies checkbox  - Start");					
			//dfc.flameClickEyeIcon();
			dfc.luceClickCheckbox();
			logger.write("Selecting 5 ELE supplies checkbox  - Complete");	
			
			//dfc.iconClick(); 
			//dfc.clickComponent(dfc.supplyCheckbox);
		
			logger.write("Verify the Esci and disattivazione button - Start");
			dfc.verifyComponentExistence(dfc.buttonEsci);
			dfc.verifyComponentExistence(dfc.disattivazioneButton);
			//dfc.clickComponent(dfc.disattivazioneButton);
			logger.write("Verify the Esci and disattivazione button - Complete");
			
			/*logger.write("Verify the footer visibility - Start");
			dfc.verifyFooterVisibility();
			logger.write("Verify the footer visibility - Start");

			logger.write("Verify the Step menu - Start");
			dfc.verifyComponentExistence(dfc.stepMenu);
			dfc.comprareText(dfc.stepMenu, PrivateAreaDisattivazioneFornituraComponent.STEP_MENU, true);
			logger.write("Verify the Step menu - Start");
			
			logger.write("Verify the Invio Community content - Start");
			dfc.verifyComponentExistence(dfc.invioCommuni);
			dfc.comprareText(dfc.invioCommuni, PrivateAreaDisattivazioneFornituraComponent.INVIO_COMM, true);
			dfc.verifyComponentExistence(dfc.invioCommnicContent);
			dfc.comprareText(dfc.invioCommnicContent, PrivateAreaDisattivazioneFornituraComponent.INVIOCOMM_CONTENT, true);
			logger.write("Verify the Invio Community content - Complete");
			
			logger.write("Verify the radiobutton header - Start");
			dfc.verifyComponentExistence(dfc.desideriQues);
			dfc.comprareText(dfc.desideriQues, PrivateAreaDisattivazioneFornituraComponent.DESIDERI_QUES, true);
			logger.write("Verify the radiobutton header - Complete");

			logger.write("Verify the radiobutton status - Start");
			boolean si = driver.findElement(dfc.radioBtnSI).isDisplayed();
			boolean no = driver.findElement(dfc.radioBtnNO).isDisplayed();
			boolean siSelect = driver.findElement(dfc.radioBtnSI).isSelected();
			dfc.verifyComponentExistence(dfc.radionBtNoLabel);
			dfc.clickComponent(dfc.radionBtNoLabel);
			logger.write("Verify the radiobutton status - Complete");
			
			
			logger.write("Verify the Aiutaci title and contents - Start");
			dfc.verifyComponentExistence(dfc.aiutaciTitle);
			dfc.comprareText(dfc.aiutaciTitle, PrivateAreaDisattivazioneFornituraComponent.AIUTACI_TITLE, true);
			dfc.verifyComponentExistence(dfc.aiutaciContent);
			dfc.comprareText(dfc.aiutaciContent, PrivateAreaDisattivazioneFornituraComponent.AIUTACI_CONTENT, true);
			logger.write("Verify the data dis content - Complete");
			
			logger.write("Verify the Continua and Indietro button - Start");
			dfc.verifyComponentExistence(dfc.continuaButton);
			dfc.verifyComponentExistence(dfc.indietroButton);
			dfc.clickComponent(dfc.continuaButton);
			logger.write("Verify the Continua and Indietro button - Complete");
			
			logger.write("Verify the mandatory fields error message - Start");
			dfc.verifyComponentExistence(dfc.mandatoryErrorMsg);
			dfc.comprareText(dfc.mandatoryErrorMsg, PrivateAreaDisattivazioneFornituraComponent.MANDATORY_MESSAGE, true);
			logger.write("Verify the mandatory fields error message - Complete");*/
			
			prop.setProperty("RETURN_VALUE", "OK");
					
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	

	
	
}
