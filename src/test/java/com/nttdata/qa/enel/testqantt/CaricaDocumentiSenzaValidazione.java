package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CaricaDocumentiSenzaValidazione {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		driver.switchTo().defaultContent();
		DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);

		logger.write("Upload Documento - Start");
	    TimeUnit.SECONDS.sleep(30);
		doc.uploadFile(
				Utility.getDocumentPdf(prop)
				);
		System.out.println("Upload Documento");
		logger.write("Upload Documento - Completed");
		
	    TimeUnit.SECONDS.sleep(5);
				
		prop.setProperty("RETURN_VALUE", "OK");
	} catch (Exception e) {
		prop.setProperty("RETURN_VALUE", "KO");
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: "+errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
		if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

	}finally
	{
		//Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
	}

}
}
