package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Info_Enel_Energia_ACR_290 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);

			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");

			HomeComponent hc = new HomeComponent(driver);
			/*logger.write("Correct visualization of the Home Page supply card sections -- Starts");	
			hc.verifyComponentExistence(hc.nomeFornituraGas);
			hc.verifyComponentExistence(hc.addressGasSupply);
			hc.verifyComponentExistence(hc.activateSupplyText1);
			hc.verifyComponentExistence(hc.activeSupplyText2);
			hc.verifyComponentExistence(hc.seeOffers);
			logger.write("Correct visualization of the Home Page supply card sections -- Ends");
*/
			logger.write("Click on services and verify the Supply service section -- Starts");
			hc.clickComponent(hc.services);
			ServicesComponent sc = new ServicesComponent(driver);
			sc.verifyComponentExistence(sc.serviceTitle);
			logger.write("Click on services and verify the Supply service section -- Ends");

			logger.write("Click on Info EnelEnergia on services section -- Starts");
			sc.verifyComponentExistence(sc.infoEnelEnergia);
			sc.clickComponent(sc.infoEnelEnergia);
			Thread.sleep(10000);
			logger.write("Click on Info EnelEnergia on services section -- Ends");

			logger.write("Verify Info EnelEnergia page title and content -- Starts");
			InfoEnelEnergiaMyComponent ic =new InfoEnelEnergiaMyComponent(driver);
			ic.verifyComponentExistence(ic.headingInfoEnergia);
			ic.comprareText(ic.headingInfoEnergia, InfoEnelEnergiaMyComponent.HEADING_INFOENERGIA, true);
			ic.comprareText(ic.subtitleInfoEnergiaNew, InfoEnelEnergiaMyComponent.SUBTITLE2INFOENERGIA, true);
			ic.comprareText(ic.titleInfoEnergia, InfoEnelEnergiaMyComponent.TITLE_INFOENERGIA, true); 
			logger.write("Verify Info EnelEnergia page title and content -- Ends");
			
			logger.write("Verify Avvialarichiesta link and the text-- Starts");
			ic.comprareText(ic.seVoiDisattivareilServizioText, InfoEnelEnergiaMyComponent.SEVOIDISATTIVAREILSERVIZIOTEXT, true); 
			ic.verifyComponentExistence(ic.avvialarichiesta);
			logger.write("Verify on Avvialarichiesta link and the text -- Ends");
			
			logger.write("Click on Avvialarichiesta link -- Starts");
			ic.clickComponent(ic.avvialarichiesta);
			Thread.sleep(20000);
			logger.write("Click on Avvialarichiesta link -- Ends");
			
			logger.write("Verify the Disattiva InfoEnelEnergia text -- Starts");
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
			ic.comprareText(ic.headingInfoEnergia1, InfoEnelEnergiaMyComponent.HeadingInfoEnergia1, true);
			ic.comprareText(ic.headingInfoEnergia2, InfoEnelEnergiaMyComponent.HeadingInfoEnergia2, true);
			logger.write("Verify the Disattiva InfoEnelEnergia text -- Starts");
			
			logger.write("Select supply and Continue button -- Starts");
			iee.clickComponent(iee.checkBox);
			iee.clickComponent(iee.continuaButton);
			logger.write("Select supply and Continue button -- Ends");
			
			logger.write("Verify the Riepilogo e conferma dati text -- Starts");
			iee.comprareText(iee.reipilogoeConfermaDati, InfoEnelEnergiaACRComponent.REIPILOGO_CONFERMA_DATI, true);
			iee.comprareText(iee.Ilservizioverr, InfoEnelEnergiaACRComponent.IlservizioverrText, true);
			logger.write("Verify the Riepilogo e conferma dati text -- Ends");
			
			logger.write("Click on INDIETRO button and verify -- Starts");
			iee.clickComponent(iee.backButton);
			logger.write("Click on INDIETRO button and verify -- Ends");
			
			logger.write("Click on Continue and Confirm button -- Starts");
			iee.clickComponent(iee.continuaButton);
			iee.comprareText(iee.reipilogoeConfermaDati, InfoEnelEnergiaACRComponent.REIPILOGO_CONFERMA_DATI, true);
			iee.comprareText(iee.Ilservizioverr, InfoEnelEnergiaACRComponent.IlservizioverrText, true);
			iee.clickComponent(iee.confermaButton); 
			Thread.sleep(20000);
			logger.write("Click on Continue and Confirm button -- Ends");
			
			logger.write("Verify the success text -- Starts");
			iee.comprareText(iee.OperazioneTitle, InfoEnelEnergiaACRComponent.OPERAZIONE_HEADER, true);
			logger.write("Verify the success text -- Ends");
			
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			iee.clickComponent(iee.finebutton);
			Thread.sleep(20000);
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Start editing and click on Forniture and Correct visualization of Forniture Page with central text -- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
