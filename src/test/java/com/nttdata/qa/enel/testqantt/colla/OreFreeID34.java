package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.components.colla.SubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeID34 {
	

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			logger.write("verifica presenza dell'offerta 'Ore Free' - Start");
			OreFreeComponent offerta=new OreFreeComponent(driver);
			
			/*By offerta_orefree=offerta.offertaOreFree;
			offerta.verifyComponentExistence(offerta_orefree);
			offerta.scrollComponent(offerta_orefree);*/
			logger.write("verifica presenza dell'offerta 'Ore Free' - Completed");
			
			logger.write("Click sul Link VERIFICA DISPONIBITA' e check sulla popup - Start");
			By verificadisponibilita_Button=offerta.verificadisponibilitaButton;
			offerta.verifyComponentExistence(verificadisponibilita_Button);
			offerta.clickComponent(verificadisponibilita_Button);
			TimeUnit.SECONDS.sleep(3);
			By inserisci_NumeroPod=offerta.inserisciNumeroPod;
			offerta.verifyComponentExistence(inserisci_NumeroPod);
			
			By testo_popup=offerta.testopopup;
			offerta.verifyComponentExistence(testo_popup);
			
			By campo_CodicePod=offerta.campoCodicePod;
			offerta.verifyComponentExistence(campo_CodicePod);
			
			By conferma_Button=offerta.confermaButton;
			offerta.verifyComponentExistence(conferma_Button);
			
			//By link_pod=offerta.linkpod;
			//offerta.verifyComponentExistence(link_pod);
			offerta.replaceLinkCollaInXpathAndCheckExistObject(prop.getProperty("LINK"));
			logger.write("Click sul Link VERIFICA DISPONIBITA' e check sulla popup - Completed");
			
			logger.write("Click su CONFERMA inserendo un valore random nel campo POD  - Start");
			TimeUnit.SECONDS.sleep(2);
			offerta.enterPodValue(campo_CodicePod, offerta.generateRandomPodNumberEnergia());
			offerta.clickeaspetta(conferma_Button);
			
			OCR_Module_Component ocr = new OCR_Module_Component(driver);
			ocr.checkHeader("Scegli Tu Ore Free", driver);
			/*
			By titolo_Page=offerta.titoloPage;
			offerta.verifyComponentExistence(titolo_Page);
			*/
			logger.write("Click su CONFERMA inserendo un valore random nel campo POD  - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}


}
