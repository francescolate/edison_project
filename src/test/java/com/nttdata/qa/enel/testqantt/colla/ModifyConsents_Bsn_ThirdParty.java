package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModifyConsents_BsnComponent;
import com.nttdata.qa.enel.components.colla.BsnSelfCareHomePageComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyConsents_Bsn_ThirdParty {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModifyConsents_BsnComponent modCon = new ModifyConsents_BsnComponent(driver);
			
			//System.out.println("Modifying third party consent");
			logger.write("Modifying third party consent - Start");
			modCon.modifyConsent(modCon.mailThirdPartyLocators);
			modCon.modifyConsent(modCon.telThirdPartyLocators);
			modCon.verifyVisibilityThenClick(modCon.confirmBtn);
			logger.write("Modifying third party consent - Completed");
			
			//System.out.println("Verifying operation results");
			logger.write("Verifying operation results - Start");
			modCon.checkThankYouPageStrings();
			modCon.verifyVisibilityThenClick(modCon.endBtn);
			logger.write("Verifying operation results - Completed");
			
			//System.out.println("Verifying return to home page");
			logger.write("Verifying return to home page - Start");
			BsnSelfCareHomePageComponent hp = new BsnSelfCareHomePageComponent(driver);
			hp.checkHomePageLanding();
			logger.write("Verifying return to home page - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
