package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_208_ACR_Menù_Laterale {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
			
			prop.setProperty("RECOVERY_PWD_STRING_1", "Recupera password");
			prop.setProperty("RECOVERY_PWD_STRING_2", "Inserisci l'indirizzo email che hai usato per registrarti a Enel Energia. Ti invieremo le istruzioni per impostare una nuova password.");
			prop.setProperty("RECOVERY_USRNM_STRING_1", "Recupera Username");
			prop.setProperty("RECOVERY_USRNM_STRING_2", "Inserisci il tuo codice fiscale per poter recuperare il tuo username.");
			prop.setProperty("REGISTER_STRING_1", "SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE");
			prop.setProperty("REGISTER_STRING_2", "Registrati nell'area riservata");
			prop.setProperty("ACCESS_ERR_STRING_1", "Username obbligatoria");
			prop.setProperty("ACCESS_ERR_STRING_2", "Password obbligatoria");
			prop.setProperty("ACCESS_PROBLEMS_STRING", "Se hai problemi di accesso");
			
			/*
			logger.write("Accessing login page - Start");
			pac.launchLink(prop.getProperty("LINK"));
			pac.clickComponent(pac.homePageClose);
			pac.verifyComponentExistence(pac.logoEnel);
			pac.verifyComponentExistence(pac.buttonAccetta);
			pac.clickComponent(pac.buttonAccetta);
			
			Thread.sleep(10000);
			pac.verifyComponentExistence(pac.iconUser);
			pac.clickComponent(pac.iconUser);
			logger.write("Accessing login page - Completed");
			
			logger.write("Verifying username and password fields - Start");
			pac.verifyComponentVisibility(pac.username);
			pac.verifyComponentVisibility(pac.password);
			logger.write("Verifying username and password fields - Completed");
			
			logger.write("Verifying recovery password and username links - Start");
			pac.verifyComponentVisibility(pac.recoveryPassword);
			pac.verifyComponentVisibility(pac.recoveryUsername);
			logger.write("Verifying recovery password and username links - Completed");
			
			logger.write("Verifying access problems link - Start");
			pac.verifyComponentVisibility(pac.accessProblemsLabel);
			pac.verifyComponentText(pac.accessProblemsLabel, prop.getProperty("ACCESS_PROBLEMS_STRING"));
			logger.write("Verifying access problems link - Completed");
			
			logger.write("Verifying social access buttons - Start");
			pac.verifyComponentVisibility(pac.googleAccessButton);
			pac.verifyComponentVisibility(pac.facebookAccessButton);
			logger.write("Verifying social access buttons - Completed");
					
			logger.write("Verifying registration elements - Start");
			pac.verifyComponentVisibility(pac.noAccountLabel);
			pac.verifyComponentVisibility(pac.registerButton);
			pac.verifyComponentVisibility(pac.accessProblemsLink);
			logger.write("Verifying registration elements - Completed");
			
			logger.write("Enter the username and password  - Start");
			pac.enterLoginParameters(pac.username, prop.getProperty("USERNAME"));
			pac.enterLoginParameters(pac.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			pac.verifyComponentVisibility(pac.buttonLoginAccedi);
			pac.clickComponent(pac.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			logger.write("Verify the home page title and contents - Start");
			pac.verifyComponentExistence(pac.bevenutoPrivataTitle);
			pac.compareText(pac.bevenutoPrivataTitle, PrivateAreaPageComponent.BEVENUTO_PRIVATA, true);
			pac.compareText(pac.bevenutoPrivataContent, PrivateAreaPageComponent.BEVENUTOPRIVATA_CONTENT, true);
			logger.write("Verify the home page title and contents - Complete");
			*/
			//pac.verifyComponentExistence(pac.enelPremiaLink);
			//pac.clickComponent(pac.enelPremiaLink);
			//verify the link issue
			
			logger.write("Verify and click on Novita link - Start");
			pac.verifyComponentExistence(pac.novitaLink);
			pac.clickComponent(pac.novitaLink);
			logger.write("Verify and click on Novita link - Start");
			
			logger.write("Verify and click on Novita heading and contents - Start");
			pac.verifyComponentExistence(pac.novitaHeading);
			pac.compareText(pac.novitaHeading, PrivateAreaPageComponent.NOVITA_HEADING, true);
			pac.verifyComponentExistence(pac.novitaContent);
			pac.compareText(pac.novitaContent, PrivateAreaPageComponent.NOVITA_CONTENT, true);
			logger.write("Verify and click on Novita heading and contents - Start");
			
			logger.write("Verify and click on TuoiDiritti link  - Start");
			pac.verifyComponentExistence(pac.tuoiDirittiLink);
			pac.clickComponent(pac.tuoiDirittiLink);
			logger.write("Verify and click on TuoiDiritti link  - Complete");
			
			logger.write("Verify the TuoiDiritti contents  - Start");
			pac.verifyComponentExistence(pac.tuoiq1);
			pac.compareText(pac.tuoiq1, PrivateAreaPageComponent.TUOI_Q1,true);
			pac.verifyComponentExistence(pac.tuoia1);
			pac.compareText(pac.tuoia1, PrivateAreaPageComponent.TUOI_A1, true);
			
			pac.verifyComponentExistence(pac.tuoia2a);
			pac.compareText(pac.tuoia2a, PrivateAreaPageComponent.TUOI_A2a,true);
			pac.verifyComponentExistence(pac.tuoia2b);
			pac.compareText(pac.tuoia2b, PrivateAreaPageComponent.TUOI_A2b, true);
			pac.verifyComponentExistence(pac.tuoiq3);
			pac.compareText(pac.tuoiq3, PrivateAreaPageComponent.TUOI_Q3,true);
			pac.verifyComponentExistence(pac.tuoia3);
			pac.compareText(pac.tuoia3, PrivateAreaPageComponent.TUOI_A3, true);
			pac.verifyComponentExistence(pac.tuoiq4);
			pac.compareText(pac.tuoiq4, PrivateAreaPageComponent.TUOI_Q4,true);
			pac.verifyComponentExistence(pac.tuoia4a);
			pac.compareText(pac.tuoia4a, PrivateAreaPageComponent.TUOI_A4a, true);
			pac.verifyComponentExistence(pac.tuoia4b);
			pac.compareText(pac.tuoia4c, PrivateAreaPageComponent.TUOI_A4c, true);
			pac.verifyComponentExistence(pac.tuoia4a);
			pac.compareText(pac.tuoia4a, PrivateAreaPageComponent.TUOI_A4a, true);
			pac.verifyComponentExistence(pac.tuoiq5);
			pac.compareText(pac.tuoiq3, PrivateAreaPageComponent.TUOI_Q3,true);
			pac.verifyComponentExistence(pac.tuoia5a);
			pac.compareText(pac.tuoia5a, PrivateAreaPageComponent.TUOI_A5a, true);
			pac.verifyComponentExistence(pac.tuoia5b);
			pac.compareText(pac.tuoia5b, PrivateAreaPageComponent.TUOI_A5b, true);
			pac.verifyComponentExistence(pac.tuoia5c);
			pac.compareText(pac.tuoia5c, PrivateAreaPageComponent.TUOI_A5c, true);
			pac.verifyComponentExistence(pac.tuoia5d);
			pac.compareText(pac.tuoia5d, PrivateAreaPageComponent.TUOI_A5d, true);
			pac.verifyComponentExistence(pac.tuoiq6);
			pac.compareText(pac.tuoiq6, PrivateAreaPageComponent.TUOI_Q6,true);
			pac.verifyComponentExistence(pac.tuoia6a);
			pac.compareText(pac.tuoia6a, PrivateAreaPageComponent.TUOI_A6a, true);
			pac.verifyComponentExistence(pac.tuoia6b);
			pac.compareText(pac.tuoia6b, PrivateAreaPageComponent.TUOI_A6b, true);
			pac.verifyComponentExistence(pac.tuoiq7);
			pac.compareText(pac.tuoiq7, PrivateAreaPageComponent.TUOI_Q7,true);
			pac.verifyComponentExistence(pac.tuoia7);
			pac.compareText(pac.tuoia7, PrivateAreaPageComponent.TUOI_A7, true);
			pac.verifyComponentExistence(pac.tuoiq8);
			pac.compareText(pac.tuoiq8, PrivateAreaPageComponent.TUOI_Q8,true);
			pac.verifyComponentExistence(pac.tuoia8);
			pac.compareText(pac.tuoia8, PrivateAreaPageComponent.TUOI_A8, true);
			logger.write("Verify the TuoiDiritti contents  - Complete");
		
			logger.write("Verify and click on account - Start");
			pac.verifyComponentExistence(pac.accountLink);
			pac.clickComponent(pac.accountLink);
			logger.write("Verify and click on account - Complete");
			
			logger.write("Verify the popup and click on SI button - Start");
			pac.verifyComponentExistence(pac.popupAttenzione);
			pac.compareText(pac.popupAttenzione, PrivateAreaPageComponent.ATTENZIONE, true);
			pac.verifyComponentExistence(pac.popupAttenzioneTitle);
			pac.compareText(pac.popupAttenzioneTitle, PrivateAreaPageComponent.ATTENZIONE_TITLE, true);
			pac.verifyComponentExistence(pac.popupNoButton);
			pac.verifyComponentExistence(pac.popupSIButton);
			pac.clickComponent(pac.popupSIButton);
			logger.write("Verify the popup and click on SI button - Complete");
			
			logger.write("Verify the daticontatti heading and contents - Start");
			pac.verifyComponentExistence(pac.datiContattiHeading);
			pac.compareText(pac.datiContattiHeading, PrivateAreaPageComponent.DATICONTATTIHEADING, true);
			pac.verifyComponentExistence(pac.datiContattiContent);
			pac.compareText(pac.datiContattiContent, PrivateAreaPageComponent.DATICONTATTICONTENT, true);
			logger.write("Verify the daticontatti heading and contents - Complete");
			
			logger.write("Verify and click on supporto link - Start");
			pac.verifyComponentExistence(pac.supportoLink);
			pac.clickComponent(pac.supportoLink);
			pac.checkUrl(prop.getProperty("SUPPORTO_URL"));
			logger.write("Verify and click on supporto link - Complete");
			
			logger.write("Click on Brower back button - Start");
			pac.backBrowser(pac.datiContattiHeading);
			logger.write("Click on Brower back button - Start");
			
			logger.write("Verify the daticontatti heading and contents - Start");
			pac.verifyComponentExistence(pac.datiContattiHeading);			
			pac.compareText(pac.datiContattiHeading, PrivateAreaPageComponent.DATICONTATTIHEADING, true);
			pac.verifyComponentExistence(pac.datiContattiContent);
			pac.compareText(pac.datiContattiContent, PrivateAreaPageComponent.DATICONTATTICONTENT, true);
			logger.write("Verify the daticontatti heading and contents - Complete");
			
			logger.write("Verify and click on TravoSpazio link - Start");
			pac.verifyComponentExistence(pac.travoSpazioLink);
			pac.clickComponent(pac.travoSpazioLink);
			logger.write("Verify and click on TravoSpazio link - Complete");
			
			logger.write("Switch to New window - Start");
			String currentHandle = driver.getWindowHandle();
			pac.SwitchToWindow();
			logger.write("Switch to New window - Complete");
			
			logger.write("Verify the TravoSpazio url - Start");
			pac.checkUrl(prop.getProperty("TRAVOSPAZIO_URL"));
			logger.write("Verify the TravoSpazio url - Complete");
			
			logger.write("Verify Travospazio home and title - Start");
			pac.verifyComponentExistence(pac.TravoSpazioHome);
			pac.compareText(pac.TravoSpazioHome, PrivateAreaPageComponent.TRAVOSPAZIO_HOME, true);
			pac.verifyComponentExistence(pac.TravoSpazioTitle);
			pac.compareText(pac.TravoSpazioTitle, PrivateAreaPageComponent.TRAVOSPAZIO_TITLE, true);
			logger.write("Verify Travospazio home and title - Complete");
			
			logger.write("close the window and switch back to parent window- Start");
			driver.close();
			driver.switchTo().window(currentHandle);
			logger.write("close the window and switch back to parent window- Complete");	
			
			logger.write("Verify and click on Esci link- Start");
			pac.verifyComponentExistence(pac.esciLink);
			pac.clickComponent(pac.esciLink);
			logger.write("Verify and click on Esci link- Complete");
			
			
			logger.write("Verify the colla home page - Start");
			pac.verifyComponentExistence(pac.logoEnel);
			logger.write("Verify the colla home page - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} catch (Exception e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}
		}

}
