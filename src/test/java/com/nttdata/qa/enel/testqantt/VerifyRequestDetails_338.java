package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.AddebitoDirettoComponent;
import com.nttdata.qa.enel.components.colla.GetAccountDetailsComponent;
import com.nttdata.qa.enel.components.colla.GetUserDetailFromWorkBenchComponent;
import com.nttdata.qa.enel.components.colla.ModificaAddebitoDirettoComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetails_338 {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			prop.setProperty("PROFILE_INFO_TEXT", "Titolare Mancuso Salvatore Codice FiscaleMNCSVT58H26H325L Telefono Fisso0818142364 Numero di Cellulare+39 3339090890 Email$email$ PECNon presente Canale di contatto preferito Telefono Cellulare");
			
			AccountComponent ac = new AccountComponent(driver);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			GetUserDetailFromWorkBenchComponent gc = new GetUserDetailFromWorkBenchComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.Query_338_a);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			logger.write("Inserendo la query - COMPLETED");
//			
			gc.comprareText(gc.description_338, gc.Description_338, true);
			gc.comprareText(gc.activityStatus_338, gc.ActivityStatus_338, true);
			gc.comprareText(gc.CF_338, prop.getProperty("CF"), true);
			gc.comprareText(gc.category_338, gc.Catergory_338, true);
			gc.comprareText(gc.IFM_Causale_338, gc.Causale__c_338, true);
			gc.comprareText(gc.cabDescription_338, gc.CABDescription_338, true);
			gc.comprareText(gc.email_338, prop.getProperty("EMAIL_INPUT"), true);
			gc.comprareText(gc.Descrizione_Tripletta_338, gc.DescrizioneTripletta_338, true);
			gc.comprareText(gc.IBAN_338, ModificaAddebitoDirettoComponent.IBANInput, true);
			gc.comprareText(gc.MMP_Reason_338, gc.MMPReason_338, true);
			gc.comprareText(gc.MMP_Reason_SDD_338, gc.MMPReasonSDD_338, true);
			gc.comprareText(gc.operationType_338, gc.OperationType_338, true);
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.Query_338_b);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			logger.write("Inserendo la query - COMPLETED");
			
			gc.comprareText(gc.acoountId_338, prop.getProperty("ACCOUNT_ID"), true);
			gc.comprareText(gc.description_338b, gc.Description_338, true);
			gc.comprareText(gc.Current_Method_Of_Payment_338, gc.Method_Of_Payment_338, true);
			gc.comprareText(gc.Method_Of_Payment_Previous_338, gc.Method_Of_Payment_338, true);
			gc.comprareText(gc.New_IBAN_338, gc.New_IBANValue_338, true);
			gc.comprareText(gc.New_Method_Of_Payment_338, gc.New_MethodOfPayment_338, true);
			gc.comprareText(gc.New_Method_Of_Payment_Name_338, gc.Method_Of_Payment_338, true);
			gc.comprareText(gc.POD_PDR_338, prop.getProperty("POD"), true);
			gc.comprareText(gc.R2D_Status_338, gc.Status_260C, true);
			gc.comprareText(gc.SAP_Status_338, gc.SAPStatus_338, true);
			gc.comprareText(gc.Service_Use_338, gc.IFM_Service_Use_338, true);
			gc.comprareText(gc.IFM_Status_338, gc.IFMStatus_338, true);
			gc.comprareText(gc.IFM_Subject_338, gc.IFMSubject_338, true);
			gc.comprareText(gc.UDB_Status_338, gc.UDBStatus_338, true);
		
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.Query_338_c);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			logger.write("Inserendo la query - COMPLETED");
			gc.comprareText(gc.operationStatus_338, gc.OperationStatus_338, true);
			gc.comprareText(gc.IFM_Status_338c, gc.IFMStatus_338c, true);

			
			
			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
