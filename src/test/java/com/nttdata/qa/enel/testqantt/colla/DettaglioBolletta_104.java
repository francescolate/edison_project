package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;


import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioBolletta_104 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		prop.setProperty("RESIDENTIAL_MENU", " Forniture | Bollette | Servizi | enelpremia WOW! | Novità | Spazio Video | Account | I tuoi diritti | Supporto | Trova Spazio Enel | Esci ");
		prop.setProperty("SupplyServices", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
		
		BolletteComponent 	bc = new BolletteComponent(driver);
		
		logger.write("Check for Residential Menu - Start");
		bc.checkForResidentialMenu(prop.getProperty("RESIDENTIAL_MENU"));
		logger.write("Check for Residential Menu - Completed");
		
		logger.write("Click on Bills from Residential Menu - Start");
		bc.clickComponent(bc.bills);
		logger.write("Click on Bills from Residential Menu - Completed");
		
		logger.write("Verification of List of One or more bills assocaited with the supply - Started");
		bc.verifyComponentExistence(bc.listofBills);
		logger.write("Verification of List of One or more bills assocaited with the supply - Completed");
		
			
		bc.verifyComponentExistence(bc.more);
		
		logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");
		bc.waitForElementToDisplay(bc.more);
		bc.verifyComponentExistence(bc.more);
		bc.clickComponent(bc.more);
		logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");

		bc.verifyComponentExistence(bc.dettaglioBolletta);		
		bc.verifyComponentExistence(bc.sacricaPDF);
				
		logger.write("Click on Dettaglio Bolletta - Start");
		bc.clickComponent(bc.dettaglioBolletta);
		logger.write("Click on Dettaglio Bolletta - Completed");

		DettaglioBollettaComponent db = new DettaglioBollettaComponent(driver);
		
		logger.write("Verify the fields in Dettaglio Fornitura page - Started");
		db.verifyComponentExistence(db.billHeading);
		db.comprareText(db.billHeading, DettaglioBollettaComponent.BILL_HEADING, true);
		
		db.verifyComponentExistence(db.addressLabel);
		db.comprareText(db.addressLabel, DettaglioBollettaComponent.ADDRESS_LABEL, true);
		
		db.verifyComponentExistence(db.numeroClienteLabel);
		db.comprareText(db.numeroClienteLabel, DettaglioBollettaComponent.NUMERO_CLIENTE_LABEL, true);
		
		db.verifyComponentExistence(db.numeroCliente);
		db.comprareText(db.numeroCliente, DettaglioBollettaComponent.NUMERO_CLIENTE, true);
		
		db.verifyBilldetails(db.billDetails, DettaglioBollettaComponent.exp);
		logger.write("Verify the fields in Dettaglio Fornitura page - Completed");
		
		prop.setProperty("RETURN_VALUE", "OK");
	
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
