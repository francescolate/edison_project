package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CheckSezioneSelezioneFornituraNonResidenzialeVCAMessaggioErroreFornituraNonAttivabile {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				
				logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Start");
				SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				SeleniumUtilities util = new SeleniumUtilities(driver);
				Thread.sleep(60000); 
				
                By sezione_fornitura=forn.sezioneSelezioneFornitura;
                forn.verifyComponentExistence(sezione_fornitura);                           
                
                By campo_CercaFornitura=forn.campoCercaFornitura;
                forn.verifyComponentExistence(campo_CercaFornitura);
                
                forn.scrollComponent(forn.pagina);
              
                TimeUnit.SECONDS.sleep(1);
                logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Completed");
			   
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: Commodity, Stato, Numero Cliente, POD/PDR, Indirizzo fornitura, Attivabilità, Offertabilità, POD Moroso e selezionare la fornitura di interesse - Start");
				forn.verificaColonneTabella(forn.colonne);
				forn.scrollComponent(forn.pagina);
				TimeUnit.SECONDS.sleep(15);
							
				forn.selezionaTuttiRecordInTabellaNonResidenziale();
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: Commodity, Stato, Numero Cliente, POD/PDR, Indirizzo fornitura, Attivabilità, Offertabilità, POD Moroso e selezionare la fornitura di interesse - Completed");
				
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna ATTIVABILITA' sia presente il valore 'Non Attivabile' - Start");
				
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(5);
				forn.checkValoreInTable(prop.getProperty("ATTIVABILITA"));
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna ATTIVABILITA' sia presente il valore 'Non Attivabile' - Completed");
				
				logger.write("verifica che il tasto 'Conferma Selezione Fornitura' rimanga disabilitato - Start");
				
				forn.verifyButtonIsNotEnabled("Conferma Selezione Fornitura");
				
				logger.write("verifica che il tasto 'Conferma Selezione Fornitura' rimanga disabilitato - Completed");
				TimeUnit.SECONDS.sleep(2);
				
				 util.getFrameActive();
		           logger.write("Verifica in presenza di un cliente multi fornitura con almeno una fornitura Non Attivabile del messaggio di alert - Start");
		            forn.verificaMessaggioErroreSelezioneFornitura(forn.messaggioErroreSelFornituraNonAttivabile, forn.messaggioAlertTestoAttesoFornNonAttivabile);
		            logger.write("Verifica in presenza di un cliente multi fornitura con almeno una fornitura Non Attivabile del messaggio di alert - Completed");
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
