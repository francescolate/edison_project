package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_ORE_FREE_59 {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			/*
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
			
			logger.write("check the username and passsword login - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME")); 

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));
			logger.write("check the username and passsword login - Completed");
			
			logger.write("Click on the login button- Start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			By accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
			logger.write("Verify the page after successful login - Complete");
					
			*/
			OreFreeDettaglioComponent ofdf = new OreFreeDettaglioComponent(driver);
			
			logger.write("Verify the header title - Start");
			ofdf.verifyComponentExistence(ofdf.headingTitle1);
			ofdf.comprareText(ofdf.headingTitle1, OreFreeDettaglioComponent.headingtitleValue1, true);
			ofdf.verifyComponentExistence(ofdf.headingTitle2);
			ofdf.comprareText(ofdf.headingTitle2, OreFreeDettaglioComponent.headlingtitleValue2, true);
			logger.write("Verify the header title complete ");
			
			logger.write("Verify the homepage title - Start");
			ofdf.verifyComponentExistence(ofdf.homepageTitle1);
			ofdf.comprareText(ofdf.homepageTitle1, OreFreeDettaglioComponent.homepagetitleValue1, true);
			ofdf.verifyComponentExistence(ofdf.homepageTitle2);
			ofdf.comprareText(ofdf.homepageTitle2, OreFreeDettaglioComponent.homepagetitleValue2, true);
			logger.write("Verify the homepage title - Complete");
			
			logger.write("Verify the homepage logo  - Start");
			ofdf.verifyComponentExistence(ofdf.HomePageLogo);
			ofdf.verifyComponentExistence(ofdf.datiaggiornati);
			logger.write("Verify the homepage logo  - Complete");
			
			logger.write("Verify and click on Orefree button  - Start");
			ofdf.verifyComponentExistence(ofdf.oreFreeButton);
			ofdf.clickComponent(ofdf.oreFreeButton);
			logger.write("Verify and click on Orefree button  - Complete");
			
			ofdf.verifyComponentExistence(ofdf.consumi);
			ofdf.comprareText(ofdf.consumi, OreFreeDettaglioComponent.consumiValue, true);
			
			ofdf.verifyComponentExistence(ofdf.consumiContent);
		//	ofdf.comprareText(ofdf.consumiContent, OreFreeDettaglioComponent.consumiContentValue, true);
			
			ofdf.verifyComponentExistence(ofdf.martedi);
			//ofdf.comprareText(ofdf.martedi, OreFreeDettaglioComponent.martediValue, true);
			
			ofdf.verifyComponentExistence(ofdf.Orefreetime);
			ofdf.checkForText(ofdf.Orefreetime, prop.getProperty("TIME"));
			
			ofdf.verifyComponentExistence(ofdf.scegliGiorno);
			ofdf.clickComponent(ofdf.scegliGiorno);
			Thread.currentThread().sleep(2000);;
			ofdf.clickComponent(ofdf.dateFirst);
			
			ofdf.verifyComponentExistence(ofdf.dateConferma);
			ofdf.clickComponent(ofdf.dateConferma);
						
			String dat = ofdf.firstDateofMonth();
			ofdf.checkForDate(ofdf.martedi, dat);
		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		
	}


}
