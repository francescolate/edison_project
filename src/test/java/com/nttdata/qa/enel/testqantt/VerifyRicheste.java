package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.lightning.CustomerDetailsPageComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.AttivitaComponent;
import com.nttdata.qa.enel.components.lightning.AttivitaDetailsComponent;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRicheste {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			GetAccountFromComponent gafc = new GetAccountFromComponent(driver);
			
			logger.write("typing the user CF - START");	
			gafc.verifyComponentExistence(gafc.searchBar);
			gafc.searchAccount(gafc.searchBar, prop.getProperty("CF"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("checking the data existence - START");	
			Thread.sleep(5000);
			gafc.clickComponent(gafc.clientiNome);
			
			CustomerDetailsPageComponent cc = new CustomerDetailsPageComponent(driver);
			Thread.sleep(5000);
			logger.write("Click Richesta Header  -- Start");	
			
			cc.clickComponent(cc.richestaHeader);
			logger.write("Click Richesta Header  -- Completed");	
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			System.out.println(now+"Date");
			cc.verifyComponentExistence(By.xpath(cc.caseNumber.replace("$DATE$", dtf.format(now))));
			System.out.println(By.xpath(cc.caseNumber.replace("$DATE$", dtf.format(now)))+"xpath");
			logger.write("Click Request number  -- Start");	
			Thread.sleep(5000);
			cc.jsClickObject(By.xpath(cc.caseNumber.replace("$DATE$", dtf.format(now))));
			logger.write("Click Request number  -- Completed");	
			Thread.sleep(5000);
			cc.clickComponent(cc.elementoRichesta);
			Thread.sleep(3000);
			logger.write("Verify Elemento Richesta details  -- Start");	
			cc.checkelementoRichestaValues(By.xpath(cc.elementoRichValue.replace("$Value$", prop.getProperty("STATO_ER"))), prop.getProperty("STATO_ER"));
			cc.checkelementoRichestaValues(cc.podER, prop.getProperty("POD"));
			cc.checkelementoRichestaValues(cc.numeroUtenzaER, prop.getProperty("NUMERO_UTENZA"));
			cc.checkelementoRichestaValues(By.xpath(cc.elementoRichValue.replace("$Value$", prop.getProperty("STATO_R2D"))), prop.getProperty("STATO_R2D"));
			cc.checkelementoRichestaValues(By.xpath(cc.elementoRichValue.replace("$Value$", prop.getProperty("STATO_SAP"))), prop.getProperty("STATO_SAP"));
			cc.checkelementoRichestaValues(By.xpath(cc.elementoRichValue.replace("$Value$", prop.getProperty("STATO_SEMPRE"))),prop.getProperty("STATO_SEMPRE"));
			cc.checkelementoRichestaValues(By.xpath(cc.elementoRichValue.replace("$Value$", prop.getProperty("STATO_SDSAP"))), prop.getProperty("STATO_SDSAP"));
			cc.checkelementoRichestaValues(By.xpath(cc.elementoRichValue.replace("$Value$", prop.getProperty("STATO_UDB"))), prop.getProperty("STATO_UDB"));
			logger.write("Verify Elemento Richesta details  -- Completed");	
			logger.write("Click on case order Id  -- Start");	
			cc.jsClickObject(cc.caseOI);
			logger.write("Click on case order Id  -- Completed");	
			Thread.sleep(10000);
			CaseItemDetailsComponent rc = new CaseItemDetailsComponent(driver);
			Thread.sleep(5000);
			rc.checkDataDiProcessoFields(rc.datadiprocessoText, "IR020");
			rc.checkDataDiProcessoFields(rc.datadiprocessoText, "LIMITATA");
			rc.checkDataDiProcessoFields(rc.datadiprocessoText, "Bassa Tensione");
			rc.checkDataDiProcessoFields(rc.datadiprocessoNumber, "3,5");
			rc.checkDataDiProcessoFields(rc.datadiprocessoNumber, "3,85");
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(10000);
			logger.write("Click on Attivia  -- Start");	
			Thread.sleep(10000);
			cc.jsClickObject(rc.attivaHeader);
			logger.write("Click on Attivia  -- Completed");	
			AttivitaDetailsComponent ac = new AttivitaDetailsComponent(driver);
			ac.verifyAttivaDetails(ac.attivaOne);
			ac.verifyAttivaDetails(ac.attivaSecond);
			ac.verifyComponentExistence(By.xpath(ac.dataApertura1.replace("$DATE$", dtf.format(now))));
			ac.verifyComponentExistence(By.xpath(ac.dataChiusura1.replace("$DATE$", dtf.format(now))));
			ac.verifyComponentExistence(By.xpath(ac.dataApertura2.replace("$DATE$", dtf.format(now))));
			ac.verifyComponentExistence(By.xpath(ac.dataChiusura2.replace("$DATE$", dtf.format(now))));
			driver.navigate().back();
			logger.write("Click on case number  -- Start");	
			Thread.sleep(5000);
			cc.jsClickObject(cc.caseNumER);
			logger.write("Click on case number  -- Completed");	
			Thread.sleep(5000);
			rc.jsClickObject(rc.annulmentButton);
			Thread.currentThread().sleep(30000);
			rc.selectRinunciaCliente(rc.rinunciaCliente);
			driver.navigate().refresh();
			Thread.currentThread().sleep(5000);
			driver.navigate().refresh();
			Thread.currentThread().sleep(5000);
//			rc.verifyComponentExistence(rc.Chiuso);
//			rc.verifyComponentExistence(rc.annullo);
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
