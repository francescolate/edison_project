package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.DuplicatoBolletteComponent;
import com.nttdata.qa.enel.components.colla.RettificaBollettaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Rettifica_ACR_373 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DuplicatoBolletteComponent mod = new DuplicatoBolletteComponent(driver);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		RettificaBollettaComponent  rbc = new RettificaBollettaComponent (driver);
		
		mod.comprareText(mod.homePageHeading1,  DuplicatoBolletteComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2,  DuplicatoBolletteComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH2, true);
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
		mod.comprareText(mod.sectionHeading1, DuplicatoBolletteComponent.SECTION_PAGE_HEADING1, true);
		mod.comprareText(mod.sectionParagraph1, DuplicatoBolletteComponent.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		mod.clickComponent(mod.rettificaBollette);
		rbc.comprareText(rbc.RettificaBollettaPageTitle, rbc.RETTIFICABOLLETTAPAGETITLE, true);
		rbc.comprareText(rbc.RettificaBollettaPageText, rbc.RETTIFICABOLLETTAPAGETEXT, true);
		Thread.sleep(20000);
		
		rbc.verifyComponentExistence(rbc.Indirizzodellafornitura);
		rbc.verifyComponentExistence(rbc.NumeroCliente);
		rbc.verifyComponentExistence(rbc.linkStoricoLetture);
		
		mod.verifyComponentExistence(mod.textInfoIcon);
		 
		mod.clickComponent(mod.textInfoIcon);
		mod.comprareText(mod.textInfoIconInnerText, mod.TEXTINFOICON_INNERTEXT, true);
		mod.clickComponent(mod.textInfoIconCloseButton);
		
		mod.verifyComponentExistence(mod.esciButtonRES);
		mod.verifyComponentExistence(mod.rettificaButton);
		
		driver.findElement(mod.esciButtonRES).isEnabled();
		
		mod.isavvivaRateizzazionedisabled(mod.rettificaButton);
		
		mod.clickComponent(mod.esciButtonRES);
		
		mod.comprareText(mod.homePageHeading1,  DuplicatoBolletteComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2,  DuplicatoBolletteComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH2, true);
		
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
