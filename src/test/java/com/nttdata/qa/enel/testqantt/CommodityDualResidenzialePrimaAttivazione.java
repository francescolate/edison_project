package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CommodityDualResidenzialePrimaAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {			 
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			
			//compila sezione commodity per fornitura GAS
			logger.write("compila sezione commodity per fornitura GAS, click su CONFERMA FORNITURA - Start");
			conferma.confermaCommodity(prop.getProperty("POD_GAS"));
			conferma.verificaCampiSezioneCommodityGas(prop.getProperty("POD_GAS"));
			conferma.selezionaLigtheningValue("Categoria di Consumo", prop.getProperty("CATEGORIA_CONSUMO"));
			conferma.selezionaLigtheningValue("Categoria Uso", prop.getProperty("CATEGORIA_USO"));
			conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
			conferma.sendText("Telefono SMS DEL. 40", prop.getProperty("TELEFONO_SMS"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			conferma.inserisciOrdineFittizio(prop.getProperty("ORDINE_FITTIZIO"));
			
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			logger.write("compila sezione commodity per fornitura GAS, click su CONFERMA FORNITURA - Completed");
			
			logger.write("compila sezione commodity per fornitura ELE e click su CONFERMA FORNITURA e poi su CONFERMA - Start");
			conferma.confermaCommodity(prop.getProperty("POD_ELE"));
			conferma.verificaCampiSezioneCommodityEle(prop.getProperty("POD_ELE"));
			conferma.inserisciResidente(prop.getProperty("RESIDENTE"));
			conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
			conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
			conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);
			logger.write("compila sezione commodity per fornitura ELE e click su CONFERMA FORNITURA e poi su CONFERMA - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
