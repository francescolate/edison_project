package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.PubblicoID80MotorinoRicercaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_ID_81_Motorino_Ricerca_ENG {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			PubblicoID80MotorinoRicercaComponent mrc = new PubblicoID80MotorinoRicercaComponent(driver);

			logger.write("Accessing the home page - Start");
			mrc.launchLink(prop.getProperty("HOMEPAGE"));
			Thread.sleep(6000);
			mrc.launchLink(prop.getProperty("LINK"));
			Thread.sleep(10000);
			mrc.verifyComponentExistence(mrc.buttonAccetta);
			mrc.clickComponent(mrc.buttonAccetta);
			logger.write("Accessing the home page - Complete");
			
			if(driver.findElement(mrc.buttonAcceptCookie).isDisplayed())
				driver.findElement(mrc.buttonAcceptCookie).click();

			Thread.sleep(10000);
			prop.setProperty("QUERY1", "What kind of contract?");
			prop.setProperty("QUERY2", "Where?");
			prop.setProperty("QUERY3", "What for?");
			
			mrc.checkURLAfterRedirection(prop.getProperty("LINK"));
			
			mrc.verifyComponentExistence(mrc.WhatkindOfContract_Query);
			mrc.comprareText(mrc.WhatkindOfContract_Query, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.Where_Query);
			mrc.comprareText(mrc.Where_Query, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.WhatFor_Query);
			mrc.comprareText(mrc.WhatFor_Query, prop.getProperty("QUERY3"), true);
			
			mrc.verifyComponentExistence(mrc.powerANDgasHeading);
			mrc.comprareText(mrc.powerANDgasHeading, mrc.PowerANDgasHeading, true);
			
			mrc.verifyComponentExistence(mrc.powerANDgasTitle);
			mrc.comprareText(mrc.powerANDgasTitle, mrc.PowerANDgasTitle, true);
			
			mrc.verifyComponentExistence(mrc.powerANDgasDescription);
			mrc.comprareText(mrc.powerANDgasDescription, mrc.PowerANDgasDescription, true);
			
			mrc.verifyComponentExistence(mrc.nearestEnelStoreLink);
			mrc.clickComponent(mrc.nearestEnelStoreLink);
			Thread.sleep(10000);
			
			mrc.checkURLAfterRedirection(prop.getProperty("ENEL_NEAREST_STORE_LINK"));
			
			mrc.verifyHeader();
			mrc.verifyFooterVisibility();
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

}
	
}
