package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaModificaStatoResidenza;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;




import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ProcessoModificaStatoResidenza {
	
	

	public static void main(String[] args) throws Exception {

		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CheckListComponent checklist = new CheckListComponent(driver);
			IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
			CompilaModificaStatoResidenza datiStatoResidenza = new CompilaModificaStatoResidenza(driver);
			FornitureComponent forniture = new FornitureComponent(driver);
			SeleniumUtilities  utils=new SeleniumUtilities(driver);
			TimeUnit.SECONDS.sleep(60);

			checklist.clickConferma();

			String numerorichiesta = nuovaRichiesta
					.salvaNumeroRichiesta(nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);

			prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());


			forniture.cercaInTabella(prop.getProperty("POD"));
			Thread.sleep(20000);
			forniture.cliccaPrimoElementoInTabella(forniture.checkboxPrimaFornituraInTabella);
			forniture.clickConfermaFornitura();

			datiStatoResidenza.selectStatoRes(prop.getProperty("STATO_RESIDENZA"));
			datiStatoResidenza.selectAvantiButton();
			datiStatoResidenza.verificaFornitureRiepilogoStatoResidenza(1);
			datiStatoResidenza.selectAvantiButton();

			datiStatoResidenza.checkCampiModalitaFirmaPrepopolati("Vocal", "NO");
			datiStatoResidenza.modalitaFirma(prop.getProperty("MODALITA_FIRMA"));
			datiStatoResidenza.selectAvantiButton();
			datiStatoResidenza.selezionaValore("Canale invio", prop.getProperty("CANALE_INVIO"));
			datiStatoResidenza.sendText("Indirizzo Email", prop.getProperty("EMAIL"));
			datiStatoResidenza.clickComponent(By.xpath("//label[contains(text(),'Canale invio')]"));
			datiStatoResidenza.selectConfermaButton();



			//GESTIONE POP UP  erifica che l’indirizzo di residenza coincida con l’indirizzo presente ....

			String popupText = "Verifica che l’indirizzo di residenza coincida con l’indirizzo presente nell’anagrafica del"
					+ " cliente e in caso contrario, modifica quest’ultimo con il processo dedicato";
			String buttonLabel="OK";
			By modal = By.xpath("//div[contains(@class,'slds-modal__container')]//p[text()='"+popupText+"']");
			By modalButton = By.xpath("//div[contains(@class,'slds-modal')]//button[text()='" + buttonLabel + "']");
			if(utils.verifyExistence(modal, 30))
			{
				if (!utils.verifyExistence(modalButton, 10))
					throw new Exception("Dopo aver confermato il processo, come atteso è mostrata a video una popup ma non contiene il pulsante " + buttonLabel);
				utils.objectManager(modalButton, utils.scrollAndClick);
			}
			else
			{
				String screenshotPath=Utility.takeSnapShot(prop, args[0],"pop_up_non_visualizzato");
				
			}


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}

