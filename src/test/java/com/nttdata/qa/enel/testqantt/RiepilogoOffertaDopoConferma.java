package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentaleComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RiepilogoOffertaDopoConferma {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

 //
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			SeleniumUtilities util=new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			
			String frame="";
			util.getFrameActive();
			logger.write("Corretta apertura della pagina con le sezioni Cliente, Riepilogo offerta, sommario offerta e click sul pulsante 'Torna all'offerta' - Start");
			offer.verifyComponentExistence(offer.sezioneCliente);
			offer.verifyComponentExistence(offer.sezioneSommarioOfferta);
			offer.verifyComponentExistence(offer.pageRiepilogoOff);
			modalita.pressAndCheckSpinners(modalita.tornaAllOffertaButton);
			
			logger.write("Verifica header riepilogo offerta  - Start");
			offer.verifyComponentText(offer.HeaderTipoLavorazione,"PRIMA_ATTIVAZIONE");
		
			offer.verifyComponentExistence(offer.HeaderQuote);
			offer.verifyComponentExistence(offer.HeaderNumeroContratto);
			
			if(prop.getProperty("COMMODITY").contentEquals("ELE"))
			{
				offer.verifyComponentText(offer.HeaderTipoOfferta,"Elettricità");
			}
			else if(prop.getProperty("COMMODITY").contentEquals("GAS"))
			{
				offer.verifyComponentText(offer.HeaderTipoOfferta,"Gas");
			}
			offer.verifyComponentExistence(offer.HeaderDataAdesione);
			offer.verifyComponentText(offer.HeaderStato,"Inviata");

			logger.write("Verifica header riepilogo offerta  - End");
			
			driver.switchTo().defaultContent();
			logger.write("Corretta apertura della pagina con le sezioni Cliente, Riepilogo offerta, sommario offerta e click sul pulsante 'Torna all'offerta' - Completed");


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
