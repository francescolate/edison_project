package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_136 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
		    prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));

			home.clickComponent(home.services);			

			ServicesComponent services = new ServicesComponent(driver);
			
			logger.write("Check For supply services-- start");
			
			services.checkForSupplyServices(prop.getProperty("SUPPLYSERVICES"));
			
			logger.write("Check For supply services-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");
			
			services.clickComponent(services.modificaPotenzaeoTensione);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Check Power And Voltage Content-- Start");

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageContent);
			
			changePV.checkForPageContent();
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageButton);

			logger.write("Click on button Change Power And Voltage Button -- Start");

			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText);

			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");
			
			logger.write("Check for Exit And Change Power Voltage Button -- Start");
			
			changePV.verifyComponentExistence(changePV.exitButton);
			
			changePV.verifyComponentExistence(changePV.changePowerOrVoltageButton);

			logger.write("Check for Exit And Change Power Voltage Button -- Completed");
			
			logger.write("Check for Exit And Change Power Voltage Button Status-- Start");

			changePV.checkForExitButtonStatus(changePV.exitButton);
			
			changePV.checkForChangePowerOrVoltageButtonStatus(changePV.changePowerOrVoltageButton);
			
			logger.write("Check for Exit And Change Power Voltage Button Status-- Completed");
			
			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");
			
			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");

			changePV.checkForChangePowerOrVoltageButtonStatus(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
			logger.write("Check for Change Power and voltage title and sub text-- Start");

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);
			
			changePV.verifyComponentExistence(changePV.titleSubText);
			
			logger.write("Check for Change Power and voltage title and sub text-- Completed");
			
			logger.write("Check for Menu Item -- Start");

			changePV.checkForMenuItem(prop.getProperty("MENUITEM"));
			
			logger.write("Check for Menu Item -- Completed");
			
			logger.write("Check for Power Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.powerLable);
			
			logger.write("Check for Power Lable -- Completed");
			
			logger.write("Change power field drop down value -- Start");

			changePV.changeDropDownValue(changePV.powerValue, prop.getProperty("POWERDROPDOWNVALUE"));
			
			logger.write("Change power field drop down value -- Completed");
			
			logger.write("Enter input to power field -- Start");
			
			changePV.enterInputtoField(changePV.powerInput, prop.getProperty("POWERINPUT"));
			
			logger.write("Enter input to power field -- Completed");
			
			changePV.clickComponent(changePV.radioYes);
			
			logger.write("Check for message -- Start");

			changePV.checkforErrorMsg(changePV.powerError,prop.getProperty("POWERERROR"));
			
			logger.write("Check for message  -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}

}
