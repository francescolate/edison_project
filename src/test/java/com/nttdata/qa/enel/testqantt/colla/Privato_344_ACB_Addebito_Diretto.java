package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato330ACBAddebitoDirettoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_344_ACB_Addebito_Diretto {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a MyEnel");
			prop.setProperty("HOMEPAGE_PATH", "AREA RISERVATA/HOMEPAGE");
			prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area dedicata Business");
			/*prop.setProperty("SECTION_TITLE", "Le tue forniture");
			prop.setProperty("SECTION_TITLE_SUBTEXT", "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.");*/
			/*prop.setProperty("SERVIZI_HOMEPAGE", "Servizi per le forniture");
			prop.setProperty("SERVIZI_HOMEPAGE_SUBTEXT", "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture");
			prop.setProperty("Modifica_Indirizzo_Fatturazione","Modifica Indirizzo Fatturazione");*/
			
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			
			logger.write("Launching the ENEL application portal - Start");
			log.launchLink(prop.getProperty("LINK"));
			logger.write("Launching the ENEL application portal - Completed");
			
			logger.write("Handling the alert Pop-Up - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("Handling the alert Pop-Up - Completed");
			
			logger.write("Checking ENEL LOGO presence - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);
			logger.write("Checking ENEL LOGO presence - Completed");
			
			logger.write("Checking presence of cookie ACCEPT button - Start");
			By buttonAccetta = log.buttonAccetta;
			log.verifyComponentExistence(buttonAccetta);
			log.clickComponent(buttonAccetta);
			logger.write("Checking presence of cookie ACCEPT button - Completed");
			
			logger.write("Checking presence of User ICON - Start");
			By userIcon = log.iconUser;
			log.verifyComponentExistence(userIcon);
			log.clickComponent(userIcon);
			logger.write("Checking presence of User ICON - Completed");
			
			logger.write("Checking if user is navigated to Login Page - Start");
			By loginPage = log.loginPage;
			log.verifyComponentExistence(loginPage);
			logger.write("Checking if user is navigated to Login Page - Completed");
			
			logger.write("Checking for presence of Login Page Title - Start");
			By loginPageTitle = log.accediAMyEnelLabel;
			log.verifyComponentExistence(loginPageTitle);
			log.verifyComponentText(loginPageTitle, prop.getProperty("LOGIN_PAGE_TITLE"));
			logger.write("Checking for presence of Login Page Title - Completed");
			
			logger.write("Checking for UserName, Password displayed on Login Page - Start");
			By userName = log.username;
			log.verifyComponentExistence(userName);
			log.enterLoginParameters(userName, prop.getProperty("USERNAME"));
			
			By Password = log.password;
			log.verifyComponentExistence(Password);
			log.enterLoginParameters(Password, prop.getProperty("PASSWORD"));
			logger.write("Checking for UserName, Password displayed on Login Page - Completed");
			
			log.verifyComponentVisibility(log.buttonLoginAccedi);
			
			logger.write("Verify and Click Login Button on Login Page - Start");
			By loginBtn = log.buttonLoginAccedi;
			log.verifyComponentExistence(loginBtn);
			log.clickComponent(loginBtn);
			logger.write("Verify and Click Login Button on Login Page - Completed");
			
			Privato330ACBAddebitoDirettoComponent adc = new Privato330ACBAddebitoDirettoComponent(driver);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if path and heading of homePage is displayed - Start");
			By homePagePath = adc.areaRiservata;
			adc.verifyComponentExistence(homePagePath);
			adc.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			By homepageSubheading = adc.pageHeading;
			adc.verifyComponentExistence(homepageSubheading);
			adc.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Click on 'serviziSelect' menu item - Start");
			By servizi = adc.serviziSelect;
			adc.verifyComponentExistence(servizi);
			adc.clickComponent(servizi);
			logger.write("Click on 'serviziSelect' menu item - Completed");
			
			prop.setProperty("SERVIZI_PATH", "AREA RISERVATA / SERVIZI");
			prop.setProperty("SERVIZI_SUBTEXT", "In questa pagina trovi tutti i servizi.");
			
			By serviziPath = adc.areaRiservata;
			adc.verifyComponentExistence(serviziPath);
			adc.VerifyText(serviziPath, prop.getProperty("SERVIZI_PATH"));
			
			By serviziSubtext = adc.serviziPageSubText;
			adc.verifyComponentExistence(serviziSubtext);
			adc.VerifyText(serviziSubtext, prop.getProperty("SERVIZI_SUBTEXT"));
			
			By AddebitoDirettoLink = adc.AddebitoDiretto_button;
			adc.verifyComponentExistence(AddebitoDirettoLink);
			adc.clickComponent(AddebitoDirettoLink);
			
			prop.setProperty("ADDEBITO_PAGE_TITLE", "Addebito Diretto");
			prop.setProperty("ADDEBITO_DIRETTO_PATH", "AREA RISERVATA / FORNITURE / ADDEBITO DIRETTO");
			prop.setProperty("ADDEBITO_DIRETTO_SUBTEXT", "Il servizio automatico che paga le tue bollette.");
			
			By addebitoDiretto_PageTitle = adc.AddebitoDiretto_PageTitle;
			adc.verifyComponentExistence(addebitoDiretto_PageTitle);
			adc.VerifyText(addebitoDiretto_PageTitle, prop.getProperty("ADDEBITO_PAGE_TITLE"));
			
			By addebitoDirettoPath = adc.areaRiservata;
			adc.verifyComponentExistence(addebitoDirettoPath);
			adc.VerifyText(addebitoDirettoPath, prop.getProperty("ADDEBITO_DIRETTO_PATH"));
			
			/*By addebitoDirettoSubtext = adc.
			adc.verifyComponentExistence(addebitoDirettoSubtext);
			adc.VerifyText(addebitoDirettoSubtext, prop.getProperty("ADDEBITO_DIRETTO_SUBTEXT"));*/
			
			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}