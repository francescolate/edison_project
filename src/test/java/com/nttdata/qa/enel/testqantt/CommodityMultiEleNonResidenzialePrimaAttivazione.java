package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CommodityMultiEleNonResidenzialePrimaAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			conferma.confermaCommodity(prop.getProperty("POD_1"));
			conferma.verificaCampiSezioneCommodityEle(prop.getProperty("POD_1"));
			
			conferma.inserisciCategoriaMerceologica(prop.getProperty("CATEGORIA_MERCEOLOGICA"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
			conferma.inserisciOrdineFittizio(prop.getProperty("ORDINE_FITTIZIO"));
			conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
			
			//conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
			//conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			//conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
			//conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
			//conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			Thread.sleep(15);
			
			conferma.confermaCommodity(prop.getProperty("POD_2"));
			conferma.verificaCampiSezioneCommodityEle(prop.getProperty("POD_2"));
			
			conferma.inserisciCategoriaMerceologica(prop.getProperty("CATEGORIA_MERCEOLOGICA"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
			conferma.inserisciOrdineFittizio(prop.getProperty("ORDINE_FITTIZIO"));
			conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
			
			
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
