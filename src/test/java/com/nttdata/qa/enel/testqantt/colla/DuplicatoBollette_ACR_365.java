package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.DuplicatoBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DuplicatoBollette_ACR_365 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DuplicatoBolletteComponent mod = new DuplicatoBolletteComponent(driver);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		
		String invioDocumenti ="https://www-coll1.enel.it/it/area-clienti/residenziale/invio_documenti";
		String iTuoiDiritti ="https://www-coll1.enel.it/it/area-clienti/residenziale/i-tuoi-diritti#!";
		String uploadDocumenti = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AC&email=r32019p109stato.contiweb@gmail.com";
		String guidaurl = "https://www-coll1.enel.it/content/dam/enel-it/servizi-online/pdf/Guida_alla_CompilazioneIstanze19_01_2016_v3.pdf";
				
		mod.comprareText(mod.homePageHeading1,  DuplicatoBolletteComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2,  DuplicatoBolletteComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH2, true);
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
		mod.comprareText(mod.sectionHeading1, DuplicatoBolletteComponent.SECTION_PAGE_HEADING1, true);
		mod.comprareText(mod.sectionParagraph1, DuplicatoBolletteComponent.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		mod.comprareText(mod.serviziPerLeContratto, mod.SECTION_PAGE_TITLE3, true);
		mod.verifyComponentExistence(mod.invioDocumenti);
		
		mod.clickComponent(mod.invioDocumenti);
		mod.checkURLAfterRedirection(invioDocumenti);
		mod.comprareText(mod.invioDocumentititle, mod.INVIO_DOCUMENTI_TITLE, true);
		mod.comprareText(mod.invioDocumentiText1, mod.INVIO_DOCUMENTI_TEXT1, true);
		mod.comprareText(mod.invioDocumentiText2, mod.INVIO_DOCUMENTI_TEXT2, true);
		mod.comprareText(mod.invioDocumentiText3, mod.INVIO_DOCUMENTI_TEXT3, true);
		
		mod.verifyComponentExistence(mod.email);
		
		mod.comprareText(mod.emailText, mod.EMAIL_TEXT, true);
		
		mod.clickComponent(mod.proseguiButton);
		
		mod.checkURLAfterRedirection(uploadDocumenti);
		mod.comprareText(mod.uploadDocumentiTitle, mod.UPLOAD_DOCUMENTI_TITLE, true);
		mod.comprareText(mod.uploadDocumentiText, mod.UPLOAD_DOCUMENTI_TEXT, true);
		mod.verifyComponentExistence(mod.computer);
		mod.verifyComponentExistence(mod.cloudStorage);
		mod.comprareText(mod.cloudStorage, mod.ClOUD_STORAGE_TEXT, true);
		
		mod.comprareText(mod.fileinCaricamento, mod.FILE_IN_CARICAMENTO, true);
		mod.comprareText(mod.fileinCaricamentoTExt, mod.FILE_IN_CARICAMENTO_TEXT, true);
		
		mod.verifyComponentExistence(mod.confermaButton);
		mod.verifyComponentExistence(mod.annullaButton);
		
		mod.comprareText(mod.guidaAllaCompilazione, mod.GUIDA_ALLA_COMPILAZIONE, true);
		
		mod.clickComponent(mod.guidaAllaCompilazione);
		
		String currentHandle = driver.getWindowHandle();
		mod.SwitchToWindow();
		
		mod.checkURLAfterRedirection(guidaurl);
		driver.close();
		driver.switchTo().window(currentHandle);
		
		mod.backBrowser(mod.invioDocumentititle);
		mod.checkURLAfterRedirection(invioDocumenti);
		mod.comprareText(mod.invioDocumentititle, mod.INVIO_DOCUMENTI_TITLE, true);
		mod.comprareText(mod.invioDocumentiText1, mod.INVIO_DOCUMENTI_TEXT1, true);
		mod.comprareText(mod.invioDocumentiText2, mod.INVIO_DOCUMENTI_TEXT2, true);
		mod.comprareText(mod.invioDocumentiText3, mod.INVIO_DOCUMENTI_TEXT3, true);
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
