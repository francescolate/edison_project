package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConfiguraProdottoElettricoResidenzialeFUI {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			logger.write("Configura prodotto elettrico "+prop.getProperty("PRODOTTO")+"- Start");
			TimeUnit.SECONDS.sleep(10);
			//vecchia gestione
//			configura.press(configura.linkElettricoResidenziale);
////			if (prop.containsKey("OPZIONE_VAS")){
//			if (!prop.getProperty("OPZIONE_VAS").equals("")){
//				configura.configuraProdottoConVas(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_VAS"));
//				logger.write("Configura prodotto elettrico "+prop.getProperty("PRODOTTO")+" con Vas: "+prop.getProperty("OPZIONE_VAS")+"- Completed");
//			}
//			else{
//			configura.configuraProdotto(prop.getProperty("PRODOTTO"));
//			logger.write("Configura prodotto elettrico "+prop.getProperty("PRODOTTO")+"- Completed");
//			}
			
			//Nuova gestione inserita da Capuano Tommaso il 22/07/2020
			configura.press(configura.linkElettricoResidenzialeFUI);
			configura.selezionaProdotto(prop.getProperty("PRODOTTO"));
			
			logger.write("Selezione del prodotto");

			if (prop.getProperty("MERCATO","").equals("FUI")){
				logger.write("Configura Prodotto FUI - Start");
				configura.configuraProdottoFUI(prop.getProperty("SEGMENTO_DEFAULT"),prop.getProperty("CAUSALE_DEFAULT"),prop.getProperty("AREA_DEFAULT"),prop.getProperty("ATTESA_NUOVA_VENDITA"),prop.getProperty("MERCATO"));
				logger.write("Configura Prodotto FUI - End");
			}
			
			if (!prop.getProperty("OPZIONE_KAM_AGCOR","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR");
			}
			if (!prop.getProperty("SCELTA_ABBONAMENTI","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO"),prop.getProperty("SCELTA_ABBONAMENTI"));
				logger.write("Selezione Scelta Abbonamenti");
			}
			
			if (!prop.getProperty("OPZIONE_VAS","").equals("")){
				configura.configuraVas(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_VAS"));
				logger.write("Selezione VAS");
			}
			if (!prop.getProperty("SCONTO","").equals("")){
				configura.configuraSconto(prop.getProperty("PRODOTTO"),prop.getProperty("SCONTO"));
				logger.write("Selezione Sconto");
			}
			configura.salvaConfigurazione();
			logger.write("Salva Configurazione");
			configura.checkOut();
			logger.write("Check out");
			
			logger.write("Configura prodotto ele "+prop.getProperty("PRODOTTO")+"- Completed");
			
			
			TimeUnit.SECONDS.sleep(10);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
