package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.colla.ServiziOnlineCaricaDocumentiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ServiziOnlineCaricaDocumentiWithData {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			
			ServiziOnlineCaricaDocumentiComponent sodc = new ServiziOnlineCaricaDocumentiComponent(driver);
		
			logger.write("Check INSERISCI I TUOI DATI section fields - START");
			//Section1
			sodc.verifyComponentExistence(sodc.inputNome);
			sodc.verifyComponentExistence(sodc.inputCognome);
			sodc.verifyComponentExistence(sodc.prefissoBy);
			sodc.verifyComponentExistence(sodc.prefissoDefaultBy);
			sodc.verifyComponentExistence(sodc.inputCellulare);
			sodc.verifyComponentExistence(sodc.indirizzoBy);
			sodc.verifyComponentExistence(sodc.inputEmail);
			sodc.verifyComponentExistence(sodc.inputConfermaEmail);
			sodc.verifyComponentExistence(sodc.codiceFiscalePIvaBy);
			sodc.verifyComponentExistence(sodc.seiGiaClienteBy);
			sodc.verifyComponentExistence(sodc.seiGiaClienteSiBy);
			sodc.verifyComponentExistence(sodc.seiGiaClienteNoBy);
			sodc.verifyComponentExistence(sodc.numeroClienteBy);
			sodc.verifyComponentExistence(sodc.asteriscoNumeroClienteBy);
			sodc.verifyComponentExistence(sodc.diCosaHaiBisognoBy);
			sodc.verifyComponentExistence(sodc.inviareUnaRichiestaBy);
			sodc.verifyComponentExistence(sodc.informazioniBy);
			sodc.verifyComponentExistence(sodc.argomentoBy);
			sodc.verifyComponentExistence(sodc.inputArgomento);
			sodc.verifyComponentExistence(sodc.messaggioBy);
			sodc.verifyComponentExistence(sodc.vuoiInserireAllegatiBy);
			sodc.verifyComponentExistence(sodc.vuoiInserireAllegatiSiBy);
			sodc.verifyComponentExistence(sodc.vuoiInserireAllegatiNoBy);
			//Section2
			sodc.verifyComponentExistence(sodc.informativaTextBy);
			//Section3
			sodc.verifyComponentExistence(sodc.accettoBy);
			sodc.verifyComponentExistence(sodc.accettoBy2);
			sodc.verifyComponentExistence(sodc.nonAccettoBy);
			sodc.verifyComponentExistence(sodc.proseguiBy);
			sodc.verifyComponentExistence(sodc.campiObbligatoriBy);
			logger.write("Check INSERISCI I TUOI DATI section fields - END");
			
			logger.write("Check Error Privacy - START");
			sodc.verifyComponentExistence(sodc.errorPrivacyNoDisplay);
			sodc.clickComponent(sodc.proseguiBy);
			sodc.verifyComponentExistence(sodc.errorPrivacy);
			sodc.verifyComponentNotExistence(sodc.errorPrivacyNoDisplay);
			logger.write("Check Error Privacy - END");
			
			logger.write("Check Error Without Data - END");
			logger.write("Accept Privacy - START");
			sodc.accettaPrivacy();
			Thread.sleep(5000);
			logger.write("Accept Privacy - END");
			sodc.clickComponent(sodc.proseguiBy);
			Thread.sleep(5000);
			logger.write("Check Error Without Data - END");
			
			logger.write("Insert Value - START");
			sodc.sendKey(sodc.inputNome, prop.get("NOME").toString());
			sodc.sendKey(sodc.inputCognome, prop.get("COGNOME").toString());
			sodc.sendKey(sodc.inputCellulare, "abcd");
			sodc.accettaPrivacy();
			sodc.clickComponent(sodc.proseguiBy);
			sodc.verifyComponentExistence(sodc.errorCellulare);
			sodc.clearKey(sodc.inputCellulare);
			sodc.sendKey(sodc.inputCellulare, prop.get("CELLULARE").toString());
			logger.write("Insert Value - END");
			
			logger.write("Check Email - START");
			sodc.verifyComponentExistence(sodc.errorEmailInvalidNoDisplay);
			sodc.verifyComponentNotExistence(sodc.errorEmailInvalid);
			sodc.sendKey(sodc.inputEmail, "@ 123 @");
			sodc.accettaPrivacy();
			sodc.clickComponent(sodc.proseguiBy);
			sodc.verifyComponentExistence(sodc.errorEmailInvalid);
			sodc.verifyComponentNotExistence(sodc.errorEmailInvalidNoDisplay);
			sodc.clearKey(sodc.inputEmail);
			sodc.verifyComponentNotExistence(sodc.errorConfermaEmail_2);
			logger.write("Check Email - END");	
			
			logger.write("Check Conferma Email - START");
			sodc.sendKey(sodc.inputEmail, prop.get("EMAIL").toString());
			sodc.sendKey(sodc.inputConfermaEmail, "@ 123 @");
			sodc.accettaPrivacy();
			sodc.clickComponent(sodc.proseguiBy);
			sodc.verifyComponentExistence(sodc.errorConfermaEmail_2);
			sodc.clearKey(sodc.inputConfermaEmail);
			sodc.sendKey(sodc.inputConfermaEmail, prop.get("EMAIL").toString());
			logger.write("Check Conferma Email - END");
			
			logger.write("Check Codice Fiscale / P.Iva - START");
			sodc.verifyComponentNotExistence(sodc.errorCodiceFiscalePIvaInvalid);
			sodc.sendKey(sodc.inputUsername, "1234");
			sodc.accettaPrivacy();
			sodc.clickComponent(sodc.proseguiBy);
			sodc.verifyComponentExistence(sodc.errorCodiceFiscalePIvaInvalid);
			sodc.clearKey(sodc.inputUsername);
			sodc.sendKey(sodc.inputUsername, prop.get("CF_PIVA").toString());
			logger.write("Check Codice Fiscale / P.Iva - END");
			
			logger.write("Check Argomento and Messaggio and Numero Cliente - START");
			sodc.getArgomentoNuovoContratto();
			sodc.sendKey(sodc.inputMessaggio, "PROVA TEST AUTOMATION ID 87");
			sodc.sendKey(sodc.inputNumeroCliente, "123456789");
			sodc.accettaPrivacy();
			sodc.clickComponent(sodc.proseguiBy);
			logger.write("Check Argomento and Messaggio and Numero Cliente - END");
			
			logger.write("Check Final Text - START");
			sodc.comprareText(sodc.textFinalBy, sodc.textFinal, true);
			sodc.verifyComponentExistence(sodc.buttonIndietro);
			logger.write("Check Final Text - END");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
