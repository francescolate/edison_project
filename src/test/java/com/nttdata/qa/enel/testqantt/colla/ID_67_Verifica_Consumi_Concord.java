package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertaEnergiaperCondominioComponent;
import com.nttdata.qa.enel.components.colla.VerificaconsumiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_67_Verifica_Consumi_Concord {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			log.clickComponent(log.buttonAccetta);

			HamburgerMenuComponent hc = new HamburgerMenuComponent(driver);
			logger.write("Click on hambergur menu - Start");
			hc.verifyComponentExistence(hc.iconMenu);
			Thread.sleep(20000);
			//hc.clickComponent(hc.closeBtn);
			hc.clickComponent(hc.iconMenu);
			logger.write("Click on hambergur menu - Completed");
			logger.write("Click on condomini on hambergur menu - Start");
			Thread.sleep(10000);
			hc.verifyComponentExistence(hc.condomini);
			hc.clickComponent(hc.condomini);
			logger.write("Click on condomini on hambergur menu - Completed");
			OffertaEnergiaperCondominioComponent oc = new OffertaEnergiaperCondominioComponent(driver);
			oc.checkURLAfterRedirection(oc.pageUrl);
			Thread.sleep(10000);
			logger.write("Verify Page Per i condomini title - Start");
			Thread.sleep(30000);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.comprareText(oc.pageTitle, oc.PageTitle, true);
			logger.write("Verify Page Per i condomini title - Completed");
			oc.comprareText(oc.servizionline, oc.OnlineService, true);
			oc.comprareText(oc.scoprilaNostra, oc.ScoprilaNostra, true);
			oc.comprareText(oc.verificaConsumiSubText, oc.VerificaConsumi, true);
			oc.clickComponent(oc.verificaConsumiSubText);

			VerificaconsumiComponent vc = new VerificaconsumiComponent(driver);
			logger.write("Verify Page navigation and page title - Start");
			vc.checkURLAfterRedirection(vc.pageUrl);
			Thread.sleep(10000);
			vc.verifyComponentExistence(vc.pageTitle);
			logger.write("Verify Page navigation and page title - Completed");
			vc.verifyComponentExistence(vc.pageTitle1);
			vc.verifyComponentExistence(vc.richiestadiVerificaConsumiFatturati);
			vc.comprareText(vc.richiestadiVerificaConsumiFatturatiSubText, vc.RichiestadiVerificaConsumiFatturatiSubText, true);
			vc.verifyComponentExistence(vc.amministratorediCondominio);
			vc.verifyComponentExistence(vc.partitaIva);
			vc.verifyComponentExistence(vc.codiceFiscaleAlfanumericoNumerico);
			vc.verifyComponentExistence(vc.telefono);
			vc.verifyComponentExistence(vc.email);
			
			logger.write("Verify lettura Della Prima Fornitura details - Start");
			vc.verifyComponentExistence(vc.letturaDellaPrimaFornitura);
			vc.comprareText(vc.letturaDellaPrimaFornituraSubText, vc.LetturaDellaPrimaFornituraSubText, true);
			vc.verifyComponentExistence(vc.denominazioneCondominio);
			vc.verifyComponentExistence(vc.Pdr);
			vc.verifyComponentExistence(vc.numeroCliente);
			vc.verifyComponentExistence(vc.misuratore);
			vc.verifyComponentExistence(vc.letturaCorrettore);
			Thread.sleep(20000);
			vc.verifyComponentExistence(vc.tipoFornitura);
			logger.write("Verify lettura Della Prima Fornitura details - Completed");
			logger.write("Click on Altra furnitura - Start");
			vc.clickComponent(vc.altraFurnitura);
			logger.write("Click on Altra furnitura - Completed");
			
			logger.write("Verify LETTURA DELLA SECONDA FORNITURA section details - Start");
			vc.verifyComponentExistence(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyComponentExistence(vc.denominazioneCondominioSecondaFornitura);
			vc.verifyComponentExistence(vc.PdrSecondaFornitura);
			vc.verifyComponentExistence(vc.numeroClienteSecondaFornitura);
			vc.verifyComponentExistence(vc.misuratoreSecondaFornitura);
			vc.verifyComponentExistence(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyComponentExistence(vc.tipoFornituraSecondaFornitura);
			logger.write("Verify LETTURA DELLA SECONDA FORNITURA section details - Completed");
			logger.write("Click on Altra furnitura - Start");
			vc.clickComponent(vc.altraFurnituraSecondaFornitura);
			logger.write("Click on Altra furnitura - Completed");

			logger.write("Verify LETTURA DELLA TERZA FORNITURA section details - Start");
			vc.verifyComponentExistence(vc.letturaCorrettoreTerzaFornitura);
			vc.verifyComponentExistence(vc.denominazioneCondominioTerzaFornitura);
			vc.verifyComponentExistence(vc.PdrTerzaFornitura);
			vc.verifyComponentExistence(vc.numeroClienteTerzaFornitura);
			vc.verifyComponentExistence(vc.misuratoreTerzaFornitura);
			vc.verifyComponentExistence(vc.letturaCorrettoreTerzaFornitura);
			vc.verifyComponentExistence(vc.tipoFornituraTerzaFornitura);
			logger.write("Verify LETTURA DELLA TERZA FORNITURA section details - Completed");
			logger.write("Click on eliminia terza furnitura - Start");
			vc.clickComponent(vc.eliminaTerzaFornitura);
			logger.write("Click on eliminia terza furnitura - Completed");
			vc.verifyFieldDisplay(vc.letturaDellaTerzaFornitura);
			logger.write("Click on eliminia seconda furnitura - Start");
			vc.clickComponent(vc.eliminaSecondaFornitura);
			logger.write("Click on eliminia seconda furnitura - Completed");

			vc.verifyFieldDisplay(vc.letturaDellaSecondaFornitura);
			
			vc.verifyComponentExistence(vc.letturaDellaPrimaFornitura);
			vc.verifyComponentExistence(vc.tipoFornitura);
			logger.write("Click on altra furnitura - Start");
			vc.clickComponent(vc.altraFurnitura);
			logger.write("Click on altra furnitura - Completed");

			vc.verifyComponentExistence(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyComponentExistence(vc.denominazioneCondominioSecondaFornitura);
			vc.verifyComponentExistence(vc.PdrSecondaFornitura);
			vc.verifyComponentExistence(vc.numeroClienteSecondaFornitura);
			vc.verifyComponentExistence(vc.misuratoreSecondaFornitura);
			vc.verifyComponentExistence(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyComponentExistence(vc.tipoFornituraSecondaFornitura);
			
			vc.changeDropDownValue(vc.tipoFornituraDropDownSF, vc.electricaSF);
			vc.verifyComponentExistence(vc.podSecondaFornitura);
			vc.verifyComponentExistence(vc.letturaA1SecondaFornitura);
			vc.verifyComponentExistence(vc.letturaA2SecondaFornitura);
			vc.verifyComponentExistence(vc.letturaA3SecondaFornitura);
			vc.verifyFieldDisplay(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyFieldDisplay(vc.misuratoreSecondaFornitura);
			logger.write("Click on elimina seconda furnitura - Start");
			vc.clickComponent(vc.eliminaSecondaFornitura);
			logger.write("Click on elimina seconda furnitura - Completed");
			vc.verifyFieldDisplay(vc.letturaDellaSecondaFornitura);
			
			vc.comprareText(vc.informativaPolicyText, vc.InformativaPolicyText, true);
			logger.write("Click on invia - Start");
			vc.clickComponent(vc.invia);
			logger.write("Click on invia - Completed");
			
			logger.write("Verify error fields - Start");
			vc.verifyComponentExistence(vc.amministratorediCondominioError);
			vc.verifyComponentExistence(vc.codiceFiscaleAlfanumericoNumericoError);
			vc.verifyComponentExistence(vc.denominazioneCondominioError);
			vc.verifyComponentExistence(vc.emailError);
			vc.verifyComponentExistence(vc.misuratoreError);
			vc.verifyComponentExistence(vc.misuratoreError);
			vc.verifyComponentExistence(vc.numeroClienteError);
			vc.verifyComponentExistence(vc.partitaIvaError);
			vc.verifyComponentExistence(vc.PdrError);
			vc.verifyComponentExistence(vc.telefonoError);
			logger.write("Verify error fields - Completed");

			logger.write("Verify fields with invalid input- Start");
			vc.enterInputParameters(vc.email, prop.getProperty("INVALID_EMAIL"));
			vc.verifyComponentExistence(vc.invalidEmailError);
			
			vc.enterInputParameters(vc.partitaIva, prop.getProperty("PARTITA_IVA"));
			vc.clickComponent(vc.invia);
			vc.verifyComponentExistence(vc.invalidPartitaIvaError);
			
			vc.enterInputParameters(vc.partitaIva, prop.getProperty("PARTITA_IVA"));
			vc.clickComponent(vc.invia);
			vc.verifyComponentExistence(vc.invalidPartitaIvaError);
			
			vc.enterInputParameters(vc.telefono, prop.getProperty("INVALID_TELEFONO"));
			vc.clickComponent(vc.invia);
			vc.verifyComponentExistence(vc.invalidTelefonoError);
			
			vc.enterInputParameters(vc.numeroCliente, prop.getProperty("NUMERO_CLIENTE"));
			vc.verifyComponentExistence(vc.invalidNumeroClientiError);
			
			vc.enterInputParameters(vc.misuratore, prop.getProperty("MISURATORE"));
			vc.verifyComponentExistence(vc.invalidmisuratoreError);
			
			vc.clickComponent(vc.altraFurnitura);
			vc.verifyComponentExistence(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyComponentExistence(vc.denominazioneCondominioSecondaFornitura);
			vc.verifyComponentExistence(vc.PdrSecondaFornitura);
			vc.verifyComponentExistence(vc.numeroClienteSecondaFornitura);
			vc.verifyComponentExistence(vc.misuratoreSecondaFornitura);
			vc.verifyComponentExistence(vc.letturaCorrettoreSecondaFornitura);
			vc.verifyComponentExistence(vc.tipoFornituraSecondaFornitura);
			
			Thread.sleep(5000);
			vc.changeDropDownValue(vc.tipoFornituraDropDownSF, vc.electricaSF);
			vc.enterInputParameters(vc.numeroClienteSecondaFornitura, prop.getProperty("NUMERO_CLIENTE"));
			vc.verifyComponentExistence(vc.invalidNumeroClientiSFError);
			
			vc.enterInputParameters(vc.podSecondaFornitura, prop.getProperty("POD"));
			vc.verifyComponentExistence(vc.invalidPODSFError);
			
			vc.enterInputParameters(vc.letturaA3SecondaFornitura, prop.getProperty("LETTURA"));
			vc.enterInputParameters(vc.letturaA2SecondaFornitura, prop.getProperty("LETTURA"));
			vc.enterInputParameters(vc.letturaA1SecondaFornitura, prop.getProperty("LETTURA"));
			vc.verifyComponentExistence(vc.invalidlettura1SFError);
			vc.verifyComponentExistence(vc.invalidlettura2SFError);
			vc.verifyComponentExistence(vc.invalidlettura3SFError);			
			logger.write("Verify fields with invalid input- Completed");

			vc.clickComponent(vc.eliminaSecondaFornitura);
			
			logger.write("Verify fields with valid input- Start");
			vc.enterInputParameters(vc.amministratorediCondominio, prop.getProperty("AMMINISTRATORE_DI_CONDOMINIO"));
			vc.enterInputParameters(vc.codiceFiscaleAlfanumericoNumerico, prop.getProperty("CODICE_FISCALE_IP"));
			vc.enterInputParameters(vc.partitaIva, prop.getProperty("PARTITA_IVA_IP"));
			vc.enterInputParameters(vc.denominazioneCondominio, prop.getProperty("DENOMINAZIONE_CONDOMINIO_IP"));
			vc.enterInputParameters(vc.numeroCliente, prop.getProperty("NUMERO_CLIENTE_IP"));	
			vc.enterInputParameters(vc.email, prop.getProperty("EMAIL"));
			vc.enterInputParameters(vc.telefono, prop.getProperty("TELEFONO_IP"));
			
			vc.changeDropDownValue(vc.tipoFornituraDropDown, vc.electrica);
			vc.enterInputParameters(vc.pod, prop.getProperty("POD_IP"));
			vc.enterInputParameters(vc.letturaA3, prop.getProperty("LETTURA_IP"));
			vc.enterInputParameters(vc.letturaA2, prop.getProperty("LETTURA_IP"));
			vc.enterInputParameters(vc.letturaA1, prop.getProperty("LETTURA_IP"));
			logger.write("Verify fields with valid input- Completed");
			vc.clickComponent(vc.accetto);
			logger.write("Click on invia - Strat");
			vc.clickComponent(vc.invia);
			logger.write("Click on invia - Completed");
			
			vc.verifyComponentExistence(vc.verificaConsumiFatturati);
			vc.comprareText(vc.verificaConsumiFatturatiSubText, vc.VerificaConsumiFatturatiSubText, true);
			vc.verifyComponentExistence(vc.riepilogoDatiCliente);
			logger.write("Verify field details with previous inputs - Strat");
			vc.checkForData(vc.amministratorediCondominioIp, prop.getProperty("AMMINISTRATORE_DI_CONDOMINIO"));
			vc.checkForData(vc.cfIp, prop.getProperty("CODICE_FISCALE_IP"));
			vc.checkForData(vc.partitaIvaIp, prop.getProperty("PARTITA_IVA_IP"));
			vc.checkForTelefonoValue(vc.telefonoIp, prop.getProperty("TELEFONO_IP"));
			//vc.checkForData(vc.telefonoIp, prop.getProperty("TELEFONO"));
			vc.checkForData(vc.emailIp, prop.getProperty("EMAIL"));
			vc.checkForData(vc.denominazioneCondominioIp, prop.getProperty("DENOMINAZIONE_CONDOMINIO_IP"));
			vc.checkForData(vc.numeroClienteIp, prop.getProperty("NUMERO_CLIENTE_IP"));
			vc.checkForData(vc.podIp, prop.getProperty("POD_IP"));
			vc.checkForData(vc.letturaA3Ip, prop.getProperty("LETTURA_IP"));
			vc.checkForData(vc.letturaA2Ip, prop.getProperty("LETTURA_IP"));
			vc.checkForData(vc.letturaA1Ip, prop.getProperty("LETTURA_IP"));
			logger.write("Verify field details with previous inputs - Completed");
			logger.write("Click on confirma - Strat");
			vc.clickComponent(vc.confirma);
			logger.write("Click on confirma - Completed");

			Thread.sleep(10000);
			vc.comprareText(vc.confirmaText, vc.ConfirmaText, true);
						
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
