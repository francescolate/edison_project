package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AddressValidate {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
//			prop.setProperty("FORNITURETITLE", "Servizi per le forniture");
//			prop.setProperty("BOLLETTETITLE", "Servizi per le bollette");
//			prop.setProperty("CONTRATTOTITLE", "Servizi per il contratto");
//			prop.setProperty("FORNITURECONTENT", "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture");
//			prop.setProperty("BOLLETTECONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette");
//			prop.setProperty("CONTRATTOCONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture");
//			prop.setProperty("ModificaTitle", "Modifica Indirizzo di Fatturazione");
//			prop.setProperty("CITTAWARNINGMSG", "Per proseguire è necessario inserire la Città");
//			prop.setProperty("INDRIZZOWARNINGMSG", "Per proseguire è necessario inserire un indirizzo di recapito");
//			prop.setProperty("CIVICWARNINGMSG", "Per proseguire è necessario indicare il numero civico");

			//Supply Details Servizi page
			
			SuplyPrivateDetailComponent spdc = new SuplyPrivateDetailComponent(driver);
			
			logger.write("Verify and click the Servizi link - Start");
			spdc.clickComponent(spdc.ServiziLink);
			logger.write("Verify and click the Servizi link - Start");
			
			logger.write("Verify the title - Start");
//			spdc.VerifyBollette(spdc.FornituraTitle);
//			spdc.VerifyBollette(spdc.FornituraTitleContent);
			spdc.VerifyBollette(spdc.BolletteTitle);
			spdc.VerifyBollette(spdc.BolletteTitleContent);
			spdc.VerifyBollette(spdc.ContratoTitle);
			spdc.VerifyBollette(spdc.ContratoTitleContent);
			logger.write("Verify the title  - Complete");
			
			logger.write("Verify the  contents - Start");
			spdc.VerifyForniture(spdc.FornitureContents);
			spdc.VerifyBollette(spdc.BolletteContents);
			logger.write("Verify the  contents - Complete");
			
			logger.write("Click on Modiffica link - Start");
			spdc.clickComponent(spdc.ModifficaIndrizzolink);
			logger.write("Click on Modiffica link - Complete");
			
			logger.write("Verify the title and contents - Start");
			spdc.verifyComponentExistence(spdc.ModificaTitle);
			spdc.verifyComponentExistence(spdc.ModificaContents);
			logger.write("Verify the title and contents - Complete");
			
			
			logger.write("Click on the checkbox  - Start");
			spdc.clickComponent(spdc.ModificaCheckbox);
			spdc.clickComponent(spdc.ModificaButton);
			logger.write("Click on the checkbox - Complete");
					
			
			spdc.verifyComponentExistence(spdc.CittaInputField176);
			spdc.clickComponent(spdc.ContinuaBtn);
			spdc.verifyComponentExistence(spdc.CittaWarningMessage);
			spdc.enterLoginParameters(spdc.CittaInputField176, prop.getProperty("CITTAINPUTVALUE"));
			spdc.verifyComponentExistence(spdc.IndrizzoInput176);
			
			
			spdc.clickComponent(spdc.ContinuaBtn);
			spdc.verifyComponentExistence(spdc.CittaWarningMessage);
			spdc.enterLoginParameters(spdc.IndrizzoInput176, prop.getProperty("INDRIZZOINPUTVALUE"));
			spdc.verifyComponentExistence(spdc.NumercoCivicInput176);
			
			
			spdc.clickComponent(spdc.ContinuaBtn);
			spdc.verifyComponentExistence(spdc.CittaWarningMessage);
			spdc.enterLoginParameters(spdc.NumercoCivicInput176, prop.getProperty("NUMERCOCIVICINPUTVALUE"));

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		

	}

}
