package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.CheckVariazioneAnagraficaComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class Identificazione_Interlocutore_ID23 {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
		
			logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
			identificaInterlocutore.verifyInputFieldExist("Numero fattura");
			identificaInterlocutore.verifyInputFieldExist("Numero Cliente");
			identificaInterlocutore.verifyInputFieldExist("Documento");
			identificaInterlocutore.verifyInputFieldExist("Importo Ultima Fattura");
			identificaInterlocutore.verifyInputFieldExist("Data Scadenza Ultima Fattura");
			identificaInterlocutore.verifyInputFieldExist("Data Emissione Ultima Fattura");
			identificaInterlocutore.verifyInputFieldExist("Indirizzo di fornitura");
			
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
			
			identificaInterlocutore.verifyInputFieldIsNotEmpty("Nome");
			identificaInterlocutore.verifyInputFieldIsNotEmpty("Cognome");
//			identificaInterlocutore.verifyInputFieldIsNotEmpty("Codice Fiscale");
			logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Completed");
			
			logger.write("Inserimento documento e conferma - Start");
			identificaInterlocutore.insertDocumento("b");
			identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
			identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
			logger.write("Inserimento documento e conferma - Completed");
			Thread.currentThread().sleep(15000);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
