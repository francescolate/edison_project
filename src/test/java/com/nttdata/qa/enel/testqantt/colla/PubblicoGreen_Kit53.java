package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.GreenKitComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ImpreseComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.components.colla.SupportPrivacyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PubblicoGreen_Kit53 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			log.clickComponent(log.buttonAccetta);

			PublicAreaComponent pc = new PublicAreaComponent(driver);
			logger.write("click on imprese - Start");
			Thread.sleep(5000);
			pc.clickComponent(pc.impresse);
			logger.write("click on imprese - Completed");

			Thread.currentThread().sleep(10000);
			ImpreseComponent ic = new ImpreseComponent(driver);
			ic.checkURLAfterRedirection(ic.impresePageUrlCredential);
			Thread.sleep(5000);
			ic.comprareText(ic.impreseTitle, ic.ImpresePageTitle, true);
			ic.comprareText(ic.impreseSubText, ic.ImpresePageSubText, true);
			logger.write("click on scopri di pui - Start");
			ic.clickComponent(ic.scopridipui);
			logger.write("click on scopri di pui - Completed");

			Thread.currentThread().sleep(10000);
			GreenKitComponent gc = new GreenKitComponent(driver);
			ic.checkURLAfterRedirection(gc.greenKitPageUrl);
			ic.comprareText(gc.greenkitPageTitle, gc.PageTitle, true);
			ic.comprareText(gc.greenkitSubText, gc.PageSubText, true);
			
			gc.verifyComponentExistence(gc.richiediiltuoGreenKit);
			gc.verifyComponentExistence(gc.email);
			gc.verifyComponentExistence(gc.codiceCliente);
			ic.comprareText(gc.checkBoxText, gc.CheckBoxText, true);
			ic.comprareText(gc.textUnderCheckBox, gc.TextUnderCheckBox, true);
			logger.write("click on informativa policy - Start");
			gc.clickComponent(gc.informativaPolicy);
			logger.write("click on informativa policy - Completed");

			Thread.currentThread().sleep(10000);
			SupportPrivacyComponent sc = new SupportPrivacyComponent(driver);
			ic.checkURLAfterRedirection(sc.urlCredential);
			logger.write("Verify informativa policy title - Start");
			sc.verifyComponentExistence(sc.privacyTitle);
			logger.write("Verify informativa policy title - Completed");

			ic.comprareText(sc.privacyText, sc.Privacy_Text, true);
			driver.navigate().back();
			Thread.sleep(5000);
			ic.checkURLAfterRedirection(gc.greenKitPageUrl);
			ic.comprareText(gc.greenkitPageTitle, gc.PageTitle, true);
			ic.comprareText(gc.greenkitSubText, gc.PageSubText, true);
			logger.write("Verify header section - Start");
		
			gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("LUCE_E_GAS"));
			gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("IMPRESA"));
		//	gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("INSIEME A TE"));
			gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("STORIE"));
			gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("FUTUR-E"));
			gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("MEDIA"));
			gc.verifyHeaderVoice(gc.headerVoice,prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");
			
			ic.comprareText(gc.enelItalia, gc.EnelItalia, true);
			ic.comprareText(gc.enelEnergia, gc.EnelEnergia, true);			
			Thread.sleep(5000);
			logger.write("Verify footer section - Start");
			gc.verifyFooterLink(gc.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
			gc.verifyFooterLink(gc.footerLink, prop.getProperty("CREDITS"));
			gc.verifyFooterLink(gc.footerLink, prop.getProperty("PRIVACY"));
			gc.verifyFooterLink(gc.footerLink, prop.getProperty("COOKIE_POLICY"));
			gc.verifyFooterLink(gc.footerLink, prop.getProperty("REMIT"));
			logger.write("Verify footer section - Completed");
			logger.write("Click on invia - Start");
			gc.clickComponent(gc.invia);
			logger.write("Click on invia - Completed");
			ic.comprareText(gc.emailError, gc.ErrorMsg, true);
			ic.comprareText(gc.codiceClienteError, gc.ErrorMsg, true);
			ic.comprareText(gc.checkBoxError, gc.ErrorMsg, true);
			
			logger.write("Enter input to email and codice cliente field - Start");
			Thread.currentThread().sleep(5000);
			gc.enterInputParameters(gc.email, prop.getProperty("WP_USERNAME"));
			Thread.currentThread().sleep(5000);
			gc.enterInputParameters(gc.codiceCliente, prop.getProperty("CODICE_CLIENTE"));
			logger.write("Enter input to email and codice cliente field - Completed");
			gc.verifyComponentExistence(gc.checkBoxText);
			Thread.currentThread().sleep(5000);
			gc.clickComponent(gc.checkBoxText);
			//gc.clickComponent(gc.checkBoxText);
			logger.write("Click on invia - Start");
			gc.clickComponent(gc.invia);
			logger.write("Click on invia - Completed");
			
			logger.write("Verify pop up and click on chiudi - Start");
			ic.comprareText(gc.popUpTitle, gc.PopupTitle, true);
			ic.comprareText(gc.popUpText, gc.PopupText, true);
			gc.clickComponent(gc.chiudi);
			logger.write("Verify pop up and click on chiudi - Completed");

			ic.checkURLAfterRedirection(pc.publicAreaUrl);
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
