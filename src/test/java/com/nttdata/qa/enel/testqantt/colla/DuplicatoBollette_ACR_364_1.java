package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.DuplicatoBolletteComponent;
import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DuplicatoBollette_ACR_364_1 {
	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DuplicatoBolletteComponent mod = new DuplicatoBolletteComponent(driver);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		
		String duplicatoBolletta ="https://www-coll1.enel.it/it/area-clienti/residenziale/duplicato_bolletta";
		String iTuoiDiritti ="https://www-coll1.enel.it/it/area-clienti/residenziale/i-tuoi-diritti#!";
				
		mod.comprareText(mod.homePageHeading1,  DuplicatoBolletteComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2,  DuplicatoBolletteComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH2, true);
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
		mod.comprareText(mod.sectionHeading1, DuplicatoBolletteComponent.SECTION_PAGE_HEADING1, true);
		mod.comprareText(mod.sectionParagraph1, DuplicatoBolletteComponent.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		mod.comprareText(mod.serviziperLeBollette, mod.SECTION_PAGE_TITLE, true);
		mod.verifyComponentExistence(mod.duplicatoBolletta);
		
		mod.clickComponent(mod.duplicatoBolletta);
		mod.checkURLAfterRedirection(duplicatoBolletta);
		mod.comprareText(mod.sectionHeading2, mod.SECTION_PAGE_HEADING2, true);
		mod.comprareText(mod.sectionHeading3, mod.SECTION_PAGE_HEADING3, true);
		
		mod.comprareText(mod.enelEnergiaSPA, mod.ENEL_ENERGIA_SPA, true);
		mod.comprareText(mod.tuttiDiDirittiRiservati, mod.TUTTI_I_DIRITTI, true);
		mod.comprareText(mod.gruppoIVA, mod.GRUPO_IVA, true);
		mod.comprareText(mod.informazioniLegali, mod.INFORMAZIONI_LEGALI, true);
		mod.comprareText(mod.privacy, mod.PRIVACY, true);
		mod.comprareText(mod.credits, mod.CREDITS, true);
		mod.comprareText(mod.contattaci, mod.CONTATTACI, true);
		
		mod.clickComponent(mod.iTuoiDiritti);
		
		mod.checkURLAfterRedirection(iTuoiDiritti);
		
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
