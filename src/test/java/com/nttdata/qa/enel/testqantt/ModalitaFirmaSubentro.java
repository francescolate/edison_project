package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneAppuntamentoComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModalitaFirmaSubentro {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			String statoUbiest = Costanti.statusUbiest;
			logger.write("Selezione Modalità Firma - Start ");
			modalita.selezionaLigtheningValue("Modalità Firma", prop.getProperty("MODALITA_FIRMA"));
			logger.write("Selezione Modalità Firma - Start ");
			logger.write("Selezione canale Invio - Start ");
			String tipoCliente = prop.getProperty("TIPO_CLIENTE", "").toLowerCase();

			if (prop.getProperty("MODALITA_FIRMA").equals("DIGITAL")) {
				// modalita.selezionaLigtheningValue("Canale Invio", "Canale Invio",
				// prop.getProperty("CANALE_INVIO_FIRMA"));
				if (prop.getProperty("CANALE_INVIO_FIRMA").toLowerCase().contentEquals("email")) {
					modalita.sendText("Indirizzo Email", prop.getProperty("EMAIL"));
					// modalita.clearAndSendKeys(modalita.cellulareField,
					// prop.getProperty("CELLULARE"));
				}

			}

			if (prop.getProperty("CANALE_INVIO_FIRMA").equals("STAMPA LOCALE")) {
				modalita.selezionaCanaleInvio(prop.getProperty("CANALE_INVIO_FIRMA"));
				modalita.clickComponentWithJseAndCheckSpinners(modalita.confermaButton);
			} else if (!prop.getProperty("CANALE_INVIO_FIRMA").toLowerCase().contentEquals("stampa locale")) {
				if(prop.getProperty("CANALE_INVIO_FIRMA").equals("POSTA")) {
				modalita.selezionaCanaleInvio(prop.getProperty("CANALE_INVIO_FIRMA"));
				if (statoUbiest.compareTo("ON") == 0) {
					logger.write("Selezione Indirizzo Modalità firma e Canale Invio - Start");
					compila.selezionaIndirizzoEsistenteSeNonSelezionato("Modalità firma e Canale Invio");
					logger.write("Selezione Indirizzo Modalità firma e Canale Invio - Completed");
				} else if (statoUbiest.compareTo("OFF") == 0) {
					compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Modalità firma e Canale Invio",
							prop.getProperty("CAP_FORZATURA"), prop.getProperty("CITTA_FORZATURA"));
				}
				} else {
					modalita.clickComponentWithJseAndCheckSpinners(modalita.confermaButton);
				}

				if (!prop.getProperty("VERIFICA_ERRORE_SE_SEP", "").equals("")) {
					logger.write("Verifica Alert: \"La modalità di firma NO VOCAL è compatibile con il prodotto Fibra solo per canali SE e SEP e canale invio contratto STAMPA LOCALE\" - Start");
					modalita.VerificaErroreSeSep(modalita.PopupErroreSep);
					logger.write("Alert Presente");
					logger.write("Verifica Alert: \"La modalità di firma NO VOCAL è compatibile con il prodotto Fibra solo per canali SE e SEP e canale invio contratto STAMPA LOCALE\" - Completed");
				}
			} else {
				modalita.clickComponentWithJseAndCheckSpinners(modalita.confermaButton);
			}

			if (prop.getProperty("MODALITA_FIRMA").toLowerCase().contentEquals("vocal order")) {
				if (!prop.getProperty("REGISTRAZIONE_VOCAL", "").contentEquals("Y")) {
					modalita.clickComponentWithJseAndCheckSpinners(modalita.registrazioneVocaleButton);
					modalita.clickComponentWithJseAndCheckSpinners(modalita.forzaVocalOrder);
				}
			}

			logger.write("Selezione canale Invio - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
