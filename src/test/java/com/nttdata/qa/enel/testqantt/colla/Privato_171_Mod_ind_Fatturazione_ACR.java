package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato170ModIndFatturazioneACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_171_Mod_ind_Fatturazione_ACR {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato170ModIndFatturazioneACRComponent MIF = new Privato170ModIndFatturazioneACRComponent(driver);
			
			MIF.verifyComponentVisibility(MIF.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = MIF.homePageCentralText;
			MIF.verifyComponentExistence(homePage);
			MIF.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			By homepageSubheading = MIF.homePageCentralSubText;
			MIF.verifyComponentExistence(homepageSubheading);
			MIF.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = MIF.sectionTitle;
			MIF.verifyComponentExistence(sectionTitle);
			MIF.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = MIF.sectionTitleSubText;
			MIF.verifyComponentExistence(sectionTitleSubText);
			MIF.VerifyText(sectionTitleSubText, MIF.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verifying the presence of 'dettaglioFornituraButton','visualizzaLeBolletteButton' and click on 'serviziSelect' menu item - Start");
			By dettaglioFornituraButton =  MIF.dettaglioFornitura_button;
			MIF.verifyComponentVisibility(dettaglioFornituraButton);
			
			By visualizzaLeBolletteButton = MIF.visualizzaLeBollette_button;
			MIF.verifyComponentVisibility(visualizzaLeBolletteButton);
			
			By servizi = MIF.serviziSelect;
			MIF.verifyComponentExistence(servizi);
			MIF.clickComponent(servizi);
			logger.write("Verifying the presence of 'dettaglioFornituraButton','visualizzaLeBolletteButton' and click on 'serviziSelect' menu item - Completed");
			
			MIF.verifyComponentVisibility(MIF.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = MIF.serviziPage;
			MIF.verifyComponentExistence(serviziHomePage);
			MIF.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SUBTEXT is displayed in page - Start");
			By serviziPageSubText= MIF.serviziPageSubText;
			MIF.verifyComponentExistence(serviziPageSubText);
			MIF.VerifyText(serviziPageSubText, prop.getProperty("SERVIZI_HOMEPAGE_SUBTEXT"));
			logger.write("Checking if SERVIZI_HOMEPAGE_SUBTEXT is displayed in page - Completed");
			
			logger.write("Validating the presence of Modifica_Indirizzo_Fatturazione button - Start");
			By modificaIndirizzoFatturazione_button = MIF.modificaIndirizzoFatturazione_button;
			MIF.verifyComponentExistence(modificaIndirizzoFatturazione_button);
			MIF.VerifyText(modificaIndirizzoFatturazione_button, prop.getProperty("MODIFICA_INDIRIZZO_FATTURAZIONE"));
			logger.write("Validating the presence of Modifica_Indirizzo_Fatturazione button - Completed");
			
			logger.write("Validate and click on 'modificaIndirizzoFatturazione_button' - Start");
			MIF.clickComponent(modificaIndirizzoFatturazione_button);
			logger.write("Validate and click on 'modificaIndirizzoFatturazione_button' - Completed");
			Thread.sleep(40000);
			
			logger.write("Verify the page title 'modificaDiIndirizzoFatturazioneTitle' - Start");
			By modificaDiIndirizzoFatturazioneTitle = MIF.modificaDiIndirizzoFatturazioneTitle;
			MIF.verifyComponentExistence(modificaDiIndirizzoFatturazioneTitle);
			MIF.VerifyText(modificaDiIndirizzoFatturazioneTitle, prop.getProperty("MODIFICA_INDIRIZZO_FATTURAZIONE_TITLE"));
			logger.write("Verify the page title 'modificaDiIndirizzoFatturazioneTitle' - Completed");
			
			logger.write("Verify the page title descrption 'modificaDiIndirizzoFatturazioneSubTextDescription'  - Start");
			By modificaIndirizzoDiFatturazioneSubTextDescription= MIF.modificaDiIndirizzoFatturazioneSubTextDescription;
			MIF.verifyComponentExistence(modificaIndirizzoDiFatturazioneSubTextDescription);
			MIF.VerifyText(modificaIndirizzoDiFatturazioneSubTextDescription, MIF.ModificaIndirizzoFatturazioneSubTextDescription);
			logger.write("Verify the page title descrption 'modificaDiIndirizzoFatturazioneSubTextDescription'  - Completed");
			
			logger.write("Validate Supply section - Start");
			MIF.validateSupplySelection();
			logger.write("Validate Supply section - Completed");
			
			logger.write("Select the checkbox - Start");
//			MIF.clickComponent(MIF.supplySelect_310491181);
//			MIF.clickComponent(MIF.supplySelect_300001597);
			MIF.clickComponent(MIF.supplySelect_300001594);
			logger.write("Select the checkbox - Completed");
			
			logger.write("Validate and click 'modificaDiIndirizzoFatturazione_button' button - Start");
			By modificaDiIndirizzoFatturazione_button = MIF.modificaDiIndirizzoFatturazione_button;
			MIF.verifyComponentExistence(modificaDiIndirizzoFatturazione_button);
			MIF.clickComponent(modificaDiIndirizzoFatturazione_button);
			logger.write("Validate and click 'modificaDiIndirizzoFatturazione_button' button - Completed");
			Thread.sleep(10000);

			logger.write(" Validate title 'modificaDellIndirizzoFatturazioneTitle' - Start");
			By modificaDellIndirizzoFatturazioneTitle = MIF.modificaDellIndirizzoFatturazioneTitle;
			MIF.verifyComponentExistence(modificaDellIndirizzoFatturazioneTitle);
			MIF.VerifyText(modificaDellIndirizzoFatturazioneTitle, prop.getProperty("MODIFICA_INDIRIZZO_FATTURAZIONE_TITLE"));
			logger.write(" Validate title 'modificaDellIndirizzoFatturazioneTitle' - Completed");
			
			logger.write("Validate presence of 'inserimentoDati' - Start");
			By inserimentoDati = MIF.inserimentoDati;
			MIF.verifyComponentExistence(inserimentoDati);
			MIF.VerifyText(inserimentoDati, prop.getProperty("STEP1"));
			logger.write("Validate presence of 'inserimentoDati' - Completed");
			
			logger.write(" Select Billing Address from dropdown - Start");
			MIF.selectValue(MIF.selectDropdown, "1");
			logger.write(" Select Billing Address from dropdown - Completed");
			
			logger.write("Validate the presence of fields - Start");
			By cittaField = MIF.cittaField;
			MIF.verifyComponentExistence(cittaField);
			
			
			By indirizzoField = MIF.indirizzoField;
			MIF.verifyComponentExistence(indirizzoField);
			
			By numeroCivicoField = MIF.numeroCivicoField;
			MIF.verifyComponentExistence(numeroCivicoField);
						
			MIF.ValidatePrepopulatedFields();
			logger.write("Validate the presence of fields - Completed");
			
			logger.write("Validate and click on Continue button - Start");
			By continuaButton = MIF.continuaButton;
			MIF.verifyComponentExistence(continuaButton);
			MIF.clickComponent(continuaButton);
			logger.write("Validate and click on Continue button - Completed");
			Thread.sleep(5000);
			
			/*logger.write("Check if popup displayed - Start");
			MIF.ValidateDilogPopup();
			logger.write("Check if popup displayed - Completed");
			
			MIF.setFieldValues(MIF.CAPboxValue,MIF.IndirizzoFieldValue,MIF.NumeroCivicoFieldValue);
			
			
			MIF.verifyComponentExistence(continuaButton);
			MIF.clickComponent(continuaButton);
			Thread.sleep(5000);*/
			
			logger.write("Validate the section 'riepilogo_e_ConfermaDati_Section1_Heading' - Start");
			By riepilogo_e_ConfermaDati_Section1 = MIF.riepilogo_e_ConfermaDati_Section1;
			MIF.verifyComponentExistence(riepilogo_e_ConfermaDati_Section1);
			MIF.VerifyText(riepilogo_e_ConfermaDati_Section1, prop.getProperty("RIEPILOGO_E_CONFERMADATI_SECTION1_HEADING"));
			logger.write("Validate the section 'riepilogo_e_ConfermaDati_Section1_Heading' - Completed");
			
			logger.write("Validate presence of 'riepilogo_e_ConfermaDati' - Start");
			By riepilogo_e_ConfermaDati = MIF.riepilogo_e_ConfermaDati;
			MIF.verifyComponentExistence(riepilogo_e_ConfermaDati);
			MIF.VerifyText(riepilogo_e_ConfermaDati, prop.getProperty("STEP2"));
			logger.write("Validate presence of 'riepilogo_e_ConfermaDati - Completed");
			
			logger.write("Validate and compare field displayed in confirmation page - Start");
			MIF.ValidatePostpopulatedFields();
			logger.write("Validate and compare field displayed in confirmation page - Completed");
			
			logger.write("Verify the supply selection in confirmation page - Start");
			MIF.confirmSupplySelection();
			logger.write("Verify the supply selection in confirmation page - Completed");
			
			logger.write("Validate and click on indietroButton - Start");
			By indietroButton = MIF.indietroButton;
			MIF.verifyComponentExistence(indietroButton);
			MIF.clickComponent(indietroButton);
			logger.write("Validate and click on indietroButton - Completed");
			Thread.sleep(5000);
			
			logger.write("Validate presence of 'inserimentoDati' - Start");
			MIF.verifyComponentExistence(inserimentoDati);
			MIF.VerifyText(inserimentoDati, prop.getProperty("STEP1"));
			logger.write("Validate presence of 'inserimentoDati' - Completed");
			
			logger.write("Click on indietroButton - Start");
			MIF.verifyComponentExistence(indietroButton);
			MIF.clickComponent(indietroButton);
			logger.write("Click on indietroButton - Completed");
			
			logger.write("Validate presence of 'modificaDiIndirizzoFatturazioneTitle' - Start");
			MIF.verifyComponentExistence(modificaDiIndirizzoFatturazioneTitle);
			MIF.VerifyText(modificaDiIndirizzoFatturazioneTitle, prop.getProperty("MODIFICA_INDIRIZZO_FATTURAZIONE_TITLE"));
			logger.write("Validate presence of 'modificaDiIndirizzoFatturazioneTitle' - Completed");
			
			logger.write("validate and click on 'esciButton' - Start");
			By esciButton = MIF.esciButton;
			MIF.verifyComponentExistence(esciButton);
			MIF.clickComponent(esciButton);
			logger.write("validate and click on 'esciButton' - Completed");
			Thread.sleep(15000);
			
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			MIF.verifyComponentExistence(homePage);
			MIF.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			MIF.verifyComponentExistence(homepageSubheading);
			MIF.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			MIF.verifyComponentExistence(sectionTitle);
			MIF.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			MIF.verifyComponentExistence(sectionTitleSubText);
			MIF.VerifyText(sectionTitleSubText, MIF.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}
