package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFreecardComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ORE_FREE_155 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreecardComponent ofcc = new OreFreecardComponent(driver);
			
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Start");
//			ofcc.verifyComponentExistence(ofcc.datiAggiornati);
			ofcc.verifyComponentExistence(ofcc.homePageHeading1);
			ofcc.comprareText(ofcc.homePageHeading1, OreFreecardComponent.HOMEPAGE_HEADING1, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle);
			ofcc.comprareText(ofcc.homePageTitle, OreFreecardComponent.HOMEPAGE_TITLE, true);
			ofcc.verifyComponentExistence(ofcc.homePageHeading2);
			ofcc.comprareText(ofcc.homePageHeading2, OreFreecardComponent.HOMEPAGE_HEADING2, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle2);
			ofcc.comprareText(ofcc.homePageTitle2, OreFreecardComponent.HOMEPAGE_TITLE2, true);
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Ends");
			logger.write("Click on Gestisci Le Ore Free button- Start");
			ofcc.verifyComponentExistence(ofcc.gestisciOreFreeButton);
			ofcc.clickComponent(ofcc.gestisciOreFreeButton);
			logger.write("Click on Gestisci Le Ore Free button- Ends");
			
			
			logger.write("Verification of the supply detail screen layout- Start");
			ofcc.verifyComponentExistence(ofcc.rinominaFornituraButton);
			ofcc.verifyComponentExistence(ofcc.visualizzaBollette);
			ofcc.verifyComponentExistence(ofcc.lightIcon);
			ofcc.comprareText(ofcc.supplyHeading, OreFreecardComponent.SUPPLY_HEADING, true);
			ofcc.comprareText(ofcc.numeroClienta, OreFreecardComponent.NUMERO_CLIENTA, true);
			ofcc.comprareText(ofcc.numeroClientaValue, OreFreecardComponent.NUMERO_CLIENTA_VALUE, true);
			ofcc.checkForSupplyDetails(prop.getProperty("SUPPLY_DETAILS"));
			logger.write("Verification of the supply detail screen layout- Ends");
			
			logger.write("Click on Mostra Di Piu button- Starts");
			ofcc.clickComponent(ofcc.mostraDiPIU);
			logger.write("Click on Mostra Di Piu button- Ends");
						
			logger.write("Verification of the supply detail screen layout - ORE FREE DI OGGI- Start");
			ofcc.verifyMoreSupplyDetails(ofcc.moreSupplyDetails, OreFreecardComponent.MORE_SUPPLY_DETAILS);
			ofcc.verifyComponentExistence(ofcc.oreFreeDiOggi);
			ofcc.comprareText(ofcc.oreFreeDiOggi, OreFreecardComponent.ORE_FREE_DI_OGGI, true);
			ofcc.verifyComponentExistence(ofcc.Sceglilatuafascia);
			ofcc.comprareText(ofcc.oreFreeDiOggiText, OreFreecardComponent.ORE_FREE_DI_OGGI_TEXT, true);
			ofcc.verifyComponentExistence(ofcc.FasceOreFree);
			ofcc.comprareText(ofcc.FasceOreFree, OreFreecardComponent.FASCE_ORE_FREE, true);
			ofcc.verifyComponentExistence(ofcc.FasceOreFreeTiming);
//			ofcc.comprareText(ofcc.FasceOreFreeTiming, OreFreecardComponent.FASCE_ORE_FREE_TIMING, true);
			logger.write("Verification of the supply detail screen layout - ORE FREE DI OGGI- Ends ");
			
			logger.write("Verification of the supply detail screen layout - Consumi giornalieri- Start");
			ofcc.verifyComponentExistence(ofcc.consumiGiornalieri);
			ofcc.comprareText(ofcc.consumiGiornalieri, OreFreecardComponent.CONSUMI_GIORNALIERI, true);
			ofcc.verifyComponentExistence(ofcc.scegliIlGiornoLink);
			ofcc.verifyComponentExistence(ofcc.consumiGiornalieriText);
			ofcc.comprareText(ofcc.consumiGiornalieriText, OreFreecardComponent.CONSUMI_GIORNALIERI_TEXT, true);
			ofcc.verifyComponentExistence(ofcc.domenicaGiugno);
			ofcc.verifyComponentExistence(ofcc.oreFreeTimings);
//			ofcc.comprareText(ofcc.oreFreeTimings, OreFreecardComponent.ORE_FREE_TIMINGS, true);
			logger.write("Verification of the supply detail screen layout - Consumi giornalieri- Ends");
			
			logger.write("Verification of the supply detail screen layout - Fascia Base- Start");
			ofcc.verifyComponentExistence(ofcc.fasciaBase);
			ofcc.comprareText(ofcc.fasciaBase, OreFreecardComponent.FASCIA_BASE, true);
			ofcc.verifyComponentExistence(ofcc.fasciaBaseText1);
			ofcc.comprareText(ofcc.fasciaBaseText1, OreFreecardComponent.FASCIA_BASE_TEXT1, true);
			ofcc.verifyComponentExistence(ofcc.fasciaBaseText2);
			ofcc.comprareText(ofcc.fasciaBaseText2, OreFreecardComponent.FASCIA_BASE_TEXT2, true);
			ofcc.verifyComponentExistence(ofcc.fasciaOreFree);
			ofcc.comprareText(ofcc.fasciaOreFree, OreFreecardComponent.FASCIA_ORE_FREE, true);
			ofcc.verifyComponentExistence(ofcc.fasciaOreFreeTimings);
			ofcc.comprareText(ofcc.fasciaOreFreeTimings, OreFreecardComponent.FASCIA_ORE_FREE_TIMINGS, true);
			ofcc.verifyComponentExistence(ofcc.modificaLink);
			logger.write("Verification of the supply detail screen layout - Fascia Base- Ends");
			
			logger.write("Verification of the supply detail screen layout - Fasce Giornaliere- Start");
			ofcc.verifyComponentExistence(ofcc.fasceGiornaliere);
			ofcc.comprareText(ofcc.fasceGiornaliere, OreFreecardComponent.FASCE_GIORNALIERE, true);
			ofcc.verifyComponentExistence(ofcc.fasceGiornaliereText1);
			ofcc.comprareText(ofcc.fasceGiornaliereText1, OreFreecardComponent.FASCE_GIORNALIERE_TEXT1, true);
			ofcc.verifyComponentExistence(ofcc.fasceGiornaliereText2);
			ofcc.comprareText(ofcc.fasceGiornaliereText2, OreFreecardComponent.FASCE_GIORNALIERE_TEXT2, true);
			logger.write("Verification of the supply detail screen layout - Fasce Giornaliere- Ends");
			
			logger.write("Verification of the supply detail screen layout - Fasce Giornaliere Weekly Display- Start");
			ofcc.verifyComponentExistence(ofcc.day);
			ofcc.verifyComponentExistence(ofcc.dayOreFree);
			ofcc.verifyModificaComponentByDate(ofcc.modificaFasciaGiornalieraButton);
			
			prop.setProperty("RETURN_VALUE", "OK");
	
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
	}

