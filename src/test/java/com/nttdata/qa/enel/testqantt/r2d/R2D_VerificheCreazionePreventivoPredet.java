package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerificheCreazionePreventivoPredet {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		Boolean praticaTrovata = false;

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			String statopratica="vuota";

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Interrogazione","Report Post Sales");
				//Inserimento POD
				R2D_VerifichePodComponent verifichePod = new R2D_VerifichePodComponent(driver);
				if(prop.getProperty("SKIP_POD","N").equals("N")){
					verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD_ELE",prop.getProperty("POD")));
				}
				//Inserimento ID richiesta CRM
				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA",prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA"))));
				//Cerca
				verifichePod.cercaPod(verifichePod.buttonCerca);
				//Click Dettaglio Pratiche
				logger.write("Bottone Dettaglio Pratica - Start");
				praticaTrovata = false;
				verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				praticaTrovata = true;
				logger.write("Bottone Dettaglio Pratica - Completed");
				//Click su Pratica
				verifichePod.selezionaPratica(verifichePod.rigaPratica);
				//Verifica Campi
				//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
//				campiDaVerificare.add("Distributore e Partita IVA;"+prop.getProperty("DISTRIBUTORE_R2D_ATTESO_ELE"));
				//				campiDaVerificare.add("Causale CRM;"+prop.getProperty("CAUSALE_CRM_R2D_ATTESO"));
				//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
				campiDaSalvare.add("Stato R2D");
				//Salvare eventualmente stato indicizzato
				verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);

				//Verifico che lo stato iniziale della pratica sia annullo:ANAW
				logger.write("Verifiche Iniziali Stato Pratica - Start");
				statopratica=prop.getProperty("STATO_R2D");
				if(statopratica.compareToIgnoreCase("OK")!=0){
						throw new Exception("Lo stato della pratica:"+statopratica+". Stato pratica atteso OK");
					}
					logger.write("Controllo stato creazione preventivo predeterminabile:OK");
				}

			prop.setProperty("RETURN_VALUE", "OK");
			
		}catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
