package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ORE_Free_KO_65 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreeDettaglioComponent ofdf = new OreFreeDettaglioComponent(driver);
			
			

			ofdf.verifyComponentExistence(ofdf.oreFreeButton);
			ofdf.clickComponent(ofdf.oreFreeButton);
			
			ofdf.verifyComponentExistence(ofdf.ripristinaFasciaBaseFasceGiornaliereButton);
			
			//ofdf.verifyComponentExistence(ofdf.fasceGiornaliereHeader);
			//ofdf.comprareText(ofdf.fasceGiornaliereHeader, ofdf.fasceGiornaliereHeaderText, true);
			
			ofdf.clickModificaComponentByDate(ofdf.modificaFasciaGiornalieraButton, Integer.parseInt(prop.getProperty("DAY_TO_BE_ADDED")));
						
			//ofdf.verifyComponentExistence(ofdf.dalleOreSelect);
			//ofdf.verifyComponentExistence(ofdf.alleOreSelect);
			
			Robot robot = new Robot();
			int counter = 0;
			
			while(!ofdf.checkNodeValue(ofdf.dalleOreDailySelectValue, prop.getProperty("ORE_FREE_ORARIO_DI_START")) && counter < 15){
				ofdf.clickComponent(ofdf.dalleOreDailySelect);
				robot.keyPress(KeyEvent.VK_UP);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_UP);
				robot.delay(50);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(500);
				counter++;
			}	
			
			ofdf.comprareText(ofdf.alleOreSelect, "13:00", true);
			
			ofdf.clickComponent(ofdf.confermaButton);
			ofdf.clickComponent(ofdf.chiudiButton);
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
	
}
