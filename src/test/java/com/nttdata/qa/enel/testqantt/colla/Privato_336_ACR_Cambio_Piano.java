package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato336ACRCambioPianoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_336_ACR_Cambio_Piano {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato336ACRCambioPianoComponent cp = new Privato336ACRCambioPianoComponent(driver);
			
			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
                cp.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
			
			cp.verifyComponentVisibility(cp.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = cp.homePageCentralText;
			cp.verifyComponentExistence(homePage);
			cp.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
			
			By homepageSubheading = cp.homePageCentralSubText;
			cp.verifyComponentExistence(homepageSubheading);
			cp.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = cp.sectionTitle;
			cp.verifyComponentExistence(sectionTitle);
			cp.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = cp.sectionTitleSubText;
			cp.verifyComponentExistence(sectionTitleSubText);
			cp.VerifyText(sectionTitleSubText, cp.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verify and Click on left menu item 'SERVIZI' - Start");
			By servizi = cp.serviziSelect;
			cp.verifyComponentExistence(servizi);
			cp.clickComponent(servizi);
			logger.write("Verify and Click on left menu item 'SERVIZI' - Completed");
			
			cp.verifyComponentVisibility(cp.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = cp.serviziPage;
			cp.verifyComponentExistence(serviziHomePage);
			cp.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Start");
			By serviziPageSectionTitle= cp.serviziSectionTitle;
			cp.verifyComponentExistence(serviziPageSectionTitle);
			cp.VerifyText(serviziPageSectionTitle, prop.getProperty("SERVIZI_HOMEPAGE_SECTIONTITLE"));
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Completed");
			
			logger.write("Validate and Click on cambioPianoButton - Start");
			By cambioPianoButton = cp.cambioPianoButton;
			cp.verifyComponentExistence(cambioPianoButton);
			cp.clickComponent(cambioPianoButton);
			logger.write("Validate and Click on cambioPianoButton - Completed");
			Thread.sleep(20000);
			
			logger.write("Verify CAMBIO_PIANO_PAGE_TITLE and  CAMBIO_PIANO_PAGE_DESCRIPTION - Start");
			By cambioPianoPageTitle= cp.cambioPianoPageTitle;
			cp.verifyComponentExistence(cambioPianoPageTitle);
			cp.VerifyText(cambioPianoPageTitle, prop.getProperty("CAMBIO_PIANO_PAGE_TITLE"));
			
			By cambioPianoPageDescription1= cp.cambioPianoPageDescription1;
			cp.verifyComponentExistence(cambioPianoPageDescription1);
			cp.VerifyText(cambioPianoPageDescription1, cp.CambioPianoPageDescription1);
			
			By cambioPianoPageDescription2= cp.cambioPianoPageDescription2;
			cp.verifyComponentExistence(cambioPianoPageDescription2);
			cp.VerifyText(cambioPianoPageDescription2, cp.CambioPianoPageDescription2);
			logger.write("Verify CAMBIO_PIANO_PAGE_TITLE and  CAMBIO_PIANO_PAGE_DESCRIPTION - Completed");
			
			logger.write("Validating the ELE SUPPLY displayed in the page  - Start");
//			cp.luceSupplyDetailsDisplay(cp.LightDetails);
			logger.write("Validating the ELE SUPPLY displayed in the page  - Completed");
			
			logger.write("Validate and Click on ESCI_Button  - Start");
			By ESCI_Button = cp.ESCI_Button;
			cp.verifyComponentExistence(ESCI_Button);
			cp.clickComponent(ESCI_Button);
			logger.write("Validate and Click on ESCI_Button  - Completed");
			Thread.sleep(7000);
			
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			cp.verifyComponentExistence(homePage);
			cp.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
			
			cp.verifyComponentExistence(homepageSubheading);
			cp.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			cp.verifyComponentExistence(sectionTitle);
			cp.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}