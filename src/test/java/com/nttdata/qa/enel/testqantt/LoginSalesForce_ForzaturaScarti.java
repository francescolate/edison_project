package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class LoginSalesForce_ForzaturaScarti 
{
	//private Properties prop = null;

	@Step("Login Salesforce")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {

			if(prop.getProperty("KO_RIPROCESSABILE").compareTo("OK")==0){


				WebDriver driver = WebDriverManager.getNewWebDriver(prop);

				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

					if(!prop.containsKey("START_TIME")){
						Date d = new Date();
						SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						String s = fmt.format(d);
						prop.setProperty("START_TIME", s);
					}
					LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
					page.navigate(prop.getProperty("LINK"));
					logger.write("Apertura portale Salesforce");
					TimeUnit.SECONDS.sleep(5);
					page.enterUsername(Costanti.utenza_forzatura_salesforce);
					logger.write("Inserimento Username");
					page.enterPassword(Costanti.password_forzatura_salesforce);
					logger.write("Inserimento Password");
					page.submitLogin();
					logger.write("Accesso al portale");
					////System.out.println("Login ok");
					TimeUnit.SECONDS.sleep(5);
					page.killPreviousSessions();


					//	if(prop.getProperty("USERNAME").contentEquals(Costanti.utenza_admin_salesforce)){
					new RicercaOffertaComponent(driver).cliccaSwitch();	
					//	}
				}
			}
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}




}
