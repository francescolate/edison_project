package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CheckSezioneClienteUscenteAbilitata {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				logger.write("check esistenza sezione 'Selezione Cliente Uscente' - Start");
				SezioneSelezioneClienteUscenteComponent clienteUscente=new SezioneSelezioneClienteUscenteComponent(driver);
                By sezione_ClienteUscente=clienteUscente.sezioneClienteUscente;
                clienteUscente.verifyComponentExistence(sezione_ClienteUscente);
                
                By checkBox_CF=clienteUscente.checkBoxCF;
			    clienteUscente.verifyComponentExistence(checkBox_CF);
			    
			    By checkBox_PIVA=clienteUscente.checkBoxPIVA;
			    clienteUscente.verifyComponentExistence(checkBox_PIVA);
			    
				By checkBox_PodPdr=clienteUscente.checkBoxPodPdr;
				clienteUscente.verifyComponentExistence(checkBox_PodPdr);
				
				By checkBox_NumeroCliente=clienteUscente.checkBoxNumeroCliente;
				clienteUscente.verifyComponentExistence(checkBox_NumeroCliente);
				
				By inputBox_CF=clienteUscente.inputBoxCF;
				clienteUscente.verifyComponentExistence(inputBox_CF);
				
				By inputBox_PIVA=clienteUscente.inputBoxPIVA;
				clienteUscente.verifyComponentExistence(inputBox_PIVA);
				
				By inputBox_PodPdr=clienteUscente.inputBoxPodPdr;
				clienteUscente.verifyComponentExistence(inputBox_PodPdr);
				
				By inputBox_NumeroCliente=clienteUscente.inputBoxNumeroCliente;
				clienteUscente.verifyComponentExistence(inputBox_NumeroCliente);
				
				By button_Ricerca=clienteUscente.buttonRicerca;
				clienteUscente.verifyComponentExistence(button_Ricerca);
				logger.write("check esistenza sezione 'Selezione Cliente Uscente' - Completed");
				
							
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
