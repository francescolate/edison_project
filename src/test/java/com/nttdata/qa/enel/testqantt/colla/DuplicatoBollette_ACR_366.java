package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.DuplicatoBolletteComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DuplicatoBollette_ACR_366 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DuplicatoBolletteComponent mod = new DuplicatoBolletteComponent(driver);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
		
		String invioDocumenti ="https://www-coll1.enel.it/it/area-clienti/residenziale/invio_documenti";
		String iTuoiDiritti ="https://www-coll1.enel.it/it/area-clienti/residenziale/i-tuoi-diritti#!";
		String uploadDocumenti = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AC&email=rita.mastellone@accenture.com";
		String guidaurl = "https://www-coll1.enel.it/content/dam/enel-it/servizi-online/pdf/Guida_alla_CompilazioneIstanze19_01_2016_v3.pdf";
		String novitaUrl = "https://www-coll1.enel.it/it/area-clienti/residenziale/novita";
				
		mod.comprareText(mod.homePageHeading1,  DuplicatoBolletteComponent.HOME_PAGE_HEADING1, true);
		mod.comprareText(mod.homePageParagraph1,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH1, true);
		mod.comprareText(mod.homePageHeading2,  DuplicatoBolletteComponent.HOME_PAGE_HEADING2, true);
		mod.comprareText(mod.homePageParagraph2,DuplicatoBolletteComponent.HOME_PAGE_PARAGRAPH2, true);
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
		mod.comprareText(mod.sectionHeading1, DuplicatoBolletteComponent.SECTION_PAGE_HEADING1, true);
		mod.comprareText(mod.sectionParagraph1, DuplicatoBolletteComponent.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		
		mod.comprareText(mod.serviziPerLeContratto, mod.SECTION_PAGE_TITLE3, true);
		mod.verifyComponentExistence(mod.invioDocumenti);
		
		mod.clickComponent(mod.invioDocumenti);
		mod.checkURLAfterRedirection(invioDocumenti);
		mod.comprareText(mod.invioDocumentititle, mod.INVIO_DOCUMENTI_TITLE, true);
		mod.comprareText(mod.invioDocumentiText1, mod.INVIO_DOCUMENTI_TEXT1, true);
		mod.comprareText(mod.invioDocumentiText2, mod.INVIO_DOCUMENTI_TEXT2, true);
		mod.comprareText(mod.invioDocumentiText3, mod.INVIO_DOCUMENTI_TEXT3, true);
		
		mod.verifyComponentExistence(mod.email);
		mod.verifyComponentExistence(mod.proseguiButton);
		
		mod.comprareText(mod.emailText, mod.EMAIL_TEXT, true);
		
		logger.write("Verify and click on Account link- Start");
		pac.verifyComponentExistence(pac.accountLink);
		pac.clickComponent(pac.accountLink);
		logger.write("Verify and click on TuoiDiritti link- Complete");
		
		logger.write("Verify the daticontatti heading and contents - Start");
		pac.verifyComponentExistence(pac.datiContattiHeading);
		pac.compareText(pac.datiContattiHeading, PrivateAreaPageComponent.DATICONTATTIHEADING, true);
		pac.verifyComponentExistence(pac.datiContattiContent);
		pac.compareText(pac.datiContattiContent, PrivateAreaPageComponent.DATICONTATTICONTENT, true);
		logger.write("Verify the daticontatti heading and contents - Complete");
		
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.servizii);
		Thread.sleep(5000);
		mod.comprareText(mod.sectionHeading1, DuplicatoBolletteComponent.SECTION_PAGE_HEADING1, true);
		mod.comprareText(mod.sectionParagraph1, DuplicatoBolletteComponent.SECTION_PAGE_PARAGPAH1, true);
		logger.write("click on servizi menu item and verify the section heading and paragraph- Completed");
		mod.comprareText(mod.serviziPerLeContratto, mod.SECTION_PAGE_TITLE3, true);
		mod.verifyComponentExistence(mod.invioDocumenti);
		mod.clickComponent(mod.invioDocumenti);
		mod.checkURLAfterRedirection(invioDocumenti);
		mod.comprareText(mod.invioDocumentititle, mod.INVIO_DOCUMENTI_TITLE, true);
		mod.comprareText(mod.invioDocumentiText1, mod.INVIO_DOCUMENTI_TEXT1, true);
		mod.comprareText(mod.invioDocumentiText2, mod.INVIO_DOCUMENTI_TEXT2, true);
		mod.comprareText(mod.invioDocumentiText3, mod.INVIO_DOCUMENTI_TEXT3, true);
		
		mod.comprareText(mod.enelEnergiaSPA, mod.ENEL_ENERGIA_SPA, true);
		mod.comprareText(mod.tuttiDiDirittiRiservati, mod.TUTTI_I_DIRITTI, true);
		mod.comprareText(mod.gruppoIVA, mod.GRUPO_IVA, true);
		mod.comprareText(mod.informazioniLegali, mod.INFORMAZIONI_LEGALI, true);
		mod.comprareText(mod.privacy, mod.PRIVACY, true);
		mod.comprareText(mod.credits, mod.CREDITS, true);
		mod.comprareText(mod.contattaci, mod.CONTATTACI, true);
		
		mod.clickComponent(mod.novita);
		mod.checkURLAfterRedirection(novitaUrl);
		mod.comprareText(mod.novitaTitle, mod.NOVITA_TITLE, true);
		mod.comprareText(mod.novitaText, mod.NOVITA_TEXT, true);
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
