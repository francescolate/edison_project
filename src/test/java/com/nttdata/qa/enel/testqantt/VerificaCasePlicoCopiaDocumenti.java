package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaCasePlicoCopiaDocumenti {
	private final static int SEC = 120;
	
	@Step("VerificaCasePlicoCopiaDocumenti")
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DettagliSpedizioneCopiaDocComponent forn=new DettagliSpedizioneCopiaDocComponent(driver);
			
//			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			
			if(prop.getProperty("ITA_IFM_MODELLO__C").compareToIgnoreCase("Plico")!=0){
				throw new Exception("Il campo Modello contiene un valore diverso da quello atteso. Valore attuale:"+prop.getProperty("ITA_IFM_MODELLO__C")+" valore atteso:Plico Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}

			String sysdataDoc=forn.getSysdateDoc();
			if(!prop.getProperty("CREATEDDATE").contains(sysdataDoc)){
				throw new Exception("Il campo Data creazione contiene un valore diverso da quello atteso. Valore attuale:"+prop.getProperty("CREATEDDATE")+" valore atteso:"+sysdataDoc+"  Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}

			if(prop.getProperty("ITA_IFM_DELIVERY_CHANNEL__C").compareToIgnoreCase(prop.getProperty("CANALE_INVIO"))!=0){
				throw new Exception("Il campo Canale di invio contiene un valore diverso da quello atteso. Valore attuale:"+prop.getProperty("ITA_IFM_DELIVERY_CHANNEL__C")+" valore atteso:STAMPA LOCALE Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}
			if(prop.getProperty("RECORDTYPE.NAME").compareToIgnoreCase("Corrispondenza")!=0){
				throw new Exception("Il campo Tipo di record contiene un valore diverso da quello atteso. Valore attuale:"+prop.getProperty("RECORDTYPE.NAME")+" valore atteso:Corrispondenza Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}
			
			if(prop.getProperty("ITA_IFM_DIREZIONE__C").compareToIgnoreCase("Outbound")!=0){
				throw new Exception("Il campo Direzione contiene un valore diverso da quello atteso. Valore attuale:"+prop.getProperty("ITA_IFM_DIREZIONE__C")+" valore atteso:Outbound Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}
			
			if(prop.getProperty("ITA_IFM_STATUS__C").compareToIgnoreCase("In Lavorazione")!=0){
				throw new Exception("Il campo Stato contiene un valore diverso da quello atteso. Valore attuale:"+prop.getProperty("ITA_IFM_STATUS__C")+" valore atteso:In Lavorazione Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
