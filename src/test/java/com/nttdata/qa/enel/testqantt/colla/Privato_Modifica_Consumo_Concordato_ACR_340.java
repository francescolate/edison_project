package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ModificaConsumoConcordatoComponent;
import com.nttdata.qa.enel.components.colla.SupplyDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Modifica_Consumo_Concordato_ACR_340 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);

			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
                dpc.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
			
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");
			
			HomeComponent hc = new HomeComponent(driver);
			logger.write("Correct visualization of the Home Page supply card sections -- Starts");	
			hc.verifyComponentExistence(hc.nomeFornituraGas);
			hc.verifyComponentExistence(hc.addressGasSupply);
			hc.verifyComponentExistence(hc.activateSupplyText1);
			hc.verifyComponentExistence(hc.activeSupplyText2);
			hc.verifyComponentExistence(hc.seeOffers);
			logger.write("Correct visualization of the Home Page supply card sections -- Ends");
			
			logger.write("Click and Correct visualization of the Dettaglio Fornitura Page  -- Starts");		
			dpc.clickComponent(dpc.dettaglioFornituraLink);  
			SupplyDetailComponent supply = new SupplyDetailComponent(driver);
			supply.verifyComponentExistence(supply.RinominaFornitura);
			supply.verifyComponentExistence(supply.VisualizzaBollette);
			supply.verifyComponentExistence(supply.Addressfields);
			supply.verifyComponentExistence(supply.SupplyDetailFieldsNew);
			supply.verifyComponentExistence(supply.Mostradipiu);
			logger.write("Click and Correct visualization of the Dettaglio Fornitura Page  -- Ends");

			logger.write("Click and Correct visualization of Modifica Consumo Concordato Page  -- Starts");		
			ModificaConsumoConcordatoComponent mcc = new ModificaConsumoConcordatoComponent(driver);
			mcc.clickComponent(mcc.modificaConsumoConcordatoLink); 
			Thread.sleep(10000);
			mcc.comprareText(mcc.modificaConsumoConcordatoPagesubText1, ModificaConsumoConcordatoComponent.ModificaConsumoConcordatoPagesubText1, true);
			Thread.sleep(4000);
			mcc.comprareText(mcc.modificaConsumoConcordatoPagesubText2, ModificaConsumoConcordatoComponent.ModificaConsumoConcordatoPagesubText2, true);
			Thread.sleep(4000);
			mcc.verifyComponentExistence(mcc.checkBox); 
			mcc.verifyComponentExistence(mcc.icon); 
			mcc.verifyComponentExistence(mcc.indirizzo); 
			mcc.verifyComponentExistence(mcc.consumo); 
			mcc.verifyComponentExistence(mcc.letture); 		
			mcc.clickComponent(mcc.checkBox); 
			Thread.sleep(4000);
			logger.write("Click and Correct visualization of Modifica Consumo Concordato Page  -- Ends");

			logger.write("Click on CAMBIA CONSUMO CONCORDATO and verify the text  -- Starts");		
			mcc.clickComponent(mcc.cambiaConsumoButton);
			Thread.sleep(40000);
			mcc.comprareText(mcc.step1Header, ModificaConsumoConcordatoComponent.Step1HeaderText, true);
			mcc.comprareText(mcc.step2Header, ModificaConsumoConcordatoComponent.Step2HeaderText, true);
			mcc.comprareText(mcc.step3Header, ModificaConsumoConcordatoComponent.Step3HeaderText, true);
			mcc.comprareText(mcc.inserisciText, ModificaConsumoConcordatoComponent.InserisciText, true);
			mcc.verifyComponentExistence(mcc.consumoBimestraleConcordatoInput);
			logger.write("Click on CAMBIA CONSUMO CONCORDATO and verify the text -- Ends");		
			
			logger.write("Input the value and continue  -- Starts");		
			mcc.enterInputParameters(mcc.consumoBimestraleConcordatoInput, prop.getProperty("VALUE"));
			mcc.clickComponent(mcc.continuaButton);
			Thread.sleep(20000);
			mcc.comprareText(mcc.step1Header, ModificaConsumoConcordatoComponent.Step1HeaderText, true);
			mcc.comprareText(mcc.step2Header, ModificaConsumoConcordatoComponent.Step2HeaderText, true);
			mcc.comprareText(mcc.step3Header, ModificaConsumoConcordatoComponent.Step3HeaderText, true);
			mcc.comprareText(mcc.summaryText1, ModificaConsumoConcordatoComponent.SummaryText1, true);
			mcc.comprareText(mcc.summaryText2, ModificaConsumoConcordatoComponent.SummaryText2, true);
			logger.write("Input the value and continue -- Ends");	
			
			logger.write("Click on Confrma button and verify the text  -- Starts");		
			mcc.clickComponent(mcc.confermaButton);
			Thread.sleep(20000);
			mcc.comprareText(mcc.step1Header, ModificaConsumoConcordatoComponent.Step1HeaderText, true);
			mcc.comprareText(mcc.step2Header, ModificaConsumoConcordatoComponent.Step2HeaderText, true);
			mcc.comprareText(mcc.step3Header, ModificaConsumoConcordatoComponent.Step3HeaderText, true);
			mcc.comprareText(mcc.successText1, ModificaConsumoConcordatoComponent.SuccessText1, true);
			mcc.comprareText(mcc.successText2, ModificaConsumoConcordatoComponent.SuccessText2, true);
			logger.write("Click on Confrma button and verify the text -- Ends");	
			
			logger.write("Click on Fine button and verify Home page text   -- Starts");		
			mcc.clickComponent(mcc.fineButton);
			Thread.sleep(20000);
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Click on Fine button and verify Home page text   -- Ends");	
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
