package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaContrattoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class RiepilogoConfermOffertaWebVSA {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			VerificaContrattoComponent offer=new VerificaContrattoComponent(driver);

			/*Apertura della pagina con il riepilo dell'offerta con le seguenti informazioni:
				Numero offerta con i seguenti campi:
				-TIPO OFFERTA
				-POD/PDR
				-PREZZO
				-DATA RICHIESTA
				-NUMERO CONTRATTO
				-INDIRIZZO FORNITURA

				Segue riepilo dei dati con le seguenti Sezioni:
				-Sezione Dati fornitura
				-Sezione Dati di pagamento
				-Sezione Indirizzo di fatturazione
				-Sezione Indirizzo di residenza
				-Sezione Tempi di ripensamento
				-Sezione Consensi per attività di marketing

				Tasti <ESCI> e <CONFERMA>"
			 */
			TimeUnit.SECONDS.sleep(60);
			offer.clickComponent(offer.accettaCookie);
			logger.write("Verifica Campi Offerta - Start");
			TimeUnit.SECONDS.sleep(5);
			offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
			offer.verifyComponentExistence(offer.riepilogoOffertaLabel);
			offer.verificaCampiPresenti();

			logger.write("Verifica Campi Offerta - Completed");

			logger.write("Verifica Campi Offerta - Start");
			offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
			offer.verificaSezioniPresenti();

			logger.write("Verifica Campi Offerta - Completed");

			util.getFrameActive();
			if(util.verifyExistence(offer.TP2AcceptButton, 10)){
				util.objectManager(offer.TP2AcceptButton,util.scrollAndClick);


			}
			util.getFrameActive();

			logger.write("click sul pulsante Conferma - Start");
			offer.clickComponent(offer.buttonConfermaWeb);
			TimeUnit.SECONDS.sleep(30);
			logger.write("click sul pulsante Conferma - Completed");


			logger.write("click su PROSEGUI sulla popup 'Ci siamo quasi' - Start");
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(15);

			offer.clickComponent(offer.pulsanteProsegui);
			logger.write("click su PROSEGUI sulla popup 'Ci siamo quasi' - Completed");


			logger.write("corretta apertura della pagina Compilazione Documenti, check di controllo sul testo presente nella pagina, selezione della voce 'nessun provvedimento o comunicazione perché non richiesti dalla normativa vigente in materia' e della voce 'Locazione/Comodato (Atto già registrato o in corso di registrazione)' - Start");

			offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
			offer.verifyComponentExistence(offer.pageCompilazioneDocumenti);
			//				  offer.checkTesto(offer.dettaglioCompilazioneDocumenti, offer.testo2);
			offer.verifyComponentExistence(offer.sezioneDichiarazionePossessoImmobile);
			offer.verifyComponentExistence(offer.selezioneComodato);
			offer.clickComponentWithJse(offer.selezioneComodato);
			TimeUnit.SECONDS.sleep(3);
			logger.write("corretta apertura della pagina Compilazione Documenti, check di controllo sul testo presente nella pagina, selezione della voce 'nessun provvedimento o comunicazione perché non richiesti dalla normativa vigente in materia' e della voce 'Locazione/Comodato (Atto già registrato o in corso di registrazione)' - Completed");

			logger.write("click su Conferma - Start");
			offer.clickComponentWithJse(offer.conferma);
			TimeUnit.SECONDS.sleep(10);
			logger.write("click su Conferma - Completed");	


			logger.write("corretto accesso alla pagina di carica documenti - Start");

			offer.verifyComponentExistence(offer.riepilogoOffertaLabel);
			//				offer.checkTesto(offer.dettaglioCompilazioneDocumenti, offer.testo3);
			logger.write("corretto accesso alla pagina di carica documenti - Completed");
			DocumentiUploadComponent doc=new DocumentiUploadComponent(driver);


			offer.clickWithJS(offer.buttonCaricaDoc);
			TimeUnit.SECONDS.sleep(3);
			offer.verifyComponentExistence(doc.fileUpload2);


			logger.write("Upload Documento di riconoscimento - Start");
			TimeUnit.SECONDS.sleep(15);
			doc.uploadGenericFile(
					Utility.getDocumentPdf(prop),doc.fileUpload2
					);

			logger.write("Upload Documento di riconoscimento - Completed");



			logger.write("Click su pulsante Invia documento - Start");
			TimeUnit.SECONDS.sleep(5);
			offer.clickWithJS(offer.buttonInvioDocumento);
			Utility.takeSnapShot(driver, "Caricamento_Doc_TP2.jpg");
			Thread.sleep(20*1000);
			logger.write("Click su pulsante Invia documento - Completed");

			TimeUnit.SECONDS.sleep(15);


			driver.switchTo().defaultContent();
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}


	}
}
