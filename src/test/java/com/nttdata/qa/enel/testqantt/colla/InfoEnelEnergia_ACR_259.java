package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;


import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class InfoEnelEnergia_ACR_259 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
			prop.setProperty("CELLULARE", "3459089078");
			prop.setProperty("CONFERMA_CELLULARE", "3459089078");
			
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepageHeadingRES, InfoEnelEnergiaACRComponent.HOMEPAGE_HEADING_RES, true);
			iee.comprareText(iee.homepageParagraphRES, InfoEnelEnergiaACRComponent.HOMEPAGE_PARAGRAPH_RES, true);
			iee.comprareText(iee.homepageSectionHeadingRES, InfoEnelEnergiaACRComponent.HOMEPAGE_SECTION_HEADING_RES, true);
			iee.comprareText(iee.homepageSectionParagraphRES, InfoEnelEnergiaACRComponent.HOMEPAGE_SECTION_PARAGRAPH_RES, true);
			logger.write("Correct visualization of the Home Page with text- Ends");
			
			logger.write("Click on dettaglio Fornitura and verify the data- Starts");
			iee.clickComponent(iee.dettaglioFornitura259);
			iee.comprareText(iee.serviziPerLeFornitura, InfoEnelEnergiaACRComponent.SERVIZI_PER_LE_FORNITURE, true);
			iee.checkForSupplyServices(InfoEnelEnergiaACRComponent.SUPPLY_SERVICES);
			logger.write("Click on dettaglio Fornitura and verify the data- Ends");
			
			logger.write(" Click on Info Enel Energia and verify Correct display of the page- Starts");
			iee.clickComponent(iee.infoEnelEnergiaTile);
			iee.comprareText(iee.infoEnelEnergiaHeadingRES, InfoEnelEnergiaACRComponent.INFOENELENERGIA_HEADING_RES, true);
			iee.comprareText(iee.infoEnelEnergiaParagraphRES, InfoEnelEnergiaACRComponent.INFOENELENERGIA_PARAGRAPH_RES, true);
			iee.comprareText(iee.ieenonAttivoParagraph, InfoEnelEnergiaACRComponent.INFOENELENERGIA_PARAGRAPH_NONATTIVO, true);
			iee.verifyComponentExistence(iee.attivaInfoEnelEnergiaButton);
			iee.verifyComponentExistence(iee.esciButton);
			logger.write(" Click on Info Enel Energia and verify Correct display of the page- Ends");
			
			logger.write(" Click on Attiva InfoEnelEnergia and verify Correct display of the page- Starts");
			iee.clickComponent(iee.attivaInfoEnelEnergiaButton);
			iee.comprareText(iee.attivaInfoEnelEnergiaHeading, InfoEnelEnergiaACRComponent.ATTIVA_IEE_HEADING1, true);
			iee.comprareText(iee.attivaInfoEnelEnergiaHeading2, InfoEnelEnergiaACRComponent.ATTIVA_IEE_HEADING2, true);
			iee.verifyComponentExistence(iee.esciButtonAttivaIEE);
			logger.write(" Click on Attiva InfoEnelEnergia and verify Correct display of the page- Ends");
			
			logger.write("Select a supply and verify the continua button enables- Starts");
			iee.clickComponent(iee.nonAttivoSupplyCheckBox);
			iee.isButtonEnabled(iee.continuaButton);
			logger.write("Select a supply and verify the continua button enables- Ends");
			
			logger.write("Click on Continua button and verify the data of section Inserimento Dati - Starts");
			iee.clickComponent(iee.continuaButton);
			iee.comprareText(iee.attivaInfoEnelEnergiaHeading, InfoEnelEnergiaACRComponent.ATTIVA_IEE_HEADING1, true);
			iee.comprareText(iee.attivaInfoEnelEnergiaParagraph, InfoEnelEnergiaACRComponent.ATTIVA_IEE_PARAGRAPH, true);
			iee.comprareText(iee.inserminatoDati, InfoEnelEnergiaACRComponent.INSERIMENTO_DATI, true);
			iee.comprareText(iee.selezionaText, InfoEnelEnergiaACRComponent.SELEZIONA_TIPOLOGIA, true);
			
			iee.comprareText(iee.comunicaLettura, InfoEnelEnergiaACRComponent.COMUNICA_LETTURA, true);
			iee.verifyComponentExistence(iee.smsCL);
			iee.verifyComponentExistence(iee.emailCL);
			
			iee.comprareText(iee.emissioneBolletta, InfoEnelEnergiaACRComponent.EMISSIONE_BOLLETTA, true);
			iee.verifyComponentExistence(iee.smsEB);
			iee.verifyComponentExistence(iee.emailEB);
			
			iee.comprareText(iee.AvvenutoPagamento, InfoEnelEnergiaACRComponent.AVVENUTO_PAGAMENTO, true);
			iee.verifyComponentExistence(iee.smsAP);
			iee.verifyComponentExistence(iee.emailAP);
			
			iee.comprareText(iee.scadenzaProssimaBolletta, InfoEnelEnergiaACRComponent.SCADENZA_PROSSIMA_BOLLETTA, true);
			iee.verifyComponentExistence(iee.smsSPB);
			iee.verifyComponentExistence(iee.emailSPB);
			
			iee.comprareText(iee.sollecitoPagamento, InfoEnelEnergiaACRComponent.SOLLECITO_PAGAMENTO, true);
			iee.verifyComponentExistence(iee.smsSP);
			iee.verifyComponentExistence(iee.emailSP);
			
			iee.comprareText(iee.avvisoConsumi, InfoEnelEnergiaACRComponent.AVVISO_CONSUMI, true);
			iee.verifyComponentExistence(iee.smsAC);
			iee.verifyComponentExistence(iee.emailAC);
			
			iee.comprareText(iee.InserisciiTuoiDati, InfoEnelEnergiaACRComponent.INSERISCI_TUAI_DATI, true);
			iee.clickComponent(iee.smsCL);
			iee.clickComponent(iee.emailEB);
			iee.clickComponent(iee.smsAP);
			iee.clickComponent(iee.emailSPB);
			iee.clickComponent(iee.smsSP);
			iee.clickComponent(iee.emailAC);

			iee.verifyComponentExistence(iee.emailInserisciiTuoiDatiLabel);
			iee.verifyComponentExistence(iee.EmailInserisciiTuoiDatiInput);
			iee.verifyComponentExistence(iee.cellulareInserisciiTuoiDatiLabel);
			iee.enterInputParameters(iee.cellulareInserisciiTuoiDatiInput, prop.getProperty("CELLULARE"));
			iee.enterInputParameters(iee.ConfermaCellulareInserisciiTuoiDatiInput, prop.getProperty("CONFERMA_CELLULARE"));
			iee.clickComponent(iee.aggiornaDatiAnagrafici);
			logger.write("Click on Continua button and verify the data of section Inserimento Dati - Ends");
			
			logger.write("Click on Continua button and verify the data of section Riepilogo e conferma dati- Starts");
			iee.clickComponent(iee.continuaIEEButton);
			iee.comprareText(iee.reipilogoeConfermaDati, InfoEnelEnergiaACRComponent.REIPILOGO_CONFERMA_DATI, true);
			
			iee.comprareText(iee.comunicaLetturaRCD, InfoEnelEnergiaACRComponent.COMUNICA_LETTURA, true);
			iee.verifyComponentExistence(iee.SoloSMS);
			
			iee.comprareText(iee.emissioneBollettaRCD, InfoEnelEnergiaACRComponent.EMISSIONE_BOLLETTA, true);
			iee.verifyComponentExistence(iee.SoloEMail);
			
			iee.comprareText(iee.avvenutoPagamentoRCD, InfoEnelEnergiaACRComponent.AVVENUTO_PAGAMENTO, true);
			
			iee.comprareText(iee.prossimaScadenzoRCD, InfoEnelEnergiaACRComponent.PROSSIMA_SCADENZA, true);
			
			iee.comprareText(iee.sollecitoPagamentoRCD, InfoEnelEnergiaACRComponent.SOLLECITO_PAGAMENTO, true);
			
			iee.comprareText(iee.avvisoConsumiRCD, InfoEnelEnergiaACRComponent.AVVISO_CONSUMI, true);
			
			/*iee.comprareText(iee.iTuoiDati, InfoEnelEnergiaACRComponent.I_TUOI_DATI, true);
			iee.comprareText(iee.emailRCD, InfoEnelEnergiaACRComponent.EMAIL_RCD, true);
			iee.comprareText(iee.emailValueRCD, InfoEnelEnergiaACRComponent.EMAIL_VALUE_RCD, true);
			
			iee.comprareText(iee.cellulareRCD, InfoEnelEnergiaACRComponent.CELLULARE_RCD, true);
			iee.comprareText(iee.cellulareValueRCD, InfoEnelEnergiaACRComponent.CELLULARE_INPUT_RCD, true);*/
			
			iee.comprareText(iee.IEEAttivoSu, InfoEnelEnergiaACRComponent.IEE_ATTIVO_SU, true);
			iee.comprareText(iee.indirizzoDellaFornitura, InfoEnelEnergiaACRComponent.INDIRIZZO_DELLA_FORNITURA, true);
			iee.comprareText(iee.numeroCliente, InfoEnelEnergiaACRComponent.NUMERO_CLIENTE, true);
			logger.write("Click on Continua button and verify the data of section Riepilogo e conferma dati- Ends");
			
			logger.write("Click on Conferma button and verify the data of section Esito- Starts");
			iee.clickComponent(iee.confermaButton);
			iee.comprareText(iee.esito, iee.ESITO, true);
			iee.comprareText(iee.esitoText, iee.ESITO_TEXT, true);
//			iee.clickComponent(iee.esciLeftMenu);
			iee.clickComponent(iee.esciLeftMenuNew);
			

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
