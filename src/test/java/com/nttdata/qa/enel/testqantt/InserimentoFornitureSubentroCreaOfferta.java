package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class InserimentoFornitureSubentroCreaOfferta {

    public static void main(String[] args) throws Exception {
        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            By button_conferma = gestione.button_conferma;
            By button_crea_offerta = gestione.button_crea_offerta;
            By button_popup_ko = gestione.button_popup_ko;

            gestione.checkIfElementIsDisabled(button_conferma, false);
            gestione.clickComponentIfExist(button_conferma);
            gestione.verifyComponentExistence(button_crea_offerta);
            gestione.clickComponentIfExist(button_crea_offerta);
            gestione.verifyComponentInexistence(button_popup_ko);

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());

            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {

            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
