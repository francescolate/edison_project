package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class InserimentoFornitureSubentroCla extends BaseComponent {

    public InserimentoFornitureSubentroCla(WebDriver driver) {
        super(driver);
    }

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);
        String nonVerificatoText;

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            PrecheckComponent forniture = new PrecheckComponent(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
            RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
            String statoUbiest = Costanti.statusUbiest;


            if (prop.getProperty("POD").equalsIgnoreCase("GAS"))
                prop.setProperty("POD", forniture.generateRandomPodNumberGas());
            else if (prop.getProperty("POD").equalsIgnoreCase("ENERGIA"))
                prop.setProperty("POD", forniture.generateRandomPodNumberEnergia());

            logger.write("Popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Start");
            forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD"));
            forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
            forniture.clickComponentIfExist(forniture.precheckButton2);
//            gestione.checkSpinnersSFDC(); // c'è il Verify dopo che aspetta l'oggetto
            logger.write("popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Completed");


            if (prop.getProperty("SEZIONE_ISTAT").equals("Y")) {
                logger.write("selezionare il codice istat e premere conferma - Start");
                forniture.verifyComponentExistence(forniture.tabellaSelezioneIstat);
                Thread.sleep(5000);
                
//                By codice_istat_by_name = By.xpath(forniture.codice_istat_by_name.replaceFirst("##", prop.getProperty("LOCALITA_ISTAT")));
                forniture.clickComponentIfExist(forniture.codice_istat_by_name_cla);
                forniture.clickComponentIfExist(forniture.buttonConfermaIstat);
                logger.write("selezionare il codice istat e premere conferma - Completed");
                gestione.checkSpinnersSFDC();
                insert.verifyComponentExistence(insert.inputIndirizzo);
                insert.clearAndSendKeys(insert.inputIndirizzo, prop.getProperty("INDIRIZZO"));
                insert.verifyComponentExistence(insert.inputCivico);
                insert.insertTextByChar(insert.inputCivico, prop.getProperty("CIVICO"));
            }

            logger.write("verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Start");

            util.getFrameActive();

            if (prop.getProperty("SEZIONE_ISTAT").equals("N")) {

                insert.verifyComponentExistence(insert.inputProvincia);
                insert.insertTextByChar(insert.inputProvincia, prop.getProperty("PROVINCIA_COMUNE"));

                insert.verifyComponentExistence(insert.inputComune);
                insert.insertTextByChar(insert.inputComune, prop.getProperty("PROVINCIA_COMUNE"));

                insert.verifyComponentExistence(insert.inputIndirizzo);
                insert.clearAndSendKeys(insert.inputIndirizzo, prop.getProperty("INDIRIZZO"));

                insert.verifyComponentExistence(insert.inputCivico);
                insert.insertTextByChar(insert.inputCivico, prop.getProperty("CIVICO"));

            }

            insert.clickComponentIfExist(insert.buttonVerifica);


            try {
//                insert.verifyComponentInvisibility(insert.labelIndirizzzoNonVerificatoGeneric);
              insert.verifyComponentVisibility(insert.labelIndirizzzoVerificato);
            	
            } catch (Exception e) {
                nonVerificatoText = insert.getElementTextString(insert.labelIndirizzzoNonVerificatoGeneric);
                throw new Exception("Error : '" + nonVerificatoText + "'", e.fillInStackTrace());
            }

            logger.write("verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Completed");

            if (prop.getProperty("POD").equalsIgnoreCase("ENERGIA") || prop.getProperty("POD").contains("IT002E")) {
                if (!prop.getProperty("TIPO_MISURATORE", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTipoMisuratore);
                    insert.selezionaTipoMisuratore(prop.getProperty("TIPO_MISURATORE"));
                }
                if (!prop.getProperty("TENSIONE_CONSEGNA", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
                }
                if (!prop.getProperty("POTENZA_CONTRATTUALE", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
                }

                if (!prop.getProperty("POTENZA_FRANCHIGIA", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
                }
            }

            if (prop.getProperty("VERIFICA_ESITO_OFFERTABILITA").contentEquals("Y")) {
                logger.write("Verifica esito Offertabilita - Start");
                forniture.verificaEsitoOffertabilitaCla(prop.getProperty("ESITO_OFFERTABILITA"));
                logger.write("Verifica esito Offertabilita - Completed");
            }

//            if (statoUbiest.compareTo("ON") == 0) {
//                logger.write("Selezione Indirizzo di Fatturazione - Start");
//                Thread.sleep(5000);
//                compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
//                logger.write("Selezione Indirizzo di Residenza/Sede Legalee - Completed");
//            } else if (statoUbiest.compareTo("OFF") == 0) {
//                logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Start");
//                Thread.sleep(5000);
//                compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzatoNoForzatura("Inserimento forniture", prop.getProperty("CAP"), prop.getProperty("CITTA"));
//                logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Completed");
//            }

            insert.verifyComponentExistence(insert.aggiungiFornitura);
            insert.verifyComponentExistence(insert.rimuoviFornitura);
            logger.write("Verifica esistenza pulsanti 'Aggiungi fornitura' e 'Rimuovi Fornitura'");

            insert.clickComponentIfExist(insert.buttonConfermaFooter);
            logger.write("click su pulsante Conferma situato subito sotto 'Aggiungi fornitura' e 'Rimuovi Fornitura' - Completed");

            insert.clickComponentIfExist(insert.buttonCreaOfferta);
            logger.write("click su pulsante Crea offerta e apertura pagina 'Riepilogo offerta' - Start");

//            gestione.checkSpinnersSFDC();
            logger.write("apertura pagina 'Riepilogo offerta' - Completed");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            //return;
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }


}
