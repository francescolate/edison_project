package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSNServiziComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_273_InfoEnelEnergia {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivateArea_Imprese_HomePageBSNComponent pc = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Verify home page title and path -- Start");
			pc.verifyComponentExistence(pc.pageTitle);
			pc.comprareText(pc.pagePath, pc.Path, true);
			pc.comprareText(pc.pageTitle, pc.PageTitle, true);
			logger.write("Verify home page title and path -- Completed");

			logger.write("Click on service from left menu -- Start");
			pc.clickComponent(pc.servizi);
			logger.write("Click on service from left menu -- Completed");

			BSNServiziComponent bc = new BSNServiziComponent(driver);
			logger.write("Verify servizi page title and page path and click on info enel energia-- Start");
			bc.verifyComponentExistence(bc.pageTitle);
			bc.comprareText(bc.pagePath, bc.Path, true);
			bc.comprareText(bc.pageText, bc.PageText, true);
			bc.verifyComponentExistence(bc.infoEnelEnergia);
			bc.clickComponent(bc.infoEnelEnergia);
			logger.write("Verify servizi page title and page path and click on info enel energia-- Completed");

			logger.write("Click on procegue button and verify the system message-- Start");
			Thread.sleep(10000);
			bc.verifyComponentExistence(bc.prosegui);
			Thread.sleep(5000);
			bc.clickComponent(bc.prosegui);
			Thread.sleep(20000);
			bc.comprareText(bc.errorMessage, bc.ErrorMessage, true);
			logger.write("Click on procegue button and verify the system message-- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
