package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModifyConsents_BsnComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyConsents_Bsn_ConsentsPageVerification {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModifyConsents_BsnComponent modCon = new ModifyConsents_BsnComponent(driver);
			
			//System.out.println("Verifying consents modification page strings");
			logger.write("Verifying consents modification page strings - Start");
			Thread.sleep(50000);
			modCon.checkConsPageStrings();
			logger.write("Verifying consents modification page strings - Completed");
			
			//System.out.println("Verifying consents table");
			logger.write("Verifying consents table - Start");
			//modCon.checkConsentsTable();
			logger.write("Verifying consents table - Completed");
			
			//System.out.println("Verifying privacy info");
			/*logger.write("Verifying privacy info - Start");
			modCon.verifyVisibilityThenClick(modCon.privacyInfoBtn);
			modCon.verifyVisibilityAndText(modCon.privacyInfoLbl, modCon.privacyInfoString);
			modCon.clickComponent(modCon.privacyInfoBtn);
			modCon.checkInfoPrivacyInvisibility();
			logger.write("Verifying privacy info - Completed");*/
			
			//System.out.println("Verifying buttons");
			logger.write("Verifying buttons - Start");
			modCon.checkConsPageBottomElements();
			logger.write("Verifying buttons - Completed");
			
			//System.out.println("Verifying header visibility");
			logger.write("Verifying header visibility - Start");
			HeaderComponent header = new HeaderComponent(driver);
			header.checkHeaderVisibility();
			logger.write("Verifying header visibility - Completed");
			
			//System.out.println("Verifying footer visibility");
			logger.write("Verifying footer visibility - Start");
			FooterPageComponent footer = new FooterPageComponent(driver);
			By[] locators = {
					footer.informazioniLegaliLink,
					footer.privacyLink,
					footer.creditsLink,
					footer.contattaciLink,
					footer.social_icons,
					footer.instagramIcon
			};
			for (int i = 0; i < locators.length; i++) {
				footer.verifyComponentVisibility(locators[i]);
			}
			logger.write("Verifying footer visibility - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
