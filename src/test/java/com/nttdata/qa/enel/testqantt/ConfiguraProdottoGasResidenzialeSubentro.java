package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;

import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConfiguraProdottoGasResidenzialeSubentro {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);

			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Start");
			configura.press(configura.linkGasResidenziale);
			configura.selezionaProdotto(prop.getProperty("PRODOTTO"));
			logger.write("Selezione del prodotto Gas");
			if (!prop.getProperty("OPZIONE_KAM_AGCOR_GAS","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR_GAS"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR Gas");
			}
			if (!prop.getProperty("SCELTA_ABBONAMENTI","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO"),prop.getProperty("SCELTA_ABBONAMENTI_GAS"));
				logger.write("Selezione Scelta Abbonamenti");
			}
			
			if (!prop.getProperty("OPZIONE_VAS","").equals("")){
				configura.configuraVas(prop.getProperty("PRODOTTO"),prop.getProperty("OPZIONE_VAS_GAS"));
				logger.write("Selezione VAS Gas");
			}
			
			if (!prop.getProperty("VAS_EX","").equals("")){
				configura.configuraVasEx(prop.getProperty("VAS_EX"));
				logger.write("Selezione VAS Gas");
			}
			
			if (!prop.getProperty("SCONTO","").equals("")){
				configura.configuraSconto(prop.getProperty("PRODOTTO"),prop.getProperty("SCONTO"));
				logger.write("Selezione Sconto Gas");
			}
			
			if (!prop.getProperty("OPZIONE_FIBRA","").equals("")){
				logger.write("Selezione Fibra - Start");
				configura.ConfiguraFibra(prop.getProperty("OPZIONE_FIBRA"));
				logger.write("Selezione Fibra - Completed");
			}
			
			configura.salvaConfigurazione();
			logger.write("Salva Configurazione Gas");
			
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO")+"- Completed");
			
			configura.checkOut();
			logger.write("Check out carrello");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
