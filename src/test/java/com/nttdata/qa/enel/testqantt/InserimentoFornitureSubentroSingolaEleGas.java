package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class InserimentoFornitureSubentroSingolaEleGas extends BaseComponent {

	public InserimentoFornitureSubentroSingolaEleGas(WebDriver driver) {
		super(driver);
	}

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String nonVerificatoText;

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrecheckComponent forniture = new PrecheckComponent(driver);
			InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
			SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
			String statoUbiest = Costanti.statusUbiest;

			/*
			 * PER RECUPERARE IL POD UTILIZZARE ALL'INIZIO DEL TEST:
			 * RecuperaPodNonEsistente.main(args); SET NELLE PROPERTIES ELETTRICITA:
			 * prop.setProperty("COMMODITY"; "ELE"); SET NELLE PROPERTIES GAS:
			 * prop.setProperties("COMMODITY"; "GAS");
			 * 
			 */

			logger.write("Popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Start");
			if (!prop.getProperty("MODIFICA_PRECHECK", "").equals("")) {
				forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD_1"));
			} else {
				forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD"));
			}
			forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
			forniture.clickComponentIfExist(forniture.precheckButton2);

			logger.write("popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Completed");

			merc.checkSpinnersSFDC();

			if (prop.getProperty("MERCATO").equals("Salvaguardia") && (!prop.getProperty("COMMODITY").equals("ELE"))
					&& (!prop.getProperty("ERRORE_PRECHECK", "").equals(""))) {
				logger.write("Verifica esistenza errore: " + prop.getProperty("ERRORE_PRECHECK") + "- Start");
				merc.verifyExistenceComponent(merc.popupErrorePrecheck);
				logger.write("Verifica esistenza errore: " + prop.getProperty("ERRORE_PRECHECK") + " Esistenza: "
						+ merc.verifyExistenceComponent(merc.popupErrorePrecheck) + "- Completed");

			} else {

//            gestione.checkSpinnersSFDC(); // c'Ã¨ il Verify dopo che aspetta l'oggetto

				if (prop.getProperty("SEZIONE_ISTAT").equals("Y")) {
					logger.write("selezionare il codice istat e premere conferma - Start");
					forniture.verifyComponentExistence(forniture.tabellaSelezioneIstat);
					By codice_istat_by_name = By.xpath(forniture.codice_istat_Roma_xpath);
					forniture.clickComponentIfExist(codice_istat_by_name);
					forniture.clickComponentIfExist(forniture.buttonConfermaIstat);
					logger.write("selezionare il codice istat e premere conferma - Completed");
				}

				if (!insert.verifyExistence(insert.labelIndirizzzoVerificato, 50)) {
					if (prop.getProperty("SEZIONE_ISTAT").equals("Y")) {

						gestione.checkSpinnersSFDC();
						insert.verifyComponentExistence(insert.inputIndirizzo);
						insert.clearAndSendKeys(insert.inputIndirizzo, prop.getProperty("INDIRIZZO"));
						insert.verifyComponentExistence(insert.inputCivico);
						insert.insertTextByChar(insert.inputCivico, prop.getProperty("CIVICO"));

						insert.clickComponentIfExist(insert.buttonVerifica);
					} else if (statoUbiest.compareTo("OFF") == 0) {
						logger.write("Inserimento Indirizzo Forzato POD ELE- Start");
						compila.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA_COMUNE"),
								prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"),
								prop.getProperty("CIVICO"));
						compila.verificaIndirizzo(compila.buttonVerifica);
						compila.forzaIndirizzoPrimaAttivazione(prop.getProperty("CAP_FORZATURA"),
								prop.getProperty("CITTA_FORZATURA"));
						logger.write("Inserimento Indirizzo Forzato  POD ELE- Completed");
						TimeUnit.SECONDS.sleep(5);
					}

					logger.write(
							"verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Start");

					util.getFrameActive();

					if (prop.getProperty("SEZIONE_ISTAT").equals("N")) {

						if (statoUbiest.compareTo("ON") == 0) {
							logger.write("Inserimento e Validazione Indirizzo - Start");
							compila.inserisciIndirizzo(prop.getProperty("PROVINCIA_COMUNE"),
									prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"),
									prop.getProperty("CIVICO"));
							compila.verificaIndirizzo(compila.buttonVerifica);
							logger.write("Inserimento e Validazione Indirizzo POD ELE- Completed");
							TimeUnit.SECONDS.sleep(5);
						} else if (statoUbiest.compareTo("OFF") == 0) {
							logger.write("Inserimento Indirizzo Forzato POD ELE- Start");
							compila.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA_COMUNE"),
									prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"),
									prop.getProperty("CIVICO"));
							compila.verificaIndirizzo(compila.buttonVerifica);
							compila.forzaIndirizzoPrimaAttivazione(prop.getProperty("CAP_FORZATURA"),
									prop.getProperty("CITTA_FORZATURA"));
							logger.write("Inserimento Indirizzo Forzato  POD ELE- Completed");
							TimeUnit.SECONDS.sleep(5);
						}
					}

					try {
						insert.verifyComponentInvisibility(insert.labelIndirizzzoNonVerificatoGeneric);
					} catch (Exception e) {
						nonVerificatoText = insert.getElementTextString(insert.labelIndirizzzoNonVerificatoGeneric);
						throw new Exception("Error : '" + nonVerificatoText + "'", e.fillInStackTrace());
					}

					logger.write(
							"verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Completed");

				}
				if (prop.getProperty("VERIFICA_ESITO_OFFERTABILITA", "").contentEquals("Y") // se esito KO non prosegue
						&& prop.getProperty("ESITO_OFFERTABILITA").equals("KO")) {
					logger.write("Verifica esito Offertabilita - Start");
					forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
					logger.write("Offertabilità : " + forniture.recuperaOffertabilità());
					logger.write("Verifica esito Offertabilita - Completed");
				} else {
					logger.write("Verifica esito Offertabilita - Start");
					if (!prop.getProperty("ESITO_OFFERTABILITA", "").equals("")) {
						forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
						logger.write("OffertabilitÃ : " + forniture.recuperaOffertabilità());
						logger.write("Verifica esito Offertabilita - Completed");
					} else {
						//se l'esito non viene specificato nelle prop, qui viene settato automaticamente ad OK
						logger.write("Verifica esito Offertabilita - Start");
                        prop.setProperty("ESITO_OFFERTABILITA", "OK");
						forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
						logger.write("OffertabilitÃ : " + forniture.recuperaOffertabilità());
						logger.write("Verifica esito Offertabilita - Completed");
					}

					if (prop.getProperty("COMMODITY").equals("ELE")) {
						if (driver.findElement(insert.campoTipoMisuratore).isEnabled()) {
							insert.verifyComponentExistence(insert.campoTipoMisuratore);
							insert.selezionaTipoMisuratore(prop.getProperty("TIPO_MISURATORE"));
						}

						if (!prop.getProperty("TENSIONE_CONSEGNA", "").equals("")) {
							insert.verifyComponentExistence(insert.campoTensioneConsegna);
							insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
						}
						if (!prop.getProperty("POTENZA_CONTRATTUALE", "").equals("")) {
							insert.verifyComponentExistence(insert.campoPotenzaContattuale);
							insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
						}

						if (!prop.getProperty("POTENZA_FRANCHIGIA", "").equals("")) {
							insert.verifyComponentExistence(insert.campoPotenzaFranchigia);
							insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
						}

					} else if (prop.getProperty("COMMODITY").equals("GAS")
							|| prop.getProperty("COMMODITY").equals("GAS3")
							|| prop.getProperty("COMMODITY").equals("GAS4")) {
						insert.verifyComponentExistence(insert.campoMatricolaContatore);
						if (!prop.getProperty("MATRICOLA_CONTATORE", "").equals("")) {
							insert.clearAndSendKeys(insert.campoMatricolaContatore,
									prop.getProperty("MATRICOLA_CONTATORE"));
						}
					}

					if (!prop.getProperty("MODIFICA_PRECHECK", "").equals("")) {
						logger.write("Modifica Precheck - Start");
						forniture.clickComponent(forniture.ripetiPrecheck);
						logger.write("Ripeti precheck");
						forniture.clickComponentIfExist(forniture.modificaPrecheck);
						forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD_2"));
						forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
						forniture.clickComponentIfExist(forniture.precheckButton2);
						insert.verifyComponentExistence(insert.campoMatricolaContatore);
						if (!prop.getProperty("MATRICOLA_CONTATORE", "").equals("")) {
							insert.clearAndSendKeys(insert.campoMatricolaContatore,
									prop.getProperty("MATRICOLA_CONTATORE"));
						}
						if (prop.getProperty("VERIFICA_ESITO_OFFERTABILITA").contentEquals("Y")) {
							logger.write("Verifica esito Offertabilita - Start");
							forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
							logger.write("Verifica esito Offertabilita - Completed");
						}
						forniture.verificaPOD(prop.getProperty("POD_2"));
						logger.write("Modifica Precheck - Completed");
					} else {

						insert.clickComponentIfExist(insert.buttonConfermaFooter);
						logger.write(
								"click su pulsante Conferma situato subito sotto 'Aggiungi fornitura' e 'Rimuovi Fornitura' - Completed");

						insert.clickComponentIfExist(insert.buttonCreaOfferta);
						logger.write("click su pulsante Crea offerta e apertura pagina 'Riepilogo offerta' - Start");

						gestione.checkSpinnersSFDC();
						logger.write("apertura pagina 'Riepilogo offerta' - Completed");
					}

					prop.setProperty("RETURN_VALUE", "OK");
				}
			}
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			// return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
