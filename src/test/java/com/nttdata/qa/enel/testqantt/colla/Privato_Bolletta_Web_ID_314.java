package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID_314 {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		JavascriptExecutor js;

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);				
			js = (JavascriptExecutor)driver;
			SeleniumUtilities util = new SeleniumUtilities(driver);
		
			//LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			
			PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			
		
			/*logger.write("Accessing login page - Start");
			lpv.launchLink(prop.getProperty("WP_LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			logger.write("Accessing login page - Complete");
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			lpv.verifyComponentExistence(lpv.loginPage);
		
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
								
				
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("WP_USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("WP_PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			*/
			dfc.verifyComponentExistence(dfc.BenvenutoTitle);
			dfc.comprareText(dfc.BenvenutoTitle, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_TITLE, true);
			dfc.verifyComponentExistence(dfc.BenvenutoContent);
			dfc.comprareText(dfc.BenvenutoContent, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_CONTENT, true);
			dfc.verifyComponentExistence(dfc.fornitureHeader);
			dfc.comprareText(dfc.fornitureHeader, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_HEADER, true);
			dfc.verifyComponentExistence(dfc.fornitureContent);
			dfc.comprareText(dfc.fornitureContent, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_CONTENT, true);
			
			logger.write("Verify and click on Servizi link  - Start");
			dfc.verifyComponentExistence(dfc.serviziLink);
			dfc.clickComponentIfExist(dfc.serviziLink);
			logger.write("Verify and click on Servizi link  - Complete");
			
			logger.write("Verify and click on Servizi title and contents  - Start");
			dfc.verifyComponentExistence(dfc.serviziFornitureTitle);
			dfc.comprareText(dfc.serviziFornitureTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziFornitureContent);
			dfc.comprareText(dfc.serviziFornitureContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteTitle);
			dfc.comprareText(dfc.serviziBolletteTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteContent);
			dfc.comprareText(dfc.serviziBolletteContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziContratoTitle);
			dfc.comprareText(dfc.serviziContratoTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziContratoContent);
			dfc.comprareText(dfc.serviziContratoContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_CONTENT, true);
			logger.write("Verify and click on Servizi title and contents  - Complete");
			
			logger.write("Verify and click on BollettaWeb Link  - Start");
			dfc.verifyComponentExistence(dfc.bollettaWebLink);
			dfc.clickComponent(dfc.bollettaWebLink);
			logger.write("Verify and click on BollettaWeb Link  - Complete");
			
			logger.write("Verify the Bolletta web header and contents  - Start");
			dfc.verifyComponentExistence(dfc.bollettaWebHeader);
			dfc.comprareText(dfc.bollettaWebHeader, PrivateAreaDisattivazioneFornituraComponent.BOLLETTAWEB_HEADER, true);
			dfc.verifyComponentExistence(dfc.bollettaWebContent);
			dfc.comprareText(dfc.bollettaWebContent, PrivateAreaDisattivazioneFornituraComponent.BOLLETTAWEB_CONTENT, true);
			logger.write("Verify the Bolletta web header and contents  - Complete");
			
			logger.write("Click on Attiva Bolletaweb button  - Start");
			dfc.verifyComponentExistence(dfc.attivaBollettaWebButton);
			dfc.clickComponent(dfc.attivaBollettaWebButton);
			logger.write("Click on Attiva Bolletaweb button  - Complete");
			
			dfc.comprareText(dfc.attivazioneHeader, dfc.ATTIVAZIONE_HEADER, true);
			dfc.comprareText(dfc.attivazioneContent, dfc.ATTIVAZIONE_CONTENT1, true);
			
			logger.write("Select the supply by clicking on the checkbox  - Start");
			dfc.verifyComponentExistence(dfc.nonAttivoSupplyCheckBox);
			dfc.clickComponent(dfc.nonAttivoSupplyCheckBox);
			logger.write("Select the supply by clicking on the checkbox  - Complete");
			
			logger.write("Click on Continua button - Start");
			dfc.verifyComponentExistence(dfc.supplyContinuaButton);
			dfc.clickComponent(dfc.supplyContinuaButton);
			logger.write("Click on Continua button - Complete");
			
			dfc.comprareText(dfc.inserimentoDati, dfc.INSERIMENTo_DATI, true);
				
			logger.write("Verify the continua and Indietro buttons - Start");
			dfc.waitForElementToDisplay(dfc.statusContinuaButton);
			dfc.verifyComponentExistence(dfc.statusIndietroButton);
			logger.write("Verify the continua and Indietro buttons - Complete");
			
			dfc.clickComponent(dfc.statusContinuaButton);
			
			dfc.verifyComponentExistence(dfc.modificaBollettaEmailInput);
			dfc.enterInput(dfc.modificaBollettaEmailInput, prop.getProperty("EMAIL"));
			
			dfc.verifyComponentExistence(dfc.modificaBollettaConfermaEmail);
			dfc.enterInput(dfc.modificaBollettaConfermaEmail, prop.getProperty("CONFERMA_EMAIL"));
			
			dfc.verifyComponentExistence(dfc.modificaBollettaNumeroInput);
			
			dfc.enterInput(dfc.modificaBollettaNumeroInput, prop.getProperty("NUMERO"));
			dfc.verifyComponentExistence(dfc.modificaBollettaNumeroConferma);
			dfc.enterInput(dfc.modificaBollettaNumeroConferma, prop.getProperty("CONFERMA_NUMERO"));
			
			dfc.verifyComponentExistence(dfc.modificaBollettaCheckboxInput);
			dfc.verifyComponentExistence(dfc.modificaBollettaCheckboxLabel);
			dfc.clickComponent(dfc.modificaBollettaCheckboxLabel);
			
			dfc.verifyComponentExistence(dfc.statusContinuaButton);
			dfc.clickComponent(dfc.statusContinuaButton);
			
			dfc.comprareText(dfc.riepilogoeConfermaDati, dfc.RIEPILOGO_CONFERMA_DATI, true);
			
			dfc.verifyComponentExistence(dfc.BollettanumerodicellulareLabel);
			dfc.comprareText(dfc.BollettanumerodicellulareLabel, PrivateAreaDisattivazioneFornituraComponent.BOLLETTA_CELLULARELABEL, true);
				
		    dfc.verifyComponentExistence(dfc.ModificaBollettaConfermaBtn);
		    dfc.clickComponent(dfc.ModificaBollettaConfermaBtn);
		    
		    dfc.comprareText(dfc.operazioneHeader, dfc.OPERAZIONE_HEADER, true);
		    dfc.comprareText(dfc.operazioneContents1, dfc.OPERAZIONE_CONTENTS1, true);
		    
		    dfc.verifyComponentExistence(dfc.modificaBollettaFineBtn);
		    dfc.clickComponent(dfc.modificaBollettaFineBtn);
		    
		    
			
			prop.setProperty("RETURN_VALUE", "OK");
	
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	


}
