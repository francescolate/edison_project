package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class VerificaContatti {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				AccediTabClientiComponent tabClienti = new AccediTabClientiComponent(driver);
				tabClienti.accediTabClienti();
				CreaNuovoClienteComponent crea = new CreaNuovoClienteComponent(driver);
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				String frameName;
				switch (prop.getProperty("TIPOLOGIA_CLIENTE")) {
				case "RESIDENZIALE":
					crea.nuovoCliente();
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonResidenziale);
					crea.creaClienteResidenziale(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO

						indirizzi.compilaIndirizziResidenziale(frameName, "NAPOLI",
								"NAPOLI", "VIA ROMA NAPOLI", "4");
						crea.verificaObbligatorietaCampiContatti();
					}	
					else
					{//Ubiest NON Attivo
						indirizzi.forzaindirizzoDomicilioUBIEST(frameName, "NAPOLI", "NAPOLI", "VIA ROMA NAPOLI", "4");
						Thread.currentThread().sleep(1000);
						indirizzi.forzaCopiaIndirizzo(frameName);
						Thread.currentThread().sleep(1000);
						
						crea.verificaObbligatorietaCampiContatti();
//						indirizzi.forzaindirizzoResidenzaUBIEST(frameName, "NAPOLI", "NAPOLI", "VIA ROMA NAPOLI", "4");

					}

					break;
					
				case "IMPRESA":
					crea.nuovoCliente();
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonImpresaIndividuale);
					crea.creaClienteImpresaIndividuale(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
						indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					crea.verificaObbligatorietaCampiContatti();
					break;
					
				case "CONDOMINIO":
					crea.nuovoCliente();
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonCondominio);
					crea.creaClienteCondominio(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO

						indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					crea.verificaObbligatorietaCampiContatti();
					break;

				case "BUSINESS":
					crea.nuovoCliente();
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonBusiness);
					crea.creaClienteBusiness(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
						indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					crea.verificaObbligatorietaCampiContatti();
					break;

				default:
					throw new Exception(
							"Tipologia cliente diversa da RESIDENZIALE/IMPRESA/CONDOMINIO/BUSINESS. Impossibile proseguire");
				}


			}

		

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").contentEquals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
