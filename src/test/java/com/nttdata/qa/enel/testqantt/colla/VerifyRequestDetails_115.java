package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OffertaComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetails_115 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			OffertaComponent oc = new OffertaComponent(driver);
			prop.setProperty("WB_ACCOUNTTYPE", "RESIDENZIALE");		
			prop.setProperty("WB_COMMODITY", "GAS");		
			prop.setProperty("WB_NEWORDERITEMSTATUS", "Inviatae");		
			prop.setProperty("WB_ACTIVATIONREASON", "SUBENTRO");		
			prop.setProperty("WB_STATUS", "Inviata");		
			prop.setProperty("WB_OFFERTYPE", "GAS");		
			prop.setProperty("WB_OPERATIONTYPE", "Attivazione");		
			prop.setProperty("WB_ACTIVATIONREASON1", "SWITCH ATTIVO");		

			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
						
			logger.write("Inserendo la query - START");
			Thread.sleep(5000);
			wbc.insertQuery(Costanti.getQuery_115a);
			wbc.pressButton(wbc.submitQuery);
			oc.comprareText(oc.wbAccountType, prop.getProperty("WB_ACCOUNTTYPE"), true);
			oc.comprareText(oc.wbCommodity, prop.getProperty("WB_COMMODITY"), true);
			oc.comprareText(oc.wbNewOrderItemStatus, prop.getProperty("WB_NEWORDERITEMSTATUS"), true);
			
			wbc.insertQuery(Costanti.getQuery_115b);
			wbc.pressButton(wbc.submitQuery);
			oc.comprareText(oc.wbActivationReason, prop.getProperty("WB_ACTIVATIONREASON1"), true);
			oc.comprareText(oc.wbStatus, prop.getProperty("WB_STATUS"), true);
			oc.comprareText(oc.wbOfferType, prop.getProperty("WB_OFFERTYPE"), true);
			oc.comprareText(oc.wbOperationType, prop.getProperty("WB_OPERATIONTYPE"), true);
			oc.comprareText(oc.wbAccountType1, prop.getProperty("WB_ACCOUNTTYPE"), true);
			
			
			wbc.insertQuery(Costanti.getQuery_115c);
			wbc.pressButton(wbc.submitQuery);
			oc.comprareText(oc.wbActivationReason, prop.getProperty("WB_ACTIVATIONREASON1"), true);
			oc.comprareText(oc.wbStatus, prop.getProperty("WB_STATUS"), true);
			oc.comprareText(oc.wbOfferType, prop.getProperty("WB_OFFERTYPE"), true);
			oc.comprareText(oc.wbOperationType, prop.getProperty("WB_OPERATIONTYPE"), true);
			oc.comprareText(oc.wbAccountType1, prop.getProperty("WB_ACCOUNTTYPE"), true);
			
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
	
}
