package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaQualityCheckComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class SelezionaQualityCheck 
{
	//private Properties prop = null;
	
	@Step("Login Salesforce")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				SelezionaQualityCheckComponent ricerca = new SelezionaQualityCheckComponent(driver);

				logger.write("Click Quality Check - Start");
				
				ricerca.selezionaQualityCheck();
				
				logger.write("Click Quality Check - Completed");
			}
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
