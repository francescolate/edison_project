package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.components.colla.SubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeID27 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			logger.write("verifica presenza dell'offerta 'Ore Free' - Start");
			OreFreeComponent offerta=new OreFreeComponent(driver);
			
			By offerta_orefree=offerta.offertaOreFree;
			offerta.verifyComponentExistence(offerta_orefree);
			offerta.scrollComponent(offerta_orefree);
			logger.write("verifica presenza dell'offerta 'Ore Free' - Completed");
			
			logger.write("Click sul Link VERIFICA DISPONIBITA' e check sulla popup - Start");
			By verificadisponibilita_Button=offerta.verificadisponibilitaButton;
			offerta.verifyComponentExistence(verificadisponibilita_Button);
			offerta.clickComponent(verificadisponibilita_Button);
			
			By inserisci_NumeroPod=offerta.inserisciNumeroPod;
			offerta.verifyComponentExistence(inserisci_NumeroPod);
			
			By testo_popup=offerta.testopopup;
			offerta.verifyComponentExistence(testo_popup);
			
			By campo_CodicePod=offerta.campoCodicePod;
			offerta.verifyComponentExistence(campo_CodicePod);
			
			By conferma_Button=offerta.confermaButton;
			offerta.verifyComponentExistence(conferma_Button);
			offerta.replaceLinkCollaInXpathAndCheckExistObject(prop.getProperty("LINK"));
			logger.write("Click sul Link VERIFICA DISPONIBITA' e check sulla popup - Completed");
			
			logger.write("Click su CONFERMA senza inserire un valore nel campo POD  - Start");
			offerta.clickComponent(conferma_Button);
			TimeUnit.SECONDS.sleep(2);
			By label_campoObbligatorio=offerta.labelcampoObbligatorio;
			offerta.verifyComponentExistence(label_campoObbligatorio);
			logger.write("Click su CONFERMA senza inserire un valore nel campo POD  - Completed");
			
			logger.write("click sul link 'Che cos'è il POD e dove lo trovo?' e check sulla pagina di SUBENTRO - Start");
			offerta.replaceLinkCollaInXpathEClickObject(prop.getProperty("LINK"));
			TimeUnit.SECONDS.sleep(2);
			
			SubentroComponent subentro=new SubentroComponent(driver);
			subentro.checkUrl(prop.getProperty("LINK")+subentro.linkSUBENTRO);
			
			By titolo_Pagesubentro=subentro.titoloPagesubentro;
			subentro.verifyComponentExistence(titolo_Pagesubentro);
			
			By sottotitolo_Pagesubentro=subentro.sottotitoloPagesubentro;
			subentro.verifyComponentExistence(sottotitolo_Pagesubentro);
			
			logger.write("click sul link 'Che cos'è il POD e dove lo trovo?' e check sulla pagina di SUBENTRO - Completed");
			
			logger.write("effettuare il back del browser, cliccare su 'VERIFICA DISPONIBILITA'' e corretta visualizzazione del campo POD - Start");
			driver.navigate().back();
			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkLUCE_E_GAS_FILTRO);
			
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);			
			By searchMenu=headerMenu.searchSection;
			headerMenu.verifyQuestionsExistence(searchMenu);
			By precedentcontract=headerMenu.contractListvalue;
			headerMenu.verifyTextSearchMenu(precedentcontract,prop.getProperty("TYPE_OF_CONTRACT"));
			
			By precedentplace=headerMenu.placeListvalue;
			headerMenu.verifyTextSearchMenu(precedentplace,prop.getProperty("PLACE"));
			
			By precedentneed=headerMenu.Listvalue;
			headerMenu.verifyTextSearchMenu(precedentneed,prop.getProperty("NEED_PRECEDENTE"));
			
			offerta.clickComponent(verificadisponibilita_Button);
            offerta.verifyComponentExistence(inserisci_NumeroPod);
			
			offerta.verifyComponentExistence(testo_popup);
			offerta.verifyComponentExistence(campo_CodicePod);              
			logger.write("effettuare il back del browser, cliccare su 'VERIFICA DISPONIBILITA'' e corretta visualizzazione del campo POD - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
