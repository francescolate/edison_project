package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaVBLeBLComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificheVBLeBL {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

//			"Nella sezione Quality check selezionare il tasto Check Cliente 
//			Innesco dei controlli sulla VBL e BlackList di Contatto"
//			Gli esiti VBL e Blacklist saranno OK(oppure NA)
//			Selezionare il tasto Avanti
//			"Nella sezione Quality check selezionare il tasto Check Cliente 
//			Innesco dei controlli sulla VBL e BlackList di Contatto"
//			Nella sezione Forniture inserite effettuare l'Offertabilità e il precheck su tutte le linee d'offerta premendo i Button Check Offertabilità e Precheck
//			"Alla ricezione dell'esito OK oppure NA per tutte le verifiche
//			Selezionare il tasto Avanti"
//			Nella sezione Quality check  nel campo Esito Quality Check selezionare la voce "QC OK senza nuova registrazione"
//			Premere il tasto Conferma Esito

		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    driver.switchTo().defaultContent();
            VerificaVBLeBLComponent verifica = new VerificaVBLeBLComponent(driver);
		    logger.write("Click pulsante Check Cliente - Start");
		    verifica.checkCliente();
		    logger.write("Click pulsante Check Cliente - Completed");
		    logger.write("Verifica Esito VBL e Esito BL Contatto - Start");
		    verifica.verificaEsitoVBL();
		    logger.write("Verifica Esito VBL e Esito BL Contatto - Completed");
		    logger.write("Click pulsante Avanti - Start");
		    verifica.clickAvanti();
		    logger.write("Click pulsante Avanti - Completed");
		    logger.write("Click Check Offertabilità - Start");
		    verifica.clickCheckOffertabilita();
		    logger.write("Click Check Offertabilità - Completed");
		    logger.write("Click PreCheck - Start");
		    verifica.ClickPrecheck();
		    logger.write("Click PreCheck - Completed");
		    logger.write("Verifica Esito Offertabilità e Precheck - Start");
		    verifica.VerificaEsitiPrecheckOff();
		    logger.write("Verifica Esito Offertabilità e Precheck - Completed");
		    logger.write("Click pulsante Avanti - Start");
		    verifica.clickAvanti2();
		    logger.write("Click pulsante Avanti - Completed");
		    logger.write("Seleziona QC OK senza Nuova Registrazione - Start");
            verifica.selezionaEsitoQC();
            logger.write("Seleziona QC OK senza Nuova Registrazione - Completed");
            logger.write("Click pulsante Conferma - Start");
            verifica.clickConfermaEsito();
            logger.write("Click pulsante Conferma - Completed");
            
	        prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
