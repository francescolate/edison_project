package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class PrecheckSwaEVOMulti {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    PrecheckComponent pre = new PrecheckComponent(driver);
		    
		    String tipoCommodity="";
		    
			if(prop.getProperty("COMMODITY").compareTo("ELE")==0){
				tipoCommodity="ELETTRICO";
			}
			else if(prop.getProperty("COMMODITY").compareTo("GAS")==0){
				tipoCommodity="GAS";
			}
		    
		    //Precheck ELE-1
		    pre.selezionaFortinutaMulti(prop.getProperty("POD_1"),tipoCommodity);
		    logger.write("Selezione POD 1");
		    logger.write("Pressione pulsante Precheck POD 1- Start");
		    pre.effettuaPrecheck();
		    logger.write("Pressione pulsante Precheck - POD 1 Completed");
		    logger.write("Verifica esito precheck POD 1 Ok - Start");
		    
		    pre.verificaEsitoOKPrecheckMulti(prop.getProperty("POD_1"),tipoCommodity);
		    
		    //Precheck ELE-2
		    pre.selezionaFortinutaMulti(prop.getProperty("POD_2"),tipoCommodity);
		    logger.write("Selezione POD 2");
		    logger.write("Pressione pulsante Precheck POD 2- Start");
		    pre.effettuaPrecheck();
		    logger.write("Verifica esito precheck POD 2 Ok - Completed");
		    pre.verificaEsitoOKPrecheckMulti(prop.getProperty("POD_2"),tipoCommodity);
		    
		    logger.write("Conferma Precheck - Start");
		    pre.press(pre.confermaPrecheck);
		    logger.write("Conferma Precheck - Completed");
		    
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
