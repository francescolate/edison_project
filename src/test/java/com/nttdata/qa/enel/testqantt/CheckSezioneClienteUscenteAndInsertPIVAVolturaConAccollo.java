package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CheckSezioneClienteUscenteAndInsertPIVAVolturaConAccollo {

    public static void main(String[] args) throws Exception {
        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            logger.write("check esistenza sezione 'Selezione Cliente Uscente' - Start");
            SezioneSelezioneClienteUscenteComponent clienteUscente = new SezioneSelezioneClienteUscenteComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);

            By sezione_ClienteUscente = clienteUscente.sezioneClienteUscente;
            clienteUscente.verifyComponentExistence(sezione_ClienteUscente);

            By checkBox_CF = clienteUscente.checkBoxCF;
            clienteUscente.verifyComponentExistence(checkBox_CF);

            By checkBox_PIVA = clienteUscente.checkBoxPIVA;
            clienteUscente.verifyComponentExistence(checkBox_PIVA);

            By checkBox_PodPdr = clienteUscente.checkBoxPodPdr;
            clienteUscente.verifyComponentExistence(checkBox_PodPdr);

            By checkBox_NumeroCliente = clienteUscente.checkBoxNumeroCliente;
            clienteUscente.verifyComponentExistence(checkBox_NumeroCliente);

            By inputBox_CF = clienteUscente.inputBoxCF;
            clienteUscente.verifyComponentExistence(inputBox_CF);

            By inputBox_PIVA = clienteUscente.inputBoxPIVA;
            clienteUscente.verifyComponentExistence(inputBox_PIVA);

            By inputBox_PodPdr = clienteUscente.inputBoxPodPdr;
            clienteUscente.verifyComponentExistence(inputBox_PodPdr);

            By inputBox_NumeroCliente = clienteUscente.inputBoxNumeroCliente;
            clienteUscente.verifyComponentExistence(inputBox_NumeroCliente);

            By button_Ricerca = clienteUscente.buttonRicerca;
            clienteUscente.verifyComponentExistence(button_Ricerca);

            logger.write("check esistenza sezione 'Selezione Cliente Uscente' - Completed");
            logger.write("selezionare checkBox 'P.IVA',valorizzare campo P.IVA e click su ricerca - Start");
            clienteUscente.clickComponent(checkBox_PIVA);
            clienteUscente.enterPodValue(inputBox_PIVA, prop.getProperty("CODICE_FISCALE_CL_USCENTE"));
            clienteUscente.clickComponent(button_Ricerca);
            gestione.checkSpinnersSFDC();

            logger.write("selezionare checkBox 'P.IVA',valorizzare campo P.IVA e click su ricerca - Completed");
            logger.write("Spostarsi nella sezione 'Clienti' e selezionare la check box,  - Start");

            clienteUscente.verifyComponentExistence(clienteUscente.buttonConferma);
            clienteUscente.verificaColonneTabella(clienteUscente.colonne);
            //se lo scenario lo richiede si setta CL_USCENTE_DENOMINAZIONE, altrimenti lo impostiamo a 0
            String denominazioneCLUscente = prop.getProperty("CL_USCENTE_DENOMINAZIONE");

            if (denominazioneCLUscente.compareTo("0") == 0) {
                clienteUscente.selezionaRecordInTabella(prop.getProperty("CODICE_FISCALE_CL_USCENTE"));
            } else {
                clienteUscente.selectRecordDenomazioneInTabella(prop.getProperty("CL_USCENTE_DENOMINAZIONE"));
            }

            gestione.checkSpinnersSFDC();

            logger.write("Spostarsi nella sezione 'Clienti' e selezionare la check box  - Completed");
            logger.write("verifica che il tasto 'Conferma' si abiliti e click - Start");


            clienteUscente.verifyComponentExistence(clienteUscente.buttonConferma);
            clienteUscente.verifyComponentExistence(clienteUscente.buttonConfermaAbilitato);
            clienteUscente.clickComponent(clienteUscente.buttonConfermaAbilitato);
            gestione.checkSpinnersSFDC();

            logger.write("verifica che il tasto 'Conferma' si abiliti e click - Completed");


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }


    }

}
