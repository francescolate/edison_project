package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.components.colla.ModConsensiACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModConsensiACR166 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		ModConsensiACRComponent mod = new ModConsensiACRComponent(driver);
		ModConsensiACR162Component mod1 = new ModConsensiACR162Component(driver);
		
		logger.write("click on Account menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.account);
		Thread.sleep(10000);
		mod.comprareText(mod.accountHeading, ModConsensiACRComponent.ACCOUNT_HEADING, true);
		logger.write("click on Account menu item and verify the section heading and paragraph- End");
		
		logger.write("Verify the Consesi section heading and paragraph- started");
		mod.comprareText(mod.consesiHeading, ModConsensiACRComponent.CONSESI_HEADING, true);
		mod.comprareText(mod.consesiParagraph, ModConsensiACRComponent.CONSESI_PARAGRAPH, true);
		logger.write("Verify the Consesi section heading and paragraph- Ends");
		
		logger.write("Click on Modifica Consensi Button and verify the data- Starts");
		mod.clickComponent(mod.modificaConsensiButton);
		Thread.sleep(35000);
		mod.comprareText(mod.consensiSectionHeading, ModConsensiACRComponent.CONSENSI_SECTION_HEADING, true);
		logger.write("Click on Modifica Consensi Button and verify the data- Ends");
		
		logger.write("Verification of consents for Profilazione- Start ");
		mod.comprareText(mod1.consensoProfilazione, ModConsensiACR162Component.CONSENSO_PROFILAZIONE, true);
		mod.verifyComponentExistence(mod1.consensoProfilazioneSIRadio);
		mod.verifyComponentExistence(mod1.consensoProfilazioneNORadio);
		mod.radiobuttonStatus(mod.profilazoneSIRadio, mod.profilazoneNORadio, mod.profilazoneSIRadioLabel, mod.profilazoneNORadioLabel);
		mod.clickComponent(mod.salvaModificheButton);
		Thread.sleep(10000);
		mod.comprareText(mod.confirmationHeading, ModConsensiACRComponent.CONFIRMATION_HEADING, true);
		mod.comprareText(mod.confirmationParagraph, ModConsensiACRComponent.CONFIRMATION_PARAGRAPH, true);
		logger.write("Verification of consents for Profilazione- End ");
		
		logger.write("Click on Fine Button and verification of Data- Starts");
		mod.clickComponent(mod.fineButton);
		Thread.sleep(40000);
		mod1.comprareText(mod1.homePageHeading1, ModConsensiACR162Component.HOME_PAGE_HEADING1, true);
//		mod1.comprareText(mod1.homePageParagraph1, ModConsensiACR162Component.HOME_PAGE_PARAGRAPH1, true);
//		mod1.comprareText(mod1.homePageHeading2, ModConsensiACR162Component.HOME_PAGE_HEADING2, true);
//		mod1.comprareText(mod1.homePageParagraph2, ModConsensiACR162Component.HOME_PAGE_PARAGRAPH2, true);
		logger.write("Click on Fine Button and verification of Data- Ends");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
