package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerifichePodIniziali_GAS {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Interrogazione","Report PS");
				//Inserimento POD
				R2D_VerifichePodComponent verifichePod = new R2D_VerifichePodComponent(driver);
				if (!prop.getProperty("POD_GAS","").equals("")) {
					verifichePod.inserisciPod(verifichePod.inputPDR, prop.getProperty("POD_GAS",prop.getProperty("POD")));
				}

				Thread.sleep(10000);

				//Inserimento OI-RICERCA CRM
				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA"));
				//Cerca
				verifichePod.cercaPod(verifichePod.buttonCerca);
				//Click Dettaglio Pratiche
				verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				//Click su Pratica
				verifichePod.selezionaPratica(verifichePod.rigaPratica);
				//Verifica Campi
					//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
//				campiDaVerificare.add("Distributore;"+prop.getProperty("DISTRIBUTORE_R2D_ATTESO_GAS"));
					//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
				campiDaSalvare.add("ID R2D");
				campiDaSalvare.add("Stato pratica");
				campiDaSalvare.add("Distributore");
				//Salvare eventualmente stato indicizzato
				verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);
				prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS",prop.getProperty("DISTRIBUTORE"));
				
				//Verifico che lo stato iniziale della pratica può essere AW o AA
				if(prop.getProperty("STATO_PRATICA").compareToIgnoreCase("AW")!=0 && prop.getProperty("STATO_PRATICA").compareToIgnoreCase("AA")!=0){
					throw new Exception("Lo stato iniziale della pratica non è corretto:"+prop.getProperty("STATO_PRATICA")+". Stato pratica atteso AW o AA");
				}
	
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
