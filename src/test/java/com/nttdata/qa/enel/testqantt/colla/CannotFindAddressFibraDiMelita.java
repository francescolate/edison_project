package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FibraDiMelitaComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CannotFindAddressFibraDiMelita {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			HamburgerMenuComponent hamburger = new HamburgerMenuComponent(driver);
			hamburger.clickComponent(hamburger.fibraDiMelitaLink);

			FibraDiMelitaComponent fibra = new FibraDiMelitaComponent(driver);
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Start");
			fibra.checkHeaderPathLinks(fibra.pathLinks1);
			fibra.verifyComponentExistence(fibra.headerTitle);
			fibra.verifyComponentExistence(fibra.headerSubTitle);
			fibra.checkNavBarLinks();
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Completed");

			logger.write("Voce di Puntamento Verifica Copertura e check campi - Start");
			fibra.clickComponent(fibra.verificaCoperturaAnchor);
			// fibra.clickComponent(fibra.closeChatHeading);
			fibra.verifyComponentExistence(fibra.comuneField);
			fibra.verifyComponentExistence(fibra.indirizzoField);
			fibra.verifyComponentExistence(fibra.civicoField);
			fibra.verifyComponentExistence(fibra.verificaLaCoperturaButton);
			fibra.verifyComponentExistence(fibra.nonTroviIlTuoIndirizzoLink);
			logger.write("Voce di Puntamento Verifica Copertura e check campi - Completed");

			logger.write("Click sul link Non trovi il tuo indirizzo? - Start");
			fibra.clickComponent(fibra.nonTroviIlTuoIndirizzoLink);
			fibra.verifyComponentExistence(fibra.headerTitle_2);
			fibra.verifyComponentExistence(fibra.headerSubTitleKO);
			fibra.checkElementWithText(fibra.mainContactFiberLabel, "Restiamo in contatto!");
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Nome*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Cognome*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Codice Fiscale*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Telefono Cellulare*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Email*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Comune*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Indirizzo*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Civico*")));
			fibra.verifyComponentExistence(By.xpath(fibra.genericLabel.replace("#", "Ho preso visione")));
			fibra.verifyComponentExistence(By.xpath(fibra.genericLabel.replace("#", "testo marketing")));
			fibra.verifyComponentExistence(fibra.proseguiButton);
			fibra.verifyComponentExistence(fibra.tornaHomeButton);
			logger.write("Click sul link Non trovi il tuo indirizzo? - Completed");

			logger.write("Popolamento campi obbligatori e click su Prosegui - Start");
			fibra.inputField(By.xpath(fibra.inputFibra.replace("#", "Nome*")), prop.getProperty("NOME"));
			fibra.inputField(By.xpath(fibra.inputFibra.replace("#", "Cognome*")), prop.getProperty("COGNOME"));
			fibra.inputField(By.xpath(fibra.inputFibra.replace("#", "Codice Fiscale*")),
					prop.getProperty("CODICE_FISCALE"));
			fibra.inputField(By.xpath(fibra.inputFibra.replace("#", "Telefono Cellulare*")),
					prop.getProperty("CELLULARE"));
			fibra.inputField(By.xpath(fibra.inputFibra.replace("#", "Email*")), prop.getProperty("EMAIL"));
			fibra.sendKeyAndSelectValue(By.xpath(fibra.inputFibra.replace("#", "Comune*")), prop.getProperty("COMUNE"));
			fibra.sendKeyAndSelectValue(By.xpath(fibra.inputFibra.replace("#", "Indirizzo*")),
					prop.getProperty("INDIRIZZO"));
			fibra.sendKeyAndSelectValue(By.xpath(fibra.inputFibra.replace("#", "Civico*")), prop.getProperty("CIVICO"));
			fibra.clickComponent(By.xpath(fibra.genericLabel.replace("#", "Ho preso visione")));
			fibra.clickComponent(By.xpath(fibra.genericLabel.replace("#", "testo marketing")));
			fibra.clickComponent(fibra.proseguiButton);
			logger.write("Popolamento campi obbligatori e click su Prosegui - Completed");

			logger.write("Verifica ricezione dati e messaggio ricontatto cliente - Start");
			fibra.checkElementWithText(fibra.modalMainText, "Grazie, abbiamo ricevuto i tuoi dati");
			fibra.checkElementWithText(fibra.modalSecondaryText,
					"Sarai ricontattato appena il servizio fibra di Melita sarà disponibile presso il tuo indirizzo di fornitura.");
			logger.write("Verifica ricezione dati e messaggio ricontatto cliente - Start");

			logger.write("Ritorno in HomePage - Start");
			fibra.clickComponent(fibra.modalTornaAllaHomePageButton);
			fibra.checkUrl(prop.getProperty("LINK"));
			logger.write("Ritorno in HomePage - Completed");

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
