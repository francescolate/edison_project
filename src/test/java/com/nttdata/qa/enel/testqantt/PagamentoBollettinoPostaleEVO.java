

	package com.nttdata.qa.enel.testqantt;

	import java.io.FileOutputStream;
	import java.io.PrintWriter;
	import java.io.StringWriter;
	import java.util.Properties;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.remote.RemoteWebDriver;

	import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
	import com.nttdata.qa.enel.util.QANTTLogger;
	import com.nttdata.qa.enel.util.WebDriverManager;

	import io.qameta.allure.Step;

	public class PagamentoBollettinoPostaleEVO {

		@Step("Pagamento Bollettino Postale")
		public static void main(String[] args) throws Exception {

			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					SceltaMetodoPagamentoComponentEVO sceltaMetodoPagamentoComponentEVO = new SceltaMetodoPagamentoComponentEVO(
							driver);
					By bollettinoPostale;
					By buttonAvanti;

					// Attribuzione path per Bollettino postale
					//			if(prop.getProperty("SUBENTRO", "N").equals("N")) bollettinoPostale = sceltaMetodoPagamentoComponent.bollettinoPostale;
					//			else bollettinoPostale = sceltaMetodoPagamentoComponent.bollettinoPostale2;
					//
					//			if (prop.getProperty("VOLTURA_SENZA_ACCOLLO", "N").equals("Y")) bollettinoPostale = sceltaMetodoPagamentoComponent.bollettinoPostale3;
					String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE");	
					
					//if (prop.getProperty("SUBENTRO", "N").equals("Y"))
					if (tipoOperazione.equals("SUBENTRO")||tipoOperazione.equals("SUBENTRO_EVO")) 
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale2;
					else if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")) 
					//else if (prop.getProperty("VOLTURA_SENZA_ACCOLLO", "N").equals("Y"))
						//					bollettinoPostale = sceltaMetodoPagamentoComponent.bollettinoPostale3;
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale4;
					//else if (prop.getProperty("ALLACCIO", "N").equals("Y"))
					else if (tipoOperazione.equals("ALLACCIO")) 
						//					bollettinoPostale = sceltaMetodoPagamentoComponent.bollettinoPostale3;
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale6;
					//else if (prop.getProperty("VOLTURA_CON_ACCOLLO", "N").equals("Y"))
					else if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")) 
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale;
					//Controllo inserito da Tommaso Capuano in data 05/06/2019 per gestire bollettino postale per SWA residenziale
					//else if (prop.getProperty("SWITCH_ATTIVO", "N").equals("Y"))
					else if (tipoOperazione.equals("SWITCH_ATTIVO") || tipoOperazione.equals("SWITCH_ATTIVO_FAST") || tipoOperazione.equals("SWITCH_ATTIVO_EVO") || tipoOperazione.contentEquals("PRIMA_ATTIVAZIONE_EVO")||(tipoOperazione.equals("ALLACCIO_EVO"))) 
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale4;
					
					//Controllo inserito da Tommaso Capuano in data 07/06/2019 per gestire bollettino postale per prima attivazione
					//else if (prop.getProperty("PRIMA_ATTIVAZIONE", "N").equals("Y"))
					else if (tipoOperazione.equals("PRIMA_ATTIVAZIONE")) 
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale4;
					else
						bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale;
					logger.write("Attribuzione path per Bollettino Postale");

					// Attribuzione path per Button avanti

					//			if(prop.getProperty("SWITCH_ATTIVO","N").equals("N")) {
					//				buttonAvanti = sceltaMetodoPagamentoComponent.checkPayMetod;        
					//			}
					//			else {
					//				buttonAvanti = sceltaMetodoPagamentoComponent.avantiPaymentMethod;
					//			}
					//
					//			if (prop.getProperty("VOLTURA_SENZA_ACCOLLO", "N").equals("Y")) buttonAvanti=sceltaMetodoPagamentoComponent.buttonAvanti;

					//if (prop.getProperty("SWITCH_ATTIVO", "N").equals("Y")) {
					if (tipoOperazione.equals("SWITCH_ATTIVO")||tipoOperazione.equals("SWITCH_ATTIVO_FAST") ) {
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.avantiPaymentMethod;
					} //else if (prop.getProperty("VOLTURA_SENZA_ACCOLLO", "N").equals("Y"))
					else if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")) 
						//					buttonAvanti = sceltaMetodoPagamentoComponent.buttonAvanti;
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.checkPayMetod;
					//else if (prop.getProperty("ALLACCIO", "N").equals("Y"))
					else if (tipoOperazione.equals("ALLACCIO")) 
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonAvanti;
					//else if (prop.getProperty("VOLTURA_CON_ACCOLLO", "N").equals("Y"))
					else if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")) 
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonAvantiVolturaAccollo;
					else if(tipoOperazione.contentEquals("SWITCH_ATTIVO_EVO") || tipoOperazione.contentEquals("PRIMA_ATTIVAZIONE_EVO")||(tipoOperazione.equals("ALLACCIO_EVO"))) {
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonConferma2;
					}
					else
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.checkPayMetod;
					logger.write("Attribuzione path per Button Avanti");

					// Selezione metodo di pagamento

					//				if (prop.getProperty("SWITCH_ATTIVO", "N").equals("Y")
					//						&& prop.getProperty("TIPOCLIENTE", "RESIDENZIALE").equals("BUSINESS")) {
					//					sceltaMetodoPagamentoComponent.selezionaBollettinoPagamento2(prop.getProperty("INTERACTIONFRAME"),
					//							bollettinoPostale, sceltaMetodoPagamentoComponent.depositoCauzionaleBtn3, buttonAvanti,
					//							true);}
					//				//Controllo inserito da Tommaso Capuano in data 05/06/2019 per gestire bollettino postale per SWA residenziale
					//				else if (prop.getProperty("SWITCH_ATTIVO", "N").equals("Y")
					//						&& prop.getProperty("TIPOCLIENTE", "RESIDENZIALE").equals("RESIDENZIALE")) {
					//					sceltaMetodoPagamentoComponent.selezionaBollettinoPagamento2(prop.getProperty("INTERACTIONFRAME"),
					//							bollettinoPostale, sceltaMetodoPagamentoComponent.depositoCauzionaleBtn3, buttonAvanti,
					//							true);
					//				}
					//if (prop.getProperty("SWITCH_ATTIVO", "N").equals("Y")) {
					if (tipoOperazione.equals("SWITCH_ATTIVO")||tipoOperazione.equals("SWITCH_ATTIVO_FAST") ) {
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento2(prop.getProperty("INTERACTIONFRAME"),
								bollettinoPostale, sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn3, buttonAvanti,
								true);
					}
					else if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")) {
					//else if (prop.getProperty("VOLTURA_SENZA_ACCOLLO", "N").equals("Y")) {
						//					sceltaMetodoPagamentoComponent.selezionaBollettinoPagamento(prop.getProperty("INTERACTIONFRAME"),
						//							bollettinoPostale, "checked", "checked",
						//							sceltaMetodoPagamentoComponent.chiudiModalMetodoPagamento2, buttonAvanti, true);
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento2(prop.getProperty("INTERACTIONFRAME"),
								bollettinoPostale, sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn3, buttonAvanti,
								true);
					}
					//else if (prop.getProperty("PRIMA_ATTIVAZIONE", "N").equals("Y")){
					else if (tipoOperazione.equals("PRIMA_ATTIVAZIONE")) { 
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento2(prop.getProperty("INTERACTIONFRAME"),
								bollettinoPostale, sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn3, buttonAvanti,true);
					}
					//else if (prop.getProperty("VOLTURA_CON_ACCOLLO", "N").equals("Y")) {
					else if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento(prop.getProperty("INTERACTIONFRAME"),
								bollettinoPostale, "checked", "checked",
								sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn, buttonAvanti, false);
					}

					//else if (prop.getProperty("ALLACCIO", "N").equals("Y")) {
					else if (tipoOperazione.equals("ALLACCIO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento(bollettinoPostale, "checked", "checked",
								sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn3, buttonAvanti, true);
					}
					
					else if (tipoOperazione.equals("SWITCH_ATTIVO_EVO")||(tipoOperazione.equals("ALLACCIO_EVO"))) {
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamentoConRicerca(bollettinoPostale,sceltaMetodoPagamentoComponentEVO.campoRicerca, "checked", "checked",
								sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn3, buttonAvanti, true);
					}
					
					else if (tipoOperazione.contentEquals("PRIMA_ATTIVAZIONE_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento(bollettinoPostale, "checked", "checked",
						sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn3, buttonAvanti, true);
					}
					else {
						sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento(prop.getProperty("INTERACTIONFRAME"),
								bollettinoPostale, sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn, buttonAvanti,
								true);
					}
					logger.write("Selezione metodo di pagamento");
			
				}
				prop.setProperty("RETURN_VALUE", "OK");
			}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}
	}
