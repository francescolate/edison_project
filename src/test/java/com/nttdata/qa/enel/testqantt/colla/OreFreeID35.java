package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeID35 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			logger.write("verifica presenza dell'offerta 'Ore Free' - Start");
			OreFreeComponent offerta=new OreFreeComponent(driver);

			By offerta_orefree=offerta.offertaOreFree;
			offerta.verifyComponentExistence(offerta_orefree);
			offerta.scrollComponent(offerta_orefree);
			logger.write("verifica presenza dell'offerta 'Ore Free' - Completed");

			logger.write("Click sul Button Dettaglio Offerta'- Start");

			By dettagliofferta_button=offerta.dettaglioffertaButton;
			offerta.verifyComponentExistence(dettagliofferta_button);
			offerta.clickComponent(offerta_orefree);

			logger.write("Click sul Button Dettaglio Offerta'- Completed");

			logger.write("Click sul Button Verifica e Aderisci'- Start");

			By verificaeaderisci_button=offerta.verificaaderisciVerde;
			offerta.verifyComponentExistence(verificaeaderisci_button);
			offerta.clickComponent(verificaeaderisci_button);

			logger.write("Click sul Button Verifica e Aderisci'- Completed");
			
			logger.write("Check sulla popup - Start");
			
			By inserisci_NumeroPod=offerta.inserisciNumeroPod;
			offerta.verifyComponentExistence(inserisci_NumeroPod);
			
			By testo_popup=offerta.testopopup;
			offerta.verifyComponentExistence(testo_popup);
			
			By campo_CodicePod=offerta.campoCodicePod;
			offerta.verifyComponentExistence(campo_CodicePod);
			
			By conferma_Button=offerta.confermaButton;
			offerta.verifyComponentExistence(conferma_Button);
			
		//	By link_pod=offerta.linkpod;
		//	offerta.verifyComponentExistence(link_pod);
			offerta.replaceLinkCollaInXpathAndCheckExistObject(prop.getProperty("LINK"));
			logger.write("Check sulla popup - Completed");
			
			logger.write("effettuare il back del browser e check sul motorino di ricerca - Start");
			driver.navigate().back();
			
			TimeUnit.SECONDS.sleep(4);
			
			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkLUCE_E_GAS_FILTRO);
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);			
			By searchMenu=headerMenu.searchSection;
			headerMenu.verifyQuestionsExistence(searchMenu);
			By precedentcontract=headerMenu.contractListvalue;
			headerMenu.verifyTextSearchMenu(precedentcontract,prop.getProperty("TYPE_OF_CONTRACT"));
			
			By precedentplace=headerMenu.placeListvalue;
			headerMenu.verifyTextSearchMenu(precedentplace,prop.getProperty("PLACE"));
			
			By precedentneed=headerMenu.Listvalue;
			headerMenu.verifyTextSearchMenu(precedentneed,prop.getProperty("NEED_PRECEDENTE"));
			logger.write("effettuare il back del browser e check sul motorino di ricerca - Completed");
			
			logger.write("Click sul Link VERIFICA DISPONIBITA'- Start");
			By verificadisponibilita_Button=offerta.verificadisponibilitaButton;
			offerta.verifyComponentExistence(verificadisponibilita_Button);
			offerta.scrollComponent(verificadisponibilita_Button);
			TimeUnit.SECONDS.sleep(3);
			offerta.clickComponent(verificadisponibilita_Button);

			logger.write("Click sul Link VERIFICA DISPONIBITA' - Completed");

			logger.write("Click su CONFERMA inserendo un valore configurato nel campo POD  - Start");
			TimeUnit.SECONDS.sleep(2);

			offerta.enterPodValue(campo_CodicePod, prop.getProperty("CODICE_POD"));

			offerta.clickComponent(conferma_Button);

			logger.write("Click su CONFERMA inserendo un valore configurato nel campo POD  - Completed");
			logger.write("Check alert popup - Start");
			
			By testo_titolo_error_popup=offerta.titolopopup2;
			offerta.verifyComponentExistence(testo_titolo_error_popup);
			
			By testo_error_popup=offerta.testopopup2;
			offerta.verifyComponentExistence(testo_error_popup);

			logger.write("Check alert popup - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

			//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
