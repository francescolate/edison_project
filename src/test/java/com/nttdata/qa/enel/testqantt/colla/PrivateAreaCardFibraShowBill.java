package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.commons.collections.iterators.CollatingIterator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaCardFibraShowBill {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);

			By card_fibra_active = paf.card_fibra_active;

			paf.verifyComponentExistence(card_fibra_active);
			paf.verifyComponentVisibility(card_fibra_active);

			//STEP 7
			logger.write("click the button for showing bill in the card with FIBRA offer - START");

			By button_show_bill_card_fibra = paf.button_show_bill_card_fibra;
			
			paf.verifyComponentExistence(button_show_bill_card_fibra);		
			paf.clickComponent(button_show_bill_card_fibra);
			
			logger.write("click the button for showing bill in the card with FIBRA offer - COMPLETED");

			logger.write("check the popup title text in popup - START");

			By title_popup_fibra_card = paf.title_popup_fibra_card;

			paf.verifyComponentExistence(title_popup_fibra_card);
			paf.verifyComponentVisibility(title_popup_fibra_card);
			paf.verifyComponentText(title_popup_fibra_card, "Visualizza importi FIBRA DI MELITA");
			
			logger.write("check the popup title text in popup - COMPLETED");
			
			logger.write("check the popup desc text in popup - START");

			By desc_popup_fibra_card = paf.desc_popup_fibra_card;

			paf.verifyComponentExistence(desc_popup_fibra_card);
			paf.verifyComponentVisibility(desc_popup_fibra_card);
			paf.verifyComponentText(desc_popup_fibra_card, "Il canone dell'offerta FIBRA DI MELITA è inserito nelle bollette della fornitura di Enel Energia a cui è abbinata. Proseguendo troverai le bollette di quella fornitura. Scarica i PDF delle bollette di Enel Energia per individuare la voce specifica");		
			
			logger.write("check the popup desc text in popup - COMPLETED");
			
			logger.write("click the button decline in popup - START");

			By button_decline_popup_fibra_card = paf.button_decline_popup_fibra_card;

			paf.verifyComponentExistence(button_decline_popup_fibra_card);
			paf.clickComponent(button_decline_popup_fibra_card);
			
			logger.write("click the button decline in popup - COMPLETED");
			
			//STEP 8			
			logger.write("click the button for showing bill in the card with FIBRA offer - START");
			
			paf.verifyComponentExistence(button_show_bill_card_fibra);		
			paf.clickComponent(button_show_bill_card_fibra);
			
			logger.write("click the button for showing bill in the card with FIBRA offer - COMPLETED");
			
			logger.write("click the button next in popup - START");

			By button_next_popup_fibra_card = paf.button_next_popup_fibra_card;

			paf.verifyComponentExistence(button_next_popup_fibra_card);
			paf.clickComponent(button_next_popup_fibra_card);
			
			logger.write("click the button next in popup - COMPLETED");


			//			By popup_x = paf.popup_x;
			//			
			//			By x_popup_fibra_card = new ByChained(popup_fibra_card, popup_x);
			//			
			//			paf.verifyComponentExistence(x_popup_fibra_card);
			//			paf.clickComponent(x_popup_fibra_card);

			//da risolvere
			//			By button_show_details = By.xpath(".//*[contains(@class,'button-container')]//*[contains(text(),\"Dettaglio Offerta\")]"); 
			//			
			//			By button_show_detais_card_fibra = new ByChained(card_fibra_active, button_show_details);
			//			
			//			paf.clickComponent(button_show_detais_card_fibra);


			logger.write("check the card with FIBRA offer state active - COMPLETED");

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

			//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
