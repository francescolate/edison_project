package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.ResCutomerProfileComponent;
import com.nttdata.qa.enel.components.colla.StatoRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Modifica_Password_ACR_377 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			/*logger.write("Correct visualization of the Home Page with central text -- Starts");	
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");*/
	
			logger.write("Verify the home page logo  - Starts");
			ResCutomerProfileComponent rcp = new ResCutomerProfileComponent(driver);
			rcp.verifyComponentExistence(rcp.custHomePagelogo);
			logger.write("Verify the home page logo  - Ends");
			
			logger.write("Click on Dati di Registrazione link - Starts");
			HomeComponent hc = new HomeComponent(driver);
			hc.verifyComponentExistence(hc.downArrow);
			hc.clickComponent(hc.downArrow); 
			hc.verifyComponentExistence(hc.editProfile);
			hc.clickComponent(hc.editProfile);
			logger.write("Click on Dati di Registrazione link - Ends");
			
			logger.write("Click on Modifica password link - Starts");
			rcp.verifyComponentExistence(rcp.modificaPassowrdLink);
			rcp.clickComponent(rcp.modificaPassowrdLink);
			logger.write("Click on Modifica password link - Ends");
			
			logger.write("Verify Modifica password header and contents - Starts");
			rcp.verifyComponentExistence(rcp.modificaPasswdHeader);
			rcp.comprareText(rcp.modificaPasswdHeader, ResCutomerProfileComponent.MODIFICAPASSWD_HEADER, true);
			rcp.verifyComponentExistence(rcp.modificaPasswdSubHeader);
			rcp.comprareText(rcp.modificaPasswdSubHeader, ResCutomerProfileComponent.MODIFICAPASSWDSUB_HEADER, true);
			logger.write("Verify Modifica password header and contents - Ends");
			
			logger.write("Verify the value fields  - Starts");
			rcp.verifyComponentExistence(rcp.nuovaPasswordLabel);
			rcp.verifyComponentExistence(rcp.nuovaPasswordInput);
			rcp.verifyComponentExistence(rcp.confermaPasswordLabel);
			rcp.verifyComponentExistence(rcp.confermaPasswordInput);
			rcp.verifyComponentExistence(rcp.confermaChangePasswdBtn);
			rcp.verifyComponentExistence(rcp.passwordCampiValue);
			rcp.comprareText(rcp.passwordCampiValue, ResCutomerProfileComponent.CAMPI_TEXT, true);
			rcp.verifyComponentExistence(rcp.nuovaPasswordValue);
			rcp.comprareText(rcp.nuovaPasswordValue, ResCutomerProfileComponent.NUOVAPASSWD_VALUE, true);
			logger.write("Verify the value fields  - Ends");
			
			logger.write("Verify the value fields  - Starts");
			rcp.enterValueInInputBox(rcp.nuovaPasswordInput, prop.getProperty("NuovaValue"));
			rcp.enterValueInInputBox(rcp.confermaPasswordInput, prop.getProperty("ConfermaValue"));
			rcp.clickComponent(rcp.confermaChangePasswdBtn);
			rcp.verifyComponentExistence(rcp.errorMsg);
			rcp.comprareText(rcp.errorMsg, ResCutomerProfileComponent.ERRORMSG_VALUE, true);
			logger.write("Verify the value fields  - Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
