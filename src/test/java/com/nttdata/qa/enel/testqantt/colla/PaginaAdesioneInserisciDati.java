package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CalcolaCFPopupComponent;
import com.nttdata.qa.enel.components.colla.PaginaAdesioneInserisciDatiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PaginaAdesioneInserisciDati {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            
			logger.write("sulla pagina di adesione dell'offerta 'Ore Free' nella sezione 'Inserisci Dati' premere prosegui senza aver popolato i campi :Nome;Cognome;Codice Fiscale;Telefono Cellulare; Email;Conferma Email;Check box 'Ho preso visione dell'informativa sulla privacy - Start");
			PaginaAdesioneInserisciDatiComponent inserisci=new PaginaAdesioneInserisciDatiComponent(driver);
			By frameInserisciDati=inserisci.frame;
			inserisci.setframe(frameInserisciDati);
			
			By campo_Nome=inserisci.campoNome;
			inserisci.verifyComponentExistence(campo_Nome);
			
			By campo_Cognome=inserisci.campoCognome;
			inserisci.verifyComponentExistence(campo_Cognome);
			
			By campo_Codicefiscale=inserisci.campoCodicefiscale;
			inserisci.verifyComponentExistence(campo_Codicefiscale);
			
			By campo_Telefonocellulare=inserisci.campoTelefonocellulare;
			inserisci.verifyComponentExistence(campo_Telefonocellulare);
			
			By campo_Email=inserisci.campoEmail;
			inserisci.verifyComponentExistence(campo_Email);
			
			By campo_ConfermaEmail=inserisci.campoConfermaEmail;
			inserisci.verifyComponentExistence(campo_ConfermaEmail);
			
		    By checkbox_Privacy=inserisci.checkboxPrivacy;
		    inserisci.verifyComponentExistence(checkbox_Privacy);
			
			By prosegui_Button=inserisci.proseguiButton;
			inserisci.verifyComponentExistence(prosegui_Button);
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.checkErrorLabelCampoObbligatorio(inserisci.labelErrorCampoObbligatorio,inserisci.campoObbl);
			
			logger.write("sulla pagina di adesione dell'offerta 'Ore Free' nella sezione 'Inserisci Dati' premere prosegui senza aver popolato i campi :Nome;Cognome;Codice Fiscale;Telefono Cellulare; Email;Conferma Email;Check box 'Ho preso visione dell'informativa sulla privacy - Completed");
			
			logger.write("Popolare il campo Nome con caratteri speciali - Start");
			inserisci.enterValueInInputBox(inserisci.campoNomeError, prop.getProperty("CARATTERI_SPECIALI"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(inserisci.formatoNonCorrettoNome);
			inserisci.scrollComponent(inserisci.formatoNonCorrettoNome);
			logger.write("Popolare il campo Nome con caratteri speciali - Completed");
			
			logger.write("Popolare il campo nome con una o piu' lettere e il campo cognome con caratteri speciali - Start");
			inserisci.enterValueInInputBox(inserisci.campoNomeError, prop.getProperty("NOME"));
			TimeUnit.SECONDS.sleep(1);
			inserisci.enterValueInInputBox(inserisci.campoCognomeError, prop.getProperty("CARATTERI_SPECIALI"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(inserisci.formatoNonCorrettoCognome);
			inserisci.scrollComponent(inserisci.formatoNonCorrettoCognome);
			logger.write("Popolare il campo nome con una o piu' lettere e il campo cognome con caratteri speciali - Completed");
			
			logger.write("Popolare il campo  cognome e inserire un valore errato nel campo codice fiscale - Start");
			TimeUnit.SECONDS.sleep(1);
			inserisci.enterValueInInputBox(inserisci.campoCognomeError, prop.getProperty("COGNOME"));
			TimeUnit.SECONDS.sleep(1);
		    inserisci.enterValueInInputBox(inserisci.campoCodicefiscaleError,prop.getProperty("CARATTERI_SPECIALI"));
						
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(inserisci.formatoNonCorrettoCF);
			inserisci.scrollComponent(inserisci.formatoNonCorrettoCF);
			logger.write("Popolare il campo  cognome e inserire un valore errato nel campo codice fiscale - Completed");
			
		/*	logger.write("Premere su 'calcola codice fiscale', visualizzare una modale con alcuni campi e premere la CTA 'Calcola' senza aver popolato alcun campo - Start");
			By calcolalo_Ora=inserisci.calcolaloOra;
			inserisci.verifyComponentExistence(calcolalo_Ora);
			inserisci.clickComponent(calcolalo_Ora);
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(1);
			
			CalcolaCFPopupComponent cf=new CalcolaCFPopupComponent(driver);
			By nome_Popup=cf.nomePopup;
			cf.verifyComponentExistence(nome_Popup);
			
			By cognome_Popup=cf.cognomePopup;
			cf.verifyComponentExistence(cognome_Popup);
			
			By datadinascita_labelPopup=cf.datadinascitalabelPopup;
			cf.verifyComponentExistence(datadinascita_labelPopup);
			
			By giorno_Popup=cf.giornoPopup;
			cf.verifyComponentExistence(giorno_Popup);
			
			By mese_Popup=cf.mesePopup;
			cf.verifyComponentExistence(mese_Popup);
			
			By anno_Popup=cf.annoPopup;
			cf.verifyComponentExistence(anno_Popup);
			
			By donna_CheckBox=cf.donnaCheckBoxPopup;
			cf.verifyComponentExistence(donna_CheckBox);
			By uomo_CheckBox=cf.uomoCheckBoxPopup;
			cf.verifyComponentExistence(uomo_CheckBox);
			
			By si_CheckBoxPopup=cf.siCheckBoxPopup;
			cf.verifyComponentExistence(si_CheckBoxPopup);
			By no_CheckBoxPopup=cf.noCheckBoxPopup;
			cf.verifyComponentExistence(no_CheckBoxPopup);
			
			By button_Calcola=cf.buttonCalcola;
			cf.verifyComponentExistence(button_Calcola);
			cf.click(button_Calcola);
			
			
			By labelCampoObbligatorio_Giorno=cf.labelCampoObbligatorioGiorno;
			cf.verifyComponentExistence(labelCampoObbligatorio_Giorno);
			By labelCampoObbligatorio_Mese=cf.labelCampoObbligatorioMese;
			cf.verifyComponentExistence(labelCampoObbligatorio_Mese);
			By labelCampoObbligatorio_Anno=cf.labelCampoObbligatorioAnno;
			cf.verifyComponentExistence(labelCampoObbligatorio_Anno);
			By labelCampoObbligatorioSesso=cf.labelCampoObbligatorioSesso;
			cf.verifyComponentExistence(labelCampoObbligatorioSesso);
			By labelCampoObbligatorioItalia=cf.labelCampoObbligatorioItalia;
			cf.verifyComponentExistence(labelCampoObbligatorioItalia);
			logger.write("Premere su 'calcola codice fiscale', visualizzare una modale con alcuni campi e premere la CTA 'Calcola' senza aver popolato alcun campo - Completed");
			
			logger.write("Popolare il campo nome con caratteri speciali - Start");
			cf.inserisciValue(nome_Popup, prop.getProperty("CARATTERI_SPECIALI"));
			cf.click(button_Calcola);
			TimeUnit.SECONDS.sleep(1);
			By nome_NonCorretto=cf.nomeNonCorretto;
			cf.verifyComponentExistence(nome_NonCorretto);
			logger.write("Popolare il campo nome con caratteri speciali - Completed");
			
			logger.write("Popolare il campo nome con una o piu' lettere e il campo cognome con caratteri speciali - Start");
			cf.inserisciValue(cf.nomePopupError, prop.getProperty("NOME"));
			TimeUnit.SECONDS.sleep(1);
			cf.inserisciValue(cognome_Popup, prop.getProperty("CARATTERI_SPECIALI"));
			cf.click(button_Calcola);
			TimeUnit.SECONDS.sleep(1);
			By cognome_NonCorretto=cf.cognomeNonCorretto;
			cf.verifyComponentExistence(cognome_NonCorretto);
			logger.write("Popolare il campo nome con una o piu' lettere e il campo cognome con caratteri speciali - Completed");
			
			logger.write("Popolare campi:Data,sesso,sei nato in - Start");
			
			if(prop.getProperty("SESSO").contentEquals("UOMO")) cf.click(uomo_CheckBox);
			else cf.click(donna_CheckBox);
			
			if(prop.getProperty("NAZIONE").contentEquals("ITALIA")) cf.click(si_CheckBoxPopup);
			else cf.click(no_CheckBoxPopup);
			
			
		// inserisci data 	
		//	cf.inserisciData(prop.getProperty("DATA_NASCITA"), button_Calcola );
			cf.click(button_Calcola);
			
			By comune_Obbligatorio=cf.comuneObbligatorio;
			cf.verifyComponentExistence(comune_Obbligatorio);
			
			logger.write("Popolare campi:Data,sesso,sei nato in - Completed");
			
			logger.write("Popolare tutti i campi restanti e premere Calcola- Start");
			cf.inserisciValue(cf.cognomePopupError, prop.getProperty("COGNOME"));
			TimeUnit.SECONDS.sleep(1);
			cf.inserisciValue(cf.inputComune, prop.getProperty("CITTA"));
			TimeUnit.SECONDS.sleep(1);
			cf.inserisciComuneDiNascita(prop.getProperty("CITTA"));
		//	cf.click(button_Calcola);
			cf.clickeaspetta(button_Calcola);
			logger.write("Popolare tutti i campi restanti e premere Calcola- Completed");*/
			
		//	inserisci.setframe(frameInserisciDati);
		//	inserisci.verifyNotComponentExistence(inserisci.formatoNonCorrettoCF);
			
			inserisci.enterValueInInputBox(inserisci.campoCodicefiscaleError,"RSSMRA02C02D086U");
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyNotComponentExistence(inserisci.formatoNonCorrettoCF);
			
			
			logger.write("Popolare il campo cellulare, popolare il campo email senza @ - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulareError,prop.getProperty("CELLULARE"));
			inserisci.enterValueInInputBox(inserisci.campoEmailError,prop.getProperty("EMAIL_SENZA_@"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			By campoEmailFormatoNon_Corretto=inserisci.campoEmailFormatoNonCorretto;
			inserisci.verifyComponentExistence(campoEmailFormatoNon_Corretto);
			logger.write("Popolare il campo cellulare, popolare il campo email senza @ - Completed");
			
			
			logger.write("Popolare il campo cellulare, popolare il campo email senza (.com, .it . Net, ecc. ecc.) - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulare,prop.getProperty("CELLULARE"));
			inserisci.enterValueInInputBox(inserisci.campoEmailError,prop.getProperty("EMAIL_SENZA_IT"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(campoEmailFormatoNon_Corretto);
			logger.write("Popolare il campo cellulare, popolare il campo email senza (.com, .it . Net, ecc. ecc.) - Completed");
			
			logger.write("Popolare il campo cellulare e non il campo email - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulare,prop.getProperty("CELLULARE"));
			inserisci.enterValueInInputBox(inserisci.campoEmailError,prop.getProperty("EMAIL_VUOTA"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			By campoEmail_Obbligatoria=inserisci.campoEmailObbligatoria;
			inserisci.verifyComponentExistence(campoEmail_Obbligatoria);
			logger.write("Popolare il campo cellulare e non il campo email - Completed");
			
			logger.write("Popolare il campo cellulare, popolare il campo email senza @ - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulare,prop.getProperty("CELLULARE"));
			inserisci.enterValueInInputBox(inserisci.campoEmailError,prop.getProperty("EMAIL_SENZA_@"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(campoEmailFormatoNon_Corretto);
			logger.write("Popolare il campo cellulare, popolare il campo email senza @ - Completed");
			
			
			logger.write("Popolare il campo cellulare, popolare il campo email senza (.com, .it . Net, ecc. ecc.) - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulare,prop.getProperty("CELLULARE"));
			inserisci.enterValueInInputBox(inserisci.campoEmailError,prop.getProperty("EMAIL_SENZA_IT"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(campoEmailFormatoNon_Corretto);
			logger.write("Popolare il campo cellulare, popolare il campo email senza (.com, .it . Net, ecc. ecc.) - Completed");
			
			logger.write("Popolare il campo Email e Conferma Email ma non popolare il campo Cellulare e premere Prosegui - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulare,prop.getProperty("CELLULARE_VUOTO"));
			inserisci.enterValueInInputBox(inserisci.campoEmailError,prop.getProperty("EMAIL"));
			inserisci.enterValueInInputBox(inserisci.campoConfermaEmailError,prop.getProperty("EMAIL"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			By campoTelefonocellulare_Obbligatorio=inserisci.campoTelefonocellulareObbligatorio;
			inserisci.verifyComponentExistence(campoTelefonocellulare_Obbligatorio);
			logger.write("Popolare il campo Email e Conferma Email ma non popolare il campo Cellulare e premere Prosegui - Completed");
			
			
			logger.write("Popolare il campo Email e Conferma Email, popolare il campo Cellulare con 8 caratteri numerici e premere Prosegui - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulareError,prop.getProperty("CELLULARE_8_CARATTERI"));
			inserisci.enterValueInInputBox(inserisci.campoEmail,prop.getProperty("EMAIL"));
			inserisci.enterValueInInputBox(inserisci.campoConfermaEmail,prop.getProperty("EMAIL"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			By campoTelefonocellulare_FormatoNonCorretto=inserisci.campoTelefonocellulareFormatoNonCorretto;
			inserisci.verifyComponentExistence(campoTelefonocellulare_FormatoNonCorretto);
			logger.write("Popolare il campo Email e Conferma Email, popolare il campo Cellulare con 8 caratteri numerici e premere Prosegui - Completed");
			
			logger.write("Popolare il campo Email e Conferma Email, popolare il campo Cellulare con lettere e premere Prosegui - Start");
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulareError,prop.getProperty("CELLULARE_CARATTERI"));
			inserisci.enterValueInInputBox(inserisci.campoEmail,prop.getProperty("EMAIL"));
			inserisci.enterValueInInputBox(inserisci.campoConfermaEmail,prop.getProperty("EMAIL"));
			inserisci.click(prosegui_Button);
			TimeUnit.SECONDS.sleep(1);
			inserisci.verifyComponentExistence(campoTelefonocellulare_FormatoNonCorretto);
			logger.write("Popolare il campo Email e Conferma Email, popolare il campo Cellulare con lettere e premere Prosegui - Start");
			
			
			logger.write("Premere il testo Informativa Privacy, cliccare sul pulsante HO PRESO VISIONE, popolare correttamente tutti i campi e premere su PROSEGUI  - Start ");
			By link_Informativa=inserisci.linkInformativa;
			inserisci.verifyComponentExistence(link_Informativa);
			inserisci.clickComponent(link_Informativa);
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(3);
			By titolo_Informativa1=inserisci.titoloInformativaParte1;
			inserisci.verificaTesto(titolo_Informativa1, inserisci.informativa);
			By presa_visone=inserisci.presavisone;
			inserisci.click(presa_visone);
		
			
			inserisci.setframe(frameInserisciDati);
			
			TimeUnit.SECONDS.sleep(1);
			inserisci.enterValueInInputBox(inserisci.campoTelefonocellulareError,prop.getProperty("CELLULARE"));
			TimeUnit.SECONDS.sleep(3);
			inserisci.verifyComponentExistence(inserisci.checkboxInfo);
			
			inserisci.scrollComponent(prosegui_Button);
			inserisci.click(prosegui_Button);
			inserisci.verifyNotComponentExistence(inserisci.checkBoxInformativaError);
			TimeUnit.SECONDS.sleep(1);
			inserisci.scrollComponent(prosegui_Button);
			inserisci.click(prosegui_Button);
			logger.write("Premere il testo Informativa Privacy, cliccare sul pulsante HO PRESO VISIONE, popolare correttamente tutti i campi e premere su PROSEGUI  - Completed ");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
