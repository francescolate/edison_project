package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertagasocrComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyEmailSubjectAndContent116 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String content = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            content = gm.recuperaLink(prop, "ID_116_RES_SUB_GAS_OCR", false);
		            Thread.sleep(5000);
		            System.out.println(content);
					OffertagasocrComponent oc =new OffertagasocrComponent(driver);
					oc.verifyEmailContent(content, oc.EmailContent116Line1);
					oc.verifyEmailContent(content, oc.EmailContent116Line2);
					oc.verifyEmailContent(content, oc.EmailContent116Line3);
					oc.verifyEmailContent(content, oc.EmailContent116Line4);
					oc.verifyEmailContent(content, oc.EmailContent116Line5);
					String launchLink = content.substring(content.indexOf("https://www.enel.it/content")+0, content.indexOf("prochat=n")+9).replace("amp;", "");
					System.out.println(launchLink);
					LoginLogoutEnelCollaComponent log =new LoginLogoutEnelCollaComponent(driver);
					log.launchLink(launchLink);
					oc.verifyComponentExistence(oc.emailConfermaTitle);
					oc.comprareText(oc.emailConfermaContent, oc.EmailConfermaContent, true);
					oc.clickComponent(oc.RICHIEDI_CODICE_DI_VERIFICA);
					Thread.sleep(5000);
					oc.comprareText(oc.otpText, oc.OTPText, true);
					oc.verifyComponentExistence(oc.codiceDiVerifica);
					oc.verifyComponentExistence(oc.verificaCode);					
					
				}
				
				prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
          	StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
