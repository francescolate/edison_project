package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ArticlePageComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FooterPrivacy {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			/* Set Valori da verificare
			 * 
			 */
			String separator = "<;>";
		    String [] legal_links_text = {"Informazioni Legali" , "Credits" , "Privacy" , "Cookie Policy" , "Remit"};
		    prop.setProperty("LEGAL_LINKS_TEXT" , convertArrayToStringWhitSeparator(legal_links_text, separator));
		    prop.setProperty("SOCIAL_ICONS_TYPE" , "aria-label");
		    
		    String [] social_icons_text = {"Twitter" , "Facebook" , "Youtube" , "LinkedIn" , "Instagram" , "Telegram"};
		    prop.setProperty("SOCIAL_ICONS_TEXT" , convertArrayToStringWhitSeparator(social_icons_text, separator));
		 
		    prop.setProperty("PRIVACY_URL" , "it/supporto/faq/privacy");
		    
		    prop.setProperty("PRIVACY_LEGAL_LINK" , "Privacy");
		    prop.setProperty("PRIVACY_PATH_MENU_TEXT" , "HOME / SUPPORTO / PRIVACY");
		    prop.setProperty("PRIVACY_TITLE_MENU_TEXT" , "Privacy");
		    prop.setProperty("PRIVACY_DETAIL_MENU_TEXT" ,"Le società Enel Energia S.p.A. ed Enel Italia S.p.A. (di seguito, complessivamente, le “Società”), in qualità di titolari autonomi del trattamento, informano che i dati personali degli interessati verranno trattati nel rispetto delle disposizioni previste dal REGOLAMENTO UE 2016/679 (“GDPR”) per le finalità e con le modalità indicate nelle rispettive Informative privacy.' not equals to Le società Enel Energia S.p.A. ed Enel Italia S.r.l. (di seguito, complessivamente, le “Società”), in qualità di titolari autonomi del trattamento, informano che i dati personali degli interessati verranno trattati nel rispetto delle disposizioni previste dal REGOLAMENTO UE 2016/679 (“GDPR”) per le finalità e con le modalità indicate nelle rispettive Informative privacy.");
		    
		    prop.setProperty("PRIVACY_TABS_TEXT", "Enel Energia;Enel Italia");
		  //  prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Italia S.p.a.");
		  //  prop.setProperty("AFTER_FOOTER_TEXT", "© Enel Energia S.p.a. - Gruppo Iva P.IVA 12345678901");
		    prop.setProperty("BEFORE_FOOTER_TEXT", "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.");
		    prop.setProperty("AFTER_FOOTER_TEXT", "Gruppo IVA Enel P.IVA 15844561009");
		   
		    
		    String [] privacy_questions_text_1 = {"Titolare e responsabile del trattamento dei dati personali"
		    , "Responsabile della protezione dei dati personali (RPD)"
		    , "Oggetto e modalità del trattamento"
		    , "Finalità e base giuridica del trattamento"
		    , "Registrazione all'area riservata e social login"
		    , "Finalità di marketing e/o profilazione"
		    , "Destinatari dei dati personali"
		    , "Trasferimento dei dati personali"
		    , "Periodo di conservazione dei dati personali"
		    , "Diritti degli interessati"
		    , "Utilizzo dei Cookie"};

		    String [] privacy_questions_text_2 = {"Titolare del trattamento dei Dati Personali"
		    , "Responsabile della Protezione dei dati (RPD)"
		    , "Oggetto e Modalità del trattamento"
		    , "Finalità e base giuridica del trattamento"
		    , "Destinatari dei Dati Personali"
		    , "Trasferimento dei Dati Personali"
		    , "Periodo di conservazione dei Dati Personali"
		    , "Diritti degli interessati"
		    , "Utilizzo dei cookie"};
		    	    
		    prop.setProperty("PRIVACY_QUESTIONS_TEXT_1" , convertArrayToStringWhitSeparator(privacy_questions_text_1, separator));
		    prop.setProperty("PRIVACY_QUESTIONS_TEXT_2" , convertArrayToStringWhitSeparator(privacy_questions_text_2, separator));	    
		    
		    
		    String [] privacy_answers_text_1 = {
		    		  "Enel Energia S.p.A., con sede legale in viale Regina Margherita n. 125, 00198, Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007 (di seguito “Enel Energia” o “Titolare”), in qualità di Titolare del trattamento, tratterà i suoi dati personali forniti tramite il sito www-coll1.enel.it (di seguito “Sito”) in conformità a quanto stabilito dalla normativa applicabile in materia di protezione dei dati personali e dalla presente Informativa."
		    	    , "Enel Energia ha nominato un Responsabile della Protezione dei Dati personali (RPD) che può essere contattato al seguente indirizzo e-mail: dpo.enelenergia@enel.com."
		    	    , "1.1. Enel Energia tratterà i dati personali da lei comunicati o legittimamente raccolti (“Dati Personali”). In particolare, possono essere oggetto di trattamento i seguenti Dati Personali:" + 
		    	    		"1.1.1. Dati Identificativi: dati che permettono l'identificazione diretta, come i dati anagrafici (quali ad esempio nome, cognome, codice fiscale, Partita IVA, indirizzo, ecc.), conferiti al Titolare e trattati ai fini della sottoscrizione e della gestione del contratto." + 
		    	    		"1.1.2. Dati di consumo: dati relativi alla fornitura e ai livelli di consumo registrati, raccolti ed elaborati nel corso della durata del contratto." + 
		    	    		"1.1.3. Dati dell’area riservata: al momento della creazione di un account le sarà richiesto di fornire alcuni Dati Personali, (quali ad esempio nome, cognome, indirizzo email, numero di cellulare e codice utenza), necessari per completare con successo la registrazione al sito web del Titolare e per la fruizione dei relativi servizi. Enel Energia le consente inoltre di accedere all’area riservata anche tramite APP o mediante l’utilizzo di credenziali fornite da un social network (c.d. “social login”), ad esempio utilizzando le identità create su Facebook, Twitter, Google+, ecc." + 
		    	    		"1.1.4. Dati di Pagamento: in caso di acquisto di prodotti o servizi sono altresì trattati, in conformità alla normativa vigente in materia, i dati di pagamento, inclusi gli estremi identificativi." + 
		    	    		"1.1.5. Dati di contatto: informazioni di contatto (quali ad esempio numeri di telefono, fisso e/o mobile, indirizzo email) forniti al Titolare in fase di sottoscrizione o nel corso della durata del contratto che consentono di contattarla ai fini della gestione del contratto, per fornire risposte alle sue richieste e/o servizi adeguati alle sue esigenze." + 
		    	    		"1.1.6. Dati di navigazione: i sistemi informatici e telematici e le procedure software preposte al funzionamento del Sito, ovvero dedicati al funzionamento e utilizzo delle APP rese disponibili da Enel Energia, acquisiscono, nel corso del loro normale esercizio, alcuni dati (es. la data e l’ora dell’accesso, le pagine visitate, il nome dell’Internet Service Provider e l’indirizzo del Protocollo Internet (IP) attraverso il quale lei accede a Internet, l’indirizzo Internet dal quale lei si è collegato al Sito, ecc.), la cui trasmissione è implicita nell’uso dei protocolli di comunicazione web o è utile per la migliore gestione e ottimizzazione del sistema di invio di dati ed e-mail." + 
		    	    		"1.1.7. Dati acquisiti dal Servizio Clienti: dati forniti in occasione di interazioni con il Servizio Clienti di Enel Energia, ivi incluse le registrazioni delle telefonate, qualora lei abbia acconsentito alla registrazione ovvero nei casi in cui la registrazione venga realizzata in forza di obbligo di legge o regolamento." + 
		    	    		"  1.2. Per trattamento di Dati Personali ai fini della presente informativa è da intendersi qualsiasi operazione o insieme di operazioni, compiute con l’ausilio di processi automatizzati e applicate ai Dati Personali, come la raccolta, la registrazione, l’organizzazione, la strutturazione, la conservazione, l’adattamento o la modifica, l’estrazione, la consultazione, l’uso, la comunicazione mediante trasmissione, diffusione o qualsiasi altra forma di messa a disposizione, il raffronto o l’interconnessione, la limitazione, la cancellazione o la distruzione." + 
		    	    		"  1.3. La informiamo che tali Dati Personali verranno trattati manualmente e/o con il supporto di mezzi informatici o telematici."
		    	    , "1.1. Enel Energia tratterà i suoi Dati Personali per il conseguimento di finalità precise delle quali sarà di volta in volta informato e solo in presenza di una specifica base giuridica prevista dalla legge applicabile in materia di protezione dei dati personali. Nello specifico, Enel Energia tratterà i suoi Dati Personali solo quando ricorra una o più delle seguenti basi giuridiche:" + 
		    	    		"Lei ha prestato il suo consenso libero, specifico, informato, inequivocabile ed espresso al trattamento;" + 
		    	    		"Il trattamento è necessario all’esecuzione di un contratto di cui lei è parte od all’esecuzione di misure precontrattuali adottate su sua richiesta;" + 
		    	    		"In presenza di un legittimo interesse di Enel Energia;" + 
		    	    		"In forza di un obbligo di legge." + 
		    	    		"  1.2. Nella tabella sono elencate le finalità per cui i suoi Dati Personali sono trattati da Enel Energia e la base giuridica su cui si basa il trattamento." + 
		    	    		"  1.3. Il conferimento dei suoi Dati Personali è necessario in tutti i casi in cui il trattamento avviene sulla base di un obbligo di legge, in esecuzione di misure precontrattuali adottate su sua richiesta o per eseguire un contratto di cui lei è parte. Un eventuale suo rifiuto potrebbe comportare per Enel Energia l’impossibilità di dar corso alle finalità per cui i Dati Personali sono raccolti." + 
		    	    		"  1.4. Il conferimento dei suoi Dati Personali è invece facoltativo per il perseguimento delle finalità indicate nella tabella per le quali la base giuridica del trattamento è il suo consenso. Il mancato conferimento del suo consenso in relazione a tali ultime finalità non avrà nessuna conseguenza sulla conclusione del contratto. La natura necessaria o facoltativa del conferimento sarà specificata all’atto della raccolta dei Dati Personali. rimento sarà specificata all’atto della raccolta. "
		    	    , "1.1. L’accesso all’area riservata è consentito tramite le seguenti modalità:" + 
		    	    		"Creando un account sul Sito web di Enel Energia;" + 
		    	    		"Creando un account sull’APP di Enel Energia;" + 
		    	    		"Mediante l’utilizzo di credenziali fornite da un social network (c.d. “social login”), ad esempio utilizzando le identità create su Facebook, Twitter, Google+, ecc." + 
		    	    		"  1.2. Nell’eventualità lei decida di accedere all’area riservata tramite le credenziali di un social network, Enel Energia dovrà acquisire alcuni dei suoi dati personali (Nome, Cognome, Email) in conformità alle autorizzazioni da lei rilasciate al social network di riferimento. Lei è dunque tenuto a verificare le impostazioni previste dal social network di riferimento nonché a leggere con attenzione le relative privacy policy, in quanto le stesse potrebbero autorizzare il social network a condividere con Enel Energia i suoi dati personali autorizzando, così, Enel Energia a raccogliere informazioni come i contatti, gli amici e altri suoi Dati Personali. In ogni caso Enel Energia non verrà a conoscenza dei dati di registrazione da lei utilizzati." + 
		    	    		"  1.3. Enel Energia tratterà il codice identificativo associato al suo account presso il servizio del social network di riferimento (es. indirizzo di posta elettronica) nel momento in cui lei lo utilizza per il login al sito web o alle APP di Enel Energia o su sua richiesta per la condivisione di contenuti ospitati dal Sito o dalle APP. Il trattamento avrà luogo per il tempo necessario a fornire i servizi richiesti." + 
		    	    		"  1.4. Se lei crea un account Enel Energia o usa servizi del Sito collegandosi attraverso il social login, Enel Energia potrà utilizzare le informazioni contenute nell’account di provenienza per completare il suo profilo sul Sito. Lei potrà in ogni momento aggiornare o modificare le informazioni del suo profilo e i suoi dati di contatto attraverso l’area riservata."
		    	    , "1.1. L’indirizzo di posta elettronica e/o di posta cartacea da lei fornito nell’ambito del rapporto contrattuale in essere con Enel Energia potrà essere da quest’ultima utilizzato per l’invio di offerte commerciali relative a prodotti e/o servizi analoghi a quelli in precedenza acquistati (cd. “soft-spam”). Lei potrà in ogni momento opporsi al trattamento inviando una richiesta alla casella di posta dedicata di cui al paragrafo 10 della presente informativa." + 
		    	    		"  1.2. Per tutto quanto non ricompreso nel precedente punto 6.1, all’interno dell’area riservata lei potrà liberamente decidere se acconsentire all’utilizzo dei suoi Dati Personali per il l’invio di materiale pubblicitario, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing e per il compimento di ricerche di mercato, relativa ai prodotti e/o servizi offerti da Enel Energia, società del Gruppo Enel, società controllanti, controllate e/o collegate ovvero partner commerciali di Enel Energia." + 
		    	    		"  1.3. Inoltre, all’interno dell’area riservata lei potrà liberamente decidere se acconsentire all’utilizzo dei suoi Dati Personali da parte di Enel Energia, o da parte di Società del Gruppo Enel, da società controllanti, controllate, collegate o da partner commerciali di Enel Energia, per attività di profilazione sulle sue abitudini di consumo e sui dati personali e le informazioni acquisite osservando l’utilizzo dei prodotti o servizi offerti, al fine di proporle offerte personalizzate." + 
		    	    		"  1.4. Il trattamento dei Dati Personali per le predette finalità di marketing potrà essere realizzato con modalità “tradizionali” (a titolo esemplificativo posta cartacea o chiamate da operatore), ovvero mediante sistemi “automatizzati” di contatto (a titolo esemplificativo SMS, MMS, chiamate telefoniche senza l’intervento dell’operatore, e-mail, fax, applicazioni interattive)." + 
		    	    		"  1.5. Il consenso da lei eventualmente prestato al trattamento dei suoi dati personali per la predetta finalità di marketing effettuato mediante l’utilizzo di sistemi automatizzati di contatto, si intenderà riferito anche all’utilizzo delle modalità tradizionali di contatto. Parimenti, l’eventuale diritto di opposizione al trattamento dei suoi dati personali per la medesima finalità di marketing, effettuato mediante l’utilizzo di sistemi automatizzati di contatto, si estenderà anche all’utilizzo di modalità tradizionali di contatto, fatta salva la possibilità, a lei riconosciuta, di esercitare tale diritto solo in parte." + 
		    	    		"  1.6. La preventiva acquisizione del suo consenso per le finalità di marketing e profilazione come sopra descritte, non è richiesta quando il contatto avviene attraverso numeri di telefono (fisso e/o mobile) e indirizzi di posta cartacea appartenenti a soggetti presenti negli elenchi telefonici pubblici, in relazione ai quali risulta infatti applicabile la disciplina in tema di Registro Pubblico delle Opposizioni che consente il trattamento di tali dati di contatto nei confronti di chi non abbia esercitato il diritto di opposizione." + 
		    	    		"  1.7. Il consenso per le predette finalità di marketing e profilazione è facoltativo e non impedisce la possibilità di fruire dei prodotti o servizi offerti da Enel Energia. In particolare:" + 
		    	    		"Previo libero e specifico consenso, che è possibile rilasciare all’interno dell’area riservata nella sezione “Gestione Consensi”, l’interessato acconsente all’utilizzo dei propri dati di contatto da parte di Enel Energia per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing e per l’invio di materiale pubblicitario effettuato sia mediante modalità automatizzate di contatto (es. SMS, e-mail, telefonate senza operatore) che mediante modalità tradizionali di contatto (es. posta cartacea, telefonate con operatore);" + 
		    	    		"Previo libero e specifico consenso, che è possibile rilasciare all’interno dell’area riservata nella sezione “Gestione Consensi”, i Dati Personali conferiti potranno altresì essere comunicati a società del Gruppo Enel, società controllanti, controllate, collegate o a partner commerciali di Enel Energia, ai quali Enel Energia potrà comunicare o cedere i dati acquisiti per le finalità descritte al punto precedente effettuate sia mediante modalità automatizzate di contatto (es. SMS, e-mail, telefonate senza operatore) che mediante modalità tradizionali di contatto (es. posta cartacea, telefonate con operatore);" + 
		    	    		"Infine, previo libero e specifico consenso, che è possibile rilasciare all’interno dell’area riservata nella sezione “Gestione Consensi”, Enel Energia o altre Società del Gruppo Enel, società controllanti, controllate, collegate o partner commerciali di Enel Energia, potranno svolgere attività di profilazione basata sulle sue abitudini di consumo e sui dati personali e le informazioni acquisite osservando l’utilizzo dei prodotti o servizi offerti, al fine di proporle offerte personalizzate. "
		    	    , "I suoi Dati Personali potranno essere resi accessibili, per le finalità sopra menzionate:" + 
		    	    		"Ai dipendenti e ai collaboratori di Enel Energia, agenti, appositamente nominati Persone Autorizzate al trattamento, o alle società del Gruppo Enel presenti nel territorio dell’Unione Europea, che potranno agire quali Titolari autonomi, o quali Responsabili al trattamento, allo scopo nominate;" + 
		    	    		"Alle società terze o altri soggetti (“Terze Parti”) che svolgono alcune attività in outsourcing per conto di Enel Energia, nella loro qualità di responsabili esterni del trattamento," + 
		    	    		"A terzi soggetti, società del Gruppo Enel, società controllanti, controllate o collegate o partner commerciali di Enel Energia, per finalità di marketing diretto, nel rispetto delle prescrizioni del GDPR, previo l’ottenimento del suo consenso. "
		    	    , "I suoi Dati Personali saranno trattati all’interno dell’Unione Europea e conservati su server ubicati all’interno dell’Unione Europea. "
		    	    , "1.1 I Dati Personali oggetto di trattamento per le finalità descritte nel capitolo “Finalità e base giuridica del trattamento” saranno conservati nel rispetto dei principi di proporzionalità e necessità, e comunque fino a che non siano state perseguite le finalità del trattamento." + 
		    	    		"1.2 Nel caso in cui Lei abbia sottoscritto un contratto con Enel Energia, i suoi Dati Personali saranno conservati per un periodo di 10 anni dalla cessazione del rapporto contrattuale stesso, fatta salva la conservazione ai fini del trattamento per adempiere a specifici obblighi di legge o a provvedimenti dell’Autorità, per la riscossione dei crediti residui e per la gestione di contestazioni, reclami e azioni giudiziali." + 
		    	    		"1.3. Nel caso in cui lei abbia prestato il suo consenso al trattamento dei suoi Dati Personali per le finalità di marketing e/o profilazione di cui al capitolo 6 della presente informativa e non abbia sottoscritto un contratto con Enel Energia, i suoi Dati Personali saranno trattati per le predette finalità fatto salvo il suo diritto di opporsi in ogni momento al trattamento inviando una richiesta alla casella di posta dedicata di cui al paragrafo 10 della presente informativa e di revocare il consenso prestato."
		    	    , "Ai sensi degli artt. 15 – 22 del GDPR, in relazione ai Dati Personali trattati, lei ha il diritto di:" + 
		    	    		"Accedere e chiederne copia;" + 
		    	    		"Richiedere la rettifica;" + 
		    	    		"Richiedere la cancellazione;" + 
		    	    		"Ottenere la limitazione del trattamento;" + 
		    	    		"Opporsi al trattamento;" + 
		    	    		"Ricevere in un formato strutturato, di uso comune e leggibile da dispositivo automatico e di trasmettere senza impedimenti tali dati a un altro titolare del trattamento; ove tecnicamente fattibile." + 
		    	    		"1.1. La informiamo che lei ha comunque il diritto di opporsi in qualsiasi momento al trattamento dei Dati Personali che la riguardano effettuato per finalità di marketing diretto, compresa la profilazione, nella misura in cui sia connessa a tale marketing diretto. Il diritto di opporsi a tale trattamento eseguito tramite mezzi di contatto automatizzati si estende anche al trattamento dei Dati Personali tramite mezzi di contatto tradizionali, a meno che lei non desideri opporsi solo in parte." + 
		    	    		"1.2. Qualora si opponga al trattamento per finalità di marketing diretto, i suoi Dati Personali non saranno più oggetto di trattamento per tali finalità." + 
		    	    		"1.3. Per l’esercizio dei suoi diritti e per la revoca del suo consenso lei potrà inviare una comunicazione all’indirizzo email privacy.enelenergia@enel.com." + 
		    	    		"1.4. Per maggiori informazioni relative ai suoi Dati Personali potrà rivolgersi al Responsabile per la Protezione dei Dati Personali, raggiungibile al seguente indirizzo e-mail dpo.enelenergia@enel.com." + 
		    	    		"1.5. Le ricordiamo che è un suo diritto proporre un reclamo al Garante per la Protezione dei Dati Personali, mediante:" + 
		    	    		"raccomandata A/R indirizzata a Garante per la protezione dei dati personali, Piazza Venezia, 11, 00187 Roma;" + 
		    	    		"e-mail all'indirizzo: garante@gpdp.it, oppure protocollo@pec.gpdp.it;" + 
		    	    		"fax al numero: 06/696773785." + 
		    	    		"  Informativa aggiornata a gennaio 2019"
		    	    , "Per approfondire consulta la pagina disponibile qui."};
		    prop.setProperty("PRIVACY_ANSWERS_TEXT_1" , convertArrayToStringWhitSeparator(privacy_answers_text_1, separator));
		    
		    String [] privacy_answers_text_2 = {
		    		"1.1. Enel Italia S.r.l., con sede legale in Roma, Viale Regina Margherita 125, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007 (di seguito, Enel o il “Titolare”), in qualità di Titolare del trattamento, tratterà i suoi dati personali forniti tramite il sito www-coll1.enel.it (di seguito “Sito”) in conformità a quanto stabilito dalla normativa applicabile in materia di privacy e protezione dei dati personali e dalla presente informativa." + 
		    		"1.2. In occasione della sottoscrizione dei vari servizi o dell’accesso agli stessi verranno comunicati i nominativi degli ulteriori eventuali Titolari e dei Responsabili del trattamento."
		    		, "2.1 Il Titolare ha nominato un Responsabile della Protezione dei dati (RPD) che può essere contatto al seguente indirizzo e-mail: dpoenelitalia@enel.com."
		    		, "3.1. Enel tratterà i dati personali da lei comunicati o legittimamente reperiti dal Titolare (“Dati Personali”). In particolare sono trattati i seguenti Dati Personali:" + 
		    				"3.1.1. Dati di contatto: nome, cognome, indirizzo email, numero di telefono, il contenuto del messaggio da lei inviato e altri Dati Personali che ci può aver fornito durante le comunicazioni intercorse. Tratteremo questi Dati Personali nel caso in cui lei ci ponga dei quesiti, richieda informazioni o ci trasmetta comunicazioni di varia natura." + 
		    				"Lei ci ha trasmesso questi Dati Personali nel momento in cui ci ha contattato. Il trattamento di questi Dati Personali è necessario per fornire un riscontro alle comunicazioni ricevute o alle richieste da lei avanzate. Il conferimento ulteriore di Dati Personali deve ritenersi assolutamente facoltativo." + 
		    				"3.1.2. Dati di navigazione: i sistemi informatici e telematici e le procedure software preposte al funzionamento del Sito acquisiscono, nel corso del loro normale esercizio, alcuni dati (es. la data e l’ora dell’accesso, le pagine visitate, il nome dell’Internet Service Provider e l’indirizzo del Protocollo Internet (IP) attraverso il quale lei accede a Internet, l’indirizzo Internet dal quale lei si è collegato al nostro Sito, ecc.), la cui trasmissione è implicita nell’uso dei protocolli di comunicazione web o è utile per la migliore gestione e ottimizzazione del sistema di invio di dati ed e-mail." + 
		    				"3.2. Per trattamento di Dati Personali ai fini della presente informativa è da intendersi qualsiasi operazione o insieme di operazioni, compiute con l’ausilio di processi automatizzati e applicate ai Dati Personali, come la raccolta, la registrazione, l’organizzazione, la strutturazione, la conservazione, l’adattamento o la modifica, l’estrazione, la consultazione, l’uso, la comunicazione mediante trasmissione, diffusione o qualsiasi altra forma di messa a disposizione, il raffronto o l’interconnessione, la limitazione, la cancellazione o la distruzione." + 
		    				"3.3. La informiamo che tali Dati Personali verranno trattati manualmente e/o con il supporto di mezzi informatici o telematici."
		    		, "4.1. Enel tratterà i suoi Dati Personali per il conseguimento di finalità precise e solo in presenza di una specifica base giuridica prevista dalla legge applicabile in materia di privacy e protezione dei dati personali. Nello specifico, Enel tratterà i suoi Dati Personali solo quando ricorre una o più delle seguenti basi giuridiche:" + 
		    				"·lei ha prestato il suo consenso libero, specifico, informato, inequivocabile ed espresso al trattamento;" + 
		    				"il trattamento è necessario all’esecuzione di un contratto di cui lei è parte o dall’esecuzione di misure precontrattuali adottate su sua richiesta;" + 
		    				"in presenza di un legittimo interesse di Enel;" + 
		    				"Enel è tenuta in forza di un obbligo di legge a trattare i Dati Personali." + 
		    				"4.2. Nella tabella sono elencate le finalità per cui i suoi Dati Personali sono trattati dal Titolare e la base giuridica su cui si basa il trattamento." + 
		    				"4.3. Il conferimento dei suoi Dati Personali è necessario in tutti i casi in cui il trattamento avviene sulla base di un obbligo di legge o per eseguire un contratto di cui lei è parte o dall’esecuzione di misure precontrattuali adottate su sua richiesta. Un eventuale suo rifiuto potrebbe comportare per Enel l’impossibilità di dar corso alla finalità per cui i Dati Personali sono raccolti." + 
		    				"4.4. Il conferimento dei suoi Dati Personali è invece volontario per il perseguimento di ulteriori finalità e il mancato conferimento del suo consenso in relazione a esse non avrà nessuna conseguenza sulla conclusione del contratto. La natura obbligatoria o facoltativa del conferimento sarà specificata all’atto della raccolta."
		    		, "5.1 I suoi Dati Personali potranno essere resi accessibili, per le finalità sopra menzionate:" + 
		    				"  ai dipendenti e ai collaboratori del Titolare, a tal fine nominati Autorizzati al trattamento, o alle società del Gruppo Enel presenti nel territorio dell’Unione Europea;" + 
		    				"alle società terze o altri soggetti che svolgono attività in outsourcing per conto del Titolare per consentire il funzionamento del Sito, nella loro qualità di Responsabili esterni del trattamento."
		    		, "6.1 I suoi Dati Personali saranno trattati all’interno dell’Unione Europea e conservati su server ubicati all’interno dell’Unione Europea."
		    		, "7.1 I Dati Personali oggetto di trattamento per le finalità di cui sopra saranno conservati nel rispetto dei principi di proporzionalità e necessità, e comunque fino a che non siano state perseguite le finalità del trattamento."
		    		, "8.1 Ai sensi degli artt. 15 – 21 del Regolamento UE 2016/679 (GDPR), in relazione ai Dati Personali comunicati, lei ha il diritto di:" + 
		    				"  accedere e chiederne copia;" + 
		    				"richiedere la rettifica;" + 
		    				"richiedere la cancellazione;" + 
		    				"ottenere la limitazione del trattamento;" + 
		    				"opporsi al trattamento;" + 
		    				"ricevere in un formato strutturato, di uso comune e leggibile da dispositivo automatico e di trasmettere senza impedimenti tali dati a un altro titolare del trattamento, ove tecnicamente fattibile." + 
		    				"" +
		    				"8.2 La informiamo che lei ha comunque il diritto di opporsi in qualsiasi momento al trattamento dei Dati Personali che la riguardano effettuato sulla base del legittimo interesse di Enel." + 
		    				"8.3 Qualora si opponga al trattamento dei suoi Dati Personali secondo quanto indicato nell’articolo 8.2, il Titolare si asterrà dal trattare ulteriormente i Dati Personali, salvo che egli dimostri l’esistenza di motivi legittimi cogenti per procedere al trattamento, oppure per l’accertamento, l’esercizio o la difesa di un diritto in sede giudiziaria." + 
		    				"8.4 Per l’esercizio dei suoi diritti e per la revoca del suo consenso, nonché per maggiori informazioni relative ai suoi Dati Personali, lei potrà inviare una comunicazione al  Responsabile per la Protezione dei Dati di Enel Italia S.r.l., raggiungibile al seguente indirizzo e-mail dpoenelitalia@enel.com, indicando inderogabilmente nell’oggetto il contenuto della sua richiesta." + 
		    				"8.5 Le ricordiamo che è un suo diritto proporre un reclamo all’autorità competente per la protezione dei Dati Personali." + 
		    				"8.6 Nel caso sia adito il Garante per la Protezione dei Dati Personali, lei potrà presentare un reclamo mediante:" + 
		    				"raccomandata A/R indirizzata a Garante per la Protezione dei Dati Personali, Piazza di Monte Citorio, 121 00186 Roma;" + 
		    				"e-mail all'indirizzo: garante@gpdp.it, oppure protocollo@pec.gpdp.it;" + 
		    				" fax al numero: 06/696773785."
		    		, "Consulta anche la Cookie policy di Enel Italia"};
		    prop.setProperty("PRIVACY_ANSWERS_TEXT_2" , convertArrayToStringWhitSeparator(privacy_answers_text_2, separator));

			//** END Set Valori da controllare
			
			
			
			logger.write("check text and links on the footer of page 'https://www-coll1.enel.it/' - Start ");

			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By links_object=footer.legal_links;
			//footer.checkFooterLegalLinksByTexts(links_object, prop.getProperty("LEGAL_LINKS_TEXT").split(separator));
			By social_icons_object=footer.social_icons;
			footer.checkFooterSocialIconsByAttibuteText(
					social_icons_object, prop.getProperty("SOCIAL_ICONS_TEXT").split(separator) , prop.getProperty("SOCIAL_ICONS_TYPE"));
			Thread.sleep(5000);
			footer.clickFooterLegalLinkByText(prop.getProperty("PRIVACY_LEGAL_LINK"));
			footer.checkUrl(prop.getProperty("LINK") + prop.getProperty("PRIVACY_URL"));

			TopMenuComponent topMenu = new TopMenuComponent(driver);

			topMenu.checkMenuPath(prop.getProperty("PRIVACY_PATH_MENU_TEXT"));
			topMenu.checkTitle(prop.getProperty("PRIVACY_TITLE_MENU_TEXT"));
			//topMenu.checkDetailByClass(prop.getProperty("PRIVACY_DETAIL_MENU_TEXT") , "hero_detail text");

			ArticlePageComponent article  = new ArticlePageComponent(driver);

			article.checkArticlePageTabs(prop.getProperty("PRIVACY_TABS_TEXT").split(";"));
			article.checkQuestionsBySection(prop.getProperty("PRIVACY_QUESTIONS_TEXT_1").split(separator), "dropdown detail");
			article.checkQuestionsBySection(prop.getProperty("PRIVACY_QUESTIONS_TEXT_2").split(separator), "dropdown_1601160761 detail");

			article.checkAnswersByQuestionsTextAndSection(prop.getProperty("PRIVACY_ANSWERS_TEXT_1").split(separator)
					, prop.getProperty("PRIVACY_QUESTIONS_TEXT_1").split(separator)
					,"dropdown detail");
			
			/*article.checkAnswersByQuestionsTextAndSection(prop.getProperty("PRIVACY_ANSWERS_TEXT_2").split(separator)
					, prop.getProperty("PRIVACY_QUESTIONS_TEXT_2").split(separator)
					,"dropdown_1601160761 detail");*/
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

			//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

	public static String convertArrayToStringWhitSeparator(String[] array , String separator) {
		String prop = "";
		for(String el : array) {
			prop = prop.concat(el).concat(separator);
		}
		prop = prop.substring(0, prop.length() - separator.length());
		return prop;
	}
}
