package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class ConfermaIndirizziAllaccioAttivazioneDomicilio {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			String statoUbiest=Costanti.statusUbiest;

			//Se il tasto conferma è abilitato, viene confermata l'intera sezione senza inserire indirizzo
			if(!util.exists(compila.buttonConfermaIndizzoResidenzaDisabilitato,5)){
				compila.pressButton(compila.buttonConfermaIndizzoResidenza);
			}
			else{
				String fornitura_indirizzo=prop.getProperty("CLIENTE_FORNISCE_INDIRIZZO");
				if(prop.getProperty("TIPO_CLIENTE").compareTo("Residenziale")==0){
					//Se il campo 'fornisce indirizzo' è abilitato viene popolato
					if(!util.exists(compila.clienteFornisceIndirizzoSelectDisabilitato,5)){
						logger.write("Selezione campo 'cliente fornisce indirizzo?- Start");
						compila.clienteFornisceIndirizzo(fornitura_indirizzo);
						logger.write("Selezione campo 'cliente fornisce indirizzo?- Completed");
					}
					//				if(fornitura_indirizzo.compareTo("NO")==0){
					// se il tasto Modifica Indirizzo non è abilitato, viene 
					//					if (util.exists(compila.buttonModificaIndizzoResidenzaAbilitato,5)) {
					//						compila.confermaIndirizzo("Indirizzo di Residenza/Sede Legale");
					//						//}
				}
				//					else{
				if(statoUbiest.compareTo("ON")==0){
					logger.write("Selezione Indirizzo di Fatturazione - Start");
					compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
					logger.write("Selezione Indirizzo di Residenza/Sede Legalee - Completed");
				}
				else if (statoUbiest.compareTo("OFF")==0){
					logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Start");
					compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Indirizzo di Residenza/Sede Legale",prop.getProperty("CAP"),prop.getProperty("CITTA"));
					logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Completed");
				}
			}
			//				}
			//			}
			if(statoUbiest.compareTo("ON")==0){
				logger.write("Selezione Indirizzo di Fatturazione - Start");
				compila.selezionaIndirizzoDomicilio("Indirizzo di Fatturazione");
				logger.write("Selezione Indirizzo di Fatturazione - Completed");
			}
			else if (statoUbiest.compareTo("OFF")==0){
				logger.write("Selezione Indirizzo di Fatturazione Forzato - Start");
				compila.selezionaIndirizzoDomicilioForzato("Indirizzo di Fatturazione",prop.getProperty("CAP"),prop.getProperty("CITTA"));
				logger.write("Selezione Indirizzo di Fatturazione Forzato - Completed");
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
