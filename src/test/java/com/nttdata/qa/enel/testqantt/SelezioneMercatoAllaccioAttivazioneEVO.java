package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class SelezioneMercatoAllaccioAttivazioneEVO {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				Thread.currentThread().sleep(10000);
				IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(
						driver);
				logger.write("Verifica checklist e conferma - Start");
				CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
				
				checkListModalComponent.clickConferma();
				prop.setProperty("NUMERO_RICHIESTA",
						intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
				logger.write("Verifica checklist e conferma - Completed");
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
		
				gestione.selezionaLigtheningValue("Mercato di riferimento prodotto", prop.getProperty("MERCATO"));
//				Thread.currentThread().sleep(5000);
//				if(prop.getProperty("FLAG_CLIENTE_PRODUTTORE").equals("Y")) {
//					gestione.press(gestione.clienteProduttoreFlag);
//					   gestione.selezionaLigtheningValue("Tipologia Produttore", prop.getProperty("TIPOLOGIA_PRODUTTORE"));
//					   gestione.clickComponent(gestione.genericConfermaButton);
//						gestione.checkSpinnersSFDC();
//                   if(prop.getProperty("TIPO_UTENZA").contentEquals("S2S")) {
//                	   if(!gestione.getElementExistance(By.xpath("//p[text()='Non sei abilitato ad eseguire operazioni sui Clienti Produttori. Invita il cliente a recarsi presso un Punto Enel.']"))) {
//                		   throw new Exception("Utilizzando una utenza S2S e selezionato la Tipologia Produttore non viene mostrato l'alert di mancata abilitazione ad eseguire su clienti produttori.");
//                	   }
//                   }
//				}
				
				 gestione.clickComponent(gestione.confermaButtonSelezioneMercato);
				 gestione.checkSpinnersSFDC();
				 
				 
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
	
	
	public static String checkListText = "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE CHE:• il contatore elettrico sia presente e non abbia erogato energia• il contatore gas sia presente: non sia mai stato attivato o l'impianto sia stato modificato• il cliente sia in possesso dei dati necessari : POD/PDR - Matricola Contatore/EneltelINFORMAZIONI UTILI• Se il cliente attiva la domiciliazione bancaria e la Bolletta Web riceverà un bonus in fattura• In caso di Dual le forniture dovranno avere stesso: USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO• In caso di Multi le forniture dovranno avere stesso: USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO – PRODOTTO – COMMODITY• In caso di Dual/Multi non sarà possibile eseguire la modifica potenza e/o tensione• In caso di richiesta attivazione ascensore sarà necessario che il cliente presenti la relativa scheda tecnica• In caso di Cliente Pubblica Amministrazione sarà necessario il Codice Ufficio• In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della prima attivazione. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta  In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associata SCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it";
	public static String checkListText2 = "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE CHE:• il contatore elettrico sia presente e non abbia erogato energia• il contatore gas sia presente: non sia mai stato attivato o l'impianto sia stato modificato• il cliente sia in possesso dei dati necessari : POD/PDR - Matricola Contatore/Eneltel• nel caso di cliente in SALVAGUARDIA, ricorda che è obbligatorio il rientro dell'ISTANZA DI SALVAGUARDIAINFORMAZIONI UTILI• Se il cliente attiva la domiciliazione bancaria e la Bolletta Web riceverà un bonus in fattura• In caso di Dual le forniture dovranno avere stesso: USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO• In caso di Multi le forniture dovranno avere stesso: USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO – PRODOTTO – COMMODITY• In caso di Dual/Multi non sarà possibile eseguire la modifica potenza e/o tensione• In caso di richiesta attivazione ascensore sarà necessario che il cliente presenti la relativa scheda tecnica• In caso di Cliente Pubblica Amministrazione sarà necessario il Codice Ufficio• In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della prima attivazione. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta  In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associata SCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it"; 


}
