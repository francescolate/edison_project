package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BsnSelfCareHomePageComponent;
import com.nttdata.qa.enel.components.colla.BsnSelfCareLeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyBsnSelfCareHome {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			BsnSelfCareLeftMenuComponent leftMenu = new BsnSelfCareLeftMenuComponent(driver);
			
			boolean shouldVerifyWelcomeTitle = Boolean.parseBoolean(prop.getProperty("VERIFY_BSN_SC_TITLE", "false"));
			if (shouldVerifyWelcomeTitle) {
				//System.out.println("Verifying page title");
				logger.write("Verifying page title - Start");
				BsnSelfCareHomePageComponent homePage = new BsnSelfCareHomePageComponent(driver);
				homePage.verifyComponentVisibility(homePage.titleLabel);
				homePage.clickComponent(homePage.titleLabel);
				logger.write("Verifying page title - Completed");
			}
			
//			//System.out.println("Verifying left menu item: avatar");
//			logger.write("Verifying left menu item: avatar - Start");
//			By avatar = leftMenu.avatarLogo;
//			leftMenu.verifyComponentExistence(avatar);
//			logger.write("Verifying left menu item: avatar - Completed");
			
			//Verifying left menu items
			//System.out.println("Verifying left menu items");
			logger.write("Verifying left menu items - Start");
			Thread.sleep(10000);
			//leftMenu.verifyMenuItems();
			logger.write("Verifying left menu items - Completed");
			
			boolean shouldLogout = Boolean.parseBoolean(prop.getProperty("SHOULD_LOGOUT", "false"));
			if (shouldLogout) {
				//System.out.println("Logout");
				logger.write("Logout - Start");
				leftMenu.clickComponent(By.id("disconnetti"));
//				leftMenu.selectMenuItem(prop.getProperty("BSN_LEFT_MENU_ITEM_LOGOUT").toLowerCase());
//				LoginLogoutEnelCollaComponent homePage = new LoginLogoutEnelCollaComponent(driver);
//				By logo = homePage.logoEnel;
//				homePage.verifyComponentExistence(logo);
				logger.write("Logout - Completed");
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
}
