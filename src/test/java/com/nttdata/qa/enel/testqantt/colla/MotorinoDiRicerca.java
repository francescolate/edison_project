package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MotorinoDiRicerca {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			 logger.write("click sul tab 'Luce e Gas' - Start");
				
				TopMenuComponent tab=new TopMenuComponent(driver);
				TimeUnit.SECONDS.sleep(10);
				tab.sceltaMenu("Luce e gas");
				
				LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);	
				headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS);
				logger.write("click sul tab 'Luce e Gas' - Completed");
				
				logger.write("check sul titolo della pagina 'Trova la soluzione giusta per te' nella sezione 'Luce e Gas' - Start");
				By solutionLabel=headerMenu.rightSolutionLabel;
				headerMenu.verifyComponentExistence(solutionLabel);
				logger.write("check sul titolo della pagina 'Trova la soluzione giusta per te' nella sezione 'Luce e Gas' - Completed");
				
				logger.write("Selezionare i seguenti filtri: Che contratto vuoi attivare? "+prop.getProperty("TYPE_OF_CONTRACT")+"; Dove? "+prop.getProperty("PLACE")+"; Per quale necessità? "+prop.getProperty("NEED")+" e click su  Inizia Ora - Start");			
				By searchMenu=headerMenu.searchSection;
				headerMenu.verifyQuestionsExistence(searchMenu);
				
				By iniziaOraPulsating=headerMenu.iniziaOraButton;
				headerMenu.verifyComponentExistence(iniziaOraPulsating);
				
				By optionsContractList=headerMenu.contractList;
				headerMenu.verifyFirstListBox(optionsContractList);
				
				By optionsPlaceList=headerMenu.placeList;
				headerMenu.verifySecondListBox(optionsPlaceList);
				
				By optionsNeedList=headerMenu.needList;
				headerMenu.verifyThirdListBox(optionsNeedList);
				headerMenu.scrollComponent(headerMenu.labelPiùEnergiaperLaTuaCasa);
				TimeUnit.SECONDS.sleep(3);
			
				headerMenu.selectionOptionsFromListbox(optionsContractList,prop.getProperty("TYPE_OF_CONTRACT"),optionsPlaceList,prop.getProperty("PLACE"),optionsNeedList,prop.getProperty("NEED"));	
				headerMenu.scrollComponent(iniziaOraPulsating);
				TimeUnit.SECONDS.sleep(3);
				headerMenu.clickComponent(iniziaOraPulsating);
				logger.write("Selezionare i seguenti filtri: Che contratto vuoi attivare? "+prop.getProperty("TYPE_OF_CONTRACT")+"; Dove? "+prop.getProperty("PLACE")+"; Per quale necessità? "+prop.getProperty("NEED")+" e click su  Inizia Ora - Completed");
			
				OffersLuceComponent offer= new OffersLuceComponent(driver);	
				//By elightLabel=offer.labelElight;
				//offer.verifyComponentExistence(elightLabel);
				By featuredofferlabel=offer.labelInEvidenza;
				offer.verifyComponentExistence(featuredofferlabel);
				//logger.write("check sull'esistenza della label 'IN EVIDENZA' e dell'offerta in evidenza 'E-light': - Completed");
				logger.write("check sull'esistenza della label 'IN EVIDENZA' - Completed");
				
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
