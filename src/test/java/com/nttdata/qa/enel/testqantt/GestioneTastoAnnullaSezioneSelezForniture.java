package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;



public class GestioneTastoAnnullaSezioneSelezForniture {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

		try {

			
			SeleniumUtilities util = new SeleniumUtilities(driver);
			//logger.write("Seleziona Tipologia Documento "+ prop.getProperty("TIPOLOGIA_DOC")+"- Start");
			SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            
			
			logger.write("Premi tasto Annulla, verifica pop UP- Start");
			forn.clickComponent(forn.buttonAnnulla);
			TimeUnit.SECONDS.sleep(1);
			forn.verificaPopUpconTesto(forn.popUpAnnullaTitle, forn.popUpAnnullaTesto, forn.popUpAnnullaButtonConferma, forn.popUpAnnullaTestoAtteso);
			logger.write("Premi tasto Annulla, verifica pop UP - Completed ");
			TimeUnit.SECONDS.sleep(5);
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
