package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato170ModIndFatturazioneACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_170_Mod_ind_Fatturazione_ACR {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato170ModIndFatturazioneACRComponent MIF = new Privato170ModIndFatturazioneACRComponent(driver);
			
			MIF.verifyComponentVisibility(MIF.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = MIF.homePageCentralText;
			MIF.verifyComponentExistence(homePage);
			MIF.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			By homepageSubheading = MIF.homePageCentralSubText;
			MIF.verifyComponentExistence(homepageSubheading);
			MIF.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = MIF.sectionTitle;
			MIF.verifyComponentExistence(sectionTitle);
			MIF.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = MIF.sectionTitleSubText;
			MIF.verifyComponentExistence(sectionTitleSubText);
			MIF.VerifyText(sectionTitleSubText, MIF.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verifying the presence of 'dettaglioFornituraButton','visualizzaLeBolletteButton' and click on 'dettaglioFornituraButton' - Start");
			By dettaglioFornituraButton =  MIF.dettaglioFornitura_button;
			MIF.verifyComponentVisibility(dettaglioFornituraButton);
			
			By visualizzaLeBolletteButton = MIF.visualizzaLeBollette_button;
			MIF.verifyComponentVisibility(visualizzaLeBolletteButton);
			
			MIF.clickComponent(dettaglioFornituraButton);
			logger.write("Verifying the presence of 'dettaglioFornituraButton','visualizzaLeBolletteButton' and click on 'dettaglioFornituraButton' - Completed");
			Thread.sleep(5000);
			
			MIF.verifyComponentVisibility(MIF.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = MIF.serviziPage;
			MIF.verifyComponentExistence(serviziHomePage);
			MIF.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SUBTEXT is displayed in page - Start");
			By serviziPageSubText= MIF.serviziPageSubText;
			MIF.verifyComponentExistence(serviziPageSubText);
			MIF.VerifyText(serviziPageSubText, prop.getProperty("SERVIZI_HOMEPAGE_SUBTEXT"));
			logger.write("Checking if SERVIZI_HOMEPAGE_SUBTEXT is displayed in page - Completed");
			
			logger.write("Validating the presence of Modifica_Indirizzo_Fatturazione button - Start");
			By modificaIndirizzoFatturazione_button = MIF.modificaIndirizzoFatturazione_button;
			MIF.verifyComponentExistence(modificaIndirizzoFatturazione_button);
			MIF.VerifyText(modificaIndirizzoFatturazione_button, prop.getProperty("MODIFICA_INDIRIZZO_FATTURZIONE"));
			logger.write("Validating the presence of Modifica_Indirizzo_Fatturazione button - Completed");
			

			logger.write("Verify and Click on left menu item 'SERVIZI' - Start");
			By servizi = MIF.serviziSelect;
			MIF.verifyComponentExistence(servizi);
			MIF.clickComponent(servizi);
			logger.write("Verify and Click on left menu item 'SERVIZI' - Completed");
			Thread.sleep(5000);
			
			MIF.verifyComponentVisibility(MIF.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			MIF.verifyComponentExistence(serviziHomePage);
			MIF.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SUBTEXT is displayed in page - Start");
			MIF.verifyComponentExistence(serviziPageSubText);
			MIF.VerifyText(serviziPageSubText, prop.getProperty("SERVIZI_HOMEPAGE_SUBTEXT"));
			logger.write("Checking if SERVIZI_HOMEPAGE_SUBTEXT is displayed in page - Completed");
			
			logger.write("Validating the presence of Modifica_Indirizzo_Fatturazione button - Start");
			MIF.verifyComponentExistence(modificaIndirizzoFatturazione_button);
			MIF.VerifyText(modificaIndirizzoFatturazione_button, prop.getProperty("MODIFICA_INDIRIZZO_FATTURZIONE"));
			logger.write("Validating the presence of Modifica_Indirizzo_Fatturazione button - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}