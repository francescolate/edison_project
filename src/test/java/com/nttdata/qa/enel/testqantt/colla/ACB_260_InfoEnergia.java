package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNFornitureComponent;
import com.nttdata.qa.enel.components.colla.BSN_AttivazioneInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.BSN_InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergia_BsnComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_260_InfoEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Click on Piu Informazioni Link -- Start");
			pa.verifyComponentExistence(pa.piuInformazioniLink);
			pa.clickComponent(pa.piuInformazioniLink);
			logger.write("Click on Piu Informazioni Link -- Completed");
			
			FornitureBSNComponent sc = new FornitureBSNComponent(driver);
			Thread.sleep(10000);
			logger.write("Verify page navigation and page details -- Start");
			pa.verifyComponentExistence(sc.pagePath_260);
			sc.comprareText(sc.pageTitle_260, sc.PageTitle_260, true);
			sc.comprareText(sc.titleSubText_260, sc.TitleSubText_260, true);
					
			pa.verifyComponentExistence(sc.gestisciletueForniture);
			sc.comprareText(sc.infoEnelEnergiaText, sc.InfoEnelEnergiaRowText, true);
			pa.verifyComponentExistence(sc.scopriDiPiuLink);
			logger.write("Verify page navigation and page details -- Completed");

			logger.write("Click on scopri di piu link -- Start");
			sc.clickComponent(sc.scopriDiPiuLink);
			Thread.sleep(20000);
			
			BSNFornitureComponent fc = new BSNFornitureComponent(driver);
			fc.verifyComponentExistence(fc.pagePath);
			fc.verifyComponentExistence(fc.pageTitle);
			/*fc.comprareText(fc.pagePath, fc.Path, true);
			fc.comprareText(fc.pageText, fc.PageText, true);*/
			fc.verifyComponentExistence(fc.prosegui);
			fc.clickComponent(fc.prosegui);
			
			BSN_InfoEnelEnergiaComponent bic = new BSN_InfoEnelEnergiaComponent(driver);
			BSN_AttivazioneInfoEnelEnergiaComponent ic = new BSN_AttivazioneInfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			/*ic.verifyComponentExistence(ic.pagePath);
			ic.comprareText(ic.pageTitle, ic.PageTitle, true);
			ic.comprareText(ic.pageSubText, ic.PageSubText, true);*/
			ic.verifyComponentExistence(ic.esci);
			ic.verifyComponentExistence(ic.indietro);
			
			ic.verifyComponentExistence(ic.menuOne);
			ic.comprareText(ic.menuOneDescription, ic.MenuOneDescription, true);
			ic.verifyComponentExistence(ic.avvisoComunicaLettura);
			ic.verifyComponentExistence(ic.avvisoComunicaLetturaSMS);
			ic.verifyComponentExistence(ic.avvisoComunicaLetturaEmail);
			
			ic.verifyComponentExistence(ic.avvisoEmissioneBolletta);
			ic.verifyComponentExistence(ic.avvisoEmissioneBollettaSMS);
			ic.verifyComponentExistence(ic.avvisoEmissioneBollettaEmail);
			
			ic.verifyComponentExistence(ic.avvenutoPagamento);
			ic.verifyComponentExistence(ic.avvenutoPagamentoSMS);
			ic.verifyComponentExistence(ic.avvenutoPagamentoEmail);
			
			ic.verifyComponentExistence(ic.avvisoProssimaScadenza);
			ic.verifyComponentExistence(ic.avvisoProssimaScadenzaSMS);
			ic.verifyComponentExistence(ic.avvisoProssimaScadenzaEmail);
			
			ic.verifyComponentExistence(ic.avvisoSollecitoPagamento);
			ic.verifyComponentExistence(ic.avvisoSollecitoPagamentoSMS);
			ic.verifyComponentExistence(ic.avvisoSollecitoPagamentoEmail);
			
			ic.verifyComponentExistence(ic.avvisoConsumi);
			ic.verifyComponentExistence(ic.avvisoConsumiSMS);
			ic.verifyComponentExistence(ic.avvisoConsumiEmail);
			
			ic.verifyComponentExistence(ic.email);
			ic.verifyComponentExistence(ic.confirmEmail);
			ic.verifyComponentExistence(ic.cellulare);
			ic.verifyComponentExistence(ic.confermaCellulare);
			ic.verifyFieldEnable(ic.email);
			ic.verifyFieldEnable(ic.confirmEmail);
			ic.verifyFieldEnable(ic.cellulare);
			ic.verifyFieldEnable(ic.confermaCellulare);
			
			ic.clickComponent(ic.avvisoComunicaLetturaEmail);
			ic.clickComponent(ic.avvisoEmissioneBollettaEmail);
			ic.clickComponent(ic.avvenutoPagamentoEmail);
			ic.clickComponent(ic.avvisoProssimaScadenzaEmail);
			ic.clickComponent(ic.avvisoSollecitoPagamentoEmail);
			ic.clickComponent(ic.avvisoConsumiEmail);
			ic.enterInputParameters(ic.email, prop.getProperty("EMAIL_INPUT"));
			ic.enterInputParameters(ic.confirmEmail, prop.getProperty("EMAIL_INPUT"));
			ic.verifyFieldEnable(ic.cellulare);
			ic.verifyFieldEnable(ic.confermaCellulare);
			ic.verifyComponentExistence(ic.aggiornaIDatiDiContatto);
			ic.clickComponent(ic.aggiornaIDatiDiContatto);
			ic.clickComponent(ic.prosegui);
			
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Start");
			ic.verifyComponentExistence(ic.menuTwo);
			ic.comprareText(ic.titleSubText, ic.TitleSubText, true);
			ic.comprareText(ic.menuTwoTitle, ic.MenuTwoTitle, true);
			ic.verifyComponentExistence(ic.avvisoComunicaLettura);
			ic.verifyComponentExistence(ic.avvisoEmissioneBolletta);
			ic.verifyComponentExistence(ic.avvenutoPagamento);
			ic.verifyComponentExistence(ic.avvisoProssimaScadenza);
			ic.verifyComponentExistence(ic.avvisoSollecitoPagamento);
			ic.verifyComponentExistence(ic.avvisoConsumi);
			
			ic.comprareText(ic.supplyDataText, ic.SupplyDataText, true);
			ic.verifyComponentExistence(ic.tipo);
			ic.verifyComponentExistence(ic.numeroCliente);
			ic.verifyComponentExistence(ic.address);
			ic.comprareText(ic.tipoValue, ic.TipoValue, true);
			ic.comprareText(ic.numeroClientiValue, ic.NumeroClientiValue_260, true);
			ic.comprareText(ic.addressValue, ic.AddressValue_260, true);
			ic.verifyComponentExistence(ic.esci);
			ic.verifyComponentExistence(ic.indietro);
			ic.verifyComponentExistence(ic.prosegui);
			ic.clickComponent(ic.prosegui);
			
			logger.write("Verify  Info enel enegia page,menu three details-- Start");
			Thread.sleep(30000);
			bic.verifyComponentExistence(bic.infoTitle);
			//bic.comprareText(bic.pagePath, bic.PagePath, true);
			bic.comprareText(bic.titleSubText, bic.TitleSubText, true);
			bic.verifyComponentExistence(bic.estito);
			bic.comprareText(bic.sectionTitle, bic.SectionTitle, true);
			bic.comprareText(bic.sectionText, bic.SectionText, true);
			bic.verifyComponentExistence(bic.attivaBollettaWeb);
			bic.verifyComponentExistence(bic.fineButton);
			bic.clickComponent(bic.fineButton);
			Thread.sleep(10000);
			logger.write("Verify  Info enel enegia page,menu three details-- Completed");
			
			logger.write("Click on scopri di piu link -- Completed");
			logger.write("Verify home page title and page path -- Start");
			pa.verifyComponentExistence(pa.pageTitle);
			pa.comprareText(pa.pagePath, pa.Path, true);
			pa.comprareText(pa.pageTitle, pa.PageTitle, true);
			logger.write("Verify home page title and page path -- Completed");
			
			Thread.sleep(120000);
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
