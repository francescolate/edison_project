package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneAppuntamentoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class GestioneAppuntamento {

	@Step("SalvaInBozza")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			GestioneAppuntamentoComponent appuntamento = new GestioneAppuntamentoComponent(driver);
			
			appuntamento.presaAppuntamento(prop.getProperty("PRESA_APPUNTAMENTO"));
		    logger.write("Scelta opzione presa appuntamento");
		    appuntamento.inserimentoDatiAppuntamento(prop.getProperty("NOME_APPUNTAMENTO"), prop.getProperty("COGNOME_APPUNTAMENTO"), prop.getProperty("TELEFONO_APPUNTAMENTO"));
		    logger.write("Inserimento dati appuntamenti");
		    appuntamento.press(appuntamento.confermaButton);
		    logger.write("Click Conferma");
		    

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
