package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench {
	private final static int SEC = 120;
	
	@Step("VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench")
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DettagliSpedizioneCopiaDocComponent forn=new DettagliSpedizioneCopiaDocComponent(driver);
			
//			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			
			if(prop.getProperty("STATUS").compareToIgnoreCase("In lavorazione")!=0){
				throw new Exception("Il campo Stato contiene un valore diverso da quello atteso. Valore attuale: "+prop.getProperty("STATUS")+" valore atteso: 'In lavorazione' Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}
			
			if(prop.getProperty("ITA_IFM_SUBSTATUS__C").compareToIgnoreCase("Inviato")!=0){
				throw new Exception("Il campo Sottostato contiene un valore diverso da quello atteso. Valore attuale: "+prop.getProperty("ITA_IFM_SUBSTATUS__C")+" valore atteso: 'Inviato' Case documento di riferimento:"+prop.getProperty("NUMERO_DOCUMENTO"));
				}
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
