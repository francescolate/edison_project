package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class GetNumeroCellulareOTP {

	@Step("Abilita Confirmation Call")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriverNoKill(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				// System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}

				SeleniumUtilities util = new SeleniumUtilities(driver);
									
				
				String query = "SELECT id, Name, ITA_IFM_Status__c, ITA_IFM_Account__c, ITA_IFM_Tipologia_Touch_Point__c, ITA_IFM_Email__c, ITA_IFM_Cellulare__c, ITA_IFM_Cellulare_Touch_Point__c, " + 
						"ITA_IFM_Expired_Date__c, ITA_IFM_DaysExpiry__c, " + 
						"ITA_IFM_Channel1__c, ITA_IFM_DateReminder1__c, ITA_IFM_DaysReminder1__c,  " + 
						"ITA_IFM_Channel2__c, ITA_IFM_DateReminder2__c, ITA_IFM_DaysReminder2__c, " + 
						"ITA_IFM_Channel3__c, ITA_IFM_DateReminder3__c, ITA_IFM_DaysReminder3__c, " + 
						"ITA_IFM_Channel4__c, ITA_IFM_DateReminder4__c, ITA_IFM_DaysReminder4__c,  " + 
						"ITA_IFM_Channel5__c, ITA_IFM_DateReminder5__c, ITA_IFM_DaysReminder5__c " + 
						"FROM ITA_IFM_Touch_Point__c " + 
						"WHERE " + 
						"ITA_IFM_Case__r.CaseNumber = '"+prop.getProperty("NUMERO_RICHIESTA")+"' " +
						"AND ITA_IFM_Tipologia_Touch_Point__c <> null and ITA_IFM_Status__c = 'In Progress'";
				
				a.insertQuery(query);
				a.pressButton(a.submitQuery);
				if(a.verificaNoRecordFound()) {
					throw new Exception ("La query per recuperare OTP: "+prop.getProperty("NUMERO_RICHIESTA"));
				}else {
					a.recuperaRisultati(1, prop);
					TimeUnit.SECONDS.sleep(10);
				}
				
		
				TimeUnit.SECONDS.sleep(3);

				a.logoutWorkbench();

			}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
