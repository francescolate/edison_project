package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_ACR_27 {
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
			
			logger.write("check the username and passsword login - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME")); 

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));
			logger.write("check the username and passsword login - Completed");
			
			logger.write("Click on the login button- Start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			By accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
			logger.write("Verify the page after successful login - Complete");
			
			// Account page 
			AccountComponent rcc = new AccountComponent(driver);
			
			logger.write("Verify the left Menu list - Start");
			rcc.homepageRESIDENTIALMenu(rcc.LeftMenuList);
			logger.write("Verify the left Menu list - Complete");
			
			logger.write("Click on Accounts from the Menulist - Start");
			rcc.clickComponent(rcc.Accounts);
			logger.write("Click on Accounts from the Menulist - Complete");
			
			logger.write("Verify the heading and contents - Start");
			rcc.verifyComponentExistence(rcc.Heading);
			rcc.verifyComponentExistence(rcc.HeadingValue);
			logger.write("Verify the heading and contents - Complete");
			
//			rcc.VerifyDatieContatti(rcc.DatieContatti, args[0].substring(args[0].lastIndexOf("_")+1, args[0].lastIndexOf(".properties")));
		
			logger.write("Click on Modify button - Start");
			rcc.verifyComponentExistence(rcc.ModificaConsensiBtn);
			rcc.clickComponent(rcc.ModificaConsensiBtn);
			logger.write("Click on Modify button - Complete");
			
						
			logger.write("Verify the Consensi heading and contents - Start");
			rcc.verifyComponentExistence(rcc.Consensi);
			rcc.verifyComponentExistence(rcc.ConsensiValue);
			logger.write("Verify the Consensi heading and contents - Complete");
			
	
			
		//	rcc.radiobuttonStatus(rcc.TFERadioBtnNo,rcc.TFERadioBtnLabelNo,rcc.TFERadioBtnSI,rcc.TFERadioBtnLabelSI);
		//	rcc.radiobuttonStatus(rcc.TelefonoTerziRadiBtnNo,rcc.TelefonoTerizoRadioBtnLabelNo,rcc.TelefonoTerziRadiBtnSI,rcc.TelefonoTerizoRadioBtnLabelSI);
			
		//	rcc.radiobuttonStatus(rcc.NumeroEnerRadiobtnSI, rcc.NumeroEnerRadiobtnLabelSI, rcc.NumeroEnerRadiobtnNo, rcc.NumeroEnerRadiobtnLablelNo);
		//	rcc.radiobuttonStatus(rcc.NumeroTerziRadiobtnSI, rcc.NumeroTerziRadiobtnLabelSI, rcc.NumeroTerziRadiobtnNo, rcc.NumeroTerziRadiobtnLabelNo);
			
			rcc.radiobuttonStatus(rcc.EmailEnergiaRadioBtnNo, rcc.EmailEnergiaRadioBtnLabelNo, rcc.EmailEnergiaRadioBtnSI, rcc.EmailEnergiaRadioBtnLableSI);
		//	rcc.radiobuttonStatus(rcc.EmailTerziRadioBtnNo, rcc.EmailTerziRadioBtnLabelNo, rcc.EmailTerziRadioBtnSI, rcc.EmailTerziRadioBtnLabelSI);
	     	
		//	rcc.radiobuttonStatus(rcc.profilazioneRadioBtnNo, rcc.profilazioneRadioBtnLabelNo, rcc.profilazioneRadioBtnLabelNo, rcc.profilazioneRadioBtnLabelSI);
			
			logger.write("Click on Modifichi button - Start");
			
			
			rcc.clickComponent(rcc.SalvaModifiBtn);
			logger.write("Click on Modifichi button - Complete");
			
			rcc.verifyFineButton(rcc.FineBtn);
			
			logger.write("Verify the operazione heading and contents - Start");
			//rcc.verifyComponentExistence(rcc.OperazioneHeading);
			//rcc.verifyComponentExistence(rcc.OperazioneValue);
			logger.write("Verify the operazione heading and contents - Complete");
			
			logger.write("Click on Fine button - Start");
			rcc.verifyComponentExistence(rcc.FineBtn);
			rcc.clickComponent(rcc.FineBtn);
			logger.write("Click on Fine button - Complete");
			
			logger.write("Verify the left Menu list - Start");
			rcc.homepageRESIDENTIALMenu(rcc.LeftMenuList);
			logger.write("Verify the left Menu list - Complete");
			
			driver.close();
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		
	}
	

}
