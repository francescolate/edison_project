package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_ModuloReclami_69 {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			ImpressComponent ic = new ImpressComponent(driver);

			
			logger.write("Accessing Disservizio page - Start");
			ic.launchLink(prop.getProperty("LINK"));
		//	ic.verifyComponentExistence(ic.logoEnel);
			ic.verifyComponentExistence(ic.buttonAccetta);
			ic.clickComponent(ic.buttonAccetta);
			logger.write("Accessing Disservizio page - Complete");
			
			ic.verifyComponentExistence(ic.reclaimiPath);
			ic.comprareText(ic.reclaimiPath, ImpressComponent.RECLAIMI_PATH, true);
			
			ic.verifyComponentExistence(ic.ModuloHeader);
			ic.comprareText(ic.ModuloHeader, ImpressComponent.MODULO_HEADER, true);
			
			ic.verifyComponentExistence(ic.tipolgiaLabel);
			
			ic.verifyComponentExistence(ic.tipolgiaInput);
			
			ic.verifyComponentExistence(ic.InserisciHeader);
			
			ic.verifyComponentExistence(ic.NomeLabel);
			
			ic.verifyComponentExistence(ic.CognomeLabel);
			
			ic.verifyComponentExistence(ic.NomeInput);
			
			ic.verifyComponentExistence(ic.CognomeInput);
			
			ic.verifyComponentExistence(ic.TelefonoLabel);
			
			ic.verifyComponentExistence(ic.TelefonoInput);
			
			ic.verifyComponentExistence(ic.fasceLabel);
			ic.verifyComponentExistence(ic.fasceInput);
			
			ic.verifyComponentExistence(ic.codiceFiscaleLabel);
			
			ic.verifyComponentExistence(ic.codiceFiscaleInput);
			
			ic.verifyComponentExistence(ic.indrizzoInput);
			ic.verifyComponentExistence(ic.indrizzoLabel);
			
			ic.verifyComponentExistence(ic.emailLabel);
			ic.verifyComponentExistence(ic.emailInput);
			
			ic.verifyComponentExistence(ic.confermaEmailLabel);
			ic.verifyComponentExistence(ic.confermaEmailInput);
			
			ic.verifyComponentExistence(ic.numeroClienteLabel);
			ic.verifyComponentExistence(ic.numeroClienteInput);
			
			ic.verifyComponentExistence(ic.codiceLabel);
			
			ic.verifyComponentExistence(ic.codiceInput);
			ic.verifyComponentExistence(ic.argomentoHeader);
			
			ic.verifyComponentExistence(ic.argomentoLabel);
			
			ic.verifyComponentExistence(ic.argomentoInput);
			
			ic.verifyComponentExistence(ic.discrizioneLabel);
			ic.verifyComponentExistence(ic.discrizioneInput);
			
			ic.verifyComponentExistence(ic.autoletturaLabel);
			ic.verifyComponentExistence(ic.autoletturaInput);
			
			ic.verifyComponentExistence(ic.datadelLabel);
			ic.verifyComponentExistence(ic.datadelInput);
			
			ic.verifyComponentExistence(ic.informativaContent);
			
			ic.verifyComponentExistence(ic.informativaContent);
			
			ic.verifyComponentExistence(ic.informativaCheckboxContent);
			
			ic.verifyComponentExistence(ic.informativaConent2);
			
			ic.verifyComponentExistence(ic.mandatoryfield);
			
								
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
