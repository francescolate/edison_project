package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PLACETFissaLuceConsumerComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HOMEPAGE_ACR_3 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
						
			HomeComponent home = new HomeComponent(driver);
			
		    prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
			prop.setProperty("VISUALIZZALEBOLLETTETEXT", "Le tue bollette Qui potrai visualizzare bollette e/o rate delle tue forniture.");

			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
						
			logger.write("Check for Residential Menu - Completed");

			logger.write("Check for Supply label - Start");

			home.verifyComponentExistence(home.supplyLabelRegoalre);
			
			logger.write("Check for Supply label - Completed");
			
			logger.write("Check for View your bills - Start");

			home.verifyComponentExistence(home.viewYourBillsRegolare);
			
			logger.write("Check for View your bills - Completed");

			logger.write("Check for Supply Details - Start");

			//home.verifyComponentExistence(home.supplyDetailsRegolare);
			//span[@id='310504623']
			home.verifyComponentExistence(By.xpath("//span[@id='"+prop.getProperty("NUMEROCLIENTE")+"']"));
			
			logger.write("Check for Supply Details - Completed");
			
			logger.write("Check for Customer Number - Start");
						
			home.checkFieldValue(home.customerNumRegalore, prop.getProperty("NUMEROCLIENTE"));

			logger.write("Check for Customer Number - Completed");
			
			logger.write("Check for Stato Fornitura - Start");

			//home.checkFieldValue(home.nomeFornituraRegalore, prop.getProperty("STATOFORNITURA"));
			home.checkFieldValue(By.xpath("//dd[@id='fornitura"+prop.getProperty("NUMEROCLIENTE")+"']"), prop.getProperty("STATOFORNITURA"));
			
			logger.write("Check for Stato Fornitura - Completed");
			
			logger.write("Check for PAYMENTSTATUS - Start");

			home.checkFieldValue(home.paymentStatus, prop.getProperty("STATOPAGAMENTO"));
			
			logger.write("Check for PAYMENTSTATUS - Completed");
			
			/*logger.write("Check for address  - Start");

			//home.checkFieldValue(home.address, prop.getProperty("INDIRIZZO"));
			
			logger.write("Check for address - Completed");
			
			logger.write("Check for nome Fornitura - Start");

			home.checkFieldValue(home.supplyLabelRegoalre, prop.getProperty("NOMEFORNITURA"));
			
			logger.write("Check for nome Fornitura - Completed");
			
			logger.write("Click on your bills button - Start");

			home.clickComponent(home.viewYourBillsRegolare);
			
			logger.write("Click on your bills button - Completed");

			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			
			home.checkForContent(home.yourBillsTitle,home.yourBillsSubText, prop.getProperty("VISUALIZZALEBOLLETTETEXT"));

			home.navigateBack(home.fornitureTitle);		
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
			
			logger.write("Click on supply details button - Start");

			home.clickComponent(home.supplyDetailsRegolare);
			
			logger.write("Click on supply details button - Complete");

			home.verifyComponentExistence(home.gas);
			
			logger.write("Check for customer address - Start");

			log.VerifyText(home.indirizzoGas, prop.getProperty("INDIRIZZO"));
			
			logger.write("Check for customer address- Completed");

			logger.write("Check for customer number - Start");

			log.VerifyText(home.customerNum, prop.getProperty("NUMEROCLIENTE"));
			
			logger.write("Check for customer number - Completed");			
			*/
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}


}
