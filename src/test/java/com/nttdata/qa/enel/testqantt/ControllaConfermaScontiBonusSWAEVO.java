package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ScontiEBonusComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


public class ControllaConfermaScontiBonusSWAEVO {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            ScontiEBonusComponent sconti = new ScontiEBonusComponent(driver);

            // Verifica Errore Codice Amico errato
			logger.write("Sconti e bonus Inserimento numero errato nel campo Codice Amico - Start");
            sconti.inserisciCodiceAmico("4321");
            logger.write("Sconti e bonus Inserimento numero errato nel campo Codice Amico - Completed");
        	logger.write("Conferma Sconti e bonus - Start");
            sconti.pressButton(sconti.confermaScontiEBonusSWAEVO);
            Thread.currentThread().sleep(5000);
            logger.write("Conferma Sconti e bonus - Completed");
            sconti.verificaErrore();
			logger.write("Svuota campo Codice Amico");
            sconti.pulisciCodiceAmico();
			logger.write("Conferma Sconti e bonus - Start");
            sconti.pressButton(sconti.confermaScontiEBonusSWAEVO);
            logger.write("Conferma Sconti e bonus - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}

