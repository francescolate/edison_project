package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaClienteWorkbenchDaCaseNumber {

	private final static int SEC = 120;
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate(Costanti.workbenchLink);
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				a.pressButton(a.checkAgree);
				a.pressButton(a.buttonLogin);
				TimeUnit.SECONDS.sleep(2);
				//System.out.println(driver.getCurrentUrl());
				while (!driver.getCurrentUrl().startsWith(Costanti.workbenchLink)) {
					page.enterUsername(Costanti.workbenchUser);
					page.enterPassword(Costanti.workbenchPwd);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}
				a.insertQuery(String.format(prop.getProperty("QUERY"),prop.getProperty("NUMERO_RICHIESTA").trim()));
				boolean condition = false;
				int tentativi = 2;
				while (!condition && tentativi-- > 0) {
					a.pressButton(a.submitQuery);
					condition = a.aspettaRisultati(SEC);
				}
				if (condition) {
					a.recuperaRisultati(Integer.parseInt("1"), prop);
					if(prop.getProperty("TIPO_CLIENTE").contentEquals("Condominio")) {
						logger.write("Verifica anagrafica cliente condominio a db - Start");
						if(!prop.getProperty("ACCOUNT.ITA_IFM_COMPANY__C").contentEquals(prop.getProperty("RAGIONE_SOCIALE"))) throw new Exception("La ragione sociale del cliente non risulta essere "+prop.getProperty("RAGIONE_SOCIALE")+" come atteso. Valore riscontrato "+prop.getProperty("ACCOUNT.ITA_IFM_COMPANY__C"));
						logger.write("Verifica anagrafica cliente condominio a db - Completed");
					}
					if(prop.getProperty("TIPO_CLIENTE").contentEquals("Casa")) {
						logger.write("Verifica anagrafica cliente residenziale a db - Start");
						if(!prop.getProperty("ACCOUNT.NE__FISCAL_CODE__C").contentEquals(prop.getProperty("NUOVO_CODICE_FISCALE"))) throw new Exception("Il codice fiscale cliente non risulta essere "+prop.getProperty("NUOVO_CODICE_FISCALE")+" come atteso. Valore riscontrato "+prop.getProperty("ACCOUNT.NE__FISCAL_CODE__C"));
						logger.write("Verifica anagrafica cliente residenziale a db - Completed");
					}
					if(prop.getProperty("TIPO_CLIENTE").contentEquals("Impresa Individuale")) {
						logger.write("Verifica anagrafica cliente Impresa Individuale a db - Start");
						if(!prop.getProperty("ACCOUNT.ITA_IFM_COMPANY__C").contentEquals(prop.getProperty("RAGIONE_SOCIALE"))) throw new Exception("La ragione sociale del cliente non risulta essere "+prop.getProperty("RAGIONE_SOCIALE")+" come atteso. Valore riscontrato "+prop.getProperty("ACCOUNT.ITA_IFM_COMPANY__C"));
						logger.write("Verifica anagrafica cliente Impresa Individuale a db - Completed");
					}
					
					a.logoutWorkbench();
				} else
					throw new Exception("Impossibile recuperare i risultati dalla query workbench. Sistema lento");

			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}