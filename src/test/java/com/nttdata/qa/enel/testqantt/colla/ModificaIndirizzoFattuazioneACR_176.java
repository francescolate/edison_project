package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.GetAccountDetailsComponent;
import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazioneACR_176 {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			SuplyPrivateDetailComponent spdc = new SuplyPrivateDetailComponent(driver);
			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
		
			prop.setProperty("CITTAINPUTVALUE", "Roma");
			prop.setProperty("INDRIZZOINPUTVALUE", "Via Appia Antica");
			prop.setProperty("NUMERCOCIVICINPUTVALUE", "1");
			prop.setProperty("CAPVALUE", "00198");
			
			logger.write("Home Page Title and Description Verification- Starts");
			mif.comprareText(mif.homepageHeadingRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING_RES, true);
			mif.comprareText(mif.homepageParagraphRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_PARAGRAPH_RES, true);
			logger.write("Home Page Title and Description Verification- Ends");
			
			logger.write("Verify and click the Servizi link - Start");
			spdc.clickComponent(spdc.ServiziLink);
			logger.write("Verify and click the Servizi link - Start");
			
			logger.write("Servizi Page Title and Description Verification- Starts");
//			mif.comprareText(mif.servizipageHeadingRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_HEADING_RES, true);
//			mif.comprareText(mif.servizipageParagraphRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_PARAGRAPH_RES, true);
			logger.write("Servizi Page Title and Description Verification- Ends");
			
			logger.write("Click on Modiffica link - Start");
			spdc.clickComponent(spdc.ModifficaIndrizzolink);
			logger.write("Click on Modiffica link - Complete");
			
			logger.write("Verify the title and contents - Start");
			spdc.verifyComponentExistence(spdc.ModificaTitle);
			spdc.verifyComponentExistence(spdc.ModificaContents);
			logger.write("Verify the title and contents - Complete");
			
			logger.write("Click on the checkbox  - Start");
			spdc.clickComponent(spdc.ModificaCheckbox1);
			spdc.clickComponent(spdc.ModificaButton);
			logger.write("Click on the checkbox - Complete");
			Thread.sleep(2000);
			
			logger.write("Verify the mandatory fields  - Start");
			mif.verifyComponentExistence(mif.mifHeading);
			mif.verifyComponentExistence(mif.mifHeading2);
			spdc.verifyComponentExistence(spdc.CittaLabel);
			spdc.verifyComponentExistence(spdc.IndrizzoLabel);
			spdc.verifyComponentExistence(spdc.NumeroCivicoLabel);
			logger.write("Verify the mandatory fields  - Ends");
			
			logger.write("Enter the input values in the mandatory fields  - Start");		
			spdc.verifyComponentExistence(spdc.CittaInputField176);
			spdc.enterLoginParameters(spdc.CittaInputField176, prop.getProperty("CITTAINPUTVALUE"));
			Thread.sleep(2000);
			//spdc.verifyComponentExistence(spdc.CittaDropDownRoma);
			//spdc.clickComponent(spdc.CittaDropDownRoma);
			
			Thread.sleep(2000);
			spdc.verifyComponentExistence(spdc.IndrizzoInput176);	
			spdc.enterText(spdc.IndrizzoInput176, prop.getProperty("INDRIZZOINPUTVALUE"));
			Thread.sleep(1000);
			//spdc.verifyComponentExistence(spdc.IndrizzoDropDownAntica);
			//spdc.clickComponent(spdc.IndrizzoDropDownAntica);
			
			Thread.sleep(1000);
			spdc.verifyComponentExistence(spdc.NumercoCivicInput176);
			spdc.enterLoginParameters(spdc.NumercoCivicInput176, prop.getProperty("NUMERCOCIVICINPUTVALUE"));
			logger.write("Enter the input values in the mandatory fields  - Complete");
			
			Thread.sleep(2000);
			logger.write("Click on the Contiue button - Start");
			spdc.verifyComponentExistence(spdc.ContinuaBtn);
			spdc.clickComponent(spdc.ContinuaBtn);	
			Thread.sleep(2000);
			logger.write("Click on the Contiue button - Complete");
			
			//-------------------
			Thread.sleep(2000);
			/*mif.clickComponent(mif.popCloseinfoicon);
			
			spdc.enterLoginParameters(spdc.CapInput, prop.getProperty("CAPVALUE"));
			logger.write("Enter the input values in the mandatory fields  - Complete");
			
			logger.write("Click on the Contiue button - Start");
			
			spdc.clickComponent(spdc.ContinuaBtn);	
			logger.write("Click on the Contiue button - Complete");
			
			spdc.clickComponent(spdc.CapIRoma);	
			
			spdc.clickComponent(spdc.ContinuaBtn);	
			Thread.sleep(2000);
			
			//spdc.verifyComponentExistence(spdc.IndrizzoInput);	
			spdc.enterText(spdc.IndrizzoInput, prop.getProperty("INDRIZZOINPUTVALUE"));
			
			//spdc.verifyComponentExistence(spdc.NumercoCivicInput);
			spdc.enterLoginParameters(spdc.NumercoCivicInput, prop.getProperty("NUMERCOCIVICINPUTVALUE"));
			
			spdc.clickComponent(spdc.ContinuaBtn);	*/
			
			
			//--------------------
			
			logger.write("Verify the address details- Starts");
			mif.verifyAddressDetails(mif.mifHeading, ModificaIndirizzoFattuazioneComponent.INDIRIZO_FIELDS, mif.INDIRIZO_VALUES);
			mif.comprareText(mif.indirizzoDiFatturazione, ModificaIndirizzoFattuazioneComponent.INDIRIZZO_DI_FATTURAZIONE, true);
			//mif.comprareText(mif.addressValue, ModificaIndirizzoFattuazioneComponent.ADDRESS_VALUE, true);
			//mif.comprareText(mif.numeroCliente, ModificaIndirizzoFattuazioneComponent.NUMERO_CLIENTE, true);
			logger.write("Verify the address details- Ends");
			
			logger.write("Click on servizi and veify the text- Starts");
			spdc.clickComponent(spdc.ServiziLink);
			Thread.sleep(2000);
			mif.comprareText(mif.popupHeadingRES, ModificaIndirizzoFattuazioneComponent.POPUP_HEADING, true);
			mif.comprareText(mif.popupParagraph, ModificaIndirizzoFattuazioneComponent.POPUP_PARAGRAPH, true);
			mif.verifyComponentExistence(mif.popupNoButton);
			mif.verifyComponentExistence(mif.popupSIButton);
			Thread.sleep(2000);
			mif.clickComponent(mif.popupSIButton);
			Thread.sleep(2000);
			logger.write("Click on Forniture and veify the text- Starts");
			
			logger.write("Servizi Page Title and Description Verification- Starts");
//			mif.comprareText(mif.servizipageHeadingRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_HEADING_RES, true);
//			mif.comprareText(mif.servizipageParagraphRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_PARAGRAPH_RES, true);
			logger.write("Servizi Page Title and Description Verification- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		

	}

}
