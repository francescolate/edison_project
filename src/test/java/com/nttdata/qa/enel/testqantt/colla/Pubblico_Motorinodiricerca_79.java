package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LuceEGasComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID80MotorinoRicercaComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.components.colla.SupportFaqComponent;
import com.nttdata.qa.enel.components.colla.SupportPrivacyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_Motorinodiricerca_79 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			HomeComponent home = new HomeComponent(driver);
			LuceEGasComponent lg = new LuceEGasComponent(driver);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);

			// Step 1
			logger.write("Accessing home page - Start");
			LoginLogoutEnelCollaComponent homePage = new LoginLogoutEnelCollaComponent(driver);
			homePage.launchLink(prop.getProperty("LINK"));
			By logo = homePage.logoEnel;
			homePage.verifyComponentExistence(logo);
			homePage.clickComponentIfExist(By.xpath("//div[@id='fsa-close-button']"));
			By acceptCookieBtn = homePage.buttonAccetta;
			homePage.verifyComponentExistence(acceptCookieBtn);
			homePage.clickComponent(acceptCookieBtn);
			logger.write("Accessing home page - Completed");

			prop.setProperty("ACTIVESUPPLY", "Vuoi attivare una nuova fornitura Scopri le offerte per la tua casa");
			prop.setProperty("RESIDENTIALMENU",
					" Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
			prop.setProperty("VISUALIZZALEBOLLETTETEXT",
					"Le tue bollette Qui potrai visualizzare bollette e/o rate delle tue forniture.");
			prop.setProperty("QST1", "Che contratto vuoi attivare?");
			prop.setProperty("ANS1", "Luce");
			prop.setProperty("QST2", "Dove?");
			prop.setProperty("ANS2", "Casa");
			prop.setProperty("QST3", "Per quale necessità?");
			prop.setProperty("ANS3", "Visualizza tutte");
			prop.setProperty("PATH", "HOME / LUCE E GAS");

			// Step 2
			logger.write("Check for questions - Start");
			log.VerifyText(lg.qst1, prop.getProperty("QST1"));
			log.VerifyText(lg.qst2, prop.getProperty("QST2"));
			log.VerifyText(lg.qst3, prop.getProperty("QST3"));
			logger.write("Check for questions - Completed");

			// Step 3
			logger.write("select drop down value - Start");
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			PubblicoID80MotorinoRicercaComponent mrc = new PubblicoID80MotorinoRicercaComponent(driver);
			Thread.sleep(30000);
			pc.clickComponent(pc.iniziaOra);
			Thread.sleep(30000);
			pc.changeDropDownValue(pc.productDropDown, pc.luceEgas);
			pc.changeDropDownValue(pc.placeDropDown, pc.nagozio);
			pc.changeDropDownValue(pc.myselectionDD, pc.voltura);
			pc.clickComponent(pc.iniziaOra);
			Thread.sleep(30000);
			mrc.checkURLAfterRedirection(prop.getProperty("MAIN_LINK"));
			logger.write("select drop down value - Completed");

			// Step 4
			logger.write("Verify links - Start");
			prop.setProperty("LUCE_E_GAS_HEADING", "Luce e gas per le imprese");
			prop.setProperty("LUCE_E_GAS_TITLE", "Luce e gas per la tua impresa");
			mrc.verifyComponentExistence(mrc.luceEgasHeading);
			mrc.comprareText(mrc.luceEgasHeading, prop.getProperty("LUCE_E_GAS_HEADING"), true);
			mrc.verifyComponentExistence(mrc.luceEgasTitle);
			mrc.comprareText(mrc.luceEgasTitle, prop.getProperty("LUCE_E_GAS_TITLE"), true);
			mrc.verifyComponentExistence(mrc.luceEgasContent);
			mrc.comprareText(mrc.luceEgasContent, mrc.LuceEgasContentText, true);
			mrc.verifyComponentExistence(mrc.visualizzaLuceLink);
			mrc.verifyComponentExistence(mrc.visualizzaGasLink);
			mrc.verifyComponentExistence(mrc.contattaLink);
			logger.write("Verify links - Completed");

			// Step 5
			logger.write("Verify Luce link - Start");
			mrc.clickComponent(mrc.visualizzaLuceLink);
			mrc.checkURLAfterRedirection(prop.getProperty("VISUALIZZA_OFFERTA_LUCE_LINK"));
			Thread.sleep(10000);
			pc.verifyComponentExistence(pc.luceEgas);
			pc.verifyComponentExistence(pc.nagozio);
			// pc.verifyComponentExistence(pc.visualiza);
			logger.write("Verify Luce link - Completed");

			// Step 6
			logger.write("Verify back navigation - Start");
			driver.navigate().back();
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("MAIN_LINK"));
			pc.verifyComponentExistence(pc.luceEgas);
			pc.verifyComponentExistence(pc.nagozio);
			// pc.verifyComponentExistence(pc.voltura);
			pc.verifyComponentExistence(pc.iniziaOra);
			logger.write("Verify back navigation - Completed");

			// Step 7
			logger.write("Verify Gas link - Start");
			mrc.clickComponent(mrc.visualizzaGasLink);
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("VISUALIZZA_OFFERTA_GAS_LINK"));
			pc.verifyComponentExistence(pc.luceEgas);
			pc.verifyComponentExistence(pc.nagozio);
			// pc.verifyComponentExistence(pc.visualiza);
			logger.write("Verify Gas link - Completed");

			// Step 8
			logger.write("Verify back navigation - Start");
			driver.navigate().back();
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("MAIN_LINK"));
			pc.verifyComponentExistence(pc.luceEgas);
			pc.verifyComponentExistence(pc.nagozio);
			// pc.verifyComponentExistence(pc.voltura);
			pc.verifyComponentExistence(pc.iniziaOra);
			logger.write("Verify back navigation - Completed");

			// Step 9
			logger.write("Verify Contatta link - Start");
			mrc.clickComponent(mrc.contattaLink);
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("CONTATTA_LINK"));

			mrc.verifyComponentExistence(mrc.pagePath);
			// mrc.comprareText(mrc.pagePath, mrc.ContattaciPath, true);
			mrc.verifyComponentExistence(mrc.pageTitle);
			mrc.comprareText(mrc.pageTitle, mrc.ContattaciTitle, true);
			mrc.verifyComponentExistence(mrc.contattaciSubTitle);
			mrc.comprareText(mrc.contattaciSubTitle, mrc.ContattaciSubTitle, true);
			logger.write("Verify Gas link - Completed");

			// Step 10
			logger.write("Verify back navigation - Start");
			driver.navigate().back();
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("MAIN_LINK"));
			pc.verifyComponentExistence(pc.luceEgas);
			pc.verifyComponentExistence(pc.nagozio);
			// pc.verifyComponentExistence(pc.voltura);
			pc.verifyComponentExistence(pc.iniziaOra);
			logger.write("Verify back navigation - Completed");

			ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
			SupportFaqComponent sc = new SupportFaqComponent(driver);
			SupportPrivacyComponent spc = new SupportPrivacyComponent(driver);
			// Step 11
			logger.write("Verify header section - Start");
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("LUCE_E_GAS"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("IMPRESA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("STORIE"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("FUTUR-E"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("MEDIA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");

			// Step 12
			mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
			mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);
			Thread.sleep(5000);
			logger.write("Verify footer section - Start");
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
			logger.write("Verify footer section - Completed");

			// Step 13
			mod.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("INFORMAZIONI_LEGALI"))));
			mrc.checkURLAfterRedirection(prop.getProperty("INFORMAZIONI_LEGALI_LINK"));
			mod.comprareText(sc.infoLegaliContent, sc.InfoLegaliContent, true);

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
