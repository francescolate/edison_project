package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_217_ACR_Disattivazione {
	

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
			/*
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			
			
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
								
			
						
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			//dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			//dfc.verifyComponentVisibility(dfc.buttonLoginAccedi);
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			dfc.verifyComponentExistence(dfc.BenvenutoTitle);
			dfc.comprareText(dfc.BenvenutoTitle, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_TITLE, true);
			dfc.verifyComponentExistence(dfc.BenvenutoContent);
			dfc.comprareText(dfc.BenvenutoContent, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_CONTENT, true);
			dfc.verifyComponentExistence(dfc.fornitureHeader);
			dfc.comprareText(dfc.fornitureHeader, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_HEADER, true);
			dfc.verifyComponentExistence(dfc.fornitureContent);
			dfc.comprareText(dfc.fornitureContent, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_CONTENT, true);
			*/
			logger.write("Verify and click on Servizi link  - Start");
			dfc.verifyComponentExistence(dfc.serviziLink);
			dfc.clickComponentIfExist(dfc.serviziLink);
			logger.write("Verify and click on Servizi link  - Complete");
			
			logger.write("Verify and click on Servizi title and contents  - Start");
			dfc.verifyComponentExistence(dfc.serviziFornitureTitle);
			dfc.comprareText(dfc.serviziFornitureTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziFornitureContent);
			dfc.comprareText(dfc.serviziFornitureContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteTitle);
			dfc.comprareText(dfc.serviziBolletteTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteContent);
			dfc.comprareText(dfc.serviziBolletteContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziContratoTitle);
			dfc.comprareText(dfc.serviziContratoTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziContratoContent);
			dfc.comprareText(dfc.serviziContratoContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_CONTENT, true);
			logger.write("Verify and click on Servizi title and contents  - Complete");
			
			
			logger.write("Verify and click on disattivazioneFornitura link  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneFornitura);
			dfc.clickComponent(dfc.disattivazioneFornitura);
			logger.write("Verify and click on disattivazioneFornitura link  - Complete");
			
			logger.write("Verify disattivazione header and contents  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneHeader);
			dfc.comprareText(dfc.disattivazioneHeader, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_HEADER, true);
			dfc.verifyComponentExistence(dfc.disattivazioneContent1);
			dfc.comprareText(dfc.disattivazioneContent1, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_CONTENT1, true);
			dfc.verifyComponentExistence(dfc.disattivazioneContent2);
			dfc.comprareText(dfc.disattivazioneContent2, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_CONTENT2, true);
			dfc.verifyComponentExistence(dfc.disattivazioneContent3);
			dfc.comprareText(dfc.disattivazioneContent3, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_CONTENT3, true);
			logger.write("Verify disattivazione header and contents  - Complete");
			
			logger.write("Verify domandeFrequenti header  - Start");
			dfc.verifyComponentExistence(dfc.domandeFrequenti);
			dfc.comprareText(dfc.domandeFrequenti, PrivateAreaDisattivazioneFornituraComponent.DOMANDE_HEADER, true);
			logger.write("Verify domandeFrequenti header  - Start");
			
			
			dfc.verifyComponentExistence(dfc.frequentQuestions);
			dfc.Questions(dfc.frequentQuestions);
						
			dfc.verifyComponentExistence(dfc.ques1PlusIcon);
			
			
			logger.write("Verify the first questions and answers  - Start");
			dfc.clickComponent(dfc.ques1PlusIcon);
			Thread.sleep(4000);
			dfc.verifyComponentExistence(dfc.ans1);
			dfc.comprareText(dfc.ans1, PrivateAreaDisattivazioneFornituraComponent.ANS1, true);
			logger.write("Verify the first questions and answers  - Complete");
			
			logger.write("Verify the second questions and answers  - Start");
			dfc.clickComponent(dfc.ques2PlusIcon);
			dfc.verifyComponentExistence(dfc.ans2);
			dfc.comprareText(dfc.ans2, PrivateAreaDisattivazioneFornituraComponent.ANS2, true);
			logger.write("Verify the second questions and answers  - Complete");
			
			logger.write("Verify the third questions and answers  - Start");
			dfc.clickComponent(dfc.ques3PlusIcon);
			dfc.verifyComponentExistence(dfc.ans3);
			dfc.comprareText(dfc.ans3, PrivateAreaDisattivazioneFornituraComponent.ANS3, true);
			logger.write("Verify the third questions and answers  - Complete");
			
			logger.write("Verify the fourth questions and answers  - Start");
			dfc.clickComponent(dfc.ques4PlusIcon);
			dfc.verifyComponentExistence(dfc.ans4);
			dfc.comprareText(dfc.ans4, PrivateAreaDisattivazioneFornituraComponent.ANS4, true);
			logger.write("Verify the fourth questions and answers  - Complete");
			
			logger.write("Verify the fifth questions and answers  - Start");
			dfc.clickComponent(dfc.ques5PlusIcon);
			dfc.verifyComponentExistence(dfc.ans5);
			dfc.comprareText(dfc.ans5, PrivateAreaDisattivazioneFornituraComponent.ANS5, true);
			logger.write("Verify the fifth questions and answers  - Complete");
			
			logger.write("Verify the sixth questions and answers  - Start");
			dfc.clickComponent(dfc.ques6PlusIcon);
			dfc.verifyComponentExistence(dfc.ans6);
			dfc.comprareText(dfc.ans6, PrivateAreaDisattivazioneFornituraComponent.ANS6, true);
			logger.write("Verify the sixth questions and answers  - Complete");
			
			PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
			
			logger.write("Verify and click on TravoSpazio link - Start");
			pac.verifyComponentExistence(pac.travoSpazioLink);
			pac.clickComponent(pac.travoSpazioLink);
			logger.write("Verify and click on TravoSpazio link - Complete");
			
			pac.clickComponent(pac.popupSIButton);
			
			logger.write("Switch to New window - Start");
			String currentHandle = driver.getWindowHandle();
			pac.SwitchToWindow();
			logger.write("Switch to New window - Complete");
			
			/*logger.write("Verify the TravoSpazio url - Start");
			pac.checkUrl(prop.getProperty("TRAVOSPAZIO_URL"));
			logger.write("Verify the TravoSpazio url - Complete");*/
			
			logger.write("Verify Travospazio home and title - Start");
			pac.verifyComponentExistence(pac.TravoSpazioHome);
			pac.compareText(pac.TravoSpazioHome, PrivateAreaPageComponent.TRAVOSPAZIO_HOME, true);
			pac.verifyComponentExistence(pac.TravoSpazioTitle);
			pac.compareText(pac.TravoSpazioTitle, PrivateAreaPageComponent.TRAVOSPAZIO_TITLE, true);
			logger.write("Verify Travospazio home and title - Complete");	
			
			logger.write("close the window and switch back to parent window- Start");
			driver.close();
			driver.switchTo().window(currentHandle);
			logger.write("close the window and switch back to parent window- Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			

		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			

}
