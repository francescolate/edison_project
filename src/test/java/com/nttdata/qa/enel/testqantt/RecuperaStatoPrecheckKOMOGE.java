package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class RecuperaStatoPrecheckKOMOGE {

	@Step("Abilita Confirmation Call")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			

			RemoteWebDriver driver = WebDriverManager.getNewWebDriverNoKill(prop);

			
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				// System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}

				SeleniumUtilities util = new SeleniumUtilities(driver);
				if(prop.getProperty("ESITO_PRECHECK").equals("")){
					throw new Exception ("Lo stato del precheck non ha restituito Esito e Codice Motivazione.");
				}
				
				//prima devo ricercare il canale e sottocanale per l'utenza che sto utilizzando 
				
				String query = "SELECT Username, ITA_IFM_User_Matricola__c, ITA_IFM_Channel__c,ITA_IFM_Subchannel__c " + 
						"FROM User " + 
						"WHERE Username = '"+prop.getProperty("USERNAME")+"'";
				a.insertQuery(query);
				a.pressButton(a.submitQuery);
				if(a.verificaNoRecordFound()) {
					throw new Exception ("La query per recuperare il canale e sottocanale non ha prodotto risultati per l'utenza: "+prop.getProperty("USERNAME"));
				}else {
					a.recuperaRisultati(1, prop);
				}
				
//				Elettrico: and ITA_IFM_Commodity__c = 'ELETTRICO' and ITA_IFM_Result_Code__c = '300' and ITA_IFM_Reason_Code__c = 'R2D_603'
//				Gas: and ITA_IFM_Commodity__c = 'GAS' and ITA_IFM_Result_Code__c = '003' and ITA_IFM_Reason_Code__c = 'DIST_001'
						
					query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_Commodity__c, ITA_IFM_Reason_Code__c, ITA_IFM_Result_Code__c, ITA_IFM_PrecheckOutcome__c, ITA_IFM_FlagMoge__c, ITA_IFM_CloseToBeConfirmedBlocker__c, ITA_IFM_OfferClosingBlocker__c, ITA_IFM_ReworkBlocker__c, ITA_IFM_SendingBlocker__c " + 
							" FROM " + 
							"ITA_IFM_PrecheckResultAdmin__c " + 
							"where ITA_IFM_Channel__c in ('"+prop.getProperty("ITA_IFM_Channel__c".toUpperCase())+"',null) " +
							"and ITA_IFM_Result_Code__c = '"+prop.getProperty("ESITO_PRECHECK")+"' " + 
							"and ITA_IFM_Reason_Code__c in ('"+prop.getProperty("CODICE_MOTIVAZIONE")+"') ";
							if(prop.getProperty("COMMODITY").contains("ELE")) {
								query = query +"and ITA_IFM_Commodity__c = 'ELETTRICO' " ;
									
							}else {
								query = query +"and ITA_IFM_Commodity__c = 'GAS' " ;
								
							}
							query=query+"and ITA_IFM_Status__c = 'Attivo' ";
					
					a.insertQuery(query);
					a.pressButton(a.submitQuery);
					TimeUnit.SECONDS.sleep(10);
					if(a.verificaNoRecordFound()) {
						throw new Exception ("La query non ha restituito la riga di configurazione su SFDC per l’esito 100/001_01");
					}else {
						a.recuperaRisultati(1, prop);
					}
					
					HashMap<String , String> hm = new HashMap<String, String>();
			
					hm.put("ITA_IFM_PrecheckOutcome__c", "KO");
					hm.put("ITA_IFM_SendingBlocker__c", "Y");
					hm.put("ITA_IFM_FlagMoge__c", "true");
					hm.put("ITA_IFM_CloseToBeConfirmedBlocker__c", "Y"); 
					hm.put("ITA_IFM_OfferClosingBlocker__c", "Y");
					hm.put("ITA_IFM_ReworkBlocker__c", "Y");
					  
					a.updateRows(hm);

					
					prop.setProperty("ID_PRECHECK_RESULT", prop.getProperty("ID"));
					//prop.getProperty("ITA_IFM_PrecheckOutcome__c".toUpperCase()
					
				TimeUnit.SECONDS.sleep(3);

				a.logoutWorkbench();

				driver.close();
				
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
