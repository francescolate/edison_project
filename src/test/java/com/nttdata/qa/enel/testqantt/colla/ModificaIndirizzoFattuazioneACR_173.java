package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazioneACR_173 {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			SuplyPrivateDetailComponent spdc = new SuplyPrivateDetailComponent(driver);
			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
		
			logger.write("Home Page Title and Description Verification- Starts");
			mif.comprareText(mif.homepageHeadingRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING_RES, true);
			mif.comprareText(mif.homepageParagraphRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_PARAGRAPH_RES, true);
			mif.comprareText(mif.homepageHeading2RES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING2_RES, true);
//			mif.comprareText(mif.homepageParagraph2RES, ModificaIndirizzoFattuazioneComponent.NUMERO__PARAGRAPH2_RES, true);
			mif.verifyComponentExistence(mif.attiva1);
			mif.verifyComponentExistence(mif.visualizzalLeBollette);
			mif.verifyComponentExistence(mif.dettaglioFornitura);
			mif.verifySupplyComponentExistence(mif.attiva2New, mif.visualizzalLeBollette, mif.dettaglioFornitura);
			logger.write("Home Page Title and Description Verification- Ends");
			
			logger.write("Verify and click the Servizi link - Start");
			spdc.clickComponent(spdc.ServiziLink);
			logger.write("Verify and click the Servizi link - Start");
			
			logger.write("Servizi Page Title and Description Verification- Starts");
//			mif.comprareText(mif.servizipageHeadingRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_HEADING_RES, true);
//			mif.comprareText(mif.servizipageParagraphRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_PARAGRAPH_RES, true);
			logger.write("Servizi Page Title and Description Verification- Ends");
			
			logger.write("Click on Modiffica link - Start");
			spdc.verifyComponentExistence(spdc.ModifficaIndrizzolink);
			spdc.clickComponent(spdc.ModifficaIndrizzolink);
			logger.write("Click on Modiffica link - Complete");
			
			logger.write("Verify the text of Modifica Indirizzo Di Fatturazione Page- Starts");
			Thread.sleep(20000);
			mif.comprareText(mif.mifHeading, ModificaIndirizzoFattuazioneComponent.MIF_HEADING, true);
			mif.comprareText(mif.mifHeading2, ModificaIndirizzoFattuazioneComponent.MIF_PARAGRAPH, true);
			mif.verifyComponentExistence(mif.supplyDetail);
			mif.verifyComponentExistence(mif.infoIcon);
			logger.write("Verify the text of Modifica Indirizzo Di Fatturazione Page- Ends");
			
			logger.write("Click on i and verify the text - Starts");
			mif.clickComponent(mif.infoIcon);
			Thread.sleep(10000);
			mif.comprareText(mif.infoIconPopupHeading, ModificaIndirizzoFattuazioneComponent.POPUP_HEADING_INFO, true);
			mif.comprareText(mif.infoIconPopupText, ModificaIndirizzoFattuazioneComponent.POPUP_PARAGRAPH_INFOICON, true);
			mif.clickComponent(mif.popCloseinfoicon);
			logger.write("Click on i and verify the text - Ends");
			
			logger.write("Verify the text of Modifica Indirizzo Di Fatturazione Page- Starts");
			mif.comprareText(mif.mifHeading, ModificaIndirizzoFattuazioneComponent.MIF_HEADING, true);
			mif.comprareText(mif.mifHeading2, ModificaIndirizzoFattuazioneComponent.MIF_PARAGRAPH, true);
			mif.verifyComponentExistence(mif.supplyDetail);
			mif.verifyComponentExistence(mif.infoIcon);
			logger.write("Verify the text of Modifica Indirizzo Di Fatturazione Page- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		

	}

}
