package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class DataConfiguration {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {

			Field[] fields = Costanti.class.getFields();
			boolean found = false;
			String queryProp=prop.getProperty("QUERY_PROPERTIES");
			String queryRef=prop.getProperty("QUERY_REFERENCES");
			String [] queryPropSplit = queryProp.split(";");
			String [] queryRefSplit = queryRef.split(";");
			if(queryPropSplit.length!=queryRefSplit.length) throw new Exception("Il numero di Query Properties non corrisponde a quello delle Query references.");
			for(int x = 0; x<queryPropSplit.length; x++) {
				String property = queryPropSplit[x];
				String ref = queryRefSplit[x];
				String query = "";
				for (Field field : fields) {
					String name = field.getName();
					if (name.contentEquals(ref)) {
						Object value = field.get(new Costanti());
						query = value.toString();
						found = true;
						break;
					}
				}

				if (!found)
					throw new Exception("Non e' stata trovata nella classe Costanti la variabile "
							+ ref);
				else
					prop.setProperty(property, query);
				
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
