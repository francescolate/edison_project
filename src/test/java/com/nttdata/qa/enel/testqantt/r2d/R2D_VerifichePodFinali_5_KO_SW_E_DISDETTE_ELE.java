package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerificaPodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerifichePodFinali_5_KO_SW_E_DISDETTE_ELE {

	@Step("R2D Verifiche POD Finali 5 KO per SW - ELE")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			String statopratica="vuota";
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Menu Interrogazione
				logger.write("Menu Report Switch e Disdette - Start");
				 menuBox.selezionaVoceMenuBox("Interrogazione","Report Switch e Disdette");
				logger.write("Menu Report Switch e Disdette - Completed");
				R2D_VerificaPodComponent verifichePod = new R2D_VerificaPodComponent(driver);
				//Indice POD
				int indice=Integer.parseInt(prop.getProperty("INDICE_POD","1"));
				if(prop.getProperty("ID_RICHIESTA").isEmpty()){
					logger.write("inserisci Pod - Start");
					 verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD"));
					logger.write("inserisci Pod - Completed");
				} else {
					logger.write("inserisci Id_Richiesta - Start");
					verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA"));
					logger.write("inserisci Id_Richiesta - Completed");
				}
				//Inserimento ID richiesta CRM
//				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA_CRM_ELE_"+indice,prop.getProperty("ID_RICHIESTA_CRM_ELE")));
				//Click Cerca POD
				logger.write("Pressione bottone Cerca - Start");
				 verifichePod.cercaPod(verifichePod.buttonCerca);
				logger.write("Pressione bottone Cerca - Completed");
				TimeUnit.SECONDS.sleep(5);
				//Click Dettaglio Pratiche
				logger.write("Bottone Dettaglio Pratica - Start");
				 verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				logger.write("Bottone Dettaglio Pratica - Completed");
				TimeUnit.SECONDS.sleep(5);
				//Click su Pratica
				logger.write("Apri Pratica - Start");
				 verifichePod.selezionaPratica(verifichePod.rigaPratica);
				logger.write("Apri Pratica - Completed");
				TimeUnit.SECONDS.sleep(5);
				//Verifica Campi
				//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
				campiDaVerificare.add("Stato Pratica R2D;"+"OK");
				campiDaVerificare.add("Esito MUTI;"+"OK");
				//				campiDaVerificare.add("Causale CRM;"+prop.getProperty("CAUSALE_CRM_R2D_ATTESO"));
				//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
//				campiDaSalvare.add("Dispacciamento");
				campiDaSalvare.add("Stato Pratica R2D");
//				campiDaSalvare.add("Codice Pratica Venditore");
				campiDaSalvare.add("Data Switch / Disdetta");
				logger.write("Verifiche e Salvataggio campi Pratica - Start");
				verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);
				logger.write("Verifiche e Salvataggio campi Pratica - Completed");
//				statopratica=prop.getProperty("STATO_PRATICA_R2D");
				statopratica=prop.getProperty("STATO_R2D");
				logger.write("Verifica Finale Stato Pratica = 'KO' - Start");
				if(statopratica.compareToIgnoreCase("KO")!=0){
					logger.write("Verifica Finale Stato Pratica = 'KO' - Failed");
					} 
				logger.write("Verifica Finale Stato Pratica = 'KO' - Completed");
			}
			prop.setProperty("RETURN_VALUE", "KO");
			System.out.println("Verifica Finale Stato: "+ statopratica);
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
