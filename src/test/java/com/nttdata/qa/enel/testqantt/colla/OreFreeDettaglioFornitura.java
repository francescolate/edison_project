package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFreeDettaglioFornituraComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeDettaglioFornitura {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreeDettaglioFornituraComponent ofdf = new OreFreeDettaglioFornituraComponent(driver);
			
			ofdf.verifyComponentExistence(ofdf.oreFreeButton);
			ofdf.clickComponent(ofdf.oreFreeButton);
			
			Thread.sleep(50000);
			
			ofdf.verifyComponentExistence(ofdf.fasciaBaseTextContainer);
			ofdf.comprareText(ofdf.fasciaBaseTextContainer, ofdf.fasciaBaseText, true);
			Thread.sleep(5000);
			ofdf.verifyComponentExistence(ofdf.fasciaOreFreeHeader);
			ofdf.comprareText(ofdf.fasciaOreFreeHeader, ofdf.fasciaOreFreeHeaderText, true);
			Thread.sleep(5000);
			ofdf.verifyComponentExistence(ofdf.fasciaOreFreeDayTimeHeader);
			ofdf.comprareText(ofdf.fasciaOreFreeDayTimeHeader, ofdf.fasciaOreFreeDayTimeText, true);
		
			Thread.sleep(5000);
			ofdf.scrollComponent(ofdf.fasciaOreFreeModificaButton);
			ofdf.verifyComponentExistence(ofdf.fasciaOreFreeModificaButton);
			ofdf.jsClickObject(ofdf.fasciaOreFreeModificaButton);
			
			
			ofdf.verifyComponentExistence(ofdf.fasciaOreFreeModificaDialogHeader);
			ofdf.comprareText(ofdf.fasciaOreFreeModificaDialogHeader, ofdf.fasciaOreFreeModificaDialogHeaderText, true);
			
			ofdf.verifyComponentExistence(ofdf.fasciaOreFreeModificaDialogParagraph);
			ofdf.comprareText(ofdf.fasciaOreFreeModificaDialogParagraph, ofdf.fasciaOreFreeModificaDialogParagraphText, true);
			
			ofdf.verifyComponentExistence(ofdf.fasciaOreFreeModificaDialogFasciaImpostata);
			//ofdf.comprareText(ofdf.fasciaOreFreeModificaDialogFasciaImpostata, ofdf.fasciaOreFreeModificaDialogFasciaImpostataText, true);
			
			ofdf.verifyComponentExistence(ofdf.alleOreSelect);
			ofdf.verifyComponentExistence(ofdf.dalleOreSelect);
			
			Robot robot = new Robot();
			
			while(!ofdf.checkNodeValue(ofdf.dalleOreSelectValue, prop.getProperty("ORE_FREE_ORARIO_DI_START"))){
				ofdf.clickComponent(ofdf.dalleOreSelect);
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(20);
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.delay(20);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(20);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(500);
			}			
			
			//ofdf.comprareText(ofdf.alleOreSelect, "23:00", true);
			ofdf.clickComponent(ofdf.annullaButton);

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
