package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNServiziComponent;
import com.nttdata.qa.enel.components.colla.BSN_InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_264_InfoEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("Verify home page title and page path -- Start");
			PrivateArea_Imprese_HomePageBSNComponent pc = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			pc.verifyComponentExistence(pc.pageTitle);
			pc.comprareText(pc.pagePath, pc.Path, true);
			pc.comprareText(pc.pageTitle, pc.PageTitle, true);
			logger.write("Verify home page title and page path -- Completed");

			logger.write("Click on servizi from left menu,verify page navigation and page title -- Start");
			pc.clickComponent(pc.servizi);
			BSNServiziComponent bc = new BSNServiziComponent(driver);
			bc.verifyComponentExistence(bc.pageTitle);
			bc.comprareText(bc.pagePath, bc.Path, true);
			bc.comprareText(bc.pageText, bc.PageText, true);
			logger.write("Click on servizi from left menu,verify page navigation and page title -- Completed");
			logger.write("Click on Info Enel Energia-- Start");
			bc.clickComponent(bc.infoEnelEnergia);
			logger.write("Click on Info Enel Energia-- Completed");

			logger.write("Verify info enel energia page details-- Start");
			BSN_InfoEnelEnergiaComponent bic = new BSN_InfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			bic.verifyComponentExistence(bic.pageTitle);
			Thread.sleep(10000);
			bic.comprareText(bic.pageTitle, BSN_InfoEnelEnergiaComponent.PageTitle, true);
			bic.comprareText(bic.pageSubText, BSN_InfoEnelEnergiaComponent.PageSubText, true);
			logger.write("Verify info enel energia page details-- End");
			
			logger.write("Click on Prosegui Button - Starts");
			bic.clickComponent(bic.proseguiButton);
			logger.write("Click on Prosegui Button - Ends");

			logger.write("Verify  Info enel enegia page details-- Start");
			BSN_ModificaInfoEnelEnergiaComponent bmc = new BSN_ModificaInfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			bmc.verifyComponentExistence(bmc.pageTitle);
			bmc.comprareText(bmc.ieePath1, BSN_ModificaInfoEnelEnergiaComponent.IEE_PATH1, true);
			bmc.comprareText(bmc.ieePath2, BSN_ModificaInfoEnelEnergiaComponent.IEE_PATH2, true);
			bmc.comprareText(bmc.ieePath3, BSN_ModificaInfoEnelEnergiaComponent.IEE_PATH3, true);
			bmc.comprareText(bmc.ieePageTitle, BSN_ModificaInfoEnelEnergiaComponent.IEE_PAGE_TITLE, true);
			bmc.comprareText(bmc.ieePageSubText, BSN_ModificaInfoEnelEnergiaComponent.IEE_PAGE_SUBTEXT, true);
			bmc.verifyComponentExistence(bmc.customerSupplyCheckbox);
			driver.findElement(By.xpath("(//input[contains(@id,'checkbox')])[1]")).isSelected();
			driver.findElement(bmc.esciButton);
			driver.findElement(bmc.attivaButton);
			logger.write("Verify  Info enel enegia page details-- Ends");
			
			logger.write("Click on Attiva Button - Starts");
			bmc.clickComponent(bmc.attivaButton);
			logger.write("Click on Attiva Button - Ends");
			
			bmc.comprareText(bmc.ieePath1, BSN_ModificaInfoEnelEnergiaComponent.IEE_PATH1, true);
			bmc.comprareText(bmc.ieePath2, BSN_ModificaInfoEnelEnergiaComponent.IEE_PATH2, true);
			bmc.comprareText(bmc.ieePath3, BSN_ModificaInfoEnelEnergiaComponent.IEE_PATH3, true);
			bmc.comprareText(bmc.ieePageTitle, BSN_ModificaInfoEnelEnergiaComponent.IEE_PAGE_TITLE, true);
			bmc.comprareText(bmc.ieePage1SubText, BSN_ModificaInfoEnelEnergiaComponent.IEE_PAGE1_SUBTEXT, true);
			bmc.comprareText(bmc.multifornitura, BSN_ModificaInfoEnelEnergiaComponent.MULTIFORNITURA, true);
			bmc.comprareText(bmc.step1subtext,BSN_ModificaInfoEnelEnergiaComponent.STEP1_SUBTEXT, true);
			
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaSMS);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaSMS);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaEmail);
			
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaSMS);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			bmc.verifyComponentExistence(bmc.avvisoConsumiSMS);
			bmc.verifyComponentExistence(bmc.avvisoConsumiEmail);
			
			bmc.comprareText(bmc.sectionTitle, bmc.SectionTitle, true);
			
			bmc.verifyComponentExistence(bmc.email);
			bmc.verifyComponentExistence(bmc.confirmEmail);
			bmc.verifyComponentExistence(bmc.cellulare);
			bmc.verifyComponentExistence(bmc.confermaCellulare);
		
			//bmc.enterInputToField(bmc.cellulare, prop.getProperty("CELL"));
			//bmc.enterInputToField(bmc.confermaCellulare, prop.getProperty("CELL"));
			bmc.verifyComponentExistence(bmc.aggiornaIDatiDiContatto);
			bmc.clickComponent(bmc.aggiornaIDatiDiContatto);
			
			bmc.selectCheckBox(bmc.avvenutoPagamentoSMS);
			
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.clickComponent(bmc.prosegui);
			bmc.enterInputToField(bmc.cellulare, prop.getProperty("CELL"));
			bmc.enterInputToField(bmc.confermaCellulare, prop.getProperty("CELL"));
			bmc.clickComponent(bmc.prosegui);
			logger.write("Verify Modifica Info enel enegia page,menu one details-- Completed");
			
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Start");
			bmc.verifyComponentExistence(bmc.menuOne);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			
			bmc.comprareText(bmc.supplyDataText, bmc.SupplyDataText, true);
			bmc.verifyComponentExistence(bmc.tipo);
			bmc.verifyComponentExistence(bmc.numeroCliente);
			bmc.verifyComponentExistence(bmc.address);
			bmc.comprareText(bmc.tipoValue, bmc.TipoValue, true);
			bmc.verifyComponentExistence(bmc.numeroClientiValue);
			bmc.verifyComponentExistence(bmc.addressValue);
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.verifyComponentExistence(bmc.confermaButton);
			bmc.clickComponent(bmc.confermaButton);
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Completed");

			logger.write("Verify  Info enel enegia page,menu three details-- Start");
			bic.verifyComponentExistence(bic.infoTitle);
			bic.comprareText(bic.pagePath, bic.PagePath, true);
			bic.comprareText(bic.titleSubText, BSN_InfoEnelEnergiaComponent.TitleSubText, true);
			bic.verifyComponentExistence(bic.estito);
			//bic.comprareText(bic.sectionText, bic.SectionText, true);
			bic.verifyComponentExistence(bic.attivaBollettaWeb);
			bic.verifyComponentExistence(bic.fineButton);
			bic.clickComponent(bic.fineButton);
			logger.write("Verify  Info enel enegia page,menu three details-- Completed");

			logger.write("Verify home page title and page path -- Start");
			pc.verifyComponentExistence(pc.pageTitle);
			pc.comprareText(pc.pagePath, pc.Path, true);
			pc.comprareText(pc.pageTitle, pc.PageTitle, true);
			logger.write("Verify home page title and page path -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
