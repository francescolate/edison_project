package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GiustaPerTeImpresaGasComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CaratteristicheDettaglioGiustaPerTeImpresaGas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			GiustaPerTeImpresaGasComponent offerta=new GiustaPerTeImpresaGasComponent(driver);
			logger.write("Verifica testi presenti nella pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' Sezione Caratteristiche - Start");
			By sezione_Caratteristiche=offerta.sezioneCaratteristiche;
			offerta.verifyComponentExistence(sezione_Caratteristiche);
			offerta.scrollComponent(sezione_Caratteristiche);
			
			By lista_caratteristiche=offerta.listacaratteristiche;
			offerta.verificaSottotitolieTesti(lista_caratteristiche);
			logger.write("Verifica testi presenti nella pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' Sezione Caratteristiche - Completed");
			
			logger.write("Click sul TAB DETTAGLI - Start ");
			By tab_Dettagli=offerta.tabDettagli;
			offerta.verifyComponentExistence(tab_Dettagli);
			offerta.clickComponent(tab_Dettagli);
			TimeUnit.SECONDS.sleep(2);
			By sezione_DettagliLabel=offerta.sezioneDettagliLabel;
			offerta.verifyComponentExistence(sezione_DettagliLabel);
			logger.write("Click sul TAB DETTAGLI - Completed ");
			
			logger.write("verifica testo delle 5 domande e relativo testo di risposta ad esse - Start ");
			By lista_DomandeSezioneDettagli=offerta.listaDomandeSezioneDettagli;
//			offerta.checkQuestionsDetails(lista_DomandeSezioneDettagli,prop.getProperty("LINK")+offerta.linkMINUS_IMAGE,prop.getProperty("LINK")+offerta.linkPLUS_IMAGE);
			offerta.checkQuestionsDetails(lista_DomandeSezioneDettagli,offerta.linkMINUS_IMAGE,offerta.linkPLUS_IMAGE);
			logger.write("verifica testo delle 5 domande e relativo testo di risposta ad esse - Completed ");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
