package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Recupera_Password_Pubblico_42 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			LoginPageValidateComponent lp = new LoginPageValidateComponent(driver);
			
			logger.write("Accessing login page - Start");
			lp.launchLinkBasicAuth(prop.getProperty("LINK"));
			lp.clickComponent(lp.homePageClose);
			lp.verifyComponentExistence(lp.logoEnel);
			lp.verifyComponentExistence(lp.buttonAccetta);
			Thread.sleep(5000);
			lp.clickWithJS(lp.buttonAccetta);
			Thread.sleep(5000);
			lp.verifyComponentExistence(lp.iconUser);
			lp.clickComponent(lp.iconUser);
			logger.write("Accessing login page - Complete");
			
			logger.write("Verify the login problem  - Start");
			lp.verifyComponentExistence(lp.loginProblem);
			lp.compareText(lp.loginProblem, LoginPageValidateComponent.LOGIN_PROBLEM, true);
			logger.write("Verify the login problem  - Complete");
			
			logger.write("Verify the google and facebook buttons  - Start");
			lp.verifyComponentExistence(lp.googleAccessBtn);
			lp.verifyComponentExistence(lp.facebookAccessBtn);
			logger.write("Verify the google and facebook buttons  - Complete");
			
			logger.write("Verify and click on rucepera password link  - Start");
			lp.verifyComponentExistence(lp.rucuperaPassword);
			lp.clickComponent(lp.rucuperaPassword);
			logger.write("Verify and click on rucepera password link  - Complete");
			
			lp.verifyComponentExistence(lp.recuperaHeader);
			lp.compareText(lp.recuperaHeader, LoginPageValidateComponent.RUCUPERAPASSWORD_HEADER, true);
			lp.verifyComponentExistence(lp.recuperaHeader1);
			lp.compareText(lp.recuperaHeader1, LoginPageValidateComponent.RUCUPERAPASSWORD_HEADER1, true);
			//lp.verifyComponentExistence(lp.emaiLabel);
			//lp.verifyText(lp.emaiLabel,prop.getProperty("EMAILLABEL") );

			lp.verifyComponentExistence(lp.emailInput);
			lp.insertText(lp.emailInput, prop.getProperty("EMAIL"));
			lp.verifyComponentExistence(lp.continuaButton);
			lp.clickComponent(lp.continuaButton);
			
			lp.verifyComponentExistence(lp.nuovaPasswordInput);
			Thread.sleep(5000);
			lp.insertText(lp.nuovaPasswordInput, prop.getProperty("NUOVA_PASSWD"));
			
			lp.verifyComponentExistence(lp.nuovaConfermaPassword);
			lp.insertText(lp.nuovaConfermaPassword, prop.getProperty("NUOVACONFERMA_PASSWD"));
			lp.verifyComponentExistence(lp.cofermaButton);
			Thread.sleep(2000);
			lp.clickComponent(lp.cofermaButton);
			
			Thread.sleep(30000);
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
