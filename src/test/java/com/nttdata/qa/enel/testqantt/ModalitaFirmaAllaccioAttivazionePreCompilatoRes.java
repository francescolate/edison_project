package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneAppuntamentoComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModalitaFirmaAllaccioAttivazionePreCompilatoRes {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			
			// controllo che Modalità Firma sia NO Vocal
			logger.write("Controllo che Modalità Firma sia precompilato su No Vocal - Start ");
			modalita.verificaModalitàFirmaPrecompilato(prop.getProperty("MODALITA_FIRMA"));
			logger.write("Controllo che Modalità Firma sia precompilato su No Vocal - Completed ");
			
			
			// controllo che Canale Invio sia EMAIL
			logger.write("Controllo che Canale Invio sia precompilato su No Vocal - Start ");
			modalita.verificaCanaleInvioPrecompilato(prop.getProperty("CANALE_INVIO"));
			logger.write("Controllo che Canale Invio sia precompilato su No Vocal - Completed ");
			
			
			// controllo che email sia precompilato
		logger.write("Controllo che email sia precompilato - Start ");
		modalita.verificaEmailPrecompilato();
		logger.write("Controllo che email sia precompilato - Completed ");
				
		logger.write("Controllo indirizzo prepopolato - Start "); 
		CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
		compila.verificaIndirizzoPrepopolatoSezModalitaFirma(prop.getProperty("REGIOENINDFATT"), prop.getProperty("PROVINCIAINDFATT"), prop.getProperty("COMUNEINDFATT"), prop.getProperty("INDIRIZZOINDFATT"), prop.getProperty("CIVICOINDFATT"), prop.getProperty("CAPINDFATT"), prop.getProperty("LOCALITAINDFATT"));
		logger.write("Controllo indirizzo prepopolato - Completed "); 

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
