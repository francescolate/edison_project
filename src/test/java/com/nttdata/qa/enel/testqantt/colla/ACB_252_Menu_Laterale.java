package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSNMessaggiComponent;
import com.nttdata.qa.enel.components.colla.BSNServiziComponent;
import com.nttdata.qa.enel.components.colla.BSN_ConactDetailsComponent;
import com.nttdata.qa.enel.components.colla.BolleteBSNComponent;
import com.nttdata.qa.enel.components.colla.CaricaDoccumentiComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.components.colla.ReportBillingComponent;
import com.nttdata.qa.enel.components.colla.StatoRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_252_Menu_Laterale {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Verify left menu items -- Start");
			pa.leftMenuItemsDisplay(pa.leftMenuItems);
			logger.write("Verify left menu items -- Completed");
			logger.write("Click on left menu item Bollete and verify page navigation -- Start");
			pa.clickComponent(pa.bolleteMenu);
			Thread.sleep(5000);
			BolleteBSNComponent bc = new BolleteBSNComponent(driver);
			bc.verifyComponentExistence(bc.pageTitle);
			Thread.sleep(10000);
			bc.comprareText(bc.titleSubText, bc.TitleSubText, true);
//			bc.comprareText(bc.pageText, bc.PageText, true);
			logger.write("Click on left menu item Bollete and verify page navigation -- Completed");

			logger.write("Click on left menu item BolleteDaPagare and verify page navigation -- Start");
			pa.clickComponent(pa.bolleteDaPagare);
			Thread.sleep(5000);
			bc.verifyComponentExistence(bc.bolleteDaPagareTitle);
			bc.verifyComponentExistence(bc.bolleteDaPagarePath);
			logger.write("Click on left menu item BolleteDaPagare and verify page navigation -- Completed");

			logger.write("Click on left menu item furniture and verify page navigation -- Start");
			pa.clickComponent(pa.furnitureMenu);
			Thread.sleep(5000);
			FornitureBSNComponent fc = new FornitureBSNComponent(driver);
			pa.verifyComponentExistence(fc.pageTitle);
			pa.verifyComponentExistence(fc.path);
			fc.comprareText(fc.path, fc.Path, true);
			fc.comprareText(fc.pageTitle, fc.PageTitle, true);
			fc.comprareText(fc.titleSubText, fc.TitleSubText, true);
			logger.write("Click on left menu item furniture and verify page navigation -- Completed");

			logger.write("Click on left menu item servizi and verify page navigation and details -- Start");
			pa.clickComponent(pa.servizi);
			Thread.sleep(5000);
			BSNServiziComponent sc = new BSNServiziComponent(driver);
			sc.verifyComponentExistence(sc.pageTitle);
			sc.comprareText(sc.pagePath, sc.Path, true);
			sc.comprareText(sc.pageText, sc.PageText, true);
			logger.write("Click on left menu item servizi and verify page navigation and details -- Completed");
			
			logger.write("Click on left menu item messaggi and verify page navigation and details -- Start");
			pa.clickComponent(pa.messaggiMenu);
			Thread.sleep(5000);
			BSNMessaggiComponent mc = new BSNMessaggiComponent(driver);
			mc.verifyComponentExistence(mc.pageTitle);
			sc.comprareText(mc.path, mc.Path, true);
			sc.comprareText(mc.pageSubText, mc.TitleSubText, true);
			logger.write("Click on left menu item messaggi and verify page navigation and details -- Completed");

			logger.write("Click on left menu item stato richiesta and verify page navigation and details -- Start");
			pa.clickComponent(pa.statoRichiesta);
			Thread.sleep(5000);
			StatoRichiesteComponent stato =new StatoRichiesteComponent(driver);
			stato.verifyComponentExistence(stato.pageTitle);
			sc.comprareText(stato.pagePath, stato.Path, true);
			sc.comprareText(stato.subText, stato.SubText, true);
			logger.write("Click on left menu item stato richiesta and verify page navigation and details -- Completed");

			logger.write("Click on left menu item Report billing management and verify page navigation and details -- Start");
			pa.clickComponent(pa.reportBillingManagement);
			Thread.sleep(5000);
			ReportBillingComponent rc = new ReportBillingComponent(driver);
	//		rc.verifyComponentExistence(rc.pageTitle);
	//		sc.comprareText(rc.path, rc.Path, true);
			logger.write("Click on left menu item Report billing management and verify page navigation and details -- Completed");

			logger.write("Click on left menu item Carica doccumenti and verify page navigation and details -- Start");
			pa.clickComponent(pa.caricaDoccumenti);
			Thread.sleep(5000);
			CaricaDoccumentiComponent cc = new CaricaDoccumentiComponent(driver);
			cc.verifyComponentExistence(cc.pageTitle);
			sc.comprareText(cc.path, cc.Path, true);
			sc.comprareText(cc.pageTitle, cc.PageTitle, true);
			logger.write("Click on left menu item Carica doccumenti and verify page navigation and details -- Completed");

			logger.write("Click on left menu item contatti and verify page navigation and details -- Start");
			pa.clickComponent(pa.contatti);
			Thread.sleep(5000);
			BSN_ConactDetailsComponent cont = new BSN_ConactDetailsComponent(driver);
			cont.verifyComponentExistence(cont.pageTitle);
		//	cont.comprareText(cont.subText, cont.SubText, true);
			cont.comprareText(cont.path, cont.Path, true);
			driver.navigate().back();
			logger.write("Click on left menu item contatti and verify page navigation and details -- Completed");

			logger.write("Click on left menu item logout and verify page navigation and details -- Start");
			pa.clickComponent(pa.logout);
			Thread.sleep(5000);
			String url="https://www-coll1.enel.it/it";
			if(url.equals(driver.getCurrentUrl()))
			logger.write("Click on left menu item logout and verify page navigation and details -- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
