package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSN_47_CustomerSupplyComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.components.colla.SupplyDetailComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Modifica_Addebito_diretto_Conto_Corrente_ACB_339 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
			DettaglioFornitureComponent dfc = new DettaglioFornitureComponent(driver);
			
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Correct visualization of the Home Page with text- Ends");
			
			logger.write("Click on forniture and verify the data- Starts");
			iee.clickComponent(iee.forniture);
			Thread.sleep(2000);
			iee.comprareText(iee.serviziHomepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.fornitureHomepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_FORNITURE_PATH2, true);
			logger.write("Click on forniture and verify the data- Ends");
			
			logger.write("Click on mostra filtri and verify the data- Starts");
			iee.clickComponent(iee.mostraFiltriNew);
			Thread.sleep(2000);
			iee.clickComponent(iee.numeroCliente339New);
			Thread.sleep(2000);
			iee.clickComponent(iee.numeroCliente310492564New); 
			Thread.sleep(2000);
			iee.clickComponent(iee.buttonApplica);
			Thread.sleep(2000);
			iee.clickComponent(iee.vaiAlDetataglio);
			Thread.sleep(2000);
			logger.write("Click on mostra filtri and verify the data- Ends");
			
			logger.write("Click on servizi and verify the data- Starts");
			iee.clickComponent(iee.servizi);
			iee.comprareText(iee.serviziHomepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.serviziHomepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_SERVIZI_PATH2, true);
			iee.comprareText(iee.serviziText, InfoEnelEnergiaACRComponent.SERVIZI_TEXT, true);
			logger.write("Click on servizi and verify the data- Ends");
					
			logger.write("Click on Addebito Diretto and verify the data- Starts");
			dfc.clickComponent(dfc.addebitoDirettoLinkIn);
			iee.comprareText(iee.serviziHomepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.serviziHomepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_SERVIZI_PATH2, true);
			dfc.comprareText(dfc.addebitoDirettoTtile, DettaglioFornitureComponent.AddebitoDirettoTtile, true);
			dfc.comprareText(dfc.addebitoDirettoSubTtile1, DettaglioFornitureComponent.AddebitoDirettoSubTtile1, true);
			dfc.comprareText(dfc.addebitoDirettoSubTtile2, DettaglioFornitureComponent.AddebitoDirettoSubTtile2, true);
			dfc.verifyComponentExistence(dfc.indirizzodella);
			dfc.verifyComponentExistence(dfc.iBAN2);
			logger.write("Click on Addebito Diretto and verify the data- Ends");
						
			logger.write("Click on modifica and verify the data- Starts");
			dfc.clickComponent(dfc.modificaNew);
			Thread.sleep(20000);
			iee.comprareText(iee.serviziHomepagePath1New, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1New, true);
			iee.comprareText(iee.fornitureHomepagePath2New, InfoEnelEnergiaACRComponent.HOMEPAGE_FORNITURE_PATH2New, true);		
			iee.comprareText(iee.modificaTittleNew, InfoEnelEnergiaACRComponent.MODIFICA_TITTLENEW, true);
			iee.comprareText(iee.modificaTittle, InfoEnelEnergiaACRComponent.MODIFICA_TITTLE, true);
			dfc.comprareText(dfc.modificaSubTitle, DettaglioFornitureComponent.ModificaSubTitle, true);
			dfc.comprareText(dfc.modificaSubTitle2, DettaglioFornitureComponent.ModificaSubTitle2, true);
			iee.comprareText(iee.modificaMultifornitura, InfoEnelEnergiaACRComponent.ModificaMultifornitura, true);
			iee.comprareText(iee.modificaItuoidati, InfoEnelEnergiaACRComponent.ModificaItuoidati, true);
			iee.comprareText(iee.modificaRiepilogoeconfermadati, InfoEnelEnergiaACRComponent.ModificaRiepilogoeconfermadati, true);
			iee.comprareText(iee.modificaMultiEsito, InfoEnelEnergiaACRComponent.ModificaMultiEsito, true);
			logger.write("Click on modifica and verify the data- Ends");
			
			logger.write("Verify supply and pROSEGUI- Starts"); 
			dfc.verifyComponentExistence(dfc.signCheckNew310492564);
			dfc.clickComponent(dfc.pROSEGUI);
			Thread.sleep(5000);
			logger.write("Verify supply and pROSEGUI- Ends"); 
			
			logger.write("Enter input data and pROSEGUI- Starts"); 
			iee.comprareText(iee.serviziHomepagePath1New, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1New, true);
			iee.comprareText(iee.fornitureHomepagePath2New, InfoEnelEnergiaACRComponent.HOMEPAGE_FORNITURE_PATH2New, true);		
			iee.comprareText(iee.modificaTittleNew, InfoEnelEnergiaACRComponent.MODIFICA_TITTLENEW, true);
			iee.comprareText(iee.modificaTittle, InfoEnelEnergiaACRComponent.MODIFICA_TITTLE, true);
			iee.comprareText(iee.modificaMultifornitura, InfoEnelEnergiaACRComponent.ModificaMultifornitura, true);
			iee.comprareText(iee.modificaItuoidati, InfoEnelEnergiaACRComponent.ModificaItuoidati, true);
			iee.comprareText(iee.modificaRiepilogoeconfermadati, InfoEnelEnergiaACRComponent.ModificaRiepilogoeconfermadati, true);
			iee.comprareText(iee.modificaMultiEsito, InfoEnelEnergiaACRComponent.ModificaMultiEsito, true);
			dfc.enterInput(dfc.cognome, "IMPRESA MAIRA OPEN ENERGY"); 
			dfc.enterInput(dfc.iBAN, "IT38P0303240021000000000418"); 
			dfc.clickComponent(dfc.pROSEGUI);
			Thread.sleep(5000);
			logger.write("Enter input data and pROSEGUI- Ends");
			
			logger.write("Click on cONFERMA and verify the data- Starts"); 
			iee.comprareText(iee.serviziHomepagePath1New, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1New, true);
			iee.comprareText(iee.fornitureHomepagePath2New, InfoEnelEnergiaACRComponent.HOMEPAGE_FORNITURE_PATH2New, true);		
			iee.comprareText(iee.modificaTittleNew, InfoEnelEnergiaACRComponent.MODIFICA_TITTLENEW, true);
			iee.comprareText(iee.modificaTittle, InfoEnelEnergiaACRComponent.MODIFICA_TITTLE, true);
			iee.comprareText(iee.modificaMultifornitura, InfoEnelEnergiaACRComponent.ModificaMultifornitura, true);
			iee.comprareText(iee.modificaItuoidati, InfoEnelEnergiaACRComponent.ModificaItuoidati, true);
			iee.comprareText(iee.modificaRiepilogoeconfermadati, InfoEnelEnergiaACRComponent.ModificaRiepilogoeconfermadati, true);
			iee.comprareText(iee.modificaMultiEsito, InfoEnelEnergiaACRComponent.ModificaMultiEsito, true);
			iee.comprareText(iee.inserisciText, InfoEnelEnergiaACRComponent.InserisciText, true);
			dfc.clickComponent(dfc.cONFERMA);
			Thread.sleep(5000);
			logger.write("Click on cONFERMA and verify the data- Ends");
			
			logger.write("Click on fine and verify the data- Starts"); 
			iee.comprareText(iee.modificaMultifornitura, InfoEnelEnergiaACRComponent.ModificaMultifornitura, true);
			iee.comprareText(iee.modificaItuoidatiNew, InfoEnelEnergiaACRComponent.ModificaItuoidatiNew, true);
			iee.comprareText(iee.modificaRiepilogoeconfermadatiNew, InfoEnelEnergiaACRComponent.ModificaRiepilogoeconfermadatiNew, true);
			iee.comprareText(iee.modificaMultiEsito, InfoEnelEnergiaACRComponent.ModificaMultiEsito, true);
			dfc.comprareText(dfc.finePage_Text1, DettaglioFornitureComponent.FinePage_Text1, true);
			dfc.comprareText(dfc.finePage_Text2, DettaglioFornitureComponent.FinePage_Text2, true);
			dfc.clickComponent(dfc.fine);
			Thread.sleep(5000);
			logger.write("Click on fine and verify the data- Ends");
			
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Correct visualization of the Home Page with text- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
