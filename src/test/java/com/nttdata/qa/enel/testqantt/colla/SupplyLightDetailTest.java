package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;

import com.nttdata.qa.enel.components.colla.SupplyLightDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SupplyLightDetailTest {


		
		public static void main(String[] args) throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				/*

				LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
				
			   				
				logger.write("apertura del portale web Enel di test - Start");
				log.launchLink(prop.getProperty("LINK"));
				logger.write("apertura del portale web Enel di test - Completed");
				
				logger.write("check sulla presenza del logo Enel - Start");
				By logo = log.logoEnel;
				log.verifyComponentExistence(logo);// verifica esistenza logo enel
				logger.write("check sulla presenza del logo Enel - Completed");
				
				//Click chusura bunner pubblicitario se esistente
				log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

				By accettaCookie = log.buttonAccetta;
				log.verifyComponentExistence(accettaCookie);
				log.clickComponent(accettaCookie);
	            
				logger.write("click su icona utente - Start");
				By icon = log.iconUser;
				log.verifyComponentExistence(icon);
				log.clickComponent(icon); 
				logger.write("click su icona utente - Completed");
				
				logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
				By pageLogin = log.loginPage;
				log.verifyComponentExistence(pageLogin);
				logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
				
				logger.write("check the username and passsword login - Start");
				By user = log.username;
				log.verifyComponentExistence(user);
				log.enterLoginParameters(user, prop.getProperty("USERNAME")); 

				By pw = log.password;
				log.verifyComponentExistence(pw);
				log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
				logger.write("check the username and passsword login - Completed");
				
				logger.write("Click on the login button- Start");
				By accedi = log.buttonLoginAccedi;
				log.verifyComponentExistence(accedi);
				log.clickComponent(accedi); 
				logger.write("Click on the login button- Complete");	
				
				logger.write("Verify the page after successful login - Start");
				By accessLoginPage=log.loginSuccessful;
				log.verifyComponentExistence(accessLoginPage); 
				logger.write("Verify the page after successful login - Complete");
				
				//Login Page
				*/
				SupplyLightDetailComponent sld = new SupplyLightDetailComponent(driver);
				
				logger.write("click on LightTest details page  - Start");
				sld.verifyComponentExistence(sld.DettaligoFornitura);
				sld.clickComponent(sld.DettaligoFornitura);
				logger.write("click on LightTest details page  - Complete");
				
				
				logger.write("Verify Rename and Display Bills buttons - Start");
				sld.verifyComponentExistence(sld.RinominaButton);
				sld.verifyComponentExistence(sld.VisualizzaButton);
				logger.write("Verify Rename and Display Bills buttons  - Completed");
				
				logger.write("Verify the title   - Start");
				sld.verifyComponentExistence(sld.LightTest);
				logger.write("Verify the title   - Compelete");
				
				logger.write("Verify and click on more details button - Start");
				sld.verifyComponentExistence(sld.Mostradi);
				sld.clickComponent(sld.Mostradi);
				logger.write("Verify and click on more details button - Completed");
				
				logger.write("Verify the labels and values  - Start");
				sld.verifyComponentExistence(sld.StatoForitura);
				sld.verifyComponentExistence(sld.StatoForituraValue);
				sld.verifyComponentExistence(sld.StatoPagamento);
				sld.verifyComponentExistence(sld.StatoPagamentoValue);
				sld.verifyComponentExistence(sld.Modalita);
				sld.verifyComponentExistence(sld.ModalitaValue);
				sld.verifyComponentExistence(sld.Ricezione);
				sld.verifyComponentExistence(sld.RicezioneValue);
				sld.verifyComponentExistence(sld.DataDi);
				sld.verifyComponentExistence(sld.DataDiValue);
				sld.verifyComponentExistence(sld.Offerata);
				sld.verifyComponentExistence(sld.OfferataValue);
				sld.verifyComponentExistence(sld.Numero);
				sld.verifyComponentExistence(sld.NumeroValue);
				sld.verifyComponentExistence(sld.POD);
				sld.verifyComponentExistence(sld.PODValue);
				sld.verifyComponentExistence(sld.Potenza);
				sld.verifyComponentExistence(sld.Tenzione);
				sld.verifyComponentExistence(sld.TenzioneValue);
				sld.verifyComponentExistence(sld.ModalitaSotto);
				sld.verifyComponentExistence(sld.ModalitSottoValue);
				sld.verifyComponentExistence(sld.Contratto);
				sld.verifyComponentExistence(sld.ContrattoValue);
				logger.write("Verify the labels and values  - Completed");
				
				logger.write("Click on Rename button  - start");
				sld.clickComponent(sld.RinominaButton);
				logger.write("Click on Rename button  - Completed");
				
				logger.write("Verify the tite,enter the input and click on continue button -- start");
				sld.verifyComponentExistence(sld.InserisciText);
				sld.verifyComponentExistence(sld.NomeFornituraInput);
				sld.enterLoginParameters(sld.NomeFornituraInput, prop.getProperty("INPUTFIELD"));
				sld.clickComponent(sld.ContinuaButton);
				logger.write("Verify the tite,enter the input and click on continue button -- Completed");
				
				
				logger.write("Verify back and confirmation buttons  - start");
				sld.verifyComponentExistence(sld.ConfermaButton);
				sld.verifyComponentExistence(sld.Indietrobuttn);
				logger.write("Verify back and confirmation buttons  - start");
				
				logger.write("Verify the labels and values - start");
				sld.verifyComponentExistence(sld.VerificaText);
				sld.verifyComponentExistence(sld.NomeFornituraTextValue);
				sld.verifyComponentExistence(sld.TipologiaText);
				sld.verifyComponentExistence(sld.TipologiaTextValue);
				sld.verifyComponentExistence(sld.NumeroClienteText);
				sld.verifyComponentExistence(sld.NumeroClienteTextValue);
				logger.write("Verify the labels and values - Completed ");
				
				logger.write("Verify the Status - start");
			    sld.verifyComponentExistence(sld.InserimentoDatiStatus);
			    sld.verifyComponentExistence(sld.RiepilogoStatus);
			    logger.write("Verify the statut - completed");
			    
			    logger.write("click on back and continue button - start");
			    sld.clickComponent(sld.Indietrobuttn);
			    logger.write("click on back and continue button - completed");
			    
			    logger.write("click on continue and confirm button - start");
			    sld.clickComponent(sld.ContinuaButton);
			    sld.clickComponentIfExist(sld.ConfermaButton);
			    logger.write("click on continue and confirm button - completed");
			    
			       
				
			    prop.setProperty("RETURN_VALUE", "OK");
				
			} catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}
				
				
	}

}
