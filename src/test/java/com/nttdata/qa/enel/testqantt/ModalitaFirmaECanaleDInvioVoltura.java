package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneAppuntamentoComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModalitaFirmaECanaleDInvioVoltura {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		SeleniumUtilities util;

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			util = new SeleniumUtilities(driver);

			// controllare che Modalità Firma sia No Vocal

			logger.write("Seleziona Modalità Firma - Start ");
//			modalita.selezionaModalitaFirma(modalita.modalitaFirmaCanaleInvio, prop.getProperty("MODALITA_FIRMA"));
			modalita.selezionaLigtheningValue("Modalità Firma", prop.getProperty("MODALITA_FIRMA", "NO VOCAL"));
			logger.write("Seleziona Modalità Firma - Completed ");


			logger.write("Selezione canale Invio - Start ");
			String tipoCliente = prop.getProperty("TIPO_CLIENTE","").toLowerCase();
			if (prop.getProperty("CANALE_INVIO", "EMAIL").compareTo("EMAIL")==0) {
				
				modalita.selezionaCanaleInvio("EMAIL");
				modalita.sendText("Indirizzo Email", prop.getProperty("EMAIL", "testing.crm.automation@gmail.com"));
			}
			logger.write("Selezione canale Invio - Completed");
			

			modalita.clickComponentIfExist(modalita.confermaButton);
				
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
