package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Identificazione_Interlocutore_SubDP10 {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
			
			logger.write("Identificazione interlocutore - Start");
		
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
			logger.write("Identificazione interlocutore - Completed");
		
			
			logger.write("Documento identità del titolare - Start");
			Thread.currentThread().sleep(5000);
			DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
			
			if(!prop.getProperty("TIPO_DOCUMENTO", "").equals("")) {
				identificaInterlocutore.selezionaLigtheningValue("Tipo documento", prop.getProperty("TIPO_DOCUMENTO"));
			} 
			else 
			{
				identificaInterlocutore.selezionaLigtheningValue("Tipo documento","Passaporto");
			}
			
			if(!prop.getProperty("NUMERO_DOCUMENTO", "").equals("")) {
				identificaInterlocutore.sendText("Numero documento", prop.getProperty("NUMERO_DOCUMENTO"));
			} else {			
				identificaInterlocutore.sendText("Numero documento", "00000");			
			}
			
			if(!prop.getProperty("RILASCIATO_DA", "").equals("")) {
				identificaInterlocutore.sendText("Rilasciato da", prop.getProperty("RILASCIATO_DA"));
			} else {
				identificaInterlocutore.sendText("Rilasciato da", "ccccc");
			}
			
			if(!prop.getProperty("RILASCIATO_IL", "").equals("")) {
				identificaInterlocutore.sendText("Rilasciato il", prop.getProperty("RILASCIATO_IL"));
			} else {
				identificaInterlocutore.sendText("Rilasciato il", "04/03/2019");
			}
			

			identificaInterlocutore.press(identificaInterlocutore.button_conferma);
			logger.write("Documento identità del titolare - Completed");
			
			logger.write("Delegato - Start");
			//delega.selezionaDelega("Delega light");
			//identificaInterlocutore.selezionaLigtheningValue("Tipo delega", "Delega light");
			Thread.currentThread().sleep(2000);
			//identificaInterlocutore.selezionaLigtheningValue("Tipo delega", "Nessuna delega");
//			identificaInterlocutore.press(identificaInterlocutore.confirmButton3);
//			identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
//			delega.verifyComponentExistence(delega.inputTipoDelega);
//			delega.verificaValoreTipoDelega(prop.getProperty("TIPO_DELEGA"));
			identificaInterlocutore.pressButton(identificaInterlocutore.confermaFinaleInBasso);
			logger.write("Delegato - Completed");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
