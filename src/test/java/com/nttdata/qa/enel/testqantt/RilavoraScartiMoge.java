package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.RilavoraScartiComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RilavoraScartiMoge {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			if(prop.getProperty("KO_RIPROCESSABILE").compareTo("OK")==0){
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				SeleniumUtilities util = new SeleniumUtilities(driver);
				driver.switchTo().defaultContent();

				//Click su rilavora scarti
				RilavoraScartiComponent scarti = new RilavoraScartiComponent(driver);
				logger.write("Click pulsante Rilavora scarti - Start");
				scarti.clickRilavoraScarti();
				logger.write("Click pulsante Rilavora scarti - Completed");
				//Click su Forza OK
				scarti.clickForzaturaOK();
				Thread.sleep(10000);
				scarti.checkSpinnersSFDC();
				
				
			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
