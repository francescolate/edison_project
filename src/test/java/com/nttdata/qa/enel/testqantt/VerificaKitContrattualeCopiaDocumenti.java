package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaKitContrattualeCopiaDocumenti {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				logger.write("check esistenza campo 'Cerca Kit Contruattuale' - Start'");
				SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);               
                By campoRicercaKitContr=forn.campoRicercaKitContr;
                forn.verifyComponentExistence(campoRicercaKitContr);              
                TimeUnit.SECONDS.sleep(1);
                
                logger.write("check esistenza campo 'Cerca Kit Contruattuale' - Completed");
			   
				
				logger.write("inserire nel campo 'Cerca' l'ID_DOCUMENTO - Start");
				TimeUnit.SECONDS.sleep(1);
				forn.enterPodValue(campoRicercaKitContr, prop.getProperty("ID_DOCUMENTO_2"));
				TimeUnit.SECONDS.sleep(3);
				logger.write("inserire nel campo 'Cerca' l'ID_DOCUMENTO - Completed");
				
				logger.write("verifica che nella tabella Kit Contrattuale sono riportate le seguenti informazioni: Id Documento, ordinamento discendente, Numero Offerta, Prodotto, Modello, Data Invio Documento, POD/PDR, Processo e selezionare l'id documento di interesse - Start");
				forn.verificaColonneTabellaKitContrattuale(forn.colonne_kit_contruattuale);
				
				TimeUnit.SECONDS.sleep(5);
				forn.clickWithJS(forn.radioButtonKitContrattualeFiglio);
				logger.write("verifica che nella tabella Kit Contrattuale sono riportate le seguenti informazioni: Id Documento, ordinamento discendente, Numero Offerta, Prodotto, Modello, Data Invio Documento, POD/PDR, Processo e selezionare l'id documento di interesse - Completed");
				
				TimeUnit.SECONDS.sleep(5);
										
				logger.write("verifica che il pulsante 'Conferma Kit Contrattuale' sia abilitato e click su questo - Start");
				forn.verifyComponentExistence(forn.buttonConfermaKitContrattualeFiglio);
				forn.clickComponent(forn.buttonConfermaKitContrattualeFiglio);
				logger.write("verifica che il pulsante 'Conferma Kit Contrattuale' sia abilitato e click su questo - Completed");	
					
					gestione.checkSpinnersSFDC();
					
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
