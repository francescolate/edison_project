package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaBolletteBSN {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaBolletteBSNComponent pabc = new PrivateAreaBolletteBSNComponent(driver);
			
			logger.write("cliccando sulla voce di menu bollette - START");
			pabc.clickComponent(pabc.bolletteMenuItem);
			Thread.sleep(10000);
			logger.write("cliccando sulla voce di menu bollette - COMPLETED");
			
			logger.write("verificando lo header della pagina bollette - START");
			pabc.verifyComponentExistence(pabc.bolletteHeader);
			//pabc.compareText(pabc.bolletteHeader, pabc.bolletteHeaderText, true);
			logger.write("verificando lo header della pagina bollette - COMPLETED");
			
			logger.write("verificando il sub header della pagina bollette - START");
			pabc.verifyComponentExistence(pabc.bolletteHeader);
			//pabc.compareText(pabc.bolletteHeaderSub, pabc.bolletteHeaderSubText, true);
			logger.write("verificando il sub header della pagina bollette - COMPLETED");
			
			logger.write("cliccando sulla voce di menu bollette da pagare - START");
			pabc.verifyComponentExistence(pabc.bolletteDaPagareMenuItem);
			pabc.clickComponent(pabc.bolletteDaPagareMenuItem);
			logger.write("cliccando sulla voce di menu bollette da pagare - COMPLETED");
			
			logger.write("verificando lo header della pagina bollette - START");
			pabc.verifyComponentExistence(pabc.bolletteDaPagareHeader);
			pabc.compareText(pabc.bolletteDaPagareHeader, pabc.bolletteDaPagareHeaderText, true);
			logger.write("verificando lo header della pagina bollette - COMPLETED");
			
			logger.write("verificando il sub header della pagina bollette - START");
			pabc.verifyComponentExistence(pabc.bolletteDaPagareHeaderSub);
			pabc.compareText(pabc.bolletteDaPagareHeaderSub, pabc.bolletteDaPagareHeaderSubText, true);
			logger.write("verificando il sub header della pagina bollette - COMPLETED");
			
			logger.write("verificando il sub header della pagina bollette - START");
			//pabc.verifyComponentExistence(pabc.bolletteDaPagareTable);
			//pabc.checkBolletteTableRows(pabc.bolletteDaPagareTableRows);
			logger.write("verificando il sub header della pagina bollette - COMPLETED");
			
			//driver.close();

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
