package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.Luce30SpringComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DocumentiLuce30Spring {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Luce30SpringComponent dettaglio =new Luce30SpringComponent(driver);
			
			logger.write("verifica presenza della sezione documenti:Documenti Contratto e Documenti Generali - Start");
			By tab_Documenti=dettaglio.tabDocumenti;
			dettaglio.verifyComponentExistence(tab_Documenti);
			dettaglio.clickComponent(tab_Documenti);
			TimeUnit.SECONDS.sleep(2);
			By sezione_Documenti=dettaglio.sezioneDocumentiLabel;
			dettaglio.verifyComponentExistence(sezione_Documenti);
						
			By document=dettaglio.documenti;
//Ros 25/05			dettaglio.checkDocumenti(document, prop.getProperty("LINK")+dettaglio.linkPLUS_IMAGE,prop.getProperty("LINK")+dettaglio.linkMINUS_IMAGE);
			dettaglio.checkDocumenti(document, dettaglio.linkPLUS_IMAGE, dettaglio.linkMINUS_IMAGE);
			logger.write("verifica presenza della sezione documenti:Documenti Contratto e Documenti Generali - Completed");
			
			logger.write("Click sulla sezione Documenti Contratto e verifica documenti - Start");
			dettaglio.dettagliDocumentiContratto(document);
			logger.write("Click sulla sezione Documenti Contratto e verifica documenti - Completed");
			
			logger.write("Click per ogni documento nella sezione 'Documenti Contratto' su Download - Start");
			By link_Dowload1=dettaglio.linkDowload1;
			dettaglio.verifyComponentExistence(link_Dowload1);
			dettaglio.checkColor(link_Dowload1,"DeepPink","DOWLOAD");
			String winHandleBefore = driver.getWindowHandle();
			winHandleBefore = driver.getWindowHandle();
			dettaglio.clickComponent(link_Dowload1);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			dettaglio.checkUrl(prop.getProperty("LINK")+dettaglio.linkDOWNLOAD1);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			By link_Dowload2=dettaglio.linkDowload2;
			dettaglio.verifyComponentExistence(link_Dowload2);
			dettaglio.checkColor(link_Dowload1,"DeepPink","DOWLOAD");
			winHandleBefore = driver.getWindowHandle();
			dettaglio.clickComponent(link_Dowload2);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			dettaglio.checkUrl(prop.getProperty("LINK")+dettaglio.linkDOWNLOAD2);
			driver.close();
			driver.switchTo().window(winHandleBefore);
	// Su coll1 non più presenti, redazionale		
//			By link_Dowload3=dettaglio.linkDowload3;
//			dettaglio.verifyComponentExistence(link_Dowload3);
//			dettaglio.checkColor(link_Dowload1,"DeepPink","DOWLOAD");
//			winHandleBefore = driver.getWindowHandle();
//			dettaglio.clickComponent(link_Dowload3);
//			for(String winHandle : driver.getWindowHandles()){
//			    driver.switchTo().window(winHandle);
//			}
//			dettaglio.checkUrl(prop.getProperty("LINK")+dettaglio.linkDOWNLOAD3);
//			driver.close();
//			driver.switchTo().window(winHandleBefore);
//			
//			By link_Dowload4=dettaglio.linkDowload4;
//			dettaglio.verifyComponentExistence(link_Dowload4);
//			dettaglio.checkColor(link_Dowload1,"DeepPink","DOWLOAD");
//			winHandleBefore = driver.getWindowHandle();
//			dettaglio.clickComponent(link_Dowload4);
//			for(String winHandle : driver.getWindowHandles()){
//			    driver.switchTo().window(winHandle);
//			}
//			dettaglio.checkUrl(prop.getProperty("LINK")+dettaglio.linkDOWNLOAD4);
//			driver.close();
//			driver.switchTo().window(winHandleBefore);
			logger.write("Click per ogni documento nella sezione 'Documenti Contratto' su Download - Completed");
			
			logger.write("verifica dei documenti relativi alla sezione 'Documenti Generali' - Start");
			dettaglio.dettagliDocumentiGenerali(document);
			logger.write("verifica dei documenti relativi alla sezione 'Documenti Generali' - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
