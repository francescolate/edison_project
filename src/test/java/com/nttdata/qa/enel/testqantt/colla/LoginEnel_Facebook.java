package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginFacebookComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LoginEnel_Facebook {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
						
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
				
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.verifyComponentExistence(log.recoverPasswordBtn);
			log.verifyComponentExistence(log.recoverUsernameBtn);
			//log.verifyComponentExistence(log.accessProblemsLabel);
			//log.verifyComponentExistence(log.accessProblemsBtn);
			log.verifyComponentExistence(log.googleAccessBtn);
			log.verifyComponentExistence(log.facebookAccessBtn);
			
			logger.write("Click on facebook login-- start");
			log.clickComponent(log.facebookAccessBtn);
			logger.write("Click on facebook login-- Completed");

			LoginFacebookComponent lf = new LoginFacebookComponent(driver);
			Thread.currentThread().sleep(5000);
			lf.clickComponent(lf.accettaTuttiCookie);
			lf.verifyComponentExistence(lf.fbPageTitle);
			logger.write("Enter email address and password-- start");
			lf.enterLoginParameters(lf.email, prop.getProperty("WP_USERNAME"));			
			lf.enterLoginParameters(lf.password, prop.getProperty("WP_PASSWORD"));
			logger.write("Enter email address and password-- Completed");

			logger.write("Click on login-- start");
			lf.verifyCookies();
			lf.clickComponent(lf.loginButton);
			logger.write("Click on login-- Completed");
//			Thread.sleep(1000);
//			lf.clickComponent(By.xpath("//button[@name='__CONFIRM__']"));
//			
			HomeComponent home = new HomeComponent(driver);
			home.verifyComponentExistence(home.pageTitleRes);
			Thread.sleep(10000);
			//home.comprareText(home.pageTitleRes, home.PageTitle,true);
			home.comprareText(home.pageTitleResNew2, home.PageTitle,true);
		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
