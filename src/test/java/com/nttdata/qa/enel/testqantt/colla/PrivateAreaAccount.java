package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CheckPrivateAreaComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaAccountComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaAccount {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaAccountComponent paac = new PrivateAreaAccountComponent(driver);
			String privateAreaHomePageURL = driver.getCurrentUrl();
			
			logger.write("checking whether the menu item Account is present within the page - START");	
			Thread.sleep(5000);
			paac.verifyComponentExistence(paac.accountMenuItem);
			paac.clickComponent(paac.accountMenuItem);
			Thread.sleep(5000);
			logger.write("Account menu item click  - COMPLETED");
			
			logger.write("scrolling to Dati e Contatti- START");
			paac.scrollComponent(paac.datiAndContattiHeader);
			logger.write("scrolling to Dati e Contatti- COMPLETED");
			
			Thread.sleep(3000);
			
			logger.write("checking the text contained within the header of the page - START");
			paac.comprareText(paac.datiAndContattiHeaderText, paac.ACCOUNT_HEADER_TEXT, true);
			logger.write("checking the text contained within the header of the page - COMPLETED");
			
			logger.write("checking whether the button I is present within the page, then it will be clicked - START");	
			paac.verifyComponentExistence(paac.infoButton);
			paac.clickComponent(paac.infoButton);
			logger.write("checking whether the button I is present within the page, then it will be clicked - COMPLETED");
			
			logger.write("checking whether the button X is present within the page, then it will be clicked - START");	
			paac.verifyComponentExistence(paac.closeInfoButton);
			paac.clickComponent(paac.closeInfoButton);
			logger.write("checking whether the button X is present within the page, then it will be clicked - COMPLETED");
			
			logger.write("checking the text contained within the header of the page - START");
			//paac.comprareText(paac.profileInfo, prop.getProperty("PROFILE_INFO_TEXT"), true);
			logger.write("checking the text contained within the header of the page - COMPLETED");		
			
			logger.write("checking whether the button MODIFICA CONTATTI is present within the page, then it will be clicked - START");	
			paac.verifyComponentExistence(paac.modificaContattiButton);
			paac.clickComponent(paac.modificaContattiButton);
			logger.write("checking whether the button MODIFICA CONTATTI is present within the page, then it will be clicked - COMPLETED");
			
			Thread.currentThread().sleep(10000);
			
			if(prop.getProperty("ACTION_TYPE").equals("INDIETRO")){
				logger.write("checking whether the button INDIETRO is present within the page - START");	
				paac.verifyComponentExistence(paac.modificaContattiIndietroButton);
				logger.write("checking whether the button INDIETRO is present within the page - COMPLETED");
				
				logger.write("scrolling indietro Button - START");
				paac.scrollComponent(paac.modificaContattiIndietroButton);
				logger.write("scrolling indietro Button - COMPLETED");
				
				logger.write("clicking INDIETRO button - START");
				paac.clickComponent(paac.modificaContattiIndietroButton);
				logger.write("clicking INDIETRO button - COMPLETED");
				
				Thread.currentThread().sleep(5000);
				logger.write("checking whether redirection has been done correctly - START");
				paac.verifyComponentExistence(paac.datiAndContattiHeader);
				logger.write("checking whether redirection has been done correctly - COMPLETED");
				
			}else if(prop.getProperty("ACTION_TYPE").equals("EMAIL")){
				logger.write("checking whether the field EMAIL is present within the page - START");	
				paac.verifyComponentExistence(paac.emailInput);
				logger.write("checking whether the field EMAIL is present within the page - COMPLETED");
				
				logger.write("scrolling to email field - START");
				paac.scrollComponent(paac.emailInput);
				logger.write("scrolling to email field - COMPLETED");
				
				logger.write("changing email - START");
				paac.changeInputText(paac.emailInput, prop.getProperty("EMAIL"));
				logger.write("changing email - COMPLETED");
				
				logger.write("checking whether the button 'Salva Modifiche' is present within the page, then it will be clicked - START");	
				paac.verifyComponentExistence(paac.modificaContattiSalvaModificheButton);
				paac.clickComponent(paac.modificaContattiSalvaModificheButton);
				logger.write("checking whether the button 'Salva Modifiche' is present within the page, then it will be clicked - COMPLETED");	
				
				logger.write("checking whether the text header after modifying some data - START");	
				Thread.sleep(10000);
				paac.verifyComponentExistence(paac.datiSalvatiHeaderText);
				logger.write("checking whether the text header after modifying some data - COMPLETED");
				
				logger.write("checking the text contained within the header of the page - START");
				paac.comprareText(paac.datiSalvatiHeaderText, paac.SAVED_DATA, true);
				logger.write("checking the text contained within the header of the page - COMPLETED");
				
				logger.write("checking whether the button 'Fine' is present within the page, then it will be clicked - START");	
				paac.verifyComponentExistence(paac.fineButton);
				paac.clickComponent(paac.fineButton);
				logger.write("checking whether the button 'Fine' is present within the page, then it will be clicked - COMPLETED");
				
				Thread.currentThread().sleep(5000);
				logger.write("checking whether redirection has been done correctly - START");
				paac.verifyComponentExistence(paac.loginSuccessful);
				logger.write("checking whether redirection has been done correctly - COMPLETED");
			}
			
			driver.close();

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
