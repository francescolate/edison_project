package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CheckPrivateAreaComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModificaDatiContattoBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaAccountComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaDatiContattoBSN88 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModificaDatiContattoBSNComponent mdcb = new ModificaDatiContattoBSNComponent(driver);
			
			mdcb.verifyComponentExistence(mdcb.logoButton);
			mdcb.clickComponent(mdcb.logoButton);

			mdcb.verifyComponentExistence(mdcb.datiDiContattoButton);
			mdcb.clickComponent(mdcb.datiDiContattoButton);
			
			mdcb.verifyComponentExistence(mdcb.panelBodyHeader);
			//mdcb.comprareText(mdcb.panelBodyHeader, mdcb.panelBodyHeaderText, true);
			
			mdcb.verifyComponentExistence(mdcb.datiDiContattoButton);
			//mdcb.clickComponent(mdcb.prosegui);
			mdcb.clickeaspetta(mdcb.modificaButton);
			Thread.sleep(20000);
			
			if(prop.getProperty("FAKE_PHONE") != null && !prop.getProperty("FAKE_PHONE").equals("")){
			//	String phone = mdcb.modifyExistingPhoneWithAFakeOne(mdcb.telefonoInput, prop.getProperty("FAKE_PHONE"));
				mdcb.verifyComponentExistence(mdcb.proseguiButton);
				mdcb.clickComponent(mdcb.proseguiButton);
			//	mdcb.verifyComponentExistence(mdcb.phoneErrorMessage);
			//	mdcb.comprareText(mdcb.phoneErrorMessage, mdcb.phoneErrorMessageText, true);
			//	mdcb.changeInputText(mdcb.telefonoInput, phone);
			}
			TimeUnit.SECONDS.sleep(10);
			Thread.sleep(10000);
			//prop.setProperty("PHONE", mdcb.modifyExistingPhone(mdcb.telefonoInput));
			//prop.setProperty("EMAIL", mdcb.modifyExistingEmail(mdcb.emailInput));
			Thread.sleep(10000);
			mdcb.verifyComponentExistence(mdcb.esciButton);
			//Uncomment for 87 
			/*Thread.sleep(50000);
			mdcb.verifyComponentExistence(mdcb.proseguiButton);
			mdcb.clickComponent(mdcb.proseguiButton);
			Thread.sleep(10000);*/
			mdcb.clickComponent(mdcb.confermaButton);
			Thread.sleep(50000);
			mdcb.verifyComponentExistence(mdcb.requestSentHeader);
			Thread.sleep(5000);
			mdcb.verifyComponentExistence(mdcb.requestSent);
			//mdcb.comprareText(mdcb.requestSentHeader, mdcb.requestSentHeaderText, true);
			mdcb.comprareText(mdcb.requestSent, mdcb.requestSentText, true);
			
			mdcb.verifyComponentExistence(mdcb.fineButton);
			Thread.sleep(10000);
			mdcb.clickComponent(mdcb.fineButton);
			Thread.sleep(10000);
			mdcb.verifyComponentExistence(mdcb.loginBSNSuccessful);

			prop.store(new FileOutputStream(args[0]), null);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
