package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazione_79 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
			prop.setProperty("SUPPLY_SERVICE", "Addebito Diretto Bolletta Web Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione Autolettura InfoEnelEnergia Opzione Bolletta Rateizzazione ");
			
			logger.write("Verifying the list of devices after clicking on Servizi  STARTS");
			mif.clickComponent(mif.serivzi);
			Thread.sleep(5000);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.SERVIZI_PATH, mif.serviziPath);
			
			mif.comprareText(mif.serviziTitle, ModificaIndirizzoFattuazioneComponent.SERVIZI_TITLE, true);
			mif.checkForSupplyServices(prop.getProperty("SUPPLY_SERVICE"));
			logger.write("Verifying the list of devices after clicking on Servizi  ENDS");
			
			Thread.sleep(5000);
			
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-STARTS");
			mif.clickComponent(mif.modificaIndirizzodiFatturazione);
			Thread.sleep(5000);
			mif.checkMenuPath(mif.INDIRIZZO_PATH, mif.indirizzoDiFatturazionePath);
			mif.comprareText(mif.sectionData, ModificaIndirizzoFattuazioneComponent.SECTION_DATA, true);
			mif.comprareText(mif.sectionBillingAddress, ModificaIndirizzoFattuazioneComponent.SECTION_BILLING_ADDRESS, true);
			mif.comprareText(mif.sectionTechnicalSpec, ModificaIndirizzoFattuazioneComponent.SECTION_TECH_SPECIFICATION, true);
			mif.comprareText(mif.sectionContract, ModificaIndirizzoFattuazioneComponent.SECTION_CONTRACT, true);
			mif.comprareText(mif.clientNumber, prop.getProperty("CLIENT_NUMBER"), true);
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-ENDS");
			
			logger.write("Verifying the content after clicking on Prosegui Button-STARTS");
			mif.clickComponent(mif.modificaButton);
			Thread.sleep(5000);
			mif.clickComponent(mif.indirizziGiaVerificati);
			mif.clickComponent(mif.addressOption1);
			mif.clickComponent(mif.proseguiButton);
			mif.verifyComponentExistence(mif.riepilogoeConfermaDati);
			Thread.sleep(15000);
			mif.comprareText(mif.riepilogoeConfermaDati, mif.REIPILOGO_DATI, true);
			mif.verifyFeildAttribute(mif.provinciaxpath, prop.getProperty("PROVINCE"));
			mif.verifyFeildAttribute(mif.comunexpath, prop.getProperty("COMUNE"));
			mif.verifyFeildAttribute(mif.indrizzoxpath, prop.getProperty("INDIRIZZO"));
			mif.verifyFeildAttribute(mif.numerocivicoxpath, prop.getProperty("NUMERO_CIVICO"));
//			mif.verifyFeildAttribute(mif.localitta, prop.getProperty("LOCALITA"));
//			mif.verifyFeildAttribute(mif.capxpath, prop.getProperty("CAP"));
//			mif.clickComponent(mif.proseguiButton);
//			mif.clickComponent(mif.cliccaQui);
//			mif.enterinputParameters(mif.cap1, "13040");
//			mif.clickComponent(mif.selectCap);
//			mif.clickComponent(mif.proseguiButton);
			mif.comprareText(mif.savedAddress, ModificaIndirizzoFattuazioneComponent.SAVED_ADDRESS, true);
			logger.write("Verifying the content after clicking on Prosegui Button-STARTS");
			
			logger.write("Clicking on Conferma Button-STARTS");
			mif.clickComponent(mif.confermaButton);
			Thread.sleep(30000);
			//mif.verifyComponentExistence(mif.esito);
			mif.verifyComponentExistence(mif.messageOk);
			mif.comprareText(mif.messageOk, ModificaIndirizzoFattuazioneComponent.MESSAGE_OK, true);
			logger.write("Clicking on Conferma Button-ENDS");
			
			logger.write("Clicking on Fine Button-STARTS");
			mif.clickComponent(mif.fineButton);
			logger.write("Clicking on Fine Button-ENDS");
			
			mif.verifyComponentExistence(mif.HomepagePath);
			mif.comprareText(mif.HomepagePath, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_PATH_TEXT, true);
			mif.verifyComponentExistence(mif.homepageHeading);
			mif.comprareText(mif.homepageHeading, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING, true);
						
						
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
