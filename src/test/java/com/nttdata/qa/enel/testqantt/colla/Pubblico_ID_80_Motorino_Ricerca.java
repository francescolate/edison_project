package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.nttdata.qa.enel.components.colla.PubblicoID80MotorinoRicercaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_ID_80_Motorino_Ricerca {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			PubblicoID80MotorinoRicercaComponent mrc = new PubblicoID80MotorinoRicercaComponent(driver);

			logger.write("Accessing the home page - Start");
			mrc.launchLink(prop.getProperty("HOMEPAGE"));
			Thread.sleep(6000);
			mrc.launchLink(prop.getProperty("LINK"));
			Thread.sleep(10000);
			mrc.verifyComponentExistence(mrc.buttonAccetta);
			mrc.clickComponent(mrc.buttonAccetta);
			logger.write("Accessing the home page - Complete");
			
			Thread.sleep(10000);
			prop.setProperty("QUERY1", "Che contratto vuoi attivare?");
			prop.setProperty("QUERY2", "Dove?");
			prop.setProperty("QUERY3", "Per quale necessità?");
			
			mrc.checkURLAfterRedirection(prop.getProperty("LINK"));
			
			mrc.verifyComponentExistence(mrc.cheContratooQuery);
			mrc.comprareText(mrc.cheContratooQuery, prop.getProperty("QUERY1"), true);
			
			mrc.verifyComponentExistence(mrc.doveQuery);
			mrc.comprareText(mrc.doveQuery, prop.getProperty("QUERY2"), true);
			
			mrc.verifyComponentExistence(mrc.perQualeQuery);
			mrc.comprareText(mrc.perQualeQuery, prop.getProperty("QUERY3"), true);
			
			mrc.selectDropDown(mrc.cheContratooQueryDropdown, mrc.cheContratooQueryDropdown_luceEgas);
			mrc.verifyDefaultValue(mrc.cheContratooQueryDropdown, mrc.Query1DropdownValue);
			
			mrc.selectDropDown(mrc.doveQueryDropdown, mrc.doveQueryDropdown_negozioUfficio);
			mrc.verifyDefaultValue(mrc.doveQueryDropdown, mrc.Query2DropdownValue);
			
			mrc.selectDropDown(mrc.perQualeQueryDropdown, mrc.perQualeQueryDropdown_PA);
			mrc.verifyDefaultValue(mrc.perQualeQueryDropdown, mrc.Query3DropdownValue);
			
			mrc.verifyComponentExistence(mrc.button_INIZIA_ORA);
			mrc.clickComponent(mrc.button_INIZIA_ORA);
			Thread.sleep(10000);
			
			prop.setProperty("LUCE_E_GAS_HEADING", "Luce e gas per le imprese");
			prop.setProperty("LUCE_E_GAS_TITLE", "Luce e gas per la tua impresa");
			
			mrc.verifyComponentExistence(mrc.luceEgasHeading);
			mrc.comprareText(mrc.luceEgasHeading, prop.getProperty("LUCE_E_GAS_HEADING"), true);
			
			mrc.verifyComponentExistence(mrc.luceEgasTitle);
			mrc.comprareText(mrc.luceEgasTitle, prop.getProperty("LUCE_E_GAS_TITLE"), true);
			
			mrc.verifyComponentExistence(mrc.luceEgasContent);
			mrc.comprareText(mrc.luceEgasContent, mrc.LuceEgasContentText, true);
			
			mrc.verifyComponentExistence(mrc.visualizzaLuceLink);
			mrc.verifyComponentExistence(mrc.visualizzaGasLink);
			mrc.verifyComponentExistence(mrc.contattaLink);
			
			mrc.clickComponent(mrc.visualizzaLuceLink);
			Thread.sleep(10000);
			
			mrc.checkURLAfterRedirection(prop.getProperty("VISUALIZZA_OFFERTA_LUCE_LINK"));
			
			mrc.verifyDefaultValue(mrc.cheContratooQueryDropdown, mrc.QueryDropdownValue_Luce);
			mrc.verifyDefaultValue(mrc.doveQueryDropdown, mrc.Query2DropdownValue);
			mrc.verifyDefaultValue(mrc.perQualeQueryDropdown, mrc.QueryDropdownValue_VisualizzaTutte);
			
			mrc.backBrowser(mrc.visualizzaGasLink);
			Thread.sleep(10000);
			
			mrc.checkURLAfterRedirection(prop.getProperty("LINK"));
			
			mrc.verifyDefaultValue(mrc.cheContratooQueryDropdown, mrc.Query1DropdownValue);
			mrc.verifyDefaultValue(mrc.doveQueryDropdown, mrc.Query2DropdownValue);
			mrc.verifyDefaultValue(mrc.perQualeQueryDropdown, mrc.Query3DropdownValue);
			mrc.verifyComponentExistence(mrc.button_INIZIA_ORA);
			
			mrc.clickComponent(mrc.visualizzaGasLink);
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("VISUALIZZA_OFFERTA_GAS_LINK"));
			
			mrc.verifyDefaultValue(mrc.cheContratooQueryDropdown, mrc.QueryDropdownValue_Gas);
			mrc.verifyDefaultValue(mrc.doveQueryDropdown, mrc.Query2DropdownValue);
			mrc.verifyDefaultValue(mrc.perQualeQueryDropdown, mrc.QueryDropdownValue_VisualizzaTutte);
			
			mrc.backBrowser(mrc.contattaLink);
			Thread.sleep(10000);
			
			mrc.checkURLAfterRedirection(prop.getProperty("LINK"));
			
			mrc.verifyDefaultValue(mrc.cheContratooQueryDropdown, mrc.Query1DropdownValue);
			mrc.verifyDefaultValue(mrc.doveQueryDropdown, mrc.Query2DropdownValue);
			mrc.verifyDefaultValue(mrc.perQualeQueryDropdown, mrc.Query3DropdownValue);
			mrc.verifyComponentExistence(mrc.button_INIZIA_ORA);
			
			mrc.clickComponent(mrc.contattaLink);
			Thread.sleep(10000);
			
			mrc.checkURLAfterRedirection(prop.getProperty("CONTATTA_LINK"));
			
			mrc.verifyComponentExistence(mrc.pagePath);
			mrc.comprareText(mrc.pagePath, mrc.ContattaciPath, true);
			mrc.verifyComponentExistence(mrc.pageTitle);
			mrc.comprareText(mrc.pageTitle, mrc.ContattaciTitle, true);
			mrc.verifyComponentExistence(mrc.contattaciSubTitle);
			mrc.comprareText(mrc.contattaciSubTitle, mrc.ContattaciSubTitle, true);
			
			
			mrc.backBrowser(mrc.button_INIZIA_ORA);
			Thread.sleep(10000);
			
			mrc.checkURLAfterRedirection(prop.getProperty("LINK"));
			
			mrc.verifyDefaultValue(mrc.cheContratooQueryDropdown, mrc.Query1DropdownValue);
			mrc.verifyDefaultValue(mrc.doveQueryDropdown, mrc.Query2DropdownValue);
			mrc.verifyDefaultValue(mrc.perQualeQueryDropdown, mrc.Query3DropdownValue);
			mrc.verifyComponentExistence(mrc.button_INIZIA_ORA);
			
			mrc.verifyHeader();
			mrc.verifyFooterVisibility();
			
			mrc.clickComponent(mrc.creditsFooterLink);
			Thread.sleep(10000);
			mrc.checkURLAfterRedirection(prop.getProperty("CREDITS_FOOTER_LINK"));
			
			mrc.verifyComponentExistence(mrc.pagePath);
			mrc.comprareText(mrc.pagePath, mrc.CreditsPath, true);
			mrc.verifyComponentExistence(mrc.pageTitle);
			mrc.comprareText(mrc.pageTitle, mrc.CreditsTitle, true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

}
	
}
