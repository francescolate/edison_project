package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FibraDiMelitaComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.components.colla.VoipComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaCoperturaOKFibraDiMelita {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			//Thread.sleep(20000);
			HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
			hamburger.clickComponent(hamburger.fibraDiMelitaLink);
			
			FibraDiMelitaComponent fibra = new FibraDiMelitaComponent(driver);
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Start");
			fibra.checkHeaderPathLinks(fibra.pathLinks1);
			fibra.verifyComponentExistence(fibra.headerTitle);
			fibra.verifyComponentExistence(fibra.headerSubTitle);
			fibra.checkNavBarLinks();
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Completed");
			
			logger.write("Voce di Puntamento Verifica Copertura e check campi - Start");
			fibra.clickComponent(fibra.verificaCoperturaAnchor);
			//fibra.clickComponent(fibra.closeChatHeading);
			fibra.verifyComponentExistence(fibra.comuneField);
			fibra.verifyComponentExistence(fibra.indirizzoField);
			fibra.verifyComponentExistence(fibra.civicoField);
			fibra.verifyComponentExistence(fibra.verificaLaCoperturaButton);
			fibra.verifyComponentExistence(fibra.nonTroviIlTuoIndirizzoLink);
			logger.write("Voce di Puntamento Verifica Copertura e check campi - Completed");
			
			logger.write("Verifica Copertura Esito OK - Start");
			fibra.inserisciIndirizzo(prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
			logger.write("VinserisciIndirizzo - Passed");
			fibra.clickComponent(fibra.verificaLaCoperturaButton);
			logger.write("clickComponent - Passed");
			
			logger.write("Check page FibraOK - Start");
			fibra.checkHeaderPathLinks(fibra.pathLinks2);
			fibra.checkHeaderPage();
			fibra.checkTextPageOK();
			Thread.sleep(10000);
			fibra.checkTextOperatore();
			FooterPageComponent fpc = new FooterPageComponent(driver);
			fpc.verifyFooter();
			logger.write("Check page FibraOK - Completed");
			
//			logger.write("Maggiori Informazioni - Start");
//			fibra.verifyComponentExistence(fibra.maggioriInfo);
//			fibra.clickComponent(fibra.maggioriInfo);
//			logger.write("Maggiori Informazioni - completed");
//			
//			logger.write("Check Voip - Start");
//			Thread.sleep(10000);			
//			for(String winHandle : driver.getWindowHandles()){
//			    driver.switchTo().window(winHandle);
//			}
//			VoipComponent vc = new VoipComponent(driver);
//			vc.confirmUrl(vc.url);
//			vc.verifyComponentExistence(vc.pathBy);
//			vc.verifyComponentExistence(vc.title);
//			vc.verifyComponentExistence(vc.dropbase);
//			vc.verifyDropbase();
//			vc.verifyTextDropbase();
//			logger.write("Check Voip - completed");
//			
//			logger.write("Check Header and Footer - Start");
//			FooterPageComponent fpc2 = new FooterPageComponent(driver);
//			fpc2.verifyFooter();
//			HeaderComponent hc = new HeaderComponent(driver);
//			hc.checkHeaderVisibility();
//			logger.write("Check Header and Footer - Completed");
			
			
			//fibra.checkUrl(prop.getProperty("LINK")+"it/INDIRIZZO_FIBRA_OK");
			/*logger.write("checkUrl - Passed");
			fibra.checkHeaderPathLinks(fibra.pathLinks2);
			logger.write("checkHeaderPathLinks - Passed");
			fibra.verifyComponentExistence(fibra.headerTitle);
			fibra.verifyComponentExistence(fibra.headerSubTitleNew);
			fibra.verifyComponentExistence(fibra.fibraDiMelitaArrivataLabel);
			logger.write("verifyComponentExistence - Passed");
			fibra.verificaCoperturaIndirizzo(prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
			logger.write("verificaCoperturaIndirizzo - Passed");
			fibra.checkElementWithText(fibra.articleh3strongelement, "Ora scegli l'offerta luce/gas di Enel Energia a cui abbinare la fibra di Melita");
			fibra.checkElementWithText(fibra.articlepstrongelement, "Seleziona un'offerta luce o gas e richiedi contestualmente l'adesione a FIBRA DI MELITA.L'attivazione del servizio fibra di Melita avverrà dopo le verifiche tecniche per l'attivazione della tua nuova fornitura Enel Energia.");
			fibra.checkElementWithText(fibra.articledivstrongelement, "Se hai già una fornitura Enel Energia a questo indirizzo puoi accedere all'Area Clienti MyEnel e abbinare alla fornitura luce o gas la fibra di Melita.");
			logger.write("checkElementWithText - Passed");
			logger.write("Verifica Copertura Esito OK - Completed");*/
			
			/*
			logger.write("Click su Accedi All'area My Enel - Start");
			fibra.verifyComponentExistence(fibra.linkAccediEnel);
			fibra.clickComponent(fibra.linkAccediEnel);
			fibra.checkUrl(prop.getProperty("LINK")+"it/login?zoneid=fibra_copertura_confermata-link_contextual_accedi");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			logger.write("Click su Accedi All'area My Enel - Completed");
			*/

			
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			
//			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

	private static VoipComponent verifyTextDropbase() {
		// TODO Auto-generated method stub
		return null;
	}

}
