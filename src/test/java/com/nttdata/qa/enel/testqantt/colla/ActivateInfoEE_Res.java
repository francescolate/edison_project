package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.InfoEEStepsComponent;
import com.nttdata.qa.enel.util.GmailQuickStart;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ActivateInfoEE_Res {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
//			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			InfoEnelEnergiaComponent infoEE = new InfoEnelEnergiaComponent(driver);
			
			System.out.println("Accessing servizi forniture page");
			logger.write("Accessing servizi forniture page - Start");
			infoEE.accessServiziForniture();
			logger.write("Accessing servizi forniture page - Completed");
			
			System.out.println("Accessing InfoEnelEnergia");
			logger.write("Accessing InfoEnelEnergia - Start");
			infoEE.verifyComponentVisibility(infoEE.infoEnelEnergiaBtn);
			infoEE.clickComponent(infoEE.infoEnelEnergiaBtn);
			logger.write("Accessing InfoEnelEnergia - Completed");
			
			System.out.println("Activate InfoEnelEnergia");
			logger.write("Activate InfoEnelEnergia - Start");
			infoEE.verifyComponentVisibility(infoEE.activateInfoEnelEnergiaBtn);
			infoEE.clickComponent(infoEE.activateInfoEnelEnergiaBtn);
			logger.write("Activate InfoEnelEnergia - Completed");
			Thread.sleep(15000);
			logger.write("Saving client number - Start");
			infoEE.baseTimeoutInterval = 90;
			String clientNumber = infoEE.getElementTextString(infoEE.clientNumber);
			infoEE.baseTimeoutInterval = 20;
			System.out.println("Saving client number: " + clientNumber);
			prop.setProperty("CLIENT_NUMBER", clientNumber);
			//Let's insert the actual client number into the query
			String theQuery = prop.getProperty("QUERY");
			theQuery = theQuery.replace("#NUMERO_CLIENTE#", clientNumber);
			System.out.println("Query: " + theQuery);
			prop.setProperty("QUERY", theQuery);
			logger.write("Saving client number - Completed");
			
			System.out.println("Selecting first possible supply");
			logger.write("Selecting first possible supply - Start");
			infoEE.verifyComponentVisibility(infoEE.firstSupplyCheckBox);
			infoEE.clickComponent(infoEE.firstSupplyCheckBox);
			infoEE.verifyComponentVisibility(infoEE.continueBtn);
			infoEE.clickComponent(infoEE.continueBtn);
			logger.write("Selecting first possible supply - Completed");
			
			System.out.println("Step 1");
			logger.write("Step 1 - Start");
			InfoEEStepsComponent steps = new InfoEEStepsComponent(driver);
			steps.checkStep1ElementsExistence();
			steps.modifyItem(steps.comunicaLetturaMailChk);
			steps.changeCertificationMail(prop.getProperty("USERNAME"));
			steps.clickComponent(steps.continueBtn);
			logger.write("Step 1 - Completed");
			
			System.out.println("Step 2");
			logger.write("Step 2 - Start");
			steps.step2Continue();
			logger.write("Step 2 - Completed");
			
			System.out.println("Step 3");
			logger.write("Step 3 - Start");
			steps.checkActivationResults();
			if (steps.activationMailIsCertified) {
				//Let's skip the mail management code execution
				prop.setProperty("MAIL_CERTIFIED", "Y");
				//Let's block the execution of the SFDC login
				prop.setProperty("MODULE_ENABLED", "N");
				System.out.println("Email address already certified");
				logger.write("Email address already certified");
			} else {
				prop.setProperty("MAIL_CERTIFIED", "N");
				System.out.println("Email address NOT certified");
				logger.write("Email address NOT certified");
			}
			steps.clickComponent(steps.endBtnNew);
			logger.write("Step 3 - Completed");
			
			if (prop.getProperty("MAIL_CERTIFIED").equals("N")) {
				
				//To be sure the mail ha been sent, we'll wait for 90 minutes before going on
				Thread.sleep(5400000);
				System.out.println("Getting mail certification link");
				logger.write("Getting mail certification link - Start");
				// Before completing the activation let's set a reference date the code will use to search only the OTP mails received today
				Calendar calendar = new GregorianCalendar();
				// reset hour, minutes, seconds and milliseconds
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				Date mailDate = calendar.getTime();
				System.out.println("Mail search reference date: " + mailDate);
				String certificationLink = GmailQuickStart.recuperaLink2(prop, prop.getProperty("EMAIL_SUBJECT"), mailDate, prop.getProperty("VERIFICATION_STRING"));
				prop.setProperty("CERTIFICATION_LINK", certificationLink);
				driver.get(certificationLink);
				logger.write("Getting mail certification link - Completed");
				
				System.out.println("Verifying thank you page");
				logger.write("Verifying thank you page - Start");
				steps.checkActivationTyp();
				logger.write("Verifying thank you page - Completed");
				
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
