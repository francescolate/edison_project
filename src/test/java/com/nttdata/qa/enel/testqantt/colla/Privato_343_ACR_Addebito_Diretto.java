package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_343_ACR_Addebito_Diretto {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			/*WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
				
			lpv.launchLink(prop.getProperty("WP_LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
												
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("WP_USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("WP_PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			dfc.verifyComponentVisibility(dfc.buttonLoginAccedi);
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");*/
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
			
			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			df.verifyComponentExistence(df.fornitureHeader);
			df.comprareText(df.fornitureHeader, DettaglioFornitureComponent.FORNITURE_HEADER, true);
			df.verifyComponentExistence(df.fornitureContent);
//			df.comprareText(df.fornitureContent, DettaglioFornitureComponent.FORNITURE_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
			
			
			logger.write("Click on Servizi Link  - Start");
			df.verifyComponentExistence(df.serivizLink);
			df.clickComponent(df.serivizLink);
			logger.write("Click on Servizi Link  - Complete");
			
			Thread.sleep(5000);
			
			logger.write("Click on Addebito directo   - Start");
			df.pressJavascript(df.addebittoDirettoLink);
			logger.write("Click on Addebito directo   - Complete");
			
			logger.write("Verify the Addebito directo header   - Start");
			df.verifyComponentExistence(df.addebitoDirectoHeader);
			df.comprareText(df.addebitoDirectoHeader, DettaglioFornitureComponent.ADDEBITTODIRECTO_HEADER, true);
			df.verifyComponentExistence(df.addebitoDirectoContent);
			df.comprareText(df.addebitoDirectoContent, DettaglioFornitureComponent.ADDEBITTODIRECTO_CONTENT, true);
			logger.write("Verify the Addebito directo header  - Complete");
			
			
			df.verifyComponentExistence(df.seVuoiContent);
			df.comprareText(df.seVuoiContent, DettaglioFornitureComponent.SEVUOI_CONTENT, true);
			df.verifyComponentExistence(df.attivoContent);
			df.comprareText(df.attivoContent, DettaglioFornitureComponent.ATTIVO_CONTENT, true);
					
			logger.write("Verify the Indrizzo and Iban value  - Start");
			df.verifyComponentExistence(df.indrizzo_00135);
			df.comprareText(df.indrizzo_00135, DettaglioFornitureComponent.INDRIZZO_00135, true);
			df.verifyComponentExistence(df.iban_it58);
			df.comprareText(df.iban_it58, DettaglioFornitureComponent.IBAN_IT58, true);
			logger.write("Verify the Indrizzo and Iban value  - Complete");
			
			logger.write("Click on Modifica Corrente Button  - Start");
			df.verifyComponentExistence(df.modificaCorrenteButton);
			logger.write("Click on Modifica Corrente Button  - Complete");
			
			logger.write("Click on avvia Richesta link  - Start");
			df.verifyComponentExistence(df.avviaRichestaLink);
			df.clickComponent(df.avviaRichestaLink);
			logger.write("Click on avvia Richesta link  - Start");
			
			logger.write("Verify the dissativa Diretto contents  - Start");
			df.verifyComponentExistence(df.dissativaDiretto);
			df.comprareText(df.dissativaDiretto, DettaglioFornitureComponent.DISSATIVA_DIRETTO, true);
			df.verifyComponentExistence(df.dissativoDirettoContent);
			df.comprareText(df.dissativoDirettoContent, DettaglioFornitureComponent.DISATTIVADIRETTO_CONTENT, true);
			logger.write("Verify the dissativa Diretto contents  - Complete");
			
			logger.write("Select the radio button  for 310490795  - Start");
			df.verifyComponentExistence(df.Radiobtn_310490795);
			df.verifyComponentExistence(df.Radiobtn_310499155);
			df.clickComponent(df.Radiobtn_310490795);
			logger.write("Select the radio button  for 310490795  - Complete");
			
			logger.write("Click on disattivo Addebiton button - Start");
			df.verifyComponentExistence(df.disattivoAddebitoBtn);
			df.clickComponent(df.disattivoAddebitoBtn);
			logger.write("Click on disattivo Addebiton button  - Complete");
			
			logger.write("Verify the disattivando content - Start");
			df.verifyComponentExistence(df.disattivandoContent);
			df.comprareText(df.disattivandoContent, DettaglioFornitureComponent.DISATTVANDO_CONTENT, true);
			df.verifyComponentExistence(df.disattivandoContent1);
			df.comprareText(df.disattivandoContent1, DettaglioFornitureComponent.DISATTVANDO_CONTENT1, true);
			df.verifyComponentExistence(df.numero_310490795);
			df.comprareText(df.numero_310490795, DettaglioFornitureComponent.NUMERO_310490795, true);
			logger.write("Verify the disattivando content - Complete");
			
			logger.write("Click on Conferma button - Start");
			df.verifyComponentExistence(df.disattivaConfermaButton);
			df.clickComponent(df.disattivaConfermaButton);
			logger.write("Click on Conferma button - Complete");
			
			df.verifyComponentExistence(df.direttoFineContent);
			df.comprareText(df.direttoFineContent, DettaglioFornitureComponent.DIRETTOFINE_CONTENT, true);
			df.verifyComponentExistence(df.addebitoFineButton);
			df.clickComponent(df.addebitoFineButton);
			
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
		
			logger.write("Click on Servizi Link  - Start");
			df.verifyComponentExistence(df.serivizLink);
			df.clickComponent(df.serivizLink);
			logger.write("Click on Servizi Link  - Complete");
			
			logger.write("Click on Addebito directo   - Start");
			df.pressJavascript(df.addebittoDirettoLink);
			logger.write("Click on Addebito directo   - Complete");
			
			df.verifyComponentExistence(df.attivaCorrentebtn_310490795);
			df.clickComponent(df.attivaCorrentebtn_310490795);
			
			df.verifyComponentExistence(df.checkbox_310490795);
			df.clickComponent(df.checkbox_310490795);

			df.verifyComponentExistence(df.attivaAddebitoButton);
			df.clickComponent(df.attivaAddebitoButton);
			
			df.enterInput(df.cognomeInput, prop.getProperty("COGNOME"));
			
			df.enterInput(df.nomeInput, prop.getProperty("NOME"));
			df.enterInput(df.codiceInput, prop.getProperty("CODICE"));
			df.verifyComponentExistence(df.ibanInput);
			df.enterInput(df.ibanInput, prop.getProperty("IBAN"));
			
			df.verifyComponentExistence(df.continuaButton);
			df.clickComponent(df.continuaButton);
			

			logger.write("Verify the steps - Start");
		
			df.attivazioneverifyStepStatus(prop.getProperty("INSERTIMOCOLOR"));
			logger.write("Verify the steps - Complte");
			
			df.verifyComponentExistence(df.confermaButton);
			df.clickComponent(df.confermaButton);
			
			df.verifyComponentExistence(df.addebitoFineButton);
			df.clickComponent(df.addebitoFineButton);
		
			prop.setProperty("RETURN_VALUE", "OK");
			
			
		} catch (Exception e) {
			
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	

}
