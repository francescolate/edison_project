package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_121_RES_SWA_ELE_OCR_DUAL_Est_Module {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		int counter = 0; boolean exitCondition;
		QANTTLogger logger = new QANTTLogger(prop);
		
		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OCR_Module_Component omc = new OCR_Module_Component(driver);
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);	
			
			prop.setProperty("POD", "IT004E"+omc.generateRandomPOD(8));
			System.out.print(prop.getProperty("POD"));
			prop.setProperty("PDR", "116900000"+omc.generateRandomPOD(5));
			System.out.print(prop.getProperty("PDR"));

			Thread.sleep(3000);
			
			headerMenu.selectionOptionsFromListbox(headerMenu.contractList,prop.getProperty("TYPE_OF_CONTRACT"),headerMenu.contractList,prop.getProperty("PLACE"),headerMenu.contractList,prop.getProperty("NEED"));	
			TimeUnit.SECONDS.sleep(3);
			headerMenu.clickComponent(headerMenu.iniziaOraButton);
			
			TimeUnit.SECONDS.sleep(3);

			headerMenu.clickComponent(By.xpath("//*[text()='"+prop.getProperty("OFFER") + "']"));
			//headerMenu.clickComponent(By.xpath("//a[@href='"+prop.getProperty("OCR_OFFER_LINK") + "']"));
			
			TimeUnit.SECONDS.sleep(3);

			omc.clickComponent(omc.attivaOraButton);
			
			TimeUnit.SECONDS.sleep(3);
			
			omc.checkHeader("E-Light Bioraria WEB", driver);
			omc.checkHeader2("E-Light Gas WEB", driver);
			
			omc.inserisciDati(prop.getProperty("FIRSTNAME"), prop.getProperty("LASTNAME"), prop.getProperty("CF"), prop.getProperty("MOBILE_NUMBER"), prop.getProperty("EMAIL"));
			
			TimeUnit.SECONDS.sleep(3);
			
			omc.informazioniFornituraLuceGas();
			
			omc.enterPodCap(prop.getProperty("POD"), prop.getProperty("CAP"), prop.getProperty("CITY"));
			omc.enterPdrCap(prop.getProperty("PDR"), prop.getProperty("CAP"), prop.getProperty("CITY"));
			
			TimeUnit.SECONDS.sleep(3);

			omc.checkFornituraLuceGas();
			omc.insertFornituraLuceGas(prop.getProperty("CITY"), prop.getProperty("REGION"), prop.getProperty("ADDRESS"), prop.getProperty("CIVIC"), prop.getProperty("SUPPLIER_LUCE"), prop.getProperty("SUPPLIER_GAS"));
			
			TimeUnit.SECONDS.sleep(5);
		
			//omc.pagamentiBollette(prop.getProperty("EMAIL"), prop.getProperty("MOBILE_NUMBER"));

			//TimeUnit.SECONDS.sleep(5);

			omc.insertIbanAndContinue(true, prop.getProperty("IBAN"));
			
			TimeUnit.SECONDS.sleep(5);

			omc.insertMandatiConsensiLuceGas();
			
			TimeUnit.SECONDS.sleep(5);

			
			omc.checkFinalText(prop.getProperty("EMAIL"));
			
			/*
			do{
				if(!omc.getElementHref(omc.offerDetailsButtonGas).equals(prop.get("OCR_OFFER_LINK"))){
					counter++;
					Thread.sleep(5000);
				}else
					counter=10;
			}while(counter<10);
			
			omc.verifyComponentExistence(omc.offerDetailsButtonGas);
			omc.clickComponent(omc.offerDetailsButtonGas);
			
			omc.verifyComponentExistence(omc.offerActivationButton);
			omc.clickComponent(omc.offerActivationButton);
			
			omc.verifyComponentExistence(omc.compilaManualmenteButton);
			omc.clickComponent(omc.compilaManualmenteButton);
			
			omc.verifyComponentExistence(omc.firstnameInputField);
			omc.clearAndSendKeys(omc.firstnameInputField, prop.getProperty("FIRSTNAME"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.lastnameInputField);
			omc.clearAndSendKeys(omc.lastnameInputField, prop.getProperty("LASTNAME"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.fiscalCodeInputField);
			omc.clearAndSendKeys(omc.fiscalCodeInputField, prop.getProperty("CF"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.mobileInputField);
			omc.clearAndSendKeys(omc.mobileInputField, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.emailInputField);
			omc.clearAndSendKeys(omc.emailInputField, prop.getProperty("EMAIL"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.emailConfirmationInputField);
			omc.clearAndSendKeys(omc.emailConfirmationInputField, prop.getProperty("EMAIL"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.privacyCheckbox);
			omc.clickComponent(omc.privacyCheckbox);
			
			Thread.sleep(1000);
			
			omc.verifyComponentExistence(omc.continueButton);
			omc.clickComponent(omc.continueButton);
			
			Thread.sleep(1000);
			
			System.out.println("Informazioni fornitura - Start");
			omc.informazioniFornituraGas();
			omc.enterPodCap(prop.getProperty("POD_PDR"),"40121");
			System.out.println("Informazioni fornitura - Completed");
			
			
			
			
			Thread.sleep(10000000);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			System.out.println("Check dialog - Start");
			omc.checkSalvaOraContinuaDopo(prop.getProperty("FIRSTNAME"), prop.getProperty("LASTNAME"), prop.getProperty("EMAIL"), prop.getProperty("MOBILE_NUMBER"), prop.getProperty("CF"), driver);
			omc.clickComponent(omc.esciButton);
			System.out.println("Check dialog - Completed");

			System.out.println("Click Altro dialog - Start");
			omc.verifyComponentVisibility(omc.salvaContinuaDopoButton);
			omc.clickComponent(omc.salvaContinuaDopoButton);
			omc.verifyComponentExistence(omc.checkAltro);
			omc.clickComponent(omc.checkAltro);
			omc.verifyComponentVisibility(omc.areaAltro);
			System.out.println("Click Altro dialog - Completed");
			
			System.out.println("Click Non ho le informazioni richieste dialog - Start");
			omc.verifyComponentExistence(omc.checkInfo);
			omc.clickComponent(omc.checkInfo);
			omc.verifyComponentInexistence(omc.areaAltro);
			System.out.println("Click Non ho le informazioni richieste dialog - Completed");
			
			System.out.println("Click Salva ed Esci dialog - Start");
			omc.verifyComponentVisibility(omc.salvaEsciButton);
			omc.clickComponent(omc.salvaEsciButton);
			System.out.println("Click Salva ed Esci dialog - Completed");
			
			System.out.println("Check last page - Start");
			omc.verifyComponentVisibility(omc.finalText);
			omc.containsText(omc.finalText, omc.finalText1);
			omc.containsText(omc.finalText, omc.finalText2);
			omc.verifyComponentExistence(omc.backToHomeButton);
			omc.clickComponent(omc.backToHomeButton);
			System.out.println("Click last page - Completed");
			
*/
			
			prop.setProperty("RETURN_VALUE", "OK");

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
	public static void closeChat(OCR_Module_Component omc) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, 1))
			omc.jsClickComponent(omc.chatCloseButton);
	}
	
	public static void closeChat(OCR_Module_Component omc, int timeout) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, timeout))
			omc.jsClickComponent(omc.chatCloseButton);
	}
}
