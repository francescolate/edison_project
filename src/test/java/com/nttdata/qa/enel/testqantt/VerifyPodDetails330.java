package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.Privato170ModIndFatturazioneACRComponent;
import com.nttdata.qa.enel.components.colla.Privato330ACBAddebitoDirettoComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyPodDetails330 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			Privato330ACBAddebitoDirettoComponent adc = new Privato330ACBAddebitoDirettoComponent(driver);
			
			
			logger.write("typing the user POD - START");	
			adc.verifyComponentExistence(adc.searchBar);
			adc.searchPOD(adc.searchBar, prop.getProperty("POD"), true);
			logger.write("typing the user POD - COMPLETED");
			
			logger.write("Validate and click on POD - START");	
			adc.verifyComponentExistence(adc.selectPod_02660026599153);
//			adc.clickComponent(adc.autoSuggestSelectPod_02660026599153);
			
			adc.clickComponent(adc.selectPod_02660026599153);
			logger.write("Validate and click on POD - COMPLETED");	
			
			logger.write("Validate and click on SERVICE REQUEST LINK - START");
			adc.click_Service_Requests_Link(adc.Service_Requests_Link,"Service Requests");
			logger.write("Validate and click on SERVICE REQUEST LINK - COMPLETED");
			
			logger.write("Verify and select latest date SERVICE REQUEST - START");
			adc.Validate_Service_Request_Section();
			logger.write("Verify and select latest date SERVICE REQUEST - COMPLETED");
			
			logger.write("Store the SR number - START");
			adc.storeServiceRequestId(adc.Service_Requests_RecordID);
			logger.write("Store the SR number - COMPLETED");
			
			logger.write("typing the user POD - START");	
			adc.verifyComponentExistence(adc.searchBar);
			adc.searchPOD(adc.searchBar, prop.getProperty("POD"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("Validate and click on POD - START");
			adc.verifyComponentExistence(adc.selectPod_02660026599153);
			
			adc.clickComponent(adc.selectPod_02660026599153);
			logger.write("Validate and click on POD - COMPLETED");
			
			logger.write("Validate and click on ELEMENTI OPERZIONE LINK - START");
			adc.click_Elementi_Operazione_Link(adc.Elementi_Operazione_Link,"Elementi Operazione");
			logger.write("Validate and click on ELEMENTI OPERZIONE LINK - COMPLETED");
			
			logger.write("Verify and select latest date ORDER_ID or CASE ITEM - START");
			adc.Validate_Elementi_Operazione_Section();
			logger.write("Verify and select latest date ORDER_ID or CASE ITEM - COMPLETED");
			
			logger.write("Verify CASE ITEM details - START");
			adc.verifyComponentExistence(adc.Processo_FieldValue);
			adc.VerifyText(adc.Processo_FieldValue, "ATTIVAZIONE SDD");
			
			adc.verifyComponentExistence(adc.Stato_FieldValue);
			adc.VerifyText(adc.Stato_FieldValue, "In attesa");
			
			adc.verifyComponentExistence(adc.Metodo_di_pagamento_SectionHeading);
			adc.VerifyText(adc.Metodo_di_pagamento_SectionHeading, "Metodo di pagamento");
			
			adc.verifyComponentExistence(adc.Nuovo_Metodo_di_pagamento_FieldValue);
			adc.VerifyText(adc.Nuovo_Metodo_di_pagamento_FieldValue, "SDD");
			
			adc.verifyComponentExistence(adc.Nuovo_titolare_del_conto_bancario_FieldValue);
			adc.VerifyText(adc.Nuovo_titolare_del_conto_bancario_FieldValue, "CARLO EDOARDO");
			
			adc.verifyComponentExistence(adc.CF_del_nuovo_titolare_del_conto_bancario_FieldValue);
			adc.VerifyText(adc.CF_del_nuovo_titolare_del_conto_bancario_FieldValue, "DRDCRL84S14H501N");
			
			adc.verifyComponentExistence(adc.Nuovo_IBAN_FieldValue);
			adc.VerifyText(adc.Nuovo_IBAN_FieldValue, "IT30P0301503200000000246802");
			logger.write("Verify CASE ITEM details - COMPLETED");
			
			logger.write("SWITCH to UDB Platform - START");
			adc.switchToUDBwindow_ClickSubmit(adc.idBpmVALUE, "KO",prop.getProperty("LINK_UDB"));
			logger.write("SWITCH to UDB Platform - COMPLETED");
			adc.refreshBrowser();
			
			logger.write("SWITCH to WB to update SR status - START");
			adc.switchToWb_UpdateServiceRequest(prop.getProperty("SERVICEREQUEST_LINK"),adc.ServiceRequestId);
			logger.write("SWITCH to WB to update SR status - COMPLETED");
			
			adc.refreshBrowser();
			
			logger.write("Click on ANNULMENT BUTTON and perform Annulment - START");
			adc.verifyComponentExistence(adc.AnnulmentButton);
			adc.clickComponent(adc.AnnulmentButton);
			
			adc.switchFrame("ITA_IFM_Case_Items__c.Annulment");
			
//			adc.verifyComponentExistence(adc.AnnulmentDialogTitle);
//			adc.VerifyText(adc.AnnulmentDialogTitle, "Annullamento Richiesta");
			
			adc.selectRinunciaCliente(adc.Rinuncia_cliente_Dropdown);
			logger.write("Click on ANNULMENT BUTTON and perform Annulment - COMPLETED");
			Thread.sleep(3000);
			adc.refreshBrowser();
			
			logger.write("Verify the ANNULMENT STATUS - START");
			adc.verifyComponentExistence(adc.Stato_FieldValue);
			adc.VerifyText(adc.Stato_FieldValue, "Annullato");
			logger.write("Verify the ANNULMENT STATUS - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}