package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_3OK_ELE {

	@Step("R2D Caricamento esiti 3OK - ELE")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				String statopratica=prop.getProperty("STATO_R2D");
				//Se lo Stato Pratica <> AA non è possibile dare il 3-OK
				if (statopratica.equals("AA"))  {
					
					R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
					//Selezione Opzione dal nenù a sinistra
					menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
					logger.write("Menu Code di comunicazione e Caricamento Esiti");
					R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
					//Selezione tipologia Caricamento
					caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
					logger.write("Selezione PS");
					//Inserisci Pod
					if(prop.getProperty("SKIP_POD","N").equals("N")){
					caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD_ELE",prop.getProperty("POD")));
					}
					//Inserisci ID Richiesta CRM
					caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA",prop.getProperty("OI_RICHIESTA",prop.getProperty("OI_ORDINE"))));
					logger.write("Id Richiesta");
					//Cerca
					caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Bottone Cerca");
					//Click button Azione
					caricamentoEsiti.selezionaTastoAzionePratica();
					logger.write("Bottone Azione");
					//Verifica Stato pratica atteso
					caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "AA");
					logger.write("Verifica Stato Pratica Atteso");
					//Selezione evento 3OK
					caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_3OK_ELE"));
					logger.write("Selezionato Evento 3 - OK");
					//Inserimento esito
					caricamentoEsiti.selezioneEsito("OK");
					logger.write("Caricato Evento 3 - OK");
					//Calcolo sysdate
					Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
	//				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String data = simpleDateFormat.format(calendar.getTime()).toString();
					
					//TIPO_OPERAZIONE:
					String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE","");
			
					if (tipoOperazione.equals("PRIMA_ATTIVAZIONE")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("SUBENTRO_R2D")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					if (tipoOperazione.equals("DISDETTA_CON_SUGGELLO")){
						caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
						}
	
					if (tipoOperazione.equals("SUBENTRO_COMBINATO")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
				
					if (tipoOperazione.equals("ALLACCIO")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")){	
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")){		
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("VARIAZIONE_INDIRIZZO_FORNITURA")){		
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("RETTIFICA_MERCATO")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("MOGE")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKMogeAnagraficaELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("CAMBIO_USO")){
						caricamentoEsiti.inserimentoDettaglioEsito3OKCodiceRichiestaELE(prop.getProperty("OI_RICHIESTA"), data);
						}
					
					if (tipoOperazione.equals("MODIFICA_STATO_RESIDENTE")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA",prop.getProperty("OI_ORDINE")), data);
					}
				
					if (tipoOperazione.equals("VARIAZIONE_INDIRIZZO_FATTURAZIONE")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
				
					if (tipoOperazione.equals("VERIFICA_CONTATORE")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("SPOSTAMENTO_CONTATORE_ENTRO_10M")){
						caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
					
					if (tipoOperazione.equals("SPOSTAMENTO_CONTATORE_OLTRE_10M")){
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
					}
	
					if (tipoOperazione.equals("PREDISPOSIZIONE_PRESA")){
						caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneELE(prop.getProperty("OI_RICHIESTA"), data);
						logger.write("Caricamento Dati per Esito 3OK Richiesta N01 Allaccio");
					}
					//Salvataggio stato attuale pratica
					prop.setProperty("STATO_R2D", "CI");
					logger.write("Imposta Stato Pratica = 'CI'");
				} else if (statopratica.equals("CI"))  {
						logger.write("Esitazione 3OK Stato Pratica già in 'CI' ");
					} else if (statopratica.equals("OK")) {
						logger.write("Esitazione 3OK Stato Pratica già in 'OK'");
						} else if (statopratica.equals("AW")) {
							logger.write("Esitazione 3OK: Attenzione Stato Pratica 'AW' necessaria Forzatura! ");
				}
				prop.setProperty("RETURN_VALUE", "OK");
			}
			//
			prop.setProperty("STATUS", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}	
	}
}
