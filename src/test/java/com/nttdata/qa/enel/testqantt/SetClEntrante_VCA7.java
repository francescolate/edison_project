package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SetClEntrante_VCA7 {
	public static void main(String[] args) throws Exception
	{
		Properties prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			logger.write("Settaggio cliente Entrante mediante query - Start");

			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);

			int rigaDaEstrarre=Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE", "1"));



			//SET CLIENTE ENTRANTE
			Map<Integer,Map> lista_Cl_Entrante=wb.EseguiQuery(Costanti.recupera_CF_per_VoltureConAccolloid7_entr);
			Map row=lista_Cl_Entrante.get(rigaDaEstrarre);

			prop.setProperty("CODICE_FISCALE", row.get("NE__Fiscal_code__c").toString());





			logger.write("Settaggio cliente Entrante mediante query - End");


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
