package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_329_ACR_Addebito_Diretto {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			/*WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
				
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
												
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			//dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			//dfc.verifyComponentVisibility(dfc.buttonLoginAccedi);
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");*/
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);

			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			df.verifyComponentExistence(df.fornitureHeader);
			df.comprareText(df.fornitureHeader, DettaglioFornitureComponent.FORNITURE_HEADER, true);
			df.verifyComponentExistence(df.fornitureContent);
			df.comprareText(df.fornitureContent, DettaglioFornitureComponent.FORNITURE_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
						
			logger.write("Click on Servizi link  - Start");
			df.verifyComponentExistence(df.serivizLink);
			df.clickComponent(df.serivizLink);
			logger.write("Click on Servizi link  - Complete");
									
			logger.write("Verify the Bollette servizi header  - Start");
			df.verifyComponentExistence(df.serviziBolletteHeading);
			df.comprareText(df.serviziBolletteHeading, DettaglioFornitureComponent.SERVIZIBOllE_HEADING, true);
			df.verifyComponentExistence(df.seriviziBolletteContent);
			df.comprareText(df.seriviziBolletteContent, DettaglioFornitureComponent.SERVIZIBOLLE_CONTENT, true);
			logger.write("Verify the Bollette servizi header  - Complete");
		
			logger.write("Click on Addebito director   - Start");
			df.verifyComponentExistence(df.addebitoLink);
			df.clickComponent(df.addebitoLink);
			logger.write("Click on Addebito director   - Complete");
						
			logger.write("Verify the Addebito directo header   - Start");
			df.verifyComponentExistence(df.addebitoDirectoHeader);
			df.comprareText(df.addebitoDirectoHeader, DettaglioFornitureComponent.ADDEBITTODIRECTO_HEADER, true);
			df.verifyComponentExistence(df.addebitoDirectoContent);
			df.comprareText(df.addebitoDirectoContent, DettaglioFornitureComponent.ADDEBITTODIRECTO_CONTENT, true);
			logger.write("Verify the Addebito directo header  - Complete");
			
			logger.write("Verify the Indrizzo and Iban label and values   - Start");
			df.verifyComponentExistence(df.indrizzo00144);
			df.comprareText(df.indrizzo00144, DettaglioFornitureComponent.INDRIZZO_00144, true);
			df.comprareText(df.ibanIT45K03, DettaglioFornitureComponent.IBAN_IT45K03, true);
			logger.write("Verify the Indrizzo and Iban label and values   - Complete");	
			
			logger.write("Click on Attiva Corrente  - Start");
			df.verifyComponentExistence(df.modificaCorrente00144);
			df.clickComponent(df.modificaCorrente00144);
			logger.write("Click on Attiva Corrente  - Complete");
			
			df.verifyComponentExistence(df.indirzzoModifica00144);
			df.comprareText(df.indirzzoModifica00144, DettaglioFornitureComponent.INDRIZZOMODIFICA_00144, true);
			df.verifyComponentExistence(df.numeroCliente310492951);
			df.comprareText(df.numeroCliente310492951, DettaglioFornitureComponent.NUMERO_310492951, true);
			df.comprareText(df.modalita, DettaglioFornitureComponent.MODALITA, true);
			
						
			logger.write("Click on info icon  - Start");
			df.verifyComponentExistence(df.iconCircle310492951);
			df.clickComponent(df.iconCircle310492951);
			logger.write("Click on info icon  - Complete");
			
			df.verifyComponentExistence(df.popupHeader);
			df.comprareText(df.popupHeader, DettaglioFornitureComponent.POPUP_HEADER, true);
			df.verifyComponentExistence(df.popupContent);
			df.comprareText(df.popupContent, DettaglioFornitureComponent.POPUP_CONTENT, true);
			
			logger.write("Close the popup window  - Start");
			df.verifyComponentExistence(df.popupClose);
			df.clickComponent(df.popupClose);
			logger.write("Close the popup window  - Complete");
			prop.setProperty("RETURN_VALUE", "OK");
						
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
	

}
