package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.GDPRComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Diritto_di_Portabilita_ACR_195 {

	public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		
		logger.write("Correct visualization of the Home Page with central text- Starts");		
		dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
		dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
		dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
		dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
		logger.write("Correct visualization of the Home Page with central text- Ends");
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		dpc.comprareText(dpc.section1PageHeading3, DirittodiPortabilitaComponent.SECTION1_HEADING3, true);
		dpc.comprareText(dpc.section1PageHeading4, DirittodiPortabilitaComponent.SECTION1_HEADING4, true);
		dpc.comprareText(dpc.section1PageParagraph1,DirittodiPortabilitaComponent.SECTION1_PARAGRAPH1, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Click on privacy policy and verify correct opening of another browser- Starts");
		//===========
		System.out.println(driver.getWindowHandle());
		PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
		
		String currentHandle = driver.getWindowHandle();
		
		dpc.clickComponent(dpc.privacyPolicy);
		pac.switchToNewTab();
		dpc.comprareText(dpc.privacyPolicyHomePagePath1, DirittodiPortabilitaComponent.PRIVACY_POLICY_HOMEPAGE_PATH1, true);
		dpc.comprareText(dpc.privacyPolicyHomePagePath2, DirittodiPortabilitaComponent.PRIVACY_POLICY_HOMEPAGE_PATH2, true);
		dpc.comprareText(dpc.privacyPolicyHomePagePath3, DirittodiPortabilitaComponent.PRIVACY_POLICY_HOMEPAGE_PATH3, true);
		dpc.comprareText(dpc.privacyPolicyHomePageTitle, DirittodiPortabilitaComponent.PRIVACY_POLICY_HOMEPAGE_TITLE, true);
		dpc.comprareText(dpc.privacyPolicyHomePageText, DirittodiPortabilitaComponent.PRIVACY_POLICY_HOMEPAGE_TEXT, true);
		driver.close();
		logger.write("Click on privacy policy and verify correct opening of another browser- Ends");
		
		logger.write("Return to the page with the customer logged in and view the section Diritto di Accesso- Starts");
		pac.switchToOriginaTab(currentHandle);
		dpc.clickComponent(dpc.cookiePolicy);
		pac.switchToNewTab();
		dpc.comprareText(dpc.cookiePolicyHomePagePath1, dpc.COOKIE_POLICY_HOMEPAGE_PATH1, true);
		dpc.comprareText(dpc.cookiePolicyHomePagePath2, dpc.COOKIE_POLICY_HOMEPAGE_PATH2, true);
		dpc.comprareText(dpc.cookiePolicyHomePagePath3, dpc.COOKIE_POLICY_HOMEPAGE_PATH3, true);
		dpc.comprareText(dpc.cookiePolicyHomePageTitle, DirittodiPortabilitaComponent.COOKIE_POLICY_HOMEPAGE_TITLE, true);
		dpc.comprareText(dpc.cookiePolicyHomePageText, dpc.COOKIE_POLICY_HOMEPAGE_TEXT, true);
		driver.close();
		pac.switchToOriginaTab(currentHandle);
		
		dpc.comprareText(dpc.section1H3DirittoDiAccesso, DirittodiPortabilitaComponent.DIRITTO_DI_ACCESSO_H3, true);
		dpc.comprareText(dpc.section1H4DirittoDiAccesso, DirittodiPortabilitaComponent.DIRITTO_DI_ACCESSO_H4, true);
		dpc.comprareText(dpc.section1PageParagraph2, DirittodiPortabilitaComponent.SECTION1_PARAGRAPH2, true);
		logger.write("Return to the page with the customer logged in and view the section Diritto di Accesso- Ends");
		
		logger.write("Click on sezione account and verify correct display of the page- Starts ");
		dpc.clickComponent(dpc.sezioneAccount);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.sezioneAccountTitle, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TITLE, true);
		dpc.comprareText(dpc.sezioneAccountText, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TEXT, true);
		logger.write("Click on sezione account and verify correct display of the page- Ends ");
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Click on Addebito Diretto and verify correct display of the page- Starts ");
		dpc.clickComponent(dpc.addebitoDiretto);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.addebitoDirettoTitle, DirittodiPortabilitaComponent.ADDEBITO_DIRETTO_TITLE, true);
		dpc.comprareText(dpc.addebitoDirettoText, DirittodiPortabilitaComponent.ADDEBITO_DIRETTO_TEXT, true);
		logger.write("Click on Addebito Diretto and verify correct display of the page- Ends ");
		
		logger.write("Click on I TUOI DIRITTI  and verify Correct pop up display with NO and SI buttons. Starts ");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I TUOI DIRITTI  and verify Correct pop up display with NO and SI buttons. Ends ");
		
		logger.write("Click on selezionando una tua fornitura and verify correct display of the page- Starts ");
		dpc.clickComponent(dpc.selezionandoUnaTuaFornitura);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
		dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
		dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
		dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
		logger.write("Click on selezionando una tua fornitura and verify correct display of the page- Ends ");
		
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Section view Diritto di Cancellazione- Starts");
		dpc.comprareText(dpc.dirittoDiCancellationTitle1, DirittodiPortabilitaComponent.DIRITTO_DI_CANCELLATION_TITLE1, true);
		dpc.comprareText(dpc.dirittoDiCancellationTitle2, DirittodiPortabilitaComponent.DIRITTO_DI_CANCELLATION_TITLE2, true);
		dpc.comprareText(dpc.dirittoDiCancellationText1, DirittodiPortabilitaComponent.DIRITTO_DICANCELLATION_TEXT1, true);
		dpc.comprareText(dpc.dirittoDiCancellationText3, DirittodiPortabilitaComponent.DIRITTO_DICANCELLATION_TEXT3, true);
		dpc.comprareText(dpc.dirittoDiCancellationText2, DirittodiPortabilitaComponent.DIRITTO_DICANCELLATION_TEXT2, true);
		logger.write("Section view Diritto di Cancellazione- Ends");
		
		logger.write("Section view Diritto di Limitazione- Starts");
		dpc.comprareText(dpc.dirittoDiLimitazioneText, DirittodiPortabilitaComponent.DIRITTO_DI_LIMITAZIONE_TEXT, true);
		logger.write("Section view Diritto di Limitazione- Ends");
		
		logger.write("Section view Diritto di Opposizione- Starts");
		dpc.comprareText(dpc.dirittoDiOpposizioneText, DirittodiPortabilitaComponent.DIRITTO_DI_OPPOSIZIONE_TEXT, true);
		logger.write("Section view Diritto di Opposizione- Ends");
		
		logger.write("Click on Sezione Account and verify the correct display of text- Starts");
		dpc.clickComponent(dpc.sezioneAccountOpposizione);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.sezioneAccountTitle, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TITLE, true);
		dpc.comprareText(dpc.sezioneAccountOpposizioneText, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TEXT, true);
		logger.write("Click on Sezione Account and verify the correct display of text- Ends");
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Section view Diritto di Rettifica- Starts");
		dpc.comprareText(dpc.dirittoDiRettificaText, DirittodiPortabilitaComponent.DIRITTO_RETTIFICA, true);
		logger.write("Section view Diritto di Rettifica- Ends");
		
		logger.write("Click on Sezione Account and verify the correct display of text- Starts");
		dpc.clickComponent(dpc.sezioneAccountRettifica);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.sezioneAccountTitle, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TITLE, true);
		dpc.comprareText(dpc.sezioneAccountRettificaText, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TEXT, true);
		logger.write("Click on Sezione Account and verify the correct display of text- Ends");
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Click on Addebito Diretto and verify correct display of the page- Starts ");
		dpc.clickComponent(dpc.addebitoDiretto);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.addebitoDirettoTitle, DirittodiPortabilitaComponent.ADDEBITO_DIRETTO_TITLE, true);
		dpc.comprareText(dpc.addebitoDirettoText, DirittodiPortabilitaComponent.ADDEBITO_DIRETTO_TEXT, true);
		logger.write("Click on Addebito Diretto and verify correct display of the page- Ends ");
		
		logger.write("Click on I TUOI DIRITTI  and verify Correct pop up display with NO and SI buttons. Starts ");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I TUOI DIRITTI  and verify Correct pop up display with NO and SIs buttons. Ends ");
		
		logger.write("Section view Diritto di Portabilita- Starts");
		//dpc.comprareText(dpc.dirittoDiPortabilitaText, DirittodiPortabilitaComponent.DIRITTO_PORTABILITA, true);
		logger.write("Section view Diritto di Portabilita- Ends");
		
		logger.write("Click on Sezione Account and verify the correct display of text- Starts");
		dpc.clickComponent(dpc.sezioneAccountPortabilita);
		dpc.comprareText(dpc.popupHeading, DirittodiPortabilitaComponent.POPUP_HEADING, true);
		dpc.comprareText(dpc.popupParagraph, DirittodiPortabilitaComponent.POPUP_PARAGRAPH, true);
		dpc.verifyComponentExistence(dpc.popupNoButton);
		dpc.verifyComponentExistence(dpc.popupSIButton);
		dpc.clickComponent(dpc.popupSIButton);
		dpc.comprareText(dpc.sezioneAccountTitle, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TITLE, true);
		dpc.comprareText(dpc.sezioneAccountPortabilitaText, DirittodiPortabilitaComponent.SEZIONE_ACCOUNT_TEXT, true);
		logger.write("Click on Sezione Account and verify the correct display of text- Ends");
		
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Starts");
		dpc.clickComponent(dpc.iTuoiDiritti);
		dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
		dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
		logger.write("Click on I tuoi diritti on left menu item and verify Correct visualization page text- Ends");
		
		logger.write("Verify ACCEDI AL SERVIZIO button- Starts");
		dpc.verifyComponentExistence(dpc.accediAlServizioButton);
		logger.write("Verify ACCEDI AL SERVIZIO button- Ends");
		
		logger.write("Verify Header is not Displayed-Starts");
		dpc.isElementNotPresent(dpc.headerDisplay);
		logger.write("Verify Header is not Displayed-Starts");
		
		logger.write("Verify Footer Display-Starts");
		dpc.comprareText(dpc.footerDisplay, DirittodiPortabilitaComponent.FOOTER_DISPLAY, true);
		logger.write("Verify Footer Display-Starts");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
