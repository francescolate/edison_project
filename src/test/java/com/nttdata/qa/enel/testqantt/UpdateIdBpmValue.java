package com.nttdata.qa.enel.testqantt;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RichiestaCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class UpdateIdBpmValue {

	@Step("Cancel item using CF")
	public static void main(String[] args) throws Exception {
			
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			try 
			{
				
				RichiestaCollaComponent rcc = new RichiestaCollaComponent(driver);			
				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				
				page.navigate(prop.getProperty("LINK_UDB"));
				rcc.enterInputParameters(rcc.iDbmpValueInput, prop.getProperty("iDbmpValue"));
				rcc.clickComponent(rcc.esitoOptionKO);
				Thread.sleep(1000);
				rcc.clickComponent(rcc.submitButton2);
				Thread.sleep(50000);
				Thread.sleep(20000);
							
				prop.setProperty("RETURN_VALUE", "OK");
				
			} catch (Exception e) {
				prop.setProperty("RETURN_VALUE", "KO");
				
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
				
			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
			}
		}
}

