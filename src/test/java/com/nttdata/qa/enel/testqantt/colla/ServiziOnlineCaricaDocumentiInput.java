package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ServiziOnlineCaricaDocumentiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ServiziOnlineCaricaDocumentiInput {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			
			ServiziOnlineCaricaDocumentiComponent sodc = new ServiziOnlineCaricaDocumentiComponent(driver);
		
					
			logger.write("Insert Value - START");
			
			sodc.verifyComponentExistence(sodc.inputNome);
			sodc.verifyComponentExistence(sodc.inputCognome);
			sodc.verifyComponentExistence(sodc.inputCellulare);
			sodc.verifyComponentExistence(sodc.indirizzoBy);
			sodc.verifyComponentExistence(sodc.inputEmail);
			sodc.verifyComponentExistence(sodc.inputConfermaEmail);
			sodc.verifyComponentExistence(sodc.codiceFiscalePIvaBy);
			sodc.comprareText(sodc.seiGiaClienteBy, sodc.seiGiaCliente, true);
			
			sodc.verifyComponentExistence(sodc.si);
			sodc.verifyComponentExistence(sodc.no);
//			sodc.isDefaultRadioSelected(sodc.si);
			
			sodc.comprareText(sodc.diCosaHaiBisognoBy, sodc.diCosaHaiBisogno, true);
			sodc.verifyComponentExistence(sodc.inviareUnaRichiestaBy);
			sodc.verifyComponentExistence(sodc.informazioniBy);
			
			sodc.isDefaultRadioSelected(sodc.inviareUnaRichiestaBy);
			
			sodc.verifyComponentExistence(sodc.argomentoBy);
			sodc.verifyComponentExistence(sodc.messaggioBy);
			
			sodc.verifyComponentExistence(sodc.vuoiInserireAllegatiBy);
			sodc.comprareText(sodc.vuoiInserireAllegatiBy, sodc.vuoiInserireAllegati, true);
			sodc.verifyComponentExistence(sodc.vuoiInserireAllegatiSiBy);
			sodc.verifyComponentExistence(sodc.vuoiInserireAllegatiNoBy);
//			sodc.isDefaultRadioSelected(sodc.vuoiInserireAllegatiSiBy);
			
			sodc.verifyComponentExistence(sodc.numeroClienteBy);
			
			sodc.comprareText(sodc.informativaTextBy, sodc.informativaText, true);
		
			sodc.verifyComponentExistence(sodc.accettoBy);
			sodc.verifyComponentExistence(sodc.nonAccettoBy);
			sodc.verifyComponentExistence(sodc.proseguiBy);
			sodc.comprareText(sodc.campiObbligatoriBy, sodc.campiObligatori, true);

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
