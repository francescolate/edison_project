package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class VerificaSezionePianoDiRientroCopiaDocumenti {

    public static void main(String[] args) throws Exception {
        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            DettagliSpedizioneCopiaDocComponent forn = new DettagliSpedizioneCopiaDocComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            SezioneSelezioneFornituraComponent aut = new SezioneSelezioneFornituraComponent(driver);

            util.getFrameActive();

            logger.write("check esistenza campo 'Ricerca piano di rientro' - Start'");
            By campoRicercaPianoRientro = forn.buttonRicercaPianoDiRientro;
            forn.verifyComponentExistence(campoRicercaPianoRientro);
            logger.write("check esistenza campo 'Ricerca piano di rientro' - Completed");

            logger.write("inserire nel campo 'Ricerca piano di rientro' il Numero Pdr - Start");
            aut.enterPodValue(campoRicercaPianoRientro, prop.getProperty("NUMERO_PDR"));
            logger.write("inserire nel campo 'Ricerca piano di rientro' il Numero Pdr - Completed");

            logger.write("verifica che nella tabella della sezione piano di rientro sono riportate le seguenti informazioni: Id Documento, Data Pagamento, Modalità Pagamento, Importo Pagato, Numero Fattura e selezionare l'Id documento di interesse - Start");
            aut.verificaColonneTabellaSezionePianoDiRientro(forn.colonneSezionePianoDiRientro);
            forn.clickWithJS(forn.radioButtonSezionePianoDiRientro);
            logger.write("verifica che nella tabella della sezione piano di rientro sono riportate le seguenti informazioni: Id Documento, Data Pagamento, Modalità Pagamento, Importo Pagato, Numero Fattura e selezionare l'Id documento di interesse - Completed");


            logger.write("verifica che il pulsante 'Conferma piano di rientro' sia abilitato e click su questo - Start");
            forn.verifyComponentExistence(forn.buttonConfermaPianoDiRientro);
            forn.clickComponent(forn.buttonConfermaPianoDiRientro);
            logger.write("verifica che il pulsante 'Conferma piano di rientro' sia abilitato e click su questo - Completed");

            gestione.checkSpinnersSFDC();

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }


    }

}
