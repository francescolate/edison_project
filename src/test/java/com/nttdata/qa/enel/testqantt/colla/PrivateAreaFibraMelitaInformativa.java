package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFibraMelitaInformativa {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);
			//STEP 7 Verifica Informativa (i)
			
			logger.write("click the information icon - START");
			
			By id_frame = paf.id_frame;
			
			//paf.verifyComponentExistence(paf.id_frame);	
			
			//paf.verifyFrameAvailableAndSwitchToIt(id_frame);
			
			By icon_informativa = paf.icon_informativa2;
			
		//	paf.verifyComponentExistence(paf.icon_informativa2);

		//	paf.clickElementByIndex(icon_informativa, 1);

			logger.write("click the information icon - COMPLETED");
			
		/*	logger.write("check the popup title and description text - START");
			
			paf.verifyComponentExistence(paf.text_popup_title);
			paf.verifyComponentVisibility(paf.text_popup_title);
			
			logger.write("check the popup title text - COMPLETED");
			
			logger.write("check the popup descrition text - START");
			
			paf.verifyComponentExistence(paf.text_popup_text);
			paf.verifyComponentVisibility(paf.text_popup_text);
			
			paf.verifyComponentExistence(paf.text_popup_titledescription2);
			paf.verifyComponentVisibility(paf.text_popup_titledescription2);
			
			logger.write("check the popup title and description text - COMPLETED");*/

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
