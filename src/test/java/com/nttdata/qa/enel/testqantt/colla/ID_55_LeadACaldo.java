package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.LeadACaldoComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_55_LeadACaldo {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			LeadACaldoComponent lead = new LeadACaldoComponent(driver);
			String homePageUrl = "https://www-coll1.enel.it/";
						
			prop.setProperty("NOME", "Mario");
			prop.setProperty("COGNOME", "Rossi");
			prop.setProperty("CELLULARE", "2568945825");
			
			//1
			logger.write("apertura del portale web Enel di test - Start");
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
 			logger.write("apertura del portale web Enel di test - Completed");
			
			lead.comprareText(lead.compilaModulaHeading1, LeadACaldoComponent.COMPILA_MODULA_HEADING1, true);
			lead.comprareText(lead.compilaModulaHeading2, LeadACaldoComponent.COMPILA_MODULA_HEADING2, true);
				
			logger.write("apertura del portale web Enel di test - Completed");
			
			//2
			logger.write("Verification of fields nome,cognome and cellulare- Start");
			lead.verifyComponentExistence(lead.nome);
			lead.verifyComponentExistence(lead.cognome);
			lead.verifyComponentExistence(lead.cellulare);
			logger.write("Verification of fields nome,cognome and cellulare- End");
			
			//3
			logger.write("Verification of Informativa text content- Start");
			lead.comprareText(lead.InformativaHeading, LeadACaldoComponent.INFORMATIVA_HEADING, true);
			lead.comprareText(lead.InformativaText, LeadACaldoComponent.INFORMATIVA_TEXT, true);
			logger.write("Verification of Informativa text content- End");
			
			//4
			logger.write("Verification of accetto, non-accetto and prosegui button- Start");
			lead.verifyComponentExistence(lead.accetto);
			lead.verifyComponentExistence(lead.nonAccetto);
			lead.comprareText(lead.campiObligatori, LeadACaldoComponent.CAMPI_OBBLIGATORI, true);
			lead.verifyComponentExistence(lead.proseguiButton);
			logger.write("Verification of accetto, non-accetto and prosegui button- End");
			
			//5
			logger.write("Verification of Footer - Start ");
			logger.write("Verifica Footer - Start");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			prop.setProperty("BEFORE_FOOTER_TEXT", "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.");
			prop.setProperty("AFTER_FOOTER_TEXT", "Gruppo IVA Enel P.IVA 15844561009");
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By linkInformazioniLegali=footer.informazioniLegaliLink;
			footer.verifyComponentExistence(linkInformazioniLegali);
			footer.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=footer.creditsLink;
			footer.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=footer.privacyLink;
			footer.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=footer.cookieLink;
			footer.verifyComponentExistence(linkCookie);
			
			By linkRemit=footer.remitLink;
			footer.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=footer.twitterIcon;
			footer.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=footer.facebookIcon;
			footer.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=footer.youtubeIcon;
			footer.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=footer.linkedinIcon;
			footer.verifyComponentExistence(iconLinkedin);
			
			footer.verifyComponentExistence(footer.instagramIcon);
			footer.verifyComponentExistence(footer.telegramIcon);
			logger.write("Verifica Footer - Completed");
			logger.write("Verification of Footer - End ");
			
			Thread.sleep(5000);
			
			//6
			logger.write("Verification of Campo obbligatorio text after click on invia button- Start ");
			lead.clickComponent(lead.proseguiButton);
			
			lead.verifyComponentExistence(lead.campoObbligatorioNome);
			lead.comprareText(lead.campoObbligatorioNome, LeadACaldoComponent.CAMPO_OBBLIGATORIO, true);
			
			lead.verifyComponentExistence(lead.campoObbligatorioCogNome);
			lead.comprareText(lead.campoObbligatorioCogNome, LeadACaldoComponent.CAMPO_OBBLIGATORIO, true);
			
			lead.verifyComponentExistence(lead.campoObbligatorioCellulare);
			lead.comprareText(lead.campoObbligatorioCellulare, LeadACaldoComponent.CAMPO_OBBLIGATORIO, true);
			
			lead.comprareText(lead.errorMessage, LeadACaldoComponent.ERROR_MESSAGE, true);
			logger.write("Verification of Campo obbligatorio text after click on invia button- End ");
			
			//7
			logger.write("Enter the inputs and click on Prosegui Button and verify the data- Start ");
			lead.enterInputParameters(lead.nome, prop.getProperty("NOME"));
			lead.enterInputParameters(lead.cognome, prop.getProperty("COGNOME"));
			lead.enterInputParameters(lead.cellulare, prop.getProperty("CELLULARE"));
			lead.clickComponent(lead.clickAccetto);
			lead.clickComponent(lead.proseguiButton);
			
			//lead.comprareText(lead.confirmationMessageHeading, LeadACaldoComponent.CONFIRMATION_MESSAGE_HEADING, true);
			lead.comprareText(lead.confirmationMessageText, LeadACaldoComponent.CONFIRMATION_MESSAGE_TEXT, true);
			lead.verifyComponentExistence(lead.tornaAllaHomeButton);
			
			logger.write("Enter the inputs and click on Prosegui Button and verify the data- End ");
			
			//8
			logger.write("Click on Torna Alla Home Button and verification of page redirection- Start ");
			Thread.sleep(6000);
			lead.jsClickObject(lead.tornaAllaHomeButton);
			
			lead.checkURLAfterRedirection(homePageUrl);
	
			logger.write("Click on Torna Alla Home Button and verification of page redirection- End ");
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
