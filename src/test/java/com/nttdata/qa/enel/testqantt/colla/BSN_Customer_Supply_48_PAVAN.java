package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSN_47_CustomerSupplyComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class BSN_Customer_Supply_48_PAVAN {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			prop.setProperty("HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("HOMEPAGE_TITLE2", "HOMEPAGE");
		    prop.setProperty("HOMEPAGE_TITLE_DESCRIPTION", "Benvenuto nella tua area dedicata Business");
		    prop.setProperty("SUPPLY_BILL_TITLE", "Forniture e bollette");
		    prop.setProperty("SUPPLY_DETAIL_HEADING", "La tua fornitura nel dettaglio.");
		    prop.setProperty("SUPPLY_DETAIL_TITLE1", "Tutto quello che c'è da sapere sulla tua fornitura");
		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION1", "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.");
		    prop.setProperty("SUPPLY_HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("SUPPLY_HOMEPAGE_TITLE2", "FORNITURA");
		    prop.setProperty("SUPPLY_DETAILS1", "Fornitura Luce Cliente N°: "+prop.getProperty("CUSTOM_NUM"));
		    prop.setProperty("BILL_DETAILS", "Le tue bollette");
		    prop.setProperty("AGGIUNITEXTVALUE1", "Prova2BSN");
			
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			logger.write("Launching the ENEL application portal - Start");
			log.launchLink(prop.getProperty("LINK"));
			logger.write("Launching the ENEL application portal - Completed");
			
			logger.write("Checking ENEL LOGO presence - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);
			logger.write("Checking ENEL LOGO presence - Completed");
			
			logger.write("Checking presence of cookie ACCEPT button - Start");
			By accettaCookie = log.buttonAccetta;
			
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			logger.write("Checking presence of cookie ACCEPT button - Completed");
			
			logger.write("Checking presence of User ICON - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon);
			logger.write("Checking presence of User ICON - Completed");
			
			logger.write("Checking if user is navigated to Login Page - Start");
			By loginPage = log.loginPage;
			log.verifyComponentExistence(loginPage);
			logger.write("Checking if user is navigated to Login Page - Completed");
			
			logger.write("Checking for UserName, Password displayed on Login Page - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("USERNAME"));
			
			
			By pwd = log.password;
			log.verifyComponentExistence(pwd);
			log.enterLoginParameters(pwd, prop.getProperty("PASSWORD"));
			logger.write("Checking for UserName, Password displayed on Login Page - Completed");
			
			logger.write("Verify and Click Login Button on Login Page - Start");
			By loginBtnAccedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(loginBtnAccedi);
			log.clickComponent(loginBtnAccedi);
			logger.write("Verify and Click Login Button on Login Page - Completed");
			
			logger.write("User should be logged in Successfully - Start");
			By accessLoginPage = log.loginBSNSuccessful;
			log.verifyComponentExistence(accessLoginPage);
			logger.write("User should be logged in Successfully - Completed");
			
			
			BSN_47_CustomerSupplyComponent bscp = new BSN_47_CustomerSupplyComponent(driver);
			
			logger.write("Home Page Logo, Title and Description Verification- Start");
			bscp.verifyComponentExistence(bscp.homePageLogo);
			bscp.VerifyText(bscp.homePageTitle1, prop.getProperty("HOMEPAGE_TITLE1"));
			bscp.VerifyText(bscp.homePageTitle2, prop.getProperty("HOMEPAGE_TITLE1"));
			bscp.VerifyText(bscp.homePageTitleDescriptiom, "HOMEPAGE_TITLE1");
			logger.write("Home Page Logo, Title and Description Verification- Completed");
			
			logger.write("Home Page Left Menu Items Verification- Starts");
			bscp.verifyComponentExistence(bscp.leftMenuItems);
			logger.write("Home Page Left Menu Items Verification- Completed");
			
			logger.write("Home  Page Supply Detail Verification- Starts");
			bscp.VerifyText(bscp.supplyBillTitle,prop.getProperty("SUPPLY_BILL_TITLE"));
			bscp.VerifyText(bscp.supplyHeading, prop.getProperty("SUPPLY_DETAIL_HEADING"));
			bscp.supplyDetailsDisplay(bscp.supplyDetails);
			logger.write("Home Page Supply Detail Verification- Ends");	
			
			logger.write("Bill Details Verification- Start");
			bscp.VerifyText(bscp.billDetails, prop.getProperty("BILL_DETAILS"));
			bscp.supplyBillDetailsDisplay(bscp.billTable);
			logger.write("Bill Details Verification- Complete");
			
			bscp.clickComponent(bscp.piuInformazionlink);
			Thread.sleep(6000);
			
			logger.write("Fornitura Home Page Title and Description Verification- Start");
			bscp.VerifyText(bscp.supplyHomePageTitle1,prop.getProperty("SUPPLY_HOMEPAGE_TITLE1"));
			//bcsp.VerifyText(bcsp.supplyHomePageTitle2,prop.getProperty("SUPPLY_HOMEPAGE_TITLE2"));
			bscp.VerifyText(bscp.SupplyTitle1,prop.getProperty("SUPPLY_DETAIL_TITLE1"));
			bscp.VerifyText(bscp.supplyDescription1,prop.getProperty("SUPPLY_DETAIL_DESCRIPTION1"));
			logger.write("Fornitura Home Page Log,Title and Description Verification- Complete");
			
			logger.write("Click on Modifica link- Start");
			bscp.VerifyText(bscp.supplyDetails1, prop.getProperty("SUPPLY_DETAILS1"));
			bscp.verifyComponentExistence(bscp.modificaLink);
			bscp.clickComponent(bscp.modificaLink);
			logger.write("Click on Modifica link- Complete");
			
			logger.write("Enter the value and click on Salvi button - Start");
			bscp.enterLoginParameters(bscp.FornituraField, prop.getProperty("AGGIUNITEXTVALUE1"));
			bscp.verifyComponentExistence(bscp.SalviLink);
			bscp.clickComponent(bscp.SalviLink);
			logger.write("Enter the value and click on Salvi button - Complete");
			 
			 
			bscp.verifyComponentExistence(bscp.modificaLink);
			bscp.clickComponent(bscp.modificaLink);
			bscp.verifyComponentExistence(bscp.SalviLink);
			bscp.clickComponent(bscp.SalviLink);
			 
			 
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}