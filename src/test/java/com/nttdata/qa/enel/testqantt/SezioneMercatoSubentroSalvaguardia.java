package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SezioneMercatoSubentroSalvaguardia {

    public static void main(String[] args) throws Exception {
        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            WebElement element;
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            logger.write("Verifica checklist e conferma - Start");
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);

            Thread.sleep(5000);

            checkListModalComponent.clickConferma();


            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");
            SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
            merc.verifyComponentExistence(merc.tabSubentro);
            merc.verifyComponentExistence(merc.labelSubentro);

            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            merc.verifyComponentExistence(insert.sezioneInserimentoDati);

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");

            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
            merc.verifyComponentExistence(merc.checkContatto);
            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");


            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
            merc.verifyComponentExistence(merc.checkCf);
            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");

            //Box Sezione Mercato

            merc.clickComponentIfExist(merc.inputSezioneMercato);

            merc.simulateDownArrowKey(merc.inputSezioneMercato);

            merc.simulateEnterKey(merc.inputSezioneMercato);

            merc.clickComponentIfExist(merc.buttonConfermaSezioneMercato);

          /*  if (merc.getElementTextString(merc.textSezioneMercato).equals("Non sei abilitato ad eseguire operazioni sul Mercato Salvaguardia. Invita il cliente a recarsi presso un Punto Enel."))
                logger.write("Il testo che appare è corretto");
            else
                throw new Exception("Il testo che appare non è corretto");*/


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
