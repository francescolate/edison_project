package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioBolletta_105 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		prop.setProperty("RESIDENTIALMenu", " Forniture | Bollette | Servizi | enelpremia WOW! | Novità | Spazio Video | Account | I tuoi diritti | Supporto | Trova Spazio Enel | Esci ");
	    prop.setProperty("SupplyServices", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
		
	    BolletteComponent 	bc = new BolletteComponent(driver);
		
		logger.write("Check for Residential Menu - Start");
		bc.checkForResidentialMenu(prop.getProperty("RESIDENTIALMenu"));
		logger.write("Check for Residential Menu - Completed");
		
		logger.write("Click on Bills from Residential Menu - Start");
		bc.clickComponent(bc.bills);
		logger.write("Click on Bills from Residential Menu - Completed");
		
		logger.write("Verification of List of One or more bills assocaited with the supply - Started");

//		bc.verifyComponentExistence(bc.listofBills);
		logger.write("Verification of List of One or more bills assocaited with the supply - Completed");
		Thread.sleep(5000);
		bc.jsClickObject(bc.plusInLuce);
		//bc.clickComponent(bc.visualizzaTutte);
				
		logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");
		bc.waitForElementToDisplay(bc.moreLuce4000007572);
		bc.verifyComponentExistence(bc.moreLuce4000007572);
		bc.clickComponent(bc.moreLuce4000007572);
		logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");

		bc.verifyComponentExistence(bc.dettaglioBolletta6100000274);		
		bc.verifyComponentExistence(bc.scaricaPDF4000007572);
				
		logger.write("Click on Dettaglio Bolletta - Start");
		bc.jsClickObject(bc.scaricaPDF4000007572);
		logger.write("Click on Dettaglio Bolletta - Completed");

		Thread.currentThread().sleep(30000);
		
		DettaglioBollettaComponent db = new DettaglioBollettaComponent(driver);
		if(db.isFileDownloaded_Ext(DettaglioBollettaComponent.DIR_PATH, DettaglioBollettaComponent.FILENAME))
			db.deleteDownloadedFile(db.DIR_PATH, db.FILENAME);
		
		prop.setProperty("RETURN_VALUE", "OK");
			
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
