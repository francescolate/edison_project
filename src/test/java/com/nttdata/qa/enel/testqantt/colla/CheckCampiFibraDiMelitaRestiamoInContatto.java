package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FibraDiMelitaComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CheckCampiFibraDiMelitaRestiamoInContatto {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			HamburgerMenuComponent hamburger = new HamburgerMenuComponent(driver);
			hamburger.clickComponent(hamburger.fibraDiMelitaLink);

			FibraDiMelitaComponent fibra = new FibraDiMelitaComponent(driver);
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Start");
			fibra.checkHeaderPathLinks(fibra.pathLinks1);
			fibra.verifyComponentExistence(fibra.headerTitle);
			fibra.verifyComponentExistence(fibra.headerSubTitle);
			fibra.checkNavBarLinks();
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Completed");

			logger.write("Voce di Puntamento Verifica Copertura e check campi - Start");
			fibra.clickComponent(fibra.verificaCoperturaAnchor);
			// fibra.clickComponent(fibra.closeChatHeading);
			fibra.verifyComponentExistence(fibra.comuneField);
			fibra.verifyComponentExistence(fibra.indirizzoField);
			fibra.verifyComponentExistence(fibra.civicoField);
			fibra.verifyComponentExistence(fibra.verificaLaCoperturaButton);
			fibra.verifyComponentExistence(fibra.nonTroviIlTuoIndirizzoLink);
			logger.write("Voce di Puntamento Verifica Copertura e check campi - Completed");

			logger.write("Click sul link Non trovi il tuo indirizzo? - Start");
			fibra.clickComponent(fibra.nonTroviIlTuoIndirizzoLink);
			fibra.verifyComponentExistence(fibra.headerTitle_2);
			fibra.verifyComponentExistence(fibra.headerSubTitleKO);
			fibra.checkElementWithText(fibra.mainContactFiberLabel, "Restiamo in contatto!");
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Nome*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Cognome*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Codice Fiscale*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Telefono Cellulare*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Email*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Comune*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Indirizzo*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Civico*")));
			fibra.verifyComponentExistence(By.xpath(fibra.genericLabel.replace("#", "Ho preso visione")));
			fibra.verifyComponentExistence(By.xpath(fibra.genericLabel.replace("#", "testo marketing")));
			fibra.verifyComponentExistence(fibra.proseguiButton);
			fibra.verifyComponentExistence(fibra.tornaHomeButton);
			logger.write("Click sul link Non trovi il tuo indirizzo? - Completed");

			logger.write("Non valorizzare nessun campo e fare click su tasto Prosegui e controlla - Start");
			fibra.checkMandatoryFields();
			logger.write("Non valorizzare nessun campo e fare click su tasto Prosegui e controlla - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Nome - Start");
			fibra.checkMandatoryFieldsAfterInputField("Nome*", "TestNome", true);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Nome - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Cognome - Start");
			fibra.checkMandatoryFieldsAfterInputField("Cognome*", "TestCognome", true);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Cognome - Completed");
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Codice Fiscale - Start");

			fibra.checkMandatoryFieldsAfterInputField("Codice Fiscale*", "GLDRRT85C04F839H", true);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Codice Fiscale - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Telefono Cellulare - Start");
			fibra.checkMandatoryFieldsAfterInputField("Telefono Cellulare*", "3925567884", true);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Telefono Cellulare - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Email - Start");
			fibra.checkMandatoryFieldsAfterInputField("Email*", "testtest@test.it", true);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Email - Completed");
			
			logger.write("Verifica campi obbligatori dopo valorizzazione solo checkbox Ho preso visione - Start");
			fibra.checkMandatoryFieldsAfterSelectedCheckbox("Ho preso visione");
			logger.write("Verifica campi obbligatori dopo valorizzazione solo checkbox Ho preso visione - Completed");
			
			logger.write("Verifica campi obbligatori dopo valorizzazione solo checkbox testo marketing - Start");
            fibra.checkMandatoryFieldsAfterSelectedCheckbox("testo marketing");
			logger.write("Verifica campi obbligatori dopo valorizzazione solo checkbox testo marketing - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Comune - Start");
			List<String> removeFields = Arrays.asList("Comune*");
			fibra.sendKeyAndSelectValue(By.xpath(fibra.inputFibra.replace("#", "Comune*")), "ROMA");
			fibra.checkMandatoryFields(removeFields);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Comune - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Indirizzo - Start");
			removeFields = Arrays.asList("Comune*","Indirizzo*");
			fibra.sendKeyAndSelectValue(By.xpath(fibra.inputFibra.replace("#", "Indirizzo*")), "VIA FIUME DELLE PER");
			//fibra.clickComponent(By.xpath("//*[contains(text(), 'VIA FIUME DELLE PERLE')]")) ;
			fibra.checkMandatoryFields(removeFields);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Indirizzo - Completed");

			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Civico - Start");
			removeFields = Arrays.asList("Comune*","Indirizzo*","Civico*");
			fibra.sendKeyAndSelectValue(By.xpath(fibra.inputFibra.replace("#", "Civico*")), "104");
			fibra.checkMandatoryFields(removeFields);
			logger.write("Verifica campi obbligatori dopo valorizzazione solo campo Civico - Completed");

			

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
