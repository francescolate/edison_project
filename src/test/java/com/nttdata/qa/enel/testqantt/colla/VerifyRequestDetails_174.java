package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GetUserDetailFromWorkBenchComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetails_174 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
						
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			GetUserDetailFromWorkBenchComponent gc = new GetUserDetailFromWorkBenchComponent(driver);
			
			prop.setProperty("CLIENT_NUMBER", "300001597");
			//prop.setProperty("MODIFIED_ADDRESS", "VIA APPIA ANTICA 1, 00179, ROMA, ROMA (ROMA) LAZIO, RM");
			prop.setProperty("MODIFIED_ADDRESS", "ARCO VIA APPIA ANTICA 1, 00198, ROMA, ROMA (ROMA) LAZIO, RM");
			//ARCO VIA APPIA ANTICA 1, 00198, ROMA, ROMA (ROMA) LAZIO, RM
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.Query_174.replace("$Numero cliente$", prop.getProperty("CLIENT_NUMBER")));
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			
			Thread.sleep(5000);
			gc.verifyComponentExistence(gc.queryResultFields);
			gc.comprareText(gc.supplyNum_174, prop.getProperty("CLIENT_NUMBER"), true);
//			gc.comprareText(gc.supplyNum_174a, prop.getProperty("MODIFIED_ADDRESS"), true);
			
			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
