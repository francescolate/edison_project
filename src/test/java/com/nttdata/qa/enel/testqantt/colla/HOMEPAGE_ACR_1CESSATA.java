package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.PLACETFissaLuceConsumerComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HOMEPAGE_ACR_1CESSATA {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
						
			HomeComponent home = new HomeComponent(driver);
			
		    prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");

			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
						
			logger.write("Check for Residential Menu - Completed");

			logger.write("Check for Supply label - Start");

			home.verifyComponentExistence(home.supplyLabelLuce);
			
			logger.write("Check for Supply label - Completed");
			
			logger.write("Check for View your bills - Start");

			home.verifyComponentExistence(home.viewYourBillsLuce);
			
			logger.write("Check for View your bills - Completed");

			logger.write("Check for Supply Details - Start");

			home.verifyComponentExistence(home.supplyDetail_1Cessata);
						
			home.checkSupplyStatus(home.supplyDetail_1Cessata, prop.getProperty("SUPPLYSTATUS"));
			
			logger.write("Check for Supply Details - Completed");
			

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}

	
}
