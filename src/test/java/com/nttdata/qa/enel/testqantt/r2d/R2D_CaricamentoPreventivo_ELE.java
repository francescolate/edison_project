package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_PreventivoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoPreventivo_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		SeleniumUtilities util;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			util = new SeleniumUtilities(driver);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Caricamento Dati","Caricamento Preventivo");
				R2D_PreventivoComponent caricamentoPreventivo = new R2D_PreventivoComponent(driver);
				//Inserimento codice richiesta distributore
				caricamentoPreventivo.InserisciCodiceRichiestaDistributore(caricamentoPreventivo.inputCodiceRichiestaDistributore, prop.getProperty("OI_RICERCA",prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA"))));
				//Click su cerca
				caricamentoPreventivo.cercaPratica(caricamentoPreventivo.buttonCerca);
				//Seleziona Tasto azione
				caricamentoPreventivo.selezionaTastoAzionePratica();
				//Calcolo sysdate
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String data = simpleDateFormat.format(calendar.getTime()).toString();
				//Calcolo data sysdata+15 gg
				Calendar calendar15 = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
				calendar15.setTime(simpleDateFormat.parse(data));
				calendar15.add(Calendar.DATE, 15);  // number of days to add
				String data15 = simpleDateFormat.format(calendar15.getTime());  
				String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE");
				
				//Selezione tipo caricamento
				if (tipoOperazione.equals("ALLACCIO_EVO")){
				caricamentoPreventivo.caricaPreventivoAllaccio(data, util.randomNumeric(8), prop.getProperty("OI_ORDINE"), prop.getProperty("OI_ORDINE"), data15, prop.getProperty("TEMPO_ESECUZIONE_LAVORI"), prop.getProperty("VOCE_COSTO"), prop.getProperty("IMPORTO_PREVENTIVO"));
				prop.setProperty("STATO_R2D", "OK");
			}
				//if (prop.getProperty("SUBENTRO_COMBINATO_R2D","N").equals("Y")||prop.getProperty("MODIFICA_POTENZA_TENSIONE","N").equals("Y")){
				if (tipoOperazione.equals("SUBENTRO_COMBINATO_R2D")||tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")) {
					caricamentoPreventivo.caricaPreventivoSubentroCombinato(data, prop.getProperty("NUMERO_PREVENTIVO"), prop.getProperty("CODICE_RINTRACCIABILITA"), prop.getProperty("ID_RICHIESTA_CRM_ELE"), data15, prop.getProperty("TEMPO_ESECUZIONE_LAVORI"), prop.getProperty("VOCE_COSTO_1"), prop.getProperty("IMPORTO_PREVENTIVO_1"),prop.getProperty("VOCE_COSTO_2"),prop.getProperty("IMPORTO_PREVENTIVO_2"));
					prop.setProperty("STATO_R2D", "OK");
				}
			}
			//
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
