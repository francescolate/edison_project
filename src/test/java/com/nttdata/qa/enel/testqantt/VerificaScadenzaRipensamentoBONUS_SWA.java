package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaScadenzaRipensamentoBONUS_SWA {
	private final static int SEC = 120;
	
	@Step("VerificaScadenzaRipensamentoBONUS_SWA")
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
//			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
			//Verifica stato offerta
			if(prop.getProperty("NE__STATUS__C").compareToIgnoreCase(prop.getProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO_BONUS"))!=0){
				throw new Exception("Lo stato dell'offerta VAS non corrisponde a quello atteso. Valore attuale:"+prop.getProperty("NE__STATUS__C")+" valore atteso:"+prop.getProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO_BONUS")+" Pod riferimento:"+prop.getProperty("POD"));
				
			}
			logger.write("Verifica stato offerta alla scandenza data ripensamento");
			

			}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
