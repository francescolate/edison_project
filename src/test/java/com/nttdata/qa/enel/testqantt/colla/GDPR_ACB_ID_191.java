package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GDPRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class GDPR_ACB_ID_191 {

	public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		GDPRComponent gdpr = new GDPRComponent(driver);
				
		logger.write("Verification of Home Page Data  - Start ");
		gdpr.comprareText(gdpr.homePagePath1, GDPRComponent.HOME_PAGE_PATH1, true);
		gdpr.comprareText(gdpr.homePagePath2, GDPRComponent.HOME_PAGE_PATH2, true);
		gdpr.comprareText(gdpr.homePageHeading, GDPRComponent.HOME_PAGE_HEADING, true);
		logger.write("Verification of Home Page Data  - End ");
		
		logger.write("Clicking on I Tuoi Dirtti and Verification of Data  - Start ");
		gdpr.clickComponent(gdpr.iTuoiDirtti);
		gdpr.comprareText(gdpr.iTuoiDirttiPath1, GDPRComponent.PATH1_ITUOIDIRTTI, true);
		gdpr.comprareText(gdpr.iTuoiDirttiPath2, GDPRComponent.PATH2_ITUOIDIRTTI, true);
		logger.write("Clicking on I Tuoi Dirtti and Verification of Data  - End ");
		
		logger.write(" Verification of Data Anagrafici details- Start ");
		gdpr.comprareText(gdpr.datiAnagraficiHeading, GDPRComponent.DATA_ANAGRAFICI_HEADING, true);
		gdpr.verifyComponentExistence(gdpr.titolare);
		gdpr.verifyComponentExistence(gdpr.codiFiscale);
		gdpr.verifyComponentExistence(gdpr.email);
		logger.write(" Verification of Data Anagrafici details- End ");
		
		logger.write(" Verification of Diritto Di Rettifica details by clicking + symbol - Start ");
		gdpr.comprareText(gdpr.dirittoDiPortabilita, GDPRComponent.DIRITTO_DI_PORTABILITA, true);
		gdpr.clickComponent(gdpr.collapseIconDirittoDiPortabilita);
		gdpr.comprareText(gdpr.dirittoDiPortabilitaHeading, GDPRComponent.DIRITTO_DI_PORTABILITA_HEADING, true);
		gdpr.comprareText(gdpr.dirittoDiPortabilitaText1, GDPRComponent.DIRITTO_DI_PORTABILITA_TEXT1, true);
		gdpr.comprareText(gdpr.dirittoDiPortabilitaText2, GDPRComponent.DIRITTO_DI_PORTABILITA_TEXT2_190, true);
		gdpr.comprareText(gdpr.dirittoDiPortabilitaText3, GDPRComponent.DIRITTO_DI_PORTABILITA_TEXT3, true);
		gdpr.clickComponent(gdpr.collapseIconDirittoDiPortabilita);
		gdpr.isElementNotPresent(gdpr.dirittoDiPortabilitaHeading);
		gdpr.isElementNotPresent(gdpr.dirittoDiPortabilitaText1);
		gdpr.isElementNotPresent(gdpr.dirittoDiPortabilitaText2);
		gdpr.isElementNotPresent(gdpr.dirittoDiPortabilitaText3);
		logger.write(" Verification of Diritto Di Rettifica details by clicking + symbol - End ");
		
		logger.write(" Verification of page navigation by clicking Clicca qui link - Start ");
		gdpr.clickComponent(gdpr.collapseIconDirittoDiPortabilita);
		gdpr.verifyComponentExistence(gdpr.cliccaQuiDirittoDiPortabilita);
		gdpr.verifyComponentExistence(gdpr.aacediAlServizoButton);
		logger.write(" Verification of page navigation by clicking Clicca qui link - End ");
		
		logger.write("Click on accedi al servizo button and verify the data - Start ");
		Thread.sleep(5000);
		gdpr.clickComponent(gdpr.aacediAlServizoButton);
		gdpr.comprareText(gdpr.sectionPath1,GDPRComponent.SECTION_PATH1, true);
		gdpr.comprareText(gdpr.sectionPath2,GDPRComponent.SECTION_PATH2, true);
		gdpr.comprareText(gdpr.sectionHeading, GDPRComponent.SECTION_HEADING, true);
		logger.write("Click on accedi al servizo button and verify the data - End ");
		
		logger.write("Verification of table columns tipology, numero cliente and Indirizzo Di Fornitura- Start ");
		gdpr.comprareText(gdpr.tipologia, GDPRComponent.TIPOLOGIA, true);
		gdpr.comprareText(gdpr.numeroCliente, GDPRComponent.NUMERO_CLIENTE, true);
		gdpr.comprareText(gdpr.indirizzoDiFornitura, GDPRComponent.INDIRIZZO_DI_FORNITURA, true);
		logger.write("Verification of table columns tipology, numero cliente and Indirizzo Di Fornitura- End ");
		
		logger.write("Verification of the functionality of bill supply check box and Riched I Tuoi Dati button- Start");
		gdpr.isRichiediITuoiDatiEnabled(gdpr.checkBox);
		gdpr.verifyComponentExistence(gdpr.buttonRichediITuoiDati);
		Thread.sleep(5000);
		gdpr.isRichiediITuoiDatiDisabled(gdpr.buttonRichediITuoiDati);
		gdpr.clickComponent(gdpr.checkBox3);
		gdpr.isRichiediITuoiDatiDisabled(gdpr.buttonRichediITuoiDati);
		logger.write("Verification of the functionality of bill supply check box and Riched I Tuoi Dati button- End");
		
		logger.write("Click on Riched I Tuoi Dati button and verification of Data - starts");
		gdpr.clickComponent(gdpr.buttonRichediITuoiDati);
		gdpr.comprareText(gdpr.section2Text1, GDPRComponent.SECTION2_TEXT1, true);
		gdpr.comprareText(gdpr.section2Text2, GDPRComponent.SECTION2_TEXT2, true);
		logger.write("Click on Riched I Tuoi Dati button and verification of Data - Ends");
		
		logger.write("Click on info icon button and verify the text - Start");
		gdpr.clickComponent(gdpr.iTuoiDirtti);
		gdpr.comprareText(gdpr.attizoneHeading, GDPRComponent.ATTIZONE_HEADING, true);
		gdpr.comprareText(gdpr.attizoneText, GDPRComponent.ATTIZONE_TEXT, true);
		gdpr.verifyComponentExistence(gdpr.tornaAlProcessoButton);
		gdpr.verifyComponentExistence(gdpr.popupEsciButton);
		gdpr.clickComponent(gdpr.popupEsciButton);
		gdpr.clickComponent(gdpr.collapseIconDirittoDiPortabilita);
		gdpr.clickComponent(gdpr.aacediAlServizoButton);
		gdpr.clickComponent(gdpr.infoIcon);
		gdpr.comprareText(gdpr.infoPopupHeading, GDPRComponent.INFO_POPUP_HEADING, true);
		gdpr.comprareText(gdpr.infoPopupText, GDPRComponent.INFO_POPUP_TEXT, true);
		logger.write("Click on info icon button and verify the text - End");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
