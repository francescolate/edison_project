//HerokuAPILoginstrong_2 package APP - COMPARE - chiama HerokuAPILoginstrongComponent_Err per gestire errore -1 

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;

import com.nttdata.qa.enel.components.colla.api.HerokuAPILoginstrongComponent_Err;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPILoginstrong_2 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Loginstrong Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			//Da configurare come OutPut
//    		prop.setProperty("TID", UUID.randomUUID().toString()); //"8b8fdc68-ee96-11e9-81b4-2a2ae2mano18")
//    		prop.setProperty("SID", UUID.randomUUID().toString()); //"8b8fdc68-ee96-11e9-81b4-2a2ae2mano19")
			prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130");
			prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130");
		    prop.setProperty("JSON_INPUT", "{     \"gen\": {         \"tid\": \"" + prop.getProperty("TID") + "\",   \"sid\": \""+ prop.getProperty("SID") + "\",         \"username\": \""+ prop.getProperty("USERNAME") + "\",         \"keys\": [             {                 \"key\": \"APP_VER\",                 \"value\": \"10.0.1\"             },             {                 \"key\": \"OS\",                 \"value\": \"ANDROID\"             },             {                 \"key\": \"APP\",                 \"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"             },             {                 \"key\": \"ID_DISPOSITIVO\",                 \"value\": \""+prop.getProperty("IDDISPOSITIVO") + "\"             }         ]     },     \"data\": {         \"username\": \""+ prop.getProperty("USERNAME") + "\",         \"password\": \""+ prop.getProperty("PASSWORD") + "\",         \"migrationToken\": \"\",         \"oidcToken\": \"\",         \"pwd_crypt\": \"\",         \"resolution\": \""+prop.getProperty("RESOLUTION") + "\",         \"osVer\": \""+prop.getProperty("OSVER") + "\",         \"idDispositivo\": \""+prop.getProperty("IDDISPOSITIVO") + "\",         \"appVer\": \"10.0.1\",         \"os\": \"ANDROID\",         \"context\": \""+prop.getProperty("CONTEXT") + "\",         \"device\": \""+prop.getProperty("DEVICE") + "\"     } }");

    		// prop.setProperty("Authorization", "Basic QXZ2MGYyRjA6ZjBlWERzSHc=");
    		prop.setProperty("CHANNELKEY","cfa3a38e-b820-4226-a66f-bd9c14cbbe0d");
    		prop.setProperty("SOURCECHANNEL","APP");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPILoginstrongComponent_Err HRKComponent = new HerokuAPILoginstrongComponent_Err();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
