package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNServiziComponent;
import com.nttdata.qa.enel.components.colla.BSN_InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_276_InfoEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("Verify home page title and page path -- Start");
			PrivateArea_Imprese_HomePageBSNComponent pc = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			pc.verifyComponentExistence(pc.pageTitle);
			pc.comprareText(pc.pagePath, pc.Path, true);
			pc.comprareText(pc.pageTitle, pc.PageTitle, true);
			logger.write("Verify home page title and page path -- Completed");

			logger.write("Click on servizi from left menu,verify page navigation and page title -- Start");
			pc.clickComponent(pc.servizi);
			BSNServiziComponent bc = new BSNServiziComponent(driver);
			bc.verifyComponentExistence(bc.pageTitle);
			bc.comprareText(bc.pagePath, bc.Path, true);
			bc.comprareText(bc.pageText, bc.PageText, true);
			logger.write("Click on servizi from left menu,verify page navigation and page title -- Completed");
			logger.write("Click on Info Enel Energia-- Start");
			bc.clickComponent(bc.infoEnelEnergia);
			logger.write("Click on Info Enel Energia-- Completed");

			logger.write("Verify info enel energia page details-- Start");
			BSN_InfoEnelEnergiaComponent bic = new BSN_InfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			bic.verifyComponentExistence(bic.pageTitle);
			bic.comprareText(bic.pageTitle276, bic.PageTitle, true);
			bic.comprareText(bic.pageSubtitle276, bic.PageSubText276, true);
			bic.verifyComponentExistence(bic.indirzzoText);
			bic.verifyComponentExistence(bic.email);
			bic.verifyComponentExistence(bic.cellulare);
			bic.verifyComponentExistence(bic.modifica);
			bic.comprareText(bic.modificaText, bic.ModificaText, true);
			bic.verifyComponentExistence(bic.modificaScopriDiPiu);
			bic.verifyComponentExistence(bic.revoca);
			bic.comprareText(bic.revocaText, bic.RevocaText, true);
			bic.verifyComponentExistence(bic.revocaScopriDiPiu);
			String value = driver.findElement(bic.supply).getText();
			System.out.println(value);
			String [] supplyText = value.split(" ");
			String supply = supplyText[0].replace("\\s", "");
			System.out.println(supply);
			prop.setProperty("SUPPLY", supply);
			logger.write("Verify info enel energia page details-- Completed");
			logger.write("Click on ModificaScopriDiPiu-- Start");
			bic.clickComponent(bic.modificaScopriDiPiu);
			logger.write("Click on ModificaScopriDiPiu-- Completed");

			logger.write("Verify Modifica Info enel enegia page,menu one details-- Start");
			BSN_ModificaInfoEnelEnergiaComponent bmc = new BSN_ModificaInfoEnelEnergiaComponent(driver);
			Thread.sleep(20000);
			bmc.verifyComponentExistence(bmc.pageTitleNew);
			bmc.comprareText(bmc.pagePath, bmc.PagePath, true);
			bmc.comprareText(bmc.pageTitleNew, bmc.PageTitle, true);
			bmc.comprareText(bmc.pageSubText, bmc.PageSubText, true);
			bmc.verifyComponentExistence(bmc.menuOne);
			bmc.comprareText(bmc.menuOneDescription, bmc.MenuOneDescription, true);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaSMS);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaSMS);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaEmail);
			
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaSMS);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			bmc.verifyComponentExistence(bmc.avvisoConsumiSMS);
			bmc.verifyComponentExistence(bmc.avvisoConsumiEmail);
			
			bmc.comprareText(bmc.sectionTitle, bmc.SectionTitle, true);
			bmc.selectCheckBox(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.email);
			bmc.verifyComponentExistence(bmc.confirmEmail);
			bmc.verifyComponentExistence(bmc.cellulare);
			bmc.verifyComponentExistence(bmc.confermaCellulare);
			bmc.verifyComponentExistence(bmc.aggiornaIDatiDiContatto);
			bmc.clickComponent(bmc.aggiornaIDatiDiContatto);
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.clickComponent(bmc.prosegui);
			logger.write("Verify Modifica Info enel enegia page,menu one details-- Completed");

			bmc.verifyComponentExistence(bmc.cellulareError);
			String cellulare_input = "3333323231";
			String invalid_cellulare = "11111111111";
			bmc.enterInputToField(bmc.cellulare, invalid_cellulare);
			bmc.enterInputToField(bmc.confermaCellulare, invalid_cellulare);
			//bmc.clickComponent(bmc.prosegui);	
			bmc.verifyComponentExistence(bmc.cellulareError);
			bmc.enterInputToField(bmc.cellulare, cellulare_input);
			bmc.enterInputToField(bmc.confermaCellulare, cellulare_input);
			bmc.clickComponent(bmc.prosegui);
			
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Start");
			bmc.verifyComponentExistence(bmc.menuTwo);
			bmc.comprareText(bmc.titleSubText, bmc.TitleSubText, true);
			bmc.comprareText(bmc.menuTwoTitle, bmc.MenuTwoTitle, true);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			
			bmc.comprareText(bmc.supplyDataText, bmc.SupplyDataText, true);
			bmc.verifyComponentExistence(bmc.tipo);
			bmc.verifyComponentExistence(bmc.numeroCliente);
			bmc.verifyComponentExistence(bmc.address);
			bmc.comprareText(bmc.tipoValue, bmc.TipoValue, true);
			bmc.comprareText(bmc.numeroClientiValue, bmc.NumeroClientiValue_278, true);
			bmc.comprareText(bmc.addressValue, bmc.AddressValue, true);
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.verifyComponentExistence(bmc.prosegui);
			bmc.clickComponent(bmc.prosegui);
			Thread.sleep(2000);
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Completed");

			logger.write("Verify  Info enel enegia page,menu three details-- Start");
			Thread.sleep(10000);
			bic.verifyComponentExistence(bic.infoTitle);
			//bic.comprareText(bic.pagePath, bic.PagePath, true);
			bic.comprareText(bic.titleSubText, bic.TitleSubText, true);
			bic.verifyComponentExistence(bic.estito);
		//	bic.comprareText(bic.sectionTitle, bic.SectionTitle, true);
		//	bic.comprareText(bic.sectionText, bic.SectionText, true);
		//	bic.verifyComponentExistence(bic.attivaBollettaWeb);
			bic.verifyComponentExistence(bic.fineButton);
			bic.clickComponent(bic.fineButton);
			logger.write("Verify  Info enel enegia page,menu three details-- Completed");

			logger.write("Verify home page title and page path -- Start");
			pc.verifyComponentExistence(pc.pageTitle);
			pc.comprareText(pc.pagePath, pc.Path, true);
			pc.comprareText(pc.pageTitle, pc.PageTitle, true);
			logger.write("Verify home page title and page path -- Completed");

		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
