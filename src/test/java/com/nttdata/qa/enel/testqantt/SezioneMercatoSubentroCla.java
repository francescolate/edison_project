package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class  SezioneMercatoSubentroCla {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);

            util.getFrameActive();

            checkListModalComponent.clickConferma();

            //gestione.checkSpinnersSFDC();

            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");
            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");

//            merc.verifyComponentExistence(merc.tabSubentro);
//            merc.verifyComponentExistence(merc.labelSubentro);
//            merc.verifyComponentExistence(insert.sezioneInserimentoDati);
//            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");
//
//            merc.verifyComponentExistence(merc.checkContatto);
//            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");
//            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");
//
//            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
//            merc.verifyComponentExistence(merc.checkContatto);
//            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");
//
//            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
//            merc.verifyComponentExistence(merc.checkCf);
//            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");
//
//            logger.write("verifica esistenza sezione mercato e selezionare Conferma - Start");
//            merc.verifyComponentExistence(merc.labelSelezioneMercato);
//            logger.write("verifica esistenza sezione mercato e selezionare Conferma - Completed");

            if (prop.getProperty("MERCATO").contentEquals("Salvaguardia")) {
                merc.selezionaMercato(prop.getProperty("MERCATO"));
            } else {
                merc.selezionaMercato("Libero");
            }

            merc.clickComponentIfExist(merc.buttonConfermaSezioneMercato);
//            gestione.checkSpinnersSFDC();
            logger.write("verifica esistenza sezione mercato e selezionare Conferma - Completed");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());

            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }

}
