package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OffersGasComponent;
import com.nttdata.qa.enel.components.colla.OrdinaPerMenuComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Sub_Bsn_Gas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Thread.currentThread().sleep(3000);
			
		    logger.write("click sul tab 'Luce e Gas' - Start");
			
			TopMenuComponent tab=new TopMenuComponent(driver);
			tab.sceltaMenu("Luce e gas");
			
			logger.write("click sul tab 'Luce e Gas' - Completed");
			
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);
			/*
			logger.write("check sul titolo della pagina 'Trova la soluzione giusta per te' nella sezione 'Luce e Gas' - Start");
			By solutionLabel=headerMenu.rightSolutionLabel;
			headerMenu.verifyComponentExistence(solutionLabel);
			logger.write("check sul titolo della pagina 'Trova la soluzione giusta per te' nella sezione 'Luce e Gas' - Completed");
			*/
					
			headerMenu.scrollComponent(headerMenu.labelLuceandGas);
			
			logger.write("visualizzazione dell domande 'Che contratto vuoi attivare';'Dove?';'Per quale necessità?' relative al motorino di ricerca e del pulsante 'INIZIA ORA' - Start");
			By iniziaOraPulsating=headerMenu.iniziaOraButton;
			headerMenu.verifyComponentExistence(iniziaOraPulsating);
			logger.write("check existence button 'INIZIA ORA' - Completed");
			
			
			By searchMenu=headerMenu.searchSection;
			headerMenu.verifyQuestionsExistence(searchMenu);
			logger.write("visualizzazione dell domande 'Che contratto vuoi attivare';'Dove?';'Per quale necessità?' relative al motorino di ricerca e del pulsante 'INIZIA ORA' - Completed");
			
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Che contratto vuoi attivare': Luce; Gas; Luce e Gas - Start");
			By optionsContractList=headerMenu.contractList;
			headerMenu.verifyFirstListBox(optionsContractList);
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Che contratto vuoi attivare': Luce; Gas; Luce e Gas - Completed");
			
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Dove': Casa; Negozio - Ufficio; Sei una start up, una PMI o una grande azienda? SCOPRI LE OFFERTE PER LE IMPRESE - Start");
			By optionsPlaceList=headerMenu.placeList;
			headerMenu.verifySecondListBox(optionsPlaceList);
			
			
			By linkOnPlaceList=headerMenu.thirdOptionLinkOnPlaceList;
			headerMenu.verifyComponentExistence(linkOnPlaceList);
			By scopri_le_offerte=headerMenu.scoprileofferte;
			headerMenu.checkColor(scopri_le_offerte, "DeepPink", "'SCOPRI LE OFFERTE'");
			logger.write("verifica presenza opzioni sulla pagina web 'Luce e Gas' per la listbox 'Dove': Casa; Negozio - Ufficio; Sei una start up, una PMI o una grande azienda? SCOPRI LE OFFERTE PER LE IMPRESE - Completed");
			
			logger.write("accesso alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkIMPRESE+"' - Start");
			headerMenu.clickComponent(linkOnPlaceList);
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkIMPRESE);
			By impresePageAccess=headerMenu.impresePage;
			headerMenu.verifyComponentExistence(impresePageAccess);
			logger.write("accesso alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkIMPRESE+"' - Completed");
			
			logger.write("back alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS+"' - Start");
			driver.navigate().back();
			headerMenu.checkUrl(prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS);
			headerMenu.verifyComponentExistence(iniziaOraPulsating);
			logger.write("back alla pagina web '"+prop.getProperty("LINK")+headerMenu.linkLUCE_E_GAS+"' - Completed");
			
			logger.write("verifica presenza processi con relativa descrizione sulla pagina web 'Luce e Gas' per la listbox 'Per quale necessita':CAMBIO FORNITORE; PRIMA ATTIVAZIONE; SUBENTRO; VOLTURA; VISUALIZZA TUTTE  - Start");
			By optionsNeedList=headerMenu.needList;
			headerMenu.verifyThirdListBox(optionsNeedList);
			logger.write("verifica presenza processi con relativa descrizione sulla pagina web 'Luce e Gas' per la listbox 'Per quale necessita':CAMBIO FORNITORE; PRIMA ATTIVAZIONE; SUBENTRO; VOLTURA; VISUALIZZA TUTTE  - Completed");
			
			logger.write("effettuare ricerca con il filtro: Gas;Negozio_Ufficio;Subentro' e check sulla pagina alla quale si accede - Start");
			headerMenu.selectionOptionsFromListbox(optionsContractList,prop.getProperty("TYPE_OF_CONTRACT"),optionsPlaceList,prop.getProperty("PLACE"),optionsNeedList,prop.getProperty("NEED"));		
			headerMenu.clickComponent(iniziaOraPulsating);
			OffersGasComponent gas=new OffersGasComponent(driver);
			gas.checkUrl(prop.getProperty("LINK")+gas.linkLUCE_E_GAS_CON_FILTRO);
			
			headerMenu.verifyQuestionsExistence(searchMenu);
						
			By dettaglio_Offerta=gas.dettaglioOfferta;
			gas.verifyComponentExistence(dettaglio_Offerta);
			gas.verificaDettaglioOfferta(dettaglio_Offerta);
			
			logger.write("effettuare ricerca con il filtro: Gas;Negozio_Ufficio;Subentro' e check sulla pagina alla quale si accede - Completed");
			
			logger.write("verifica del testo sotto il motorino di ricerca - Start"); 
			By label_GasPerLeImprese=gas.labelGasPerLeImprese;
			gas.verifyComponentExistence(label_GasPerLeImprese);
			
			By titolotesto_gas=gas.titolotesto;
			gas.verifyComponentExistence(titolotesto_gas);
			
			By sottotitolo_testo=gas.sottotitolotesto;
			gas.verifyComponentExistence(sottotitolo_testo);
			
			By linkVedituttegas=gas.linkVedituttegas;
			gas.verifyComponentExistence(linkVedituttegas);
			gas.checkColor(linkVedituttegas,"DeepPink","link VEDI TUTTE GAS");
			
			By freccia_Link=gas.frecciaLink;
			gas.verifyComponentExistence(freccia_Link);
			gas.checkColor(freccia_Link,"DeepPink","link VEDI TUTTE GAS");
			logger.write("verifica del testo sotto il motorino di ricerca - Start"); 
			
			logger.write("click sul link VEDI TUTTE GAS e check che si apra un'altra pagina con il motorino di ricerca impostato a Gas/Negozio- Ufficio/Visualizza Tutte - Completed"); 
			gas.clickComponent(linkVedituttegas);
			
			headerMenu.verifyQuestionsExistence(searchMenu);
			
			By precedentcontract=headerMenu.contractListvalue;
			headerMenu.verifyTextSearchMenu(precedentcontract,"Gas");
			By precedentplace=headerMenu.placeListvalue;
			headerMenu.verifyTextSearchMenu(precedentplace,"Negozio - Ufficio");
			By precedentneed=headerMenu.Listvalue;
			headerMenu.verifyTextSearchMenu(precedentneed,"Visualizza tutte");
			logger.write("click sul link VEDI TUTTE GAS e check che si apra un'altra pagina con il motorino di ricerca impostato a Gas/Negozio- Ufficio/Visualizza Tutte - Completed"); 
			
			logger.write("effettuare il back del browser e verificare che il motorino di ricerca sia impostato a Gas/Negozio- Ufficio/Subentro - Start"); 
			driver.navigate().back();
			headerMenu.verifyQuestionsExistence(searchMenu);
			
			headerMenu.verifyTextSearchMenu(precedentcontract,"Gas");
			
			headerMenu.verifyTextSearchMenu(precedentplace,"Negozio - Ufficio");
		
			headerMenu.verifyTextSearchMenu(precedentneed,"Subentro");
			logger.write("effettuare il back del browser e verificare che il motorino di ricerca sia impostato a Gas/Negozio- Ufficio/Subentro - Completed"); 
			
			logger.write("visualizzazione della voce 'ORDINA PER:' con un solo valore possibile : IN PROMOZIONE - Start ");
			OrdinaPerMenuComponent ordinaperMenu= new OrdinaPerMenuComponent(driver);			
			By ordinaLabel=ordinaperMenu.labelOrdinaPer;
			ordinaperMenu.verifyComponentExistence(ordinaLabel);
						
			By inpromozioneLabel=ordinaperMenu.defaultLabel;
			ordinaperMenu.verifyComponentExistence(inpromozioneLabel);
			logger.write("visualizzazione della voce 'ORDINA PER:' con un solo valore possibile : IN PROMOZIONE - Completed ");
			
			logger.write("Verifica lista offerte presentate per il filtro impostato - Start");
			By titoliListaOfferte=gas.titoliListaOfferte;
			//gas.verifyOfferList(titoliListaOfferte);		
			logger.write("Verifica lista offerte presentate per il filtro impostato - Completed");
			
			logger.write("Verificare che tutte le offerte hanno pulsante DETTAGLIO OFFERTA - Start");
			//gas.verificaDettaglioOfferta(dettaglio_Offerta);
			logger.write("Verificare che tutte le offerte hanno pulsante DETTAGLIO OFFERTA - Completed");
			
			logger.write("Verificare che tutte le offerte hanno Link ATTIVA ORA - Start");
			By listaAttiva_Ora=gas.listaAttivaOra;
			gas.verificaAttivaOra(listaAttiva_Ora);
			logger.write("Verificare che tutte le offerte hanno Link ATTIVA ORA - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
