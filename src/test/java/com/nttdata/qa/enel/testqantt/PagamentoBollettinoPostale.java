

package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class PagamentoBollettinoPostale {

    @Step("Pagamento Bollettino Postale")
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
                SceltaMetodoPagamentoComponentEVO sceltaMetodoPagamentoComponentEVO = new SceltaMetodoPagamentoComponentEVO(
                        driver);

                By bollettinoPostale = sceltaMetodoPagamentoComponentEVO.bollettinoPostale4;
                By depo = sceltaMetodoPagamentoComponentEVO.depositoCauzionaleBtn5;
                logger.write("Selezione metodo di pagamento");

                sceltaMetodoPagamentoComponentEVO.selezionaBollettinoPagamento(bollettinoPostale, depo);
               
                sceltaMetodoPagamentoComponentEVO.press(sceltaMetodoPagamentoComponentEVO.buttonConferma2);
                
                logger.write("Selezione metodo di pagamento");

            }
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
