package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_5OK_CambioUso_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				// Selezione Menu Interrogazione
				menuBox.selezionaVoceMenuBox("Code di comunicazione", "Caricamento Esiti");
				R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
				// Selezione tipologia Caricamento
				caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
				// Inserisci Pod
				if (prop.getProperty("SKIP_POD", "N").equals("N")) {
					caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD,
							prop.getProperty("POD_ELE", prop.getProperty("POD")));
					// Inserisci ID Richiesta CRM
				}
				caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM,
						prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA")));
				// Cerca
				caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
				// Click button Azione
				caricamentoEsiti.selezionaTastoAzionePratica();
				// Verifica Stato pratica atteso
				caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "CI");
				// Selezione esito 5OK
				caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_5OK_ELE"));
				// Inserimento esito
				caricamentoEsiti.selezioneEsito("OK");
				// Calcolo sysdate
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
//				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String data = simpleDateFormat.format(calendar.getTime()).toString();

				caricamentoEsiti.inserimentoDettaglioEsito5OKCambioUsoEle(prop.getProperty("OI_RICHIESTA"), data,
						"CAMBIOUSO");

				// Salvataggio stato attuale pratica
				prop.setProperty("STATO_R2D", "OK");

			}
			//
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
