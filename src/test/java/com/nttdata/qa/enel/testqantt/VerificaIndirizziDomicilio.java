package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaIndirizziDomicilio {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				AccediTabClientiComponent tabClienti = new AccediTabClientiComponent(driver);
				tabClienti.accediTabClienti();
				CreaNuovoClienteComponent crea = new CreaNuovoClienteComponent(driver);
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				String frameName;
				switch (prop.getProperty("TIPOLOGIA_CLIENTE")) {
				case "RESIDENZIALE":
					crea.nuovoCliente();
					TimeUnit.SECONDS.sleep(10);
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonResidenziale);
					TimeUnit.SECONDS.sleep(10);
					crea.verificaTipologiaCliente("Residenziale");
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
	
						if (prop.getProperty("FORZA_INDIRIZZO", "N").contentEquals("N")) {
							crea.verificaObbligatorietaCampiAnagraficaResidenziale();
							indirizzi.verificaObbligatorietaIndirizzoDomicilio(frameName);
						} else
							indirizzi.forzaindirizzoDomicilio(frameName, prop.getProperty("PROVINCIA"),
									prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					}
					else
					{//Ubiest NON Attivo
							crea.verificaObbligatorietaCampiAnagraficaResidenziale();
							indirizzi.forzaindirizzoDomicilioUBIEST(frameName, prop.getProperty("PROVINCIA"),
								prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					}
					break;

				default:
					throw new Exception(
							"Tipologia cliente diversa da RESIDENZIALE/IMPRESA/CONDOMINIO/BUSINESS. Impossibile proseguire");
				}

			}

			logger.write("Verifica Anagrafica OK");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
