package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CarrelloComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneFibraComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CreaOffertaVolturaSenzaAccollo_Finalizzazione {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			SezioneFibraComponent fibra=new SezioneFibraComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			//voltura
			CarrelloComponent carrello=new CarrelloComponent(driver);
			carrello.checkAfterCheckoutEffettuato(prop.getProperty("PRODOTTO"), prop.getProperty("POD"));
			logger.write("Andare nella sezione CVP e posizionarsi sul tasto già abilitato <Confema> e fare click - Start");
			offer.clickComponent(offer.buttonConfermaCvp);
			gestione.checkSpinnersSFDC();
			//offer.verificaCvp();
			logger.write("Andare nella sezione CVP e posizionarsi sul tasto già abilitato <Confema> e fare click - Completed");
			
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si'  e click su 'Conferma' - Start");
			offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
			offer.scrollComponent(offer.sezioneContattiEConsensi);
			offer.clickComponent(offer.checkBoxSiContattiConsensiVolture);
			TimeUnit.SECONDS.sleep(1);
			offer.clickComponent(offer.buttonConfermaContattiConsensi);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si'  e click su 'Conferma' - Completed");
			
			logger.write("Navigare fino alla sezione Fatturazione Elettronica  e click su 'Conferma' - Start");
			offer.clickComponent(offer.buttonConfermaFatElettronica);
			gestione.checkSpinnersSFDC();
			logger.write("Navigare fino alla sezione Fatturazione Elettronica  e click su 'Conferma' - Completed");
		

			logger.write("Navigare fino alla sezione Fibra, popola i campi indirizzio e click su 'Verifica' - start");
			fibra.verifyComponentExistence(fibra.sezioneFibra);
			fibra.scrollComponent(fibra.sezioneFibra);
			fibra.popolareCampo(prop.getProperty("PROVINCIA_COMUNE"), fibra.ProvinciaSezioneFibra);
			fibra.popolareCampo(prop.getProperty("PROVINCIA_COMUNE"), fibra.ComuneSezioneFibra);
			fibra.popolareCampo(prop.getProperty("INDIRIZZO COMPLETO"), fibra.IndirizzoSezioneFibra);
			fibra.popolareCampo(prop.getProperty("NUMERO CIVICO"), fibra.CivicoSezioneFibra);
			offer.clickComponent(fibra.VerificaButtonSezioneFibra);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			offer.verifyComponentExistence(fibra.IndirizzoVerificatoButton);
			
			logger.write("Navigare fino alla sezione Fibra, popola i campi indirizzio e click su 'Verifica' - completed");
			
			logger.write("Accetto i consensi, popola cellulafre, conferma cellulare e email e click su 'Conferma' - start");
			
			offer.clickComponent(fibra.AccettoConsensiSezioneFibra);
			
			//fibra.popolaConfermaCellulareSezioneFibra(prop.getProperty("CELLULARE"));
			//TimeUnit.SECONDS.sleep(1);
			//fibra.popolaEmailSezioneFibra(prop.getProperty("MAIL"));
			//TimeUnit.SECONDS.sleep(1);
			fibra.popolareCampo(prop.getProperty("CELLULARE"), fibra.ConfermaCellulareSezioneFibra);
			fibra.popolareCampo(prop.getProperty("email"), fibra.EmailSezioneFibra);
			
			offer.clickComponent(fibra.ConsensTrattDatiPersonSezFibra);
			TimeUnit.SECONDS.sleep(5);
			
			offer.clickComponent(fibra.ConfermaButtonSezioneFibra);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			logger.write("Accetto i consensi, popola cellulare, conferma cellulare e email e click su 'Conferma' - completed");
			
			
			if (prop.getProperty("MODALITA_FIRMA","").contentEquals("DOCUMENTI VALIDI")) {
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DOCUMENTI VALIDI', canale con 'EMAIL' e popolare il campo email  e click su 'Conferma' - Start");
				offer.popolareCampo(prop.getProperty("MODALITA_FIRMA"), offer.campoModalitaFirma);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("CANALE"), offer.campoCanale);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("EMAIL"), offer.campoEmail);
				TimeUnit.SECONDS.sleep(1);
				offer.clickComponent(offer.buttonConfermaModFirmaCanale);
				gestione.checkSpinnersSFDC();
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DOCUMENTI VALIDI', canale con 'EMAIL' e popolare il campo email  e click su 'Conferma' - Completed");
			}
			else if (prop.getProperty("MODALITA_FIRMA","").contentEquals("DIGITAL")) {
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DIGITAL', canale con 'EMAIL', popolare i campi email, cellulare e click su 'Conferma' - Start");
				offer.popolareCampo("DIGITAL", offer.campoModalitaFirma);
				offer.checkCampiModFirma();	
				TimeUnit.SECONDS.sleep(1);
				logger.write("popolare campo email in modo errato");
				offer.popolareCampo("d.calabresi.reply.it", offer.campoEmail);
				TimeUnit.SECONDS.sleep(1);
				
				logger.write("verifica corretto messaggio di errore di indirizzo non valido start ");
				offer.verifyComponentExistence(offer.messaggioErroreEmailNonValida);
				TimeUnit.SECONDS.sleep(1);
				logger.write("verifica corretto cmessaggio di errore di indirizzo non valido compleated");
				
				logger.write("popolare campo email in modo giusto");
				offer.popolareCampo(prop.getProperty("EMAIL"), offer.campoEmail);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("CELLULARE"), offer.campoCellulare);
				TimeUnit.SECONDS.sleep(1);
				offer.clickComponent(offer.buttonConfermaModFirmaCanale);
				gestione.checkSpinnersSFDC();
				
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DIGITAL', canale con 'EMAIL', popolare i campi email, cellulare e click su 'Conferma' - Completed");
			}
			else {
			logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'NO VOCAL'  e click su 'Conferma' - Start");
			offer.popolareCampo("DIGITAL", offer.campoModalitaFirma);
			offer.checkCampiModFirma();
			offer.popolareCampo("NO VOCAL", offer.campoModalitaFirma);
			offer.clickComponent(offer.buttonConfermaModFirmaCanale);
			gestione.checkSpinnersSFDC();
			logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'NO VOCAL'  e click su 'Conferma' - Completed");
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
