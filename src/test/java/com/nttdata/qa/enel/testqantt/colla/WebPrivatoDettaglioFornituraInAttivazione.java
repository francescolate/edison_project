package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CheckPrivateAreaComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.WebPrivatoDettaglioFornituraInAttivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.sun.jna.platform.win32.Wdm;

public class WebPrivatoDettaglioFornituraInAttivazione {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			WebPrivatoDettaglioFornituraInAttivazioneComponent wpdfia = new WebPrivatoDettaglioFornituraInAttivazioneComponent(driver);
			wpdfia.checkDocumentReadyState();
			
			logger.write("Controllando se la fornitura esiste e clicca sul button Visualizza Stato - START");
			wpdfia.verifyComponentExistence(By.xpath(wpdfia.Visualizzalebollette.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			//wpdfia.clickComponent(By.xpath(wpdfia.Visualizzalebollette.replace("$customerCode$", prop.getProperty("CUSTOMER_CODE"))));
			logger.write("Controllando se la fornitura esiste e clicca sul button Visualizza Stato - COMPLETED");
			
			logger.write("controllando la presenza dell'icona della fornitura associata al test - START");
			wpdfia.verifyComponentExistence(wpdfia.supplyIcon);
			logger.write("controllando la presenza dell'icona della fornitura associata al test - COMPLETED");
			
			/*logger.write("confrontando i dettagli della fornitura - START");
			Thread.sleep(10000);
			wpdfia.compareText(wpdfia.supplyDetailsHeading, wpdfia.supplyAddressPrefix.replace("$supplyType$", prop.getProperty("SUPPLY_TYPE"))+
			prop.getProperty("SUPPLY_ADDRESS")+wpdfia.customerNumber+prop.getProperty("CUSTOMER_CODE"), true);
			logger.write("confrontando i dettagli della fornitura - COMPLETED");*/
			
			/*logger.write("verificando/cliccando il pulsanse 'I' - START");
			wpdfia.verifyComponentExistence(wpdfia.tipoAttivazioneInfoButton);
			wpdfia.jcClickComponent(wpdfia.tipoAttivazioneInfoButton);
			logger.write("verificando/cliccando il pulsanse 'I' - COMPLETED");
			
			logger.write("verificando tutti i dettagli della fornitura - START");
			wpdfia.verifyComponentExistence(wpdfia.subentroInnerText);
			wpdfia.compareText(wpdfia.subentroInnerText, wpdfia.subentroInfoTextNew, true);
			wpdfia.clickComponent(wpdfia.tipoAttivazioneInfoCloseButton);
			wpdfia.clickComponent(wpdfia.mostraDiPiu);
			wpdfia.compareText(wpdfia.supplyDetailContainer, wpdfia.moreSupplyDetailsNew, true);
			wpdfia.compareText(wpdfia.lavoriSulContatoreHeading, wpdfia.lavoriSulContatoreTextNew, true);
			wpdfia.compareText(wpdfia.preventivoContainer, wpdfia.preventivoTextNew, true);
			wpdfia.compareText(wpdfia.statoDiAtticazioneContainer, wpdfia.statoDiAttivazioneText, true);
			logger.write("verificando tutti i dettagli della fornitura - COMPLETED");*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
