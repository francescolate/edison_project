package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CompilaIndirizziVolturaConAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);

			if (Costanti.statusUbiest.contentEquals("OFF")) {
				// sezione Indirizzi di fatturazione 
				
				if (offer.verifyClickability(offer.buttonConfermaIndirizzoFat)) {
					offer.clickComponent(offer.buttonConfermaIndirizzoFat);
					gestione.checkSpinnersSFDC();
				} else if (offer.verifyClickability(offer.buttonVerificaIndirizzoFat)) {
					offer.clickComponent(offer.buttonVerificaIndirizzoFat);
					gestione.checkSpinnersSFDC();
					// forza indirizzo 
					compila.ForzaIndirizzoFatturazione(prop.getProperty("CAP"));
					offer.clickComponent(offer.buttonConfermaIndirizzoFat);
					
				} 
				
				// sezione Indirizzi di Residenza/sede Legale
				if (offer.verifyClickability(offer.buttonConfermaIndirizzoRes)) {
					offer.clickComponent(offer.buttonConfermaIndirizzoRes);
					gestione.checkSpinnersSFDC();
				} else if (offer.verifyClickability(offer.buttonModificaIndirizzoRes)) {
					offer.clickComponent(offer.buttonVerificaIndirizzoRes);
					gestione.checkSpinnersSFDC();
					// forza indirizzo 
					compila.ForzaIndirizzoResidenza(prop.getProperty("CAP"));
					offer.clickComponent(offer.buttonConfermaIndirizzoRes);
				}
			
				// sezione Indirizzi Ultima Fattura
				if (offer.verifyClickability(offer.buttonConfermaIndUltimaFat)) {
					offer.clickComponent(offer.buttonConfermaIndUltimaFat);
					gestione.checkSpinnersSFDC();	
				} else {
					compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzatoSezUltimaFattura("Indirizzo Ultima Fattura", "00198", "ROMA");
				}
				
			}
			else {
			if (util.verifyExistence(offer.labelIndirizzoVerificato, 5)) {
				offer.verifyClickability(offer.buttonConfermaIndirizzoFat);
				offer.clickComponent(offer.buttonConfermaIndirizzoFat);
				gestione.checkSpinnersSFDC();
				//offer.verifyComponentExistence(offer.labelIndirizzoResVerificato);
				if (util.verifyExistence(offer.labelIndirizzoResVerificato, 5)) {
					logger.write("Indirizzo Res già verificato");
					if(util.verifyExistence(offer.buttonConfermaIndirizzoRes, 5)
							&& util.verifyClickability(offer.buttonConfermaIndirizzoRes, 5))
					{
						logger.write("Bottone conferma Abilitat - Click su Bottone conferma per indirizzo Res");
						util.objectManager(offer.buttonConfermaIndirizzoRes,util.scrollAndClick);
					}
					else
					{
						logger.write("Bottone Conferma non abilitato - Click su Bottone Modifica per indirizzo Res");
						offer.clickComponent(offer.buttonModificaIndirizzoRes);
						TimeUnit.SECONDS.sleep(2);
						offer.clickComponent(offer.buttonVerificaIndirizzoRes);
						TimeUnit.SECONDS.sleep(2);
						offer.clickComponent(offer.buttonConfermaIndirizzoRes);
						gestione.checkSpinnersSFDC();
						TimeUnit.SECONDS.sleep(2);
					}
					//offer.popolaCampiIndirizzoResidenzaSeVuoti(prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					//offer.clickComponent(offer.buttonVerificaIndirizzoRes);
					TimeUnit.SECONDS.sleep(5);
					if (util.verifyExistence(offer.labelIndirizzoUltFattVerificato, 30)) {
						offer.clickComponent(offer.buttonConfermaIndUltimaFat);
					}
				
					
//					if (util.verifyExistence(offer.labelIndirizzoResNonVerificato, 30)) {
//						offer.popolaIndirizzoRes();
//					}
//				
				}
			}
			else {
				
			
				logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di Residenza', cliccare su pulsante 'Verifica', verificare esistenza della label 'Indirizzo verificato' e verificare che il pulsante 'Conferma' si abiliti - Start");
				offer.popolaCampiIndirizzoResidenzaSeVuoti(prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				offer.clickComponent(offer.buttonVerificaIndirizzoRes);
				TimeUnit.SECONDS.sleep(2);
				offer.verifyComponentExistence(offer.labelIndirizzoResVerificato);
				offer.verifyClickability(offer.buttonConfermaIndirizzoRes);
				offer.clickComponent(offer.buttonConfermaIndirizzoRes);
				gestione.checkSpinnersSFDC();
				logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di Residenza', cliccare su pulsante 'Verifica', verificare esistenza della label 'Indirizzo verificato' e verificare che il pulsante 'Conferma' si abiliti - Completed");
				
			logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di Fatturazione', cliccare su pulsante 'Verifica', verificare esistenza della label 'Indirizzo verificato' e verificare che il pulsante 'Conferma' si abiliti - Start");
			offer.popolaCampiIndirizzoFatturazioneSeVuoti(prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
			offer.clickComponent(offer.buttonVerificaIndirizzoFat);
			TimeUnit.SECONDS.sleep(2);
			offer.verifyComponentExistence(offer.labelIndirizzoVerificato);
			offer.verifyClickability(offer.buttonConfermaIndirizzoFat);
			offer.clickComponent(offer.buttonConfermaIndirizzoFat);
			gestione.checkSpinnersSFDC();
			logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di Fatturazione', cliccare su pulsante 'Verifica', verificare esistenza della label 'Indirizzo verificato' e verificare che il pulsante 'Conferma' si abiliti - Completed");
			
			
			logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di ultima fatturazione', cliccare su pulsante 'Verifica', verificare esistenza della label 'Indirizzo verificato' e verificare che il pulsante 'Conferma' si abiliti - Start");
			offer.popolaCampiIndirizzoUltimaFatturaSeVuoti(prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
			offer.clickComponent(offer.buttonVerificaIndUltimaFat);
			TimeUnit.SECONDS.sleep(2);
			offer.verifyComponentExistence(offer.labelIndirizzoUltFattVerificato);
			offer.verifyClickability(offer.buttonConfermaIndUltimaFat);
			logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di ultima fatturazione', cliccare su pulsante 'Verifica', verificare esistenza della label 'Indirizzo verificato' e verificare che il pulsante 'Conferma' si abiliti - Completed");
			
			
			logger.write("click sui pulsanti 'Conferma' nelle sezioni 'Indirizzo di Fatturazione' e 'Indirizzo di Residenza' - Start");
			
			gestione.checkSpinnersSFDC();
			
			logger.write("click sui pulsanti 'Conferma' nelle sezioni 'Indirizzo di Fatturazione' e 'Indirizzo di Residenza' - Completed");
			}
			TimeUnit.SECONDS.sleep(5);
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
