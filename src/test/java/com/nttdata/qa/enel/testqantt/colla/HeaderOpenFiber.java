package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HeaderOpenFiber {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			logger.write("Corretta visualizzazione del menù a sinistra con le sue voci / sezioni  - Start");
			HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
						
			By altrisiti_label=hamburger.altriSitiLabel;
			hamburger.verifyComponentExistence(altrisiti_label);
			hamburger.checkColor(altrisiti_label, "DarkGray", "'Altri siti'");
						
			By openFiber_Link=hamburger.openFiberLink;
			hamburger.verifyComponentExistence(openFiber_Link);
			
			By enelcuore_Link=hamburger.enelcuoreLink;
			hamburger.verifyComponentExistence(enelcuore_Link);
			
			By sponsorizzazioni_Link=hamburger.sponsorizzazioniLink;
			hamburger.verifyComponentExistence(sponsorizzazioni_Link);
			
			By negoziazioneparitetica_Link=hamburger.negoziazionepariteticaLink;
			hamburger.verifyComponentExistence(negoziazioneparitetica_Link);
						
			By sitiGlobali_Label=hamburger.sitiGlobaliLabel;
			hamburger.verifyComponentExistence(sitiGlobali_Label);
			//hamburger.verifyColorLabel(sitiGlobali_Label,"#999999");
			hamburger.checkColor(sitiGlobali_Label, "DarkGray", "'Siti globali'");
			
			By enelGroup_Link=hamburger.enelGroupLink;
			hamburger.verifyComponentExistence(enelGroup_Link);
			
			By enelGreenPower_Link=hamburger.enelGreenPowerLink;
			hamburger.verifyComponentExistence(enelGreenPower_Link);
			
			By enelX_Link=hamburger.enelXLink;
			hamburger.verifyComponentExistence(enelX_Link);
			
			By globalTrading_Link=hamburger.globalTradingLink;
			hamburger.verifyComponentExistence(globalTrading_Link);
			
			By enelStartup_Link=hamburger.enelStartupLink;
			hamburger.verifyComponentExistence(enelStartup_Link);
			
			By globalProcurement_Link=hamburger.globalProcurementLink;
			hamburger.verifyComponentExistence(globalProcurement_Link);
			
			By openInnovability_Link=hamburger.openInnovabilityLink;
			hamburger.verifyComponentExistence(openInnovability_Link);
			logger.write("click sull' 'hamburger menu' e check sulla presenza dei suoi tre menu - Completed");
			
			logger.write("click sul link 'OPEN FIBER' e check del suo link - Start");
		//	hamburger.clickComponent(By.xpath("//span[@class='icon-menu']"));
			String winHandleBefore = driver.getWindowHandle();
			hamburger.clickComponent(openFiber_Link);

			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			hamburger.checkUrl(prop.getProperty("LINK_OPEN_FIBER"));
			By accetta_OpenFiber=hamburger.accettaOpenFiber;
			hamburger.verifyComponentExistence(accetta_OpenFiber);
			logger.write("Corretta visualizzazione del menù a sinistra con le sue voci / sezioni  - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
