package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HeaderLogin {

	public static void main(String[] args) throws Exception {
		
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);
			try {
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				HeaderComponent login = new HeaderComponent(driver);
				
				logger.write("sull' home page al link '"+prop.getProperty("LINK")+"' e' visibile l' Header - Start ");
				By electricityandgasLabel=login.luceAndGasTab;
				login.verifyComponentExistence(electricityandgasLabel);
				
				By companiesLabel=login.impreseTab;
				login.verifyComponentExistence(companiesLabel);
				
//				By withyouLabel=login.insiemeateTab;
//				login.verifyComponentExistence(withyouLabel);
			    
				By storiesLabel=login.storieTab;
				login.verifyComponentExistence(storiesLabel);
			    
				By futur_eLabel=login.futureTab;
				login.verifyComponentExistence(futur_eLabel);
				
				By mediaLabel=login.mediaTab;
				login.verifyComponentExistence(mediaLabel);
				
				By supportLabel=login.supportoTab;
				login.verifyComponentExistence(supportLabel);		
				
				By userIcon=login.iconUser;
				login.verifyComponentExistence(userIcon);
				
				By glassIcon=login.magnifyingglassIcon;
				login.verifyComponentExistence(glassIcon);
				HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
				By menuIcon=hamburger.iconMenu;
				hamburger.verifyComponentExistence(menuIcon);
				logger.write("sull' home page al link '"+prop.getProperty("LINK")+"' e' visibile l' Header - Completed ");

				prop.setProperty("RETURN_VALUE", "OK");
			} catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}
	}

}
