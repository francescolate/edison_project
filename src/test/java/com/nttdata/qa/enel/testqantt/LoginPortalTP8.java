 
package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class LoginPortalTP8 
{
	//private Properties prop = null;
	
	@Step("Login Salesforce")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			
			//Istanzia oggetto Selenium WebDriver
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			//System.out.println(prop.getProperty("POD_ELE"));
		
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				if(!prop.containsKey("START_TIME")){
					Date d = new Date();
					SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String s = fmt.format(d);
					prop.setProperty("START_TIME", s);
				}
				
				LoginPageComponent page = new LoginPageComponent(driver);

				//credenziali di accesso al portale basic https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/
				System.out.println("https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it");
				page.launchLink("https://www-coll1.enel.it");

				Thread.currentThread().sleep(30000);
				
				logger.write("Apertura portale per TP8 - Start");
				page.launchLink(prop.getProperty("CONFIRMATIONLINK"));
				logger.write("Apertura portale per TP8 - Completed");
				TimeUnit.SECONDS.sleep(5);
				page.clickComponentIfExist(page.buttonAccedi);
				TimeUnit.SECONDS.sleep(15);
				logger.write("Inserimento credenziali portale per TP8 - Start");
				page.enterLoginParameters(page.username,prop.getProperty("USERNAME"));
				logger.write("Inserimento Username");
				page.enterLoginParameters(page.password, prop.getProperty("PASSWORD"));
				logger.write("Inserimento credenziali portale per TP8 - Completed");
				page.clickComponentIfExist(page.buttonLoginAccedi);
				logger.write("Accesso al portale");
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				
				if(prop.getProperty("USERNAME").contentEquals(Costanti.utenza_admin_salesforce)){
					new RicercaOffertaComponent(driver).cliccaSwitch();	
				}
			}
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
