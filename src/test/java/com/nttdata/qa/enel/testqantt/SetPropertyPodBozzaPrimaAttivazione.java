package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetPropertyPodBozzaPrimaAttivazione {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")){
				prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
				prop.setProperty("STATO_RICHIESTA", "BOZZA");
				prop.setProperty("STATO_R2D", "N.D.");
				prop.setProperty("STATO_SAP", "N.D.");
				prop.setProperty("STATO_SEMPRE", "N.D.");
				prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - NULL / DRAFT");
			}


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
