package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SalvaEditaOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaOffertaInBozza {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		       logger.write("Modifica Offerta In Bozza - Start");
				SeleniumUtilities util = new SeleniumUtilities(driver);
				SalvaEditaOffertaComponent edita = new SalvaEditaOffertaComponent(driver);
				driver.switchTo().defaultContent();
				edita.switchToFrameByIndex(1);
				edita.clickComponentWithJse(edita.modificaQuote);
				driver.switchTo().defaultContent();
				edita.checkSpinnersSFDC();
				logger.write("Modifica Offerta In Bozza - Completed");
				driver.switchTo().defaultContent();
				
				
				if(prop.getProperty("TIPO_OPERAZIONE").contentEquals("PRIMA_ATTIVAZIONE_EVO")) {
					logger.write("Verifica riapertura offerta prima attivazione - Start");
					GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
					try {
					gestione.verifySectionExistence(By.xpath("//h2[text()='Riepilogo offerta']"));
					gestione.verifySectionExistence(By.xpath("//h2[text()='CVP']"));
					gestione.verifySectionExistence(By.xpath("//h2[text()='Selezione Uso Fornitura']"));
					gestione.verifySectionExistence(By.xpath("//h2[text()='Fatturazione Elettronica']"));
					}catch(Exception e) {
						throw new Exception("Errore dopo aver cliccato sul pulsante Modifica per riprendere l'offerta "+e.getMessage());
					}
					logger.write("Verifica riapertura offerta prima attivazione - Completed");
					
				}
				
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
