package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.SupportoCambioPotenzaContatore;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_47_CambioPotenzaContatore {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			SupportoCambioPotenzaContatore sup = new SupportoCambioPotenzaContatore(driver);
			String supportoURL = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto";
			String supportoURL1 = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto.html";
			String supportoPotenzaETensioneURL = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto/faq/cambio-potenza-contatore";
			prop.setProperty("HOMEPAGE_TITLE1", "Area riservata");
			prop.setProperty("HOMEPAGE_TITLE2", "Homepage");
			prop.setProperty("HOMEPAGE_TITLE_DESCRIPTION", "Benvenuto nella tua area dedicata Business");
			prop.setProperty("SERVIZI_PATH1", "Area riservata");
			prop.setProperty("SERVIZI_PATH2", "Servizi");
			prop.setProperty("SERVIZI_HEADING", "Servizi");
			prop.setProperty("SERVIZI_TEXT", "In questa pagina trovi tutti i servizi.");
			prop.setProperty("MODIFICA_TENSIONE_PATH1", "Area riservata");
			prop.setProperty("MODIFICA_TENSIONE_PATH2", "Fornitura");
			prop.setProperty("MODIFICA_TENSIONE_PATH3", "Potenza e tensione");
			prop.setProperty("MODIFICA_TENSIONE_HEADING", "Potenza e tensione");
			prop.setProperty("MODIFICA_TENSIONE_TEXT", "Per richiedere aumenti e/o diminuzioni di potenza superiori a 50 kW chiama il numero verde 800 900 860.");
			
			//1
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			By accettaCookie = log.buttonAccetta;
			if(log.verifyComponentExistence(accettaCookie,10))
					log.clickComponent(accettaCookie);
 			logger.write("apertura del portale web Enel di test - Completed");
 			
 			//2
 			logger.write("Clicking on Supporto HEader and verification of data-Started");	
 			Thread.sleep(10000);
			sup.clickComponent(sup.supportoHeader);
			
			sup.checkURLAfterRedirection(supportoURL);
			sup.compareText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.compareText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
			sup.compareText(sup.supportoHeading, sup.SUPPORTO_HEADING, true);
		//	sup.compareText(sup.supportoTitleText, SupportoCambioPotenzaContatore.SUPPORTO_TITLE_TEXT, true);
			logger.write("Clicking on Supporto HEader and verification of data-Completed");
			
			//3
			logger.write("Clicking on contratti e modulistica and verification of menu-Started");
			Thread.sleep(5000);
			sup.clickComponent(sup.contrattiemodulistica);
			sup.verifyComponentExistence(sup.sosLuceeGas);
			sup.compareText(sup.sosLuceeGas, sup.SOS_LUCE_E_GAS, true);
			
			sup.verifyComponentExistence(sup.autolettura);
			sup.compareText(sup.autolettura, sup.AUTOLETTURA, true);
			
			sup.verifyComponentExistence(sup.appEnel);
			sup.compareText(sup.appEnel, sup.APP_ENEL, true);
			
			sup.verifyComponentExistence(sup.gestisciITuoiDati);
			sup.compareText(sup.gestisciITuoiDati, sup.GESTISCI_I_TUOI_DATI, true);
			
			sup.verifyComponentExistence(sup.modificaGagliaAConsumi);
			sup.compareText(sup.modificaGagliaAConsumi, sup.MODIFICA_TAGLIA_A_CONSUMI, true);
			
			sup.verifyComponentExistence(sup.guidaAiServizieTutorial);
			sup.compareText(sup.guidaAiServizieTutorial, sup.GUIDA_AI_SERVIZI_E_TUTORIAL, true);
			
			sup.verifyComponentExistence(sup.serviziSMS);
			sup.compareText(sup.serviziSMS, sup.SERVIZI_SMS, true);
			
			sup.verifyComponentExistence(sup.modulistica);
			sup.compareText(sup.modulistica, sup.MODULISTICA, true);
			
			sup.verifyComponentExistence(sup.subentro);
			sup.compareText(sup.subentro, sup.SUBENTRO, true);
			
			sup.verifyComponentExistence(sup.volutra);
			sup.compareText(sup.volutra, sup.VOLUTRA, true);
			
			sup.verifyComponentExistence(sup.primaAttivazione);
			sup.compareText(sup.primaAttivazione, sup.PRIMA_ATTIVAZIONE, true);
			
			sup.verifyComponentExistence(sup.modificaPotenzaeTensione);
			sup.compareText(sup.modificaPotenzaeTensione, sup.MODIFICA_POTENZA_TENSIONE, true);
			
			sup.verifyComponentExistence(sup.disAttivaZione);
			sup.compareText(sup.disAttivaZione, sup.DISATTIVAZIONE, true);
			
			sup.verifyComponentExistence(sup.caricaDocumenti);
			sup.compareText(sup.caricaDocumenti, sup.CARICA_DOCUMENTI, true);
			
			sup.verifyComponentExistence(sup.pianoSalvaBlackOut);
			sup.compareText(sup.pianoSalvaBlackOut, sup.PIANO_SALVA_BLACK_OUT, true);
			
			sup.verifyComponentExistence(sup.faqAreaclienti);
			sup.compareText(sup.faqAreaclienti, sup.FAQ_AREA_CLIENTI, true);
			
			sup.verifyComponentExistence(sup.assicurazioneClientiFinale);
			sup.compareText(sup.assicurazioneClientiFinale, sup.ASSICURAZIONE_CLIENTI_FINALE, true);
			
			sup.verifyComponentExistence(sup.contattaci);
			sup.compareText(sup.contattaci, sup.CONTATTACI, true);
			
			sup.verifyComponentExistence(sup.conciliazionieRisoluzioneDelleControversie);
			sup.compareText(sup.conciliazionieRisoluzioneDelleControversie, sup.CONCILIAZIONI_E_RESOLUZIONE, true);
			
			sup.verifyComponentExistence(sup.guidaAllaBollettaLuceGas);
			sup.compareText(sup.guidaAllaBollettaLuceGas, sup.GUIDA_ALLA_BOLLETTA_LUCE_E_GAS, true);
			logger.write("Clicking on contratti e modulistica and verification of menu-Completed");
			
			//4
			logger.write("Clicking on modifica Potenza e Tensione and verification of Data-Started");
			Thread.sleep(5000);
			sup.clickComponent(sup.modificaPotenzaeTensione);
			
			sup.checkURLAfterRedirection(supportoPotenzaETensioneURL);
			sup.compareText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.compareText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
			sup.containsText(sup.supportoHomePagePath3, sup.SUPPORTO_HOMEPAGE_PATH3, true);
			sup.compareText(sup.PotenzaeTenzioneTitle, sup.POTENZA_E_TENZIONE_TITLE, true);
			sup.compareText(sup.potenzaETenzioneText, sup.POTENZA_E_TENZIONE_TEXT, true);
			
			//5
			
			sup.compareText(sup.potenzaContent1, sup.POTENZA_CONTENT1, true);
			sup.compareText(sup.potenzaContent2, sup.POTENZA_CONTENT2, true);
			sup.compareText(sup.potenzaContent3, sup.POTENZA_CONTENT3, true);
			sup.compareText(sup.potenzaContent4, sup.POTENZA_CONTENT4, true);
			
			//6
			sup.verifyComponentExistence(sup.email);
			sup.verifyComponentExistence(sup.password);
			sup.verifyComponentExistence(sup.accediButton);
			sup.verifyComponentExistence(sup.nonRegistratoRegisrtiLink);
			logger.write("Clicking on modifica Potenza e Tensione and verification of Data-Completed");
			
			//7
			logger.write("verification of Email and Password fields and navigating to HomePage Area Riserveta BSN customer -Started");
			prop.setProperty("EMAIL", "r.32019p109statoc.ontiweb@gmail.com");
			prop.setProperty("PASSWORD", "Password01");
					
			sup.insertText(sup.email, prop.getProperty("EMAIL"));
			sup.insertText(sup.password, prop.getProperty("PASSWORD"));
			sup.clickComponent(sup.accediButton);
			Thread.sleep(10000);
			sup.clickComponent(By.xpath("//button[text()='MODIFICA POTENZA E/O TENSIONE']"));
			Thread.sleep(10000);
  			sup.compareText(sup.homePageTitle1,prop.getProperty("HOMEPAGE_TITLE1"), true);
			sup.compareText(sup.homePageTitle2,prop.getProperty("HOMEPAGE_TITLE2"), true);
			sup.compareText(sup.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"),true);
			logger.write("verification of Email and Password fields and navigating to HomePage Area Riserveta BSN customer -Completed");			
			
			//8
			logger.write("Clicking on Servizi item and Verification of data -Started");
			sup.clickComponent(sup.servizii);
			sup.compareText(sup.serviziPath1,prop.getProperty("SERVIZI_PATH1"), true);
			sup.compareText(sup.serviziPath2,prop.getProperty("SERVIZI_PATH2"), true);
			sup.compareText(sup.serviziHeading,prop.getProperty("SERVIZI_HEADING"), true);
			sup.compareText(sup.serviziText,prop.getProperty("SERVIZI_TEXT"),true);
			
			
			//9
			sup.verifyComponentExistence(sup.modificaPotenzaTensione);
			logger.write("Clicking on Servizi item and Verification of data -Completed");
			
			//10
			logger.write("Clicking on modifica E Potenza and Verification of data -Started");
			Thread.sleep(5000);
			sup.clickComponent(sup.modificaPotenzaTensioneButton);
			Thread.sleep(10000);
			sup.compareText(sup.modficaPotenzaTensionePath1,prop.getProperty("MODIFICA_TENSIONE_PATH1"), true);
			sup.compareText(sup.modficaPotenzaTensionePath2,prop.getProperty("MODIFICA_TENSIONE_PATH2"), true);
			sup.compareText(sup.modficaPotenzaTensionePath3,prop.getProperty("MODIFICA_TENSIONE_PATH3"), true);
			sup.compareText(sup.modificaPotenzaTensioneHeading,prop.getProperty("MODIFICA_TENSIONE_HEADING"), true);
			sup.compareText(sup.modificaPotenzaTensioneText,prop.getProperty("MODIFICA_TENSIONE_TEXT"),true);
			logger.write("Clicking on modifica E Potenza and Verification of data -Completed");
			
			//11
			logger.write("Clicking on supporto Header and Verification of data -Started");
			LoginPageComponent logg = new LoginPageComponent(driver);
			logg.launchLink(prop.getProperty("WP_LINK"));
			 			
			sup.clickComponent(sup.supportoHeader);
			
			sup.checkURLAfterRedirection(supportoURL);
			sup.compareText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.compareText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
			sup.compareText(sup.supportoHeading, sup.SUPPORTO_HEADING, true);
			//sup.compareText(sup.supportoTitleText, SupportoCambioPotenzaContatore.SUPPORTO_TITLE_TEXT, true);
			logger.write("Clicking on supporto Header and Verification of data -Completed");
			
			//12
			logger.write("Clicking on contratti e modulistica and Verification of data -Started");
			Thread.sleep(15000);
			sup.clickComponent(sup.contrattiemodulistica);
			Thread.sleep(5000);
			sup.verifyComponentExistence(sup.sosLuceeGas);
			sup.compareText(sup.sosLuceeGas, sup.SOS_LUCE_E_GAS, true);
			
			sup.verifyComponentExistence(sup.autolettura);
			sup.compareText(sup.autolettura, sup.AUTOLETTURA, true);
			
			sup.verifyComponentExistence(sup.appEnel);
			sup.compareText(sup.appEnel, sup.APP_ENEL, true);
			
			sup.verifyComponentExistence(sup.gestisciITuoiDati);
			sup.compareText(sup.gestisciITuoiDati, sup.GESTISCI_I_TUOI_DATI, true);
			
			sup.verifyComponentExistence(sup.modificaGagliaAConsumi);
			sup.compareText(sup.modificaGagliaAConsumi, sup.MODIFICA_TAGLIA_A_CONSUMI, true);
			
			sup.verifyComponentExistence(sup.guidaAiServizieTutorial);
			sup.compareText(sup.guidaAiServizieTutorial, sup.GUIDA_AI_SERVIZI_E_TUTORIAL, true);
			
			sup.verifyComponentExistence(sup.serviziSMS);
			sup.compareText(sup.serviziSMS, sup.SERVIZI_SMS, true);
			
			sup.verifyComponentExistence(sup.modulistica);
			sup.compareText(sup.modulistica, sup.MODULISTICA, true);
			
			sup.verifyComponentExistence(sup.subentro);
			sup.compareText(sup.subentro, sup.SUBENTRO, true);
			
			sup.verifyComponentExistence(sup.volutra);
			sup.compareText(sup.volutra, sup.VOLUTRA, true);
			
			sup.verifyComponentExistence(sup.primaAttivazione);
			sup.compareText(sup.primaAttivazione, sup.PRIMA_ATTIVAZIONE, true);
			
			sup.verifyComponentExistence(sup.modificaPotenzaeTensione);
			sup.compareText(sup.modificaPotenzaeTensione, sup.MODIFICA_POTENZA_TENSIONE, true);
			
			sup.verifyComponentExistence(sup.disAttivaZione);
			sup.compareText(sup.disAttivaZione, sup.DISATTIVAZIONE, true);
			
			sup.verifyComponentExistence(sup.caricaDocumenti);
			sup.compareText(sup.caricaDocumenti, sup.CARICA_DOCUMENTI, true);
			
			sup.verifyComponentExistence(sup.pianoSalvaBlackOut);
			sup.compareText(sup.pianoSalvaBlackOut, sup.PIANO_SALVA_BLACK_OUT, true);
			
			sup.verifyComponentExistence(sup.faqAreaclienti);
			sup.compareText(sup.faqAreaclienti, sup.FAQ_AREA_CLIENTI, true);
			
			sup.verifyComponentExistence(sup.assicurazioneClientiFinale);
			sup.compareText(sup.assicurazioneClientiFinale, sup.ASSICURAZIONE_CLIENTI_FINALE, true);
			
			sup.verifyComponentExistence(sup.contattaci);
			sup.compareText(sup.contattaci, sup.CONTATTACI, true);
			
			sup.verifyComponentExistence(sup.conciliazionieRisoluzioneDelleControversie);
			sup.compareText(sup.conciliazionieRisoluzioneDelleControversie, sup.CONCILIAZIONI_E_RESOLUZIONE, true);
			
			sup.verifyComponentExistence(sup.guidaAllaBollettaLuceGas);
			sup.compareText(sup.guidaAllaBollettaLuceGas, sup.GUIDA_ALLA_BOLLETTA_LUCE_E_GAS, true);
			logger.write("Clicking on contratti e modulistica and Verification of data -Completed");
			
			//13
			logger.write("Clicking on modifica e Potenza and Verification of data -Started");
			sup.clickComponent(sup.modificaPotenzaeTensione);
			
			sup.checkURLAfterRedirection(supportoPotenzaETensioneURL);
			sup.compareText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.compareText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
			sup.compareText(sup.supportoHomePagePath3, sup.SUPPORTO_HOMEPAGE_PATH3, true);
			sup.compareText(sup.PotenzaeTenzioneTitle, sup.SUPPORTO_HOMEPAGE_PATH4, true);
			sup.compareText(sup.potenzaETenzioneText, sup.POTENZA_E_TENZIONE_TEXT, true);
			
			sup.compareText(sup.potenzaContent1, sup.POTENZA_CONTENT1, true);
			sup.compareText(sup.potenzaContent2, sup.POTENZA_CONTENT2, true);
			sup.compareText(sup.potenzaContent3, sup.POTENZA_CONTENT3, true);
			sup.compareText(sup.potenzaContent4, sup.POTENZA_CONTENT4, true);
			logger.write("Clicking on modifica e Potenza and Verification of data -Completed");
			
			//14
			logger.write(" Verification of non availability of accedi and VaiallareaClient Button -Started");
			Thread.sleep(10000);
			sup.compareText(sup.serviziodisponsibleText, sup.SERVIZIO_DISPONSIBLE_TEXT, true);
			
			sup.isElementNotPresent(sup.VaiallareaclientiButton);
			logger.write(" Verification of non availability of accedi and VaiallareaClient Button -Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
