package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaCardFibra {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);

			logger.write("check the card with FIBRA offer state active - START");

			By card_fibra_active = paf.card_fibra_active;

			paf.verifyComponentExistence(card_fibra_active);
			paf.verifyComponentVisibility(card_fibra_active);

			logger.write("check the card with FIBRA offer state active - COMPLETED");
			
			//STEP 4
			logger.write("check the address text in the card with FIBRA offer - START");

			By text_address_card_fibra = paf.text_address_card_fibra;

			paf.verifyComponentExistence(text_address_card_fibra);
			paf.verifyComponentVisibility(text_address_card_fibra);
//			paf.verifyComponentText(text_address_card_fibra, "VIA GRAZIOLI DON BARTOLOMEO 080 - 20161 MILANO MI");

			logger.write("check the address text in the card with FIBRA offer - COMPLETED");
			
			//STEP 5
			logger.write("check the color and text status in the card with FIBRA offer - START");

			By text_status_card_fibra = paf.text_status_card_fibra;

			paf.verifyComponentExistence(text_status_card_fibra);
			paf.verifyComponentVisibility(text_status_card_fibra);
			Color green = Color.fromString("#008757");
			paf.verifyElementColor(text_status_card_fibra, green);

			logger.write("check the color and text status in the card with FIBRA offer - COMPLETED");
			
			//STEP 6
			logger.write("check the text number in the card with FIBRA offer - START");

			By text_number_card_fibra = paf.text_number_card_fibra;

			paf.verifyComponentExistence(text_number_card_fibra);
			paf.verifyComponentVisibility(text_number_card_fibra);
//			paf.verifyComponentText(text_number_card_fibra, "883569408");

			logger.write("check the text number in the card with FIBRA offer - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
