package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConfermaCheckListAttivazionSDD {


		
		@Step("Processo Switch Attivo EVO")
		public static void main(String[] args) throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {

				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					SeleniumUtilities util = new SeleniumUtilities(driver);
					IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
					CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
					
					
					TimeUnit.SECONDS.sleep(3);
					
					logger.write("Attendo checklist - Start");
					checkListModalComponent.attendiChecklist();
					logger.write("Attendo checklist - Completed");
					TimeUnit.SECONDS.sleep(5);
					logger.write("Verifica checklist e conferma - Start");
					checkListModalComponent.clickConferma();
					logger.write("Verifica checklist e conferma - Completed");
					String numeroRichiesta=intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6);
					prop.setProperty("NUMERO_RICHIESTA",numeroRichiesta );
					logger.write("Numero Richiesta Salvato");

				}
				prop.setProperty("RETURN_VALUE", "OK");
			} catch (Exception e) {
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//				return;
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
			}
		}

	

}
