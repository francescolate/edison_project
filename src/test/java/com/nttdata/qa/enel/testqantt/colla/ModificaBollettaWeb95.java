package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.AttivaBollettaWebComponent;
import com.nttdata.qa.enel.components.colla.ModificaBollettaWebComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaBollettaWeb95 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModificaBollettaWebComponent mbwc = new ModificaBollettaWebComponent(driver);
			
			mbwc.verifyComponentExistence(mbwc.serviziMenuItem);
			mbwc.clickComponent(mbwc.serviziMenuItem);
			
			mbwc.verifyComponentExistence(mbwc.serviziPerLeFornitureHeader);
			mbwc.scrollComponent(mbwc.serviziPerLeFornitureHeader);
			
			Thread.sleep(20000);
			
			mbwc.verifyComponentExistence(mbwc.bollettaWebTile);
			mbwc.clickComponent(mbwc.bollettaWebTile);
			
			Thread.sleep(20000);
			
			mbwc.verifyComponentExistence(mbwc.modificaIndirizzoEmailButton);
			mbwc.jsClickObject(mbwc.modificaIndirizzoEmailButton);
			
			Thread.sleep(20000);
			
			//mbwc.verifyComponentExistence(mbwc.numeroCliente);
			prop.setProperty("CUSTOMER_NUMBER", mbwc.jsGetCustomerNumber(mbwc.numeroCliente));
			
			mbwc.jsClickObject(mbwc.fornituraCheckBox);
			mbwc.jsClickObject(mbwc.continuaButton);
			
			mbwc.changeInputText(mbwc.inputEmail, "");
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputEmailError);
			mbwc.compareText(mbwc.inputEmailError, "Il campo Email è obbligatorio.", true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME").replace("@", ""));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputEmailError);
			mbwc.compareText(mbwc.inputEmailError, "Email non valida", true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME").replace(".com", ""));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputEmailError);
			mbwc.compareText(mbwc.inputEmailError, "Email non valida", true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME").replace(".com", ".co"));
			Thread.sleep(500);
			mbwc.clickComponent(mbwc.inputEmail);
			Thread.sleep(500);
			Robot robot = new Robot(); 
			robot.keyPress(KeyEvent.VK_M); 
			robot.delay(40);
			robot.keyRelease(KeyEvent.VK_M);
			Thread.sleep(500);
			mbwc.copy(mbwc.inputEmail);
			Thread.sleep(100);
//			mbwc.paste(mbwc.inputConfirmEmail);
//			mbwc.verifyComponentExistence(mbwc.inputEmailError);
//			mbwc.compareText(mbwc.inputConfirmEmailError, mbwc.copiaIncollaErrore, true);
			
			//i successivi 3 step sono ripetuti
			mbwc.changeInputText(mbwc.inputEmail, "");
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputEmailError);
			mbwc.compareText(mbwc.inputEmailError, mbwc.campoEmailObbligatorio, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME").replace("@", ""));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputEmailError);
			mbwc.compareText(mbwc.inputEmailError, mbwc.emailNonValida, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME").replace(".com", ""));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputEmailError);
			mbwc.compareText(mbwc.inputEmailError, mbwc.emailNonValida, true);
			//fine ripetizione
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
//			mbwc.changeInputText(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputMobile, "");
			mbwc.clickComponent(mbwc.continuaSecondButton);
			Thread.sleep(500);
			mbwc.verifyComponentExistence(mbwc.inputMobileError);
			mbwc.compareText(mbwc.inputMobileError, mbwc.campoMobileObbligatorio, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
			Thread.sleep(100);
//			mbwc.changeInputText(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			Thread.sleep(100);
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(100);
			mbwc.copy(mbwc.inputMobile);
			Thread.sleep(100);
			robot.keyPress(KeyEvent.VK_1);
			robot.delay(40);
			robot.keyRelease(KeyEvent.VK_1);
			/*Thread.sleep(500);
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputMobileError);
			mbwc.compareText(mbwc.inputMobileError, mbwc.cellulareNonValido, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER").substring(0, 7));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputMobileError);
			mbwc.compareText(mbwc.inputMobileError, mbwc.cellulareNonValido, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputMobile, "");
			Thread.sleep(100);
			mbwc.clickComponent(mbwc.inputMobile);
			Thread.sleep(500);
			robot = new Robot(); 
			robot.keyPress(KeyEvent.VK_M); 
			robot.delay(40);
			robot.keyRelease(KeyEvent.VK_M);
			Thread.sleep(500);
			mbwc.verifyComponentExistence(mbwc.inputMobileError);
			mbwc.compareText(mbwc.inputMobileError, mbwc.cellulareSoloNumeri, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			mbwc.verifyComponentExistence(mbwc.inputConfirmMobileError);
			mbwc.compareText(mbwc.inputConfirmMobileError, mbwc.campiCellulareNonCoincidono, true);
			
			mbwc.changeInputText(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(100);
			mbwc.clickComponent(mbwc.inputMobile);
			mbwc.paste(mbwc.inputConfirmMobile);
			Thread.sleep(100);
			mbwc.verifyComponentExistence(mbwc.inputConfirmMobileError);
			
			mbwc.fillInputField(mbwc.inputEmail, prop.getProperty("WP_USERNAME"));
			mbwc.fillInputField(mbwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			mbwc.changeInputText(mbwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			mbwc.fillInputField(mbwc.inputConfirmMobile, prop.getProperty("MOBILE_NUMBER"));
			mbwc.clickComponent(mbwc.continuaSecondButton);
			
			Thread.sleep(15000);
			
			mbwc.compareText(By.xpath(mbwc.verificaCorrettezzaEmail.replace("$email$", prop.getProperty("WP_USERNAME"))), prop.getProperty("WP_USERNAME"), true);
			mbwc.compareText(By.xpath(mbwc.verificaCorrettezzaMobile.replace("$mobile$", prop.getProperty("MOBILE_NUMBER"))), prop.getProperty("MOBILE_NUMBER"), true);
//			mbwc.compareText(By.xpath(mbwc.verificaCorrettezzaNumeroCliente.replace("$numeroCliente$", prop.getProperty("CUSTOMER_NUMBER"))), prop.getProperty("CUSTOMER_NUMBER"), true);
			mbwc.jsClickObject(mbwc.confermaButton);
			
			Thread.sleep(15000);
			
			mbwc.verifyComponentExistence(mbwc.fineButton);
			mbwc.jsClickObject(mbwc.fineButton);*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
