package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato331ACBAddebitoDirettoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_331_ACB_Addebito_Diretto {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato331ACBAddebitoDirettoComponent adb = new Privato331ACBAddebitoDirettoComponent(driver);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if path and heading of homePage is displayed - Start");
			By homePagePath = adb.areaRiservata;
			adb.verifyComponentExistence(homePagePath);
			adb.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			By homepageSubheading = adb.pageHeading;
			adb.verifyComponentExistence(homepageSubheading);
			adb.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Click on 'fornitureSelect' menu item - Start");
			By forniture = adb.fornitureSelect;
			adb.verifyComponentExistence(forniture);
			adb.clickComponent(forniture);
			logger.write("Click on 'fornitureSelect' menu item - Completed");
			
			By forniturePath = adb.areaRiservata;
			adb.verifyComponentExistence(forniturePath);
			adb.VerifyText(forniturePath, prop.getProperty("FORNITURE_PATH"));
			
			By fornitureHeading = adb.fornitureHeading;
			adb.verifyComponentExistence(fornitureHeading);
			adb.VerifyText(fornitureHeading, prop.getProperty("FORNITURE_HEADING"));
			
			By mostraFiltri = adb.mostraFiltri;
			adb.verifyComponentExistence(mostraFiltri);
			adb.clickComponent(mostraFiltri);
			
			By inputNumeroCliente = adb.inputNumeroCliente;
			adb.verifyComponentExistence(inputNumeroCliente);
			adb.clickComponent(inputNumeroCliente);
			
			By selectNumeroCliente_310512806 = adb.selectNumeroCliente_310512806;
			adb.verifyComponentExistence(selectNumeroCliente_310512806);
			adb.clickComponent(selectNumeroCliente_310512806);
			
			By applicaButton = adb.applicaButton;
			adb.verifyComponentExistence(applicaButton);
			adb.clickComponent(applicaButton);
			Thread.sleep(3000);
			
			By Vai_al_dettaglio_LINK = adb.Vai_al_dettaglio_LINK;
			adb.verifyComponentExistence(Vai_al_dettaglio_LINK);
			adb.clickComponent(Vai_al_dettaglio_LINK);
			Thread.sleep(3000);
			
			logger.write("Click on 'serviziSelect' menu item - Start");
			By servizi = adb.serviziSelect;
			adb.verifyComponentExistence(servizi);
			adb.clickComponent(servizi);
			logger.write("Click on 'serviziSelect' menu item - Completed");
			
			By serviziPath = adb.areaRiservata;
			adb.verifyComponentExistence(serviziPath);
			adb.VerifyText(serviziPath, prop.getProperty("SERVIZI_PATH"));
			
			By serviziSubtext = adb.serviziPageSubText;
			adb.verifyComponentExistence(serviziSubtext);
			adb.VerifyText(serviziSubtext, prop.getProperty("SERVIZI_SUBTEXT"));
			
			By AddebitoDiretto_WebTile = adb.AddebitoDiretto_WebTile;
			adb.verifyComponentExistence(AddebitoDiretto_WebTile);
			adb.clickComponent(AddebitoDiretto_WebTile);
			Thread.sleep(8000);
			
			By addebitoDirettoPath = adb.AddebitoDirettoPath;
			adb.verifyComponentExistence(addebitoDirettoPath);
			adb.VerifyText(addebitoDirettoPath, prop.getProperty("ADDEBITO_DIRETTO_PATH"));
			
			By AddebitoDiretto_PageTitle = adb.AddebitoDiretto_PageTitle;
			adb.verifyComponentExistence(AddebitoDiretto_PageTitle);
			adb.VerifyText(AddebitoDiretto_PageTitle, prop.getProperty("ADDEBITO_DIRETTO_TITLE"));
			
			By addebitoDirettoSubtext = adb.AddebitoDiretto_SubText;
			adb.verifyComponentExistence(addebitoDirettoSubtext);
			adb.VerifyText(addebitoDirettoSubtext, prop.getProperty("ADDEBITO_DIRETTO_SUBTEXT"));
			
			By addebitoDirettoNonActiveStatus = adb.AddebitoDiretto_Status;
			adb.verifyComponentExistence(addebitoDirettoNonActiveStatus);
			adb.VerifyText(addebitoDirettoNonActiveStatus, prop.getProperty("ADDEBITO_DIRETTO_NONACTIVE_STATUS"));
			
			adb.validateSupply(adb.AttivaAddebitoDiretto_Supply,prop.getProperty("NUMEROCLIENTE1"));
			
			By AttivaAddebitoDirettoButton = adb.AttivaAddebitoDiretto_button;
			adb.verifyComponentExistence(AttivaAddebitoDirettoButton);
			adb.clickComponent(AttivaAddebitoDirettoButton);
			Thread.sleep(10000);

			By attivazioneAddebitoDirettoPath = adb.areaRiservata;
			adb.verifyComponentExistence(attivazioneAddebitoDirettoPath);
			adb.VerifyText(attivazioneAddebitoDirettoPath, prop.getProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_PATH"));
			
			By AttivazioneAddebitoDiretto_Subtext = adb.AttivazioneAddebitoDiretto_Subtext;
			adb.verifyComponentExistence(AttivazioneAddebitoDiretto_Subtext);
			adb.VerifyText(AttivazioneAddebitoDiretto_Subtext, prop.getProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT"));
			
			adb.validateMultiAttivazioneSteps();
			
			logger.write("Verify the color of I_tuoi_dati - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num1, adb.serviceColor, "Multifornitura");
			logger.write("Verify the color of I_tuoi_dati - Start");
			
			adb.verifyComponentExistence(adb.checos);
			adb.VerifyText(adb.checos, prop.getProperty("CHECOS"));
			
			adb.verifyComponentExistence(adb.checosContent);
			adb.VerifyText(adb.checosContent, adb.ChecosContent);
			
			adb.validateAttivazioneAddebitoDirettoSupply(prop.getProperty("NUMEROCLIENTE1"),prop.getProperty("NUMEROCLIENTE2"));
			
			adb.clickComponent(adb.PROSEGUIButton);
			Thread.sleep(7000);
			
			adb.validateMultiAttivazioneSteps();
			
			adb.validateAttivazioneFields();
			
			adb.validateAttivazionePrepopulatedFields();
			
			adb.verifyCheckboxnotSelected(adb.IBAN_ESTERO_Checkbox);
			
			adb.clickComponent(adb.PROSEGUIButton);
			Thread.sleep(10000);
			
			By AttivazioneAddebitoDiretto_Subtext2 = adb.AttivazioneAddebitoDiretto_Subtext2;
			adb.verifyComponentExistence(AttivazioneAddebitoDiretto_Subtext2);
			adb.VerifyText(AttivazioneAddebitoDiretto_Subtext2, prop.getProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT2"));
			
			/*logger.write("Verify the color of I_tuoi_dati - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num1, adb.servicesColor, "I_tuoi_dati");
			logger.write("Verify the color of I_tuoi_dati - Start");
			
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num2, adb.serviceColor, "Riepilogo_e_conferma_dati");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");*/
			
			adb.validateAttivazionePostpopulatedFields();
			
			By SupplyInfo = adb.SupplyInfo;
			adb.verifyComponentExistence(SupplyInfo);
			adb.VerifyText(SupplyInfo, prop.getProperty("SUPPLY_INFO"));
			
			adb.confirmSupply(adb.AttivazioneAddebitoDiretto_ConfirmSupply,prop.getProperty("NUMEROCLIENTE1"),prop.getProperty("NUMEROCLIENTE2"));
			
			By EmailInfo = adb.EmailInfo;
			adb.verifyComponentExistence(EmailInfo);
			adb.VerifyText(EmailInfo, prop.getProperty("EMAIL_INFO"));
			
			By EmailInputField = adb.EmailInputField;
			adb.verifyComponentExistence(EmailInputField);
			adb.checkPrePopulatedValueAndCompare(adb.emailInputField, prop.getProperty("WP_USERNAME"));
			
			By CONFERMAButton = adb.CONFERMAButton;
			adb.verifyComponentExistence(CONFERMAButton);
			adb.clickComponent(CONFERMAButton);
			Thread.sleep(7000);
			
			/*logger.write("Verify the color of Multifornitura - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num1, adb.servicesColor, "Multifornitura");
			logger.write("Verify the color of Multifornitura - Start");
			
			logger.write("Verify the color of I_tuoi_dati - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num2, adb.servicesColor, "I_tuoi_dati");
			logger.write("Verify the color of I_tuoi_dati - Start");
			
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num3, adb.servicesColor, "Riepilogo_e_conferma_dati");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");		
			
			logger.write("Verify the color of Esito - Start");
			adb.checkColor(adb.AttivazioneAddebitoDiretto_Num4, adb.servicesColor, "Esito");
			logger.write("Verify the color of Esito - Start");*/
			
			By RequestInfo = adb.RequestInfo;
			adb.verifyComponentExistence(RequestInfo);
			adb.VerifyText(RequestInfo, adb.requestInfo);
			
			By AttentionInfo = adb.AttentionInfo;
			adb.verifyComponentExistence(AttentionInfo);
			adb.VerifyText(AttentionInfo, adb.attentionInfo);
			
			By FINEButton = adb.FINEButton;
			adb.verifyComponentExistence(FINEButton);
			adb.clickComponent(FINEButton);
			Thread.sleep(7000);
			
			logger.write("Checking if path and heading of homePage is displayed - Start");
			adb.verifyComponentExistence(homePagePath);
			adb.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			adb.verifyComponentExistence(homepageSubheading);
			adb.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}
