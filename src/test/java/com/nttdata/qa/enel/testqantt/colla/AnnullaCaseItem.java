package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.components.colla.SearchSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AnnullaCaseItem {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
			
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		SeleniumUtilities util = new SeleniumUtilities(driver);		
		SpinnerManager spinner = new SpinnerManager(driver);
		
		LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
        page.navigate(Costanti.salesforceLink + "/" + prop.getProperty("QUERY_ID"));
        /*       
        SearchSalesforceComponent ssc = new SearchSalesforceComponent(driver);      
        ssc.openObjectInSalesforce(ssc.caseItem, prop.getProperty("QUERY_NAME"));
        */
        CaseItemDetailsComponent cidc = new CaseItemDetailsComponent(driver);       
        cidc.clickComponent(cidc.annulmentButton);
        
        Thread.sleep(2000);
        
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@name='ITA_IFM_Case_Items__c.Annulment']")));
        //cidc.switchFrame(cidc.iframeAnnullment);
        
        cidc.clickComponent(cidc.dropDown);
        Thread.sleep(500);
        cidc.clickComponent(cidc.dropDownOption);
        //cidc.cancellationReasonsPicklist = new Select (driver.findElement(cidc.dropDown));
        //cidc.cancellationReasonsPicklist.selectByVisibleText(cidc.annulatoFrontOffice);

        
        cidc.clickComponent(cidc.confermaAnnullament);
        driver.switchTo().defaultContent();
        spinner.waitForSpinnerBySldsHide(cidc.spinnerAfterAnnullament);
        
        
        

		prop.setProperty("RETURN_VALUE", "OK");

	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
