package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertaFreeComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_102_RES_FREE_SWA_ELE_OCR {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			//WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);			
			prop.setProperty("MODULORECLAMILINK", "https://www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			Thread.sleep(10000);
			logger.write("select drop down value - Start");
			pc.changeDropDownValue(pc.productDropDown, pc.luce);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, pc.cambio);
			logger.write("select drop down value - Completed");
			
			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			
			
			
			OffertaFreeComponent oc = new OffertaFreeComponent(driver);
			oc.verifyComponentExistence(oc.VERIFICAEADERISCI);
			oc.clickComponent(oc.VERIFICAEADERISCI);
			
					oc.verifyComponentExistence(oc.Inserisci);
			//oc.comprareText(oc.insercitext1 , oc.Insercitext1 , true);
			oc.enterInputParameters(oc.pod, "IT004E"+PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			oc.clickComponent(oc.CONFERMA);
			oc.verifyComponentExistence(oc.aderisci);
			oc.checkURLAfterRedirection(oc.offertaUrl);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.comprareText(oc.pageText, oc.PageText, true);
			oc.verifyComponentExistence(oc.inserisciITuoiDati);
			
			oc.verifyComponentExistence(oc.pagamentiEBollette);
			oc.verifyComponentExistence(oc.consensi);
			oc.verifyComponentExistence(oc.caricaBolletta);
			oc.verifyComponentExistence(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");
			
		
			oc.clickComponent(oc.compilaManualmente);
			
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Strat");
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(5000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(10000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");
			
			logger.write("Verify and Enter the values in informazioniFornitura section-- Start");
			
			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);
			//oc.checkPrePopulatedValueAndCompare(oc.pod, prop.getProperty("POD"));
			oc.verifyComponentExistence(oc.pod);
			
			oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
			oc.clickComponent(oc.capdropdown);
			oc.comprareText(oc.privacyText, oc.PrivacyText, true);
			oc.verifyComponentExistence(oc.prosegui1);
			
			oc.clickComponent(oc.prosegui1);
			
			oc.comprareText(oc.indrizzoDiFornituraText, oc.IndrizzoDiFornituraText, true);
			oc.verifyComponentExistence(oc.citta);
			oc.verifyComponentExistence(oc.indrizzo);
			oc.verifyComponentExistence(oc.numeroCivico);
			oc.verifyComponentExistence(oc.indrizzoDiFornituraCAP);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText, oc.IndrizzoDiFornituraPrivacyText, true);
			oc.verifyComponentExistence(oc.attuleFornitore);
			oc.verifyComponentExistence(oc.recapiti);
			oc.comprareText(oc.recapitiqst1, oc.Recapitiqst1, true);
			oc.verifyComponentExistence(oc.recapitians1);
			oc.verifyComponentExistence(oc.recapitians2);
			oc.comprareText(oc.recapitiqst2, oc.Recapitiqst2, true);
			oc.comprareText(oc.recapitiansOne, oc.RecapitiansOne, true);
			oc.comprareText(oc.recapitiansTwo, oc.RecapitiansTwo, true);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText2, oc.IndrizzoDiFornituraPrivacyText2, true);

			oc.enterInputParameters(oc.citta, prop.getProperty("CITTA"));
			oc.enterInputParameters(oc.indrizzo, prop.getProperty("INDRIZZO"));
			oc.enterInputParameters(oc.numeroCivico, prop.getProperty("NUMEROCIVICO"));
			oc.clickComponent(oc.indrizzo);
			Thread.sleep(3000);
			oc.verifyComponentExistence(oc.insertManually);
			oc.clickComponent(oc.insertManually);
			Thread.sleep(3000);
			oc.enterInputParameters(oc.indrizzo, prop.getProperty("INDRIZZO"));
			oc.enterInputParameters(oc.indrizzoDiFornituraCAP, prop.getProperty("CAP"));
			oc.clickComponent(oc.indrizzoDiFornituraCapDropdown);
			
			oc.clickComponent(oc.Attuale_Fornitore);

oc.verifyComponentExistence(oc.Sceglila);

oc.enterInputParameters(oc.scegilavalue, prop.getProperty("SCEGILAVALUE"));
oc.clickComponent(oc.scegilavalue);

			oc.clickComponent(oc.recapitians1);
			oc.clickComponent(oc.recapitiansOne);
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(5000);
			logger.write("Verify and Enter the values in informazioniFornitura section-- Completed");

			logger.write("Verify and Enter the values in pagamentiEBollette section-- Start");
			oc.verifyComponentExistence(oc.pagamentiEBollette);
			oc.verifyComponentExistence(oc.Metodo_di_Pagamento_Field);
			
		//	oc.verifyDefaultValue(oc.Metodo_di_Pagamento_FieldDefaultValue, oc.METADO_DROPDOWN_VALUE);
			oc.verifyComponentExistence(oc.Codice_IBAN_Field);			
			oc.verifyComponentExistence(oc.Codice_IBAN_Radio_yesAccountHolder);
			oc.verifyComponentExistence(oc.Codice_IBAN_Radio_noAccountHolder);
			
			oc.verifyComponentExistence(oc.Modalità_di_ricezione_Heading);
						
			oc.checkPrePopulatedValueAndCompare(oc.Modalità_di_ricezione_EmailInput, prop.getProperty("EMAIL"));
			oc.checkPrePopulatedValueAndCompare(oc.Modalità_di_ricezione_TelefonoInput, prop.getProperty("CELLULARE"));
			
			oc.verifyComponentExistence(oc.Codici_Promozionali_Heading);
			oc.verifyComponentExistence(oc.Codici_Promozionali_Subtext);
			oc.comprareText(oc.Codici_Promozionali_Subtext, oc.CodiciPromozionaliSubtext,true);
			oc.verifyComponentExistence(oc.Codici_Promozionali_DiscountCode);
			
			oc.selectMetodoPagamentoValue();
//			oc.verifyElementNotDisplaying(oc.Codice_IBAN_Field);
//			oc.verifyElementNotDisplaying(oc.Codice_IBAN_Radio_yesAccountHolder);
//			oc.verifyElementNotDisplaying(oc.Codice_IBAN_Radio_noAccountHolder);
			oc.comprareText(oc.bollettinoPostaleText, oc.BollettinoPostaleText, true);
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(5000);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Completed");

			oc.verifyComponentExistence(oc.Mandati_e_Consensi_Heading);
			oc.comprareText(oc.Mandati_e_Consensi_TextInBox, oc.Mandati_E_Consensi_TextInBox, true);
			oc.comprareText(oc.Mandati_e_Consensi_chkbx1_Text, oc.Mandati_e_Consensi_chkbx1Text, true);
			oc.comprareText(oc.Mandati_e_Consensi_chkbx2_Text, oc.Mandati_e_Consensi_chkbx2Text, true);
			oc.comprareText(oc.Mandati_e_Consensi_chkbx3_Text, oc.Mandati_e_Consensi_chkbx3Text, true);
			oc.verifyComponentExistence(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Heading);
			oc.verifyComponentExistence(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_LinkText);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_TextInBox, oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text, true);			
			oc.verifyComponentExistence(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_SI);
			oc.verifyComponentExistence(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_LinkText);
		//	oc.comprareText(oc.Richiesta_Di_Esecuzione_POPUP_content, oc.Richiesta_Di_Esecuzione_POPUP_Content, true);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_POPUP_close);
			
			oc.verifyComponentExistence(oc.Consenso_Marketing_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Marketing_Enel_Energia_TextInBox, oc.Consenso_Marketing_Enel_Energia_Text, true);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Enel_Energia_Accetto);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Enel_Energia_NonAccetto);
			
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_Heading);
			oc.comprareText(oc.Consenso_Marketing_Terzi_TextInBox, oc.Consenso_Marketing_Terzi_Text, true);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_Accetto);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_NonAccetto);
			
			oc.verifyComponentExistence(oc.Consenso_Profilazione_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Profilazione_Enel_Energia_TextInBox, oc.Consenso_Profilazione_Enel_Energia_Text, true);
			oc.verifyComponentExistence(oc.Consenso_Profilazione_Enel_Energia_Accetto);
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_NonAccetto);
			
			oc.clickComponent(oc.Mandati_e_Consensi_chkbx1_Text);
			oc.clickComponent(oc.Mandati_e_Consensi_chkbx2_Text);
			oc.clickComponent(oc.Mandati_e_Consensi_chkbx3_Text);
			oc.clickComponent(oc.Consenso_Marketing_Enel_Energia_Accetto);
			oc.clickComponent(oc.Consenso_Marketing_Terzi_Accetto);
			oc.clickComponent(oc.Consenso_Profilazione_Enel_Energia_Accetto);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupContent, oc.Richiesta_Di_Esecuzione_POPUP_Content, true);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupClose);
			oc.clickComponent(oc.COMPLETA_ADESIONE_Button);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Start");

			Thread.sleep(10000);
			oc.comprareText(oc.tornaAllaHomePageTitle, oc.TornaAllaHomePageTitle, true);
			oc.provideEmail(prop.getProperty("EMAIL"));

			oc.comprareText(oc.tornaAllaHomeMSG1, oc.TornaAllaHomeMSG1, true);
			oc.comprareText(oc.tornaAllaHomeMSG2, oc.TornaAllaHomeMSG2, true);
			oc.clickComponent(oc.TornaAllaHomeButton);
			Thread.sleep(5000);
			
			oc.checkURLAfterRedirection(prop.getProperty("WP_LINK"));
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
