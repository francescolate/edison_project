package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.gargoylesoftware.htmlunit.javascript.configuration.ClassConfiguration.ConstantInfo;
import com.nttdata.qa.enel.components.colla.GetUserDetailsFromWorkBenchComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class GetUserDetailsFromWorkBench {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			prop.setProperty("PROFILE_INFO_TEXT", "Titolare Mancuso Salvatore Codice FiscaleMNCSVT58H26H325L Telefono Fisso0818142364 Numero di Cellulare+39 3339090890 Email$email$ PECNon presente Canale di contatto preferito Telefono Cellulare");
			
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			GetUserDetailsFromWorkBenchComponent gdfc = new GetUserDetailsFromWorkBenchComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.Query_338_d);
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			Thread.sleep(5000);
			String address=driver.findElement(gdfc.addressField).getText();
			prop.setProperty("ADDRESS", address);
			System.out.println("Add "+ prop.getProperty("ADDRESS"));
			String cf = driver.findElement(gdfc.cf).getText();
			prop.setProperty("CF", cf);
			System.out.println("cf "+ prop.getProperty("CF"));
			
			wbc.insertQuery(Costanti.Query_338_e);
			wbc.pressButton(wbc.submitQuery);
			Thread.sleep(5000);
			String First_Name = driver.findElement(gdfc.firstName).getText();
			prop.setProperty("FIRST_NAME", First_Name);
			System.out.println("first name "+prop.getProperty("FIRST_NAME"));
			String Last_Name = driver.findElement(gdfc.lastName).getText();
			prop.setProperty("LAST_NAME", Last_Name);
			System.out.println("last name "+prop.getProperty("LAST_NAME"));

			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
