package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetQueryModificaStatoResidente14Query2 {
    public static void main(String[] args) throws Exception {
        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {
            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
                String msr_id2_ =
                        "SELECT Account.RecordType.Name, Account.NE__Fiscal_code__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n" +
                                "FROM Asset\r\n" +
                                "WHERE NE__Status__c = 'Active'\r\n" +
                                "AND ITA_IFM_Commodity__c = 'ELE'\r\n" +
                                "AND RecordType.Name = 'Commodity'\r\n" +
                                "AND Account.NE__Fiscal_code__c = '"+prop.getProperty("CF")+"' ";
                prop.setProperty("QUERY", msr_id2_);
            }

            prop.setProperty("RETURN_VALUE", "OK");

        }
        catch (Exception e)
        {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: "+errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
            if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
