package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SezioneMercatoSubentroLiberoAggiungiFornituraOffertabilitàKo {
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {
            
            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            PrecheckComponent forniture = new PrecheckComponent(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            InserimentoFornitureSubentroComponent subentro = new InserimentoFornitureSubentroComponent(driver);
            
            int numberOfIteration = 3, iterator;

            logger.write("Verifica checklist e conferma - Start");
            checkListModalComponent.clickConferma();
            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");
            
            System.out.println("set NUMERO_RICHIESTA");
            
            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");
            merc.verifyComponentExistence(merc.tabSubentro);
            merc.verifyComponentExistence(merc.labelSubentro);
            merc.verifyComponentExistence(insert.sezioneInserimentoDati);
            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");

            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
            merc.verifyComponentExistence(merc.checkContatto);
            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");

            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
            merc.verifyComponentExistence(merc.checkCf);
            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");

            System.out.println("verifica esistenza componenti");
            
            merc.clickComponentIfExist(merc.inputSezioneMercato);
            merc.clickComponentIfExist(merc.buttonConfermaSezioneMercato);

            
            
            merc.clearAndSendKeys(merc.inputPodPdr, prop.getProperty("POD"));

            merc.clearAndSendKeys(merc.inputCap, prop.getProperty("CAP"));

            System.out.println("inserimento POD/CAP");
            
            merc.clickComponentIfExist(merc.buttonEseguiPrecheck);

            System.out.println("esegui precheck");
            
            merc.checkSpinnersSFDC();

            if (!merc.verifyExistenceComponent(merc.popupCodiceIstat))
                logger.write("modale con la selezion del codice Istat non presente , probabile causa servizio precheck");
            
            System.out.println("entra nell'if verifica popup codice istat");

            if (prop.getProperty("TIPO_FORNITURA").equals("ELETTRICO")) {
            	if (insert.Existence(insert.campoTipoMisuratore))
            	//insert.verifyComponentExistence(insert.campoTipoMisuratore);
                insert.selezionaTipoMisuratore(prop.getProperty("TIPO_MISURATORE"));
                	
               System.out.println("misuratore inserito");
                //non prende tipo misuratore
               
                insert.verifyComponentExistence(insert.campoTensioneConsegna);  
                System.out.println("tensione consegna esiste");
                insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
                System.out.println("tensione consegna settata");
                insert.verifyComponentExistence(insert.campoTensioneConsegna);
                insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
              
                insert.verifyComponentExistence(insert.campoTensioneConsegna);
                insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
                
                System.out.println("if set properties");
            }

            if (merc.getElementTextString(merc.offertabilità).equals("OK")) {
                logger.write("Offertabilità OK");
            System.out.println("offertabilità OK");
            } else {
                throw new Exception("Offertabilità KO");
            }
            /*
            for (iterator = 0; iterator <= numberOfIteration; iterator++) {
               //todo Parte da sviluppare
            }
            */
            prop.setProperty("RETURN_VALUE", "OK");
            
           /* --- */
            if(subentro.verifyClickability(subentro.aggiungiFornitura)) { 
            	subentro.clickComponent(subentro.aggiungiFornitura);             	
            } 
            
            logger.write("Fornitura Aggiunta");
            if(subentro.verifyClickability(subentro.buttonConfermaFooter)) {
            	subentro.clickComponent(subentro.buttonConfermaFooter);
            }
            
            merc.checkSpinnersSFDC();
            
            if(subentro.verifyClickability(subentro.buttonCreaOfferta)) {
            	subentro.clickComponent(subentro.buttonCreaOfferta);
            }
            
            logger.write("Offerta Creata");
            
            merc.checkSpinnersSFDC();
            /* --- */
            
            
            
        } catch (
                Exception e) {
        	System.out.println("entra nel catch");
        	
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace();
           // e.printStackTrace(new PrintWriter(errors));
            //errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }

}
