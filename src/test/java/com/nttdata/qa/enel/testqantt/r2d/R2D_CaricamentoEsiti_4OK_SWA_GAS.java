package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_4OK_SWA_GAS {

	@Step("R2D Caricamento esiti 5OK per SWA - GAS")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				String statopratica=prop.getProperty("STATO_R2D");
				//l'esito RD2 1OK è opzionale e viene dato solo quando lo stato della pratica è AW

				if (statopratica.compareToIgnoreCase("CI")==0)  {
						
					R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
					//Selezione Menu Caricamento Esiti
					logger.write("Code di comunicazione/Caricamento Esiti - Start");
					 menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
					logger.write("Code di comunicazione/Caricamento Esiti - Completed");
					R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
					//Selezione tipologia Caricamento
					logger.write("Selezione tipologia Caricamento - Start");
					 caricamentoEsiti.selezionaTipoCaricamento("Puntuale SWA");
					logger.write("Selezione tipologia Caricamento - Completed");
					//Indice POD
					int indice=Integer.parseInt(prop.getProperty("INDICE_POD","1"));
					//Inserisci Pod
					//Inserimento PDR oppure Id Richiesta
//					if(prop.getProperty("PDR").isEmpty()){
						logger.write("inserisci Id_Richiesta - Start");
//						caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
						if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("ID_RICHIESTA"));
						} else {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("ID_ORDINE"));
						}
						logger.write("inserisci Id_Richiesta - Completed");
//					}else {
//						logger.write("inserisci Pdr - Start");
//						caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPDR, prop.getProperty("PDR"));
//						logger.write("inserisci Pdr - Completed");
//					}
//					logger.write("Caricamento valore PDR - Start");
//					 caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPDR, prop.getProperty("PDR"));
//					logger.write("Caricamento valore PDR - Completed");
					//Inserisci ID Richiesta CRM
	//				caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA_CRM_GAS_"+indice,prop.getProperty("ID_RICHIESTA_CRM_GAS")));
					//Click Cerca
					logger.write("Selezione pulsante Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione pulsante Cerca - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Click button Azione
					logger.write("Selezione Bottone Azione Pratica - Start");
					 caricamentoEsiti.selezionaTastoAzionePratica();
					logger.write("Selezione Bottone Azione Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Verifica Stato pratica atteso
					logger.write("Verifico Stato Pratica - Start");
					 caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "CI");
					logger.write("Verifico Stato Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Selezione esito FA
					logger.write("Caricamento Evento FA - Start");
					 caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_FA_GAS"));
					logger.write("Caricamento Evento - Completed");
					//Inserimento esito FA
					logger.write("Caricamento Esito FA - Start");
					 caricamentoEsiti.selezioneEsito("FA");
					logger.write("Caricamento Esito FA - Completed");
					logger.write("Esitazione Anagrafica SWA - FA - Start");
					prop.setProperty("TIPO_PDR", "0");

					caricamentoEsiti.inserimentoDettaglioEsitoFA_SWA_GAS(prop.getProperty("MATRICOLA_CONTATORE"),
							prop.getProperty("CLASSE_CONTATORE"),
							prop.getProperty("TIPO_PDR"),
							prop.getProperty("PROFILO_PRELIEVO_ANNUO"), 
							prop.getProperty("PRELIEVO_ANNUO"),
							prop.getProperty("CODICE_POOL"),
							prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
					logger.write("Esitazione Anagrafica SWA - FA - Completed");
					//Salvataggio stato attuale pratica
					prop.setProperty("STATO_R2D", "FA");
					
				}

				//Stato attuale pratica
//				System.out.println("Caricamento 4OK Stato: "+ prop.getProperty("STATO_R2D"));
				prop.setProperty("RETURN_VALUE", "OK");

			}
			//
			prop.setProperty("STATUS", "OK");
		} 		
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
