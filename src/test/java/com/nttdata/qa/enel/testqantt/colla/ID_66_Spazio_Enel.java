package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SpazioEnelMyComponent;
import com.nttdata.qa.enel.components.colla.SupportoCambioPotenzaContatore;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_66_Spazio_Enel {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			SpazioEnelMyComponent spaenel = new SpazioEnelMyComponent(driver);
			SupportoCambioPotenzaContatore sup = new SupportoCambioPotenzaContatore(driver);
//			String supportoURL = prop.getProperty("WP_LINK")+"it/supporto";
//			String contattaciUrl = prop.getProperty("WP_LINK")+"it/supporto/faq/contattaci";
//			String napoliUrl = prop.getProperty("WP_LINK")+"spazio-enel/napoli/";
//			String napoliNewUrl = prop.getProperty("WP_LINK")+"spazio-enel/napoli/a-g-energie-s-r-l-s/10236/";
//			String spazioEnelUrl = prop.getProperty("WP_LINK")+"spazio-enel/";
//			String lodiUrl = prop.getProperty("WP_LINK")+"spazio-enel/lodi/";
//			String messinaUrl = prop.getProperty("WP_LINK")+"spazio-enel/messina/";
			
			String supportoURL = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto";
			String contattaciUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto/faq/contattaci";
			String napoliUrl = "https://www-coll1.enel.it/spazio-enel/napoli/";
			String napoliNewUrl = "https://www-coll1.enel.it/spazio-enel/napoli/al-ba-srl/10600/";
//			String napoliNewUrl = "https://www-coll1.enel.it/spazio-enel/napoli/al-ba-srl/10600/";
			String spazioEnelUrl = "https://www-coll1.enel.it/spazio-enel/";
			String lodiUrl = "https://www-coll1.enel.it/spazio-enel/lodi/";
			String messinaUrl = "https://www-coll1.enel.it/spazio-enel/messina/";
			
			//1
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
 			logger.write("apertura del portale web Enel di test - Completed");
			
			//2
			logger.write("Clicking on supporto Header - Started");
			Thread.sleep(30000);
			sup.clickComponent(sup.supportoHeader);
			
			sup.checkURLAfterRedirection(supportoURL);
			sup.comprareText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.comprareText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
			sup.comprareText(sup.supportoHeading, sup.SUPPORTO_HEADING, true);
			sup.comprareText(sup.supportoTitleText, sup.SUPPORTO_TITLE_TEXT, true);
			logger.write("Clicking on supporto Header - Completed");
			
			//3
			logger.write("Clicking on Contattaci Menu - Started");
			Thread.sleep(10000);
			sup.clickComponent(sup.contrattiemodulistica);
			sup.clickComponent(sup.contattaci);
			Thread.sleep(20000);
			
			spaenel.checkURLAfterRedirection(contattaciUrl);
			spaenel.comprareText(spaenel.supportoContattaciPath, SpazioEnelMyComponent.CONTATTACI_PATH, true);
			sup.comprareText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.comprareText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
//			sup.comprareText(sup.supportoHomePagePath4, SupportoCambioPotenzaContatore.SUPPORTO_HOMEPAGE_PATH4, true);
			spaenel.comprareText(spaenel.contattaciTitle, SpazioEnelMyComponent.CONTATTACI_TITLE, true);
			spaenel.comprareText(spaenel.contattaciText, SpazioEnelMyComponent.CONTATTACI_TEXT, true);
			logger.write("Clicking on Contattaci Menu - Started");
			
			//4
			spaenel.comprareText(spaenel.recandotiPhrase, SpazioEnelMyComponent.RECANDOTI_TEXT, true);
			spaenel.clickComponent(spaenel.recandotiPhraseCliccaQui);
			
			spaenel.comprareText(spaenel.spazioEnelPath, SpazioEnelMyComponent.SPAZIO_ENEL_PATH, true);
			sup.comprareText(sup.supportoHome1PagePath, sup.SUPPORTO_HOMEPAGE_PATH1, true);
//			sup.comprareText(sup.supportoHomePagePath5, SupportoCambioPotenzaContatore.SUPPORTO_HOMEPAGE_PATH5, true);
			spaenel.comprareText(spaenel.spazioEnelText, SpazioEnelMyComponent.SPAZIO_ENEL_TEXT, true);
			
			//5
			logger.write("Verification of Header - Start ");
			spaenel.verifyComponentExistence(spaenel.luceEGasHeader);
			spaenel.verifyComponentExistence(spaenel.impreseHeader);
			spaenel.verifyComponentExistence(spaenel.insiemeATeHeader);
			spaenel.verifyComponentExistence(spaenel.storieHeader);
			spaenel.verifyComponentExistence(spaenel.futurEHeader);
			spaenel.verifyComponentExistence(spaenel.mediaHeader);
			spaenel.verifyComponentExistence(spaenel.supportoHeader);
			logger.write("Verification of Header - End ");
			
			//6
			logger.write("Verification of Footer - Start ");
			logger.write("Verifica Footer - Start");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			prop.setProperty("BEFORE_FOOTER_TEXT", "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.");
			prop.setProperty("AFTER_FOOTER_TEXT", "Gruppo IVA Enel P.IVA 15844561009");
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By linkInformazioniLegali=footer.informazioniLegaliLink;
			footer.verifyComponentExistence(linkInformazioniLegali);
			footer.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=footer.creditsLink;
			footer.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=footer.privacyLink;
			footer.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=footer.cookieLink;
			footer.verifyComponentExistence(linkCookie);
			
			By linkRemit=footer.remitLink;
			footer.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=footer.twitterIcon;
			footer.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=footer.facebookIcon;
			footer.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=footer.youtubeIcon;
			footer.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=footer.linkedinIcon;
			footer.verifyComponentExistence(iconLinkedin);
			
			footer.verifyComponentExistence(footer.instagramIcon);
			logger.write("Verifica Footer - Completed");
			logger.write("Verification of Footer - End ");
			
			//7
			logger.write("Verification of all the city details - Started");
			spaenel.checkForCityValue(spaenel.abruzzoList, SpazioEnelMyComponent.FIELD, SpazioEnelMyComponent.VALUES);
			logger.write("Verification of all the city details - Completed");
			
			//8
			logger.write("Clicking on Napoli and verification of page details - Started");
			spaenel.clickComponent(spaenel.napoli);
			
			spaenel.checkURLAfterRedirection(napoliUrl);
			spaenel.comprareText(spaenel.napoliPath, SpazioEnelMyComponent.NAPOLI_PATH, true);
			spaenel.comprareText(spaenel.napoliText, SpazioEnelMyComponent.NAPOLI_TEXT, true);
			
			//9
			spaenel.verifyComponentExistence(spaenel.firstElementSpazioNapoli);
			//spaenel.waitForElementToDisplayAndClick(spaenel.vaiAlloSpazioEnelButton);
			spaenel.clickComponent(spaenel.vaiAlloSpazioEnelButton);
						
			spaenel.checkURLAfterRedirection(napoliNewUrl);
			spaenel.comprareText(spaenel.spazioEnelNapoliPath, SpazioEnelMyComponent.SPAZIOENEL_NAPOLI_PATH, true);
			spaenel.comprareText(spaenel.spazioEnelnapoliText, SpazioEnelMyComponent.SPAZIOENEL_NAPOLI_TEXT, true);
			logger.write("Clicking on Napoli and verification of page details - Started");
			
			//10
			spaenel.navigateBack();
			spaenel.navigateBack();
			
			spaenel.checkURLAfterRedirection(spazioEnelUrl);
			spaenel.comprareText(spaenel.spazioEnelPath, SpazioEnelMyComponent.SPAZIO_ENEL_PATH, true);
			spaenel.comprareText(spaenel.spazioEnelText, SpazioEnelMyComponent.SPAZIO_ENEL_TEXT, true);
			
			//11	
			logger.write("Clicking on Lodi and verification of page details - Started");
			spaenel.clickComponent(spaenel.lodi);
			
			spaenel.checkURLAfterRedirection(lodiUrl);
			spaenel.comprareText(spaenel.lodiPath, SpazioEnelMyComponent.LODI_PATH, true);
			spaenel.comprareText(spaenel.lodiText, SpazioEnelMyComponent.LODI_TEXT, true);
			
			
			//12
			spaenel.navigateBack();
			
			spaenel.checkURLAfterRedirection(spazioEnelUrl);
			spaenel.comprareText(spaenel.spazioEnelPath, SpazioEnelMyComponent.SPAZIO_ENEL_PATH, true);
			spaenel.comprareText(spaenel.spazioEnelText, SpazioEnelMyComponent.SPAZIO_ENEL_TEXT, true);
			logger.write("Clicking on Lodi and verification of page details - Completed");
			
			//13
			logger.write("Clicking on Messina and verification of page details - Started");
			spaenel.clickComponent(spaenel.messina);
			
			spaenel.checkURLAfterRedirection(messinaUrl);
			spaenel.comprareText(spaenel.messinaPath, SpazioEnelMyComponent.MESSINA_PATH, true);
			spaenel.comprareText(spaenel.messinaText, SpazioEnelMyComponent.MESSINA_TEXT, true);
			logger.write("Clicking on Messina and verification of page details - Completed");
			
			//14
			
			logger.write("Verification of Header - Start ");
			spaenel.verifyComponentExistence(spaenel.luceEGasHeader);
			spaenel.verifyComponentExistence(spaenel.impreseHeader);
			spaenel.verifyComponentExistence(spaenel.insiemeATeHeader);
			spaenel.verifyComponentExistence(spaenel.storieHeader);
			spaenel.verifyComponentExistence(spaenel.futurEHeader);
			spaenel.verifyComponentExistence(spaenel.mediaHeader);
			spaenel.verifyComponentExistence(spaenel.supportoHeader);
			logger.write("Verification of Header - End ");
			
			//15
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			footer.verifyComponentExistence(linkInformazioniLegali);
			footer.scrollComponent(linkInformazioniLegali);
			footer.verifyComponentExistence(linkCredits);
			footer.verifyComponentExistence(linkPrivacy);
			footer.verifyComponentExistence(linkCookie);
			footer.verifyComponentExistence(linkRemit);
			footer.verifyComponentExistence(iconTwitter);
			footer.verifyComponentExistence(iconFacebook);
			footer.verifyComponentExistence(iconYoutube);
			footer.verifyComponentExistence(iconLinkedin);
			footer.verifyComponentExistence(footer.instagramIcon);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
