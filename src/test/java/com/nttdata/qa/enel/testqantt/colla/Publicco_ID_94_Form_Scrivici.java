package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.CaricaDoccumentiComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Publicco_ID_94_Form_Scrivici {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		prop.setProperty("NUMERO_CLIENTI", "310521194");
		prop.setProperty("NOME", "ALBUS");
		prop.setProperty("COGNOME", "SILENTE");
		prop.setProperty("INVALID_CF", "12 @ £ $");
		prop.setProperty("CF", "SLNLBS68M28H501Y");
		prop.setProperty("INVALID_CELLULARE", "123@%$");
		prop.setProperty("INVALID_CELLULARE1", "320898998765");
		prop.setProperty("CELLULARE", "3208989987");
		prop.setProperty("INVALID_CELLULARE1", "320898998765");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("INVALID_EMAIL", "@3557£$%");
		prop.setProperty("INCORRECT_CONFERMAEMAIL", "raffae.ll.aquomo@gmail.com");
		prop.setProperty("CONFERMAEMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("MESSAGGIO", "PROVA TEST AUTOMATION ID 94");

	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		CaricaDoccumentiComponent cd = new CaricaDoccumentiComponent(driver);
		
		logger.write("Verify Carcia Doccumentic page input field -- Start");
		cd.checkURLAfterRedirection(cd.caricaDoccumentiUrl);
		cd.verifyComponentExistence(cd.nome);
		cd.verifyPlaceholderText(cd.nome, cd.NomePlaceholder);
		cd.verifyComponentExistence(cd.cogNome);
		cd.verifyPlaceholderText(cd.cogNome, cd.CognomePlaceholder);
		cd.verifyComponentExistence(cd.cf);
		cd.verifyPlaceholderText(cd.cf, cd.CFPlaceholder);
		cd.verifyComponentExistence(cd.cellulare);
		cd.verifyPlaceholderText(cd.cellulare, cd.CellularePlaceholder);
		cd.verifyComponentExistence(cd.email);
		cd.verifyPlaceholderText(cd.email, cd.EmailPlaceholder);
		cd.verifyComponentExistence(cd.confermaEmail);
		cd.verifyPlaceholderText(cd.confermaEmail, cd.ConfermaEmailPlaceholder);
		cd.verifyComponentExistence(cd.numeroClienti);
		cd.verifyPlaceholderText(cd.numeroClienti, cd.NumeroClientiPlaceholder);
		cd.verifyComponentExistence(cd.seiGiaCliente);
		cd.verifyComponentExistence(cd.radioButtonSi);
		cd.verifyComponentExistence(cd.radioButtonNo);
		cd.verifyComponentExistence(cd.messagio);
		cd.verifyComponentExistence(cd.privacyLabel);
		cd.comprareText(cd.informativaPrivacySection, cd.InformativaPrivacySection, true);
		cd.verifyComponentExistence(cd.accetto);
		cd.verifyComponentExistence(cd.nonAccetto);
		cd.verifyComponentExistence(cd.guideDocument);
		cd.verifyComponentExistence(cd.prosegui);
		logger.write("Verify Carcia Doccumentic page input field -- Completed");

		logger.write("Verify header section - Start");
		Thread.sleep(5000);
		ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("LUCE_E_GAS"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("IMPRESA"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("STORIE"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("FUTUR-E"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("MEDIA"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("SUPPORTO"));
		logger.write("Verify header section - Completed");

		logger.write("Verify footer section - Start");
		mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
		mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);
		Thread.sleep(5000);
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
		logger.write("Verify footer section - Completed");		
		
		logger.write("Click on Prosegui and verify the pop up -- Start");
		cd.clickComponent(cd.prosegui);
		cd.verifyComponentExistence(cd.popUpBody);
		cd.comprareText(cd.popUpBody, cd.PopUpBody, true);
		cd.verifyComponentExistence(cd.popUpCross);
		cd.verifyComponentExistence(cd.popUpOK);
		cd.clickComponent(cd.popUpOK);
		logger.write("Click on Prosegui and verify the pop up -- Completed");

		logger.write("Verify field error message -- Start");
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.clickComponent(cd.prosegui);
		cd.comprareText(cd.privacyError, cd.PrivacyError, true);
		
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.clickComponent(cd.accetto);
		cd.clickComponent(cd.prosegui);
		cd.comprareText(cd.nomeError, cd.FieldError, true);
		cd.comprareText(cd.cogNomeError, cd.FieldError, true);
		cd.comprareText(cd.emailError, cd.FieldError, true);
		cd.comprareText(cd.messagioError, cd.FieldError, true);
		cd.comprareText(cd.cfError, cd.CFError, true);
		cd.comprareText(cd.cellulareError, cd.CellulareError, true);
		cd.comprareText(cd.confermaEmailError, cd.ConfermaEmailError, true);
		cd.comprareText(cd.nomeError, cd.FieldError, true);
		
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
		cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
		cd.enterInputParameters(cd.cf, prop.getProperty("INVALID_CF"));
		Thread.sleep(3000);
		cd.clickComponent(cd.accetto);
		cd.clickComponent(cd.prosegui);
		Thread.sleep(5000);
		cd.comprareText(cd.cfError, cd.CFError1, true);
		
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
		cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
		cd.enterInputParameters(cd.cf, prop.getProperty("CF"));
		cd.enterInputParameters(cd.cellulare, prop.getProperty("INVALID_CELLULARE"));
		cd.clickComponent(cd.accetto);
		cd.clickComponent(cd.prosegui);
		cd.comprareText(cd.cellulareError, cd.CellulareError1, true);
		
		Thread.sleep(5000);
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
		cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
		cd.enterInputParameters(cd.cf, prop.getProperty("CF"));
		cd.enterInputParameters(cd.cellulare, prop.getProperty("INVALID_CELLULARE1"));
		cd.comprareText(cd.cellulareError, cd.CellulareError1, true);

		Thread.sleep(5000);
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
		cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
		cd.enterInputParameters(cd.cf, prop.getProperty("CF"));
		cd.enterInputParameters(cd.cellulare, prop.getProperty("CELLULARE"));
		cd.enterInputParameters(cd.email, prop.getProperty("INVALID_EMAIL"));
		Thread.sleep(5000);
		cd.clickComponent(cd.accetto);
		cd.clickComponent(cd.prosegui);
		
		Thread.sleep(5000);
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
		cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
		cd.enterInputParameters(cd.cf, prop.getProperty("CF"));
		cd.enterInputParameters(cd.cellulare, prop.getProperty("CELLULARE"));
		cd.enterInputParameters(cd.email, prop.getProperty("EMAIL"));
		cd.enterInputParameters(cd.confermaEmail, prop.getProperty("INCORRECT_CONFERMAEMAIL"));
		Thread.sleep(3000);
		cd.clickComponent(cd.accetto);
		cd.clickComponent(cd.prosegui);
		logger.write("Verify field error message -- Completed");

		Thread.sleep(5000);
		logger.write("Enter valid input and verify page navigation -- Start");
		cd.enterInputParameters(cd.numeroClienti, prop.getProperty("NUMERO_CLIENTI"));
		cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
		cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
		cd.enterInputParameters(cd.cf, prop.getProperty("CF"));
		cd.enterInputParameters(cd.cellulare, prop.getProperty("CELLULARE"));
		cd.enterInputParameters(cd.email, prop.getProperty("EMAIL"));
		cd.enterInputParameters(cd.confermaEmail, prop.getProperty("EMAIL"));
		cd.enterInputParameters(cd.messagio, prop.getProperty("MESSAGGIO"));
		cd.clickComponent(cd.accetto);
		cd.clickComponent(cd.prosegui);
		
		Thread.sleep(30000);
		cd.verifyComponentExistence(cd.successMsgTitle);
		cd.comprareText(cd.successMsg, cd.SuccessMsg, true);
		cd.verifyComponentExistence(cd.indietro);
		logger.write("Enter valid input and verify page navigation -- Completed");
		
		logger.write("Verify header section - Start");
		Thread.sleep(5000);
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("LUCE_E_GAS"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("IMPRESA"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("STORIE"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("FUTUR-E"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("MEDIA"));
		mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("SUPPORTO"));
		logger.write("Verify header section - Completed");

		mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
		mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);
		Thread.sleep(5000);
		logger.write("Verify footer section - Start");
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
		mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
		logger.write("Verify footer section - Completed");		
		
		logger.write("Verify Carcia Doccumentic page input field -- Start");
		cd.clickComponent(cd.indietro);
		Thread.sleep(5000);
		cd.checkURLAfterRedirection(cd.caricaDoccumentiUrlNew);
		cd.verifyComponentExistence(cd.nome);
		cd.verifyPlaceholderText(cd.nome, cd.NomePlaceholder);
		cd.verifyComponentExistence(cd.cogNome);
		cd.verifyPlaceholderText(cd.cogNome, cd.CognomePlaceholder);
		cd.verifyComponentExistence(cd.cf);
		cd.verifyPlaceholderText(cd.cf, cd.CFPlaceholder);
		cd.verifyComponentExistence(cd.cellulare);
		cd.verifyPlaceholderText(cd.cellulare, cd.CellularePlaceholder);
		cd.verifyComponentExistence(cd.email);
		cd.verifyPlaceholderText(cd.email, cd.EmailPlaceholder);
		cd.verifyComponentExistence(cd.confermaEmail);
		cd.verifyPlaceholderText(cd.confermaEmail, cd.ConfermaEmailPlaceholder);
		cd.verifyComponentExistence(cd.numeroClienti);
		cd.verifyPlaceholderText(cd.numeroClienti, cd.NumeroClientiPlaceholder);
		cd.verifyComponentExistence(cd.seiGiaCliente);
		cd.verifyComponentExistence(cd.radioButtonSi);
		cd.verifyComponentExistence(cd.radioButtonNo);
		cd.verifyComponentExistence(cd.messagio);
		cd.verifyComponentExistence(cd.privacyLabel);
		cd.comprareText(cd.informativaPrivacySection, cd.InformativaPrivacySection, true);
		cd.verifyComponentExistence(cd.accetto);
		cd.verifyComponentExistence(cd.nonAccetto);
		cd.verifyComponentExistence(cd.guideDocument);
		cd.verifyComponentExistence(cd.prosegui);
		logger.write("Verify Carcia Doccumentic page input field -- Completed");


		prop.setProperty("RETURN_VALUE", "OK");
		
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

	} finally {
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
