package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SurveyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_Survey_NPS_64 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			SurveyComponent sc = new SurveyComponent(driver);
			
			logger.write("Accessing Survey page - Start");
			sc.launchLink(prop.getProperty("LINK"));
			logger.write("Accessing Survey page - Complete");
			
			ImpressComponent ic = new ImpressComponent(driver);
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
		//	ic.hanldeFullscreenMessage(ic.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			logger.write("Verify the logo and click Accetta button - Start");
			ic.verifyComponentExistence(ic.logoEnel);
			/*
			if (driver.findElement(ic.buttonAcceptCookie).isDisplayed()) {
				
				ic.clickComponent(ic.buttonAcceptCookie);
			}
			Thread.sleep(10000);
			*/
			ic.verifyComponentExistence(ic.buttonAccetta);
			ic.clickComponent(ic.buttonAccetta);
			logger.write("Verify the logo and click Accetta button - Complete");
			
			logger.write("Verify the title - Start");
			sc.verifyComponentExistence(sc.surveyTitle1);
			sc.verifyComponentExistence(sc.surveyTitle2);
			sc.verifyContent(sc.surveyTitle1, SurveyComponent.SURVEY_TITLE1);
			sc.verifyContent(sc.surveyTitle2, SurveyComponent.SURVEY_TITLE2);
			logger.write("Verify the title - Complete");
			
			logger.write("Click on invia button - Start");
			sc.verifyComponentExistence(sc.inviaBtn);
			sc.clickComponent(sc.inviaBtn);
			logger.write("Click on invia button - Complete");
			
			logger.write("Verify the error message - Start");
			sc.verifyContent(sc.errorMsgReq, SurveyComponent.ERRORMSG_REQ);
			logger.write("Verify the error message - Start");
			
			logger.write("Verify the star icons - Start");
			sc.verifyIconDisplay(sc.surveryStars);
			logger.write("Verify the star icons - Complete");
			
			logger.write("Click on all the stars - Start");
			sc.clickIcon();
			logger.write("Click on all the stars - Complete");
			
			Thread.sleep(5000);
			logger.write("Verify the header- Start");
			//ic.verifyHeaderVisibility();
			logger.write("Verify the header- Complete");
			
			logger.write("Verify the footer- Start");
			ic.verifyFooterVisibility();
			logger.write("Verify the footer- Start");
			
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			
			logger.write("Verify and click on the icon- Start");
			log.verifyComponentExistence(log.iconUser);
			log.clickComponent(log.iconUser); 
			logger.write("Verify and click on the icon- Complete");
			
			logger.write("check the username and passsword login - Start");
			log.verifyComponentExistence(log.username);
			log.enterLoginParameters(log.username, prop.getProperty("USERNAME"));
					
			log.verifyComponentExistence(log.password);
			log.enterLoginParameters(log.password, prop.getProperty("PASSWORD"));
			logger.write("check the username and passsword login - Complete");
			
			logger.write("Click on the login button- Start");
			log.verifyComponentExistence(log.buttonLoginAccedi);
			log.clickComponent(log.buttonLoginAccedi); 
			logger.write("Click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			log.verifyComponentExistence(log.loginSuccessful); 
			logger.write("Verify the page after successful login - Complete");
			
			logger.write("Replace the survey url - Start");
			driver.navigate().to(prop.getProperty("LINK_SURVEY"));
			logger.write("Replace the survey url - Complete");
			
			logger.write("Invia button to display - Start");
			sc.waitForElementToDisplay(sc.inviaBtn);
			logger.write("Invia button to display - Complete");
			
			logger.write("Select all the stars - Start");
			sc.clickComponent(sc.surveySelectAll);
			logger.write("Select all the stars - Complete");
			
			logger.write("Click on invia button - Start");
			sc.clickComponent(sc.inviaBtn);
			logger.write("Click on invia button - Complete");
			
			logger.write("Click on torna button - Start");
			sc.verifyComponentExistence(sc.tornaBtn);
			sc.clickComponent(sc.tornaBtn);
			logger.write("Click on torna button - Complete");
			
			logger.write("Verify the contents - Start");
			sc.verifyContent(sc.headerprivato1, SurveyComponent.Header1_Privato);
			sc.verifyContent(sc.headerprivato2, SurveyComponent.Header2_Privato);
			sc.verifyContent(sc.titleprivato1, SurveyComponent.Title1_Privato);
			sc.verifyContent(sc.titleprivato2, SurveyComponent.Title2_Privato);
			logger.write("Verify the contents - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
						
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
