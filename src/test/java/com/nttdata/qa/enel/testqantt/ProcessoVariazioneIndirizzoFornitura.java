package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoVariazioneIndirizzoFornitura {
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
				SceltaProcessoComponent variazioneIn = new SceltaProcessoComponent(driver);
				variazioneIn.clickAllProcess();
				variazioneIn.chooseProcessToStart(variazioneIn.avvioVariazioneIndirizzoFornitura);	
				try {
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					IdentificazioneInterlocutoreComponent identificaInterlocutore = new IdentificazioneInterlocutoreComponent(driver);
					identificaInterlocutore.insertDocumentoNuovoInterlocutore(prop.getProperty("NUMERO_DOCUMENTO","1234"));
					identificaInterlocutore.pressButton(identificaInterlocutore.confermaDocumento);
					identificaInterlocutore.pressButton(identificaInterlocutore.conferma); 
					Thread.currentThread().sleep(10000);
		
				}
				catch (TimeoutException e){
					logger.write("La pagina di identificazione cliente non è presente, messaggio errore :" +e.getMessage());

				}finally {
					driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
				}
	

			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}
