package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Info_Enel_Energia_ACR_280 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);

			//Step 3
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");

			//Step 4
			HomeComponent hc = new HomeComponent(driver);
			logger.write("Correct visualization of the Home Page supply card sections -- Starts");	
			hc.verifyComponentExistence(hc.nomeFornituraGas);
			hc.verifyComponentExistence(hc.addressGasSupply);
			hc.verifyComponentExistence(hc.activateSupplyText1);
			hc.verifyComponentExistence(hc.activeSupplyText2);
			//hc.verifyComponentExistence(hc.seeOffers);
			logger.write("Correct visualization of the Home Page supply card sections -- Ends");

			//Step 5
			logger.write("Click on services and verify the Supply service section -- Starts");
			hc.clickComponent(hc.services);
			ServicesComponent sc = new ServicesComponent(driver);
			sc.verifyComponentExistence(sc.serviceTitle);
			Thread.sleep(20000);
			logger.write("Click on services and verify the Supply service section -- Ends");

			logger.write("Click on Info EnelEnergia on services section -- Starts");
			sc.verifyComponentExistence(sc.infoEnelEnergia);
			sc.clickComponent(sc.infoEnelEnergia);
			Thread.sleep(20000);
			logger.write("Click on Info EnelEnergia on services section -- Ends");

			//Step 6
			InfoEnelEnergiaMyComponent ic =new InfoEnelEnergiaMyComponent(driver);
			logger.write("Verify Info EnelEnergia page title and content -- Starts");
			ic.verifyComponentExistence(ic.headingInfoEnergia);
			ic.comprareText(ic.headingInfoEnergia, InfoEnelEnergiaMyComponent.HEADING_INFOENERGIA, true);
			ic.comprareText(ic.subtitleInfoEnergiaNew, InfoEnelEnergiaMyComponent.SUBTITLE2INFOENERGIA, true);
			ic.comprareText(ic.titleInfoEnergia, InfoEnelEnergiaMyComponent.TITLE_INFOENERGIA, true); 
			logger.write("Verify Info EnelEnergia page title and content -- Ends");

			logger.write("Click on Modifica InfoEnelEnergia Button -- Starts");
			ic.verifyComponentExistence(ic.modificabutton);
			ic.clickComponent(ic.modificabutton);
			Thread.sleep(50000);
			logger.write("Click on Modifica InfoEnelEnergia Button -- Ends");

			//Step 7
			logger.write("Verify Modifica InfoEnelEnergia title and page text -- Starts");
			ModificaInfoEnelEnergiaComponent mc = new ModificaInfoEnelEnergiaComponent(driver);
			Thread.sleep(30000);
			mc.comprareText(mc.pageTitle, ModificaInfoEnelEnergiaComponent.PageTitle, true);
			mc.comprareText(mc.titleSubText, ModificaInfoEnelEnergiaComponent.TitleSubText, true);
			logger.write("Verify Modifica InfoEnelEnergia title and page text -- Ends");

			//Step 8
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
			logger.write("Select a supply using radio button and press continua button -- Starts");
			iee.clickComponent(iee.checkBox);
			iee.isButtonEnabled(iee.continuaButton);
			iee.clickComponent(iee.continuaButton);
			Thread.sleep(5000);
			logger.write("Select a supply using radio button and press continua button -- Ends");

			//Step 9
			logger.write("Verify the Edit InfoEnelEnergia service page text and componets -- Starts");
			iee.comprareText(iee.inserminatoDati, InfoEnelEnergiaACRComponent.INSERIMENTO_DATI, true);
			iee.comprareText(iee.selezionaText, InfoEnelEnergiaACRComponent.SELEZIONA_TIPOLOGIA, true);

			iee.comprareText(iee.comunicaLettura, InfoEnelEnergiaACRComponent.COMUNICA_LETTURA, true);
			iee.verifyComponentExistence(iee.smsCL);
			iee.verifyComponentExistence(iee.emailCL);

			iee.comprareText(iee.emissioneBolletta, InfoEnelEnergiaACRComponent.EMISSIONE_BOLLETTA, true);
			iee.verifyComponentExistence(iee.smsEB);
			iee.verifyComponentExistence(iee.emailEB);

			iee.comprareText(iee.AvvenutoPagamento, InfoEnelEnergiaACRComponent.AVVENUTO_PAGAMENTO, true);
			iee.verifyComponentExistence(iee.smsAP);
			iee.verifyComponentExistence(iee.emailAP);

			iee.comprareText(iee.scadenzaProssimaBolletta, InfoEnelEnergiaACRComponent.SCADENZA_PROSSIMA_BOLLETTA, true);
			iee.verifyComponentExistence(iee.smsSPB);
			iee.verifyComponentExistence(iee.emailSPB);

			iee.comprareText(iee.sollecitoPagamento, InfoEnelEnergiaACRComponent.SOLLECITO_PAGAMENTO, true);
			iee.verifyComponentExistence(iee.smsSP);
			iee.verifyComponentExistence(iee.emailSP);

			iee.comprareText(iee.avvisoConsumi, InfoEnelEnergiaACRComponent.AVVISO_CONSUMI, true);
			iee.verifyComponentExistence(iee.smsAC);
			iee.verifyComponentExistence(iee.emailAC);
			logger.write("Verify the Edit InfoEnelEnergia service page text and componets -- Ends");

			//Step 10
			iee.comprareText(iee.InserisciiTuoiDati, InfoEnelEnergiaACRComponent.INSERISCI_TUAI_DATI, true);
			logger.write("Verify the Email and Phone fields -- Starts");
			iee.verifyComponentExistence(iee.emailInserisciiTuoiDatiLabel);
			iee.verifyComponentExistence(iee.EmailInserisciiTuoiDatiInput);
			iee.verifyComponentExistence(iee.cellulareInserisciiTuoiDatiLabel);
			iee.verifyComponentExistence(iee.cellulareInserisciiTuoiDatiInput);
			logger.write("Verify the Email and Phone fields -- Ends");

			//Step 11 
			logger.write("Verify the Aggiorna dati di anagrafica check box -- Starts");
			iee.verifyComponentExistence(iee.aggiornaDatiAnagrafici);
			logger.write("Verify the Aggiorna dati di anagrafica check box -- Ends");

			//Step 12
			logger.write("Click on INDIETRO button and verify it goes to previous page -- Starts");
			iee.clickComponent(iee.backButton);
			logger.write("Click on INDIETRO button and verify it goes to previous page -- Ends");

			//Step 13
			logger.write("Click on continue and modify at least one of the checkboxes present, press continue again -- Starts");
			iee.clickComponent(iee.continuaButton);
			iee.clickComponent(iee.emailEB);
			
			//
			
			iee.enterInputParameters(iee.EmailInserisciiTuoiDatiInput, prop.getProperty("EMAIL"));
			iee.enterInputParameters(iee.ConfirmEmailInserisciiTuoiDatiInput, prop.getProperty("CONFERMA_EMAIL"));
			
			//
			iee.clickComponent(iee.continuaIEEButton);
			logger.write("Click on continue and modify at least one of the checkboxes present, press continue again -- Ends");

			//Step 14 
			logger.write("Verify the system navigates to step2 -- Starts");
			iee.verifyComponentExistence(iee.tipologiaDiAvviso);
			logger.write("Verify the system navigates to step2 -- Ends");

			//Step 15
			logger.write("Click on confirm and verify the success text -- Starts");
			iee.clickComponent(iee.confermaButton); 
			Thread.sleep(5000);
			//iee.comprareText(iee.OperazioneTitle, InfoEnelEnergiaACRComponent.OPERAZIONE_HEADER, true);
			logger.write("Click on confirm and verify success text -- Ends");

			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
