package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.DuplicatoBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DuplicatoBollette_ACR_369 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		DuplicatoBolletteComponent mod = new DuplicatoBolletteComponent(driver);
		DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
		
		String invioDocumenti ="https://www-coll1.enel.it/it/area-clienti/residenziale/invio_documenti";
		String iTuoiDiritti ="https://www-coll1.enel.it/it/area-clienti/residenziale/i-tuoi-diritti#!";
		String uploadDocumenti = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AC&email=rita.mastellone@accenture.com";
		String guidaurl = "https://www-coll1.enel.it/content/dam/enel-it/servizi-online/pdf/Guida_alla_CompilazioneIstanze19_01_2016_v3.pdf";
				
		mod.comprareText(mod.areaRiservata,  DuplicatoBolletteComponent.AREA_RISERVATA, true);
		mod.comprareText(mod.homePage,DuplicatoBolletteComponent.HOME_PAGE, true);
				
		logger.write("click on servizi menu item and verify the section heading and paragraph- started");
		mod.clickComponent(mod.serviziiBSN);
		Thread.sleep(5000);
		
		mod.clickComponent(mod.rateizzazione);
		
		mod.comprareText(mod.areaRiservata,  DuplicatoBolletteComponent.AREA_RISERVATA, true);
		mod.comprareText(mod.fornitura,  DuplicatoBolletteComponent.FORNITURE, true);
		mod.comprareText(mod.rateizzazioneActive,  DuplicatoBolletteComponent.RATEIZZAZIONE, true);
		
		mod.comprareText(mod.rateizzazioneParagraph, mod.RATEIZZAZIONE_PARAGRAPH, true);
		
		mod.verifyComponentExistence(mod.esciButton);
		
		mod.clickComponent(mod.esciButton);
		
		mod.comprareText(mod.areaRiservata,  DuplicatoBolletteComponent.AREA_RISERVATA, true);
		mod.comprareText(mod.homePage,DuplicatoBolletteComponent.HOME_PAGE, true);
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
