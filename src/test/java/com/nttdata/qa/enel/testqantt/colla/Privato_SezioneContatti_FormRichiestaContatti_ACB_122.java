package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSN_ConactDetailsComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_SezioneContatti_FormRichiestaContatti_ACB_122 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
	    prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
	    
		PrivateArea_Imprese_HomePageBSNComponent home = new PrivateArea_Imprese_HomePageBSNComponent(driver);
		logger.write("Check for Left Menu - Start");
		home.leftMenuItemsDisplay(home.leftMenuItems);
		logger.write("Check for Left Menu - Completed");
		
		logger.write("Click on contatti from Left Menu - Start");
		home.clickComponent(home.contatti);
		logger.write("Click on contatti from Left Menu - Completed");

		BSN_ConactDetailsComponent bs = new BSN_ConactDetailsComponent(driver);
		
		bs.waitForElementToDisplay(bs.pageTitle);
		bs.verifyComponentExistence(bs.pageTitle);
		bs.comprareText(bs.subText, bs.SubText, true);
		logger.write("Check for contatti fields - Start");
		bs.verifyFields(bs.seiInteressatoA, bs.fields);
		bs.verifyFields(bs.tipologiaOfferta, bs.fields);
		bs.verifyFields(bs.provincia, bs.fields);
		bs.verifyFields(bs.canaleContattoPreferito, bs.fields);
		bs.verifyFields(bs.email, bs.fields);
		bs.verifyFields(bs.telefonoFisso, bs.fields);
		bs.verifyFields(bs.cellulare, bs.fields);
		logger.write("Check for contatti fields - Completed");
		bs.verifyComponentExistence(bs.informativaSullaPrivacy);
		bs.comprareText(bs.bottemText, bs.bottomText, true);
		bs.verifyComponentExistence(bs.acceto);
		bs.verifyComponentExistence(bs.nonAcceto);
		logger.write("Click on invia button - Start");
		bs.clickComponent(bs.invia);
		logger.write("Click on invia button - Completed");

		logger.write("Check for error message - Start");
		bs.verifyComponentExistence(bs.seiInteressatoAError);
		bs.verifyComponentExistence(bs.tipologiaOffertaError);
		bs.verifyComponentExistence(bs.provinciaError);
		bs.verifyComponentExistence(bs.canaleContattoPreferitoError);
		bs.verifyComponentExistence(bs.emailError);
		logger.write("Check for error message - Completed");

		logger.write("Enter incorrect email input  - Start");
		bs.enterInput(bs.emailInput, prop.getProperty("EMAIL_INPUT"));
		logger.write("Enter incorrect email input  - Completed");

		logger.write("Click on invia button - Start");
		bs.clickComponent(bs.invia);
		logger.write("Click on invia button - Completed");
		
		bs.verifyComponentExistence(bs.emailError);
		
		logger.write("Enter incorrect contact input  - Start");
		bs.enterInput(bs.cellularInput, prop.getProperty("CELLULAR_INPUT"));
		logger.write("Enter incorrect contact input  - Completed");

		logger.write("Click on invia button - Start");
		bs.clickComponent(bs.invia);
		logger.write("Click on invia button - Completed");
		
		logger.write("Check for cellular error  - Start");
		bs.checkFieldDisplay(bs.cellulareError);
		logger.write("Check for cellular error  - Completed");

		prop.setProperty("RETURN_VALUE", "OK"); 
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
