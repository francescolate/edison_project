package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class AzioniDettagliSpedizioneCopiaDocumenti {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            DettagliSpedizioneCopiaDocComponent forn = new DettagliSpedizioneCopiaDocComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);

            util.getFrameActive();

            logger.write("Nella sezione Dettagli spedizione seleziona Canale Invio " + prop.getProperty("CANALE_INVIO") + " - Start");
            forn.inserisciCanaleInvio(prop.getProperty("CANALE_INVIO"));
            logger.write("Nella sezione Dettagli spedizione seleziona Canale Invio " + prop.getProperty("CANALE_INVIO") + " - Completed");

            gestione.checkSpinnersSFDC();

            //inserimento indirizzo se canale invio corrisponde a EMAIL
            if (prop.getProperty("CANALE_INVIO").contentEquals("EMAIL")) {
                logger.write("Inserimento indirizzo email - Start");
                By ind_mail = forn.indirizzo_mail1;
                forn.enterDeliveryChannel(ind_mail, prop.getProperty("INDIRIZZO_EMAIL"));
                logger.write("Inserimento indirizzo email - Completed");
            } else if (prop.getProperty("CANALE_INVIO").contentEquals("PEC")) {
                logger.write("Inserimento indirizzo pec - Start");
                By ind_mail_pec = forn.indirizzo_pec1;
                forn.enterDeliveryChannel(ind_mail_pec, prop.getProperty("INDIRIZZO_EMAIL"));
                logger.write("Inserimento indirizzo pec - Completed");
            } else if (prop.getProperty("CANALE_INVIO").contentEquals("FAX")) {
                logger.write("Inserimento FAX - Start");
                By ind_num_fax = forn.indirizzo_fax1;
                forn.enterDeliveryChannel(ind_num_fax, prop.getProperty("NUMERO_FAX"));
                logger.write("Inserimento FAX - Completed");
            } else if (prop.getProperty("CANALE_INVIO").contentEquals("POSTA")) {
                logger.write("Inserimento indirizzo - Start");
                if (Costanti.statusUbiest.contentEquals("ON")) {
                    // sezione da verificare, per indisponibilità di UBItest
                    //forn.verifyComponentExistence(forn.indirizzoVerificato);
                    forn.clickComponent(forn.buttonConfermaIndirizzi);
                }
                logger.write("Inserimento indirizzo - Completed");
            }


            util.getFrameActive();

            if (prop.getProperty("CANALE_INVIO").contentEquals("EMAIL")) {
                logger.write("Verifica apertura popup 'Il Cliente è interessato all'attivazione della Bolletta in formato elettronico (WEB)?' e click su No - Start");
                if (util.exists(forn.button_NO_popup, 5))
                    forn.clickComponentIfExist(forn.button_NO_popup);
                logger.write("Verifica apertura popup 'Il Cliente è interessato all'attivazione della Bolletta in formato elettronico (WEB)?' e click su No - Completed");
            }
            gestione.checkSpinnersSFDC();

            if (prop.getProperty("CANALE_INVIO").contentEquals("PEC")) {
                logger.write("Verifica apertura popup 'Il Cliente è interessato all'attivazione della Bolletta in formato elettronico (WEB)?' e click su No - Start");
                if (util.exists(forn.button_NO_popup, 5))
                    forn.clickComponentIfExist(forn.button_NO_popup);
                logger.write("Verifica apertura popup 'Il Cliente è interessato all'attivazione della Bolletta in formato elettronico (WEB)?' e click su No - Completed");
            }

            gestione.checkSpinnersSFDC();

            logger.write("verifica che il pulsante 'Conferma Dettagli Spedizione' sia abilitato e click su questo - Start");

            forn.clickComponentIfExist(forn.buttonConfermaDettagliSpedizione);
            logger.write("verifica che il pulsante 'Conferma Dettagli Spedizione' sia abilitato e click su questo - Completed");
            Thread.sleep(60000);
            try {
            	forn.clickComponentIfExist(forn.button_NO_popup);
            } catch (Exception e) {
            	System.out.println("No button_NO_popup. Continuing execution flow");
            }
            //forn.clickComponentIfExist(forn.button_NO_popup);
            logger.write("verifica che il pulsante 'Conferma' sia abilitato e click su questo - Start");
            forn.clickComponentIfExist(forn.buttonConferma);
            logger.write("verifica che il pulsante 'Conferma' sia abilitato e click su questo - Completed");


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }


    }

}
