package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.PLACETFissaLuceConsumerComponent;
import com.nttdata.qa.enel.components.colla.PaginaAdesionePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PLACETFissaLuceConsumer {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);	
			
			PLACETFissaLuceConsumerComponent offerta=new PLACETFissaLuceConsumerComponent(driver);
			logger.write("Ricercare l'offerta 'Enel Energia PLACET Fissa Luce Consumer', clickare sul link ATTIVA ORA e check ipod - Start");
			By offerta_placetFissaLuceConsumer=offerta.offertaplacetFissaLuceConsumer;
			offerta.verifyComponentExistence(offerta_placetFissaLuceConsumer);
			offerta.scrollComponent(offerta_placetFissaLuceConsumer);
			
			By attivaOra_Link=offerta.attivaOraLink;
			offerta.verifyComponentExistence(attivaOra_Link);
						
			offerta.clickeaspetta(attivaOra_Link);
			By titolo_Pagina=offerta.titoloPagina;
			offerta.verifyComponentExistence(titolo_Pagina);
			
			PaginaAdesionePodComponent pod=new PaginaAdesionePodComponent(driver);
			By sotto_titolo=pod.testoconpod;
			pod.verificaTesto(sotto_titolo, pod.testoconIconaInformativaPod1);
			
			By i_pod=pod.ipod;
			pod.clickComponent(i_pod);
			TimeUnit.SECONDS.sleep(3);
			By titolo_IpodPopup=pod.titoloIpodPopup;
			pod.verificaTesto(titolo_IpodPopup,pod.titoloPod);
			
			By testo_ipod=pod.testoipod;
			pod.verificaTesto(testo_ipod,pod.descrizionePod);
			
			By chiudi_ipod=pod.chiudiipod;
			pod.clickComponent(chiudi_ipod);
			TimeUnit.SECONDS.sleep(1);
			offerta.verifyComponentExistence(titolo_Pagina);
			logger.write("Ricercare l'offerta 'Enel Energia PLACET Fissa Luce Consumer', clickare sul link ATTIVA ORA e check ipod - Completed");
						
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sul motorino di ricerca - Start");
			By chiudi_Adesione=pod.chiudiAdesione;
			pod.clickComponent(chiudi_Adesione);
			By escisenzasalvare_button=pod.escisenzasalvarebutton;
			pod.verifyComponentExistence(escisenzasalvare_button);
			pod.scrollComponent(escisenzasalvare_button);
			TimeUnit.SECONDS.sleep(3);
			pod.clickcomponentjavascript(escisenzasalvare_button);
			
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);			
			By searchMenu=headerMenu.searchSection;
			
			headerMenu.verifyQuestionsExistence(searchMenu);
			By precedentcontract=headerMenu.contractListvalue;
			headerMenu.verifyTextSearchMenu(precedentcontract,prop.getProperty("TYPE_OF_CONTRACT"));
			By precedentplace=headerMenu.placeListvalue;
			headerMenu.verifyTextSearchMenu(precedentplace,prop.getProperty("PLACE"));
			By precedentneed=headerMenu.Listvalue;
			headerMenu.verifyTextSearchMenu(precedentneed,prop.getProperty("NEED_PRECEDENTE"));
			
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sul motorino di ricerca - Completed");
			
			logger.write("Ricercare l'offerta 'Enel Energia PLACET Fissa Luce Consumer', clickare sul link DETTAGLIO OFFERTA e check sulla presenza della spalletta - Start");
			offerta.verifyComponentExistence(offerta_placetFissaLuceConsumer);
			offerta.scrollComponent(offerta_placetFissaLuceConsumer);
			By dettaglioOfferta_Button=offerta.dettaglioOffertaButton;
			offerta.verifyComponentExistence(dettaglioOfferta_Button);
			offerta.clickComponent(dettaglioOfferta_Button);
					
			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkOFFERTA_PLACET);
			
			By attivaora_spalletta=offerta.attivaoraspalletta;
			offerta.verifyComponentExistence(attivaora_spalletta);
			
			By dettagli_Splattetta=offerta.dettagliSplattetta;
			//offerta.verificaTesto(dettagli_Splattetta,offerta.dettagli_Spalletta);
			offerta.verificaTesto2(dettagli_Splattetta);
			By scritta_PiccolaSpalletta=offerta.scrittaPiccolaSpalletta;
			offerta.verificaTesto(scritta_PiccolaSpalletta,offerta.scritta_in_piccolo);
			offerta.checkColor(scritta_PiccolaSpalletta, "Black", "la scritta in piccolo della spalletta");
			logger.write("Ricercare l'offerta 'Enel Energia PLACET Fissa Luce Consumer', clickare sul link DETTAGLIO OFFERTA e check sulla presenza della spalletta - Completed");
			
			logger.write("verifica presenza della spalletta dopo lo scroll della pagina- Start");
			By documenti_generali=offerta.documentigenerali;
			offerta.scrollComponent(documenti_generali);
			offerta.verifyComponentExistence(dettagli_Splattetta);
			logger.write("verifica presenza della spalletta dopo lo scroll della pagina- Completed");
			TimeUnit.SECONDS.sleep(1);
			
			logger.write("click su ATTIVA ORA in alto alla spalletta - Start");
			offerta.verifyComponentExistence(attivaora_spalletta);
			TimeUnit.SECONDS.sleep(3);
			offerta.clickeaspetta(attivaora_spalletta);
			TimeUnit.SECONDS.sleep(3);
			offerta.verifyComponentExistence(titolo_Pagina);
			
			pod.verificaTesto(sotto_titolo, pod.testoconIconaInformativaPod1);
			TimeUnit.SECONDS.sleep(1);
			
			pod.clickComponent(i_pod);	
			TimeUnit.SECONDS.sleep(3);
			pod.verificaTesto(titolo_IpodPopup,pod.titoloPod);
			pod.verificaTesto(testo_ipod,pod.descrizionePod);
			
			pod.clickComponent(chiudi_ipod);
			TimeUnit.SECONDS.sleep(1);
			offerta.verifyComponentExistence(titolo_Pagina);
			logger.write("Ricercare l'offerta 'Enel Energia PLACET Fissa Luce Consumer', clickare sul link ATTIVA ORA e check ipod - Completed");
			
		
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e ritorno sulla pagina dell'offerta placet-consumer-luce-fissa - Start");
			
			pod.clickComponent(chiudi_Adesione);
			pod.verifyComponentExistence(escisenzasalvare_button);
			pod.scrollComponent(escisenzasalvare_button);
			TimeUnit.SECONDS.sleep(3);
			pod.clickcomponentjavascript(escisenzasalvare_button);
			
			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkOFFERTA_PLACET);
			
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e ritorno sulla pagina dell'offerta placet-consumer-luce-fissa - Completed");
			
			logger.write("click sul pulsante ATTIVA L'OFFERTA nel banner della pagina di dettaglio dell'offerta Enel Energia PLACET Fissa Luce Consumer - Start");
			By attiva_offertaButton=offerta.attivaoffertaButton;
			offerta.verifyComponentExistence(attiva_offertaButton);
			TimeUnit.SECONDS.sleep(3);
			offerta.clickeaspetta(attiva_offertaButton);
			TimeUnit.SECONDS.sleep(3);
			
			offerta.verifyComponentExistence(titolo_Pagina);
			TimeUnit.SECONDS.sleep(3);
			
	
            pod.verificaTesto(sotto_titolo, pod.testoconIconaInformativaPod1);
			TimeUnit.SECONDS.sleep(1);
			
			pod.clickComponent(i_pod);
			TimeUnit.SECONDS.sleep(3);
			pod.verificaTesto(titolo_IpodPopup,pod.titoloPod);
			pod.verificaTesto(testo_ipod,pod.descrizionePod);
			
			pod.clickComponent(chiudi_ipod);
			TimeUnit.SECONDS.sleep(1);
			offerta.verifyComponentExistence(titolo_Pagina);
			logger.write("click sul pulsante ATTIVA L'OFFERTA nel banner della pagina di dettaglio dell'offerta Enel Energia PLACET Fissa Luce Consumer - Completed");
			
		
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e ritorno sulla pagina dell'offerta placet-consumer-luce-fissa - Start");
			
			pod.clickComponent(chiudi_Adesione);
			pod.verifyComponentExistence(escisenzasalvare_button);
			pod.scrollComponent(escisenzasalvare_button);
			TimeUnit.SECONDS.sleep(3);
			pod.clickcomponentjavascript(escisenzasalvare_button);
			
			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkOFFERTA_PLACET);
			
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e ritorno sulla pagina dell'offerta placet-consumer-luce-fissa - Completed");
		
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
