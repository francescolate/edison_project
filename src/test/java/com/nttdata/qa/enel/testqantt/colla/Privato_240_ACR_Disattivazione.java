package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_240_ACR_Disattivazione {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
				/*
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
	
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
								
									
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			logger.write("Click on Residential cusotmer button  - Start");
			dfc.verifyComponentExistence(dfc.PrivatoResCustomerButton);
			dfc.clickComponent(dfc.PrivatoResCustomerButton);
			logger.write("Click on Residential cusotmer button  - Start");
			
			
			dfc.verifyComponentExistence(dfc.BenvenutoTitle);
			dfc.comprareText(dfc.BenvenutoTitle, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_TITLE, true);
			dfc.verifyComponentExistence(dfc.BenvenutoContent);
			dfc.comprareText(dfc.BenvenutoContent, PrivateAreaDisattivazioneFornituraComponent.BENVENUTO_CONTENT, true);
			dfc.verifyComponentExistence(dfc.fornitureHeader);
			dfc.comprareText(dfc.fornitureHeader, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_HEADER, true);
			dfc.verifyComponentExistence(dfc.fornitureContent);
			dfc.comprareText(dfc.fornitureContent, PrivateAreaDisattivazioneFornituraComponent.FORNITURE_CONTENT, true);
			*/
			logger.write("Verify and click on Servizi link  - Start");
			dfc.verifyComponentExistence(dfc.serviziLink);
			dfc.clickComponentIfExist(dfc.serviziLink);
			logger.write("Verify and click on Servizi link  - Complete");
			
			logger.write("Verify and click on Servizi title and contents  - Start");
			dfc.verifyComponentExistence(dfc.serviziFornitureTitle);
			dfc.comprareText(dfc.serviziFornitureTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziFornitureContent);
			dfc.comprareText(dfc.serviziFornitureContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIFORNITURE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteTitle);
			dfc.comprareText(dfc.serviziBolletteTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziBolletteContent);
			dfc.comprareText(dfc.serviziBolletteContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZIBOLLETTE_CONTENT, true);
			dfc.verifyComponentExistence(dfc.serviziContratoTitle);
			dfc.comprareText(dfc.serviziContratoTitle, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_TITLE, true);
			dfc.verifyComponentExistence(dfc.serviziContratoContent);
			dfc.comprareText(dfc.serviziContratoContent, PrivateAreaDisattivazioneFornituraComponent.SERVIZICONTRATO_CONTENT, true);
			logger.write("Verify and click on Servizi title and contents  - Complete");
			
			
			logger.write("Verify and click on disattivazioneFornitura link  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneFornitura);
			dfc.clickComponent(dfc.disattivazioneFornitura);
			logger.write("Verify and click on disattivazioneFornitura link  - Complete");
						
			logger.write("Click on prosegui button  - Start");
			dfc.verifyComponentExistence(dfc.proseguiButton);
			dfc.clickComponent(dfc.proseguiButton);
			logger.write("Click on prosegui button  - Complete");
			
			logger.write("Verify the Esci and disattivazione button - Start");
			dfc.verifyComponentExistence(dfc.buttonEsci);
			dfc.verifyComponentExistence(dfc.disattivazioneButtonDisabled);
			logger.write("Verify the Esci and disattivazione button - Complete");
			
			logger.write("Click on Bollette link  - Start");
			dfc.verifyComponentExistence(dfc.bolletteLink);
			dfc.clickComponent(dfc.bolletteLink);
			logger.write("Click on Bollette link  - Complete");
			
			logger.write("Click on No button in the popup window  - Start");
			dfc.verifyComponentExistence(dfc.bollettePopupNoBtn);
			dfc.clickComponent(dfc.bollettePopupNoBtn);
			logger.write("Click on No button in the popup window  - Complete");
			
			Thread.sleep(2000);
			logger.write("Verify disattivazione header and contents  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneHeader);
			//dfc.comprareText(dfc.disattivazioneHeader, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_HEADER, true);
			dfc.verifyComponentExistence(dfc.disattivaContent);
			dfc.comprareText(dfc.disattivaContent, PrivateAreaDisattivazioneFornituraComponent.DISATTIVA_CONTENT, true);

			logger.write("Click on Bollette link  - Start");
			dfc.clickComponent(dfc.bolletteLink);
			logger.write("Click on Bollette link  - Complete");
			
			logger.write("Verify the popup title and buttons  - Start");
			dfc.verifyComponentExistence(dfc.bollettePopupContent);
			dfc.comprareText(dfc.bollettePopupContent, PrivateAreaDisattivazioneFornituraComponent.BOLLETTEPOUP_CONTENT	, true);
			dfc.verifyComponentExistence(dfc.bollettePopupNoBtn);
			dfc.verifyComponentExistence(dfc.bollettePopupSIBtn);
			dfc.verifyComponentExistence(dfc.bollettePoupupClose);
			logger.write("Verify the popup title and buttons  - Complete");
			
			logger.write("Click on close in popup window  - Start");
			dfc.clickComponent(dfc.bollettePoupupClose);
			logger.write("Click on close in popup window  - Complete");
			
			Thread.sleep(2000);
			logger.write("Verify disattivazione header and contents  - Start");
			dfc.verifyComponentExistence(dfc.disattivazioneHeader);
			//dfc.comprareText(dfc.disattivazioneHeader, PrivateAreaDisattivazioneFornituraComponent.DISATTIVAZIONE_HEADER, true);
			dfc.verifyComponentExistence(dfc.disattivaContent);
			dfc.comprareText(dfc.disattivaContent, PrivateAreaDisattivazioneFornituraComponent.DISATTIVA_CONTENT, true);

			logger.write("Verify disattivazione header and contents  - Complete");
			
			logger.write("Click on Bollette link  - Start");
			dfc.clickComponent(dfc.bolletteLink);
			logger.write("Click on Bollette link  - Complete");
			
			logger.write("Verify the popup title and buttons  - Start");
			dfc.verifyComponentExistence(dfc.bollettePopupContent);
			dfc.comprareText(dfc.bollettePopupContent, PrivateAreaDisattivazioneFornituraComponent.BOLLETTEPOUP_CONTENT	, true);
			dfc.verifyComponentExistence(dfc.bollettePopupNoBtn);
			dfc.verifyComponentExistence(dfc.bollettePopupSIBtn);
			dfc.verifyComponentExistence(dfc.bollettePoupupClose);
			logger.write("Verify the popup title and buttons  - Complete");
			
			logger.write("Click on SI button in the popup window  - Start");
			dfc.clickComponent(dfc.bollettePopupSIBtn);
			logger.write("Click on SI button in the popup window  - Complete");		
			
			dfc.verifyComponentExistence(dfc.letueBollette);
			dfc.comprareText(dfc.letueBollette, PrivateAreaDisattivazioneFornituraComponent.LETUE_BOLLETTE, true);
			dfc.verifyComponentExistence(dfc.letueContent);
			dfc.comprareText(dfc.letueContent, PrivateAreaDisattivazioneFornituraComponent.LETUE_CONTENT, true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
	
	
}
