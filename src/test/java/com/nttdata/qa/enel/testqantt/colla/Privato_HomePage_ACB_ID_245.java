package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_HomePage_ACB_ID_245 {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			
			logger.write("Launch the page and verify the fields and button  - Start");
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
			logger.write("Launch the page and verify the fields and button  - Complete");
		
								
			PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
						
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			logger.write("Click on BSN customer area  - Start");
			dfc.verifyComponentExistence(dfc.areaClientiImpressa);
			dfc.clickComponent(dfc.areaClientiImpressa);
			logger.write("Click on BSN customer area  - Complete");
			
			logger.write("Verify the home page page and headers  - Start");
			dfc.verifyComponentExistence(dfc.homePageBSNPath);
			dfc.comprareText(dfc.homePageBSNPath, PrivateAreaDisattivazioneFornituraComponent.HOMEPAGE_BSNPATH, true);
			dfc.verifyComponentExistence(dfc.headerBSN);
			dfc.comprareText(dfc.headerBSN, PrivateAreaDisattivazioneFornituraComponent.HEADER_BSN, true);
			dfc.verifyComponentExistence(dfc.fornitureHeaderBSN);
			dfc.comprareText(dfc.fornitureHeaderBSN, PrivateAreaDisattivazioneFornituraComponent.FORNITUREHEADER_BSN, true);
			dfc.verifyComponentExistence(dfc.fornituraAggiungi);
			dfc.comprareText(dfc.fornituraAggiungi, PrivateAreaDisattivazioneFornituraComponent.FORNITURA_AGGIUNGI, true);
			dfc.verifyComponentExistence(dfc.fornitureLuce);
			dfc.comprareText(dfc.fornitureLuce, PrivateAreaDisattivazioneFornituraComponent.FORNITURALUCE_AGGIUNGI, true);
			logger.write("Verify the home page page and headers  - Complete");
			
			logger.write("Verify the Forniture e bollette label and values - Start");
			dfc.verifyComponentExistence(dfc.fornituraDettaglio);
			dfc.comprareText(dfc.fornituraDettaglio, PrivateAreaDisattivazioneFornituraComponent.FORNITURA_DETTAGLIO, true);
			dfc.verifyComponentExistence(dfc.luogo);
			dfc.comprareText(dfc.luogo, PrivateAreaDisattivazioneFornituraComponent.LUOGO, true);
			dfc.verifyComponentExistence(dfc.LuogoValue_92019);
			dfc.comprareText(dfc.LuogoValue_92019, PrivateAreaDisattivazioneFornituraComponent.LUOGOVALUE_92019, true);
			dfc.verifyComponentExistence(dfc.tipologia);
			dfc.comprareText(dfc.tipologia, PrivateAreaDisattivazioneFornituraComponent.TIPOLOGIA, true);
			dfc.verifyComponentExistence(dfc.offerta);
			dfc.comprareText(dfc.offerta, PrivateAreaDisattivazioneFornituraComponent.OFFERTA, true);
			dfc.verifyComponentExistence(dfc.offertaLuce);
			dfc.comprareText(dfc.offertaLuce, PrivateAreaDisattivazioneFornituraComponent.OFFERTA_LUCE, true);
			dfc.verifyComponentExistence(dfc.stato);
			dfc.comprareText(dfc.stato, PrivateAreaDisattivazioneFornituraComponent.STATO, true);
			dfc.verifyComponentExistence(dfc.statoValue);
			dfc.comprareText(dfc.statoValue, PrivateAreaDisattivazioneFornituraComponent.STATO_VALUE, true);
			logger.write("Verify the Forniture e bollette label and values - Complete");
			
			logger.write("Click on PIU Information link - Start");
			dfc.verifyComponentExistence(dfc.piuInformationLink);
			dfc.clickComponent(dfc.piuInformationLink);
			logger.write("Click on PIU Information link - Complete");
			
			logger.write("Verify the Tuoi dati details - Start");
			dfc.verifyComponentExistence(dfc.tuttoHeader);
			dfc.comprareText(dfc.tuttoHeader, PrivateAreaDisattivazioneFornituraComponent.TUTTO_HEADER, true);
			dfc.verifyComponentExistence(dfc.tuttoContent);
			dfc.comprareText(dfc.tuttoContent, PrivateAreaDisattivazioneFornituraComponent.TUTTO_CONTENT, true);
			dfc.verifyComponentExistence(dfc.cliente);
			dfc.comprareText(dfc.cliente, PrivateAreaDisattivazioneFornituraComponent.CLIENTE, true);
			dfc.verifyComponentExistence(dfc.codiceFiscale);
			dfc.comprareText(dfc.codiceFiscale, PrivateAreaDisattivazioneFornituraComponent.CODICEFISCALE, true);
			logger.write("Verify the Tuoi dati details - Complete");
			
			
			dfc.verifyComponentExistence(dfc.indrizzoFornitura);
			dfc.comprareText(dfc.indrizzoFornitura, PrivateAreaDisattivazioneFornituraComponent.INDRIZZOFORNITURA, true);
			dfc.verifyComponentExistence(dfc.tipologiaElectrico);
			dfc.comprareText(dfc.tipologiaElectrico, PrivateAreaDisattivazioneFornituraComponent.TIPOLOGIA_ELECTRICO, true);
			dfc.verifyComponentExistence(dfc.modalitaEmail);
			dfc.comprareText(dfc.modalitaEmail, PrivateAreaDisattivazioneFornituraComponent.MODALITA_EMAIL, true);
			dfc.verifyComponentExistence(dfc.modalitaPostale);
			dfc.comprareText(dfc.modalitaPostale, PrivateAreaDisattivazioneFornituraComponent.MODALITA_POSTALE, true);
			
			logger.write("Click on home page link  - Start");
			dfc.verifyComponentExistence(dfc.homePageLink);
			dfc.clickComponent(dfc.homePageLink);
			logger.write("Click on home page link  - Complete");
			
			logger.write("Click on Vai All Bollette  - Start");
			dfc.verifyComponentExistence(dfc.vaiAllBollette);
			dfc.clickComponent(dfc.vaiAllBollette);
			logger.write("Click on Vai All Bollette  - Complete");
			
			Thread.sleep(5000);
			
			logger.write("Verify the bill columns  - Start");
			dfc.verifyBillColumn(dfc.billColumns);
			logger.write("Verify the bill columns  - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			


}
