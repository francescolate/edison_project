package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.AddebitoDirettoComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ModificaAddebitoDirettoComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_338_AddebitoContoCorrente {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			hm.clickComponent(hm.services);
			
			ServicesComponent sc = new ServicesComponent(driver);
			sc.verifyComponentExistence(sc.directDebit);
			sc.clickComponent(sc.directDebit);
			Thread.sleep(10000);
			AddebitoDirettoComponent ac = new AddebitoDirettoComponent(driver);
			ac.verifyComponentExistence(ac.pageTitle);
			ac.comprareText(ac.titleSubText, ac.TitleSubText, true);
			ac.comprareText(ac.subText, ac.SubText, true);
			Thread.sleep(5000);
			ac.verifyComponentExistence(ac.iIcon);
			ac.clickComponent(ac.iIcon);
			ac.verifyComponentExistence(ac.alertTitle);
			ac.comprareText(ac.alertInnerText, ac.AlertInnerText, true);
			ac.clickComponent(ac.alertClose);
			
			ac.verifyComponentExistence(ac.addressLable);
		//	ac.comprareText(ac.address, prop.getProperty("ADDRESS"), true);
			ac.verifyAddress(ac.address, prop.getProperty("ADDRESS"));
			ac.verifyComponentExistence(ac.IBANLable);
			ac.comprareText(ac.IBANValue, ac.IBAN, true);
			ac.comprareText(ac.text, ac.Text, true);
			ac.verifyComponentExistence(ac.modificaContoCorrente);
			ac.verifyComponentExistence(ac.attivaSuCartaDiCredito);
			ac.verifyComponentExistence(ac.ESCI);
			ac.clickComponent(ac.visualizzaTutte);
			ac.clickComponent(ac.modificaContoCorrente);
			
			ModificaAddebitoDirettoComponent mc = new ModificaAddebitoDirettoComponent(driver);
			Thread.sleep(50000);
			mc.verifyComponentExistence(mc.pageTitle);
			mc.comprareText(mc.titleSubText, mc.TitleSubText, true);
			mc.verifyComponentExistence(mc.addressLable);
			ac.verifyAddress(mc.address, prop.getProperty("ADDRESS"));
			//mc.comprareText(mc.address, prop.getProperty("ADDRESS"), true);
			mc.verifyComponentExistence(mc.numerloClienteLable);
			mc.comprareText(mc.numerloCliente, mc.NumerloCliente, true);
			mc.verifyComponentExistence(mc.modalitaAddebitoDiretto);
			mc.comprareText(mc.modalitaAddebitoDirettoValue, mc.ModalitaAddebitoDirettoValue, true);
			mc.verifyComponentExistence(mc.radioButton);
			mc.clickComponent(mc.radioButton);
			mc.clickComponent(mc.modificaAddebito);
			
			mc.verifyComponentExistence(mc.pageTitle);
			Thread.sleep(10000);
			mc.comprareText(mc.titleSubText1, mc.TitleSubText1, true);
			mc.verifyComponentExistence(mc.inserimentoDati);
			mc.verifyComponentExistence(mc.Esito);
			mc.verifyComponentExistence(mc.riepilogoEConfermaDati);
			
			mc.checkPrePopulatedValue(mc.cognomeString, prop.getProperty("LAST_NAME"));
			mc.checkPrePopulatedValue(mc.nomeString, prop.getProperty("FIRST_NAME"));
			mc.checkPrePopulatedValue(mc.CFString, prop.getProperty("CF"));
			
			
			mc.verifyFieldIsNotEditable(mc.agenzia);
			mc.verifyFieldIsNotEditable(mc.nomeBanca);
			mc.verifyComponentExistence(mc.INDIETRO);
			mc.verifyComponentExistence(mc.CONTINUA);
			mc.clickComponent(mc.CONTINUA);
			mc.comprareText(mc.IBANError, mc.IBANERROR, true);
			mc.enterInputToField(mc.IBAN, mc.IBANInput);
			Thread.sleep(10000);
			mc.checkPrePopulatedValue(mc.agenziaString, mc.AGENCIA);
			mc.checkPrePopulatedValue(mc.nomeBancaString, mc.NOMEBANCA);
			mc.clickComponent(mc.CONTINUA);
			
			mc.verifyComponentExistence(mc.pageTitle);
			mc.comprareText(mc.titleSubText1, mc.TitleSubText1, true);
			mc.comprareText(mc.menuTwoDescription, mc.MenuTwoDescription, true);
			mc.verifyComponentExistence(mc.ragione);
			mc.comprareText(mc.ragioneValue, mc.RagioneValue, true);
			mc.verifyComponentExistence(mc.nomeIntestatario);
			mc.comprareText(mc.nomeIntestatarioValue, mc.NomeIntestatarioValue, true);
			mc.verifyComponentExistence(mc.CF1);
			mc.comprareText(mc.CF1Value, prop.getProperty("CF"), true);
			mc.verifyComponentExistence(mc.IBAN1);
			mc.comprareText(mc.IBAN1Value, mc.IBANInput, true);
			mc.verifyComponentExistence(mc.Agenzia1);
			mc.comprareText(mc.Agenzia1Value, mc.AGENCIA, true);
			mc.verifyComponentExistence(mc.nomeBanca1);
			mc.comprareText(mc.nomeBanca1Value, mc.NOMEBANCA, true);
			mc.comprareText(mc.menuTwoText, mc.MenuTwoText, true);
			mc.verifyComponentExistence(mc.addressLable);
			ac.verifyAddress(mc.address, prop.getProperty("ADDRESS"));
			//mc.comprareText(mc.address, prop.getProperty("ADDRESS"), true);
			mc.verifyComponentExistence(mc.numeroClienteLable);
			mc.comprareText(mc.numeroCliente, mc.NumerloCliente, true);
			
			mc.comprareText(mc.menuTwoText1, mc.MenuTwoText1, true);
			mc.verifyComponentExistence(mc.email);
			mc.verifyComponentExistence(mc.campiObbligatori);
			mc.verifyComponentExistence(mc.INDIETRO);
			mc.verifyComponentExistence(mc.conferma);
			mc.enterInputToField(mc.email, prop.getProperty("EMAIL_INPUT"));
			mc.enterInputToField(mc.confirmEmail, prop.getProperty("EMAIL_INPUT"));
			mc.clickComponent(mc.conferma);
			
			mc.verifyComponentExistence(mc.menuThreeDescription);
			mc.comprareText(mc.menuThreeDescription, mc.MenuThreeDescrition, true);
			mc.comprareText(mc.menuThreeText, mc.MenuThreeText, true);
			mc.verifyComponentExistence(mc.fine);
			mc.clickComponent(mc.fine);
			
			hm.verifyComponentExistence(hm.pageTitleRes);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			
			Thread.sleep(90000);
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
