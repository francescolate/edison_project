package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.ScegliTu100x100Component;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ScegliTu100x100 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			OffersLuceComponent olc = new OffersLuceComponent(driver);
			
			logger.write("verifica presenza dell'offerta 'Scegli Tu 100x100' - Start");
			ScegliTu100x100Component offerta=new ScegliTu100x100Component(driver);
			
			By offerta_offertaSceglitu100x100=offerta.offertaSceglitu100x100;
			offerta.verifyComponentExistence(offerta_offertaSceglitu100x100);
			offerta.scrollComponent(offerta_offertaSceglitu100x100);
			logger.write("verifica presenza dell'offerta 'Scegli Tu 100x100' - Completed");
			
			logger.write("Click su <ATTIVA ORA> per l'offerta 'Scegli Tu 100x100' - Start");
			olc.verifyComponentExistence(olc.scegliTu100x100);
			olc.clickComponent(olc.scegliTu100x100);
//			By attivaOra_Button=offerta.attivaOraButton;
//			offerta.verifyComponentExistence(attivaOra_Button);
//			offerta.clickComponent(attivaOra_Button);
			
			By titolo_ScegliTu=offerta.titoloScegliTu;
			offerta.verifyComponentExistence(titolo_ScegliTu);
			
			By domanda_Nel_Footer=offerta.domandaNelFooter;
			offerta.verifyComponentExistence(domanda_Nel_Footer);
			offerta.scrollComponent(domanda_Nel_Footer);
			
			By label_TiaiutiamoNoi=offerta.labelTiaiutiamoNoi;
			offerta.verifyComponentExistence(label_TiaiutiamoNoi);
			
			
			By button_IniziaOra=offerta.buttonIniziaOra;
			offerta.verifyComponentExistence(button_IniziaOra);
			logger.write("Click su <ATTIVA ORA> per l'offerta 'Scegli Tu 100x100' - Completed");
			Thread.sleep(3000);
			logger.write("Click su INIZIA ORA per l'offerta 'Scegli Tu 100x100' - Start");
			offerta.jsClickComponent(button_IniziaOra);
			
			logger.write("Click su INIZIA ORA per l'offerta 'Scegli Tu 100x100' - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
