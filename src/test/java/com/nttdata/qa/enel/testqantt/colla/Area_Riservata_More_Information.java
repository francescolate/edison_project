package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AreaRiservataHomePageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Area_Riservata_More_Information {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			AreaRiservataHomePageComponent home = new AreaRiservataHomePageComponent(driver);
									
			logger.write("Fornitura Home Page Title and Description Verification- Starts");
			home.VerifyText(home.supplyHomePageTitle1,prop.getProperty("SUPPLY_HOMEPAGE_TITLE1"));
			home.VerifyText(home.supplyHomePageTitle2,prop.getProperty("SUPPLY_HOMEPAGE_TITLE2"));
			home.VerifyText(home.SupplyTitle1,prop.getProperty("SUPPLY_DETAIL_TITLE1"));
			home.VerifyText(home.supplyDescription1,prop.getProperty("SUPPLY_DETAIL_DESCRIPTION1"));
			logger.write("Fornitura Home Page Log,Title and Description Verification- Ends");
			
//			home.VerifyText(home.supplyDetails1, prop.getProperty("SUPPLY_DETAILS1"));
//			home.verifyComponentExistence(home.modificaLink);
			
			//back Browser
			home.backBrowser(home.homePageLogo);		
											
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}

			
			
