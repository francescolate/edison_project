package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaDatiContatto;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ConfermaButtonComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.ModificaClienteComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaCommodityComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaServizioVASComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModificaAnagraficaCondominio {

	public static void main(String[] args) throws Exception {

		Properties prop = null;

		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			SceltaProcessoComponent sceltaProcesso = new SceltaProcessoComponent(driver);
			ModificaClienteComponent modifica = new ModificaClienteComponent(driver);	
				
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
				sceltaProcesso.clickAllProcess();
				sceltaProcesso.chooseProcessToStart(sceltaProcesso.avvioModificaAnagrafica);
			logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
				identificaInterlocutore.verifyInputFieldExist("Documento");
				identificaInterlocutore.verifyInputFieldExist("Indirizzo di fornitura");
				identificaInterlocutore.verifyInputFieldExist("Numero Cliente");
				identificaInterlocutore.verifyInputFieldExist("Numero fattura");
				
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
				
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Codice Fiscale");
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Completed");
				
				logger.write("Inserimento documento e conferma - Start");
				identificaInterlocutore.insertDocumento("b");
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
				logger.write("Inserimento documento e conferma - Completed");

				TimeUnit.SECONDS.sleep(60);
				
				// NEW SOFIA 12/11 
				CheckListComponent checklist = new CheckListComponent(driver);
				checklist.clickConferma();
				IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
				String numerorichiesta = nuovaRichiesta
				.salvaNumeroRichiesta(nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
				prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());
				
				
				modifica.verificaControlloForniture();
				//Modifica inserita in data 11/02 per salvataggio nuova ragione sociale
				String rag_sociale = SeleniumUtilities.randomAlphaNumeric(8);
				modifica.modificaRagioneSociale( rag_sociale);
				
				prop.setProperty("RAGIONE_SOCIALE", rag_sociale);
				
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				
				driver.switchTo();
				if (util.verifyExistence(indirizzi.indirizzoVerificato, 5))
				driver.findElement(indirizzi.buttonVerificaIndirizzo).click();
//				
//				if (Costanti.statusUbiest.contentEquals("OFF")) {
//						if (!util.verifyExistence(indirizzi.indirizzoVerificato, 5))	{
//							indirizzi.compileAddressForzatura(indirizzi.buttonVerificaIndirizzo);
//							indirizzi.closeAlert();
//							indirizzi.forzaIndirizzoSedeLegale(indirizzi.buttonVerificaIndirizzo);						
//						}
//					} else {
//						indirizzi.compileAddressIfAvailable("ROMA", indirizzi.province2, "ROMA", indirizzi.city2,
//								"VIA NIZZA", indirizzi.street2, "5", indirizzi.civico2, indirizzi.buttonVerificaIndirizzo);
//						
//					}
//				
				modifica.inserisciTelefonoCliente( "08133225474");
				
				if (prop.getProperty("MODALITA_ACCETTAZIONE", "Documenti da Firmare").equals("Documenti Validi")) {
					modifica.selezionaModalitaAccettazione( "Documenti Validi");
				}
				
			
				//driver.switchTo();
				modifica.pressButton( modifica.buttonConferma);
				TimeUnit.SECONDS.sleep(10);
				
				// OLD
				/*
				CheckListComponent checklist = new CheckListComponent(driver);
				String frame = checklist.clickFrameConferma2();	
				IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
				String numerorichiesta = nuovaRichiesta
				.salvaNumeroRichiesta(frame,nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
	           //System.out.println(numerorichiesta);
	         	prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());
				ModificaClienteComponent modifica = new ModificaClienteComponent(driver);
				modifica.verificaControlloForniture(frame);
				String ragioneSociale = SeleniumUtilities.randomAlphaNumeric(8);
				prop.setProperty("RAGIONE_SOCIALE", ragioneSociale);
				modifica.modificaRagioneSociale(frame, ragioneSociale);
				
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				if (Costanti.statusUbiest.contentEquals("OFF")) {
					
				} else {
					indirizzi.compileAddressIfAvailable(frame, "NAPOLI", indirizzi.province2, "NAPOLI", indirizzi.city2,
							"VIA ROMA", indirizzi.street2, "1", indirizzi.civico2, indirizzi.buttonVerificaIndirizzo);
		
				}
				*/
				
				/*
				modifica.inserisciTelefonoCliente( "08133225474");
				Thread.currentThread().sleep(5000);
				if (prop.getProperty("MODALITA_ACCETTAZIONE").equals("Documenti Validi")) {
					modifica.selezionaModalitaAccettazione(frame, "Documenti Validi");
				}
				
				modifica.pressButton(frame, modifica.buttonConferma);
				TimeUnit.SECONDS.sleep(10);
				 */
			prop.setProperty("RETURN_VALUE", "OK");
			}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}
	}

