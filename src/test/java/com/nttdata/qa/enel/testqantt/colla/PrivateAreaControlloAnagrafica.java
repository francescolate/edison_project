package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaControlloAnagraficaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


public class PrivateAreaControlloAnagrafica {
	public static void main(String[] args) throws Exception {
		Properties prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaControlloAnagraficaComponent paca = new PrivateAreaControlloAnagraficaComponent(driver);
			By by = null;
			
			// 4
			logger.write("Verificare la presenza della voce 'Account' nel menu a sinistra");	
			paca.verifyComponentExistence(paca.sidebarMenuItemAccount);
			paca.clickComponent(paca.sidebarMenuItemAccount);
			logger.write("Il menu <Account> è visualizzato - COMPLETED");
			
			logger.write("Verificare la presenza e comparazione dei testi 'Dati e contatti' nel Account");
			Thread.sleep(20000);
			//paca.verifyAccountText();
			logger.write("Comparazione del titolo e del testo nella sezione Account - COMPLETED");
			
			// 5
			logger.write("Verificare che il pop-up 'Modifica dati' è visualizzato");	
			paca.verifyComponentExistence(paca.datiContattiPopupBtn);
			paca.clickComponent(paca.datiContattiPopupBtn);
			logger.write("Premere il pulsante 'Modifica dati' - COMPLETED");
			
			logger.write("Verificare la presenza e comparazione dei testi nel pop-up");
			paca.verifyPopupText();
			logger.write("Comparazione dei testi e titoli in 'Modifica dati' - COMPLETED");
			
			logger.write("Prova a chiudere il popup - START");
			paca.clickComponentIfExist(paca.datiContattiPopupCloseBtn);
			logger.write("Apertura popup - COMPLETED");
			
			// 6
			logger.write("Verificare la visibilita e integrita dei valori della pagina 'Dati e contatti'");
			//paca.checkContactFields();
			logger.write("I campi/dati del modulo 'Dati e contatti' sono visibili e corretti - COMPLETED");
			
			// 7
			logger.write("Premere sul pulsante 'Modifica Contatti' e verificare i testi");
			paca.clickComponent(paca.datiContattiModificaBtn);
			Thread.currentThread().sleep(5000);
			//paca.compareText("Dati e Contatti", paca.datiContattiModificaTitle, true);
			//paca.compareText("Consulta e aggiorna i tuoi dati di contatto", paca.datiContattiModificaText, true);
			paca.compareText(paca.datiEContattiHeader1, paca.datiEContattiHeader1Text, true);
			paca.compareText(paca.datiEContattiHeader2, paca.datiEContattiHeader2Text, true);
			logger.write("La pagina 'Modifica contatti' è aperta e i testi sono corretti");
			
			// 8
			logger.write("Verificare i campi Titolare, Codice fiscale non sono editabili.");
			paca.verifyDisabledFields();
			logger.write("I campi Titolare, Codice fiscale non sono editabili - COMPLETED");
			
			Thread.currentThread().sleep(5000);
			
			logger.write("Verificare i valori dei campi editabili della pagina 'Modifica contatti'");
//			paca.checkHolderFields();
			paca.checkCanaleContattoPreferitoFieldSet();
			logger.write("I valori dei campi 'Modifica contatti' sono corretti- COMPLETED");
			
			logger.write("Verificare la presenza dei campi Indietro e 'Salve modifiche' e i loro valori");
			paca.checkReturnAndSaveButtons();
			logger.write("I campi Indietro e 'Salve modifiche' sono visibili e corretti - COMPLETED");
			
			// 9
			logger.write("Pulizia del campo <Cellulare> e visualizzazione del messaggio di errore");
			paca.eliminaCell();
			logger.write("Il messaggio di errore è visualizzato - COMPLETED");
			
			// 10
			logger.write("Pulizia del campo <Email> e visualizzazione del messaggio di errore");
			paca.eliminaEmail();
			logger.write("Il messaggio di errore è visualizzato - COMPLETED");
			
			// 11
			logger.write("Inserimento di un indirizzo e-mail invalido");
//			paca.insertWrongEmail();
			logger.write("Il messaggio di errore è visualizzato - COMPLETED");
			
			// 12
			// paca.insertWrongEmailAndCell();
			
			// 13
			logger.write("Inserimento di un indirizzo PEC invalido");
			paca.insertWrongPEC();
			logger.write("Il messaggio di errore è visualizzato - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
			
		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
}
