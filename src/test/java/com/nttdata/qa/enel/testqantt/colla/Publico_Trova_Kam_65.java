package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Publico_Trova_Kam_65 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ImpressComponent ic = new ImpressComponent(driver);
			
			logger.write("Accessing login page - Start");
			ic.launchLink(prop.getProperty("LINK"));
			logger.write("Accessing login page - Complete");
			
			logger.write("Closing the alert close and accept button - Start");		
			ic.hanldeFullscreenMessage(ic.homeFullscreenAlertCloseButton);
			ic.verifyComponentExistence(ic.logoEnel);
			
						
			ic.verifyComponentExistence(ic.buttonAccetta);
			ic.clickComponent(ic.buttonAccetta);
			logger.write("Closing the alert close and accept button - Complete");
			
			logger.write("Verify the header and click on Impress tab - Start");
			ic.verifyComponentExistence(ic.headerImpress);
			ic.clickComponent(ic.headerImpress);
			logger.write("Verify the header and click on Impress tab - Start");
			
			logger.write("Verify the url - Start");
			ic.checkUrl(prop.getProperty("URL"));
			logger.write("Verify the url - Complete");
			
			Thread.sleep(5000);
			logger.write("Verify the contents in Impress page - Start");
			ic.comprareText(ic.impressHome, ImpressComponent.IMPRESS_HOME, true);
			//ic.comprareText(ic.impressHeader, ImpressComponent.IMPRESS_HEADER, true);
			//ic.comprareText(ic.impressHeader1, ImpressComponent.IMPRESS_HEADER1, true);
			logger.write("Verify the contents in Impress page - Complete");
			
			logger.write("Click on grandiAzendi link - Start");
			ic.pressJavascript(ic.grandiAzendi);
			logger.write("Click on grandiAzendi link - Complete");
			
			logger.write("Switch to New window - Start");
			ic.SwitchToWindow();
			logger.write("Switch to New window - Complete");
			Thread.sleep(3000);
			
			logger.write("Verify the New window URL - Start");
			ic.checkUrl(prop.getProperty("URL2"));
			logger.write("Verify the New window URL - Complete");
			
			logger.write("Verify the Grandi page contents  - Start");
			//ic.comprareText(ic.grandiHome, ImpressComponent.GRANDI_HOME, true);
			ic.comprareText(ic.grandiHeader1, ImpressComponent.GRANDI_HEADER1, true);
			ic.comprareText(ic.grandiHeader2, ImpressComponent.GRANDI_HEADER2, true);
			//ic.comprareText(ic.grandiContent1, ImpressComponent.GRANDI_CONTENT1, true);
			ic.verifyComponentExistence(ic.grandiContent2);
			ic.comprareText(ic.grandiContent2, ImpressComponent.GRANDI_CONTENT2, true);
			ic.verifyComponentExistence(ic.grandiContent3);
			ic.comprareText(ic.grandiContent3, ImpressComponent.GRANDI_CONTENT3, true);
			ic.verifyComponentExistence(ic.grandiContent4);
			ic.comprareText(ic.grandiContent4, ImpressComponent.GRANDI_HEADER4, true);
			logger.write("Verify the Grandi page contents  - Complete");
			
			//Header display
			logger.write("Verify the header tab  - Start");
			ic.verifyHeaderVisibility();
			logger.write("Verify the header tab  - Complete");
			
			//footer display
			logger.write("Verify the footer tab  - Start");
			ic.verifyFooterVisibility();
			logger.write("Verify the footer tab  - Complete");
			
			logger.write("Click on Cerca button with empty input  - Start");
			ic.verifyComponentExistence(ic.cerca_btn);
			ic.clickComponent(ic.cerca_btn);
			logger.write("Click on Cerca button with empty input  - Complete");
			
			logger.write("Verify the error message - Start");
			ic.verifyComponentExistence(ic.errorMsgNoInput);
			ic.verifyComponentText(ic.errorMsgNoInput, prop.getProperty("ERROR_NOINPUT"));
			logger.write("Verify the error message - Complete");
			
			logger.write("Enter the invalid input and click on Cerca button - Start");
			ic.verifyComponentExistence(ic.inputValue);
			ic.enterInput(ic.inputValue, prop.getProperty("INPUT"));
			ic.clickComponent(ic.cerca_btn);
			logger.write("Enter the invalid input and click on Cerca button - Complete");
			
			logger.write("Verifyt the error message for invalid input - Start");
			ic.verifyText(ic.errorMsgInvalid, ImpressComponent.ERRORMSG_INVALIDINPUT);
			logger.write("Verifyt the error message for invalid input - Start");
			
			logger.write("Clear the input value and enter valid input - Start");
			ic.clearText(ic.inputValue);
			ic.enterInput(ic.inputValue, prop.getProperty("INPUTVALID"));
			logger.write("Clear the input value and enter valid input - Complete");
			
			logger.write("Click on cerca button - Start");
			ic.clickComponent(ic.cerca_btn);
			Thread.sleep(3000);
			logger.write("Click on cerca button - Complete");
			
			logger.write("Verify the messsage and content for valid input - Start");
			ic.verifyComponentExistence(ic.msgFederica);
			ic.comprareText(ic.msgFederica, ImpressComponent.MSG_FEDRICA, true);;
			ic.verifyComponentExistence(ic.msgEmail);
			ic.comprareText(ic.msgEmail, ImpressComponent.MSG_EMAIL, true);		
			logger.write("Verify the messsage and content for valid input - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
