package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_HomePage_ACB_ID_246 {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			
			logger.write("Launch the page and verify the fields and button  - Start");
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
			logger.write("Launch the page and verify the fields and button  - Complete");
		
								
			PrivateAreaDisattivazioneFornituraComponent  dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
						
				
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			logger.write("Verify the BSN header and content - Start");			
			dfc.verifyComponentExistence(dfc.homePageBSNPath);
			dfc.comprareText(dfc.homePageBSNPath, PrivateAreaDisattivazioneFornituraComponent.HOMEPAGE_BSNPATH, true);
			dfc.verifyComponentExistence(dfc.headerBSN);
			dfc.comprareText(dfc.headerBSN, PrivateAreaDisattivazioneFornituraComponent.HEADER_BSN, true);
			dfc.verifyComponentExistence(dfc.fornitureHeaderBSN);
			dfc.comprareText(dfc.fornitureHeaderBSN, PrivateAreaDisattivazioneFornituraComponent.FORNITUREHEADER_BSN, true);
			logger.write("Verify the BSN header and content - Complete");	
			
			
			logger.write("Verify the field and values - Start");
			dfc.verifyComponentExistence(dfc.LugoValue_3430);
			dfc.comprareText(dfc.LugoValue_3430, PrivateAreaDisattivazioneFornituraComponent.LUGOVALUE_3430, true);
			dfc.verifyComponentExistence(dfc.offerta_Sempre);
			dfc.comprareText(dfc.offerta_Sempre, PrivateAreaDisattivazioneFornituraComponent.OFFERTA_SEMPRE, true);
			dfc.verifyComponentExistence(dfc.statoValue);
			dfc.comprareText(dfc.statoValue, PrivateAreaDisattivazioneFornituraComponent.STATO_VALUE, true);
			logger.write("Verify the field and values - Complete");
			
			logger.write("Click on PIU information link - Start");
			dfc.verifyComponentExistence(dfc.piuInformationLink);
			dfc.clickComponent(dfc.piuInformationLink);
			logger.write("Click on PIU information link - complete");
			
			logger.write("Verify the Tutto Header and contents - Start");
			dfc.verifyComponentExistence(dfc.tuttoHeader);
			dfc.comprareText(dfc.tuttoHeader, PrivateAreaDisattivazioneFornituraComponent.TUTTO_HEADER, true);
			dfc.verifyComponentExistence(dfc.tuttoContent);
			dfc.comprareText(dfc.tuttoContent, PrivateAreaDisattivazioneFornituraComponent.TUTTO_CONTENT, true);
			logger.write("Verify the Tutto Header and contents - Complte");
			
			logger.write("Verify the Numero and Fornitura  - Start");
			dfc.verifyComponentExistence(dfc.fornituraLuce_300232254);
			dfc.comprareText(dfc.fornituraLuce_300232254, PrivateAreaDisattivazioneFornituraComponent.FORNITURALUCE_300232254, true);
			logger.write("Verify the Numero and Fornitura  - Complete");
			
			logger.write("Click on browser back button - Start");
			dfc.backBrowser(dfc.homePageBSNPath);
			logger.write("Click on browser back button  - Start");
			
			logger.write("Verify the Imprese page  - Start");
			prop.setProperty("IMPRESE","https://www-coll1.enel.it/it/area-clienti/imprese");
			dfc.checkUrl(prop.getProperty("IMPRESE"));
			logger.write("Verify the Imprese page  - Complete ");
			
			logger.write("Verify the Bollette page and its columns  - Start");
			dfc.verifyComponentExistence(dfc.bolleHeader);
			dfc.comprareText(dfc.bolleHeader, PrivateAreaDisattivazioneFornituraComponent.LETUE_BOLLETTE, true);
			dfc.verifyBolletteBillColumn(dfc.bolletteBillColumns);
			logger.write("Verify the Bollette page and its columns  - Complete");
			
			logger.write("Click on Vai All Bollette link  - Start");
			dfc.verifyComponentExistence(dfc.vaiAllBollette);
			dfc.clickComponent(dfc.vaiAllBollette);
			logger.write("Click on Vai All Bollette link  - Complete");
			
			logger.write("Verify the Bollette header and contents  - Start");
			dfc.verifyComponentExistence(dfc.letueBolletteHeader);
			dfc.comprareText(dfc.letueBolletteHeader, PrivateAreaDisattivazioneFornituraComponent.LETUEBOLLETTEHEADER, true);
			dfc.verifyComponentExistence(dfc.letueBolletteContent);
			dfc.comprareText(dfc.letueBolletteContent, PrivateAreaDisattivazioneFornituraComponent.LETUEBOLLETTECONTENT, true);
			logger.write("Verify the Bollette header and contents  - Complete");
			
			logger.write("Verify the Mostra filtri and Esporta excel buttons  - Start");
			dfc.verifyComponentExistence(dfc.mostraFiltriButton);
			dfc.verifyComponentExistence(dfc.esportaExcelButton);
			logger.write("Verify the Mostra filtri and Esporta excel buttons  - Complete");
			
			logger.write("Click on browser back button  - Start");
			dfc.backBrowser(dfc.homePageBSNPath);
			logger.write("Click on browser back button  - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	

}
