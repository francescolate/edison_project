package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AttivaBollettaWebComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID321Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID322 {

public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
	
		prop.setProperty("LOGIN_PAGE_TITLE", "Accedi a MyEnel");
		prop.setProperty("HOMEPAGE_HEADING", "Benvenuto nella tua area privata");
		prop.setProperty("HOMEPAGE_SUBHEADING", "In questa sezione potrai gestire le tue forniture");
		prop.setProperty("SECTION_TITLE", "Le tue forniture");
		prop.setProperty("SECTION_TITLE_SUBTEXT", "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.");
		
		PrivatoBollettaWebID321Component bol = new PrivatoBollettaWebID321Component(driver);
		AttivaBollettaWebComponent abw = new AttivaBollettaWebComponent(driver);
		
//		if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
//            bol.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
		
		logger.write("User shoulf be logged in successfully - Start");
		logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
		By homePage = bol.homePageCentralText;
		bol.verifyComponentExistence(homePage);
		bol.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
					
		By homepageSubheading = bol.homePageCentralSubText;
		bol.verifyComponentExistence(homepageSubheading);
		bol.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
		logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
		logger.write("User shoulf be logged in successfully - Completed");
		
		logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
		By sectionTitle = bol.sectionTitle;
		bol.verifyComponentExistence(sectionTitle);
		bol.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
		
		By sectionTitleSubText = bol.sectionTitleSubText;
		bol.verifyComponentExistence(sectionTitleSubText);
		bol.VerifyText(sectionTitleSubText, prop.getProperty("SECTION_TITLE_SUBTEXT"));
		logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
		
		logger.write("Click on 'serviziSelect' menu item - Start");
		By servizi = bol.serviziSelect;
		bol.verifyComponentExistence(servizi);
		bol.clickComponent(servizi);
		logger.write("Click on 'serviziSelect' menu item - Completed");
		Thread.sleep(5000);
		
		prop.setProperty("SERVIZI_HOMEPAGE", "Servizi per le forniture");
		prop.setProperty("SERVIZI_HOMEPAGE_SECTIONTITLE", "Servizi per le bollette");
		
		bol.verifyComponentVisibility(bol.serviziPage);
		
		logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
		By serviziHomePage = bol.serviziPage;
		bol.verifyComponentExistence(serviziHomePage);
		bol.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
		logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
		
		prop.setProperty("SERVIZI_SECTION_TITLE", "Accedi a MyEnel");
		
		logger.write("Validate for serviziSectionTitle and click on BollettaWebTile - Start");
		By serviziSectionTitle = bol.serviziSectionTitle;
		bol.verifyComponentExistence(serviziSectionTitle);
		bol.VerifyText(serviziSectionTitle, prop.getProperty("SERVIZI_HOMEPAGE_SECTIONTITLE"));
		
		By BollettaWebTile = bol.BollettaWebTile;
		bol.verifyComponentExistence(BollettaWebTile);
		bol.clickComponent(BollettaWebTile);
		logger.write("Validate for serviziSectionTitle and click on BollettaWebTile - Completed");
		Thread.sleep(5000);
		
		prop.setProperty("BollettaWebPage_Title", "Bolletta Web");
		logger.write("Validate for BollettaWebPageTitle and click on modificaBollettaWeb_button - Start");
		By BollettaWebPageTitle = bol.BollettaWebPageTitle;
		bol.verifyComponentExistence(BollettaWebPageTitle);
		bol.VerifyText(BollettaWebPageTitle, prop.getProperty("BollettaWebPage_Title"));
		
		logger.write("Check that the system displays the active supply with the web bill service- Started");
		abw.verifyComponentExistence(abw.bollettaWebactivesupply);
		abw.compareText(abw.bollettaWebactivesupply, AttivaBollettaWebComponent.AttivoForText, true);
		abw.compareText(abw.indirizzoLabel, AttivaBollettaWebComponent.IndirizzoLabelText, true);
		abw.compareText(abw.avvivaLaRichiesta, AttivaBollettaWebComponent.AvviaLaRichiesta, true);
		logger.write("Check that the system displays the active supply with the web bill service- Started");
		
		logger.write("Click on Avvia La Richiesta- Started");
		abw.clickComponent(abw.avvivaLaRichiestaLink);
		logger.write("Click on Avvia La Richiesta- Completed");
		
		logger.write("Select the supply through the checkbox on the left side of the record- Satrted");
		abw.clickComponent(abw.attivoSupplyCheckbox);
		logger.write("Select the supply through the checkbox on the left side of the record- Completed");
		
		logger.write("Click on Disattiva Bolletta Button - Started");
		abw.clickComponent(abw.disattivaBollettaWebButton);
		logger.write("Click on Disattiva Bolletta Button - Started");
		
		logger.write("Verify system navigates the view Riepilogo e Conferma Dati- Started");
		abw.compareText(abw.confermaDati, AttivaBollettaWebComponent.CONFERMA_DATI, true);
		abw.compareText(abw.confermaDatiText, AttivaBollettaWebComponent.CONFERMA_DATI_TEXT, true);
		logger.write("Verify system navigates the view Riepilogo e Conferma Dati- Completed");
		
		logger.write("Click on Indietro Button- Started");
		abw.verifyComponentExistence(abw.indietroButtonn);
		abw.clickComponent(abw.indietroButtonn);
		logger.write("Click on Indietro Button- Completed");
		
		logger.write("Check that the system displays the active supply with the web bill service- Started");
		abw.verifyComponentExistence(abw.attivoSupplyCheckbox);
		abw.clickComponent(abw.disattivaBollettaWebButton);
		logger.write("Check that the system displays the active supply with the web bill service- Started");
		
		logger.write("Click on DisattivaBollettaWeb button- Started");
		abw.clickComponent(abw.confermaButtonn);
		logger.write("Click on DisattivaBollettaWeb button- Completed");
		
		logger.write("Verify system navigates the view Riepilogo e Conferma Dati- Started");
		abw.compareText(abw.confermaDati, AttivaBollettaWebComponent.CONFERMA_DATI, true);
//		abw.compareText(abw.confermaDatiText, AttivaBollettaWebComponent.CONFERMA_DATI_TEXT, true);
		logger.write("Verify system navigates the view Riepilogo e Conferma Dati- Completed");
		
		prop.setProperty("RETURN_VALUE", "OK");
	
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	


}
