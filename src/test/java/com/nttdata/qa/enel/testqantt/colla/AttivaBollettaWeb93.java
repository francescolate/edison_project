package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.AttivaBollettaWebComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AttivaBollettaWeb93 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			AttivaBollettaWebComponent abwc = new AttivaBollettaWebComponent(driver);
			
			logger.write("controllo e scrollo alla sezione 'le tue attività' - START");
			abwc.verifyComponentExistence(abwc.leTueAttivitaHeader);
			abwc.scrollComponent(abwc.leTueAttivitaHeader);
			logger.write("controllo e scrollo alla sezione 'le tue attività' - COMPLETED");
			
			logger.write("clicco su pulsante Attiva ora la tua bolletta web - START");
			abwc.verifyComponentExistence(abwc.attivaLaTuaBollettaWebButton);
			abwc.clickComponent(abwc.attivaLaTuaBollettaWebButton);
			logger.write("clicco su pulsante Attiva ora la tua bolletta web - COMPLETED");
			
			logger.write("cliccando su Attiva Bolletta Web - START");
			abwc.verifyComponentExistence(abwc.attivaBollettaWebButton);
			abwc.clickComponent(abwc.attivaBollettaWebButton);
			logger.write("cliccando su Attiva Bolletta Web - COMPLETED");
			
			Thread.sleep(15000);
			
			logger.write("scegliendo la fornitura per il caso di test - START");
			abwc.verifyComponentExistence(abwc.fornituraCheckBox);
			prop.setProperty("CUSTOMER_NUMBER", abwc.jsGetCustomerNumber(abwc.numeroCliente));
			prop.store(new FileOutputStream(args[0]), null);
			abwc.jsClickObject(abwc.fornituraCheckBox);
			logger.write("scegliendo la fornitura per il caso di test - COMPLETED");
			
			logger.write("cliccando sul pulsante Continua - START");
			abwc.verifyComponentExistence(abwc.continuaButton);
			abwc.clickComponent(abwc.continuaButton);
			logger.write("cliccando sul pulsante Continua - COMPLETED");
			
			logger.write("cliccando sul pulsante Indietro - START");
			abwc.verifyComponentExistence(abwc.indietroButton);
			abwc.clickComponent(abwc.indietroButton);
			logger.write("cliccando sul pulsante Indietro - COMPLETED");
			
			logger.write("verificando che si è ritornati alla pagina precedente, per poi proseguire nuovamente - START");
			abwc.verifyComponentExistence(abwc.esciButton);
			abwc.verifyComponentExistence(abwc.continuaButton);
			abwc.jsClickObject(abwc.continuaButton);
			logger.write("verificando che si è ritornati alla pagina precedente, per poi proseguire nuovamente - COMPLETED");
			
			Thread.sleep(5000);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			//Thread.sleep(500);
			//Robot robot = new Robot(); 
			//robot.keyPress(KeyEvent.VK_M);
			Thread.sleep(100);
			//abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(100);
			abwc.clickComponent(abwc.aggiornaIMieiDatiAnagrafici);
			Thread.sleep(100);
			abwc.clickComponent(abwc.continuaSecondButton);
			Thread.sleep(100);
			abwc.clickComponent(abwc.indietroButton);
			Thread.sleep(100);
			abwc.clickComponent(abwc.continuaSecondButton);
			
			abwc.compareText(By.xpath(abwc.verificaCorrettezzaEmail.replace("$email$", prop.getProperty("WP_USERNAME"))), prop.getProperty("WP_USERNAME"), true);
			abwc.compareText(By.xpath(abwc.verificaCorrettezzaMobile.replace("$mobile$", prop.getProperty("MOBILE_NUMBER"))), prop.getProperty("MOBILE_NUMBER"), true);
			abwc.compareText(By.xpath(abwc.verificaCorrettezzaNumeroCliente.replace("$numeroCliente$", prop.getProperty("CUSTOMER_NUMBER"))), prop.getProperty("CUSTOMER_NUMBER"), true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
