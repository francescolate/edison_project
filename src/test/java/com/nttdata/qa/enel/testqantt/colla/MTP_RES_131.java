package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_131 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
		    prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
			prop.setProperty("BILLSERVICES", "Bolletta Web Pagamento Online Addebito Diretto Rettifica Bolletta Rateizzazione Bolletta di Sintesi o di Dettaglio Duplicato Bolletta");
			prop.setProperty("CONTRACTSERVICES", " Invio Documenti |Disattivazione Fornitura");
			prop.setProperty("FAQ", "Cosa si intende per potenza e tensione? Quando è necessario modificare la potenza? Quali sono i tempi per l’esecuzione della modifica di potenza o tensione? Cosa si intende per sopralluogo tecnico? Quali sono i costi dell’operazione?"
					+ "Come posso pagare il preventivo?");
			prop.setProperty("PVVALUE", "3 kW / 220 V");
			prop.setProperty("MENUITEM", "Inserimento dati Preventivo Esito");		
			prop.setProperty("PLACEHOLDER", "Inserisci il valore in Ampere");
			prop.setProperty("WARNING", "Attenzione La potenza inserita non è sufficiente per il sollevamento");
			prop.setProperty("POWERDEFAULT", "3 kW(potenza attuale)");
			prop.setProperty("VOLTAGEFAULT", "220 V(tensione attuale)");
			prop.setProperty("WARNING", "Attenzione La potenza consigliata è di 1.5 kW. Ti consigliamo di consultare la scheda tecnica relativa al tuo apparato di sollevamento per verificare la tensione necessaria per il corretto funzionamento.");
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));

			home.clickComponent(home.services);
			
			ServicesComponent services = new ServicesComponent(driver);
			
			logger.write("Check For supply services-- start");
			
			services.checkForSupplyServices(prop.getProperty("SUPPLYSERVICES"));
			
			logger.write("Check For supply services-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");
			
			Thread.sleep(10000);
			
			services.clickComponent(services.modificaPotenzaeoTensione);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Check Power And Voltage Content-- Start");

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageContent);
			
			changePV.checkForPageContent();
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageButton);
			
			logger.write("Check Power And Voltage Content-- Start");
			
			logger.write("Check Faq -- Start");
			
			changePV.checkForFaqQst(prop.getProperty("FAQ"));
			
			logger.write("Check Faq -- Completed");
			
			logger.write("Check Faq plus Icon -- Start");
			
			changePV.checkForPlusIcon();
			
			logger.write("Check Faq plus Icon -- Completed");

			logger.write("Click on button Change Power And Voltage Button -- Start");

			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText);

			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");
			
			logger.write("Check for Exit And Change Power Voltage Button -- Start");
			
			changePV.verifyComponentExistence(changePV.exitButton);
			
			changePV.verifyComponentExistence(changePV.changePowerOrVoltageButton);

			logger.write("Check for Exit And Change Power Voltage Button -- Completed");
			
			logger.write("Check for Exit And Change Power Voltage Button Status-- Start");

			changePV.checkForExitButtonStatus(changePV.exitButton);
			
			changePV.checkForChangePowerOrVoltageButtonStatus(changePV.changePowerOrVoltageButton);
			
			logger.write("Check for Exit And Change Power Voltage Button Status-- Completed");
			
			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");
			
			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");

			changePV.checkForChangePowerOrVoltageButtonStatus(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
			logger.write("Check for Change Power and voltage title and sub text-- Start");

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);
			
			changePV.verifyComponentExistence(changePV.titleSubText);
			
			logger.write("Check for Change Power and voltage title and sub text-- Completed");
			
			logger.write("Check for Menu Item -- Start");

			changePV.checkForMenuItem(prop.getProperty("MENUITEM"));
			
			logger.write("Check for Menu Item -- Completed");
			
			logger.write("Check for Power Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.powerLable);
			
			logger.write("Check for Power Lable -- Completed");
			
			logger.write("Check for Default Power value -- Start");
			
			changePV.checkForDefaultValue(changePV.powerValue,prop.getProperty("POWERDEFAULT"));
			
			logger.write("Check for Default Power value -- Completed");

			logger.write("Check for Drop down Power value -- Start");

			changePV.checkForPowerDropDownValue();
			
			logger.write("Check for Drop down Power value -- Completed");
			
			logger.write("Check for Voltage Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.voltageLable);
			
			logger.write("Check for Voltage Lable -- Completed");
			
			logger.write("Check for Default Voltage value -- Start");
			
			changePV.checkForDefaultValue(changePV.voltageValue,prop.getProperty("VOLTAGEFAULT"));
			
			logger.write("Check for Default Voltage value -- Completed");

			logger.write("Check for Drop down Voltage value -- Start");

			changePV.checkForVoltageDropDownValue();
			
			logger.write("Check for Drop down Voltage value -- Completed");
			
			changePV.verifyComponentExistence(changePV.formLine2);
			
			logger.write("Check for radio button yes and no -- Start");
			
			changePV.verifyComponentExistence(changePV.radioNo);
			
			changePV.verifyComponentExistence(changePV.radioYes);
			
			logger.write("Check for radio button yes and no -- Completed");
			
			logger.write("Check for back button  -- Start");
			
			changePV.verifyComponentExistence(changePV.backButton);
			
			logger.write("Check for back button  -- Completed");

			logger.write("Check for Calculate quote button  -- Start");
			
			changePV.verifyComponentExistence(changePV.calculateQuote);
			
			logger.write("Check for Calculate quote button  -- Completed");
			
			logger.write("Click on radio button Yes  -- Start");
			
			changePV.clickComponent(changePV.radioYes);
			
			logger.write("Click on radio button Yes  -- Completed");
			
			logger.write("Check for lift type lable  -- Start");

			changePV.verifyComponentExistence(changePV.liftTypeLabel);
			
			logger.write("Check for lift type lable  -- Completed");

			changePV.verifyComponentExistence(changePV.runningCurrentLable);
			
			changePV.verifyComponentExistence(changePV.suggestedPowerLable);

			logger.write("Check for Placeholder text  -- Start");

			changePV.checkForPlaceholderText(changePV.runningCurrentInput,prop.getProperty("PLACEHOLDER"));
			
			logger.write("Check for Placeholder text  -- Completed");
			
			changePV.checkSuggestPowerButtonStatus();
			
			logger.write("Click on i icon  -- Start");
			
			changePV.clickComponent(changePV.iIcon);
			
			logger.write("Click on i icon  -- Completed");
			
			changePV.checkForPopup();

			logger.write("Click on pop up close  -- Start");
			
			changePV.clickComponent(changePV.popupClose);

			logger.write("Click on pop up close  -- Completed");
			
			changePV.verifyComponentExistence(changePV.formLine1);
			
			changePV.checkForLiftTypeDropDownValue();
			
			logger.write("Check For running current label  -- Start");
			
			changePV.verifyComponentExistence(changePV.runningCurrentLable);
			
			logger.write("Check For running current label  -- Completed");
			
			logger.write("Check For running current placeholder text  -- Start");

			changePV.checkForPlaceholderText(changePV.runningCurrentInput, prop.getProperty("PLACEHOLDER"));

			logger.write("Check For running current placeholder text  -- Completed");

			changePV.changeDropDownValue(changePV.liftTypeDropdown,prop.getProperty("ARGANO"));
			
			logger.write("Check For starting current label  -- Start");
			
			changePV.verifyComponentExistence(changePV.startingCurrentLable);
			
			logger.write("Check For starting current label  -- Completed");
			
			logger.write("Check For starting current Placeholder text  -- Start");

			changePV.checkForPlaceholderText(changePV.startingCurrentInput, prop.getProperty("PLACEHOLDER"));
			
			logger.write("Check For starting current Placeholder text  -- Completed");

			logger.write("Change lift type drop down to Oleodinamico  -- Start");

			changePV.changeDropDownValue(changePV.liftTypeDropdown,prop.getProperty("OLEODINAMICO"));
			
			logger.write("Change lift type drop down to Oleodinamico  -- Completed");

			logger.write("Check for operating current field  -- Start");

			changePV.verifyComponentExistence(changePV.runningCurrentLable);
			
			logger.write("Check for operating current field  -- Completed");
			
			changePV.checkForPlaceholderText(changePV.runningCurrentInput, prop.getProperty("PLACEHOLDER"));
						
			logger.write("Check for operating current field placeholder text  -- Start");

			changePV.enterInputtoField(changePV.runningCurrentInput, prop.getProperty("OPERATINGCURRENTINPUT"));
			
			logger.write("Check for operating current field placeholder text  -- Completed");
			
			changePV.clickComponent(changePV.email);
			
			changePV.checkForHydraulicWarning(prop.getProperty("WARNING"));
			
			logger.write("Check For Suggested Power Value  -- Start");
			
			changePV.checkSuggestedPowerValue(changePV.suggestedPowerFieldString, prop.getProperty("SUGGESTEDPOWER"));

			logger.write("Check For Suggested Power Value  -- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
