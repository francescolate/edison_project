package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.VolturaConAccolloEVO_Id_4;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetPropertyVolturaConAccolloEVONOME {
    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y"))
            {
                prop.setProperty("NOME", prop.getProperty("QUERY_ACCOUNT.RECORDTYPE.NAME"));
                prop.setProperty("CODICE_FISCALE_CL_USCENTE", prop.getProperty("ACCOUNT.NE__FISCAL_CODE__C"));
                prop.setProperty("POD", prop.getProperty("NE__SERVICE_POINT__C.ITA_IFM_POD__C"));
                
                //per voltura con accollo 4 è necessario settare anche la denominazione cliente
                if(args[0].contentEquals(VolturaConAccolloEVO_Id_4.class.getSimpleName()+".properties"))
                {
                	prop.setProperty("CL_USCENTE_DENOMINAZIONE", prop.getProperty("ACCOUNT.NAME"));
                }

            }
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
