package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SFDCBoxSubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class CreaOffertaSubentroId19 {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);
        RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
        GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
        SFDCBoxSubentroComponent boxSubentro = new SFDCBoxSubentroComponent(driver);

        try {

           //BOX Riepilogo offerta
            int boxStepId = 0;
            String boxErrorMsg = null;
            if (prop.getProperty("CONTAINER_RIEPILOGO_OFFERTE").contains("Y")) {
                try {
                    System.out.println("BOX Riepilogo offerta");
                    //STEP 1 CHECK BOX EXISTENCE
                    boxStepId++;
                    boxErrorMsg = "CHECK BOX EXISTENCE";
                    By container_rieoff = boxSubentro.container_rieoff;
                    boxSubentro.verifyComponentExistence(container_rieoff);

                    //STEP 2 CHECK BOX ERROR
                    boxStepId++;
                    boxErrorMsg = "CHECK BOX ERROR";
                    By rieoff_error_banner = boxSubentro.rieoff_error_banner;
                    boxSubentro.verifyComponentInvisibility(rieoff_error_banner);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    throw new Exception("Error in compiling Container Riepilogo offerta at step N : " + boxStepId + " , Msg : " + boxErrorMsg, e.fillInStackTrace());
                }
            }

            //BOX Seleziona Uso Forniture
            System.out.println("BOX Seleziona Uso Forniture");

            By container_suf = boxSubentro.container_suf;
            boxSubentro.verifyComponentExistence(container_suf);

            By suf_pickuplist_uf = boxSubentro.suf_pickuplist_uf;
            boxSubentro.verifyComponentExistence(suf_pickuplist_uf);
            boxSubentro.clickAncestorClickable(boxSubentro.xpathToString(suf_pickuplist_uf), 7);

            By suf_pickuplist_uf_uda = boxSubentro.suf_pickuplist_uf_uda;
            boxSubentro.clickComponentIfExist(suf_pickuplist_uf_uda);

            By suf_button_conferma = boxSubentro.suf_button_conferma;
            boxSubentro.verifyComponentExistence(suf_button_conferma);
            boxSubentro.clickComponent(suf_button_conferma);


            //BOX Commodity
            System.out.println("BOX Commodity");

            By container_comm = boxSubentro.container_comm;
            boxSubentro.verifyComponentExistence(container_comm);

            By comm_radio_elettrico = boxSubentro.comm_radio_elettrico;
            boxSubentro.verifyComponentExistence(comm_radio_elettrico);
            boxSubentro.clickComponentIfExist(comm_radio_elettrico);

            By comm_pickuplist_cms = boxSubentro.comm_pickuplist_cms;
            boxSubentro.verifyComponentExistence(comm_pickuplist_cms);
            boxSubentro.clickComponentIfExist(comm_pickuplist_cms);

            By comm_pickuplist_cms_al_se = boxSubentro.comm_pickuplist_cms_al_se;
            boxSubentro.verifyComponentExistence(comm_pickuplist_cms_al_se);
            boxSubentro.clickComponentIfExist(comm_pickuplist_cms_al_se);

            By comm_pickuplist_residente = boxSubentro.comm_pickuplist_residente;
            boxSubentro.verifyComponentExistence(comm_pickuplist_residente);
            boxSubentro.verifyAttributeValue(comm_pickuplist_residente, "value", "NO");

            By comm_pickuplist_ofi = boxSubentro.comm_pickuplist_ofi;
            boxSubentro.verifyComponentExistence(comm_pickuplist_ofi);
            boxSubentro.clickComponentIfNotDisabled(comm_pickuplist_ofi);

            By comm_pickuplist_ofi_no = boxSubentro.comm_pickuplist_ofi_no;

            if (boxSubentro.checkDisabled(comm_pickuplist_ofi_no)) {
                boxSubentro.verifyComponentExistence(comm_pickuplist_ofi_no);
                boxSubentro.clickComponentIfExist(comm_pickuplist_ofi_no);
            }

            By comm_input_tdi_dist = boxSubentro.comm_input_tdi_dist;
            boxSubentro.verifyComponentExistence(comm_input_tdi_dist);
            boxSubentro.clickComponentIfExist(comm_input_tdi_dist);
            boxSubentro.clearAndSendKeys(comm_input_tdi_dist, prop.getProperty("TELEFONO_DISTRIBUTORE"));

            By comm_pickuplist_asc = boxSubentro.comm_pickuplist_asc;
            boxSubentro.verifyComponentExistence(comm_pickuplist_asc);
            boxSubentro.clickComponentIfExist(comm_pickuplist_asc);

            By comm_asc_pickuplist_no = boxSubentro.comm_asc_pickuplist_no;
            boxSubentro.verifyComponentExistence(comm_asc_pickuplist_no);
            boxSubentro.clickComponentIfExist(comm_asc_pickuplist_no);

            By comm_pickuplist_dis = boxSubentro.comm_pickuplist_dis;
            boxSubentro.verifyComponentExistence(comm_pickuplist_dis);
            boxSubentro.clickComponent(comm_pickuplist_dis);

            By comm_dis_pickuplist_si = boxSubentro.comm_dis_pickuplist_si;
            boxSubentro.verifyComponentExistence(comm_dis_pickuplist_si);
            boxSubentro.clickComponent(comm_dis_pickuplist_si);

            By comm_input_consumo_annuo = boxSubentro.comm_input_consumo_annuo;
            boxSubentro.verifyComponentExistence(comm_input_consumo_annuo);
            boxSubentro.clickComponent(comm_input_consumo_annuo);
            boxSubentro.clearAndSendKeys(comm_input_consumo_annuo, prop.getProperty("CONSUMO_ANNUO"));

            By comm_button_conferma_fornitura = boxSubentro.comm_button_conferma_fornitura;
            boxSubentro.verifyComponentExistence(comm_button_conferma_fornitura);
            boxSubentro.clickComponent(comm_button_conferma_fornitura);


            By comm_button_conferma = boxSubentro.comm_button_conferma;
            boxSubentro.verifyComponentExistence(comm_button_conferma);
            boxSubentro.clickComponent(comm_button_conferma);


            //BOX Indirizzo di Residenza/Sede Legale
            System.out.println("BOX Indirizzo di Residenza/Sede Legale");

            By container_irsl = boxSubentro.container_irsl;
            boxSubentro.verifyComponentExistence(container_irsl);

            By irsl_indirizzo_verificato = boxSubentro.irsl_indirizzo_verificato;
            try {
                boxSubentro.verifyComponentExistence(irsl_indirizzo_verificato);
            } catch (Exception e) {
                throw new Exception("Error : banner \"Indirizzo verificato\"  not present in container \"Indirizzo di Residenza/Sede Legale\" .");
            }

            By irsl_button_conferma = boxSubentro.irsl_button_conferma;
            boxSubentro.verifyComponentExistence(irsl_button_conferma);
            boxSubentro.clickComponent(irsl_button_conferma);


            //BOX INDIRIZZO DI FATTURAZIONE
            System.out.println("BOX INDIRIZZO DI FATTURAZIONE");
            By container_idf = boxSubentro.container_idf;
            By idf_banner_indirizzo = boxSubentro.idf_banner_indirizzo;
            By idf_button_conferma = boxSubentro.idf_button_conferma;
            By idf_input_regione_disabled = boxSubentro.idf_input_regione_disabled;
            By idf_input_cap_disabled = boxSubentro.idf_input_cap_disabled;
            By idf_input_provincia = boxSubentro.idf_input_provincia;
            By idf_input_comune = boxSubentro.idf_input_comune;
            By idf_input_localita = boxSubentro.idf_input_localita;
            By idf_input_indirizzo = boxSubentro.idf_input_indirizzo;
            By idf_input_ncivico = boxSubentro.idf_input_ncivico;
            By idf_input_cap = boxSubentro.idf_input_cap;
            By idf_button_verifica = boxSubentro.idf_button_verifica;
            By idf_button_forza = boxSubentro.idf_button_forza;
            By spanIndirizzoNonForzato = boxSubentro.spanForzaIndirizzo;
            By textVerifica = boxSubentro.textVerifica;
            By inputValueForCup = boxSubentro.inputValueForCup;
            boxSubentro.verifyComponentExistence(container_idf);
            if (!boxSubentro.checkDisabled(idf_input_regione_disabled)) {
                logger.write("Elemento : " + idf_input_regione_disabled + "non disabilitato");
                System.out.println("Elemento : " + idf_input_regione_disabled + "non disabilitato");
            }
            Thread.sleep(1000);
            boxSubentro.clearAndSendKeys(idf_input_provincia, "Lecce");
            Thread.sleep(1000);
            boxSubentro.clearAndSendKeys(idf_input_comune, "Lecce");
            Thread.sleep(1000);
            boxSubentro.verifyComponentExistence(idf_input_localita);
            Thread.sleep(1000);
            boxSubentro.clearAndSendKeys(idf_input_indirizzo, "via asdasda");
            Thread.sleep(1000);
            boxSubentro.clearAndSendKeys(idf_input_ncivico, "1");
            if (!boxSubentro.checkDisabled(idf_input_cap_disabled)) {
                logger.write("Elemento : " + idf_input_cap_disabled + "non disabilitato");
                System.out.println("Elemento : " + idf_input_cap_disabled + "non disabilitato");
            }
            Thread.sleep(5000);
            boxSubentro.clickComponentIfExist(idf_button_verifica);
            Thread.sleep(5000);
            boxSubentro.clickComponentIfExist(spanIndirizzoNonForzato);
            boxSubentro.clearAndSendKeys(idf_input_cap, "73100");
            boxSubentro.clickComponentIfExist(inputValueForCup);
            boxSubentro.clickComponentIfExist(idf_button_forza);
            boxSubentro.clickComponentIfExist(idf_button_conferma);


            //BOX FATTURAZIONE ELETTRONICA
            System.out.println("BOX FATTURAZIONE ELETTRONICA");
            By container_fe = boxSubentro.container_fe;
            boxSubentro.verifyComponentExistence(container_fe);
            By fe_input_cu = boxSubentro.fe_input_cu;

            if (!boxSubentro.checkDisabled(fe_input_cu)) {
                boxSubentro.clickComponentIfExist(fe_input_cu);
                boxSubentro.clearAndSendKeys(fe_input_cu, "0000");
                By fe_valore_corto = boxSubentro.fe_valore_corto;
                boxSubentro.verifyComponentExistence(fe_valore_corto);
                boxSubentro.clearAndSendKeys(fe_input_cu, "0000000");
            }

            By fe_input_data_inizio_validita_disabled = boxSubentro.fe_input_data_inizio_validita_disabled;
            boxSubentro.verifyComponentExistence(fe_input_data_inizio_validita_disabled);


            By fe_input_data_fine_validita = boxSubentro.fe_input_data_fine_validita;
            boxSubentro.verifyComponentExistence(fe_input_data_fine_validita);
            boxSubentro.verifyComponentExistence(fe_input_data_fine_validita);

            By fe_input_data_fine_validita_disabled = boxSubentro.fe_input_data_fine_validita_disabled;
            boxSubentro.verifyComponentExistence(fe_input_data_fine_validita_disabled);

            By fe_button_conferma = boxSubentro.fe_button_conferma;
            boxSubentro.verifyComponentExistence(fe_button_conferma);
            boxSubentro.clickComponent(fe_button_conferma);



            //BOX METODO DI PAGAMENTO
            System.out.println("BOX METODO DI PAGAMENTO");

            By container_mdp = boxSubentro.container_mdp;
            boxSubentro.verifyComponentExistence(container_mdp);

            By mdp_radio_bollettino_postale = boxSubentro.mdp_radio_bollettino_postale;
            boxSubentro.verifyComponentExistence(mdp_radio_bollettino_postale);
            boxSubentro.clickComponent(mdp_radio_bollettino_postale);

            By pop_up_button_ok = boxSubentro.pop_up_button_ok;
            boxSubentro.verifyComponentExistence(pop_up_button_ok);
            boxSubentro.clickComponent(pop_up_button_ok);

            By mdp_button_conferma = boxSubentro.mdp_button_conferma;
            boxSubentro.verifyComponentExistence(mdp_button_conferma);
            boxSubentro.clickComponent(mdp_button_conferma);



            //BOX SCONTI E BONUS
            System.out.println("BOX SCONTI E BONUS");

            By container_seb = boxSubentro.container_seb;
            boxSubentro.verifyComponentExistence(container_seb);

            By seb_input_codice_compagnia = boxSubentro.seb_input_codice_compagnia;
            boxSubentro.verifyComponentExistence(seb_input_codice_compagnia);

            By seb_button_conferma = boxSubentro.seb_button_conferma;
            boxSubentro.clickComponentIfExist(seb_button_conferma);


            //BOX Carrello
            System.out.println("Box Carrello");
            Thread.sleep(1000);
            ConfiguraProdottoElettricoNonResidenziale.main(args);


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            //			return;
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
