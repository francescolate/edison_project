package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNFornitureComponent;
import com.nttdata.qa.enel.components.colla.BolleteBSNComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HomePage_ACB_250 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			Thread.sleep(15000);
			pa.verifyComponentExistence(pa.fornitureebollette);
			pa.verifyComponentExistence(pa.latuaFornituranelDettaglio);
			logger.write("Verify default supply details -- Start");
			pa.checkDefaultSupply(pa.defaultSupply);
			pa.checkSupplyDetails(pa.supplies);
			logger.write("Verify default supply details -- Completed");
			pa.verifyChronologicalOrder(pa.bills);
			logger.write("Click on vaiAllElencoBollette,verify page navigation -- Start ");
			pa.clickComponent(pa.vaiAllElencoBollette1);
			
			BolleteBSNComponent bc = new BolleteBSNComponent(driver);
			bc.verifyComponentExistence(bc.pageTitle);
	//		bc.verifyComponentExistence(bc.Bollete);
			logger.write("Click on vaiAllElencoBollette,verify page navigation -- Completed ");

			logger.write("Verify table fields -- Start ");
			bc.comprareText(bc.tipo, bc.Tipo, true);
			bc.comprareText(bc.nBollete, bc.NBolletta, true);
			bc.comprareText(bc.scadenza, bc.Scadenza, true);
			bc.comprareText(bc.stato, bc.Stato, true);
	//		bc.comprareText(bc.indirizzoFornitura, bc.Indirizzo_fornitura, true);
			/*bc.clickComponent(bc.plusIcon);
			bc.verifyComponentExistence(bc.dettagliBolletta);
			logger.write("Verify table fields -- Completed ");
			logger.write("Verify invoice details -- Start");
			bc.verifyInvoiceField(bc.invoiceDetails,bc.InvoiceDetails);*/
			logger.write("Verify invoice details -- Completed");
			
			/*logger.write("Verify and click on home menu -- Start");
			bc.verifyComponentExistence(bc.homeMenu);
			bc.clickComponent(bc.homeMenu);
			logger.write("Verify and click on home menu -- Completed");
			logger.write("click on VaiAllelencoForniture -- Start");
			pa.verifyComponentExistence(pa.VaiAllelencoForniture);
			pa.clickComponent(pa.VaiAllelencoForniture);
			logger.write("click on VaiAllelencoForniture -- Completed");

			logger.write("Verify page navigation and table details -- Start");
			FornitureBSNComponent fc = new FornitureBSNComponent(driver);
			Thread.sleep(5000);
			fc.verifyComponentExistence(fc.pageTitle);
//			fc.comprareText(fc.tipo, fc.Tipo, true);
//			fc.comprareText(fc.stato, fc.Stato, true);
//			fc.comprareText(fc.numerocliente, fc.Numero_cliente, true);
//			fc.comprareText(fc.indirizzoFornitura, fc.Indirizzo_fornitura, true);
			logger.write("Verify page navigation and table details -- Completed");
*/
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
