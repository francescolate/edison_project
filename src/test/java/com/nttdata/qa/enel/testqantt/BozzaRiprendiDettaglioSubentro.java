package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class BozzaRiprendiDettaglioSubentro {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			BaseComponent base = new BaseComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			base.baseTimeoutInterval = 1;
			logger.write("Verificare che creata l'offerta, sia possibile salvare in bozza. - Start");


			try {
				By container_referente = gestione.container_referente;
				gestione.verifyComponentExistence(container_referente);
			} catch (Exception e) {
				logger.write("Referente non presente " + e.toString());
			}

			By popup_vbl_ko = gestione.popup_vbl_ko;
			gestione.verifyComponentInexistence(popup_vbl_ko);

			By radio_first_in_referente_table = gestione.radio_first_in_referente_table;
			gestione.clickComponentIfExist(radio_first_in_referente_table);

			By button_conferma_in_referente_container = gestione.buttonConfermaReferente;
			gestione.clickComponentIfExist(button_conferma_in_referente_container);

			gestione.verifyComponentExistence(gestione.sezioneSelezioneUsoFornitura);
			gestione.selezionaUsoFornitura("Uso Abitativo");

			By button_salva_in_bozza = gestione.button_salva_in_bozza;
			gestione.clickComponentIfExist(button_salva_in_bozza);

			By check_stato_by_text = By.xpath(gestione.check_stato_by_text.replaceFirst("##", "Bozza"));
			gestione.verifyComponentExistence(check_stato_by_text);

			By check_statodc_by_text = By.xpath(gestione.check_statodc_by_text.replaceFirst("##", "IN LAVORAZIONE"));
			gestione.verifyComponentExistence(check_statodc_by_text);

			By check_sstatodc_by_text = By
					.xpath(gestione.check_sstatodc_by_text.replaceFirst("##", "Offer - Null / Draft"));
			gestione.verifyComponentExistence(check_sstatodc_by_text);

			By button_ssdc_by_text = gestione.button_ssdc_by_text;
			gestione.clickComponentIfExist(button_ssdc_by_text);

			util.getFrameActive();
			logger.write("Verifica esito Offertabilita - Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			System.out.println("fine test");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//    return; 
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}