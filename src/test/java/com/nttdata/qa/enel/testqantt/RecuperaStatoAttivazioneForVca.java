package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
/**
 @COSA_FA recupera lo lo stato dell'order item di attivazione  in modo tale da poter verificare se eventuali scarti SAP e SEMPRE sono Forzabili/Rilavorabili
 @PROPERTY: OI_ITEM_ATTIVAZIONE,POD 
 
 */
public class RecuperaStatoAttivazioneForVca {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			logger.write("Recupero stato attivzione da Workbench - start");
			
			String oiAttivazione=prop.getProperty("OI_ITEM_ATTIVAZIONE");
			String pod=prop.getProperty("POD");

			
			Map<Integer,Map> queryResult=null;
			Map row=null;
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);

			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);


			

			String query=Costanti.query_VCA_OrderItemdaOrderItemId;;
			queryResult=wb.EseguiQuery(query.replace("@ORDER_ITEM_ID@", oiAttivazione).replace("@POD@", pod));
			row=queryResult.get(1);
			wb.SaveRowToProperties(row, prop);
			logger.write("Recupero stato attivzione da Workbench - end");



			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
