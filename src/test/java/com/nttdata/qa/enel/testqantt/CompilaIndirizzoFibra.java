package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ChiudiCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

/**
 * ***** Lista Properties 
 * @param INDIRIZZO_FIBRA_OK - Valorizzare a SI per quando il risultato atteso è la corretta validazione dell'indirizzo - NO, quando il risultato atteso è un indirizzo non coperto
 * @param PROVINCIA_FIBRA
 * @param INDIRIZZO_FIBRA
 * @param SCELTA_MELITA\n
 * @param CELLULARE\n
 * @param CODICE_MIGRAZIONE
 * @param TELEFONO_FIBRA
 * @param TELEFONO_FIBRA
 * @param CONSENSO_DATI_FIBRA_OK
 * @param EMAIL_FIBRA
 */
public class CompilaIndirizzoFibra {

	public static void main (String[] args) throws Exception{
		Properties prop = null;
		SeleniumUtilities util;

		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			String statoUbiest=Costanti.statusUbiest;
			util = new SeleniumUtilities(driver);
			CompilaIndirizziComponent indir = new CompilaIndirizziComponent(driver);
	
			TimeUnit.SECONDS.sleep(5);
			
			if (prop.getProperty("INDIRIZZO_FIBRA_OK").equals("SI")) {
			    logger.write("Inserimento dati FIBRA OK - Start");
				indir.inserisciIndirizzoFibraOk(prop.getProperty("PROVINCIA_FIBRA"), 
						prop.getProperty("CITTA_FIBRA"), prop.getProperty("INDIRIZZO_FIBRA"),
						prop.getProperty("CIVICO_FIBRA"), prop.getProperty("SCELTA_MELITA"),
						prop.getProperty("CELLULARE"),
						prop.getProperty("CODICE_MIGRAZIONE"),
						prop.getProperty("TELEFONO_FIBRA"),
						prop.getProperty("CONSENSO_DATI_FIBRA_OK", ""),prop.getProperty("EMAIL_FIBRA"));
			    logger.write("Inserimento dati FIBRA OK - Completed");
			} else {
			    logger.write("Inserimento dati FIBRA KO - Start");
				indir.inserisciIndirizzoFibraKo(prop.getProperty("PROVINCIA_FIBRA"), 
					prop.getProperty("CITTA_FIBRA"), prop.getProperty("INDIRIZZO_FIBRA"),
					prop.getProperty("CIVICO_FIBRA"));
					logger.write("OK - Copertura Fibra non presente!");
					logger.write("*** FINE TEST ***");
			        //System.out.println("OK - Copertura Fibra non presente! *** FINE TEST ***");
			    logger.write("Inserimento dati FIBRA KO - Completed");
			}

			TimeUnit.SECONDS.sleep(2);

			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}


}