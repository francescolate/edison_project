package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato347ACRTrackingRVCComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_347_ACR_Tracking_RVC {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato347ACRTrackingRVCComponent rvc = new Privato347ACRTrackingRVCComponent(driver);
			
			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
                rvc.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
			
			rvc.verifyComponentVisibility(rvc.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = rvc.homePageCentralText;
			rvc.verifyComponentExistence(homePage);
			rvc.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			By homepageSubheading = rvc.homePageCentralSubText;
			rvc.verifyComponentExistence(homepageSubheading);
			rvc.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = rvc.sectionTitle;
			rvc.verifyComponentExistence(sectionTitle);
			rvc.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = rvc.sectionTitleSubText;
			rvc.verifyComponentExistence(sectionTitleSubText);
			rvc.VerifyText(sectionTitleSubText, rvc.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verifying and click on 'dettaglioFornituraButton' - Start");
			By dettaglioFornituraButton =  rvc.dettaglioFornitura_button_621855744;
			rvc.verifyComponentVisibility(dettaglioFornituraButton);
			rvc.clickComponent(dettaglioFornituraButton);
			logger.write("Verifying and click on 'dettaglioFornituraButton' - Completed");
			Thread.sleep(8000);
			
			logger.write("Verifying correct display of Supply and status in 'DETTAGLIO FORNITURE' page - Start");
			By supplyHeading = rvc.supplyHeading;
			rvc.verifyComponentExistence(supplyHeading);
			rvc.VerifyText(supplyHeading, prop.getProperty("SUPPLY_HEADING"));
			
			By supplyHeadingDetails = rvc.supplyHeadingDetails;
			rvc.verifyComponentExistence(supplyHeadingDetails);
			rvc.VerifyText(supplyHeadingDetails, prop.getProperty("SUPPLY_DETAILS"));
			
			By supplyNumeroCliente = rvc.supplyNumeroCliente;
			rvc.verifyComponentExistence(supplyNumeroCliente);
			rvc.VerifyText(supplyNumeroCliente, prop.getProperty("NUMEROCLIENTE"));
			
			By supplyNumeroClienteNumber = rvc.supplyNumeroClienteNumber;
			rvc.verifyComponentExistence(supplyNumeroClienteNumber);
			rvc.VerifyText(supplyNumeroClienteNumber, prop.getProperty("NUMEROCLIENTE_NUMBER"));
			
			By supplyStatusHeading = rvc.supplyStatusHeading;
			rvc.verifyComponentExistence(supplyStatusHeading);
			rvc.VerifyText(supplyStatusHeading, prop.getProperty("SUPPLY_STATUS_HEADING"));
			
			By supplyStatus = rvc.supplyStatus;
			rvc.verifyComponentExistence(supplyStatus);
			rvc.VerifyText(supplyStatus, prop.getProperty("SUPPLY_STATUS"));
			logger.write("Verifying correct display of Supply and status in 'DETTAGLIO FORNITURE' page - Completed");
			
			logger.write("Validate 'Cambio Offerta' section details and steps - Start");
			By section1Heading = rvc.section1Heading;
			rvc.verifyComponentExistence(section1Heading);
			rvc.VerifyText(section1Heading, prop.getProperty("SECTION_1_HEADING"));
			
			By section1Subtext = rvc.section1Subtext;
			rvc.verifyComponentExistence(section1Subtext);
			rvc.VerifyText(section1Subtext, prop.getProperty("SECTION_1_HEADING_SUBTEXT"));
			
			rvc.verifyCambioOffertaStepsRVC_Progress(rvc.CambioOffertaSteps, rvc.stepColor, rvc.Documentazione_d_restituire_Step_Subtext, rvc.Documentazione_d_restituire_Step_SubtextValue);
			
			logger.write("Validate 'Cambio Offerta' section details and steps - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}
