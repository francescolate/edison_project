package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BillsComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HOMEPAGE_ACR_5 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			HomeComponent home = new HomeComponent(driver);
			
		    prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
			prop.setProperty("LETUEATTIVITÀ", "Qui troverai tutte le attività per poter gestire le tue forniture e scoprire i vantaggi.");
			prop.setProperty("FORNITURARIDOTTASOSPESA", "Provvedi subito al pagamento dell’intera morosità indicata nella lettera di diffida e la tua fornitura verrà riattivata");
			prop.setProperty("BOLLETTETEXT", "Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.");
			prop.setProperty("TEXT", "Provvedi al pagamento online delle tue bollette e/o rate anche se la tua modalità di pagamento è il bollettino postale. Ricordati che se effettui il pagamento online in Area Clienti oppure in APP, con Lottomatica Servizi, Sisal Pay, Punti vendita Coop abilitati, Cbill, CityPoste Payment e Paytipper non dovrai inviare alcuna ricevuta. Registreremo automaticamente il tuo pagamento!");

			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
						
			logger.write("Check for Residential Menu - Completed");
			
			logger.write("Check for View your bills - Start");

			home.verifyComponentExistence(home.viewYourBillsRidotta);
			
			logger.write("Check for View your bills - Completed");

			logger.write("Check for Supply Details - Start");

			home.verifyComponentExistence(home.supplyDetailsRidotta);
						
			logger.write("Check for Supply Details - Completed");
			
			home.checkSupplyStatus(home.ridotta, prop.getProperty("FORNITURAFIELD"));
			
			home.verifyComponentExistence(home.yourActivity);
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);

			log.VerifyText(home.yourActivitySubText, prop.getProperty("LETUEATTIVITÀ"));
			
			home.verifyComponentExistence(home.reducedOrSuspendedSupply);
			
			log.VerifyText(home.reducedOrSuspendedSupplyContent, prop.getProperty("FORNITURARIDOTTASOSPESA"));
			
			home.verifyComponentExistence(home.goDetails);
			
			home.clickComponent(home.goDetails);
			
			BillsComponent bc = new BillsComponent(driver);
			
			bc.verifyComponentExistence(bc.bolletteHeader);
			
			log.VerifyText(bc.bolletteHeaderText, prop.getProperty("BOLLETTETEXT"));
			
			bc.verifyComponentExistence(bc.yourFilter);
			
			home.waitForElementToDisplay(bc.yourFilter);

			/*bc.clickComponent(bc.yourFilter);			
			
			bc.verifyFilterValues(bc.paymentStatus,prop.getProperty("STATO_PAGAMENTO"));
			
			bc.verifyFilterValues(bc.typeOfDoc,prop.getProperty("TIPO_DI_DOCUMENTO"));

			bc.verifyFilterValues(bc.year,prop.getProperty("ANNO"));

			bc.verifyFilterValues(bc.supply,prop.getProperty("FORNITURA"));
			
			bc.verifyComponentExistence(bc.payOnline);
			
			bc.checkForText(bc.redBoxText, prop.getProperty("TEXT"));*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}

}
