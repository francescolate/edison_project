package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.ContractChoiceComponent;
import com.nttdata.qa.enel.components.colla.ScegliTu100x100HelpComponent;
import com.nttdata.qa.enel.components.colla.ScegliTuBiorariaComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyMotorinoScegliTu100x100_2 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			//** Start Set Dati da verificare
			prop.setProperty("CONTRACT_TYPE", "Luce");
			prop.setProperty("PLACE", "Casa");
			prop.setProperty("REQUIREMENTS", "Visualizza tutte");
			
			prop.setProperty("UPPER_LABEL", "TROVA LA TUA SOLUZIONE");
			
			prop.setProperty("FIRST_QUESTION_COUNTER", "1 / 3");
			prop.setProperty("FIRST_QUESTION_TITLE", "Quando tu e la tua famiglia trascorrete più tempo in casa?");
			prop.setProperty("FIRST_QUESTION_OPTION_A", "Solitamente durante tutto il giorno");
			prop.setProperty("FIRST_QUESTION_OPTION_B", "Prevalentemente la sera e il weekend");
			prop.setProperty("FIRST_QUESTION_OPTION_C", "Prevalentemente la notte e la domenica");
			
			prop.setProperty("SECOND_QUESTION_COUNTER", "2 / 3");
			prop.setProperty("SECOND_QUESTION_TITLE", "In quale fascia oraria utilizzi principalmente gli elettrodomestici?");
			prop.setProperty("SECOND_QUESTION_SUBTITLE", "Lavatrice, Lavastoviglie, Condizionatore, Scaldabagno...");
			prop.setProperty("SECOND_QUESTION_OPTION_A", "Durante tutto il giorno indistintamente");
			prop.setProperty("SECOND_QUESTION_OPTION_B", "Prevalentemente la sera e il weekend");
			prop.setProperty("SECOND_QUESTION_OPTION_C", "Prevalentemente la notte e la domenica");
			
			prop.setProperty("THIRD_QUESTION_COUNTER", "3 / 3");
			prop.setProperty("THIRD_QUESTION_TITLE", "Saresti disposto a spostare i tuoi consumi per avere una bolletta più leggera?");
			prop.setProperty("THIRD_QUESTION_OPTION_A", "No, Voglio consumare indistintamente tutto il giorno");
			prop.setProperty("THIRD_QUESTION_OPTION_B", "Si, verso la fascia serale e il weekend");
			prop.setProperty("THIRD_QUESTION_OPTION_C", "Si, verso la fascia notturna e la domenica");
			
			prop.setProperty("RESULT_TITLE", "Ecco la soluzione migliore per te!");
			prop.setProperty("RESULT_TITLE", "Ecco il piano tariffario più adatto a te!");
			prop.setProperty("RESULT_SUBTITLE", "E se le tue abitudini cambiano? Potrai scegliere di cambiare il tuo piano fino a 3 volte l’anno");
			prop.setProperty("SUMMARY_TITLE", "Bioraria");
			prop.setProperty("SUMMARY_SUBTITLE", "La tranquillità di risparmiare la sera, nei weekend e nei festivi");
			prop.setProperty("SUMMARY_DESCRIPTION_1", "Da lunedì a venerdì, dalle 8 alle 19");
			prop.setProperty("SUMMARY_DESCRIPTION_2", "Da lunedì a venerdì, dalle 19 alle 8. Sabato, domenica e festivi");
			prop.setProperty("SUMMARY_DESCRIPTION_UNITS", "€/kWh");
			
			prop.setProperty("SCEGLI_TU_BIORARIA_TITLE", "Scegli Tu Bioraria");
			prop.setProperty("SCEGLI_TU_BIORARIA_DESCRIPTION", "Ricorda che per completare l'adesione devi avere a portata di mano:\n" + 
					"Codice fiscale / POD / Iban\n" + 
					"se devi fare un Subentro, una Prima attivazione o una Voltura servono anche:\n" + 
					"dati dell'immobile / Carta d'identità\n\n" + 
					"L'offerta che hai scelto è valida solo per forniture ad uso abitativo.");
			
			prop.setProperty("POD_POPUP_TEXT_1", "COS'E' IL POD");
			prop.setProperty("POD_POPUP_TEXT_2", "Il POD (Point of Delivery) è il codice identificativo della fornitura elettrica e serve ad individuare il punto di prelievo dell'energia su tutto il territorio nazionale.\n" + 
					"Il POD:");
			prop.setProperty("POD_POPUP_TEXT_3", "inizia sempre con IT");
			prop.setProperty("POD_POPUP_TEXT_4", "è un codice alfanumerico composto da 14 caratteri");
			prop.setProperty("POD_POPUP_TEXT_5", "potrebbe essere composto da 15 caratteri, ma è sufficiente considerare i primi 14");
			prop.setProperty("POD_POPUP_TEXT_6", "non cambia anche se cambi fornitore di energia elettrica");
			prop.setProperty("POD_POPUP_TEXT_7", "Se la fornitura è già stata attiva lo trovi nella bolletta.");
			prop.setProperty("POD_POPUP_TEXT_8", "L'EnelTel è un codice numerico specifico che identifica il contatore che il distributore ha installato. Genericamente di 8-9 cifre, viene visualizzato sul display dell'apparecchio di misura alla voce \"Numero Cliente\".");
			
			
			prop.setProperty("BUILDING_DATA_POPUP_TEXT_1", "Dati dell'immobile");
			prop.setProperty("BUILDING_DATA_POPUP_TEXT_2", "Per procedere con la tua attivazione, tieni a portata di mano le seguenti informazioni:");
			prop.setProperty("BUILDING_DATA_POPUP_TEXT_3", "Dati sul regolare possesso e detenzione dell' immobile. Uno tra queste informazioni:");
			prop.setProperty("BUILDING_DATA_POPUP_TEXT_4", "Proprietà/Usufrutto");
			prop.setProperty("BUILDING_DATA_POPUP_TEXT_5", "Locazione/Comodato");
			prop.setProperty("BUILDING_DATA_POPUP_TEXT_6", "Altro diritto sull'immobile");

			//** END Set Dati da verificare
			////System.out.println.println("Accessing home page");
			logger.write("Accessing home page - Start");
			LoginLogoutEnelCollaComponent homePage = new LoginLogoutEnelCollaComponent(driver);
			ScegliTu100x100HelpComponent detailPage = new ScegliTu100x100HelpComponent(driver);
			homePage.launchLink(prop.getProperty("LINK100"));
			//Click chusura bunner pubblicitario se esistente
			homePage.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

			By acceptCookieBtn = homePage.buttonAccetta;
			homePage.verifyComponentExistence(acceptCookieBtn);
			homePage.clickComponent(acceptCookieBtn);
			logger.write("Accessing home page - Completed");
			Thread.sleep(10000);
			//System.out.println.println("Question 1");
			logger.write("Question 1 - Start");
			detailPage.verifyComponentExistence(detailPage.startNowBtn);
			detailPage.clickComponent(detailPage.startNowBtn);
			detailPage.verifyQuestionsOverlay(
					prop.getProperty("UPPER_LABEL"),
					prop.getProperty("FIRST_QUESTION_COUNTER"),
					prop.getProperty("FIRST_QUESTION_TITLE"),
					null,
					prop.getProperty("FIRST_QUESTION_OPTION_A"),
					prop.getProperty("FIRST_QUESTION_OPTION_B"),
					prop.getProperty("FIRST_QUESTION_OPTION_C"));
			detailPage.verifyComponentExistence(detailPage.answerB_Btn);
			detailPage.clickComponent(detailPage.answerB_Btn);
			logger.write("Question 1 - Completed");
			
			//System.out.println.println("Question 2");
			logger.write("Question 2 - Start");
			detailPage.verifyQuestionsOverlay(
					prop.getProperty("UPPER_LABEL"),
					prop.getProperty("SECOND_QUESTION_COUNTER"),
					prop.getProperty("SECOND_QUESTION_TITLE"),
					prop.getProperty("SECOND_QUESTION_SUBTITLE"),
					prop.getProperty("SECOND_QUESTION_OPTION_A"),
					prop.getProperty("SECOND_QUESTION_OPTION_B"),
					prop.getProperty("SECOND_QUESTION_OPTION_C"));
			detailPage.verifyComponentExistence(detailPage.answerB_Btn);
			detailPage.clickComponent(detailPage.answerB_Btn);
			logger.write("Question 2 - Completed");
			
			//System.out.println.println("Question 3");
			logger.write("Question 3 - Start");
			detailPage.verifyQuestionsOverlay(
					prop.getProperty("UPPER_LABEL"),
					prop.getProperty("THIRD_QUESTION_COUNTER"),
					prop.getProperty("THIRD_QUESTION_TITLE"),
					null,
					prop.getProperty("THIRD_QUESTION_OPTION_A"),
					prop.getProperty("THIRD_QUESTION_OPTION_B"),
					prop.getProperty("THIRD_QUESTION_OPTION_C"));
			detailPage.verifyComponentExistence(detailPage.answerC_Btn);
			detailPage.clickComponent(detailPage.answerC_Btn);
			logger.write("Question 3 - Completed");
			
			//System.out.println.println("Verifying offer detail");
			logger.write("Verifying offer detail - Start");
//			new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(detailPage.resultTitle));
			detailPage.verifyComponentVisibility(detailPage.resultTitle);
			detailPage.verifyComponentText(detailPage.resultTitle, prop.getProperty("RESULT_TITLE"));
			detailPage.verifyComponentVisibility(detailPage.resultSubtitle);
			detailPage.verifyComponentText(detailPage.resultSubtitle, prop.getProperty("RESULT_SUBTITLE"));
			detailPage.verifyComponentVisibility(detailPage.summaryTitle);
			detailPage.verifyComponentText(detailPage.summaryTitle, prop.getProperty("SUMMARY_TITLE"));
			detailPage.verifyComponentVisibility(detailPage.summarySubtitle);
			detailPage.verifyComponentText(detailPage.summarySubtitle, prop.getProperty("SUMMARY_SUBTITLE"));
			detailPage.verifyComponentVisibility(detailPage.summaryDescription);
//			String[] substrings = {prop.getProperty("SUMMARY_DESCRIPTION_1"), prop.getProperty("SUMMARY_DESCRIPTION_2")};
//			detailPage.verifyComponentTextContainsSubstrings(detailPage.summaryDescription, substrings);
//			String[]substrings2 = {prop.getProperty("SUMMARY_DESCRIPTION_UNITS")};
//			detailPage.verifyComponentTextContainsSubstrings(detailPage.firstPrice, substrings2);
//			detailPage.verifyComponentTextContainsSubstrings(detailPage.secondPrice, substrings2);
			detailPage.verifyComponentVisibility(detailPage.resultActivationBtn);
			detailPage.clickComponent(detailPage.resultActivationBtn);
			logger.write("Verifying offer detail - Completed");
			
			logger.write("Check page adesione contratto Scegli Tu Bioraria - Start");
			OCR_Module_Component ocr = new OCR_Module_Component(driver);
			ocr.checkHeader("Scegli Tu Bioraria", driver);
			ocr.checkPage();	
			logger.write("Check page adesione contratto Scegli Tu Bioraria - Completed");
			
			
/*			
			
			//System.out.println.println("Verifying selected offer landing page");
			logger.write("Verifying selected offer landing page - Start");
			ScegliTuBiorariaComponent landingPage = new ScegliTuBiorariaComponent(driver);
			landingPage.verifyComponentVisibility(landingPage.header);
			landingPage.verifyComponentText(landingPage.header, prop.getProperty("SCEGLI_TU_BIORARIA_TITLE"));
			landingPage.verifyComponentVisibility(landingPage.detailLabel);
			landingPage.verifyComponentText(landingPage.detailLabel, prop.getProperty("SCEGLI_TU_BIORARIA_DESCRIPTION"));
			logger.write("Verifying selected offer landing page - Completed");
			
			//System.out.println.println("Verifying POD popup");
			logger.write("Verifying POD popup - Start");
			landingPage.verifyComponentVisibility(landingPage.podInfoBtn);
//			landingPage.clickComponent(landingPage.podInfoBtn);
			landingPage.waitAndKeepClickingOnElement(20, landingPage.podInfoBtn, 30);
			String[] podStrings = {
					prop.getProperty("POD_POPUP_TEXT_1"),
					prop.getProperty("POD_POPUP_TEXT_2"),
					prop.getProperty("POD_POPUP_TEXT_3"),
					prop.getProperty("POD_POPUP_TEXT_4"),
					prop.getProperty("POD_POPUP_TEXT_5"),
					prop.getProperty("POD_POPUP_TEXT_6"),
					prop.getProperty("POD_POPUP_TEXT_7"),
					prop.getProperty("POD_POPUP_TEXT_8")
			};
			landingPage.verifyElementsArrayText(landingPage.podInfoPopup, podStrings);
			landingPage.verifyComponentVisibility(landingPage.podPopupCloseBtn);
			landingPage.clickComponent(landingPage.podPopupCloseBtn);
			logger.write("Verifying POD popup - Completed");
			
			//System.out.println.println("Verifying building data popup");
			logger.write("Verifying building data popup - Start");
			landingPage.verifyComponentVisibility(landingPage.buildingDataBtn);
//			landingPage.clickComponent(landingPage.buildingDataBtn);
			landingPage.waitAndKeepClickingOnElement(20, landingPage.buildingDataBtn, 30);
			String[] buildingStrings = {
					prop.getProperty("BUILDING_DATA_POPUP_TEXT_1"),
					prop.getProperty("BUILDING_DATA_POPUP_TEXT_2"),
					prop.getProperty("BUILDING_DATA_POPUP_TEXT_3"),
					prop.getProperty("BUILDING_DATA_POPUP_TEXT_4"),
					prop.getProperty("BUILDING_DATA_POPUP_TEXT_5"),
					prop.getProperty("BUILDING_DATA_POPUP_TEXT_6")
			};
			landingPage.verifyElementsArrayText(landingPage.buildingDataPopup, buildingStrings);
			landingPage.verifyComponentVisibility(landingPage.buildingInfoCloseBtn);
			landingPage.clickComponent(landingPage.buildingInfoCloseBtn);
			logger.write("Verifying building data popup - Completed");
			
*/
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
