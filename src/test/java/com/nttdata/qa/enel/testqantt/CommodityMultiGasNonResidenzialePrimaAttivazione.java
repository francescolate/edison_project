package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CommodityMultiGasNonResidenzialePrimaAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			logger.write("Selezione primo POD, compila sezione commodity e click su 'Conferma Fornitura' - Start");
			conferma.confermaCommodity(prop.getProperty("POD_1"));
			TimeUnit.SECONDS.sleep(3);
			conferma.verificaCampiSezioneCommodityGas(prop.getProperty("POD_1"));
			conferma.selezionaLigtheningValueInSrollView("Categoria di Consumo", prop.getProperty("CATEGORIA_CONSUMO"));
			
			conferma.selezionaLigtheningValueInSrollView("Categoria Uso", prop.getProperty("CATEGORIA_USO"));
			
			//conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
			conferma.sendText("Telefono SMS DEL. 40", prop.getProperty("TELEFONO_SMS"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
		//	conferma.inserisciOrdineFittizio(prop.getProperty("ORDINE_FITTIZIO"));

//			conferma.selezionaLigtheningValue("Prelievi Consumi/Anno", prop.getProperty("CONSUMO_ANNUO"));			
			conferma.selezionaLigtheningValueInSrollView("Categoria di Marketing", prop.getProperty("CATEGORIA_MARKETING"));
            
	//		conferma.selezionaLigtheningValue("Ordine di Grandezza", prop.getProperty("ORDINE_GRANDEZZA"));
			conferma.selezionaLigtheningValue("Profilo di Consumo", prop.getProperty("PROFILO_CONSUMO"));
			conferma.selezionaLigtheningValue("Classe di Prelievo", prop.getProperty("CLASSE_PRELIEVO"));
//			
//            conferma.sendText("Potenzialita’ Richiesta", prop.getProperty("POTENZIALITA_RICHIESTA"));
			
			conferma.sendText("Note", prop.getProperty("COMMODITY"));
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			TimeUnit.SECONDS.sleep(3);
			conferma.clickComponent(conferma.confermaFornituraButton);
			gestione.checkSpinnersSFDC();
			logger.write("Selezione primo POD, compila sezione commodity e click su 'Conferma Fornitura' - Completed");
			logger.write("Selezione secondo POD, compila sezione commodity e click su 'Conferma Fornitura' - Start");
			conferma.confermaCommodity(prop.getProperty("POD_2"));
			TimeUnit.SECONDS.sleep(3);
			conferma.verificaCampiSezioneCommodityGas(prop.getProperty("POD_2"));
			conferma.selezionaLigtheningValueInSrollView("Categoria di Consumo", prop.getProperty("CATEGORIA_CONSUMO"));
			conferma.selezionaLigtheningValueInSrollView("Categoria Uso", prop.getProperty("CATEGORIA_USO"));
			//conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
			conferma.sendText("Telefono SMS DEL. 40", prop.getProperty("TELEFONO_SMS"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
		//	conferma.inserisciOrdineFittizio(prop.getProperty("ORDINE_FITTIZIO"));
			
 //    		conferma.selezionaLigtheningValue("Classe di Prelievo", prop.getProperty("CLASSE_PRELIEVO"));
//			conferma.selezionaLigtheningValue("Prelievi Consumi/Anno", prop.getProperty("CONSUMO_ANNUO"));
//			
//			
			conferma.selezionaLigtheningValueInSrollView("Categoria di Marketing", prop.getProperty("CATEGORIA_MARKETING"));
//            
	//		conferma.selezionaLigtheningValue("Ordine di Grandezza", prop.getProperty("ORDINE_GRANDEZZA"));
			conferma.selezionaLigtheningValue("Profilo di Consumo", prop.getProperty("PROFILO_CONSUMO"));
     		conferma.selezionaLigtheningValue("Classe di Prelievo", prop.getProperty("CLASSE_PRELIEVO"));
//            conferma.sendText("Potenzialita’ Richiesta", prop.getProperty("POTENZIALITA_RICHIESTA"));
			
			conferma.sendText("Note", prop.getProperty("COMMODITY"));
			
			TimeUnit.SECONDS.sleep(3);
			conferma.clickComponent(conferma.confermaFornituraButton);
			gestione.checkSpinnersSFDC();
			logger.write("Selezione secondo POD, compila sezione commodity e click su 'Conferma Fornitura' - Completed");
			logger.write("click su 'Conferma' - Start");
			conferma.clickComponent(conferma.confirmButton);
			gestione.checkSpinnersSFDC();
			logger.write("click su 'Conferma' - Completed");
			
	
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
