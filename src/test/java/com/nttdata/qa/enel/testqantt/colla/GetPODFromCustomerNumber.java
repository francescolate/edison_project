package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.bouncycastle.pqc.math.linearalgebra.GF2nPolynomial;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.lightning.GetPODFromCustomerNumberComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class GetPODFromCustomerNumber {
	@Step("Get account from CF")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			GetPODFromCustomerNumberComponent gpfcn = new GetPODFromCustomerNumberComponent(driver);					
			
			logger.write("typing the user CF - START");	
			gpfcn.verifyComponentExistence(gpfcn.searchBar);
			gpfcn.searchAccount(gpfcn.searchBar, prop.getProperty("CF"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("checking the data existence - START");	
			gpfcn.verifyComponentExistence(gpfcn.accountLink);
			gpfcn.clickComponent(gpfcn.accountLink);
			logger.write("checking the data existence - COMPLETE");
			
			if(prop.getProperty("WP_USERNAME") != null && !prop.getProperty("WP_USERNAME").equals("")){
				logger.write("checking account email - START");	
				//gpfcn.verifyComponentExistence(By.xpath(gpfcn.accountEmail.replace("$email$", prop.getProperty("WP_USERNAME"))));
				logger.write("checking account email - COMPLETE");
			}
			
			/*gpfcn.verifyComponentExistence(gpfcn.serviziEBeni);
			gpfcn.clickComponent(gpfcn.serviziEBeni);
			
			gpfcn.fillInputField(gpfcn.podSearchBar, prop.getProperty("CUSTOMER_NUMBER"));
			gpfcn.verifyComponentExistence(By.xpath(gpfcn.pod.replace("$CUSTOMER_NUMBER$", prop.getProperty("CUSTOMER_NUMBER"))));
			gpfcn.jsClickObject(By.xpath(gpfcn.pod.replace("$CUSTOMER_NUMBER$", prop.getProperty("CUSTOMER_NUMBER"))));
			
			gpfcn.scrollToVisibility(gpfcn.serviceRequestHeader);
			gpfcn.verifyComponentExistence(gpfcn.serviceRequestHeader);
			gpfcn.jsClickObject(gpfcn.serviceRequestHeader);
			
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			//gpfcn.verifyComponentExistence(By.xpath(gpfcn.attivazioneVasTD.replace("$DATE$", dtf.format(now))));
			*/
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
