package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ImpressComponent;
import com.nttdata.qa.enel.components.colla.SupportBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Attiva_Bolletta_Pubblico_45 {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			SupportBolletteComponent sbc = new SupportBolletteComponent(driver);
			
			logger.write("Launch the url  - START");
			sbc.launchLinkBasicAuth(prop.getProperty("LINK"));
			sbc.verifyComponentExistence(sbc.homePageClose);
			sbc.clickComponent(sbc.homePageClose);
			logger.write("Launch the url  - Complete");
			
			logger.write("Verify and click on Accetta button  - START");
			sbc.verifyComponentExistence(sbc.buttonAccetta);
			sbc.clickComponent(sbc.buttonAccetta);
			logger.write("Verify and click on Accetta button  - Complete");
			Thread.sleep(10000);
			logger.write("Verify and click on Supporto button  - START");
			sbc.verifyComponentExistence(sbc.supporto);
			sbc.clickComponent(sbc.supporto);
			logger.write("Verify and click on Supporto button  - Complete");
			
			logger.write("Verify and click on BolleteWeb link  - START");
			sbc.verifyComponentExistence(sbc.bolletteWeb);
			sbc.clickComponent(sbc.bolletteWeb);
			logger.write("Verify and click on BolleteWeb link  - Complete");
			
			logger.write("Verify the Supporto home path and title  - START");
			sbc.verifyComponentExistence(sbc.supportoPath);
			sbc.containsText(sbc.supportoPath, SupportBolletteComponent.SUPPORTO_PATH, true);
			sbc.containsText(sbc.supportoTitle1, SupportBolletteComponent.SUPPORTO_TITLE1, true);
			sbc.containsText(sbc.supportoTitle2, SupportBolletteComponent.SUPPORTO_TITLE2, true);
			logger.write("Verify the Supporto home path and title  - START");
			
			logger.write("Verify the Attiva bolletta link  - START");
			sbc.verifyComponentExistence(sbc.attivaBolleta);
			sbc.clickComponent(sbc.attivaBolleta);
			logger.write("Verify the Attiva bolletta link  - Complete");
			
			logger.write("Verify the Attiva bolletta contents - START");
			sbc.containsText(sbc.attivaBolletaContent, sbc.ATTIVA_BOLLETTA_CONTENT, true);
			logger.write("Verify the Attiva bolletta contents - START");
			
			logger.write("Click on cliccaqui link - START");
			sbc.verifyComponentExistence(sbc.cliccaqui);
			sbc.clickComponent(sbc.cliccaqui);
			logger.write("Click on cliccaqui link - Complete");
			
			logger.write("Switch to New tab window - START");
			Thread.sleep(3000);
			sbc.SwitchToWindow_2();
			logger.write("Switch to New tab window - Complete");
			
			logger.write("Wait for Numerco field to display - START");			
			sbc.waitForElementToDisplay(sbc.numeroClienteField);
			logger.write("Wait for Numerco field to display - Complete");	
			
			logger.write("Verify the mandatory fields - START");
			sbc.verifyComponentExistence(sbc.numeroClienteField);
			sbc.verifyComponentExistence(sbc.codiceFiscaleInput);
			sbc.verifyComponentExistence(sbc.codiceFiscaleLabel);
			sbc.verifyComponentExistence(sbc.numeroClienteLabel);
			logger.write("Verify the mandatory fields - Complete");
			
			logger.write("Verify the Mandatory field label - START");
			sbc.containsText(sbc.numeroClienteLabel, SupportBolletteComponent.NUMEROCLIENTE_LABEL);
			sbc.containsText(sbc.codiceFiscaleLabel, SupportBolletteComponent.CODICEFISCALE_LABEL);
			logger.write("Verify the Mandatory field label - Complete");
			
			logger.write("Verify and click on attiva button - START");
			sbc.verifyComponentExistence(sbc.attivaButton);
			sbc.clickComponent(sbc.attivaButton);
			logger.write("Verify and click on attiva button - Complete");
			
			logger.write("Verify the error message for mandatory fields - START");
			sbc.containsText(sbc.numeroClientelFieldMsg, SupportBolletteComponent.NUMEROCLIENTE_MSG);		
			sbc.containsText(sbc.codiceFiscaleMsg, SupportBolletteComponent.CODICEFISCA_MSG);
			logger.write("Verify the error message for mandatory fields - Complete");
			
			logger.write("Enter the input and click on Attiva button - START");
			sbc.insertText(sbc.numeroClienteField, prop.getProperty("NUMERO_CLIENTE"));
			sbc.insertText(sbc.codiceFiscaleInput, prop.getProperty("CODICE_FISCALE"));
			sbc.clickComponent(sbc.attivaButton);
			logger.write("Enter the input and click on Attiva button - Complete");
			
			Thread.sleep(2000);
			logger.write("Enter the input Telephone Number and click Invia Codice - START");
			sbc.insertText(sbc.numeroTelefonoInput, prop.getProperty("NUMERO_TELEFONO"));	
			sbc.clickComponent(sbc.inviaCodiceButton);
			logger.write("Enter the input Telephone Number and click Invia Codice - Completed");
					
			ImpressComponent ic = new ImpressComponent(driver);
					
			logger.write("Verify the footer tab and label - START");
			ic.verifyFooterVisibility();
			logger.write("Verify the footer tab and label - Complete");
			
			logger.write("Verify the header tab and label - START");
			ic.verifyHeaderVisibility();
			logger.write("Verify the header tab and label - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
