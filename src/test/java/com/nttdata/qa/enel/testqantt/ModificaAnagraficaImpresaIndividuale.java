package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.ModificaClienteComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaAnagraficaImpresaIndividuale {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			ModificaClienteComponent modifica = new ModificaClienteComponent(driver);
			SceltaProcessoComponent sceltaProcesso = new SceltaProcessoComponent(driver);
				

				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
					sceltaProcesso.clickAllProcess();
				sceltaProcesso.chooseProcessToStart(sceltaProcesso.avvioModificaAnagrafica);
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
				identificaInterlocutore.verifyInputFieldExist("Documento");
				identificaInterlocutore.verifyInputFieldExist("Indirizzo di fornitura");
				identificaInterlocutore.verifyInputFieldExist("Numero Cliente");
				identificaInterlocutore.verifyInputFieldExist("Numero fattura");
				
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
				
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Codice Fiscale");
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Completed");
				
				logger.write("Inserimento documento e conferma - Start");
				identificaInterlocutore.insertDocumento("b");
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
				logger.write("Inserimento documento e conferma - Completed");
				TimeUnit.SECONDS.sleep(60);
				
				// NEW SOFIA 10/11 
				CheckListComponent checklist = new CheckListComponent(driver);
				checklist.clickConferma();
				IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
				String numerorichiesta = nuovaRichiesta
				.salvaNumeroRichiesta(nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
				prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());
				
				
				modifica.verificaControlloForniture();
				String rag_soc=SeleniumUtilities.randomAlphaNumeric(8);
				modifica.modificaRagioneSociale( rag_soc);
				prop.setProperty("RAGIONE_SOCIALE", rag_soc.trim());
				
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
//				if (Costanti.statusUbiest.contentEquals("OFF")) {
//					indirizzi.compileAddressForzatura(indirizzi.buttonVerificaIndirizzo);
//					indirizzi.closeAlert();
//					indirizzi.forzaIndirizzoSedeLegale(indirizzi.buttonVerificaIndirizzo);
//					
//					} else {
//						indirizzi.compileAddressIfAvailable("ROMA", indirizzi.province2, "ROMA", indirizzi.city2,
//								"VIA NIZZA", indirizzi.street2, "5", indirizzi.civico2, indirizzi.buttonVerificaIndirizzo);
//						
//					}
				

				if (prop.getProperty("MODALITA_ACCETTAZIONE", "Documenti da Firmare").equals("Documenti Validi")) {
					modifica.selezionaModalitaAccettazione( "Documenti Validi");
				} else {

					DocumentiUploadComponent document = new DocumentiUploadComponent(driver);
					document.generaDocumentoInDocumentale();
					document.caricaDocumentoInDocumentaleSenzaAlert( Utility.getDocumentPdf(prop));

				}
				modifica.pressButton( modifica.buttonConferma);
				
				// OLD
				/*
				CheckListComponent checklist = new CheckListComponent(driver);
				String frame = checklist.clickFrameConferma2();
				IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
				String numerorichiesta = nuovaRichiesta
				.salvaNumeroRichiesta(frame,nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
	
	         	prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());
	         	
				ModificaClienteComponent modifica = new ModificaClienteComponent(driver);
				modifica.verificaControlloForniture(frame);
				String ragionesociale=SeleniumUtilities.randomAlphaNumeric(8);
				prop.setProperty("RAGIONE_SOCIALE", ragionesociale);
				modifica.modificaRagioneSociale(frame, ragionesociale);
		
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
			

					indirizzi.compileAddressIfAvailable(frame, "ROMA", indirizzi.province2, "ROMA", indirizzi.city2,
							"VIA NIZZA", indirizzi.street2, "5", indirizzi.civico2, indirizzi.buttonVerificaIndirizzo);
				
				if (prop.getProperty("MODALITA_ACCETTAZIONE", "Documenti da Firmare").equals("Documenti Validi")) {
					modifica.selezionaModalitaAccettazione(frame, "Documenti Validi");
				} else {

					DocumentiUploadComponent document = new DocumentiUploadComponent(driver);
					document.generaDocumentoInDocumentale(frame);
					document.caricaDocumentoInDocumentaleSenzaAlert(frame, Utility.getDocumentPdf(prop));

				}
				modifica.pressButton(frame, modifica.buttonConferma);

*/
				
			
			prop.setProperty("RETURN_VALUE", "OK");
			}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}
	}

