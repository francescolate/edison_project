package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergia_BsnComponent;
import com.nttdata.qa.enel.components.colla.InfoEESteps_BsnComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ActivateInfoEnelEnergia_Bsn {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Accessing servizi page");
			logger.write("Accessing servizi page - Start");
			InfoEnelEnergia_BsnComponent infoEE = new InfoEnelEnergia_BsnComponent(driver);
			InfoEnelEnergiaMyComponent iee = new InfoEnelEnergiaMyComponent(driver);
			Thread.sleep(30000);
			infoEE.clickComponent(infoEE.serviziItemUpdatedNew);
			//infoEE.checkServiziPageStrings();
			logger.write("Accessing servizi page - Completed");
			
			System.out.println("Accessing InfoEnelEnergia");
			logger.write("Accessing InfoEnelEnergia - Start");
			Thread.sleep(30000);
			//infoEE.verifyComponentVisibility(infoEE.infoEnelEnergiaBtn);
			//infoEE.clickComponent(infoEE.infoEnelEnergiaBtn);
			iee.clickComponent(iee.infoEnergiaIconUpdated);
			infoEE.verifyVisibilityAndText(infoEE.infoEETitleLbl, infoEE.infoEETitle);
			logger.write("Accessing InfoEnelEnergia - Completed");
			
			
			/*boolean shouldSaveClientNumber = Boolean.parseBoolean(prop.getProperty("SAVE_CLIENT_NUMBER", "false"));
			if (shouldSaveClientNumber) {
				System.out.println("Updating query with client number");
				logger.write("Updating query with client number - Start");
				String clientNumber = infoEE.extractClientNumber();
				String theQuery = prop.getProperty("QUERY");
				//Let's insert the actual client number into the query
				theQuery = theQuery.replace("#NUMERO_CLIENTE#", clientNumber);
				System.out.println("Query: " + theQuery);
				prop.setProperty("QUERY", theQuery);
				logger.write("Updating query with client number - Completed");
			}*/
			
			System.out.println("Activate InfoEnelEnergia");
			logger.write("Activate InfoEnelEnergia - Start");
			infoEE.verifyVisibilityThenClick(infoEE.continueActivationBtn);
			Thread.sleep(30000);
			infoEE.verifyVisibilityThenClick(infoEE.attivaBtn);
			Thread.sleep(5000);
			InfoEESteps_BsnComponent steps = new InfoEESteps_BsnComponent(driver);
			steps.checkStep1ActivationElements();
			logger.write("Activate InfoEnelEnergia - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
