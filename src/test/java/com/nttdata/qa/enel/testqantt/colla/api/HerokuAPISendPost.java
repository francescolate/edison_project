// Package HerokuAPICoperturaCivico

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.jwt.gen.JWToken;
import com.nttdata.qa.enel.components.colla.api.HerokuAPISendPostComponent;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIRegistrationComponent_Err;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIUserByEnelIDComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;
/*import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;*/

public class HerokuAPISendPost {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Authenticate Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {	
			String localAut = prop.getProperty("Authorization");
			if (localAut.equalsIgnoreCase("Y"))
				prop.setProperty("Authorization", "Bearer ");
			else if (localAut.equalsIgnoreCase("Token"))
				prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
			else
				prop.setProperty("Authorization", "");
		
			//prop.setProperty("API_URL", prop.getProperty("API_URL"));
    		prop.setProperty("TID", UUID.randomUUID().toString());
    		prop.setProperty("SID", UUID.randomUUID().toString());  
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPISendPostComponent HRKComponent = new HerokuAPISendPostComponent();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			if(prop.getProperty("JSON_DATA_OUTPUT_2")!=null){
				if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT_2")))
				    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT_2"));
			}
		
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


