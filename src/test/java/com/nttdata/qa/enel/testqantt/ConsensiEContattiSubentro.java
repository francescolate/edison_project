package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SelezioneUsoFornituraComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConsensiEContattiSubentro {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			if(!prop.getProperty("CHECK_RIPENSAMENTO", "").equals("")) {
				
				logger.write("Verifica popup ripensamento - Start");
				offer.clickWithJS(offer.buttonInformativa);
				offer.verificaPopUpRipensamento(prop.getProperty("TITOLO_POPUP"), prop.getProperty("TESTO_POPUP"));
				logger.write(offer.verificaPopUpRipensamento(prop.getProperty("TITOLO_POPUP"), prop.getProperty("TESTO_POPUP")));
				logger.write("Verifica popup ripensamento - Completed");
				
			} else {
			
			//per commodity DUAL
			if (prop.getProperty("DUAL","").contentEquals("Y")) {
				SelezioneUsoFornituraComponentEVO uso = new SelezioneUsoFornituraComponentEVO(driver);
				logger.write("Selezione 'Unita' abitativa' - Start");
				uso.selezionaLigtheningValue("Unità abitativa", prop.getProperty("UNITA_ABITATIVA"));
				logger.write("Selezione 'Unita' abitativa' - Completed");
								
				uso.clickComponent(gestione.selezionaUnitaAbitativaConferma);
				uso.checkSpinnersSFDC();
			
	}
			//offer.scrollComponent(offer.buttonConfermaContattiConsensi);
			offer.scrollComponent(offer.sezioneContattiEConsensi);
			
			if (prop.getProperty("CLIENTE_BUSINESS","").contentEquals("Y")) {
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Profilazione");
		    	offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Telefonico");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Cellulare");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Marketing Email");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Telefonico");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Cellulare");
				offer.popolaCampiSezioneContattiConsensiSeVuoti("Consenso Terzi Email");
			}
			else {
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Start");
			offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
			offer.scrollComponent(offer.sezioneContattiEConsensi);
//			offer.clickComponentIfExist(offer.checkBoxSiContattiConsensi);
			offer.press(offer.checkBoxSiContattiConsensi);
			TimeUnit.SECONDS.sleep(1);
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si' per la voce 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?' e click su 'Conferma' - Completed");
			}
			
			offer.pressButton(offer.buttonConfermaContattiConsensi);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
