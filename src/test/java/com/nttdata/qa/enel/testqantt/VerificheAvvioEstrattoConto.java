package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CompilaAvvioEstrattoContoComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificheAvvioEstrattoConto {

	public static void main(String[] args) throws Exception {
        Properties prop = null;

        try {
                      prop = WebDriverManager.getPropertiesIstance(args[0]);
                      RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
                      QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
/*
				driver.switchTo().defaultContent();
				VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
				verifica.attesaCaricamentoSezioneCliente();
				TimeUnit.SECONDS.sleep(15);
				int tentativi = 10;

				do {
					try {
						driver.navigate().refresh();
						logger.write("Verifica stato e sottostato richiesta - Start");
						
						verifica.verificaStatoSottostatoRichiesta(prop.getProperty("STATO_GLOBALE_RICHIESTA"),
								prop.getProperty("SOTTOSTATO_GLOBALE_RICHIESTA"));
						logger.write("Verifica stato e sottostato richiesta - Completed");
						tentativi = 0;
					} catch (Exception e) {
						--tentativi;
						if(tentativi==0) throw e;
						else TimeUnit.SECONDS.sleep(60);
						
					}
				} while (tentativi > 0);
				
				VerificaRichiestaComponent verificaEstrattoConto = new VerificaRichiestaComponent(driver);

				verificaEstrattoConto.attesaCaricamentoPaginaRichiesta();

				// Controllo Attività Creata
				verificaEstrattoConto.verificaStatoAttivitaComplete(prop.getProperty("DESCRIZIONEATTIVITA"),
						prop.getProperty("STATOATTIVITA"));
				verificaEstrattoConto.closeTabAttivita();
*/				
				
				VerificaRichiestaComponent verificaEstrattoConto = new VerificaRichiestaComponent(driver);


				logger.write("Verifica Stato Attività");

				TimeUnit.SECONDS.sleep(5);

				// Controllo Documenti Creati
				verificaEstrattoConto.verificaStatoDocumentiComplete(prop.getProperty("MODELLODOCUMENTO"),
						prop.getProperty("STATODOCUMENTO"));
//				logger.write("Verifica Plico In Lavorazione"); //non c'è il collegamento con i sistemi esterni per avere Inviato come Stato
				logger.write("Verifica Plico " + prop.getProperty("STATODOCUMENTO")); // Attenzione per eMaile PEC non c'è il collegamento con i sistemi esterni per avere Inviato come Stato
				System.out.println("Verifica Plico " + prop.getProperty("STATODOCUMENTO"));
				
//				verificaEstrattoConto.verificaDocumentiCreati(
//						Integer.parseInt(prop.getProperty("NUMERODOCUMENTIATTESI")),
//						prop.getProperty("DESCRIZIONIDOCUMENTICREATI").split(";"));
//				logger.write("Verifica Documenti Creati");

//				verificaEstrattoConto.verificaCreazioneLinkDocumenti("Estratto Conto");
//				verificaEstrattoConto.closeTabDocumenti();
//				logger.write("Verifica Link Documento");
				
			}
            prop.setProperty("RETURN_VALUE", "OK");
        }
        catch (Exception e) 
        {
                       prop.setProperty("RETURN_VALUE", "KO");
                       StringWriter errors = new StringWriter();
                       e.printStackTrace(new PrintWriter(errors));
                       errors.toString();

                       prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
                      if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
                       //Store WebDriver Info in properties file
                       prop.store(new FileOutputStream(args[0]), "Set TestObject Info");                               
        }
	}

}

