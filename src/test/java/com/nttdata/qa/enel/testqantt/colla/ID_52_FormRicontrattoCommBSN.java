package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.FormRicontrattoCommBSNComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_52_FormRicontrattoCommBSN {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			FormRicontrattoCommBSNComponent form = new FormRicontrattoCommBSNComponent(driver);
			String impreseUrl = "https://www-coll1.enel.it/it/imprese";
			String contattaciUrl = "https://www-coll1.enel.it/it/form-ricontatto-commerciale";
			
			prop.setProperty("NOME", "Mario");
			prop.setProperty("COGNOME", "Rossi");
			prop.setProperty("REGIONE_SOCIALE", "Prova S.r.l.");
			prop.setProperty("IMPORTA_MEDIA_BOLLETTA", "oltre 10.000");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CAP", "00198");
			prop.setProperty("EMAIL", "fabiana.manzo@nttdata.com");
			prop.setProperty("CONFERMA_EMAIL", "fabiana.manzo@nttdata.com");
			prop.setProperty("CELLULARE", "3669047153");
					
							
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);

			log.clickComponent(log.buttonAccetta);
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			//2
			logger.write("Click on Imprese Header and Verification of Data- Start ");
			form.clickComponent(form.imprese);
			
			form.checkURLAfterRedirection(impreseUrl);
			form.comprareText(form.impreseHomePagePath, FormRicontrattoCommBSNComponent.IMPRESE_HOMEPAGE_PATH, true);
			//form.comprareText(form.impreseHomePageHeading, FormRicontrattoCommBSNComponent.IMPRESE_HOMEPAGE_HEADING, true);
			log.verifyComponentVisibility(form.impreseHomePageHeading_2);
			form.comprareText(form.impreseHomePageParagraph, FormRicontrattoCommBSNComponent.IMPRESE_HOMEPAGE_PARAGRAPH, true);
			logger.write("Click on Imprese Header and verification of Data- Start ");
			
			//3
			form.comprareText(form.comePossiamoAiutarti, FormRicontrattoCommBSNComponent.COME_POSSIAMO_AIUTARTI, true);
			form.comprareText(form.contattaci, FormRicontrattoCommBSNComponent.CONTATTACI, true);
			form.comprareText(form.contattaciSubText, FormRicontrattoCommBSNComponent.CONTATTACI_SUBTEXT, true);
			
			//4
			logger.write("Verification of contattci Heading and Text and clicking on cliccaqui- Start ");
			form.comprareText(form.consulentiEsperti, FormRicontrattoCommBSNComponent.CONSULTENTI_ESPERTI, true);
			form.clickComponent(form.cliccaQui);
			
			form.checkURLAfterRedirection(contattaciUrl);
			Thread.sleep(5000);
			//form.comprareText(form.contattaciHomepagePath, FormRicontrattoCommBSNComponent.CONTATTACI_HOMEPAGE_PATH, true);
			form.comprareText(form.contattciParagraph, FormRicontrattoCommBSNComponent.CONTATTACI_HOMEPAGE_PARAGRAPH, true);
			logger.write("Verification of contattci Heading and Text and clicking on cliccaqui- End ");
			
			//5
			logger.write("Verification of form fields- Start ");
			form.comprareText(form.contattciHeading2, FormRicontrattoCommBSNComponent.CONTATTACI_HEADING, true);
			form.verifyComponentExistence(form.nome);
			form.verifyComponentExistence(form.cognome);
			form.verifyComponentExistence(form.regioneSociale);
			form.verifyComponentExistence(form.importaMedia);
			form.verifyComponentExistence(form.provincia);
			form.verifyComponentExistence(form.cap);
			form.verifyComponentExistence(form.email);
			form.verifyComponentExistence(form.confermaEmail);
			form.verifyComponentExistence(form.telefonoCellulare);
			logger.write("Verification of form fields- End ");
			
			//6
			logger.write("Verification of SI and No Radio Buttons - Start ");
			form.comprareText(form.seigiaclientieHeading, FormRicontrattoCommBSNComponent.SEI_GIA_HEADING, true);
			form.comprareText(form.indicaText, FormRicontrattoCommBSNComponent.INDICA_TEXT, true);
			form.verifyComponentExistence(form.siRadio);
			form.verifyComponentExistence(form.noRadio);
			logger.write("Verification of SI and No Radio Buttons - End ");
			
			//7
			logger.write("Verification of Informativa Heading and Text - Start ");
			form.comprareText(form.informativaHeading, FormRicontrattoCommBSNComponent.INFORMATIVA_HEADING, true);
			form.comprareText(form.informativaText, FormRicontrattoCommBSNComponent.INFORMATIVA_TEXT, true);
			logger.write("Verification of Informativa Heading and Text - End ");
			
			//8
			logger.write("Verification of Header - Start ");
			form.verifyComponentExistence(form.luceEGasHeader);
			form.verifyComponentExistence(form.impreseHeader);
			//form.verifyComponentExistence(form.insiemeATeHeader);
			form.verifyComponentExistence(form.storieHeader);
			form.verifyComponentExistence(form.futurEHeader);
			form.verifyComponentExistence(form.mediaHeader);
			form.verifyComponentExistence(form.supportoHeader);
			logger.write("Verification of Header - End ");
			
			//9
			logger.write("Verification of Footer - Start ");
			logger.write("Verifica Footer - Start");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Italia S.p.a.");
			prop.setProperty("AFTER_FOOTER_TEXT", "© Enel Energia S.p.a. - Gruppo Iva P.IVA 12345678901");
			//footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By linkInformazioniLegali=footer.informazioniLegaliLink;
			footer.verifyComponentExistence(linkInformazioniLegali);
			footer.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=footer.creditsLink;
			footer.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=footer.privacyLink;
			footer.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=footer.cookieLink;
			footer.verifyComponentExistence(linkCookie);
			
			By linkRemit=footer.remitLink;
			footer.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=footer.twitterIcon;
			footer.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=footer.facebookIcon;
			footer.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=footer.youtubeIcon;
			footer.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=footer.linkedinIcon;
			footer.verifyComponentExistence(iconLinkedin);
			
			footer.verifyComponentExistence(footer.instagramIcon);
			footer.verifyComponentExistence(footer.telegramIcon);
			logger.write("Verifica Footer - Completed");
			logger.write("Verification of Footer - End ");
			
			//10
			logger.write("Verification of Accetto and Non Accetto Buttons - Start ");
			form.verifyComponentExistence(form.accettoRadio);
			form.verifyComponentExistence(form.NonAccettoRadio);
			form.comprareText(form.tuttiCampiText, FormRicontrattoCommBSNComponent.TUTTI_CAMPI_TEXT, true);
			form.verifyComponentExistence(form.inviaButton);
			logger.write("Verification of Accetto and Non Accetto Buttons - End ");
			
			//11
			logger.write("Verification of Campo obbligatorio text after click on invia button- Start ");
			form.clickComponent(form.inviaButton);
			form.verifyComponentExistence(form.campoObbligatorioNome);
			form.verifyComponentExistence(form.campoObbligatorioCogome);
			form.verifyComponentExistence(form.campoObbligatorioRegioneSociale);
			form.verifyComponentExistence(form.campoObbligatorioImportaMedio);
			form.verifyComponentExistence(form.campoObbligatorioProvincia);
			form.verifyComponentExistence(form.campoObbligatorioCap);
			form.verifyComponentExistence(form.campoObbligatorioEmail);
			form.verifyComponentExistence(form.campoObbligatorioTelefonaCellulare);
			logger.write("Verification of Campo obbligatorio text after click on invia button- End ");
			
			//12
			logger.write("Enter the valid input in the form and click on Invia button- Start ");
			Thread.sleep(5000);
			form.enterInputParameters(form.nome, prop.getProperty("NOME"));
			form.enterInputParameters(form.cognome, prop.getProperty("COGNOME"));
			form.enterInputParameters(form.regioneSociale, prop.getProperty("REGIONE_SOCIALE"));
			form.clickComponent(form.importaMedia);
			form.clickComponent(form.oltre10000);
			form.clickComponent(form.provincia);
			form.clickComponent(form.roma);
			Thread.sleep(5000);
			form.clickComponent(form.cap);
			form.clickComponent(form.cap00198);
			form.enterInputParameters(form.email, prop.getProperty("EMAIL"));
			form.enterInputParameters(form.confermaEmail, prop.getProperty("CONFERMA_EMAIL"));
			form.enterInputParameters(form.telefonoCellulare, prop.getProperty("CELLULARE"));
			form.enterInputParameters(form.confirmTelefonoCellulare, prop.getProperty("CELLULARE"));
			form.isRadioSelected(form.noRadio);
			form.clickComponent(form.accettoRadio);
			
			form.clickComponent(form.inviaButton);
			form.comprareText(form.step2ContattaciHeading, FormRicontrattoCommBSNComponent.STEP2_CONTATTACI_HEADING, true);
			form.comprareText(form.step2ContattaciParagraph, FormRicontrattoCommBSNComponent.STEP2_CONTATTACI_PARAGRAPH, true);
			form.comprareText(form.riepilogoHeading, FormRicontrattoCommBSNComponent.RIEPILOGO_HEADING, true);
			form.comprareText(form.enteredDataNome, FormRicontrattoCommBSNComponent.ENTERED_DATA_NOME, true);
			form.comprareText(form.enteredDataCognome, FormRicontrattoCommBSNComponent.ENTERED_DATA_COGNOME, true);
			form.comprareText(form.enteredDataRegioneSociale, FormRicontrattoCommBSNComponent.ENTERED_DATA_REGIONESOCIALE, true);
			form.comprareText(form.enteredDataImportoMedio, FormRicontrattoCommBSNComponent.ENTERED_DATA_IMPORTOMEDIO, true);
			form.comprareText(form.enteredDataProvincia, FormRicontrattoCommBSNComponent.ENTERED_DATA_PROVINCIA, true);
			form.comprareText(form.enteredDatacap, FormRicontrattoCommBSNComponent.ENTERED_DATA_CAP, true);
			form.comprareText(form.enteredDataEmail, FormRicontrattoCommBSNComponent.ENTERED_DATA_EMAIL, true);
			form.VerifyText(form.enteredDataCellulare, FormRicontrattoCommBSNComponent.ENTERED_DATA_CELLULARE);
			form.comprareText(form.enteredDataSeiGiaCliente, FormRicontrattoCommBSNComponent.ENTERED_DATA_SEIGUI_CLIENTI, true);
			logger.write("Enter the valid input in the form and click on Invia button- End ");
			
			//13
			logger.write("Clicking on Conferma Button- Start ");
			form.verifyComponentExistence(form.confermaButton);
			form.verifyComponentExistence(form.indietroButton);
			form.clickComponent(form.confermaButton);
			form.comprareText(form.step3contattoRichiestaHeading, FormRicontrattoCommBSNComponent.STEP3_CONTATTO_RICHIESTA_HEADING, true);
			form.comprareText(form.step3contattoRichiestaParagraph1, FormRicontrattoCommBSNComponent.STEP3_CONTATTO_RICHIESTA_PARAGRAPH1, true);
			form.comprareText(form.step3contattoRichiestaParagraph2, FormRicontrattoCommBSNComponent.STEP3_CONTATTO_RICHIESTA_PARAGRAPH2, true);
			logger.write("Clicking on Conferma Button- End ");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
