package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.SFDCBox;
import com.nttdata.qa.enel.components.lightning.ScontiEBonusComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import org.junit.Assert;

public class ConfermaScontiBonusSubentro extends BaseComponent implements SFDCBox {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ScontiEBonusComponent sconti = new ScontiEBonusComponent(driver);

			if (!prop.getProperty("CHECK_BANNER_ERRORE", "").equals("")) {
				sconti.inserisciCodiceAmico(prop.getProperty("CODICE_AMICO"));
				sconti.pressButton(sconti.confermaScontiEBonusSWAEVO);
				sconti.verificaErrore();
			} else if (!prop.getProperty("CHECK_CODICE_AMICO").equals("")) {
				try {
					new WebDriverWait(driver, 30)
							.until(ExpectedConditions.visibilityOfElementLocated(sconti.codiceAmico));
					System.out.println("il campo codice amico  è presente");
				} catch (TimeoutException e) {
				System.out.println("Il campo codce amico non è presente");
				} catch (Exception e) {
                      throw e;
				}

			} else {
				logger.write("Conferma Sconti e bonus - Start");
				sconti.pressButton(sconti.confermaScontiEBonusSWAEVO);
				logger.write("Conferma Sconti e bonus - Completed");
				prop.setProperty("RETURN_VALUE", "OK");
			}

		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
