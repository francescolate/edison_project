package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

//Il seguente modulo si occupa della creazione di un nuovo Cliente accedendo prima alla sezione Clienti dal menu a tendina del primo tab disponibile 
//Poi cliccando su Nuovo inserirà la tipologia di cliente , e compilerà tutti i campi necessari al completamento della creazione quali nomi, indirizzi, referenti etc..
public class VerificaCalcoloCodiceFiscaleNuovoCliente {

	@Step("Creazione Nuovo Cliente")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			logger.write("Accesso al Tab Clienti - Start");
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				AccediTabClientiComponent tabClienti = new AccediTabClientiComponent(driver);
				tabClienti.accediTabClienti();
				logger.write("Accesso al Tab Clienti - Completed");
				CreaNuovoClienteComponent crea = new CreaNuovoClienteComponent(driver);
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				String frameName;
				switch (prop.getProperty("TIPOLOGIA_CLIENTE")) {
				case "RESIDENZIALE":
					logger.write("Pressione pulsante Nuovo - Start");
					crea.nuovoCliente();
					logger.write("Pressione pulsante Nuovo - Completed");
					logger.write("Scelta Tipologia Cliente Residenziale - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonResidenziale);
					logger.write("Scelta Tipologia Cliente Residenziale - Completed");
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Start");
					crea.verificaCalcoloCodiceFiscale(prop);
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Completed");

					break;
					
				case "IMPRESA":
					logger.write("Pressione pulsante Nuovo - Start");
					crea.nuovoCliente();
					logger.write("Pressione pulsante Nuovo - Completed");
					logger.write("Scelta Tipologia Cliente Impresa Individuale - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonImpresaIndividuale);
					logger.write("Scelta Tipologia Cliente Impresa Individuale - Completed");
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Start");
					crea.verificaCalcoloCodiceFiscaleReferente(prop);
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Completed");

					break;
					
				case "CONDOMINIO":
					logger.write("Pressione pulsante Nuovo - Start");
					crea.nuovoCliente();
					logger.write("Pressione pulsante Nuovo - Completed");
					logger.write("Scelta Tipologia Cliente Condominio - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonCondominio);
					logger.write("Scelta Tipologia Cliente Condominio - Completed");
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Start");
					crea.verificaCalcoloCodiceFiscaleReferente(prop);
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Completed");

					break;
					
					
				case "BUSINESS":
					logger.write("Pressione pulsante Nuovo - Start");
					crea.nuovoCliente();
					logger.write("Pressione pulsante Nuovo - Completed");
					logger.write("Scelta Tipologia Cliente Business - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonBusiness);
					logger.write("Scelta Tipologia Cliente Business - Completed");
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Start");
					crea.verificaCalcoloCodiceFiscaleReferente(prop);
					logger.write("Inserimento campi anagrafica e verifica calcolo codice fiscale - Completed");

					break;
				
				default:
					throw new Exception(
							"Tipologia cliente diversa da RESIDENZIALE/IMPRESA/CONDOMINIO/BUSINESS. Impossibile proseguire");
				}
				
				

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
