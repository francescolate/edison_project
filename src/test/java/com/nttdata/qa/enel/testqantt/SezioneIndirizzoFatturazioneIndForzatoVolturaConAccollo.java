package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class SezioneIndirizzoFatturazioneIndForzatoVolturaConAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);

			// se è verificato, premi modifica e inserisci. 
			if (util.verifyExistence(offer.labelIndirizzoVerificato, 5)) {
				offer.verifyClickability(offer.buttonModificaIndirizzoFatturazione);
				offer.clickComponent(offer.buttonModificaIndirizzoFatturazione);
				gestione.checkSpinnersSFDC();
			
				// popolaCampi in modo errato e premi verifica 
				offer.popolaCampiIndirizzoFatturazioneSeVuoti(prop.getProperty("PROVINCIA_COMUNE"), "Via ignazio Sorrentino", prop.getProperty("CIVICO"));
				offer.clickComponent(offer.buttonVerificaIndirizzoFat);
				TimeUnit.SECONDS.sleep(2);
				
				// verifica errore e premi indirizzo forzato 
				if (!util.verifyExistence(offer.labelIndirizzoVerificato, 5)){
				compila.ForzaIndirizzoNonVerificato(prop.getProperty("CIVICO"), prop.getProperty("CAP"));	
				}
			
				// premi conferma
				offer.clickComponent(offer.buttonConfermaIndirizzoFat);
			
				
			}
			else {
				logger.write("compilare i campi obbligatori nella sezione 'Indirizzo di Fatturazione', cliccare su pulsante 'Verifica', premere su indirizzo non verificato, e verificare l'indirizzo. Poi premi conferma - Start");
				// popolaCampi in modo errato e premi verifica 
				offer.popolaCampiIndirizzoFatturazioneSeVuoti(prop.getProperty("PROVINCIA_COMUNE"), "Via ignazio Sorrentino", prop.getProperty("CIVICO"));
				offer.clickComponent(offer.buttonVerificaIndirizzoFat);
				TimeUnit.SECONDS.sleep(2);
				
				// verifica errore e premi indirizzo forzato 
				if (!util.verifyExistence(offer.labelIndirizzoVerificato, 5)){
				compila.ForzaIndirizzoNonVerificato(prop.getProperty("CIVICO"), prop.getProperty("CAP"));	
				}
			
				// premi conferma
				offer.clickComponent(offer.buttonConfermaIndirizzoFat);
			}
			TimeUnit.SECONDS.sleep(5);

						
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
