package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ChiudiCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

/**
 * 
 * @param VALIDAZIONE_CAMPO_FIBRA
 *            campo fibra da validare in caso di errore front end .
 *
 */
public class CompilaIndirizzoFibraPA {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

		SeleniumUtilities util;

		try {

			String statoUbiest = Costanti.statusUbiest;
			util = new SeleniumUtilities(driver);
			CompilaIndirizziComponent indir = new CompilaIndirizziComponent(driver);
			TimeUnit.SECONDS.sleep(5);

			boolean ClickConfirm = true;

			if (prop.getProperty("INDIRIZZO_FIBRA_OK").equals("SI")) {
				// inserisco l'indirizzo fibra corretto
				logger.write("Inserimento dati FIBRA OK - Start");
				indir.InserisciIndirizzoFibra(prop.getProperty("PROVINCIA_FIBRA"), prop.getProperty("CITTA_FIBRA"),
						prop.getProperty("INDIRIZZO_FIBRA"), prop.getProperty("CIVICO_FIBRA"));
				indir.verificaFibraOK();

				// scelta alert fibra SI//NO
				if (prop.getProperty("SCELTA_MELITA", "NO").contentEquals("SI")) {
					indir.selezionaSceltaMelita(prop.getProperty("SCELTA_MELITA"));
					indir.InserisciCdmFibra(prop.getProperty("CODICE_MIGRAZIONE"));
					indir.InserisciNumeroTelFibra(prop.getProperty("TELEFONO_FIBRA"));
				} else {
					indir.selezionaSceltaMelita(prop.getProperty("SCELTA_MELITA"));
				}
				// inserimento cellulare ed email con verifica se richiesta.
				String Validazione = prop.getProperty("VALIDAZIONE_CAMPO_FIBRA", "");
				switch (Validazione) {
				case "CELLULARE":
					indir.confermaTelefono(prop.getProperty("CELLULARE_FIBRA"));
					if (!prop.getProperty("VALIDAZIONE_CELLULARE_FIBRA", "").contentEquals("")) {
						ClickConfirm = indir.VerificaValidazioneNumeroCellulare(prop.getProperty("VALIDAZIONE_CELLULARE_FIBRA"));
					} else {
						ClickConfirm = true;
					}
					break;

				case "EMAIL":
					indir.InserisciMailFibra(prop.getProperty("EMAIL_FIBRA"));
					if (!prop.getProperty("VALIDAZIONE_EMAIL_FIBRA", "").contentEquals("")) {
						ClickConfirm = indir.VerificaValidazioneEmailFibra(prop.getProperty("VALIDAZIONE_EMAIL_FIBRA"));
					} else {
						ClickConfirm = true;
					}
					break;

				default:
					indir.InserisciMailFibra(prop.getProperty("EMAIL_FIBRA"));
					indir.confermaTelefono(prop.getProperty("CELLULARE_FIBRA"));

					System.out.println("NESSUNA VALIDAZIONE PER I CAMPI FIBRA");

					break;
				}

				
				if (prop.getProperty("CONSENSO_DATI_FIBRA_OK", "").contentEquals("SI")) {
					indir.GestioneConsensiFibra(prop.getProperty("CONSENSO_DATI_FIBRA_OK"));
					if (ClickConfirm) {
						indir.confermaIndirizzo("Fibra");
					}

				} else {
					indir.confermaIndirizzoFibraDisabilitato("Fibra");
				}

				logger.write("Inserimento dati FIBRA OK - Completed");
			} else {
				logger.write("Inserimento dati FIBRA KO - Start");
				indir.InserisciIndirizzoFibra(prop.getProperty("PROVINCIA_FIBRA"), prop.getProperty("CITTA_FIBRA"),
						prop.getProperty("INDIRIZZO_FIBRA"), prop.getProperty("CIVICO_FIBRA"));
				indir.verificaFibraKO();
				logger.write("OK - Copertura Fibra non presente!");
				logger.write(" *** FINE TEST ***");
				System.out.println("OK - Copertura Fibra non presente! *** FINE TEST ***");
				logger.write("Inserimento dati FIBRA KO - Completed");
			}

			TimeUnit.SECONDS.sleep(2);

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}