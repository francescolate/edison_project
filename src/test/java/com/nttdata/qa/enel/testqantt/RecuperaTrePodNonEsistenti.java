package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class RecuperaTrePodNonEsistenti {

	@Step("Recupero Pod da Workbench")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				a.pressButton(a.checkAgree);
				a.pressButton(a.buttonLogin);

				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					logger.write("Inserimento user");
					page.enterPassword(Costanti.password_admin_salesforce);
					logger.write("Inserimento password");
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}

				boolean flag = true;

				SeleniumUtilities util = new SeleniumUtilities(driver);
				String pod = "";
				int numPod = 3;
				int found = 0;
				while(!flag || found < 3) {

					if (prop.getProperty("COMMODITY").equals("ELE")) {
						pod = util.generateRandomPodNumberEnergia();
					} else if (prop.getProperty("COMMODITY").equals("GAS")) {
						pod = util.generateRandomPodNumberGAS();
					} else if (prop.getProperty("COMMODITY").equals("GAS4")) {
						pod = util.generateRandomPodNumberGAS4();
					}

					String query = "SELECT ITA_IFM_POD__c FROM NE__Service_Point__c where ITA_IFM_POD__c='"+pod+"'";
					a.insertQuery(query);
					a.pressButton(a.submitQuery);
					if(a.verificaNoRecordFound()) {
						flag = true;
						found++;
						if(found==1){
							prop.setProperty("POD_1", pod);	
							System.out.println(prop.getProperty("POD_1"));
						}
						if(found==2){
							prop.setProperty("POD_2", pod);	
							System.out.println(prop.getProperty("POD_2"));

						}
						if(found==3) {
							prop.setProperty("POD_3", pod);
							System.out.println(prop.getProperty("POD_3"));

						}
					}
				}

				a.logoutWorkbench();



			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
