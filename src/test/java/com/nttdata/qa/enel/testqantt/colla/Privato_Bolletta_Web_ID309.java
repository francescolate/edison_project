package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID308Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID309 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivatoBollettaWebID308Component atti = new PrivatoBollettaWebID308Component(driver);
			
			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
				atti.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
			
			atti.verifyComponentVisibility(atti.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = atti.homePageCentralText;
			atti.verifyComponentExistence(homePage);
			atti.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
			
			By homepageSubheading = atti.homePageCentralSubText;
			atti.verifyComponentExistence(homepageSubheading);
			atti.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = atti.sectionTitle;
			atti.verifyComponentExistence(sectionTitle);
			atti.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = atti.sectionTitleSubText;
			atti.verifyComponentExistence(sectionTitleSubText);
			atti.VerifyText(sectionTitleSubText, atti.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verify and Click on left menu item 'Visualizza_le_bollette_button' - Start");
			By Visualizza_le_bollette_button = atti.Visualizza_le_bollette_button;
			atti.verifyComponentExistence(Visualizza_le_bollette_button);
			atti.clickComponent(Visualizza_le_bollette_button);
			logger.write("Verify and Click on left menu item 'Visualizza_le_bollette_button' - Completed");
			Thread.sleep(8000);
			
			logger.write("Verify and Click on left menu item 'Bolletta_Web_box_Tile' - Start");
			By Bolletta_Web_box_Tile = atti.Bolletta_Web_box_Tile;
			atti.verifyComponentExistence(Bolletta_Web_box_Tile);
			atti.clickComponent(Bolletta_Web_box_Tile);
			logger.write("Verify and Click on left menu item 'Bolletta_Web_box_Tile' - Completed");
			Thread.sleep(8000);
			
			logger.write("Checking if Bolletta_Web_TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By Bolletta_Web_Title = atti.Bolletta_Web_Title;
			atti.verifyComponentExistence(Bolletta_Web_Title);
			atti.VerifyText(Bolletta_Web_Title, prop.getProperty("BOLLETTA_WEB_TITLE"));
			
			By Bolletta_Web_Title_Subtext = atti.Bolletta_Web_Title_Subtext;
			atti.verifyComponentExistence(Bolletta_Web_Title_Subtext);
			atti.VerifyText(Bolletta_Web_Title_Subtext, prop.getProperty("BOLLETTA_WEB_TITLE_SUBTEXT"));
			logger.write("Checking if Bolletta_Web_TITLE and Bolletta_Web_Title_SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verify the presence of 'ATTIVA_BOLLETTA_WEB_BUTTON' button - Start");
			By ATTIVA_BOLLETTA_WEB_BUTTON = atti.ATTIVA_BOLLETTA_WEB_BUTTON;
			atti.verifyComponentExistence(ATTIVA_BOLLETTA_WEB_BUTTON);
			logger.write("Verify the presence of 'ATTIVA_BOLLETTA_WEB_BUTTON' button - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}	
