package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class VerificaSelezioneFattureMultipleCopiaDocumenti {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            SezioneSelezioneFornituraComponent aut = new SezioneSelezioneFornituraComponent(driver);
            
            Thread.sleep(15000);
            util.getFrameActive();

            /*aut.clickComponentIfExist(aut.SelectAllFatture);
            TimeUnit.SECONDS.sleep(2);*/

            WebElement table;

            table = util.waitAndGetElement(aut.tableBy);

            util.scrollToElement(table);

            if(util.getTableRowCount(table) < 1)
                throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

            if(util.getTableRowCount(table) > 1)
            {
                List<WebElement> rowElement = util.waitAndGetElements(table, aut.SelectAllFatture);
                rowElement.get(0).click();
                System.out.println(rowElement.size());
            }


            logger.write("verifica messaggio di errore - Start");
            aut.verificaPopUpError(aut.popUpErrore, aut.popUpErroreTesto, aut.popUpErroreTestoAtteso);
            logger.write("verifica messaggio di errore -  - Completed");


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }


    }

}
