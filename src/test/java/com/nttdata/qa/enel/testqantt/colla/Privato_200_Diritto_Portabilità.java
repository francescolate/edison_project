package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_200_Diritto_Portabilità {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
			
			/*
			prop.setProperty("ACCESS_PROBLEMS_STRING", "Se hai problemi di accesso");
						
			logger.write("Accessing login page - Start");
			pac.launchLink(prop.getProperty("LINK"));
			pac.verifyComponentExistence(pac.homePageClose);
			pac.clickComponent(pac.homePageClose);
			pac.verifyComponentExistence(pac.logoEnel);
			pac.verifyComponentExistence(pac.buttonAccetta);
			pac.clickComponent(pac.buttonAccetta);
			Thread.sleep(10000);
			pac.verifyComponentExistence(pac.iconUser);
			pac.clickComponent(pac.iconUser);
			logger.write("Accessing login page - Completed");
			
			logger.write("Verifying username and password fields - Start");
			pac.verifyComponentVisibility(pac.username);
			pac.verifyComponentVisibility(pac.password);
			logger.write("Verifying username and password fields - Completed");
			
			logger.write("Verifying recovery password and username links - Start");
			pac.verifyComponentVisibility(pac.recoveryPassword);
			pac.verifyComponentVisibility(pac.recoveryUsername);
			logger.write("Verifying recovery password and username links - Completed");
			
			logger.write("Verifying access problems link - Start");
			pac.verifyComponentVisibility(pac.accessProblemsLabel);
			pac.verifyComponentText(pac.accessProblemsLabel, prop.getProperty("ACCESS_PROBLEMS_STRING"));
			logger.write("Verifying access problems link - Completed");
			
			logger.write("Verifying social access buttons - Start");
			pac.verifyComponentVisibility(pac.googleAccessButton);
			pac.verifyComponentVisibility(pac.facebookAccessButton);
			logger.write("Verifying social access buttons - Completed");
					
			logger.write("Verifying registration elements - Start");
			pac.verifyComponentVisibility(pac.noAccountLabel);
			pac.verifyComponentVisibility(pac.registerButton);
			pac.verifyComponentVisibility(pac.accessProblemsLink);
			logger.write("Verifying registration elements - Completed");
			
			logger.write("Enter the username and password  - Start");
			pac.enterLoginParameters(pac.username, prop.getProperty("USERNAME"));
			pac.enterLoginParameters(pac.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			pac.verifyComponentVisibility(pac.buttonLoginAccedi);
			pac.clickComponent(pac.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			logger.write("Verify the privato home page title and contents   - Start");
			pac.verifyComponentExistence(pac.BenvenutoTitle);
			pac.verifyComponentText(pac.BenvenutoTitle,PrivateAreaPageComponent.BENVENUTO_TITLE );
			pac.verifyComponentExistence(pac.BenvenutoContent);
			pac.verifyComponentText(pac.BenvenutoContent, PrivateAreaPageComponent.BENVENUTO_CONTENT);
			pac.verifyComponentExistence(pac.fornitureHeader);
			pac.verifyComponentText(pac.fornitureHeader, PrivateAreaPageComponent.FORNITURE_HEADER);
			pac.verifyComponentExistence(pac.fornitureContent);
			pac.verifyComponentText(pac.fornitureContent, PrivateAreaPageComponent.FORNITURE_CONTENT);
			logger.write("Verify the privato home page title and contents   - Complete");
			*/
			
			logger.write("Verify and click on tuoiDiritti link  - Start");
			pac.verifyComponentExistence(pac.tuoiDirittiLink);
			pac.clickComponent(pac.tuoiDirittiLink);
			logger.write("Verify and click on tuoiDiritti link  - Complete");
			
			logger.write("Verify diPortabilità title and contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitàTitle);
			pac.verifyComponentText(pac.diPortabilitàTitle, PrivateAreaPageComponent.DIPORTABILITA_TITLE);
			pac.verifyComponentExistence(pac.tuoDirittiQues1);
			pac.verifyComponentText(pac.tuoDirittiQues1, PrivateAreaPageComponent.TUOIDIRITTI_QUES1);
			pac.verifyComponentExistence(pac.tuoDirittiAns1);
			pac.verifyComponentText(pac.tuoDirittiAns1, PrivateAreaPageComponent.TUOIDIRITTI_ANS1);
			logger.write("Verify diPortabilità title and contents - Complete");
			
			logger.write("Verify question and answer contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitaLastQues);
			pac.verifyComponentText(pac.diPortabilitaLastQues, PrivateAreaPageComponent.DIPORTABILITA_LASTQUES);
			pac.verifyComponentExistence(pac.diPortabilitaLastAns);
			//pac.compareText(pac.diPortabilitaLastAns, PrivateAreaPageComponent.LUCEDIPORTABILITA_LASTANS1,true);
			logger.write("Verify question and answer contents - Complete");
			
			logger.write("Click on AccediServizo button - Start");
			pac.verifyComponentExistence(pac.accediAlServizio);
			pac.clickComponent(pac.accediAlServizio);
			Thread.sleep(2000);
			logger.write("Click on AccediServizo button - Complete");
			
			logger.write("Verify the DiPortabilita contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitaContent);
			pac.verifyComponentText(pac.diPortabilitaContent, PrivateAreaPageComponent.DIPORTBILITTA_CONTENT);
			logger.write("Verify the DiPortabilita contents - Start");
			
			logger.write("Verify label and value - Start");
			pac.verifyComponentExistence(pac.luceIndrizzoLabel);
			pac.verifyComponentExistence(pac.luceNumeroLabel);
			pac.verifyComponentExistence(pac.luceIndrizzoValue);
			pac.verifyComponentExistence(pac.luceNumeroValue);
			prop.setProperty("LUCEINDRIZZO_LABEL", "Indirizzo della fornitura");
			pac.compareLabelValue(pac.luceIndrizzoLabel, prop.getProperty("LUCEINDRIZZO_LABEL"));
			prop.setProperty("LUCEINDRIZZO_LABEL", "Via Foro Romano 2 00186 Roma Roma Rm");
			pac.compareLabelValue(pac.luceIndrizzoValue, prop.getProperty("LUCEINDRIZZO_LABEL"));
			prop.setProperty("LUCEINDRIZZO_LABEL", "Numero Cliente");
			pac.compareLabelValue(pac.luceNumeroLabel, prop.getProperty("LUCEINDRIZZO_LABEL"));
			prop.setProperty("LUCENUMERO_VALUE", "310494829");
			pac.compareLabelValue(pac.luceNumeroValue, prop.getProperty("LUCENUMERO_VALUE"));
			logger.write("Verify label and value - Complete");	
			
			logger.write("Verify esci and richiedi button - Start");
			pac.verifyComponentExistence(pac.esciButton);
			pac.verifyComponentExistence(pac.richiediButton);
			logger.write("Verify esci and richiedi button - Complete");
			
			logger.write("Verify and click on LuceEye Icon - Start");
			pac.verifyComponentExistence(pac.luceEyeIcon);
			pac.clickComponent(pac.luceEyeIcon);
			logger.write("Verify and click on LuceEye Icon - Complete");
			
			
			pac.verifyComponentExistence(pac.eyeIconPopupHeader);
			pac.compareText(pac.eyeIconPopupHeader, PrivateAreaPageComponent.EYEICONPOPUP_HEADER, true);
			pac.verifyComponentExistence(pac.eyeIconPopupContent);
			pac.compareText(pac.eyeIconPopupContent, PrivateAreaPageComponent.EYICONPOPUP_CONTENT, true);
			
			pac.verifyComponentExistence(pac.eyeIconPopupClosebtn);
			pac.clickComponent(pac.eyeIconPopupClosebtn);
			
			prop.setProperty("RETURN_VALUE", "OK");	
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
