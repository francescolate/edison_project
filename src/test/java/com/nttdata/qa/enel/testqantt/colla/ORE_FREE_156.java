package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFreecardComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ORE_FREE_156 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			
			OreFreecardComponent ofcc = new OreFreecardComponent(driver);
			
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Start");
			ofcc.verifyComponentExistence(ofcc.datiAggiornati);
			ofcc.verifyComponentExistence(ofcc.homePageHeading1);
			ofcc.comprareText(ofcc.homePageHeading1, OreFreecardComponent.HOMEPAGE_HEADING1, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle);
			ofcc.comprareText(ofcc.homePageTitle, OreFreecardComponent.HOMEPAGE_TITLE, true);
			ofcc.verifyComponentExistence(ofcc.homePageHeading2);
			ofcc.comprareText(ofcc.homePageHeading2, OreFreecardComponent.HOMEPAGE_HEADING2, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle2);
			ofcc.comprareText(ofcc.homePageTitle2, OreFreecardComponent.HOMEPAGE_TITLE2, true);
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Ends");
			logger.write("Click on Gestisci Le Ore Free button- Start");
			ofcc.verifyComponentExistence(ofcc.gestisciOreFreeButton);
			ofcc.clickComponent(ofcc.gestisciOreFreeButton);
			logger.write("Click on Gestisci Le Ore Free button- Ends");
			
			
			logger.write("Verification of the supply detail screen layout- Start");
			ofcc.verifyComponentExistence(ofcc.rinominaFornituraButton);
			ofcc.verifyComponentExistence(ofcc.visualizzaBollette);
			ofcc.verifyComponentExistence(ofcc.lightIcon);
			ofcc.comprareText(ofcc.supplyHeading, OreFreecardComponent.SUPPLY_HEADING, true);
			ofcc.comprareText(ofcc.numeroClienta, OreFreecardComponent.NUMERO_CLIENTA, true);
			ofcc.comprareText(ofcc.numeroClientaValue, OreFreecardComponent.NUMERO_CLIENTA_VALUE, true);
			ofcc.checkForSupplyDetails(prop.getProperty("SUPPLY_DETAILS"));
			logger.write("Verification of the supply detail screen layout- Ends");
			
			logger.write("click on Bollettino Postal i icon and verify details- Start");
			Thread.currentThread();
			Thread.sleep(3000);
			//ofcc.jsClickObject(ofcc.IconInfoCircle);
			Robot r = new Robot();
			r.mousePress(InputEvent.BUTTON1_MASK);
			try
			{Thread.sleep(250);}
			catch(InterruptedException e){
				
			}
			r.mouseRelease(InputEvent.BUTTON1_MASK);

			ofcc.jsClickObject(ofcc.iconInfoCircle);
			
			ofcc.comprareText(ofcc.PopupTitle, OreFreecardComponent.POPUP_TITLE, true);
			ofcc.comprareText(ofcc.PopupInnerText, OreFreecardComponent.POPUP_TITLE_INNERTEXT, true);
			ofcc.verifyComponentExistence(ofcc.CloseButton);
			ofcc.verifyComponentExistence(ofcc.ScopriButton);
			ofcc.clickComponent(ofcc.CloseButton);
			logger.write("click on Bollettino Postal i icon and verify details- Ends");
			
			logger.write("click on Ricezione bollette i icon and verify details- Start");
//			ofcc.jsClickObject(ofcc.iconInfocartacea);
//			ofcc.comprareText(ofcc.cartaceaPopupTitle, OreFreecardComponent.CARTACEA_POPUP_TITLE, true);
//			ofcc.comprareText(ofcc.cartaceaPopupInnertext, OreFreecardComponent.CARTACEA_POPUP_INNERTEXT, true);
//			ofcc.verifyComponentExistence(ofcc.CloseButton);
//			ofcc.verifyComponentExistence(ofcc.ScopriButton);
			logger.write("click on Ricezione bollette i icon and verify details- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
	}

