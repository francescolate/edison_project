package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFreecardComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ORE_FREE_159 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreecardComponent ofcc = new OreFreecardComponent(driver);
			
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Start");
//			ofcc.verifyComponentExistence(ofcc.datiAggiornati);
			ofcc.verifyComponentExistence(ofcc.homePageHeading1);
			ofcc.comprareText(ofcc.homePageHeading1, OreFreecardComponent.HOMEPAGE_HEADING1, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle);
			ofcc.comprareText(ofcc.homePageTitle, OreFreecardComponent.HOMEPAGE_TITLE, true);
			ofcc.verifyComponentExistence(ofcc.homePageHeading2);
			ofcc.comprareText(ofcc.homePageHeading2, OreFreecardComponent.HOMEPAGE_HEADING2, true);
			ofcc.verifyComponentExistence(ofcc.homePageTitle2);
			ofcc.comprareText(ofcc.homePageTitle2, OreFreecardComponent.HOMEPAGE_TITLE2, true);
			logger.write("Verify the Correct display of the RESIDENTIAL customer's Home Page data - Ends");
			
			logger.write("Click on Gestisci Le Ore Free button- Start");
			ofcc.verifyComponentExistence(ofcc.gestisciOreFreeButton);
			ofcc.clickComponent(ofcc.gestisciOreFreeButton);
			logger.write("Click on Gestisci Le Ore Free button- Ends");
			
			logger.write("Verification of the supply detail screen layout- Start");
			ofcc.verifyComponentExistence(ofcc.serviziPerBollette);
			ofcc.comprareText(ofcc.serviziPerBollette, OreFreecardComponent.SERVIZI_PER_LI_BOLLETTE, true);
			ofcc.checkForservicesforBillDetails(OreFreecardComponent.BILL_SERVICE_DETAILS);
			ofcc.comprareText(ofcc.bollettaWebAttiva, OreFreecardComponent.BOLLETTA_WEB, true);
			ofcc.comprareText(ofcc.addebitoDirettoAttiva, OreFreecardComponent.ADDEBITO_DIRETTO_ATTIVA, true);
			logger.write("Verification of the supply detail screen layout- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
	}

