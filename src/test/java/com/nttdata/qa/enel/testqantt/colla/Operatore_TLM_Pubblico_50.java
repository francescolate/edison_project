package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.TeleMarketingComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Operatore_TLM_Pubblico_50 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			TeleMarketingComponent tlc = new TeleMarketingComponent(driver);
			
			logger.write("Launch the  portal  - Start");
			lpv.launchLinkBasicAuth(prop.getProperty("LINK"));
			//lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			lpv.verifyComponentExistence(lpv.loginPage);
			lpv.verifyComponentExistence(lpv.username);
			lpv.verifyComponentExistence(lpv.password);
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
			logger.write("Launch the  portal  - Complete");
			
			Thread.sleep(5000);
			
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
			
			logger.write("Verify the rucupera password header and fields  - Start");
			lpv.clickComponent(lpv.rucuperaPassword);
			lpv.verifyComponentExistence(lpv.recuperaHeader);
			lpv.compareText(lpv.recuperaHeader, LoginPageValidateComponent.RUCUPERAPASSWORD_HEADER, true);
			lpv.verifyComponentExistence(lpv.recuperaHeader1);
			lpv.compareText(lpv.recuperaHeader1, LoginPageValidateComponent.RUCUPERAPASSWORD1_HEADER1, true);
			//lpv.verifyComponentExistence(lpv.emaiLabel);
			//lpv.verifyText(lpv.emaiLabel, prop.getProperty("EMAIL_LABEL"));
			logger.write("Verify the rucupera password header and fields  - Complete");
				
			logger.write("click on back button in the browser  - Start");
			tlc.backBrowser(tlc.buttonLoginAccedi);
			logger.write("click on back button in the browser  - Complete");
			
			tlc.checkUrl(prop.getProperty("LINK"));
			
			logger.write("Verify and click on rucupera username link  - Start");
			tlc.verifyComponentExistence(tlc.recuperaUsername);
			tlc.clickComponent(tlc.recuperaUsername);
			logger.write("Verify and click on rucupera username link  - Complete");
			
			tlc.verifyComponentExistence(tlc.rucuperaUserNameTitle);
			tlc.containsText(tlc.rucuperaUserNameTitle, TeleMarketingComponent.RUCUPERAUSERNAME_TITLE, true);
			tlc.containsText(tlc.rucuperaUserNameTitle1, TeleMarketingComponent.RUCUPERAUSERNAME_TITLE1, true);
			tlc.containsText(tlc.rucuperaUsernameLabel, TeleMarketingComponent.RUCUPERAUSERNAME_LABEL, true);
			
			
			logger.write("click on back button in the browser  - Start");
			tlc.backBrowser(tlc.buttonLoginAccedi);
			logger.write("click on back button in the browser  - Complete");
			
			tlc.checkUrl(prop.getProperty("LINK"));
			
			logger.write("Verify and click on register button  - Start");
			tlc.verifyComponentExistence(tlc.registerButton);
			tlc.clickComponent(tlc.registerButton);
			logger.write("Verify and click on register button  - Complete");
			
			
			tlc.verifyComponentExistence(tlc.registerHeader);
			tlc.verifyComponentExistence(tlc.registerHeader1);
			tlc.containsText(tlc.registerHeader, TeleMarketingComponent.REGISTER_HEADER, true);
			tlc.containsText(tlc.registerHeader1, TeleMarketingComponent.REGISTER_HEADER1, true);
			
			logger.write("click on back button in the browser from register page  - Start");
			tlc.backBrowser(tlc.buttonLoginAccedi);
			logger.write("click on back button in the browser from register page  - Complete");
			
			tlc.checkUrl(prop.getProperty("LINK"));
			
			logger.write("Verify and click on problemi button  - Start");
			tlc.verifyComponentExistence(tlc.problemidiLink);
			tlc.clickComponent(tlc.problemidiLink);
			logger.write("Verify and click on problemi button  - Complete");
			
			tlc.containsText(tlc.problemHeader, tlc.problemHeaderText, true);
			tlc.containsText(tlc.problemHeader1, tlc.problemHeaderText1, true);
			
			logger.write("click on back button in the browser from problem di page  - Start");
			tlc.backBrowser(tlc.buttonLoginAccedi);
			logger.write("click on back button in the browser from problem di page  - Complete");
			
			logger.write("Verify the Telemarketing URL  - Start");
			tlc.checkUrl(prop.getProperty("LINK"));
			logger.write("Verify the Telemarketing URL - Complete");
			
			logger.write("Enter login button without input  - Start");
			tlc.verifyComponentExistence(tlc.username);
			tlc.verifyComponentExistence(tlc.password);
			tlc.verifyComponentExistence(tlc.buttonLoginAccedi);
			tlc.clickComponent(tlc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Verify the error message for user name and password  - Start");
			tlc.verifyComponentExistence(tlc.loginErrorMsg);
			tlc.verifyComponentText(tlc.loginErrorMsg, TeleMarketingComponent.LOGIN_ERRORMSG);
			tlc.verifyComponentExistence(tlc.passwordErrorMsg);
			tlc.verifyComponentText(tlc.passwordErrorMsg, TeleMarketingComponent.PASSWORD_ERRORMSG);
			logger.write("Verify the error message for user name and password  - Complete");
			
			
			logger.write("Enter the invalid userand click on login button - Start");
			tlc.insertText(tlc.username, prop.getProperty("INVALIDUSER"));
			tlc.clickComponent(tlc.buttonLoginAccedi);
			logger.write("Verify the invalid userand click on login button  - Complete");
			
			logger.write("Verify the error message for invalid input - Start");
			tlc.verifyComponentText(tlc.invalidInputErrorMsg, TeleMarketingComponent.INVALID_ERROR_MSG);
			logger.write("Verify the error message for invalid input - Complete");
			
			logger.write("Clear the username input - Start");
			tlc.clearText(tlc.username);
			logger.write("Clear the username input - Complete");
			
			
			logger.write("Enter the invalid password and verify the error message - Start");
			tlc.insertText(tlc.password, prop.getProperty("INVALIDPASSWORD"));
			tlc.insertText(tlc.username, prop.getProperty("USERNAME"));
			tlc.clickComponent(tlc.buttonLoginAccedi);
			tlc.verifyComponentText(tlc.invalidPasswordErrorMsg, TeleMarketingComponent.INVALID_PASSWORD_MSG);
			logger.write("Enter the invalid password and verify the error message - Complete");
			//Step 15
			logger.write("Enter the valid user name and password - Start");
			tlc.insertText(tlc.username, prop.getProperty("USERNAME"));
			tlc.insertText(tlc.password, prop.getProperty("PASSWORD"));
			tlc.clickComponent(tlc.buttonLoginAccedi);
			logger.write("Enter the valid user name and password - Complete");
			
			logger.write("Verify the Telemarketing url - Start");
			tlc.checkUrl(prop.getProperty("TELEURL"));
			logger.write("Verify the Telemarketing url - Complete");
			
			logger.write("Verify the Telemarketing header - Start");
			tlc.verifyComponentExistence(tlc.TeleHeader);
			//tlc.comprareText(tlc.TeleHeader, TeleMarketingComponent.TELE_HEADER, true);
			tlc.verifyComponentExistence(tlc.caseLuce);
			tlc.verifyComponentExistence(tlc.caseGas);
			tlc.verifyComponentExistence(tlc.impresaLuce);
			tlc.verifyComponentExistence(tlc.impresaGas);
			tlc.verifyComponentExistence(tlc.funzionaita);
			logger.write("Verify the Telemarketing header - Complete");
			
			logger.write("Verify and click on Aderisci bound link - Start");
			tlc.verifyComponentExistence(tlc.aderisciInboundEnelOneUltra2G);
			tlc.clickComponent(tlc.aderisciInboundEnelOneUltra2G);
			logger.write("Verify and click on Aderisci bound link - Complete");
			
			logger.write("Verify the POD title,input field,ok and cancel button - Start");
			tlc.verifyComponentExistence(tlc.verificaPODTitle);
			tlc.verifyComponentText(tlc.verificaPODTitle, TeleMarketingComponent.POD_TITLE);
			tlc.verifyComponentExistence(tlc.podInput);
			tlc.verifyComponentExistence(tlc.podOkButton);
			tlc.verifyComponentExistence(tlc.podCancelButton);
			logger.write("Verify the POD title,input field,ok and cancel button - Complete");
			
			logger.write("Enter the invalid POD input and verify the error message - Start");
			tlc.insertText(tlc.podInput,prop.getProperty("PODINVALIDINPUT"));
			tlc.clickComponent(tlc.podOkButton);
			tlc.verifyComponentExistence(tlc.podInputError);
			tlc.verifyComponentText(tlc.podInputError, TeleMarketingComponent.POD_INPUT_ERROR);
			logger.write("Enter the invalid POD input and verify the error message - Complete");
			
			logger.write("Enter the valid POD input click on Ok button  - Start");
			tlc.clearText(tlc.podInput);
			tlc.insertText(tlc.podInput, prop.getProperty("PODINPUT"));
			tlc.clickComponentIfExist(tlc.podOkButton);
			logger.write("Enter the valid POD input click on Ok button  - Complete");

			Thread.sleep(5000);
			tlc.SwitchToWindow_2();
			
			logger.write("Verify the POD header and contents - Start");
			logger.write("Verify the POD header and contents - Complete");
			
			logger.write("Close the new window and switch back to current window - Start");
			driver.close();
			tlc.SwitchToWindow_2();
			logger.write("Close the new window and switch back to current window - Complete");
			
			logger.write("Verify and click on Aderisci button - Start");
			tlc.verifyComponentExistence(tlc.AderisciFIBRA);
			tlc.clickComponent(tlc.AderisciFIBRA);
			logger.write("Verify and click on Aderisci button - Complete");
			
			logger.write("Verify the aderisci title and enter the value in the fields - Start");
			tlc.verifyComponentExistence(tlc.aderisciTitle);
			tlc.verifyComponentText(tlc.aderisciTitle, TeleMarketingComponent.ADERISCI_TITLE);
			tlc.verifyComponentExistence(tlc.aderisciInputComune);
			tlc.verifyComponentExistence(tlc.aderisciInputIndrizzo);
			tlc.verifyComponentExistence(tlc.aderisciInputNumero);
			tlc.verifyComponentExistence(tlc.aderisciOkButton);
			tlc.verifyComponentExistence(tlc.aderisciCancelButton);
			tlc.clickComponent(tlc.aderisciCancelButton);
			logger.write("Verify the aderisci title and enter the value in the fields - Complete");
			
			logger.write("click on Aderisci link - Start");
			tlc.clickComponent(tlc.AderisciFIBRA);
			logger.write("click on Aderisci link - Complete");
			
			logger.write("click on ok button without input  - Start");
			tlc.clickComponent(tlc.aderisciOkButton);
			logger.write("click on ok button without input  - Complete");
			
			logger.write("Verify the error message   - Start");
			tlc.verifyComponentExistence(tlc.adersiciInputError);
			tlc.verifyComponentText(tlc.adersiciInputError, TeleMarketingComponent.ADERISCI_ERROR_MSG);
			tlc.clickComponent(tlc.aderisciCancelButton);
			logger.write("Verify the error message   - Complete");
				
			logger.write("Click on Aderisci link   - Start");
			tlc.clickComponent(tlc.AderisciFIBRA);
			logger.write("Click on Aderisci link   - Complete");
			
			logger.write("Enter the input and click on ok button   - Start");
			tlc.insertText(tlc.aderisciInputComune, prop.getProperty("COMMUNE"));
			tlc.simulateDownArrowKey(tlc.aderisciInputComune);
			tlc.simulateEnterKey(tlc.aderisciInputComune);
			tlc.insertText(tlc.aderisciInputIndrizzo, prop.getProperty("INDRIZZO"));
			tlc.simulateDownArrowKey(tlc.aderisciInputIndrizzo);
			tlc.simulateEnterKey(tlc.aderisciInputIndrizzo);
			tlc.insertText(tlc.aderisciInputNumero, prop.getProperty("NUMERO"));
			tlc.simulateDownArrowKey(tlc.aderisciInputNumero);
			tlc.simulateEnterKey(tlc.aderisciInputNumero);
			tlc.clickComponent(tlc.aderisciOkButton);
			logger.write("Enter the input and click on ok button  - Complete");
			Thread.sleep(10000);
			//tlc.SwitchToWindow();
			//tlc.verifyComponentExistence(By.xpath("//h1[@class='header']"));
			//tlc.comprareText(tlc.adesioneStandard, "GiustaXTe", true);

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
