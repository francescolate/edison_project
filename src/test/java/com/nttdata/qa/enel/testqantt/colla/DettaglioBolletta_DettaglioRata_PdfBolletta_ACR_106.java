package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BillsComponent;
import com.nttdata.qa.enel.components.colla.BolleteDettaglioRataComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_106 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
					
		prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
	    prop.setProperty("SUPPLYSERVICES", "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ");
		
		HomeComponent home = new HomeComponent(driver);
		
		logger.write("Check for Residential Menu - Start");
		
		home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
					
		logger.write("Check for Residential Menu - Completed");
		
		logger.write("Click on Bills from Residential Menu - Start");

		home.clickComponent(home.bills);
		
		logger.write("Click on Bills from Residential Menu - Completed");

		BillsComponent bs = new BillsComponent(driver);
		
		bs.verifyComponentExistence(bs.bolletteHeader);
		
		bs.waitForElementToDisplay(bs.yourFilter);
		
		logger.write("Click on Mostra Filtri - Start");

		bs.clickComponent(bs.yourFilter);
		
		logger.write("Click on Mostra Filtri - Completed");

		logger.write("Change filter value from bolleta to rata- Start");

		bs.clickComponent(bs.rataFilter);
		
		bs.clickComponent(bs.bolletaFilter);
		
		bs.clickComponent(bs.applicaFiltri);
		
		logger.write("Change filter value from bolleta to rata- Completed");

		bs.verifyComponentExistence(bs.more);
		
		logger.write("Click on more - Start");

		bs.waitForElementToDisplay(bs.more);
		
		bs.verifyComponentExistence(bs.more);

		bs.clickComponent(bs.more);
		
		logger.write("Click on more - Completed");

		bs.verifyComponentExistence(bs.pagaOnline);		
		bs.verifyComponentExistence(bs.dettaglioRata);
		bs.verifyComponentExistence(bs.dettaglioPiano);
		bs.verifyComponentExistence(bs.scaricaPDFPiano);
		
		logger.write("Click on Dettaglio Rata - Start");

		bs.clickComponent(bs.dettaglioRata);
		
		logger.write("Click on Dettaglio Rata - Completed");

		BolleteDettaglioRataComponent bdrc = new BolleteDettaglioRataComponent(driver);
		
		logger.write("Verify the fields in Dettaglio Rata page - Start");

//		bdrc.verifyComponentExistence(bdrc.datiAggiornati);
		
		bdrc.verifyComponentExistence(bdrc.VAIATUTTELEBOLLETTEButton);
		
		bdrc.verifyComponentExistence(bdrc.SCARICAPDFPIANOButton);
		
		bdrc.verifyComponentExistence(bdrc.gasIcon);
		
		bdrc.verifyComponentExistence(bdrc.supplyHeading);
		
		bdrc.verifyComponentExistence(bdrc.addressLabel);
		
		bdrc.verifyBilldetails(bdrc.numeroClienteLabel, BolleteDettaglioRataComponent.exp);		
		bdrc.verifyBilldetails(bdrc.dataEmissioneLabel, BolleteDettaglioRataComponent.exp);
		bdrc.verifyBilldetails(bdrc.dataScadenzaLabel, BolleteDettaglioRataComponent.exp);
		bdrc.verifyBilldetails(bdrc.dataPagamentoLabel, BolleteDettaglioRataComponent.exp);
		bdrc.verifyBilldetails(bdrc.statoPagamentoLabel, BolleteDettaglioRataComponent.exp);
		bdrc.verifyBilldetails(bdrc.modalitàdiPagamentoLabel, BolleteDettaglioRataComponent.exp);
		bdrc.verifyBilldetails(bdrc.attestazionePagamentoLabel, BolleteDettaglioRataComponent.exp);
		bdrc.verifyBilldetails(bdrc.importoRataLabel, BolleteDettaglioRataComponent.exp);
		
		bdrc.verifyComponentExistence(bdrc.totaledaPagareLabel);
		
		bdrc.verifyComponentExistence(bdrc.pagaOnline);
		
		bdrc.verifyComponentExistence(bdrc.dettaglioPiano);
				
		logger.write("Verify the fields in Dettaglio Rata page - Completed");
		
		prop.setProperty("RETURN_VALUE", "OK");
	
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
