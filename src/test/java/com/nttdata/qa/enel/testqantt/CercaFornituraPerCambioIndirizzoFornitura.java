package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ForniturePerCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CercaFornituraPerCambioIndirizzoFornitura {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			SpinnerManager spinner = new SpinnerManager(driver);
			IntestazioneNuovaRichiestaComponent intestazioneNuovaRichiesta = new IntestazioneNuovaRichiestaComponent(
					driver);

			String nrichiesta = intestazioneNuovaRichiesta.salvaNumeroRichiesta2(
					intestazioneNuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
			prop.setProperty("NUMERO_RICHIESTA", nrichiesta);
			
			logger.write("Selezione fornitura e conferma - Start");
			ForniturePerCambioIndirizzoComponent selectFornitura = new ForniturePerCambioIndirizzoComponent(driver);
			selectFornitura.cercaECliccaFornitura(prop.getProperty("POD"),
					selectFornitura.checkboxPrimaFornituraInTabella);
			selectFornitura.clickConfermaFornitura();
			logger.write("Selezione fornitura e conferma - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
		
	}
}
