package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.ServiziEBeniComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyUserDetails_303 {

 public static void main(String[] args) throws Exception {      
         Properties prop = null;
         prop = WebDriverManager.getPropertiesIstance(args[0]);
         QANTTLogger logger = new QANTTLogger(prop);
         RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
        
         try
         {
        	 GetAccountFromComponent gafc = new GetAccountFromComponent(driver);
 			
 			logger.write("typing the user CF - START");	
 			gafc.verifyComponentExistence(gafc.searchBar);
 			gafc.searchAccount(gafc.searchBar, prop.getProperty("NUMERO_CLIENTE"), true);
 			logger.write("typing the user CF - COMPLETED");
 		
        	ServiziEBeniComponent sc = new ServiziEBeniComponent(driver);
        	sc.verifyComponentExistence(sc.serviziEBeni);
        	sc.clickComponent(sc.serviziEBeni);
        	
        	sc.verifyComponentExistence(sc.bollettaWeb);
        	sc.clickComponent(sc.bollettaWeb);
        	sc.verifyComponentExistence(sc.cellulare);
        	sc.verifyComponentExistence(sc.email);
        	sc.comprareText(sc.cellulare, prop.getProperty("NUMERO"), true);
        	sc.comprareText(sc.email, prop.getProperty("EMAIL"), true);
			prop.setProperty("RETURN_VALUE", "OK");

        	 
         }catch (Exception e) {
  			prop.setProperty("RETURN_VALUE", "KO");
  			StringWriter errors = new StringWriter();
  			e.printStackTrace(new PrintWriter(errors));
  			errors.toString();
  			logger.write("ERROR_DESCRIPTION: " + errors.toString());

  			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
  			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
  				throw e;

  		} finally {
  			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
  		}
     
      }
}
