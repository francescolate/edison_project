package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ExportExcel {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			
			//Click chusura bunner pubblicitario se esistente
			log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
			
			logger.write("check the username and passsword login - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("USERNAME")); 

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
			logger.write("check the username and passsword login - Completed");
			
			logger.write("Click on the login button- Start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			By accessLoginPage=log.loginBSNSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			logger.write("Verify the page after successful login - Complete");
			
			PrivateAreaPageComponent PAC = new PrivateAreaPageComponent(driver);
			
			logger.write("Verify the Home page leftMenulist - Start");
			PAC.BusinessHomePageMenu(PAC.BusinessHomePage);
			logger.write("Verify the Home page leftMenulist - Completed");
			
		
			logger.write("Verify the contents of HomePage - Start");
			PAC.verifyComponentExistence(PAC.FornitureBollette);
			//PAC.verifyComponentExistence(PAC.ProvaAliasBSN);
			PAC.verifyComponentExistence(PAC.FornituraLuceCliente);
			PAC.verifyComponentExistence(PAC.Dettaglio);
			PAC.verifyComponentExistence(PAC.Luogo);
			PAC.verifyComponentExistence(PAC.Tiplogia);
			PAC.verifyComponentExistence(PAC.Offerata);
			PAC.verifyComponentExistence(PAC.Stato);
			PAC.verifyComponentExistence(PAC.BolletteArrow);
			logger.write("Verify the contents of HomePage - Complete");
			
			logger.write("Click on GO TO BILLS  - Start");
			PAC.clickComponent(PAC.BolletteArrow);
			logger.write("Click on GO TO BILLS  - Complete");
			
			logger.write("Verify the Bill details page and buttons  - Start");
			PAC.verifyComponentExistence(PAC.LeTueBollette);
			PAC.verifyComponentExistence(PAC.BolletteContent);
			PAC.verifyComponentExistence(PAC.BolletteContent2);
			PAC.verifyComponentExistence(PAC.InviaDocumentiBtn);
			PAC.verifyComponentExistence(PAC.MostraFiltriBtn);
			PAC.verifyComponentExistence(PAC.EsportaExcelBtn);
			logger.write("Verify the Bill details page and buttons  - Completed");
			
			
			logger.write("Click on Export Excel button  - Start");
			PAC.clickComponent(PAC.EsportaExcelBtn);
			logger.write("Click on Export Excel button  - Start");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
		

}


