package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltagePreventintoComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangePowerAndVoltagePreventivo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ChangePowerAndVoltagePreventintoComponent cc = new ChangePowerAndVoltagePreventintoComponent(driver);
			Thread.sleep(50000);
			
			logger.write("Check for Change Power and Voltage Preventinto -- Start");		
			
			cc.waitForElementToDisplay(cc.preventintoText);
			cc.verifyComponentExistence(cc.preventintoText);
			//cc.comprareText(cc.preventintoText, cc.provinica_text, true);
			cc.comprareText(cc.preventintoText, cc.preventivo_realizzato, true);				//Marco
			
			cc.verifyComponentExistence(cc.potenzoLabel);
			
			logger.write("Check for Power Value -- Start");
			cc.checkData(cc.potenzoValue, prop.getProperty("POWER_INPUT"));
			logger.write("Check for Power Value-- Completed");
			
			cc.verifyComponentExistence(cc.tensionLabel);
			
			logger.write("Check for Voltage Value -- Start");
			cc.checkData(cc.tensionValue, prop.getProperty("TENZION"));
			logger.write("Check for Voltage Value -- Completed");
			
			cc.comprareText(cc.detagliocostiTitle, cc.dettaglio_costi, true);					//Marco - uncommented
			cc.verifyComponentExistence(cc.costOfLabour);										//Marco - uncommented

			/*logger.write("Check for Cost Value -- Start");
			//cc.checkData(cc.costOfLabourValue, prop.getProperty("COSTOFLABOUR_VALUE"));
			//cc.checkData(cc.costOfLabourValue, prop.getProperty("COSTOFLABOUR_VALUE_2"));		//Marco
			cc.comprareText(cc.costOfLaourSubTxt, cc.costOfLabourText, true);					//Marco - uncommented
			cc.verifyComponentExistence(cc.cumertialBurden);									//Marco - uncommented
			cc.verifyComponentExistence(cc.powerQuote);											//Marco - uncommented
			cc.verifyComponentExistence(cc.administrativeBurden);								//Marco - uncommented
			logger.write("Check for Cost Value -- Completed");*/
			
//			cc.verifyComponentExistence(cc.executionTime);
			
//			Thread.currentThread().sleep(5000);
			
			cc.comprareText(cc.executionTimeSubText, cc.ExecutionTimeSubText, true);			//Marco - uncommented
			
			cc.comprareText(cc.executionTimeText1, cc.ExecutionTimeBottomText, true);			//Marco - uncommented
			
			cc.comprareText(cc.preventivoText2, cc.PreventivoText4, true);						//Marco - uncommented
			
			cc.verifyComponentExistence(cc.modalitàdiPagamento);
			cc.comprareText(cc.modalitàdiPagamentoText, cc.ModalitadiPagamento, true);
			cc.verifyComponentExistence(cc.addebitoInBolletta);
			cc.verifyComponentExistence(cc.voglioPagareOnline);
			cc.verifyComponentExistence(cc.checkBox);
			cc.comprareText(cc.checkBoxText, cc.CheckBoxText, true);
			cc.verifyComponentExistence(cc.informativa);
			logger.write("Click Informativa -- Start");
			cc.clickComponent(cc.informativa);
			cc.verifyComponentExistence(cc.informativaClose);
			cc.verifyComponentExistence(cc.informativaTitle);
			cc.comprareText(cc.informativaText, cc.InformativaText, true);
			cc.clickComponent(cc.informativaClose);
			logger.write("Click Informativa -- Completed");
			cc.comprareText(cc.preventivoText1, cc.PreventivoText1, true);
			cc.verifyComponentExistence(cc.email);
			
			logger.write("Check Email Value -- Start");
			cc.checkData(cc.emailInput, prop.getProperty("WP_USERNAME"));
			logger.write("Check Email Value -- Completed");
			
			cc.verifyComponentExistence(cc.cellular);
			
			logger.write("Check Telephone Value -- Start");
			cc.checkData(cc.cellularInput, prop.getProperty("CELLULAR"));
			logger.write("Check Telephone Value -- Completed");
			
			cc.comprareText(cc.preventintoText2, cc.PreventivoText2, true);
			cc.comprareText(cc.preventivoText3, cc.PreventivoText3, true);
			cc.verifyComponentExistence(cc.addressLable);
			cc.checkData(cc.address, prop.getProperty("ADDRESS"));
			cc.verifyComponentExistence(cc.PODLable);
			cc.checkData(cc.POD, prop.getProperty("POD"));
			
			logger.write("Check for Change Power and Voltage Preventinto -- Completed");			//Marco
			
			logger.write("Select Modalità di pagamento -- Start");
			cc.clickComponent(cc.voglioPagareOnline);
			cc.clickComponent(cc.checkBox);
			logger.write("Select Modalità di pagamento -- Completed");
			
			logger.write("Click Conferma -- Start");
			cc.clickComponent(cc.conferma);
			cc.comprareText(cc.modalAlert, cc.modalAlertTxet, true);
			Thread.currentThread().sleep(90000);
			cc.comprareText(cc.modalAlert, cc.modalAlert2, true);
			Thread.sleep(5000);
			cc.jsClickComponent(cc.si);
			Thread.currentThread().sleep(30000);
			cc.comprareText(cc.richesta, cc.RichestaText, true);
			cc.clickComponent(cc.fineButton);
			logger.write("Click Conferma -- Completed");
			
			HomeComponent home = new HomeComponent(driver);
			home.checkForResidentialMenu(home.RESIDENTIALMENU);

			prop.setProperty("RETURN_VALUE", "OK");

			
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
