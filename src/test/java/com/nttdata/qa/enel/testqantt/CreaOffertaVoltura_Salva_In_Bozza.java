package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CarrelloComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CreaOffertaVoltura_Salva_In_Bozza {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			//voltura
			CarrelloComponent carrello=new CarrelloComponent(driver);
			carrello.checkAfterCheckoutEffettuato(prop.getProperty("PRODOTTO"), prop.getProperty("POD"));
			logger.write("Andare nella sezione CVP e posizioanrsi sul tasto già abilitato <Confema> e fare click - Start");
			offer.clickComponent(offer.buttonConfermaCvp);
			gestione.checkSpinnersSFDC();
			offer.verificaCvp();
			logger.write("Andare nella sezione CVP e posizioanrsi sul tasto già abilitato <Confema> e fare click - Completed");
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si'  e click su 'Conferma' - Start");
			offer.verifyComponentExistence(offer.sezioneContattiEConsensi);
			offer.scrollComponent(offer.sezioneContattiEConsensi);
			TimeUnit.SECONDS.sleep(1);
		//	offer.clickComponent(offer.checkboxContattiConsensiClick);	
			TimeUnit.SECONDS.sleep(1);
			offer.clickComponent(offer.checkBoxSiContattiConsensiVolture);
			TimeUnit.SECONDS.sleep(1);
			offer.clickComponent(offer.buttonConfermaContattiConsensi);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			logger.write("Navigare fino alla sezione contatti e consensi, click su checkBox 'Si'  e click su 'Conferma' - Completed");
			logger.write("Navigare fino alla sezione Fatturazione Elettronica  e click su 'Conferma' - Start");
			offer.clickComponent(offer.buttonConfermaFatElettronica);
			
			gestione.checkSpinnersSFDC();
			logger.write("Navigare fino alla sezione Fatturazione Elettronica  e click su 'Conferma' - Completed");
			
			if (prop.getProperty("MODALITA_FIRMA","").contentEquals("DOCUMENTI VALIDI")) {
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DOCUMENTI VALIDI', canale con 'EMAIL' e popolare il campo email  e click su 'Conferma' - Start");
				offer.popolareCampo(prop.getProperty("MODALITA_FIRMA"), offer.campoModalitaFirma);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("CANALE"), offer.campoCanale);
				TimeUnit.SECONDS.sleep(1);
				offer.popolareCampo(prop.getProperty("EMAIL"), offer.campoEmail);
				TimeUnit.SECONDS.sleep(1);
				offer.clickComponent(offer.buttonConfermaModFirmaCanale);
				gestione.checkSpinnersSFDC();
				logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'DOCUMENTI VALIDI', canale con 'EMAIL' e popolare il campo email  e click su 'Conferma' - Completed");
			}
			else {
			logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'NO VOCAL'  e click su 'Conferma' - Start");
			offer.popolareCampo("DIGITAL", offer.campoModalitaFirma);
			offer.checkCampiModFirma();
			offer.popolareCampo("NO VOCAL", offer.campoModalitaFirma);
			offer.clickComponent(offer.buttonConfermaModFirmaCanale);
			gestione.checkSpinnersSFDC();
			logger.write("Navigare fino alla sezione Modalità firma e Canale Invio, popolare campo Modalita' firma con valore 'NO VOCAL'  e click su 'Conferma' - Completed");
			}
			
			logger.write("verifica che tutte le sezioni sono state confermate  e click su 'Salva in bozza' - Start");
			offer.clickComponent(offer.salvaInBozza);
			
			logger.write("verifica che tutte le sezioni sono state confermate  e click su 'Salva in bozza' - Completed");
			

			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
