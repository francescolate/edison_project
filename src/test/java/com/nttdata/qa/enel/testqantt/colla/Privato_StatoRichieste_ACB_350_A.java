package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.Privato_StatoRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_StatoRichieste_ACB_350_A {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Privato_StatoRichiesteComponent psc = new Privato_StatoRichiesteComponent(driver);
			
			logger.write("Correct dispaly of the Home Page with text- Starts");
			psc.comprareText(psc.homepageDetail1, psc.HOMEPAGE_DETAIL1, true);
			psc.comprareText(psc.homepageDetail2, psc.HOMEPAGE_DETAIL2, true);
			psc.comprareText(psc. homepageHeading, psc.HOMEPAGE_HEADING, true);
			logger.write("Correct diaplay of the Home Page with text- Ends");

			logger.write("Click on STATO RICHIESTE and correct visualization of the page validation - Starts");
			psc.clickComponent(psc.Stato_Richesthe);
			psc.verifyComponentExistence(psc.pageText);
			psc.comprareText(psc. PageText, psc.PAGETEXT, true);
			psc.comprareText(psc.statoText, psc.STATOTEXT, true);
			logger.write("Click on STAtipologiaDropDownTO RICHIESTE and correct visualization of the page validation--completed");
			
			logger.write("verify tabs on STATO RICHIESTE page - Starts");
			psc.verifyComponentExistence(psc.Contratti);
			psc.verifyComponentExistence(psc.Servizi);
			psc.verifyComponentExistence(psc. Modifiche_Contatore);
			psc.verifyComponentExistence(psc.Mostra);
			logger.write("verify tabs on STATO RICHIESTE page - Ends");
			psc.clickComponent(psc.Mostra);
			logger.write("verification of the first practice with the fields displayed-started");
			psc.verifyComponentExistence(psc.openEnergyDigital);
			psc.verifyComponentExistence(psc.Tipovalue);
			psc.verifyComponentExistence(psc.TipoLuce);
			psc.verifyComponentExistence(psc. numeroOfferta);
			psc.verifyComponentExistence(psc.numeroOffertaValue);
			psc.verifyComponentExistence(psc.POD);
			psc.verifyComponentExistence(psc.PODValue);
			psc.verifyComponentExistence(psc.DATARichiesta);
			psc.verifyComponentExistence(psc.Open_EnergyDigital);
			logger.write("verification of the first practice with the fields displayed--completed");
			logger.write("verification of button existence-started");
			psc.verifyComponentExistence(psc.Applica);
			psc.verifyComponentExistence(psc.Annulla);
			logger.write("verification of button existence-completed");
			
			psc.changeDropDownValue(psc.tipologiaDropDown, psc.tipologia);
			psc.clickComponent(psc.Annulla);
			psc.changeDropDownValue(psc.tipologiaDropDown, psc.tipologia);
			psc.clickComponent(psc.Applica);
			psc.changeDropDownValue(psc.tipologiaDropDown, psc.tipologia);
			psc.clickComponent(psc.Applica);
					psc.clickComponent(psc.Annulla);
			psc.changeDropDownValue(psc.tipologiaDropDown, psc.tipologia);
			psc.clickComponent(psc.Applica);
			psc.changeDropDownValue(psc.tipologiaDropDown, psc.tipologia);
			psc.clickComponent(psc.Applica);
			psc.clickComponent(psc.NascondiFiltri);
						
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
