package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergia_BsnComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.InfoEEModBsnExitPopUpComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ActivateInfoEnelEnergia_Bsn_Exit {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Verifying exit popup");
			logger.write("Verifying exit popup - Start");
			InfoEnelEnergia_BsnComponent infoEE = new InfoEnelEnergia_BsnComponent(driver);
			infoEE.clickComponent(infoEE.fornitureItem);
			InfoEEModBsnExitPopUpComponent popUp = new InfoEEModBsnExitPopUpComponent(driver);
			popUp.checkStringsAndElements();
			logger.write("Verifying exit popup - Completed");
			
			System.out.println("Dismiss and reload exit popup");
			logger.write("Dismiss and reload exit popup - Start");
			popUp.clickComponent(popUp.tornaProcessoBtn);
			infoEE.clickComponent(infoEE.bolletteItem);
			popUp.checkStringsAndElements();
			logger.write("Dismiss and reload exit popup - Completed");
			
			System.out.println("Exit");
			logger.write("Exit - Start");
			popUp.clickComponent(popUp.exitBtn);
//			LoginLogoutEnelCollaComponent bsnHp = new LoginLogoutEnelCollaComponent(driver);
//			//Verifying correct return to home page
//			bsnHp.verifyComponentExistence(bsnHp.loginBSNSuccessful);
			logger.write("Exit - Completed");
			
			prop.setProperty("bolletePath","Area riservataBollette");
			prop.setProperty("bolletePageTitle","Bollette");
			
			//popUp.comprareText(popUp.bolletePath, prop.getProperty("bolletePath"), true);
			//popUp.comprareText(popUp.bolletePageTitle, prop.getProperty("bolletePageTitle"), true);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
