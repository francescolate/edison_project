package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_204_ACR_Diritto_di_Portabilità {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
			
			prop.setProperty("RECOVERY_PWD_STRING_1", "Recupera password");
			prop.setProperty("RECOVERY_PWD_STRING_2", "Inserisci l'indirizzo email che hai usato per registrarti a Enel Energia. Ti invieremo le istruzioni per impostare una nuova password.");
			prop.setProperty("RECOVERY_USRNM_STRING_1", "Recupera Username");
			prop.setProperty("RECOVERY_USRNM_STRING_2", "Inserisci il tuo codice fiscale per poter recuperare il tuo username.");
			prop.setProperty("REGISTER_STRING_1", "SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE");
			prop.setProperty("REGISTER_STRING_2", "Registrati nell'area riservata");
			prop.setProperty("ACCESS_ERR_STRING_1", "Username obbligatoria");
			prop.setProperty("ACCESS_ERR_STRING_2", "Password obbligatoria");
			prop.setProperty("ACCESS_PROBLEMS_STRING", "Se hai problemi di accesso");
			
			
			/*logger.write("Accessing login page - Start");
			pac.launchLink(prop.getProperty("LINK"));
			pac.clickComponent(pac.homePageClose);
			pac.verifyComponentExistence(pac.logoEnel);
			pac.verifyComponentExistence(pac.buttonAccetta);
			pac.clickComponent(pac.buttonAccetta);
			pac.verifyComponentExistence(pac.iconUser);
			pac.clickComponent(pac.iconUser);
			logger.write("Accessing login page - Completed");
			
			logger.write("Verifying username and password fields - Start");
			pac.verifyComponentVisibility(pac.username);
			pac.verifyComponentVisibility(pac.password);
			logger.write("Verifying username and password fields - Completed");
			
			logger.write("Verifying recovery password and username links - Start");
			pac.verifyComponentVisibility(pac.recoveryPassword);
			pac.verifyComponentVisibility(pac.recoveryUsername);
			logger.write("Verifying recovery password and username links - Completed");
			
			logger.write("Verifying access problems link - Start");
			pac.verifyComponentVisibility(pac.accessProblemsLabel);
			pac.verifyComponentText(pac.accessProblemsLabel, prop.getProperty("ACCESS_PROBLEMS_STRING"));
			logger.write("Verifying access problems link - Completed");
			
			logger.write("Verifying social access buttons - Start");
			pac.verifyComponentVisibility(pac.googleAccessButton);
			pac.verifyComponentVisibility(pac.facebookAccessButton);
			logger.write("Verifying social access buttons - Completed");
					
			logger.write("Verifying registration elements - Start");
			pac.verifyComponentVisibility(pac.noAccountLabel);
			pac.verifyComponentVisibility(pac.registerButton);
			pac.verifyComponentVisibility(pac.accessProblemsLink);
			logger.write("Verifying registration elements - Completed");
			
			logger.write("Enter the username and password  - Start");
			pac.enterLoginParameters(pac.username, prop.getProperty("USERNAME"));
			pac.enterLoginParameters(pac.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			pac.verifyComponentVisibility(pac.buttonLoginAccedi);
			pac.clickComponent(pac.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");*/
			
			logger.write("Verify the privato home page title and contents   - Start");
			Thread.sleep(4000);
			pac.verifyComponentExistence(pac.BenvenutoTitle);
			pac.verifyComponentText(pac.BenvenutoTitle,PrivateAreaPageComponent.BENVENUTO_TITLE );
			pac.verifyComponentExistence(pac.BenvenutoContent);
			pac.verifyComponentText(pac.BenvenutoContent, PrivateAreaPageComponent.BENVENUTO_CONTENT);
			pac.verifyComponentExistence(pac.fornitureHeader);
			pac.verifyComponentText(pac.fornitureHeader, PrivateAreaPageComponent.FORNITURE_HEADER);
			pac.verifyComponentExistence(pac.fornitureContent);
			pac.verifyComponentText(pac.fornitureContent, PrivateAreaPageComponent.FORNITURE_CONTENT);
			logger.write("Verify the privato home page title and contents   - Complete");
			
			
			logger.write("Verify and click on tuoiDiritti link  - Start");
			pac.verifyComponentExistence(pac.tuoiDirittiLink);
			pac.clickComponent(pac.tuoiDirittiLink);
			logger.write("Verify and click on tuoiDiritti link  - Complete");
			
			logger.write("Verify diPortabilità title and contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitàTitle);
			pac.verifyComponentText(pac.diPortabilitàTitle, PrivateAreaPageComponent.DIPORTABILITA_TITLE);
			pac.verifyComponentExistence(pac.tuoDirittiQues1);
			pac.verifyComponentText(pac.tuoDirittiQues1, PrivateAreaPageComponent.TUOIDIRITTI_QUES1);
			pac.verifyComponentExistence(pac.tuoDirittiAns1);
			pac.verifyComponentText(pac.tuoDirittiAns1, PrivateAreaPageComponent.TUOIDIRITTI_ANS1);
			logger.write("Verify diPortabilità title and contents - Complete");
			
			logger.write("Verify question and answer contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitaLastQues);
			pac.verifyComponentText(pac.diPortabilitaLastQues, PrivateAreaPageComponent.DIPORTABILITA_LASTQUES);
			pac.verifyComponentExistence(pac.diPortabilitaLastAns);
			//pac.verifyComponentText(pac.diPortabilitaLastAns, PrivateAreaPageComponent.DIPORTABILITA_LASTANS);
			logger.write("Verify question and answer contents - Complete");
			
			logger.write("Click on AccediServizo button - Start");
			pac.verifyComponentExistence(pac.accediAlServizio);
			pac.clickComponent(pac.accediAlServizio);
			logger.write("Click on AccediServizo button - Complete");
			
			Thread.sleep(5000);
			
			logger.write("Verify the DiPortabilita contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitaContent);
			pac.verifyComponentText(pac.diPortabilitaContent, PrivateAreaPageComponent.DIPORTBILITTA_CONTENT);
			logger.write("Verify the DiPortabilita contents - Start");
			
			logger.write("Verify the Dirrito contents - Start");
			pac.verifyComponentExistence(pac.dirritoContent1);
			pac.compareText(pac.dirritoContent1, PrivateAreaPageComponent.DIRRITO_CONTENT1, true);
			pac.verifyComponentExistence(pac.dirritoContent2);
			pac.compareText(pac.dirritoContent2, PrivateAreaPageComponent.DIRRITO_CONTENT2, true);
			logger.write("Verify the Dirrito contents - Complete");
			
			logger.write("Verify the Scarica link and title - Start");
			pac.verifyComponentExistence(pac.scaricaLinkTitle);
			pac.compareText(pac.scaricaLinkTitle, PrivateAreaPageComponent.SCARICALINK_TITLE, true);
			pac.verifyComponentExistence(pac.scaricaLink1);
			//pac.clickComponent(pac.scaricaLink);
			logger.write("Verify the Scarica link and title - Complete");
			
			
			logger.write("Verify esci and richiedi button - Start");
			pac.verifyComponentExistence(pac.esciButton);
			pac.verifyComponentExistence(pac.richiediButton);
			logger.write("Verify esci and richiedi button - Complete");
			
			logger.write("Click on richiedi button - Start");
			pac.clickComponent(pac.richiediButton);
			//pac.verifyComponentExistence(pac.esciButton);
			Thread.sleep(10000);
			logger.write("Verify esci and richiedi button - Complete");
			
			logger.write("Verify the operazione  title - Start");
			pac.verifyComponentExistence(pac.operazioneTitlte);
			logger.write("Verify the operazione  title - Complete");	
			
			logger.write("Verify the Home page title and contents - Start");
			pac.clickComponent(pac.fineButton);
			pac.verifyComponentExistence(pac.fornitureHeader);
			pac.compareText(pac.fornitureHeader, PrivateAreaPageComponent.FORNITURE_HEADER, true);
			pac.verifyComponentExistence(pac.fornitureContent);
			pac.compareText(pac.fornitureHeader,PrivateAreaPageComponent.FORNITURE_CONTENT , true);
			logger.write("Verify the Home page title and contents - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");	
			
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
