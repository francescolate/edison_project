package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.HomePageResidentialComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.NovitaComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaAccountComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.components.colla.SupplyDetailComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Home_Prospect_ACB_207 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomePageResidentialComponent hpr = new HomePageResidentialComponent(driver);
			PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);

			logger.write("Correct visualization of the Home Page Menu Items text -- Starts");		
			hpr.comprareText(hpr.Forniture, HomePageResidentialComponent.FornitureMenuText, true);
			//hpr.comprareText(hpr.EnelpremiaWow, HomePageResidentialComponent.EnelpremiaMenuText, true);
			hpr.comprareText(hpr.Novita, HomePageResidentialComponent.NovitaMenuText, true);
			hpr.comprareText(hpr.Account, HomePageResidentialComponent.AccountMenuText, true);
			hpr.comprareText(hpr.itu, HomePageResidentialComponent.ItuMenuText, true);
			hpr.comprareText(hpr.Supporto, HomePageResidentialComponent.SupportoMenuText, true);
			hpr.comprareText(hpr.TrovaSpazioEnel, HomePageResidentialComponent.TrovaMenuText, true);
			hpr.comprareText(hpr.Esci, HomePageResidentialComponent.EsciMenuText, true);
			//hpr.comprareText(hpr.chiamaci, HomePageResidentialComponent.ChiamaciMenuText, true);
			logger.write("Correct visualization of the Home Page Menu Items text -- Ends");
			
			String currentHandle ="";
			Set <String> windows = driver.getWindowHandles();
			currentHandle = driver.getWindowHandle();			
			for (String winHandle : windows) {
				driver.switchTo().window(winHandle);
			}
			hpr.clickComponent(hpr.Forniture);	
			pac.checkUrl(prop.getProperty("FORNITURE_URL"));
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			HomeComponent home = new HomeComponent(driver);
			prop.setProperty("ACTIVESUPPLY", "Vuoi attivare una nuova fornitura Scopri le offerte per la tua casa");
			home.checkForContent(home.activateSupplyText1, home.activeSupplyText2, prop.getProperty("ACTIVESUPPLY"));
			logger.write("Correct visualization of the Home Page with central text -- Ends");

			logger.write("Click on EnelpremiaWow and verify the page -- Starts");
			hpr.clickComponent(hpr.EnelpremiaWow);
			pac.checkUrl(prop.getProperty("ENELPREMIA_URL"));
			hpr.verifyComponentExistence(hpr.enelpremiaWowtext1);
			hpr.comprareText(hpr.enelpremiaWowtext1, HomePageResidentialComponent.EnelpremiaWowtext1, true);
			hpr.comprareText(hpr.enelpremiaWowtext2, HomePageResidentialComponent.EnelpremiaWowtext2, true);
			//hpr.comprareText(hpr.enelpremiaWowtext3, HomePageResidentialComponent.EnelpremiaWowtext3, true);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			By icon = log.iconUserButton;
			Thread.sleep(2000);
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("Click on EnelpremiaWow and verify the page -- Ends");

			logger.write("Click on Novita and verify the page -- Starts");
			hpr.clickComponent(hpr.Novita);
			pac.checkUrl(prop.getProperty("NOVITA_URL"));
			NovitaComponent nc = new NovitaComponent(driver);
			nc.comprareText(nc.titleSubText, NovitaComponent.TitleSubText, true);
			//hpr.clickComponent(hpr.novitaCardLink);
			logger.write("Click on Novita and verify the page -- Ends");
			
			logger.write("Click on Account and verify the page -- Starts");
			Thread.sleep(2000);
			driver.switchTo().window(currentHandle);
			Thread.sleep(2000);
			hpr.clickComponent(hpr.Account);
			pac.checkUrl(prop.getProperty("ACCOUNT_URL"));
			PrivateAreaAccountComponent paac = new PrivateAreaAccountComponent(driver);
			paac.comprareText(paac.datiAndContattiHeader, PrivateAreaAccountComponent.ACCOUNT_HEADER_TEXT, true);
			paac.comprareText(paac.accountTitolare, PrivateAreaAccountComponent.AccountTitolare, true);
			paac.comprareText(paac.accountCodiceFiscale, PrivateAreaAccountComponent.AccountCodiceFiscale, true);
			paac.comprareText(paac.accountTelefonoFisso, PrivateAreaAccountComponent.AccountTelefonoFisso, true);
			paac.comprareText(paac.accountNumerodiCellulare, PrivateAreaAccountComponent.AccountNumerodiCellulare, true);
			paac.comprareText(paac.accountEmail, PrivateAreaAccountComponent.AccountEmail, true);
			paac.comprareText(paac.accountPEC, PrivateAreaAccountComponent.AccountPEC, true);
			paac.comprareText(paac.accountCanale, PrivateAreaAccountComponent.AccountCanale, true);
			logger.write("Click on Account and verify the page -- Ends");

			logger.write("Click on Itu and verify the page -- Starts");
			hpr.clickComponent(hpr.itu);
			Thread.sleep(2000);
			pac.checkUrl(prop.getProperty("ITUOI_URL"));
			dpc.comprareText(dpc.section1PageHeading1, DirittodiPortabilitaComponent.SECTION1_HEADING1, true);
			dpc.comprareText(dpc.section1PageHeading2, DirittodiPortabilitaComponent.SECTION1_HEADING2, true);
			dpc.comprareText(dpc.dirittoDiPortabilitaTitle1, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_TITLE1, true);
			dpc.comprareText(dpc.dirittoDiPortabilitaTitle2, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_TITLE2, true);
			Thread.sleep(2000);
			//dpc.comprareText(dpc.dirittoDiPortabilitaText, DirittodiPortabilitaComponent.DIRITTO_DI_PORTABILITA_TEXT, true);
			logger.write("Click on Itu and verify the page -- Ends");

			logger.write("Click on Supporto and verify the page -- Starts");
			SupplyDetailComponent sdc = new SupplyDetailComponent(driver);
			hpr.clickComponent(hpr.Supporto);
			sdc.verifyComponentExistence(sdc.ScopriButton);
			sdc.clickComponent(sdc.ScopriButton);
			Thread.sleep(2000);
			pac.checkUrl(prop.getProperty("SUPPORTO_URL"));
			SupportoSectionComponent ss = new SupportoSectionComponent(driver);
			ss.comprareText(ss.pageTitle, SupportoSectionComponent.PageTitle, true);
			ss.comprareText(ss.titleSubText, SupportoSectionComponent.PageTitleSubText, true);
			hpr.clickComponent(hpr.iconUser);
			logger.write("Click on Supporto and verify the page -- Ends");

			logger.write("Click on TrovaSpazioEnel and verify the page -- Starts");
			hpr.clickComponent(hpr.TrovaSpazioEnel);
			//hpr.clickComponent(hpr.TrovaSiButton);
			dpc.switchToNewTab();
			pac.checkUrl(prop.getProperty("TRAVOSPAZIO_URL"));
			Thread.sleep(2000);
			pac.compareText(pac.TravoSpazioHome, PrivateAreaPageComponent.TRAVOSPAZIO_HOMENew, true);
			pac.verifyComponentExistence(pac.TravoSpazioTitle);
			pac.compareText(pac.TravoSpazioTitle, PrivateAreaPageComponent.TRAVOSPAZIO_TITLE, true);
			logger.write("Click on TrovaSpazioEnel and verify the page -- Ends");

			logger.write("Click on Esci and verify the page goes to main -- Starts");
			//driver.switchTo().window(currentHandle);
			hpr.clickComponent(hpr.iconUser);
			Thread.sleep(2000);
			hpr.clickComponent(hpr.Esci);
			//hpr.verifyComponentExistence(hpr.iconUser);
			logger.write("Click on Esci and verify the page goes to main -- Ends");
			
			/*logger.write("Click on chiamaci and verify the page -- Starts");
			driver.switchTo().window(currentHandle);
			hpr.clickComponent(hpr.chiamaci);
			dpc.switchToNewTab();
			pac.checkUrl(prop.getProperty("CHIAMACI_URL"));
			hpr.comprareText(hpr.chiamaciText1, HomePageResidentialComponent.ChiamaciText1, true);
			hpr.comprareText(hpr.chiamaciText2, HomePageResidentialComponent.ChiamaciText2, true);
			logger.write("Click on chiamaci and verify the page -- Ends");*/

			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
