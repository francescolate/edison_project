package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato211ACRDisattivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_216_ACR_Disattivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
						
			Privato211ACRDisattivazioneComponent dzcp = new Privato211ACRDisattivazioneComponent(driver);
			
			dzcp.verifyComponentVisibility(dzcp.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = dzcp.homePageCentralText;
			dzcp.verifyComponentExistence(homePage);
			dzcp.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
			
			By homepageSubheading = dzcp.homePageCentralSubText;
			dzcp.verifyComponentExistence(homepageSubheading);
			dzcp.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = dzcp.sectionTitle;
			dzcp.verifyComponentExistence(sectionTitle);
			dzcp.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = dzcp.sectionTitleSubText;
			dzcp.verifyComponentExistence(sectionTitleSubText);
			dzcp.VerifyText(sectionTitleSubText, dzcp.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verify and Click on left menu item 'SERVIZI' - Start");
			By servizi = dzcp.serviziSelect;
			dzcp.verifyComponentExistence(servizi);
			dzcp.clickComponent(servizi);
			logger.write("Verify and Click on left menu item 'SERVIZI' - Completed");
			
			dzcp.verifyComponentVisibility(dzcp.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = dzcp.serviziPage;
			dzcp.verifyComponentExistence(serviziHomePage);
			dzcp.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Start");
			By serviziPageSectionTitle= dzcp.serviziSectionTitle;
			dzcp.verifyComponentExistence(serviziPageSectionTitle);
			dzcp.VerifyText(serviziPageSectionTitle, prop.getProperty("SERVIZI_HOMEPAGE_SECTIONTITLE"));
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Completed");
			
			By serviziPage_disattivazioneFornituraItem = dzcp.disattivazioneFornituraItem;
			dzcp.verifyComponentExistence(serviziPage_disattivazioneFornituraItem);
			dzcp.clickComponent(serviziPage_disattivazioneFornituraItem);
			logger.write("Validating and clicking on Disattivazione Forniture button - Completed");
			Thread.sleep(5000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornitura);
			
			logger.write("User is navigated to home page of DISATTIVAZIONE_FORNITURA_PAGE - Start");
			By disattivazioneFornituraPageHeading = dzcp.disattivazioneFornitura;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageHeading);
			dzcp.VerifyText(disattivazioneFornituraPageHeading, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_HEADING"));
			logger.write("User is navigated to home page of DISATTIVAZIONE_FORNITURA_PAGE - Completed");
			
			logger.write("Validating the presence of Domande Frequenti Section- Start");
			By domandeFrequentiSection = dzcp.domandeFrequentiSection;
			dzcp.verifyComponentExistence(domandeFrequentiSection);
			dzcp.VerifyText(domandeFrequentiSection, prop.getProperty("DOMANDE_FREQUENTI_HEADING"));
			logger.write("Validating the presence of Domande Frequenti Section- Completed");
			
			logger.write("Validating Domande Frequenti Section- Start");
			dzcp.checkDomandeFrequentiSectionFaqDetails();
			logger.write("Validating Domande Frequenti Section- Completed");
			
			logger.write("Verify and click on PROSEGUI_CON_LA_DISATTIVAZIONE_button - Start");
			By PROSEGUI_CON_LA_DISATTIVAZIONE_button = dzcp.PROSEGUI_CON_LA_DISATTIVAZIONE_button;
			dzcp.verifyComponentExistence(PROSEGUI_CON_LA_DISATTIVAZIONE_button);
			dzcp.clickComponent(PROSEGUI_CON_LA_DISATTIVAZIONE_button);
			logger.write("Verify and click on PROSEGUI_CON_LA_DISATTIVAZIONE_button - Completed");
			Thread.sleep(25000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornituraPageTitle);
			
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Start");
			By disattivazioneFornituraPageTitle = dzcp.disattivazioneFornituraPageTitle;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageTitle);
			dzcp.VerifyText(disattivazioneFornituraPageTitle, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_TITLE"));
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Completed");
			
			logger.write("In New page Validate the page title subtext of DISATTIVAZIONE_FORNITURA - Start");
			By disattivazioneFornituraPageTitleSubText = dzcp.disattivazioneFornituraPageTitleSubText;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageTitleSubText);
			dzcp.VerifyText(disattivazioneFornituraPageTitleSubText, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_TITLE_SUBTEXT"));
			logger.write("In New page Validate the page title subtext of DISATTIVAZIONE_FORNITURA - Completed");
						
			logger.write("Validating the DISATTIVAZIONE_FORNITURA dailog box content- Start");
			By dailogBox = dzcp.dailogBox;
			dzcp.verifyComponentExistence(dailogBox);
			dzcp.validateDailogBoxContent(dailogBox, dzcp.dailogBoxContent);
			logger.write("Validating the DISATTIVAZIONE_FORNITURA dailog box content- Completed");
			
			logger.write("Validating the Supply for Gas Commodity - Start");
			
			By checkbox_00800006885570 = dzcp.pdrCheckbox_00800006885570;
			dzcp.verifyComponentExistence(checkbox_00800006885570);
			
			By IconFlame_00800006885570 = dzcp.pdrIconFlame_00800006885570;
			dzcp.verifyComponentExistence(IconFlame_00800006885570);
			
			By pdrHeading_00800006885570 = dzcp.pdrText_00800006885570;
			dzcp.verifyComponentExistence(pdrHeading_00800006885570);
			
			By pdrValue_00800006885570 = dzcp.pdrValue_00800006885570;
			dzcp.verifyComponentExistence(pdrValue_00800006885570);
			dzcp.VerifyText(pdrValue_00800006885570, prop.getProperty("PDR_VALUE"));
			
			By indrizzodellaFornituraHeading__00800006885570 = dzcp.indrizzodellafornitura_text_00800006885570;
			dzcp.verifyComponentExistence(indrizzodellaFornituraHeading__00800006885570);
			
			By indrizzodellaFornituraValue__00800006885570 = dzcp.indrizzodellafornitura_value_00800006885570;
			dzcp.verifyComponentExistence(indrizzodellaFornituraValue__00800006885570);
			
			By modalitaDiRichiestaHeading__00800006885570 = dzcp.modalitadirichiesta_text_00800006885570;
			dzcp.verifyComponentExistence(modalitaDiRichiestaHeading__00800006885570);
			
			By modalitaDiRichiestaValue__00800006885570 = dzcp.modalitadirichiesta_value_00800006885570;
			dzcp.verifyComponentExistence(modalitaDiRichiestaValue__00800006885570);
			
			logger.write("Validating the Supply for Gas Commodity - Completed");	
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			


}