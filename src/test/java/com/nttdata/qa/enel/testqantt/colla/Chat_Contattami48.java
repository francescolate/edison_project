package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.colla.ContattaciBannerComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Chat_Contattami48 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			BaseComponent bc = new BaseComponent(driver);
			ContattaciBannerComponent cbc = new ContattaciBannerComponent(driver);

			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			Thread.sleep(10000);
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			logger.write("select drop down value - Start");
			pc.changeDropDownValue(pc.productDropDown, pc.luce);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, pc.visualizza);
			logger.write("select drop down value - Completed");

			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			pc.waitForElementToDisplay(pc.dettaglioOfferta);
			Thread.sleep(10000);
			pc.jsClickObject(pc.dettaglioOfferta);
			Thread.sleep(30000);			
		//	pc.clickComponent(pc.closeBtn);
			
			cbc.clickComponent(cbc.contattaci);
			Thread.sleep(2000);
			cbc.clickComponent(cbc.boxOffer);
			Thread.sleep(2000);
			cbc.clickComponent(cbc.BoxCallme);
			Thread.sleep(2000);
			cbc.insertText(cbc.telefonoInput, "3333313132");
			cbc.verifyComponentExistence(cbc.fasciaOraria);
			cbc.clickComponent(cbc.richiamami);
			Thread.sleep(2000);
			cbc.compareText(cbc.successText, cbc.successTextText, true);
			
			
			
//			pc.waitForElementToDisplay(pc.bannerPhrase);			
//			//pc.verifyComponentExistence(pc.banner);
//			//pc.comprareText(pc.bannerPhrase, pc.Banner, true);
//			
//			logger.write("Click on Banner and verify the message - Start");
//			//pc.clickComponent(pc.banner);	
//			pc.clickComponent(pc.bannerPhrase);			
//			Thread.sleep(10000);
//			pc.waitForElementToDisplay(pc.leNostreOfferte);			
//			pc.clickComponent(pc.leNostreOfferte);	
//			pc.waitForElementToDisplay(pc.chiamamiGratis);			
//			pc.clickComponent(pc.chiamamiGratis);		
//
//			//pc.comprareText(pc.bannerText1, pc.BannerTitle, true);
//			pc.verifyComponentExistence(pc.telefono);
//			pc.verifyComponentExistence(pc.fasciaOraria);
//			//pc.comprareText(pc.bannerText2, pc.BannerText, true);
//			logger.write("Click on Banner and verify the message - Completed");
//
//			logger.write("Enter telefono and fascia oraria - Start");
//			prop.setProperty("TELEFONO", "3333313132");
//			pc.enterInputParameters(pc.telefono, prop.getProperty("TELEFONO"));
//			Thread.sleep(1000);
//			pc.clickComponent(pc.fasciaOraria);
//			Thread.sleep(1000);
//			bc.simulateDownArrowKey(pc.fasciaOraria);
//			bc.simulateEnterKey(pc.fasciaOraria);
//			//pc.changeFasciaOrariaValue(pc.fasciaOraria, pc.fasciaOrariaInput);
//			logger.write("Enter telefono and fascia oraria - Completed");
//
//			logger.write("Click on chiamamigratis - Start");
//			pc.clickComponent(pc.chiamamigratis);
//			logger.write("Click on chiamamigratis - Completed");
//
//			pc.verifyComponentExistence(pc.responseMsg);
//			//pc.checkTextValue(pc.responseMsg, pc.ResponseMessage);
//			//pc.checkForData(pc.telefonoValue, prop.getProperty("TELEFONO"));
//			//pc.checkTelefonoValue(pc.telefonoValue, prop.getProperty("TELEFONO"));
//			//pc.checkForFasciaValue(pc.fasciaOrariaValue);
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
