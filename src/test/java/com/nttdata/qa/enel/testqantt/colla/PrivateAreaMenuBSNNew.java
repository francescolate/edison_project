package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CheckPrivateAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaMenuBSNNew {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("checking the menu located on private area left side page");
			CheckPrivateAreaComponent cpac = new CheckPrivateAreaComponent(driver);
			cpac.checkDocumentReadyState();
			Thread.currentThread().sleep(5000);
			if(prop.containsKey("ACCOUNT_TYPE"))
				if(prop.getProperty("ACCOUNT_TYPE").equals("BSN"))
					cpac.verifyBSNMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_BSN);
				else if(prop.getProperty("ACCOUNT_TYPE").equals("RES_LIKE"))
					cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_RES_LIKE);
				else if(prop.getProperty("ACCOUNT_TYPE").equals("NO_RES"))
					cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_NO_RES);
				else if(prop.getProperty("ACCOUNT_TYPE").equals("BSN_LIKE"))
					cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_BSN_LIKE);
				else if(prop.getProperty("ACCOUNT_TYPE").equals("RES_FOR_ACTIVE"))
					cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_RES_FOR_ACTIVE);
				else
					cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_RES);
			else
				cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_RES);
			logger.write("private area left menu checks: completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
