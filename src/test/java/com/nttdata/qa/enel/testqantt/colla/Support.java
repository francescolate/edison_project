package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerVoltageSectionComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Support {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Thread.sleep(10000);

			TopMenuComponent menu=new TopMenuComponent(driver);
			SupportoSectionComponent supporto=new SupportoSectionComponent(driver);
			logger.write("click sul tab 'SUPPORTO' e check testo nell' header della pagina web "+prop.getProperty("LINK")+supporto.linkSUPPORTO+" - Start");
			menu.sceltaMenu("Supporto");
			
			supporto.checkUrl(prop.getProperty("LINK")+supporto.linkSUPPORTO);
			By headerDescription_Supporto=supporto.headerDescriptionSupporto;
			By homeSupporto_Path=supporto.homeSupportoPath;
			By supporto_Title=supporto.supportoTitle;
			By supporto_Subtitle=supporto.supportoSubtitle;
			
			supporto.checkHeaderPageSupporto(headerDescription_Supporto,homeSupporto_Path,supporto_Title,supporto_Subtitle);
			logger.write("click sul tab 'SUPPORTO' e check testo nell' header della pagina web "+prop.getProperty("LINK_SUPPORTO")+" - Completed");
			
			logger.write("click sul link 'Contratti e modulistica' e check dei suoi link - Start");
			By contrattiAndModulistica_Link=supporto.ContrattiAndModulisticaLink;
			supporto.clickComponent(contrattiAndModulistica_Link);
						
			By contrattiAndModulistica_Links=supporto.linksOfContrattiAndModulistica;
			supporto.checklinksOnContrattiAndModulisticaSection(contrattiAndModulistica_Links);
			logger.write("click sul link 'Contratti e modulistica' e check dei suoi link - Completed");
			
			logger.write("click sull'opzione 'Modifica potenza e tensione' - Start");
			ChangePowerVoltageSectionComponent powerChange=new ChangePowerVoltageSectionComponent(driver);
			By modificaPotenzaETensione_Button=powerChange.modificaPotenzaETensioneButton;
			powerChange.clickComponent(modificaPotenzaETensione_Button);
				
			powerChange.checkUrl(prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA);
									
			By headerDescriptionCambioPotenzaContatore_Page=powerChange.headerDescriptionCambioPotenzaContatorePage;
			By homePotenzaTensione_Path=powerChange.homePotenzaTensionePath;
			By potenzaTensione_Title=powerChange.potenzaTensioneTitle;
			By potenzaTensione_Subtitle=powerChange.potenzaTensioneSubtitle;
			
			powerChange.checkHeaderCambioPotenzaContatorePage(headerDescriptionCambioPotenzaContatore_Page,homePotenzaTensione_Path,potenzaTensione_Title,potenzaTensione_Subtitle);
			logger.write("click sull'opzione 'Modifica potenza e tensione' - Completed");
			
			logger.write("verifica presenza testo sulla pagina web con url '"+prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA+"' - Start");
			By textCambioPotenzaContatore_Page=powerChange.textCambioPotenzaContatorePage;
			powerChange.checkTextCambioPotenzaContatorePage(textCambioPotenzaContatore_Page);
			logger.write("verifica presenza testo sulla pagina web con url '"+prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA+"' - Completed");
			
			logger.write("sulla pagina web con url  '"+prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA+"' e' presente la sezione con inputBoxes email,password, button ACCEDI, label con descrizione 'Non sei registrato?', link Registrati - Start"); 
			By accediAreaRiservata_Label=powerChange.accediAreaRiservataLabel;
			powerChange.verifyComponentExistence(accediAreaRiservata_Label);
			
			By email_Label=powerChange.emailLabel;
			powerChange.verifyComponentExistence(email_Label);
			
			By email_Inputtext=powerChange.emailInputtext;
			powerChange.verifyComponentExistence(email_Inputtext);
			
			By password_Label=powerChange.passwordLabel;
			powerChange.verifyComponentExistence(password_Label);
			
			By password_Inputtext=powerChange.passwordInputtext;
			powerChange.verifyComponentExistence(password_Inputtext);
			
			By accedi_Button=powerChange.accediButton;
			powerChange.verifyComponentExistence(accedi_Button);
			
			By notregistered_Label=powerChange.notregisteredLabel;
			powerChange.verifyComponentExistence(notregistered_Label);
			
			By registrati_Link=powerChange.registratiLink;
			powerChange.verifyComponentExistence(registrati_Link);
			logger.write("sulla pagina web con url  '"+prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA+"' e' presente la sezione con inputBoxes email,password, button ACCEDI, label con descrizione 'Non sei registrato?', link Registrati - Completed"); 
			
			logger.write("click sul link 'REGISTRATI' e check sua area - Start");
			powerChange.clickComponent(registrati_Link);
			powerChange.checkUrl(prop.getProperty("LINK")+powerChange.linkREGISTRATI);
			
			By headerDescription_RegistrazionePage=powerChange.headerDescriptionRegistrazionePage;
			powerChange.checkHeaderRegistratiPage(headerDescription_RegistrazionePage);
			
			By continua_Button=powerChange.continuaButton;
			powerChange.notclickableVerify(continua_Button, prop.getProperty("LINK")+powerChange.linkREGISTRATI);
			logger.write("click sul link 'REGISTRATI' e check sua area - Completed");
			
			logger.write("back del browser alla pagina web con link '"+prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA+"' - Start");
			driver.navigate().back();
			
			powerChange.checkUrl(prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA);
			powerChange.checkHeaderCambioPotenzaContatorePage(headerDescriptionCambioPotenzaContatore_Page,homePotenzaTensione_Path,potenzaTensione_Title,potenzaTensione_Subtitle);
			logger.write("back del browser alla pagina web con link '"+prop.getProperty("LINK")+powerChange.linkCAMBIO_POTENZA+"' - Completed");	
			
			logger.write("verifica presenza button 'VAI ALL'AREA CLIENTI'- Start");
			By vaiAreaClienti_Button=powerChange.vaiAreaClientiButton;
			powerChange.scrollComponent(vaiAreaClienti_Button);
			powerChange.verifyComponentExistence(vaiAreaClienti_Button);
			logger.write("verifica presenza button 'VAI ALL'AREA CLIENTI'- Completed");
		
			logger.write("verifica presenza della sezione 'Domande Frequenti' - Start");
			By questions_List=powerChange.questionsList;
			powerChange.scrollComponent(questions_List);
			powerChange.checkfrequentlyquestionssection(questions_List);
			
			By plus_image=powerChange.plusImage;
			powerChange.checkPlusImage(plus_image,prop.getProperty("LINK"));
			logger.write("verifica presenza della sezione 'Domande Frequenti' - Completed");
			
			logger.write("verifica presenza del testo di risposta a ciascuna delle domande frequenti -  Start");
			By questions_Details=powerChange.questionsDetails;
			powerChange.checkQuestionsDetails(questions_Details, powerChange.domande, powerChange.risposte);
			logger.write("verifica presenza del testo di risposta a ciascuna delle domande frequenti -  Completed");
			
			logger.write("accesso all'area clienti dalla pagina web '"+prop.getProperty("LINK_CAMBIO_POTENZA")+"' - Start");
			powerChange.scrollComponent(email_Inputtext);
			powerChange.enterLoginParameters(email_Inputtext,prop.getProperty("USERNAME_RES"));
			powerChange.enterLoginParameters(password_Inputtext,prop.getProperty("PASSWORD_RES"));
			powerChange.clickComponent(vaiAreaClienti_Button);
			logger.write("accesso all'area clienti dalla pagina web '"+prop.getProperty("LINK_CAMBIO_POTENZA")+"' - Completed");
		  
			logger.write("accesso all'area riservata - Start");
			LoginLogoutEnelCollaComponent login=new LoginLogoutEnelCollaComponent(driver);
			By loginLabel=login.accediAMyEnelLabel;
		    login.verifyComponentExistence(loginLabel);
		    
		    By user = login.username;
		    login.verifyComponentExistence(user);
		    login.enterLoginParameters(user, prop.getProperty("USERNAME_RES")); 

			By pw = login.password;
			login.verifyComponentExistence(pw);
			login.enterLoginParameters(pw, prop.getProperty("PASSWORD_RES"));

			By accedi = login.buttonLoginAccedi;
			login.verifyComponentExistence(accedi);
			login.clickComponent(accedi);
						
			By textOnVoltagePower_Page=powerChange.textOnVoltagePowerPage;
			By modificapotenzatensione_Title=powerChange.modificapotenzatensioneTitle;
			
			powerChange.checkTextOnVoltagePowerPage(modificapotenzatensione_Title,textOnVoltagePower_Page);
			logger.write("accesso all'area riservata - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
