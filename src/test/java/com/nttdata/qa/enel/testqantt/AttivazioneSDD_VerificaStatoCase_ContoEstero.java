package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AttivazioneSDD_VerificaStatoCase_ContoEstero {
	/**
	 *  verifica lo stato del case creato a valle del completamento del processo di ATTIVAZIONE SDD
	 *  @param NUMERO_RICHIESTA
	 *  @param ATT_SDD_STATO_ATTESO_CASE
	 *  @param ATT_SDD_SOTTOSTATO_ATTESO_CASE
	 *	@param ATT_SDD_STATO_ATTESO_SAP
	 *	@param	ATT_SDD_STATO_ATTESO_UDB"

	 */
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			String query="SELECT CaseNumber, Status, ITA_IFM_SubStatus__c, Subject  FROM Case WHERE CaseNumber = '@ID_RICHIESTA@'";
			
			String idRichiesta=prop.getProperty("NUMERO_RICHIESTA");
			query=query.replace("@ID_RICHIESTA@", idRichiesta);



			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);
		
			Map<String,String> result=wb.EseguiQuery(query).get(1);
			

			String subject=result.get("Subject");
			String Case_CaseNumber	=result.get("CaseNumber");
			String Case_Status	=result.get("Status");
			String Case_ITA_IFM_SubStatus__c=result.get("ITA_IFM_SubStatus__c");



			logger.write("Start Verifica Stato Case");
			String statoAttesoCase=prop.getProperty("ATT_SDD_STATO_ATTESO_CASE") ;
			String sottoStatoAttesoCase=prop.getProperty("ATT_SDD_SOTTOSTATO_ATTESO_CASE") ;
			String subjectCaseAtteso="ATTIVAZIONE SDD";

			
			
			if(!subject.equalsIgnoreCase(subjectCaseAtteso))
			{
				throw new Exception("Valore non corrispondente con valore atteso ");
			}

			if(!statoAttesoCase.equalsIgnoreCase(Case_Status))
			{

				throw new Exception(String.format("Stato Case - Valore: %s - Valore atteso: %s",Case_Status,statoAttesoCase));
			}
			if(!sottoStatoAttesoCase.equalsIgnoreCase(Case_ITA_IFM_SubStatus__c))
			{
				throw new Exception(String.format("SottoStato Case - Valore: %s - Valore atteso: %s",Case_ITA_IFM_SubStatus__c,sottoStatoAttesoCase));

			}
			




			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}

}
