package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerVoltageSectionComponent;
import com.nttdata.qa.enel.components.colla.ContattaciComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SupportoToContattaci {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			TopMenuComponent menu=new TopMenuComponent(driver);
			SupportoSectionComponent supporto=new SupportoSectionComponent(driver);
			Thread.sleep(10000);
			logger.write("click sul tab 'SUPPORTO' e check testo nell' header della pagina web "+prop.getProperty("LINK")+supporto.linkSUPPORTO+" - Start");		
			menu.sceltaMenu("Supporto");
						
			//supporto.checkHeaderPageSupporto(headerDescription_Supporto,homeSupporto_Path,supporto_Title,supporto_Subtitle);
			//logger.write("click sul tab 'SUPPORTO' e check testo nell' header della pagina web "+prop.getProperty("LINK_SUPPORTO")+" - Completed");
			
			logger.write("click sul link 'Contratti e modulistica' e check dei suoi link - Start");
			supporto.clickComponent(supporto.ContrattiAndModulisticaLink);
						
			supporto.checklinksOnContrattiAndModulisticaSection(supporto.linksOfContrattiAndModulistica);
			logger.write("click sul link 'Contratti e modulistica' e check dei suoi link - Completed");
			
			supporto.clickComponent(supporto.contattaci);
			logger.write("click sul link Contattaci");
			
			logger.write("Check Contattaci - Start");
			ContattaciComponent contattaci=new ContattaciComponent(driver);
			contattaci.checkURLAfterRedirection(prop.get("LINK_CONTATTACI").toString());
			//contattaci.comprareText(contattaci.contattaciPath, prop.getProperty("PATH_CONTATTACI"), true);
			contattaci.comprareText(contattaci.contattaciTitle, prop.getProperty("TITLE_CONTATTACI"), true);
			contattaci.comprareText(contattaci.contattaciSubtitle, prop.getProperty("SUBTITLE_CONTATTACI"), true);
			contattaci.clickComponent(contattaci.clickVuoiScriverciViaWeb);
			Thread.sleep(5000);
				
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
