package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_72_ModuloReclami {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ModuloReclamiComponent mrc = new ModuloReclamiComponent(driver);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			mrc.launchLink(prop.getProperty("WP_LINK"));
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
 			mrc.comprareText(mrc.home, mrc.HOME, true);
			mrc.comprareText(mrc.formReclami, mrc.FORM_RECLAMI, true);
			mrc.comprareText(mrc.reclamiHeading, mrc.FORM_RECLAMI_HEADING, true);
			
			
			mrc.verifyComponentExistence(mrc.nome);
			mrc.verifyComponentExistence(mrc.cognome);
			mrc.verifyComponentExistence(mrc.telefonoCellulare);
			mrc.verifyComponentExistence(mrc.fasceOrarieDiReperibilità);
			mrc.verifyComponentExistence(mrc.indirizzoDellaForniture);
			mrc.verifyComponentExistence(mrc.email);
			mrc.verifyComponentExistence(mrc.confermaEmail);
			mrc.verifyComponentExistence(mrc.numeroCliente);
			mrc.verifyComponentExistence(mrc.codicePOD);
			
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessageTipologiaReclamo, mrc.EFFTTUAUNASELEZIONE, true);
			
			mrc.clickComponent(mrc.tipologiaDelReclamo);
			mrc.clickComponent(mrc.optionReclamoGas);
			mrc.clickComponent(mrc.confermaButton);
			
			mrc.verifyComponentExistence(mrc.optionReclamoGas);
			mrc.verifyComponentExistence(mrc.codicePDR);
			mrc.comprareText(mrc.codicePDRLabel, mrc.CODICE_PDR_LABEL, true);
			mrc.comprareText(mrc.errorMessageNome, mrc.ERROR_MESSAGE, true);
			
			mrc.clickComponent(mrc.tipologiaDelReclamo);
			mrc.clickComponent(mrc.optionReclamoLuce);
			mrc.verifyComponentExistence(mrc.codicePOD);
			mrc.comprareText(mrc.codicePODLabel, mrc.CODICE_POD_LABEL, true);
			 			
			mrc.enterInputParameters(mrc.nome, prop.getProperty("INCORRECT_NOME"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessgaeincorrectNome, mrc.ERROR_MESSAGE_INCORRECTVALUE, true);
			
			mrc.enterInputParameters(mrc.nome, prop.getProperty("NOME"));
			mrc.clickComponent(mrc.confermaButton);
			//mrc.verifyFeildAttribute(mrc.nome, prop.getProperty("NOME"));
			mrc.comprareText(mrc.errorMessgaeCogNome, mrc.ERROR_MESSAGE, true);
			
			mrc.enterInputParameters(mrc.cognome, prop.getProperty("INCORRECT_COGNOME"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessgaeCogNome, mrc.ERROR_MESSAGE_INCORRECTVALUE, true);
			
			mrc.enterInputParameters(mrc.cognome, prop.getProperty("COGNOME"));
			Thread.sleep(1000);
			mrc.clickComponent(mrc.telefonoCellulare);
			Thread.sleep(1000);
			mrc.clickComponent(mrc.telefonoCellulare);
			Thread.sleep(1000);
			mrc.jsClickComponent(mrc.codiceFiscale);
			Thread.sleep(1000);
			mrc.jsClickComponent(mrc.codiceFiscale);
			Thread.sleep(1000);
			mrc.clickComponent(mrc.indirizzoDellaForniture);
			Thread.sleep(1000);
			mrc.clickComponent(mrc.indirizzoDellaForniture);
			Thread.sleep(1000);
			mrc.jsClickComponent(mrc.email);
			Thread.sleep(1000);
			mrc.clickComponent(mrc.confermaEmail);
			Thread.sleep(500);
			mrc.comprareText(mrc.errorMessagetelefonaCellulare, mrc.ERROR_MESSAGE, true);
			mrc.comprareText(mrc.errorMessageCodiceFiscale, mrc.ERROR_MESSAGE, true);
			mrc.comprareText(mrc.errorMessageindirizzoDellaForniture, mrc.ERROR_MESSAGE, true);
			mrc.comprareText(mrc.errorMessageEmail, mrc.ERROR_MESSAGE, true);
		//	mrc.comprareText(mrc.errorMessageConfermaEmail, mrc.ERROR_MESSAGE, true);
			
			mrc.clickComponent(mrc.fasceOrarieDiReperibilità);
			mrc.comprareText(mrc.errorMessagefasceOrarieDiReperibilità, mrc.EFFTTUAUNASELEZIONE, true);
			
			mrc.enterInputParameters(mrc.telefonoCellulare, prop.getProperty("INCORRECT_TELEFONA"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessagetelefonaCellulare2, mrc.FORMATO_NON_CORETTO, true);
			
			mrc.enterInputParameters(mrc.telefonoCellulare, prop.getProperty("TELEFONOCELLULARE"));
			mrc.clickComponent(mrc.fasceOrarieDiReperibilità);
			mrc.clickComponent(mrc.option8to12);
			mrc.enterInputParameters(mrc.codiceFiscale, prop.getProperty("INCORRECT_CODICE_FISCALE"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessageCodiceFiscale2, mrc.ERROR_MESSAGE_CODICE_FISCALE, true);
			
			mrc.enterInputParameters(mrc.codiceFiscale, prop.getProperty("CODICE_FISCALE"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("INCORRECT_EMAIL"));
			mrc.clickComponent(mrc.confermaButton);
		//	mrc.verifyFeildAttribute(mrc.codiceFiscale, prop.getProperty("CODICE_FISCALE"));
			mrc.comprareText(mrc.errorMessageindirizzoDellaForniture, mrc.ERROR_MESSAGE, true);
			
			mrc.enterInputParameters(mrc.codiceFiscale, prop.getProperty("CODICE_FISCALE"));
			mrc.enterInputParameters(mrc.indirizzoDellaForniture, prop.getProperty("INDIRIZZO_DELLA_FORNITURE"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("INCORRECT_EMAIL"));
			mrc.clickComponent(mrc.confermaButton);
			
			//mrc.verifyFeildAttribute(mrc.indirizzoDellaForniture, prop.getProperty("INDIRIZZO_DELLA_FORNITURE"));
			mrc.comprareText(mrc.errorMessageEmail, mrc.ERROR_MESSAGE_FORMATO_NON_CORRETTO, true);
									
			mrc.enterInputParameters(mrc.confermaEmail, prop.getProperty("INCORRECT_EMAIL"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("EMAIL"));
			mrc.enterInputParameters(mrc.indirizzoDellaForniture, prop.getProperty("INCORRECT_INDIRIZZO"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessageindirizzoDellaForniture2, mrc.ERROR_MESSAGE_FORMATO_NON_CORRETTO, true);
			
			mrc.enterInputParameters(mrc.indirizzoDellaForniture, prop.getProperty("INDIRIZZO_DELLA_FORNITURE"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("EMAIL"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("EMAIL2"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessageConfermaEmail, mrc.ERROR_MESSAGE_CORRISPONDONO, true);
			
			
			mrc.enterInputParameters(mrc.email, prop.getProperty("EMAIL"));
			mrc.enterInputParameters(mrc.email, prop.getProperty("EMAIL"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessageNumeroCliente, mrc.FORMATO_NON_CORETTO, true);
			
			mrc.enterInputParameters(mrc.numeroCliente, prop.getProperty("INCORRECT_NUMERO_CLIENTE"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.comprareText(mrc.errorMessageNumeroCliente, mrc.FORMATO_NON_CORETTO, true);
			
			mrc.enterInputParameters(mrc.numeroCliente, prop.getProperty("NUMERO_CLIENTE"));
			mrc.clickComponent(mrc.confermaButton);
			mrc.verifyComponentNonExistence(mrc.errorMessageTipologiaReclamo);
			mrc.verifyComponentNonExistence(mrc.errorMessageNome);
			mrc.verifyComponentNonExistence(mrc.errorMessgaeCogNome);
			mrc.verifyComponentNonExistence(mrc.errorMessagetelefonaCellulare);
			mrc.verifyComponentNonExistence(mrc.errorMessagefasceOrarieDiReperibilità);
			mrc.verifyComponentNonExistence(mrc.errorMessageCodiceFiscale);
			mrc.verifyComponentNonExistence(mrc.errorMessageindirizzoDellaForniture);
			mrc.verifyComponentNonExistence(mrc.errorMessageEmail);
			mrc.verifyComponentNonExistence(mrc.errorMessageConfermaEmail);
			mrc.verifyComponentNonExistence(mrc.errorMessageNumeroCliente);
			
					
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
