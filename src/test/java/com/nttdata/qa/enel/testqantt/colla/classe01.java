package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class classe01 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		SeleniumUtilities util = null;

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
//			By accessLoginPage = null;
			
			BaseComponent bsc = new BaseComponent(driver);
			
			//STEP 4
			By logoMenuBsn = By.id("avatarUtente");
			bsc.verifyComponentExistence(logoMenuBsn);
			bsc.clickComponent(logoMenuBsn);
			
			//STEP 5
			By buttonDataContact = By.id("modificaProfiloLink");
			bsc.verifyComponentExistence(buttonDataContact);
			bsc.clickComponent(buttonDataContact);
			
			//STEP 6
//			By buttonConsensus = By.id("modificaProfiloLink");
//			bsc.verifyComponentExistence(buttonDataContact);
//			bsc.clickComponent(buttonDataContact);
	
			By buttonYesConsensus = By.name("Dai il tuo consenso");
			driver.findElements(buttonYesConsensus);
			
//			bsc.prova(buttonYesConsensus);
			bsc.verifyComponentExistence(buttonYesConsensus);
/*ERROR*/	bsc.clickComponentWithJse(buttonYesConsensus);
			driver.get("https://www-colla.enel.it/it/area-clienti/imprese/thankyou.gestione_consensi.5.2");
			
////			logger.write("apertura del portale web Enel di test - Start");
//			log.launchLink(prop.getProperty("WP_LINK"));
////			logger.write("apertura del portale web Enel di test - Completed");
//			
////			logger.write("check sulla presenza del logo Enel - Start");
//			By logo = log.logoEnel;
//			log.verifyComponentExistence(logo);// verifica esistenza logo enel
////			logger.write("check sulla presenza del logo Enel - Completed");
//			By accettaCookie = log.buttonAccetta;
//			log.verifyComponentExistence(accettaCookie);
//			log.clickComponent(accettaCookie);
//            
////			logger.write("click su icona utente - Start");
//			By icon = log.iconUser;
//			log.verifyComponentExistence(icon);
//			log.clickComponent(icon); 
////			logger.write("click su icona utente - Completed");
//			
////			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
//			By pageLogin = log.loginPage;
//			log.verifyComponentExistence(pageLogin);
//			
//			By user = log.username;
//			log.verifyComponentExistence(user);
//			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));
//			
//			By pw = log.password;
//			log.verifyComponentExistence(pw);
//			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));
//
//			By accedi = log.buttonLoginAccedi;
//			log.verifyComponentExistence(accedi);
//			log.clickComponent(accedi); 
//			
//			try{
////				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Start");
//				log.verifyComponentExistence(log.areaClientiCondizioni);
//				log.clickComponent(log.areaClientiCondizioni);
//				log.verifyComponentExistence(log.areaClientiPrivacy);
//				log.clickComponent(log.areaClientiPrivacy);
//				log.verifyComponentExistence(log.terminiECondizioniButton);
//				log.clickComponent(log.terminiECondizioniButton);
////				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Completed");
//			}catch(Exception e){
////				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Pagina inesistente. Proseguo.");
//			}
//			
//			try{
//				if(prop.containsKey("AREA_CLIENTI")){
////					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
//					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
//						log.verifyComponentExistence(log.areaClientiCasa);
//						log.clickComponent(log.areaClientiCasa);
//					}else{
//						log.verifyComponentExistence(log.areaClientiImpresa);
//						log.clickComponent(log.areaClientiImpresa);
//					}
////					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
//				}
//			}catch(Exception e){
////				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
//			}
//			
//			if(prop.containsKey("ACCOUNT_TYPE"))
//				if(prop.getProperty("ACCOUNT_TYPE").contains("BSN"))
//					accessLoginPage=log.loginBSNSuccessful;
//				else
//					accessLoginPage=log.loginSuccessful;
//			else
//				accessLoginPage=log.loginSuccessful;
//			log.verifyComponentExistence(accessLoginPage);
//			
//			if(prop.containsKey("VERIFY_SC_SELECTION_PAGE")){
//				boolean shouldVerifySelfCareSelectionPage = Boolean.parseBoolean(prop.getProperty("VERIFY_SC_SELECTION_PAGE", "false"));
//				if (!shouldVerifySelfCareSelectionPage) {
//					accessLoginPage=log.loginSuccessful;
//					log.verifyComponentExistence(accessLoginPage); 
//				}
//			}
//			
////			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
		
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
