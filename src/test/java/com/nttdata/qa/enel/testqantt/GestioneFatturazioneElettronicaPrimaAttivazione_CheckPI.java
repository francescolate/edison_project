package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.FatturazioneElettronicaComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class GestioneFatturazioneElettronicaPrimaAttivazione_CheckPI {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            FatturazioneElettronicaComponent fatturazione = new FatturazioneElettronicaComponent(driver);
            
            String partita_iva=prop.getProperty("CODICE_FISCALE");
            String start_digit=partita_iva.substring(0,1);
            System.out.println("Start digit:"+start_digit);
            if(start_digit.compareTo("8")==0 || start_digit.compareTo("9")==0){
            	logger.write("Verifica se tutti i campi sono read only - Start");
            	boolean check=fatturazione.verificaAllFiledReadOnly();
            	if(check==false){
            		throw new Exception("I campi della sezione Fatturazione Elettronica non risultano essere tutti read only");
            	}
            	logger.write("Verifica se tutti i campi sono read only - Completed");
            	
            }
            else{
            if(prop.getProperty("SELEZIONA_CANALE_INVIO","").equals("Y")) {
            logger.write("Selezione canale invio - Start");
            fatturazione.selezionaCanaleInvio(prop.getProperty("CANALE_INVIO_FATTURAZIONE"));
            logger.write("Selezione canale invio - Completed");
            logger.write("Inserimento Codice Ufficio - Start");
            fatturazione.inserisciCodiceUfficio(prop.getProperty("CODICE_UFFICIO"));
            logger.write("Inserimento Codice Ufficio - Completed");
            }
            if(prop.getProperty("PUBBLICA_AMMINISTRAZIONE","").equals("Locale")) {
            	
            	RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
            	offer.checkCampiFatturazioneElettronicaConPA();
            	fatturazione.selezionaTipologiaPA(prop.getProperty("PUBBLICA_AMMINISTRAZIONE"));
            	if(prop.getProperty("CODICE_UFFICIO","").equals("YIEOE4")) {
            	fatturazione.selezionaCodiceUfficio(prop.getProperty("CODICE_UFFICIO"));}
            }
            }
			logger.write("Conferma Fatturazione Elettronica - Start");
            fatturazione.pressButton(fatturazione.confermaFatturazioneElettronicaSWAEVO);
            logger.write("Conferma Fatturazione Elettronica - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
