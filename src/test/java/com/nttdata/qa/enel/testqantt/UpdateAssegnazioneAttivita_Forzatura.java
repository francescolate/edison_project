package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Step;
import io.qameta.allure.aspects.StepsAspects;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;

public class UpdateAssegnazioneAttivita_Forzatura {

	@Step("UpdateDataRipensamentoSWA")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				if(prop.getProperty("KO_RIPROCESSABILE").compareTo("OK")==0){

					LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
					page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					////System.out.println("Login ok");
					TimeUnit.SECONDS.sleep(5);
					page.navigate("https://workbench.developerforce.com/query.php");
					WorkbenchComponent a = new WorkbenchComponent(driver);
					a.selezionaEnvironment("Sandbox");
					a.pressButton(a.checkAgree);
					a.pressButton(a.buttonLogin);
					TimeUnit.SECONDS.sleep(2);
					//System.out.println(driver.getCurrentUrl());
					while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
						page.enterUsername(Costanti.utenza_admin_salesforce);
						page.enterPassword(Costanti.password_admin_salesforce);
						page.submitLogin();
						TimeUnit.SECONDS.sleep(2);
					}

					a.insertQuery(Costanti.aggiorna_data_ripensamento_SWA.replaceAll("@ID_OFFERTA_CRM@",prop.getProperty("ID_OFFERTA")));
					logger.write("Inserimento Query");
					boolean condition = false;
					int tentativi = 2;
					while (!condition && tentativi-- > 0) {
						a.pressButton(a.submitQuery);
						condition = a.aspettaRisultati();
						logger.write("Esecuzione Query");
					}
					if (condition) {
						a.recuperaRisultati(Integer.parseInt("1"), prop);
					} else
						throw new Exception("Impossibile recuperare i risultati dalla query workbench. Sistema lento");

					TimeUnit.SECONDS.sleep(5);
					//Effettuo Update
					a.updateRow("ITA_IFM_Assegnato_A__c", "ACOL96");
					logger.write("Update data ripensamento");
					//a.logoutWorkbench();

				}
			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}