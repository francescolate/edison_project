package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BollettaWebAttivataComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.NovitaComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import javassist.bytecode.analysis.Util;

public class ACB_302_Attiva_Bolletta_WEB {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String currentHandle ="";
		JavascriptExecutor js;

		try {		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);				
			js = (JavascriptExecutor)driver;
			SeleniumUtilities util = new SeleniumUtilities(driver);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			
			ServiziComponent sc = new ServiziComponent(driver);
			
			logger.write("Verify home page title, Logo and details -- Start");
			sc.verifyComponentExistence(sc.homePageLogoServizi);			
			sc.comprareText(sc.homePagePathServizi, sc.HomePagePathSer, true);			//false
			sc.comprareText(sc.homePageTitleServizi, sc.HomePageTitleSer, true);	
			logger.write("Verify home page title, Logo and details -- Completed");
			
			logger.write("Click on Servizi -- Start");
			sc.verifyComponentExistence(sc.services);
			sc.clickComponent(sc.services);
			logger.write("Click on Servizi -- Completed");
					
			logger.write("Verify Servizi page Path, title and Text -- Start");
			sc.comprareText(sc.pagePathServizi, sc.PAGEPATH, true);		//false
			sc.verifyComponentExistence(sc.pageTitleServizi);
			sc.comprareText(sc.pagetextServizi, sc.PAGETEXT, true);
			logger.write("Verifi Servizi page Path, title and Text -- Completed");
			
			sc.verifyComponentExistence(sc.bollettaWeb);
			logger.write("Click on Bolletta Web-- Start");
			sc.jsClickComponent(sc.bollettaWeb);
			Thread.sleep(3000);
			driver.navigate().back();
			sc.verifyComponentExistence(sc.bollettaWeb);
			sc.jsClickComponent(sc.bollettaWeb);
			logger.write("Click on Bolletta Web-- Completed");
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(sc.homePagePathServizi));
			
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathBW, true);					//false
			sc.comprareText(sc.pageTitleBW, sc.PageTitleBW, true);
			sc.comprareText(sc.textBollettaWebBy, sc.textBollettaWeb, true);
			//sc.comprareText(sc.servizioBollettaWebBy, sc.servizioBollettaWeb, true);
			sc.containsText(sc.webNonAtt, sc.servizioBollettaWeb);
			sc.verifyComponentExistence(sc.attivaBollettaWebButton);
			//sc.comprareText(sc.pageTextBW, sc.PageTextBW, true);
			//sc.comprareText(sc.pageSubTitleBW, sc.PageSubTitleBW, true);
			//sc.comprareText(sc.pageSubTextBW, sc.PageSubTextBW, true);
			//sc.comprareText(sc.activation, sc.ACTIVATION, false);
			//sc.comprareText(sc.edit, sc.EDIT, false);
			//sc.comprareText(sc.revocation, sc.REVOCATION, false);
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Completed");
			
			//sc.verifyComponentExistence(sc.findMore);
			//logger.write("Click on SCOPRI DI PIU-- Start");
			//sc.clickComponent(sc.findMore);
			//logger.write("Click on SCOPRI DI PIU-- Completed");
			
			logger.write("Click on Attiva Bolletta Web - Start");
			sc.clickComponent(sc.attivaBollettaWebButton);
			logger.write("Click on Attiva Bolletta Web - Completed");
			
			Thread.sleep(20000);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(sc.homePagePathServizi));
			
			logger.write("Verifi Bolletta Supply Web page Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathSupply, true);						//false
			sc.comprareText(sc.pageTitleSupply, sc.PageTitleSupply, true);
			sc.comprareText(sc.pageTextSupply, sc.PageTextSupply, true);
			logger.write("Verifi Bolletta Supply Web page Path, title and Text etc. -- Completed");
			
			logger.write("Select the supply type - Start");
			sc.selectSupplyType(sc.checkBoxForSupply4);
			//sc.selectSupplyTypeList(sc.listForniture(), 0);
			Thread.sleep(5000);
			sc.clickComponent(sc.activeButton);
			logger.write("Select the supply type - Start - Complete");
			
			logger.write("Verifi Bolletta Supply Web page for activation - Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathSupply, true);						//false
			sc.comprareText(sc.pageTitleSupply, sc.PageTitleSupply, true);
			sc.comprareText(sc.pageTextSupply, sc.PageTextActive, true);
			logger.write("Verifi Bolletta Supply Web page for activation - Path, title and Text etc. -- Completed");
			
			logger.write("Inserimento Dati. -- Start");
			sc.verifyComponentExistence(sc.bollettaEmailInput);
			sc.enterInput(sc.bollettaEmailInput, prop.getProperty("EMAIL"));
			
			sc.verifyComponentExistence(sc.bollettaConfermaEmail);
			sc.enterInput(sc.bollettaConfermaEmail, prop.getProperty("CONFERMA_EMAIL"));
			
			//sc.verifyComponentExistence(sc.bollettaNumeroInput);
			//sc.enterInput(sc.bollettaNumeroInput, prop.getProperty("NUMERO"));
			
			//sc.verifyComponentExistence(sc.bollettaNumeroConferma);
			//sc.enterInput(sc.bollettaNumeroConferma, prop.getProperty("CONFERMA_NUMERO"));
			
			sc.verifyComponentExistence(sc.cellulareInput);
			sc.enterInput(sc.cellulareInput, prop.getProperty("CELLULARE"));
			
			sc.verifyComponentExistence(sc.cellulareConferma);
			sc.enterInput(sc.cellulareConferma, prop.getProperty("CELLULARE"));
	
			//sc.verifyComponentExistence(sc.updateCheckboxInput);
			//sc.clickComponent(sc.updateCheckboxInput);
			sc.verifyComponentExistence(sc.aggiornadati);
			//sc.clickComponent(sc.aggiornadati);
			sc.jsClickComponent(sc.aggiornadati);
			logger.write("Inserimento Dati. -- Completed");

			//sc.verifyComponentExistence(sc.continuaButton);
			//sc.clickComponent(sc.continuaButton);
			sc.verifyComponentExistence(sc.proseguiButton);
			sc.clickComponent(sc.proseguiButton);
						
			logger.write("Verifi Bolletta Supply Web page for activation Process - Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathSupply, true);							//false
			sc.comprareText(sc.pageTitleSupply, sc.PageTitleSupply, true);
			sc.comprareText(sc.pageTextSupply, sc.PageTextProgress, true);
			logger.write("Verifi Bolletta Supply Web page for activation Process - Path, title and Text etc. -- Completed");
	
			
			sc.verifyComponentExistence(sc.confirmButton);
			sc.clickComponent(sc.confirmButton);
			
			Thread.sleep(20000);

			BollettaWebAttivataComponent bwa = new BollettaWebAttivataComponent(driver);
			bwa.verifyComponentExistence(bwa.path);
			bwa.verifyComponentExistence(bwa.title);
			bwa.verifyComponentExistence(bwa.status);
			bwa.verifyComponentExistence(bwa.addebitoDiretto);
			bwa.compareText(bwa.path, bwa.pathText, true);
			bwa.compareText(bwa.title, bwa.titleText, true);
			bwa.compareText(bwa.status, bwa.statusText, true);
			bwa.compareText(bwa.addebitoDiretto, bwa.addebitoDirettoText, true);
			//sc.comprareText(sc.homePagePathServizi, sc.PagePathBW, false);
			//sc.comprareText(sc.pageTitleBW, sc.PageTitleBW, true);
			//sc.comprareText(sc.pageTextBW, sc.PageTextConfirmBW, true);
			
			sc.verifyComponentExistence(sc.fineButton);
			sc.clickComponent(sc.fineButton);
			
			logger.write("Verifi home page title, Logo and details -- Start");

			sc.verifyComponentExistence(sc.homePageLogoServizi);
			
			sc.comprareText(sc.homePagePathServizi, sc.HomePagePathSer, true);						//false
			sc.comprareText(sc.homePageTitleServizi, sc.HomePageTitleSer, true);	
			logger.write("Verifi home page title, Logo and details -- Completed");
			
			sc.verifyComponentExistence(sc.services);
			logger.write("Click on Servizi -- Start");
			sc.clickComponent(sc.services);
			logger.write("Click on Servizi -- Completed");
			
			
			logger.write("Verifi Servizi page Path, title and Text -- Start");
			sc.comprareText(sc.pagePathServizi, sc.PAGEPATH, true);										//false
			sc.verifyComponentExistence(sc.pageTitleServizi);
			sc.comprareText(sc.pagetextServizi, sc.PAGETEXT, true);
			logger.write("Verifi Servizi page Path, title and Text -- Completed");
			
			sc.verifyComponentExistence(sc.bollettaWeb);
			logger.write("Click on Bolletta Web-- Start");
			sc.clickComponent(sc.bollettaWeb);
			logger.write("Click on Bolletta Web-- Completed");
			
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathBW, true);								//false
			sc.comprareText(sc.pageTitleBW, sc.PageTitleBW, true);
			sc.comprareText(sc.textBollettaWebBy, sc.textBollettaWeb, true);
			//sc.comprareText(sc.pageTextBW, sc.PageTextBW, true);
			//sc.comprareText(sc.pageSubTitleBW, sc.PageSubTitleBW, true);
			//sc.comprareText(sc.pageSubTextBW, sc.PageSubTextBW, true);
			logger.write("Verifi Bolletta Web page Path, title and Text etc. -- Completed");
			
			//logger.write("Click on SCOPRI DI PIU-- Start");
			//sc.clickComponent(sc.findMore);
			//logger.write("Click on SCOPRI DI PIU-- Completed");
			
			logger.write("Click on Attiva Bolletta Web - Start");
			sc.clickComponent(sc.attivaBollettaWebButton);
			logger.write("Click on Attiva Bolletta Web - Completed");
			
			Thread.sleep(20000);
			
			logger.write("Verifi Bolletta Supply Web page Path, title and Text etc. -- Start");
			sc.comprareText(sc.homePagePathServizi, sc.PagePathSupply, true);							//false
			sc.comprareText(sc.pageTitleSupply, sc.PageTitleSupply, true);
			sc.comprareText(sc.pageTextSupply, sc.PageTextSupply, true);
			logger.write("Verifi Bolletta Supply Web page Path, title and Text etc. -- Completed");
			
			logger.write("Select the supply type - Start");		
			//sc.verifyComponentExistence(sc.crossForSupply4);
			sc.selectSupplyType(sc.checkBoxForSupply4);
			Thread.sleep(5000);
			sc.comprareText(sc.pageTextCross, sc.PageTextCross, true);
			logger.write("Select the supply type - Start - Complete");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
