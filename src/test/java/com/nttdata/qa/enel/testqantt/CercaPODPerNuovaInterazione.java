package com.nttdata.qa.enel.testqantt;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CercaPODComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovaInterazioneComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaClienteAttivoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CercaPODPerNuovaInterazione  
{

	@Step("Cerca POD Per Nuova Iterazione")
	public static void main (String[] args) throws Exception
	{
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if(prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) 
			{
				CreaNuovaInterazioneComponent startIterationPage = new CreaNuovaInterazioneComponent(driver);
				startIterationPage.createNewiteration();

				CercaPODComponent searchPODPage =  new CercaPODComponent(driver);

				searchPODPage.fillCfAndSearch2(prop.getProperty("POD"));

				//Seleziona il cliente attivo del POD appena trovato.
				SelezionaClienteAttivoComponent selectCliente = new SelezionaClienteAttivoComponent(driver);
				selectCliente.fillCfAndSearch2();		

				selectCliente.setInterlocutore(0);

			}

			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
		
	}
}
