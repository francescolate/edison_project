package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfirmationCallComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

//Il seguente modulo si occupa di Confermare la Sezione Firma Digitale

public class ConfirmationCall {

	@Step("Modalità Firma Digital")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		SeleniumUtilities util;
		QANTTLogger logger = new QANTTLogger(prop);

		try {


			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			util = new SeleniumUtilities(driver);
			ConfirmationCallComponent CC = new ConfirmationCallComponent(driver);
		
			switch(prop.getProperty("SCENARIO")) {
			case "CONFIRMATION_CALL":
				logger.write("Esegui Confirmation Call - Start");
				CC.press(CC.CCButton);
				logger.write("Esegui Confirmation Call - Completed");
				break;
			case "QUALITY_CALL_INTERNA":
				logger.write("Esegui Quality Call - Start");
				CC.press(CC.QCButton);
				logger.write("Esegui Quality Call - Completed");
				break;
			case "QUALITY_CALL":
				logger.write("Esegui Quality Call - Start");
				CC.press(CC.QCButton);
				logger.write("Esegui Quality Call - Completed");
				break;
			}

			logger.write("Termina Confirmation Call - Start");
			CC.selezionaTerminaChiamataVisibile(CC.CCTerminaChiamata);
			logger.write("Termina Confirmation Call - Completed");

			logger.write("Seleziona Esito Chiamata Confirmation Call - Start");
			util.getFrameActive();
			CC.selectOption(CC.CCEsitoChiamata, "Chiamata completata");
			driver.switchTo().defaultContent();
			logger.write("Seleziona Esito Chiamata Confirmation Call - Completed");
			
			logger.write("Conferma Confirmation Call - Start");
			CC.conferma(CC.CCConferma);
			logger.write("Conferma Confirmation Call - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
