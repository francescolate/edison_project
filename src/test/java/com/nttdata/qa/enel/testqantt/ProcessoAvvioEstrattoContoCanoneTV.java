package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CanaleInvioDocumentaleComponent;
import com.nttdata.qa.enel.components.lightning.CompilaAvvioEstrattoContoComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoAvvioEstrattoContoCanoneTV {

	@Step("Processo Avvio Estratto Conto Canone TV")

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			String frame = null;

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				SceltaProcessoComponent sceltaprocesso = new SceltaProcessoComponent(driver);
				sceltaprocesso.clickAllProcess();
				sceltaprocesso.chooseProcessToStart(sceltaprocesso.avvioEstrattoContoCanoneTv);

//				// Verifica Intestazione nuova richiesta
				IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
//				String intestazionepaginaattesa = nuovaRichiesta.intestazionePaginaAvvioEstrattoContoCanoneTV;
//				nuovaRichiesta.verificaIntestazioneNuovaRichiesta(nuovaRichiesta.h1IntestazionePaginaNuovaRichiesta,
//						intestazionepaginaattesa);

				// Salvataggio Numero Richiesta
				String numerorichiesta = nuovaRichiesta
						.salvaNumeroRichiesta(nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta5);
				prop.setProperty("NUMERO_RICHIESTA", numerorichiesta);

				// Identificazione Cliente

				IdentificazioneInterlocutoreComponent identificazioneCliente = new IdentificazioneInterlocutoreComponent(
						driver);

				identificazioneCliente.insertIndirizzoFornitura(identificazioneCliente.indirizzoDiFornituraInput,
						"aaaaa");
				identificazioneCliente.pressButton(identificazioneCliente.confirmButton);

				// Configurazione Nuova richiesta estratto conto

				CompilaAvvioEstrattoContoComponent avvioEstrattoConto = new CompilaAvvioEstrattoContoComponent(driver);
				// Selezione fornitura
				String fornitura = prop.getProperty("POD_PDR");
				frame = util.getFrameByIndex(1);
				avvioEstrattoConto.cercaFornitura(frame, avvioEstrattoConto.inputFornituraDaCercare,
						avvioEstrattoConto.divPrimaFornitura, fornitura);
				avvioEstrattoConto.selezionaFornitura(frame, avvioEstrattoConto.tabellaFornitura, "POD/PDR", fornitura);
				avvioEstrattoConto.confermaSelezione(frame, avvioEstrattoConto.buttonConfermaSelezionaFornitura);

				// Selezione fattura
				String fattura = prop.getProperty("NUMEROFATTURA");
				String statoPagamenticonsumi = prop.getProperty("STATOPAGAMENTICONSUMI", "N");
				String flagInserimentoDateFattura = prop.getProperty("INSERIMENTODATEFATTURA");
//				String dataDa = prop.getProperty("FATTURADATAINIZIO");
//				String dataA = prop.getProperty("FATTURADATAFINE");
				frame = util.getFrameByIndex(1);

				String seCanoneTV = prop.getProperty("ESTRATTO_CANONE_TV", "N");
//				avvioEstrattoConto.ricercaFatture(frame, seCanoneTV, avvioEstrattoConto.selectStatoPagamentoConsumi,
//						avvioEstrattoConto.inputFatturaDataInizio, avvioEstrattoConto.inputFatturaDataFine,
//						avvioEstrattoConto.buttonApplicaFiltri, avvioEstrattoConto.inputFatturaDaCercare,
//						avvioEstrattoConto.buttonStampa, fattura, statoPagamenticonsumi, flagInserimentoDateFattura,
//						dataDa, dataA);
				avvioEstrattoConto.ricercaFatture(frame, seCanoneTV, avvioEstrattoConto.selectStatoPagamentoConsumi,
						avvioEstrattoConto.inputFatturaDataInizio, avvioEstrattoConto.inputFatturaDataFine,
						avvioEstrattoConto.buttonApplicaFiltri, avvioEstrattoConto.inputFatturaDaCercare,
						avvioEstrattoConto.buttonStampa, fattura, statoPagamenticonsumi, flagInserimentoDateFattura);

				// Inserimento canale di invio
				String canale = prop.getProperty("CANALE_INVIO");
				String email = prop.getProperty("INDIRIZZO_EMAIL");
				CanaleInvioDocumentaleComponent canaleInvio = new CanaleInvioDocumentaleComponent(driver);
				frame = util.getFrameByIndex(1);
				canaleInvio.selezionaCanale(frame, canaleInvio.labelCanaleInvio, canale);
				TimeUnit.SECONDS.sleep(5);
				if (canale.compareToIgnoreCase("Email") == 0) {
					canaleInvio.inserisciEmail(frame, canaleInvio.inputEmail, email);
					TimeUnit.SECONDS.sleep(5);
				}
				if (canale.compareToIgnoreCase("Posta") == 0) {
					// Inserire parte indirizzi per posta
					CompilaIndirizziComponent infoindirizzo = new CompilaIndirizziComponent(driver);
					frame = util.getFrameByIndex(1);
					String presso = prop.getProperty("PRESSO");
					String provincia = prop.getProperty("PROVINCIA");
					String comune = prop.getProperty("COMUNE");
					String localita = prop.getProperty("LOCALITA");
					String indirizzo = prop.getProperty("INDIRIZZO");
					String civico = prop.getProperty("CIVICO");
					String scala = prop.getProperty("SCALA");
					String piano = prop.getProperty("PIANO");
					String interno = prop.getProperty("INTERNO");

					infoindirizzo.reinserisciIndirizzo(frame, infoindirizzo.inputPresso, infoindirizzo.inputProvincia,
							infoindirizzo.inputLocalita, infoindirizzo.inputComune, infoindirizzo.inputIndirizzo,
							infoindirizzo.inputCivico, infoindirizzo.inputInterno, infoindirizzo.inputPiano,
							infoindirizzo.inputScala, presso, provincia, localita, comune, indirizzo, civico, interno,
							piano, scala);
					infoindirizzo.verificaIndirizzo(frame, infoindirizzo.buttonVerifica,
							infoindirizzo.spanEsitoVerificaIndirizzo);
				}

				// Conferma dati inseriti
				frame = util.getFrameByIndex(1);
				avvioEstrattoConto.confermaInserimentoDatiEstrattoConto(avvioEstrattoConto.buttonConferma, frame);

				// Conferma pop-up
				frame = util.getFrameByIndex(1);
				String mess = prop.getProperty("MESSAGGIOPRESAINCARICO");
				avvioEstrattoConto.popUpPresaInCarico(frame, avvioEstrattoConto.spanMessPopupPresaInCarico,
						avvioEstrattoConto.buttonOkPresaInCarico, mess);
				TimeUnit.SECONDS.sleep(10);

			}
            prop.setProperty("RETURN_VALUE", "OK");
			}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}

	}
