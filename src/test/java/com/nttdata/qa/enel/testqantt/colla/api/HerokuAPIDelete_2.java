// Package HerokuAPIRegistration_1

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;

import com.nttdata.qa.enel.components.colla.api.HerokuAPIDeleteComponent_Err;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class HerokuAPIDelete_2 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Authenticate Scenario_2")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			prop.setProperty("Authorization", "Bearer " + APIService.getToken());
			prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account/"+ prop.getProperty("ENELID"));
			
			//Da configurare come OutPut
	   		prop.setProperty("TID", UUID.randomUUID().toString());
    		prop.setProperty("SID", UUID.randomUUID().toString()); 
    		prop.setProperty("SOURCE_CHANNEL","EE");
    		prop.setProperty("TOUCHPOINT","WEB");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIDeleteComponent_Err HRKComponent = new HerokuAPIDeleteComponent_Err();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


