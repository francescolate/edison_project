package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SupplyDetailTest {

	
		public static void main(String[] args) throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				
				/*
				logger.write("apertura del portale web Enel di test - Start");
				LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
				log.launchLink(prop.getProperty("LINK"));
				logger.write("apertura del portale web Enel di test - Completed");
				
				logger.write("check sulla presenza del logo Enel - Start");
				By logo = log.logoEnel;
				log.verifyComponentExistence(logo);// verifica esistenza logo enel
				logger.write("check sulla presenza del logo Enel - Completed");
				
				//Click chusura bunner pubblicitario se esistente
				log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

				By accettaCookie = log.buttonAccetta;
				log.verifyComponentExistence(accettaCookie);
				log.clickComponent(accettaCookie);
	            
				logger.write("click su icona utente - Start");
				By icon = log.iconUser;
				log.verifyComponentExistence(icon);
				log.clickComponent(icon); 
				logger.write("click su icona utente - Completed");
				
				logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
				By pageLogin = log.loginPage;
				log.verifyComponentExistence(pageLogin);
				logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
				
				logger.write("check the username and passsword login - Start");
				By user = log.username;
				log.verifyComponentExistence(user);
				log.enterLoginParameters(user, prop.getProperty("USERNAME")); 

				By pw = log.password;
				log.verifyComponentExistence(pw);
				log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
				logger.write("check the username and passsword login - Completed");
				
				logger.write("Click on the login button- Start");
				By accedi = log.buttonLoginAccedi;
				log.verifyComponentExistence(accedi);
				log.clickComponent(accedi); 
				logger.write("Click on the login button- Complete");	
				
				logger.write("Verify the page after successful login - Start");
				By accessLoginPage=log.loginSuccessful;
				log.verifyComponentExistence(accessLoginPage); 
				logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
				logger.write("Verify the page after successful login - Complete");
				
				
				//Home Page 
				*/
				SuplyPrivateDetailComponent sd = new SuplyPrivateDetailComponent(driver);
				
				logger.write("Verify the left menu list  - Start");
				sd.homepageRESIDENTIALMenu(sd.LeftMenuList);
				logger.write("Verify the left menu list  - Completed");
				
				//Gas Details Page
				logger.write("click on Gas details page  - Start");
			    sd.verifyComponentExistence(sd.GasDetail);
			    sd.clickComponent(sd.GasDetail);
			    logger.write("click on Gas details page  - Complete");
			    
			    logger.write("click on more details button  - Start");
			    sd.verifyComponentExistence(sd.ShowMoreClick);
			    sd.clickComponent(sd.ShowMoreClick);
			    logger.write("click on more details button  - Complete");
			    
			    logger.write("Verify the details - Start");
			    sd.verifyComponentExistence(sd.StatoFornitura);
			    sd.verifyComponentExistence(sd.Statopagamento);
			    sd.verifyComponentExistence(sd.Modalitàdipagamento);
			    sd.verifyComponentExistence(sd.Ricezionebollette);
			    sd.verifyComponentExistence(sd.Datadiattivazione);
			    sd.verifyComponentExistence(sd.Offertaattiva);
			    sd.verifyComponentExistence(sd.NumeroOfferta);
			    sd.verifyComponentExistence(sd.PDR);
			    sd.verifyComponentExistence(sd.Categoriadiconsumo);
			    sd.verifyComponentExistence(sd.Contratto);
			    logger.write("Verify the details - Complete");
			    
			    logger.write("Verify the Rename and Viewbills buttons - Start");
			    sd.verifyComponentExistence(sd.RenameSupply);
			    sd.verifyComponentExistence(sd.ViewBills);
			    logger.write("Verify the Rename and Viewbills buttons - Compelete");
			    
			    logger.write("Click on Rename button - Start");
			    sd.clickComponent(sd.RenameSupply);
			    logger.write("Click on Rename button - Complete");
			    
			    logger.write("Verify the status - Start");
			    sd.verifyComponentExistence(sd.NomeFornitura);
			    sd.verifyComponentExistence(sd.Campi);
			    sd.verifyComponentExistence(sd.Indietro);
			    sd.verifyComponentExistence(sd.Continua);
			    logger.write("Verify the status - Complete");
			    
			    logger.write("Click on Indietro button - Start");
			    sd.clickComponent(sd.Indietro);
			    logger.write("Click on Indietro button - Complete");
			    
			    
			    logger.write("Click on RenameSupply button - Start");
			    sd.clickComponent(sd.RenameSupply);
			    logger.write("Click on RenameSupply button - Complete");
			    
			    logger.write("Verify the status - Complete");
			    sd.verifyComponentExistence(sd.Inserimento);
			    sd.verifyComponentExistence(sd.Riepilogo);
			    sd.verifyComponentExistence(sd.Esito);
			    logger.write("Verify the status - Complete");
			    
			    logger.write("Verify the status without entering the values - Complete");
			    sd.clickComponent(sd.Continua);
			    sd.verifyComponentExistence(sd.Questo);
			    logger.write("Verify the status without entering the values - Complete");
			    
			    logger.write("Enter the input - Start");
			    sd.enterLoginParameters(sd.InputNomeFornitura, prop.getProperty("INPUTFIELD"));
			    logger.write("Enter the input  - Complete");
			    
			    logger.write("Click on Contiue button - Start");
			    sd.clickComponent(sd.Continua);
			    logger.write("Click on Contiue button - Complete");
			    
			    logger.write("Verify the status  - Start");
			    sd.verifyComponentExistence(sd.Inserimentodati);
			    sd.verifyComponentExistence(sd.RiepilogoConferma);
			    logger.write("Verify the status  - Complete");
			    
			   
				
		prop.setProperty("RETURN_VALUE", "OK");
				
			} catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}
				
				
	}

}
