package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSN_InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_261_InfoEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Click on Piu Informazioni Link -- Start");
			pa.isDisplayedChatWindow(pa.chatClose);
			pa.clickComponent(pa.piuInformazioniLink);
			logger.write("Click on Piu Informazioni Link -- Completed");
			
			FornitureBSNComponent sc = new FornitureBSNComponent(driver);
			Thread.sleep(10000);
			logger.write("Verify page navigation and page details -- Start");
			pa.verifyComponentExistence(sc.pageTitle);
			sc.comprareText(sc.pageTitle, sc.PageTitle, true);
	//		sc.comprareText(sc.path, sc.PagePath, true);
			sc.comprareText(sc.titleSubText, sc.TitleSubText, true);
			
			pa.verifyComponentExistence(sc.gestisciletueForniture);
			sc.comprareText(sc.infoEnelEnergiaText, sc.InfoEnelEnergiaRowText, true);
			pa.verifyComponentExistence(sc.scopriDiPiuLink);
//			Thread.sleep(10000);
			logger.write("Verify page navigation and page details -- Completed");

			logger.write("Click on scopri di piu link -- Start");
			sc.clickComponent(sc.scopriDiPiuLink);
			Thread.sleep(10000);
			logger.write("Click on scopri di piu link -- Completed");

			logger.write("Verify info enel energia page details-- Start");
			BSN_InfoEnelEnergiaComponent bic = new BSN_InfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			bic.verifyComponentExistence(bic.pageTitle);
			bic.comprareText(bic.page_title261, bic.PageTitle, true);
			bic.comprareText(bic.page_subtitle261, bic.PageSubText261, true);
	/*		bic.comprareText(bic.text, bic.Text, true);*/
			String value = driver.findElement(bic.supply).getText();
			String [] supplyText = value.split(" ");
			String supply = supplyText[4].replace("\\s", "");
			System.out.println(supply);
			prop.setProperty("SUPPLY", supply);
			logger.write("Verify info enel energia page details-- Completed");
			/*logger.write("Click on ModificaScopriDiPiu-- Start");
			bic.clickComponent(bic.modificaScopriDiPiu);
			Thread.sleep(10000);
			logger.write("Click on ModificaScopriDiPiu-- Completed");*/
			logger.write("Click on Prosegui Button - Starts");
			bic.clickComponent(bic.proseguiButton);
			logger.write("Click on Prosegui Button - Ends");

			logger.write("Verify Modifica Info enel enegia page,menu one details-- Start");
			BSN_ModificaInfoEnelEnergiaComponent bmc = new BSN_ModificaInfoEnelEnergiaComponent(driver);
			Thread.sleep(30000);
//			bmc.verifyComponentExistence(bmc.pageTitleNew);
			bmc.verifyComponentExistence(bmc.pageTitle);
//			bmc.comprareText(bmc.pagePath, bmc.PagePath, true);
//			bmc.comprareText(bmc.pageTitleNew, bmc.PageTitle, true);
			bmc.comprareText(bmc.pageTitle, bmc.PageTitle, true);
			bmc.comprareText(bmc.pageSubText, bmc.PageSubText, true);
			bmc.verifyComponentExistence(bmc.menuOne);
			bmc.comprareText(bmc.menuOneDescription, bmc.MenuOneDescription, true);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaSMS);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaSMS);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaEmail);
			
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaSMS);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			bmc.verifyComponentExistence(bmc.avvisoConsumiSMS);
			bmc.verifyComponentExistence(bmc.avvisoConsumiEmail);
			
			bmc.comprareText(bmc.sectionTitle, bmc.SectionTitle, true);
			bmc.selectCheckBox(bmc.avvenutoPagamentoSMS);
			bmc.selectCheckBox(bmc.avvenutoPagamentoEmail);
			bmc.verifyComponentExistence(bmc.email);
			bmc.verifyComponentExistence(bmc.confirmEmail);
			bmc.verifyComponentExistence(bmc.cellulare);
			bmc.verifyComponentExistence(bmc.confermaCellulare);
			bmc.verifyFieldEnable(bmc.email);
			bmc.verifyFieldEnable(bmc.confirmEmail);
			bmc.verifyFieldEnable(bmc.cellulare);
			bmc.verifyFieldEnable(bmc.confermaCellulare);
			bmc.verifyComponentExistence(bmc.aggiornaIDatiDiContatto);
			bmc.clickComponent(bmc.aggiornaIDatiDiContatto);
			Thread.sleep(10000);
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.selectCheckBox(bmc.avvenutoPagamentoEmail);
			bmc.clickComponent(bmc.prosegui);
			Thread.sleep(10000);
			logger.write("Verify Modifica Info enel enegia page,menu one details-- Completed");

			String cellulare_input = "3333323231";

			bmc.enterInputToField(bmc.cellulare, cellulare_input);
			bmc.enterInputToField(bmc.confermaCellulare, cellulare_input);
			bmc.clickComponent(bmc.prosegui);
			Thread.sleep(10000);
			
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Start");
			bmc.verifyComponentExistence(bmc.menuTwo);
			bmc.comprareText(bmc.titleSubText, bmc.TitleSubText, true);
			bmc.comprareText(bmc.menuTwoTitle, bmc.MenuTwoTitle, true);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			
			bmc.comprareText(bmc.supplyDataText, bmc.SupplyDataText, true);
			bmc.verifyComponentExistence(bmc.tipo);
			bmc.verifyComponentExistence(bmc.numeroCliente);
			bmc.verifyComponentExistence(bmc.address);
			bmc.comprareText(bmc.tipoValue, bmc.TipoValue, true);
			bmc.comprareText(bmc.numeroClientiValue, bmc.NumeroClientiValue_261, true);
			bmc.comprareText(bmc.addressValue, bmc.AddressValue_261, true);
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.verifyComponentExistence(bmc.prosegui);
			bmc.clickComponent(bmc.prosegui);
			Thread.sleep(20000);
			logger.write("Verify Modifica Info enel enegia page,menu two details-- Completed");

			logger.write("Verify  Info enel enegia page,menu three details-- Start");
			Thread.sleep(20000);
			bic.verifyComponentExistence(bic.infoTitle);
//			bic.comprareText(bic.pagePath, bic.PagePath, true);
			bic.comprareText(bic.titleSubText, bic.TitleSubText, true);
			bic.verifyComponentExistence(bic.estito);
			bic.comprareText(bic.sectionTitle, bic.SectionTitle, true);
			bic.comprareText(bic.sectionText, bic.SectionText, true);
			bic.verifyComponentExistence(bic.attivaBollettaWeb);
			bic.verifyComponentExistence(bic.fineButton);
			bic.clickComponent(bic.fineButton);
			Thread.sleep(10000);
			logger.write("Verify  Info enel enegia page,menu three details-- Completed");

			logger.write("Verify home page title and page path -- Start");
			pa.verifyComponentExistence(pa.pageTitle);
			pa.comprareText(pa.pagePath, pa.Path, true);
			pa.comprareText(pa.pageTitle, pa.PageTitle, true);
			logger.write("Verify home page title and page path -- Completed");
			Thread.sleep(60000);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
