package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaAlertRosso {

	/**
	 
	 * @param CHECK_RED_ALERT_MESSAGE valorizzare a Y per eseguire verifica su alert rosso
	 * @oaram RED_ALERT_MESSAGE il testo che deve avere il messaggio
	 */
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			if (prop.getProperty("CHECK_RED_ALERT_MESSAGE","").contentEquals("Y")) {
				
				WebElement alertMsg=driver.findElement(By.xpath("//p[text()='Attenzione! Banner di errore']/parent::div/h2"));
				String messageText=alertMsg.getText();
				String expectedMessageText=prop.getProperty("RED_ALERT_MESSAGE");
				logger.write(String.format("Alert Visualizzato: %s",messageText));
				logger.write(String.format("Alert Atteso: %s",expectedMessageText));
				
				if(messageText.contentEquals(expectedMessageText))
				{
					logger.write(String.format("Testo alert corrispondente a valore atteso - %s",expectedMessageText));
				}
				else
				{
					throw new Exception("Messaggio Visualizzato non corrispondente con valore atteso");
				}
				
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
		
		
		
		

	}

}
