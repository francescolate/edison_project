package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_3OK_GAS {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				// Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Code di comunicazione", "Caricamento Esiti");
				R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
				// Selezione tipologia Caricamento
				caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
				// Inserisci Pdr
				if (!prop.getProperty("POD_GAS","").equals("")) {
					caricamentoEsiti.inserisciPdr(caricamentoEsiti.inputPDR,
							prop.getProperty("POD_GAS", prop.getProperty("POD")));
				}
				// Inserisci OI Ricerca CRM
				caricamentoEsiti.inserisciNumeroOrdineCRM(caricamentoEsiti.inputNumeroOrdineCRM,
						prop.getProperty("OI_RICERCA"));
				// Cerca
				caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
				// Click button Azione
				caricamentoEsiti.selezionaTastoAzionePratica();
				// Verifica Stato pratica atteso
				caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "AA");
				// Selezione avento 3OK
				caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_3OK_GAS"));
				// Inserimento esito
				caricamentoEsiti.selezioneEsito("OK");
				// Calcolo sysdate
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String data = simpleDateFormat.format(calendar.getTime()).toString();
				String tipoOperazione = prop.getProperty("TIPO_OPERAZIONE");

				// Inserimento dettaglio esito 3OK in base alla tipologia processo
				// if (prop.getProperty("PRIMA_ATTIVAZIONE","N").equals("Y"))
				if (tipoOperazione.equals("PRIMA_ATTIVAZIONE")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				// if (prop.getProperty("VOLTURA_SENZA_ACCOLLO","N").equals("Y")){
				if (tipoOperazione.equals("VOLTURA_SENZA_ACCOLLO")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				if (tipoOperazione.equals("MOGE")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKMogeAnagrafica(data,
							prop.getProperty("OI_RICERCA"));
				}
				if (tipoOperazione.equals("CAMBIO_USO")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKconGestoreGAS(data,
							prop.getProperty("OI_RICERCA"));
				}
				
				
				// if (prop.getProperty("SUBENTRO_R2D","N").equals("Y")){
				if (tipoOperazione.equals("SUBENTRO_R2D")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				if (tipoOperazione.equals("DISDETTA_CON_SUGGELLO")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}

				// if (prop.getProperty("VOLTURA_CON_ACCOLLO","N").equals("Y")){
				if (tipoOperazione.equals("VOLTURA_CON_ACCOLLO")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				// if (prop.getProperty("VARIAZIONE_INDIRIZZO_FORNITURA","N").equals("Y")){
				if (tipoOperazione.equals("VARIAZIONE_INDIRIZZO_FORNITURA")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				// if (prop.getProperty("VERIFICA_CONTATORE","N").equals("Y")){
				if (tipoOperazione.equals("VERIFICA_CONTATORE")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				// if (prop.getProperty("MODIFICA_IMPIANTO","N").equals("Y")){
				if (tipoOperazione.equals("MODIFICA_IMPIANTO")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("CP_GESTORE"));
				}
				
				if (tipoOperazione.equals("PREDISPOSIZIONE_PRESA")) {
					caricamentoEsiti.inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(data,
							prop.getProperty("OI_RICERCA"));
				}

				// Salvo valore stato pratica attuale
				prop.setProperty("STATO_PRATICA", "CI");

			
			//
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
