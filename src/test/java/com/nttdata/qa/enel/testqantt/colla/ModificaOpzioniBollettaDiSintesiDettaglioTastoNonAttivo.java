package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.colla.ModificaOpzioniBollettaDiSintesiComponent;

public class ModificaOpzioniBollettaDiSintesiDettaglioTastoNonAttivo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModificaOpzioniBollettaDiSintesiComponent mocc = new ModificaOpzioniBollettaDiSintesiComponent(driver);
			
			//STEP 4
			//Click servizi
			
			By button_servizi = mocc.button_servizi;
			mocc.verifyComponentExistence(button_servizi);
			mocc.clickComponentWithJse(button_servizi);
		
//			//STEP 5
//			//Modifica tipologia bolletta
			By opzioni_bolletta_card = mocc.opzioni_bolletta_card;
			mocc.verifyComponentExistence(opzioni_bolletta_card);
			mocc.clickComponent(opzioni_bolletta_card);
			
			//Corretta apertura della pagina
//			By text_path = mocc.text_path;
//			
//			mocc.verifyComponentText(text_path, "AREA RISERVATA FORNITURE MODIFICA TIPOLOGIA BOLLETTA");
//			
//			By text_titolo = mocc.text_titolo;
//			mocc.verifyComponentText(text_titolo, "Opzioni Bolletta");
//			
//			By text_lead = mocc.text_lead;			
//			mocc.verifyComponentText(text_lead, "Con Opzioni Bolletta puoi scegliere di ricevere un bolletta sintetica o dettagliata.");
//			
//
//			By tipo_boll_title = mocc.tipo_boll_title;
//			mocc.verifyComponentText(tipo_boll_title, "Tipologia bolletta");
//			
//			By tipo_boll = mocc.tipo_boll;
//			mocc.verifyComponentText(tipo_boll, "Sintetica");
//			
//			By desc = mocc.desc;
//			mocc.verifyComponentExistence(desc);
			
			//STEP 6
			//Check sul button Modifica if is clickable
			
			Thread.sleep(10000);
			
			By button_modifica = mocc.button_modifica;
			mocc.verifyComponentExistence(button_modifica);
//			if(mocc.verifyClickability(button_modifica)) {
//				throw new Exception("The element located by: " + button_modifica + " is clickable");
//			};
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
