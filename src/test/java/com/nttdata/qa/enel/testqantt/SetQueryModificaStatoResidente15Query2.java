package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetQueryModificaStatoResidente15Query2 {
    public static void main(String[] args) throws Exception {
        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {
            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
                String msr_id4_ =
                        "SELECT Account.RecordType.Name, Account.NE__Fiscal_code__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c, ITA_IFM_Resident__c\n" +
                                "FROM Asset\n" +
                                "WHERE NE__Status__c = 'Active'\n" +
                                "AND ITA_IFM_Commodity__c = 'ELE'\n" +
                                "AND RecordType.Name = 'Commodity'\n" +
                                "AND Account.NE__Fiscal_code__c =  '"+prop.getProperty("CODICE_FISCALE")+"'";
                prop.setProperty("QUERY", msr_id4_);
            }

            prop.setProperty("RETURN_VALUE", "OK");

        }
        catch (Exception e)
        {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: "+errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
            if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
