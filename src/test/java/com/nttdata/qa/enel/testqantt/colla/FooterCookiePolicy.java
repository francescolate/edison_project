package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ArticlePageComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FooterCookiePolicy {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			//* Start Set valori da controllare
			String separator = "<;>";
			String [] legal_links_text = {"Informazioni Legali" , "Credits" , "Privacy" , "Cookie Policy" , "Remit"};
			prop.setProperty("LEGAL_LINKS_TEXT", convertArrayToStringWhitSeparator(legal_links_text, separator));

			String [] social_icons_text = {"Twitter" , "Facebook" , "Youtube" , "LinkedIn" , "Instagram" , "Telegram"};
			prop.setProperty("SOCIAL_ICONS_TEXT", convertArrayToStringWhitSeparator(social_icons_text, separator));
			prop.setProperty("SOCIAL_ICONS_TYPE" , "aria-label");	    

			prop.setProperty("COOKIE_LEGAL_LINK" , "Cookie Policy");
			prop.setProperty("COOKIE_PRIVACY_URL" , "it/supporto/faq/cookie-policy");
			prop.setProperty("COOKIE_PATH_MENU_TEXT" , "HOME / SUPPORTO / COOKIE POLICY");
			prop.setProperty("COOKIE_TITLE_MENU_TEXT" , "Cookie policy");
			prop.setProperty("COOKIE_DETAIL_MENU_TEXT" , "Informativa estesa sull’uso dei Cookie");
			
			  prop.setProperty("BEFORE_FOOTER_TEXT", "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.");
			    prop.setProperty("AFTER_FOOTER_TEXT", "Gruppo IVA Enel P.IVA 15844561009");
			   
			
		//	prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Italia S.p.a.");
		//	prop.setProperty("AFTER_FOOTER_TEXT", "© Enel Energia S.p.a. - Gruppo Iva P.IVA 12345678901");
			String [] cookie_article_texts = {
					  "Questa Cookie Policy ha lo scopo di informare gli utenti in merito alle procedure seguite per la raccolta, tramite i cookie e/o altre tecnologie di monitoraggio, delle informazioni fornite quando visitano il sito web enel.it (di seguito definito il “Sito”)."
					, "Il titolare del trattamento dei dati personali raccolti attraverso il Sito è Enel Energia S.p.A., con sede legale in viale Regina Margherita n. 125, 00198, Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007 (di seguito indicato come “Enel Energia”), che tratterà gli stessi in conformità a quanto stabilito dalla normativa applicabile in materia di privacy e protezione dei dati personali."
					, "Definizione di Cookie"
					, "I cookie sono stringhe di testo di piccole dimensioni che i siti visitati dall’utente inviano al dispositivo utilizzato (ad esempio computer, tablet, smartphone, notebook, ecc.), dove vengono memorizzati per essere poi ritrasmessi agli stessi siti alla successiva visita del medesimo utente. I cookie possono essere soggetti al preventivo consenso."
					, "Il cookie permette di identificare il dispositivo in cui è conservato per tutto il suo periodo di validità o registrazione. Durante le visite al Sito, le informazioni relative ai dati di navigazione ricevute dal dispositivo dell’utente potrebbero essere archiviate in “cookie” installati su tale dispositivo."
					, "I cookie svolgono diverse funzioni e consentono di navigare sul Sito in modo efficiente, ricordando le preferenze dell’utente e migliorandone l’esperienza di navigazione. Possono anche contribuire a personalizzare le pubblicità mostrate durante la navigazione e le attività di marketing a lui dirette, garantendo che siano realizzate in modo conforme ai suoi interessi e preferenze."
					, "È possibile modificare in ogni momento le impostazioni relative ai cookie. Di seguito sono fornite maggiori informazioni circa i cookie utilizzati dal Sito e sulle modalità di gestione delle impostazioni relative ad essi."
					, "Tipologie di Cookie utilizzati"
					, "Al momento del collegamento al Sito tramite il browser, potrebbero essere installati sul dispositivo dell’utente cookie al fine di riconoscere il dispositivo utilizzato, garantire la fruizione del Sito per la durata del periodo di validità dei cookie e raccogliere informazioni sugli accessi al Sito stesso."
					, "Durante la navigazione sul Sito, l’utente può ricevere sul suo terminale anche cookie che sono inviati da siti o da web server diversi (c.d. “terze parti”), sui quali possono risiedere alcuni elementi (es.: immagini, mappe, suoni, specifici link a pagine di altri domini) presenti sul sito che lo stesso sta visitando. Possono esserci, quindi, sotto questo profilo:"
					, "Cookie di prima parte, inviati direttamente da Enel Energia al dispositivo dell’utente."
					, "Cookie di terza parte, provenienti da una terza parte ma inviati per conto di Enel Energia."
					, "In funzione della finalità di utilizzazione dei cookie, questi si possono distinguere in:"
					, "Cookie Tecnici: il loro uso è strettamente limitato alla trasmissione di dati identificativi di sessione (numeri casuali generati dal server), necessari per consentire l’esplorazione sicura ed efficiente del Sito; essi non vengono memorizzati in modo persistente sul computer dell’utente e svaniscono con la chiusura del browser. L’uso di tali cookie è finalizzato alla sicurezza e miglioramento del servizio offerto. In particolare Enel Energia utilizza i seguenti Cookie Tecnici."
					, "Cookie Analytics: Enel Energia utilizza cookie analytics al fine di analizzare il numero degli utenti e le modalità di utilizzo del Sito, informazioni che vengono raccolte in forma aggregata. In particolare Enel Energia utilizza i seguenti Cookie Analytics."
					, "Cookie di Profilazione: i cookie di profilazione sono volti a creare profili relativi all’utente e vengono utilizzati al fine di inviare messaggi pubblicitari in linea con le preferenze manifestate dallo stesso nell’ambito della navigazione in rete o per migliorarne l’esperienza di navigazione. Questi cookie sono utilizzabili per tali scopi soltanto con il consenso dell’utente. In particolare Enel Energia si avvale anche del Pixel di Facebook, un cookie utilizzato per l’attività di retargeting, e che permette di esporre, per esempio sui social network o quando l’utente effettua una ricerca sui motori di ricerca, annunci pubblicitari coerenti con le pagine visitate in precedenza e con la navigazione sul Sito. Questa tecnologia permette a Enel Energia di inviare all’utente solo annunci a tema con i suoi gusti piuttosto che pubblicità standard."
					, "Di seguito la descrizione del cookie di profilazione di prima e di terza parte utilizzati sul Sito."
					, "Social Plugin: il Sito contiene plug-in dei social networks (per esempio Facebook, Twitter, Google+, Pinterest, ecc.). Accedendo a una pagina del Sito dotata di un simile plug-in, il browser Internet si collega direttamente ai server del social network e il plug-in viene visualizzato sullo schermo grazie alla connessione con il browser."
					, "Anche se l’utente non è registrato ai social network, i siti web con social plugin attivi possono mandare dei dati ai network. Attraverso un plugin attivo, a ogni accesso al Sito sarà impostato un cookie con una codifica. Dal momento che il browser dell’utente a ogni collegamento con un server di un network invia senza richiesta questo cookie, il network potrebbe creare un profilo che richiama dei siti web che l’utente ha visitato. In questo caso, è possibile ricondurre la codifica a una persona, per esempio quando l’utente si connetterà al social network."
					, "Gestione dei cookie tramite le impostazioni del browser"
					, "Quasi tutti i browser Internet consentono di verificare quali cookie sono presenti sul proprio hard drive, bloccare tutti i cookie o ricevere un avviso ogni volta che un cookie viene installato. In alcuni casi, tuttavia, la mancata installazione di un cookie può comportare l’impossibilità di utilizzare alcune parti del Sito."
					, "Di seguito si riportano le modalità offerte dai principali browser per consentire all’utente di manifestare le proprie opzioni in merito all’uso dei cookie:"
					, "Internet Explorer"
					, "Chrome"
					, "Firefox"
					, "Safari"
					, "Se si usa un dispositivo mobile, occorre fare riferimento al suo manuale di istruzioni per scoprire come gestire i cookie."
					, "Per ulteriori informazioni sui cookie, anche su come visualizzare quelli che sono stati impostati sul dispositivo, su come gestirli ed eliminarli, visitare il seguente link."
					, "I cookie di Facebook e Adobe Marketing Cloud possono essere rifiutati secondo la procedura indicata dai due fornitori. Pertanto, l’utilizzo del Sito senza rifiutare questi cookie implica il consenso dell’utente al trattamento dei propri dati da parte di Facebook e Adobe Marketing Cloud, limitatamente alle modalità e finalità sopraindicate."
					, "Per consultare l’informativa privacy della società Facebook, l’utente è pregato di visitare il seguente sito Internet. Per consultare l’informativa privacy di Adobe Marketing Cloud, l’utente è pregato di visitare il seguente sito Internet."
					, "I cookies di Google Analytics possono essere rifiutati secondo la procedura indicata da Google . Pertanto, l’utilizzo del Sito senza rifiutare i cookies di Google implica il consenso dell’utente al trattamento dei propri dati da parte di Google, limitatamente alle modalità e finalità sopraindicate. Per consultare l’informativa privacy della società Google, relativa al servizio Google Analytics, l’utente è pregato di visitare il seguente sito Internet. Per conoscere le norme sulla privacy di Google, l’Utente è pregato di visitare il seguente sito Internet."
					, "Se l’utente non desidera associare la visita delle pagine del Sito al proprio account social, deve effettuare il log-off. Di seguito si riportano le modalità offerte dai principali social network per consentire all’utente di disabilitare i social plugin:"
					, "Facebook: accedere al proprio account, sezione privacy."
					, "Twitter"
					, "LinkedIn"
					, "Google+"
					, "Per maggiori informazioni sull’elaborazione e l’utilizzo dei dati attraverso i social network, così come sulle impostazioni per la tutela della privacy, è opportuno verificare le privacy policy dei rispettivi social network o siti web. Di seguito sono riportati i link in questione:"
					, "Facebook"
					, "Twitter"
					, "LinkedIn"
					, "Google+"
					, "Per maggiori informazioni sul trattamento dei dati personali dell’utente, consulta l’Informativa Privacy."
					, "Informativa aggiornata maggio 2018."};
			
			prop.setProperty("COOKIE_ARTICLE_TEXTS", convertArrayToStringWhitSeparator(cookie_article_texts, separator));
			
//			setArrayPorp("COOKIE_ARTICLE_TEXT", cookie_article_texts);

			String [] cookie_article_links = {
					"i seguenti Cookie Tecnici<,>/content/dam/enel-it/documenti-supporto/cookie-policy/Cookie%20Tecnici%20Enel%20Energia.pdf<,>https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/cookie-policy/Cookie%20Tecnici%20Enel%20Energia.pdf"
		    		, "i seguenti Cookie Analytics<,>/content/dam/enel-it/documenti-supporto/cookie-policy/Cookie%20Analytics%20Enel%20Energia.pdf<,>https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/cookie-policy/Cookie%20Analytics%20Enel%20Energia.pdf"
//		    		, "la descrizione del cookie di profilazione<,>/content/dam/enel-it/documenti-supporto/cookie-policy/Cookie%20Policy%20EE_%20tabella%20cookie_prof_ottobre.pdf<,>https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/cookie-policy/Cookie%20Policy%20EE_%20tabella%20cookie_prof_ottobre.pdf"
		    		, "Internet Explorer<,>http://windows.microsoft.com/it-it/internet-explorer/delete-manage-cookies#ie=ie-11<,>https://support.microsoft.com/en-us/help/17442/windows-internet-explorer-delete-manage-cookies#ie=ie-11"
		    		, "Chrome<,>https://support.google.com/chrome/answer/95647?hl=it<,>https://support.google.com/chrome/answer/95647?hl=it"
		    		, "Firefox<,>https://support.mozilla.org/it/kb/Gestione%20dei%20cookie<,>https://support.mozilla.org/it/kb/Gestione%20dei%20cookie"
		    		, "Safari<,>http://support.apple.com/kb/HT1677?viewlocale=it_IT<,>https://support.apple.com/it-it/HT201265"
		    		, "il seguente link<,>http://www.allaboutcookies.org/<,>https://www.allaboutcookies.org/"
		    		, "visitare il seguente sito Internet<,>https://www.facebook.com/policies/cookies/<,>https://www.facebook.com/policies/cookies/"
		    		, "il seguente sito Internet<,>https://www.adobe.com/it/privacy.html<,>https://www.adobe.com/it/privacy.html"
		    		, "da Google<,>https://tools.google.com/dlpage/gaoptout?hl=it<,>https://tools.google.com/dlpage/gaoptout?hl=it"
		    		, "visitare il seguente sito Internet<,>http://www.google.com/analytics/learn/privacy.html<,>https://marketingplatform.google.com/about/"
		    		, "di visitare il seguente sito Internet<,>http://www.google.com/intl/it/privacy/privacy-policy.html<,>https://policies.google.com/privacy?hl=it"
		    		, "Twitter<,>https://twitter.com/settings/security<,>https://twitter.com/login"
		    		, "LinkedIn<,>https://www.linkedin.com/settings/<,>https://www.linkedin.com/signup/cold-join?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fpsettings%2F"
		    		, "Google+<,>http://www.google.it/intl/it/policies/technologies/managing/<,>https://policies.google.com/technologies/managing?hl=it"
		    		, "Facebook<,>https://www.facebook.com/help/cookies/<,>https://www.facebook.com/policies/cookies/"
		    		, "Twitter<,>https://support.twitter.com/articles/20170514<,>https://help.twitter.com/en/rules-and-policies/twitter-cookies"
		    		, "LinkedIn<,>https://www.linkedin.com/legal/cookie-policy<,>https://www.linkedin.com/legal/cookie-policy"
		    		, "Google+<,>http://www.google.it/intl/it/policies/technologies/cookies/<,>https://policies.google.com/technologies/cookies?hl=it"
		    		, "l’Informativa Privacy<,>it/supporto/faq/privacy<,>https://www-coll1.enel.it/it/supporto/faq/privacy"
		    		, "DOWNLOAD<,>/content/dam/enel-it/documenti-supporto/cookie-policy/Tabella%20Cookie%20Enel%20Energia.pdf<,>https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/cookie-policy/Tabella%20Cookie%20Enel%20Energia.pdf"
		    		};
			
			prop.setProperty("COOKIE_ARTICLE_LINKS", convertArrayToStringWhitSeparator(cookie_article_links , separator));

			//** END Valori da controllare
			
			String separator_1 = "<;>";
			String separator_2 = "<,>";
			logger.write("check text and links on the footer of page 'https://www-coll1.enel.it/' - Start ");
			
			FooterPageComponent footer=new FooterPageComponent(driver);
			
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			
			Thread.sleep(5000);
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By links_object=footer.legal_links;
			String[] local_legal_links_text = prop.getProperty("LEGAL_LINKS_TEXT").split(separator_1);
			//footer.checkFooterLegalLinksByTexts(links_object, local_legal_links_text);
			By social_icons_object=footer.social_icons;
			String[] local_social_icons_text = prop.getProperty("SOCIAL_ICONS_TEXT").split(separator_1);
			footer.checkFooterSocialIconsByAttibuteText(social_icons_object, local_social_icons_text , prop.getProperty("SOCIAL_ICONS_TYPE"));
			footer.clickFooterLegalLinkByText(prop.getProperty("COOKIE_LEGAL_LINK"));
			footer.checkUrl(prop.getProperty("LINK") + prop.getProperty("COOKIE_PRIVACY_URL"));
			
			TopMenuComponent topMenu = new TopMenuComponent(driver);
			
			topMenu.checkMenuPath(prop.getProperty("COOKIE_PATH_MENU_TEXT"));
			topMenu.checkTitle(prop.getProperty("COOKIE_TITLE_MENU_TEXT"));
			topMenu.checkDetailByClass(prop.getProperty("COOKIE_DETAIL_MENU_TEXT") , "hero_detail text");	
			
			ArticlePageComponent article = new ArticlePageComponent(driver);
			
			String[] local_cookie_article_texts = prop.getProperty("COOKIE_ARTICLE_TEXTS").split(separator_1);
			//article.checkArticlePageText(local_cookie_article_texts);
			By cookie_button = article.article_cookie_button;
			article.clickCookieButton(cookie_button);
			String[] local_cookie_article_links = prop.getProperty("COOKIE_ARTICLE_LINKS").split(separator_1);
			//article.checkPageLinksMultiTab(local_cookie_article_links , separator_2);
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
	
	public static String convertArrayToStringWhitSeparator(String[] array , String separator) {
		String prop = "";
		for(String el : array) {
			prop = prop.concat(el).concat(separator);
		}
		prop = prop.substring(0, prop.length() - separator.length());
		return prop;
	}

//	public static String[] getArrayProp(String key, Properties prop) {
//		List<String> list = new ArrayList<String>();
//		int i=0;
//		while(prop.getProperty(key + "_" + String.format("%03d", i)) != null) {
//			list.add(prop.getProperty(key + "_" + String.format("%03d", i)));
//			i++;
//		}
//		String[] array = new String[list.size()];
//		list.toArray(array);
//		return array;
//	}
}
