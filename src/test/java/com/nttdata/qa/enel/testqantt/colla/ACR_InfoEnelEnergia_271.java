package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_InfoEnelEnergia_271 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			Thread.sleep(10000);
		//	driver.navigate().to("https://www-coll1.enel.it/it/area-clienti/residenziale");
			hm.verifyComponentExistence(hm.dettaglioFurniture_271);
			hm.clickComponent(hm.dettaglioFurniture_271);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			Thread.sleep(10000);
			dc.verifyComponentExistence(dc.infoEnelEnergiaTile);
			dc.clickComponent(dc.infoEnelEnergiaTile);
			//dc.verifyComponentExistence(dc.errorTitle);
			//dc.comprareText(dc.errorText, dc.ErrorText, true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
