package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaCVPDipendentiENEL {
	@Step("Carica Documenti")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		driver.switchTo().defaultContent();
		RicercaOffertaComponent offerta = new RicercaOffertaComponent(driver);
		DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
		TimeUnit.SECONDS.sleep(30);
		util.objectManager(doc.tabDettagli, util.scrollAndClick);
		logger.write("Verifica CVP Dipendente ENEL-Inizio");
		if(offerta.verificaEsitoCVP(offerta.statoCVP)){
			throw new Exception("Per richieste con Promo Dipendente ENEL l'esito CVP non deve essere invocato. Il campo Stato CVP non deve essere popolato");
		}
	    logger.write("Verifica CVP Dipendente ENEL-Fine");

	} catch (Exception e) {
		prop.setProperty("RETURN_VALUE", "KO");
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: "+errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
		if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

	}finally
	{
		//Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
	}

}
}
