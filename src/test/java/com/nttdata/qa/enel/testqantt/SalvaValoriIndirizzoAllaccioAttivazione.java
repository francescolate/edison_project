package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class SalvaValoriIndirizzoAllaccioAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
  
      
            logger.write("Salva valori Indirizzo di Fatturazione - Start");
            
            prop.setProperty("PROVINCIAINDFATT", compila.salvaValoreIndirizzo("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../input"));
			prop.setProperty("REGIOENINDFATT", compila.salvaValoreIndirizzo("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Regione')]/../input"));
			prop.setProperty("COMUNEINDFATT", compila.salvaValoreIndirizzo("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../input"));
			prop.setProperty("INDIRIZZOINDFATT", compila.salvaValoreIndirizzo("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Indirizzo')]/../input"));
			prop.setProperty("CIVICOINDFATT", compila.salvaValoreNumeroCivico("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Numero Civico')]"));
			prop.setProperty("CAPINDFATT", compila.salvaValoreIndirizzo("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../input"));
			prop.setProperty("LOCALITAINDFATT", compila.salvaValoreIndirizzo("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Località')]/../input"));

            logger.write("Salva valori Indirizzo di Fatturazione - Completed");
    
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
