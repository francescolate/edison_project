package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaComboClienteFornisceIndirizzoResidenzaSedeLegale {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
				SeleniumUtilities util = new SeleniumUtilities(driver);
				RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				
				logger.write("verifica valore combo 'Cliente fornisce l’indirizzo' sia uguale a SI  - Start");
				String comp=compila.getValue(compila.clienteFornisceIndirizzoSelect);
				if (!comp.equals("SI"))
					{throw new Exception("La Combo 'Cliente fornisce indirizzo' non presenta il valore di default settato a SI");
					}	
				// sezione Indirizzi di Residenza/sede Legale Click su Conferma
				if (offer.verifyClickability(offer.buttonConfermaIndirizzoRes)) {
					offer.clickComponent(offer.buttonConfermaIndirizzoRes);
					gestione.checkSpinnersSFDC();
				} else if (offer.verifyClickability(offer.buttonModificaIndirizzoRes)) {
					offer.clickComponent(offer.buttonVerificaIndirizzoRes);
					gestione.checkSpinnersSFDC();
					offer.clickComponent(offer.buttonConfermaIndirizzoRes);
				}

				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
