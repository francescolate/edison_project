package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_ACR_78 {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
//			prop.setProperty("FORNITURETITLE", "Servizi per le forniture");
//			prop.setProperty("BOLLETTETITLE", "Servizi per le bollette");
//			prop.setProperty("CONTRATTOTITLE", "Servizi per il contratto");
//			prop.setProperty("FORNITURECONTENT", "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture");
//			prop.setProperty("BOLLETTECONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette");
//			prop.setProperty("CONTRATTOCONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture");
//			prop.setProperty("BOLLETTEHEADING", "Benvenuto nella tua area privata");
//			prop.setProperty("BOLLETTEHEADINGCONTENT", "In questa sezione potrai gestire le tue forniture");
//			prop.setProperty("BOLLETTEDETAILSHEADING", "Le tue bollette");
//			prop.setProperty("BOLLETTEDETAILSHEADINGCONTENT", "Qui potrai visualizzare bollette e/o rate delle tue forniture.");
			
			//Supply Details Servizi page
			
			SuplyPrivateDetailComponent spdc = new SuplyPrivateDetailComponent(driver);
			
			spdc.clickComponent(spdc.ServiziLink);
			Thread.sleep(5000);
			
			logger.write("Verify the title - Start");
//			spdc.VerifyBollette(spdc.FornituraTitle);
//			spdc.VerifyBollette(spdc.FornituraTitleContent);
			spdc.VerifyBollette(spdc.BolletteTitle);
			spdc.VerifyBollette(spdc.BolletteTitleContent);
			spdc.VerifyBollette(spdc.ContratoTitle);
			spdc.VerifyBollette(spdc.ContratoTitleContent);
			logger.write("Verify the title  - Complete");
			
			logger.write("Verify the  contents - Start");
			spdc.VerifyForniture(spdc.FornitureContents);
			spdc.VerifyBollette(spdc.BolletteContents);
			logger.write("Verify the  contents - Complete");
			
			logger.write("Click on Modiffica link - Start");
			spdc.clickComponent(spdc.ModifficaIndrizzolink);
			logger.write("Click on Modiffica link - Complete");
			
			logger.write("Verify the title and contents - Start");
			spdc.verifyComponentExistence(spdc.ModificaTitle);
			spdc.verifyComponentExistence(spdc.ModificaContents);
			logger.write("Verify the title and contents - Complete");
			
			logger.write("Verify Esci button and click - Start");
			spdc.verifyComponentExistence(spdc.ModificaEsci);
			spdc.clickComponent(spdc.ModificaEsci);
			logger.write("Verify Esci button and click - Complete");
			
			logger.write("Verify the left menu list - Start");
//			spdc.homepageRESIDENTIALMenu(spdc.LeftMenuList);
			logger.write("Verify the left menu list - Complete");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		

	}

}
