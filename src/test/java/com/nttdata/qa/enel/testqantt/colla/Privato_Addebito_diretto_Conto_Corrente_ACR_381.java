package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AddebitoDirettoComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Addebito_diretto_Conto_Corrente_ACR_381 {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			AddebitoDirettoComponent ad = new AddebitoDirettoComponent(driver);
			
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			df.verifyComponentExistence(df.fornitureHeader);
			df.comprareText(df.fornitureHeader, DettaglioFornitureComponent.FORNITURE_HEADER, true);
			df.verifyComponentExistence(df.fornitureContent);
			df.comprareText(df.fornitureContent, DettaglioFornitureComponent.FORNITURE_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
						
			logger.write("Click on Servizi link  - Start");
			df.verifyComponentExistence(df.serivizLink);
			df.clickComponent(df.serivizLink);
			Thread.sleep(50000);
			df.verifyComponentExistence(df.serviziBolletteHeading);
			df.comprareText(df.serviziBolletteHeading, DettaglioFornitureComponent.SERVIZIBOllE_HEADING, true);
			df.verifyComponentExistence(df.seriviziBolletteContent);
			df.comprareText(df.seriviziBolletteContent, DettaglioFornitureComponent.SERVIZIBOLLE_CONTENT, true);
			logger.write("Click on Servizi link  - Complete");
		
			logger.write("Click on Addebito director   - Start");
			df.verifyComponentExistence(df.addebitoLink);
			df.clickComponent(df.addebitoLink);
			logger.write("Click on Addebito director   - Complete");
						
			logger.write("Verify the Addebito directo header   - Start");
			df.verifyComponentExistence(df.addebitoDirectoHeader);
			df.comprareText(df.addebitoDirectoHeader, DettaglioFornitureComponent.ADDEBITTODIRECTO_HEADER, true);
			df.verifyComponentExistence(df.addebitoDirectoContent);
			df.comprareText(df.addebitoDirectoContent, DettaglioFornitureComponent.ADDEBITTODIRECTO_CONTENT, true);
			logger.write("Verify the Addebito directo header  - Complete");
			
			logger.write("click on visualizzaTutte and verify   - Start");
			ad.clickComponent(ad.visualizzaTutte);
			
			logger.write("click on visualizzaTutte and verify   - Complete");
			
			logger.write("click on ATTIVASUCONTOCORRENTE and verify   - Start");
			df.clickComponent(df.aTTIVASUCONTOCORRENTE);
			logger.write("click on ATTIVASUCONTOCORRENTE and verify   - Complete");
			
			logger.write("click on checkBox and verify   - Start");
			df.clickComponent(df.checkBox);
			logger.write("click on checkBox and verify   - Complete");
			
			logger.write("click on AttivaAddebitoBtn and verify   - Start");
			df.clickComponent(df.attivaAddebitoBtn);
			logger.write("click on AttivaAddebitoBtn and verify   - Complete");
			
			logger.write("Enter IBAN and verify   - Start");
			df.clickComponent(df.ibanCheckbox);
			df.enterInput(df.ibanInput, prop.getProperty("IBAN"));
			logger.write("click on AttivaAddebitoBtn and verify   - Complete");
			
			logger.write("click on continuaButton and verify   - Start");
			df.verifyComponentExistence(df.continuaButton);
			df.clickComponent(df.continuaButton);
			logger.write("click on continuaButton and verify   - Complete");
			
			logger.write("click on confermaButton and verify   - Start");
			df.verifyComponentExistence(df.confermaButton);
			df.clickComponent(df.confermaButton);
			logger.write("click on confermaButton and verify   - Complete");
			
			logger.write("click on addebitoFineButton and verify   - Start");
			df.verifyComponentExistence(df.addebitoFineButton);
			df.clickComponent(df.addebitoFineButton);
			logger.write("click on addebitoFineButton and verify   - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
						
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());
			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
}