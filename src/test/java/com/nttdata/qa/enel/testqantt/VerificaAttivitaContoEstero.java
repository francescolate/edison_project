package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaAttivitaContoEstero {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			String query=Costanti.Query_id_94_by_id_richiesta;
			String idRichiesta=prop.getProperty("NUMERO_RICHIESTA");
			query=query.replace("@ID_RICHIESTA@", idRichiesta);
			
			
			
			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);
			Map<String,String> result=wb.EseguiQuery(query).get(1);
			
			
			String ITA_IFM_Number__c=result.get("ITA_IFM_Number__c");
			String ITA_IFM_Causale_Contatto__c=result.get("ITA_IFM_Causale_Contatto__c");
			String ITA_IFM_Descrizione__c=result.get("ITA_IFM_Descrizione__c");	
			String ITA_IFM_Specifica__c=result.get("ITA_IFM_Specifica__c");	
			String ITA_IFM_Tipo_Attivita__c=result.get("ITA_IFM_Tipo_Attivita__c");	
			String ITA_IFM_Status__c=result.get("ITA_IFM_Status__c");	
			String Name=result.get("Name");	
	

			
			
			prop.setProperty("ITA_IFM_Number__c",ITA_IFM_Number__c );
			
			

			
			
			
			
			if(!ITA_IFM_Tipo_Attivita__c.contentEquals("Altro"))
			{
				
				throw new Exception(String.format("Tipo Attività - Valore: %s - Valore atteso: %s",ITA_IFM_Tipo_Attivita__c,"Altro"));
			}
			if(!ITA_IFM_Causale_Contatto__c.contentEquals("AA ANY vs CRE"))
			{
				
				throw new Exception(String.format("Causale Contratto - Valore: %s - Valore atteso: %s",ITA_IFM_Causale_Contatto__c,"AA ANY vs CRE"));
			}
			if(!ITA_IFM_Descrizione__c.contentEquals("Credito"))
			{
				
				throw new Exception(String.format("Descrizione Credito - Valore: %s - Valore atteso: %s",ITA_IFM_Descrizione__c,"Credito"));
			}	
			if(!ITA_IFM_Specifica__c.contentEquals("Domiciliazione estera"))
			{
				
				throw new Exception(String.format("Specifica - Valore: %s - Valore atteso: %s",ITA_IFM_Specifica__c,"Domiciliazione estera"));
			}	
		

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
