package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.colla.CompletaTP2Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ConfermaTP2Subentro {

    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            CompletaTP2Component CompletaTP2Component = new CompletaTP2Component(driver);

            TimeUnit.SECONDS.sleep(30);
            driver.switchTo().defaultContent();

            CompletaTP2Component.clickComponent(CompletaTP2Component.confermaButtonTp2);

            TimeUnit.SECONDS.sleep(30);
            driver.switchTo().defaultContent();
            // clicca prosegui al banner "Ci siamo quasi!"
            CompletaTP2Component.clickComponent(CompletaTP2Component.proseguiButtonTp2);

            TimeUnit.SECONDS.sleep(30);
            driver.switchTo().defaultContent();

            //inserimento sezione "COMPILAZIONE DOCUMENTI"
            CompletaTP2Component.clickComponent(CompletaTP2Component.radioButtonTp2);

            CompletaTP2Component.clickComponent(CompletaTP2Component.radioButtonAbitazioneTp2);

            CompletaTP2Component.clickComponent(CompletaTP2Component.confermaButton2tp2);

            TimeUnit.SECONDS.sleep(30);
            driver.switchTo().defaultContent();

            CompletaTP2Component.clickComponent(CompletaTP2Component.uploadDocument);



            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
