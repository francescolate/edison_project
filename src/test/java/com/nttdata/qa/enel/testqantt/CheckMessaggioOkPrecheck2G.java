package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CheckMessaggioOkPrecheck2G {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			logger.write("Verifica Messaggio Precheck 2G OK- Start");
			
			configura.verifyComponentExistence(configura.messaggioOKPrecheck2G);
			
			logger.write("Verifica Messaggio Precheck 2G OK- Completed");

			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
