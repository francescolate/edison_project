package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFibraMelitaEsci {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);
	
			//Click Esci
//			logger.write("checking the menu item Servizi is present within the page - START");
			
//			By icon_close_chat = paf.icon_close_chat;
//			
//			paf.verifyComponentExistence(icon_close_chat);
//			paf.clickComponent(icon_close_chat);
			
//			By div_chat_box = paf.div_chat_box;
//			
//			paf.verifyAttributeValue(div_chat_box, "style", "display: block;");
//			
//			By close_chat = By.partialLinkText("javascript:void(0);");
//			
//			paf.verifyComponentExistence(close_chat);
//			paf.clickComponent(close_chat);
			
			//STEP 7 Tasto Esci
			logger.write("click the exit button - START");
			
			By id_frame = paf.id_frame;
			
			paf.verifyComponentExistence(paf.id_frame);	
			
			paf.verifyFrameAvailableAndSwitchToIt(id_frame);
			
			By button_exit = paf.button_exit;
			
			paf.verifyComponentExistence(button_exit);
			
			/*paf.clickComponent(button_exit);
			
			logger.write("click the exit button - COMPLETED");
			
			logger.write("check if the exit button worked by checking the url - START");
			
			paf.confirmUrl(prop.getProperty("WP_LINK") + paf.url_residential_client);
			
			logger.write("check if the exit button worked by checking the url - COMPLETED");*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	
		
		
	}

}
