package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.CheckVariazioneAnagraficaComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CheckVariazioneAnagrafica {
	@Step("Get account from CF")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			Thread.sleep(120000);
			
			CheckVariazioneAnagraficaComponent cvac = new CheckVariazioneAnagraficaComponent(driver);
			GetAccountFromComponent gafc = new GetAccountFromComponent(driver);
			
			logger.write("typing the user CF - START");	
			gafc.verifyComponentExistence(gafc.searchBar);
			gafc.searchAccount(gafc.searchBar, prop.getProperty("CF"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("checking the data existence - START");	
			/*gafc.verifyComponentExistence(gafc.accountLink);
			gafc.clickComponent(gafc.accountLink);
			*/
			gafc.verifyComponentExistence(gafc.clientiNome);
			gafc.jsClickComponent(gafc.clientiNome);
			logger.write("checking the data existence - COMPLETE");
			
			/*cvac.verifyComponentExistence(cvac.richiesteHeader);
			cvac.scrollToElement(cvac.richiesteHeader);
			cvac.jsClickComponent(cvac.richiesteHeader);
			TimeUnit.SECONDS.sleep(5);
			cvac.jsClickComponent(cvac.variazioneAnagraficaTD);
			
			cvac.clickComponent(cvac.dettagliButton);
			
			cvac.verifyComponentExistence(cvac.origineCaso);
			cvac.verifyComponentExistence(cvac.stato);
			cvac.verifyComponentExistence(cvac.statoSecondario);
			
			cvac.switchToFrame(cvac.newDataIframe);
			
			Thread.sleep(10000);
			cvac.comprareText(cvac.newDataEmail, prop.getProperty("EMAIL"), true);
			cvac.comprareText(cvac.newDataTelefono, prop.getProperty("PHONE"), true);*/
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
