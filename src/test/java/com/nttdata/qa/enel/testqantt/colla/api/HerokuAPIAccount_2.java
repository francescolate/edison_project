// HerokuAPIAccount_1 package WEB - COMPARE - per scenari senza Source e Channel

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;

import com.nttdata.jwt.gen.JWToken;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIAccountComponent_Err;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIAccount_2 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku Authenticate Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			//Da configurare come OutPut
    		// prop.setProperty("Authorization", "Basic QXZ2MGYyRjA6ZjBlWERzSHc=");
    		// prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
	   		// prop.setProperty("SOURCECHANNEL","WEB");
			if(prop.getProperty("Authorization")==null)
				prop.setProperty("Authorization", "Bearer " + JWToken.getWebToken(prop.getProperty("CF_PIVA"), prop.getProperty("USERNAME"),prop.getProperty("USERUPN"), prop.getProperty("ENELID"), prop.getProperty("EXPIRATION")));
			 
			if(prop.getProperty("JSON_INPUT")==null)
				prop.setProperty("JSON_INPUT", "{   \"data\":{      \"enelId\":\"" + prop.getProperty("ENELID") + "\"  }}");

			prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString());
			prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString());
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIAccountComponent_Err HRKComponent = new HerokuAPIAccountComponent_Err();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
