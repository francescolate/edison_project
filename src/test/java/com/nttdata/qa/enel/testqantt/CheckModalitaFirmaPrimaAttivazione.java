package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;



public class CheckModalitaFirmaPrimaAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			RiepilogoOffertaComponent modalita = new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
	          
			gestione.checkSpinnersSFDC();
			logger.write("check campi Modalità Firma - Start ");
			modalita.verificaCampiPrepopolatiModalitaFirmaCanale();
			logger.write("check campi Modalità Firma - Completed ");
			logger.write("click su Conferma nella sezione 'Modalita' firma e Canale Invio' - Start ");
			modalita.clickComponent(modalita.buttonModificaIndirizzoModFirmaCanale);
			TimeUnit.SECONDS.sleep(3);
            modalita.clickComponent(modalita.buttonConfermaModFirmaCanale);
            gestione.checkSpinnersSFDC();
            TimeUnit.SECONDS.sleep(3);
            logger.write("click su Conferma nella sezione 'Modalita' firma e Canale Invio' - Completed ");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
