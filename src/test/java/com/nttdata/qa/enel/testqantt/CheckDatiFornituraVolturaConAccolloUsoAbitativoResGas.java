package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class CheckDatiFornituraVolturaConAccolloUsoAbitativoResGas {
    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
            GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(driver);

            logger.write("Selezione commodity - Start");
            compila.selezionaFornitura(compila.fornitura_gas_allaccio);
            logger.write("Selezione commodity - Completed");

            logger.write("Inserimento Dati Fornitura - Start");

            String[] lista = {
                    "Riscald.to appartamento <100mq",
                    "Riscald.to appartamento >100mq",
                    "Riscald.to appartamento <60mq",
                    "Riscald.to villa/casa indipend",
                    "Uso Cucina",
                    "Uso cucina prod.ne acqua calda"
            };

            compila.verificaCategoriaDiConsumo(lista, prop.getProperty("CATEGORIA_DI_CONSUMO"));

            conferma.verificaCampiSezioneCommodityVolturaConAccolloResGas(prop.getProperty("POD"));

            logger.write("Inserimento Dati Fornitura - Completed");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }


    public static String checklistMessage = "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:  POD/PDR/numero utenza.INFORMAZIONI UTILI - Il Cambio Uso della fornitura di energia elettrica ha un costo pari a: 0,00 €.- Il Cambio Uso della fornitura di gas ha un costo pari a: 0,00 €.- Contestualmente alla variazione uso è possibile attivare/modificare/revocare i Servizi VASe il Metodo di pagamento - Per la variazione uso gas, nel caso di modifica dell'uso fornitura, è necessario eseguire il cambio prodotto.Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto.";
}
