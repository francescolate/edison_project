package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetSubentro7R2D {
    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")){
                // Dati R2d
        		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
        		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
        		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
                prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
                prop.setProperty("OI_RICHIESTA", prop.getProperty("OI_ORDINE"));
                prop.setProperty("TIPO_OPERAZIONE", "SUBENTRO_EVO");
                prop.setProperty("SKIP_POD", "N");
                prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
                prop.setProperty("STATO_R2D", "AW");
                prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Subentro");
                prop.setProperty("INDICE_POD", "1");
                prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Esito Ammissibilità Richiesta S01");
                prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito S01 subentro a parità di condizioni");
                prop.setProperty("CODICE_CAUSALE_5OK_ELE", "EVASA");
                prop.setProperty("VOLTAGGIO", "220");
                prop.setProperty("POTENZA", "4.4");
                prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
                prop.setProperty("POTENZA_IMPEGNATA", "4");
                prop.setProperty("RIGA_DA_ESTRARRE", "1");
            }

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
