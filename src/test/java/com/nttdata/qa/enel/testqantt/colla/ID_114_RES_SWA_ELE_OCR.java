package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.OffertalaunchComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_114_RES_SWA_ELE_OCR {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			OffertalaunchComponent oc =new OffertalaunchComponent(driver);
			logger.write("Verify page navigation to Offerta page and page details -- Strat");
			driver.navigate().to(oc.offertaUrl);
			
			oc.verifyComponentExistence(oc.aderisci);
			oc.checkURLAfterRedirection(oc.offertaUrl);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.comprareText(oc.pageText, oc.PageText, true);
			oc.verifyComponentExistence(oc.inserisciITuoiDati);
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.verifyComponentExistence(oc.pagamentiEBollette);
			oc.verifyComponentExistence(oc.consensi);
			oc.verifyComponentExistence(oc.caricaBolletta);
			oc.verifyComponentExistence(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");

			oc.clickComponent(oc.compilaManualmente);
			
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Strat");
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(5000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(25000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");
			//div[@id='panel1a-header']//h3[@class='sub_header']
			//p[normalize-space()='1/4']
			oc.clickComponent(oc.inserisciITuoiDati );
			oc.clickComponent(oc.Modi);
			oc.clickComponent(oc.ANNULLA);
			oc.clickComponent(oc.Modi);
			oc.jsClickComponent(oc.CONTINUA);
		
			oc.enterInputParameters(oc.cellularechange, prop.getProperty("CELLULARECHANGE"));
			oc.clickComponent(oc.prosegui1);
		
			oc.clickComponent(oc.inserisciITuoiDati );

			
						
			oc.checkURLAfterRedirection(prop.getProperty("WP_LINK"));
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
