package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_328_ACR_Addebito_Diretto {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			/*WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
				
			lpv.launchLink(prop.getProperty("WP_LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
	
			logger.write("Launch the login page  - Complete");	
			
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
												
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentExistence(dfc.buttonLoginAccedi);
			
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("WP_USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("WP_PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");*/
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);

			
			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			df.verifyComponentExistence(df.fornitureHeader);
			df.comprareText(df.fornitureHeader, DettaglioFornitureComponent.FORNITURE_HEADER, true);
			df.verifyComponentExistence(df.fornitureContent);
			df.comprareText(df.fornitureContent, DettaglioFornitureComponent.FORNITURE_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
			
			logger.write("Click on Detaglio Fornitura button  - Start");
			df.verifyComponentExistence(df.DetaglioFornitura_310502654);
			df.clickComponent(df.DetaglioFornitura_310502654);
			logger.write("Click on Detaglio Fornitura button  - Complete");
			
			logger.write("Verify the Luce header and contents  - Start");
			df.verifyComponentExistence(df.luceHeading);
			df.comprareText(df.luceHeading, DettaglioFornitureComponent.LUCE_HEADING, true);
			df.verifyComponentExistence(df.luceContent);
			df.comprareText(df.luceContent, DettaglioFornitureComponent.LUCE_CONTENT, true);
			logger.write("Verify the Luce header and contents  - Complete");
			
			logger.write("Verify the Bollette servizi header  - Start");
			df.verifyComponentExistence(df.serviziBolletteHeading);
			df.comprareText(df.serviziBolletteHeading, DettaglioFornitureComponent.SERVIZIBOllE_HEADING, true);
			df.verifyComponentExistence(df.seriviziBolletteContent);
			df.comprareText(df.seriviziBolletteContent, DettaglioFornitureComponent.SERVIZIBOLLE_CONTENT, true);
			logger.write("Verify the Bollette servizi header  - Complete");
			
			logger.write("Click on Addebito director   - Start");
			df.verifyComponentExistence(df.addebitoDirectolink);
			df.clickComponent(df.addebitoDirectolink);
			logger.write("Click on Addebito director   - Complete");
			
			logger.write("Verify the Addebito directo header   - Start");
			df.verifyComponentExistence(df.addebitoDirectoHeader);
			df.comprareText(df.addebitoDirectoHeader, DettaglioFornitureComponent.ADDEBITTODIRECTO_HEADER, true);
			df.verifyComponentExistence(df.addebitoDirectoContent);
			df.comprareText(df.addebitoDirectoContent, DettaglioFornitureComponent.ADDEBITTODIRECTO_CONTENT, true);
			logger.write("Verify the Addebito directo header  - Complete");
			
			logger.write("Verify the Indrizzo and Numero cliente values   - Start");
			df.verifyComponentExistence(df.indrizzo00141);
			df.comprareText(df.indrizzo00141, DettaglioFornitureComponent.INDRIZZO_VALUE, true);
			df.verifyComponentExistence(df.numeroCliente310502654);
			df.comprareText(df.numeroCliente310502654, DettaglioFornitureComponent.NUMEROCLIENTE_VALUE, true);
			logger.write("Verify the Indrizzo and Numero cliente values   - Complete");
			
			logger.write("Click on Attiva Corrente  - Start");
			df.verifyComponentExistence(df.attivaCorrente);
			df.clickComponent(df.attivaCorrente);
			logger.write("Click on Attiva Corrente  - Complete");
			
			logger.write("Verify the Attiazione header and contents  - Start");
			df.verifyComponentExistence(df.attivazioneHeader);
			df.comprareText(df.attivazioneHeader, DettaglioFornitureComponent.ATTIVAZIONE_HEADER, true);
			df.verifyComponentExistence(df.attivazioneContent);
			df.comprareText(df.attivazioneContent, DettaglioFornitureComponent.ATTIVAZIONE_CONTENT, true);
			logger.write("Verify the Attiazione header and contents  - Complete");
			
			logger.write("Verify the Attiva Indrizzo and Numero cliente values  - Start");
			df.verifyComponentExistence(df.attivaIndrizzo);
			df.comprareText(df.attivaIndrizzo, DettaglioFornitureComponent.ATTIVA_INDRIZZO, true);
			df.verifyComponentExistence(df.attivaNumero310502654);
			df.comprareText(df.attivaNumero310502654, DettaglioFornitureComponent.ATTIVA_NUMERO, true);
			logger.write("Verify the Attiva Indrizzo and Numero cliente values  - Complete");
			
			logger.write("Select  on the checkbox and click on Attiva Addebito button - Start");
			df.verifyComponentExistence(df.checkbox310502654);
			df.clickComponent(df.checkbox310502654);
			df.verifyComponentExistence(df.attivaAddebitoBtn);
			df.clickComponent(df.attivaAddebitoBtn);
			logger.write("Select  on the checkbox and click on Attiva Addebito button - Complete");
			
			Thread.sleep(5000);
			logger.write("Verify the steps - Start");
			
			df.attivazioneverifyStepStatus(prop.getProperty("INSERTIMOCOLOR"));
			logger.write("Verify the steps - Complte");
			
			logger.write("Verify the input fields - Start");
			df.verifyComponentExistence(df.cognomeLabel);
			df.comprareText(df.cognomeLabel, DettaglioFornitureComponent.CONGNOME_LABEL, true);
			df.verifyComponentExistence(df.codiceLabel);
			df.comprareText(df.codiceLabel, DettaglioFornitureComponent.CODICELABEL, true);
			df.verifyComponentExistence(df.ibanLabel);
			df.comprareText(df.ibanLabel, DettaglioFornitureComponent.IBANLABEL, true);

			df.verifyComponentExistence(df.ibanCheckbox);
		//	df.clickComponent(df.ibanCheckbox);

			//df.verifyComponentExistence(df.ibanCheckbox);
			//df.clickComponent(df.ibanCheckbox);

			df.enterInput(df.ibanInput, prop.getProperty("IBAN"));
			df.verifyComponentExistence(df.attivazioneindietroButton);
			df.verifyComponentExistence(df.continuaButton);
			df.clickComponent(df.continuaButton);
			logger.write("Verify the input fields - Complete");
			
			logger.write("Verify labels and values - Start");
			df.verifyComponentExistence(df.verificaCongnomelabel);
			df.comprareText(df.verificacongnomeValue,DettaglioFornitureComponent.VERIFICACONGNOME_VALUE , true);
			df.verifyComponentExistence(df.verificaNomeLabel);
			df.comprareText(df.verificaNomeValue, DettaglioFornitureComponent.VERIFICANOME_VALUE, true);
			df.verifyComponentExistence(df.veroficaCcodiceLabel);
			df.comprareText(df.verificaCcodiceValue, DettaglioFornitureComponent.VERIFICACODICE_VALUE, true);
			df.verifyComponentExistence(df.verificaIbanLabel);
			df.comprareText(df.verificaIbanValue, DettaglioFornitureComponent.VERIFICAIBAN_VALUE, true);
			logger.write("Verify the labels and values- Complete");
			
			logger.write("Verify the header and buttons- Start");
			df.verifyComponentExistence(df.laModalitaHeader);
			df.comprareText(df.laModalitaHeader, DettaglioFornitureComponent.LAMODALITA_HEADER, true);
			df.verifyComponentExistence(df.confermaButton);
			df.verifyComponentExistence(df.attivazioneindietroButton);
			logger.write("Verify the header and buttons- Complete");		
			
			logger.write("Click on Conferma button- Start");
			//df.clickComponent(df.confermaButton);
			logger.write("Click on Conferma button- Complete");
			

			Thread.sleep(6000);

			logger.write("Click on Conferma button- Start");
			df.clickComponent(df.confermaButton);
			logger.write("Click on Conferma button- Complete");


			logger.write("Verify operazione contents - Start");
			df.verifyComponentExistence(df.confermaOperazioneContents);
			df.comprareText(df.confermaOperazioneContents, DettaglioFornitureComponent.CONFERMAOPERAZIONE_CONTENTS1, true);
			logger.write("Verify the Operazione contents - Complete");

			Thread.sleep(6000);
			
			
			logger.write("Click on fine button- Start");
			df.verifyComponentExistence(df.fineButton);
			df.clickComponent(df.fineButton);
			logger.write("Click on fine button- Complete");
			
			logger.write("Verify the home page header and contents - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			logger.write("Verify the home page header and contents - Complete");
			
				

			prop.setProperty("RETURN_VALUE", "OK");
			
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
	

}
