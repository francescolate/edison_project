package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class RecuperaStatusCase_and_Order_RVC_31 {
	private final static int SEC = 120;
	
	@Step("RecuperaStatusCase_and_Order_Creation")
	public static void main(String[] args) throws Exception {
//		 ITA_IFM_Case__r.Subject
//		 ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c
//		 ITA_IFM_Case__r.CaseNumber
//		 ITA_IFM_Case__c
//		 ITA_IFM_Case__r.Status
//		 ITA_IFM_Case__r.ITA_IFM_SubStatus__c
//		 Name
//		NE__OrderId__r.RecordType.Name
//		NE__Status__c
//		ITA_IFM_IDBPM__c
//		ITA_IFM_POD__c
//		ITA_IFM_R2D_Status__c
//		ITA_IFM_SAP_Status__c
//		ITA_IFM_SEMPRE_Status__c
//		ITA_IFM_UDB_Status__c
//		ITA_IFM_Commodity__c
//		ITA_IFM_Customer_Type_Calc__c
//		ITA_IFM_RecordTypeName__c
//		NE__BillingProfId__r.NE__Payment__c
//		ITA_IFM_Temporary_Billing__c
//		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}
				
				String tipocommodity="";
				
				if(prop.getProperty("COMMODITY").compareTo("ELE")==0){
					tipocommodity="ELETTRICO";
				}
				else if(prop.getProperty("COMMODITY").compareTo("GAS")==0 || prop.getProperty("COMMODITY").compareTo("GAS4")==0){
					tipocommodity="GAS";
				}
				if(prop.getProperty("TIPO_OI_ORDER").compareTo("Commodity")==0){
				a.insertQuery(Costanti.recupera_stati_item_case_order1.replaceAll("@RICHIESTA@", prop.getProperty("NUMERO_RICHIESTA")).replaceAll("@TIPOLOGIA_POD@", tipocommodity).replaceAll("@POD@", prop.getProperty("POD")).replaceAll("@TIPO_OI@", prop.getProperty("TIPO_OI_ORDER")));
				}
//				if(prop.getProperty("TIPO_OI_ORDER").compareTo("VAS")==0){
				else{
					a.insertQuery(Costanti.recupera_stati_item_case_order_vas.replaceAll("@RICHIESTA@", prop.getProperty("NUMERO_RICHIESTA")).replaceAll("@TIPO_OI@", prop.getProperty("TIPO_OI_ORDER")));	
				}
				logger.write("Inserimento Query");
				boolean condition = false;
//				int tentativi = 2;
//				while (!condition && tentativi-- > 0) {
				try{
					a.pressButton(a.submitQuery);
					condition = a.aspettaRisultati(SEC);
					logger.write("Esecuzione Query");
				if (condition) {
					a.recuperaRisultati(Integer.parseInt("1"), prop);
					logger.write("Recupero risultati e salvataggio nei property");
//					a.logoutWorkbench();
				}
				} catch (Exception e) {
					throw new Exception("L'ordine non è stato ancora creato. Verificare");
				}
	
		

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
