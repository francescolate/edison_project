package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Pubblico_ID_61_Processo_A_RES_SWA_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			PubblicoID61ProcessoAResSwaEleComponent prs = new PubblicoID61ProcessoAResSwaEleComponent(driver);

			logger.write("Accessing the home page - Start");
			prs.launchLink(prop.getProperty("HOMEPAGE"));
			Thread.sleep(6000);
			prs.launchLink(prop.getProperty("LINK"));
			Thread.sleep(20000);
			prs.verifyComponentExistence(prs.buttonAccetta);
			prs.clickComponent(prs.buttonAccetta);
			logger.write("Accessing the home page - Complete");

			Thread.sleep(10000);
			
			logger.write("Click on attivaLoffertaButton - Start");
			By attivaLoffertaButton = prs.attivaLoffertaButton;
			prs.verifyComponentExistence(attivaLoffertaButton);
			prs.clickComponent(attivaLoffertaButton);
			logger.write("Click on attivaLoffertaButton - Complete");
			
			Thread.sleep(25000);
					
			prs.checkURLAfterRedirection(prop.getProperty("ADISIONE"));
//			By adesioneContents = prs.adesioneContents;
//			prs.verifyComponentExistence(adesioneContents);
//			prs.comprareText(adesioneContents, prs.ADISIONE_CONTENT, true);
//			
//			driver.switchTo().frame(driver.findElement(By.xpath(prs.switchFrame)));
			
			By inserisci_I_Tuoi_Dati_Heading = prs.inserisci_I_Tuoi_Dati_Heading;
			prs.verifyComponentExistence(inserisci_I_Tuoi_Dati_Heading);
			//prs.comprareText(inserisci_I_Tuoi_Dati_Heading, prs.INPUT_HEADER, true);
			
			prs.verifyComponentExistence(prs.compilaManualmente);
			prs.clickComponent(prs.compilaManualmente);
			
			logger.write("Enter the input for mandatory fields - Start");
			//prs.verifyComponentExistence(prs.labelNome);
			prs.enterInput(prs.inputNome, prop.getProperty("NOME"));
			prs.enterInput(prs.inputCognome, prop.getProperty("COGNOME"));
			prs.enterInput(prs.inputCodiceFiscale, prop.getProperty("CODICE_FISCALE"));
			prs.enterInput(prs.inputTelefonoCellulare, prop.getProperty("TELEFONO"));
			Thread.sleep(5000);
			prs.enterInput(prs.inputEmail, prop.getProperty("EMAIL"));
			prs.enterInput(prs.inputConfirmEmail, prop.getProperty("EMAIL"));
			logger.write("Enter the input for mandatory fields - Complete");
			
			logger.write("Click on privacy checkbox and prosegui button - Start");
			By labelPrivacyCheckbox = prs.labelPrivacyCheckbox;
			prs.verifyComponentExistence(labelPrivacyCheckbox);
			prs.clickComponent(labelPrivacyCheckbox);
			
			prs.comprareText(prs.lablePrivacyContents, prs.PRIVACY_CONTENT, true);
			
			By ProseguiButton = prs.ProseguiButton;
			prs.verifyComponentExistence(ProseguiButton);
			prs.clickComponent(ProseguiButton);
			Thread.sleep(7000);
			
			logger.write("Click on privacy checkbox and prosegui button - Complete");
			
			By informazioni_Fornitura_Heading = prs.informazioni_Fornitura_Heading;
			prs.verifyComponentExistence(informazioni_Fornitura_Heading);
//			prs.verifyText(informazioni_Fornitura_Heading, prop.getProperty("INFORMAZIONI_FORNITURA_HEADING"));
			
			By informazioni_Fornitura_Heading_Subtext = prs.informazioni_Fornitura_Heading_Subtext;
			prs.verifyComponentExistence(informazioni_Fornitura_Heading_Subtext);
			prs.verifyText(informazioni_Fornitura_Heading_Subtext, prop.getProperty("INFORMAZIONI_FORNITURA_HEADING_SUBTEXT"));
			
			By IF_Radio1 = prs.IF_Radio1;
			prs.verifyComponentExistence(IF_Radio1);
			prs.verifyText(IF_Radio1, prop.getProperty("IF_RADIO_BUTTON_1"));
			
			By IF_Radio1_description = prs.IF_Radio1_description;
			prs.verifyComponentExistence(IF_Radio1_description);
			prs.verifyText(IF_Radio1_description, prop.getProperty("IF_RADIO_BUTTON_1_DESCRIPTION"));
			
			By IF_Radio2 = prs.IF_Radio2;
			prs.verifyComponentExistence(IF_Radio2);
			prs.verifyText(IF_Radio2, prop.getProperty("IF_RADIO_BUTTON_2"));
			
			By IF_Radio2_description = prs.IF_Radio2_description;
			prs.verifyComponentExistence(IF_Radio2_description);
			prs.verifyText(IF_Radio2_description, prop.getProperty("IF_RADIO_BUTTON_2_DESCRIPTION"));
			
			By IF_Radio3 = prs.IF_Radio3;
			prs.verifyComponentExistence(IF_Radio3);
			prs.verifyText(IF_Radio3, prop.getProperty("IF_RADIO_BUTTON_3"));
			
			By IF_Radio3_description = prs.IF_Radio3_description;
			prs.verifyComponentExistence(IF_Radio3_description);
			prs.verifyText(IF_Radio3_description, prop.getProperty("IF_RADIO_BUTTON_3_DESCRIPTION"));
			
			By IF_Radio4 = prs.IF_Radio4;
			prs.verifyComponentExistence(IF_Radio4);
			prs.verifyText(IF_Radio4, prop.getProperty("IF_RADIO_BUTTON_4"));
			
			By IF_Radio4_description = prs.IF_Radio4_description;
			prs.verifyComponentExistence(IF_Radio4_description);
			prs.verifyText(IF_Radio4_description, prop.getProperty("IF_RADIO_BUTTON_4_DESCRIPTION"));
			
			By POD_Heading = prs.POD;
			prs.verifyComponentExistence(POD_Heading);
			prs.verifyText(POD_Heading, prop.getProperty("POD_HEADING"));
			
			By CAP_Heading = prs.CAP;
			prs.verifyComponentExistence(CAP_Heading);
			prs.verifyText(CAP_Heading, prop.getProperty("CAP_HEADING"));
			
			By step2_Disclaimer = prs.step2_Disclaimer;
			prs.verifyComponentExistence(step2_Disclaimer);
			prs.verifyText(step2_Disclaimer, prs.step2DiscliamerText);
				
			prs.clickComponent(prs.IF_Radio1);
			
			prs.enterInput(prs.POD_Input, "IT004E"+PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			prs.enterInput(prs.CAP_Input, prop.getProperty("CAP"));
			Thread.sleep(2000);
			prs.clickComponent(prs.CAP_Select);
			
			prs.verifyComponentExistence(ProseguiButton);
			prs.clickComponent(ProseguiButton);
			
			Thread.sleep(15000);
			
			By iTuoi_Dati_Heading = prs.iTuoi_Dati_Heading;
			prs.verifyComponentExistence(iTuoi_Dati_Heading);
			prs.verifyText(iTuoi_Dati_Heading, prop.getProperty("ITUOI_DATI_HEADING"));
			
			prs.verifyComponentExistence(prs.Citta_Input);
			prs.verifyComponentExistence(prs.Indirizzo_Input);
			prs.verifyComponentExistence(prs.Numero_Civico_Input);
			prs.verifyComponentExistence(prs.CAP_Input);
			prs.verifyComponentExistence(prs.Attuale_Fornitore_Input);
			
			prs.enterInput(prs.Citta_Input, prop.getProperty("CITTA"));
			prs.enterInput(prs.Indirizzo_Input, prop.getProperty("INDIRIZZO"));
			prs.enterInput(prs.Numero_Civico_Input, prop.getProperty("NUMERICO_CIVICO"));
//			prs.checkPrePopulatedValueAndCompare(prs.CapXpath, prs.CapExpectedValue);
			prs.clickTab(prs.Numero_Civico_Input);
			Thread.sleep(3000);
			prs.verifyComponentExistence(prs.insertManually);
			prs.clickComponent(prs.insertManually);
			Thread.sleep(2000);
			prs.enterInput(prs.CAP_Input, prop.getProperty("CAP"));
			Thread.sleep(2000);
			prs.clickComponent(prs.CAP_Select1);
			
			prs.verifyComponentExistence(prs.confirmIndrizzo);
			prs.clickComponent(prs.confirmIndrizzo);
			
			By fieldInfoText = prs.fieldInfoText;
			prs.verifyComponentExistence(fieldInfoText);
			prs.verifyText(fieldInfoText, prs.fieldInfoMessage);
			
			prs.enterInput(prs.Attuale_Fornitore_Input, prop.getProperty("ATTUALE_FORNITORE"));
			Thread.sleep(2000);
			prs.clickComponent(prs.Attuale_Fornitore_InputSelect);
			
			By recapiti_Heading = prs.recapiti_Heading;
			prs.verifyComponentExistence(recapiti_Heading);
			prs.verifyText(recapiti_Heading, prop.getProperty("RECAPITI_HEADING"));
			
			By recapiti_question1 = prs.recapiti_question1;
			prs.verifyComponentExistence(recapiti_question1);
			prs.verifyText(recapiti_question1, prop.getProperty("RECAPITI_QUESTION_1"));
			
			prs.verifyComponentExistence(prs.recapiti_question1_Radio_SI);
			prs.verifyComponentExistence(prs.recapiti_question1_Radio_NO);
			
			By recapiti_question1_Info_Text = prs.recapiti_question1_Info_Text;
			prs.verifyComponentExistence(recapiti_question1_Info_Text);
			prs.verifyText(recapiti_question1_Info_Text, prs.RECAPITI_INFO_CONTENT);
			
			By recapiti_question2 = prs.recapiti_question2;
			prs.verifyComponentExistence(recapiti_question2);
			prs.verifyText(recapiti_question2, prop.getProperty("RECAPITI_QUESTION_2"));
			
			prs.verifyComponentExistence(prs.recapiti_question2_Radio_yesBillingAddress);
			prs.verifyComponentExistence(prs.recapiti_question2_Radio_noBillingAddress);
			
//			prs.verifyElementSelected(prs.recapiti_question2_Radio_yesBillingAddress);
			prs.clickComponent(prs.recapiti_question1_Radio_SI);
			prs.clickComponent(prs.recapiti_question2_Radio_yesBillingAddress);
			
			prs.verifyComponentExistence(ProseguiButton);
			prs.clickComponent(ProseguiButton);
			Thread.sleep(9000);
			
			By pagamenti_E_Bollette_Heading = prs.pagamenti_E_Bollette_Heading;
			prs.verifyComponentExistence(pagamenti_E_Bollette_Heading);
			prs.verifyText(pagamenti_E_Bollette_Heading, prop.getProperty("PAGAMENTI_HEADING"));
			
			By Metodo_di_Pagamento_Field = prs.Metodo_di_Pagamento_Field;
			prs.verifyComponentExistence(Metodo_di_Pagamento_Field);
			prs.verifyText(Metodo_di_Pagamento_Field, prop.getProperty("METODO_DI_PAGAMENTO"));
			
//			prs.verifyDefaultDropDownValue(prs.Metodo_di_Pagamento_FieldSelectValue, prs.METADO_DROPDOWN_VALUE);
			prs.verifyDefaultValue(prs.Metodo_di_Pagamento_FieldDefaultValue, prs.METADO_DROPDOWN_VALUE);
			
			By Codice_IBAN_Field = prs.Codice_IBAN_Field;
			prs.verifyComponentExistence(Codice_IBAN_Field);
			prs.verifyText(Codice_IBAN_Field, prop.getProperty("CODICE_IBAN"));
			
			prs.verifyComponentExistence(prs.Codice_IBAN_Radio_yesAccountHolder);
			prs.verifyComponentExistence(prs.Codice_IBAN_Radio_noAccountHolder);
			
			prs.clickComponent(prs.Codice_IBAN_Radio_yesAccountHolder);
			
			By Modalità_di_ricezione_Heading = prs.Modalità_di_ricezione_Heading;
			prs.verifyComponentExistence(Modalità_di_ricezione_Heading);
			prs.verifyText(Modalità_di_ricezione_Heading, prop.getProperty("MODALITA_DI_RICEZIONE"));
			
			prs.clickComponent(prs.Modalità_di_ricezione_Radio1);
			
			prs.checkPrePopulatedValueAndCompare(prs.Modalità_di_ricezione_EmailInput, prop.getProperty("EMAIL"));
			prs.checkPrePopulatedValueAndCompare(prs.Modalità_di_ricezione_TelefonoInput, prop.getProperty("TELEFONO"));
			
			By Codici_Promozionali_Heading = prs.Codici_Promozionali_Heading;
			prs.verifyComponentExistence(Codici_Promozionali_Heading);
			prs.verifyText(Codici_Promozionali_Heading, prop.getProperty("CODICI_PROMOZIONALI_HEADING"));
			
			By Codici_Promozionali_Subtext = prs.Codici_Promozionali_Subtext;
			prs.verifyComponentExistence(Codici_Promozionali_Subtext);
			prs.verifyText(Codici_Promozionali_Subtext, prop.getProperty("CODICI_PROMOZIONALI_SUBTEXT"));
			
			By Codici_Promozionali_DiscountCode = prs.Codici_Promozionali_DiscountCode;
			prs.verifyComponentExistence(Codici_Promozionali_DiscountCode);
			
//			prs.selectDropDownValue(prs.Metodo_di_Pagamento_FieldSelectValue, prs.METADO_DROPDOWN_SELECTVALUE);
			prs.selectMetodoPagamentoValue();
			
/*			prs.verifyComponentNotExistence(prs.Codice_IBAN_Field);
			prs.verifyComponentNotExistence(prs.Codice_IBAN_Radio_yesAccountHolder);
			prs.verifyComponentNotExistence(prs.Codice_IBAN_Radio_noAccountHolder);*/
			
			prs.verifyComponentExistence(ProseguiButton);
			prs.clickComponent(ProseguiButton);
			Thread.sleep(9000);
			
			By Mandati_e_Consensi_Heading = prs.Mandati_e_Consensi_Heading;
			prs.verifyComponentExistence(Mandati_e_Consensi_Heading);
			prs.verifyText(Mandati_e_Consensi_Heading, prop.getProperty("MANDATI_E_CONSENSI_HEADING"));
			
			By Mandati_e_Consensi_TextInBox = prs.Mandati_e_Consensi_TextInBox;
			prs.verifyComponentExistence(Mandati_e_Consensi_TextInBox);
			prs.verifyText(Mandati_e_Consensi_TextInBox, prs.Mandati_e_Consensi_TextBoxContent);
			
			By Mandati_e_Consensi_chkbx1_Text1 = prs.Mandati_e_Consensi_chkbx1_Text1;
			prs.verifyComponentExistence(Mandati_e_Consensi_chkbx1_Text1);
			prs.verifyText(Mandati_e_Consensi_chkbx1_Text1,prs.Mandati_e_Consensi_chkbx1Text);
			
			By Mandati_e_Consensi_chkbx2 = prs.Mandati_e_Consensi_chkbx2;
			prs.verifyComponentExistence(Mandati_e_Consensi_chkbx2);
			By Mandati_e_Consensi_chkbx2_Text1 = prs.Mandati_e_Consensi_chkbx2_Text1;
			prs.verifyComponentExistence(Mandati_e_Consensi_chkbx2_Text1);
			prs.verifyText(Mandati_e_Consensi_chkbx2_Text1,prs.Mandati_e_Consensi_chkbx2Text1);
			By Mandati_e_Consensi_chkbx2_Text2 = prs.Mandati_e_Consensi_chkbx2_Text2;
			prs.verifyComponentExistence(Mandati_e_Consensi_chkbx2_Text2);
			prs.verifyText(Mandati_e_Consensi_chkbx2_Text2,prs.Mandati_e_Consensi_chkbx2Text2);
			
			By Mandati_e_Consensi_chkbx3_Text = prs.Mandati_e_Consensi_chkbx3_Text;
			prs.verifyComponentExistence(Mandati_e_Consensi_chkbx3_Text);
			prs.verifyText(Mandati_e_Consensi_chkbx3_Text,prs.Mandati_e_Consensi_chkbx3Text);
			
			prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Heading);
			prs.verifyText(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Heading, prs.RichestaHeading);
			
			//prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_TextInBox);
			//prs.verifyText(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_TextInBox, prs.RichestaText);
			
			prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_LinkText);
			prs.verifyText(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_LinkText, prs.RichestaLink);
			
			prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_SI);
			
			prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO);
			
			prs.clickComponent(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_SI);
			Thread.sleep(5000);
			
			driver.switchTo().parentFrame();
			
			prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_content);
			prs.verifyText(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_content, prs.RichestaPopUpContent);
			
			prs.verifyComponentExistence(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_close);
			prs.clickComponent(prs.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_close);
			Thread.sleep(5000);
			
			driver.switchTo().frame(driver.findElement(By.xpath(prs.switchFrame)));
			
			By Consenso_Marketing_Enel_Energia_Heading = prs.Consenso_Marketing_Enel_Energia_Heading;
			prs.verifyComponentExistence(Consenso_Marketing_Enel_Energia_Heading);
			prs.verifyText(Consenso_Marketing_Enel_Energia_Heading, prop.getProperty("CONSENSI_MARKETING_ENEL_ENERGIA_HEADING"));
			
			By Consenso_Marketing_Enel_Energia_TextInBox = prs.Consenso_Marketing_Enel_Energia_TextInBox;
			prs.verifyComponentExistence(Consenso_Marketing_Enel_Energia_TextInBox);
			prs.verifyText(Consenso_Marketing_Enel_Energia_TextInBox, prs.Consenso_Marketing_Enel_Energia_TextBoxContent);
			
			By Consenso_Marketing_Enel_Energia_Accetto = prs.Consenso_Marketing_Enel_Energia_Accetto;
			prs.verifyComponentExistence(Consenso_Marketing_Enel_Energia_Accetto);
			prs.verifyText(Consenso_Marketing_Enel_Energia_Accetto,prop.getProperty("ACCETTO"));
			
			By Consenso_Marketing_Enel_Energia_NonAccetto = prs.Consenso_Marketing_Enel_Energia_NonAccetto;
			prs.verifyComponentExistence(Consenso_Marketing_Enel_Energia_NonAccetto);
			prs.verifyText(Consenso_Marketing_Enel_Energia_NonAccetto,prop.getProperty("NON_ACCETTO"));
			
			By Consenso_Marketing_Terzi_Heading = prs.Consenso_Marketing_Terzi_Heading;
			prs.verifyComponentExistence(Consenso_Marketing_Terzi_Heading);
			prs.verifyText(Consenso_Marketing_Terzi_Heading, prop.getProperty("CONSENSI_MARKETING_TERZI_HEADING"));
			
			By Consenso_Marketing_Terzi_TextInBox = prs.Consenso_Marketing_Terzi_TextInBox;
			prs.verifyComponentExistence(Consenso_Marketing_Terzi_TextInBox);
			prs.verifyText(Consenso_Marketing_Terzi_TextInBox, prs.Consenso_Marketing_Terzi_TextBoxContent);
			
			By Consenso_Marketing_Terzi_Accetto = prs.Consenso_Marketing_Terzi_Accetto;
			prs.verifyComponentExistence(Consenso_Marketing_Terzi_Accetto);
			prs.verifyText(Consenso_Marketing_Terzi_Accetto,prop.getProperty("ACCETTO"));
			
			By Consenso_Marketing_Terzi_NonAccetto = prs.Consenso_Marketing_Terzi_NonAccetto;
			prs.verifyComponentExistence(Consenso_Marketing_Terzi_NonAccetto);
			prs.verifyText(Consenso_Marketing_Terzi_NonAccetto,prop.getProperty("NON_ACCETTO"));
			
			By Consenso_Profilazione_Enel_Energia_Heading = prs.Consenso_Profilazione_Enel_Energia_Heading;
			prs.verifyComponentExistence(Consenso_Profilazione_Enel_Energia_Heading);
			prs.verifyText(Consenso_Profilazione_Enel_Energia_Heading, prop.getProperty("CONSENSI_PROFILAZIONE_ENEL_ENERGIA_HEADING"));
			
			By Consenso_Profilazione_Enel_Energia_TextInBox = prs.Consenso_Profilazione_Enel_Energia_TextInBox;
			prs.verifyComponentExistence(Consenso_Profilazione_Enel_Energia_TextInBox);
			prs.verifyText(Consenso_Profilazione_Enel_Energia_TextInBox, prs.Consenso_Profilazione_Enel_Energia_TextBoxContent);
			
			By Consenso_Profilazione_Enel_Energia_Accetto = prs.Consenso_Profilazione_Enel_Energia_Accetto;
			prs.verifyComponentExistence(Consenso_Profilazione_Enel_Energia_Accetto);
			prs.verifyText(Consenso_Profilazione_Enel_Energia_Accetto,prop.getProperty("ACCETTO"));
			
			By Consenso_Profilazione_Enel_Energia_NonAccetto = prs.Consenso_Profilazione_Enel_Energia_NonAccetto;
			prs.verifyComponentExistence(Consenso_Profilazione_Enel_Energia_NonAccetto);
			prs.verifyText(Consenso_Profilazione_Enel_Energia_NonAccetto,prop.getProperty("NON_ACCETTO"));
						
			prs.clickComponent(prs.Mandati_e_Consensi_chkbx1);
			prs.clickComponent(Mandati_e_Consensi_chkbx2);
			prs.clickComponent(Mandati_e_Consensi_chkbx3_Text);
			prs.clickComponent(Consenso_Marketing_Enel_Energia_Accetto);
			prs.clickComponent(Consenso_Marketing_Terzi_Accetto);
			prs.clickComponent(Consenso_Profilazione_Enel_Energia_Accetto);
			
			By COMPLETA_ADESIONE_Button = prs.COMPLETA_ADESIONE_Button;
			prs.verifyComponentExistence(COMPLETA_ADESIONE_Button);
			prs.clickComponent(COMPLETA_ADESIONE_Button);
			Thread.sleep(7000);
			
			driver.switchTo().defaultContent();
					
			By TornaAllaHomePageTitle = prs.TornaAllaHomePageTitle;
			prs.verifyComponentExistence(TornaAllaHomePageTitle);
			prs.verifyText(TornaAllaHomePageTitle,prop.getProperty("TORNA_ALLA_HOMEPAGE_TITLE"));
			
			prs.provideEmail(prop.getProperty("EMAIL"));
			
			By TornaAllaHomeMSG1 = prs.TornaAllaHomeMSG1;
			prs.verifyComponentExistence(TornaAllaHomeMSG1);
			prs.verifyText(TornaAllaHomeMSG1,prs.torna_alla_homepage_message1);
			
			By TornaAllaHomeMSG2 = prs.TornaAllaHomeMSG2;
			prs.verifyComponentExistence(TornaAllaHomeMSG2);
			prs.verifyText(TornaAllaHomeMSG2,prop.getProperty("TORNA_ALLA_HOMEPAGE_MESSAGE2"));
			
			By TornaAllaHomeButton = prs.TornaAllaHomeButton;
			prs.verifyComponentExistence(TornaAllaHomeButton);
			prs.clickComponent(TornaAllaHomeButton);
			
			Thread.sleep(8000);
			
			prs.checkURLAfterRedirection(prop.getProperty("HOMEPAGE"));
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

}
	
}
