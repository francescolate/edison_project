package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_43_RecuperaUsername {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			String privateAreaURL = prop.getProperty("LINK")+"it/recupera-username";
			String privateAreaLoginURL = prop.getProperty("LINK")+"it/login";
			logger.write("apertura del portale web Enel di test - Start");
			//Login Page
			LoginPageComponent log = new LoginPageComponent(driver);
			LoginLogoutEnelCollaComponent lognew = new LoginLogoutEnelCollaComponent(driver);
			APIService api = new APIService();
			
			lognew.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			lognew.hanldeFullscreenMessage(lognew.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			By accettaCookie = lognew.buttonAccetta;
			lognew.verifyComponentExistence(accettaCookie);
			lognew.clickComponent(accettaCookie);
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			Thread.sleep(5000);
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("Check login page - completed");
			
			logger.write("Check the user name and password fields - start");
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			logger.write("Check the user name and password fields - completed");
			
			logger.write("Check the Login button - start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			logger.write("Check the Login button - completed");

			By recName = log.RecUserName;
	        log.verifyComponentExistence(recName);
	        By RecPasswd=log.RecUserPasswd;
	        log.verifyComponentExistence(RecPasswd);
	        
	        log.verifyComponentExistence(log.haiProblemiText);
	        log.comprareText(log.haiProblemiText, LoginPageComponent.HAI_PROBLEMI_TEXT, true);
        
	        logger.write("Check the google and facebook  - start");
	        By signInFacebook=log.FacebookSignIn;
	        log.verifyComponentExistence(signInFacebook);
	        By signInGoogle=log.GoogleSignIn;
	        log.verifyComponentExistence(signInGoogle);
	        logger.write("Check the google and facebook - completed");
	        
	        log.verifyComponentExistence(log.nonHaiAccount);
	        log.comprareText(log.nonHaiAccount, LoginPageComponent.NON_HAI_ACCOUNT, true);
	        
	        logger.write("Check the registrati button  - start");
	        log.verifyComponentExistence(log.registratiButton);
	        logger.write("Check the registrati button  - ends");
	        
	        
	        logger.write("Verification of recover Username Section- Starts");       
	        log.clickComponent(recName);
	      	log.checkURLAfterRedirection(privateAreaURL);
	      	
	      	log.comprareText(log.recupraUsernameText, LoginPageComponent.RECUPRA_USERNAME_TEXT, true);
	      	
	      	log.verifyComponentExistence(log.codiceFiscaleLable);
	      	log.comprareText(log.codiceFiscaleLable, LoginPageComponent.CODICE_FISCALE_LABEL, true);
	      	log.verifyComponentExistence(log.codiceFiscaleField);
	      	log.verifyComponentExistence(log.continuaButton);
	      	
	      	log.clickComponent(log.continuaButton);
	      	log.verifyComponentExistence(log.campoObbligatoriaErrorText);
	      	log.comprareText(log.campoObbligatoriaErrorText, LoginPageComponent.CAMPO_OBBLIGATORIA, true);
	      	
	      	log.enterLoginParameters(log.codiceFiscaleField, prop.getProperty("FISCALE"));
	      	Thread.sleep(5000);
	      	//log.clickComponent(log.continuaButton);
	      	log.clickWithJS(log.continuaButton);
	      	Thread.sleep(5000);
	      	log.comprareText(log.recuperaUsernameHeading, LoginPageComponent.RECUPERA_USERNAME_HEADING, true);
	      	//log.comprareText(log.recuperaUsernameParagraph1, LoginPageComponent.RECUPERA_USERNAME_PARAGRAPH1, true);
	      	log.comprareText(log.recuperaUsernameParagraph2, LoginPageComponent.RECUPERA_USERNAME_PARAGRAPH2, true);
	      	log.comprareText(log.recuperaUsernameParagraph3, LoginPageComponent.RECUPERA_USERNAME_PARAGRAPH3, true);
	      	log.comprareText(log.recuperaEmail, LoginPageComponent.RECUPERA_EMAIL, true);
	      	log.comprareText(log.recuperaCellulare, LoginPageComponent.RECUPERA_CELLULARE, true);
	      		      	
	      	log.verifyComponentExistence(log.accediButton);
	      	//log.verifyComponentExistence(log.modificaUserNameButton);
	      	logger.write("Verification of recover Username Section- Ends");
	      	
	      	log.clickComponent(log.accediButton);
	      	
	      	logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			log.verifyComponentExistence(pageLogin);
			log.checkURLAfterRedirection(privateAreaLoginURL);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - End");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
