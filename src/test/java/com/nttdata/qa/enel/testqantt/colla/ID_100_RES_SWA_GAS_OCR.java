package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;

import com.nttdata.qa.enel.components.colla.OffertagasComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_100_RES_SWA_GAS_OCR {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		

		try {
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			prop.setProperty("LUCEEGASLINK", "https://www-coll2.enel.it/it/modulo-reclami-enel-energia-luce-gas.html");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			OffertagasComponent oc =new OffertagasComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			PublicAreaComponent pa = new PublicAreaComponent(driver);
			Thread.sleep(5000);
			logger.write("select drop down value - Start");
			pa.changeDropDownValue(pa.productDropDown, pa.gas);
			pa.changeDropDownValue(pa.placeDropDown, pa.casa);
			pa.changeDropDownValue(pa.myselectionDD, pa.cambio);
					logger.write("Click on inizia ora - Start");
			pa.clickComponent(pa.iniziaOra);
			Thread.sleep(3000);
			logger.write("Click on inizia ora - Completed");
			pa.checkURLAfterRedirection(pa.detaglioOffertgasaUrl);
			pa.clickComponent(pa.dettaglioOfferta);
			
			oc.clickComponent(oc.attivaSubito);
			logger.write("Verify page navigation to attiva Offerta page and page details -- start");
			driver.navigate().to(oc.offertaUrlgas);
						
			oc.verifyComponentExistence(oc.aderisci);
			oc.checkURLAfterRedirection(oc.offertaUrlgas);
						
			oc.verifyComponentExistence(oc.compilaManualmente);
			oc.clickComponent(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Start");
							
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(2000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.jsClickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(25000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");
			
			
			logger.write("Verify and Enter the values in informazioniFornitura section-- Strat");
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);
						oc.enterInputParameters(oc.pdr, prop.getProperty("PDR"));
			oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
			oc.clickComponent(oc.capdropdown);
			
			oc.verifyComponentExistence(oc.prosegui1);
			oc.clickComponent(oc.prosegui1);
			oc.comprareText(oc.indrizzoDiFornituraText, oc.IndrizzoDiFornituraText, true);
			oc.verifyComponentExistence(oc.citta);
			oc.verifyComponentExistence(oc.indrizzo);
			oc.verifyComponentExistence(oc.numeroCivico);
			oc.verifyComponentExistence(oc.indrizzoDiFornituraCAP);
			oc.comprareText(oc.indrizzoDiFornituraPrivacyText, oc.IndrizzoDiFornituraPrivacyText, true);
			oc.verifyComponentExistence(oc.attuleFornitore);
			oc.clickComponent(oc.AttualeFornitoreselect);
			
			
	oc.clickComponent(oc.prosegui1);
			Thread.sleep(5000);
			logger.write("Verify and Enter the values in informazioniFornitura section-- Completed");

		
			oc.verifyComponentExistence(oc.Metodo_di_Pagamento_Field);
			oc.verifyDefaultValue(oc.Metodo_di_Pagamento_Bolletino, oc.Bolletino_selection);
			
			
			oc.verifyComponentExistence(oc.Modalità_di_ricezione_Heading);
						oc.checkPrePopulatedValueAndCompare(oc.ModalitaricezioneEmailbollete, prop.getProperty("EMAIL"));
			oc.checkPrePopulatedValueAndCompare(oc.ModalitaricezioneTelefonobollete, prop.getProperty("CELLULARE"));
			
			oc.verifyComponentExistence(oc.Codici_Promozionali_Heading);
			oc.verifyComponentExistence(oc.Codici_Promozionali_Subtext);
			oc.enterInputParameters(oc.Codici_Promozionalitest, prop.getProperty("Codici_Promozionali"));
			oc.clickComponent(oc.prosegui1);
			
									
			Thread.sleep(5000);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Completed");
//3 checkboxes display 
			oc.verifyComponentExistence(oc.Consensi_Heading);
		
			oc.comprareText(oc.Consensi_chkbx1_Text, oc.Consensi_chkbx1Text, true);
			oc.comprareText(oc.Consensi_chkbx2_Text, oc.chkbx2Text, true);
			oc.comprareText(oc.Mandati_e_Consensi_chkbx3_Text, oc.Consensi_chkbx3Text, true);
			
			oc.clickComponent(oc.Consensi_chkbx1_Text);
			oc.clickComponent(oc.Consensi_chkbx2_Text);
			oc.clickComponent(oc.Mandati_e_Consensi_chkbx3_Text);
			
			//
			oc.verifyComponentExistence(oc.Richiesta_Heading);
			oc.verifyComponentExistence(oc.Richiesta_LinkText);
			oc.comprareText(oc.Richiesta_TextInBox, oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text, true);			
			oc.verifyComponentExistence(oc.Richiesta_SI);
			oc.verifyComponentExistence(oc.Richiesta_Di_NO);
			oc.clickComponent(oc.Richiesta_LinkText);
		
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_POPUP_close);
			
			oc.clickComponent(oc.Maggiori_Informazioni);
			oc.comprareText(oc.Maggiori_Informazioni_Text, oc.MAGGIORI_INFORMAZIONI_TEXT, true);
			oc.comprareText(oc.Maggiori_Informazioni_Text1, oc.MAGGIORI_INFORMAZIONI_TEXT1, true);
			oc.comprareText(oc.Maggiori_Informazioni_Text2, oc.MAGGIORI_INFORMAZIONI_TEXT2, true);
			oc.comprareText(oc.Maggiori_Informazioni_Text3, oc.MAGGIORI_INFORMAZIONI_TEXT3, true);
			
			oc.verifyComponentExistence(oc.Consenso_Marketing_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Marketing_Enel_Energia_TextInBox, oc.Consenso_Marketing_Enel_Energia_Text, true);
			oc.verifyComponentExistence(oc.Enel_Energia_Accetto);
			oc.verifyComponentExistence(oc.Enel_Energia_NonAccetto);
			
			oc.verifyComponentExistence(oc.Consenso_Marketing_Terzi_Heading);
			oc.comprareText(oc.Consenso_Marketing_Terzi_TextInBox, oc.Consenso_Marketing_Terzi_Text, true);
			oc.verifyComponentExistence(oc.Terzi_Accetto);
			oc.verifyComponentExistence(oc.Terzi_NonAccetto);
			
			oc.verifyComponentExistence(oc.Consenso_Profilazione_Enel_Energia_Heading);
			oc.comprareText(oc.Consenso_Profilazione_Enel_Energia_TextInBox, oc.Consenso_Profilazione_Enel_Energia_Text, true);
						oc.verifyComponentExistence(oc.Terzi_NonAccetto);
			
			
			oc.clickComponent(oc.Enel_Energia_Accetto);
			oc.clickComponent(oc.Terzi_Accetto);
					oc.clickComponent(oc.Richiesta_Di_NO);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupContent, oc.Richiesta_Di_Esecuzione_POPUP_Content, true);
			oc.clickComponent(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupClose);
			oc.verifyComponentExistence(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text1);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text1, oc.Richiesta_Di_Esecuzione_Anticipata_Del_CONTRATO_TEXT1, true);
			oc.comprareText(oc.Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text2, oc.Richiesta_Di_Esecuzione_Anticipata_Del_CONTRATO_TEXT2, true);
		
			oc.clickComponent(oc.COMPLETA_ADESIONE_Button);
			logger.write("Verify and Enter the values in pagamentiEBollette section-- Start");

			Thread.sleep(10000);
			oc.comprareText(oc.tornaAllaHomePageTitle, oc.TornaAllaHomePageTitle, true);
			oc.provideEmail(prop.getProperty("EMAIL"));

		
			oc.clickComponent(oc.TornaAllaHomeButton);
			Thread.sleep(5000);
						oc.checkURLAfterRedirection(prop.getProperty("WP_LINK"));
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
