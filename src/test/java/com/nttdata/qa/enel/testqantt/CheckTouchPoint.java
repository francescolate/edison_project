package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.VerificaDocumentoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;



public class CheckTouchPoint {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			VerificaRichiestaComponent contratto = new VerificaRichiestaComponent(driver);
			SeleniumUtilities selenium=new SeleniumUtilities(driver);
			logger.write("verifica che nella richiesta alla voce Touch Point sono presenti i record di TP1- TP2 e TP4 in stato Creato - Start ");
			contratto.pressJavascript(contratto.richiesta);
			TimeUnit.SECONDS.sleep(5);
			VerificaDocumentoComponent doc=new VerificaDocumentoComponent(driver);
			driver.navigate().refresh();
			TimeUnit.SECONDS.sleep(10);
			doc.clickComponent(doc.tabCorrelato);
			driver.navigate().refresh();
			TimeUnit.SECONDS.sleep(20);
			selenium.scrollDown();
			TimeUnit.SECONDS.sleep(5);
			doc.scrollComponent(doc.sezioneTouchPoint);
			
			TimeUnit.SECONDS.sleep(5);
			doc.clickWithJS(doc.linkVisualizzaTuttoTouchPoint);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			gestione.checkSpinnersSFDC();
			
			if (prop.getProperty("TIPO_OPERAZIONE","").contentEquals("PRIMA_ATTIVAZIONE_EVO")) {
				doc.verificaValoreInTabellaTouchPoint("TP1",prop.getProperty("STATO_TP"));
				doc.verificaValoreInTabellaTouchPoint("TP2",prop.getProperty("STATO_TP"));
				doc.verificaValoreInTabellaTouchPoint("TP4",prop.getProperty("STATO_TP"));
			}
			
			doc.clickComponent(doc.buttonChiudiTouchPoint);
			logger.write("verifica che nella richiesta alla voce Touch Point sono presenti i record di TP1- TP2 e TP4 in stato Creato - Completed ");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
