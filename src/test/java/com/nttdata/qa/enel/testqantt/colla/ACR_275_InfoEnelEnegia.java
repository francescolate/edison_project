package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_275_InfoEnelEnegia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleResOld);
			Thread.sleep(30000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleResOld, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			logger.write("Click on Service from residential menu -- Strat");
			hm.clickComponent(hm.services);
			
			logger.write("Verify services title and supply details,click on info enel energia -- Strat");
			ServicesComponent sc= new ServicesComponent(driver);
			sc.verifyComponentExistence(sc.serviceTitle);
			sc.checkForSupplyServices(sc.SupplyServices);
			sc.clickComponent(sc.infoEnelEnergia);
			logger.write("Verify services title and supply details,click on info enel energia -- Completed");

			InfoEnelEnergiaMyComponent ic = new InfoEnelEnergiaMyComponent(driver);
			logger.write("Verify Info enel enegia title and click on modifica info enel energia -- Strat");
			//ic.verifyComponentExistence(ic.headingInfoEnergia);
			ic.comprareText(ic.titleInfoEnergia, ic.TITLE_INFOENERGIA, true);
			ic.comprareText(ic.headingInfoEnergia, ic.HEADING_INFOENERGIA, true);
			ic.comprareText(ic.subTitle2InfoEnergia, ic.SUBTITLE2INFOENERGIA, true);
			ic.clickComponent(ic.modificabutton);
			Thread.sleep(50000);
			logger.write("Verify Info enel enegia title and click on modifica info enel energia -- Completed");

			ModificaInfoEnelEnergiaComponent mc = new ModificaInfoEnelEnergiaComponent(driver);
			Thread.sleep(50000);
			logger.write("Verify Modifica Info enel enegia title and button details, click on Continue button -- Strat");
			mc.verifyComponentExistence(mc.pageTitle);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.titleSubText, mc.TitleSubText, true);
			mc.clickComponent(mc.radioButton);
			mc.verifyComponentExistence(mc.esciButton);
			mc.verifyComponentExistence(mc.continuaButton);
			String supply = driver.findElement(mc.supplyNew).getText();
			System.out.println("Supply "+supply);
			prop.setProperty("SUPPLY", supply);
			mc.clickComponent(mc.continuaButton);
			logger.write("Verify Modifica Info enel enegia title and button details, click on Continue button -- Completed");

			logger.write("Verify Modifica Info enel enegia title and page details -- Strat");
			mc.verifyComponentExistence(mc.pageTitle);
			mc.comprareText(mc.titleSubText1, mc.TitleSubText1, true);
			mc.verifyComponentExistence(mc.inserimentoDati);
			mc.verifyComponentExistence(mc.stepOneDescription);
			mc.verifyComponentExistence(mc.comunicaLettura);
			mc.verifyComponentExistence(mc.comunicaLetturaSMS);
			mc.verifyComponentExistence(mc.comunicaLetturaEmail);
			mc.clickComponent(mc.comunicaLetturaEmail);
			
			mc.verifyComponentExistence(mc.emissioneBolletta);
			mc.verifyComponentExistence(mc.emissioneBollettaSMS);
			mc.verifyComponentExistence(mc.emissioneBollettaEmail);
			
			mc.verifyComponentExistence(mc.avvenutoPagamento);
			mc.verifyComponentExistence(mc.avvenutoPagamentoSMS);
			mc.verifyComponentExistence(mc.avvenutoPagamentoEmail);
			
			mc.verifyComponentExistence(mc.scadenzaProssimaBolletta);
			mc.verifyComponentExistence(mc.scadenzaProssimaBollettaSMS);
			mc.verifyComponentExistence(mc.scadenzaProssimaBollettaEmail);
			
			mc.verifyComponentExistence(mc.sollecitoPagamento);
			mc.verifyComponentExistence(mc.sollecitoPagamentoSMS);
			mc.verifyComponentExistence(mc.sollecitoPagamentoEmail);
			
			mc.verifyComponentExistence(mc.avvisoConsumi);
			mc.verifyComponentExistence(mc.avvisoConsumiSMS);
			mc.verifyComponentExistence(mc.avvisoConsumiEmail);
			
			mc.verifyComponentExistence(mc.inserisciITuoiDati);
			mc.verifyComponentExistence(mc.emailIp);
			mc.checkPrePopulatedValue(mc.emailInput);
			mc.verifyComponentExistence(mc.cellulare);
			mc.modifyExistingPhone(mc.cellulare);
			
			mc.verifyComponentExistence(mc.confirmCellulare);
			mc.clickComponent(mc.checkBox);
			String email = "fabiana.monza@nttdata.com";
			mc.enterInput(mc.emailIp, email);
			
			//mc.enterInput(mc.emailIp, email);
			mc.verifyComponentExistence(mc.confirmEmail);
			mc.enterInput(mc.confirmEmail, email);
			
			//mc.verifyComponentExistence(mc.confirmEmail);
			mc.ClearInputFieldValue(mc.cellulare);
			mc.clickComponent(mc.continueButton);
			mc.comprareText(mc.cellularError, mc.EmptyFieldError, true);
		
			String cellulare ="121212121212";
			mc.enterInput(mc.cellulare, cellulare);
			mc.enterInput(mc.confirmCellulare, cellulare);
			mc.clickComponent(mc.continueButton);
			mc.comprareText(mc.cellulareInvalidError, mc.CellularInvalidError, true);
			
			String empty_cellulare ="";
			mc.enterInput(mc.cellulare, empty_cellulare);
			mc.clickComponent(mc.continueButton);
			
			mc.ClearInputFieldValue(mc.cellulare);
			String valid_Cellulare = "3669047153";
			mc.enterInput(mc.cellulare, valid_Cellulare);
			mc.enterInput(mc.confirmCellulare, valid_Cellulare);
			mc.clickComponent(mc.continueButton);
			
			logger.write("Verify Modifica Info enel enegia title and page details -- Start");
			mc.verifyComponentExistence(mc.pageTitle);
			mc.comprareText(mc.titleSubText1, mc.TitleSubText1, true);
			mc.verifyComponentExistence(mc.riepilogoEConfermaDati);
			mc.verifyComponentExistence(mc.tipologiaDiAvviso);
			mc.verifyComponentExistence(mc.comunicaLetturaTitle);
			mc.verifyComponentExistence(mc.comunicaLetturaValue);
			mc.verifyComponentExistence(mc.emissioneBollettaTitle);
			mc.verifyComponentExistence(mc.emissioneBollettaValue);
			mc.verifyComponentExistence(mc.avvenutoPagamentoTitle);
			mc.verifyComponentExistence(mc.avvenutoPagamentoValue);
			mc.verifyComponentExistence(mc.prossimaScadenzaTitle);
			mc.verifyComponentExistence(mc.prossimaScadenzaValue);
			mc.verifyComponentExistence(mc.sollecitoPagamentoTitle);
			mc.verifyComponentExistence(mc.sollecitoPagamentoValue);
			mc.verifyComponentExistence(mc.avvisoConsumiTitle);
			mc.verifyComponentExistence(mc.avvisoConsumiValue);
			mc.verifyComponentExistence(mc.iTuoiDati);
			mc.verifyComponentExistence(mc.supplyTitle);
			mc.verifyComponentExistence(mc.address);
			logger.write("Verify Modifica Info enel enegia title and page details -- Completed");
			
			logger.write("Click on conferma and verify the text -- Start");
			mc.clickComponent(mc.confermaButton);
			Thread.sleep(50000);
			//mc.comprareText(mc.confermaText1, mc.ConfermaText1, true);
			//mc.comprareText(mc.confermaText2, mc.ConfermaText2, true);
			logger.write("Click on conferma and verify the text -- Completed");

			/*logger.write("Click on Fine button and verify Home page text   -- Starts");		
			//mc.clickComponent(mc.fineButton);
			Thread.sleep(50000);
			//dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			//dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			//dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			//dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Click on Fine button and verify Home page text   -- Ends");	
			*/
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
