package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SelezioneCausaleVolturaCheckCampiBusiness {
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            SezioneMercatoComponent mercato = new SezioneMercatoComponent(driver);
            logger.write("Verifica checklist e conferma - Start");

            gestione.checkSpinnersSFDC();
            checkListModalComponent.clickConferma();

            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");
            logger.write("check esistenza sezione 'Causale', selezione mercato 'Trasformazione/Fusione Azienda' e click su Conferma - Start");

            By label_SelezioneCausale = mercato.labelSelezioneCausale;
            mercato.verifyComponentExistence(label_SelezioneCausale);
            mercato.verificaSezioneCausaleBSN();
            mercato.selezionaCusale("Trasformazione/Fusione Azienda");

            By conferma = mercato.buttonConfermaSezioneCausale;
            mercato.clickComponentIfExist(conferma);
            gestione.checkSpinnersSFDC();
            logger.write("check esistenza sezione 'Causale', selezione mercato 'Trasformazione/Fusione Azienda' e click su Conferma - Completed");
            logger.write("click su 'Modifica' - Start");

            By modifica = mercato.buttonModificaSezioneMercato;
            mercato.clickComponentIfExist(modifica);

            logger.write("click su 'Modifica' - Completed");
            logger.write("check esistenza sezione 'Causale', selezione mercato 'Cessione Ramo Azienda' e click su Conferma - Start");

            mercato.verificaSezioneCausaleBSN();
            mercato.selezionaCusale("Cessione Ramo Azienda");


            mercato.clickComponentIfExist(conferma);
            gestione.checkSpinnersSFDC();
            logger.write("check esistenza sezione 'Causale', selezione mercato 'Cessione Ramo Azienda' e click su Conferma - Completed");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
