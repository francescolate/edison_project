package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CondizionigeneralideiservizioffertiModificaProfiloComponentConstatnt;
import com.nttdata.qa.enel.components.colla.EditProfileComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SupportFaqComponent;
import com.nttdata.qa.enel.components.colla.SupportLegalClausesComponent;
import com.nttdata.qa.enel.components.colla.SupportPrivacyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DATI_DI_REGISTRAZIONE_ACR_56 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			HomeComponent home = new HomeComponent(driver);
			
			prop.setProperty("LOGOUTURL", "https://www-coll1.enel.it/"+"it");
		    prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
			prop.setProperty("POPUPTEXT", "Modifica numero di cellulare Potrai modificare in ogni momento il numero di cellulare inserito seguendo le indicazioni riportate qui");
			prop.setProperty("SUPPORTFAQ1", "Non riesco ad accedere e a registrarmi al mio profilo unico");
			prop.setProperty("SUPPORTFAQ2", "Ho dimenticato la password. Posso recuperarla?");
			prop.setProperty("SUPPORTFAQ3", "Non riesci ancora ad accedere o a registrarti?");
			prop.setProperty("SUPPORTFAQ4", "Ho problemi con il codice OTP. Cosa devo fare?");
			prop.setProperty("SUPPORTFAQ5", "Non ricordo lo username con il quale mi sono registrato, cosa devo fare?");
			prop.setProperty("MODIFICAEMAILTEXT", "Inserisci un nuovo indirizzo e-mail che utilizzerai per accedere alla tua Area Clienti ed App**");
			prop.setProperty("ELIMINA_REGISTRAZIONE_TITLE", "Sei sicuro di voler eliminare la tua registrazione?");
			prop.setProperty("NUOVO_INDIRIZZO_EMAIL", "Example@mail.com");
			prop.setProperty("CONFERMA_NUOVO_INDIRIZZO_EMAIL", "Example@mail.com");
			prop.setProperty("VECCHIA_PASSWORD", "Conferma nuova password");
			prop.setProperty("NUOVA_PASSWORD", "Nuova password");
			prop.setProperty("CONFERMA_NUOVA_PASSWORD", "Conferma nuova password");

			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
						
			logger.write("Check for Residential Menu - Completed");	
			
			home.verifyComponentExistence(home.userIcon);
			
			LoginLogoutEnelCollaComponent log =new LoginLogoutEnelCollaComponent(driver);
			
			log.VerifyText(home.userName, prop.getProperty("NOME"));
			
			home.verifyComponentExistence(home.downArrow);
			
			logger.write("Click down arrow - Start");
			
			home.clickComponent(home.downArrow);
			
			logger.write("Click down arrow - Completed");
			
			home.verifyComponentExistence(home.editProfile);
			
			home.verifyComponentExistence(home.goOut);
			
			logger.write("Click on edit profile - Start");

			home.clickComponent(home.editProfile);

			logger.write("Click on edit profile - Completed");
			
			EditProfileComponent ed = new EditProfileComponent(driver);
			
			logger.write("Check For name - Start");

			ed.verifyComponentExistence(ed.namelabel);
			
			ed.checkFieldValue(ed.name, prop.getProperty("NOME"));
			
			logger.write("Check For name - Completed");
			
			logger.write("Check For cognome - Start");

			ed.verifyComponentExistence(ed.cogNomeLabel);
			
			ed.checkFieldValue(ed.cogNome, prop.getProperty("COGNOME"));
		
			logger.write("Check For cognome - Completed");

			logger.write("Check For phone number - Start");

			ed.verifyComponentExistence(ed.phoneNumLabel);
			
			ed.checkFieldValue(ed.phoneNum, prop.getProperty("CELLULARE"));

			logger.write("Check For phone number - Completed");

			logger.write("Check For codice fiscale - Start");

			ed.verifyComponentExistence(ed.codiceFiscaleLabel);
			
			ed.checkFieldValue(ed.codiceFiscale, prop.getProperty("CODICE_FISCALE"));
			
			logger.write("Check For codice fiscale - Completed");

//			logger.write("Click on icon - Start");
//
//			ed.clickComponent(ed.iIcon);
//			
//			logger.write("Click on icon - Completed");
			
//			ed.checkPopUpText(ed.modalText,ed.modalTitle,prop.getProperty("POPUPTEXT"));
//			
//			ed.verifyComponentExistence(ed.hereLink);
//			
//			logger.write("Click on qui link in pop up - Start");
//
//			ed.clickComponent(ed.hereLink);
//			
//			logger.write("Click on qui link in pop up - Completed");

//			SupportFaqComponent sqc = new SupportFaqComponent(driver);
//			
//			logger.write("Check Support Faq Details - Start");
//
//			sqc.CheckForFaq(prop.getProperty("SUPPORTFAQ1"),prop.getProperty("SUPPORTFAQ2"),prop.getProperty("SUPPORTFAQ3"),prop.getProperty("SUPPORTFAQ4"),prop.getProperty("SUPPORTFAQ5"));
//						
//			logger.write("Check Support Faq Details - Completed");
//
//			logger.write("Close pop up - Start");
//
//			ed.clickComponent(ed.modalClose);
//			
//			logger.write("Close pop up - Completed");

			logger.write("Check sezione Condizioni Generali - Start");
			ed.verifyComponentExistence(ed.titleCondizioniGenerali);
			
			ed.verifyComponentExistence(ed.checkbox1);
			
			ed.verifyComponentExistence(ed.checkbox2);

			logger.write("Check sezione Condizioni Generali - End");

			logger.write("Click on privacy disclaimer and check details - Start");

			ed.clickComponent(ed.checkbox1);
			
			ed.comprareText(ed.informativaPrivacy, CondizionigeneralideiservizioffertiModificaProfiloComponentConstatnt.informativaPrivacyConstant, true);
		
			logger.write("Click on privacy disclaimer and check details - Close");

			ed.clickComponent(ed.privacyDisclaimerClose);
			
			logger.write("Click on Genaral conditions and check details - Start");

			ed.clickComponent(ed.checkbox2);
						
			ed.comprareText(ed.generalConditions, CondizionigeneralideiservizioffertiModificaProfiloComponentConstatnt.Condizionigeneralideiserviziofferti, true);
			
			logger.write("Click on Genaral conditions and check details - Start");

			ed.clickComponent(ed.generalConditionClose);

			ed.verifyComponentExistence(ed.modificaEmailMenu);
			
//			logger.write("Click on Modifica Email menu option and check details - Start");
//
//			ed.clickComponent(ed.modificaEmailMenu);
//			
//			ed.verifyComponentExistence(ed.modificaEmailText);
//			
//			ed.checkForText(ed.modificaEmailText, prop.getProperty("ModificaEmailText"));
//
//			ed.verifyComponentExistence(ed.newEmailLabel);
//			
//			ed.checkForPlaceholderText(ed.newEmailInput, prop.getProperty("NUOVO_INDIRIZZO_EMAIL"));
//			
//			ed.verifyComponentExistence(ed.confirmNewEmail);
//			
//			ed.checkForPlaceholderText(ed.confirmNewEmailInput, prop.getProperty("CONFERMA_NUOVO_INDIRIZZO_EMAIL"));
//			
//			ed.verifyComponentExistence(ed.requiredFieldEmail);
//			
//			ed.verifyComponentExistence(ed.emailConfirmation);
//			
//			logger.write("Click on Modifica Email menu option and check details - Completed");

			logger.write("Click on Modifica Password menu option and check details - Start");

			ed.clickComponent(ed.modificaPasswordMenu);
			
			ed.verifyComponentExistence(ed.modificaPasswordTitle);
			
			ed.verifyComponentExistence(ed.oldPassword);
			
			ed.checkForPlaceholderText(ed.oldPasswordInput, prop.getProperty("VECCHIA_PASSWORD"));
			
			ed.verifyComponentExistence(ed.newPassword);
			
			ed.checkForPlaceholderText(ed.newPasswordInput, prop.getProperty("NUOVA_PASSWORD"));
			
			ed.verifyComponentExistence(ed.confirmPassword);
			
			ed.checkForPlaceholderText(ed.confirmPasswordInput, prop.getProperty("CONFERMA_NUOVA_PASSWORD"));
			
			ed.verifyComponentExistence(ed.requiredFieldPsw);
			
			ed.verifyComponentExistence(ed.pswConfirmation);
			
			logger.write("Click on Modifica Password menu option and check details - Start");

			logger.write("Click on Elimina Registrazione menu option and check details - Start");

			ed.clickComponent(ed.deleteRegistrationMenu);
			
			ed.checkForText(ed.deleteRegistrationText1, prop.getProperty("ELIMINA_REGISTRAZIONE_TITLE"));
			
		//	ed.checkForText(ed.deleteRegistrationText2, prop.getProperty("Elimina_Registrazione_Text"));
			
			ed.verifyComponentExistence(ed.continueDeleteReg);
			
			logger.write("Click on Elimina Registrazione menu option and check details - Start");

			logger.write("Click on FAQ menu option and check details - Start");

			ed.clickComponent(ed.faqMenu);
			
			SupportFaqComponent sqc = new SupportFaqComponent(driver);
			sqc.CheckForFaq(prop.getProperty("SUPPORTFAQ1"),prop.getProperty("SUPPORTFAQ2"),prop.getProperty("SUPPORTFAQ3"),prop.getProperty("SUPPORTFAQ4"),prop.getProperty("SUPPORTFAQ5"));

			logger.write("Click on FAQ menu option and check details - Completed");

			logger.write("Click on Privacy menu option  - Start");

			ed.verifyComponentExistence(ed.privacyMenu);
			
			ed.clickComponent(ed.privacyMenu);
			
			logger.write("Click on Privacy menu option  - Completed");

			SupportPrivacyComponent spc = new SupportPrivacyComponent(driver);
					
			logger.write("Check Privacy page details - Start");
			
			spc.SwitchTabToPrivacy();
			
			logger.write("Check Privacy page details - Completed");
									
			logger.write("Click on Clauses Legali menu option and check details - Start");

			ed.clickComponent(ed.legalClausesMenu);
			
			SupportLegalClausesComponent slc = new SupportLegalClausesComponent(driver);

			slc.SwitchTabToLegalClauses();

			logger.write("Click on Clauses Legali menu option and check details - Completed");
			
			logger.write("Click on Torna all'Area Clienti menu option and check details - Start");

			ed.clickComponent(ed.customerAreaMenu);
			
			home.SwitchTabToCustomerArea();
			
			logger.write("Click on Torna all'Area Clienti menu option and check details - Completed");

			logger.write("Click on Logout menu option and check details - Start");

			ed.clickComponent(ed.logOutMenu);
			
			logger.write("Click on Logout menu option and check details - Completed");
			
		
//			ed.checkURLAfterRedirection(prop.getProperty("LOGOUTURL"));
			
			prop.setProperty("RETURN_VALUE", "OK");
			
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
