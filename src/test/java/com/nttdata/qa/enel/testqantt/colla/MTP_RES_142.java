package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_142 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
											
			home.verifyComponentExistence(home.enelLogo);
								
			home.clickComponent(home.detaglioFurnitura);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			
			logger.write("Check For page title-- start");
			
			dc.verifyComponentExistence(dc.pageTitle);
			
			logger.write("Check For page title-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			dc.verifyComponentExistence(dc.serviziPerleforniture);
			
			Thread.sleep(10000);
			
			dc.verifyComponentExistence(dc.modificaPotenzo);
			
			dc.jsClickObject(dc.modificaPotenzo);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.ModificaSubText, true);
			
			changePV.verifyComponentExistence(changePV.radioButton);
			
			changePV.verifyComponentExistence(changePV.flashIcon);
			
			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");
			
			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.TitleSubText, true);			
			
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");
			
			changePV.comprareText(changePV.formLine1, changePV.FormLine1, true);
			
			logger.write("Check for Power Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.powerLable);
			
			logger.write("Check for Power Lable -- Completed");
			
			logger.write("Check for Default Power value -- Start");
			
			changePV.checkForDefaultValue(changePV.powerValue,prop.getProperty("POWERDEFAULT"));
			
			logger.write("Check for Default Power value -- Completed");
			
			logger.write("Check for Voltage Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.voltageLable);
			
			logger.write("Check for Voltage Lable -- Completed");
			
			logger.write("Check for Default Voltage value -- Start");
			
			changePV.checkForDefaultValue(changePV.voltageValue,prop.getProperty("VOLTAGEFAULT"));
			
			logger.write("Check for Default Voltage value -- Completed");
			
			changePV.comprareText(changePV.formLine2, changePV.FormLine2, true);
			
			changePV.checkButtonIsEnable(changePV.radioNo);
			
			changePV.comprareText(changePV.formLine3, changePV.FormLine3, true);
			
			changePV.changeDropDownValue(changePV.voltageValue,changePV.setVoltageValue);
			changePV.clearFieldValue(changePV.cellular);
			changePV.enterInputtoField(changePV.cellular, "3391234567");
			

			changePV.checkFieldValue(changePV.emailInput, prop.getProperty("WP_USERNAME"));
			
			
			logger.write("Clear email field and verify error message -- Start");

			changePV.clearFieldValue(changePV.email);
			changePV.enterInputtoField(changePV.confirmCellular, "3391234567");
			changePV.clickComponent(changePV.calculateQuote);
			changePV.verifyComponentExistence(changePV.emailError);
			changePV.comprareText(changePV.emailError, changePV.emptyEmailError, true);
			
			logger.write("Clear email field and verify error message -- Completed");
			
			logger.write("Enter invalid email field and verify error message -- Start");

			changePV.clearFieldValue(changePV.email);

			changePV.enterInputtoField(changePV.email, prop.getProperty("INVALID_EMAIL"));
			changePV.clickComponent(changePV.calculateQuote);
			changePV.verifyComponentExistence(changePV.emailError);
			changePV.comprareText(changePV.emailError, changePV.invalidEmailError, true);
			
			logger.write("Enter invalid email field and verify error message -- Completed");
			
			logger.write("Enter invalid confirm email and verify error message -- Start");

			changePV.clearFieldValue(changePV.email);
			changePV.enterInputtoField(changePV.email, prop.getProperty("WP_USERNAME"));
			changePV.clickComponent(changePV.calculateQuote);
			changePV.verifyComponentExistence(changePV.confirmEmail);

			changePV.enterInputtoField(changePV.confirmEmail, prop.getProperty("INVALID_CONFIRMEMAIL"));
			changePV.clickComponent(changePV.calculateQuote);
			changePV.comprareText(changePV.confirmInvalidEmailError, changePV.confirmEmailErrorMsg, true);

			logger.write("Enter invalid confirm email and verify error message -- Completed");

			logger.write("Enter empty cellular and verify error message -- Start");

			Thread.sleep(5000);
			changePV.clearFieldValue(changePV.cellular);
			changePV.clickComponent(changePV.calculateQuote);
			changePV.comprareText(changePV.cellularError, changePV.cellularErrorMsg, true);
			
			logger.write("Enter empty cellular and verify error message -- Completed");

			logger.write("Enter invalid cellular and verify error message -- Start");
			changePV.enterInputtoField(changePV.cellular, prop.getProperty("INVALID_CELLULAR"));
			changePV.comprareText(changePV.cellularError, changePV.InvalidCellularErrorMsg, true);
			
			logger.write("Enter invalid cellular and verify error message -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
		
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
