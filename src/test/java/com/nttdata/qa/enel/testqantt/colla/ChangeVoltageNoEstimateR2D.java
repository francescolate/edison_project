package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_InvioPraticheComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangeVoltageNoEstimateR2D {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Selecting Report Post Sales");
			logger.write("Selecting Report Post Sales - Start");
			driver.manage().window().maximize();
			R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
			menuBox.selezionaVoceMenuBox("Interrogazione", "Report Post Sales");
			logger.write("Selecting Report Post Sales - Completed");
			
			System.out.println("Inserting POD");
			logger.write("Inserting POD - Start");
			R2D_VerifichePodComponent verifichePod = new R2D_VerifichePodComponent(driver);
			verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD"));
			logger.write("Inserting POD - Completed");
			
			System.out.println("Selecting dettaglio pratiche");
			logger.write("Selecting dettaglio pratiche - Start");
			verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
			logger.write("Selecting dettaglio pratiche - Completed");
			
			System.out.println("Checking status AW and distributor");
			logger.write("Checking status and distributor - Start");
			String praticaRow = verifichePod.findPraticaRowNumber(prop.getProperty("REFERENCE_DATE"));
			By statusLocator = By.xpath(verifichePod.statusRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			By distributorLocator = By.xpath(verifichePod.distributorRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			verifichePod.checkPraticaStatus(statusLocator, verifichePod.status_AW);
			String distributorShort = driver.findElement(distributorLocator).getText();
			prop.setProperty("DISTRIBUTOR_SHORT", distributorShort);
			logger.write("Checking status and distributor - Completed");
			
			System.out.println("Accessing details");
			logger.write("Accessing details - Start");
			verifichePod.selezionaPratica(distributorLocator);
			String distributor = verifichePod.getDistributor();
			prop.setProperty("DISTRIBUTOR", distributor);
			logger.write("Accessing details - Completed");
			
			System.out.println("Selecting Invio Ps a Portale");
			logger.write("Selecting Invio Ps a Portale - Start");
			menuBox.selezionaVoceMenuBox("Code elaborazione dati", "Invio PS a portale");
			logger.write("Selecting Invio Ps a Portale - Completed");
			
			System.out.println("Selecting distributor");
			logger.write("Selecting distributor - Start");
			R2D_InvioPraticheComponent selezionaPratiche = new R2D_InvioPraticheComponent(driver);
			selezionaPratiche.selezionaDistributore(prop.getProperty("DISTRIBUTOR_SHORT"));
			logger.write("Selecting distributor - Completed");
			
			System.out.println("Selecting tipo lavorazione");
			logger.write("Selecting tipo lavorazione - Start");
			selezionaPratiche.selezionaTipoLavorazioneCoda(prop.getProperty("TIPO_LAVORAZIONE_CRM_ELE"));
			logger.write("Selecting tipo lavorazione - Completed");
			
			System.out.println("Sending paperwork and downloading excel file");
			logger.write("Sending paperwork and downloading excel file - Start");
			selezionaPratiche.invioPratiche();
			logger.write("Sending paperwork and downloading excel file - Completed");
			
			System.out.println("Selecting Report Post Sales");
			logger.write("Selecting Report Post Sales - Start");
			menuBox.selezionaVoceMenuBox("Interrogazione", "Report Post Sales");
			logger.write("Selecting Report Post Sales - Completed");
			
			System.out.println("Inserting POD");
			logger.write("Inserting POD - Start");
			verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD"));
			logger.write("Inserting POD - Completed");
			
			System.out.println("Selecting dettaglio pratiche");
			logger.write("Selecting dettaglio pratiche - Start");
			verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
			logger.write("Selecting dettaglio pratiche - Completed");
			
			System.out.println("Checking status AA");
			logger.write("Checking status - Start");
			praticaRow = verifichePod.findPraticaRowNumber(prop.getProperty("REFERENCE_DATE"));
			statusLocator = By.xpath(verifichePod.statusRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			verifichePod.checkPraticaStatus(statusLocator, verifichePod.status_AA);
			logger.write("Checking status and distributor - Completed");
			
			System.out.println("Selecting Caricamento Esiti");
			logger.write("Selecting Caricamento Esiti - Start");
			menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
			R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
			caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
			logger.write("Selecting Caricamento Esiti - Completed");
			
			System.out.println("Inserting POD");
			logger.write("Inserting POD - Start");
			caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD"));
			caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
			logger.write("Inserting POD - Completed");
			
			System.out.println("Saving Codice Richiesta CRM");
			logger.write("Saving Codice Richiesta CRM - Start");
			String crmCode = driver.findElement(caricamentoEsiti.codiceRichiestaCrmPratica).getText();
			prop.setProperty("CODICE_RICHIESTA_CRM", crmCode);
			logger.write("Saving Codice Richiesta CRM - Completed");
			
			System.out.println("Selecting Esito Ammissibilità");
			logger.write("Selecting Esito Ammissibilità - Start");
			caricamentoEsiti.selezionaTastoAzionePratica();
			caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, "Esito Ammissibilità - Esito Ammissibilità Richiesta MC1");
			caricamentoEsiti.selezioneEsito("OK");
			logger.write("Selecting Esito Ammissibilità - Completed");
			
			System.out.println("Inserting Esito Ammissibilità");
			logger.write("Inserting Esito Ammissibilità - Start");
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String esitoDate = simpleDateFormat.format(calendar.getTime()).toString();
			caricamentoEsiti.inserimentoDettaglioEsitoCambioTensione(crmCode, esitoDate);
			logger.write("Inserting Esito Ammissibilità - Completed");
			
			System.out.println("Selecting Report Post Sales");
			logger.write("Selecting Report Post Sales - Start");
			menuBox.selezionaVoceMenuBox("Interrogazione", "Report Post Sales");
			logger.write("Selecting Report Post Sales - Completed");
			
			System.out.println("Inserting POD");
			logger.write("Inserting POD - Start");
			verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD"));
			logger.write("Inserting POD - Completed");
			
			System.out.println("Selecting dettaglio pratiche");
			logger.write("Selecting dettaglio pratiche - Start");
			verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
			logger.write("Selecting dettaglio pratiche - Completed");
			
			System.out.println("Checking status CI, Tipo Esito, and Esito MUTI");
			logger.write("Checking status CI, Tipo Esito, and Esito MUTI - Start");
			praticaRow = verifichePod.findPraticaRowNumber(prop.getProperty("REFERENCE_DATE"));
			statusLocator = By.xpath(verifichePod.statusRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			distributorLocator = By.xpath(verifichePod.distributorRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			verifichePod.checkPraticaStatus(statusLocator, verifichePod.status_CI);
			verifichePod.selezionaPratica(distributorLocator);
			ArrayList<String> campiDaVerificare = new ArrayList<String>();
			campiDaVerificare.add("Tipo Esito;" + "3");
			campiDaVerificare.add("Esito MUTI;" + "OK");
			ArrayList<String> campiDaSalvare = new ArrayList<String>();
			verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);
			logger.write("Checking status CI, Tipo Esito, and Esito MUTI - Completed");
			
			System.out.println("Selecting Caricamento Esiti");
			logger.write("Selecting Caricamento Esiti - Start");
			menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
			caricamentoEsiti.selezionaTipoCaricamento("Puntuale PS");
			logger.write("Selecting Caricamento Esiti - Completed");
			
			System.out.println("Inserting POD");
			logger.write("Inserting POD - Start");
			caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD"));
			caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
			logger.write("Inserting POD - Completed");
			
			System.out.println("Selecting Esito Richiesta MC1 Preventivo Rapido");
			logger.write("Selecting Esito Richiesta MC1 Preventivo Rapido - Start");
			caricamentoEsiti.selezionaTastoAzionePratica();
			caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, "Esito Richiesta - Esito Richiesta MC1 Preventivo Rapido");
			caricamentoEsiti.selezioneEsito("OK");
			logger.write("Selecting Esito Richiesta MC1 Preventivo Rapido - Completed");
			
			System.out.println("Accessing estimate preparation page");
			logger.write("Accessing estimate preparation page - Start");
			caricamentoEsiti.verifyComponentVisibility(caricamentoEsiti.bisognaCaricarePreventivoBtn);
			caricamentoEsiti.clickComponent(caricamentoEsiti.bisognaCaricarePreventivoBtn);
			caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD"));
			caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
			caricamentoEsiti.verifyComponentVisibility(caricamentoEsiti.modificaPraticaBtn);
			caricamentoEsiti.clickComponent(caricamentoEsiti.modificaPraticaBtn);
			caricamentoEsiti.verifyComponentVisibility(caricamentoEsiti.uploadTypeSelect);
			caricamentoEsiti.selectItem(caricamentoEsiti.uploadTypeSelect, "Preventivo");
			logger.write("Accessing estimate preparation page - Completed");
			
			System.out.println("Sending estimate");
			logger.write("Sending estimate - Start");
			String dataRicezione = prop.getProperty("REFERENCE_DATE");
			Random rand = new Random();
//			String numeroPreventivo = Integer.toString(rand.nextInt(999999999));
			String numeroPreventivo = "123456789";
			String dataPreventivo = prop.getProperty("REFERENCE_DATE");
			String codiceRintracciabilita = prop.getProperty("CODICE_RICHIESTA_CRM");
			//One month from today
			calendar.add(Calendar.MONTH, 1);
			String dataScadenza = simpleDateFormat.format(calendar.getTime()).toString();
			calendar.add(Calendar.MONTH, -1);
			String tempoMax = "10";
			String priceOption = " MANODOPERA ";
			String price = "100";
			caricamentoEsiti.sendEstimate(dataRicezione, numeroPreventivo, dataPreventivo, codiceRintracciabilita, dataScadenza, tempoMax, priceOption, price);
			logger.write("Sending estimate - Completed");
			
			System.out.println("Selecting Report Post Sales");
			logger.write("Selecting Report Post Sales - Start");
			menuBox.selezionaVoceMenuBox("Interrogazione", "Report Post Sales");
			logger.write("Selecting Report Post Sales - Completed");
			
			System.out.println("Inserting POD");
			logger.write("Inserting POD - Start");
			verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD"));
			logger.write("Inserting POD - Completed");
			
			System.out.println("Selecting dettaglio pratiche");
			logger.write("Selecting dettaglio pratiche - Start");
			verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
			logger.write("Selecting dettaglio pratiche - Completed");
			
			System.out.println("Checking status OK, Tipo Esito, and Esito MUTI");
			logger.write("Checking status OK, Tipo Esito, and Esito MUTI - Start");
			praticaRow = verifichePod.findPraticaRowNumber(prop.getProperty("REFERENCE_DATE"));
			statusLocator = By.xpath(verifichePod.statusRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			distributorLocator = By.xpath(verifichePod.distributorRigaPratica.replace("$ROW_NUMBER$", praticaRow));
			verifichePod.checkPraticaStatus(statusLocator, verifichePod.status_OK);
			verifichePod.selezionaPratica(distributorLocator);
			ArrayList<String> campiDaVerificare2 = new ArrayList<String>();
			campiDaVerificare2.add("Tipo Esito;" + "5");
			campiDaVerificare2.add("Esito MUTI;" + "OK");
			verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);
			logger.write("Checking status OK, Tipo Esito, and Esito MUTI - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
