package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GiustaPerTeImpresaGasComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DocumentiGiustaPerTeImpresaGas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			GiustaPerTeImpresaGasComponent documenti=new GiustaPerTeImpresaGasComponent(driver);
            
			logger.write("Click sulla sezione Documenti (sotto al banner dell offerta) - start");
			By tab_Documenti=documenti.tabDocumenti;
			documenti.verifyComponentExistence(tab_Documenti);
			documenti.clickComponent(tab_Documenti);
			TimeUnit.SECONDS.sleep(2);
			By sezione_Documenti=documenti.sezioneDocumentiLabel;
			documenti.verifyComponentExistence(sezione_Documenti);
			logger.write("Click sulla sezione Documenti (sotto al banner dell offerta) - Completed");
			
			logger.write("Click sulla sezione 'Documenti Contratto'  - Start");
			By documenti_Contratto=documenti.documentiContratto;
//			documenti.checkDocumenti(documenti_Contratto,prop.getProperty("LINK")+documenti.linkPLUS_IMAGE,prop.getProperty("LINK")+documenti.linkMINUS_IMAGE);
			documenti.checkDocumenti(documenti_Contratto,documenti.linkPLUS_IMAGE,documenti.linkMINUS_IMAGE);
					
			documenti.dettagliDocumentiContratto(documenti_Contratto);
			logger.write("Click sulla sezione 'Documenti Contratto' - Completed");
			
			
			
			logger.write("Click per ogni documento nella sezione 'Documenti Contratto' su Download - Start");
			By link_Dowload1=documenti.linkDowload1;
			documenti.verifyComponentExistence(link_Dowload1);
			
			String winHandleBefore = driver.getWindowHandle();
			winHandleBefore = driver.getWindowHandle();
			documenti.clickComponent(link_Dowload1);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			documenti.checkUrl(prop.getProperty("LINK")+documenti.linkDOWNLOAD1);
			driver.close();
			driver.switchTo().window(winHandleBefore);

			logger.write("Click per ogni documento nella sezione 'Documenti Contratto' su Download - Completed");

			//----
			logger.write("Click sulla sezione 'Documenti Generali'  - Start");
			By documenti_Generali=documenti.documentiGenerali;
			documenti.checkDocumentiGenerali(documenti_Generali,documenti.linkPLUS_IMAGE,documenti.linkMINUS_IMAGE);
					
	//		documenti.dettagliDocumentiContratto(documenti_Generali);
			logger.write("Click sulla sezione 'Documenti Generali ' - Completed");


			logger.write("Click per ogni documento nella sezione 'Documenti Generali' su Download - Start");

			By link_Dowload2=documenti.linkDowload2;
			documenti.verifyComponentExistence(link_Dowload2);
			
			winHandleBefore = driver.getWindowHandle();
			documenti.clickComponent(link_Dowload2);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			documenti.checkUrl(prop.getProperty("LINK")+documenti.linkDOWNLOAD2);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			By link_Dowload3=documenti.linkDowload3;
			documenti.verifyComponentExistence(link_Dowload3);
			
			winHandleBefore = driver.getWindowHandle();
			documenti.clickComponent(link_Dowload3);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			documenti.checkUrl(prop.getProperty("LINK")+documenti.linkDOWNLOAD3);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			By link_Dowload4=documenti.linkDowload4;
			documenti.verifyComponentExistence(link_Dowload4);
			
			winHandleBefore = driver.getWindowHandle();
			documenti.clickComponent(link_Dowload4);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			documenti.checkUrl(prop.getProperty("LINK")+documenti.linkDOWNLOAD4);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			By link_Dowload5=documenti.linkDowload5;
			documenti.verifyComponentExistence(link_Dowload5);
			
			winHandleBefore = driver.getWindowHandle();
			documenti.clickComponent(link_Dowload5);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			documenti.checkUrl(prop.getProperty("LINK")+documenti.linkDOWNLOAD5);
			driver.close();
			driver.switchTo().window(winHandleBefore);
			
			By link_Dowload6=documenti.linkDowload6;
			documenti.verifyComponentExistence(link_Dowload6);
			
			winHandleBefore = driver.getWindowHandle();
			documenti.clickComponent(link_Dowload6);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			documenti.checkUrl(prop.getProperty("LINK")+documenti.linkDOWNLOAD6);
			driver.close();
			driver.switchTo().window(winHandleBefore);


			logger.write("Click per ogni documento nella sezione 'Documenti Generali' su Download - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
