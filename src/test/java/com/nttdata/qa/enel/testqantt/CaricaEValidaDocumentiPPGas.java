package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CaricaEValidaDocumentiPPGas {
	@Step("Carica Documenti")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		driver.switchTo().defaultContent();
		DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
		DocumentValidatorComponent validator = new DocumentValidatorComponent(driver); 

		logger.write("Upload Documento - Start");
	    TimeUnit.SECONDS.sleep(30);
		System.out.println("Upload Documento - Start");
		doc.uploadFile(Utility.getDocumentPdf(prop));
		System.out.println("Upload Documento - OK");
		logger.write("Upload Documento - Completed");
		
		logger.write("Visualizza Documento - Start");
	    TimeUnit.SECONDS.sleep(15);
    
		doc.visualizzaDocumenti();
		System.out.println("Visualizza Documento - Completed");
		logger.write("Visualizza Documento - Completed");
		
		
		logger.write("Valida Documento - Start");
		System.out.println("Valida Documento - Start");
	    TimeUnit.SECONDS.sleep(15);
	    
//		validator.validaDoc(util.getFrameByIndex(1));
	    validator.validaDoc("");
		System.out.println("Validazione OK");
		logger.write("Valida Documento - Completed");
		TimeUnit.SECONDS.sleep(10);
//		// cambio STATO_RICHIESTA per verifiche finali dopo Validazione Documentazione
//		prop.setProperty("STATO_RICHIESTA", "CHIUSA DA CONFERMARE");
//		if(prop.getProperty("VALIDA_DOCUMENTI", "N").equals("N")){
//			new GestioneAppuntamentoComponent(driver).esci();
//		}
		
		// Gestione Appuntamento - cliccare sul bottone Esci
		logger.write("Verifica Appuntamento bottone Esci");
		validator.Esci();
		
		logger.write("Chiudi Validator Documento - Start");
		validator.chiudi();
		System.out.println("CHIUDI Validazione");
		logger.write("Chiudi Validator Documento - Completed");
		prop.setProperty("RETURN_VALUE", "OK");
	} catch (Exception e) {
		prop.setProperty("RETURN_VALUE", "KO");
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: "+errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
		if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

	}finally
	{
		//Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
	}

}
}
