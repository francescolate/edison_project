package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_296_MoGe_E2E {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			
			try{
				if (!prop.getProperty("AREA_CLIENTI").equals(""))
				{
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
						hm.verifyComponentExistence(hm.areaClientiCasa);
						hm.clickComponent(hm.areaClientiCasa);
					}
					else
					{
						hm.verifyComponentExistence(hm.areaClientiImpresa);
						hm.clickComponent(hm.areaClientiImpresa);
					}
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
				} else {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
				}
			}catch(Exception e){
				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
			}
			
			Thread.sleep(7000);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			logger.write("Click on Account from residential menu -- Strat");
			hm.clickComponent(hm.account);
			Thread.sleep(20000);
			logger.write("Click on Account from residential menu -- Completed");

			AccountComponent ac = new AccountComponent(driver);
			logger.write("Verify page navigation and details in Account page -- Strat");
			ac.verifyComponentExistence(ac.pageTitle);
			hm.comprareText(ac.pageTitle, ac.PAGE_TITLE, true);
			hm.comprareText(ac.titleSubText, ac.TITLE_SUBTEXT, true);
			logger.write("Verify page navigation and details in Account page -- Completed");
			logger.write("Click on Modifica Contatti button-- Strat");
			ac.clickComponent(ac.modificaContatti);
			logger.write("Click on Modifica Contatti button-- Completed");
			Thread.sleep(10000);
			logger.write("Verify Dati e contatti title and subtext-- Strat");
			ac.verifyComponentExistence(ac.datiEContattiTitle);
			Thread.sleep(20000);
			ac.comprareText(ac.datiEContattiTitle, ac.PAGE_TITLE, true);
			ac.comprareText(ac.datiEContattiSubText, ac.DATI_E_CONTATTI_SUBTEXT, true);
			logger.write("Verify Dati e contatti title and subtext-- Completed");
			
	//		ac.clearInputField(ac.telefonoFissoIp);
			ac.clearInputField(ac.emailIp);
			ac.clearInputField(ac.emailPECIp);
			ac.clickComponent(ac.SalvaModifiBtn);
			
			ac.comprareText(ac.emailError, ac.EmailError, true);
			
			ac.enterInputParameters(ac.telefonoFissoIp, prop.getProperty("INVALID_INPUT"));
			ac.clickComponent(ac.SalvaModifiBtn);
			Thread.sleep(7000);
	//		ac.comprareText(ac.telefonoError, ac.TelefonoError, true);
			
			ac.enterInputParameters(ac.emailPECIp, prop.getProperty("INVALID_INPUT"));
			ac.clickComponent(ac.SalvaModifiBtn);
			Thread.sleep(7000);
			ac.comprareText(ac.emailPECError, ac.EmailPECError, true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
