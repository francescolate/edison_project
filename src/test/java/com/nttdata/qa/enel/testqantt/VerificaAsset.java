package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaAsset {
	private final static int SEC = 120;
	
	@Step("RecuperaStatusAsset")
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {


				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}
				GregorianCalendar calendar = new GregorianCalendar();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String sDate = simpleDateFormat.format(calendar.getTime()).toString();
				a.insertQuery(Costanti.query_120.replaceAll("@POD@", prop.getProperty("POD")));
				logger.write("Inserimento Query");
				boolean condition = false;
				int tentativi = 2;
				while (!condition && tentativi-- > 0) {
					a.pressButton(a.submitQuery);
					condition = a.aspettaRisultati(SEC);
					logger.write("Esecuzione Query");
				}
				
				if (condition) {
					Map<Integer,Map> HM = a.recuperaTuttiIRisultati();
					//il primo record è revocato 
					
					if(!HM.get(HM.size()-1).get("ITA_IFM_Contract_Agreement_Status__c").equals("REVOCATA")) {
						throw new Exception("Lo stato del campo ITA_IFM_Contract_Agreement_Status__c non è REVOCATA");
					}
					//il secondo record è attiva
					if(!HM.get(HM.size()).get("ITA_IFM_Contract_Agreement_Status__c").equals("ATTIVA")) {
						throw new Exception("Lo stato del campo ITA_IFM_Contract_Agreement_Status__c non è ATTIVA");

					}
					//il secondo record ha il prodotto selezionato
					if(!HM.get(HM.size()).get("NE__Product__c.Name").equals(prop.getProperty("PRODOTTO"))){
						throw new Exception("Lo stato del campo NE__Product__c.Name non è"+prop.getProperty("PRODOTTO"));

					}
//					a.logoutWorkbench();
				} else
					throw new Exception("Impossibile recuperare i risultati dalla query workbench. Sistema lento");

			}
		

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
