package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONObject;

import com.nttdata.qa.enel.components.colla.api.HerokuConsistenzaClienteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIconsistenzaCliente_NumeroConsistenze {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputpu contenuto in Args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			//Da configurare come OutPut
    		prop.setProperty("Authorization", "Basic QXZ2MGYyRjA6ZjBlWERzSHch");
    		prop.setProperty("CHANNELKEY","da692338-b670-4987-86b9-28cfadfdad8a");
    		prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130");//UUID.randomUUID().toString()); //"8b8fdc68-ee96-11e9-81b4-2a2ae2mano18")
    		prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130");//UUID.randomUUID().toString()); //"8b8fdc68-ee96-11e9-81b4-2a2ae2mano19")
    		prop.setProperty("SOURCECHANNEL","ENELX");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuConsistenzaClienteComponent HRKComponent = new HerokuConsistenzaClienteComponent();
			JSONObject jsonResponce = HRKComponent.sendPostRequestReturnJsonBody(prop);

//			if(!dataResponce.equalsIgnoreCase(prop.getProperty("JSON_DATA_OUTPUT")))
//			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			jsonResponce = new JSONObject(jsonResponce.get("data").toString());
			String forniture = jsonResponce.get("forniture").toString();
			String[] arrayForniture = forniture.split("numeroUtente");

			if (arrayForniture.length < 2)
			{
				throw new Exception("Il cliente ha meno di 2 Forniture");
			}
			
			prop.setProperty("JsonObjectDataResponse", jsonResponce.toString());
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
