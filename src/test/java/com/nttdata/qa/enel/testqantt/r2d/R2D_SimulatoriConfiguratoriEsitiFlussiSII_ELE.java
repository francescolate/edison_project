package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_SimulatoreComponent;
import com.nttdata.qa.enel.components.r2d.R2D_SimulatoreFlussoSIIComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_SimulatoriConfiguratoriEsitiFlussiSII_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Simulatori","Configurazione Esiti Flussi SII");
				R2D_SimulatoreFlussoSIIComponent simulatore = new R2D_SimulatoreFlussoSIIComponent(driver);

				//Cerca flusso CK POD
				simulatore.cercaFlusso("PK1", "0150",prop.getProperty("POD"));
				//Configura simulatore
				simulatore.configuraSimulatoreFlussoPK1(prop.getProperty("PIVA_VENDITORE"), prop.getProperty("CP_VENDITORE"), prop.getProperty("CP_GESTORE"),
						prop.getProperty("VERIFICA_AMM"), prop.getProperty("ESITO"), prop.getProperty("COD_ESITO"), prop.getProperty("DETTAGLIO_ESITO"),
						prop.getProperty("CODICE_FISCALE"), prop.getProperty("CODICE_FISCALE"), prop.getProperty("CF_STRANIERO"), prop.getProperty("PIVA_CONTR_COMM"), prop.getProperty("RAG_SOC_CONTR_COMM"),
						prop.getProperty("EMAIL_CONTR_COMM"), prop.getProperty("POTENZA_MEDIA_ANNUA"), prop.getProperty("TIPOLOGIA_CLIENTE"));
	

			simulatore.verificaConfigurazioneFlussoSII();

			prop.setProperty("RETURN_VALUE", "OK");
			
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
