package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SupportFaqComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LaunchCaricaDocumenti {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		String login = "https://www-coll1.enel.it/it/login";
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			Thread.sleep(3000);
			driver.navigate().to(login);
			log.verifyComponentExistence(log.loginPageTitle);
			
			log.verifyComponentExistence(log.changetext);
			log.clickComponent(log.cliccaQui);
			
			SupportFaqComponent sc = new SupportFaqComponent(driver);
			sc.verifyComponentExistence(sc.pageTitle);
			sc.comprareText(sc.pageTitle, sc.PageTitle, true);
			sc.checkURLAfterRedirection(sc.suppotoFaqUrl);
			sc.comprareText(sc.supportoFaqPath, sc.SupportoFaqPathNew, true);
			sc.checkForQuestions(sc.SuppotFaqArray, sc.qst1);
			sc.checkForQuestions(sc.SuppotFaqArray, sc.qst2);
			sc.checkForQuestions(sc.SuppotFaqArray, sc.qst3);
			sc.checkForQuestions(sc.SuppotFaqArray, sc.qst4);
			sc.checkForQuestions(sc.SuppotFaqArray, sc.qst5);
			sc.clickComponent(sc.qst3);
			sc.comprareText(sc.ans3, sc.Ans3, true);
			sc.clickComponent(sc.inviaci);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
}
