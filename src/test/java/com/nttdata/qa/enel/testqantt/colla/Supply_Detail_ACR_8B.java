package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomePageResidentialComponent;
import com.nttdata.qa.enel.components.colla.SupplyDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Supply_Detail_ACR_8B {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
    	    prop.setProperty("BILL_TITLE", "Le tue bollette");
		    prop.setProperty("BILL_TITLE_DESCRIPTION", "Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.");

			SupplyDetailComponent supply = new SupplyDetailComponent(driver);
			HomePageResidentialComponent home = new HomePageResidentialComponent(driver);
			
			By leftMenu=home.leftMenu;
			home.homepageRESIDENTIALMenu(leftMenu);
			
			supply.verifyComponentExistence(supply.gestisciLeOreFree);
			supply.clickComponent(supply.gestisciLeOreFree);
			
			By Addressfields = supply.Addressfields;
			By SupplyDetailFields = supply.SupplyDetailFields;
			By Mostradipiu = supply.Mostradipiu;
						
			supply.lightSupplyAddressDetailsDisplay(Addressfields);
			logger.write("Supply Address Details - Completed");
			
			supply.clickComponent(Mostradipiu);
			logger.write("Bolletino Postale - Verified");
						
			supply.lightSupplyDetailsDisplay(SupplyDetailFields);
			logger.write("Supply Details - Completed");
			
			supply.verifyComponentExistence(supply.RinominaFornitura);
			logger.write("Button RinominaFornitura - Verified");
			
			supply.verifyComponentExistence(supply.VisualizzaBollette);
			logger.write("Button Visualizza Bollette - Verified");
			supply.clickComponent(supply.VisualizzaBollette);
			
			logger.write("Bill Title and Description Verification- Starts");
			supply.VerifyText(supply.billTitle,prop.getProperty("BILL_TITLE"));
			//supply.VerifyText(supply.billTitleDescription,prop.getProperty("BILL_TITLE_DESCRIPTION"));
			logger.write("Bill Title and Description Verification- Ends");
			
			supply.verifyComponentExistence(supply.statoPagamento);
			logger.write("Bill Detail: Stato Pagamento - Verified");
			
			supply.verifyComponentExistence(supply.tipoDiDocumento);
			logger.write("Bill Detail: Tipo Di Documento - Verified");
			
			supply.verifyComponentExistence(supply.anno);
			logger.write("Bill Detail: Anno - Verified");
			
			supply.verifyComponentExistence(supply.fornitura);
			logger.write("Bill Detail: Fornitura - Verified");
			
			supply.clickComponent(supply.servizi);
			logger.write("Area Clienti Enel Energia page- Verified");
			
			logger.write("Servizi per le forniture section for the supply of ELE commodities there are in the order from left to righ- Verification starts");
			supply.serviziPerLeFornitureDetailsDisplay(supply.servizi);
			logger.write("Servizi per le forniture section for the supply of ELE commodities there are in the order from left to righ- Verification Ends");
			
			logger.write("Servizi per le Bollette section for the supply of ELE commodities there are in the order from left to righ- Verification starts");
			supply.serviziPerLeBolletteDetailsDisplay(supply.servizi);
			logger.write("Servizi per le Bollette section for the supply of ELE commodities there are in the order from left to righ- Verification Ends");
			
			logger.write("Servizi per il contratto section for the supply of ELE commodities there are in the order from left to righ- Verification starts");
			supply.serviziPerIlContrattoDetailsDisplay(supply.servizi);
			logger.write("Servizi per il contrattosection for the supply of ELE commodities there are in the order from left to righ- Verification Ends");
					
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	}

			
			
			


