package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.AccountRegistrationComponent;
import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.RegistrationTypeChoiceComponent;
import com.nttdata.qa.enel.components.colla.RegistrazioneComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_37_RegistrazioneRES {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			RegistrazioneComponent reg = new RegistrazioneComponent(driver);
			AccountRegistrationComponent acc = new AccountRegistrationComponent (driver);
			
			logger.write("apertura del portale web Enel di test - Start");
			//Login Page
			LoginPageComponent log = new LoginPageComponent(driver);
			
			prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber2());
			
			acc.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			acc.hanldeFullscreenMessage(acc.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			

			
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			//1
			Thread.sleep(10000);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("Check login page - completed");
			
			//2
			logger.write("Check the user name and password fields - start");
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			logger.write("Check the user name and password fields - completed");
			
			logger.write("Check the Login button - start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			logger.write("Check the Login button - completed");

			By recName = log.RecUserName;
	        log.verifyComponentExistence(recName);
	        By RecPasswd=log.RecUserPasswd;
	        log.verifyComponentExistence(RecPasswd);
	        
	        //log.verifyComponentExistence(log.haiProblemiText);
	        //log.comprareText(log.haiProblemiText, LoginPageComponent.HAI_PROBLEMI_TEXT, true);
        
//	        logger.write("Check the google and facebook  - start");
//	        By signInFacebook=log.FacebookSignIn;
//	        log.verifyComponentExistence(signInFacebook);
//	        By signInGoogle=log.GoogleSignIn;
//	        log.verifyComponentExistence(signInGoogle);
//	        logger.write("Check the google and facebook - completed");
	        
//	        log.verifyComponentExistence(log.nonHaiAccount);
//	        log.comprareText(log.nonHaiAccount, LoginPageComponent.NON_HAI_ACCOUNT, true);
	        
	        logger.write("Check the registrati button  - start");
	        log.verifyComponentExistence(log.registratiButton);
	        logger.write("Check the registrati button  - ends");
	        
	       //4
	        logger.write("Click on button Registrati and verify the data- Starts");
	        log.clickComponent(log.registratiButton);
	        
	        RegistrationTypeChoiceComponent rc = new RegistrationTypeChoiceComponent(driver);
			rc.verifyComponentExistence(rc.welcomeMessageLabel);
			reg.comprareText(rc.welcomeMessageLabel, rc.WelcomeMsg, true);
			rc.verifyComponentExistence(rc.pageHeader);
			reg.comprareText(rc.pageHeader, rc.PageTitle, true);
			rc.verifyComponentExistence(rc.privateSelector);
			rc.verifyComponentExistence(rc.businessSelector);
			//rc.checkContinueButtonIsDisabled();
			Thread.sleep(30000);
			logger.write("Click on button Registrati and verify the data- Ends");
			
			//5
			logger.write("Click on PRIVATO and when CONTINUA button is enabled- Click on CONTINUA- Starts");
			rc.clickComponent(rc.privato);
			rc.clickComponent(rc.continueBtn);
			
			reg.comprareText(acc.regHomePagePath1, acc.REG_HOMEPAGE_PATH1, true);
			reg.comprareText(acc.regHomePagePath2, acc.REG_HOMEPAGE_PATH2, true);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			logger.write("Click on PRIVATO and when CONTINUA button is enabled- Click on CONTINUA- Ends");
			
			//6
	        
	      	logger.write("Verify the link ULTERIORI INFORMAZIONI SUL PROFILO UNICO -->Starts");
	      	reg.verifyComponentExistence(reg.ulterioriInformazioniLink);
	      	logger.write("Verify the link ULTERIORI INFORMAZIONI SUL PROFILO UNICO -->Ends");
	      	
	      	//7
	      	logger.write("Click the link ULTERIORI INFORMAZIONI SUL PROFILO UNICO and verify the data- Starts");
	      	reg.clickComponent(reg.ulterioriInformazioniLink);
			reg.verifyComponentExistence(reg.cosEilProfiloUnico);
			reg.comprareText(acc.cosEilProfiloUnicoHeaing, acc.PROFILO_UNIOC_HEADING, true);
			reg.comprareText(reg.cosEilProfiloUnicoText, reg.CosEilProfiloUnicoText, true);
			logger.write("Click the link ULTERIORI INFORMAZIONI SUL PROFILO UNICO and verify the data- Ends");
			
			//8
			logger.write("Click on Close button and verify the text - Starts");
			reg.clickComponent(reg.cosEilProfiloUnicoClose);
			reg.comprareText(acc.regHomePagePath1, acc.REG_HOMEPAGE_PATH1, true);
			reg.comprareText(acc.regHomePagePath2, acc.REG_HOMEPAGE_PATH2, true);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			logger.write("Click on Close button and verify the text - Ends");
			
			//9
			logger.write("Verify the existence of fields in section credenziali - Starts");
			reg.verifyComponentExistence(reg.credenziali);
			reg.verifyComponentExistence(reg.email);
			reg.verifyComponentExistence(reg.confirmEmail);
			reg.verifyComponentExistence(reg.password);
			reg.verifyComponentExistence(reg.confirmPassword);
			Thread.sleep(5000);
			reg.comprareText(reg.confirmPswText, reg.ConfirmPswText, true);
			logger.write("Verify the existence of fields in section credenziali - Ends");
			
			//10
			logger.write("Verify the existence of fields in section Data Anagrafica - Starts");
			reg.verifyComponentExistence(reg.datiAnagrafici);
			reg.verifyComponentExistence(reg.nome);
			reg.verifyComponentExistence(reg.cognome);
			acc.verifyComponentExistence(acc.cfInputField);
			acc.verifyComponentExistence(acc.calcolaOra);
			
			reg.verifyComponentExistence(reg.condizioniGenerali);
			//reg.verifyComponentExistence(reg.checkBox1Text);
			//reg.verifyComponentExistence(reg.checkBox2Text);
			reg.verifyComponentExistence(reg.campoObbligatorio);
			reg.verifyComponentExistence(reg.registrationButton);
			logger.write("Verify the existence of fields in section Data Anagrafica - Ends");
			
			//11
			logger.write("Click on Condizioni generali dei servizi offerti * and verify the text- Starts");
			reg.jsClickObject(reg.condizioniGenaraliPolicy);
			reg.verifyComponentExistence(reg.condizioniGeneraliTitle);
			reg.comprareText(reg.condizioniGeneraliContent, reg.CondizioniGeneraliContent, true);
			logger.write("Click on Condizioni generali dei servizi offerti * and verify the text- Ends");
			
			//12
			logger.write("Click on close button and verify the text- Starts");
			reg.clickComponent(reg.condizioniGenaraliPolicyClose);
			reg.comprareText(acc.regHomePagePath1, acc.REG_HOMEPAGE_PATH1, true);
			reg.comprareText(acc.regHomePagePath2, acc.REG_HOMEPAGE_PATH2, true);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			logger.write("Click on close button and verify the text- Ends");
			
			//13
			logger.write("Click on Informativa privacy * and verify the text- Starts");
			reg.jsClickObject(reg.informativaPrivacy);
			reg.verifyComponentExistence(reg.informativaPrivacyTitle);
			//reg.comprareText(reg.informativaPrivacyContent, reg.InformativaPrivacyContent, true);
			logger.write("Click on Informativa privacy * and verify the text- Ends");
			
			//14
			logger.write("Click on close button and verify the text- Starts");
			reg.clickComponent(reg.informativaPrivacyClose);
			reg.comprareText(acc.regHomePagePath1, acc.REG_HOMEPAGE_PATH1, true);
			reg.comprareText(acc.regHomePagePath2, acc.REG_HOMEPAGE_PATH2, true);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			logger.write("Click on close button and verify the text- Ends");
			
			//15
			logger.write("Click on calcola ora and verify the data- Starts");
			acc.verifyComponentExistence(acc.c_nome);
			acc.verifyComponentExistence(acc.c_cognome);
			acc.verifyComponentExistence(acc.dataDiNascita);
			acc.verifyComponentExistence(acc.giorno);
			acc.verifyComponentExistence(acc.mese);
			acc.verifyComponentExistence(acc.anno);
			acc.verifyComponentExistence(acc.c_sesso);
			acc.verifyComponentExistence(acc.femmina);
			acc.verifyComponentExistence(acc.maschio);
			acc.verifyComponentExistence(acc.seiNato);
			acc.verifyComponentExistence(acc.si);
			acc.verifyComponentExistence(acc.no);
			acc.verifyComponentExistence(acc.calcolaOra);
			acc.clickComponent(acc.calcolaOra);
			logger.write("Click on calcola ora and verify the data- Ends");
			
			//16
			logger.write("Click on the CALCOLA* without entering data and verify the Campo obbligatorio- Starts");
			acc.clickComponent(acc.calcolaButton);
			acc.verifyComponentExistence(acc.nome_campoObb);
			acc.verifyComponentExistence(acc.cognome_campObb);
			acc.verifyComponentExistence(acc.giorno_camObb);
			acc.verifyComponentExistence(acc.mese_camObb);
			acc.verifyComponentExistence(acc.ano_camObb);
			logger.write("Click on the CALCOLA* without entering data and verify the Campo obbligatorio- Ends");
			
			//17
			logger.write("Click on close button and verify the text- Starts");
			acc.clickComponent(acc.calcola_close);
			reg.comprareText(acc.regHomePagePath1, acc.REG_HOMEPAGE_PATH1, true);
			reg.comprareText(acc.regHomePagePath2, acc.REG_HOMEPAGE_PATH2, true);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			logger.write("Click on close button and verify the text- Ends");
			
			//18
			logger.write("without inserting values in the exposed fields, click on REGISTRATI ALL'AREA RISERVATA-starts");
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.emailError, reg.ErrorMsg, true);
			reg.comprareText(reg.confirmEmailError, reg.ErrorMsg, true);
			reg.comprareText(reg.passwordError, reg.ErrorMsg, true);
			reg.comprareText(reg.confirmPasswordError, reg.ErrorMsg, true);
			reg.comprareText(reg.nomeError, reg.ErrorMsg, true);
			reg.comprareText(reg.cognomeError, reg.ErrorMsg, true);
			//reg.comprareText(reg.cfError, reg.ErrorMsg, true);
			//reg.comprareText(reg.checkBox1Error, reg.ErrorMsg, true);
			//reg.comprareText(reg.checkBox2Error, reg.ErrorMsg, true);
			logger.write("without inserting values in the exposed fields, click on REGISTRATI ALL'AREA RISERVATA-Ends");
			
			//19
			logger.write("Enter incorrect value for indirizzo email - starts");
			prop.setProperty("INVALID_EMAIL", "iii@@@@@");
			reg.enterInputParameters(reg.email, prop.getProperty("INVALID_EMAIL"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidEmailError, reg.InvalidEmailErrorMsg, true);
			logger.write("Enter incorrect value for indirizzo email - Ends");
			
			//20
			logger.write("Enter incorrect value for conferma email - starts");
			reg.enterInputParameters(reg.confirmEmail, prop.getProperty("INVALID_EMAIL"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidConfirmEmailError, reg.InvalidEmailErrorMsg, true);
			logger.write("Enter incorrect value for conferma email - Ends");
			
			//21
			logger.write("Enter correct value for indirizzo email and incorrect value in the conferma email - starts");
			prop.setProperty("EMAIL", "immilezell-1212@yopmail.com");
			reg.enterInputParameters(reg.email, prop.getProperty("EMAIL"));
			reg.enterInputParameters(reg.confirmEmail, prop.getProperty("INVALID_EMAIL"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidConfirmEmailError, reg.InvalidEmailErrorMsg, true);
			reg.enterInputParameters(reg.confirmEmail, prop.getProperty("EMAIL"));
			reg.clickComponent(reg.registrationButton);

			//reg.checkFieldDisplay(reg.emailError);
			//reg.checkFieldDisplay(reg.confirmEmailError);
			logger.write("Enter correct value for indirizzo email and incorrect value in the conferma email - Ends");
			
			//23
			logger.write("Enter incorrect value for password - starts");
			prop.setProperty("INVALID_PSW", "123");
			reg.enterInputParameters(reg.password, prop.getProperty("INVALID_PSW"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidPasswordError, reg.InvalidPswErrorMsg, true);
			logger.write("Enter incorrect value for password - Ends");
			
			//24
			logger.write("Enter incorrect value for confirm password - starts");
			reg.enterInputParameters(reg.confirmPassword, prop.getProperty("INVALID_PSW"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.confirmPasswordError, reg.InvalidPswErrorMsg, true);
			
			//25
			logger.write("Enter correct value for password and incorrect value for confirm password - starts");
			prop.setProperty("PASSWORD", "Password01");
			reg.enterInputParameters(reg.password, prop.getProperty("PASSWORD"));
			reg.enterInputParameters(reg.confirmPassword, prop.getProperty("INVALID_PSW"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.confirmPasswordError, reg.InvalidPswErrorMsg, true);
			logger.write("Enter correct value for password and incorrect value for confirm password - Ends");
			
			//27
			logger.write("Enter the incorrect value in the nome and cognome- Starts");
			prop.setProperty("INVALID_NAME", "123");
			reg.enterInputParameters(reg.nome, prop.getProperty("INVALID_NAME"));
			reg.enterInputParameters(reg.cognome, prop.getProperty("INVALID_NAME"));
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.nomeError, reg.InvalidNomeErrorMsg, true);
			reg.comprareText(reg.cognomeError, reg.InvalidCogNomeErrorMsg, true);
			logger.write("Enter the incorrect value in the nome and cognome- Ends");
			
			//28
			logger.write("Enter incorrect value in the code Fiscale- Starts");
			prop.setProperty("CIVALID_CF", "123");
			reg.enterInputParameters(acc.cfInputField, prop.getProperty("CIVALID_CF"));
			reg.comprareText(acc.cf_errmsg, reg.ErrorMsg, true);
			logger.write("Enter incorrect value in the code Fiscale- Ends");
			
			//22 & 26
			logger.write("Enter Valid user name and password- Starts");
			acc.enterInputtoField(acc.emailInputField, prop.getProperty("WP_USERNAME"));
			acc.enterInputtoField(acc.confirmEmailInputField, prop.getProperty("WP_USERNAME"));
			acc.enterInputtoField(acc.passwordInputField, prop.getProperty("WP_PASSWORD"));
			acc.enterInputtoField(acc.confirmPasswordInputField, prop.getProperty("WP_PASSWORD"));
			logger.write("Enter Valid user name and password- Ends");
			
			//29
			acc.fillInputField(acc.nomeInputField, prop.getProperty("ACCOUNT_FIRSTNAME"));
			acc.fillInputField(acc.cognomeInputField, prop.getProperty("ACCOUNT_LASTNAME"));
			acc.clickComponent(acc.calcolaOra);
			//reg.comprareText(acc.c_nome, prop.getProperty("ACCOUNT_FIRSTNAME"), true);
			//reg.comprareText(acc.c_cognome, prop.getProperty("ACCOUNT_LASTNAME"), true);
			
			//30
			logger.write("Click on close button and verify the text- Starts");
			acc.clickComponent(acc.calcola_close);
			reg.comprareText(acc.regHomePagePath1, acc.REG_HOMEPAGE_PATH1, true);
			reg.comprareText(acc.regHomePagePath2, acc.REG_HOMEPAGE_PATH2, true);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			logger.write("Click on close button and verify the text- Ends");
			
			//31
			logger.write("Enter codice Fiscale and click on the check boxes- Starts");
			//By cf = null;
			acc.scrollComponent(acc.cfInputField);
			//cf = acc.cfInputField;
			acc.fillInputField(acc.cfInputField, prop.getProperty("CF"));
			
			acc.scrollComponent(acc.registratiAllAreaRiservataButton);
			acc.jsClickObject(acc.informativaPrivacyCheckbox);
			acc.jsClickObject(acc.condizioniGeneraliCheckbox);
			
			acc.clickComponent(acc.registratiAllAreaRiservataButton);
			logger.write("Enter codice Fiscale and click on the check boxes- Ends");
			
			//33
			logger.write("Enter invalid mobile number and veirfy the error- Starts");
			prop.setProperty("MOBILE_NUMBER_INVALID","32090901111111111111111111111");
			acc.verifyComponentExistence(acc.mobileNumberInputField);
			acc.fillInputField(acc.mobileNumberInputField, prop.getProperty("MOBILE_NUMBER_INVALID"));
			
			//reg.comprareText(acc.cell_errmsg, acc.CELL_ERR_MSG, true);
			logger.write("Enter invalid mobile number and veirfy the error- Ends");
			
			//34
			logger.write("Enter valid mobile number and veirfy the error- Starts");
			acc.verifyComponentExistence(acc.mobileNumberInputField);
			acc.fillInputField(acc.mobileNumberInputField, prop.getProperty("MOBILE_NUMBER"));
			acc.clickComponent(acc.continuaWithOTPButton);
			logger.write("Enter valid mobile number and veirfy the error- Ends");
			
			//35 & 36
			logger.write("Enter the otp - Starts");
			Thread.sleep(10000);
			String otp = APIService.getOTP(prop.getProperty("WP_USERNAME"), prop.getProperty("MOBILE_NUMBER"));
			acc.verifyComponentExistence(acc.otpHeader);
			acc.fillInputField(acc.otpInputField, otp);
			logger.write("Enter the otp - Ends");
			
			//37
			logger.write("Click on continua and verify the page navigation- Starts");
			acc.clickComponent(acc.confermaOTPButton);
			
			acc.verifyComponentExistence(acc.thanksForRegistering);
			reg.comprareText(acc.thanksForRegSecHeading, acc.THANKS_FOR_REG_SEC_HEADING, true);
			reg.comprareText(acc.thanksForRegistering, acc.THANKS_FOR_REGISTERING, true);
			acc.verifyComponentExistence(acc.emailMessage);
			reg.comprareText(acc.emailMessage2, acc.EMAIL_MESSAGE2, true);
			acc.clickComponent(acc.thanksForRegisteringButton);
			logger.write("Click on continua and verify the page navigation- Ends");
			
			Thread.sleep(30000);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {

				prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
