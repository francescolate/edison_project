package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.Privato211ACRDisattivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_211_ACR_Disattivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato211ACRDisattivazioneComponent dzcp = new Privato211ACRDisattivazioneComponent(driver);
			
			dzcp.verifyComponentVisibility(dzcp.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = dzcp.homePageCentralText;
			dzcp.verifyComponentExistence(homePage);
			dzcp.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
			
			By homepageSubheading = dzcp.homePageCentralSubText;
			dzcp.verifyComponentExistence(homepageSubheading);
			dzcp.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = dzcp.sectionTitle;
			dzcp.verifyComponentExistence(sectionTitle);
			dzcp.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = dzcp.sectionTitleSubText;
			dzcp.verifyComponentExistence(sectionTitleSubText);
			dzcp.VerifyText(sectionTitleSubText, dzcp.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verify and Click on left menu item 'Dettaglio fornitura' - Start");
			By dettaglioFornituraButton = dzcp.dettaglioFornituraButton;
			dzcp.verifyComponentExistence(dettaglioFornituraButton);
			dzcp.clickComponent(dettaglioFornituraButton);
			logger.write("Verify and Click on left menu item 'Dettaglio fornitura' - Completed");
			Thread.sleep(20000);
			
			dzcp.verifyComponentVisibility(dzcp.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = dzcp.serviziPage;
			dzcp.verifyComponentExistence(serviziHomePage);
			dzcp.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Start");
			By serviziPageSectionTitle= dzcp.serviziSectionTitle;
			dzcp.verifyComponentExistence(serviziPageSectionTitle);
			dzcp.VerifyText(serviziPageSectionTitle, prop.getProperty("SERVIZI_HOMEPAGE_SECTIONTITLE"));
			
			By serviziPage_disattivazioneFornituraItem = dzcp.disattivazioneFornituraItem;
			dzcp.verifyComponentExistence(serviziPage_disattivazioneFornituraItem);
			dzcp.clickComponent(serviziPage_disattivazioneFornituraItem);
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Completed");
			Thread.sleep(30000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornitura);
			
			logger.write("User is navigated to home page of DISATTIVAZIONE_FORNITURA_PAGE - Start");
			By disattivazioneFornituraPageHeading = dzcp.disattivazioneFornitura;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageHeading);
			dzcp.VerifyText(disattivazioneFornituraPageHeading, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_HEADING"));
			logger.write("User is navigated to home page of DISATTIVAZIONE_FORNITURA_PAGE - Completed");
			
			logger.write("Checking and validating the presence of 'ENEL LOGO','LITTLE_MAN_ICON', 'USERNAME' - Start");
			By enelLogo = dzcp.enelLogo;
			dzcp.verifyComponentExistence(enelLogo);
			
			By littleManIcon = dzcp.littleManIcon;
			dzcp.verifyComponentExistence(littleManIcon);
			
			By loggedInUserName = dzcp.loggedInUserName;
			dzcp.verifyComponentExistence(loggedInUserName);
			logger.write("Checking and validating the presence of 'ENEL LOGO','LITTLE_MAN_ICON', 'USERNAME' - Completed");
			
			logger.write("Validate the presence of menu on the left side of page - Start");
			dzcp.leftMenuItemsDisplay(dzcp.serviziSelect);
			logger.write("Validate the presence of menu on the left side of page - Completed");
			
			logger.write("Validating if serviziSelect element is highlighted -- Start");
			dzcp.verifyElementIfHiglightedByCompare(dzcp.serviziSelect);
			logger.write("Validating if serviziSelect element is highlighted -- Start");
			
			
			logger.write("Check presence of Fotter Section -- Start");
			dzcp.verifyFooterVisibility();
			logger.write("Check presence of Fotter Section -- Completed");
			
			logger.write("Verify and click on PROSEGUI_CON_LA_DISATTIVAZIONE_button - Start");
			By PROSEGUI_CON_LA_DISATTIVAZIONE_button = dzcp.PROSEGUI_CON_LA_DISATTIVAZIONE_button;
			dzcp.verifyComponentExistence(PROSEGUI_CON_LA_DISATTIVAZIONE_button);
			dzcp.clickComponent(PROSEGUI_CON_LA_DISATTIVAZIONE_button);
			logger.write("Verify and click on PROSEGUI_CON_LA_DISATTIVAZIONE_button - Completed");
			Thread.sleep(30000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornituraPageTitle);
			
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Start");
			By disattivazioneFornituraPageTitle = dzcp.disattivazioneFornituraPageTitle;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageTitle);
			dzcp.VerifyText(disattivazioneFornituraPageTitle, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_TITLE"));
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Completed");
			
			logger.write("Validating the LIST OF SUPPLY displayed in the page  - Start");
			dzcp.gasSupplyDetailsDisplay(dzcp.GasDetails);
			Thread.sleep(3000);
			dzcp.lightSupplyDetailsDisplay(dzcp.LightDetails);
			logger.write("Validating the LIST OF SUPPLY displayed in the page  - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}