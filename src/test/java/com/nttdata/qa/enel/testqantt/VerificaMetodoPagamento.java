package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaMetodoPagamento {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			String query=Costanti.query_id_104;
			String pod=prop.getProperty("POD");
			query=query.replace("@@POD@@", pod);
			
			
			
			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);
			Map<String,String> result=wb.EseguiQuery(query).get(1);
			
			
			String modalitaPagamentoPagamento=result.get("NE__Billing_Profile__c.NE__Payment__c");
			String ibanFornitura=result.get("NE__Billing_Profile__c.NE__Iban__c");
			
			String ibanAtteso=prop.getProperty("ATT_SDD_IBAN");
			String modalitaPagamentoAttesa="RID";
			
			
			if(!ibanAtteso.contentEquals(ibanFornitura))
			{
				throw new Exception(String.format("IBAN presente su fornitura non corretto -  Presente su fornitura: %s - Iban inserito per lavorazione: %s "
						,ibanFornitura,ibanAtteso));
			}
			
			if(!modalitaPagamentoAttesa.contentEquals(modalitaPagamentoPagamento))
			{
				
				throw new Exception(String.format("Modalità di pagamento errata fornitura -  Presente su fornitura: %s - valore atteso: %s ",modalitaPagamentoPagamento,modalitaPagamentoAttesa));
			}
			
				
	
		

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}
