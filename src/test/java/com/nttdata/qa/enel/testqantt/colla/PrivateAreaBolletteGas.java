package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CheckPrivateAreaComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaBolletteGas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaBolletteComponent pabc = new PrivateAreaBolletteComponent(driver);
		  //  prop.setProperty("SERVIZI_BOLLETTE", "Bolletta Web;Pagamento Online;Addebito Diretto;Rettifica Bolletta;Rateizzazione;Bolletta di Sintesi o di Dettaglio;Duplicato Bolletta");
		    prop.setProperty("SERVIZI_BOLLETTE", "Bolletta Web;Pagamento Online;Addebito Diretto;Rettifica Bolletta;Rateizzazione;Bolletta di Sintesi o di Dettaglio;Duplicato Bolletta;Invio Attestazione di Pagamento");
		    
			By by = null;

			if(prop.containsKey("ACTION_TYPE"))
				if(prop.get("ACTION_TYPE").equals("MENU_ITEM_SERVIZI"))
					by = pabc.serviziMenuItem; 
				else if(prop.get("ACTION_TYPE").equals("DETTAGLIO_FORNITURA_BUTTON"))
					by = pabc.buttonDettaglioFornituraUpdated;
				else
					by = null;
			else
				by = pabc.buttonDettaglioFornituraUpdated;
					
			logger.write("checking and clicking the button related to the active supply - START");
			pabc.verifyComponentExistence(by);
			pabc.clickComponent(by);
			logger.write("checking and clicking the button related to the active supply  - COMPLETED");
			
			logger.write("scrolling to Servizi per le bollette - START");
			pabc.scrollComponent(pabc.serviziPerLeBolletteHeader);
			logger.write("scroll - COMPLETED");
			
			logger.write("checking the text contained within the header of the page - START");
			pabc.comprareText(pabc.serviziPerLeBolletteHeaderText, pabc.BOLLETTE_HEADER_TEXT_GAS, true);
			logger.write("header text checking - COMPLETED");
			
			logger.write("scrolling to Servizi per le bollette - START");
			pabc.scrollComponent(pabc.tilesHeader);
			logger.write("scroll - COMPLETED");
			
			logger.write("checking tiles order - START");
			Thread.sleep(10000);
			//pabc.verifyMenuItemsPresenceAndOrder(prop.getProperty("SERVIZI_BOLLETTE"), pabc.TILE_HEADER);
			logger.write("checking tiles order - COMPLETED");
			
			logger.write("clicking on Bolletta di Sintesi o di Dettaglio - START");
			pabc.clickComponent(pabc.bollettaSintesi);
			logger.write("clicking on Bolletta di Sintesi o di Dettaglio - COMPLETED");
			
			logger.write("checking Bolletta di Sintesi o di Dettaglio Header Text - START");
			pabc.verifyComponentExistence(pabc.bollettaSintesiHeaderText);
			pabc.comprareText(pabc.bollettaSintesiHeaderText, pabc.BOLLETTA_SINTESI_HEADER_TEXT, true);
			logger.write("checking Bolletta di Sintesi o di Dettaglio Header Text - COMPLETED");
			
			/*logger.write("checking customer supply address - START");
			pabc.verifyComponentExistence(pabc.accountAddressGas);
			//pabc.comprareText(pabc.accountAddressGas, prop.getProperty("SUPPLY_ADDRESS"), true);
			logger.write("checking customer supply address - COMPLETED");
			
			logger.write("checking customer code - START");
			pabc.verifyComponentExistence(pabc.customerNumberGas);
			//pabc.comprareText(pabc.customerNumberGas, prop.getProperty("CUSTOMER_CODE"), true);
			logger.write("checking customer code - COMPLETED");
			
			logger.write("checking Scegli bolletta di dettaglio button - START");
			by = By.xpath(pabc.tipologiaBollettaButton.replace("$tipologiaBollettaButton$", prop.getProperty("TIPOLOGIA_BOLLETTA_BUTTON")));
			pabc.verifyComponentExistence(by);
			logger.write("checking Scegli bolletta di dettaglio button - COMPLETED");*/
			
			logger.write("checking e click Button Esci - START");
			pabc.verifyComponentExistence(pabc.buttonEsci);
			pabc.clickComponent(pabc.buttonEsci);
			logger.write("checking e click Button Esci - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
