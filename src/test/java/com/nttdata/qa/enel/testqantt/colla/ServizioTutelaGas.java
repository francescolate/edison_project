package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.UsefulLinksItaComponent;
import com.nttdata.qa.enel.components.colla.HomePageHeaderComponent;
import com.nttdata.qa.enel.components.colla.UsefulLinksEngComponent;
import com.nttdata.qa.enel.components.colla.TutelaPageComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ServizioTutelaGas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			String BASE_LINK = prop.getProperty("LINK");
			Thread.sleep(5000);
			prop.setProperty("TUTELA_PAGE_LINK", BASE_LINK + "en/supporto/faq/offerta-servizio-tutela-gas");

			//System.out.println("Verifying useful links section - ITA");
			logger.write("Verifying useful links section - ITA - Start");
			UsefulLinksItaComponent linksSection = new UsefulLinksItaComponent(driver);
			linksSection.verifySectionStrings();
			logger.write("Verifying useful links section - ITA - Completed");
			
			//System.out.println("Verifying useful links section - ENG");
			logger.write("Verifying useful links section - ENG - Start");
			HomePageHeaderComponent homePage = new HomePageHeaderComponent(driver);
			homePage.clickComponent(homePage.hamburgerMenu);
			homePage.verifyComponentExistence(homePage.englishSelectionButton);
			homePage.clickComponent(homePage.englishSelectionButton);
			UsefulLinksEngComponent engLinksSection = new UsefulLinksEngComponent(driver);
			engLinksSection.verifySectionStrings();
			logger.write("Verifying useful links section - ENG - Completed");
			
			//System.out.println("Opening Tutela Gas page");
			logger.write("Opening Tutela Gas page - Start");
			engLinksSection.clickComponent(engLinksSection.tutelaLink);
			TutelaPageComponent tutelaPage = new TutelaPageComponent(driver);
			tutelaPage.confirmUrl(prop.getProperty("TUTELA_PAGE_LINK"));
			tutelaPage.verifyPagePresentation();
			logger.write("Opening Tutela Gas page - Completed");
			
			//System.out.println("Validating bar");
			logger.write("Validating bar - Start");
			tutelaPage.validateBar();
			logger.write("Validating bar - Completed");
			
			//System.out.println("Validating page strings and elements");
			logger.write("Validating page strings and elements - Start");
			tutelaPage.validatePageContent();
			logger.write("Validating page strings and elements - Completed");
			
			//System.out.println("Checking empty field error");
			logger.write("Checking empty field error - Start");
			tutelaPage.checkEmptyFieldError();
			logger.write("Checking empty field error - Completed");
			
			//System.out.println("Selecting city: Roma");
			logger.write("Selecting city: Roma - Start");
			tutelaPage.selectCity(tutelaPage.romaMenuItem , prop.getProperty("LOC_ROMA"));
			logger.write("Selecting city: Roma - Completed");
			
			//System.out.println("Checking text for: Roma");
			logger.write("Checking text for: Roma - Start");
			tutelaPage.checkTextForCity();
			logger.write("Checking text for: Roma - Completed");
			
			//System.out.println("Checking scaglioni consumo: Roma");
			logger.write("Checking scaglioni consumo: Roma - Start");
			tutelaPage.checkScaglioniConsumo();
			logger.write("Checking scaglioni consumo: Roma - Completed");
			
			//System.out.println("Checking componente tariffaria: Roma");
			logger.write("Checking componente tariffaria: Roma - Start");
			tutelaPage.checkComponenteTariffaria(prop.getProperty("LOC_ROMA"));
			logger.write("Checking componente tariffaria: Roma - Completed");
			
			//System.out.println("Checking altre componenti: Roma"); - i valori possono cambiare
//			logger.write("Checking altre componenti: Roma - Start");
//			tutelaPage.checkAltreComponenti(prop.getProperty("LOC_ROMA"));
//			logger.write("Checking altre componenti: Roma - Completed");
			
			//System.out.println("Checking nota: Roma");
			logger.write("Checking nota: Roma - Start");
			tutelaPage.checkNota();
			logger.write("Checking nota: Roma - Completed");
			
			//System.out.println("Opening ARERA site (1)");
			logger.write("Opening ARERA site (1) - Start");
			tutelaPage.openAreraSite();
			logger.write("Opening ARERA site (1) - completed");
			
			//System.out.println("Verifying economic conditions text");
			logger.write("Verifying economic conditions text - Start");
			driver.navigate().back();
			driver.navigate().refresh();
			tutelaPage.checkEconomicConditions();
			logger.write("Verifying economic conditions text - Completed");
			
			//System.out.println("Selecting city: Milano");
			logger.write("Selecting city: Milano - Start");
			tutelaPage.selectCity(tutelaPage.milanoMenuItem , prop.getProperty("LOC_MILANO"));
			logger.write("Selecting city: Milano - Completed");
			
			//System.out.println("Checking text for: Milano");
			logger.write("Checking text for: Milano - Start");
			tutelaPage.checkTextForCity();
			logger.write("Checking text for: Milano - Completed");
			
			//System.out.println("Checking scaglioni consumo: Milano");
			logger.write("Checking scaglioni consumo: Milano - Start");
			//tutelaPage.checkScaglioniConsumo();
			logger.write("Checking scaglioni consumo: Milano - Completed");
			
			//System.out.println("Checking componente tariffaria: Milano");
			logger.write("Checking componente tariffaria: Milano - Start");
			//tutelaPage.checkComponenteTariffaria(prop.getProperty("LOC_MILANO"));
			logger.write("Checking componente tariffaria: Milano - Completed");
			
			//System.out.println("Checking altre componenti: Milano");
			logger.write("Checking altre componenti: Milano - Start");
			//tutelaPage.checkAltreComponenti(prop.getProperty("LOC_MILANO"));
			logger.write("Checking altre componenti: Milano - Completed");
			
			//System.out.println("Checking nota: Milano");
			logger.write("Checking nota: Milano - Start");
			tutelaPage.checkNota();
			logger.write("Checking nota: Milano - Completed");
			
			//System.out.println("Opening ARERA site (2)");
			logger.write("Opening ARERA site (2) - Start");
			tutelaPage.openAreraSite();
			logger.write("Opening ARERA site (2) - completed");
			
			//System.out.println("Verifying Price text");
			logger.write("Verifying Price text - Start");
			driver.navigate().back();
			driver.navigate().refresh();
			tutelaPage.checkPriceElements();
			logger.write("Verifying Price text - Completed");
			
			//System.out.println("Selecting city: Napoli");
			logger.write("Selecting city: Napoli - Start");
			tutelaPage.selectCity(tutelaPage.napoliMenuItem , prop.getProperty("LOC_NAPOLI"));
			logger.write("Selecting city: Napoli - Completed");
			
			//System.out.println("Checking text for: Napoli");
			logger.write("Checking text for: Napoli - Start");
			tutelaPage.checkTextForCity();
			logger.write("Checking text for: Napoli - Completed");
			
			//System.out.println("Checking scaglioni consumo: Napoli");
			logger.write("Checking scaglioni consumo: Napoli - Start");
			tutelaPage.checkScaglioniConsumo();
			logger.write("Checking scaglioni consumo: Napoli - Completed");
			
			//System.out.println("Checking componente tariffaria: Napoli");
			logger.write("Checking componente tariffaria: Napoli - Start");
			//tutelaPage.checkComponenteTariffaria(prop.getProperty("LOC_NAPOLI"));
			logger.write("Checking componente tariffaria: Napoli - Completed");
			
			//System.out.println("Checking altre componenti: Napoli");
			logger.write("Checking altre componenti: Napoli - Start");
			//tutelaPage.checkAltreComponenti(prop.getProperty("LOC_NAPOLI"));
			logger.write("Checking altre componenti: Napoli - Completed");
			
			//System.out.println("Checking nota: Napoli");
			logger.write("Checking nota: Napoli - Start");
			tutelaPage.checkNota();
			logger.write("Checking nota: Napoli - Completed");
			
			//System.out.println("Opening ARERA site (3)");
			logger.write("Opening ARERA site (3) - Start");
			tutelaPage.openAreraSite();
			logger.write("Opening ARERA site (3) - completed");
			
			//System.out.println("Verifying Documents elements");
			logger.write("Verifying Documents elements - Start");
			driver.navigate().back();
			driver.navigate().refresh();
			tutelaPage.checkDocumentsElements();
			logger.write("Verifying Documents elements - Completed");
			
			//System.out.println("Checking pdf links");
			logger.write("Checking pdf links - Start");
			tutelaPage.checkPdfLinks();
			logger.write("Checking pdf links - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
