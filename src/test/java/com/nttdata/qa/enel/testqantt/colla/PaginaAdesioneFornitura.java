package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PaginaAdesioneFornituraComponent;
import com.nttdata.qa.enel.components.colla.PaginaAdesioneInserisciDatiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PaginaAdesioneFornitura {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            
			logger.write("Selezionare CAMBIO FORNITORE ed inserire un cap valido tipo 00178, premere il tasto 'Prosegui' - Start");
			PaginaAdesioneInserisciDatiComponent inserisci=new PaginaAdesioneInserisciDatiComponent(driver);
			/*By frame=inserisci.frame;
			WebElement frameToSw = driver.findElement(frame);
			driver.switchTo().frame(frameToSw);*/
			
			
			PaginaAdesioneFornituraComponent fornitura=new PaginaAdesioneFornituraComponent(driver);
			By campo_Cap=fornitura.campoCap;
			fornitura.verifyComponentExistence(campo_Cap);
			
			By checkBox_CambioFornitore=fornitura.checkBoxCambioFornitore;
			fornitura.verifyComponentExistence(checkBox_CambioFornitore);
			fornitura.scrollComponent(checkBox_CambioFornitore);
			fornitura.click(checkBox_CambioFornitore);
			fornitura.enterValueInInputBox(campo_Cap, prop.getProperty("CAP"));
			TimeUnit.SECONDS.sleep(1);
			fornitura.inserisciCap(prop.getProperty("CAP"));
			TimeUnit.SECONDS.sleep(1);
			
			//manca il pulsante prosegui relativo allo step 24 della tl per cui non e' possibile continuare con la registrazione
			
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
