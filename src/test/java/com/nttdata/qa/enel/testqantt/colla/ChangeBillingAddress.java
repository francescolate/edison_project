package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangeBillingAddress {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			prop.setProperty("RETURN_VALUE", "OK");
			
			//Login Page
			
			SuplyPrivateDetailComponent cba = new SuplyPrivateDetailComponent(driver);
			
			logger.write("Verify and click the Servizi link - Start");
			cba.clickComponent(cba.ServiziLink);
			logger.write("Verify and click the Servizi link - Start");
			
			
			logger.write("Click on Modiffica link - Start");
			cba.clickComponent(cba.ModifficaIndrizzolink);
			logger.write("Click on Modiffica link - Complete");
			
			logger.write("Click on the checkbox  - Start");
			Thread.sleep(20000);
			cba.jsClickComponent(cba.ModificaCheckbox);
			Thread.sleep(1000);
			cba.jsClickComponent(cba.ModificaButton);
			logger.write("Click on the checkbox - Complete");
			
			cba.verifyComponentExistence(cba.CittaInputField176);
			//cba.verifyComponentExistence(cba.ScalaLable);
			//cba.verifyComponentExistence(cba.PianoLabel);
			//cba.verifyComponentExistence(cba.InternoLabel);
			//cba.verifyComponentExistence(cba.CaplabelNew);
			//cba.verifyComponentExistence(cba.PressoLabel);
			
			cba.enterLoginParameters(cba.CittaInputField176, prop.getProperty("CITTAINPUTVALUE"));
			cba.enterLoginParameters(cba.IndrizzoInput176, prop.getProperty("INDRIZZOINPUTVALUE"));
			cba.enterLoginParameters(cba.NumercoCivicInput176, prop.getProperty("NUMERCOCIVIINPUTVALUE"));
//			cba.clickComponent(cba.ContinuaBtn);
//			
//			cba.verifyComponentExistence(cba.AttenzionePopup);
//			cba.verifyComponentExistence(cba.AttenzionePopupText);
//			cba.verifyComponentExistence(cba.AttenzionePopupClose);
//			cba.clickComponent(cba.AttenzionePopupClose);
//			
//			cba.verifyComponentExistence(cba.DropdownBivo);
//			cba.clickComponent(cba.DropdownBivo);
//			cba.verifyComponentExistence(cba.CapInput);
//			cba.enterLoginParameters(cba.CapInput, prop.getProperty("CAPINPUTVALUE"));
//			cba.verifyComponentExistence(cba.CapDropdownValue);
//			cba.clickComponent(cba.CapDropdownValue);
//			cba.clickComponent(cba.ContinuaBtn);
//			
//			cba.verifyComponentExistence(cba.IndrizzoInputAfterWarn);
//			cba.enterLoginParameters(cba.IndrizzoInputAfterWarn, prop.getProperty("INDRIZZOINPUTVALUE"));
//			cba.enterLoginParameters(cba.NumercoCivicInput, prop.getProperty("NUMERCOCIVIINPUTVALUE"));
			cba.clickComponent(cba.ContinuaBtn);
			
			
			/*
			cba.enterLoginParameters(cba.ScalaInput, prop.getProperty("ScalaInputValue"));
			cba.enterLoginParameters(cba.PianoInput, prop.getProperty("PianoInputValue"));
			cba.enterLoginParameters(cba.InternoInput, prop.getProperty("InternoInputValue"));
			
			cba.clickComponent(cba.ContinuaBtn);
			
			*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		

	}

}
