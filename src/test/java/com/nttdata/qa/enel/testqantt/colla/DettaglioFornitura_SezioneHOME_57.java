package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioFornitura_SezioneHOME_57 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			HomeComponent home = new HomeComponent(driver);
			
			logger.write("Check for Residential Menu - Start");
			
			home.checkForResidentialMenu(home.RESIDENTIALMENU);
						
			logger.write("Check for Residential Menu - Completed");	
			
			home.clickComponent(home.services);
			
			ServicesComponent sc = new ServicesComponent(driver);
			
			sc.verifyComponentExistence(sc.serviceTitle);
			
			sc.clickComponent(sc.haiBisogno);
			
			sc.comprareText(sc.haiBisognoExpansionText,sc.HAI_BISOGNO_EXPANDING_TEXT,true);
			
			sc.clickComponent(sc.cliccaSulRiquadro);
			
			sc.verifyComponentExistence(sc.reasonsTitle);
			
			sc.checkLiveChatReason(sc.LIVECHATREASONS);
			
			sc.clickComponent(sc.bolletaLiveChat);
			
			sc.checkBolletaSection(sc.bolletaSection);
			
			sc.clickComponent(sc.infoSuvociinBolletta);
			
			sc.verifyComponentExistence(sc.avviaChat);
			
			sc.clickComponent(sc.avviaChat);
			
			sc.verifyComponentExistence(sc.esci);
			
			sc.comprareText(sc.message1, sc.Message1, true);
			
			sc.clickComponent(sc.esci);
			
			sc.verifyComponentExistence(sc.haiBisogno);
			/*sc.switchFrame(sc.frameContainer);
			
			sc.switchFrame(sc.mainFrame);
			
			sc.switchFrame(sc.chatFrame);
								
			sc.checkLoadingMessage(sc.loadingChat,sc.loadingMessage);
						
			sc.verifyComponentExistence(sc.chatText);
			
			sc.comprareText(sc.chatText, sc.chatMessage, true);
			
			sc.switchToDefault();*/
			
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
