package com.nttdata.qa.enel.testqantt;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificheRichiestaPPDopoPreventivoEVO {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    driver.switchTo().defaultContent();
		  //  TimeUnit.SECONDS.sleep(5);
	        logger.write("Refresh pagina SFDC");
            driver.navigate().refresh();
		    VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
		    logger.write("Recupero valore id richiesta - Start");
//			System.out.println("Recupero valore id richiesta");
			Thread.currentThread().sleep(30000); 
		    verifica.pressJavascript(verifica.elementiRichiestaSection);
			Thread.currentThread().sleep(10000); 
		    verifica.pressJavascript(verifica.elementiOrdineSection);
//			System.out.println("Recupero OI richiesta");
		    String id = verifica.recuperaOIRichiesta();
//		    elementiOrdineSection
	        prop.setProperty("OI_RICHIESTA", id);
//			System.out.println("Valore OI richiesta: "+prop.getProperty("OI_RICHIESTA"));
	        logger.write("Recupero valore id richiesta - Completed");
	        logger.write("Verifica Stati Richiesta dopo Preventivo R2D - Start");
	        verifica.verificaStatiRichiestaPP(prop.getProperty("NUMERO_RICHIESTA"), prop.getProperty("TIPO"),  
	        		prop.getProperty("STATO_RICHIESTA"), prop.getProperty("SOTTO_STATO_RICHIESTA"),prop.getProperty("STATO_ELEMENTO_RICHIESTA"));
			logger.write("Verifica Stati Richiesta dopo Preventivo R2D - Completed");
			logger.write("Verifica Stati Ordine - Start");
	        verifica.verificaStatiOrdinePP(prop.getProperty("STATO_ELEMENTO_ORDINE"), prop.getProperty("TIPO_ORDINE"),  
	        		prop.getProperty("STATO_POD_ORDINE"), prop.getProperty("STATO_R2D_ORDINE"), prop.getProperty("COMMODITY_ORDINE","N.D."), 
	        		prop.getProperty("STATO_SAP_SD_ORDINE"), prop.getProperty("STATO_SAP_ISU_ORDINE"), 
	        		prop.getProperty("STATO_SEMPRE_ORDINE"), prop.getProperty("PRATICA_ORDINE"),prop.getProperty("STATO_PRODOTTO"));
					System.out.println("Verifica Stati Ordine - OK");
			logger.write("Verifica Stati Ordine - Completed");
	        
	        prop.setProperty("RETURN_VALUE", "OK");
	        
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
