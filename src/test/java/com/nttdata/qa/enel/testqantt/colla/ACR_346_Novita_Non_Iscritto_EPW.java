package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.NovitaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_346_Novita_Non_Iscritto_EPW {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String currentHandle ="";
		
		try {		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			logger.write("Click on novita -- Start");
			hm.checkForErrorPopup();
			hm.clickComponent(hm.news);
			logger.write("Click on novita -- Completed");

			NovitaComponent nc = new NovitaComponent(driver);
			logger.write("Verifi novita page title and Subtext -- Start");
			Thread.sleep(20000);
			nc.verifyComponentExistence(nc.pageTitle);
			nc.comprareText(nc.titleSubText, nc.TitleSubText, true);
			logger.write("Verifi novita page title and Subtext -- Completed");
			nc.verifyComponentExistence(nc.comeFunzionaBanner);
			nc.comprareText(nc.comeFunzionaBanner, nc.ComeFunzionaBanner, true);
			nc.verifyComponentExistence(nc.scopriDiPiuComeFunziona);
			
			logger.write("Click on Come Funziona Banner and verify the page navigation details-- Start");
			nc.clickComponent(nc.scopriDiPiuComeFunziona);
			Set <String> windows = driver.getWindowHandles();
			 currentHandle = driver.getWindowHandle();			
			for (String winHandle : windows) {
			    driver.switchTo().window(winHandle);
			 }
			nc.verifyComponentExistence(nc.comeFunzionaBannerTitle);
			nc.checkURLAfterRedirection(nc.comeFunzionaBannerUrl);
//			nc.comprareText(nc.comeFunzionaBannerPath, nc.ComeFunzionaBannerPath, true);
			nc.verifyComponentExistence(nc.comeFunzionaBannerTitle);
			driver.close();
			driver.switchTo().window(currentHandle);
			logger.write("Click on Come Funziona Banner and verify the page navigation details-- Completed");

//			logger.write("Verify Banners details on novita page-- Start");
//			nc.comprareText(nc.scopriDiPiuSuBanner, nc.ScopriDiPiuSuBanner, true);
//			nc.comprareText(nc.scaricaIappDiBanner, nc.ScaricaIappDiBanner, true);
//			nc.comprareText(nc.primaGasBanner, nc.PrimaGasBanner, true);
//			nc.comprareText(nc.conIappufirstBanner, nc.ConIappufirstBanner, true);
//			logger.write("Verify Banners details  on novita page-- Completed");
//			
//			logger.write("Click on Scopri Di Piu Su Banner and verify the page navigation details-- Start");
//			nc.clickComponent(nc.scopriDiPiuSuBanner);
//			Set <String> windows1 = driver.getWindowHandles();
//			currentHandle = driver.getWindowHandle();			
//			for (String winHandle : windows1) {
//			    driver.switchTo().window(winHandle);
//			 }
//			nc.checkURLAfterRedirection(nc.scopriDiPiuSuBannerUrl);
//			driver.close();
//			driver.switchTo().window(currentHandle);
//			logger.write("Click on Scopri Di Piu Su Banner and verify the page navigation details-- Completed");

			logger.write("Click on Scarica I'app Di Banner and verify the page navigation details-- Start");
			nc.clickComponent(nc.scaricaIappDiBanner);
			Set <String> windows2 = driver.getWindowHandles();
			currentHandle = driver.getWindowHandle();			
			for (String winHandle : windows2) {
			    driver.switchTo().window(winHandle);
			 }
			nc.verifyComponentExistence(nc.scaricaIappDiBannerTitle);
			nc.checkURLAfterRedirection(nc.scaricaIappDiBannerUrl);
//			nc.comprareText(nc.scaricaIappDiBannerPath, nc.ScaricaIappDiBannerPath, true);
			nc.comprareText(nc.scaricaIappDiBannerTitle, nc.ScaricaIappDiBannerTitle, true);
			nc.comprareText(nc.scaricaIappDiBannerTitleSubText, nc.ScaricaIappDiBannerTitleSubText, true);
			driver.close();
			driver.switchTo().window(currentHandle);
			logger.write("Click on Scarica I'app Di Banner and verify the page navigation details-- Completed");

			logger.write("Click on Con I'appu First Banner and verify the page navigation details-- Start");
			nc.clickComponent(nc.conIappufirstBanner);
			Set <String> windows3 = driver.getWindowHandles();
			currentHandle = driver.getWindowHandle();			
			for (String winHandle : windows3) {
			    driver.switchTo().window(winHandle);
			 }
			nc.checkURLAfterRedirection(nc.conIappufirstBannerUrl);
			logger.write("Click on Con I'appu First Banner and verify the page navigation details-- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
		
}
