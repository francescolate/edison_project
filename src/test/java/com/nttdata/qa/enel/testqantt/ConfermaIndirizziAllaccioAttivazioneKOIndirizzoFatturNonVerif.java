package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class ConfermaIndirizziAllaccioAttivazioneKOIndirizzoFatturNonVerif {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
            
            String fornitura_indirizzo=prop.getProperty("CLIENTE_FORNISCE_INDIRIZZO");
        

            logger.write("Conferma Indirizzo Residenza/Sede Legale - Start");
            if(fornitura_indirizzo.compareTo("NO")==0){
            	compila.confermaIndirizzo("Indirizzo di Residenza/Sede Legale");
            }
            else{
            	compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
            }
            logger.write("Conferma Indirizzo Residenza/Sede Legale - Completed");

            logger.write("Compila Indirizzo di Fatturazione - Start");
            
            // compila indirizzo di fatturazione e premi verifica 
            compila.compileAddressInfo(prop.getProperty("PROVINCIA"), compila.inputProvinciaIndFattur,
					prop.getProperty("CITTA"), compila.inputComuneIndFattur, prop.getProperty("INDIRIZZOFATTURAZIONE"),
					compila.inputIndirizzoFatturazione, prop.getProperty("CIVICOFATTURAZIONE"), compila.inputCivicoIndFatturazione,
					compila.buttonVerificaIndirizzoFatturazione);
            
            // verifica messaggio di errore   
            compila.verificaMessaggioErroreIndirizzoFatturazioneNonVerificato(compila.indirizzoFattNonVerificatoObjTesto, compila.indirizzoFattNonVerifTestoAtteso);
            
            // Forza Indirizzo 
            compila.ForzaIndirizzoNonVerificato(prop.getProperty("CIVICOFATTURAZIONE"), prop.getProperty("CAP"));
            logger.write("Compila Indirizzo di Fatturazione - Completed");
   
            
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
