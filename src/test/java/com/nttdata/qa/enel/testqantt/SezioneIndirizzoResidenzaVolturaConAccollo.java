package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class SezioneIndirizzoResidenzaVolturaConAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
		
				CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);

				
				if (Costanti.statusUbiest.contentEquals("OFF")) {
					if (offer.verifyClickability(offer.buttonConfermaIndirizzoRes)) {
						offer.clickComponent(offer.buttonConfermaIndirizzoRes);
						gestione.checkSpinnersSFDC();
					} else if (offer.verifyClickability(offer.buttonVerificaIndirizzoRes)) {
						offer.clickComponent(offer.buttonVerificaIndirizzoRes);
						gestione.checkSpinnersSFDC();
						// forza indirizzo 
						compila.ForzaIndirizzoResidenza(prop.getProperty("CAP"));
						offer.clickComponent(offer.buttonConfermaIndirizzoRes);
					}
				} else {
				
				if (offer.verifyClickability(offer.buttonConfermaIndirizzoRes)) {
					offer.clickComponent(offer.buttonConfermaIndirizzoRes);
				} 
				else{
					String fornitura_indirizzo=prop.getProperty("CLIENTE_FORNISCE_INDIRIZZO");
					if(prop.getProperty("TIPO_CLIENTE").compareTo("Residenziale")==0){

						logger.write("Selezione campo 'cliente fornisce indirizzo?- Start");
						compila.clienteFornisceIndirizzo(fornitura_indirizzo);
						logger.write("Selezione campo 'cliente fornisce indirizzo?- Completed");
					}

					logger.write("Conferma Indirizzo Residenza/Sede Legale - Start");
					if(fornitura_indirizzo.compareTo("NO")==0){
						// se il tasto Modifica Indirizzo non è abilitato
						if (!compila.isElementEnabled(compila.buttonModificaIndizzoResidenza)) {
							compila.confermaIndirizzo("Indirizzo di Residenza/Sede Legale");
							//}
						}
						else{
							compila.inserisciIndirizzo(prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("PROVINCIA_COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
							}
					}
				}
				logger.write("Conferma Indirizzo Residenza/Sede Legale - Completed");
				}		
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
