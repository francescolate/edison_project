package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class DisabilitaConfirmationCall {

	@Step("Disabilita Confirmation Call")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				logger.write("Inserimento link SFDC");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				logger.write("Inserimento user");
				page.enterPassword(Costanti.password_admin_salesforce);
				logger.write("Inserimento password");
				page.submitLogin();
				logger.write("Click LogIn");
				// System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				logger.write("Inserimento link WorkBanch");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				logger.write("Seleziona Environment Sandbox");
				a.pressButton(a.checkAgree);
				logger.write("Click button Agree");
				a.pressButton(a.buttonLogin);
				logger.write("Click button Login");
				
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}

				SeleniumUtilities util = new SeleniumUtilities(driver);
									
	
				String query = "SELECT Username, ITA_IFM_User_Matricola__c, ITA_IFM_Channel__c,ITA_IFM_Subchannel__c\r\n" + 
						"FROM User\r\n" + 
						"WHERE Username = '"+prop.getProperty("USERNAME")+"'";
				a.insertQuery(query);
				a.pressButton(a.submitQuery);
				if(a.verificaNoRecordFound()) {
					throw new Exception ("La query per recuperare il canale e sottocanale non ha prodotto risultati per l'utenza: "+Costanti.utenza_antichurn);
				}else {
					a.recuperaRisultati(1, prop);
				}
				
				//una volta recuperati canale e sottocanale devo eseguire il seguente Update.
	
				switch(prop.getProperty("SCENARIO", "CONFIRMATION_CALL")) {
				case "CONFIRMATION_CALL":
					query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_TypeCall__c, ITA_IFM_SubChannel__c, ITA_IFM_ProcessType__c, ITA_IFM_CustomerType__c, ITA_IFM_Residential_Use__c, ITA_IFM_Alphanum_CF__c, ITA_IFM_CustomerChoice__c, ITA_IFM_CallDirection__c, ITA_IFM_TypeLegalForm__c, ITA_IFM_Offer_From_Tablet__c,ITA_IFM_Offer_With_Delegation__c, ITA_IFM_Mobile_Phone__c, ITA_IFM_Email__c, LastModifiedById, LastModifiedDate " + 
							" FROM " + 
							"ITA_IFM_Attivazione_QC_CC__c " + 
							"where id = '"+prop.getProperty("ID_ATTIVAZIONE")+"'";
						/*	"where ITA_IFM_ProcessType__c = 'SWA' " + 
							"and ITA_IFM_CallDirection__c = null " + 
							"and ITA_IFM_Offer_From_Tablet__c = null " + 
							"and ITA_IFM_Offer_With_Delegation__c = null "+ 
							"and ITA_IFM_TypeCall__c = 'Confirmation Call' "+
							"and ITA_IFM_CustomerType__c = '" +prop.getProperty("TIPOLOGIA_CLIENTE")+ "' " +
							"and ITA_IFM_Channel__c ='"+prop.getProperty("ITA_IFM_Channel__c")+"' " + 
							"and ITA_IFM_SubChannel__c = '"+prop.getProperty("ITA_IFM_Subchannel__c")+"'";*/
					break;
				case "QUALITY_CALL":
					query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_TypeCall__c, ITA_IFM_SubChannel__c, ITA_IFM_ProcessType__c, ITA_IFM_CustomerType__c, ITA_IFM_Residential_Use__c, ITA_IFM_Alphanum_CF__c, ITA_IFM_CustomerChoice__c, ITA_IFM_CallDirection__c, ITA_IFM_TypeLegalForm__c, ITA_IFM_Offer_From_Tablet__c,ITA_IFM_Offer_With_Delegation__c, ITA_IFM_Mobile_Phone__c, ITA_IFM_Email__c, LastModifiedById, LastModifiedDate " + 
							" FROM " + 
							"ITA_IFM_Attivazione_QC_CC__c " + 
							"where id = '"+prop.getProperty("ID_ATTIVAZIONE")+"'";
					break;
				case "QUALITY_CALL_INTERNA":
					query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_TypeCall__c, ITA_IFM_SubChannel__c, ITA_IFM_ProcessType__c, ITA_IFM_CustomerType__c, ITA_IFM_Residential_Use__c, ITA_IFM_Alphanum_CF__c, ITA_IFM_CustomerChoice__c, ITA_IFM_CallDirection__c, ITA_IFM_TypeLegalForm__c, ITA_IFM_Offer_From_Tablet__c,ITA_IFM_Offer_With_Delegation__c, ITA_IFM_Mobile_Phone__c, ITA_IFM_Email__c, LastModifiedById, LastModifiedDate " + 
							" FROM " + 
							"ITA_IFM_Attivazione_QC_CC__c " + 
							"where id = '"+prop.getProperty("ID_ATTIVAZIONE")+"'";
					break;
				case "CONFIRMATION_CALL_DIGITAL":
					query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_TypeCall__c, ITA_IFM_SubChannel__c, ITA_IFM_ProcessType__c, ITA_IFM_CustomerType__c, ITA_IFM_Residential_Use__c, ITA_IFM_Alphanum_CF__c, " + 
							"ITA_IFM_CustomerChoice__c, ITA_IFM_CallDirection__c, ITA_IFM_TypeLegalForm__c, ITA_IFM_Offer_From_Tablet__c, ITA_IFM_Offer_With_Delegation__c, ITA_IFM_Mobile_Phone__c, ITA_IFM_Email__c, " +
							"LastModifiedById, LastModifiedDate " + 
							" FROM " + 
							"ITA_IFM_Attivazione_QC_CC__c " + 
							"where ITA_IFM_ProcessType__c = 'SWA' " + 
							"and ITA_IFM_CallDirection__c = null " + 
							"and ITA_IFM_Offer_From_Tablet__c = null " + 
							"and ITA_IFM_Offer_With_Delegation__c = null " + 
							"and ITA_IFM_TypeCall__c = 'Confirmation Call Digital' and ITA_IFM_Mobile_Phone__c = true and ITA_IFM_Email__c = true " + 
							"and ITA_IFM_CustomerType__c = '" +prop.getProperty("TIPOLOGIA_CLIENTE")+ "' " +
							"and ITA_IFM_Channel__c ='"+prop.getProperty("ITA_IFM_Channel__c".toUpperCase())+"' " + 
							"and ITA_IFM_SubChannel__c = '"+prop.getProperty("ITA_IFM_Subchannel__c".toUpperCase())+"'";
				break;
				case "QUALITY_CALL_DIGITAL":
					query = "select id, ITA_IFM_Status__c, ITA_IFM_Channel__c, ITA_IFM_TypeCall__c, ITA_IFM_SubChannel__c, ITA_IFM_ProcessType__c, ITA_IFM_CustomerType__c, ITA_IFM_Residential_Use__c, ITA_IFM_Alphanum_CF__c, " + 
							"ITA_IFM_CustomerChoice__c, ITA_IFM_CallDirection__c, ITA_IFM_TypeLegalForm__c, ITA_IFM_Offer_From_Tablet__c, ITA_IFM_Offer_With_Delegation__c, ITA_IFM_Mobile_Phone__c, ITA_IFM_Email__c, " +
							"LastModifiedById, LastModifiedDate " + 
							" FROM " + 
							"ITA_IFM_Attivazione_QC_CC__c " + 
							"where ITA_IFM_ProcessType__c = 'SWA' " + 
							"and ITA_IFM_CallDirection__c = null " + 
							"and ITA_IFM_Offer_From_Tablet__c = null " + 
							"and ITA_IFM_Offer_With_Delegation__c = null " + 
							"and ITA_IFM_TypeCall__c = 'Quality Call Digital' and ITA_IFM_Mobile_Phone__c = true and ITA_IFM_Email__c = true " + 
							"and ITA_IFM_CustomerType__c = '" +prop.getProperty("TIPOLOGIA_CLIENTE")+ "' " +
							"and ITA_IFM_Channel__c in ('"+prop.getProperty("ITA_IFM_Channel__c".toUpperCase())+"', null) " + 
							"and ITA_IFM_SubChannel__c in ( '"+prop.getProperty("ITA_IFM_Subchannel__c".toUpperCase())+"',null)";
					break;
				}
					
				a.insertQuery(query);
				a.pressButton(a.submitQuery);
				a.recuperaRisultati(1, prop);
				//se era già attivo non faccio nulla
				if (!prop.getProperty("ITA_IFM_Status__c".toUpperCase()).equals("Inattivo")) {
					a.updateRow("ITA_IFM_Status__c", "Inattivo");
					TimeUnit.SECONDS.sleep(10);
				}
				TimeUnit.SECONDS.sleep(3);

				a.logoutWorkbench();

			}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
