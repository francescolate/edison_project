package com.nttdata.qa.enel.testqantt;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class RecuperaLinkTP8 {

	@Step("Leggi email e clicca sul Touch Point di Attivazione")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				String link = null;			
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
		           
		            //gestione con Modulo Java
		            logger.write("Recupera Link TP8 - Start");
//		            GmailQuickStart gm = new GmailQuickStart();
		            GmailQuickStart gm = new GmailQuickStart();
		            		            
		    		link = gm.recuperaLink(prop, "TP8", false);
		    		prop.setProperty("CONFIRMATIONLINK", link);
		    		System.out.println("Link TP8:"+link);
		            logger.write("Recupera Link TP8 - Completed");
		            
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}


