package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CreaOffertaGestioneAnnullaVolturaConAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			
				logger.write("Premi Annulla, verifica popup e premi chiudi - Start ");
				offer.press(offer.buttonAnnulla);
				offer.verificaPopUpconTesto(offer.popUpAnnullaTitle, offer.popUpAnnullaTesto, offer.popUpAnnullaButtonChiudi, offer.popUpAnnullaTestoAtteso);
				logger.write("Premi Annulla, verifica popup e premi chiudi - Completed ");
				
				TimeUnit.SECONDS.sleep(3);
				
				logger.write("Premi Annulla, verifica popup e premi Conferma - Start ");
				offer.press(offer.buttonAnnulla);
				offer.verificaPopUpconTesto(offer.popUpAnnullaTitle, offer.popUpAnnullaTesto, offer.popUpAnnullaButtonConferma, offer.popUpAnnullaTestoAtteso);
				logger.write("Premi Annulla, verifica popup e premi Conferma - Completed ");
				
				TimeUnit.SECONDS.sleep(3);

				logger.write("Gestione popup Annullamento richiesta - Start ");
				offer.popUpSceltaAnnullamento();
				TimeUnit.SECONDS.sleep(3);
				offer.popUpSceltaAnnullamentoEsito(offer.popUpAnnullaSceltaMotivoEsito);
				logger.write("Gestione popup Annullamento richiesta - Completed ");
				

				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
