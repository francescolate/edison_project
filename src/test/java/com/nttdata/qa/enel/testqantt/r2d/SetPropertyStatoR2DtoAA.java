package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

/**
Lo stato R2D dopo il 10K deve essere AA - 
R2D_CaricamentoEsiti_3OK_ELE va a leggere la property STATO_R2D che non viene aggiornata dopo il 1OK
 */
public class SetPropertyStatoR2DtoAA {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			prop.setProperty("STATO_R2D","AA");
			prop.setProperty("RETURN_VALUE", "OK");
			prop.store(new FileOutputStream(args[0]), null);
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}