package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BillsComponent;
import com.nttdata.qa.enel.components.colla.BolleteDettaglioPianoComponent;
import com.nttdata.qa.enel.components.colla.BolleteDettaglioRataComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioBolletta_DettaglioRata_PdfBolletta_ACR_107 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		prop.setProperty("RESIDENTIALMENU", " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ");
		prop.setProperty("PAYMENTALERT", "Sulla rata è presente un sollecito di pagamento/lettera di diffida. Regolarizza subito i pagamenti.");
		
		HomeComponent home = new HomeComponent(driver);
		
		logger.write("Check for Residential Menu - Start");
		
		home.checkForResidentialMenu(prop.getProperty("RESIDENTIALMENU"));
					
		logger.write("Check for Residential Menu - Completed");
		
		logger.write("Click on Bills from Residential Menu - Start");

		home.clickComponent(home.bills);
		
		logger.write("Click on Bills from Residential Menu - Completed");

		BillsComponent bs = new BillsComponent(driver);
		
		bs.verifyComponentExistence(bs.bolletteHeader);
		
		bs.waitForElementToDisplay(bs.yourFilter);
		
		logger.write("Click on Mostra Filtri - Start");

		bs.clickComponent(bs.yourFilter);
		
		logger.write("Click on Mostra Filtri - Completed");

		logger.write("Change filter value from bolleta to rata- Start");

		bs.clickComponent(bs.rataFilter);
		
		bs.clickComponent(bs.bolletaFilter);
		
		bs.clickComponent(bs.applicaFiltri);
		
		logger.write("Change filter value from bolleta to rata- Completed");

		logger.write("Click on more - Start");

		bs.clickComponent(bs.more);
		
		logger.write("Click on more - Completed");

		bs.verifyComponentExistence(bs.pagaOnline);		
		bs.verifyComponentExistence(bs.dettaglioRata);
		bs.verifyComponentExistence(bs.dettaglioPiano);
		bs.verifyComponentExistence(bs.scaricaPDFPiano);
		
		logger.write("Click on Dettaglio Rata - Start");

		bs.clickComponent(bs.dettaglioPiano);
		
		logger.write("Click on Dettaglio Rata - Completed");

		BolleteDettaglioPianoComponent bdpc = new BolleteDettaglioPianoComponent(driver);
		
		logger.write("Verify the fields in Dettaglio Rata page - Start");
		
		bdpc.verifyComponentExistence(bdpc.VAIATUTTELEBOLLETTEButton);
		
		bdpc.verifyComponentExistence(bdpc.SCARICAPDFPIANOButton);
						
		bdpc.verifyComponentExistence(bdpc.addressLabel);
		
		bdpc.verifyComponentExistence(bdpc.numeroClienteLabel);
		bdpc.verifyBilldetails(bdpc.numeroPiano, BolleteDettaglioPianoComponent.exp);		
		bdpc.verifyBilldetails(bdpc.importoRateizzato, BolleteDettaglioPianoComponent.exp);
		bdpc.verifyBilldetails(bdpc.interessiDiMora, BolleteDettaglioPianoComponent.exp);
		bdpc.verifyBilldetails(bdpc.interessiDiDilazione, BolleteDettaglioPianoComponent.exp);
		bdpc.verifyBilldetails(bdpc.numeroRate, BolleteDettaglioPianoComponent.exp);
		bdpc.verifyBilldetails(bdpc.modalitàdiPagamentoLabel, BolleteDettaglioPianoComponent.exp);
		bdpc.verifyBilldetails(bdpc.addebitoConBollettino, BolleteDettaglioPianoComponent.exp);
		bdpc.verifyBilldetails(bdpc.totaledaPagareLabel, BolleteDettaglioPianoComponent.exp);
		
		/*
		
		bdpc.verifyComponentExistence(bdpc.numeroPiano);
		
		bdpc.verifyComponentExistence(bdpc.importoRateizzato);
						
		bdpc.verifyComponentExistence(bdpc.interessiDiMora);
		
		bdpc.verifyComponentExistence(bdpc.interessiDiDilazione);
		
		bdpc.verifyComponentExistence(bdpc.numeroRate);
		
		bdpc.verifyComponentExistence(bdpc.modalitàdiPagamentoLabel);
		
		bdpc.verifyComponentExistence(bdpc.addebitoConBollettino);
		
		bdpc.verifyComponentExistence(bdpc.totaledaPagareLabel);
		*/
		bdpc.verifyComponentExistence(bdpc.commodityIcon);
		
		bdpc.verifyComponentExistence(bdpc.commodity);
						
		bdpc.verifyComponentExistence(bdpc.rataN);
		
		bdpc.verifyComponentExistence(bdpc.NDocumento);
		
		bdpc.verifyComponentExistence(bdpc.scade);
		
		bdpc.verifyComponentExistence(bdpc.importo);
		
		bdpc.verifyComponentExistence(bdpc.statoPagamento);
		
		bdpc.verifyComponentExistence(bdpc.pagataDaPagare);
		
		bdpc.comprareText(bdpc.paymentAlert, prop.getProperty("PAYMENTALERT"), true);
				
		logger.write("Verify the fields in Dettaglio Rata page - Completed");
		
		prop.setProperty("RETURN_VALUE", "OK");
	
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
