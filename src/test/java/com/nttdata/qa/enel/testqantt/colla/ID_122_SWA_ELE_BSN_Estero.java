package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_122_SWA_ELE_BSN_Estero {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		int counter = 0; boolean exitCondition;
		QANTTLogger logger = new QANTTLogger(prop);
		
		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OCR_Module_Component omc = new OCR_Module_Component(driver);
			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);	
			
			prop.setProperty("POD", "IT004E"+omc.generateRandomPOD(8));
			System.out.print(prop.getProperty("POD"));
			prop.setProperty("PDR", "116900000"+omc.generateRandomPOD(5));
			System.out.print(prop.getProperty("PDR"));

			Thread.sleep(3000);
			
			headerMenu.selectionOptionsFromListbox(headerMenu.contractList,prop.getProperty("TYPE_OF_CONTRACT"),headerMenu.contractList,prop.getProperty("PLACE"),headerMenu.contractList,prop.getProperty("NEED"));	
			TimeUnit.SECONDS.sleep(3);
			headerMenu.clickComponent(headerMenu.iniziaOraButton);
			
			TimeUnit.SECONDS.sleep(3);
			
			headerMenu.clickComponent(By.xpath("//h4[contains(text(),'"+prop.getProperty("OFFER") + "')]//ancestor::div[@class='cmEvidenceBanner_offer']//a"));
			
			TimeUnit.SECONDS.sleep(3);

			omc.clickComponent(By.xpath("//a[@data-gtm-name='"+prop.getProperty("OFFER_FULL") + "']"));
			
			TimeUnit.SECONDS.sleep(10);
			
			omc.clickComponent(omc.compilaManualmenteBtn);
			
			omc.inserisciDatiBusiness("s.a.s.", "01533540447", "ASSIMETA SAS", "01533540447", "", "3400000300", "testing.crm.automation@gmail.com", "testing.crm.automation@gmail.com", "Rappresentante legale", "BRUNA", "MASSACCI", "MSSBRN64T43G005E", "", "3400000300", false, driver);
			
			omc.insertInformazioniFornitura("CAMBIO FORNITORE", prop.getProperty("POD"), prop.getProperty("CAP"), prop.getProperty("CITY"), prop.getProperty("CITY_FULL"), prop.getProperty("ADDRESS"), prop.getProperty("CIVIC"), "", prop.getProperty("SUPPLIER_LUCE"), true, true);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
	public static void closeChat(OCR_Module_Component omc) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, 1))
			omc.jsClickComponent(omc.chatCloseButton);
	}
	
	public static void closeChat(OCR_Module_Component omc, int timeout) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, timeout))
			omc.jsClickComponent(omc.chatCloseButton);
	}
}
