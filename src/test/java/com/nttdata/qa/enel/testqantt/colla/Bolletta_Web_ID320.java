package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ModificaBollettaWebComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Bolletta_Web_ID320 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			logger.write("verify and click on services from residential menu -- Start");
			hm.verifyComponentExistence(hm.services);
			hm.clickComponent(hm.services);
			logger.write("verify and click on services from residential menu -- Completed");

			ServicesComponent sc = new ServicesComponent(driver);
			logger.write("verify and click on BolleteWeb from Servizi screen -- Start");
			sc.verifyComponentExistence(sc.webBill);
			sc.clickComponent(sc.webBill);
			logger.write("verify and click on BolleteWeb from Servizi screen -- Completed");

			BolletteComponent bc = new BolletteComponent(driver);
			bc.verifyComponentExistence(bc.bollettaWebTitle);
			bc.verifyComponentExistence(bc.ModificaIndirizzoEmail);
			logger.write("verify and click on ModificaIndirizzoEmail -- Start");
			bc.clickComponent(bc.ModificaIndirizzoEmail);
			logger.write("verify and click on ModificaIndirizzoEmail -- Completed");

			ModificaBollettaWebComponent mc = new ModificaBollettaWebComponent(driver);
			logger.write("verify modificaBolletaWeb screen details -- Start");
			mc.verifyComponentExistence(mc.modificaBolletaWeb);
			mc.compareText(mc.modificaBolletaWeb, mc.ModificaBolletaTitle, true);
			mc.compareText(mc.modificaBolletaWebSubText, mc.ModificaBolletaWebSubText, true);
			mc.verifyComponentExistence(mc.checkBox);
			mc.verifyComponentExistence(mc.bolleteWebService);
			logger.write("verify modificaBolletaWeb screen details -- Completed");

			logger.write("Click on Forniture from side menu -- Start");
			mc.clickComponent(mc.Forniture);
			logger.write("Click on Forniture from side menu -- Completed");

			logger.write("Verify pop up details-- Start");
			mc.verifyComponentExistence(mc.popUpTitle);
			mc.compareText(mc.popUpText, mc.PopUpText, true);
			mc.compareText(mc.popUpTitle, mc.PopUpTitle, true);
			mc.verifyComponentExistence(mc.popUpNoButton);
			mc.verifyComponentExistence(mc.popUpYesButton);
			logger.write("Verify pop up details-- Completed");

			logger.write("Click on Pop up No option and verify page navigation-- Start");
			mc.clickComponent(mc.popUpNoButton);
			mc.verifyComponentExistence(mc.modificaBolletaWeb);
			mc.compareText(mc.modificaBolletaWeb, mc.ModificaBolletaTitle, true);
			mc.compareText(mc.modificaBolletaWebSubText, mc.ModificaBolletaWebSubText, true);
			logger.write("Click on Pop up No option and verify page navigation-- Completed");

			mc.clickComponent(mc.Forniture);
			logger.write("Click on Pop up Yes option and verify page navigation-- Start");
			mc.clickComponent(mc.popUpYesButton);
			hm.verifyComponentExistence(hm.pageTitleRes);
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			logger.write("Click on Pop up Yes option and verify page navigation-- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
