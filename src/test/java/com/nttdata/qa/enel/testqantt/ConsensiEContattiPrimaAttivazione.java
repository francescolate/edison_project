package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConsensiEContattiPrimaAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {


			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ContattiConsensiComponent contatti = new ContattiConsensiComponent(driver);
			if(prop.getProperty("SKIP_CONTATTI_CONSENSI","N").contentEquals("N")) {
			if(prop.getProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","").contentEquals("Y")) {
			logger.write("Verifica Informativa Diritto Ripensamento - Start");
			contatti.verificaInformativaDirittoRipensamento();
			logger.write("Verifica Informativa Diritto Ripensamento - Completed");
			}
			logger.write("Verifica Prepopolamento campi contatti - Start");
//			contatti.verificaPopolamentoCampiContatti(prop.getProperty("TELEFONO"),prop.getProperty("CELLULARE"),prop.getProperty("EMAIL"));
			contatti.verificaPopolamentoCampiContatti();
			logger.write("Verifica Prepopolamento campi contatti - Completed");
			
			logger.write("Inserimento contatti e consensi - Start");
			
			
			contatti.selezionaFasciaContabilita(prop.getProperty("FASCIA_CONTABILITA"));
			contatti.selezionaConsensoProfilazione(prop.getProperty("CONSENSO_PROFILAZIONE"));
			contatti.selezionaConsensoMarketingTelefonico(prop.getProperty("CONSENSO_MARKETING_TELEFONICO"));
			contatti.selezionaConsensoTerziTelefonico(prop.getProperty("CONSENSO_TERZI_TELEFONICO"));
			contatti.selezionaConsensoMarketingCellulare(prop.getProperty("CONSENSO_MARKETING_CELLULARE"));
			contatti.selezionaConsensoTerziCellulare(prop.getProperty("CONSENSO_TERZI_CELLULARE"));
			contatti.selezionaConsensoMarketingEmail(prop.getProperty("CONSENSO_MARKETING_EMAIL"));
			contatti.selezionaConsensoTerziEmail(prop.getProperty("CONSENSO_TERZI_EMAIL"));
			}
			if(!prop.getProperty("ESECUZIONE_ANTICIPATA","").contentEquals("SKIP")) {
				contatti.selezionaEsecuzioneAnticipata(prop.getProperty("ESECUZIONE_ANTICIPATA"));
				if (prop.getProperty("ESECUZIONE_ANTICIPATA","").contentEquals("NO")) {
					RiepilogoOffertaComponent contatti2 = new RiepilogoOffertaComponent(driver);
					contatti2.verifyComponentExistence(contatti2.testo4ContattiConsensi);
				}
			}
			
		    contatti.press(contatti.confermaButton);
		    logger.write("Inserimento contatti e consensi - Completed");


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
