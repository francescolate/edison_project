package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CompilaDatiFornituraVolturaConAccolloUsoAbitativoResPEDisalimentabilitaNO {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
					driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			logger.write("Selezione commodity - Start");
			compila.selezionaFornitura(compila.fornitura_ele_allaccio);
			logger.write("Selezione commodity - Completed");
		
			logger.write("Inserimento Dati Fornitura - Start");
		
			compila.selezionaLigtheningValue("Residente", prop.getProperty("RESIDENTE"));			
			
			compila.selezionaLigtheningValue("Ordine Fittizio", prop.getProperty("ORDINE_FITTIZIO"));
			
			compila.selezionaLigtheningValue("Disalimentabilita’", prop.getProperty("DISALIMENTABILITA"));
					
			
			logger.write("Vengono abilitati e resi obbligatori i campi 'Telefono Disalimentabilità' e 'Tipologia Disalimentabilità' - Start");
			
			compila.verifyInputFieldIsEnabled("Tipologia Disalimentabilita’");			
			compila.verifyInputFieldIsEnabled("Telefono Disalimentabilita’");
			logger.write("Vengono abilitati e resi obbligatori i campi 'Telefono Disalimentabilità' e 'Tipologia Disalimentabilità' - Completed");
			
			
			logger.write("Sono selezionabili le seguenti voci: 'Apparecchiature Medico Terapeutiche' e 'Pubblica Utilità' - Start");
			compila.selezionaLigtheningValue("Tipologia Disalimentabilita’", "PUBBLICA UTILITA’");
			TimeUnit.SECONDS.sleep(2);
			compila.selezionaLigtheningValue("Tipologia Disalimentabilita’", "APPARECCHIATURE MEDICO TERAPEUTICHE");
			logger.write("Sono selezionabili le seguenti voci: 'Apparecchiature Medico Terapeutiche' e 'Pubblica Utilità' - Completed");
			
			logger.write("Premere il tasto conferma Fornitura - Start");
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			logger.write("Premere il tasto conferma Fornitura - Completed");
			TimeUnit.SECONDS.sleep(2);
			logger.write("Verifica che il sistema richiede attraverso un PopUp la valorizzazione del campo 'Telefono Disalimentabilità' come obbligatorio - Start");
			compila.verifyComponentExistence(compila.popUpErrore_telefono_disalimentabilita);
			logger.write("Verifica che il sistema richiede attraverso un PopUp la valorizzazione del campo 'Telefono Disalimentabilità' come obbligatorio - Completed");	

			logger.write("Premere il tasto Chiudi della popUp - Start");
			conferma.pressAndCheckSpinners(compila.buttonChiudi_popUpErrore_telefono_disalimentabilita);
			logger.write("Premere il tasto Chiudi della popUp - Completed");
			
			logger.write("Impostare il campo 'Disalimentabilita' a SI - Start");		
			compila.selezionaLigtheningValue("Disalimentabilita’", "SI");							
			logger.write("Impostare il campo 'Disalimentabilita' a SI - Completed");
		
			logger.write("Vengono disabilitati i campi 'Telefono Disalimentabilità' e 'Tipologia Disalimentabilità' - Start");
			compila.verifyInputFieldIsNotEnabled("Tipologia Disalimentabilita’");
			compila.verifyInputFieldIsNotEnabled("Telefono Disalimentabilita’");
		
			logger.write("Vengono disabilitati i campi 'Telefono Disalimentabilità' e 'Tipologia Disalimentabilità' - Completed");
			
					
			logger.write("Inserimento Dati Fornitura - Completed");			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
	
	//public static String checklistMessage="ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:  POD/PDR/numero utenza.INFORMAZIONI UTILI - Il Cambio Uso della fornitura di energia elettrica ha un costo pari a: 0,00 €.- Il Cambio Uso della fornitura di gas ha un costo pari a: 0,00 €.- Contestualmente alla variazione uso è possibile attivare/modificare/revocare i Servizi VASe il Metodo di pagamento - Per la variazione uso gas, nel caso di modifica dell'uso fornitura, è necessario eseguire il cambio prodotto.Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto."; 
}
