package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificheRichiestaChiusaDaConfermare {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    driver.switchTo().defaultContent();
		    TimeUnit.SECONDS.sleep(60);
            driver.navigate().refresh();
		    VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
		    logger.write("Recupero valore id richiesta - Start");
//			System.out.println("Recupero valore id richiesta");
		    verifica.pressJavascript(verifica.elementiRichiestaSection);
//			System.out.println("Recupero OI richiesta");
		    String id = verifica.recuperaOIRichiesta();
	        prop.setProperty("OI_RICHIESTA", id);
//			System.out.println("Valore OI richiesta: "+prop.getProperty("OI_RICHIESTA"));
	        logger.write("Recupero valore id richiesta - Completed");
	        logger.write("Verifica Stati Richiesta - Start");
//			System.out.println("Verifica Stati Richiesta - Start");
//			System.out.println("Stato Richiesta Aspettato: " + prop.getProperty("STATO_RICHIESTA"));
	        verifica.verificaStatiRichiesta("CHIUSA DA CONFERMARE", prop.getProperty("STATO_R2D"), prop.getProperty("STATO_SAP"), prop.getProperty("STATO_SEMPRE"));
//			System.out.println("Verifica Stati Richiesta - Completed");
			logger.write("Verifica Stati Richiesta - Completed");
			
			
			verifica.verificaSottoStatiRichiesta("OFFER - CHIUSA DA CONFERMARE");
			
	        
	        prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
