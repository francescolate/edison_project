package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SezioneMercatoSubentroLiberoPodGas {

    public static void main(String[] args) throws Exception {
        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            String descEsitoPrecheck;
            SeleniumUtilities util = new SeleniumUtilities(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            logger.write("Verifica checklist e conferma - Start");
            CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);

            Thread.sleep(5000);

            checkListModalComponent.clickConferma();

            IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
            prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
            logger.write("Verifica checklist e conferma - Completed");

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");
            SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
            merc.verifyComponentExistence(merc.tabSubentro);
            merc.verifyComponentExistence(merc.labelSubentro);

            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            merc.verifyComponentExistence(insert.sezioneInserimentoDati);

            logger.write("verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");

            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
            merc.verifyComponentExistence(merc.checkContatto);
            logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");

            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
            merc.verifyComponentExistence(merc.checkCf);
            logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");

            //Box Sezione Mercato

            merc.clickComponentIfExist(merc.inputSezioneMercato);

            merc.clickComponentIfExist(merc.buttonConfermaSezioneMercato);

            merc.clearAndSendKeys(merc.inputPodPdr, "54531214151116");

            merc.clearAndSendKeys(merc.inputCap, "20121");

            merc.clickComponentIfExist(merc.buttonEseguiPrecheck);

            merc.checkSpinnersSFDC();

            Thread.sleep(1000);

            if (merc.getElementTextString(merc.offertabilità).equals("OK"))
                logger.write("Offertabilità OK");
            else
                throw new Exception("Offertabilità KO");


            merc.clickComponentIfExist(merc.ripetiPrecheck);

            merc.checkSpinnersSFDC();

            merc.clickComponentIfExist(merc.modificaPrecheck);

            merc.clearAndSendKeys(merc.inputPodPdr, "54531214151116");

            merc.clearAndSendKeys(merc.inputCap, "20121");

            merc.clickComponentIfExist(merc.buttonEseguiPrecheck);

            merc.checkSpinnersSFDC();

            Thread.sleep(1000);

            if (merc.getElementTextString(merc.esitoPrecheck).equals("OK"))
                logger.write("Esito Precheck Ok");
            else
                logger.write("Esito Precheck KO");


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
