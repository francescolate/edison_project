package com.nttdata.qa.enel.testqantt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SetPropertyPredPresaEleId2_04 {


    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
        		// Verifica Stato Richiesta (Preventivo Inviato - Fatto)
            	prop.setProperty("STATO_RICHIESTA", "CHIUSO");
        		prop.setProperty("SOTTO_STATO_RICHIESTA", "ESPLETATO");
        		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ESPLETATO");
        		prop.setProperty("STATO_ELEMENTO_ORDINE", "ESPLETATO");
        		prop.setProperty("STATO_R2D", "OK");
        		prop.setProperty("STATO_R2D_ORDINE", "OK");
        		prop.setProperty("STATO_SAP_SD_ORDINE", "OK");
        		prop.setProperty("STEP_PP","4");
       		    System.out.println("STEP_PP=4");

            }
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
