package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazione_82 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
		    prop.setProperty("SupplyServices", "Addebito Diretto Bolletta Web Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione Autolettura InfoEnelEnergia Opzione Bolletta Rateizzazione ");

		    
			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
			String privateAreaHomePageURL = driver.getCurrentUrl();
			String privateAreaServiziURL = prop.getProperty("WP_LINK")+"it/area-clienti/imprese/forniture/indirizzo_fatturazione/dispositiva";
			
			
			logger.write("Verifying the list of devices after clicking on Servizi  STARTS");
			mif.clickComponent(mif.serivzi);
			Thread.sleep(5000);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.SERVIZI_PATH, mif.serviziPath);
			
			mif.comprareText(mif.serviziTitle, ModificaIndirizzoFattuazioneComponent.SERVIZI_TITLE, true);
			mif.checkForSupplyServices(prop.getProperty("SupplyServices"));
			logger.write("Verifying the list of devices after clicking on Servizi  ENDS");
			
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-STARTS");
			mif.clickComponent(mif.modificaIndirizzodiFatturazione);
			Thread.sleep(5000);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.INDIRIZZO_PATH, mif.indirizzoDiFatturazionePath);
			mif.comprareText(mif.sectionData, ModificaIndirizzoFattuazioneComponent.SECTION_DATA, true);
			mif.comprareText(mif.sectionBillingAddress, ModificaIndirizzoFattuazioneComponent.SECTION_BILLING_ADDRESS, true);
			mif.comprareText(mif.sectionTechnicalSpec, ModificaIndirizzoFattuazioneComponent.SECTION_TECH_SPECIFICATION, true);
			mif.comprareText(mif.sectionContract, ModificaIndirizzoFattuazioneComponent.SECTION_CONTRACT, true);
			mif.comprareText(mif.clientNumber, prop.getProperty("CLIENT_NUMBER"), true);
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-ENDS");
			
			logger.write("Valorize some mandatory fields and proceed with the selection of the ESCI key-STARTS");
			mif.clickComponent(mif.modificaButton);
			Thread.sleep(5000);
			mif.enterinputParameters(mif.provincia, prop.getProperty("PROVINCE"));
			mif.enterinputParameters(mif.comune, prop.getProperty("COMUNE"));
			mif.enterinputParameters(mif.indrizzo, prop.getProperty("INDIRIZZO"));
			mif.enterinputParameters(mif.numerocivico, prop.getProperty("NUMERO_CIVICO"));
			mif.clickComponent(mif.esciButton);
			mif.comprareText(mif.popupHeading, ModificaIndirizzoFattuazioneComponent.POPUP_HEADING, true);
			mif.comprareText(mif.popupTitle, ModificaIndirizzoFattuazioneComponent.POPUP_TITLE, true);
			mif.verifyComponentExistence(mif.tornaAlProcessoButton);
			mif.verifyComponentExistence(mif.esciPopupButton);
			mif.verifyComponentExistence(mif.popupClose);
			logger.write("Valorize some mandatory fields and proceed with the selection of the ESCI key-ENDS");
			
			logger.write("Verifying the functionality of TORNA AL PROCESSOS button in the POPUP-STARTS");
			mif.clickComponent(mif.tornaAlProcessoButton);
			Thread.currentThread().sleep(5000);
			mif.checkURLAfterRedirection(privateAreaServiziURL);
			mif.verifyFeildAttribute(mif.provinciaBeforeSave, prop.getProperty("PROVINCE"));
			mif.verifyFeildAttribute(mif.comuneBeforeSave, prop.getProperty("COMUNE"));
			mif.verifyFeildAttribute(mif.indrizzoBeforeSave, prop.getProperty("INDIRIZZO"));
			mif.verifyFeildAttribute(mif.numerocivicoBeforeSave, prop.getProperty("NUMERO_CIVICO"));
			mif.clickComponent(mif.esciButton);
			logger.write("Verifying the functionality of TORNA AL PROCESSOS button in the POPUP-ENDS");
			
			logger.write("Verifying the functionality of Esci button -STARTS");
			mif.comprareText(mif.popupHeading, ModificaIndirizzoFattuazioneComponent.POPUP_HEADING, true);
			mif.comprareText(mif.popupTitle, ModificaIndirizzoFattuazioneComponent.POPUP_TITLE, true);
			mif.verifyComponentExistence(mif.tornaAlProcessoButton);
			mif.verifyComponentExistence(mif.esciPopupButton);
			mif.verifyComponentExistence(mif.popupClose);
			logger.write("Verifying the functionality of Esci button -ENDS");
		
			logger.write("Verifying the functionality of Esci button in the POPUP-STARTS");
			mif.clickComponent(mif.esciPopupButton);
			Thread.sleep(10000);
			mif.checkURLAfterRedirection(privateAreaHomePageURL);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.HOMEPAGE_PATH, mif.HomepagePath);
			logger.write("Verifying the functionality of Esci button in the POPUP-ENDS");
						
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
