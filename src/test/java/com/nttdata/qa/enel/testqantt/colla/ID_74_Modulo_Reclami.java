package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.ImpreseComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.components.colla.SupportFaqComponent;
import com.nttdata.qa.enel.components.colla.SupportPrivacyComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_74_Modulo_Reclami {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);		
			By accessLoginPage = null;
			
			ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
			SupportFaqComponent sc = new SupportFaqComponent(driver);
			SupportPrivacyComponent spc = new SupportPrivacyComponent(driver);

			logger.write("Verify header section - Start");			
			mod.verifyHeaderVoice(mod.headerVoice,prop.getProperty("LUCE_E_GAS"));
			mod.verifyHeaderVoice(mod.headerVoice,prop.getProperty("IMPRESA"));
			mod.verifyHeaderVoice(mod.headerVoice,prop.getProperty("STORIE"));
			mod.verifyHeaderVoice(mod.headerVoice,prop.getProperty("FUTUR-E"));
			mod.verifyHeaderVoice(mod.headerVoice,prop.getProperty("MEDIA"));
			mod.verifyHeaderVoice(mod.headerVoice,prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");
			
			logger.write("Verify footer section - Start");			
			mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
			mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);			
			Thread.sleep(5000);
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
			logger.write("Verify footer section - Completed");
			
			logger.write("Click on PREFERENZE_COOKIE and verify page navigation and details- Start");			
			mod.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("PREFERENZE_COOKIE"))));
			Thread.sleep(5000);
			WebElement element = driver.findElement(mod.popupFrame);
			driver.switchTo().frame(element);
			mod.verifyComponentExistence(mod.inviaPreferenze);
			mod.verifyComponentExistence(mod.informativaPolicy);
			mod.clickComponent(mod.informativaPolicy);
			driver.switchTo().defaultContent();
			String winHandleBefore = driver.getWindowHandle();
			Thread.sleep(3000);
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			sc.checkURLAfterRedirection(spc.url);
			spc.verifyComponentExistence(spc.privacyTitle);
			//mod.comprareText(spc.path, spc.Path, true);
//			mod.comprareText(spc.privacyText, spc.Privacy_Text, true);
			spc.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("PREFERENZE_COOKIE"))));
			Thread.sleep(5000);
			
			WebElement element1 = driver.findElement(mod.popupFrame);
			driver.switchTo().frame(element1);
			spc.clickComponent(mod.inviaPreferenze);
			mod.verifyComponentExistence(mod.impostazioniTrasmesse);
			mod.comprareText(mod.impostazioniTrasmesseContent, mod.ImpostazioniTrasmesseContent, true);
			mod.verifyComponentExistence(mod.chiudere);
			mod.verifyComponentExistence(mod.informativaPolicy);
			driver.switchTo().defaultContent();
			driver.close();
			driver.switchTo().window(winHandleBefore);
			logger.write("Click on PREFERENZE_COOKIE and verify page navigation and details- Completed");			

			logger.write("Click on INFORMAZIONI_LEGALI and verify page navigation and details- Start");			
			driver.navigate().refresh();
			mod.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("INFORMAZIONI_LEGALI"))));
			sc.checkURLAfterRedirection(sc.InfoLegaliUrl);
			mod.comprareText(sc.infoLegaliContent, sc.InfoLegaliContent, true);
			driver.navigate().back();			
			mod.comprareText(mod.moduloReclamiPagePath, mod.MODULORECLAMIPAGEPATH, true);
			mod.verifyComponentExistence(mod.moduloReclamiTitle);
			logger.write("Click on INFORMAZIONI_LEGALI and verify page navigation and details- Completed");			

			logger.write("Click on CREDITS and verify page navigation and details- Start");			
			mod.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("CREDITS"))));
			sc.checkURLAfterRedirection(sc.creditsUrl);
			sc.verifyComponentExistence(sc.creditsTitle);
//			mod.comprareText(sc.creditsPath, sc.CreditsPath, true);
			mod.comprareText(sc.creditsContent, sc.CreditsContent, true);
			driver.navigate().back();			
			mod.comprareText(mod.moduloReclamiPagePath, mod.MODULORECLAMIPAGEPATH, true);
			mod.verifyComponentExistence(mod.moduloReclamiTitle);
			logger.write("Click on CREDITS and verify page navigation and details- Completed");			

			logger.write("Click on IMPRESA and verify page navigation and details- Start");			
			mod.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("IMPRESA"))));
			ImpreseComponent ic = new ImpreseComponent(driver);
			ic.checkURLAfterRedirection(ic.impresePageUrl);
			driver.navigate().back();			
			mod.comprareText(mod.moduloReclamiPagePath, mod.MODULORECLAMIPAGEPATH, true);
			mod.verifyComponentExistence(mod.moduloReclamiTitle);
			logger.write("Click on IMPRESA and verify page navigation and details- Completed");			

			logger.write("Click on SUPPORTO and verify page navigation and details- Start");			
			mod.clickComponent(By.xpath(mod.footerLink.replace("$Value$", prop.getProperty("SUPPORTO"))));
			SupportoSectionComponent ssc = new SupportoSectionComponent(driver);
			ic.checkURLAfterRedirection(ssc.PageUrl);
			driver.navigate().back();
			mod.comprareText(mod.moduloReclamiPagePath, mod.MODULORECLAMIPAGEPATH, true);
			mod.verifyComponentExistence(mod.moduloReclamiTitle);
			logger.write("Click on SUPPORTO and verify page navigation and details- Complted");			

			logger.write("Click on user icon and verify page navigation and details- Start");			
			mod.clickComponent(mod.userIcon);
			String loginUrl = "https://www-coll1.enel.it/it/login";
			ic.checkURLAfterRedirection(loginUrl);
			driver.navigate().back();			
			mod.comprareText(mod.moduloReclamiPagePath, mod.MODULORECLAMIPAGEPATH, true);
			mod.verifyComponentExistence(mod.moduloReclamiTitle);
			logger.write("Click on user icon and verify page navigation and details- Completed");			

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
	
}
