package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_292_MoGe_E2E {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			hm.checkForErrorPopup();
			logger.write("Click on Account from residential menu -- Strat");
			hm.clickComponent(hm.account);
			logger.write("Click on Account from residential menu -- Completed");

			AccountComponent ac = new AccountComponent(driver);
			logger.write("Verify page navigation and details in Account page -- Strat");
			ac.verifyComponentExistence(ac.pageTitle);
			hm.comprareText(ac.pageTitle, ac.PAGE_TITLE, true);
			hm.comprareText(ac.titleSubText, ac.TITLE_SUBTEXT, true);
			logger.write("Verify page navigation and details in Account page -- Completed");
			logger.write("Click on Modifica Contatti button-- Strat");
			ac.clickComponent(ac.modificaContatti);
			logger.write("Click on Modifica Contatti button-- Completed");

			logger.write("Verify Dati e contatti title and subtext-- Strat");
			Thread.sleep(20000);
			ac.verifyComponentExistence(ac.datiEContattiTitle);
			ac.comprareText(ac.datiEContattiTitle, ac.PAGE_TITLE, true);
			ac.comprareText(ac.datiEContattiSubText, ac.DATI_E_CONTATTI_SUBTEXT, true);
			logger.write("Verify Dati e contatti title and subtext-- Completed");
	
			ac.modifyExistingPhone(ac.inputTelefono);
		    JavascriptExecutor js = (JavascriptExecutor) driver;
		    String Telefono = (String)js.executeScript("return (document.evaluate(\""+"//input[@id='cntPhoneFieldInput']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			prop.setProperty("TELEFONO_INPUT", Telefono);
			//System.out.println(prop.get("TELEFONO_INPUT"));
			ac.modifyExistingPhone(ac.inputCellular);
			String CellularePrefix = (String)js.executeScript("return (document.evaluate(\""+"//select[@name='areaCodeMobilePhone']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			String Cellulare = CellularePrefix + (String)js.executeScript("return (document.evaluate(\""+"//input[@id='cntMobileFieldInput']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			prop.setProperty("CELLULARE_INPUT", Cellulare);
			System.out.println(prop.get("CELLULARE_INPUT"));
			ac.modifyExistingEmail(ac.inputEmail);
			String Email_IP = (String)js.executeScript("return (document.evaluate(\""+"//input[@id='cntEmailFieldInput']"+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			prop.setProperty("EMAIL_INPUT", Email_IP);
			//System.out.println(prop.get("EMAIL_INPUT"));
			ac.selectRadioButton();
			ac.clickComponent(ac.SalvaModifiBtn);
			Thread.sleep(4000);
			ac.verifyComponentExistence(ac.OperazioneHeading_1);
			ac.comprareText(ac.OperazioneHeading_1, ac.OPERAZIONE_HEADING, true);
			ac.comprareText(ac.OperazioneValue, ac.OPERAZIONE_VALUE, true);
						
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
