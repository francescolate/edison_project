package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CommodityGasNonResidenzialeSubentro {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
//			//selezione referente
			conferma.press(conferma.selezionaReferente);
			//selezione fornitura
			conferma.press(conferma.selezionaPrimaFornituraRes);
			
			
			conferma.verificaCampiSezioneCommodityGasBusiness(prop.getProperty("POD"));
			//conferma.inserisciOrdineFittizio(prop.getProperty("ORDINE_FITTIZIO"));
			conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
			
			conferma.inserisciCategoriaConsumo(prop.getProperty("CATEGORIA_CONSUMO"));
			conferma.inserisciOrdineGrandezza(prop.getProperty("ORDINE_GRANDEZZA"));
			conferma.inserisciProfiloConsumo(prop.getProperty("PROFILO_CONSUMO"));
//			conferma.inserisciPotenzialitaRichiesta(prop.getProperty("POTENZIALITA"));
			conferma.inserisciCategoriaUso(prop.getProperty("CATEGORIA_USO"));
			conferma.inserisciClassePrelievo(prop.getProperty("CLASSE_DI_PRELIEVO"));
			
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
