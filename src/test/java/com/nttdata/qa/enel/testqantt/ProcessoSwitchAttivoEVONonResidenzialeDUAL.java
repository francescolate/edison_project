package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoSwitchAttivoEVONonResidenzialeDUAL {

	@Step("ProcessoSwitchAttivoEVONonResidenzialeDUAL")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
			SeleniumUtilities util = new SeleniumUtilities(driver);
				SceltaProcessoComponent switchAttivo = new SceltaProcessoComponent(driver);
				GestionePODperSWAEVO pod = new GestionePODperSWAEVO(driver);
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(
						driver);
				DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
				IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(
						driver);
				
				logger.write("Selezione processo Switch Attivo Evo - Start");
				TimeUnit.SECONDS.sleep(10);
				switchAttivo.clickAllProcess();
				TimeUnit.SECONDS.sleep(2);
				switchAttivo.chooseProcessToStart(switchAttivo.switchAttivoEVO);
				logger.write("Selezione processo Switch Attivo Evo - Completed");

				logger.write("Identificazione interlocutore - Start");
				identificaInterlocutore.insertDocumento("b");
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				logger.write("Identificazione interlocutore - Completed");

				// Dopo Identifica interlocutore Numero Documento --> passa direttamente al
				// Checkpoint senza richiedere i dati Documenti
				logger.write("Inserimento documenti identita cliente - Start");
//				if(!prop.getProperty("TIPO_UTENZA","PE").contentEquals("S2S")) {
				if(!prop.getProperty("TIPO_UTENZA").contentEquals("S2S")) {
					GestioneDocumentiIdentitaComponentEVO documenti = new GestioneDocumentiIdentitaComponentEVO(driver);
					
					documenti.inserisciDocumento(prop.getProperty("TIPO_DOCUMENTO"), prop.getProperty("NUMERO_DOCUMENTO"),
							prop.getProperty("RILASCIATO_DA"), prop.getProperty("RILASCIATO_IL"));
					
					documenti.conferma(documenti.confermaButton);
					logger.write("Inserimento documenti identita cliente - Completed");
				}
				
				//if(!prop.getProperty("TIPO_UTENZA").contentEquals("S2S")) {
				if(util.exists(delega.confermaButton, 5)) {
					throw new Exception("La sezione del delegato non dovrebbe essere visibile per uno scenario non residenziale.");
				}
				
				// Aggiunto Bottone Conferma 6 luglio
				logger.write("Conferma Interlocutore - Start");
				delega.conferma(delega.confermaPagina);
				logger.write("Conferma Interlocutore - Completed");
				
				logger.write("Attendo checklist - Start");
				checkListModalComponent.attendiChecklist();
				logger.write("Attendo checklist - Completed");
				TimeUnit.SECONDS.sleep(5);
				logger.write("Verifica checklist e conferma - Start");
				checkListModalComponent.clickConferma();
				logger.write("Verifica checklist e conferma - Completed");
				
				prop.setProperty("NUMERO_RICHIESTA", intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
				logger.write("NUMERO_RICHIESTA=" + prop.getProperty("NUMERO_RICHIESTA"));

				logger.write("Verifica pulsanti aggiungi e rimuovi fornitura disabilitati - Start");
				
				pod.verificaPulsantiFornituraDisabilitati();
				logger.write("Verifica pulsanti aggiungi e rimuovi fornitura disabilitati - Completed");
				logger.write("Inserimento Pod per verifica - Start");
				logger.write("Inizio inserimento dati POD ELE");
				pod.inserisciPOD(prop.getProperty("POD_ELE"));
				logger.write("Inserimento Pod per verifica - Completed");
				logger.write("Inserimento e Validazione Indirizzo - Start");
				// Inserimento e Validazione Indirizzo
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA_ELE"), prop.getProperty("CITTA_ELE"),
						prop.getProperty("INDIRIZZO_ELE"), prop.getProperty("CIVICO_ELE"));
				
				// *** GESTIONE UBIEST ***************************************************************
//				pod.verificaIndirizzo(pod.verificaIndirizzoButton);
				pod.verificaIndirizzoNew(prop.getProperty("CAP_ELE"));
				logger.write("Inserimento e Validazione Indirizzo - Completed");

				TimeUnit.SECONDS.sleep(10);
				logger.write("Conferma Pod - Start");
				pod.confermaPod(pod.confermaPodButton);
				logger.write("Conferma Pod - Completed");
				TimeUnit.SECONDS.sleep(20);
				logger.write("Verifica popolamento tabella Forniture Inserite - Start");
				pod.verificaTabellaFornitureInserite("ELETTRICO", prop.getProperty("POD_ELE"),
						prop.getProperty("INDIRIZZO_ELE"), "OK", "OK");
				logger.write("Verifica popolamento tabella Forniture Inserite - Completed");

				// Inserimento Dettaglio Sito
				TimeUnit.SECONDS.sleep(10);
				logger.write("Valorizzazione e verifica dei campi Dettaglio Sito - Start");
				// Inserimento e Validazione Indirizzo


				pod.inserisciDettaglioSitoNonResidenziale(prop.getProperty("USO"), 
						prop.getProperty("SOCIETA_DI_VENDITA_ELE"), 
						prop.getProperty("MERCATO_DI_PROVENIENZA_ELE"), 
						prop.getProperty("CATEGORIA_MERCEOLOGICA"), 
						prop.getProperty("POTENZA"), 
						prop.getProperty("TENSIONE"), 
						prop.getProperty("TIPO_MISURATORE"), 
						prop.getProperty("CONSUMO_ANNUO_ELE"));
				
				// Click Conferma Dati Sito
				TimeUnit.SECONDS.sleep(10);
				
				pod.confermaDatiSito(pod.confermaDatiSitoButton);

				logger.write("Valorizzazione e verifica dei campi Dettaglio Sito - Completed");
				
				logger.write("Fine inserimento dati POD ELE");
				
				//Click su aggiungi fornitura
				pod.aggiungiFornitura();
				logger.write("Click su aggiungi fornitura");
				
				//Inserimento POD GAS
				logger.write("Inizio inserimento dati POD GAS");
				
				logger.write("Inserimento Pod per verifica - Start");
				pod.inserisciPOD(prop.getProperty("POD_GAS"));
				logger.write("Inserimento Pod per verifica - Completed");
				logger.write("Inserimento e Validazione Indirizzo - Start");
				// Inserimento e Validazione Indirizzo
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA_GAS"), prop.getProperty("CITTA_GAS"),
						prop.getProperty("INDIRIZZO_GAS"), prop.getProperty("CIVICO_GAS"));
				
				// *** GESTIONE UBIEST ***************************************************************
//				pod.verificaIndirizzo(pod.verificaIndirizzoButton);
				pod.verificaIndirizzoNew(prop.getProperty("CAP_GAS"));
				logger.write("Inserimento e Validazione Indirizzo - Completed");

				TimeUnit.SECONDS.sleep(10);
				logger.write("Conferma Pod - Start");
				pod.confermaPod(pod.confermaPodButton);
				logger.write("Conferma Pod - Completed");
				TimeUnit.SECONDS.sleep(30);
				logger.write("Verifica popolamento tabella Forniture Inserite - Start");
				pod.verificaTabellaFornitureInseriteGas("GAS", prop.getProperty("POD_GAS"),
						prop.getProperty("INDIRIZZO_GAS"), "OK", "OK");
				logger.write("Verifica popolamento tabella Forniture Inserite - Completed");

				// Inserimento Dettaglio Sito
				TimeUnit.SECONDS.sleep(10);
				logger.write("Valorizzazione e verifica dei campi Dettaglio Sito - Start");
				// Inserimento e Validazione Indirizzo
				pod.inserisciDettaglioSitoNonResidenzialeGas( 
						prop.getProperty("SOCIETA_DI_VENDITA_GAS"), 
						prop.getProperty("MATRICOLA_CONTATORE"), 
						prop.getProperty("TIPO_INSTALLAZIONE"), 
						prop.getProperty("CATEGORIA_CONSUMO"), 
						prop.getProperty("CATEGORIA_MARKETING"), 
						prop.getProperty("ORDINE_GRANDEZZA"), 
						prop.getProperty("PROFILO_CONSUMO"), 
						prop.getProperty("POTENZIALITA"), 
						prop.getProperty("UTILIZZO"), 
						prop.getProperty("CONSUMO_ANNUO_GAS"),
						prop.getProperty("MERCATO_DI_PROVENIENZA_GAS"));
				// Click Conferma Dati Sito
				TimeUnit.SECONDS.sleep(10);
//				
				pod.confermaDatiSito(pod.confermaDatiSitoButton);
				
				logger.write("Fine inserimento dati POD GAS");

				logger.write("Valorizzazione e verifica dei campi Dettaglio Sito - Completed");

				TimeUnit.SECONDS.sleep(20);
				logger.write("Conferma Fornitura - Start");
				pod.confermaForniture(pod.confermaFornitureButton);
				logger.write("Conferma Fornitura - Completed");
				

				TimeUnit.SECONDS.sleep(20);
				logger.write("Crea Offerta - Start");

				pod.creaOfferta(pod.creaOffertaButton);
				logger.write("Crea Offerta - Completed");

				TimeUnit.SECONDS.sleep(60);



			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
