package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.FrequentQuestionsComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HeaderComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Storie_FuturE_MediaSectionsComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FrequentQuestions {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
		    RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("Click sulla voce Domande Frequenti - Start");
			HamburgerMenuComponent hamburger = new HamburgerMenuComponent(driver);
			hamburger.clickComponent(hamburger.frequentQuestionsLink);
			Thread.currentThread().sleep(5000);
			FrequentQuestionsComponent question = new FrequentQuestionsComponent(driver);
			question.checkUrl(prop.getProperty("DOMANDE_FREQUENTI_URL"));
			question.checkHeaderPathLinks(question.pathLinks1);
		    question.verifyComponentExistence(question.headerTitle);
			logger.write("Click sulla voce Domande Frequenti - Completed");
			
			logger.write("Verifica Header - Start");
			HeaderComponent login = new HeaderComponent(driver);
			By electricityandgasLabel=login.luceAndGasTab;
			login.verifyComponentExistence(electricityandgasLabel);
			
			By companiesLabel=login.impreseTab;
			login.verifyComponentExistence(companiesLabel);
			
//			By withyouLabel=login.insiemeateTab;
//			login.verifyComponentExistence(withyouLabel);
		    
			By storiesLabel=login.storieTab;
			login.verifyComponentExistence(storiesLabel);
		    
			By footerLabel=login.futureTab;
			login.verifyComponentExistence(footerLabel);
			
			By mediaLabel=login.mediaTab;
			login.verifyComponentExistence(mediaLabel);
			
			By supportLabel=login.supportoTab;
			login.verifyComponentExistence(supportLabel);		
			
			By userIcon=login.iconUser;
			login.verifyComponentExistence(userIcon);
			
			By glassIcon=login.magnifyingglassIcon;
			login.verifyComponentExistence(glassIcon);
			By menuIcon=hamburger.iconMenu;
			hamburger.verifyComponentExistence(menuIcon);
			logger.write("Verifica Header - Completed");
			
			
			logger.write("Verifica Footer - Start");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By linkInformazioniLegali=footer.informazioniLegaliLink;
			footer.verifyComponentExistence(linkInformazioniLegali);
			footer.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=footer.creditsLink;
			footer.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=footer.privacyLink;
			footer.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=footer.cookieLink;
			footer.verifyComponentExistence(linkCookie);
			
			By linkRemit=footer.remitLink;
			footer.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=footer.twitterIcon;
			footer.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=footer.facebookIcon;
			footer.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=footer.youtubeIcon;
			footer.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=footer.linkedinIcon;
			footer.verifyComponentExistence(iconLinkedin);
			
			footer.verifyComponentExistence(footer.instagramIcon);
			footer.verifyComponentExistence(footer.telegramIcon);
			logger.write("Verifica Footer - Completed");
			
			logger.write("Verifica corretta visualizzazione Filtri - Start");
			question.verifyComponentExistence(question.showFilterButton);
			question.verifyComponentExistence(question.orderFilterSelected);
			logger.write("Verifica corretta visualizzazione Filtri - Completed");
			
			logger.write("Verifica corretta visualizzazione Domande frequenti - Start");
			
			question.checkQuestions(question.question1);
			question.checkQuestions(question.question4);
			question.checkQuestions(question.question5);
			question.checkQuestions(question.question6);
			//question.checkQuestions(question.question7);
			question.checkColor(question.disabledprevpage, "DarkGray", "'Prev Button'");
			question.checkColor(question.nextpage, "DeepPink", "'Next Button'");
			logger.write("Verifica corretta visualizzazione Domande frequenti - Completed");
			
			logger.write("Click su next e verifica Next e Prev Button - Start");
			question.clickComponent(question.nextpage);
			question.checkColor(question.nextpage, "DeepPink", "'Next Button'");
			question.checkColor(question.prevpage, "DeepPink", "'Prev Button'");
			logger.write("Click su next e verifica Next e Prev Button - Completed");
			
			logger.write("Click ultima pagina e verifica Next e Prev Button - Start");
			question.changePage("15");
			question.checkColor(question.disablednextpage, "DarkGray", "'Next Button'");
			question.checkColor(question.prevpage, "DeepPink", "'Prev Button'");
			logger.write("Click ultima pagina e verifica Next e Prev Button - Completed");
			
		
			logger.write("Click su Prev e verifica Next e Prev Button- Start");
			question.clickComponent(question.prevpage);
			question.checkColor(question.nextpage, "DeepPink", "'Next Button'");
			question.checkColor(question.prevpage, "DeepPink", "'Prev Button'");
			logger.write("Click su Prev e verifica Next e Prev Button- Completed");			
			
			logger.write("Verifica corretta visualizzazione sezione 'Contattaci' - Start");
			question.verifyComponentExistence(question.haiBisognodiAiutoLabel);
			question.verifyComponentExistence(question.contattaciButton);
			logger.write("Verifica corretta visualizzazione sezione 'Contattaci' - Completed");
			
			logger.write("Click su Contattaci e Verifica - Start");
			question.clickComponent(question.contattaciButton);
			Thread.currentThread().sleep(5000);
			question.checkUrl(prop.getProperty("CONTATTACI_URL"));
			logger.write("Check Url OK : " +prop.getProperty("CONTATTACI_URL"));
			question.checkHeaderPathLinks(question.pathLinks2);
			logger.write("Check HeaderPathLinks OK " );
		    question.verifyComponentExistence(question.headerTitleContatti);
		    logger.write("Check headerTitleContatti OK " );
			question.verifyComponentExistence(question.headerSubTitleContatti);
			logger.write("Click su Contattaci e Verifica - Completed");
			
			logger.write("Back del browser e ritorno a pagina Domande Frequenti - Start");
			driver.navigate().back();
			question.checkUrl(prop.getProperty("DOMANDE_FREQUENTI_URL"));
			logger.write("Back del browser e ritorno a pagina Domande Frequenti - Completed");
			
			logger.write("Effettuare l'ordinamento per RILEVANZA ( a destra sul filtro ORDINA PER ) - Start");
			question.selectFilter("Rilevanza");
			question.checkQuestions(question.questionOrderedByRelevance);
			logger.write("Effettuare l'ordinamento per RILEVANZA ( a destra sul filtro ORDINA PER ) - Completed");
			
			logger.write("Effettuare l'ordinamento per MENO RECENTE ( a destra sul filtro ORDINA PER ) - Start");
			question.selectFilter("Meno recente");
			question.checkQuestions(question.questionOrderedLessRecent);
			logger.write("Effettuare l'ordinamento per MENO RECENTE ( a destra sul filtro ORDINA PER ) - Completed");
			
			
			logger.write("Click Mostra filtri e verifica - Start");
            question.clickComponent(question.showFilterButton);
            question.checkFilters(question.filterGroup1,"Categoria", question.groupFilters1);
            question.checkFilters(question.filterGroup1,"Tipologia", question.groupFilters2);
            question.checkFilters(question.filterGroup1,"Data di pubblicazione", question.groupFilters3);
            question.checkFilters(question.filterGroup2,"Seleziona periodo", question.groupFilters4);
			logger.write("Click Mostra filtri e verifica - Completed");
			
			logger.write("Verifica pulsanti Annulla e Applica - Start");
			question.verifyComponentExistence(question.annullaFilterButton);
			question.verifyComponentExistence(question.applicaFilterButton);
			logger.write("Verifica pulsanti Annulla e Applica - Completed");
			
			logger.write("Click su Annulla e verifica non applicazione filtri - Start");
			question.clickComponent(question.annullaFilterButton);
			question.checkQuestions(question.questionOrderedLessRecent);
			logger.write("Click su Annulla e verifica non applicazione filtri - Completed");
			
			
			//Dopo aver selezionato un filtro e fatto le opportune verifiche, rimuovo prima il filtro precedente e proseguo
			
			logger.write("Applica filtro Offerte e verifica - Start");
			question.clickComponent(question.showFilterButton);
			question.clickFilter(question.filterGroup1,"Tipologia", "Offerte");
			question.clickComponent(question.applicaFilterButton);
			question.checkQuestions(question.questionOfferteFilter);
			question.clickFilter(question.filterGroup1,"Tipologia", "Offerte");
			logger.write("Applica filtro Offerte e verifica - Completed");
			

//			logger.write("Applica filtro Ultimo Mese e verifica - Start");
//			question.clickFilter(question.filterGroup1,"Data di pubblicazione", "Ultimo mese");
//			question.clickComponent(question.applicaFilterButton);
//			question.checkNumOfQuestions(0);
//			logger.write("Applica filtro Ultimo Mese e verifica - Completed");
			
			logger.write("Annullo filti - Start");
			question.clickComponent(question.annullaFilterButton);
            question.clickComponent(question.showFilterButton);
			logger.write("Annullo filti - Completed");
			
			logger.write("Applica filtro Contratto e verifica - Start");
			question.clickFilter(question.filterGroup1,"Categoria", "contratto");
			question.clickComponent(question.applicaFilterButton);
			question.checkQuestions(question.questionContrattoFilter);
			logger.write("Applica filtro Contratto e verifica - Completed");
			
//
//			logger.write("Applica filtro Contratto e verifica - Start");
//			question.clickFilter(question.filterGroup1,"Data di pubblicazione", "Ultimo anno");
//			question.clickFilter(question.filterGroup1,"Categoria", "contratto");
//			question.clickComponent(question.applicaFilterButton);
//			question.checkQuestions(question.questionContrattoFilter);
//			logger.write("Applica filtro Contratto e verifica - Completed");
			
			logger.write("Applica filtro Bolletta e verifica - Start");
			//question.clickFilter(question.filterGroup1,"Categoria", "Contratto");
			question.clickFilter(question.filterGroup1,"Tipologia", "Bolletta");
			question.clickFilter(question.filterGroup1,"Tipologia", "Contatori");
			question.clickFilter(question.filterGroup1,"Categoria", "contratto");
			question.clickComponent(question.applicaFilterButton);
			question.checkQuestions(question.questionBollettaContatoriFilters);
			logger.write("Applica filtro Contratto e verifica - Completed");
			
			
			logger.write("Selezionare la pagina corretta e click sulla domanda BOLLETTA Come leggere la bolletta luce Enel Energia - Start");
			question.clickFilter(question.filterGroup1,"Tipologia", "Bolletta");
			question.clickFilter(question.filterGroup1,"Tipologia", "Contatori");
			question.clickComponent(question.applicaFilterButton);
//			if(!question.searchQuestionInPage(By.xpath(question.genericQuestion.replace("#", "Come leggere la bolletta luce Enel Energia")))) throw new Exception("Impossibile trovare la domanda 'Come leggere la bolletta luce Enel Energia");
//			question.clickComponent(By.xpath(question.genericQuestion.replace("#", "Come leggere la bolletta luce Enel Energia")));
//			question.checkUrl(prop.getProperty("LEGGERE_BOLLETTA_URL"));
//			question.checkHeaderPathLinks(question.pathLinks3);
//		    question.verifyComponentExistence(question.headerTitleLeggereBolletta);
//			logger.write("Selezionare la pagina corretta e click sulla domanda BOLLETTA Come leggere la bolletta luce Enel Energia - Completed");
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
