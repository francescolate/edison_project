package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.components.colla.ModConsensiACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACBContatti168 {

	public static void main(String[] args) throws Exception {
		
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		ModConsensiACRComponent mod = new ModConsensiACRComponent(driver);

		logger.write("Click on Contatti item in the menu on the left - Starts");
		Thread.currentThread();
		Thread.sleep(3000);
		mod.clickComponent(mod.contatti);
		Thread.sleep(15000);
		mod.comprareText(mod.contattiPath1, ModConsensiACRComponent.CONTATTI_PATH1, true);
		mod.comprareText(mod.contattiPath2, ModConsensiACRComponent.CONTATTI_PATH2, true);
		mod.comprareText(mod.contattiHeading, ModConsensiACRComponent.CONTATTI_HEADING, true);
		mod.comprareText(mod.contattiParagraph, ModConsensiACRComponent.CONTATTI_PARAGRAPH, true);
		logger.write("Click on Contatti item in the menu on the left - Ends");
		
		logger.write("Verify the following fields are visible- Starts");
		mod.verifyComponentExistence(mod.seiInteressato);
		mod.verifyComponentExistence(mod.tipologiaOfferta);
		mod.verifyComponentExistence(mod.provincia);
		mod.verifyComponentExistence(mod.canaleContattoPreferito);
		mod.verifyComponentExistence(mod.email);
		mod.verifyComponentExistence(mod.telefonFisso);
		mod.verifyComponentExistence(mod.cellulare);
		logger.write("Verify the following fields are visible- Ends");
		
		logger.write("Check value list for Sei Interessato a*- Starts");
		mod.clickComponent(mod.seiInteressato);
		mod.verifyDropDownValue(mod.seiInteressato,mod.seiInteressatoList,ModConsensiACRComponent.INTERESSATO_VALUES);
		logger.write("Check value list for Sei Interessato a*- Ends");
		
		logger.write("Check value list for Tipologia Offerta- Starts");
		mod.clickComponent(mod.tipologiaOfferta);
		mod.verifyDropDownValue(mod.tipologiaOfferta, mod.tipologiaOffertaList, ModConsensiACRComponent.TIPOLOGIA_OFFERTA_VALUES);
		logger.write("Check value list for Tipologia Offerta- Ends");
		
		Thread.sleep(5000);
		logger.write("Check value list for Provincia*-Insert ROMA - Starts");
		prop.setProperty("PROVINCIA", "ROMA");
		mod.enterInputParameters(mod.provincia, prop.getProperty("PROVINCIA"));
		Thread.sleep(5000);
		mod.verifyComponentExistence(mod.roma);
		logger.write("Check value list for Provincia*-Insert ROMA - Ends");
		
		logger.write("Check value list for Provincia*-Insert MILANO - Starts");
		prop.setProperty("PROVINCIA", "MILANO");
		mod.enterInputParameters(mod.provincia, prop.getProperty("PROVINCIA"));
		Thread.sleep(5000);
		mod.verifyComponentExistence(mod.milano);
		logger.write("Check value list for Provincia*-Insert MILANO - Ends");
		
		logger.write("Check value list for Provincia*-Insert CESENA - Starts");
		prop.setProperty("PROVINCIA", "CESENA");
		mod.enterInputParameters(mod.provincia, prop.getProperty("PROVINCIA"));
		Thread.sleep(5000);
		mod.verifyComponentExistence(mod.cesena);
		logger.write("Check value list for Provincia*-Insert CESENA - Ends");
		
		logger.write("Verify the following fields are visible- Starts");
		mod.verifyComponentExistence(mod.email);
		mod.verifyComponentExistence(mod.telefonFisso);
		mod.verifyComponentExistence(mod.cellulare);
		logger.write("Verify the following fields are visible- Ends");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
