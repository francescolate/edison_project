package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ContactComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_97_Form_Scrivici {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		String login = "https://www-coll1.enel.it/it/login";
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			ContactComponent con = new ContactComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			Thread.sleep(3000);
			logger.write("Click on support -- Start");
			con.clickComponent(con.supporto);
			logger.write("Click on support -- Completed");
			Thread.sleep(3000);
			con.verifyComponentExistence(con.contrattiEModulistica);
			Thread.sleep(30000);
			con.clickComponent(con.contrattiEModulistica);
			Thread.sleep(5000);
			con.verifyComponentExistence(con.contattaci);
			logger.write("Click on contattaci and verify page navigation-- Start");
			con.clickComponent(con.contattaci);			
			con.checkURLAfterRedirection(con.contattaciUrl);
			con.comprareText(con.contattaciTitle, con.ContattaciTitle, true);
			con.comprareText(con.contattaciSubtitle, con.ContattaciSubtitle, true);
			con.comprareText(con.contattaciPath, con.ContattaciPath, true);
			con.verifyComponentExistence(con.perINostriClienti);
		//	con.comprareText(con.text, con.Text, true);
			logger.write("Click on contattaci and verify page navigation-- Completed");

			logger.write("Click on link qui and verify page navigation-- Start");
			Thread.sleep(30000);
			con.clickComponent(con.qui);
			con.checkURLAfterRedirection(con.serviziOnlineUrl);
			con.comprareText(con.serviziOnlineText, con.ServiziOnlineText, true);
			con.clickComponent(con.cliccaQui);
			logger.write("Click on link qui and verify page navigation-- Completed");

			con.verifyComponentExistence(con.contattiTitle);
			con.checkURLAfterRedirection(con.contattiUrl);
			con.verifyComponentExistence(con.section1);
			con.verifyComponentExistence(con.section2);
			con.verifyComponentExistence(con.section3);
			con.clickComponent(con.siGiaClientiNo);
			con.verifyArgumentoDropdownValues(con.dropdownValues, con.argumentoDropdown);
			
			OreFreeDettaglioComponent ofdf = new OreFreeDettaglioComponent(driver);
			Robot robot = new Robot();									
			
			con.clickComponent(con.nomeassociazione);
			con.clickComponent(con.codici);
			
			String cfAssociazione = driver.findElement(con.cfAssociazione).getText();
			prop.setProperty("CFASSOCIAZIONE", cfAssociazione);
			con.enterInputParameters(con.cfDelegato, prop.getProperty("CF_DELEGATO"));
			con.enterInputParameters(con.nominativoDelegato, prop.getProperty("NOMINATIVO_DELEGATO"));
			con.enterInputParameters(con.email, prop.getProperty("EMAIL"));
			con.enterInputParameters(con.cellulare, prop.getProperty("TELEPONE"));
			
			String prefix = driver.findElement(con.prefix).getText();
			prop.setProperty("PREFIX", prefix);
			con.enterInputParameters(con.nome, prop.getProperty("NOME"));
			con.enterInputParameters(con.cognome, prop.getProperty("COGNOME"));
			con.enterInputParameters(con.cf, prop.getProperty("CF"));
		//	con.selectValueFromDropdown(con.argumentoDropdown, prop.getProperty("ARGOMENTO"));
		
			for (int i=0; i<=11; i++){
				ofdf.clickComponent(ofdf.ddvaluecontainer);
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(2000);
				}
			
			con.enterInputParameters(con.messagio, prop.getProperty("MESSAGGIO"));
			
			con.clickComponent(con.no);
			con.clickComponent(con.acceptto);
			con.clickComponent(con.prosegui);
			
			con.clickComponent(con.popupYes);
			con.verifyComponentExistence(con.contattiTitle);
			con.comprareText(con.successMsg, con.SuccessMsg, true);
			con.verifyComponentExistence(con.indetro);

			Thread.sleep(120000);
			prop.setProperty("RETURN_VALUE", "OK");
		}	
			catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

			} finally {
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}
	}
}
