package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BolleteBSNComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_251_Menu_Lateral {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Verify left menu items is displaying- Start");
			pa.leftMenuItemsDisplay(pa.leftMenuItems);
			logger.write("Verify left menu items is displaying- Completed");
			pa.isElementSelected(pa.homeMenu);
			Thread.sleep(5000);
			logger.write("Verify home page title and sub text- Start");
			pa.verifyComponentExistence(pa.pagePath);
			pa.comprareText(pa.pageTitle, pa.PageTitle, true);
			logger.write("Verify home page title and sub text- Completed");

			logger.write("Verify pannel one details- Start");
			pa.verifyComponentExistence(pa.fornitureebollette);
	//		pa.comprareText(pa.panel1, pa.Panel1Text, true);			
			pa.comprareText(pa.latuaFornituranelDettaglio, pa.latuaFornituranelDettaglioText, true);
			pa.comprareText(pa.latuaFornituranelDettaglioValue1, pa.LatuaFornituranelDettaglioValue1, true);
			pa.comprareText(pa.latuaFornituranelDettaglioValue2, pa.LatuaFornituranelDettaglioValue2, true);
//			pa.comprareText(pa.latuaFornituranelDettaglioValue3, pa.LatuaFornituranelDettaglioValue3, true);
//			pa.comprareText(pa.latuaFornituranelDettaglioValue4, pa.LatuaFornituranelDettaglioValue4, true);
			pa.verifyComponentExistence(pa.piuInformazioniLink);
			logger.write("Verify pannel one details- Completed");

			logger.write("Verify bill details column names- Start");
			pa.verifyComponentExistence(pa.letuebollete);
			pa.verifyComponentExistence(pa.numero);
			pa.verifyComponentExistence(pa.importo);
			pa.verifyComponentExistence(pa.stato);
			pa.verifyComponentExistence(pa.scadenza);
			logger.write("Verify bill details column names- Completed");

			logger.write("Verify bill details - Start");
//			pa.comprareText(pa.bill1, pa.BillOneDetails, true);
//			pa.comprareText(pa.bill2, pa.BillTwoDetails, true);
//			pa.comprareText(pa.bill3, pa.BillThreeDetails, true);
			pa.verifyComponentExistence(pa.vaiAllElencoBollette);
			logger.write("Verify bill details - Completed");

			logger.write("Click on Piu Informazioni Link- Start");
			pa.clickComponent(pa.piuInformazioniLink);
			logger.write("Click on Piu Informazioni Link- Completed");
			Thread.sleep(5000);

			FornitureBSNComponent fc = new FornitureBSNComponent(driver);
			logger.write("Verify page navigation and Furniture page details- Start");
			pa.verifyComponentExistence(fc.path);
			fc.comprareText(fc.pageTitle, fc.PageTitle, true);
			fc.comprareText(fc.titleSubText, fc.TitleSubText, true);
			pa.verifyComponentExistence(fc.forniture);
			logger.write("Verify page navigation and Furniture page details- Completed");

			driver.navigate().back();
			logger.write("Verify home page title and sub text- Start");
			pa.verifyComponentExistence(pa.pagePath);
			pa.comprareText(pa.pageTitle, pa.PageTitle, true);
			logger.write("Verify home page title and sub text- Completed");

			logger.write("Click on Vai All Elenco Bollette link - Start");
			pa.clickComponent(pa.vaiAllElencoBollette);
			logger.write("Click on Vai All Elenco Bollette link - Completed");
			Thread.sleep(5000);
			
			BolleteBSNComponent bc = new BolleteBSNComponent(driver);
			logger.write("Verify page navigation and Bollete page details- Start");
			pa.verifyComponentExistence(bc.path);
			pa.verifyComponentExistence(bc.pageTitle);
			Thread.sleep(10000);
			bc.comprareText(bc.titleSubText, bc.TitleSubText, true);
//			bc.comprareText(bc.pageText, bc.PageText, true);
			logger.write("Verify page navigation and Bollete page details- Start");

			driver.navigate().back();
			logger.write("Verify home page title and sub text- Start");
			pa.verifyComponentExistence(pa.pagePath);
			pa.comprareText(pa.pageTitle, pa.PageTitle, true);
			logger.write("Verify home page title and sub text- Completed");

			logger.write("Verify Banner details in home page- Start");
			pa.verifyComponentExistence(pa.vantaggiperil);
//			pa.comprareText(pa.banner1, pa.BannerOneTxt, true);
			pa.comprareText(pa.banner2, pa.BannerTwoText, true);
			logger.write("Verify Banner details in home page- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
