package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_1_RES_SWA_ELE_PROD_Module {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		int counter = 0; boolean exitCondition;
		QANTTLogger logger = new QANTTLogger(prop);
		try{
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OCR_Module_Component omc = new OCR_Module_Component(driver);
			Thread.sleep(6000);
			
//			do{
//				if(!omc.getElementHref(omc.offerDetailsButton).equals(prop.get("OCR_OFFER_LINK"))){
//					counter++;
//					Thread.sleep(5000);
//				}else
//					counter=10;
//			}while(counter<10);
//			
//			omc.verifyComponentExistence(omc.offerDetailsButton);
//			omc.clickComponent(omc.offerDetailsButton);
			
			omc.verifyComponentExistence(omc.offerActivationButton);
			omc.clickComponent(omc.offerActivationButton);
			
			Thread.sleep(1000);
			
			try {
				driver.manage().window().maximize();
				driver.navigate().to("https://www.enel.it/it/bd_npa_contratto?productType=res&zoneid=e-light_bioraria-hero");
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + prop.getProperty("OCR_OFFER_LINK"));

			}
			
			omc.verifyComponentExistence(omc.compilaManualmenteButton);
			omc.clickComponent(omc.compilaManualmenteButton);
			
			omc.verifyComponentExistence(omc.firstnameInputField);
			omc.clearAndSendKeys(omc.firstnameInputField, prop.getProperty("FIRSTNAME"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.lastnameInputField);
			omc.clearAndSendKeys(omc.lastnameInputField, prop.getProperty("LASTNAME"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.fiscalCodeInputField);
			omc.clearAndSendKeys(omc.fiscalCodeInputField, prop.getProperty("CF"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.mobileInputField);
			omc.clearAndSendKeys(omc.mobileInputField, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.emailInputField);
			omc.clearAndSendKeys(omc.emailInputField, prop.getProperty("EMAIL"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.emailConfirmationInputField);
			omc.clearAndSendKeys(omc.emailConfirmationInputField, prop.getProperty("EMAIL"));
			Thread.sleep(1000);
			omc.verifyComponentExistence(omc.privacyCheckbox);
			omc.clickComponent(omc.privacyCheckbox);
			
			Thread.sleep(1000);
			
			omc.verifyComponentExistence(omc.continueButton);
			omc.clickComponent(omc.continueButton);
			
			System.out.println("Informazioni fornitura - Start");
			omc.informazioniFornituraLuce();
			omc.selectBisogno("CAMBIO FORNITORE");
			omc.enterPodCap(prop.getProperty("POD_PDR"),prop.getProperty("CAP"), "city");
			omc.indirizzoFornitura(prop.getProperty("CITTA"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"), prop.getProperty("CAP"), prop.getProperty("ATTUALE_FORNITORE"), true, true);
			System.out.println("Informazioni fornitura - Completed");
			
			System.out.println("Pagamenti e bollette - Start");
			omc.pagamentiBollette(prop.getProperty("EMAIL"), prop.getProperty("MOBILE_NUMBER"));
			omc.selectMOP("Bollettino Postale");
			omc.clickComponent(omc.continueButton1);
			System.out.println("Pagamenti e bollette - Completed");
			
			System.out.println("Mandati e Consensi - Start");
			omc.checkMandatiConsensiLuce();
			System.out.println("Mandati e Consensi - Completed");
			
			
			
			
		
			
			
			/*
			
			omc.verifyComponentExistence(omc.podPdr);
			omc.clearAndSendKeys(omc.podPdr, prop.getProperty("POD_PDR"));
			
			omc.verifyComponentExistence(omc.cap);
			omc.clearAndSendKeys(omc.cap, prop.getProperty("CAP"));
			omc.clickComponent(omc.cap);
//			omc.robotSendKeys();
			
			Thread.sleep(1000);
			
			omc.verifyComponentExistence(omc.capDropdown);
			omc.clickComponent(omc.capDropdown);
			
			omc.verifyComponentExistence(omc.proceedButton_Step2);
			omc.clickComponent(omc.proceedButton_Step2);
			
			*/
			
			prop.setProperty("RETURN_VALUE", "OK");

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
	public static void closeChat(OCR_Module_Component omc) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, 1))
			omc.jsClickComponent(omc.chatCloseButton);
	}
	
	public static void closeChat(OCR_Module_Component omc, int timeout) throws Exception{
		if(omc.verifyExistence(omc.chatCloseButton, timeout))
			omc.jsClickComponent(omc.chatCloseButton);
	}
}
