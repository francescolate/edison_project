package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.CarrelloComponent;
import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class AzioniSezioneCarrelloBusiness {

    public static void main(String[] args) throws Exception {
        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            logger.write("seleziona la voce 'Elettrico - Business' nella sezione 'Carrello' - Start");
            CarrelloComponent carrello = new CarrelloComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
            
            carrello.verifyComponentExistence(carrello.voceElettricoBusiness);
            carrello.clickWithJS(carrello.voceElettricoBusiness);

            gestione.checkSpinnersSFDC();
            TimeUnit.SECONDS.sleep(1);
            logger.write("seleziona la voce 'Elettrico - Business' nella sezione 'Carrello' - Completed");
            logger.write("seleziona come prodotto l'offerta " + prop.getProperty("PRODOTTO") + " e aggiungerla al carrello - Start");
            carrello.clickWithJS(carrello.buttonCercaChiuso);
            TimeUnit.SECONDS.sleep(1);
            carrello.insertValore(carrello.inputNomeProdotto, prop.getProperty("PRODOTTO"));
            carrello.clickComponent(carrello.cercaButton);
            gestione.checkSpinnersSFDC();
            carrello.aggiungiAlCarrello(prop.getProperty("PRODOTTO"));
            gestione.checkSpinnersSFDC();
            logger.write("seleziona come prodotto l'offerta " + prop.getProperty("PRODOTTO") + " e aggiungerla al carrello - Completed");
            logger.write("corretta apertura pagina di configurazione del prodotto, verifica presenza della voce 'Opzione KAM_AGCOR' e delle sottovoci, click su SALVA e poi su CHECKOUT  - Start");
            carrello.verifyComponentExistence(carrello.pageConfigurazioni);


        	if (!prop.getProperty("OPZIONE_KAM_AGCOR","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR");
			}
			if (!prop.getProperty("SCELTA_ABBONAMENTI","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO"),prop.getProperty("SCELTA_ABBONAMENTI"));
				logger.write("Selezione Scelta Abbonamenti");
			}
                      
            
            carrello.clickComponent(carrello.buttonSalva);
            gestione.checkSpinnersSFDC();
            carrello.clickWithJS(carrello.buttonCheckout);
            gestione.checkSpinnersSFDC();
            carrello.checkPopupCheckoutAndClickConferma();
            gestione.checkSpinnersSFDC();
            carrello.checkoutEffettuato();
            logger.write("corretta apertura pagina di configurazione del prodotto, verifica presenza delle voci 'Componenti Prezzi Energia' e delle sottovoci, click su SALVA e poi su CHECKOUT  - Start");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());

            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
