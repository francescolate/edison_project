package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AttivitaComponent;
import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CaricaEValidaDocumentiCambioUso {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			//PATH_DOCUMENTO;DOCUMENTI_DA_VALIDARE;CAUSALE_CONTATTO_ATTIVITA;DESCRIZIONE_ATTIVITA;SPECIFICA_ATTIVITA;STATO_ATTIVITA;POD
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		DocumentValidatorComponent validator = new DocumentValidatorComponent(driver);
		driver.switchTo().defaultContent();
		DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
		logger.write("Upload Documento - Start");
		doc.uploadFile(Utility.getDocumentPdf(prop));
		logger.write("Upload Documento - Completed");
		TimeUnit.SECONDS.sleep(10);
		doc.visualizzaDocumenti();
		TimeUnit.SECONDS.sleep(20);
		logger.write("Visualizza Documenti - Completed");
		 
		logger.write("Valida Documenti - Start");
//		validator.verificaValidaDoc(util.getFrameByIndex(2),prop.getProperty("DOCUMENTI_DA_VALIDARE"));
		validator.verificaValidaDoc(prop.getProperty("DOCUMENTI_DA_VALIDARE"));
		logger.write("Valida Documenti - Completed");
		TimeUnit.SECONDS.sleep(10);
//		// cambio STATO_RICHIESTA per verifiche finali dopo Validazione Documentazione
//		prop.setProperty("STATO_RICHIESTA", "CHIUSA DA CONFERMARE");
//		if(prop.getProperty("VALIDA_DOCUMENTI", "N").equals("N")){
//			new GestioneAppuntamentoComponent(driver).esci();
//		}

		AttivitaComponent attivita = new AttivitaComponent(driver);
		attivita.press(attivita.modificaAttivita);
		attivita.setActivity(prop.getProperty("CAUSALE_CONTATTO_ATTIVITA"), prop.getProperty("DESCRIZIONE_ATTIVITA"), prop.getProperty("SPECIFICA_ATTIVITA"), prop.getProperty("STATO_ATTIVITA"), prop.getProperty("POD"));
		
		logger.write("Chiudi Validator Documenti - Start");
		validator.chiudi();
		logger.write("Chiudi Validator Documenti - Completed");
		
		prop.setProperty("RETURN_VALUE", "OK");
	} catch (Exception e) {
		prop.setProperty("RETURN_VALUE", "KO");
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: "+errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
		if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

	}finally
	{
		//Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
	}

}
}
