package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetSubentro15R2DGAS {
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")){
                prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
                prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
                prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
                prop.setProperty("TIPO_OPERAZIONE", "SUBENTRO_R2D");
                prop.setProperty("SKIP_POD", "N");
                prop.setProperty("DISTRIBUTORE_RD2_ATTESO_GAS","ITALGAS RETI SPA");
                prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Subentro");
                prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
                prop.setProperty("EVENTO_FA_GAS","Anagrafica SWA");
                prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
                prop.setProperty("STATO_R2D", "AW");
                prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Subentro");
                prop.setProperty("INDICE_POD", "1");
                prop.setProperty("CP_GESTORE", "12345");
                prop.setProperty("RIGA_DA_ESTRARRE","1");
                prop.setProperty("MATRICOLA_CONTATORE", "1234567890");
                prop.setProperty("LETTURA_CONTATORE", "2222");
                prop.setProperty("ANNO_COSTRUZIONE", "2020");
                prop.setProperty("COEFFICENTE_C", "1");
                prop.setProperty("ACCESSIBILITA_229", "1 (Accessibile)");                
                prop.setProperty("STATO_CONTATORE", "1 (APERTO)");
                prop.setProperty("CLASSE_CONTATORE", "G0004");
                prop.setProperty("ECCEZIONI_LISTINO", "Tariffa estratta - 30");
            }

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
