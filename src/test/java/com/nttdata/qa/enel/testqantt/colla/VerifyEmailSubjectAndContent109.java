package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OffertaComponent;
import com.nttdata.qa.enel.components.colla.OffertagasComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyEmailSubjectAndContent109 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String content = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            content = gm.recuperaLink(prop, "ID_108_SWA_GAS_BSN", false);
		            Thread.sleep(5000);
		            System.out.println(content);
		            OffertagasComponent oc =new  OffertagasComponent(driver);
					oc.verifyEmailContent(content, oc.EmailContent109Line1);
					oc.verifyEmailContent(content, oc.EmailContent109Line2);
					oc.verifyEmailContent(content, oc.EmailContent109Line3);
					oc.verifyEmailContent(content, oc.EmailContent109Line4);
					oc.verifyEmailContent(content, oc.EmailContent109Line5);
					oc.verifyEmailContent(content, oc.EmailContent109Line6);
					oc.verifyEmailContent(content, oc.EmailContent109Line7);
					oc.verifyEmailContent(content, oc.EmailContent109Line8);
					
				     String endingLimit = "\" aria-label=\"Clicca per confermare la tua offerta:";
				        String startingLimit = "<a href=\"";
				       
			            String link = content.substring((content.indexOf(startingLimit)+9),content.indexOf( endingLimit)+0);
			            
			            System.out.println(link);
			            logger.write("Recupera Link - Completed");		            
			            
					}
				
					prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
          	StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
