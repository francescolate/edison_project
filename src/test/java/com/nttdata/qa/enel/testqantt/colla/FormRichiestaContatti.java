package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.colla.FormRichiestaContattiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FormRichiestaContatti {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);

			FormRichiestaContattiComponent frc = new FormRichiestaContattiComponent(driver);

			//STEP 4
			//Click contatti
			By button_contatti = frc.button_contatti;
			frc.verifyComponentExistence(button_contatti);
			frc.clickComponentWithJse(button_contatti);

			//Corretta apertura della pagina
			By text_path = frc.text_path;
			frc.verifyComponentText(text_path, "AREA RISERVATA RICHIESTA CONTATTO");

			By text_titolo = frc.text_titolo;
			frc.verifyComponentText(text_titolo, "Richiesta contatto");

			By text_lead = frc.text_lead;			
			frc.verifyComponentText(text_lead, "Gentile cliente, compilando il modulo sottostante la richiesta verrà inoltrata ad un nostro consulente. Verrai ricontattato al più presto.");
			
			//STEP 5
			//Interessi
			By id_richiesta = frc.id_richiesta;
//			By select_interessi = /*new ByChained(*/By.className("customselect-choice");//, By.xpath("//input[@name='interessato' and @value='Nuova attivazione']"));
			By select_interessi = frc.select_interessi;
			By full_select_richiesta_interessi = new ByChained(id_richiesta , select_interessi);
			frc.clickComponent(full_select_richiesta_interessi);
			
//			By id_offerta = By.id("prefofferta_key");
			By interessi_new_activation = new ByChained(select_interessi, By.xpath("//input[@name='interessato']"),  By.xpath("//span[text()='Nuova attivazione']"));
//			By full_select_activation_interessi = new ByChained(id_offerta , interessi_new_activation);

			frc.clickComponent(interessi_new_activation);

			//Tipologia Offerta
//			By id_offerta = frc.id_offerta;
//			By select_offer_type = /*new ByChained(*/By.className("customselect-choice");//, By.xpath("//input[@name='tipologiaofferta' and @value='Nuova attivazione']"));
//			By select_offer_type = frc.select_offer_type;
			By full_select_activation_interessi = frc.full_select_activation_interessi;
			frc.clickComponent(full_select_activation_interessi);

			By offer_luce = frc.offer_luce;
			frc.clickComponent(offer_luce);

			//Provincia
			By text_provincia = frc.text_provincia;

			frc.verifyComponentExistence(text_provincia);
			frc.insertText(text_provincia, "Roma");

//			By select_city = By.className("ui-autocomplete ui-front ui-menu ui-widget ui-widget-content");
//			By roma_city = new ByChained(select_city , By.xpath("//li[text()='ROMA']"));

			By first_result_city = frc.first_result_city;

			frc.verifyComponentExistence(first_result_city);
			frc.clickComponent(first_result_city);

			//Canale contatto preferito
//			By id_contact = frc.id_contact;
//			By select_contact = frc.select_contact;
			By full_select_contact = frc.full_select_contact;
			frc.clickComponent(full_select_contact);

			By contact_email = frc.contact_email;
			frc.clickComponent(contact_email);

			//Inserisci Email
			By text_email = frc.text_email;

			frc.verifyComponentExistence(text_email);
			frc.insertText(text_email, prop.getProperty("EMAIL_CONTATTO"));

			//Accetto Privacy 
			By radio_block = frc.radio_block;

			frc.verifyComponentExistence(radio_block);

			By radio_button_accetto = frc.radio_button_accetto;
			frc.verifyComponentExistence(radio_button_accetto);
			frc.clickComponent(radio_button_accetto);
			
			//Corretto popolamento			
			frc.verifyComponentText(full_select_richiesta_interessi, "Nuova attivazione");			
			frc.verifyComponentText(full_select_activation_interessi , "Luce");			
			frc.verifyComponentValue(text_provincia , "Roma");			
			frc.verifyComponentText(full_select_contact , "Email");
			
			//STEP 6
			By button_invia = frc.button_invia;
			frc.verifyComponentExistence(button_invia);
			frc.clickComponent(button_invia);
					
			//Corretta visualizzazione
			frc.verifyComponentText(text_path, "AREA RISERVATA RICHIESTA CONTATTO");			
			frc.verifyComponentText(text_titolo, "Richiesta contatto");			
			//frc.verifyComponentText(text_lead, "Operazione eseguita correttamente");
			prop.setProperty("RETURN_VALUE", "OK");
			

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
