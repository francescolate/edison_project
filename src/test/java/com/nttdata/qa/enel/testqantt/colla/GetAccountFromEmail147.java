package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CustomerDetailsPageComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromEmailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class GetAccountFromEmail147 {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			GetAccountFromEmailComponent gf = new GetAccountFromEmailComponent(driver);
			logger.write("Verify search bar-- start");
			gf.verifyComponentExistence(gf.searchBar);
			logger.write("Verify search bar-- Completed");
			logger.write("Search for user by entering POD-- start");
			gf.searchAccount(gf.searchBar, prop.getProperty("WP_USERNAME"), true);
			logger.write("Search for user by entering POD-- Completed");

			CustomerDetailsPageComponent cc = new CustomerDetailsPageComponent(driver);
        	
			cc.verifyComponentExistence(cc.richestaHeader1);
			cc.jsClickObject(cc.richestaHeader1);
        	
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			System.out.println("xpath "+By.xpath(gf.request147.replace("$DATE$", dtf.format(now))));
			gf.verifyComponentExistence(By.xpath(gf.request147.replace("$DATE$", dtf.format(now))));
			System.out.println("xpath "+By.xpath(gf.request147.replace("$DATE$", dtf.format(now))));
			logger.write("Click on user-- start");
			gf.clickComponent(By.xpath(gf.request147.replace("$DATE$", dtf.format(now))));
			logger.write("Click on user-- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
