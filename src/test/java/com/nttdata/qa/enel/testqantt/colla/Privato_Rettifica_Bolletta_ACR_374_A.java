package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.RettificaBollettaComponent ;
//import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Rettifica_Bolletta_ACR_374_A {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			RettificaBollettaComponent  rbc = new RettificaBollettaComponent (driver);
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			rbc.comprareText(rbc.homepageDetail1, rbc .HOMEPAGEDETAIL, true);
			rbc.comprareText(rbc.homepageheadingdetail, rbc.HOMEPAGEHEADINGDETAIL, true);
			rbc.comprareText(rbc.homepageheadingdetail2, rbc .HOMEPAGEHEADINGDETAIL2, true);
			rbc.comprareText(rbc.homepageParagraphtext, rbc .HOMEPAGE_PARAGRAPHTEXT, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");			
			logger.write("Click on service and verify the Supply service section -- Starts");
			rbc.clickComponent(rbc.Servizi);
			rbc.verifyComponentExistence(rbc.serviceTitle);	
			Thread.sleep(20000);
			logger.write("Click on click on Rettifica Bollette -- start");
			rbc.verifyComponentExistence(rbc.RettificaBolletta);
			rbc.clickComponent(rbc.RettificaBolletta);
			rbc.comprareText(rbc.RettificaBollettaPageTitle, rbc.RETTIFICABOLLETTAPAGETITLE, true);
			rbc.comprareText(rbc.RettificaBollettaPageText, rbc.RETTIFICABOLLETTAPAGETEXT, true);
			Thread.sleep(20000);
			logger.write("Click on click on Rettifica Bollette -- completed");
			logger.write("Verify RettificaBollette information and supply details-- Starts");
			rbc.verifyComponentExistence(rbc.Indirizzodellafornitura);
			rbc.verifyComponentExistence(rbc.NumeroCliente);
			rbc.verifyComponentExistence(rbc.linkStoricoLetture);
			rbc.verifyComponentExistence(rbc.esciButton);
			rbc.verifyComponentExistence(rbc.rettificiabutton);
			logger.write("Verify RettificaBollette information and supply details-- Completed");
			logger.write("Verify the popup dispalayed and left menu items- Starts");
			rbc.clickComponent(rbc.bollette);
		    rbc.verifyComponentExistence(rbc.Attenzione);
			rbc.verifyComponentExistence(rbc.Seisicurotext);
			rbc.comprareText(rbc.Seisicurotext,rbc.SEISICUROTEXT, true);
			rbc.verifyComponentExistence(rbc.popUpInnerText);
			rbc.verifyComponentExistence(rbc.popUpHeading);
			rbc.verifyComponentExistence(rbc.si);
			rbc.verifyComponentExistence(rbc.no);
		    rbc.clickComponent(rbc.si);
		    logger.write("Verify the popup dispalayed and left menu items- Completed");
			//rbc.verifyComponentExistence(rbc.symbolx);
			//rbc.clickComponent(rbc.symbolx); 
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
			finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
