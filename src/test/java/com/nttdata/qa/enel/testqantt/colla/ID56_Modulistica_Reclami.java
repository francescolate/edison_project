package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModulisticaReclamiLinkUtiliComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID56_Modulistica_Reclami {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");

			log.clickComponent(log.buttonAccetta);
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			pc.waitForLinkUtili(pc.linkutili);
			pc.verifyComponentExistence(pc.linkutili);
			pc.comprareText(pc.utiliLinks, pc.Text, true);
			pc.verifyComponentExistence(pc.modulisticaReclami);
			logger.write("Click on the util link modulistica Reclami - Start");
			Thread.sleep(5000);
			pc.clickComponent(pc.modulisticaReclami);
			logger.write("Click on the util link modulistica Reclami - Completed");

			ModulisticaReclamiLinkUtiliComponent mc = new ModulisticaReclamiLinkUtiliComponent(driver);
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Start");
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);
			mc.comprareText(mc.pageText, mc.PageText, true);
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Completed");

			logger.write("Click on compilail Modulo Link - Start");
			mc.jsClickObject(mc.compilailModulo);
			logger.write("Click on compilail Modulo Link - Completed");

			mc.checkURLAfterRedirection(mc.moduloReclamiUrl);
			mc.comprareText(mc.moduloReclamiTitle, mc.ModuloReclamiTitle, true);		
			driver.navigate().back();
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Start");
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Completed");

			mc.verifyComponentExistence(mc.moduloReclamiLuce);
			mc.jsClickObject(mc.moduloReclamiLuce);
			Thread.sleep(5000);
			mc.checkURLAfterRedirection(mc.moduloReclamiLuceUrl);
			driver.navigate().back();
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);

			mc.jsClickObject(mc.moduloReclamiGas);
			Thread.sleep(5000);
			mc.checkURLAfterRedirection(mc.moduloReclamiGasUrl);
			driver.navigate().back();
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);
			
			
			mc.jsClickObject(mc.moduloReclamiTutelaGas);
			Thread.sleep(5000);
			mc.checkURLAfterRedirection(mc.moduloReclamiTutelaGasUrl);
			driver.navigate().back();
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);
			
			logger.write("Click on Qui link - Start");
			mc.clickComponent(mc.quiLink);
			logger.write("Click on Qui link - Completed");
			logger.write("Verify page navigation and fields - Start");
			mc.checkURLAfterRedirection(mc.quiUrl);
			mc.verifyComponentExistence(mc.invioDoccumentiLogin);
			mc.verifyComponentExistence(mc.accediButton);
			mc.verifyComponentExistence(mc.email);
			mc.verifyComponentExistence(mc.confirmEmail);
			mc.verifyComponentExistence(mc.cfField);
			mc.verifyComponentExistence(mc.autenticati);
			logger.write("Verify page navigation and fields - Completed");

			driver.navigate().back();
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Start");
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Completed");

			logger.write("Click on negozio Enel Link- Start");
			mc.clickComponent(mc.negozioEnelLink);
			logger.write("Click on negozio Enel Link- Completed");
			mc.checkURLAfterRedirection(mc.negozioEnelUrl);
			driver.navigate().back();
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Start");
			mc.checkURLAfterRedirection(mc.pageUrl);
			mc.comprareText(mc.pageTitle, mc.PageTitle, true);
			mc.comprareText(mc.comeFareunreclamoText, mc.ComeFareunreclamoText, true);
			mc.verifyComponentExistence(mc.compilailModulo);
			logger.write("Verify page navigation and page text in modulistica Reclami Page - Completed");

			logger.write("Verify header section - Start");
		//	prop.setProperty("LUCE_E_GAS", "Luce e gas");
			mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("LUCE_E_GAS"));
		//	prop.setProperty("IMPRESA", "Imprese");
			mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("IMPRESA"));
		//	prop.setProperty("INSIEME A TE", "Insieme a te");
		//	mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("INSIEME A TE"));
		//	prop.setProperty("STORIE", "Storie");
			mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("STORIE"));
		//	prop.setProperty("FUTUR-E", "Futur-e");
			mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("FUTUR-E"));
		//	prop.setProperty("MEDIA", "Media");
			mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("MEDIA"));
		//	prop.setProperty("SUPPORTO", "Supporto");
			mc.verifyHeaderVoice(mc.headerVoice,prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");
			
			pc.comprareText(mc.enelItalia, mc.EnelItalia, true);
			pc.comprareText(mc.enelEnergia, mc.EnelEnergia, true);	
			logger.write("Verify footer section - Start");
		//	prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
			mc.verifyFooterLink(mc.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
		//	prop.setProperty("CREDITS", "Credits");
			mc.verifyFooterLink(mc.footerLink, prop.getProperty("CREDITS"));
		//	prop.setProperty("PRIVACY", "Privacy");
			mc.verifyFooterLink(mc.footerLink, prop.getProperty("PRIVACY"));
		//	prop.setProperty("COOKIE_POLICY", "Cookie Policy");
			mc.verifyFooterLink(mc.footerLink, prop.getProperty("COOKIE_POLICY"));
		//	prop.setProperty("REMIT", "Remit");
			mc.verifyFooterLink(mc.footerLink, prop.getProperty("REMIT"));
			logger.write("Verify footer section - Completed");
						
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
	
}
