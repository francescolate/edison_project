package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class LoginSap 
{
	//private Properties prop = null;
	
	@Step("Login Salesforce")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		SeleniumUtilities util;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			
//			prop = new Properties();
//			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
//			
//			prop.setProperty("RETURN_VALUE","KO");
//			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
//			prop.setProperty("OUTPUT_DIR","C:/AgentOutputRepository\\ProjectEnelDemo\\2019-06-07\\DEMO_LOGIN_TC_1_821\\DEMO_LOGIN_TO_1_1386"); 
//			prop.setProperty("PASSWORD","Enel$012");
//			prop.setProperty("ERROR_DESCRIPTION","");
//			prop.setProperty("BROWSER","CHROME");
			//Istanzia oggetto Selenium WebDriver
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			//System.out.println(prop.getProperty("POD_ELE"));
		
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				if(!prop.containsKey("START_TIME")){
					Date d = new Date();
					SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String s = fmt.format(d);
					prop.setProperty("START_TIME", s);
				}
//				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
//				page.navigate(prop.getProperty("LINK"));
				driver.get(prop.getProperty("LINK"));
				
				logger.write("Apertura portale Sap");
//				page.enterUsername(prop.getProperty("USERNAME"));
//				driver.findElement(By.xpath("sap-user")).sendKeys(prop.getProperty("USERNAME"));
				
				
				
				logger.write("Inserimento Username");
//				page.enterPassword(prop.getProperty("PASSWORD"));
				logger.write("Inserimento Password");
				driver.findElement(By.xpath("//input[@id='sap-user']")).sendKeys(prop.getProperty("USERNAME"));
				logger.write("Accesso al portale");
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(1);
				driver.findElement(By.xpath("//input[@id='sap-password']")).sendKeys(prop.getProperty("PASSWORD"));
//				page.killPreviousSessions();
				driver.findElement(By.xpath("//a[@id='b1']")).click();
//				
//				
				TimeUnit.SECONDS.sleep(5);
				driver.findElement(By.xpath("//a[@data-unique-id='NIT~Z_SAP_PROJECTMANAGER-N:7']")).click();
				
				TimeUnit.SECONDS.sleep(5);
				util = new SeleniumUtilities(driver);
				driver.switchTo().frame(0);
				driver.findElement(By.xpath("//a[@data-unique-id='sm-NIT~Z_SAP_PROJECTMANAGER-N:97']")).click();
							
				//util.jsClickElement(By.xpath("//a[@data-unique-id='sm-NIT~Z_SAP_PROJECTMANAGER-N:97']"));
				
				driver.switchTo().defaultContent();
				
				TimeUnit.SECONDS.sleep(5);
				driver.switchTo().frame(0);
				driver.switchTo().frame(driver.findElement(By.id("ITSFRAME1")));
				
				driver.findElement(By.xpath("//input[@id='grid#113#1,6#if']")).sendKeys("F01007");
				
				driver.findElement(By.xpath("//input[@id='grid#113#1,9#if']")).sendKeys("1");
				
				driver.findElement(By.xpath("//input[@id='grid#113#1,10#if']")).sendKeys("D80");

				driver.findElement(By.xpath("//input[@id='grid#113#1,11#if']")).sendKeys("100");
				
				driver.findElement(By.xpath("//input[@id='grid#113#1,15#if']")).sendKeys("01122020");

				driver.findElement(By.xpath("//input[@id='grid#113#1,17#if']")).sendKeys("125");

				driver.findElement(By.xpath("//input[@id='grid#113#1,18#if']")).sendKeys("11845");

				driver.findElement(By.xpath("//div[@id='113_hscroll-Nxt']")).click();
				driver.findElement(By.xpath("//div[@id='113_hscroll-Nxt']")).click();
				driver.findElement(By.xpath("//div[@id='113_hscroll-Nxt']")).click();
				driver.findElement(By.xpath("//div[@id='113_hscroll-Nxt']")).click();
				
				driver.findElement(By.xpath("//input[@id='grid#113#1,24#if']")).sendKeys("3020");
				
				driver.findElement(By.xpath("//input[@id='grid#113#1,24#if']")).submit();

//			//	if(prop.getProperty("USERNAME").contentEquals(Costanti.utenza_admin_salesforce)){
//					new RicercaOffertaComponent(driver).cliccaSwitch();	
//			//	}
			}
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
