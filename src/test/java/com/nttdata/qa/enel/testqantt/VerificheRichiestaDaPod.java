package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificheRichiestaDaPod {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
			driver.switchTo().defaultContent();
			verifica.attesaCaricamentoSezioneCliente();
			TimeUnit.SECONDS.sleep(5);
			int tentativi = 2;

			do {
				try {
					driver.navigate().refresh();
					logger.write("Apro Elementi Richiesta");
					verifica.pressJavascript(verifica.elementiRichiestaSection);
					
					String flag = prop.getProperty("RECUPERA_OI_RICHIESTA","Y");
					if(flag==null || flag.length()==0) flag = "N";
					if(flag.contentEquals("Y")) {
					String id = verifica.recuperaOIRichiestaDaPod(
							prop.getProperty("POD", prop.getProperty("POD_ELE", prop.getProperty("POD_GAS"))));
					prop.setProperty("OI_RICHIESTA", id);
					}
					logger.write("Recupero valore id richiesta - Completed");

					logger.write("Verifica Stati Richiesta Integrazione Sistemi - Start");
					verifica.verificaStatiRichiestaConPod(prop.getProperty("STATO_RICHIESTA"),
							prop.getProperty("STATO_R2D"), prop.getProperty("STATO_SAP"),
							prop.getProperty("STATO_SEMPRE"),
							prop.getProperty("POD", prop.getProperty("POD_ELE", prop.getProperty("POD_GAS"))));
					logger.write("Verifica Stati Richiesta Integrazione Sistemi- Completed");

					logger.write("Verifica stato e sottostato richiesta - Start");
					
					
					if (prop.containsKey("STATO_GLOBALE_RICHIESTA_CHIUSO")
							&& prop.containsKey("STATO_GLOBALE_RICHIESTA_CLOSED")) {
				
						verifica.verificaStatoSottostatoRichiestaGestioneMultipla(
								prop.getProperty("STATO_GLOBALE_RICHIESTA_CHIUSO"),
								prop.getProperty("STATO_GLOBALE_RICHIESTA_CLOSED"),
								prop.getProperty("SOTTOSTATO_GLOBALE_RICHIESTA"));
					} else {
						verifica.verificaStatoSottostatoRichiesta(prop.getProperty("STATO_GLOBALE_RICHIESTA"),
								prop.getProperty("SOTTOSTATO_GLOBALE_RICHIESTA"));
					}
					logger.write("Verifica stato e sottostato richiesta - Completed");
					
					tentativi = 0;
				
				} catch (Exception e) {
					--tentativi;
					if(tentativi==0) throw e;
					else TimeUnit.SECONDS.sleep(20);
					
				}
			} while (tentativi > 0);

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
