package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OfferOpenEnerygyDigitalComponent;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.components.colla.ScriviciComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OfferOpenEnerygyDigital_106 {

public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			OfferOpenEnerygyDigitalComponent ooed = new OfferOpenEnerygyDigitalComponent(driver);
			 prop.setProperty("DAYS", "01");
			 prop.setProperty("MONTHS", "1");
			 prop.setProperty("YEARS", "1985");
			 prop.setProperty("INAVALID_EMAILSOCIETA", "fa.ma@enel.");
			 prop.setProperty("INAVALID_PECEMAIL", "+@gmai.com");
			 prop.setProperty("EMAILSOCIETA", "fabiana.manzo@nttdata.com");
			 prop.setProperty("INVALID_REF_TELEPHONE", "Abcgd");
			 prop.setProperty("CODICEFISCALE_SOCIETA", "01533540447");
			 prop.setProperty("REGIONE_SOCIALE", "ASSIMETA SAS");
			 prop.setProperty("NOME", "BRUNA");
			 prop.setProperty("COGNOME", "MASSACCI");
			 prop.setProperty("CODICEFISCALE_REFERENTE", "MSSBRN64T43G005E");
			 prop.setProperty("POD", "IT004Exxxxxxxx");
			 prop.setProperty("CAP", " 60027 - OSIMO");
			 
			 ooed.launchLink("https://www-coll1.enel.it/it/adesione?productType=bsn");
			
			ooed.clickComponent(ooed.calcolaOra);
			ooed.selectDropdown(ooed.days, prop.getProperty("DAYS"));
			ooed.selectDropdown(ooed.months, prop.getProperty("MONTHS"));
			ooed.selectDropdown(ooed.years, prop.getProperty("YEARS"));
			ooed.clickComponent(ooed.dunno);
			ooed.clickComponent(ooed.si);
			ooed.clickComponent(ooed.birthPlace);
			ooed.enterInputParameters(ooed.birthPlace, "ROMENO (TN)");
			ooed.clickComponentIfExist(ooed.calcola);
			
			ooed.enterInputParameters(ooed.busFiscalCode, driver.findElement(ooed.codiceFiscaleReferente).getText());
			ooed.enterInputParameters(ooed.emailSocieta, prop.getProperty("INAVALID_EMAILSOCIETA"));
			ooed.comprareText(ooed.errMsgemailSoc, ooed.FORMATO_NON_CORETTO, true);
			ooed.enterInputParameters(ooed.pecEmail, prop.getProperty("INAVALID_PECEMAIL"));
			ooed.comprareText(ooed.errMsgemailPEC, ooed.FORMATO_NON_CORETTO, true);
			
			ooed.enterInputParameters(ooed.emailSocieta, prop.getProperty("EMAILSOCIETA"));
			ooed.clickComponent(ooed.infoIconEmailSoc);
			ooed.comprareText(ooed.popupText, ooed.POPUP_TEXT, true);
			ooed.clickComponent(ooed.popupCloseButton);
			
			ooed.enterInputParameters(ooed.referenceTelephone, prop.getProperty("INVALID_REF_TELEPHONE"));
			ooed.comprareText(ooed.errMsgRefTelephone, ooed.FORMATO_NON_CORETTO, true);
			
			ooed.checkForDropdownValue(ooed.formaGiuridica, ooed.FORMA_GIURIDICA);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.condominio);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE_CONDOMINION);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.consorziDiBonifica);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE_CONSORZI_DI_BONIFICA);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.ConsorzidiImpresa);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE_CONSORZI_DI_IMPERSA);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.enteMorale);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.impInd);
			ooed.verifyComponentExistence(ooed.nome);
			ooed.verifyComponentExistence(ooed.cognome);
			ooed.clickComponent(ooed.prosegui);
			ooed.comprareText(ooed.nomeErrMsg, ooed.CAMPO_OBBLIGATORIO, true);
			ooed.comprareText(ooed.cognomeErrMsg, ooed.CAMPO_OBBLIGATORIO, true);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE_IMPIND);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.onlus);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.socCoopAgr);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.sapa);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.sas);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.sc);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.scrl);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.snc);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.spa);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
   			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.srl);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.ss);
			ooed.checkForDropdownValue(ooed.tipoReferenteList, ooed.TIPO_REFERENTE);
			
			ooed.enterInputParameters(ooed.emailSocieta, prop.getProperty("EMAILSOCIETA"));
			ooed.clickComponent(ooed.infoPEC);
			ooed.comprareText(ooed.infoPECText, ooed.INFO_PEC_TEXT, true);
			
			//Enter all Mandatory Fields
			ooed.clickComponent(ooed.formaGiuridica);
			ooed.clickComponent(ooed.sas);
			ooed.enterInputParameters(ooed.busFiscalCode, prop.getProperty("CODICEFISCALE_SOCIETA"));
			ooed.enterInputParameters(ooed.regioneSociale, prop.getProperty("REGIONE_SOCIALE"));
			ooed.enterInputParameters(ooed.piva, prop.getProperty("CODICEFISCALE_SOCIETA"));
			ooed.enterInputParameters(ooed.telefonoSoc, "3987612345");
			ooed.enterInputParameters(ooed.emailSocieta, prop.getProperty("EMAILSOCIETA"));
			ooed.clickComponent(ooed.tipoReferente);
			ooed.clickComponent(ooed.tipo_Rappresentante);
			ooed.enterInputParameters(ooed.nome, prop.getProperty("NOME"));
			ooed.enterInputParameters(ooed.cognome, prop.getProperty("COGNOME"));
			ooed.enterInputParameters(ooed.codiceFiscaleReferente, prop.getProperty("CODICEFISCALE_REFERENTE"));
			ooed.enterInputParameters(ooed.telefonoReferente, "3654456782");
			ooed.clickComponent(ooed.no);
			ooed.comprareText(ooed.noText, ooed.NO_TEXT, true);
			ooed.clickComponent(ooed.privacyConsent);
			ooed.clickComponent(ooed.prosegui);
			
			ooed.comprareText(ooed.headinginfoFor, ooed.HEADING_INFO_FORNITURA, true);
			ooed.comprareText(ooed.textinfoFor, ooed.TEXT_INFO_FORNITURA, true);
			ooed.comprareText(ooed.hcambioFornituro, ooed.H_CAMBIO_FORNITORE, true);
			ooed.comprareText(ooed.textCambioFornituro, ooed.T_CAMBIO_FORNITORE, true); 
			ooed.comprareText(ooed.hprimaAttivazone, ooed.H_PRIMA_ATTIVAZIONE, true);
			ooed.comprareText(ooed.textprimaAttivazone, ooed.TEXT_PRIMA_ATTIVAZONE, true);
			ooed.comprareText(ooed.hSubentro, ooed.H_SUBENTRO, true);
			ooed.comprareText(ooed.textsubentro, ooed.TEXT_SUBENTRO, true);
			ooed.verifyComponentExistence(ooed.pod);
			ooed.verifyComponentExistence(ooed.cap);
			ooed.comprareText(ooed.textSte2, ooed.TEXT_STEP2, true);
			ooed.verifyComponentExistence(ooed.prosegui);
			ooed.verifyComponentExistence(ooed.inserisciloDopo);
			
			ooed.clickComponent(ooed.cambioFornitura);
			ooed.enterInputParameters(ooed.pod, "IT004E"+PubblicoID61ProcessoAResSwaEleComponent.generateRandomPOD(8));
			ooed.enterInputParameters(ooed.cap, prop.getProperty("CAP"));
			ooed.clickComponent(ooed.prosegui);
			
			
			ooed.comprareText(ooed.indrizzoDiFornituraText, ooed.IndrizzoDiFornituraText, true);
			ooed.verifyComponentExistence(ooed.citta);
			ooed.verifyComponentExistence(ooed.indrizzo);
			ooed.verifyComponentExistence(ooed.numeroCivico);
			ooed.verifyComponentExistence(ooed.indrizzoDiFornituraCAP);
			ooed.verifyComponentExistence(ooed.potenza);
			ooed.comprareText(ooed.indrizzoDiFornituraPrivacyText, ooed.IndrizzoDiFornituraPrivacyText, true);
			
			ooed.comprareText(ooed.headingRecapti, ooed.HEADING_RECAPTI, true);
			ooed.comprareText(ooed.textRecapti, ooed.TEXT_RECAPTI, true);
			ooed.verifyComponentExistence(ooed.siRecapti);
			ooed.verifyComponentExistence(ooed.noRecapti);
			ooed.comprareText(ooed.addressRadio, ooed.ADDRESS_RADIO, true);
			ooed.comprareText(ooed.yesBillingAddress, ooed.YES_BILLING, true);
			ooed.comprareText(ooed.noBillingAddress, ooed.NO_BILLING, true);
			ooed.verifyComponentExistence(ooed.prosegui);
			ooed.verifyComponentExistence(ooed.salvaContinuaDopo);
			
			
			ooed.enterInputParameters(ooed.citta,"OSIMO(ANCORA) MARCHE");
			ooed.enterInputParameters(ooed.indrizzo, "VIA MONTECERNO");
			ooed.enterInputParameters(ooed.numeroCivico, "33");
			ooed.clickComponent(ooed.potenza);
			ooed.clickComponent(ooed.insertManually);
			Thread.sleep(3000);
			ooed.enterInputParameters(ooed.cap, "60027");
			ooed.clickComponent(ooed.siRecapti);
			ooed.clickComponent(ooed.yesBillingAddress);
			ooed.clickComponent(ooed.prosegui);
			
			ooed.comprareText(ooed.headingPagamentiBollette, ooed.HEADING_PAGAMENTI_BOLLETTE, true);
			ooed.verifyComponentExistence(ooed.labelPaymentMode);
			ooed.verifyFeildAttribute(ooed.paymentModeSelectBox, ooed.PAYMENTMODE_SELECTBOX);
			ooed.verifyComponentExistence(ooed.iBANCode);
			ooed.verifyComponentExistence(ooed.yesAccountHolder);
			ooed.verifyComponentExistence(ooed.noAccountHolder);
			
			ooed.verifyComponentExistence(ooed.Modalità_di_ricezione_Heading);
			ooed.comprareText(ooed.Modalità_di_ricezione_Radio1, ooed.Modalità_Di_ricezione_Radio1, true);
			ooed.comprareText(ooed.Modalità_di_ricezione_Radio2, ooed.Modalità_Di_ricezione_Radio2, true);
			ooed.verifyComponentExistence(ooed.email);
			ooed.verifyComponentExistence(ooed.prefisso);
			ooed.verifyComponentExistence(ooed.cellulare);
			
			
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
