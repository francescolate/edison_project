package com.nttdata.qa.enel.testqantt.colla;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CreateEmailAddressesList {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			CreateEmailAddressesListComponent gnac = new CreateEmailAddressesListComponent(driver);
			String email = Costanti.GMAIL_ADDRESS_FOR_REGISTRATION;
			File f = new File("EmailAddressesForRegistration");
			FileOutputStream fos = new FileOutputStream(f);
			
			HashMap<String, String> addresses = new HashMap<String, String>();
			while((email = gnac.setDotOnEmailAddress(email)) != null)
				if(!addresses.containsKey(email))
					addresses.put(email, email);
			
			for(Entry<String, String> entry : addresses.entrySet())
				fos.write((entry.getKey().replace("\n", "").replace("\r", "")+"\n").getBytes());
			
			fos.close();
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
