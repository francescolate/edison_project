package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato211ACRDisattivazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_234_ACR_Disattivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato211ACRDisattivazioneComponent dzcp = new Privato211ACRDisattivazioneComponent(driver);
			
			dzcp.verifyComponentVisibility(dzcp.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = dzcp.homePageCentralText;
			dzcp.verifyComponentExistence(homePage);
			dzcp.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			By homepageSubheading = dzcp.homePageCentralSubText;
			dzcp.verifyComponentExistence(homepageSubheading);
			dzcp.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = dzcp.sectionTitle;
			dzcp.verifyComponentExistence(sectionTitle);
			dzcp.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = dzcp.sectionTitleSubText;
			dzcp.verifyComponentExistence(sectionTitleSubText);
			dzcp.VerifyText(sectionTitleSubText, dzcp.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Verify and Click on left menu item 'SERVIZI' - Start");
			By servizi = dzcp.serviziSelect;
			dzcp.verifyComponentExistence(servizi);
			dzcp.clickComponent(servizi);
			logger.write("Verify and Click on left menu item 'SERVIZI' - Completed");
			
			dzcp.verifyComponentVisibility(dzcp.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = dzcp.serviziPage;
			dzcp.verifyComponentExistence(serviziHomePage);
			dzcp.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Start");
			By serviziPageSectionTitle= dzcp.serviziSectionTitle;
			dzcp.verifyComponentExistence(serviziPageSectionTitle);
			dzcp.VerifyText(serviziPageSectionTitle, prop.getProperty("SERVIZI_HOMEPAGE_SECTIONTITLE"));
			Thread.sleep(5000);
			
			By serviziPage_disattivazioneFornituraItem = dzcp.disattivazioneFornituraItem;
			dzcp.verifyComponentExistence(serviziPage_disattivazioneFornituraItem);
			dzcp.clickComponent(serviziPage_disattivazioneFornituraItem);
			logger.write("Checking if SERVIZI_HOMEPAGE_SECTIONTITLE is displayed in page - Completed");
			Thread.sleep(7000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornitura);
			
			logger.write("User is navigated to home page of DISATTIVAZIONE_FORNITURA_PAGE - Start");
			By disattivazioneFornituraPageHeading = dzcp.disattivazioneFornitura;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageHeading);
			dzcp.VerifyText(disattivazioneFornituraPageHeading, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_HEADING"));
			logger.write("User is navigated to home page of DISATTIVAZIONE_FORNITURA_PAGE - Completed");
			
			logger.write("Verify and click on PROSEGUI_CON_LA_DISATTIVAZIONE_button - Start");
			By PROSEGUI_CON_LA_DISATTIVAZIONE_button = dzcp.PROSEGUI_CON_LA_DISATTIVAZIONE_button;
			dzcp.verifyComponentExistence(PROSEGUI_CON_LA_DISATTIVAZIONE_button);
			dzcp.clickComponent(PROSEGUI_CON_LA_DISATTIVAZIONE_button);
			logger.write("Verify and click on PROSEGUI_CON_LA_DISATTIVAZIONE_button - Completed");
			Thread.sleep(20000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornituraPageTitle);
			dzcp.verifyComponentVisibility(dzcp.pdrCheckbox_00080000211465);
			dzcp.verifyComponentVisibility(dzcp.podCheckbox_IT004E20070603);
			
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Start");
			By disattivazioneFornituraPageTitle = dzcp.disattivazioneFornituraPageTitle;
			dzcp.verifyComponentExistence(disattivazioneFornituraPageTitle);
			dzcp.VerifyText(disattivazioneFornituraPageTitle, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_TITLE"));
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Completed");
			
			logger.write("Validating the LIST OF SUPPLY displayed in the page and click on Disattivazione  - Start");
			dzcp.gasSupplyDetailsDisplay(dzcp.GasDetails);
			dzcp.lightSupplyDetailsDisplay(dzcp.LightDetails);
			dzcp.validateAndClickDisattivazioneButton(dzcp.Disattivazione_button);
			logger.write("Validating the LIST OF SUPPLY displayed in the page and click on Disattivazione  - Completed");
			Thread.sleep(5000);
			
			dzcp.verifyComponentVisibility(dzcp.disattivazioneFornituraPageTitle);
			
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Start");
			dzcp.verifyComponentExistence(disattivazioneFornituraPageTitle);
			dzcp.VerifyText(disattivazioneFornituraPageTitle, prop.getProperty("DISATTIVAZIONE_FORNITURA_PAGE_TITLE"));
			logger.write("In New page Validate the page title of DISATTIVAZIONE_FORNITURA - Completed");
			
			logger.write("Checking presence of DISATTIVAZIONE steps - Start");
			dzcp.verifyComponentExistence(dzcp.DisattivazioneSteps);
			logger.write("Checking presence of DISATTIVAZIONE steps - Completed");
			
			logger.write("Verifying the content of DISATTIVAZIONE steps and Color of step 1 - Start");
			dzcp.validateDisattivazioneSteps();
			logger.write("Verifying the content of DISATTIVAZIONE steps and Color of step 1 - Completed");
			
			logger.write("Verify the color of Inserimento_dati - Start");
			dzcp.checkColor(dzcp.Inserimento_dati, dzcp.servicesColor, "Inserimento_dati");
			logger.write("Verify the color of Inserimento_dati - Start");
			
			logger.write("Verify the section for Inserimento dati - Data Entry - Start");
			dzcp.validateDisattivazioneSupplyDetails(dzcp.Invio_comunicazioni_ExpectedEmail);
			logger.write("Verify the section for Inserimento dati - Data Entry - Completed");
			
			logger.write("Validate that radio button is not Selected and click 'Inviocomunicazioni_RadioButton_No' radio button - start");
			By Inviocomunicazioni_RadioButton_No = dzcp.Inviocomunicazioni_RadioButton_No;
			dzcp.verifyComponentExistence(Inviocomunicazioni_RadioButton_No);
			dzcp.verifyCheckboxnotSelected(Inviocomunicazioni_RadioButton_No);
			
			dzcp.clickComponent(Inviocomunicazioni_RadioButton_No);
			logger.write("Validate that radio button is not Selected and click 'Inviocomunicazioni_RadioButton_No' radio button - completed");
			
			logger.write("Verify the presence of section 'Indical_indirizzoPresso' - Start");
			dzcp.verifyComponentVisibility(dzcp.Indical_indirizzoPresso);
			
			By Indical_indirizzoPresso = dzcp.Indical_indirizzoPresso;
			dzcp.verifyComponentExistence(Indical_indirizzoPresso);
			dzcp.VerifyText(Indical_indirizzoPresso, dzcp.Indical_indirizzoPresso_Heading);
			logger.write("Verify the presence of section 'Indical_indirizzoPresso' - Completed");
			
			logger.write("Enter valid details in 'Indical_indirizzoPresso' section - Start");
			logger.write("Enter field values to section 'Indical_indirizzoPresso' - start");
			dzcp.enter_Indica_l_indirizzo_FieldDetails(dzcp.indicaFields);
			logger.write("Enter field values to section 'Indical_indirizzoPresso' - completed");
			
			logger.write("validate the error message and enter cap field value - Start");
			By indirizzoinserto_non_e_stato_ErrorMsg = dzcp.indirizzoinserto_non_e_stato_ErrorMsg;
			dzcp.verifyComponentExistence(indirizzoinserto_non_e_stato_ErrorMsg);
			
			/*By Inserisci_manualmente_Button = dzcp.Inserisci_manualmente_Button;
			dzcp.verifyComponentExistence(Inserisci_manualmente_Button);
			dzcp.clickComponent(dzcp.Inserisci_manualmente_Button);
			
			dzcp.selectDropDownByValue(dzcp.ToponomasticaSelect, dzcp.Toponomastica);
			dzcp.enterInput(dzcp.NumeroCivicoInputbox, dzcp.NumeroCivicoInputText);
			dzcp.enterInput(dzcp.CAP_Inputbox, dzcp.CAPbox);
			
			By CAP_InputSelect = dzcp.CAP_InputSelect;
			dzcp.verifyComponentExistence(CAP_InputSelect);
			dzcp.clickComponent(CAP_InputSelect);
			logger.write("validate the error message and enter cap field value - Completed");
			
			logger.write("Validate presence of Conferma_indirizzo_Button - Start");
			By Conferma_indirizzo_Button = dzcp.Conferma_indirizzo_Button;
			dzcp.verifyComponentExistence(Conferma_indirizzo_Button);
			dzcp.clickComponent(Conferma_indirizzo_Button);
			logger.write("Validate presence of Conferma_indirizzo_Button - Completed");*/
			
			logger.write("Enter valid details in 'Indical_indirizzoPresso' section - Completed");
			dzcp.checkForDropDownValue_Aiutaci_a_migliorare(dzcp.Aiutaci_a_migliorare_MotivoDellaDisattivazione_dropDown);
			dzcp.selectOptions(dzcp.Aiutaci_MotivoDellaDisattivazione_selectDropdownValue);
			logger.write("Enter valid details in 'Indical_indirizzoPresso' section - Completed");
			
			logger.write("Click on CONTINUA button - start");
			dzcp.clickComponent(dzcp.CONTINUA);
			logger.write("Click on CONTINUA button - completed");
			Thread.sleep(5000);
			
			logger.write("Checking the presence of DisattivazioneSteps - Start");
			dzcp.verifyComponentExistence(dzcp.DisattivazioneSteps);
			logger.write("Checking the presence of DisattivazioneSteps - Completed");
			
			logger.write("Verifying the color displayed for 'Inserimento_dati' and 'Riepilogo_e_conferma_dati' steps - Start");
			dzcp.checkColor(dzcp.Inserimento_dati, dzcp.successColor, "Inserimento_dati");
			dzcp.checkColor(dzcp.Riepilogo_e_conferma_dati, dzcp.servicesColor, "Riepilogo_e_conferma_dati");
			logger.write("Verifying the color displayed for 'Inserimento_dati' and 'Riepilogo_e_conferma_dati' steps - Completed");
			
			logger.write("Valdiating the content cofirmation displayed in 'Riepilogo_e_conferma_dati' page - Start");
			dzcp.validateDisattivazioneSupplyAndConfirmDetails(dzcp.ConfirmGasDetails, dzcp.ConfirmElectricityDetails);
			logger.write("Valdiating the content cofirmation displayed in 'Riepilogo_e_conferma_dati' page - Completed");
						
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}