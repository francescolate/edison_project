package com.nttdata.qa.enel.testqantt;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CercaClienteComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovaInterazioneComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaClienteAttivoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;


public class CercaClientePerNuovaInterazione  {
	
	
	public static void main (String[] args) throws Exception
	{
		
		Properties prop = null;
		
		try {
			
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		QANTTLogger logger = new QANTTLogger(prop);
	
		CreaNuovaInterazioneComponent startIterationPage = new CreaNuovaInterazioneComponent(driver);
		startIterationPage.createNewiteration();
		logger.write("Creazione nuova Interazione");
		CercaClienteComponent searchClientPage =  new CercaClienteComponent(driver);
		searchClientPage.fillCfAndSearch2(prop.getProperty("CODICE_FISCALE"));
		logger.write("GetProp: CODICE FISCALE");
		
		SelezionaClienteAttivoComponent selectCliente = new SelezionaClienteAttivoComponent(driver);
		
		if(prop.getProperty("SCELTA_TIPO_CLIENTE","N").equals("Y")) {
		    selectCliente.fillCfAndChooseClientType(prop.getProperty("TIPO_CLIENTE"),prop.getProperty("COMPLETE_NAME"));
		}
		else {
			selectCliente.fillCfAndSearch3();
		}
				
		selectCliente.setInterlocutore(0);
		
		prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
		
	}
}
