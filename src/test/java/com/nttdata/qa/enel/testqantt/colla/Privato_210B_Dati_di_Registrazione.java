package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.CondizionigeneralideiservizioffertiModificaProfiloComponentConstatnt;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.ResCutomerProfileComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_210B_Dati_di_Registrazione {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			
			logger.write("Accessing login page - Start");
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			logger.write("Accessing login page - Complete");
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
			logger.write("Enter login button without input  - Start");
			lpv.verifyComponentExistence(lpv.buttonLoginAccedi);
			
			logger.write("Enter login button without input  - Complete");
						
			logger.write("Enter the username and password  - Start");
			lpv.enterInput(lpv.username, prop.getProperty("USERNAME"));
			lpv.enterInput(lpv.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			
			lpv.clickComponent(lpv.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
					
			
			ResCutomerProfileComponent rcp =  new ResCutomerProfileComponent(driver);
			
		
			logger.write("Verify the home page location  - Start");
			rcp.verifyComponentExistence(rcp.custHomePageloc);
			rcp.comprareText(rcp.custHomePageloc, ResCutomerProfileComponent.CUSTHOMEPAGE_LOC, true);
			logger.write("Verify the home page location  - Complete");
			
			logger.write("Verify the customer header  - Start");
			rcp.verifyComponentExistence(rcp.cutHomePageHeader);
			rcp.comprareText(rcp.cutHomePageHeader, ResCutomerProfileComponent.CUSTHOMEPAGE_HEADER, true);
			logger.write("Verify the customer header  - Complete");
						
			logger.write("Verify and click on icon  - Start");
			rcp.verifyComponentExistence(rcp.iconUser);
			rcp.clickComponent(rcp.iconUser);
			logger.write("Verify and click on icon  - Complete");
			
			logger.write("Verify the credenziali header  - Start");
			rcp.verifyComponentExistence(rcp.credenzialiHeader);
			rcp.comprareText(rcp.credenzialiHeader, ResCutomerProfileComponent.CREDENZIALI_HEADER, true);
			logger.write("Verify the credenziali header  - Complete");
			String currentHandle = driver.getWindowHandle();
			
			/*	
			Thread.sleep(4000);
			logger.write("Verify the field input values - Start");
			rcp.checkFieldValue(rcp.emailIn, ResCutomerProfileComponent.EMAIL_RAF);
			rcp.checkFieldValue(rcp.nomeIn, ResCutomerProfileComponent.NOME_MARI);
			rcp.checkFieldValue(rcp.cogNome, ResCutomerProfileComponent.COGNOME_RIO);
			rcp.checkFieldValue(rcp.cellulare, ResCutomerProfileComponent.CELLULARE_RIO);
			rcp.checkFieldValue(rcp.codiceFiscale, ResCutomerProfileComponent.CODICEFISCALE_RIO);
			logger.write("Verify the field input values - Complete");
			
			//rcp.checkForPlaceholderText(rcp.emailInput, ResCutomerProfileComponent.EMAIL);
				
		
			logger.write("Verify  and click on cellulare iocon  - Start");
			rcp.verifyComponentExistence(rcp.cellulareEyeIcon);
			rcp.clickComponent(rcp.cellulareEyeIcon);
			logger.write("Verify  and click on cellulare iocon  - Start");
			
			logger.write("Verify the popup title and contents - Start");
			rcp.verifyComponentExistence(rcp.popupModificaTitle);
			rcp.comprareText(rcp.popupModificaTitle, ResCutomerProfileComponent.MODIFICA_TITLE, true);
			rcp.comprareText(rcp.popupPotraiContent, ResCutomerProfileComponent.POTRAI_CONTENT, true);
			logger.write("Verify the popup title and contents - Complete");
			
			logger.write("Verify and click on qui link - Start");
			rcp.verifyComponentExistence(rcp.quiLink);
			rcp.clickComponent(rcp.quiLink);
			logger.write("Verify and click on qui link - Complete");
			
			logger.write("Switch to New window - Start");
			String currentHandle = driver.getWindowHandle();
			rcp.SwitchTabToSupporto();
			logger.write("Switch to New window - Complete");
			
			System.out.println(driver.getCurrentUrl());
			prop.setProperty("SUPPORTURL", "https://www-coll1.enel.it/it/supporto/faq/faq-area-clienti");
			rcp.checkURLAfterRedirection(prop.getProperty("SUPPORTURL"));
			
			logger.write("Verify the supporto header and path - Start");
			rcp.verifyComponentExistence(rcp.quiSupportoPath);
			rcp.comprareText(rcp.quiSupportoPath, ResCutomerProfileComponent.QUISUPPORTO_PATH, true);
			rcp.verifyComponentExistence(rcp.quiSupportoHeader);
			rcp.comprareText(rcp.quiSupportoHeader, ResCutomerProfileComponent.QUISUPPORTO_HEADER, true);
			logger.write("Verify the supporto header and path - Complete");
			
			logger.write("Verify the Questions - Start");
			rcp.verifyComponentExistence(rcp.quiSupportoQues1);
			rcp.comprareText(rcp.quiSupportoQues1, ResCutomerProfileComponent.QUISUPPORTO_QUES1, true);
			rcp.verifyComponentExistence(rcp.quiSupportoQues2);
			rcp.comprareText(rcp.quiSupportoQues2, ResCutomerProfileComponent.QUISUPPORTO_QUES2, true);
			rcp.verifyComponentExistence(rcp.quiSupportoQues3);
			rcp.comprareText(rcp.quiSupportoQues3, ResCutomerProfileComponent.QUISUPPORTO_QUES3, true);
			rcp.verifyComponentExistence(rcp.quiSupportoQues4);
			rcp.comprareText(rcp.quiSupportoQues4, ResCutomerProfileComponent.QUISUPPORTO_QUES4, true);
			rcp.verifyComponentExistence(rcp.quiSupportoQues5);
			rcp.comprareText(rcp.quiSupportoQues5, ResCutomerProfileComponent.QUISUPPORTO_QUES5, true);
			logger.write("Verify the Questions - complete");
			
			logger.write("Switch to parent window - Start");
			driver.switchTo().window(currentHandle);
			logger.write("Switch to parent window - Complete");
			
			System.out.println(driver.getCurrentUrl());
			prop.setProperty("PROFILoURL", "https://www-coll1.enel.it/it/modifica-profilo#modal-info-cell");
			rcp.checkURLAfterRedirection(prop.getProperty("PROFILoURL"));
			
			logger.write("Close the popup - Start");
			rcp.verifyComponentExistence(rcp.popupClose);
			rcp.clickComponent(rcp.popupClose);
			logger.write("Close the popup - Complete");
			*/
			logger.write("Verify the informativa and condizioni title - Start");
			rcp.verifyComponentExistence(rcp.informativaTitle);
			rcp.comprareText(rcp.informativaTitle, ResCutomerProfileComponent.INFORMATIVA_TITLE, true);
			rcp.verifyComponentExistence(rcp.condizioniTitle);
			rcp.comprareText(rcp.condizioniTitle, ResCutomerProfileComponent.CONDIZIONI_TITLE, true);
			logger.write("Verify the informativa and condizioni title - Complete");
			
	
			
			logger.write("Click on informativa link and contents - Start");
			rcp.verifyComponentExistence(rcp.linkInformativa);
			rcp.clickComponent(rcp.linkInformativa);
			rcp.comprareText(rcp.informativaPrivacy, CondizionigeneralideiservizioffertiModificaProfiloComponentConstatnt.informativaPrivacyConstant1, true);
			logger.write("Click on informativa link and contents - Complete");
			
			logger.write("Close the popup - Start");
			rcp.clickComponent(rcp.privacyDisclaimerClose);
			logger.write("Close the popup - Complete");
			
			logger.write("Click on Condizioni link and contents - Start");
			rcp.verifyComponentExistence(rcp.linkCondizioni);
			rcp.clickComponent(rcp.linkCondizioni);
			rcp.comprareText(rcp.generalConditions, CondizionigeneralideiservizioffertiModificaProfiloComponentConstatnt.Condizionigeneralideiserviziofferti, true);
			logger.write("Click on Condizioni link and contents - Complete");
			
			logger.write("Close the popup - Start");
			rcp.clickComponent(rcp.generalConditionClose);
			logger.write("Close the popup - Complete");
			
			logger.write("Verify the Anagrafica header - Start");
			rcp.verifyComponentExistence(rcp.anagraficaHeader);
			rcp.comprareText(rcp.anagraficaHeader, ResCutomerProfileComponent.ANAGRAFICA_HEADER, true);
			logger.write("Verify the Anagrafica header - Complete");
			
			/*
			logger.write("Verify the Anagrafica fields - Start");
			rcp.verifyComponentExistence(rcp.denominazione);
			rcp.verifyComponentExistence(rcp.partita);
			rcp.verifyComponentExistence(rcp.codice);
			logger.write("Verify the Anagrafica fields - Complete");
			
			logger.write("Click on Modifica Email link - Start");
			rcp.verifyComponentExistence(rcp.modificaEmailLink);
			rcp.clickComponent(rcp.modificaEmailLink);
			logger.write("Click on Modifica Email link - Complete");
			
			logger.write("Verify the Modifica Email header and contents - Start");
			rcp.verifyComponentExistence(rcp.modificaEmailHeader);
			rcp.comprareText(rcp.modificaEmailHeader, ResCutomerProfileComponent.MODIFICAEMAIL_TITLE, true);
			rcp.verifyComponentExistence(rcp.modificaEmailContent);
			rcp.comprareText(rcp.modificaEmailContent, ResCutomerProfileComponent.MODIFICAEMAIL_CONTENT, true);
			logger.write("Verify the Modifica Email header and contents - Complete");
			
			logger.write("Verify the Modifica Email input values - Start");
			rcp.verifyComponentExistence(rcp.NuovoEmailInput);
			rcp.checkForPlaceholderText(rcp.NuovoEmailInput, ResCutomerProfileComponent.EMAILINPUT_VALUE);
			rcp.verifyComponentExistence(rcp.confermaNuovoEmailInput);
			rcp.checkForPlaceholderText(rcp.confermaNuovoEmailInput, ResCutomerProfileComponent.EMAILINPUT_VALUE);
			logger.write("Verify the Modifica Email input values - Complete");
			
			logger.write("Verify the Conferma button - Start");
			rcp.verifyComponentExistence(rcp.confermaEmailPasswdBtn);
			rcp.verifyComponentExistence(rcp.EmailCampiValue);
			rcp.comprareText(rcp.EmailCampiValue, ResCutomerProfileComponent.CAMPI_TEXT, true);
			logger.write("Verify the Conferma button - Complete");
			*/
			logger.write("Click on Modifica password link - Start");
			rcp.verifyComponentExistence(rcp.modificaPassowrdLink);
			rcp.clickComponent(rcp.modificaPassowrdLink);
			logger.write("Click on Modifica password link - Start");
			/*
			logger.write("Verify the Modifica password input values - Start");
			rcp.verifyComponentExistence(rcp.ModificaPassword);
			rcp.checkForPlaceholderText(rcp.ModificaPassword, ResCutomerProfileComponent.MODIFICA_PASSWORD);
			rcp.verifyComponentExistence(rcp.nuovaPassword);
			rcp.checkForPlaceholderText(rcp.nuovaPassword, ResCutomerProfileComponent.NUOVA_PASSWORD);
			rcp.verifyComponentExistence(rcp.conferaNuovaPassword);
			rcp.checkForPlaceholderText(rcp.conferaNuovaPassword, ResCutomerProfileComponent.CONFERMANUOAVA_PASSWORD);
			logger.write("Verify the Modifica password input values - Complete");
			*/
			logger.write("Verify Modifica password header and contents - Start");
			rcp.verifyComponentExistence(rcp.modificaPasswdHeader);
			rcp.comprareText(rcp.modificaPasswdHeader, ResCutomerProfileComponent.MODIFICAPASSWD_HEADER, true);
			logger.write("Verify Modifica password header and contents - Start");
			
			logger.write("Verify the Conferma change password button  - Start");
			rcp.verifyComponentExistence(rcp.confermaChangePasswdBtn);
			rcp.verifyComponentExistence(rcp.passwordCampiValue);
			logger.write("Verify the Conferma change password button  - Complete");
			
			logger.write("Verify the Nuova password value  - Start");
			rcp.comprareText(rcp.passwordCampiValue, ResCutomerProfileComponent.CAMPI_TEXT, true);
			rcp.verifyComponentExistence(rcp.nuovaPasswordValue);
			rcp.comprareText(rcp.nuovaPasswordValue, ResCutomerProfileComponent.NUOVAPASSWD_VALUE, true);
			logger.write("Verify the Nuova password value  - Complete");
			
			logger.write("Click on Elimina Registrazione link  - Start");
			rcp.verifyComponentExistence(rcp.eliminaRegistrazioneLink);
			rcp.clickComponent(rcp.eliminaRegistrazioneLink);
			logger.write("Click on Elimina Registrazione link  - Complete");
			
			logger.write("Verify the Elimina header and contents  - Start");
			rcp.verifyComponentExistence(rcp.eliminaHeader);
			rcp.comprareText(rcp.eliminaHeader, ResCutomerProfileComponent.ELIMINA_HEADER, true);
			rcp.verifyComponentExistence(rcp.elininaContent);
			rcp.comprareText(rcp.elininaContent, ResCutomerProfileComponent.ELININA_CONTENT, true);
			logger.write("Verify the Elimina header and contents  - Complete");
			
			logger.write("Verify the Proseugi button  - Start");
			rcp.verifyComponentExistence(rcp.proseguiButton);
			logger.write("Verify the  the Proseugi button   - Complete");
			
			logger.write("Verify and click Faq link  - Start");  
			rcp.verifyComponentExistence(rcp.faqMenu);
			rcp.clickComponent(rcp.faqMenu);
			logger.write("Verify and click Faq link  - Complete");
			
			logger.write("Swith to New window  - Start");
			rcp.SwitchTabToSupporto();
			logger.write("Swith to New window  - Complete");
			
			prop.setProperty("SUPPORTURL", "https://www-coll1.enel.it/it/supporto/faq/faq-area-clienti");
			System.out.println(driver.getCurrentUrl());
			rcp.checkURLAfterRedirection(prop.getProperty("SUPPORTURL"));
			
			logger.write("Verify the Faq path and header   - Start");
			rcp.verifyComponentExistence(rcp.faqPath);
			rcp.comprareText(rcp.faqPath, ResCutomerProfileComponent.FAQ_PATH, true);
			rcp.verifyComponentExistence(rcp.faqHeader);
			rcp.comprareText(rcp.faqHeader, ResCutomerProfileComponent.FAQ_HEADER, true);
			logger.write("Verify the Faq path and header   - Complete");
			
			logger.write("Verify the Faq questions   - Start");
			rcp.verifyComponentExistence(rcp.faqQues1);
			rcp.comprareText(rcp.faqQues1, ResCutomerProfileComponent.FAQ_QUES1, true);
			rcp.verifyComponentExistence(rcp.faqQues2);
			rcp.comprareText(rcp.faqQues2, ResCutomerProfileComponent.FAQ_QUES2, true);
			rcp.verifyComponentExistence(rcp.faqQues3);
			rcp.comprareText(rcp.faqQues3, ResCutomerProfileComponent.FAQ_QUES3, true);
			rcp.verifyComponentExistence(rcp.faqQues4);
			rcp.comprareText(rcp.faqQues4, ResCutomerProfileComponent.FAQ_QUES4, true);
			rcp.verifyComponentExistence(rcp.faqQues5);
			rcp.comprareText(rcp.faqQues5, ResCutomerProfileComponent.FAQ_QUES5, true);
			logger.write("Verify the Faq questions  - Complete");
			
			
			logger.write("Swith to parent window   - Start");
			driver.switchTo().window(currentHandle);
			logger.write("Switch to parent window   - Complete");
			
					
			logger.write("Verify and click on  the Informativa Privacy - Start");
			rcp.verifyComponentExistence(rcp.informativaPrivacyLink);
			rcp.clickComponent(rcp.informativaPrivacyLink);
			logger.write("Verify and click on  the Informativa Privacy - Complete");
			
			logger.write("Switch to New window - Start");
			rcp.SwitchTabToSupporto();
			logger.write("Switch to New window - Complete");
			
			System.out.println("Check " +driver.getCurrentUrl());
			prop.setProperty("PRIVACY", "https://www-coll1.enel.it/it/supporto/faq/privacy");
			rcp.checkURLAfterRedirection(prop.getProperty("PRIVACY"));
							
			logger.write("Verify the Privacy content - Start");
			rcp.verifyComponentExistence(rcp.privacyContent);
			rcp.comprareText(rcp.privacyContent, ResCutomerProfileComponent.PRIVACY_CONTENT, true);
			logger.write("Verify the Privacy content - Complete");
			
			logger.write("Verify the Privacy questions - Start");
			rcp.verifyComponentExistence(rcp.privacyQues1);
			rcp.comprareText(rcp.privacyQues1, ResCutomerProfileComponent.PRIVACY_QUES1, true);
			rcp.verifyComponentExistence(rcp.privacyQues2);
			rcp.comprareText(rcp.privacyQues2, ResCutomerProfileComponent.PRIVACY_QUES2, true);
			rcp.verifyComponentExistence(rcp.privacyQues3);
			rcp.comprareText(rcp.privacyQues3, ResCutomerProfileComponent.PRIVACY_QUES3, true);
			rcp.verifyComponentExistence(rcp.privacyQues4);
			rcp.comprareText(rcp.privacyQues4, ResCutomerProfileComponent.PRIVACY_QUES4, true);
			rcp.verifyComponentExistence(rcp.privacyQues5);
			rcp.comprareText(rcp.privacyQues5, ResCutomerProfileComponent.PRIVACY_QUES5, true);
			rcp.verifyComponentExistence(rcp.privacyQues6);
			rcp.comprareText(rcp.privacyQues6, ResCutomerProfileComponent.PRIVACY_QUES6, true);
			rcp.verifyComponentExistence(rcp.privacyQues7);
			rcp.comprareText(rcp.privacyQues7, ResCutomerProfileComponent.PRIVACY_QUES7, true);
			rcp.verifyComponentExistence(rcp.privacyQues8);
			rcp.comprareText(rcp.privacyQues8, ResCutomerProfileComponent.PRIVACY_QUES8, true);
			rcp.verifyComponentExistence(rcp.privacyQues9);
			rcp.comprareText(rcp.privacyQues9, ResCutomerProfileComponent.PRIVACY_QUES9, true);
			rcp.verifyComponentExistence(rcp.privacyQues10);
			rcp.comprareText(rcp.privacyQues10, ResCutomerProfileComponent.PRIVACY_QUES10, true);
			rcp.verifyComponentExistence(rcp.privacyQues11);
			rcp.comprareText(rcp.privacyQues11, ResCutomerProfileComponent.PRIVACY_QUES11, true);
			logger.write("Verify the Privacy questions - Complete");
			
			logger.write("Swith to parent window   - Start");
			driver.switchTo().window(currentHandle);
			logger.write("Switch to parent window   - Complete");
			
			System.out.println(driver.getCurrentUrl());
			prop.setProperty("MODIFICAPROF", "https://www-coll1.enel.it/it/modifica-profilo");
			//rcp.checkURLAfterRedirection(prop.getProperty("MODIFICAPROF"));
			
			logger.write("Verify and click on Clausole link - Start");
			rcp.verifyComponentExistence(rcp.clausoleLink);
			rcp.clickComponent(rcp.clausoleLink);
			logger.write("Verify and click on Clausole link - Complete");
			
			logger.write("Switch to New window - Start");
			rcp.SwitchTabToSupporto();
			logger.write("Switch to New window - Complete");
			
			System.out.println(driver.getCurrentUrl());
			prop.setProperty("CLAUSOLE", "https://www-coll1.enel.it/it/supporto/faq/clausole-legali");
			rcp.checkURLAfterRedirection(prop.getProperty("CLAUSOLE"));
			
			rcp.verifyComponentExistence(rcp.faqPath);
			
			logger.write("Verify the Clausole Legali header - Start");
			rcp.verifyComponentExistence(rcp.clausoleLegaliHeader);
			rcp.comprareText(rcp.clausoleLegaliHeader, ResCutomerProfileComponent.CLAUSOLELEGAL_HEADER, true);
			logger.write("Verify the Clausole Legali header - Complete");
			
			logger.write("Verify the Clausole Legali Questions - Start");
			rcp.verifyComponentExistence(rcp.clausoleQues1);
			rcp.comprareText(rcp.clausoleQues1, ResCutomerProfileComponent.CLAUSOLE_QUES1, true);
			rcp.verifyComponentExistence(rcp.clausoleQues2);
			rcp.comprareText(rcp.clausoleQues2, ResCutomerProfileComponent.CLAUSOLE_QUES2, true);
			rcp.verifyComponentExistence(rcp.clausoleQues3);
			rcp.comprareText(rcp.clausoleQues3, ResCutomerProfileComponent.CLAUSOLE_QUES3, true);
			rcp.verifyComponentExistence(rcp.clausoleQues4);
			rcp.comprareText(rcp.clausoleQues4, ResCutomerProfileComponent.CLAUSOLE_QUES4, true);
			rcp.verifyComponentExistence(rcp.clausoleQues5);
			rcp.comprareText(rcp.clausoleQues5, ResCutomerProfileComponent.CLAUSOLE_QUES5, true);
			rcp.verifyComponentExistence(rcp.clausoleQues6);
			rcp.comprareText(rcp.clausoleQues6, ResCutomerProfileComponent.CLAUSOLE_QUES6, true);
			rcp.verifyComponentExistence(rcp.clausoleQues7);
			rcp.comprareText(rcp.clausoleQues7, ResCutomerProfileComponent.CLAUSOLE_QUES7, true);
			rcp.verifyComponentExistence(rcp.clausoleQues8);
			rcp.comprareText(rcp.clausoleQues8, ResCutomerProfileComponent.CLAUSOLE_QUES8, true);
			rcp.verifyComponentExistence(rcp.clausoleQues9);
			rcp.comprareText(rcp.clausoleQues9, ResCutomerProfileComponent.CLAUSOLE_QUES9, true);
			logger.write("Verify the Clausole Legali Questions - Complete");
			
			logger.write("Swith to parent window   - Start");
			driver.switchTo().window(currentHandle);
			logger.write("Switch to parent window   - Complete");
			
			System.out.println(driver.getCurrentUrl());
			prop.setProperty("MODIFICAPROF", "https://www-coll1.enel.it/it/modifica-profilo");
			rcp.checkURLAfterRedirection(prop.getProperty("MODIFICAPROF"));
			
			logger.write("Verify and click on Torna clienti link - Start");
			rcp.verifyComponentExistence(rcp.tornaClientiLink);
			rcp.clickComponent(rcp.tornaClientiLink);
			logger.write("Verify and click on Torna clienti link - Complete");
					
					
			logger.write("Switch to New window - Start");
			rcp.SwitchTabToSupporto();
			logger.write("Switch to New window - Complete");
			
					
			logger.write("Verify the home page location  - Start");
			rcp.verifyComponentExistence(rcp.custHomePageloc);
			rcp.comprareText(rcp.custHomePageloc, ResCutomerProfileComponent.CUSTHOMEPAGE_LOC, true);
			logger.write("Verify the home page location  - Complete");
			
			
			logger.write("Swith to parent window   - Start");
			driver.switchTo().window(currentHandle);
			logger.write("Switch to parent window   - Complete");
			
						
			System.out.println(driver.getCurrentUrl());
			rcp.checkURLAfterRedirection(prop.getProperty("MODIFICAPROF"));
			
			logger.write("Close the child windows   - Start");
			rcp.closeChildWindows();
			logger.write("Close the child windows   - Complete");
			
			driver.switchTo().window(currentHandle);
			System.out.println(driver.getCurrentUrl());
			rcp.checkURLAfterRedirection(prop.getProperty("MODIFICAPROF"));
			
			
			logger.write("Verify and click on Esci link   - Start");
			rcp.verifyComponentExistence(rcp.esciLink);
			rcp.clickComponent(rcp.esciLink);
			logger.write("Verify and click on Esci link   - Complete");
			
			logger.write("Verify the home page URL   - Start");
			rcp.checkURLAfterRedirection(prop.getProperty("HOME_URL"));
			logger.write("Verify the home page URL   - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
		
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	


}
