package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaDisattivazioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Info_Enel_Energia_279_ACR {
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			LoginPageValidateComponent lpv = new LoginPageValidateComponent(driver);
			PrivateAreaDisattivazioneFornituraComponent dfc = new PrivateAreaDisattivazioneFornituraComponent(driver);
				
			lpv.launchLink(prop.getProperty("LINK"));
			lpv.verifyComponentExistence(lpv.homePageClose);
			lpv.clickComponent(lpv.homePageClose);
			lpv.verifyComponentExistence(lpv.logoEnel);
			lpv.verifyComponentExistence(lpv.buttonAccetta);
			lpv.clickComponent(lpv.buttonAccetta);
			
			lpv.verifyComponentExistence(lpv.iconUser);
			lpv.clickComponent(lpv.iconUser);
						
			logger.write("Verify  rucepera password link  - Start");
			lpv.verifyComponentExistence(lpv.rucuperaPassword);
			logger.write("Verify rucepera password link  - Complete");
												
			logger.write("Enter login button without input  - Start");
			dfc.verifyComponentExistence(dfc.username);
			dfc.verifyComponentExistence(dfc.password);
			dfc.verifyComponentVisibility(dfc.buttonLoginAccedi);
			logger.write("Enter login button without input  - Complete");
			
			logger.write("Enter the username and password  - Start");
			dfc.enterInput(dfc.username, prop.getProperty("USERNAME"));
			dfc.enterInput(dfc.password, prop.getProperty("PASSWORD"));
			logger.write("Enter the username and password  - Complete");
			
			logger.write("Click on login button  - Start");
			dfc.clickComponent(dfc.buttonLoginAccedi);
			logger.write("Click on login button  - Complete");
			
			DettaglioFornitureComponent df = new DettaglioFornitureComponent(driver);
			
			logger.write("Verify the home page header  - Start");
			df.verifyComponentExistence(dfc.BenvenutoTitle);
			df.comprareText(df.BenvenutoTitle, DettaglioFornitureComponent.BENVENUTO_TITLE, true);
			df.verifyComponentExistence(df.BenvenutoContent);
			df.comprareText(df.BenvenutoContent, DettaglioFornitureComponent.BENVENUTO_CONTENT, true);
			df.verifyComponentExistence(df.fornitureHeader);
			df.comprareText(df.fornitureHeader, DettaglioFornitureComponent.FORNITURE_HEADER, true);
			df.verifyComponentExistence(df.fornitureContent);
			//df.comprareText(df.fornitureContent, DettaglioFornitureComponent.FORNITURE_CONTENT, true);
			logger.write("Verify the home page header  - Complete");
						
			logger.write("Click on Servizi link  - Start");
			df.verifyComponentExistence(df.serivizLink);
			df.clickComponent(df.serivizLink);
			logger.write("Click on Servizi link  - Complete");
									
			logger.write("Verify the Bollette servizi header  - Start");
			df.verifyComponentExistence(df.serviziBolletteHeading);
			df.comprareText(df.serviziBolletteHeading, DettaglioFornitureComponent.SERVIZIBOllE_HEADING, true);
			df.verifyComponentExistence(df.seriviziBolletteContent);
			df.comprareText(df.seriviziBolletteContent, DettaglioFornitureComponent.SERVIZIBOLLE_CONTENT, true);
			logger.write("Verify the Bollette servizi header  - Complete");
						
			logger.write("Verify the Servizi forniture contents  - Start");
			df.verifyComponentExistence(df.serviziForniture);
			df.comprareText(df.serviziForniture, DettaglioFornitureComponent.SERVIZI_FORNITURE, true);
			df.verifyComponentExistence(df.serviziFornitureContent);
			df.comprareText(df.serviziFornitureContent, DettaglioFornitureComponent.SERVIZIFORNITURE_CONTENT, true);
			logger.write("Verify the Servizi forniture contents  - Start");
			
			logger.write("Click on Info Enel Energia link  - Start");
			df.verifyComponentExistence(df.infoEnelEnergiaLink);
			df.clickComponent(df.infoEnelEnergiaLink);
			logger.write("Click on Info Enel Energia link  - Start");
			
			logger.write("Verify the InfoEnel Energia contents - Start");
			df.verifyComponentExistence(df.infoEnelEnergiaHeader);
			df.comprareText(df.infoEnelEnergiaHeader, DettaglioFornitureComponent.INFOENELENERGIA_HEADER, true);
			df.comprareText(df.infoEnelEnergiaContent, DettaglioFornitureComponent.INFOENELENERGIA_CONTENT, true);
			logger.write("Verify the InfoEnel Energia contents - Start");
			
			logger.write("Click on Modifica InfoEnel Energia button - Start");
			df.verifyComponentExistence(df.modificaInfoEnelEnergiaBtn);
			df.clickComponent(df.modificaInfoEnelEnergiaBtn);
			logger.write("Click on Modifica InfoEnel Energia button - Complete");
			
			logger.write("Verify Modifica infoEnel Energia contents - Start");
			df.verifyComponentExistence(df.modificaInfoEnelEnergiaHeader);
			df.comprareText(df.modificaInfoEnelEnergiaHeader, DettaglioFornitureComponent.MODIFICA_INFOENELHEADER, true);
			df.verifyComponentExistence(df.modificaInfoEnelEnergiaContent);
			df.comprareText(df.modificaInfoEnelEnergiaContent, DettaglioFornitureComponent.MODIFICA_INFOENELCONTENT, true);
			logger.write("Verify Modifica infoEnel Energia contents - Complete");
			
			logger.write("Click on radio button for Electricity - Start");
			df.verifyComponentExistence(df.radioBtnGas);
			df.clickComponent(df.radioBtnGas);
			logger.write("Click on radio button for Electricity - Complete");
			
			logger.write("Verify the Esci and Continua buttons - Start");
			df.verifyComponentExistence(df.esciButton);
			df.verifyComponentExistence(df.continuaBtn);
			df.clickComponent(df.continuaBtn);
			logger.write("Verify the Esci and Continua buttons - Complete");
			
			//step 8
			logger.write("Verify the status - Start");
			df.verifyStatus();
			logger.write("Verify the status - Complete");
			
			logger.write("Verify the checbox title  - Start");
			df.verifyComponentExistence(df.checkboxHeader);
			df.checkboxTitle(df.checkboxHeader);
			df.verifyCheckboxLabel(df.checkBoxLabel);
			logger.write("Verify the checbox title  - Complete");
			
			logger.write("Enter the input and select the checkbox - Start");
			df.verifyComponentExistence(df.inputEmail);
			df.clearText(df.inputEmail);
			df.enterInput(df.inputEmail, prop.getProperty("USERNAME"));
			df.verifyComponentExistence(df.confermaEmail);
			df.enterInput(df.confermaEmail, prop.getProperty("CONFERMAEMAIL"));
			df.verifyComponentExistence(df.cellulareField);
			df.enterInput(df.cellulareField, "CELLULARE");
			df.verifyComponentExistence(df.aggiornaCheckboxInput);
			df.verifyComponentExistence(df.aggironaCheckboxLabel);
			df.comprareText(df.aggironaCheckboxLabel, DettaglioFornitureComponent.CHECKBOX_LABEL, true);
			df.clickComponent(df.aggironaCheckboxLabel);
			logger.write("Enter the input and select the checkbox - Complete");
			
			logger.write("Click on continua button - Start");
			df.verifyComponentExistence(df.continuButton);
			df.verifyComponentExistence(df.indietroButton);
			//df.clickComponent(df.continuButton);
			logger.write("Click on continua button - Complete");
			
			//step 9
			df.clickComponent(df.indietroButton);
			

			logger.write("Verify Modifica infoEnel Energia contents - Start");
			df.verifyComponentExistence(df.modificaInfoEnelEnergiaHeader);
			df.comprareText(df.modificaInfoEnelEnergiaHeader, DettaglioFornitureComponent.MODIFICA_INFOENELHEADER, true);
			df.verifyComponentExistence(df.modificaInfoEnelEnergiaContent);
			df.comprareText(df.modificaInfoEnelEnergiaContent, DettaglioFornitureComponent.MODIFICA_INFOENELCONTENT, true);
			logger.write("Verify Modifica infoEnel Energia contents - Complete");
			
			//step 10
			df.clickComponent(df.continuaBtn);
			
			logger.write("Verify the status - Start");
			df.verifyStatus();
			logger.write("Verify the status - Complete");
			
			logger.write("Verify the checbox title  - Start");
			df.verifyComponentExistence(df.checkboxHeader);
			df.checkboxTitle(df.checkboxHeader);
			df.verifyCheckboxLabel(df.checkBoxLabel);
			logger.write("Verify the checbox title  - Complete");
			
			df.enterInput(df.inputEmail, prop.getProperty("USERNAME"));
			
			//step10	
			
			df.verifyComponentExistence(df.checkboxSMSFirst);
			df.clickComponent(df.checkboxSMSFirst);
			
			df.clickComponent(df.continuButton);
												
			logger.write("Verify the checkbox headers - Start");
			df.verifyCheckboxHeaders(df.checkboxHeaders);
			logger.write("Verify the checkbox headers - Complete");
		
			logger.write("Verify the tipologia contents  - Start");
			df.verifyComponentExistence(df.tipologiaHeader);
			df.comprareText(df.tipologiaHeader, DettaglioFornitureComponent.TIPOLOGIA_HEADER, true);
			df.verifyComponentExistence(df.supplyData);
			df.comprareText(df.supplyData, DettaglioFornitureComponent.SUPPLY_DATA, true);
			logger.write("Verify the tipologia contents  - Complete ");
			
												
			logger.write("Verify the Indietro and Conferma buttons  - Start");
			df.verifyComponentExistence(df.energiaIndietroBtn);
			df.verifyComponentExistence(df.energiaConfermaBtn);
			logger.write("Verify the Indietro and Conferma buttons  - Complete");
			
			df.clickComponent(df.energiaIndietroBtn);
			
			logger.write("Verify the status - Start");
			df.verifyStatus();
			logger.write("Verify the status - Complete");
			
			logger.write("Verify the checbox title  - Start");
			df.verifyComponentExistence(df.checkboxHeader);
			df.checkboxTitle(df.checkboxHeader);
			df.verifyCheckboxLabel(df.checkBoxLabel);
			logger.write("Verify the checbox title  - Complete");
						
			df.clickComponent(df.continuButton);
			
			logger.write("Verify the Indietro and Conferma buttons  - Start");
			df.verifyComponentExistence(df.energiaIndietroBtn);
			df.verifyComponentExistence(df.energiaConfermaBtn);
			logger.write("Verify the Indietro and Conferma buttons  - Complete");
						
			logger.write("Click on Conferma button  - Start");
			//df.clickComponent(df.energiaConfermaBtn);
			logger.write("Click on Conferma button  - Start");
			
			prop.setProperty("RETURN_VALUE", "OK");
						
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
	

	
	
}
