package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_138 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
								
			home.clickComponent(home.services);
			
			ServicesComponent services = new ServicesComponent(driver);
			
			logger.write("Check For supply services-- start");
			
			services.verifyComponentExistence(services.serviceTitle);
			
			logger.write("Check For supply services-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			services.waitForElementToDisplay(services.modificaPotenzaeoTensione);
			
			services.clickComponent(services.modificaPotenzaeoTensione);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Check Power And Voltage Content-- Start");

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);

			changePV.verifyComponentExistence(changePV.changePowerAndVoltageContent);
			
			changePV.checkForPageContent();
			
			logger.write("Check Power And Voltage Content-- Start");
			
			changePV.verifyComponentExistence(changePV.enelLogo);
			
			changePV.verifyComponentExistence(changePV.userIcon);
			
			changePV.comprareText(changePV.user, prop.getProperty("USER_NAME"), true);
			
			changePV.checkTextHighlight(changePV.services, changePV.servicesColor);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageButton);

			logger.write("Check Faq one details -- Start");
			
			changePV.checkFaqOneDetails();
			
			logger.write("Check Faq one details -- Completed");

			logger.write("Check Faq details -- Start");

			changePV.checkFaqDetails();
			
			logger.write("Check Faq details -- Completed");
			
			logger.write("Check Bottom section -- Start");

			changePV.checkFooterSection(changePV.enelEnergia, changePV.bottomSection);
			changePV.checkFooterSection(changePV.tuttiIDirittiRiservati, changePV.bottomSection);
			changePV.checkFooterSection(changePV.pVIA, changePV.bottomSection);
			changePV.checkFooterSection(changePV.legali, changePV.bottomSection);
			changePV.checkFooterSection(changePV.privacy, changePV.bottomSection);
			changePV.checkFooterSection(changePV.credits, changePV.bottomSection);
			changePV.checkFooterSection(changePV.contatti, changePV.bottomSection);
			
			logger.write("Check Bottom section -- Completed");

			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			prop.setProperty("RETURN_VALUE", "OK");

			
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}

}
