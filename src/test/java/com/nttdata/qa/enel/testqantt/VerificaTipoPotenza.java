package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;
import org.junit.Assert;

import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class VerificaTipoPotenza {
    @Step("Verifica Tipo Potenza")
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);
        RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

        try {

/*
            logger.write("Verifica Tipo Potenza - Start");
    		if (prop.getProperty("ITA_IFM_POWERTYPE__C").contentEquals("NON LIMITATA")) 
    			throw new IllegalArgumentException("Attenzione! Tipo Potenza Errata: '"+prop.getProperty("ITA_IFM_POWERTYPE__C")+"' Valore Atteso: LIMITATA");
//                prop.setProperty("RETURN_VALUE", "KO");
    		 if (prop.getProperty("ITA_IFM_POWERTYPE__C").contentEquals("LIMITATA")) {
                logger.write("Tipo Potenza corrispondente al valore atteso: '"+prop.getProperty("ITA_IFM_POWERTYPE__C")+ "'");
                prop.setProperty("RETURN_VALUE", "OK");
    		} else {
    			throw new IllegalArgumentException("Tipo Potenza contiene un valore inatteso:  '"+prop.getProperty("ITA_IFM_POWERTYPE__C")+"'");
//                prop.setProperty("RETURN_VALUE", "KO");
    		}
    		logger.write("Verifica Tipo Potenza - Completed"); 
*/    		 
    		
    		
    		
    		logger.write("Verifica Tipo Potenza - Start");
    		
    		if(prop.getProperty("VERIFICA_POTENZA").contentEquals("LIMITATA")) {
    			Assert.assertEquals("LIMITATA", prop.getProperty("ITA_IFM_POWERTYPE__C"));
    			logger.write("Tipo Potenza corrispondente al valore atteso: '"+prop.getProperty("ITA_IFM_POWERTYPE__C")+ "'");
    		} else if (prop.getProperty("VERIFICA_POTENZA").contentEquals("NON LIMITATA")) {
    			Assert.assertEquals("NON LIMITATA", prop.getProperty("ITA_IFM_POWERTYPE__C"));
    			logger.write("Tipo Potenza corrispondente al valore atteso: '"+prop.getProperty("ITA_IFM_POWERTYPE__C")+ "'");
    		} else {
    			throw new Exception("Verifica Potenza Non Eseguibile");
    		}
    		 
    		 
            logger.write("Verifica Tipo Potenza - Completed");

        } catch (Exception e) {
        	System.out.println("entra nel catch 2");
        	e.printStackTrace();
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
