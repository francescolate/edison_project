package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_266_InfoEnergia {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
			
			logger.write("Verification of correct display of the customer's Home Page with Logo on the left- Starts");
			iee.verifyComponentExistence(iee.homepageLogo);
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Verification of correct display of the customer's Home Page with Logo on the left- Ends");
			
			logger.write("Click on Servizi Menu Item - Starts");
			iee.clickComponent(iee.servizi);
			logger.write("Click on Servizi Menu Item - Ends");
			
			logger.write("Verification of correct display of the servizi Page with data- Starts");
			iee.comprareText(iee.serviziHomepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.serviziHomepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_SERVIZI_PATH2, true);
			iee.comprareText(iee.serviziTitle, InfoEnelEnergiaACRComponent.SERVIZI_TITLE, true);
			iee.comprareText(iee.serviziText, InfoEnelEnergiaACRComponent.SERVIZI_TEXT, true);
			logger.write("Verification of correct display of the servizi Page with data- Ends");
			
			logger.write("Click on InfoEnelEnrgia Menu Item - Starts");
			Thread.sleep(10000);
			iee.clickComponent(iee.infoEnelEnergia);
			logger.write("Click on InfoEnelEnrgia Menu Item - Ends");
			
			logger.write("Verification of correct display of the info enel enrgia Page with data- Starts");
			iee.comprareText(iee.infoEnelEnergiaTitle, InfoEnelEnergiaACRComponent.INFO_ENEL_ENERGIA_TITLE, true);
			iee.comprareText(iee.infoEnelEnergiaText, InfoEnelEnergiaACRComponent.INFO_ENEL_ENERGIA_TEXT, true);
			iee.verifyComponentExistence(iee.clientNumber);
			iee.comprareText(iee.notificheText, InfoEnelEnergiaACRComponent.NOTIFICHE_TEXT, true);
			iee.verifyComponentExistence(iee.email);
			iee.verifyComponentExistence(iee.cellulare);
			iee.comprareText(iee.modifica, InfoEnelEnergiaACRComponent.MODIFICA, true);
			iee.comprareText(iee.modificaText, InfoEnelEnergiaACRComponent.MODIFICA_TEXT, true);
			iee.verifyComponentExistence(iee.scopriDiPIUModifica);
			iee.comprareText(iee.revoca, InfoEnelEnergiaACRComponent.REVOCA, true);
			iee.comprareText(iee.revocaText, InfoEnelEnergiaACRComponent.REVOCA_TEXT, true);
			iee.verifyComponentExistence(iee.scopriDiPIURevoca);
			logger.write("Verification of correct display of the info enel enrgia Page with data- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
