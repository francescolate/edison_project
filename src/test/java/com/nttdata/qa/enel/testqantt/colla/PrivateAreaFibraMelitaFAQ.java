package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFibraMelitaFAQ {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);

			logger.write("click the button Show Offer in the card with FIBRA-MELITA offer - START");

			//Seleziona il frame
			By id_frame = paf.id_frame;
			
			paf.verifyComponentExistence(paf.id_frame);	
			
			paf.verifyFrameAvailableAndSwitchToIt(id_frame);

			logger.write("click the button Show Offer in the card with FIBRA-MELITA offer - COMPLETED");

			//Click tasto close
		/*	logger.write("click the Close button - START");

			By button_close = paf.button_close;

			Thread.sleep(3000);

			paf.verifyComponentExistence(button_close);
			paf.clickComponent(button_close);

			logger.write("click the Close button - COMPLETED");
			
			//STEP 7 Verifica Informativa
			By icon_informativa = paf.icon_informativa;
			Thread.currentThread().sleep(2000);
			//paf.clickElementByIndex(icon_informativa, 2);
			paf.clickComponent(paf.icon_informativa);
			
			logger.write("check the popup title text - START");

			paf.verifyComponentExistence(paf.text_popup_title_2);
			paf.verifyComponentVisibility(paf.text_popup_title_2);

			logger.write("check the popup title text - COMPLETED");

			logger.write("check the popup descrition text - START");

			paf.verifyComponentExistence(paf.text_popup_text_2);
			paf.verifyComponentVisibility(paf.text_popup_text_2);

			logger.write("check the popup descrition text - COMPLETED");
		
			//STEP 8 Domande frequenti
			logger.write("check if the 'Domande Frequenti' aren't visible - START");
			
			By faq_text = By.xpath("//*[text(),'Domande Frequenti']");
			
			paf.verifyComponentInvisibility(faq_text);
			
			logger.write("check if the 'Domande Frequenti' aren't visible - COMPLETED");*/

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
