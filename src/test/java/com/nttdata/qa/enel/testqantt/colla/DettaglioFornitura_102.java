package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioFornitura_102 {

public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

try {
	
	RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
	prop.setProperty("RESIDENTIAL_MENU", " Forniture | Bollette | Servizi | enelpremia WOW! | Novità | Spazio Video | Account | I tuoi diritti | Supporto | Trova Spazio Enel | Esci ");
	
	BolletteComponent 	bc = new BolletteComponent(driver);
	
	logger.write("Check for Residential Menu - Start");
	bc.checkForResidentialMenu(prop.getProperty("RESIDENTIAL_MENU"));
	logger.write("Check for Residential Menu - Completed");
	
	logger.write("Click on Bills from Residential Menu - Start");
	bc.clickComponent(bc.bills);
	logger.write("Click on Bills from Residential Menu - Completed");
	
	logger.write("Verification of List of One or more bills assocaited with the supply - Started");
	bc.verifyComponentExistence(bc.listofBills);
	logger.write("Verification of List of One or more bills assocaited with the supply - Completed");
	
	logger.write("Clicking on the one of the bills listed - Start");
	bc.clickComponent(bc.oneOfBillList);
	logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");
		
	DettaglioBollettaComponent db = new DettaglioBollettaComponent(driver);
	
	logger.write("Verify the fields in Dettaglio Fornitura page - Started");
	
	db.verifyComponentExistence(db.datiAggiornati);
	db.verifyComponentExistence(db.gasIcon);
	db.verifyComponentExistence(db.VAIATUTTELEBOLLETTEButton);
	db.verifyComponentExistence(db.SCARICAPDFPIANOButton);
	
	db.verifyComponentExistence(db.billHeading);
	db.comprareText(db.billHeading, DettaglioBollettaComponent.BILL_HEADING, true);
	
	db.verifyComponentExistence(db.addressLabel);
	db.comprareText(db.addressLabel, DettaglioBollettaComponent.ADDRESS_LABEL, true);
	
	db.verifyComponentExistence(db.addressvalue);
	db.comprareText(db.addressvalue, DettaglioBollettaComponent.ADDRESS_VALUE, true);
	
	db.verifyComponentExistence(db.numeroClienteLabel);
	db.comprareText(db.numeroClienteLabel, DettaglioBollettaComponent.NUMERO_CLIENTE_LABEL, true);
	
	db.verifyComponentExistence(db.numeroCliente);
	db.comprareText(db.numeroCliente, DettaglioBollettaComponent.NUMERO_CLIENTE, true);
	
	db.verifyBilldetails(db.billDetails, DettaglioBollettaComponent.exp);
	logger.write("Verify the fields in Dettaglio Fornitura page - Completed");
	
	db.isStatusDaPagare(db.statoPagamento);
		
	prop.setProperty("RETURN_VALUE", "OK");

} catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

	StringWriter errors = new StringWriter();
	e.printStackTrace(new PrintWriter(errors));
	errors.toString();
	logger.write("ERROR_DESCRIPTION: " + errors.toString());

	prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
	if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
		throw e;

//	prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
} finally {
	// Store WebDriver Info in properties file
	prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
}
}
}
