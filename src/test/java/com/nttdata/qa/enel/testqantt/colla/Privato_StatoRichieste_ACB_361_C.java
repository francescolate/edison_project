package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.StatoRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_StatoRichieste_ACB_361_C {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);	
			StatoRichiesteComponent src = new StatoRichiesteComponent(driver);
			
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			Thread.sleep(5000);
			logger.write("Correct visualization of the Home Page with text- Ends");

			logger.write("Click on STATO RICHIESTE and verify - Starts");
			src.clickComponent(src.statoRichieste);
			src.verifyComponentExistence(src.pageTitle);
			src.comprareText(src.pagePath, StatoRichiesteComponent.Path, true);
			src.comprareText(src.subText, StatoRichiesteComponent.SubText, true);
			logger.write("Click on STATO RICHIESTE and verify- Ends");
			
			logger.write("verify tabs on STATO RICHIESTE page - Starts");
			src.verifyComponentExistence(src.contratti);
			src.verifyComponentExistence(src.servizi);
			src.verifyComponentExistence(src.modificheContatore);
			src.verifyComponentExistence(src.mostra);
			Thread.sleep(5000);
			logger.write("Cverify tabs on STATO RICHIESTE page - Ends");
			
			logger.write("Click on servizi and verify the page - Starts");
			src.verifyComponentExistence(src.servizi);
			src.clickComponent(src.servizi);
			src.verifyComponentExistence(src.contratti);
			src.verifyComponentExistence(src.modificheContatore);
			Thread.sleep(5000);
			logger.write("Click on servizi and verify the page - Ends");
			
			logger.write("verify the page fields - Starts");
			src.verifyComponentExistence(src.infoenelenergia);
			src.verifyComponentExistence(src.infoenelenergiaTipo);
			src.verifyComponentExistence(src.infoenelenergiaNumerocliente);
			src.verifyComponentExistence(src.infoenelenergiaTipoperazione);
			src.verifyComponentExistence(src.infoenelenergiaDatarichiesta);
			logger.write("verify the page fields - Ends");
			
			logger.write("Verify the status - Starts");
			src.verifyComponentExistence(src.inlavorazione);
			src.verifyComponentExistence(src.eSITO);
			logger.write("Verify the status  - Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
