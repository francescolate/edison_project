package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerificaPodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS {

	@Step("R2D Verifiche POD Iniziali Switch - GAS")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			String statopratica="vuota";
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				logger.write("Menu Report Switch e Disdette - Start");
				menuBox.selezionaVoceMenuBox("Interrogazione","Report SWA");
				logger.write("Menu Report Switch e Disdette - Completed");
				//Indice POD
				int indice=0;
//				if(!prop.containsKey("INDICE_POD")){
				if (prop.getProperty("INDICE_POD","").equals("")){
					indice=1;
					prop.setProperty("INDICE_POD", String.valueOf(indice));
				}
				else{
					indice=Integer.parseInt(prop.getProperty("INDICE_POD"));
				}
				//Selezione tipologia
				R2D_VerificaPodComponent verifichePod = new R2D_VerificaPodComponent(driver);
				//Inserimento PDR oppure Id Richiesta
//				if(prop.getProperty("PDR").isEmpty()){
					logger.write("inserisci Id_Richiesta - Start");
//					verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
					if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
						verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA"));
					} else {
						verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_ORDINE"));
					}
					logger.write("inserisci Id_Richiesta - Completed");
//				}else{
//					logger.write("inserisci Pod - Start");
//					verifichePod.inserisciPod(verifichePod.inputPDR, prop.getProperty("PDR"));
//					logger.write("inserisci Pod - Completed");
//				}
				//Inserimento ID richiesta CRM
//				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA_CRM_GAS_"+indice,prop.getProperty("ID_RICHIESTA_CRM_GAS")));
				//Click Cerca
				logger.write("Pressione bottone Cerca - Start");
				verifichePod.cercaPod(verifichePod.buttonCerca);
				logger.write("Pressione bottone Cerca - Completed");
				//Click Dettaglio Pratiche
				logger.write("Bottone Dettaglio Pratica - Start");
				verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				logger.write("Bottone Dettaglio Pratica - Completed");
				//Click su Pratica
				logger.write("Apri Pratica - Start");
				verifichePod.selezionaPratica(verifichePod.rigaPratica);
				logger.write("Apri Pratica - Completed");
				//Verifica Campi
				//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
				//				campiDaVerificare.add("Esito MUTI;"+"OK");
				//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
//				campiDaSalvare.add("ID R2D");
//				campiDaSalvare.add("Data decorrenza");
				campiDaSalvare.add("Numero Ordine CRM"); 
				campiDaSalvare.add("Data Minima Switch"); 
				campiDaSalvare.add("Stato Pratica");
				campiDaSalvare.add("Data Invio Recesso"); 
				campiDaSalvare.add("Distributore"); 
				campiDaSalvare.add("Matricola Contatore"); 
				campiDaSalvare.add("Classe Contatore"); 
//				campiDaSalvare.add("Tipo PDR");  // Cod tipologia pdr
				campiDaSalvare.add("Codice Richiesta Trader"); 
				campiDaSalvare.add("Codice Pool"); 
							
				//Salvare eventualmente stato indicizzato
				logger.write("Verifiche e Salvataggio campi Pratica - Start");
				verifichePod.verificaDettaglioPratica_GAS(prop,campiDaVerificare,campiDaSalvare);
				logger.write("Verifiche e Salvataggio campi Pratica - Completed");

				//Verifico che lo stato iniziale della pratica può essere AW o AA
//				statopratica = statopratica.substring(0, 2);
				statopratica = prop.getProperty("STATO_PRATICA").substring(0, 2);
				logger.write("Verifiche Iniziali Stato Pratica - Start");
				if(statopratica.compareToIgnoreCase("CI")==0 || statopratica.compareToIgnoreCase("OK")==0 || statopratica.compareToIgnoreCase("FA")==0){
					logger.write("Verifiche Iniziali Stato Pratica (CI/FA/OK) - Completed");
					} else if(statopratica.compareToIgnoreCase("AW")!=0 && statopratica.compareToIgnoreCase("AA")!=0){
						throw new Exception("Lo stato iniziale della pratica non è corretto:"+statopratica+". Stato pratica atteso AW o AA");
					}
					logger.write("Verifiche Iniziali Stato Pratica - Completed");
				}


			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
