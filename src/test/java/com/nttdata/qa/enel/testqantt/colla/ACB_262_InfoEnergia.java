package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSNFornitureComponent;
import com.nttdata.qa.enel.components.colla.BSN_InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_262_InfoEnergia {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Click on Piu Informazioni Link -- Start");
			pa.isDisplayedChatWindow(pa.chatClose);
			pa.clickComponent(pa.piuInformazioniLink);
			logger.write("Click on Piu Informazioni Link -- Completed");
			
			FornitureBSNComponent sc = new FornitureBSNComponent(driver);
			Thread.sleep(10000);
			logger.write("Verify page navigation and page details -- Start");
			pa.verifyComponentExistence(sc.pageTitle);
			sc.comprareText(sc.pageTitle, sc.PageTitle, true);
			sc.comprareText(sc.path, sc.Path, true);
			sc.comprareText(sc.titleSubText, sc.TitleSubText, true);
			
			pa.verifyComponentExistence(sc.gestisciletueForniture);
			sc.comprareText(sc.infoEnelEnergiaText, sc.InfoEnelEnergiaRowText, true);
			pa.verifyComponentExistence(sc.scopriDiPiuLink);
			Thread.sleep(10000);
			logger.write("Verify page navigation and page details -- Completed");

			logger.write("Click on scopri di piu link -- Start");
			sc.clickComponent(sc.scopriDiPiuLink);
			logger.write("Click on scopri di piu link -- Completed");

			logger.write("Verify info enel energia page details-- Start");
			BSN_InfoEnelEnergiaComponent bic = new BSN_InfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			bic.verifyComponentExistence(bic.page_title261);
			bic.comprareText(bic.page_title261, bic.PageTitle, true);
			bic.comprareText(bic.page_subtitle261, bic.PageSubText261, true);
//			bic.comprareText(bic.text, bic.Text, true);
			logger.write("Verify info enel energia page details-- Completed");
			logger.write("Click on ModificaScopriDiPiu-- Start");
			bic.clickComponent(bic.modificaScopriDiPiu);
			logger.write("Click on ModificaScopriDiPiu-- Completed");

			logger.write("Verify Modifica Info enel enegia page,menu one details-- Start");
			BSN_ModificaInfoEnelEnergiaComponent bmc = new BSN_ModificaInfoEnelEnergiaComponent(driver);
			Thread.sleep(10000);
			bmc.verifyComponentExistence(bmc.pageTitleNew);
			bmc.comprareText(bmc.pagePath, bmc.PagePath1, true);
			bmc.comprareText(bmc.pageTitleNew, bmc.PageTitle1, true);
			bmc.comprareText(bmc.pageSubText, bmc.PageSubText, true);
			bmc.verifyComponentExistence(bmc.menuOne);
			bmc.comprareText(bmc.menuOneDescription, bmc.MenuOneDescription, true);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLettura);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaSMS);
			bmc.verifyComponentExistence(bmc.avvisoComunicaLetturaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBolletta);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaSMS);
			bmc.verifyComponentExistence(bmc.avvisoEmissioneBollettaEmail);
			
			bmc.verifyComponentExistence(bmc.avvenutoPagamento);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvenutoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenza);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaSMS);
			bmc.verifyComponentExistence(bmc.avvisoProssimaScadenzaEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamento);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.avvisoSollecitoPagamentoEmail);
			
			bmc.verifyComponentExistence(bmc.avvisoConsumi);
			bmc.verifyComponentExistence(bmc.avvisoConsumiSMS);
			bmc.verifyComponentExistence(bmc.avvisoConsumiEmail);
			
			bmc.comprareText(bmc.sectionTitle, bmc.SectionTitle, true);
			bmc.selectCheckBox(bmc.avvenutoPagamentoSMS);
			bmc.verifyComponentExistence(bmc.email);
			bmc.verifyComponentExistence(bmc.confirmEmail);
			bmc.verifyComponentExistence(bmc.cellulare);
			bmc.verifyComponentExistence(bmc.confermaCellulare);
			bmc.verifyComponentExistence(bmc.aggiornaIDatiDiContatto);
			bmc.clickComponent(bmc.aggiornaIDatiDiContatto);
			Thread.sleep(10000);
			bmc.verifyComponentExistence(bmc.esci);
			bmc.verifyComponentExistence(bmc.indietro);
			bmc.clickComponent(bmc.prosegui);
			Thread.sleep(10000);
			bmc.verifyComponentExistence(bmc.iIcon);
			bmc.clickComponent(bmc.iIcon);
			Thread.sleep(10000);
			bmc.comprareText(bmc.iIconText, bmc.IiconText, true);
			bmc.clickComponent(bmc.iIcon);
			Thread.sleep(10000);
			
			bmc.verifyContentNotDispaying(bmc.iIconText);
			logger.write("Verify Modifica Info enel enegia page,menu one details-- Completed");
			bmc.clickComponent(bmc.indietro);
			
			logger.write("Verify info enel energia page details-- Start");
			Thread.sleep(10000);
			bic.verifyComponentExistence(bic.pageTitle);
			bic.comprareText(bic.page_title261, bic.PageTitle, true);
			bic.comprareText(bic.page_subtitle261, bic.PageSubText261, true);
//			bic.comprareText(bic.text, bic.Text, true);
			logger.write("Verify info enel energia page details-- Completed");
		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
