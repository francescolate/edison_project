package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteMyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaBolletteBSNSection  {

	public static void main(String[] args)throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaBolletteMyComponent pabc = new PrivateAreaBolletteMyComponent(driver);
			By by = null;
			
			if(prop.containsKey("ACTION_TYPE"))
				if(prop.get("ACTION_TYPE").equals("MENU_ITEM_SERVIZI"))
					by = pabc.serviziMenuItem; 
				else if(prop.get("ACTION_TYPE").equals("DETTAGLIO_FORNITURA_BUTTON"))
					by = pabc.buttonDettaglioFornitura;
				else
					by = null;
			else
				by = pabc.bolletteLink;
					
			logger.write("checking and clicking the link related to the active supply - START");
			pabc.verifyComponentExistence(by);
			pabc.clickComponent(by);
			logger.write("checking and clicking the link related to the active supply  - COMPLETED");
			
			logger.write("checking the Bollete Page path - START");
//			pabc.checkMenuPath(PrivateAreaBolletteMyComponent.BOLLETTEPATH, pabc.bollettePath);
			logger.write("checking the Bollete Page path - COMPLETED");
			
			logger.write("checking the Bollete Header and Text  - START");
			pabc.verifyComponentExistence(pabc.bolletteHeader1);
			pabc.verifyComponentExistence(pabc.bolletteTitle);
			logger.write("checking the Bollete Header and Text  - CMPLETED");
			
			logger.write("checking the feild and text contained within feild - START");
//			pabc.verifyComponentExistence(pabc.bolletteClienteNFeild);
//			pabc.verifyFeildAttribute(pabc.bolletteClienteNFeild, PrivateAreaBolletteMyComponent.text);
			logger.write("checking the feild and text contained within feild - COMPLETED");
			
			logger.write("Verify Applica Button-START");
			pabc.verifyComponentExistence(pabc.applica);
			logger.write("Verify Applica Button-COMPLETED");
			
			logger.write("checking the list of address available to the customer - START");
			pabc.checkForLiftTypeDropDownValue(pabc.addressList);
			logger.write("checking the list of address available to the customer - START");
			
			logger.write("checking the error message display when entering wrong input - START");
			pabc.clickComponent(pabc.bolletteClienteNFeild);
			pabc.enterInputtoField(pabc.bolletteClienteNFeild, prop.getProperty("INCORRECTINPUT"));
			pabc.clickComponent(pabc.buttonApplica);
			pabc.comprareText(pabc.errorText, PrivateAreaBolletteMyComponent.ERROR_TEXT_MESSAGE, true);
			logger.write("checking the error message display when entering wrong input as ABCDEFG - ENDS");
						
			logger.write("checking the error message display when entering wrong input as 12345678 - ENDS");
			pabc.clickComponent(pabc.bolletteClienteNFeild);
			pabc.enterInputtoField(pabc.bolletteClienteNFeild, prop.getProperty("INCORRECTINPUT1"));
			pabc.clickComponent(pabc.buttonApplica);
			pabc.comprareText(pabc.errorText, PrivateAreaBolletteMyComponent.ERROR_TEXT_MESSAGE, true);
			logger.write("checking the error message display when entering wrong input as ABCDEFG - ENDS");
			
			logger.write("Verifying the result by selecting the correct Address - STARTS");
			//pabc.clearInputFromField(pabc.bolletteClienteNFeild);
			//pabc.clickComponent(pabc.buttonApplica);
			//pabc.refreshPage();
			//pabc.jsClickElement(pabc.bolletteClientNFieldNew);
			//pabc.waitForElementToDisplay(pabc.waitForList);
			//pabc.jsClickElement(pabc.bolletteClientNFieldNew);
			//pabc.buttonClickRobot(pabc.bolletteClientNFieldNew);
			//pabc.clickComponent(pabc.bolletteClientNFieldNew);
//			pabc.clickComponent(pabc.bolletteClienteNFeild);
//			pabc.enterInputtoField(pabc.bolletteClienteNFeild, "");
			Thread.currentThread().sleep(500);
			driver.findElement(pabc.bolletteClienteNFeild).clear();
			Thread.currentThread().sleep(500);
			pabc.clickComponent(pabc.bolletteClienteNFeild);
			pabc.enterInputtoField(pabc.bolletteClienteNFeild, " ");
			Thread.currentThread().sleep(500);
			driver.findElement(pabc.bolletteClienteNFeild).sendKeys(Keys.BACK_SPACE);
			Thread.currentThread().sleep(500);
			
			pabc.checkForLiftTypeDropDownValue(pabc.addressList);
			Thread.currentThread().sleep(500);
			pabc.clickComponent(pabc.addressOption1);
			pabc.clickComponent(pabc.buttonApplica);
			pabc.verifySupplyDisplay(pabc.myTable);
			logger.write("Verifying the result by selecting the correct Address - ENDS");
			
			pabc.clickComponent(pabc.vediBollette);
			pabc.comprareText(pabc.billHeader, "Le tue Bollette", true);
			pabc.comprareText(pabc.billTitle, PrivateAreaBolletteMyComponent.BILL_TITLE, true);
			pabc.comprareText(pabc.billSubTitle, PrivateAreaBolletteMyComponent.BILL_SUBTITLE, true);
			
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
