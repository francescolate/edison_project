package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class LoginSalesForce 
{
	//private Properties prop = null;
	
	@Step("Login Salesforce")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			
//			prop = new Properties();
//			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
//			
//			prop.setProperty("RETURN_VALUE","KO");
//			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
//			prop.setProperty("OUTPUT_DIR","C:/AgentOutputRepository\\ProjectEnelDemo\\2019-06-07\\DEMO_LOGIN_TC_1_821\\DEMO_LOGIN_TO_1_1386"); 
//			prop.setProperty("PASSWORD","Enel$012");
//			prop.setProperty("ERROR_DESCRIPTION","");
//			prop.setProperty("BROWSER","CHROME");
			//Istanzia oggetto Selenium WebDriver
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			//System.out.println(prop.getProperty("POD_ELE"));
		
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				if(!prop.containsKey("START_TIME")){
					Date d = new Date();
					SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String s = fmt.format(d);
					prop.setProperty("START_TIME", s);
				}
				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate(prop.getProperty("LINK"));
				logger.write("Apertura portale Salesforce");
				TimeUnit.SECONDS.sleep(5);
				page.enterUsername(prop.getProperty("USERNAME"));
				logger.write("Inserimento Username");
				page.enterPassword(prop.getProperty("PASSWORD"));
				logger.write("Inserimento Password");
				page.submitLogin();
				logger.write("Accesso al portale");
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.killPreviousSessions();
				

			//	if(prop.getProperty("USERNAME").contentEquals(Costanti.utenza_admin_salesforce)){
					new RicercaOffertaComponent(driver).cliccaSwitch();	
			//	}
			}
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
