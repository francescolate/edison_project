package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergia_BsnComponent;
import com.nttdata.qa.enel.components.colla.InfoEESteps_BsnComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyInfoEnelEnergia_Bsn {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			//System.out.println("Accessing servizi page");
			logger.write("Accessing servizi page - Start");
			InfoEnelEnergia_BsnComponent infoEE = new InfoEnelEnergia_BsnComponent(driver);
			infoEE.clickComponent(infoEE.serviziItem);
			infoEE.checkServiziPageStrings();
			logger.write("Accessing servizi page - Completed");
			
			//System.out.println("Accessing InfoEnelEnergia");
			logger.write("Accessing InfoEnelEnergia - Start");
			infoEE.verifyComponentVisibility(infoEE.infoEnelEnergiaBtn);
			infoEE.clickComponent(infoEE.infoEnelEnergiaBtn);
			//infoEE.checkInfoEEPageElements();
			logger.write("Accessing InfoEnelEnergia - Completed");
			
			//System.out.println("Modify InfoEnelEnergia");
			logger.write("Modify InfoEnelEnergia - Start");
			infoEE.verifyComponentVisibility(infoEE.modificaBtn);
			infoEE.clickComponent(infoEE.modificaBtn);
			logger.write("Modify InfoEnelEnergia - Completed");
			
			//System.out.println("Verifying InfoEnelEnergia modification page");
			logger.write("Verifying InfoEnelEnergia modification page - Start");
			InfoEESteps_BsnComponent steps = new InfoEESteps_BsnComponent(driver);
			Thread.sleep(50000);
			steps.checkStep1Elements();
			logger.write("Verifying InfoEnelEnergia modification page - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
