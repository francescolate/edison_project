package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.lightning.CustomerDetailsPageComponent;
import com.nttdata.qa.enel.components.lightning.AttivitaDetailsComponent;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.TouchPointDetailsComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangeVoltageNoEstimateSFDCVerification_Part2 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);

			CustomerDetailsPageComponent cc = new CustomerDetailsPageComponent(driver);
			Thread.sleep(5000);

			System.out.println("Click request header");
			logger.write("Click request header - Start");
			driver.manage().window().maximize();
			cc.verifyComponentExistence(cc.richestaHeader);
			cc.clickComponent(cc.richestaHeader);
			logger.write("Click request header - Completed");

			System.out.println("Click request number");
			logger.write("Click request number - Start");
			By caseNumber = By.xpath(cc.caseNumberVoltageChange.replace("$DATE$", prop.getProperty("REFERENCE_DATE")));
			cc.verifyComponentExistence(caseNumber);
			cc.clickComponent(caseNumber);
			logger.write("Click request number - Completed");

			System.out.println("Clicking and verifying elemento richiesta");
			logger.write("Clicking and verifying elemento richiesta - Start");
			Thread.sleep(5000);
			cc.clickComponent(cc.elementoRichesta);
			Thread.sleep(3000);
			//TODO Controllare i valori delle stringhe da impostare nel file del component
			cc.checkElementoRichiestaStatus2();
			logger.write("Clicking and verifying elemento richiesta - Completed");
			
			System.out.println("Clicking and verifying case order Id");
			logger.write("Clicking and verifying case order Id - Start");
			cc.jsClickObject(cc.caseOISafe);
			Thread.sleep(10000);
			CaseItemDetailsComponent rc = new CaseItemDetailsComponent(driver);
			//TODO Controllare i valori delle stringhe da impostare nel file del component
			rc.checkDatiDiProcesso2();
			logger.write("Clicking and verifying case order Id - Completed");
			
			System.out.println("Checking attività");
			logger.write("Checking attività - Start");
			logger.write("Verifying attività - Start");
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(5000);
			cc.clickComponent(cc.attivitaHeader);
			AttivitaDetailsComponent ac = new AttivitaDetailsComponent(driver);
			//TODO Verificare correttezza stringhe
			ac.verifyAttivaDetails(ac.attivaEmissionePreventivo);
			ac.verifyComponentExistence(By.xpath(ac.dataApertura1.replace("$DATE$", prop.getProperty("REFERENCE_DATE"))));
			ac.verifyComponentExistence(By.xpath(ac.dataChiusura1.replace("$DATE$", prop.getProperty("REFERENCE_DATE"))));
			//TODO Eliminare le prossime 4 righe. Solo per prove provvisorie
//			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//			LocalDateTime now = LocalDateTime.now();
//			ac.verifyComponentExistence(By.xpath(ac.dataApertura1.replace("$DATE$", dtf.format(now))));
//			ac.verifyComponentExistence(By.xpath(ac.dataChiusura1.replace("$DATE$", dtf.format(now))));
			logger.write("Checking attività - Completed");
			
			System.out.println("Checking Touch Point");
			logger.write("Checking Touch Point - Start");
			driver.navigate().back();
			Thread.sleep(5000);
			cc.clickComponent(cc.touchPointHeader);
			TouchPointDetailsComponent tp = new TouchPointDetailsComponent(driver);
			int numberOfRecords = tp.findNumberOfTpRecords(prop.getProperty("REFERENCE_DATE"), 90);
			tp.checkTipoSezioneAndScadenza(prop.getProperty("REFERENCE_DATE"), numberOfRecords);
			logger.write("Checking Touch Point - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
