package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class InserimentoFornitureSubentroid20  extends BaseComponent {

    public InserimentoFornitureSubentroid20(WebDriver driver) {
        super(driver);
    }

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            PrecheckComponent forniture = new PrecheckComponent(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
            RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
            String statoUbiest = Costanti.statusUbiest;
            logger.write("Popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Start");


            if (prop.getProperty("POD").equalsIgnoreCase("GAS"))
                prop.setProperty("POD", forniture.generateRandomPodNumberGas());
            else if (prop.getProperty("POD").equalsIgnoreCase("ENERGIA"))
                prop.setProperty("POD", forniture.generateRandomPodNumberEnergia());


            forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD"));

            forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));

            forniture.clickComponentIfExist(forniture.precheckButton2);

            gestione.checkSpinnersSFDC();

            logger.write("popolare i campi POD, CAP e premere sul pulsante 'Esegui precheck' - Completed");

            if (prop.getProperty("SEZIONE_ISTAT").equals("Y")) {
                util.getFrameActive();
                Thread.sleep(5000);
                logger.write("selezionare il codice istat e premere conferma - Start");
                forniture.verifyComponentExistence(forniture.tabellaSelezioneIstat);
                By codice_istat_by_name = By.xpath(forniture.codice_istat_by_name.replaceFirst("##", prop.getProperty("LOCALITA_ISTAT")));
                forniture.clickComponentIfExist(codice_istat_by_name);
                forniture.clickComponentIfExist(forniture.buttonConfermaIstat);
                gestione.checkSpinnersSFDC();
                logger.write("selezionare il codice istat e premere conferma - Completed");
            }

            logger.write("verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Start");

            util.getFrameActive();

            logger.write("verificare visualizzazione della sezione Indirizzo, compilare i campi obbligatori e premere il tasto Verifica. Verificare presenza del testo 'Indirizzo verificato' - Completed");

            if (prop.getProperty("POD").equalsIgnoreCase("ENERGIA") || prop.getProperty("POD").contains("IT002E")) {

                if (!prop.getProperty("TENSIONE_CONSEGNA", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.selezionaTensione(prop.getProperty("TENSIONE_CONSEGNA"));
                }
                if (!prop.getProperty("POTENZA_CONTRATTUALE", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.selezionaPotenza(prop.getProperty("POTENZA_CONTRATTUALE"));
                }
                if (!prop.getProperty("POTENZA_FRANCHIGIA", "").equals("")) {
                    insert.verifyComponentExistence(insert.campoTensioneConsegna);
                    insert.verificaValorePotenzaFranchigia(prop.getProperty("POTENZA_FRANCHIGIA"));
                }
            }

            if (prop.getProperty("VERIFICA_ESITO_OFFERTABILITA").contentEquals("Y")) {
                logger.write("Verifica esito Offertabilita - Start");
                forniture.verificaEsitoOffertabilita(prop.getProperty("ESITO_OFFERTABILITA"));
                logger.write("Verifica esito Offertabilita - Completed");
            }

            /*
            if (statoUbiest.compareTo("ON") == 0) {
                logger.write("Selezione Indirizzo di Fatturazione - Start");
                Thread.sleep(5000);
                compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
                logger.write("Selezione Indirizzo di Residenza/Sede Legalee - Completed");
            } else if (statoUbiest.compareTo("OFF") == 0) {
                logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Start");
                Thread.sleep(5000);
                compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzatoNoForzatura("Inserimento forniture", prop.getProperty("CAP"), prop.getProperty("CITTA"));
                logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Completed");
            }
            */
            insert.verifyComponentExistence(insert.aggiungiFornitura);
            insert.verifyComponentExistence(insert.rimuoviFornitura);
            logger.write("Verifica esistenza pulsanti 'Aggiungi fornitura' e 'Rimuovi Fornitura'");

            insert.clickComponentIfExist(insert.buttonConfermaFooter);
            logger.write("click su pulsante Conferma situato subito sotto 'Aggiungi fornitura' e 'Rimuovi Fornitura' - Completed");

            insert.clickComponentIfExist(insert.buttonCreaOfferta);
            logger.write("click su pulsante Crea offerta e apertura pagina 'Riepilogo offerta' - Start");

            gestione.checkSpinnersSFDC();
            logger.write("apertura pagina 'Riepilogo offerta' - Completed");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            //return;
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
