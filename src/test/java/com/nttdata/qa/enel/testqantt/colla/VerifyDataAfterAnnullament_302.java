package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.components.colla.SearchSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import javassist.bytecode.analysis.Util;

public class VerifyDataAfterAnnullament_302 {
	
	static String[] propList;
	static String[] valueList;
	

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		propList = new String[]{ (String) prop.get("QUERY_ITA_IFM_STATUS__C"), (String) prop.get("QUERY_ITA_IFM_SAP_STATUS__C"), (String) prop.get("QUERY_ITA_IFM_POD_PDR__C"),(String) prop.get("QUERY_ITA_IFM_R2D_STATUS__C"), (String) prop.get("QUERY_ITA_IFM_SEMPRE_STATUS__C"), (String) prop.get("QUERY_ITA_IFM_SAPSD_STATUS__C"), (String) prop.get("QUERY_ITA_IFM_UDB_STATUS__C") };
		valueList = new String[]{ "Annullato", "DA INVIARE", "IT001E98787632","NON PREVISTO", "NON PREVISTO", "NON PREVISTO", "NON PREVISTO" };

		BaseComponent bc = new BaseComponent(driver);
		
		bc.compareStringList(propList, valueList);
		
		

	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
