package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CarrelloComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class AzioniSezioneCarrelloVolturaConAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			CarrelloComponent carrello=new CarrelloComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			/*logger.write("seleziona la voce 'Elettrico - Residenziale' nella sezione 'Carrello' - Start");
			
			carrello.verifyComponentExistence(carrello.voceElettricoResidenziale);
			carrello.clickWithJS(carrello.voceElettricoResidenziale);
			
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(1);
			logger.write("seleziona la voce 'Elettrico - Residenziale' nella sezione 'Carrello' - Completed");
			logger.write("seleziona come prodotto l'offerta "+prop.getProperty("PRODOTTO")+" e aggiungerla al carrello - Start");
			carrello.clickWithJS(carrello.buttonCercaChiuso);
			TimeUnit.SECONDS.sleep(1);
			carrello.insertValore(carrello.inputNomeProdotto,prop.getProperty("PRODOTTO"));
			carrello.clickComponent(carrello.cercaButton);
			gestione.checkSpinnersSFDC();
			carrello.aggiungiAlCarrello(prop.getProperty("PRODOTTO"));
			gestione.checkSpinnersSFDC();
			logger.write("seleziona come prodotto l'offerta "+prop.getProperty("PRODOTTO")+" e aggiungerla al carrello - Completed");
			logger.write("corretta apertura pagina di configurazione del prodotto, verifica presenza delle voci 'Componenti Prezzi Energia' e delle sottovoci, click su SALVA e poi su CHECKOUT  - Start");
			carrello.verifyComponentExistence(carrello.pageConfigurazioni);
			carrello.verificaTipologieProdotti(carrello.prodotti);
			carrello.clickComponent(carrello.buttonSalva);
			gestione.checkSpinnersSFDC();
			
			*/
			// Click su CHECKOUT
			carrello.clickWithJS(carrello.buttonCheckout);
			gestione.checkSpinnersSFDC();
			carrello.checkPopupCheckoutAndClickConferma();
			gestione.checkSpinnersSFDC();
			carrello.checkoutEffettuato();
			
			
			// verifica esistenza campi  TODO
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
