package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.components.r2d.R2D_VerifichePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_VerifichePodIniziali_ELE {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		Boolean praticaTrovata = false;

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			String statopratica="vuota";

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Interrogazione","Report Post Sales");
				//Inserimento POD
				R2D_VerifichePodComponent verifichePod = new R2D_VerifichePodComponent(driver);
				if(prop.getProperty("SKIP_POD","N").equals("N")){
					verifichePod.inserisciPod(verifichePod.inputPOD, prop.getProperty("POD_ELE",prop.getProperty("POD")));
				}
				//Inserimento ID richiesta CRM
				verifichePod.inserisciIdRichiestaCRM(verifichePod.inputIdRichiestaCRM, prop.getProperty("OI_RICERCA",prop.getProperty("OI_ORDINE",prop.getProperty("OI_RICHIESTA"))));
				//Se Allaccio o Subentro combinato viene inserito Codice classe servizio DT
				//////
				String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE","");
				//if (prop.getProperty("ALLACCIO","N").equals("Y")||prop.getProperty("SUBENTRO_COMBINATO_R2D","N").equals("Y")||prop.getProperty("MODIFICA_POTENZA_TENSIONE","N").equals("Y")||prop.getProperty("SPOSTAMENTO_CONTATORE_OLTRE_10M","N").equals("Y")||prop.getProperty("VERIFICA_CONTATORE","N").equals("Y")){
//				if (tipoOperazione.equals("ALLACCIO")||tipoOperazione.equals("SUBENTRO_COMBINATO_R2D")||tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")||tipoOperazione.equals("SPOSTAMENTO_CONTATORE_OLTRE_10M")||tipoOperazione.equals("VERIFICA_CONTATORE")){	
				if (tipoOperazione.equals("ALLACCIO_EVO")||tipoOperazione.equals("SUBENTRO_COMBINATO")||tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")||tipoOperazione.equals("SPOSTAMENTO_CONTATORE_OLTRE_10M")||tipoOperazione.equals("VERIFICA_CONTATORE")){	
					verifichePod.selezionaCodiceServizioDt(verifichePod.selectCodiceServizioDt,prop.getProperty("CODICE_SERVIZIO_DT_R2D"));
					if(prop.getProperty("ACCETTAZIONE_PREVENTIVO_R2D","N").equals("Y")){
						verifichePod.inserisciAccettazionePreventivo(verifichePod.inputValidazionePreventivo, "Y");
					}
				}
				
				//flag fittizio a si
//				if (prop.getProperty("FLAG_FITTIZIO","").contentEquals("Y")) {
				if (prop.getProperty("FLAG_FITTIZIO","N").equals("Y")) {
					verifichePod.selezionaPratica(verifichePod.flagFittizioSi);
				}
				//Cerca
				verifichePod.cercaPod(verifichePod.buttonCerca);
				//Click Dettaglio Pratiche
				logger.write("Bottone Dettaglio Pratica - Start");
				praticaTrovata = false;
				verifichePod.dettaglioPratiche(verifichePod.buttonDettaglioPratiche);
				praticaTrovata = true;
				logger.write("Bottone Dettaglio Pratica - Completed");
				//Click su Pratica
				verifichePod.selezionaPratica(verifichePod.rigaPratica);
				//Salvare distributore
				System.out.println("Dsitributore:"+verifichePod.recuperaDistributore(verifichePod.distribure));
				prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE",verifichePod.recuperaDistributore(verifichePod.distribure));
				//Verifica Campi
				//Lista con la coppia campo/valore atteso da verificare
				ArrayList<String> campiDaVerificare = new ArrayList<String>();
//				campiDaVerificare.add("Distributore e Partita IVA;"+prop.getProperty("DISTRIBUTORE_R2D_ATTESO_ELE"));
				//				campiDaVerificare.add("Causale CRM;"+prop.getProperty("CAUSALE_CRM_R2D_ATTESO"));
				//Lista con l'elenco dei campi per i quali occorre salvare il valore
				ArrayList<String> campiDaSalvare = new ArrayList<String>();
				campiDaSalvare.add("Id R2D");
				campiDaSalvare.add("Codice Pratica Utente");
				campiDaSalvare.add("Stato R2D");
				//Salvare eventualmente stato indicizzato
				verifichePod.verificaDettaglioPratica(prop,campiDaVerificare,campiDaSalvare);

				//Verifico che lo stato iniziale della pratica può essere AW o AA
				logger.write("Verifiche Iniziali Stato Pratica - Start");
				statopratica=prop.getProperty("STATO_R2D");
				if(statopratica.compareToIgnoreCase("CI")==0 || statopratica.compareToIgnoreCase("OK")==0){
					logger.write("La pratica è già in Stato  (CI/OK)");
					} else if(statopratica.compareToIgnoreCase("AW")!=0 && statopratica.compareToIgnoreCase("AA")!=0){
						throw new Exception("Lo stato iniziale della pratica non è corretto:"+statopratica+". Stato pratica atteso AW o AA");
					}
					logger.write("Verifiche Iniziali Stato Pratica - Completed");
				}

			prop.setProperty("RETURN_VALUE", "OK");
			
		}catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
