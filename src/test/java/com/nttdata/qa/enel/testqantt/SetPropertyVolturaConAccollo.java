package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.tika.metadata.Property;

public class SetPropertyVolturaConAccollo {

	/*Esempio
	  //CLIENTE ENTRANTE 
		prop.setProperty("CODICE_FISCALE", "CRSFNC89M49H769B");
	    prop.setProperty("SET_CLIENTE_ENTRANTE","N"); //Se settato a Y il CF Entrante viene recuperato con query
		
	    //CLIENTE USCENTE: 
        prop.setProperty("SET_CLIENTE_USCENTE","N");//Se settato a Y il CF uscente viene recuperato con query altrimenti vengono utilizzate ler property CODICE_FISCALE_CL_USCENTE e  POD
        prop.setProperty("CODICE_FISCALE_CL_USCENTE", "TMTGGW85C44F839R");
        prop.setProperty("POD", "IT002E9220230A");
	 */

	

	
	private static String queryCountVolture=
							"SELECT Account.NE__Fiscal_code__c fiscal_code, count(id) FROM Case \r\n "
							+ " where  Account.NE__Fiscal_code__c IN (@fiscalCode@) \r\n "
							+ "and ITA_IFM_Descrizione_Tripletta__c IN ('Voltura con accollo','Voltura senza accollo') \r\n "
							+ "and ITA_IFM_AnnulmentState__c not in ('annullato') \r\n "
							
							+ "group by Account.NE__Fiscal_code__c";
	
	private static String queryCountPod="SELECT Account.NE__Fiscal_code__c fiscal_code, count(id)\r\n"
										+ "FROM Asset\r\n"
										+ "WHERE Account.NE__Fiscal_code__c IN (@fiscalCode@)\r\n"
										+ "AND RecordType.Name = 'Commodity'\r\n"
										+ "AND NE__Status__c = 'Active'\r\n"
										+" and ITA_IFM_Commodity__c='ELE'"
										+ "group by Account.NE__Fiscal_code__c\r\n";
										
	
	
	public static void main(String[] args) throws Exception {


		Properties prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				String scenario=args[0].replace(".properties", "");
				

				int maxNumberOfVolture=Integer.parseInt(prop.getProperty("MAX_NUM_VOLTURE","5"));
				//per i clienti business è necessario avere al max un POD 
				String causaleVoltura=prop.getProperty("CAUSALE");
				String maxNumberOfPodDefaultValue=causaleVoltura.equals("Trasformazione/Fusione Azienda")
														||causaleVoltura.equals("Cessione Ramo Azienda")?"1":"5"; 
				int maxNumberOfPod=Integer.parseInt(prop.getProperty("MAX_NUM_POD",maxNumberOfPodDefaultValue));
				Integer counter=1;
				String query="";




				switch (scenario) {
				case "VolturaConAccolloEVO_Id_3":
					query= Costanti.recupera_CF_POD_per_VoltureConAccolloid3;
					break;
				case "VolturaConAccolloEVO_Id_6":
					query= Costanti.recupera_CF_POD_per_VoltureConAccolloid6;
					break;
				case "VolturaConAccolloEVO_Id_8":
					query=Costanti.recupera_CF_POD_per_VoltureConAccolloid8;
					break;
				case "VolturaConAccolloEVO_Id_9":
					query= Costanti.recupera_CF_POD_per_VoltureConAccolloid9;
					break;
				case "VolturaConAccolloEVO_Id_12":
					query= Costanti.recupera_CF_POD_per_VoltureConAccolloid12;
					break;
				case "VolturaConAccolloEVO_Id_17":
					query= Costanti.query_voltura_con_accollo_id17;
					break;	
				case "VolturaConAccolloEVO_Id_18":
					query= Costanti.recupera_CF_POD_per_Volture18;
					break;
				default:
					throw new Exception("Non è stato indicato lo scenario per cui settare i dati");
					//break;
				}


				boolean clienteEntranteSettato=true;
				boolean clienteUscenteSettato=true;
				
				if(prop.getProperty("SET_CLIENTE_ENTRANTE","").equals("Y")
							||prop.getProperty("SET_CLIENTE_USCENTE","").equals("Y"))
				{

					
					logger.write("Inizio settaggio CF e POD per "+scenario);
					
					WorkbenchQuery wb=new WorkbenchQuery();
					
					wb.Login(args[0]);
					
					//Estraggo i cf utilizzabili utilizzando la query fornita nella TL
					wb.throwExceptionIfThreAreNoresult=true; //se non viene trovato nessun record deve essere lanciata eccezione
					Map<Integer,Map> accounts=wb.EseguiQuery(query);
					logger.write("Query eseguita - " + accounts.size() + " record  estratti");
					
					wb.throwExceptionIfThreAreNoresult=false;
					wb.tentativi=1;
					//Filtro i record che hanno più di du evolture
					String inClause=WorkbenchQuery.GeneraInClause(accounts, "Account.NE__Fiscal_code__c");
					logger.write("In Clause Generata");
					
					logger.write("Conteggio Volture");
					Map<Integer,Map> countVolture=wb.EseguiQuery(queryCountVolture.replace("@fiscalCode@", inClause));	
					logger.write("Conteggio Pod");
					Map<String,String> cfCoumt=new HashMap<String, String>();
					for (Integer rowIndex : countVolture.keySet()) {
						
						cfCoumt.put(countVolture.get(rowIndex).get("fiscal_code").toString()
								,countVolture.get(rowIndex).get("Unknown_Field__1").toString());
						
					}
					
					logger.write("Conteggio Pods");
					//FILTRO I RECORD CHE HANNO SOLO UN POD
					Map<Integer,Map> countPod=wb.EseguiQuery(queryCountPod.replace("@fiscalCode@", inClause));
					Map<String,String> cfPodCount= new HashMap<String, String>();
					for (Integer rowIndex : countPod.keySet()) {
						
						cfPodCount.put(countPod.get(rowIndex).get("fiscal_code").toString()
								,countPod.get(rowIndex).get("Unknown_Field__1").toString());
						
					}
					
					
					
					

					String cfEntrante="";

					//CLIENTE ENTRANTE
					if(prop.getProperty("SET_CLIENTE_ENTRANTE","").equals("Y"))
					{
						clienteEntranteSettato=false;
						while(counter<accounts.size())
						{
							cfEntrante=accounts.get(counter).get("Account.NE__Fiscal_code__c").toString();
							
							String count=cfCoumt.containsKey(cfEntrante)?cfCoumt.get(cfEntrante):"0";
							counter++;
							
							
							logger.write(count+ " Volture esistenti per cliente entrante: "+cfEntrante);
							if(Integer.parseInt(count)<maxNumberOfVolture)
							{
								prop.setProperty("CODICE_FISCALE", cfEntrante);
								logger.write("settaggio cliente entrante: "+cfEntrante);
								clienteEntranteSettato=true;
								break;
							}

						}
					}

					//CLIENTE USCENTE 
					if(prop.getProperty("SET_CLIENTE_USCENTE","").equals("Y"))
					{
						clienteUscenteSettato=false;
						while(counter<accounts.size())
						{



							String pod=accounts.get(counter).get("NE__Service_Point__c.ITA_IFM_POD__c").toString();;
							String cfUscente=accounts.get(counter).get("Account.NE__Fiscal_code__c").toString();;

							if(cfUscente.equals(cfEntrante)) continue;

							String count=cfCoumt.containsKey(cfUscente)?cfCoumt.get(cfUscente):"0";
							String podNumber=cfPodCount.containsKey(cfUscente)?cfPodCount.get(cfUscente):"0";
							
							counter++;
							
							
							//CHECK PER CLIENTE USCENTE
							counter++;
							logger.write(count+ " Volture e "+ podNumber +" PODesistenti per cliente uscente ");
							if(Integer.parseInt(count)<maxNumberOfVolture && Integer.parseInt(podNumber)<maxNumberOfPod)
							{
								prop.setProperty("CODICE_FISCALE_CL_USCENTE", cfUscente);
								prop.setProperty("POD", pod);
								logger.write("settaggio cliente uscente: "+cfUscente);
								clienteUscenteSettato=true;
								break;
							}



							//SOQL non gestisce l'alias 




						}//End While
					}
					
					
					if(!clienteEntranteSettato) throw new Exception("Non è stato possibile settare il cliente entrante");
					if(!clienteUscenteSettato) throw new Exception("Non è stato possibile settare il cliente uscente");
					logger.write("Fine settaggio CF e POD per "+scenario);
				}
			}//IF MODULE ENABLED

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
