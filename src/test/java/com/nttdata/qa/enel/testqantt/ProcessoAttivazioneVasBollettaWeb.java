package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CompilaDatiContatto;
import com.nttdata.qa.enel.components.lightning.ConfermaButtonComponent;
import com.nttdata.qa.enel.components.lightning.DettagliComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaCommodityComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaServizioVASComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;
import javassist.bytecode.analysis.Util;

public class ProcessoAttivazioneVasBollettaWeb {

	@Step("Processo Attivazione Servizio VAS")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				
				logger.write("Scelta processo Attivazione Vas - Start");
				SceltaProcessoComponent sceltaProcesso = new SceltaProcessoComponent(driver);
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
				IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);

				sceltaProcesso.clickAllProcess();
				sceltaProcesso.chooseProcessToStartWithSearch(sceltaProcesso.avvioAttivazioneVas,
						"Avvio Attivazione VAS");
				logger.write("Scelta processo Attivazione Vas - Completed");
				
				logger.write("Identificazione interlocutore - Start");
				// Verifica Intestazione nuova richiesta

				TimeUnit.SECONDS.sleep(10);
				
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
				identificaInterlocutore.verifyInputFieldExist("Numero fattura");
				identificaInterlocutore.verifyInputFieldExist("Numero Cliente");
				identificaInterlocutore.verifyInputFieldExist("Documento");
				identificaInterlocutore.verifyInputFieldExist("Importo Ultima Fattura");
				identificaInterlocutore.verifyInputFieldExist("Data Scadenza Ultima Fattura");
				identificaInterlocutore.verifyInputFieldExist("Data Emissione Ultima Fattura");
				identificaInterlocutore.verifyInputFieldExist("Indirizzo di fornitura");
				
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
				
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Codice Fiscale");
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Completed");
				
				logger.write("Inserimento documento e conferma - Start");
				identificaInterlocutore.insertDocumentoNew("b");
				logger.write("Identificazione interlocutore - Completed");
				
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
				logger.write("Inserimento documento e conferma - Completed");
			
				// Salvataggio Numero Richiesta
				prop.setProperty("NUMERO_RICHIESTA",
						intestazione.salvaNumeroRichiestaVas(intestazione.pNRichiestaPaginaNuovaRichiesta7));
				logger.write("NUMERO_RICHIESTA="+prop.getProperty("NUMERO_RICHIESTA"));

			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
