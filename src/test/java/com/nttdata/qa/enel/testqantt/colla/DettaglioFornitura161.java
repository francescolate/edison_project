package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.RinominaFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioFornitura161 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("Corretta visualizzazione della Home Page con testo centrale 'Benvenuto nella tua area privata', visualizzazione del testo 'In questa sezione potrai gestire le tue forniture' e della sezione con titolo 'Le tue forniture' con testo : 'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette' - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			By accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage);
			log.verifyComponentExistence(log.testoCentrale);
			//log.verifyComponentExistence(log.testoSezioneForniture);
			
			RinominaFornituraComponent forniture = new RinominaFornituraComponent(driver);
			By accessSezForniture=forniture.accessForniture;
			forniture.verifyComponentExistence(accessSezForniture);
			logger.write("Corretta visualizzazione della Home Page con testo centrale 'Benvenuto nella tua area privata', visualizzazione del testo 'In questa sezione potrai gestire le tue forniture' e della sezione con titolo 'Le tue forniture' con testo : 'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette' - Completed");
			
			logger.write("Corretta visualizzazione della fornitura attiva "+prop.getProperty("NUM_UTENTE")+" e click sul pulsante DETTAGLIO FORNITURA - Start");
			By checkFornitura=forniture.fornituraAttiva;
			forniture.verifyExistenceAndClickFornitura(checkFornitura,prop.getProperty("STATO"),prop.getProperty("NUM_UTENTE"), prop.getProperty("COMMODITY"));
			logger.write("Corretta visualizzazione della fornitura attiva "+prop.getProperty("NUM_UTENTE")+" e click sul pulsante DETTAGLIO FORNITURA - Completed");
			
			logger.write("Corretta visualizzazione del dettaglio della forntiura con TITOLO: "+prop.getProperty("COMMODITY")+" e testo: 'L’indirizzo della tua fornitura "+prop.getProperty("COMMODITY")+" è' e click sul link 'Mostra di più' - Start");
			By details=forniture.detailsF;
			forniture.verificaDettaglioFornEClickSuMostraDiPiù(details, prop.getProperty("COMMODITY"), prop.getProperty("NUM_UTENTE"));
			logger.write("Corretta visualizzazione del dettaglio della forntiura con TITOLO: "+prop.getProperty("COMMODITY")+" e testo: 'L’indirizzo della tua fornitura "+prop.getProperty("COMMODITY")+" è' e click sul link 'Mostra di più' - Completed");
			
			logger.write("Corretta visualizzazione del campo 'Contratto' con voce 'Scarica pdf'  - Start");
			forniture.verifyComponentExistence(forniture.sezioneContrattoScaricaPDF);
			logger.write("Corretta visualizzazione del campo 'Contratto' con voce 'Scarica pdf'  - Completed");
			
			logger.write("Click su 'Scarica pdf' e verifica del corretto download del file  - Start");
			forniture.clickComponent(forniture.iconaScaricaPdf);						
			TimeUnit.SECONDS.sleep(10);
			DettaglioBollettaComponent db = new DettaglioBollettaComponent(driver);
			if(db.isFileDownloaded_Ext(DettaglioBollettaComponent.DIR_PATH, DettaglioBollettaComponent.FILENAME161))
				db.deleteDownloadedFile(db.DIR_PATH, db.FILENAME161);
			logger.write("Click su 'Scarica pdf' e verifica del corretto download del file  - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
