package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.InfoEEStepsComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyInfoEnelEnergia_Continue {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaComponent infoEE = new InfoEnelEnergiaComponent(driver);
			
			System.out.println("Continue modifying InfoEnelEnergia");
			logger.write("Continue modifying InfoEnelEnergia - Start");
			//Saving client number
			String clientNumber = infoEE.getElementTextString(infoEE.clientNumber);
			System.out.println("SAving client number: " + clientNumber);
			prop.setProperty("CLIENT_NUMBER", clientNumber);
			infoEE.verifyComponentVisibility(infoEE.firstSupplyCheckBox);
			infoEE.clickComponent(infoEE.firstSupplyCheckBox);
			infoEE.verifyComponentVisibility(infoEE.continueBtn);
			infoEE.clickComponent(infoEE.continueBtn);
			logger.write("Continue modifying InfoEnelEnergia - Completed");
			
			System.out.println("Step 1");
			logger.write("Step 1 - Start");
			InfoEEStepsComponent steps = new InfoEEStepsComponent(driver);
			steps.checkStep1ElementsExistence();
			steps.modifyItemAndContinue(steps.sollecitoMailChk);
			logger.write("Step 1 - Completed");
			
			System.out.println("Step 2");
			logger.write("Step 2 - Start");
			steps.step2Continue();
			logger.write("Step 2 - Completed");
			
			System.out.println("Step 3");
			logger.write("Step 3 - Start");
//			steps.checkModifyResults();
			//steps.clickComponent(steps.endBtn);
			//steps.clickComponent(steps.endBtnNew);
			logger.write("Step 3 - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
