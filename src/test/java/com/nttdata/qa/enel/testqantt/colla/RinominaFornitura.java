package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.RinominaFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RinominaFornitura {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			By accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			
			LeftMenuComponent menu = new LeftMenuComponent(driver);
			menu.sceltaMenu("Forniture");
			
			RinominaFornituraComponent forniture = new RinominaFornituraComponent(driver);
			By accessSezForniture=forniture.accessForniture;
			forniture.verifyComponentExistence(accessSezForniture); // verifica esistenza text 'Le tue forniture'
		
			By checkFornitura=forniture.fornituraAttiva;
			forniture.verifyExistenceAndClickFornituraAttiva(checkFornitura,prop.getProperty("STATO"),prop.getProperty("NUM_UTENTE"));
						
			By details=forniture.detailsF;
			forniture.verifyComponentExistence(details);
			forniture.verifyDetailsFornituta(details, prop.getProperty("STATO"), prop.getProperty("NUM_UTENTE"), prop.getProperty("DETTAGLIO_FORNITURA"));
			
			By rinominaButton=forniture.rinominaFornituraButton;
			forniture.verifyComponentExistence(rinominaButton);
			forniture.clickComponent(rinominaButton);
			
			By accessPageRename=forniture.accessFornituraRenamePage;
			forniture.verifyComponentExistence(accessPageRename);
			
			By inputboxNomeFornitura=forniture.inputboxNomeFornitura;
			forniture.verifyComponentExistence(inputboxNomeFornitura);
			
			String nomeFornitura=forniture.createFornituraName();
			forniture.insertFornituraName(inputboxNomeFornitura, nomeFornitura);
			
			By continua=forniture.continuaButton;
			forniture.verifyComponentExistence(continua);
			forniture.clickComponent(continua);
			
			By labelNewName=forniture.newName;
			forniture.verifyComponentExistence(labelNewName);
			String pageRinominaFornitura="Rinomina fornitura";
			forniture.verifyChangeNameFornitura(labelNewName,nomeFornitura,pageRinominaFornitura);
			
			By conferma=forniture.confermaButton;
			forniture.verifyComponentExistence(conferma);
			forniture.clickComponent(conferma);
			
			By operazioneOk=forniture.finishedOperation;
			forniture.verifyComponentExistence(operazioneOk);
			
			By fine=forniture.fineButton;
			forniture.verifyComponentExistence(fine);
			forniture.clickComponent(fine);
			
			By labelUpdateName=forniture.updatedName;
			forniture.verifyComponentExistence(labelUpdateName);
			String pageDettaglioFornitura="Dettaglio fornitura";
			forniture.verifyChangeNameFornitura(labelUpdateName,nomeFornitura,pageDettaglioFornitura);
			prop.setProperty("NOME_FORNITURA", nomeFornitura);
			
			LoginLogoutEnelCollaComponent logout = new LoginLogoutEnelCollaComponent(driver);
			By account=logout.accountButton;
			logout.verifyComponentExistence(account);
			logout.clickComponent(account);
			
			By linkFine=logout.esciLink;
			logout.verifyComponentExistence(linkFine);
			logout.clickComponent(linkFine);
			
		 	By logo = logout.logoEnel;
		 	logout.verifyComponentExistence(logo); //Verifica presenza logo enel sulla pagina di accesso iniziale
		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
