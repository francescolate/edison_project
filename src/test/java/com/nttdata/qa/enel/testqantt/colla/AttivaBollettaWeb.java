package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.AttivaBollettaWebComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AttivaBollettaWeb {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			AttivaBollettaWebComponent abwc = new AttivaBollettaWebComponent(driver);
			
			logger.write("controllo e scrollo alla sezione 'le tue attività' - START");
			abwc.verifyComponentExistence(abwc.leTueAttivitaHeader);
			abwc.scrollComponent(abwc.leTueAttivitaHeader);
			Thread.sleep(10000);
			logger.write("controllo e scrollo alla sezione 'le tue attività' - COMPLETED");
			
			logger.write("clicco su pulsante Attiva ora la tua bolletta web - START");
			abwc.verifyComponentExistence(abwc.attivaLaTuaBollettaWebButton);
			Thread.sleep(10000);
			abwc.clickComponent(abwc.attivaLaTuaBollettaWebButton);
			logger.write("clicco su pulsante Attiva ora la tua bolletta web - COMPLETED");
			
			Thread.sleep(10000);
			
			logger.write("cliccando su Attiva Bolletta Web - START");
			abwc.verifyComponentExistence(abwc.attivaBollettaWebButton);
			abwc.clickComponent(abwc.attivaBollettaWebButton);
			logger.write("cliccando su Attiva Bolletta Web - COMPLETED");
			
			logger.write("scegliendo la fornitura per il caso di test - START");
			Thread.sleep(100000);
			abwc.verifyComponentExistence(abwc.fornituraCheckBox);
			abwc.clickComponent(abwc.fornituraCheckBox);
			logger.write("scegliendo la fornitura per il caso di test - COMPLETED");
			
			logger.write("cliccando sul pulsante Continua - START");
			abwc.verifyComponentExistence(abwc.continuaButton);
			abwc.clickComponent(abwc.continuaButton);
			logger.write("cliccando sul pulsante Continua - COMPLETED");
			
		/*	logger.write("cliccando sul pulsante Indietro - START");
			abwc.verifyComponentExistence(abwc.indietroButton);
			abwc.clickComponent(abwc.indietroButton);
			logger.write("cliccando sul pulsante Indietro - COMPLETED");*/
			
		/*	logger.write("verificando che si è ritornati alla pagina precedente, per poi proseguire nuovamente - START");
			abwc.verifyComponentExistence(abwc.esciButton);
			abwc.verifyComponentExistence(abwc.continuaButton);
			abwc.jsClickObject(abwc.continuaButton);
			logger.write("verificando che si è ritornati alla pagina precedente, per poi proseguire nuovamente - COMPLETED");
			*/
			Thread.sleep(5000);
			
			abwc.changeInputText(abwc.inputEmail, "");
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputEmailError, "Il campo Email è obbligatorio.", true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME").replace("@", ""));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputEmailError, "Email non valida", true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME").replace(".com", ""));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputEmailError, "Email non valida", true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME").replace(".com", ".co"));
			Thread.sleep(500);
			Robot robot = new Robot(); 
			robot.keyPress(KeyEvent.VK_M); 
			Thread.sleep(500);
			abwc.copy(abwc.inputEmail);
			Thread.sleep(100);
			abwc.paste(abwc.inputConfirmEmail);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputConfirmEmailError, abwc.copiaIncollaErrore, true);
			
			//i successivi 3 step sono ripetuti
			abwc.changeInputText(abwc.inputEmail, "");
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputEmailError, abwc.campoEmailObbligatorio, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME").replace("@", ""));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputEmailError, abwc.emailNonValida, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME").replace(".com", ""));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputEmailError);
			abwc.compareText(abwc.inputEmailError, abwc.emailNonValida, true);
			//fine ripetizione
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputMobile, "");
			abwc.clickComponent(abwc.continuaSecondButton);
			Thread.sleep(500);
			abwc.verifyComponentExistence(abwc.inputMobileError);
			abwc.compareText(abwc.inputMobileError, abwc.campoMobileObbligatorio, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			Thread.sleep(100);
			abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			Thread.sleep(100);
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(100);
			abwc.copy(abwc.inputMobile);
			Thread.sleep(100);
			robot.keyPress(KeyEvent.VK_1); 
			Thread.sleep(500);
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputMobileError);
			abwc.compareText(abwc.inputMobileError, abwc.cellulareNonValido, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER").substring(0, 7));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputMobileError);
			abwc.compareText(abwc.inputMobileError, abwc.cellulareNonValido, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputMobile, "");
			Thread.sleep(100);
			abwc.clickComponent(abwc.inputMobile);
			Thread.sleep(500);
			robot = new Robot(); 
			robot.keyPress(KeyEvent.VK_M); 
			Thread.sleep(500);
			abwc.verifyComponentExistence(abwc.inputMobileError);
			abwc.compareText(abwc.inputMobileError, abwc.cellulareSoloNumeri, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			abwc.clickComponent(abwc.continuaSecondButton);
			abwc.verifyComponentExistence(abwc.inputConfirmMobileError);
			abwc.compareText(abwc.inputConfirmMobileError, abwc.campiCellulareNonCoincidono, true);
			
			abwc.changeInputText(abwc.inputEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputConfirmEmail, prop.getProperty("WP_USERNAME"));
			abwc.changeInputText(abwc.inputMobile, prop.getProperty("MOBILE_NUMBER"));
			Thread.sleep(100);
			abwc.clickComponent(abwc.inputMobile);
			abwc.paste(abwc.inputConfirmMobile);
			Thread.sleep(100);
			abwc.verifyComponentExistence(abwc.inputConfirmMobileError);
			abwc.compareText(abwc.inputConfirmMobileError, abwc.copiaIncollaErrore, true);
			
			abwc.jsClickObject(abwc.infoButton);
			abwc.verifyComponentExistence(abwc.datiContattoModalHeader);
			abwc.verifyComponentExistence(abwc.datiContattoInnerText);
			abwc.compareText(abwc.datiContattoModalHeader, abwc.datiContattoModalHeaderText, true);
			abwc.compareText(abwc.datiContattoInnerText, abwc.datiContattoPopupText, true);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
