package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.components.colla.ContractChoiceComponent;
import com.nttdata.qa.enel.components.colla.OreFreeOfferComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFree_Verify_Cont_2G {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			//** STart Set dati da verificare
			String BASE_LINK = prop.getProperty("LINK");
			//**END Set Dati da verificare
			System.out.println("Accessing home page");
			logger.write("Accessing home page - Start");
			LoginLogoutEnelCollaComponent homePage = new LoginLogoutEnelCollaComponent(driver);
			homePage.launchLink(prop.getProperty("LINK"));
			By logo = homePage.logoEnel;
			homePage.verifyComponentExistence(logo);
			
			//Click chusura bunner pubblicitario se esistente
			homePage.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

			By acceptCookieBtn = homePage.buttonAccetta;
			homePage.verifyComponentExistence(acceptCookieBtn);
			homePage.clickComponent(acceptCookieBtn);
			logger.write("Accessing home page - Completed");
			
			System.out.println("Selecting contract type");
			logger.write("Selecting contract type - Start");
			Thread.sleep(5000);
			ContractChoiceComponent contractSelector = new ContractChoiceComponent(driver);
			contractSelector.selectMenuOption(contractSelector.contractTypeSelect, prop.getProperty("CONTRACT_TYPE"));
			Thread.sleep(2000);
			contractSelector.selectMenuOption(contractSelector.placeSelect, prop.getProperty("PLACE"));
			Thread.sleep(2000);
			contractSelector.selectMenuOption(contractSelector.requirementsSelect, prop.getProperty("REQUIREMENTS"));
			Thread.sleep(2000);
			contractSelector.verifyComponentExistence(contractSelector.continueButton);
			Thread.sleep(2000);
			contractSelector.clickComponent(contractSelector.continueButton);
			logger.write("Selecting contract type - Completed");
			
			System.out.println("Verifying Ore Free popup layout");
			logger.write("Verifying Ore Free popup layout - Start");
			OreFreeOfferComponent offerPage = new OreFreeOfferComponent(driver);
			offerPage.verifyComponentVisibility(offerPage.checkOrefreeAvailabilityBtn);
			offerPage.clickComponent(offerPage.checkOrefreeAvailabilityBtn);
			offerPage.verifyComponentExistence(offerPage.titleLabel);
			offerPage.verifyComponentText(offerPage.titleLabel, prop.getProperty("POPUP_TITLE"));
			offerPage.verifyComponentExistence(offerPage.subtitleLabel);
			offerPage.verifyComponentText(offerPage.subtitleLabel, prop.getProperty("POPUP_SUBTITLE"));
			offerPage.verifyComponentExistence(offerPage.podField);
			offerPage.verifyComponentExistence(offerPage.submitBtn);
			offerPage.verifyComponentExistence(offerPage.infoLink);
			offerPage.verifyComponentText(offerPage.infoLink, prop.getProperty("INFO_LINK_STRING"));
			logger.write("Verifying Ore Free popup layout - Completed");
			
			System.out.println("Verifying empty field error");
			logger.write("Verifying empty field error - Start");
			offerPage.clickComponent(offerPage.submitBtn);
			offerPage.verifyComponentExistence(offerPage.errorLabel);
			offerPage.verifyComponentText(offerPage.errorLabel, prop.getProperty("ERROR_LABEL_STRING"));
			logger.write("Verifying empty field error - Completed");
			
			System.out.println("Verifying info link");
			logger.write("Verifying info link - Start");
			offerPage.clickComponent(offerPage.infoLink);
			offerPage.verifyPageUrl(prop.getProperty("FAQ_LINK"));
			offerPage.verifyComponentExistence(offerPage.faqPageTitle);
			offerPage.verifyComponentText(offerPage.faqPageTitle, prop.getProperty("FAQ_TITLE"));
			offerPage.verifyComponentExistence(offerPage.faqPageSubtitle);
			offerPage.verifyComponentText(offerPage.faqPageSubtitle, prop.getProperty("FAQ_SUBTITLE"));
			logger.write("Verifying info link - Completed");
			
			System.out.println("Verifying pod format");
			logger.write("Verifying pod format");
			driver.navigate().back();
			
			OreFreeComponent ofc = new OreFreeComponent(driver);
			offerPage.verifyComponentExistence(offerPage.oreFree);
			offerPage.clickComponent(offerPage.oreFree);
			ofc.verifyComponentExistence(ofc.verificaaderisciVerde);
			ofc.clickComponent(ofc.verificaaderisciVerde);

			offerPage.insertPod(prop.getProperty("POD_TEST_STRING_1"));
			offerPage.checkWrongPodMessage(prop.getProperty("POD_TEST_ERROR_STRING"));
			offerPage.insertPod(prop.getProperty("POD_TEST_STRING_2"));
			offerPage.checkWrongPodMessage(prop.getProperty("POD_TEST_ERROR_STRING"));
			offerPage.insertPod(prop.getProperty("POD_TEST_STRING_3"));
			offerPage.checkWrongPodMessage(prop.getProperty("POD_TEST_ERROR_STRING"));
			offerPage.insertPod(prop.getProperty("POD_TEST_STRING_4"));
			logger.write("Verifying pod format - Completed");
			
			Thread.sleep(10000);
			
			logger.write("Verifying title page Scegli Tu Ore Free - Start");
			//offerPage.verifyComponentExistence(offerPage.acceptancePageTitleLabel);
			//offerPage.verifyComponentText(offerPage.acceptancePageTitleLabel, prop.getProperty("ACCEPTANCE_PAGE_TITLE"));
			offerPage.verifyComponentExistence(offerPage.adesioneContrattoTitle);
			offerPage.verifyComponentText(offerPage.adesioneContrattoTitle, prop.getProperty("ACCEPTANCE_PAGE_TITLE"));
			logger.write("Verifying title page Scegli Tu Ore Free - Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
