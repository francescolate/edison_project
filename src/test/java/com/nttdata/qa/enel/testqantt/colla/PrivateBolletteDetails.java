package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateBolletteDetails {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
//			prop.setProperty("FORNITURETITLE", "Servizi per le forniture");
//			prop.setProperty("BOLLETTETITLE", "Servizi per le bollette");
//			prop.setProperty("CONTRATTOTITLE", "Servizi per il contratto");
//			prop.setProperty("FORNITURECONTENT", "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture");
//			prop.setProperty("BOLLETTECONTENTT", "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette");
//			prop.setProperty("CONTRATTOCONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture");
			prop.setProperty("BOLLETEHEADING", "Benvenuto nella tua area privata");
			prop.setProperty("BOLLETTEHEADINGCONTENT", "In questa sezione potrai gestire le tue forniture");
			prop.setProperty("BOLLETTEDETAILSHEADING", "Le tue bollette");
			prop.setProperty("BOLLETTEDETAILSHEADINGCONTENT", "Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.");
/*
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("LINK"));
			Thread.sleep(5000);
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
			
			logger.write("check the username and passsword login - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("USERNAME")); 

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
			logger.write("check the username and passsword login - Completed");
			
			logger.write("Click on the login button- Start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			By accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
			logger.write("Verify the page after successful login - Complete");
			*/
			SuplyPrivateDetailComponent sdlg = new SuplyPrivateDetailComponent(driver);
			
			logger.write("Verify and click on Bollette heading - Start");
			sdlg.VerifyText(sdlg.BolletteHeading, prop.getProperty("BOLLETEHEADING"));
			sdlg.VerifyText(sdlg.BolletteHeadingContents, prop.getProperty("BOLLETTEHEADINGCONTENT"));
			logger.write("Verify and click on Bollette heading - Complete");
			
			logger.write("Verify and click on Bollette link - Start");
			sdlg.verifyComponentExistence(sdlg.BolletteLink);
			sdlg.clickComponent(sdlg.BolletteLink);
			logger.write("Verify and click on Bollette link - Complete");
			
			logger.write("Verify  Bollette details heading and content - Start");
			sdlg.VerifyText(sdlg.BolletteDetailsHeading, prop.getProperty("BOLLETTEDETAILSHEADING"));
			sdlg.VerifyText(sdlg.BolletteDetailsHeadingContent, prop.getProperty("BOLLETTEDETAILSHEADINGCONTENT"));
			logger.write("Verify  Bollette details heading and content - Complete");
			
			logger.write("Verify and click on MostraFiltri button - Start");
			sdlg.verifyComponentExistence(sdlg.MostraFiltri);
			sdlg.clickComponent(sdlg.MostraFiltri);
			logger.write("Verify and click on MostraFiltri button - Complete");
			
//			logger.write("Verify and click on Visualizzatutte button - Start");
//			sdlg.verifyComponentExistence(sdlg.VisualizzaTutte);
//			sdlg.clickComponent(sdlg.VisualizzaTutte);
//			logger.write("Verify and click on Visualizzatutte button - Start");
						
				
			logger.write("Verify Gas heading details - Start");
			sdlg.GasHeadingDetails(sdlg.GasDetails);
			logger.write("Verify Gas heading details - Complete");
			
			logger.write("Verify Gas  details - Start");
			sdlg.gasDetails(sdlg.GasDetailsList);
			logger.write("Verify Gas  details - Start");
			
			logger.write("Verify Luce details - Start");
//			sdlg.verifyLuceDetatils(sdlg.LucedetailsContents);
			logger.write("Verify Luce details - Start");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
