package com.nttdata.qa.enel.testqantt;
import java.awt.Dialog.ModalityType;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CercaPODComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovaInterazioneComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaClienteAttivoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class FinalizzaCambioUso  
{

	public static void main (String[] args) throws Exception
	{
		Properties prop = null;
		try {
			//MODALITA_FIRMA;DELIVERY_CHANNEL;EMAIL
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			CambioUsoComponent cambioUso = new CambioUsoComponent(driver);
		    logger.write("Inserimento campi Conferma Richiesta - Start");
		    ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
		    driver.switchTo().defaultContent();
		    //modalita.switchFrame();
			if (prop.getProperty("MODALITA_FIRMA").contentEquals("No vocal")) {
				modalita.selezionaDeliveryChannel(modalita.deliveryChannel, prop.getProperty("DELIVERY_CHANNEL"));
				if (prop.getProperty("DELIVERY_CHANNEL").toLowerCase().contentEquals("email")) {
					modalita.enterField(modalita.emailField, prop.getProperty("EMAIL"));
					
				}
				modalita.selezionaModalitaFirma(modalita.modalitaFirma, prop.getProperty("MODALITA_FIRMA"));
			} else {
				driver.switchTo();
				modalita.selezionaModalitaFirma(modalita.modalitaFirma, prop.getProperty("MODALITA_FIRMA"));
				Thread.currentThread().sleep(5000);
				modalita.selezionaModalitaFirma(modalita.modalitaFirma, prop.getProperty("MODALITA_FIRMA"));
				modalita.selezionaDeliveryChannel(modalita.deliveryChannel, prop.getProperty("DELIVERY_CHANNEL"));
			
				modalita.selezionaIndirizzoEsistente();
			}
            modalita.press(modalita.avantiButton);
			driver.switchTo().defaultContent();
			logger.write("Inserimento campi Conferma Richiesta - Completed");
			
			logger.write("Conferma cambio uso - Start");
			
			cambioUso.switchFrame();
			cambioUso.press(cambioUso.buttonConferma);
			if(prop.getProperty("MODALITA_FIRMA").contentEquals("No vocal")) {
			cambioUso.checkModalTextAndPressOk(cambioUso.modalText, "Ricorda al cliente che se non spedisce il contratto firmato entro 3 mesi la richiesta sarà considerata nulla.");
			}
			else {
				cambioUso.checkModalTextAndPressOk(cambioUso.modalText, "Richiesta inserita. Non è necessario il rientro di documentazione obbligatoria.");
			}
			driver.switchTo().defaultContent();
			logger.write("Conferma cambio uso - Completed");
            prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
		
	}
}
