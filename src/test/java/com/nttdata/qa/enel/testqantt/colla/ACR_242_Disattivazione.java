package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.DisattivazioneFornituraComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_242_Disattivazione {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			logger.write("Click on services from Residential Menu - Start");
			if(driver.findElement(By.xpath("//button[@aria-label='chiudi']")).isDisplayed())
			hm.clickComponent(By.xpath("//button[@aria-label='chiudi']"));
			hm.clickComponent(hm.services);
			logger.write("Click on services from Residential Menu - Completed");
			
			ServicesComponent sc = new ServicesComponent(driver);
			logger.write("Verify page title and subtext for services page - Start");
			sc.verifyComponentExistence(sc.serviceTitle);
			hm.comprareText(sc.serviceSubText, sc.ServiziSubText, true);
			sc.verifyComponentExistence(sc.billServices);
			hm.comprareText(sc.billServicesSubText, sc.BolleteSubText, true);
			sc.verifyComponentExistence(sc.contractServices);
			hm.comprareText(sc.contractServicesSubText, sc.ContrattoSubText, true);
			logger.write("Verify page title and subtext for services page - Completed");
			logger.write("Click on supply details - Start");
			sc.waitForElementToDisplay(sc.supplyDeactivation);
			sc.verifyComponentExistence(sc.supplyDeactivation);
			Thread.sleep(10000);
			sc.clickComponent(sc.supplyDeactivation);
			logger.write("Click on supply details - Completed");
			Thread.sleep(5000);
			
			DisattivazioneFornituraComponent dc = new DisattivazioneFornituraComponent(driver);
			logger.write("Verify page title anf page Content on Disattivazione page - Start");
			dc.verifyComponentExistence(dc.pageTitle);
			hm.comprareText(dc.pageText, dc.PageText, true);
			logger.write("Verify page title anf page Content on Disattivazione page - Completed");
			logger.write("Click on proseguiconladisattivazione Button - Start");
			dc.clickComponent(dc.proseguiconladisattivazioneButton);
			logger.write("Click on proseguiconladisattivazione Button - Completed");
			Thread.sleep(7000);
			
			logger.write("Verify page title and supply details - Start");
			dc.waitForElementToDisplay(dc.pageTitle1);
			dc.verifyComponentExistence(dc.pageTitle1);
			hm.comprareText(dc.titleSubText, dc.TitleSubText, true);
			dc.verifyComponentExistence(dc.pdr01613101028000);
			dc.verifyComponentExistence(dc.indrizzodellafornitura01613101028000);
			dc.verifyComponentExistence(dc.modalitadirichiesta01613101028000);
			dc.verifyComponentExistence(dc.informationIcon01613101028000);
			
			hm.comprareText(dc.modalitadirichiestaValue01613101028000, dc.ModalitadirichiestaValue, true);
			logger.write("Verify page title and supply details - Completed");

			logger.write("Click on information icon - Start");
			dc.clickComponent(dc.informationIcon01613101028000);
			logger.write("Click on information icon - Completed");
			logger.write("Verify pop up content - Start");
			dc.waitForElementToDisplay(dc.popupTitle);
			hm.comprareText(dc.popupContent, dc.PopupContent01613101028000, true);
			logger.write("Verify pop up content - Completed");
			logger.write("Click on clicca qui link - Start");
			dc.clickComponent(dc.cliccaquiLink);
			logger.write("Click on clicca qui link - Completed");
			Thread.sleep(7000);
			logger.write("Verify page title and re direction - Start");
			Set <String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();			
			for (String winHandle : windows) {
		        String pagetitle = driver.getTitle();
			    driver.switchTo().window(winHandle);
			 }
			String url = "https://www-coll1.enel.it/spazio-enel";
			dc.checkURLAfterRedirection(url);
			dc.verifyComponentExistence(dc.path);
			hm.comprareText(dc.trovaLoTitle, dc.TrovaText, true);		
			driver.close();
			driver.switchTo().window(currentHandle);
			logger.write("Verify page title and re direction - Completed");

			logger.write("Click on popup close - Start");
			dc.clickComponent(dc.popupClose);
			logger.write("Click on popup close - Completed");
			dc.verifyComponentExistence(dc.pageTitle1);
			hm.comprareText(dc.titleSubText, dc.TitleSubText, true);
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
