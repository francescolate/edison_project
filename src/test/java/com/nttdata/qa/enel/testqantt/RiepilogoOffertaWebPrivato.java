package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaContrattoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RiepilogoOffertaWebPrivato {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				VerificaContrattoComponent offer=new VerificaContrattoComponent(driver);
				logger.write("corretta apertura della pagina riepilogo offerta con campi TIPO OFFERTA,POD/PDR,PREZZO, DATA RICHIESTA, NUMERO CONTRATTO, INDIRIZZO FORNITURA e delle diverse sezioni in basso - Start");
				
				TimeUnit.SECONDS.sleep(60);
				offer.clickComponent(offer.accettaCookie);
				TimeUnit.SECONDS.sleep(3);
				offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
			    offer.verifyComponentExistence(offer.riepilogoOffertaLabel);
			    offer.checkOfferta(prop.getProperty("PRODOTTO"), prop.getProperty("POD"), prop.getProperty("NUMERO_CONTRATTO"));
			    offer.verifyComponentExistence(offer.sezioneRiepilogo);
			    offer.verifyComponentExistence(offer.sezioneDatiFornitura);
			    offer.verifyComponentExistence(offer.sezioneDatiPagamento);
			    offer.verifyComponentExistence(offer.sezioneIndirizzoFatturazione);
			    offer.verifyComponentExistence(offer.sezioneIndirizzoResidenza);
			    offer.verifyComponentExistence(offer.sezioneTempiRipensamento);
			    offer.verifyComponentExistence(offer.sezioneConsensi);
			    offer.verifyComponentExistence(offer.buttonEsci);
			    offer.verifyComponentExistence(offer.buttonConferma);
				logger.write("corretta apertura della pagina riepilogo offerta con campi TIPO OFFERTA,POD/PDR,PREZZO, DATA RICHIESTA, NUMERO CONTRATTO, INDIRIZZO FORNITURA e delle diverse sezioni in basso - Completed");
				logger.write("click su CONFERMA - Start");
				offer.clickComponent(offer.buttonConferma);
				logger.write("click su CONFERMA - Completed");
				
				driver.switchTo().defaultContent();
				TimeUnit.SECONDS.sleep(15);
				
				offer.clickComponent(offer.pulsanteProsegui);
				logger.write("click su PROSEGUI - Completed");
				TimeUnit.SECONDS.sleep(30);
				
				offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
			  
				logger.write("verifica esistenza pagina con titolo 'Dichiarazione di regolare possesso/detenzione dell'immobile (DL Casa)' con i suoi check e click su 'Locazione/Comodato (Atto già registrato o in corso di registrazione' - Start");
			    offer.verifyComponentExistence(offer.titleSezioneDocumenti);
			    offer.verifyComponentExistence(offer.buttonEsci);
			    offer.verifyComponentExistence(offer.buttonConferma);
			    offer.scroll(offer.buttonEsci);
			    TimeUnit.SECONDS.sleep(3);
			    offer.checkOffertaPageCompilazioneDocumentiAndSelectTerzaOpzione(prop.getProperty("PRODOTTO"), prop.getProperty("POD"), prop.getProperty("NUMERO_CONTRATTO"));
			    TimeUnit.SECONDS.sleep(5);
			    logger.write("verifica esistenza pagina con titolo 'Dichiarazione di regolare possesso/detenzione dell'immobile (DL Casa)' con i suoi check e click su 'Locazione/Comodato (Atto già registrato o in corso di registrazione' - Completed");
				
			    logger.write("click su CONFERMA - Start");
				offer.clickComponent(offer.buttonConferma);
				logger.write("click su CONFERMA - Completed");
				
				TimeUnit.SECONDS.sleep(30);
				 offer.clickWithJS(offer.buttonCaricaLabel);
				 TimeUnit.SECONDS.sleep(3);
			    offer.verifyComponentExistence(offer.buttonCarica);
			   
			    DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
			    logger.write("Upload Documento - Start");
			    TimeUnit.SECONDS.sleep(15);
				doc.uploadFile2(
						Utility.getDocumentPdf(prop)
						);
				
				logger.write("Upload Documento - Completed");
				logger.write("Click su pulsante Invia documenti - Start");
				TimeUnit.SECONDS.sleep(15);
				offer.verifyComponentExistence(offer.buttonEsci);
				offer.verifyComponentExistence(offer.buttonInviaDocumento);
				offer.clickComponent(offer.buttonInviaDocumento);
				TimeUnit.SECONDS.sleep(30);
				logger.write("Click su pulsante Invia documenti - Completed");
				TimeUnit.SECONDS.sleep(30);
				offer.verifyComponentExistence(offer.labelCaricoRichiesta);
				driver.switchTo().defaultContent();
			    prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
