package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazioneACR_174 {

	public static void main(String[] args) throws Exception {
	
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);
	try {
		WebDriver driver = WebDriverManager.getDriverInstance(prop);
		SuplyPrivateDetailComponent spdc = new SuplyPrivateDetailComponent(driver);
		ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
	
		prop.setProperty("CITTAINPUTVALUE", "Roma");
		prop.setProperty("INDRIZZOINPUTVALUE", "Via Appia Antica");
		prop.setProperty("NUMERCOCIVICINPUTVALUE", "1");
		prop.setProperty("CAPVALUE", "00198");
		
		/*CAP = 00198      ROMA (ROMA) LAZIO
				•	COMUNE = ROMA
				•	TOPONIMO = VIA
				•	INDIRIZZO = NIZZA
				•	N. CIVICO = 4*/
		
		logger.write("Home Page Title and Description Verification- Starts");
		mif.comprareText(mif.homepageHeadingRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING_RES, true);
		mif.comprareText(mif.homepageParagraphRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_PARAGRAPH_RES, true);
		mif.comprareText(mif.homepageHeading2RES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING2_RES, true);
//		mif.comprareText(mif.homepageParagraph2RES, ModificaIndirizzoFattuazioneComponent.NUMERO__PARAGRAPH2_RES, true);
		mif.verifyComponentExistence(mif.attiva1);
		mif.verifyComponentExistence(mif.visualizzalLeBollette174);
		mif.verifyComponentExistence(mif.dettaglioFornitura174);
		mif.verifySupplyComponentExistence(mif.attiva174Sub, mif.visualizzalLeBollette174, mif.dettaglioFornitura174);
		logger.write("Home Page Title and Description Verification- Ends");
		
		logger.write("Verify and click the Servizi link - Start");
		spdc.clickComponent(spdc.ServiziLink);
		logger.write("Verify and click the Servizi link - Start");
		
		logger.write("Servizi Page Title and Description Verification- Starts");
//		mif.comprareText(mif.servizipageHeadingRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_HEADING_RES, true);
//		mif.comprareText(mif.servizipageParagraphRES, ModificaIndirizzoFattuazioneComponent.SERVIZIPAGE_PARAGRAPH_RES, true);
		logger.write("Servizi Page Title and Description Verification- Ends");
		
		logger.write("Click on Modiffica link - Start");
		spdc.clickComponent(spdc.ModifficaIndrizzolink);
		Thread.sleep(50000);
		logger.write("Click on Modiffica link - Complete");
		
		logger.write("Verify the text of Modifica Indirizzo Di Fatturazione Page- Starts");
		mif.comprareText(mif.mifHeading, ModificaIndirizzoFattuazioneComponent.MIF_HEADING, true);
		mif.comprareText(mif.mifHeading2, ModificaIndirizzoFattuazioneComponent.MIF_PARAGRAPH, true);
		mif.verifyComponentExistence(mif.supplyDetail);
		logger.write("Verify the text of Modifica Indirizzo Di Fatturazione Page- Ends");
		
		logger.write("Click on the checkbox  - Start");
		spdc.clickComponent(spdc.ModificaCheckbox);
		spdc.clickComponent(spdc.ModificaButton);
		logger.write("Click on the checkbox - Complete");
		Thread.sleep(2000);
		
		logger.write("Verify the mandatory fields  - Start");
		mif.verifyComponentExistence(mif.mifHeading);
		mif.verifyComponentExistence(mif.mifHeading2);
		spdc.verifyComponentExistence(spdc.CittaLabel);
		spdc.verifyComponentExistence(spdc.IndrizzoLabel);
		spdc.verifyComponentExistence(spdc.NumeroCivicoLabel);
		logger.write("Verify the mandatory fields  - Ends");
		
		logger.write("Enter the input values in the mandatory fields  - Start");
		
		spdc.verifyComponentExistence(spdc.CittaInputField176);
		spdc.enterLoginParameters(spdc.CittaInputField176, prop.getProperty("CITTAINPUTVALUE"));
		
		//Thread.sleep(2000);
		//spdc.verifyComponentExistence(spdc.CittaDropDownRoma);
		//spdc.clickComponent(spdc.CittaDropDownRoma);
		
		Thread.sleep(2000);
		spdc.verifyComponentExistence(spdc.IndrizzoInput176);	
		spdc.enterText(spdc.IndrizzoInput176, prop.getProperty("INDRIZZOINPUTVALUE"));
		Thread.sleep(1000);
		
		//spdc.verifyComponentExistence(spdc.IndrizzoDropDownAntica);
		//spdc.clickComponent(spdc.IndrizzoDropDownAntica);
		
		spdc.verifyComponentExistence(spdc.NumercoCivicInput176);
		spdc.enterLoginParameters(spdc.NumercoCivicInput176, prop.getProperty("NUMERCOCIVICINPUTVALUE"));
		
		spdc.verifyComponentExistence(spdc.ContinuaBtn);
		spdc.clickComponent(spdc.ContinuaBtn);	
		
		Thread.sleep(5000);
//		mif.clickComponent(mif.popCloseinfoicon);
//		
//		spdc.enterLoginParameters(spdc.CapInput, prop.getProperty("CAPVALUE"));
//		logger.write("Enter the input values in the mandatory fields  - Complete");
//		
//		logger.write("Click on the Contiue button - Start");
//		
//		spdc.clickComponent(spdc.ContinuaBtn);	
//		logger.write("Click on the Contiue button - Complete");
//		
//		spdc.clickComponent(spdc.CapIRoma);	
//		
//		spdc.clickComponent(spdc.ContinuaBtn);	
//		Thread.sleep(2000);
//		
//		//spdc.verifyComponentExistence(spdc.IndrizzoInput);	
//		spdc.enterText(spdc.IndrizzoInputNew, prop.getProperty("INDRIZZOINPUTVALUE"));
//		
//		//spdc.verifyComponentExistence(spdc.NumercoCivicInput);
//		spdc.enterLoginParameters(spdc.NumercoCivicInput, prop.getProperty("NUMERCOCIVICINPUTVALUE"));
//		
//		spdc.clickComponent(spdc.ContinuaBtn);	
//		logger.write("Verify the address details- Starts");
//		
//		Thread.sleep(5000);
//		mif.verifyAddressDetails(mif.mifHeading, ModificaIndirizzoFattuazioneComponent.INDIRIZO_FIELDS, mif.INDIRIZO_VALUES);
//		mif.comprareText(mif.indirizzoDiFatturazione, ModificaIndirizzoFattuazioneComponent.INDIRIZZO_DI_FATTURAZIONE, true);
//		mif.comprareText(mif.addressValue, ModificaIndirizzoFattuazioneComponent.HOMEPAGESS_VALUE_174, true);
//		mif.comprareText(mif.numeroCliente, ModificaIndirizzoFattuazioneComponent.NUMERO_CLIENTE_174, true);
//		
//		
//		logger.write("Verify the address details- Starts");
//		
//		logger.write("Click on Conferma and veify the text- Starts");
		mif.clickComponent(mif.confermaButtonRES);
		Thread.sleep(10000);
		mif.comprareText(mif.esitoRES, ModificaIndirizzoFattuazioneComponent.ESITO_RES, true);
		//mif.comprareText(mif.esitoRESHeading, ModificaIndirizzoFattuazioneComponent.ESITO_RES_HEADING, true);
		//mif.comprareText(mif.esitoRESParagraph, ModificaIndirizzoFattuazioneComponent.ESITO_RES_PARAGRAPH, true);
		logger.write("Click on Conferma and veify the text- Ends");
		
		logger.write("Click on Fine and veify the text- Starts");
//		mif.clickComponent(mif.fineButtonRES);
		Thread.sleep(10000);
//		mif.comprareText(mif.homepageHeadingRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING_RES, true);
//		mif.comprareText(mif.homepageParagraphRES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_PARAGRAPH_RES, true);
//		mif.comprareText(mif.homepageHeading2RES, ModificaIndirizzoFattuazioneComponent.HOMEPAGE_HEADING2_RES, true);
//		mif.comprareText(mif.homepageParagraph2RES, ModificaIndirizzoFattuazioneComponent.NUMERO__PARAGRAPH2_RES, true);
//		logger.write("Click on Fine and veify the text- Ends");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	

}

}
