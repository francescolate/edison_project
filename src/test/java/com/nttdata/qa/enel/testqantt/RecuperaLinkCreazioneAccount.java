package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.AccountComponent;
import com.nttdata.qa.enel.components.colla.AccountRegistrationComponent;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class RecuperaLinkCreazioneAccount {

	@Step("Leggi email e clicca sul Touch Point di Attivazione")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String link = null;			
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
		            logger.write("Recupera Link - Start"); 
		            GmailQuickStart gm = new GmailQuickStart();
		    		link = gm.recuperaLink(prop, "NEW_ACCOUNT", false);
		            logger.write("Recupera Link - Completed");
		            Thread.sleep(5000);
		            AccountRegistrationComponent arc = new AccountRegistrationComponent(driver);
		            arc.launchLink(link);
		            Thread.sleep(15000);
		            arc.verifyComponentExistence(arc.profiloUnicoCreatoHeader);
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			cealc.restoreAvailableEmailAddresses(cealc.getListOfAvailableEmailAddresses(), prop.getProperty("WP_USERNAME"));
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}


