package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

//Il seguente modulo si occupa della creazione di un nuovo Cliente accedendo prima alla sezione Clienti dal menu a tendina del primo tab disponibile 
//Poi cliccando su Nuovo inserirà la tipologia di cliente , e compilerà tutti i campi necessari al completamento della creazione quali nomi, indirizzi, referenti etc..
public class CreazioneNuovoCliente {

	@Step("Creazione Nuovo Cliente")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {

			
			//QANTTLogger logger = new QANTTLogger(prop);
			logger.write("Accesso al Tab Clienti - Start");
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				AccediTabClientiComponent tabClienti = new AccediTabClientiComponent(driver);
				tabClienti.accediTabClienti();
				logger.write("Accesso al Tab Clienti - Completed");
				CreaNuovoClienteComponent crea = new CreaNuovoClienteComponent(driver);
				CompilaIndirizziComponent indirizzi = new CompilaIndirizziComponent(driver);
				String frameName;
				switch (prop.getProperty("TIPOLOGIA_CLIENTE")) {
				case "RESIDENZIALE":
					logger.write("Pressione pulsante Nuovo - Start");
					crea.nuovoCliente();
					Thread.currentThread().sleep(5000);
					logger.write("Pressione pulsante Nuovo - Completed");
					logger.write("Scelta Tipologia Cliente Residenziale - Start");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonResidenziale);
					logger.write("Scelta Tipologia Cliente Residenziale - Completed");
					logger.write("Verifica esistenza sezione Social e Web, e sezione Studi e Professione - Start");
					crea.verificaEsistenzaCampiSocialeStudi(frameName);
					logger.write("Verifica esistenza sezione Social e Web, e sezione Studi e Professione - Completed");
					logger.write("Inserimento campi anagrafica - Start");
					crea.creaClienteResidenziale(prop);
					logger.write("Inserimento campi anagrafica - Completed");
					logger.write("Inserimento indirizzo Domicilio e Residenza - Start");
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
						indirizzi.compilaIndirizziResidenziale(frameName, prop.getProperty("PROVINCIA"),
								prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaindirizzoDomicilioUBIEST(frameName, prop.getProperty("PROVINCIA"),
								prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
						Thread.currentThread().sleep(1000);
						indirizzi.forzaCopiaIndirizzo(frameName);
						Thread.currentThread().sleep(1000);

					}
						logger.write("Inserimento indirizzo Domicilio e Residenza - Completed");

					break;
				case "IMPRESA":
					logger.write("Pressione pulsante Nuovo - Start");
					crea.nuovoCliente();
					logger.write("Pressione pulsante Nuovo - Completed");
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonImpresaIndividuale);
					crea.creaClienteImpresaIndividuale(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
					indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}

					break;

				case "CONDOMINIO":
					crea.nuovoCliente();
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonCondominio);
					crea.creaClienteCondominio(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
						indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					break;

				case "BUSINESS":
					crea.nuovoCliente();
					frameName = crea.selezionaTipologiaCliente(crea.radioButtonBusiness);
					crea.creaClienteBusiness(prop);
					if (!Costanti.statusUbiest.equalsIgnoreCase("OFF"))
					{//Ubiest ATTIVO
						indirizzi.compilaSedeLegale(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
							prop.getProperty("TELEFONO_CLIENTE"));
					}
					else
					{//Ubiest NON Attivo
						indirizzi.forzaIndirizzoSedeLegaleUBIEST(frameName, prop.getProperty("PROVINCIA"), prop.getProperty("COMUNE"),
								prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"),
								prop.getProperty("TELEFONO_CLIENTE"));
					}
					break;
				default:
					throw new Exception(
							"Tipologia cliente diversa da RESIDENZIALE/IMPRESA/CONDOMINIO/BUSINESS. Impossibile proseguire");
				}
				logger.write("Compilazione campi Contatti - Start");
				if(prop.getProperty("CREA_CLIENTE_INSERISCI_MAIL","").contentEquals("Y") 
								&& !prop.getProperty("EMAIL","").contentEquals("") )
				{
					//R3 - FR
					crea.compilaContattiEmail(frameName, prop.getProperty("CELLULARE")
							, prop.getProperty("DESCRIZIONE_CELLULARE")
							, prop.getProperty("EMAIL"), prop.getProperty("DESCRIZIONE_EMAIL"));
				}
				else
				{
					crea.compilaContatti(frameName, prop.getProperty("CELLULARE"),
							prop.getProperty("DESCRIZIONE_CELLULARE"));
				}
				
				logger.write("Compilazione campi Contatti - Completed");
				
				
				if(prop.getProperty("DIPENDENTE_ENEL","Y").contentEquals("Y")) crea.fleggaDipendenteEnel(frameName);
				
				
				if(prop.getProperty("ANNULLA_CREAZIONE","N").contentEquals("N")) {
				
				
				logger.write("Conferma creazione cliente - Start");
				crea.confermaCreazione();
				TimeUnit.SECONDS.sleep(10);
				
				if(prop.getProperty("VERIFICA_PRESENZA_CLIENTE","N").contentEquals("Y")) {
					logger.write("Verifica errore creazione per cliente già presente - Start");
					crea.verificaClienteGiaPresente();
					logger.write("Verifica errore creazione per cliente già presente - Completed");
				}else {
					logger.write("Verifica creazione cliente - Start");
					crea.verificaCreazione();
					logger.write("Verifica creazione cliente - Completed");
				}
				
				}
				else {
					logger.write("Annulla creazione cliente - Start");
					
					crea.annullaCreazione();
					
					logger.write("Annulla creazione cliente - Completed");
					
					
				}
				
				

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());
			
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
