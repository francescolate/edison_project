package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

public class SezioneMercatoSubentroSalvaguardiaPodGas {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
			IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(driver);
			SezioneMercatoComponent merc = new SezioneMercatoComponent(driver);
			InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);

			Thread.sleep(5000);

			checkListModalComponent.clickConferma();

			prop.setProperty("NUMERO_RICHIESTA",
					intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
			logger.write("Verifica checklist e conferma - Completed");

			logger.write(
					"verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Start");

			merc.verifyComponentExistence(merc.tabSubentro);
			merc.verifyComponentExistence(merc.labelSubentro);

			merc.verifyComponentExistence(insert.sezioneInserimentoDati);

			logger.write(
					"verifica apertura del tab Subentro e della pagina di inserimento dati relativo al Case di Subentro - Completed");

			logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Start");
			merc.verifyComponentExistence(merc.checkContatto);
			logger.write("verifica esistenza semaforo verde per la funzionalità di Check Contatto - Completed");

			logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Start");
			merc.verifyComponentExistence(merc.checkCf);
			logger.write("verifica  esistenza semaforo grigio per la funzionalità di Check CF - Completed");

			// Box Sezione Mercato

			Thread.sleep(5000);

			merc.clickComponentIfExist(merc.inputSezioneMercato);

			merc.simulateDownArrowKey(merc.inputSezioneMercato);

			merc.simulateEnterKey(merc.inputSezioneMercato);

			merc.clickComponentIfExist(merc.buttonConfermaSezioneMercato);

			merc.clearAndSendKeys(merc.inputPodPdr, prop.getProperty("POD"));

			merc.clearAndSendKeys(merc.inputCap, prop.getProperty("CAP"));

			merc.clickComponentIfExist(merc.buttonEseguiPrecheck);
			
			try {
			WebElement istat = driver.findElement(merc.popupCodiceIstat);
			if(istat.isDisplayed()) {
				new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='slds-radio']//input[@aria-posinset='3']//..//input")));
				//driver.findElement(By.xpath("//div[@class='slds-radio']//input[@aria-posinset='3']//..//input")).click();   // seleziona il terzo radio button
				
				WebElement testJS = driver.findElement(By.xpath("//div[@class='slds-radio']//input[@aria-posinset='3']//..//input"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click()", testJS);
				driver.findElement(By.xpath("//button[@name='Conferma Istat']")).click();
			} 
			} catch (NoSuchElementException e1) {	
				System.out.println("Pop-up istat non apparso");
			} catch (Exception e2) {
				System.out.println("Eccezione intercettata, quest'eccezione non interrompe l'esecuzione del codice");
				e2.printStackTrace();
			}

			util.getFrameActive();
			List<WebElement> error = driver.findElements(merc.popupErrorePrecheck);
			for (WebElement mesErr : error) {
				if (mesErr.getText().equals("Attenzione! Per Mercato Riferimento Prodotto = " +  prop.getProperty("MERCATO") + " è possibile inserire solo siti elettrici")) {     //Costanti.subentro_id14_popupmercato
					logger.write("Testo di errore corretto");
					break;
				} else {
					logger.write("Testo di errore errato");
				}
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());
			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}