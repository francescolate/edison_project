package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModificaRecapitoBollComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_58_ModificaRecapitoBoll {

	public static void main(String[] args) throws Exception {
		
 		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			ModificaRecapitoBollComponent mrbc = new ModificaRecapitoBollComponent(driver);
			String condominiumPageUrl =  "https://www-coll1.enel.it/it/offerta-energia-per-condominio";
			String modificaRecapitoUrl = "https://www-coll1.enel.it/it/modifica-recapito-bolletta?zoneid=offerta-energia-per-condominio-tile_small";
			
			//1
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
 			logger.write("apertura del portale web Enel di test - Completed");
 			
			Thread.sleep(10000);
			//2
 			logger.write("Clicking On Hamburger Menu and condimini and verify the data- Starts");
			mrbc.verifyComponentExistence(mrbc.hamburgerMenu);
			mrbc.clickComponent(mrbc.hamburgerMenu);
			mrbc.waitForElementToDisplay(mrbc.hiddenArea);
			mrbc.clickComponent(mrbc.condomini); 
			
			mrbc.checkURLAfterRedirection(condominiumPageUrl);
			mrbc.comprareText(mrbc.condominiPath, ModificaRecapitoBollComponent.CONDOMINI_PATH, true);
			mrbc.comprareText(mrbc.condominiHeading, ModificaRecapitoBollComponent.CONDOMINI_HEADING, true);
						
			//3
			mrbc.comprareText(mrbc.condiminiSections, ModificaRecapitoBollComponent.CONDOMINI_SECTION, true);
			mrbc.comprareText(mrbc.condiminiSection2, ModificaRecapitoBollComponent.CONDOMINI_SECTION2, true);
			logger.write("Clicking On Hamburger Menu and condimini and verify the data- Ends");
			
			//4
			logger.write("Clicking On Recapito Bolleta and verify the data- Starts");
			mrbc.comprareText(mrbc.recapitoBolletta, ModificaRecapitoBollComponent.RECAPITO_BOLLETTA, true);
			mrbc.comprareText(mrbc.recapitoBollettaText, ModificaRecapitoBollComponent.RECAPITO_BOLLETTA_TEXT, true);
			mrbc.clickComponent(mrbc.recapitoBolletta);
			
			mrbc.checkURLAfterRedirection(modificaRecapitoUrl);
			mrbc.comprareText(mrbc.modificaRecapitoPath, ModificaRecapitoBollComponent.MODIFICA_RECAPITO_PATH, true);
			logger.write("Clicking On Recapito Bolleta and verify the data- Ends");
			
			//5
			logger.write("Verify the existance of all the mandatory fields- Starts");
			mrbc.comprareText(mrbc.variazioneIndirrizo, ModificaRecapitoBollComponent.VARIAZIONE_INDIRRIZO, true);
			mrbc.verifyComponentExistence(mrbc.amministratoreDiCondominio);
			mrbc.verifyComponentExistence(mrbc.partitaIva);
			mrbc.verifyComponentExistence(mrbc.codiceFiscale);
			mrbc.verifyComponentExistence(mrbc.telefonoFisso);
			mrbc.verifyComponentExistence(mrbc.email);
			
			//6
			mrbc.comprareText(mrbc.siRichiedeSectionText, ModificaRecapitoBollComponent.SI_RICHIEDE_SECTION_TEXT, true);
			mrbc.verifyComponentExistence(mrbc.denominazioneCondominio);
			mrbc.verifyComponentExistence(mrbc.tipoFornitura);
			mrbc.verifyComponentExistence(mrbc.numeroCliente);
			
			mrbc.comprareText(mrbc.inserireHeading, ModificaRecapitoBollComponent.INSERIRE_HEADING, true);
			mrbc.verifyComponentExistence(mrbc.presso);
			mrbc.verifyComponentExistence(mrbc.indirrizo);
			mrbc.verifyComponentExistence(mrbc.indirrizz);
			mrbc.verifyComponentExistence(mrbc.numero);
			mrbc.verifyComponentExistence(mrbc.provincia);
			mrbc.verifyComponentExistence(mrbc.commune);
			mrbc.verifyComponentExistence(mrbc.cap);
			mrbc.checkForIndirrizoDropDownValue(mrbc.indirrizo, ModificaRecapitoBollComponent.INDIRRIZO);
			mrbc.clickComponent(mrbc.provincia);
			mrbc.verifyComponentExistence(mrbc.provinciaList);
			logger.write("Verify the existance of all the mandatory fields- Ends");
			
			//7
			logger.write("Verify the existance of all the mandatory fields in section1 after clicking on Altra Fornitura Button- Starts");
			mrbc.clickComponent(mrbc.altraFornituraButtonn);
			
			mrbc.comprareText(mrbc.siRichiedeSectionText1, ModificaRecapitoBollComponent.SI_RICHIEDE_SECTION_TEXT, true);
			mrbc.verifyComponentExistence(mrbc.denominazioneCondominio1);
			mrbc.verifyComponentExistence(mrbc.tipoFornitura1);
			mrbc.verifyComponentExistence(mrbc.numeroCliente1);
			
			mrbc.comprareText(mrbc.inserireHeading1, ModificaRecapitoBollComponent.INSERIRE_HEADING, true);
			mrbc.verifyComponentExistence(mrbc.presso1);
			mrbc.verifyComponentExistence(mrbc.indirrizo1);
			mrbc.verifyComponentExistence(mrbc.indirrizz1);
			mrbc.verifyComponentExistence(mrbc.numero1);
			mrbc.verifyComponentExistence(mrbc.provincia1);
			mrbc.verifyComponentExistence(mrbc.commune1);
			mrbc.verifyComponentExistence(mrbc.cap1);
			logger.write("Verify the existance of all the mandatory fields in section1 after clicking on Altra Fornitura Button- Ends");
			
			//8
			logger.write("Verify the existance of all the mandatory fields in section2 after clicking on Altra Fornitura Button- Starts");
			mrbc.clickComponent(mrbc.altraFornituraButton2);
			
			mrbc.comprareText(mrbc.siRichiedeSectionText2, ModificaRecapitoBollComponent.SI_RICHIEDE_SECTION_TEXT, true);
			mrbc.verifyComponentExistence(mrbc.denominazioneCondominio2);
			mrbc.verifyComponentExistence(mrbc.tipoFornitura2);
			mrbc.verifyComponentExistence(mrbc.numeroCliente1);
			
			mrbc.comprareText(mrbc.inserireHeading2, ModificaRecapitoBollComponent.INSERIRE_HEADING, true);
			mrbc.verifyComponentExistence(mrbc.presso2);
			mrbc.verifyComponentExistence(mrbc.indirrizo2);
			mrbc.verifyComponentExistence(mrbc.indirrizz2);
			mrbc.verifyComponentExistence(mrbc.numero2);
			mrbc.verifyComponentExistence(mrbc.provincia2);
			mrbc.verifyComponentExistence(mrbc.commune2);
			mrbc.verifyComponentExistence(mrbc.cap2);	
			logger.write("Verify the existance of all the mandatory fields in section2 after clicking on Altra Fornitura Button- Ends");
			
			//9
			logger.write("Verify the non existance of all the mandatory fields in section2 after clicking on Elimina2 Button- Starts");
			mrbc.clickComponent(mrbc.eliminasection2);
			mrbc.isElementNotPresent(mrbc.section2);
			logger.write("Verify the non existance of all the mandatory fields in section2 after clicking on Elimina2 Button- Ends");
			
			
			//10
			logger.write("Verify the non existance of all the mandatory fields in section1 after clicking on Elimina1 Button- Starts");
			mrbc.clickComponent(mrbc.eliminasection1);
			mrbc.isElementNotPresent(mrbc.section1);
			logger.write("Verify the non existance of all the mandatory fields in section1 after clicking on Elimina1 Button- Ends");
			
			
			//11
			logger.write("Verify the Campi Pbbligatori text for invalid inputs Starts");
			mrbc.comprareText(mrbc.informativaPrivacyLabel, ModificaRecapitoBollComponent.INFORMATIVA_PRIVACY_LABEL, true);
		//	mrbc.comprareText(mrbc.informativaPrivacyText, ModificaRecapitoBollComponent.INFORMATIVA_PRIVACY_TEXT, true);
			
			//12
			mrbc.verifyComponentExistence(mrbc.accettoButton);
			mrbc.verifyComponentExistence(mrbc.nonAccettoButton);
			mrbc.comprareText(mrbc.campiObbligatoriPara1, ModificaRecapitoBollComponent.CAMPI_OBLIGATORI_PARA1, true);
			mrbc.comprareText(mrbc.campiObbligatoriPara2, ModificaRecapitoBollComponent.CAMPI_OBLIGATORI_PARA2, true);
			
			//13
			mrbc.clickComponent(mrbc.inviaButton);
			
			mrbc.verifyComponentExistence(mrbc.campoObbligAmministratore);
			mrbc.comprareText(mrbc.campoObbligAmministratore, ModificaRecapitoBollComponent.CAMPI_OBBLIGO_AMMINISTRATORE, true);
			
			mrbc.verifyComponentExistence(mrbc.campoObbligPartitaIva);
			mrbc.comprareText(mrbc.campoObbligPartitaIva, ModificaRecapitoBollComponent.CAMPI_OBBLIGO_PARTITAIVA, true);
			
			mrbc.verifyComponentExistence(mrbc.campoObbligCodiceFiscale);
			mrbc.comprareText(mrbc.campoObbligCodiceFiscale, ModificaRecapitoBollComponent.CAMPI_OBBLIGO_CODICE_FISCALE, true);
			
			mrbc.verifyComponentExistence(mrbc.campoObbligTelefonoFisso);
			mrbc.comprareText(mrbc.campoObbligTelefonoFisso, ModificaRecapitoBollComponent.CAMPI_OBBLIGO_CODICE_TELEFONA, true);
			
			mrbc.verifyComponentExistence(mrbc.campoObbligEmail);
			mrbc.comprareText(mrbc.campoObbligEmail, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligDenominazione);
			mrbc.comprareText(mrbc.campiObbligDenominazione, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligTipoFornitura);
			mrbc.comprareText(mrbc.campiObbligTipoFornitura, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligNumeroClientie);
			mrbc.comprareText(mrbc.campiObbligNumeroClientie, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligPresso);
			mrbc.comprareText(mrbc.campiObbligPresso, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligIndirrizzo);
			mrbc.comprareText(mrbc.campiObbligIndirrizzo, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligProvincia);
			mrbc.comprareText(mrbc.campiObbligProvincia, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligCommune);
			mrbc.comprareText(mrbc.campiObbligCommune, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			mrbc.verifyComponentExistence(mrbc.campiObbligCAP);
			mrbc.comprareText(mrbc.campiObbligCAP, ModificaRecapitoBollComponent.CAMPI_OBBLIGO, true);
			
			//14
			
			prop.setProperty("INVALID_EMAIL", "abcd@@");
			mrbc.enterInputParameters(mrbc.email, prop.getProperty("INVALID_EMAIL"));
			
			mrbc.verifyComponentExistence(mrbc.emailNonValida);
			mrbc.comprareText(mrbc.emailNonValida, ModificaRecapitoBollComponent.EMAIL_NON_VALIDA, true);
			
			//16
			prop.setProperty("INVALIDPARTITA_IVA", "A1a32323151");
			mrbc.enterInputParameters(mrbc.partitaIva, prop.getProperty("INVALIDPARTITA_IVA"));
			
			mrbc.verifyComponentExistence(mrbc.numeroNonValida);
			mrbc.comprareText(mrbc.numeroNonValida, ModificaRecapitoBollComponent.PARTITA_NON_VALIDA, true);
			
			//17
			prop.setProperty("INVALID_TELEFONO", "AAAAAAAAAAAAAAAA");
			mrbc.enterInputParameters(mrbc.telefonoFisso, prop.getProperty("INVALID_TELEFONO"));
			
			mrbc.verifyComponentExistence(mrbc.numeroNonValidaTelefono);
			//mrbc.comprareText(mrbc.numeroNonValidaTelefono, ModificaRecapitoBollComponent.NUMERO_NON_VALIDA, true);
			mrbc.comprareText(mrbc.numeroNonValidaTelefono, ModificaRecapitoBollComponent.NUMERO_NON_VALIDO, true);
			
			//18
			prop.setProperty("INVALID_NUMERO_CLIENTE", "AAAAAAAAA");
			mrbc.enterInputParameters(mrbc.numeroCliente, prop.getProperty("INVALID_NUMERO_CLIENTE"));
			
			mrbc.verifyComponentExistence(mrbc.numeroNonValidaNumeroCliente);
			mrbc.comprareText(mrbc.numeroNonValidaNumeroCliente, ModificaRecapitoBollComponent.NUMERO_NON_VALIDA1, true);
			logger.write("Verify the Campi Pbbligatori text for invalid inputs Ends");
			
			//19
			logger.write("Verify the values of Provincia, Commune and Cap - Starts");
			mrbc.clickComponent(mrbc.provincia);
			mrbc.scrollComponent(mrbc.roma);
			mrbc.clickComponent(mrbc.roma);
			Thread.sleep(10000);
			mrbc.clickComponent(mrbc.commune);
			mrbc.verifyComponentExistence(mrbc.communeList);
			
			//20
			mrbc.clickComponent(mrbc.communeRoma);
			Thread.sleep(10000);
			mrbc.clickComponent(mrbc.cap);
			mrbc.verifyComponentExistence(mrbc.capList);
			logger.write("Verify the values of Provincia, Commune and Cap - Ends");
			
			//21
			logger.write("Enter valid inputs in all mandatory fields and click on Invia Button - Starts");
			prop.setProperty("ADMINISTRATORE", "GIOCONDA BALZO");
			prop.setProperty("PARTITA_IVA", "90097060736");
			prop.setProperty("CODICE_FISCALE", "BLZGND65A46L049K");
			prop.setProperty("TELEFONO", "3669047153");
			prop.setProperty("EMAIL", "fabiana.manzo@nttdata.com");
			
			prop.setProperty("DENOMINAZIONE_CONDOMINIO", "CONDOMINIO PRESSO CONDOMINIO VIA BRUNELL");
			prop.setProperty("TIPO_FORNITURA", "Elettrica");
			prop.setProperty("NUMERO_CLIENTE", "777825292");
			
			prop.setProperty("PRESSO", "SPA COGNOME NOME");
			prop.setProperty("INDIRRIZO", "NIZA");
			prop.setProperty("NUM", "2");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("COMMUNE", "ROMA");
			prop.setProperty("CAP", "00198");
			
			mrbc.enterInputParameters(mrbc.amministratoreDiCondominio, prop.getProperty("ADMINISTRATORE"));
			mrbc.enterInputParameters(mrbc.partitaIva, prop.getProperty("PARTITA_IVA"));
			mrbc.enterInputParameters(mrbc.codiceFiscale, prop.getProperty("CODICE_FISCALE"));
			mrbc.enterInputParameters(mrbc.telefonoFisso, prop.getProperty("TELEFONO"));
			mrbc.enterInputParameters(mrbc.email, prop.getProperty("EMAIL"));
			
			mrbc.enterInputParameters(mrbc.denominazioneCondominio, prop.getProperty("DENOMINAZIONE_CONDOMINIO"));
			mrbc.clickComponent(mrbc.tipoFornitura);
			mrbc.clickComponent(mrbc.elettrica);
			mrbc.enterInputParameters(mrbc.numeroCliente, prop.getProperty("NUMERO_CLIENTE"));
			
			mrbc.enterInputParameters(mrbc.presso, prop.getProperty("PRESSO"));
			mrbc.clickComponent(mrbc.indirrizo);
			mrbc.clickComponent(mrbc.ALZ);
			mrbc.enterInputParameters(mrbc.indirrizz, prop.getProperty("INDIRRIZO"));
			mrbc.enterInputParameters(mrbc.numero, prop.getProperty("NUM"));
			mrbc.clickComponent(mrbc.provincia);
			mrbc.scrollComponent(mrbc.roma);
			mrbc.clickComponent(mrbc.roma);
			mrbc.clickComponent(mrbc.commune);
			mrbc.clickComponent(mrbc.communeRoma);
			mrbc.clickComponent(mrbc.cap);
			mrbc.clickComponent(mrbc.cap00198);
			
			mrbc.clickComponent(mrbc.accettoButton);
			mrbc.clickComponent(mrbc.inviaButton);
			
			mrbc.comprareText(mrbc.modificaRecaitoBollettaStep2Heading,ModificaRecapitoBollComponent.DATA_SUMMARY_HEADING, true);
			mrbc.comprareText(mrbc.modificaRecaitoBollettaStep2Text,ModificaRecapitoBollComponent.DATA_SUMMARY_TEXT, true);
			mrbc.comprareText(mrbc.modificaRecaitoBollettaStep2Heading2,ModificaRecapitoBollComponent.DATA_SUMMARY_HEADING2, true);
			
			mrbc.comprareText(mrbc.data1, ModificaRecapitoBollComponent.DATA1_TEXT, true);
			mrbc.comprareText(mrbc.data2, ModificaRecapitoBollComponent.DATA2_TEXT, true);
			mrbc.comprareText(mrbc.data3, ModificaRecapitoBollComponent.DATA3_TEXT, true);
			mrbc.comprareText(mrbc.data4, ModificaRecapitoBollComponent.DATA4_TEXT, true);
			mrbc.comprareText(mrbc.data5, ModificaRecapitoBollComponent.DATA5_TEXT, true);
			mrbc.comprareText(mrbc.data6, ModificaRecapitoBollComponent.DATA6_TEXT, true);
			mrbc.comprareText(mrbc.data7, ModificaRecapitoBollComponent.DATA7_TEXT, true);
			mrbc.comprareText(mrbc.data8, ModificaRecapitoBollComponent.DATA8_TEXT, true);
			mrbc.comprareText(mrbc.data9, ModificaRecapitoBollComponent.DATA9_TEXT, true);
			mrbc.comprareText(mrbc.data10, ModificaRecapitoBollComponent.DATA10_TEXT, true);
			mrbc.comprareText(mrbc.data11, ModificaRecapitoBollComponent.DATA11_TEXT, true);
			mrbc.comprareText(mrbc.data12, ModificaRecapitoBollComponent.DATA12_TEXT, true);
			mrbc.comprareText(mrbc.data13, ModificaRecapitoBollComponent.DATA13_TEXT, true);
			logger.write("Enter valid inputs in all mandatory fields and click on Invia Button - Ends");
			
			//22
			logger.write("Verification of success message - Starts");
			
			mrbc.clickComponent(mrbc.confermaButton);
			Thread.sleep(5000);
			mrbc.comprareText(mrbc.modificaRecapitoStep3Heading, ModificaRecapitoBollComponent.MODIFICA_RECAPITO_STEP3_HEADING, true);
			mrbc.comprareText(mrbc.modificaRecapitoStep3Heading2, ModificaRecapitoBollComponent.MODIFICA_RECAPITO_STEP3_HEADING2, true);
			mrbc.comprareText(mrbc.modificaRecapitoStep3Text1, ModificaRecapitoBollComponent.MODIFICA_RECAPITO_STEP3_TEXT1, true);
			mrbc.comprareText(mrbc.modificaRecapitoStep3Text2, ModificaRecapitoBollComponent.MODIFICA_RECAPITO_STEP3_TEXT2, true);
			logger.write("Verification of success message - Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} 
		catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
