package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class CaricaDocumentoSubentro {

    @Step("Carica Documenti")
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {
            
            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
           
            //util.getFrameActive();
            driver.switchTo().defaultContent();      
            Thread.sleep(5000);

            logger.write("Upload Documento - Start");
            doc.uploadFile(
                    Utility.getDocumentPdf(prop)
            );
            logger.write("Upload Documento - Completed");

            prop.setProperty("RETURN_VALUE", "OK");
            System.out.println("documento caricato");
        } catch (Exception e) {
        	System.out.println("catch carica documento");
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
