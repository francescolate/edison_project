// Package HerokuAPIDelete_1

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIDelete_Component;
import com.nttdata.qa.enel.testqantt.RecuperaCodiceDaLinkCreazioneAccount;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIDelete_1 {

	/** 
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku TAB Accessi Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			APIService.deleteAccount("cf", prop.getProperty("CF_PIVA"));
			String enelID = APIService.registerResidentialAccount_2(prop.getProperty("EMAIL"),prop.getProperty("PHONENUMBER"),prop.getProperty("FIRSTNAME"),prop.getProperty("LASTNAME"),prop.getProperty("CF_PIVA"));
			
			prop.setProperty("ENELID", enelID) ;
			prop.setProperty("API_URL", "https://uniqueid-coll.enel.com:8243/registration/account/"+ prop.getProperty("ENELID"));
			
			Thread.sleep(90000);
			RecuperaCodiceDaLinkCreazioneAccount.main(args); // deve restituire OK per poter procedere con il delete.
			Thread.sleep(30000);
			//Da configurare come OutPut
			prop.setProperty("TID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString());
			prop.setProperty("SID", "7f24e2c9-7cce-4c4f-a25a-c112cbdb6130"); //UUID.randomUUID().toString());
    		prop.setProperty("TOUCHPOINT","WEB");
    		prop.setProperty("SOURCE_CHANNEL","EE");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIDelete_Component HRKComponent = new HerokuAPIDelete_Component();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);


			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


