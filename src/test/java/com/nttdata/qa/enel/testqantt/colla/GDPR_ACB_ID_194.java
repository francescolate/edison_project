package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GDPRComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class GDPR_ACB_ID_194 {

	public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		GDPRComponent gdpr = new GDPRComponent(driver);
		String urlIP = "https://www-coll1.enel.it/it/supporto/faq/privacy";
				
		logger.write("Verification of Home Page Data  - Start ");
		gdpr.comprareText(gdpr.homePagePath1, GDPRComponent.HOME_PAGE_PATH1, true);
		gdpr.comprareText(gdpr.homePagePath2, GDPRComponent.HOME_PAGE_PATH2, true);
		gdpr.comprareText(gdpr.homePageHeading, GDPRComponent.HOME_PAGE_HEADING, true);
		logger.write("Verification of Home Page Data  - End ");
		
		logger.write("From the left side menu Menù LOGO located at the top, check that there is no Informativa Privacy item- Start ");
		gdpr.isElementNotPresent(gdpr.informativaPrivacy);
		gdpr.clickComponent(gdpr.iconUser);
		gdpr.verifyComponentExistence(gdpr.customerData);
		logger.write("From the left side menu Menù LOGO located at the top, check that there is no Informativa Privacy item- Ends ");
		
		logger.write("Click on the Informativa privacy item and check that the system navigates to the address- Starts");
		PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
		String currentHandle = driver.getWindowHandle();
		gdpr.clickComponent(gdpr.informativaPrivacy);
		pac.switchToNewTab();
		gdpr.checkURLAfterRedirection(urlIP);
		driver.close();
		pac.switchToOriginaTab(currentHandle);
		gdpr.comprareText(gdpr.condizionigenerali, GDPRComponent.CONDIZIONI_GENERALI, true);
		gdpr.comprareText(gdpr.hoLettoText, GDPRComponent.HO_LETTO_TEXT, true);
		gdpr.comprareText(gdpr.informativaPrivacyText, GDPRComponent.INFORMATIVA_PRIVACY_TEXT, true);
		logger.write("Click on the Informativa privacy item and check that the system navigates to the address- Ends");
		
		logger.write("Click on the Informativa privacy item and check that the system navigates to the address- Starts");
		gdpr.clickComponent(gdpr.informativaPrivacy);
		pac.switchToNewTab();
		gdpr.checkURLAfterRedirection(urlIP);
		driver.close();
		logger.write("Click on the Informativa privacy item and check that the system navigates to the address- Ends");
		
		prop.setProperty("RETURN_VALUE", "OK");
		
		} 
		catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}


}

}
