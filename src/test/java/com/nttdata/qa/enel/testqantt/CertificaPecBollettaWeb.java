package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class CertificaPecBollettaWeb 
{
	//private Properties prop = null;
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			String stringaIncorpo;
			String oggetto = "Completa l?attivazione del servizio Bolletta Web";
//			String splitName[] = prop.getProperty("ACCOUNT.NAME").split(" ");
			if(prop.getProperty("SPLIT_NAME","").equals("")){
				String splitName[] = prop.getProperty("COMPLETE_NAME").split(" ");
				stringaIncorpo = "Gentile "+splitName[1] +" "+splitName[0];
			} else {
				 stringaIncorpo = "Gentile "+prop.getProperty("SPLIT_NAME","");
			}
			String testoLink = "Conferma Validità Mail";
		   driver.get(prop.getProperty("LINK"));
		   util.objectManager(By.xpath("//input[@id='user']"), util.sendKeys, prop.getProperty("USERNAME"));
		   util.objectManager(By.xpath("//input[@id='password']"), util.sendKeys, prop.getProperty("PASSWORD"));
		   util.objectManager(By.xpath("//button[@id='login-button']"), util.scrollAndClick);
		   if(!util.verifyExistence(By.xpath("//button[contains(text(),'Nuovo messaggio')]"), 30)) throw new Exception("Login al portale infocert per mail pec non avvenuta correttamente.");
           Thread.currentThread().sleep(10000);
		   By x = By.xpath("//div[contains(@class,'main-content')]//ul[contains(@class,'message-list')]/li[1]//div[@class='item-body']/div/span[contains(text(),'"+oggetto+"')]");
	       List<WebElement> a = util.waitAndGetElements(x,60,1);
		   boolean found = false;
	       for(int p=0;p<a.size();p++){
	    	   driver.switchTo().defaultContent();
//	    	   Thread.currentThread().sleep(5000);
//	    	   a = util.waitAndGetElements(x,60,1);
	    	   WebElement el = a.get(p);
	    	   util.objectManager(el, util.scrollToVisibility, true);
	    	   util.objectManager(el, util.scrollAndClick);
//	    	   util.FrameSwitcher(By.xpath("//iframe[contains(@id,'message-html')]"));
	    	   util.FrameSwitcher(By.xpath("//iframe[contains(@id,'vivocha_data')]"));
	    	   if(util.verifyExistence(By.xpath("//body//span[contains(text(),'"+stringaIncorpo+"')]"), 10)) {
	    		   found = true;
	    		   By link = By.xpath("//body//a[contains(text(),'"+testoLink+"')]");
	    		   if(util.verifyExistence(link, 10)) {
	    			   util.objectManager(link, util.scrollAndClick);
	    			   break;
	    		   }
	    		   
	    		   else throw new Exception("Impossibile trovare nel corpo della mail il link "+testoLink);
	    		 
	    	   }
	    	   
	       }
	       
	       if(!found) throw new Exception("Impossibile trovare la mail con oggetto :"+oggetto+" nel cui corpo sia presente la stringa "+stringaIncorpo);

		   
	       driver.switchTo().defaultContent();
	       Thread.currentThread().sleep(5000);
	       String winHandleBefore = driver.getWindowHandle();

		    // Perform the click operation that opens new window
	
		    // Switch to new window opened
		    for(String winHandle : driver.getWindowHandles()){
		        driver.switchTo().window(winHandle);
		    }
// Fino a qui OK	
 
		    // Richiesta Autorizzazione a collegamento esterno -- Non Prevista prima --
		    if(util.verifyExistence(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[2]"), 150)) {
			    util.objectManager(By.xpath("/html/body/div[1]/div/div/div/div[3]/button[2]"), util.scrollAndClick);
			    
		    }
		    // Gestione password del Portale WEB -- DA PROVARE --
			LoginPageComponent page = new LoginPageComponent(driver);
		    String Certifica = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/certificazione-sfdc?param=bb12e3fd7a19350b0800bd88fa4b201e670f611eb3882cf812eac633c838fec1&idbpm=pv0%3a0a1242bdt";
			page.launchLink(Certifica);
			page.launchLink(Certifica);

 		    By messaggio = By.xpath("//*[@id='j_id0:formId:j_id15']/div/p");
		    if(util.verifyExistence(messaggio, 150)) {
		    	util.FrameSwitcher(messaggio);
		    	//PEC risulta già certificato
		    	if(!util.verifyExistence(By.xpath("//p[contains(text(),'grazie per aver confermato il tuo indirizzo PEC')]"), 60)) throw new Exception("Dopo aver cliccato il link contenuto nella mail non viene visualizzato il messaggio di indirizzo PEC confermato");
		    		
		    }
		    else throw new Exception("Dopo aver cliccato il Link contenuto nella mail non viene correttamente caricata la pagina");
		
		    driver.close();
	
		    driver.switchTo().window(winHandleBefore);
	       
		    util.objectManager(By.xpath("//a[@id='ice-menu-account']"), util.scrollAndClick);
		    util.objectManager(By.xpath("//a[@id='ice-btn-logout']"), util.scrollAndClick);
	       
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
