package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AnnulloOffertaVoltura {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
				SceltaProcessoComponent annullaOfferta = new SceltaProcessoComponent(driver);
				logger.write("click su pulsante 'Annulla' - Start");
				offer.clickComponent(offer.buttonAnnulla);
				logger.write("click su pulsante 'Annulla' - Completed");
				
		
				
				logger.write("verifica esistenza popup 'Confermi di volere annullare l'offerta' e dei suoi pulsanti Chiudi e Conferma - Start");
				offer.verifyComponentExistence(offer.buttonPopupAnnullaConferma);
				logger.write("verifica esistenza popup 'Confermi di volere annullare l'offerta' e dei suoi pulsanti Chiudi e Conferma - Completed");
				
				
				logger.write("click su Conferma - Start");
				offer.clickComponent(offer.buttonPopupAnnullaConferma);
				logger.write("click su Conferma - Completed");
				
				TimeUnit.SECONDS.sleep(5);
				annullaOfferta.sceltaMotivoAnnullamentoRichiesta();
				TimeUnit.SECONDS.sleep(8);
				
				logger.write("click su Conferma Annullamento Richiesta - Start");
				offer.verifyComponentExistence(offer.buttonConfermaAnnullamentoRichiesta);
				offer.clickComponent(offer.buttonConfermaAnnullamentoRichiesta);
				logger.write("click su Conferma Annullamento Richiesta - Completed");
				
				logger.write("click su Chiudì Annullamento Richiesta - Start");
				offer.verifyComponentExistence(offer.buttonChiudiAnnullamentoRichiesta);
				offer.clickComponent(offer.buttonChiudiAnnullamentoRichiesta);
				logger.write("click su Chiudì Annullamento Richiesta - Completed");
			
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
