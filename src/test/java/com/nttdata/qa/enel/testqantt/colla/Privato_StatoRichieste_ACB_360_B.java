package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.StatoRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_StatoRichieste_ACB_360_B {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);	
			StatoRichiesteComponent src = new StatoRichiesteComponent(driver);
			
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Correct visualization of the Home Page with text- Ends");

			logger.write("Click on STATO RICHIESTE and verify - Starts");
			src.clickComponent(src.statoRichieste);
			src.verifyComponentExistence(src.pageTitle);
			src.comprareText(src.pagePath, StatoRichiesteComponent.Path, true);
			src.comprareText(src.subText, StatoRichiesteComponent.SubText, true);
			logger.write("Click on STATO RICHIESTE and verify- Ends");
			
			logger.write("verify tabs on STATO RICHIESTE page - Starts");
			src.verifyComponentExistence(src.contratti);
			src.verifyComponentExistence(src.servizi);
			src.verifyComponentExistence(src.modificheContatore);
			src.verifyComponentExistence(src.mostra);
			logger.write("Cverify tabs on STATO RICHIESTE page - Ends");
			
			logger.write("verify fields on STATO RICHIESTE page - Starts");
			src.verifyComponentExistence(src.openEnergyDigital);
			src.verifyComponentExistence(src.tipo);
			src.verifyComponentExistence(src.tipoValue);
			src.verifyComponentExistence(src.nomeOfferta);
			src.verifyComponentExistence(src.nomeOffertaValue);
			src.verifyComponentExistence(src.pod);
			src.verifyComponentExistence(src.podValue);
			src.verifyComponentExistence(src.dataRichiesta);
			src.verifyComponentExistence(src.aataAttivazione);
			logger.write("verify fields on STATO RICHIESTE page - Ends");
			
			logger.write("verify Inviata on STATO RICHIESTE page - Starts");
			src.verifyComponentExistence(src.inviata);
			logger.write("verify Inviata on STATO RICHIESTE page - Ends");
			
			logger.write("Click on + and verify the text - Starts");
			src.verifyComponentExistence(src.collapseIcon);
			src.clickComponent(src.collapseIcon);
			//src.verifyComponentExistence(src.numeroUtente);
			src.verifyComponentExistence(src.indirizzo);
			src.verifyComponentExistence(src.tipodiattivazione);
			src.verifyComponentExistence(src.descrizionetipodiattivazione);
			src.verifyComponentExistence(src.stato);
			src.verifyComponentExistence(src.descrizionestato);
			src.verifyComponentExistence(src.visualizzacontrattofirmato);
			src.verifyComponentExistence(src.documentazionecontrattuale);
			logger.write("Click on + and verify the text - Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
