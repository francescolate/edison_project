package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabPaginaPrincipaleComponent;
import com.nttdata.qa.enel.components.lightning.RecuperaElementiOrdineComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.components.lightning.RicercaRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

/**
 * @author CapuanoTo Il seguente modulo si occupa della ricerca di un'
 *         Offerta accedendo prima alla sezione Richieste dal menu a tendina
 *         del primo tab disponibile
 */
public class RicercaOffertaDaTab {
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

				AccediTabPaginaPrincipaleComponent tabOfferta= new AccediTabPaginaPrincipaleComponent(driver);
				tabOfferta.openTabOfferta(By.xpath(
						"//span[(text()='Offerte' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]"));

				RicercaOffertaComponent cercaRichieste = new RicercaOffertaComponent(driver);
				cercaRichieste.searchOfferta(prop.getProperty("NUMERO_OFFERTA"));
				
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
