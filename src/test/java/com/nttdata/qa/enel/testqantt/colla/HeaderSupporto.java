package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HeaderSupporto {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
			 HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
			
			logger.write("check sul menu centrale dell'  'hamburger menu' - Start");
			By news_Lists=hamburger.list_of_large_news;
		//	hamburger.clickComponent(By.xpath("//span[@class='icon-menu']"));

		//	hamburger.checkSectionLargeNews(news_Lists);	 
			logger.write("check sul menu centrale dell'  'hamburger menu' - Completed");
			
			logger.write("click link 'SOS luce e gas' nella sezione 'SUPPORTO' e check sul suo link - Start");
		    By sosLuceAndGas_Link=hamburger.sosLuceAndGasLink;
			hamburger.clickComponent(sosLuceAndGas_Link);
			hamburger.checkUrl(prop.getProperty("LINK")+hamburger.linkSOS_LUCE_E_GAS);
			By sosluceegas_Title=hamburger.sosluceegasTitle;
			hamburger.verifyComponentExistence(sosluceegas_Title);
			logger.write("click link 'SOS luce e gas' nella sezione 'SUPPORTO' e check sul suo link - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
