package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.nttdata.qa.enel.util.GmailQuickStart;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangeVoltageGetMail {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			System.out.println("Checking mail message");
			logger.write("Checking mail message - Start");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date mailDate = simpleDateFormat.parse(prop.getProperty("REFERENCE_DATE"));
			String mail = null;
			mail = GmailQuickStart.getMessageBody(GmailQuickStart.getGmailMessage(prop, prop.getProperty("EMAIL_SUBJECT"), mailDate, prop.getProperty("VERIFICATION_STRING")));
			if (mail != null) {
				System.out.println("Mail message: " + mail);
			} else {
				throw new Exception("Impossible to get mail message");
			}
			logger.write("Checking mail message - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
