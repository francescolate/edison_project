package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentaleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

//Il seguente modulo si occupa della creazione di un nuovo Cliente accedendo prima alla sezione Clienti dal menu a tendina del primo tab disponibile 
//Poi cliccando su Nuovo inserirà la tipologia di cliente , e compilerà tutti i campi necessari al completamento della creazione quali nomi, indirizzi, referenti etc..
public class GestioneDocumentaleFibraSWAEvo {

	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			GestioneDocumentaleComponent documentale = new GestioneDocumentaleComponent(driver);
			TimeUnit.SECONDS.sleep(2);
			
			
			if(prop.getProperty("CARICA_DOCUMENTO").contentEquals("SI")) {
				logger.write("Caricamento documento - Start");
				
				//il metodo verifica presenza e in caso negativo recupera pdf dalla folder del progetto
				
				String frame = documentale.caricaDocumentoInDocumentale(Utility.getDocumentPdf(prop));
//				prop.setProperty("frame", frame);
				logger.write("Caricamento documento - Completed");
			}
			
			logger.write("Conferma gestione documentale - Start");
			documentale.confermaDocumentale(documentale.confermaDocumentoButton);
			logger.write("Conferma gestione documentale - Completed");
			prop.setProperty("CARICA_DOCUMENTO", "NO");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
