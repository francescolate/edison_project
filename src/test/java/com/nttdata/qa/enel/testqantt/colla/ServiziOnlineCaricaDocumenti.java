package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerVoltageSectionComponent;
import com.nttdata.qa.enel.components.colla.ContattaciComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ServiziOnlineCaricaDocumentiComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ServiziOnlineCaricaDocumenti {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ServiziOnlineCaricaDocumentiComponent sodc = new ServiziOnlineCaricaDocumentiComponent(driver);
			sodc.comprareText(sodc.text1By, sodc.text1, true);
			sodc.comprareText(sodc.textSub1By, sodc.textSub1, true);
			sodc.comprareText(sodc.text2By, sodc.text2, true);
			sodc.comprareText(sodc.buttonMsg, sodc.buttonText, true);
			
			sodc.clickComponent(sodc.buttonMsg);
			
			logger.write("Check Servizi Online Carica Documenti - START");
			sodc.checkURLAfterRedirection(prop.get("LINK_DOCUMENTI").toString());
			Thread.sleep(5000);
			sodc.verifyItems();
			sodc.comprareText(sodc.informativaTextBy, sodc.informativaText, true);
			logger.write("Check Servizi Online Carica Documenti - END");
	
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
