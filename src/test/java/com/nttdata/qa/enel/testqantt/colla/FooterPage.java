package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class FooterPage {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("Verifica Footer - Start");
			FooterPageComponent footer=new FooterPageComponent(driver);
			By description_object1=footer.before_text;
			By description_object2=footer.after_text;
			footer.checkTextFooter(description_object1, prop.getProperty("BEFORE_FOOTER_TEXT"), description_object2, prop.getProperty("AFTER_FOOTER_TEXT"));
			By linkInformazioniLegali=footer.informazioniLegaliLink;
			footer.verifyComponentExistence(linkInformazioniLegali);
			footer.scrollComponent(linkInformazioniLegali);
			
			By linkCredits=footer.creditsLink;
			footer.verifyComponentExistence(linkCredits);
			
			By linkPrivacy=footer.privacyLink;
			footer.verifyComponentExistence(linkPrivacy);
			
			By linkCookie=footer.cookieLink;
			footer.verifyComponentExistence(linkCookie);
			
			By linkRemit=footer.remitLink;
			footer.verifyComponentExistence(linkRemit);
		    
			By iconTwitter=footer.twitterIcon;
			footer.verifyComponentExistence(iconTwitter);
			
			By iconFacebook=footer.facebookIcon;
			footer.verifyComponentExistence(iconFacebook);
			
			By iconYoutube=footer.youtubeIcon;
			footer.verifyComponentExistence(iconYoutube);
			
			By iconLinkedin=footer.linkedinIcon;
			footer.verifyComponentExistence(iconLinkedin);
			
			footer.verifyComponentExistence(footer.instagramIcon);
			footer.verifyComponentExistence(footer.telegramIcon);
			logger.write("Verifica Footer - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
		

}
