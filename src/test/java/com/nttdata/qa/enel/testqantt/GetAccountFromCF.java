package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModificaDatiContattoBSNComponent;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class GetAccountFromCF {
	@Step("Get account from CF")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			GetAccountFromComponent gafc = new GetAccountFromComponent(driver);
			
			logger.write("typing the user CF - START");	
			gafc.verifyComponentExistence(gafc.searchBar);
			gafc.searchAccount(gafc.searchBar, prop.getProperty("CF"), true);
			logger.write("typing the user CF - COMPLETED");
			
		/*	gafc.clickComponent(gafc.searchBar);
			
			logger.write("checking the data existence - START");	
			gafc.verifyComponentExistence(gafc.accountLink);
			gafc.clickComponent(gafc.accountLink);
			logger.write("checking the data existence - COMPLETE");*/
			Thread.sleep(9000);
			
			/*if(prop.getProperty("EMAIL") != null && !prop.getProperty("EMAIL").equals("")){
				logger.write("checking account email - START");	
				gafc.verifyComponentExistence(By.xpath(gafc.accountEmail.replace("$email$", ModificaDatiContattoBSNComponent.modifiedExistingEmail)));
				logger.write("checking account email - COMPLETE");
			}*/
			logger.write("checking the Item existence - START");
			By itemClick = By.xpath("//table/tbody/tr/th[@scope='row']//a[text()='"+prop.getProperty("TITOLARE")+"']");
		    gafc.verifyComponentExistence(itemClick);
		 	gafc.clickComponent(itemClick);
			logger.write("checking the Item existence - COMPLETE");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}
