package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class LoginEnel_VAIALLAREACLIENTI {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			Thread.sleep(35000);
			log.clickComponent(log.buttonAccetta);
			HamburgerMenuComponent hc = new HamburgerMenuComponent(driver);
			logger.write("Click on hambergur menu - Start");
			Thread.sleep(5000);
			hc.verifyComponentExistence(hc.iconMenu);
			hc.clickComponent(hc.iconMenu);
			logger.write("Click on hambergur menu - Completed");
			
			logger.write("Click on VAI ALL'AREA CLIENTI  - Start");
			hc.verifyComponentExistence(hc.areaClienti);
			hc.clickComponent(hc.areaClienti);
			logger.write("Click on VAI ALL'AREA CLIENTI  - Completed");

			Thread.sleep(20000);
			logger.write("Enter login inputs and click on login - Start");
			log.enterLoginParameters(log.username, prop.getProperty("WP_USERNAME"));
			log.enterLoginParameters(log.password, prop.getProperty("WP_PASSWORD"));
			log.verifyComponentExistence(log.buttonLoginAccedi);
			log.clickComponent(log.buttonLoginAccedi);
			logger.write("Enter login inputs and click on login - Completed");
			
			if (log.verifyComponentExistence(By.xpath("//a[@href='/it/area-clienti/residenziale']"), 15))
				log.clickComponent(By.xpath("//a[@href='/it/area-clienti/residenziale']"));

			Thread.sleep(10000);
			HomeComponent hm = new HomeComponent(driver);
		//	hm.hanldeFullscreenMessage(hm.overLayClose);
			hm.verifyComponentExistence(hm.pageTitle);
			log.VerifyText(hm.pageTitle, hm.PageTitle);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			logger.write("Click on exit - Start");
			hm.clickComponent(hm.exit);
			logger.write("Click on exit - Completed");

			hc.verifyComponentExistence(hc.iconMenu);
			
			String url=driver.getCurrentUrl();
			prop.setProperty("URL", "https://www-coll1.enel.it/it");
			if(url.equalsIgnoreCase(prop.getProperty("URL")))
			
				prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
