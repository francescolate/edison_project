package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFibraMelitaNoRes {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);
			
			paf.baseTimeoutInterval  = 40;
			
			//STEP 7 
			logger.write("click the button Show Offer in the card with FIBRA-MELITA offer - START");
			
			By text = By.xpath("//div[@id='root']//label[@class='no_fibra']");
			
			paf.verifyFrameAvailableAndSwitchToIt(paf.id_frame);
			
			paf.verifyComponentExistence(text);
			paf.verifyComponentText(text, "L'utente non e' di tipo Residenziale (utente business)");
			
			//STEP 8
			By continua = By.xpath(paf.element_by_text_and_parent_id.replace("#1", "CONTINUA").replace("#2", "root"));
			paf.verifyComponentInvisibility(continua);
			
			By esci = By.xpath(paf.element_by_text_and_parent_id.replace("#1", "ESCI").replace("#2", "root"));
			paf.verifyComponentExistence(esci);
			paf.clickComponent(esci);
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
