package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class Verifiche_Order_Creation {
	private final static int SEC = 120;
	
	@Step("Verifiche_Order_Creation")
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

			//Salvo OI ordine
//			prop.setProperty("ID_ORDINE", prop.getProperty("NAME"));
//			logger.write("Salvataggio OI Ordine");
//			//Salvo Id BPM
//			prop.setProperty("ID_BPM", prop.getProperty("ITA_IFM_IDBPM__c"));
//			logger.write("Salvataggio ID BPM");
//			//Salvo valore del bollettino temporaneo
//			prop.setProperty("ID_BOLLETTINO_TEMPORANEO", prop.getProperty("ITA_IFM_Temporary_Billing__c"));
//			logger.write("Salvataggio flag bollettino temporaneo");
//			//Verifico stato UBD
//			if(prop.getProperty("ITA_IFM_UDB_STATUS__C").compareToIgnoreCase("IN ATTESA")!=0){
//				throw new Exception("Lo stato UDB sull'ordine non corrisponde a quello atteso. Valore attuale:"+prop.getProperty("CASE.STATUS")+" valore atteso: IN ATTESA");
//				
//			}
//			logger.write("Verifica stato UBD alla creazione dell'ordine");
			
		}

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
