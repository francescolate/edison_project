package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CreaOffertaVerificaCampiVolturaConAccolloRes {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
				logger.write("check esistenza sui due pulsanti 'Annulla' - Start");
				offer.verificaEsistenzaAnnulla();
				logger.write("check esistenza sui due pulsanti 'Annulla' - Completed");
				
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'Riepilogo Offerta' - Start");
				offer.verificaCampiSezioneRiepilogoOfferta();
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'Riepilogo Offerta' - Completed");
				
				logger.write("verifica esistenza dei campi in sezione 'Commodity' - Start");
				offer.verificaCampiSezioneCommodity();
				logger.write("verifica esistenza dei campi in sezione 'Commodity' - Completed");
				
				logger.write("verifica esistenza della sezione 'Metodo di Pagamento' - Start");
				offer.verifyComponentExistence(offer.sezioneMetodoPagamento);
				logger.write("verifica esistenza della sezione 'Metodo di Pagamento' - Completed");
				
				logger.write("verifica esistenza del campo selezione affianco a ciascun 'Metodo di Pagamento' - Start");
				offer.verifyComponentExistence(offer.selezioneMetodoPagamento);
				logger.write("verifica esistenza del campo selezione affianco a ciascun 'Metodo di Pagamento' - Start");
				

				logger.write("verifica esistenza dei campi e del pulsante 'Verifica' in sezione 'Indirizzo di Fatturazione' - Start");
				offer.verificaCampiIndirizzoFatturazione();
				logger.write("verifica esistenza dei campi e del pulsante 'Verifica' in sezione 'Indirizzo di Fatturazione' - Completed");
				
				logger.write("verifica esistenza dei campi e del pulsante 'Verifica' in sezione 'Indirizzo di Residenza/Sede Legale' - Start");
				offer.verificaCampiIndirizzoResidenza();
				logger.write("verifica esistenza dei campi e del pulsante 'Verifica' in sezione 'Indirizzo di Residenza/Sede Legale' - Completed");
				

				logger.write("verifica esistenza dei campi e del pulsante 'Verifica' in sezione 'Indirizzo di Residenza/Sede Legale' - Start");
				offer.verificaCampiIndirizzoUltimaFattura();
				logger.write("verifica esistenza dei campi e del pulsante 'Verifica' in sezione 'Indirizzo di Residenza/Sede Legale' - Completed");
				
				
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'Fatturazione Elettronica' - Start");
				offer.verificaCampiFatturazioneElettronica();
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'Fatturazione Elettronica' - Completed");
				
				
				logger.write("verifica esistenza sezione 'Carrello' - Start");
				offer.verifyComponentExistence(offer.sezioneCarrello);
				logger.write("verifica esistenza sezione 'Carrello' - Completed");
				
				
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'contatti e consensi' - Start");
				offer.verificaCampiContattieConsensi();
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'contatti e consensi' - Completed");
				
			
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'modalità firma' - Start");
				offer.verificaCampiModalitàFirma();
				logger.write("verifica esistenza dei campi e del pulsante 'Conferma' in sezione 'modalità firma' - Completed");
				
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
