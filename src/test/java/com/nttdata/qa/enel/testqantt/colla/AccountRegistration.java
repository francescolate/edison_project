package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.AccountRegistrationComponent;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AccountRegistration {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		try {
			By accessLoginPage = null;
			
			prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
			
			logger.write("apertura del portale web Enel di test - Start");
			AccountRegistrationComponent log = new AccountRegistrationComponent (driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			Thread.sleep(1000);
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			
			By user = log.username;
			log.verifyComponentExistence(user);
			
			By pw = log.password;
			log.verifyComponentExistence(pw);

			By registratiButton = log.registratiButton;
			log.verifyComponentExistence(registratiButton);
			log.clickComponent(registratiButton); 
			
			log.verifyComponentExistence(log.registrationHeader);
			
			Thread.sleep(5000);
			By cf = null;
			if(prop.getProperty("TIPOCLIENTE").equals("RESIDENZIALE")){
				cf = log.cfInputField;
				log.verifyComponentExistence(log.privatoDivButton);
				log.jsClickObject(log.privatoDivButton);
			}else{
				cf = log.cfAziendaleInputField;
				log.verifyComponentExistence(log.businessDivButton);
				log.jsClickObject(log.businessDivButton);
			}
			Thread.sleep(6000);
			log.jsClickObject(log.continuaButton);
			Thread.sleep(9000);
			
			log.verifyComponentExistence(log.credentialsFormHeader);
			log.fillInputField(log.emailInputField, prop.getProperty("WP_USERNAME"));
			log.fillInputField(log.confirmEmailInputField, prop.getProperty("WP_USERNAME"));
			log.fillInputField(log.passwordInputField, prop.getProperty("WP_PASSWORD"));
			log.fillInputField(log.confirmPasswordInputField, prop.getProperty("WP_PASSWORD"));
			
			Thread.sleep(30000);
			
			log.scrollComponent(cf);
			log.fillInputField(cf, prop.getProperty("CF"));
			log.fillInputField(log.nomeInputField, prop.getProperty("ACCOUNT_FIRSTNAME"));
			log.fillInputField(log.cognomeInputField, prop.getProperty("ACCOUNT_LASTNAME"));
			if(prop.getProperty("TIPOCLIENTE").equals("BUSINESS"))
				log.fillInputField(log.denominazioneInputField, prop.getProperty("COMPANY_NAME"));
			
			log.scrollComponent(log.registratiAllAreaRiservataButton);
			log.jsClickObject(log.informativaPrivacyCheckbox);
			log.jsClickObject(log.condizioniGeneraliCheckbox);
			
			Thread.sleep(10000);
			
			prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
			
			log.clickComponent(log.registratiAllAreaRiservataButton);
			
			log.verifyComponentExistence(log.mobileNumberInputField);
			log.fillInputField(log.mobileNumberInputField, prop.getProperty("MOBILE_NUMBER"));
			log.clickComponent(log.continuaWithOTPButton);
			
			String otp = APIService.getOTP(prop.getProperty("WP_USERNAME"), prop.getProperty("MOBILE_NUMBER"));
			
			log.verifyComponentExistence(log.otpHeader);
			log.fillInputField(log.otpInputField, otp);
			log.clickComponent(log.confermaOTPButton);
			
			log.verifyComponentExistence(log.thanksForRegistering);
			log.clickComponent(log.thanksForRegisteringButton);
			
			Thread.sleep(120000);
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			cealc.restoreAvailableEmailAddresses(cealc.getListOfAvailableEmailAddresses(), prop.getProperty("WP_USERNAME"));
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
