package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.Privato330ACBAddebitoDirettoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_330_ACB_Addebito_Diretto {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			Privato330ACBAddebitoDirettoComponent adc = new Privato330ACBAddebitoDirettoComponent(driver);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if path and heading of homePage is displayed - Start");
			By homePagePath = adc.areaRiservata;
			adc.verifyComponentExistence(homePagePath);
			adc.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			By homepageSubheading = adc.pageHeading;
			adc.verifyComponentExistence(homepageSubheading);
			adc.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			By sectionSubheading = adc.sectionSubheading;
			adc.verifyComponentExistence(sectionSubheading);
			adc.VerifyText(sectionSubheading, prop.getProperty("SECTION_HEADING"));
			
			By AddebitoDirettoLink = adc.AddebitoDiretto_Link;
			adc.verifyComponentExistence(AddebitoDirettoLink);
			adc.clickComponent(AddebitoDirettoLink);
			Thread.sleep(8000);
			
			By addebitoDirettoPath = adc.AddebitoDirettoPath;
			adc.verifyComponentExistence(addebitoDirettoPath);
			adc.VerifyText(addebitoDirettoPath, prop.getProperty("ADDEBITO_DIRETTO_PATH"));
			
			By AddebitoDiretto_PageTitle = adc.AddebitoDiretto_PageTitle;
			adc.verifyComponentExistence(AddebitoDiretto_PageTitle);
			adc.VerifyText(AddebitoDiretto_PageTitle, prop.getProperty("ADDEBITO_DIRETTO_TITLE"));
			
			By addebitoDirettoSubtext = adc.AddebitoDiretto_SubText;
			adc.verifyComponentExistence(addebitoDirettoSubtext);
			adc.VerifyText(addebitoDirettoSubtext, prop.getProperty("ADDEBITO_DIRETTO_SUBTEXT"));
			
			By addebitoDirettoNonActiveStatus = adc.AddebitoDiretto_Status;
			adc.verifyComponentExistence(addebitoDirettoNonActiveStatus);
			adc.VerifyText(addebitoDirettoNonActiveStatus, prop.getProperty("ADDEBITO_DIRETTO_NONACTIVE_STATUS"));
			
			adc.validateSupply(adc.AttivaAddebitoDiretto_Supply);
			
			By AttivaAddebitoDirettoButton = adc.AttivaAddebitoDiretto_button;
			adc.verifyComponentExistence(AttivaAddebitoDirettoButton);
			adc.clickComponent(AttivaAddebitoDirettoButton);
			Thread.sleep(10000);
			
			By attivazioneAddebitoDirettoPath = adc.areaRiservata;
			adc.verifyComponentExistence(attivazioneAddebitoDirettoPath);
			adc.VerifyText(attivazioneAddebitoDirettoPath, prop.getProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_PATH"));
			
			By AttivazioneAddebitoDiretto_Subtext = adc.AttivazioneAddebitoDiretto_Subtext;
			adc.verifyComponentExistence(AttivazioneAddebitoDiretto_Subtext);
			adc.VerifyText(AttivazioneAddebitoDiretto_Subtext, prop.getProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT"));
			
			adc.validateAttivazioneSteps();
			
			logger.write("Verify the color of I_tuoi_dati - Start");
			adc.checkColor(adc.AttivazioneAddebitoDiretto_Num1, adc.serviceColor, "I_tuoi_dati");
			logger.write("Verify the color of I_tuoi_dati - Start");
			
			adc.validateAttivazioneFields();
			
			adc.setAttivazioneFieldValues();
			
			adc.validateAttivazionePrepopulatedFields();
			
			adc.verifyCheckboxnotSelected(adc.IBAN_ESTERO_Checkbox);
			
			By PROSEGUIButton = adc.PROSEGUIButton;
			adc.verifyComponentExistence(PROSEGUIButton);
			adc.clickComponent(PROSEGUIButton);
			Thread.sleep(20000);
			
			By AttivazioneAddebitoDiretto_Subtext2 = adc.AttivazioneAddebitoDiretto_Subtext2;
			adc.verifyComponentExistence(AttivazioneAddebitoDiretto_Subtext2);
			adc.VerifyText(AttivazioneAddebitoDiretto_Subtext2, prop.getProperty("ATTIVAZIONE_ADDEBITO_DIRETTO_SUBTEXT2"));
			
			logger.write("Verify the color of I_tuoi_dati - Start");
//			adc.checkColor(adc.AttivazioneAddebitoDiretto_Num1, adc.servicesColor, "I_tuoi_dati");
			logger.write("Verify the color of I_tuoi_dati - Start");
			
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			adc.checkColor(adc.AttivazioneAddebitoDiretto_Num2, adc.serviceColor, "Riepilogo_e_conferma_dati");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			
			adc.validateAttivazionePostpopulatedFields();

			By SupplyInfo = adc.SupplyInfo;
			adc.verifyComponentExistence(SupplyInfo);
			adc.VerifyText(SupplyInfo, prop.getProperty("SUPPLY_INFO"));
			
			adc.confirmSupply(adc.AttivazioneAddebitoDiretto_ConfirmSupply);
			
			By EmailInfo = adc.EmailInfo;
			adc.verifyComponentExistence(EmailInfo);
			adc.VerifyText(EmailInfo, prop.getProperty("EMAIL_INFO"));
			
			By EmailInputField = adc.EmailInputField;
			adc.verifyComponentExistence(EmailInputField);
			adc.checkPrePopulatedValueAndCompare(adc.emailInputField, adc.emailInputFieldValue);
			
			By CONFERMAButton = adc.CONFERMAButton;
			adc.verifyComponentExistence(CONFERMAButton);
			adc.clickComponent(CONFERMAButton);
			Thread.sleep(7000);
			
			/*logger.write("Verify the color of I_tuoi_dati - Start");
			adc.checkColor(adc.AttivazioneAddebitoDiretto_Num1, adc.servicesColor, "I_tuoi_dati");
			logger.write("Verify the color of I_tuoi_dati - Start");
			
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			adc.checkColor(adc.AttivazioneAddebitoDiretto_Num2, adc.servicesColor, "Riepilogo_e_conferma_dati");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");		
			
			logger.write("Verify the color of Esito - Start");
			adc.checkColor(adc.AttivazioneAddebitoDiretto_Num3, adc.servicesColor, "Esito");
			logger.write("Verify the color of Esito - Start");*/
			
			By RequestInfo = adc.RequestInfo;
			adc.verifyComponentExistence(RequestInfo);
			adc.VerifyText(RequestInfo, adc.requestInfo);
			
			By AttentionInfo = adc.AttentionInfo;
			adc.verifyComponentExistence(AttentionInfo);
			adc.VerifyText(AttentionInfo, adc.attentionInfo);
			
			By FINEButton = adc.FINEButton;
			adc.verifyComponentExistence(FINEButton);
			adc.clickComponent(FINEButton);
			Thread.sleep(7000);
			
			logger.write("Checking if path and heading of homePage is displayed - Start");
			adc.verifyComponentExistence(homePagePath);
			adc.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			adc.verifyComponentExistence(homepageSubheading);
			adc.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			
			adc.verifyComponentExistence(sectionSubheading);
			adc.VerifyText(sectionSubheading, prop.getProperty("SECTION_HEADING"));
			
			prop.setProperty("RETURN_VALUE", "OK");
		
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}
