package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModificaDatiContattoBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_298_DatiDiContato {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			pa.verifyComponentExistence(pa.jmeterLogo);
			logger.write("Click on jmeter logo -- Start");
			pa.clickComponent(pa.jmeterLogo);
			logger.write("Click on jmeter logo -- Completed");
			pa.verifyComponentExistence(pa.datiDiContatto);
			logger.write("Click on dati Di Contatto -- Start");
			pa.clickComponent(pa.datiDiContatto);
			logger.write("Click on dati Di Contatto -- Completed");

			ModificaDatiContattoBSNComponent mc = new ModificaDatiContattoBSNComponent(driver);
			/*logger.write("Verify page navigation and field details-- Start");
			mc.verifyComponentExistence(mc.pageTitle);
			//mc.comprareText(mc.titleSubText, mc.TitleSubText, true);
			//mc.comprareText(mc.path, mc.pagePath, true);
			mc.verifyComponentExistence(mc.tipo);
			mc.verifyComponentExistence(mc.tipoValue);
			mc.verifyComponentExistence(mc.stato);
			mc.verifyComponentExistence(mc.statoValue);
			mc.verifyComponentExistence(mc.numeroCliente);
			mc.verifyComponentExistence(mc.numeroClienteValue);
			mc.verifyComponentExistence(mc.address);
			mc.verifyComponentExistence(mc.addressValue);
			logger.write("Verify page navigation and field details-- Completed");*/

		/*	logger.write("Click on Prosegui and Verify page navigation-- Start");
			mc.clickComponent(mc.prosegui);			
			mc.verifyComponentExistence(mc.pageTitle);
			mc.comprareText(mc.inQuestasezione, mc.InQuestasezione_Text, true);
			logger.write("Click on Prosegui and Verify page navigation-- Completed");*/

//			mc.checkInputFieldValue(mc.titolareInput, prop.getProperty("TITOLARE"));
//		
//			mc.checkInputFieldValue(mc.cfInput, prop.getProperty("CF"));
//			mc.verifyComponentExistence(mc.telefonoInput);
//			
//			mc.checkInputFieldValue(mc.telefonoInputString, prop.getProperty("TELEFONO"));
//		
//			mc.checkInputFieldValue(mc.emailPECInput, prop.getProperty("EMAILPEC"));
//			
//			mc.checkInputFieldValue(mc.emailInput1, prop.getProperty("EMAIL"));
//			mc.comprareText(mc.formText, mc.FormText, true);
//			logger.write("Verify page navigation and field details-- Completed");

			logger.write("Click on Modifica Button and Verify page navigation, field details-- Start");
			mc.verifyComponentExistence(mc.modificaButton);
			mc.clickComponent(mc.modificaButton);
			Thread.sleep(20000);
			mc.verifyComponentExistence(mc.pageTitle);
//			mc.comprareText(mc.titleSubText1, mc.TitleSubText1, true);
//			mc.comprareText(mc.path, mc.pagePath, true);			
			mc.verifyComponentExistence(mc.telefonoInput1);
			mc.verifyComponentExistence(mc.emailInput);
			mc.verifyComponentExistence(By.xpath(mc.emailPECInput));
//			mc.checkInputFieldValue(mc.telefonoInputString1, "0818890127");
//			mc.checkInputFieldValue(mc.emailPECInput, prop.getProperty("EMAILPEC"));
//			mc.checkInputFieldValue(mc.emailInputString, prop.getProperty("EMAIL"));
//			logger.write("Verify page navigation and field details-- Completed");
			mc.verifyComponentExistence(mc.esciButton);
			mc.verifyComponentExistence(mc.proseguiButton);
			logger.write("Click on Modifica Button and Verify page navigation, field details-- Start");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
