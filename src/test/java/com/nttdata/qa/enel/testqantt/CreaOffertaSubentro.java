package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CreaOffertaSubentro {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);


            logger.write("click su pulsante Crea offerta e apertura pagina 'Riepilogo offerta' - Start");
            insert.verifyComponentExistence(insert.buttonCreaOfferta);
            insert.clickComponent(insert.buttonCreaOfferta);
            gestione.checkSpinnersSFDC();
            logger.write("click su pulsante Crea offerta e apertura pagina 'Riepilogo offerta' - Completed");


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//				return;
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;

        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }

}
