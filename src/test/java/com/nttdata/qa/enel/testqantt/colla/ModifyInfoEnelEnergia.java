package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyInfoEnelEnergia {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Accessing servizi forniture page");
			logger.write("Accessing servizi forniture page - Start");
			InfoEnelEnergiaComponent infoEE = new InfoEnelEnergiaComponent(driver);
			InfoEnelEnergiaMyComponent iee = new InfoEnelEnergiaMyComponent(driver);
			infoEE.accessServiziForniture();
			logger.write("Accessing servizi forniture page - Completed");
			
			System.out.println("Accessing InfoEnelEnergia");
			logger.write("Accessing InfoEnelEnergia - Start");
			//infoEE.verifyComponentVisibility(infoEE.infoEnelEnergiaBtn);
			//infoEE.clickComponent(infoEE.infoEnelEnergiaBtn);
			iee.clickComponent(iee.infoEnergiaIcon);
			logger.write("Accessing InfoEnelEnergia - Completed");
			
			System.out.println("Modify InfoEnelEnergia");
			logger.write("Modify InfoEnelEnergia - Start");
			infoEE.verifyComponentVisibility(infoEE.modInfoEnelEnergiaBtn);
			infoEE.clickComponent(infoEE.modInfoEnelEnergiaBtn);
			Thread.sleep(30000);
			logger.write("Modify InfoEnelEnergia - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
