package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DetaglioFornitura_GestiscileOreFree67_Component;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioFornituraComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeDettaglioFornituraFasceGiornaliere {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreeDettaglioFornituraComponent ofdf = new OreFreeDettaglioFornituraComponent(driver);
			logger.write("premere il tasto 'Gestisci le Ore Free' nella Card della fornitura Ore Free - Start");
			ofdf.verifyComponentExistence(ofdf.oreFreeButton);
			ofdf.clickComponent(ofdf.oreFreeButton);
			logger.write("premere il tasto 'Gestisci le Ore Free' nella Card della fornitura Ore Free - Completed");
			
			logger.write("andare nella sezione 'Fasce Giornaliere', premere sul pulsante MODIFICA relativo al giorno che si intende modificare, sulla data del "+prop.getProperty("DAY_TO_BE_ADDED")+"° giorno rispetto alla data odierna e cambiare la fascia oraria impostando 20:00 - 23:00 - Start");
			ofdf.verifyComponentExistence(ofdf.ripristinaFasciaBaseFasceGiornaliereButton);
			
			ofdf.verifyComponentExistence(ofdf.fasceGiornaliereHeader);
			//ofdf.comprareText(ofdf.fasceGiornaliereHeader, ofdf.fasceGiornaliereHeaderText, true);
			Thread.sleep(10000);
			//ofdf.clickModificaComponentByDate(ofdf.modificaFasciaGiornalieraButton, Integer.parseInt(prop.getProperty("DAY_TO_BE_ADDED")));
			//ofdf.clickComponent(ofdf.modificaFasciaGiornalieraButtonNew);
			ofdf.scrollComponent(ofdf.modificaFasciaGiornalieraButtonNew);
			ofdf.verifyComponentExistence(ofdf.modificaFasciaGiornalieraButtonNew);
			ofdf.jsClickObject(ofdf.modificaFasciaGiornalieraButtonNew);
			
			/*Thread.sleep(20000);
			ofdf.verifyComponentExistence(ofdf.alleOreSelect);
			ofdf.verifyComponentExistence(ofdf.dalleOreSelect);
			
			Robot robot = new Robot();
			int counter = 0;
			
			while(!ofdf.checkNodeValue(ofdf.dalleOreDailySelectValue, prop.getProperty("ORE_FREE_ORARIO_DI_START")) && counter < 15){

				ofdf.clickComponent(ofdf.dalleOreDailySelect);
				
				robot.keyPress(KeyEvent.VK_DOWN);
				
				robot.delay(50);
				
				robot.keyRelease(KeyEvent.VK_DOWN);
				
				robot.delay(50);
				
				robot.keyPress(KeyEvent.VK_ENTER);
				
				robot.delay(50);
				
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(500);
				counter++;
			}	
			
			//ofdf.comprareText(ofdf.alleOreSelect, "23:00", true);

			logger.write("andare nella sezione 'Fasce Giornaliere', premere sul pulsante MODIFICA relativo al giorno che si intende modificare, sulla data del "+prop.getProperty("DAY_TO_BE_ADDED")+"° giorno rispetto alla data odierna e cambiare la fascia oraria impostando 20:00 - 23:00 - Completed");
			
			logger.write("cliccare su Conferma e poi su Chiudi - Start");
			
			ofdf.clickComponent(ofdf.confermaButton);
			OreFreeDettaglioComponent of = new OreFreeDettaglioComponent(driver);
			//of.comprareText(of.titoloPopup, "La tua fascia di Ore Free è stata impostata correttamente per il giorno scelto!", true);
			//of.comprareText(of.testoPopup, "Ricordati che puoi cambiare ogni volta che vuoi la tua fascia giornaliera/base e che puoi farlo, grazie alla skill di Enel Energia per Alexa, anche semplicemente con la tua voce.", true);
			//of.comprareText(of.titoloPopup, "Operazione non riuscita!", true);
			//of.comprareText(of.titoloPopup, "La tua fascia di Ore Free è stata impostata correttamente per il giorno scelto!", true);
			//of.comprareText(of.testoPopup, "Siamo spiacenti ma non è stato possibile impostare la nuova fascia di Ore Free. Riprova più tardi. Ti ricordiamo che sarà comunque garantita l'ultima fascia in vigore.", true);
			//of.comprareText(of.testoPopup, "Ricordati che puoi cambiare ogni volta che vuoi la tua fascia giornaliera/base e che puoi farlo, grazie alla skill di Enel Energia per Alexa, anche semplicemente con la tua voce.", true);
			
			ofdf.clickComponent(ofdf.chiudiButton);
			logger.write("cliccare su Conferma e poi su Chiudi - Completed");
			if(prop.getProperty("CONFERMA_ANNULLA").equals("ANNULLA")){
				Thread.sleep(2000);
				ofdf.jsClickObject(ofdf.ripristinaFasciaBaseButton);
				ofdf.comprareText(ofdf.ripristinaFasciaBasePopupHeader, ofdf.ripristinaFasciaBasePopupHeaderText, true);
				ofdf.jsClickObject(ofdf.ripristinaFasciaBasePopupAnnullaButton);
				//ofdf.verifyModifiedComponentExistence(ofdf.fasciaGiornalieraModificata);
			}
			
			else{
				Thread.sleep(2000);
				ofdf.jsClickObject(ofdf.ripristinaFasciaBaseButton);
				ofdf.comprareText(ofdf.ripristinaFasciaBasePopupHeader, ofdf.ripristinaFasciaBasePopupHeaderText, true);
				ofdf.jsClickObject(ofdf.ripristinaFasciaBasePopupConfermaButton);
				//ofdf.jsClickObject(ofdf.fasciaBaseRipristinataPopupCloseButton);
			}
			
			Thread.sleep(2000);
			//ofdf.verifyComponentExistence(ofdf.chiudiButton);
			//ofdf.jsClickObject(ofdf.chiudiButton);
			*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
















