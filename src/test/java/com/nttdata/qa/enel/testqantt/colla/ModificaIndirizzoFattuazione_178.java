package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.AreaRiservataHomePageComponent;
import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazione_178 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
			AreaRiservataHomePageComponent home = new AreaRiservataHomePageComponent(driver);
			
			prop.setProperty("SUPPLY_SERVICE", "Addebito Diretto Bolletta Web Opzione Green Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione Autolettura InfoEnelEnergia Opzione Bolletta Rateizzazione ");
			prop.setProperty("SUPPLY_DETAIL_HEADING", "La tua fornitura nel dettaglio.");
		    prop.setProperty("BILL_DETAILS", "Le tue bollette");      
		    prop.setProperty("HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("HOMEPAGE_TITLE2", "HOMEPAGE");
		    prop.setProperty("HOMEPAGE_TITLE_DESCRIPTION", "Benvenuto nella tua area dedicata Business");
		    prop.setProperty("SUPPLY_BILL_TITLE", "Forniture e bollette");
			logger.write("Home Page Logo, Title and Description Verification- Starts");
			home.verifyComponentExistence(home.homePageLogo);
			home.VerifyText(home.homePageTitle1,prop.getProperty("HOMEPAGE_TITLE1"));
			home.VerifyText(home.homePageTitle2,prop.getProperty("HOMEPAGE_TITLE2"));
			home.VerifyText(home.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"));
			logger.write("Home Page Log,Title and Description Verification- Ends");
			
			logger.write("Verifying the list of devices after clicking on Servizi  STARTS");
			mif.clickComponent(mif.serivzi);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.SERVIZI_PATH, mif.serviziPath);
			
			mif.comprareText(mif.serviziTitle, ModificaIndirizzoFattuazioneComponent.SERVIZI_TITLE, true);
			mif.checkForSupplyServices(prop.getProperty("SUPPLY_SERVICE"));
			mif.comprareText(mif.serviziTitle, ModificaIndirizzoFattuazioneComponent.SERVIZI_TITLE, true);
			mif.comprareText(mif.serviziText, ModificaIndirizzoFattuazioneComponent.SERVIZI_TEXT, true);
			logger.write("Verifying the list of devices after clicking on Servizi  ENDS");
			
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-STARTS");
			mif.clickComponent(mif.modificaIndirizzodiFatturazione);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.INDIRIZZO_PATH, mif.indirizzoDiFatturazionePath);
			mif.comprareText(mif.indirizzoDiFatturazioneTitle, ModificaIndirizzoFattuazioneComponent.INDIRIZZO_TITLE, true);
			mif.comprareText(mif.sectionData, ModificaIndirizzoFattuazioneComponent.SECTION_DATA, true);
			mif.comprareText(mif.sectionBillingAddress, ModificaIndirizzoFattuazioneComponent.SECTION_BILLING_ADDRESS, true);
			mif.comprareText(mif.sectionTechnicalSpec, ModificaIndirizzoFattuazioneComponent.SECTION_TECH_SPECIFICATION, true);
			mif.comprareText(mif.sectionContract, ModificaIndirizzoFattuazioneComponent.SECTION_CONTRACT, true);
			//mif.comprareText(mif.clientNumber, prop.getProperty("CLIENT_NUMBER"), true);
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-ENDS");
			
			logger.write("Click on modifica button and enter the mandatory fields -STARTS");
			mif.clickComponent(mif.modificaButton);
			Thread.sleep(30000);
			
			
			/*mif.verifyComponentExistence(mif.indirizziGiaVerificati);
			mif.verifyComponentExistence(mif.provincia177);
			mif.verifyComponentExistence(mif.comune177);
			mif.verifyComponentExistence(mif.localita177);
			mif.verifyComponentExistence(mif.indrizzo177);
			mif.verifyComponentExistence(mif.numerocivico177);
			mif.clickComponent(mif.indirizziGiaVerificati);
			mif.clickComponent(mif.addressOption1);
			logger.write("Click on modifica button and enter the mandatory fields -ENDS");
			logger.write("Verify Esci, Indietro and prosegui button- Starts");
			mif.verifyComponentExistence(mif.esciButton);
			mif.verifyComponentExistence(mif.indietroButton);
			mif.verifyComponentExistence(mif.proseguiButton);
			logger.write("Verify Esci, Indietro and prosegui button- Ends");
			
			logger.write("CLick on Prosegi button and verify the data in the page navigation-Starts");
			mif.clickComponent(mif.proseguiButton);
			mif.verifyComponentExistence(mif.riepilogoeConfermaDati);
			mif.verifyFeildAttribute(mif.provinciaxpath, prop.getProperty("PROVINCE"));
			//mif.verifyFeildAttribute(mif.comunexpath, prop.getProperty("COMUNE"));
			//mif.verifyFeildAttribute(mif.indrizzoxpath, prop.getProperty("INDIRIZZO"));
			//mif.verifyFeildAttribute(mif.numerocivicoxpath, prop.getProperty("NUMERO_CIVICO"));
			//mif.verifyFeildAttribute(mif.localitaxpath, prop.getProperty("LOCALITA"));
			//mif.verifyFeildAttribute(mif.capxpath, prop.getProperty("CAP"));
			logger.write("CLick on Prosegui button and verify the data in the page navigation-Ends");
			logger.write("Verify Esci, Indietro and prosegui button- Starts");
			mif.verifyComponentExistence(mif.esciButton);
			mif.verifyComponentExistence(mif.indietroButton);
			mif.verifyComponentExistence(mif.proseguiButton);
			logger.write("Verify Esci, Indietro and prosegui button- Ends");	
			
			logger.write("CLick on Indietro button and verify the data in the page navigation-Starts");
			mif.clickComponent(mif.indietroButton);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.INDIRIZZO_PATH, mif.indirizzoDiFatturazionePath);
			mif.comprareText(mif.indirizzoDiFatturazioneTitle, ModificaIndirizzoFattuazioneComponent.INDIRIZZO_TITLE, true);
			mif.comprareText(mif.sectionData, ModificaIndirizzoFattuazioneComponent.SECTION_DATA, true);
			mif.comprareText(mif.sectionBillingAddress, ModificaIndirizzoFattuazioneComponent.SECTION_BILLING_ADDRESS, true);
			mif.comprareText(mif.sectionTechnicalSpec, ModificaIndirizzoFattuazioneComponent.SECTION_TECH_SPECIFICATION, true);
			mif.comprareText(mif.sectionContract, ModificaIndirizzoFattuazioneComponent.SECTION_CONTRACT, true);
			//mif.comprareText(mif.clientNumber, prop.getProperty("CLIENT_NUMBER"), true);
			mif.verifyComponentExistence(mif.indirizzofornitura);
			mif.comprareText(mif.indirizzoFornituraValue, ModificaIndirizzoFattuazioneComponent.INDIRIZZO_FORNITURA, true);
			logger.write("CLick on Indietro button and verify the data in the page navigation-Ends");	
			*/
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
