package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.CustomerDetailsPageComponent;
import com.nttdata.qa.enel.components.lightning.DettagliComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetails_ACB_301 {
	 public static void main(String[] args) throws Exception {

         
         Properties prop = null;
         prop = WebDriverManager.getPropertiesIstance(args[0]);
         QANTTLogger logger = new QANTTLogger(prop);
         RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
        
         try
         {
        	CustomerDetailsPageComponent cc = new CustomerDetailsPageComponent(driver);
        	Thread.sleep(5000);
//        	cc.jsClickObject(cc.richestaHeader);
        	cc.clickRichesta(cc.richestaHeader);
        	 
        	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
 			LocalDateTime now = LocalDateTime.now();
 			cc.verifyComponentExistence(By.xpath(cc.request301.replace("$DATE$", dtf.format(now))));
 			logger.write("Click Request number  -- Start");	
//			cc.jsClickObject(By.xpath(cc.request301.replace("$DATE$", dtf.format(now))));
 			cc.clickComponent(By.xpath(cc.request301.replace("$DATE$", dtf.format(now))));
 			logger.write("Click Request number  -- Completed");	
			Thread.sleep(5000);
			
			CaseItemDetailsComponent cidc = new CaseItemDetailsComponent(driver);
			cidc.verifyComponentExistence(cidc.richestaHeader);
			cidc.verifyComponentExistence(cidc.variazioneAnagrafica);
			cidc.verifyComponentExistence(cidc.dettagli);
			cidc.clickDettagli(cidc.dettagli);
			
			DettagliComponent dc = new DettagliComponent(driver);
			dc.verifyComponentExistence(dc.web);
			dc.verifyComponentExistence(dc.stato);
			dc.verifyComponentExistence(dc.statoSecondario);
			dc.verifyComponentExistence(By.xpath(dc.date.replace("$DATE$", dtf.format(now))));
			
        	 prop.setProperty("RETURN_VALUE", "OK");
       		
         } catch (Exception e) {
 			prop.setProperty("RETURN_VALUE", "KO");
 			StringWriter errors = new StringWriter();
 			e.printStackTrace(new PrintWriter(errors));
 			errors.toString();
 			logger.write("ERROR_DESCRIPTION: " + errors.toString());

 			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
 			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
 				throw e;

 		} finally {
 			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
 		}
    
     }
       
}
