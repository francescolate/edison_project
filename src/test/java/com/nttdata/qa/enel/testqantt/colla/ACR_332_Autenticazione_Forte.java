package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.ContinueWithRegistrationComponent;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_332_Autenticazione_Forte {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		try {
			By accessLoginPage = null;

			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");

			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");

			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);

			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon);
			logger.write("click su icona utente - Completed");

			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);

			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));

			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi);

			try {
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Start");
				log.verifyComponentExistence(log.areaClientiCondizioni);
				log.clickComponent(log.areaClientiCondizioni);
				log.verifyComponentExistence(log.areaClientiPrivacy);
				log.clickComponent(log.areaClientiPrivacy);
				log.verifyComponentExistence(log.terminiECondizioniButton);
				log.clickComponent(log.terminiECondizioniButton);
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Completed");
			} catch (Exception e) {
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Pagina inesistente. Proseguo.");
			}

			try {
				if (!prop.getProperty("AREA_CLIENTI").equals("")) {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
					if (prop.getProperty("AREA_CLIENTI").equals("CASA")) {
						log.verifyComponentExistence(log.areaClientiCasa);
						log.clickComponent(log.areaClientiCasa);
					} else {
						log.verifyComponentExistence(log.areaClientiImpresa);
						log.clickComponent(log.areaClientiImpresa);
					}
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
				} else {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
				}
			} catch (Exception e) {
				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
			}

			ContinueWithRegistrationComponent cwrc = new ContinueWithRegistrationComponent(driver);
			HomeComponent hm = new HomeComponent(driver);
			logger.write("Verify Promo accesso page content -- Start");
			cwrc.verifyComponentExistence(cwrc.autenticazione);
			cwrc.verifyComponentExistence(cwrc.primoAccessoHeader);
			cwrc.verifyComponentExistence(cwrc.primoAccessoMenu1);
			cwrc.verifyComponentExistence(cwrc.primoAccessoMenu2);
			cwrc.verifyComponentExistence(cwrc.primoAccessoMenu3);

			cwrc.verifyComponentExistence(cwrc.enelpremiaWoW);
			cwrc.verifyComponentExistence(cwrc.Supporto);
			cwrc.verifyComponentExistence(cwrc.trovaSpazioEnel);
			cwrc.verifyComponentExistence(cwrc.esci);

			cwrc.verifyComponentExistence(cwrc.numeroClientiTitle);
			cwrc.verifyComponentExistence(cwrc.numeroClienteLabel);
			// cwrc.verifyComponentExistence(cwrc.campiobbligatori);
			// cwrc.verifyComponentExistence(cwrc.iIcon);
			cwrc.verifyComponentExistence(cwrc.numeroClienteInputField);
			cwrc.verifyComponentExistence(cwrc.numeroContrattoLabel);
			cwrc.verifyComponentExistence(cwrc.numeroContrattoInputField);
			cwrc.verifyComponentExistence(cwrc.solouno);
			logger.write("Verify Promo accesso page content -- Ends");

			logger.write("Click on i icon and verify dialoge content -- Start");
			cwrc.clickComponent(cwrc.numeroClienteiIcon);
			cwrc.verifyComponentExistence(cwrc.dialogTitle);
			cwrc.comprareText(cwrc.dialogText, cwrc.DialogTextNext, true);
			cwrc.clickComponent(cwrc.dialogeCloseNew);
			logger.write("Click on i icon and verify dialoge content -- Completed");

			logger.write("Click on i icon and verify dialoge content -- Start");
			cwrc.clickComponent(cwrc.NumeroContrattoiIcon);
			cwrc.verifyComponentExistence(cwrc.dialogTitle);
			cwrc.comprareText(cwrc.dialogText2, cwrc.DialogTextcontratto, true);
			cwrc.clickComponent(cwrc.dialogeCloseNew2);
			logger.write("Click on i icon and verify dialoge content -- Completed");

			cwrc.verifyComponentExistence(cwrc.autenticazione);
			cwrc.verifyComponentExistence(cwrc.primoAccessoHeader);
			cwrc.verifyComponentExistence(cwrc.primoAccessoMenu1);
			cwrc.verifyComponentExistence(cwrc.primoAccessoMenu2);
			cwrc.verifyComponentExistence(cwrc.primoAccessoMenu3);

			logger.write("Enter supply number and click on continue,verify success message -- Start");
			cwrc.clickComponent(cwrc.numeroClienteContinuaButton);
			cwrc.clickComponent(cwrc.numeroClientiError);

			// hm.checkFieldDisplay(cwrc.confermaTitle);
			cwrc.fillInputField(cwrc.numeroClienteInputField, prop.getProperty("SUPPLY_NUMBER"));
			cwrc.verifyComponentExistence(cwrc.numeroClienteContinuaButton);
			cwrc.clickComponent(cwrc.numeroClienteContinuaButton);

			cwrc.verifyComponentExistence(cwrc.confermaTitle);
			cwrc.verifyComponentExistence(cwrc.numeroClienteConfermaButton);
			cwrc.verifyComponentExistence(cwrc.indietroButton);
			cwrc.clickComponent(cwrc.numeroClienteConfermaButton);

			cwrc.verifyComponentExistence(cwrc.numeroClienteFineButton);
			cwrc.verifyComponentExistence(cwrc.operazioneTitleNew);
			cwrc.comprareText(cwrc.successText, cwrc.SuccessText, true);
			cwrc.clickComponent(cwrc.numeroClienteFineButton);
			Thread.sleep(5000);
			logger.write("Enter supply number and click on continue,verify success message -- Completed");

			hm.verifyComponentExistence(hm.pageTitleRes);
			logger.write("Verifi home page title and details -- Start");
			// hm.comprareText(hm.pageTitleRes, hm.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, hm.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, hm.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			cealc.restoreAvailableEmailAddresses(cealc.getListOfAvailableEmailAddresses(),
					prop.getProperty("WP_USERNAME"));

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

			// prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
