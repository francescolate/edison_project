package com.nttdata.qa.enel.testqantt.colla;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateBollette {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			/*
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("LINK"));
			Thread.sleep(5000);
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
			
			logger.write("check the username and passsword login - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("USERNAME")); 

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
			logger.write("check the username and passsword login - Completed");
			
			logger.write("Click on the login button- Start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			By accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
			logger.write("Verify the page after successful login - Complete");
			
			*/
			
			SuplyPrivateDetailComponent  checkbox = new SuplyPrivateDetailComponent(driver);
			
			
			logger.write("Verify and click on Bollette - Start");
			checkbox.verifyComponentExistence(checkbox.BolletteLink);
			checkbox.clickComponent(checkbox.BolletteLink);
			logger.write("Verify and click on Bollette - Complete");
			
//			logger.write("Verify and click on VisualizzaTutte link - Start");
//			checkbox.verifyComponentExistence(checkbox.VisualizzaTutte);
//			checkbox.clickComponent(checkbox.VisualizzaTutte);
//			logger.write("Verify and click on VisualizzaTutte link - Complete");
			
			logger.write("Verify and click on MostraFiltri link - Start");
			checkbox.verifyComponentExistence(checkbox.MostraFiltri);
			checkbox.clickComponent(checkbox.MostraFiltri);
			logger.write("Verify and click on MostraFiltri link - Complete");
			
				
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.DaPagareCheckbox);
			checkbox.verifyComponentExistence(checkbox.DaPagareLabel);
			checkbox.verifyCheckboxSelect(checkbox.DaPagareCheckbox);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.DaPagareLabel);
			checkbox.verifyCheckboxnotSelect(checkbox.DaPagareCheckboxStatus);
			logger.write("deselect the checkbox and verify the status - Complete");
			
				logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.InPagamentocheckbox);
			checkbox.verifyComponentExistence(checkbox.InPagamentolabel);
			checkbox.verifyCheckboxSelect(checkbox.InPagamentocheckbox);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.InPagamentolabel);
			checkbox.verifyCheckboxnotSelect(checkbox.InPagamentocheckbox);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.PagatoCheckbox);
			checkbox.verifyComponentExistence(checkbox.PagatoLabel);
			checkbox.verifyCheckboxSelect(checkbox.PagatoCheckbox);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.PagatoLabel);
			checkbox.verifyCheckboxnotSelect(checkbox.PagatoCheckbox);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.BolletaCheckbox);
			checkbox.verifyComponentExistence(checkbox.BolletaLabel);
			checkbox.verifyCheckboxSelect(checkbox.BolletaCheckbox);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.BolletaLabel);
			checkbox.verifyCheckboxnotSelect(checkbox.BolletaCheckbox);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			logger.write("Verify the Documento label - Start");
			checkbox.verifyComponentExistence(checkbox.TipodiDocumentoLabel);
			logger.write("Verify the Documento label - Complete");
			
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.CheckBox2020);
			checkbox.verifyComponentExistence(checkbox.Label2020);
			checkbox.verifyCheckboxSelect(checkbox.CheckBox2020);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.Label2020);
			checkbox.verifyCheckboxnotSelect(checkbox.CheckBox2020);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.CheckBox2019);
			checkbox.verifyComponentExistence(checkbox.Label2019);
			checkbox.verifyCheckboxSelect(checkbox.CheckBox2019);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.Label2019);
			checkbox.verifyCheckboxnotSelect(checkbox.CheckBox2019);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.CheckBox2018);
			checkbox.verifyComponentExistence(checkbox.Label2018);
			checkbox.verifyCheckboxSelect(checkbox.CheckBox2018);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.Label2018);
			checkbox.verifyCheckboxnotSelect(checkbox.CheckBox2018);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.LuceCheckbox);
			checkbox.verifyComponentExistence(checkbox.LuceLabel);
			checkbox.verifyCheckboxSelect(checkbox.LuceCheckbox);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.LuceLabel);
			checkbox.verifyCheckboxnotSelect(checkbox.LuceCheckbox);
			logger.write("deselect the checkbox and verify the status - Complete");
				
			logger.write("Verify the checkbox is enabled - Start");
			checkbox.verifyComponentExistence(checkbox.GasCheckbox);
			checkbox.verifyComponentExistence(checkbox.GasLabel);
			checkbox.verifyCheckboxSelect(checkbox.GasCheckbox);
			logger.write("Verify the checkbox is enabled - Complete");
			
			logger.write("deselect the checkbox and verify the status - Start");
			checkbox.clickComponent(checkbox.GasLabel);
			checkbox.verifyCheckboxnotSelect(checkbox.GasCheckbox);
			logger.write("deselect the checkbox and verify the status - Complete");
			
			logger.write("Verify and click on Reimposta button - Start");			
			checkbox.verifyComponentExistence(checkbox.ReimpostaButton);
			checkbox.clickComponent(checkbox.ReimpostaButton);
			logger.write("Verify and click on Reimposta button - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}


