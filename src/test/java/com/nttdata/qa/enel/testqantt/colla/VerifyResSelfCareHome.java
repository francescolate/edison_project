package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SelfCareSelectionComponent;
import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.ResSelfCareHomePageComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyResSelfCareHome {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			boolean clientIsLikeRes = Boolean.parseBoolean(prop.getProperty("VERIFY_SC_SELECTION_PAGE", "false"));
			if (clientIsLikeRes) {
				//System.out.println("Verifying self care page selection");
				logger.write("Verifying self care page selection - Start");
				SelfCareSelectionComponent selectionPage = new SelfCareSelectionComponent(driver);
				
				if (selectionPage.verifyExistence(selectionPage.businessSelfCareLabel, 5)){
				

				
				selectionPage.verifyElementsStrings();
				By resSelfCareButton = selectionPage.homeSelfCareBtn;
				selectionPage.verifyComponentVisibility(resSelfCareButton);
				selectionPage.clickComponent(resSelfCareButton);
				//selectionPage.clickComponent(selectionPage.privatoBtn);
				//selectionPage.clickComponent(selectionPage.continuaBtn);
				logger.write("Verifying self care page selection - Completed");
				}
			}
			
			//System.out.println("Verifying item: Enel logo");
			logger.write("Verifying item: Enel logo - Start");
			LeftMenuComponent leftMenu = new LeftMenuComponent(driver);
			By enelLogo = leftMenu.enelLogo;
			leftMenu.verifyComponentExistence(enelLogo);
			logger.write("Verifying item: Enel logo - Completed");
			
			//Verifying left menu items
			//System.out.println("Verifying menu items strings");
			logger.write("Verifying menu items strings - Start");
			leftMenu.verifyMenuItems(clientIsLikeRes);
			logger.write("Verifying menu items strings - Completed");
			
			//System.out.println("Verifying text in res selfcare home page");
			logger.write("Verifying text in res selfcare home page - Start");
			ResSelfCareHomePageComponent homePage = new ResSelfCareHomePageComponent(driver);
			homePage.checkElementsText();
			//Load bsn selfcare homepage
			boolean shouldLoadBsnSelfcare = Boolean.parseBoolean(prop.getProperty("LOAD_BSN_SC_PAGE", "false"));
			if (shouldLoadBsnSelfcare) {
				By bsnPageSelector = leftMenu.locateMenuItem(prop.getProperty("RES_LEFT_MENU_BSN"));
				driver.findElement(bsnPageSelector).click();
			}
			logger.write("Verifying text in res selfcare home page - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		} 
	}
	
}
