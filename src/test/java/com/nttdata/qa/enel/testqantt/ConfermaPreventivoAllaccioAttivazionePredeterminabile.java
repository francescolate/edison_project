package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestionePreventivoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConfermaPreventivoAllaccioAttivazionePredeterminabile {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				GestionePreventivoComponent preventivo = new GestionePreventivoComponent(driver);
				
				TimeUnit.SECONDS.sleep(180);

				//Click su link Gestione preventivo se viene caricata la pagina di riepilogo ordine
				preventivo.apriGestionePreventivoDaRiepilogoOrdini(preventivo.tabellaRiepilogoOrdini,preventivo.linkGestionePreventivo,0);
				
				//Click su gestione preventivo se presente
				String classificazionePreventivo=prop.getProperty("CLASSIFICAZIONE_PREVENTIVO");
				
				//Verifica classificazione preventivo
				preventivo.verificaDettaglioPreventivoPredeterminabile(preventivo.testoDettaglioPreventivo, preventivo.dettaglioPreventivoPredeterminabile);
				
				logger.write("Verifica sezione Dettaglio Preventivo");
				
				//Verifica sezione Dati Richiesta
				preventivo.verificaCampiDatiRichiesta("Preventivo ricevuto", classificazionePreventivo, "NO");
				
				logger.write("Verifica sezione Dati Richiesta");
				
				//Verifica sezione Preventivo Allaccio e Attivazione
				preventivo.verificaCampiPreventivoAllaccioAttivazionePredeterminabile();
				logger.write("Verifica sezione Preventivo Allaccio e Attivazione");
				
				//Inserimento Modalità Invio Preventivo
				logger.write("Selezione Modalità Invio Preventivo");
				preventivo.selezionaValore("Canale Invio Richiesta", prop.getProperty("MODALITA_INVIO_PREVENTIVO"));
				
				//Inserimento Modalità Firma Preventivo
				preventivo.selezionaValore("Modalità Firma Preventivo", prop.getProperty("MODALITA_FIRMA_PREVENTIVO"));
				logger.write("Selezione Modalità Firma Preventivo");
				
				//Conferma
				preventivo.pressAndCheckSpinners(preventivo.confermaPreventivo);
				logger.write("Click button Conferma");
				

			}
            prop.setProperty("RETURN_VALUE", "OK");
        }
        catch (Exception e) 
        {
                       prop.setProperty("RETURN_VALUE", "KO");
                       StringWriter errors = new StringWriter();
                       e.printStackTrace(new PrintWriter(errors));
                       errors.toString();

                       prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
                      if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
                       //Store WebDriver Info in properties file
                       prop.store(new FileOutputStream(args[0]), "Set TestObject Info");                               
        }
	}

}
