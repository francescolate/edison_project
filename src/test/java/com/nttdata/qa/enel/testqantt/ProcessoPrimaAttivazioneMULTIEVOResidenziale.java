package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoPrimaAttivazioneMULTIEVOResidenziale {

	@Step("Processo Switch Attivo EVO")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
				
				logger.write("Inserimento Pod per verifica - Start");
			
				PrecheckComponent forniture = new PrecheckComponent(driver);
				CompilaIndirizziComponent pod = new CompilaIndirizziComponent(driver);
				GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
						driver);
				
				forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD_1"));
				forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
				forniture.pressAndCheckSpinners(forniture.precheckButton2);
				logger.write("Inserimento Pod per verifica - Completed");
				logger.write("Inserimento e Validazione Indirizzo - Start");
				
				// Inserimento e Validazione Indirizzo
				if(Costanti.statusUbiest.compareTo("ON")==0){
					pod.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					pod.verificaIndirizzo(pod.buttonVerifica);
					logger.write("Inserimento e Validazione Indirizzo - Completed");
				}
				else if(Costanti.statusUbiest.compareTo("OFF")==0){
					logger.write("Inserimento Indirizzo Forzato - Start");
					pod.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					pod.verificaIndirizzo(pod.buttonVerifica);
					pod.forzaIndirizzo(prop.getProperty("CAP"));
					logger.write("Inserimento Indirizzo Forzato  - Completed");
					TimeUnit.SECONDS.sleep(5);
				}
				
				
				logger.write("Inserimento dati misuratore - Start");
				
				compila.compileVoltageInfoEVO(prop.getProperty("TIPO_MISURATORE"), prop.getProperty("TENSIONE_CONSEGNA"), prop.getProperty("POTENZA_CONTRATTUALE"));
				logger.write("click su pulsante AGGIUNGI FORNITURA - Start");
				
				compila.clickComponentWithJseAndCheckSpinners(compila.button_aggiungi_fornitura);
				logger.write("click su pulsante AGGIUNGI FORNITURA - Completed");
				
				logger.write("inserisci POD e CAP per la seconda fornitura ele e click su 'Esegui precheck' - Start");
				
				forniture.insertTextByChar(forniture.insertPOD, prop.getProperty("POD_2"));
				forniture.insertTextByChar(forniture.insertCAP, prop.getProperty("CAP"));
				forniture.pressAndCheckSpinners(forniture.precheckButton2);
				logger.write("inserisci POD e CAP per la seconda fornitura ele e click su 'Esegui precheck' - Completed");
				
				
				logger.write("Inserimento e Validazione Indirizzo - Start");
				// Inserimento e Validazione Indirizzo
				
				if(Costanti.statusUbiest.compareTo("ON")==0){
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				logger.write("Inserimento e Validazione Indirizzo - Completed");
				TimeUnit.SECONDS.sleep(5);
				}
				else if(Costanti.statusUbiest.compareTo("OFF")==0){
					logger.write("Inserimento Indirizzo Forzato - Start");
					pod.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
					pod.verificaIndirizzo(pod.buttonVerifica);
					pod.forzaIndirizzo(prop.getProperty("CAP"));
					logger.write("Inserimento Indirizzo Forzato  - Completed");
					TimeUnit.SECONDS.sleep(5);
				}
				logger.write("Compilazione sezione Punto Fornitura - Start");
				
				compila.compileVoltageInfoEVO(prop.getProperty("TIPO_MISURATORE"), prop.getProperty("TENSIONE_CONSEGNA"), prop.getProperty("POTENZA_CONTRATTUALE"));
				logger.write("Compilazione sezione Punto Fornitura - Completed");
								
				logger.write("Verifica esito Offertabilita - Start");						
				forniture.verificaEsitoOffertabilita2(prop.getProperty("ESITO_OFFERTABILITA"),prop.getProperty("POD_1"));
				forniture.verificaEsitoOffertabilita2(prop.getProperty("ESITO_OFFERTABILITA"),prop.getProperty("POD_2"));
				logger.write("Verifica esito Offertabilita - Completed");
		
	
				logger.write("Click su Conferma e Crea Offerta - Start");
				  compila.press(compila.confermaButtonPrimaAttivazione);
				    TimeUnit.SECONDS.sleep(5);
					compila.clickComponentWithJseAndCheckSpinners(compila.creaOffertaMainButton);
				logger.write("Click su Conferma e Crea Offerta - Completed");
				
				//Inserimento 29/10
				TimeUnit.SECONDS.sleep(15);
								
				logger.write("Inserimento dati misuratore - Completed");
				TimeUnit.SECONDS.sleep(15);
				
				logger.write("Selezione Uso Fornitura - Start");
				compila.selezionaUsoFornitura(prop.getProperty("USO_FORNITURA"));
				compila.clickComponent(compila.buttonConfermaSelezioneUsoFornitura);
				logger.write("Selezione Uso Fornitura - Completed");
				
				
				
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
