package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;

import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ConfiguraProdottoMULTIGASNonResidenziale {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfiguraProdottoComponent configura = new ConfiguraProdottoComponent(driver);
			

			
			logger.write("Configura prodotto elettrico "+prop.getProperty("PRODOTTO_GAS")+"- Start");
			configura.press(configura.linkGasNonResidenziale);
			configura.selezionaProdotto(prop.getProperty("PRODOTTO_GAS"));
			
			logger.write("Selezione del prodotto Elettrico");
			if (!prop.getProperty("OPZIONE_KAM_AGCOR","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR Elettrico");
			}
			if (!prop.getProperty("SCELTA_ABBONAMENTI_GAS","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO_GAS"),prop.getProperty("SCELTA_ABBONAMENTI_GAS"));
				logger.write("Selezione Scelta Abbonamenti");
			}
			
			if (!prop.getProperty("OPZIONE_VAS_GAS","").equals("")){
				configura.configuraVas(prop.getProperty("PRODOTTO_GAS"),prop.getProperty("OPZIONE_VAS_GAS"));
				logger.write("Selezione VAS Elettrico");
			}
			if (!prop.getProperty("SCONTO_GAS","").equals("")){
				configura.configuraSconto(prop.getProperty("PRODOTTO_GAS"),prop.getProperty("SCONTO_GAS"));
				logger.write("Selezione Sconto Elettrico");
			}
			
			// Selezione del prodotto nel TAB del Carrello
			if (prop.getProperty("TAB_SGEGLI_TU_ELE","").equals("SI")){
				configura.ConfiguraProdottoTab(prop.getProperty("PRODOTTO_TAB_GAS"),prop.getProperty("PIANO_TARIFFARIO_GAS"));
				logger.write("Selezione il Prodotto nel TAB del carrello");
			}
			if (!prop.getProperty("PIANO_TARIFFARIO_GAS","").equals("")){
				configura.ConfiguraPianoTariffario(prop.getProperty("PIANO_TARIFFARIO_GAS"));
				logger.write("Selezione Piano Tariffario");
			}
			
			configura.salvaConfigurazione();
			logger.write("Salva Configurazione Elettrico");

			logger.write("Configura prodotto elettrico "+prop.getProperty("PRODOTTO_GAS")+"- Completed");
			
			configura.svuotaRicerca();
			logger.write("Svuota Ricerca");
			
			TimeUnit.SECONDS.sleep(5);
			
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO_GAS")+"- Start");
			configura.press(configura.linkGasNonResidenziale);
			configura.selezionaProdotto(prop.getProperty("PRODOTTO_GAS"));
			logger.write("Selezione del prodotto Gas");
			if (!prop.getProperty("OPZIONE_KAM_AGCOR","").equals("")){
				configura.configuraOpzioneKAM(prop.getProperty("OPZIONE_KAM_AGCOR"));
				logger.write("Selezione opzione KAM OPZIONE KAM_AGCOR Gas");
			}
			if (!prop.getProperty("SCELTA_ABBONAMENTI_GAS","").equals("")){
				configura.configuraSceltaAbbonamenti(prop.getProperty("PRODOTTO_GAS"),prop.getProperty("SCELTA_ABBONAMENTI_GAS"));
				logger.write("Selezione Scelta Abbonamenti");
			}
			
			if (!prop.getProperty("OPZIONE_VAS_GAS","").equals("")){
				configura.configuraVas(prop.getProperty("PRODOTTO_GAS"),prop.getProperty("OPZIONE_VAS_GAS"));
				logger.write("Selezione VAS Gas");
			}
			
			if (!prop.getProperty("VAS_EX_GAS","").equals("")){
				//configura.configuraVasEx(prop.getProperty("VAS_EX_GAS"));
				logger.write("Selezione VAS Gas");
			}
			
			if (!prop.getProperty("SCONTO_GAS","").equals("")){
				configura.configuraSconto(prop.getProperty("PRODOTTO_GAS"),prop.getProperty("SCONTO_GAS"));
				logger.write("Selezione Sconto Gas");
			}
			configura.salvaConfigurazione();
			logger.write("Salva Configurazione Gas");
			
			logger.write("Configura prodotto gas "+prop.getProperty("PRODOTTO_GAS")+"- Completed");
			
			configura.checkOut();
			logger.write("Check out carrello");

			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}

