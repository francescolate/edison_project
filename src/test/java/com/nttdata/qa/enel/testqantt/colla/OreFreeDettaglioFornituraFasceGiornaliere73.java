package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.OreFreeDettaglioFornituraComponent;
import com.nttdata.qa.enel.components.colla.OreFreecardComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeDettaglioFornituraFasceGiornaliere73 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreeDettaglioFornituraComponent ofdf = new OreFreeDettaglioFornituraComponent(driver);
			OreFreecardComponent ofc = new OreFreecardComponent(driver);
			logger.write("premere il tasto 'Gestisci le Ore Free' nella Card della fornitura Ore Free - Start");
			ofdf.verifyComponentExistence(ofdf.oreFreeButton);
			ofdf.clickComponent(ofdf.oreFreeButton);
			logger.write("premere il tasto 'Gestisci le Ore Free' nella Card della fornitura Ore Free - Completed");
			
			logger.write("andare nella sezione 'Fasce Giornaliere', premere sul pulsante MODIFICA relativo al giorno che si intende modificare, sulla data del "+prop.getProperty("DAY_TO_BE_ADDED")+"° giorno rispetto alla data odierna e cambiare la fascia oraria impostando 20:00 - 23:00 - Start");
			ofdf.verifyComponentExistence(ofdf.ripristinaFasciaBaseFasceGiornaliereButton);
			
			ofdf.verifyComponentExistence(ofdf.fasceGiornaliereHeader);
			ofdf.comprareText(ofdf.fasceGiornaliereHeader, OreFreeDettaglioFornituraComponent.fasceGiornaliereHeaderText, true);
			
			logger.write("Verification of Free hours before clicking on modifica Button- Start");
			ofc.verifyComponentExistence(ofc.dayOreFree);
			ofc.comprareText(ofc.dayOreFree, OreFreecardComponent.FASCIA_ORE_FREE_TEXT, true);
			logger.write("Verification of Free hours before clicking on modifica Button- Ends");
			
			Thread.sleep(10000);
			ofdf.clickModificaComponentByDate(ofdf.modificaFasciaGiornalieraButton, Integer.parseInt(prop.getProperty("DAY_TO_BE_ADDED")));
			
			ofdf.verifyComponentExistence(ofdf.alleOreSelect);
			ofdf.verifyComponentExistence(ofdf.dalleOreSelect);
			
			Robot robot = new Robot();
			int counter = 0;
			
			while(!ofdf.checkNodeValue(ofdf.dalleOreDailySelectValue, prop.getProperty("ORE_FREE_ORARIO_DI_START")) && counter < 15){
				ofdf.clickComponent(ofdf.dalleOreDailySelect);
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(500);
				counter++;
			}	
			
		//	ofdf.comprareText(ofdf.alleOreSelect, "14:00", true);
			logger.write("andare nella sezione 'Fasce Giornaliere', premere sul pulsante MODIFICA relativo al giorno che si intende modificare, sulla data del "+prop.getProperty("DAY_TO_BE_ADDED")+"° giorno rispetto alla data odierna e cambiare la fascia oraria impostando 20:00 - 23:00 - Completed");
			
			logger.write("Click on Annulla Button - Start");
			Thread.sleep(5000);
			ofdf.clickComponent(ofdf.alleOreSelect);
			ofdf.clickComponent(ofdf.annullaButton);
			logger.write("Click on Annulla Button - End");
			
			logger.write("Verification of Free hours after clicking on Annulla Button- Start");
			ofc.verifyComponentExistence(ofc.dayOreFree);
			ofc.comprareText(ofc.dayOreFree, OreFreecardComponent.FASCIA_ORE_FREE_TEXT, true);
			logger.write("Verification of Free hours after clicking on Annulla Button- End");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}




























































