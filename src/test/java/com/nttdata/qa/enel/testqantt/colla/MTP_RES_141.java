package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class MTP_RES_141 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
											
			home.verifyComponentExistence(home.enelLogo);
								
			home.clickComponent(home.detaglioFurnitura);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			
			logger.write("Check For page title-- start");
			
			dc.verifyComponentExistence(dc.pageTitle);
			
			logger.write("Check For page title-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			dc.verifyComponentExistence(dc.serviziPerleforniture);
			
			Thread.sleep(10000);
			
			dc.verifyComponentExistence(dc.modificaPotenzo);
			
			dc.jsClickObject(dc.modificaPotenzo);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.ModificaSubText, true);
			
			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.TitleSubText_141, true);			
			
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");
			
			changePV.comprareText(changePV.formLine1, changePV.FormLine1, true);
			
			logger.write("Check for Power Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.powerLable);
			
			logger.write("Check for Power Lable -- Completed");
			
			logger.write("Check for Default Power value -- Start");
			
			changePV.checkForDefaultValue(changePV.powerValue,prop.getProperty("POWERDEFAULT"));
			
			logger.write("Check for Default Power value -- Completed");
			
			logger.write("Check for Voltage Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.voltageLable);
			
			logger.write("Check for Voltage Lable -- Completed");
			
			logger.write("Check for Default Voltage value -- Start");
			
			changePV.checkForDefaultValue(changePV.voltageValue,prop.getProperty("VOLTAGEFAULT"));
			
			logger.write("Check for Default Voltage value -- Completed");
			
			changePV.comprareText(changePV.formLine2, changePV.FormLine2, true);
			
			changePV.checkButtonIsEnable(changePV.radioNo);
			
			changePV.comprareText(changePV.formLine3, changePV.FormLine3, true);

			changePV.checkFieldValue(changePV.emailInput, prop.getProperty("WP_USERNAME"));
			
			changePV.clickComponent(changePV.calculateQuote);
			changePV.comprareText(changePV.defaultPVError, changePV.PVErrorMsg, true);
			changePV.clickComponent(changePV.radioYes);
			logger.write("Click on radio button Yes  -- Start");
			
			changePV.clickComponent(changePV.radioYes);
			
			logger.write("Click on radio button Yes  -- Completed");
			
			logger.write("Check for lift type lable  -- Start");

			changePV.verifyComponentExistence(changePV.liftTypeLabel);
			
			logger.write("Check for lift type lable  -- Completed");

			changePV.verifyComponentExistence(changePV.runningCurrentLable);
			
			changePV.verifyComponentExistence(changePV.suggestedPowerLable);

			logger.write("Check for Placeholder text  -- Start");

			changePV.checkForPlaceholderText(changePV.runningCurrentInput,prop.getProperty("PLACEHOLDER"));
			
			logger.write("Check for Placeholder text  -- Completed");
			
			logger.write("Click on i icon  -- Start");
			
			changePV.clickComponent(changePV.iIcon);
			
			logger.write("Click on i icon  -- Completed");
			
			changePV.checkForPopup();

			logger.write("Click on pop up close  -- Start");
			
			changePV.clickComponent(changePV.popupClose);

			logger.write("Click on pop up close  -- Completed");
			
			changePV.checkForLiftTypeDropDownValue();
			
			changePV.clickComponent(changePV.calculateQuote);
			changePV.comprareText(changePV.liftTypeError, changePV.LiftTypeErrorMsg, true);
			changePV.changeDropDownValue(changePV.liftTypeDropdown,prop.getProperty("ARGANO"));
			
			logger.write("Check For starting current label  -- Start");
			
			changePV.verifyComponentExistence(changePV.startingCurrentLable);
			
			logger.write("Check For starting current label  -- Completed");

			changePV.checkForPlaceholderText(changePV.startingCurrentInput, prop.getProperty("PLACEHOLDER"));
			
			logger.write("Check running current error message  -- Start");

			changePV.enterInputtoField(changePV.startingCurrentInput,prop.getProperty("STARTINGCURRENTINPUT"));
			changePV.clickComponent(changePV.calculateQuote);
			changePV.comprareText(changePV.correnteaRegimeError, changePV.correnteaRegimeErrorMsg, true);

			logger.write("Check running current error message  -- Completed");
			
			logger.write("Check starting current error message  -- start");

			changePV.clearFieldValue(changePV.startingCurrentInput);
			changePV.enterInputtoField(changePV.runningCurrentInput,prop.getProperty("RUNNINGCURRENTINPUT"));
			changePV.clickComponent(changePV.calculateQuote);
			changePV.comprareText(changePV.correntediavviamentoError, changePV.CorrentediavviamentoErrorMsg, true);
			
			logger.write("Check starting current error message  -- Completed");

			logger.write("Change lift type drop down to Oleodinamico  -- Start");

			changePV.changeDropDownValue(changePV.liftTypeDropdown, prop.getProperty("OLEODINAMICO"));
			
			logger.write("Change lift type drop down to Oleodinamico  -- Completed");
			
			changePV.checkFieldDisplay(changePV.runningCurrentLable);
			
			changePV.checkForPowerDropDownValue();
			
			changePV.checkForVoltageDropDownValue();
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
