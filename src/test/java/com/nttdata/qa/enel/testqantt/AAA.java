package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class AAA {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
			
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
		
				gestione.selezionaLigtheningValue("Mercato di riferimento prodotto", prop.getProperty("MERCATO"));
				Thread.currentThread().sleep(5000);
				if(prop.getProperty("FLAG_CLIENTE_PRODUTTORE").equals("Y")) {
					gestione.press(gestione.clienteProduttoreFlag);
					   gestione.selezionaLigtheningValue("Tipologia Produttore", prop.getProperty("TIPOLOGIA_PRODUTTORE"));
                   gestione.clickComponent(gestione.genericConfermaButton);
                   if(prop.getProperty("TIPO_UTENZA").contentEquals("S2S")) {
                	   if(!gestione.getElementExistance(By.xpath("//p[text()='Non sei abilitato ad eseguire operazioni sui Clienti Produttori. Invita il cliente a recarsi presso un Punto Enel.']"))) {
                		   throw new Exception("Utilizzando una utenza S2S e selezionato la Tipologia Produttore non viene mostrato l'alert di mancata abilitazione ad eseguire su clienti produttori.");
                	   }
                   }
				}
				
				
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
