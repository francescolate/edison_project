package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.gargoylesoftware.htmlunit.javascript.configuration.ClassConfiguration.ConstantInfo;
import com.nttdata.qa.enel.components.colla.GetUserDetailFromWorkBenchComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyRequestDetail_260 {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
						
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			GetUserDetailFromWorkBenchComponent gc = new GetUserDetailFromWorkBenchComponent(driver);
			
			logger.write("Navigando sul workbench - START");
			wbc.launchLink(prop.getProperty("WB_LINK"));
			logger.write("Navigando sul workbench - COMPLETED");
			
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();
			
			logger.write("Inserendo la query - START");
			String query =Costanti.get_query_260A.replace("$Account_Id$", prop.getProperty("ACCOUNT_ID"));
			query = query.replace("$email$", prop.getProperty("EMAIL_INPUT"));
			wbc.insertQuery(query);
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			
			Thread.sleep(5000);
			/*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			System.out.println(now+"Date");*/
			String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            System.out.println(date);
            date = date+"T00:00:00.00Z";
            System.out.println(date);
		//	gc.comprareText(gc.date_260, date, true);
			gc.comprareText(gc.email_260, prop.getProperty("EMAIL_INPUT"), true);
		//	gc.comprareText(gc.cellulare_260, gc.Cellulare_260, true);
		//	gc.comprareText(gc.staus_260, gc.Status_260, true);
			gc.comprareText(gc.CF_260, prop.getProperty("CF"), true);
			
			logger.write("Inserendo la query - START");
			wbc.insertQuery(Costanti.get_query_260B.replace("$Account_Id$", prop.getProperty("ACCOUNT_ID")));
			wbc.pressButton(wbc.submitQuery);
			logger.write("Inserendo la query - COMPLETED");
			gc.comprareText(gc.status_260B, gc.Status_260B, true);
			gc.comprareText(gc.subStatus_260B, gc.SubStatus_260B, true);
			gc.comprareText(gc.email_260B, prop.getProperty("EMAIL_INPUT"), true);
			gc.comprareText(gc.operationType_260B, gc.OperationTYpe_260B, true);

			String Id = driver.findElement(gc.Id_260B).getText();
			System.out.println(Id);
			wbc.insertQuery(Costanti.get_query_260C.replace("$Id$", Id));
			wbc.pressButton(wbc.submitQuery);

			gc.comprareText(gc.AccountType_260C, gc.AccountType260C, true);
			gc.comprareText(gc.Subject_260C, gc.Subject260C, true);
			gc.comprareText(gc.POD_260C, gc.POD260C, true);
			gc.comprareText(gc.R2DStatus_260C, gc.Status_260C, true);
			gc.comprareText(gc.R2RStatus_260C, gc.Status_260C, true);
			gc.comprareText(gc.SAPStatus_260C, gc.Status_260C, true);
			gc.comprareText(gc.SAPFICAStatus_260C, gc.Status_260C, true);
			gc.comprareText(gc.SAPMM_260C, gc.Status_260C, true);
			gc.comprareText(gc.SAPSD_260C, gc.Status_260C, true);
			gc.comprareText(gc.SEMPRE_260C, gc.Status_260C, true);
			gc.comprareText(gc.UDBStatus_260C, gc.Status_260C, true);
			gc.comprareText(gc.NECStatus_260C, gc.NECStatus260C, true);
			
			Thread.sleep(5000);
			prop.store(new FileOutputStream(args[0]), null);
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			logger.write("Prendo i risultati - COMPLETED");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
