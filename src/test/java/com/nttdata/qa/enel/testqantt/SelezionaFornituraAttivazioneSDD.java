package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SelezionaFornituraAttivazioneSDD {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);

			
			
			logger.write("Check e Selezione fornitura per attivazione SDD - Start");


			//Thread.sleep(60000); 

			By sezione_fornitura=forn.sezioneSelezioneFornituraSdd;
			forn.verifyComponentExistence(sezione_fornitura);



			By campo_CercaFornitura=forn.campoCercaFornituraSdd;
			forn.verifyComponentExistence(campo_CercaFornitura);

			forn.scrollComponent(forn.pagina);

			TimeUnit.SECONDS.sleep(1);
			logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Completed");


			logger.write("inserire nel campo 'Cerca Fornitura' il POD - Start");
			TimeUnit.SECONDS.sleep(1);
			forn.enterPodValue(campo_CercaFornitura, prop.getProperty("POD"));
			TimeUnit.SECONDS.sleep(3);
			logger.write("inserire nel campo 'Cerca Fornitura' il POD - Completed");

			logger.write("verifica colonne tabella forniture - Start");
			forn.VerificaColonneTabellaFornitureAttivazioneSDD(forn.colonneAttivazioneSDD);
			forn.scrollComponent(forn.pagina);
			logger.write("verifica colonne tabella forniture - Completed");

			logger.write("Selezione Fornitura - Start");
			forn.selezionaFornituraAttivazioneSdd(prop.getProperty("POD"));
			logger.write("Selezione Fornitura - Completed");
			

			logger.write("verifica ESITO COMPATIBILITA' per fornitura selezionata - valore atteso  'Attivabile' - Start");

			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(5);
			forn.checkEsitCompatibilitaSDD(prop.getProperty("ESITO_COMPATIBILITA"));
			logger.write("verifica ESITO COMPATIBILITA' - Completed");


			logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Start");
			forn.verifyComponentExistence(forn.buttonConfermaSelezioneFornituraAbilitato);
			forn.clickComponent(forn.buttonConfermaSelezioneFornituraAbilitato);
			logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Completed");
			TimeUnit.SECONDS.sleep(2);
			logger.write("click sul pulsante 'Conferma' - Start");
			forn.verifyComponentExistence(forn.buttonConferma);
			forn.clickComponent(forn.buttonConferma);
			logger.write("click sul pulsante 'Conferma' - Completed");
			TimeUnit.SECONDS.sleep(2);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(2);



/*
			logger.write("click sul pulsante 'Crea offerta' - Start");
			forn.verifyComponentExistence(forn.buttonCreaOfferta);
			forn.clickComponent(forn.buttonCreaOfferta);


			logger.write("se compare la popup 'Proposta Voltura Dual' click su Si, poi su 'Conferma Selezione Fornitura', 'Conferma' e 'Crea Offerta' - Start");
			forn.clickPopupIfExist(forn.popupSi);
			logger.write("se compare la popup 'Proposta Voltura Dual' click su Si, poi su 'Conferma Selezione Fornitura', 'Conferma' e 'Crea Offerta' - Completed");
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);

			logger.write("click sul pulsante 'Crea offerta' - Completed");*/



			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}

}
