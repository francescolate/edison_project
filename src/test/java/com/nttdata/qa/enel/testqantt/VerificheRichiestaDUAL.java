package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificheRichiestaDUAL {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    driver.switchTo().defaultContent();
		    TimeUnit.SECONDS.sleep(15);
            driver.navigate().refresh();
		    VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
		    logger.write("Recupero valore id richiesta - Start");
		    verifica.pressJavascript(verifica.elementiRichiestaSection);
//		    System.out.println("Recupero valore id richiesta ELE");
//			System.out.println("Recupero OI richiesta ELE");
		    String idELE = verifica.recuperaOIRichiestaDaPod(prop.getProperty("POD_ELE"));
	        prop.setProperty("OI_RICHIESTA_ELE", idELE);
//			System.out.println("Recupero OI richiesta ELE: "+prop.getProperty("OI_RICHIESTA_ELE"));
	        logger.write("Recupero valore id richiesta ELE - Completed");
	        
	        logger.write("Verifica Stati Richiesta ELE");
//			System.out.println("Verifica Stati Richiesta ELE - Start");
	        verifica.verificaStatiRichiestaConPod(prop.getProperty("STATO_RICHIESTA"), prop.getProperty("STATO_R2D"), prop.getProperty("STATO_SAP"), prop.getProperty("STATO_SEMPRE"),prop.getProperty("POD_ELE"));
//			System.out.println("Verifica Stati Richiesta ELE - Completed");
			logger.write("Verifica Stati Richiesta ELE - Completed");
	        
//		    System.out.println("Recupero valore id richiesta GAS");
//			System.out.println("Recupero OI richiesta GAS");
		    String idGAS = verifica.recuperaOIRichiestaDaPod(prop.getProperty("POD_GAS"));
	        prop.setProperty("OI_RICHIESTA_GAS", idGAS);
			System.out.println("Recupero OI richiesta ELE: "+prop.getProperty("OI_RICHIESTA_GAS"));
	        logger.write("Recupero valore id richiesta ELE - Completed");
	        
	        
	        logger.write("Verifica Stati Richiesta ELE");
//			System.out.println("Verifica Stati Richiesta ELE - Start");
	        verifica.verificaStatiRichiestaConPod(prop.getProperty("STATO_RICHIESTA"), prop.getProperty("STATO_R2D"), prop.getProperty("STATO_SAP"), prop.getProperty("STATO_SEMPRE"),prop.getProperty("POD_ELE"));
//			System.out.println("Verifica Stati Richiesta ELE - Completed");
			logger.write("Verifica Stati Richiesta ELE - Completed");
			
			if(!prop.getProperty("SOTTOSTATO_RICHIESTA", "").equals("")) {
				verifica.verificaSottoStatiRichiesta(prop.getProperty("SOTTOSTATO_RICHIESTA"));
			}
	        
	        prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
