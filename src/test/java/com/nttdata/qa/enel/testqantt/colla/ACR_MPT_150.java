package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PowerAndVoltageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_MPT_150 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("apertura del portale web Enel di test - Start");
			PowerAndVoltageComponent pavc = new PowerAndVoltageComponent(driver);
			
			logger.write("Verifying the customer Page-- Start");
			pavc.comprareText(pavc.areaPrivataHeading, PowerAndVoltageComponent.AREA_PRAIVATA_HEADING, true);
			pavc.comprareText(pavc.leFornitureHeading, PowerAndVoltageComponent.LE_FORNITURE_HEADING, true);
			logger.write("Verifying the customer Page-- Ends");
			
			logger.write("Clicking on the Dettaglio Fornitura button in electrical supply card & verifying the page navigation-- Start");
			String electricitySupplyDetailPageUrl = prop.getProperty("WP_LINK")+"/it/area-clienti/residenziale/dettaglio_fornitura";
			pavc.clickComponent(pavc.dettaglioFornituraButton);
			pavc.checkURLAfterRedirection(electricitySupplyDetailPageUrl);
			logger.write("Clicking on the Dettaglio Fornitura button in electrical supply card & verifying the page navigation-- Ends");
			
			
			logger.write("Clicking on the Modifica potenca e/o Tenzione & verifying the page navigation-- Start");
			pavc.verifyComponentExistence(pavc.serviziPerLeFornitureHeading);
			pavc.clickComponent(pavc.modificaPotenzaTensioneTile);
			
			pavc.verifyComponentExistence(pavc.modificaPotenzaPage);
			pavc.verifyComponentExistence(pavc.modificaPotenzaButton);
			pavc.clickComponent(pavc.modificaPotenzaButton);
			
			Thread.sleep(10000);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTensioneTileHeading);
			pavc.comprareText(pavc.modificaPotenzaTensioneTileHeading, PowerAndVoltageComponent.MODIFICA_POTENZA_TENZIONE_TILE_HEADING, true);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTenzioneTitle);
			pavc.comprareText(pavc.modificaPotenzaTenzioneTitle, PowerAndVoltageComponent.MODIFICA_POTENZA_TENZIONE_TITLE, true);
			logger.write("Clicking on the Modifica potenca e/o Tenzione & verifying the page navigation-- Ends");
			
			logger.write("Clicking on the Radio Button-- Start");
			pavc.clickComponent(pavc.radioButton);
			logger.write("Clicking on the Radio Button-- Ends");
			
			logger.write("Clicking on the Modifica Potenza e/o Tenzione-- Start");
			pavc.verifyComponentExistence(pavc.modificaPotenzaTenzioneButton2);
			pavc.clickComponent(pavc.modificaPotenzaTenzioneButton2);
			logger.write("Clicking on the Modifica Potenza e/o Tenzione-- Ends");
			
			logger.write("Verifying the data on clicking Modifica Potenza e/o Tenzione button-- Start");
			pavc.verifyComponentExistence(pavc.titleMPT);
			pavc.comprareText(pavc.titleMPT, PowerAndVoltageComponent.TITLE_MPT, true);
			pavc.comprareText(pavc.subTitleMPT, PowerAndVoltageComponent.SUBTITLE_MPT, true);
			pavc.checkForoptionsAvailable(prop.getProperty("OPTIONS"));
			pavc.verifyComponentExistence(pavc.powerandVoltageDataEntryHeading);
			pavc.comprareText(pavc.powerandVoltageDataEntryHeading, PowerAndVoltageComponent.POVER_VOLTAGE_DATAENTRY_HEADING, true);
			
			pavc.comprareText(pavc.potenzalabel, PowerAndVoltageComponent.POTENZA_LABEL, true);
			pavc.validateDropDownValue(pavc.potenzaValue, PowerAndVoltageComponent.POTENZA_VALUE);
			pavc.comprareText(pavc.tensionelabel, PowerAndVoltageComponent.TENZIONE_LABEL, true);
			pavc.validateDropDownValue(pavc.tensioneValue, PowerAndVoltageComponent.TENZIONE_VALUE);
			pavc.verifyComponentExistence(pavc.textNextToTenzione);
			pavc.comprareText(pavc.textNextToTenzione, PowerAndVoltageComponent.TEXT_NEXT_TENZIONE, true);
			
			pavc.verifyComponentExistence(pavc.noRadioButton);
			pavc.isRadioButtonEnabled(pavc.noRadioButton);
			
			pavc.verifyComponentExistence(pavc.modificaPotenzaTensioneTileHeading3);
			pavc.comprareText(pavc.modificaPotenzaTensioneTileHeading3, PowerAndVoltageComponent.MODIFICA_TENZIONE_HEADING3, true);
			
			pavc.verifyComponentExistence(pavc.emailLabel);
			pavc.comprareText(pavc.emailLabel, PowerAndVoltageComponent.EMAIL_LABEL, true);
			pavc.verifyFeildAttribute(pavc.emailValue, PowerAndVoltageComponent.EMAIL_VALUE, pavc.xpathEmail);
			
			pavc.verifyComponentExistence(pavc.cellulareLabel);
			pavc.comprareText(pavc.cellulareLabel, PowerAndVoltageComponent.CELLULARE_LABEL, true);
			//pavc.verifyFeildAttribute(pavc.cellulareValue, PowerAndVoltageComponent.CELLULARE_VALUE, pavc.xpathCellulare);
			logger.write("Verifying the data on clicking Modifica Potenza e/o Tenzione button-- Ends");
			
			logger.write("Modify the dropdown value of Potenza*  field with Inserimento manuale -- Start");
			pavc.changeDropDownValue(pavc.potenzaValue, prop.getProperty("POTENZA"));
			logger.write("Modify the dropdown value of Potenza*  field with Inserimento manuale -- Ends");
			
			logger.write("Verify the Potenza Desiderata field display -- Start");
			pavc.verifyComponentExistence(pavc.potenzaDesiderataLabel);
			pavc.comprareText(pavc.potenzaDesiderataLabel, PowerAndVoltageComponent.POTENZA_DESIDERATA, true);
			logger.write("Verify the Potenza Desiderata field display -- Ends");
			
			logger.write("Enter 50 in the Potenza Desiderata field and verify the text -- Start");
			pavc.enterInputtoField(pavc.potenzaDesiderataValue, prop.getProperty("POTENZA_DESIDERATA"));
			pavc.pressTABKey(pavc.potenzaDesiderataValue);
			
			pavc.verifyComponentExistence(pavc.warningfor50KW);
			pavc.comprareText(pavc.warningfor50KW, PowerAndVoltageComponent.WARNING_TEXT_FOR50KW, true);
			logger.write("Enter 50 in the Potenza Desiderata field and verify the text -- Ends");
			
			logger.write("Enter 999 in the Potenza Desiderata field and verify only till 99 can be entered -- Start");
			pavc.enterInputtoField(pavc.potenzaDesiderataValue, prop.getProperty("POTENZA_DESIREDATA_MAX"));
			pavc.verifyFeildAttribute(pavc.potenzaDesiderataLabel, PowerAndVoltageComponent.POTENZA_DESIDERATA_MAX, pavc.xpathPotenzaDesiredata);
			logger.write("Enter 999 in the Potenza Desiderata field and verify only till 99 can be entered -- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");
		}catch (Throwable e) {
	prop.setProperty("RETURN_VALUE", "KO");

	StringWriter errors = new StringWriter();
	e.printStackTrace(new PrintWriter(errors));
	errors.toString();
	logger.write("ERROR_DESCRIPTION: " + errors.toString());

	prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
	if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
		throw e;

//	prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
	// Store WebDriver Info in properties file
	prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
}

	}

}
