package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.ForniturePerCambioIndirizzoComponent;
import com.nttdata.qa.enel.components.lightning.RicercaRichiesteComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiesteClienteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaStatiRichiesta {
	@Step("Verifica Stati Richiesta")
	public static void main(String[] args) throws Exception {
        Properties prop = null;

        try {

                      prop = WebDriverManager.getPropertiesIstance(args[0]);
                      RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
                      QANTTLogger logger = new QANTTLogger(prop);

		
			RicercaRichiesteComponent cercaRichieste = new RicercaRichiesteComponent(driver);
		
			cercaRichieste.verifyStatoRichiesta(prop.getProperty("EXPECTEDSTATUS", "Chiuso"));
			cercaRichieste.verifySottostatoRichiesta(prop.getProperty("EXPECTED_SOTTO_STATUS", "Ricevuto"));
			
            prop.setProperty("RETURN_VALUE", "OK");
        }
        catch (Exception e) 
        {
                       prop.setProperty("RETURN_VALUE", "KO");
                       StringWriter errors = new StringWriter();
                       e.printStackTrace(new PrintWriter(errors));
                       errors.toString();

                       prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
                      if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

        }finally
        {
                       //Store WebDriver Info in properties file
                       prop.store(new FileOutputStream(args[0]), "Set TestObject Info");                               
        }
	}

}
