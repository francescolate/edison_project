package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BodyCheckerComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PublicAreaBusinessEngBodyChecker {

	public static void main(String[] args) throws Exception {
		
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);
			try {
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

				BodyCheckerComponent bcc = new BodyCheckerComponent(driver);
				
				logger.write("Checking top body content -- Start");
				bcc.verifyComponentExistence(bcc.whatKindOfContract);
				bcc.verifyComponentExistence(bcc.where);
				bcc.verifyComponentExistence(bcc.whatFor);
				logger.write("Checking top body content -- End");
				
				logger.write("Checking body content -- Start");
				bcc.comprareText(bcc.bodyContainer, bcc.bodyContentBusiness, true);
				logger.write("Checking body content -- Start");
				
				prop.setProperty("RETURN_VALUE", "OK");
			} catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}
	}

}
