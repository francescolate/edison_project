package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.AccountRegistrationComponent;
import com.nttdata.qa.enel.components.colla.BSN_AttivazioneInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerifyEmailSubject_260 {
	@Step("Leggi email e clicca sul Touch Point di Attivazione")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
		try {
				String link = null;		
				if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
					
				 //  prop.setProperty("USERNAME",prop.getProperty("GMAIL_ADDRESS_INPUT"));			
					//prop.setProperty("PASSWORD",Costanti.password_Tp2_mobile);
		            logger.write("Recupera Link - Start");
		            GmailQuickStart gm = new GmailQuickStart();
		            link = gm.recuperaLink(prop, "INFO_ENEL_ACTIVATION", false);
		            Thread.sleep(5000);
		            LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
		            log.launchLink(link);
		            Thread.sleep(10000);
		            BSN_AttivazioneInfoEnelEnergiaComponent info = new BSN_AttivazioneInfoEnelEnergiaComponent(driver);
		            Thread.sleep(10000);
		            info.verifyComponentExistence(info.certificationEmailTitle);
		            logger.write("Recupera Link - Completed");
				}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
