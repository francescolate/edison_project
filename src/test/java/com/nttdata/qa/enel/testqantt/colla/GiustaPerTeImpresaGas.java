package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GiustaPerTeImpresaGasComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.PaginaAdesioneInserisciDatiComponent;
import com.nttdata.qa.enel.components.colla.PaginaAdesionePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class GiustaPerTeImpresaGas {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("Click su Attiva Ora per l'offerta GiustaPerTe Impresa GAS - Start");
			GiustaPerTeImpresaGasComponent offerta=new GiustaPerTeImpresaGasComponent(driver);
			
			offerta.verifyComponentExistence(offerta.offertaLink);
			offerta.clickeaspetta(offerta.offertaLink);
			
			Thread.sleep(5000);
			
			offerta.verifyComponentExistence(offerta.attivaOra);
			offerta.clickeaspetta(offerta.attivaOra);
			
			Thread.sleep(5000);
			
			By titolo_Pagina=offerta.titoloPagina;
			offerta.verifyComponentExistence(titolo_Pagina);
			
			By sotto_titolo=offerta.sottotitolo;
			//offerta.verificaTesto(sotto_titolo, offerta.sottotitle);
			logger.write("Click su Attiva Ora per l'offerta GiustaPerTe Impresa GAS - Completed");
	
//			logger.write("click sull'icona 'i' accanto a PDR o matricola - Start");
//			By pdr_I=offerta.pdrI;
//			offerta.verifyComponentExistence(pdr_I);
//			Thread.currentThread().sleep(5000);
//			offerta.clickComponent(pdr_I);
//			Thread.currentThread().sleep(2000);
//			
//			By titolo_PdrPopup=offerta.titoloPdrPopup;
//			offerta.verificaTesto(titolo_PdrPopup,offerta.titoloPdr);
//			
//			By testo_Pdr=offerta.testoPdr;
//			offerta.verificaTesto(testo_Pdr,offerta.descrizionepdr);
//			By chiudi_Pdr=offerta.chiudiPdr;
//			offerta.clickComponent(chiudi_Pdr);
//			Thread.currentThread().sleep(1000);
//			offerta.verifyComponentExistence(titolo_Pagina);
//			
//			offerta.verifyComponentExistence(pdr_I);
//			offerta.clickComponent(pdr_I);
//			Thread.currentThread().sleep(3000);
//			
//			By pag=offerta.pagina;
//			offerta.clickcomponentjavascript(pag);
//			logger.write("click sull'icona 'i' accanto a PDR o matricola - Completed");
//			
//			logger.write("Nella pagina sono presenti i primi due campi 'Forma giuridica*' e 'Codice Fiscale Società*' - Start");
//		
//			Thread.currentThread().sleep(3000);
//			PaginaAdesioneInserisciDatiComponent inserisci=new PaginaAdesioneInserisciDatiComponent(driver);
//			By switchFrame=inserisci.frame;
//			inserisci.setframe(switchFrame);
//			
//			By inserisci_Dati=inserisci.inserisciDati;
//			inserisci.verifyComponentExistence(inserisci_Dati);
//			
//			By campoFormaGiuridica=inserisci.campoFormaGiuridica;
//			inserisci.scrollComponent(campoFormaGiuridica);
//			inserisci.verifyComponentExistence(campoFormaGiuridica);
//			By codiceFiscaleSocieta=inserisci.codiceFiscaleSocieta;
//			inserisci.verifyComponentExistence(codiceFiscaleSocieta);
//			logger.write("Nella pagina sono presenti i primi due campi 'Forma giuridica*' e 'Codice Fiscale Società*' - Completed");
//			driver.switchTo().defaultContent();
//			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sul motorino di ricerca - Start");
//			
//			PaginaAdesionePodComponent pod=new PaginaAdesionePodComponent(driver);
//			By chiudi_Adesione=pod.chiudiAdesione;
//			pod.clickComponent(chiudi_Adesione);
//			By escisenzasalvare_button=pod.escisenzasalvarebutton;
//			pod.verifyComponentExistence(escisenzasalvare_button);
//			pod.scrollComponent(escisenzasalvare_button);
//			Thread.currentThread().sleep(3000);
//			pod.clickcomponentjavascript(escisenzasalvare_button);
//			
//			LuceAndGasSearchSectionComponent headerMenu = new LuceAndGasSearchSectionComponent(driver);	
//            By searchMenu=headerMenu.searchSection;
//			
//			headerMenu.verifyQuestionsExistence(searchMenu);
//			By precedentcontract=headerMenu.contractListvalue;
//			headerMenu.verifyTextSearchMenu(precedentcontract,"Gas");
//			By precedentplace=headerMenu.placeListvalue;
//			headerMenu.verifyTextSearchMenu(precedentplace,"Negozio - Ufficio");
//			By precedentneed=headerMenu.Listvalue;
//			headerMenu.verifyTextSearchMenu(precedentneed,"Subentro");
//			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sul motorino di ricerca - Completed");
//		     
//			logger.write("Click sul Dettaglio offerta per l'offerta GiustaPerTe Impresa GAS e apertura pagina con presenza ATTIVA ORA e ATTIVA L'OFFERTA - Start");
//			By dettaglio_OffertaButton=offerta.dettaglioOffertaButton;
//			offerta.verifyComponentExistence(dettaglio_OffertaButton);
//			offerta.clickComponent(dettaglio_OffertaButton);
//			
//			By titolo_paginaDettaglio=offerta.titolopaginaDettaglio;
//			offerta.verifyComponentExistence(titolo_paginaDettaglio);
//			
//			By attivaora_spalletta=offerta.attivaOraLink;
//			offerta.verifyComponentExistence(attivaora_spalletta);
//			
//			By attivaofferta_Button=offerta.attivaoffertaButton;
//			offerta.verifyComponentExistence(attivaofferta_Button);
//			logger.write("Click sul Dettaglio offerta per l'offerta GiustaPerTe Impresa GAS e apertura pagina con presenza ATTIVA ORA e ATTIVA L'OFFERTA - Completed");
//			
//			logger.write("click sul pulsante ATTIVA L'OFFERTA nella pagina di dettaglio dell'offerta EGiustaPerTe Impresa GAS - Start");
//			Thread.currentThread().sleep(3000);
//			offerta.clickeaspetta(attivaofferta_Button);
//			Thread.currentThread().sleep(5000);
//			
//			offerta.verifyComponentExistence(titolo_Pagina);
//			
//			offerta.verificaTesto(sotto_titolo, offerta.sottotitle);
//			logger.write("click sul pulsante ATTIVA L'OFFERTA nella pagina di dettaglio dell'offerta EGiustaPerTe Impresa GAS - Completed");
//			
//			
//			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sulla pagina di dettaglio dell'offerta Giusta per te - Start");
//		
//			Thread.currentThread().sleep(5000);
//			offerta.clickComponent(chiudi_Adesione);
//		
//			offerta.verifyComponentExistence(escisenzasalvare_button);
//			offerta.scrollComponent(escisenzasalvare_button);
//			Thread.currentThread().sleep(3000);
//			offerta.clickcomponentjavascript(escisenzasalvare_button);
//			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkGIUSTA_PER_TE);
//			offerta.verifyComponentExistence(titolo_paginaDettaglio);
//			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sulla pagina di dettaglio dell'offerta Giusta per te - Completed");
//			
//			logger.write("Click su Attiva Ora per l'offerta GiustaPerTe Impresa GAS - Start");
//			offerta.verifyComponentExistence(attivaora_spalletta);
//			offerta.scrollComponent(attivaora_spalletta);
//			Thread.currentThread().sleep(5000);
//			offerta.clickeaspetta(attivaora_spalletta);
//            Thread.currentThread().sleep(3000);
//			offerta.verifyComponentExistence(titolo_Pagina);
//			//offerta.verificaTesto(sotto_titolo, offerta.sottotitle);
//			logger.write("Click su Attiva Ora per l'offerta GiustaPerTe Impresa GAS - Completed");
//			
			
			OCR_Module_Component ocrc = new OCR_Module_Component(driver);
			

			
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sulla pagina di dettaglio dell'offerta Giusta per te - Start");
			ocrc.clickComponent(ocrc.chiudiLogo);
			ocrc.verifyComponentVisibility(ocrc.esciSenzaSalvare);
			ocrc.jsClickComponent(ocrc.esciSenzaSalvare);
			
//			
//			offerta.clickComponent(chiudi_Adesione);
//		
//			offerta.verifyComponentExistence(escisenzasalvare_button);
//			offerta.scrollComponent(escisenzasalvare_button);
//			Thread.currentThread().sleep(3000);
//			offerta.clickcomponentjavascript(escisenzasalvare_button);
//			Thread.currentThread().sleep(1000);
//			offerta.checkUrl(prop.getProperty("LINK")+offerta.linkGIUSTA_PER_TE);
//			offerta.verifyComponentExistence(titolo_paginaDettaglio);
			logger.write("click sul pulsante x in alto, click su esci pulsante ESCI SENZA SALVARE e check sulla pagina di dettaglio dell'offerta Giusta per te - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
