package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.lightning.ConfermaFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CommodityMultiEleSubentro {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
			//selezione fornitura
		    conferma.press(conferma.selezionaPrimaFornituraRes);
			
			if (prop.getProperty("USO").equalsIgnoreCase("uso abitativo")) {

				conferma.inserisciResidente(prop.getProperty("RESIDENTE"));
				conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
				conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
		
			
				/*
				try {
			        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(conferma.bannerResidenza));
					if(driver.findElement(conferma.bannerResidenza).isDisplayed()) {
						conferma.clickComponent(conferma.chiudBannerResidenza);
					}
					} catch (NoSuchElementException | TimeoutException e) {
						System.out.println("il banner residenza non è apparso");
					} catch (Exception e) {
						throw e;
					}
				*/
				try {
				conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
				} catch (Exception e) {
					if(driver.findElement(conferma.bannerResidenza).isDisplayed()) {
						conferma.clickComponent(conferma.chiudBannerResidenza);
						conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
					}
				}
					
					
				conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
				if (!prop.getProperty("ASCENSORE").equals("SI")) {
					conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
				} else {
					conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
					conferma.verifyComponentExistence(conferma.alertAscensore);
					Assert.assertEquals(
							"Ricorda che per l’inserimento dell’ascensore è necessario il calcolo della potenza richiesta.",
							driver.findElement(conferma.alertAscensore).getText());
					conferma.clickComponent(conferma.alertAscensoreOk);
					conferma.verifyCampiAscensore();
					conferma.inserisciTipologiaAscensore(prop.getProperty("TIPOLOGIA_ASCENSORE"));
					conferma.inserisciCorrenteDiRegime(prop.getProperty("CORRENTE_DI_REGIME"));
					conferma.clickComponent(conferma.calcolaAscensore);
					conferma.verifyComponentExistence(conferma.alertAscensore2);
					Assert.assertEquals(
							"La potenza richiesta/disponibile non è sufficiente rispetto ai dati ascensore. La potenza suggerita è 75 vuole continuare?",
							driver.findElement(conferma.alertAscensore2).getText());
					conferma.clickComponent(conferma.alertAscensoreSi);

				}
				conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
				
				
				
				conferma.press(conferma.selezionaSecondaFornitura);
				
					conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
					conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
					conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
					if (!prop.getProperty("ASCENSORE").equals("SI")) {
						conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
					} else {
						conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
						conferma.verifyComponentExistence(conferma.alertAscensore);
						Assert.assertEquals(
								"Ricorda che per l’inserimento dell’ascensore è necessario il calcolo della potenza richiesta.",
								driver.findElement(conferma.alertAscensore).getText());
						conferma.clickComponent(conferma.alertAscensoreOk);
						conferma.verifyCampiAscensore();
						conferma.inserisciTipologiaAscensore(prop.getProperty("TIPOLOGIA_ASCENSORE"));
						conferma.inserisciCorrenteDiRegime(prop.getProperty("CORRENTE_DI_REGIME"));
						conferma.clickComponent(conferma.calcolaAscensore);
						conferma.verifyComponentExistence(conferma.alertAscensore2);
						Assert.assertEquals(
								"La potenza richiesta/disponibile non è sufficiente rispetto ai dati ascensore. La potenza suggerita è 75 vuole continuare?",
								driver.findElement(conferma.alertAscensore2).getText());
						conferma.clickComponent(conferma.alertAscensoreSi);

					}
					
				
	
			} else {
				 if(!prop.getProperty("CATEGORIA_MERCEOLOGICA","").contentEquals("")) {
			            if(prop.getProperty("CATEGORIA_MERCEOLOGICA","").contentEquals("SERVIZI PER LA RETE STRADALE")) {
							conferma.verificaValoreCategoriaMerceologica(prop.getProperty("CATEGORIA_MERCEOLOGICA"));
						} else {
			            	if(driver.findElement(conferma.inputCategoria).isEnabled()) {
			            	conferma.inserisciCategoriaMerceologica(prop.getProperty("CATEGORIA_MERCEOLOGICA"));
			            	}
						}
		            }
				 conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
				 conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
				 conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
				 if (!prop.getProperty("ASCENSORE").equals("SI")) {
						conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
					} else {
						conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
						conferma.verifyComponentExistence(conferma.alertAscensore);
						Assert.assertEquals(
								"Ricorda che per l’inserimento dell’ascensore è necessario il calcolo della potenza richiesta.",
								driver.findElement(conferma.alertAscensore).getText());
						conferma.clickComponent(conferma.alertAscensoreOk);
						conferma.verifyCampiAscensore();
						conferma.inserisciTipologiaAscensore(prop.getProperty("TIPOLOGIA_ASCENSORE"));
						conferma.inserisciCorrenteDiRegime(prop.getProperty("CORRENTE_DI_REGIME"));
						conferma.clickComponent(conferma.calcolaAscensore);
						conferma.verifyComponentExistence(conferma.alertAscensore2);
						Assert.assertEquals(
								"La potenza richiesta/disponibile non è sufficiente rispetto ai dati ascensore. La potenza suggerita è 75 vuole continuare?",
								driver.findElement(conferma.alertAscensore2).getText());
						conferma.clickComponent(conferma.alertAscensoreSi);

					}
				 
				 conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
				
			}
			conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
			conferma.pressAndCheckSpinners(conferma.confirmButton);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}
