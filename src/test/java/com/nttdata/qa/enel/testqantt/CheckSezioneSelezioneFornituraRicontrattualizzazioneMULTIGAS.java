package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CheckSezioneSelezioneFornituraRicontrattualizzazioneMULTIGAS {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Start");
				SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
                By sezione_fornitura=forn.sezioneSelezioneFornitura;
                forn.verifyComponentExistence(sezione_fornitura);
                
                By campo_CercaFornitura=forn.campoCercaFornitura;
                forn.verifyComponentExistence(campo_CercaFornitura);
                forn.scrollComponent(forn.pagina);
              
                TimeUnit.SECONDS.sleep(1);
                logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Completed");
			   
				
				logger.write("inserire nel campo 'Cerca Fornitura' il POD - Start");
				TimeUnit.SECONDS.sleep(1);
				forn.enterPodValue(campo_CercaFornitura, prop.getProperty("POD_GAS1"));
				TimeUnit.SECONDS.sleep(3);
				logger.write("inserire nel campo 'Cerca Fornitura' il POD - Completed");
				
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: COMMODITY,STATO,NUMERO UTENZA,POD,INDIRIZZO DI FORNITURA,USO FORNITURA,MERCATO DI RIFERIMENTO,PRODOTTO,ATTIVABILITÀ,OFFERTABILITÀ e selezionare la fornitura di interesse - Start");
				forn.verificaColonneTabella(forn.colonneRic);
				forn.scrollComponent(forn.pagina);
				TimeUnit.SECONDS.sleep(15);
				forn.selezionaRecordInTabella(prop.getProperty("POD_GAS1"));
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: COMMODITY,STATO,NUMERO UTENZA,POD,INDIRIZZO DI FORNITURA,USO FORNITURA,MERCATO DI RIFERIMENTO,PRODOTTO,ATTIVABILITÀ,OFFERTABILITÀ e selezionare la fornitura di interesse - Completed");
				TimeUnit.SECONDS.sleep(15);
				String [] arr = {	"GAS", "Attivato", prop.getProperty("NUMERO_UTENZA_GAS1"),
						prop.getProperty("POD_GAS1"), prop.getProperty("INDIRIZZO_GAS1"),"Uso Abitativo", "Mercato Libero",
						prop.getProperty("PRODOTTO_GAS1_ESISTENTE"), "Attivabile", "OK"};
				ArrayList<String> campi = new ArrayList<String>();
				Collections.addAll(campi, arr);
				//forn.ControlloPrimoRecordInTabella(campi);
			
				logger.write("inserire nel campo 'Cerca Fornitura' il POD - Start");
				TimeUnit.SECONDS.sleep(1);
				forn.enterPodValue(campo_CercaFornitura, prop.getProperty("POD_GAS2"));
				TimeUnit.SECONDS.sleep(3);
				logger.write("inserire nel campo 'Cerca Fornitura' il POD - Completed");
				
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: COMMODITY,STATO,NUMERO UTENZA,POD,INDIRIZZO DI FORNITURA,USO FORNITURA,MERCATO DI RIFERIMENTO,PRODOTTO,ATTIVABILITÀ,OFFERTABILITÀ e selezionare la fornitura di interesse - Start");
				forn.verificaColonneTabella(forn.colonneRic);
				forn.scrollComponent(forn.pagina);
				TimeUnit.SECONDS.sleep(15);
				forn.selezionaRecordInTabella(prop.getProperty("POD_GAS2"));
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: COMMODITY,STATO,NUMERO UTENZA,POD,INDIRIZZO DI FORNITURA,USO FORNITURA,MERCATO DI RIFERIMENTO,PRODOTTO,ATTIVABILITÀ,OFFERTABILITÀ e selezionare la fornitura di interesse - Completed");
				TimeUnit.SECONDS.sleep(15);
					String [] arr2 = {	"GAS", "Attivato", prop.getProperty("NUMERO_UTENZA_GAS2"),
						prop.getProperty("POD_GAS2"), prop.getProperty("INDIRIZZO_GAS2"),"Uso Abitativo", "Mercato Libero",
						prop.getProperty("PRODOTTO_GAS2_ESISTENTE"), "Attivabile", "OK"};
				ArrayList<String> campi2 = new ArrayList<String>();
				Collections.addAll(campi2, arr2);
				//forn.ControlloPrimoRecordInTabella(campi2);
				
				
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna ATTIVABILITA' sia presente il valore 'Non Attivabile' - Start");
				
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(5);
				forn.checkValoreInTable(prop.getProperty("ATTIVABILITA"));
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna ATTIVABILITA' sia presente il valore 'Non Attivabile' - Completed");
				
				logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Start");
				forn.verifyComponentExistence(forn.buttonConfermaSelezioneFornituraAbilitato);
				forn.clickComponent(forn.buttonConfermaSelezioneFornituraAbilitato);
				logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Completed");
				TimeUnit.SECONDS.sleep(2);
				logger.write("click sul pulsante 'Conferma' - Start");
				forn.verifyComponentExistence(forn.buttonConferma);
				forn.clickComponent(forn.buttonConferma);
				logger.write("click sul pulsante 'Conferma' - Completed");
				TimeUnit.SECONDS.sleep(2);
				logger.write("click sul pulsante 'Crea offerta' - Start");
				forn.verifyComponentExistence(forn.buttonCreaOfferta);
				forn.clickComponent(forn.buttonCreaOfferta);
				
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(2);
				logger.write("se compare la popup 'Proposta Voltura Dual' click su Si, poi su 'Conferma Selezione Fornitura', 'Conferma' e 'Crea Offerta' - Start");
				forn.clickPopupIfExist(forn.popupSi);
				logger.write("se compare la popup 'Proposta Voltura Dual' click su Si, poi su 'Conferma Selezione Fornitura', 'Conferma' e 'Crea Offerta' - Completed");
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(3);
				
				logger.write("click sul pulsante 'Crea offerta' - Completed");
				

				
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}


