package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.Luce30SpringComponent;
import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.OrdinaPerMenuComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HeaderOffertaLuce30Spring {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Luce30SpringComponent offerta =new Luce30SpringComponent(driver);
			
			logger.write("verifica testo dell'offerta LUCE 30 Spring - Start");
			By offerta_30Spring=offerta.offerta30Spring;
			offerta.verifyComponentExistence(offerta_30Spring);
			offerta.scrollComponent(offerta_30Spring);
			Thread.currentThread().sleep(5000);
			By testo_inAlto=offerta.testoinAlto;
			offerta.verificaTesto(testo_inAlto,offerta.testoInAlto);
			
			By button_DettaglioOfferta=offerta.buttonDettaglioOfferta;
			offerta.verifyComponentExistence(button_DettaglioOfferta);
			
			By label_sconto=offerta.labelSconto;
			offerta.verifyComponentExistence(label_sconto);
			
			By testo_inBasso=offerta.testoinBasso;
			offerta.verificaTesto(testo_inBasso,offerta.testoInBasso);
			
			By link_AttivaOra=offerta.linkAttivaOra;
			offerta.verifyComponentExistence(link_AttivaOra);
			logger.write("verifica testo dell'offerta LUCE 30 Spring - Completed");
			
			logger.write("Click su Dettaglio Offerta Luce 30 Spring e check sul testo di header pagina - Start");
			offerta.clickComponent(button_DettaglioOfferta);
	
			By titolo_Luce30Spring=offerta.titoloLuce30Spring;
			offerta.verifyComponentExistence(titolo_Luce30Spring);
			
			By sottotitolo_Luce30Spring=offerta.sottotitoloLuce30Spring;
			offerta.verifyComponentExistence(sottotitolo_Luce30Spring);
			
			By Button_AttivaSubito=offerta.ButtonAttivaSubito;
			offerta.verifyComponentExistence(Button_AttivaSubito);
			logger.write("Click su Dettaglio Offerta Luce 30 Spring e check sul testo di header pagina - Completed");
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
