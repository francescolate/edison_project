package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.DocumentValidatorComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ValidaTuttiIDocumenti {
    @Step("Valida Documenti")
    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            driver.switchTo().defaultContent();
            DocumentiUploadComponent doc = new DocumentiUploadComponent(driver);
            DocumentValidatorComponent validator = new DocumentValidatorComponent(driver);

            logger.write("Visualizza Documento - Start");
            TimeUnit.SECONDS.sleep(10);
            doc.visualizzaDocumenti();
            System.out.println("Visualizza Documento");
            logger.write("Visualizza Documento - Completed");
            logger.write("Valida Documento - Start");
            TimeUnit.SECONDS.sleep(10);
            validator.validaTuttiIDoc(util.getFrameByIndex(1));
            logger.write("Valida Documento - Completed");
            TimeUnit.SECONDS.sleep(10);
            logger.write("Chiudi Validator Documento - Start");
            validator.chiudi();
            logger.write("Chiudi Validator Documento - Completed");
            driver.navigate().refresh();
            validator.verifyComponentExistence(validator.statoChiusaOfferta);
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
