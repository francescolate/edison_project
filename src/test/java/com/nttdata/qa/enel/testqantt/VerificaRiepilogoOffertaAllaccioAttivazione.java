package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaRiepilogoOffertaAllaccioAttivazione {


	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    logger.write("Verifica Campi prepopolati Riepilogo Offerta - Start");
		    RiepilogoOffertaComponent riepilogo = new RiepilogoOffertaComponent(driver);
		    String tipoUtenza=prop.getProperty("TIPO_UTENZA");
		    if(tipoUtenza.compareTo("PE")==0){
		    	riepilogo.verificaCampiPrepopolatiAllaccioPE();
		    }
		    else if(tipoUtenza.compareTo("S2S")==0){
		    	riepilogo.verificaCampiPrepopolatiAllaccioS2S();
		    }
		    else if(tipoUtenza.compareTo("PENP")==0){
		    	riepilogo.verificaCampiPrepopolatiAllaccioPENP();
		    }
		    else throw new Exception("Tipo utenza:"+tipoUtenza+" non gestita");
	        
	        TimeUnit.SECONDS.sleep(5);
	        logger.write("Verifica Campi prepopolati Riepilogo Offerta - Completed");
	        
	        
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
