package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazione_80 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    prop.setProperty("SupplyServices", "Addebito Diretto Bolletta Web Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione Autolettura InfoEnelEnergia Opzione Bolletta Rateizzazione ");
		    prop.setProperty("COLOR", "DeepPink");
		    prop.setProperty("COLOR1", "Crimson");
		    prop.setProperty("OBJECT_NAME", "MADATORYFIELD");

			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
			
			logger.write("Verifying the list of devices after clicking on Servizi  STARTS");
			mif.clickComponent(mif.serivzi);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.SERVIZI_PATH, mif.serviziPath);
			
			mif.comprareText(mif.serviziTitle, ModificaIndirizzoFattuazioneComponent.SERVIZI_TITLE, true);
			mif.checkForSupplyServices(prop.getProperty("SupplyServices"));
			logger.write("Verifying the list of devices after clicking on Servizi  ENDS");
			
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-STARTS");
			Thread.currentThread().sleep(1000);
			mif.clickComponent(mif.modificaIndirizzodiFatturazione);
			mif.checkMenuPath(ModificaIndirizzoFattuazioneComponent.INDIRIZZO_PATH, mif.indirizzoDiFatturazionePath);
			mif.comprareText(mif.sectionData, ModificaIndirizzoFattuazioneComponent.SECTION_DATA, true);
			mif.comprareText(mif.sectionBillingAddress, ModificaIndirizzoFattuazioneComponent.SECTION_BILLING_ADDRESS, true);
			mif.comprareText(mif.sectionTechnicalSpec, ModificaIndirizzoFattuazioneComponent.SECTION_TECH_SPECIFICATION, true);
			mif.comprareText(mif.sectionContract, ModificaIndirizzoFattuazioneComponent.SECTION_CONTRACT, true);
			//mif.comprareText(mif.clientNumber, prop.getProperty("CLIENT_NUMBER"), true);
			logger.write("Verifying the content after clicking on Modifica Indirizzo di Fatturazione-ENDS");
			
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Provincia-STARTS");
			mif.clickComponent(mif.modificaButton);
			mif.enterinputParameters(mif.comune, prop.getProperty("COMUNE"));
			mif.enterinputParameters(mif.indrizzo, prop.getProperty("INDIRIZZO"));
			mif.enterinputParameters(mif.numerocivico, prop.getProperty("NUMERO_CIVICO"));
			mif.clickComponent(mif.proseguiButton);
			mif.comprareText(mif.campoRichiestoProvinica, ModificaIndirizzoFattuazioneComponent.CAMPO_RICHIESTA, true);
		//	mif.checkColor(mif.provincia, prop.getProperty("COLOR"), prop.getProperty("OBJECT_NAME"));
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Provincia-ENDS");
			
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Comune-STARTS");
			mif.refreshPage();
			mif.enterinputParameters(mif.indrizzo, prop.getProperty("INDIRIZZO"));
			mif.enterinputParameters(mif.numerocivico, prop.getProperty("NUMERO_CIVICO"));
			mif.enterinputParameters(mif.provincia, prop.getProperty("PROVINCE"));
			mif.jsClickElement(mif.proseguiButton);
			mif.comprareText(mif.CampoRichiestaComune, ModificaIndirizzoFattuazioneComponent.CAMPO_RICHIESTA, true);
		//	mif.checkColor(mif.comune, prop.getProperty("COLOR"), prop.getProperty("OBJECT_NAME"));
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Comune-ENDS");
			
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Indirizzo-STARTS");
			mif.refreshPage();
			mif.enterinputParameters(mif.provincia, prop.getProperty("PROVINCE"));
			mif.enterinputParameters(mif.comune, prop.getProperty("COMUNE"));
			mif.enterinputParameters(mif.numerocivico, prop.getProperty("NUMERO_CIVICO"));
			mif.clickProseguiButton(mif.proseguiButton, mif.CampoRichiestaIndirizzo, ModificaIndirizzoFattuazioneComponent.CAMPO_RICHIESTA);
			mif.comprareText(mif.CampoRichiestaIndirizzo, ModificaIndirizzoFattuazioneComponent.CAMPO_RICHIESTA, true);
			mif.checkColor(mif.indrizzo, prop.getProperty("COLOR"), prop.getProperty("OBJECT_NAME"));
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Indirizzo-ENDS");
			
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Numerocivico-STARTS");
			mif.refreshPage();
			mif.enterinputParameters(mif.provincia, prop.getProperty("PROVINCE"));
			mif.enterinputParameters(mif.comune, prop.getProperty("COMUNE"));
			mif.enterinputParameters(mif.indrizzo, prop.getProperty("INDIRIZZO"));
			mif.clickComponent(mif.proseguiButton);
			//mif.checkColor(mif.numerocivico, prop.getProperty("COLOR1"), prop.getProperty("OBJECT_NAME"));
			logger.write("Verifying Campo Richiesto text for not filling the mandatory field Numerocivico-ENDS"); 
			
						
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
