package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.GestionePredisposizionePresaComponentEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class SceltaProcessoPredisposizionePresaEVOGas {

	@Step("Scelta Processo Predisposizione Presa GAS EVO")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//				SeleniumUtilities util = new SeleniumUtilities(driver);

				SceltaProcessoComponent predisposizionePresa = new SceltaProcessoComponent(driver);
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
//				DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
				IntestazioneNuovaRichiestaComponentEVO intestazione = new IntestazioneNuovaRichiestaComponentEVO(
						driver);
				CheckListComponentEVO checkListModalComponent = new CheckListComponentEVO(driver);
//				GestionePredisposizionePresaComponentEVO presa = new GestionePredisposizionePresaComponentEVO(driver);
//				String statoUbiest = Costanti.statusUbiest;
		
				logger.write("Selezione processo Predisposizione Presa Evo - Start");
				TimeUnit.SECONDS.sleep(10);
				predisposizionePresa.clickAllProcess();
				TimeUnit.SECONDS.sleep(2);
				predisposizionePresa.chooseProcessToStart(predisposizionePresa.predisposizionePresaEVOGas);
				logger.write("Selezione processo Predisposizione Presa Evo - Completed");

				logger.write("Identificazione interlocutore - Start");
				identificaInterlocutore.insertDocumentoNew("b");
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				logger.write("Identificazione interlocutore - Completed");

				logger.write("Conferma Interlocutore - Start");
				identificaInterlocutore.pressButton(identificaInterlocutore.confermaPagina);
				logger.write("Conferma Interlocutore - Completed");

				logger.write("Inserimento documenti identita cliente - Start");

				// CHECKLIST
//				TimeUnit.SECONDS.sleep(50);
				logger.write("Attendo checklist - Start");
				checkListModalComponent.attendiChecklist();
				logger.write("Attendo checklist - Completed");
			
				logger.write("Verifica checklist e conferma - Start");
//				String interactionFrame = checkListModalComponent.clickOk();
				checkListModalComponent.clickConferma();
				logger.write("Verifica checklist e conferma - Completed");

				// Salva NUMERO_RICHIESTA
				prop.setProperty("NUMERO_RICHIESTA",
						intestazione.salvaNumeroRichiesta(intestazione.pNRichiestaPaginaNuovaRichiesta6));
//				SetPropertyRichiesta6.main(args);
				logger.write("NUMERO_RICHIESTA="+prop.getProperty("NUMERO_RICHIESTA"));
	
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
