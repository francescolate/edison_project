package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.components.colla.SubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeID33 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			logger.write("verifica presenza dell'offerta 'Ore Free' - Start");
			OreFreeComponent offerta=new OreFreeComponent(driver);
			
			By offerta_orefree=offerta.offertaOreFree;
			offerta.verifyComponentExistence(offerta_orefree);
			offerta.scrollComponent(offerta_orefree);
			logger.write("verifica presenza dell'offerta 'Ore Free' - Completed");
			
			logger.write("Click su 'Dettaglio Offerte' nella card Ore Free - Start");
			By dettagliofferta_Button=offerta.dettaglioffertaButton;
			offerta.verifyComponentExistence(dettagliofferta_Button);
			offerta.clickComponent(offerta_orefree);
			
			By titolopaginaOreFree=offerta.titolopaginaOreFree;
			offerta.verifyComponentExistence(titolopaginaOreFree);
			logger.write("Click su 'Dettaglio Offerte' nella card Ore Free - Completed");
			
			logger.write("Cliccare sulla CTA 'Verifica e aderisci' - Start");
			By verifica_aderisciVerde=offerta.verificaaderisciVerde;
			offerta.verifyComponentExistence(verifica_aderisciVerde);
			offerta.clickComponent(verifica_aderisciVerde);
			
           logger.write("Check sulla popup - Start");
			
			By inserisci_NumeroPod=offerta.inserisciNumeroPod;
			offerta.verifyComponentExistence(inserisci_NumeroPod);
			Thread.currentThread().sleep(2000);
			By testo_popup=offerta.testopopup;
			offerta.verifyComponentExistence(testo_popup);
			
			By campo_CodicePod=offerta.campoCodicePod;
			offerta.verifyComponentExistence(campo_CodicePod);
			Thread.currentThread().sleep(2000);
			By conferma_Button=offerta.confermaButton;
			offerta.verifyComponentExistence(conferma_Button);
			Thread.currentThread().sleep(2000);
			//By link_pod=offerta.linkpod;
			//offerta.verifyComponentExistence(link_pod);
			offerta.replaceLinkCollaInXpathAndCheckExistObject(prop.getProperty("LINK"));
			logger.write("Check sulla popup - Completed");
			logger.write("Cliccare sulla CTA 'Verifica e aderisci' - Completed");
			Thread.currentThread().sleep(2000);
			logger.write("Click su CONFERMA senza inserire un valore nel campo POD  - Start");
			offerta.clickComponent(conferma_Button);
			Thread.currentThread().sleep(2000);
			By label_campoObbligatorio=offerta.labelcampoObbligatorio;
			offerta.verifyComponentExistence(label_campoObbligatorio);
			logger.write("Click su CONFERMA senza inserire un valore nel campo POD  - Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
