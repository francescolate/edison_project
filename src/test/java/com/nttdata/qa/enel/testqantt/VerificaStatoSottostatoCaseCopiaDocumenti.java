package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DettagliSpedizioneCopiaDocComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;



public class VerificaStatoSottostatoCaseCopiaDocumenti {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DettagliSpedizioneCopiaDocComponent doc = new DettagliSpedizioneCopiaDocComponent(driver);
			
			
			logger.write("Verifica Stato e Sottostato documento - Start ");
			
			
			int tentativi = 10;

			do {
				try {
					driver.navigate().refresh();
					TimeUnit.SECONDS.sleep(3);
					logger.write("Recupero stato e sottostato richiesta - Start");
					doc.verificaStatoSottostatoDocumento(prop.getProperty("STATO_DOCUMENTO"),prop.getProperty("SOTTOSTATO_DOCUMENTO"));
									
					logger.write("Verifica stato e sottostato richiesta - Completed");
					tentativi = 0;
				} catch (Exception e) {
					--tentativi;
					if(tentativi==0) throw e;
					else TimeUnit.SECONDS.sleep(60);
					
				}
			} while (tentativi > 0);	
				
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
