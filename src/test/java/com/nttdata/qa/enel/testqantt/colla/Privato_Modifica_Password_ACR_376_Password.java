package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BSN_ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.ModificapwdComponent;
import com.nttdata.qa.enel.components.colla.ResCutomerProfileComponent;
import com.nttdata.qa.enel.components.colla.StatoRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Modifica_Password_ACR_376_Password {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
			ModificapwdComponent mpc = new ModificapwdComponent(driver);
			logger.write("Validate home page details  - Starts");
			
			mpc.verifyComponentExistence(mpc.HomePagelogo);
			logger.write("Validate home page details   - Ends");
			
			logger.write("Verify DatidiRegistrazione link and Credenziali di accesso section - Starts");
		
			mpc.verifyComponentExistence(mpc.Chevrondown);
			mpc.clickComponent(mpc.Chevrondown); 
			mpc.verifyComponentExistence(mpc.EditProfile);
			mpc.clickComponent(mpc.EditProfile);
			logger.write("Verify DatidiRegistrazione link and Credenziali di accesso section - Completed");
			
			logger.write("Verify  Modifica password link - Starts");
			mpc.verifyComponentExistence(mpc.ModificaPassword1);
			mpc.clickComponent(mpc.ModificaPassword1);
			logger.write("Verify Modifica password link - Ends");
			
			logger.write("Verify Modifica password header and contents - Starts");
			mpc.verifyComponentExistence(mpc.modificaPasswordHeader);
			mpc.comprareText(mpc.modificaPasswordHeader, mpc.MODIFICAPASSWORD_HEADER, true);
			mpc.verifyComponentExistence(mpc.modificaPasswordSubHeader);
			mpc.comprareText(mpc.modificaPasswordSubHeader, mpc.MODIFICAPASSWORDSUB_HEADER, true);
			logger.write("Verify Modifica password header and contents - Ends");
			
			logger.write("Verify that fields are correctly displayed - Starts");
			mpc.verifyComponentExistence(mpc.nuovaPasswordLabel);
			mpc.verifyComponentExistence(mpc.nuovaPasswordInput);
			mpc.verifyComponentExistence(mpc.confermaPasswordLabel);
			mpc.verifyComponentExistence(mpc.confermaPasswordInput);
			mpc.verifyComponentExistence(mpc.confermaChangePasswdBtn);
			mpc.verifyComponentExistence(mpc.passwordCampiValue);
			mpc.comprareText(mpc.passwordCampiValue, ResCutomerProfileComponent.CAMPI_TEXT, true);
			mpc.verifyComponentExistence(mpc.nuovaPasswordValue);
			mpc.comprareText(mpc.nuovaPasswordValue, ResCutomerProfileComponent.NUOVAPASSWD_VALUE, true);
			logger.write("Verify the value fields  - Ends");
			
			logger.write("Verify the value fields  - Starts");
			mpc.enterValueInInputBox(mpc.nuovaPasswordInput, prop.getProperty("NuovaValue2"));
			mpc.enterValueInInputBox(mpc.confermaPasswordInput, prop.getProperty("ConfermaValue2"));
			mpc.clickComponent(mpc.confermaChangePasswdBtn);
			logger.write("Verify the value fields  - Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
