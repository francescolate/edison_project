package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.*;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CreaOffertaSubentroIdCla {

    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

            SeleniumUtilities util = new SeleniumUtilities(driver);
            logger.write("Valorizzazione Sezioni Offerta - Start");
            SFDCBoxSubentroComponent boxSubentro = new SFDCBoxSubentroComponent(driver);
            InserimentoFornitureSubentroComponent insert = new InserimentoFornitureSubentroComponent(driver);
            GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
            RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);

            TimeUnit.SECONDS.sleep(3);

            offer.verifyComponentExistence(offer.pageRiepilogoOfferta);

            //BOX Seleziona Uso Forniture
            System.out.println("BOX Seleziona Uso Forniture");

            offer.popolareUsoFornitura(prop.getProperty("USO"));
//            By container_suf = boxSubentro.container_suf;
//            boxSubentro.verifyComponentExistence(container_suf);
//
//            By suf_pickuplist_uf = boxSubentro.suf_pickuplist_uf;
//            boxSubentro.verifyComponentExistence(suf_pickuplist_uf);
//            boxSubentro.clickAncestorClickable(boxSubentro.xpathToString(suf_pickuplist_uf), 7);
//
//            By suf_pickuplist_uf_uda = boxSubentro.suf_pickuplist_uf_uda;
//            boxSubentro.verifyComponentExistence(suf_pickuplist_uf_uda);
//            boxSubentro.clickComponentIfExist(suf_pickuplist_uf_uda);

            By suf_button_conferma = boxSubentro.suf_button_conferma;
            boxSubentro.verifyComponentExistence(suf_button_conferma);
            boxSubentro.clickComponent(suf_button_conferma);

            if (prop.getProperty("DUAL", "Y").contentEquals("Y")) {
                ConfermaFornituraComponent conferma = new ConfermaFornituraComponent(driver);
                logger.write("compila sezione commodity per fornitura ELE e click su CONFERMA FORNITURA - Start");
                conferma.confermaCommodity(prop.getProperty("POD"));
                conferma.inserisciResidente(prop.getProperty("RESIDENTE"));
                conferma.inserisciTitolarita(prop.getProperty("TITOLARITA"));
                conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
                conferma.inserisciAscensore(prop.getProperty("ASCENSORE"));
                conferma.inserisciDisalimentabilita(prop.getProperty("DISALIMENTABILITA"));
                conferma.inserisciConsumoAnnuo(prop.getProperty("CONSUMO_ANNUO"));
                conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
                logger.write("compila sezione commodity per fornitura ELE e click su CONFERMA FORNITURA - Completed");

                //compila sezione commodity per fornitura GAS
                logger.write("compila sezione commodity per fornitura GAS, click su CONFERMA FORNITURA e poi su CONFERMA - Start");
                conferma.confermaCommodity(prop.getProperty("POD_GAS"));
                TimeUnit.SECONDS.sleep(3);
                conferma.selezionaLigtheningValue("Categoria di Consumo", prop.getProperty("CATEGORIA_CONSUMO"));
                conferma.selezionaLigtheningValue("Categoria Uso", prop.getProperty("CATEGORIA_USO"));
                conferma.inserisciTelefonoDistributore(prop.getProperty("TELEFONO_DISTRIBUTORE"));
                conferma.pressAndCheckSpinners(conferma.confermaFornituraButton);
                logger.write("compila sezione commodity per fornitura GAS, click su CONFERMA FORNITURA e poi su CONFERMA - Completed");
                logger.write("nella colonna 'Confermata' nella tabella COMMODITY e' presente il valore 'SI' sia per la commodity GAS che per la commodity ELE - Start");
                offer.checkConfermataDUAL(prop.getProperty("POD"));
                offer.checkConfermataDUAL(prop.getProperty("POD_GAS"));
                logger.write("nella colonna 'Confermata' nella tabella COMMODITY e' presente il valore 'SI' sia per la commodity GAS che per la commodity ELE- Completed");
            } else {
                offer.checkValoreInTable("ELETTRICO");
                offer.clickComponentIfExist(offer.buttonModificaFornitureCommodity);
                gestione.checkSpinnersSFDC();
                TimeUnit.SECONDS.sleep(3);
                if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y")) {
                    offer.popolareCommodityBusiness("NO", "ALTRI SERVIZI", "NO", "3297451908", "NO", "SI", "1250");
                } else {
                    offer.popolareCommodityCla("ACQUA", "Proprietà o Usufrutto", "NO", "3297451908", "NO", "SI", "1250");
                }
                offer.clickComponent(offer.buttonConfermaFornituraCommodity);
                if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y")) {
                    logger.write("nella colonna 'Confermata' nella tabella COMMODITY e' presente il valore 'Si' - Start");
                    offer.checkConfermata("SI");
                    logger.write("nella colonna 'Confermata' nella tabella COMMODITY e' presente il valore 'Si' - Completed");
                }

            }

            offer.clickComponent(offer.buttonConfermaCommodity);
            gestione.checkSpinnersSFDC();

            //BOX Indirizzo di Residenza/Sede Legale
            System.out.println("BOX Indirizzo di Residenza/Sede Legale");

            By container_irsl = boxSubentro.container_irsl;
            boxSubentro.verifyComponentExistence(container_irsl);

            By irsl_indirizzo_verificato = boxSubentro.irsl_indirizzo_verificato;
            try {
                boxSubentro.verifyComponentExistence(irsl_indirizzo_verificato);
            } catch (Exception e) {
                throw new Exception("Error : banner \"Indirizzo verificato\"  not present in container \"Indirizzo di Residenza/Sede Legale\" .");
            }

            By irsl_button_conferma = boxSubentro.irsl_button_conferma;
            boxSubentro.verifyComponentExistence(irsl_button_conferma);
            boxSubentro.clickComponent(irsl_button_conferma);

            //BOX INDIRIZZO DI FATTURAZIONE
            System.out.println("BOX INDIRIZZO DI FATTURAZIONE");
            offer.popolaIndirizzoFattCla();

            if (prop.getProperty("CLIENTE_BUSINESS", "").contentEquals("Y") && prop.getProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "").contentEquals("")) {
                if ((prop.getProperty("CODICE_UFFICIO", "").equals("0000000"))) {
                    offer.popolaCampiFatturazioneElettronica(prop.getProperty("CANALE_INVIO"), prop.getProperty("CODICE_UFFICIO").subSequence(0, 4).toString());
                    TimeUnit.SECONDS.sleep(1);
                    offer.verifyComponentExistence(offer.labelErrorCodiceUfficio);
                    offer.checkCampiFatturazioneElettronicaBusiness();
                }
                offer.popolaCampiFatturazioneElettronica(prop.getProperty("CANALE_INVIO"), prop.getProperty("CODICE_UFFICIO"));
            } else {
                offer.checkCampiFatturazioneElettronica();
            }
            offer.clickComponent(offer.buttonConfermaFatElettronica);

//            gestione.checkSpinnersSFDC();
            TimeUnit.SECONDS.sleep(3);

            offer.scrollComponent(offer.buttonConfermaScontiBonus);
            // Pagamento con RID messo fuori da qui
//            offer.checkValoreInTable2AndClosePopup("Bollettino Postale");
//            offer.clickComponent(offer.buttonConfermaMetodoPagamento);
            TimeUnit.SECONDS.sleep(3);
            offer.verificaValoreCodiceCampagna();
            offer.clickComponent(offer.buttonConfermaScontiBonus);
            TimeUnit.SECONDS.sleep(3);


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}

