package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SplitPaymentComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class SplitPaymentEVOVolturaConAccollo {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		    SeleniumUtilities util = new SeleniumUtilities(driver);
		    SplitPaymentComponent split = new SplitPaymentComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			split.verifyComponentExistence(split.sezioneSpliPayment);
			split.verificaColonneTabella(split.colonne);
			split.pressButton(split.radiobuttonSplitPayment);
			
			split.verifyComponentExistence(split.splitPaymentMessage);
			
			if (prop.getProperty("POPOLA_SPLIT_PAYMENT","").contentEquals("Y")) {
            	logger.write("Andare nella sezione 'Split Payment', selezionare Si e valorizzare i campi 'Inizio validita' e 'Fine validita' - Start");
            	split.selezionaSplitPayment("Sì");
            	split.popolaCampiSplitPayment();
            	logger.write("Andare nella sezione 'Split Payment', selezionare Si e valorizzare i campi 'Inizio validita' e 'Fine validita' - Completed");
			}
			
			if (prop.getProperty("POPOLA_SPLIT_PAYMENT","").contentEquals("N")) {
            	logger.write("Andare nella sezione 'Split Payment', selezionare No - Start");
            	split.selezionaSplitPayment("No");           	
            	logger.write("Andare nella sezione 'Split Payment', selezionare No - Completed");
			}
			
			split.pressButton(split.salvaFornituraSplitPayment);
			split.pressButton(split.buttonConfermaSplitPayment);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(5);
		    
		    prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
