package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.CaricaDoccumentiComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.components.colla.ScriviciComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Publicco_ID_96_Form_Scrivici {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		prop.setProperty("NUMERO_CLIENTI", "310521194");
		prop.setProperty("NOME", "ALBUS");
		prop.setProperty("COGNOME", "SILENTE");
		prop.setProperty("INVALID_CF", "12 @ £ $");
		prop.setProperty("CF", "SLNLBS68M28H501Y");
		prop.setProperty("INVALID_CELLULARE", "123@%$");
		prop.setProperty("INVALID_CELLULARE1", "320898998765");
		prop.setProperty("CELLULARE", "3208989987");
		prop.setProperty("INVALID_CELLULARE1", "320898998765");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("INVALID_EMAIL", "@3557£$%");
		prop.setProperty("INCORRECT_CONFERMAEMAIL", "raffae.ll.aquomo@gmail.com");
		prop.setProperty("CONFERMAEMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("MESSAGGIO", "PROVA TEST AUTOMATION ID 96");
		
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			FooterPageComponent fpc = new FooterPageComponent(driver);
			ScriviciComponent  sc = new ScriviciComponent(driver);
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			Thread.sleep(5000);
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			sc.verifyComponentExistence(sc.supporto);
			sc.clickComponent(sc.supporto);			
			sc.verifyComponentExistence(sc.contractiModilistica);
			sc.clickComponent(sc.contractiModilistica);
			sc.verifyComponentExistence(sc.contatacci);
			sc.clickComponent(sc.contatacci);
			
			fpc.checkUrl(prop.getProperty("SUPPORTOLINK"));
			sc.verifyComponentExistence(sc.supportoPath);
			sc.verifyComponentExistence(sc.contatacciHeader);
			sc.compareText(sc.contatacciHeader, ScriviciComponent.CONTACCI_HEADER, true);
			sc.verifyComponentExistence(sc.contatacciSubHeader);
			sc.compareText(sc.contatacciSubHeader, ScriviciComponent.CONTACCI_SUBHEADER, true);
			sc.verifyComponentExistence(sc.vuoiWebQui);
			sc.clickComponent(sc.vuoiWebQui);
			
			fpc.checkUrl(prop.getProperty("QUI_LINK"));
			sc.verifyComponentExistence(sc.quiTitle);
			sc.compareText(sc.quiTitle, ScriviciComponent.QUI_TITLE, true);
			sc.verifyComponentExistence(sc.quiSubTitle);
			sc.compareText(sc.quiSubTitle, ScriviciComponent.QUI_SUBTITLE, true);
			sc.verifyComponentExistence(sc.inviaButton);
			sc.verifyComponentExistence(sc.quiSubTitle1);
			sc.compareText(sc.quiSubTitle1, ScriviciComponent.QUI_SUBTITLE1, true);
			sc.verifyComponentExistence(sc.cliccaQui);
			sc.clickComponent(sc.inviaButton);
			
			CaricaDoccumentiComponent cd = new CaricaDoccumentiComponent(driver);
			logger.write("Verify Carcia Doccumentic page input field -- Start");
			cd.verifyComponentExistence(cd.nome);
			cd.verifyComponentExistence(cd.cogNome);
			cd.verifyComponentExistence(cd.cf);
			cd.verifyComponentExistence(cd.cellulare);
			cd.verifyComponentExistence(cd.email);
			cd.verifyComponentExistence(cd.confermaEmail);
			cd.verifyComponentExistence(cd.numeroClienti);
			cd.verifyComponentExistence(cd.seiGiaCliente);
			cd.verifyComponentExistence(cd.radioButtonSi);
			cd.verifyComponentExistence(cd.radioButtonNo);
			cd.verifyComponentExistence(cd.messagio);
			cd.comprareText(cd.informativaPrivacySection, cd.InformativaPrivacySection, true);
			cd.verifyComponentExistence(cd.accetto);
			cd.verifyComponentExistence(cd.nonAccetto);
			cd.verifyComponentExistence(cd.prosegui);
			logger.write("Verify Carcia Doccumentic page input field -- Completed");
			
			logger.write("Click on radioButtonNo, Prosegui and verify -- Start");
			cd.clickComponent(cd.radioButtonNo);
			cd.clickComponent(cd.prosegui);
			logger.write("Click on radioButtonNo, Prosegui and verify -- Completed");
			
			logger.write("Enter valid input and verify page navigation -- Start");
			Thread.sleep(5000);
			cd.enterInputParameters(cd.nome, prop.getProperty("NOME"));
			cd.enterInputParameters(cd.cogNome, prop.getProperty("COGNOME"));
			cd.enterInputParameters(cd.cf, prop.getProperty("CF"));
			cd.enterInputParameters(cd.cellulare, prop.getProperty("CELLULARE"));
			cd.enterInputParameters(cd.indrizzoMailInputNew, prop.getProperty("EMAIL"));
			cd.enterInputParameters(cd.email, prop.getProperty("EMAIL"));
			cd.enterInputParameters(cd.confermaEmail, prop.getProperty("EMAIL"));
			cd.enterInputParameters(cd.messagio, prop.getProperty("MESSAGGIO"));
			
			OreFreeDettaglioComponent ofdf = new OreFreeDettaglioComponent(driver);
			Robot robot = new Robot();					
				ofdf.clickComponent(ofdf.dropdownClickNew);
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(500);		
			
			cd.clickComponent(cd.accetto);
			cd.clickComponent(cd.prosegui);
			Thread.sleep(3000);
			cd.verifyComponentExistence(cd.indietro);
			logger.write("Enter valid input and verify page navigation -- Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally 
		{
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
