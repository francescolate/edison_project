package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangePowerAndVoltage {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);			
			logger.write("apertura del portale web Enel di test - Start");			
			HomeComponent home = new HomeComponent(driver);			
			home.checkForResidentialMenu(home.RESIDENTIALMENU);			
			home.clickComponent(home.services);			
			ServicesComponent services = new ServicesComponent(driver);			
			logger.write("Check For supply services-- start");		
			services.checkForSupplyServices(prop.getProperty("SUPPLYSERVICES"));			
			logger.write("Check For supply services-- Completed");			
			logger.write("Click on Change Power And Voltage-- start");
			services.waitForElementToDisplay(services.modificaPotenzaeoTensione);			
			services.clickComponent(services.modificaPotenzaeoTensione);			
			logger.write("Click on Change Power And Voltage-- Completed");			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);			
			logger.write("Check Power And Voltage Content-- Start");
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageTitle);
			logger.write("Click on change power and voltage button -- Start");		
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.verifyComponentExistence(changePV.changePowerAndVoltageSubText);

			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");
			
			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");

			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
			logger.write("Check for Change Power and voltage title and sub text-- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.TitleSubText_134, true);			
			
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");
			
			changePV.changeDropDownValue(changePV.powerValue, prop.getProperty("POWER_INPUT"));
			
			changePV.clickComponent(changePV.cellular);
			
			changePV.clickComponent(changePV.email);
			
			changePV.clickComponent(changePV.confirmCellular);
			
			changePV.enterInputtoField(changePV.confirmEmail, prop.getProperty("WP_USERNAME"));
			
			changePV.enterInputtoField(changePV.confirmCellular, prop.getProperty("CELLULAR"));
			
			changePV.clickComponent(changePV.calculateQuote);
			
			logger.write("Verify Pop up -- Start");

			changePV.verifyComponentExistence(changePV.attendiPopUpTitle);
			changePV.comprareText(changePV.attendiPopUp, changePV.Attendiqualchesecondo, true);
			
			logger.write("Verify Pop up -- Completed");
			prop.setProperty("RETURN_VALUE", "OK");

			
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
