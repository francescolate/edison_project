package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFibraMelitaConferma {

	public static void main(String[] args) throws Exception {	
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);
		
			
		By id_frame = paf.id_frame;
		
		paf.verifyComponentExistence(paf.id_frame);	
		
		paf.verifyFrameAvailableAndSwitchToIt(id_frame);
		
		By icon_informativa = paf.icon_informativa;
		
		paf.verifyComponentExistence(paf.icon_informativa);

		paf.clickElementByIndex(icon_informativa, 1);
		TimeUnit.SECONDS.sleep(3);
		logger.write("click the information icon - COMPLETED");
		
		logger.write("check the popup title and description text - START");
		
		paf.verifyComponentExistence(paf.text_popup_titledescription);
		paf.verifyComponentVisibility(paf.text_popup_titledescription);
		paf.clickComponent(paf.closePopup2);
		TimeUnit.SECONDS.sleep(3);
		logger.write("check the popup descrition text - COMPLETED");
		
		
		//STEP 8 Tasto Continua Disabilitato
	    logger.write("check if the button Continue is disabled - START");

		By button_continue_disabled = paf.button_continue_disabled;
		 
		paf.verifyComponentExistence(button_continue_disabled);
		
		logger.write("check if the button Continue is disabled - COMPLETED");

		prop.setProperty("RETURN_VALUE", "OK");

	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

		//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}

	}
}

