package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSN_47_CustomerSupplyComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class BSN_Customer_Supply_48 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
		
			
		    prop.setProperty("HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("HOMEPAGE_TITLE2", "HOMEPAGE");
		    prop.setProperty("HOMEPAGE_TITLE_DESCRIPTION", "Benvenuto nella tua area dedicata Business");
		    prop.setProperty("SUPPLY_BILL_TITLE", "Forniture e bollette");
//		    prop.setProperty("CUSTOMER_LIGHT_SUPPLY", "prova alias bsn\nFornitura Luce Cliente N °: 636727539");
		    prop.setProperty("SUPPLY_DETAIL_HEADING", "La tua fornitura nel dettaglio.");
		    prop.setProperty("SUPPLY_DETAIL_TITLE1", "Tutto quello che c'è da sapere sulla tua fornitura");
		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION1", "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.");
		    prop.setProperty("SUPPLY_HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("SUPPLY_HOMEPAGE_TITLE2", "FORNITURA");
		    prop.setProperty("SUPPLY_DETAILS1", "Fornitura Luce Cliente N°: "+prop.getProperty("CUSTOM_NUM"));
//		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION2", "Se cerchi una bolletta in particolare usa i filtri per trovarla più facilmente.");
//		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION3", "Ci sono bollette scadute. Pagale comodamente on line. Se richiesto, in seguito puoi inviare il dimostrato pagamento cliccando su \"invia documenti\".");
//		    prop.setProperty("SUPPLY2_TITLE", "Le tue Bollette");
		    prop.setProperty("BILL_DETAILS", "Le tue bollette");
//		    prop.setProperty("AGGIUNGI", "Aggiungi nome fornitura");
//		    prop.setProperty("AGGIUNGI_VAL", "Fornitura Luce Cliente N°: 636727539");
//		    prop.setProperty("AGGIUNGIText", "Prova BSN");
//		    prop.setProperty("ProvaBSNValue", "prova alias bsn");
		    prop.setProperty("AGGIUNITEXTVALUE1", "Prova2BSN");

			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			
			
			logger.write("apertura del portale web Enel di test - Start");
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			
			//Click chusura bunner pubblicitario se esistente
			log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");	
			
			logger.write("check the username and passsword login - Start");
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("USERNAME")); 

			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("PASSWORD"));
			logger.write("check the username and passsword login - Completed");
			
			logger.write("Verify and click on the login button- Start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Verify and click on the login button- Complete");	
			
			logger.write("Verify the page after successful login - Start");
			By accessLoginPage=log.loginBSNSuccessful;
			log.verifyComponentExistence(accessLoginPage); 
			logger.write("Verify the page after successful login - Complete");
			
			
			
			BSN_47_CustomerSupplyComponent bcsp = new BSN_47_CustomerSupplyComponent(driver);
			
			logger.write("Home Page Logo, Title and Description Verification- Start");
			bcsp.verifyComponentExistence(bcsp.homePageLogo);
			bcsp.VerifyText(bcsp.homePageTitle1,prop.getProperty("HOMEPAGE_TITLE1"));
			bcsp.VerifyText(bcsp.homePageTitle2,prop.getProperty("HOMEPAGE_TITLE2"));
			bcsp.VerifyText(bcsp.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"));
			logger.write("Home Page Log,Title and Description Verification- Complete");
		
			logger.write("Home Page Left Menu Items Verification- Starts");
			bcsp.leftMenuItemsDisplay(bcsp.leftMenuItems);
			logger.write("Home Page Left Menu Items Verification- Ends");
		
			logger.write("Home  Page Supply Detail Verification- Starts");
			bcsp.VerifyText(bcsp.supplyBillTitle,prop.getProperty("SUPPLY_BILL_TITLE"));
			//bcsp.VerifyText(bcsp.customerLightSupply, prop.getProperty("CUSTOMER_LIGHT_SUPPLY"));
			bcsp.VerifyText(bcsp.supplyHeading, prop.getProperty("SUPPLY_DETAIL_HEADING"));
		//	bcsp.supplyDetailsDisplay(bcsp.supplyDetails);
			logger.write("Home Page Supply Detail Verification- Ends");	
			
			 logger.write("Bill Details Verification- Start");
			 bcsp.VerifyText(bcsp.billDetails, prop.getProperty("BILL_DETAILS"));
			 bcsp.supplyBillDetailsDisplay(bcsp.billTable);
			 logger.write("Bill Details Verification- Complete");
			 
			 bcsp.clickComponent(bcsp.piuInformazionlink);
			Thread.sleep(6000);
				
			logger.write("Fornitura Home Page Title and Description Verification- Start");
			bcsp.VerifyText(bcsp.supplyHomePageTitle1,prop.getProperty("SUPPLY_HOMEPAGE_TITLE1"));
			//bcsp.VerifyText(bcsp.supplyHomePageTitle2,prop.getProperty("SUPPLY_HOMEPAGE_TITLE2"));
			bcsp.VerifyText(bcsp.SupplyTitle1,prop.getProperty("SUPPLY_DETAIL_TITLE1"));
			bcsp.VerifyText(bcsp.supplyDescription1,prop.getProperty("SUPPLY_DETAIL_DESCRIPTION1"));
			logger.write("Fornitura Home Page Log,Title and Description Verification- Complete");
			 
			 
			 logger.write("Click on Modifica link- Start");
			 bcsp.VerifyText(bcsp.supplyDetails1, prop.getProperty("SUPPLY_DETAILS1"));
			 bcsp.verifyComponentExistence(bcsp.modificaLink);
			 bcsp.clickComponent(bcsp.modificaLink);
			 logger.write("Click on Modifica link- Complete");
			
			 logger.write("Enter the value and click on Salvi button - Start");
			 bcsp.enterLoginParameters(bcsp.FornituraField, prop.getProperty("AGGIUNITEXTVALUE1"));
			 bcsp.verifyComponentExistence(bcsp.SalviLink);
			 bcsp.clickComponent(bcsp.SalviLink);
			 logger.write("Enter the value and click on Salvi button - Complete");
			 
			 
			 bcsp.verifyComponentExistence(bcsp.modificaLink);
			 bcsp.clickComponent(bcsp.modificaLink);
			 bcsp.verifyComponentExistence(bcsp.SalviLink);
			 bcsp.clickComponent(bcsp.SalviLink);
			 
			 
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
					
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

	}


