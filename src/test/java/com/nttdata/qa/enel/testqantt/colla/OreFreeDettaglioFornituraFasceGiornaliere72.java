package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DetaglioFornitura_GestiscileOreFree67_Component;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioFornituraComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeDettaglioFornituraFasceGiornaliere72 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			OreFreeDettaglioFornituraComponent ofdf = new OreFreeDettaglioFornituraComponent(driver);
			logger.write("premere il tasto 'Gestisci le Ore Free' nella Card della fornitura Ore Free - Start");
			ofdf.verifyComponentExistence(ofdf.oreFreeButton);
			ofdf.clickComponent(ofdf.oreFreeButton);
			logger.write("premere il tasto 'Gestisci le Ore Free' nella Card della fornitura Ore Free - Completed");
			
			logger.write("andare nella sezione 'Fasce Giornaliere', premere sul pulsante MODIFICA relativo al giorno che si intende modificare, sulla data del "+prop.getProperty("DAY_TO_BE_ADDED")+"° giorno rispetto alla data odierna e verificare che il tasto MODIFICA sia disabilitato - Start");
			ofdf.verifyComponentExistence(ofdf.ripristinaFasciaBaseFasceGiornaliereButton);
			
			ofdf.verifyComponentExistence(ofdf.fasceGiornaliereHeader);
			ofdf.comprareText(ofdf.fasceGiornaliereHeader, ofdf.fasceGiornaliereHeaderText, true);
			
			Thread.currentThread().sleep(10000);
			ofdf.clickModificaComponentByDatePerFasciaDisabilitata(ofdf.modificaFasciaGiornalieraButtonDisabilitato, Integer.parseInt(prop.getProperty("DAY_TO_BE_ADDED")));
			ofdf.verifyComponentNotExistence(ofdf.popup);
			logger.write("andare nella sezione 'Fasce Giornaliere', premere sul pulsante MODIFICA relativo al giorno che si intende modificare, sulla data del "+prop.getProperty("DAY_TO_BE_ADDED")+"° giorno rispetto alla data odierna e verificare che il tasto MODIFICA sia disabilitato - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
