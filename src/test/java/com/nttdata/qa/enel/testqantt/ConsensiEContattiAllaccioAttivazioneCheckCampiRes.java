package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class ConsensiEContattiAllaccioAttivazioneCheckCampiRes {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);
        SeleniumUtilities util;

        try {


            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            util = new SeleniumUtilities(driver);

            ContattiConsensiComponent contatti = new ContattiConsensiComponent(driver);
            //Verifica Preselezione Accettazione Condizioni Contrattuali
            contatti.verificaPreselezioneAccCondizPrecontattuali();
            logger.write("Verifica Preselezione Accettazione Condizioni Contrattuali");

            if (prop.getProperty("USO_FORNITURA").compareTo("Uso Abitativo") == 0) {
                //Verifica  Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?
                if (!util.exists(contatti.accettaComprensioneCondContrat, 60)) {
                    throw new Exception("Non è presente l'informativa Verifica informativa 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?'");
                }
                logger.write("Verifica informativa 'Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?'");
            }

            //Verifica presenza campo telefono e in read-only

            contatti.verificaSeNonAbilitato("Telefono");
            logger.write("Verifica campo Telefono in read only");

            //Verifica presenza campo cellulare e in read-only
            contatti.verificaSeNonAbilitato("Cellulare");
            logger.write("Verifica campo Cellulare in read only");

            //Verifica presenza campo email e in read-only
            contatti.verificaSeNonAbilitato("Email");
            logger.write("Verifica campo Email in read only");

            //Verifica presenza campo PEC e in read-only
            contatti.verificaSeNonAbilitato("PEC");
            logger.write("Verifica campo PEC in read only");

            //Verifica presenza campo FAX e in read-only
            contatti.verificaSeNonAbilitato("FAX");
            logger.write("Verifica campo FAX in read only");

            //Verifica presenza campo Fascia contattabilità e in read-only
            contatti.verificaSeNonAbilitato("Fascia Contattabilità");
            logger.write("Verifica campo Fascia Contattabilità in read only");

            //Verifica presenza campo Consenso Profilazione e in read-only
            contatti.verificaSeNonAbilitato("Consenso Profilazione");
            logger.write("Verifica campo Consenso Profilazione in read only");

            //Verifica presenza campo Consenso Marketing Telefonico e in read-only
            contatti.verificaSeNonAbilitato("Consenso Marketing Telefonico");
            logger.write("Verifica campo Consenso Marketing Telefonico in read only");

            //Verifica presenza campo Consenso Marketing Cellulare e in read-only
            contatti.verificaSeNonAbilitato("Consenso Marketing Cellulare");
            logger.write("Verifica campo Consenso Marketing Cellulare in read only");

            //Verifica presenza campo Consenso Marketing Email e in read-only
            contatti.verificaSeNonAbilitato("Consenso Marketing Email");
            logger.write("Verifica campo Consenso Marketing Email in read only");

            //Verifica presenza campo Consenso Terzi Telefonico e in read-only
            contatti.verificaSeNonAbilitato("Consenso Terzi Telefonico");
            logger.write("Verifica campo Consenso Terzi Telefonico in read only");

            //Verifica presenza campo Consenso Terzi Cellulare e in read-only
            contatti.verificaSeNonAbilitato("Consenso Terzi Cellulare");
            logger.write("Verifica campo Consenso Terzi Cellulare in read only");

            //Verifica presenza campo Consenso Terzi Email e in read-only
            contatti.verificaSeNonAbilitato("Consenso Terzi Email");
            logger.write("Verifica campo Consenso Terzi Email in read only");


//			 //Accettazione Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?
//
//			 contatti.accettaComprensioneCondizioniContrattuali();
//			 logger.write("Selezione Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?");

            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
