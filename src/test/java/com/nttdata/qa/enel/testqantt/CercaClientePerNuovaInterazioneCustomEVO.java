package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.CercaClienteComponentEVO;
import com.nttdata.qa.enel.components.lightning.CreaNuovaInterazioneComponentEVO;
import com.nttdata.qa.enel.components.lightning.SelezionaClienteAttivoComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class CercaClientePerNuovaInterazioneCustomEVO {

    @Step("Cerca Cliente Per Nuova Iterazione")
    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            CreaNuovaInterazioneComponentEVO startIterationPage = new CreaNuovaInterazioneComponentEVO(driver);
            CercaClienteComponentEVO searchClientPage = new CercaClienteComponentEVO(driver);
            SelezionaClienteAttivoComponentEVO selectCliente = new SelezionaClienteAttivoComponentEVO(driver);

            startIterationPage.createNewiteration();
            logger.write("Creazione nuova Interazione");

            searchClientPage.fillCfAndSearch2(prop.getProperty("CODICE_FISCALE"));
            logger.write("GetProp: CODICE FISCALE");


            logger.write("Select client start");
            selectCliente.fillCfAndSearchLastElementinTable();
            logger.write("Select client completed");



            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }

    }
}
