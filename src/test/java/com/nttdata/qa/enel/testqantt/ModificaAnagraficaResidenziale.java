package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaDatiContatto;
import com.nttdata.qa.enel.components.lightning.ConfermaButtonComponent;
import com.nttdata.qa.enel.components.lightning.GestioneConsensiComponent;
import com.nttdata.qa.enel.components.lightning.GestioneRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.ModificaClienteComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaCommodityComponent;
import com.nttdata.qa.enel.components.lightning.SelezionaServizioVASComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.CodiceFiscale;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModificaAnagraficaResidenziale {

	public static void main(String[] args) throws Exception {

		Properties prop = null;

		try {
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
				SceltaProcessoComponent sceltaProcesso = new SceltaProcessoComponent(driver);
				sceltaProcesso.clickAllProcess();
				sceltaProcesso.chooseProcessToStart(sceltaProcesso.avvioModificaAnagrafica);
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
				
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
				identificaInterlocutore.verifyInputFieldExist("Documento");
				identificaInterlocutore.verifyInputFieldExist("Indirizzo di fornitura");
				identificaInterlocutore.verifyInputFieldExist("Numero Cliente");
				identificaInterlocutore.verifyInputFieldExist("Numero fattura");
				
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
				
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Nome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Cognome");
				identificaInterlocutore.verifyInputFieldIsNotEmpty("Codice Fiscale");
				logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Completed");
				
				logger.write("Inserimento documento e conferma - Start");
				identificaInterlocutore.insertDocumento("b");
				identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
				identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
				logger.write("Inserimento documento e conferma - Completed");

				TimeUnit.SECONDS.sleep(15);
				CheckListComponent checklist = new CheckListComponent(driver);
				String frame = checklist.clickFrameConferma2();	
				IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
				String numerorichiesta = nuovaRichiesta
				.salvaNumeroRichiesta(frame,nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta2);
	
	         	prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());

				ModificaClienteComponent modifica = new ModificaClienteComponent(driver);
				modifica.verificaControlloForniture(frame);
				String nome = modifica.modificaNomeEInserisci(frame, modifica.recuperaNome(frame));
				String codice = modifica.recuperaCF(frame);
				CodiceFiscale a = new CodiceFiscale();
				String cfnome = a.modificaNC(nome, true).toUpperCase();
				String newCode = codice.substring(0, 3) + cfnome + codice.substring(6, codice.length());
				prop.setProperty("NUOVO_CODICE_FISCALE", newCode);
				modifica.inserisciCf(frame, newCode);
				modifica.modificaDatiDiContatto(frame, Utility.getPhoneNumber(), "Seconda Casa", Utility.getMobileNumber(), "Privato",
						"aaaaa@tin.it", "Privato");
				modifica.pressButton(frame, modifica.buttonConferma);

			prop.setProperty("RETURN_VALUE", "OK");
			}
			catch (Exception e) 
			{
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();

				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
				if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

			}finally
			{
				//Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
			}
		}
	}

