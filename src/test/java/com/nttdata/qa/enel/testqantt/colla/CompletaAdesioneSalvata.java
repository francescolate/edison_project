package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AdesioneContrattoComponent;
import com.nttdata.qa.enel.components.colla.LoginPageValidateComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CompletaAdesioneSalvata {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getDriverInstance(prop);
		AdesioneContrattoComponent acc = new AdesioneContrattoComponent(driver);

		try {
			String link = null;			
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
	            logger.write("Recupera Link - Start");
	            GmailQuickStart gm = new GmailQuickStart();
	    		link = gm.recuperaLink(prop, "COMPLETA_ADESIONE_SALVATA", false);
	    		//link = gm.recuperaLink(prop, "RECOVER_PASSWORD_OTP", false);
	    		prop.setProperty("LINK_CONTINUA", link);
	            logger.write("Recupera Link - Completed");
	            Thread.sleep(5000); 
			}
            try {
				driver.manage().window().maximize();
				driver.navigate().to(prop.getProperty("LINK_CONTINUA"));
            } catch (Exception e) {
				throw new Exception("it's impossible the link " + prop.getProperty("LINK_CONTINUA"));
			}
            
			acc.checkText(prop.getProperty("FIRSTNAME") + " " + prop.getProperty("LASTNAME"));
			acc.clickComponent(acc.richiediCodiceButton);
			acc.checkVerifyCode();
			
			acc.insertCodeAndVerify("271591");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("OTP non valido"))
				throw new Exception("messaggio di errore non valido");
			
			Thread.sleep(3000);
			acc.insertCodeAndVerify("1");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("OTP non valido"))
				throw new Exception("messaggio di errore non valido");
			
			Thread.sleep(3000);
			acc.insertCodeAndVerify("");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("Inserire codice di verifica"))
				throw new Exception("messaggio di errore non valido");
			
			Thread.sleep(3000);
			acc.insertCodeAndVerify("ABCS12");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("Errore generico"))
				throw new Exception("messaggio di errore non valido");
			
			Thread.sleep(3000);
			acc.insertCodeAndVerify("01010101010");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("OTP non valido"))
				throw new Exception("messaggio di errore non valido");
			
			Thread.sleep(3000);
			acc.insertCodeAndVerify("01010101010");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("OTP non valido"))
				throw new Exception("messaggio di errore non valido");
			
			Thread.sleep(3000);
			acc.insertCodeAndVerify("01010101010");
			Thread.sleep(3000);
			if(!acc.verifyError(driver).equals("Hai superato il numero di tentativi a tua disposizione. Riprova a breve."))
				throw new Exception("messaggio di errore non valido");
			

		prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());
	
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
	
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
	


}
