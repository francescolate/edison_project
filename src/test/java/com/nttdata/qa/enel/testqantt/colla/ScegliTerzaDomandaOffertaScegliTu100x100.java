package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DomandaRisposteScegliTu100x100Component;
import com.nttdata.qa.enel.components.colla.PaginaAdesionePodComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ScegliTerzaDomandaOffertaScegliTu100x100 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			DomandaRisposteScegliTu100x100Component domanda=new DomandaRisposteScegliTu100x100Component(driver);
			logger.write("verifica apertura pagina TROVA LA TUA SOLUZIONE 1 / 3 con la visualizzazione di tre risposte da poter selezionare - Start");
			By label_TrovaLaSoluzione=domanda.labelTrovaLaSoluzione;
			domanda.verifyComponentExistence(label_TrovaLaSoluzione);
			
			By page_Number1=domanda.pageNumber1;
			domanda.verifyComponentExistence(page_Number1);
			
			By question=domanda.domanda;
			domanda.verifyComponentExistence(question);
			
			By lista_risposte=domanda.listarisposte;
			domanda.verificaEsistenzaRisposte(domanda.risposte,lista_risposte);
			logger.write("verifica apertura pagina TROVA LA TUA SOLUZIONE 1 / 3 con la visualizzazione di tre risposte da poter selezionare - Completed");
			logger.write("Selezionare la risposta C "+prop.getProperty("RISPOSTA_C1")+" - Start");
			domanda.scrollComponent(lista_risposte);
			TimeUnit.SECONDS.sleep(2);
			domanda.scegliRisposta(lista_risposte, prop.getProperty("RISPOSTA_C1"));
			logger.write("Selezionare la risposta C "+prop.getProperty("RISPOSTA_C1")+" - Completed");
			
			logger.write("verifica apertura pagina TROVA LA TUA SOLUZIONE 2 / 3 con la visualizzazione di tre risposte da poter selezionare - Start");
			domanda.verifyComponentExistence(label_TrovaLaSoluzione);
			
			By page_Number2=domanda.pageNumber2;
			domanda.verifyComponentExistence(page_Number2);
			
			By domanda_2=domanda.domanda2;
			domanda.verifyComponentExistence(domanda_2);
			
			By testo_2=domanda.testo2;
			domanda.verifyComponentExistence(testo_2);
			
			By lista_risposte2=domanda.listarisposte2;
			
			domanda.verificaEsistenzaRisposte(domanda.risposte2,lista_risposte2);
			logger.write("verifica apertura pagina TROVA LA TUA SOLUZIONE 2 / 3 con la visualizzazione di tre risposte da poter selezionare - Completed");
			logger.write("Selezionare la risposta C "+prop.getProperty("RISPOSTA_C2")+" - Start");
			domanda.scrollComponent(lista_risposte2);
			TimeUnit.SECONDS.sleep(2);
			domanda.scegliRisposta(lista_risposte2, prop.getProperty("RISPOSTA_C2"));
			logger.write("Selezionare la risposta C "+prop.getProperty("RISPOSTA_C2")+" - Completed");
			
			logger.write("verifica apertura pagina TROVA LA TUA SOLUZIONE 3 / 3 con la visualizzazione di tre risposte da poter selezionare - Start");
			domanda.verifyComponentExistence(label_TrovaLaSoluzione);
			
			By page_Number3=domanda.pageNumber3;
			domanda.verifyComponentExistence(page_Number3);
			
			By domanda_3=domanda.domanda3;
			domanda.verifyComponentExistence(domanda_3);
						
			By lista_risposte3=domanda.listarisposte3;
			
			domanda.verificaEsistenzaRisposte(domanda.risposte3,lista_risposte3);
			logger.write("verifica apertura pagina TROVA LA TUA SOLUZIONE 3 / 3 con la visualizzazione di tre risposte da poter selezionare - Completed");
		
			logger.write("Selezionare la risposta B "+prop.getProperty("RISPOSTA_B")+" - Start");
			TimeUnit.SECONDS.sleep(1);
			domanda.scegliRisposta(lista_risposte3, prop.getProperty("RISPOSTA_B"));
			logger.write("Selezionare la risposta B "+prop.getProperty("RISPOSTA_B")+" - Completed");
			
			logger.write("verifica apertura pagina 'Ecco la soluzione migliore per te!' - Start");
			TimeUnit.SECONDS.sleep(3);
			By titoloPage=domanda.titolo;
			domanda.verifyComponentExistence(titoloPage);
			
			By sotto_titolo=domanda.sottotitolo;
			domanda.verifyComponentExistence(sotto_titolo);
			
			By objecttext=domanda.testo;
			String text=domanda.text.replace("$", "0,0 €/kWh").replace("£", "0,1737 €/kWh");
			//domanda.verificaTesto(objecttext,text);
			domanda.compareText(objecttext,text, true);
			logger.write("verifica apertura pagina 'Ecco la soluzione migliore per te!' - Completed");
			
			logger.write("click su ATTIVA ORA e verifica apertura della pagina di adesione - Start");
			By button_AttivaOra=domanda.buttonAttivaOra;
			domanda.verifyComponentExistence(button_AttivaOra);
		
			domanda.checkColorBackground(button_AttivaOra, "DeepPink", "sfondo del pulsante ATTIVA ORA");
			domanda.clickeaspetta(button_AttivaOra);
			//TimeUnit.SECONDS.sleep(15);
			By titolo_Pagina=domanda.titoloPagina;
			domanda.verifyComponentExistence(titolo_Pagina);
			
			/*
			
			PaginaAdesionePodComponent pod=new PaginaAdesionePodComponent(driver);
			
			By testo_con_pod=pod.testoconpod;
			pod.verificaTesto(testo_con_pod, pod.testoconIconaInformativaPod2);
			logger.write("click su ATTIVA ORA e verifica apertura della pagina di adesione - Completed");
			
			logger.write("click sull'icona 'i' accanto al POD e verifica sulla popup informativa che si apre - Start");
			By i_pod=pod.ipod;
			pod.clickComponent(i_pod);
			TimeUnit.SECONDS.sleep(3);
			By titolo_IpodPopup=pod.titoloIpodPopup;
			pod.verificaTesto(titolo_IpodPopup,pod.titoloPod);
			
			By testo_ipod=pod.testoipod;
			pod.verificaTesto(testo_ipod,pod.descrizionePod);
			
			By chiudi_ipod=pod.chiudiipod;
			pod.clickComponent(chiudi_ipod);
			TimeUnit.SECONDS.sleep(1);
			domanda.verifyComponentExistence(titolo_Pagina);
			logger.write("click sull'icona 'i' accanto al POD e verifica sulla popup informativa che si apre - Completed");
			
			logger.write("click sull'icona 'i' accanto al 'dati dell'immobile' e verifica sulla popup informativa che si apre - Start");
			By pulsanteInformativo_DatiImmobile=pod.pulsanteInformativoDatiImmobile;
			pod.clickComponent(pulsanteInformativo_DatiImmobile);
			TimeUnit.SECONDS.sleep(3);
			
			By titolo_DatiImmobilePopup=pod.titoloDatiImmobilePopup;
			pod.verificaTesto(titolo_DatiImmobilePopup,pod.titoloDatiImmobile);
			
			By testo_DatiImmobile=pod.testoDatiImmobile;
			pod.verificaTesto(testo_DatiImmobile,pod.descrizioneDatiImmobile);
			
			By chiudiPopup_DatiImmobile=pod.chiudiPopupDatiImmobile;
			pod.clickComponent(chiudiPopup_DatiImmobile);
			TimeUnit.SECONDS.sleep(1);
			domanda.verifyComponentExistence(titolo_Pagina);
			logger.write("click sull'icona 'i' accanto al 'dati dell'immobile' e verifica sulla popup informativa che si apre - Completed");
			
			*/
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
