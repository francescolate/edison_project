package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.AccountRegistrationComponent;
import com.nttdata.qa.enel.components.colla.ContinueWithRegistrationComponent;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.components.colla.RegistrationTypeChoiceComponent;
import com.nttdata.qa.enel.components.colla.RegistrazioneComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.GmailQuickStart;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Id_38_Registrazione_BSN {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			//CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			
			//String email = cealc.getEmailAddress();
			//email = cealc.getEmailAddress();
			//System.out.println(email);
			
			prop.setProperty("MOBILE_NUMBER", Utility.getMobileNumber());
			
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			RegistrazioneComponent reg = new RegistrazioneComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
						
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(10000);
			log.clickComponent(log.buttonAccetta);
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("Verify login page fields - Start");
			By pageLogin = log.loginPage;
			Thread.sleep(10000);
			log.verifyComponentExistence(pageLogin);				
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.verifyComponentExistence(log.recoverPasswordBtn);
			log.verifyComponentExistence(log.recoverUsernameBtn);
			//log.verifyComponentExistence(log.accessProblemsLabel);
			log.verifyComponentExistence(log.accessProblemsBtn);
			log.verifyComponentExistence(log.googleAccessBtn);
			log.verifyComponentExistence(log.facebookAccessBtn);
			logger.write("Verify login page fields - Completed");
			
			logger.write("Click on Register button - Start");
			Thread.sleep(5000);
			log.clickComponent(log.registerBtn);
			logger.write("Click on Register button - Completed");
			
			RegistrationTypeChoiceComponent rc = new RegistrationTypeChoiceComponent(driver);
			rc.verifyComponentExistence(rc.welcomeMessageLabel);
			reg.comprareText(rc.welcomeMessageLabel, rc.WelcomeMsg, true);
			rc.verifyComponentExistence(rc.pageHeader);
			reg.comprareText(rc.pageHeader, rc.PageTitle, true);
			rc.verifyComponentExistence(rc.privateSelector);
			rc.verifyComponentExistence(rc.businessSelector);
		//	rc.checkContinueButtonIsDisabled();
			Thread.sleep(5000);
			rc.clickComponent(rc.impresse);
			rc.clickComponent(rc.continueBtn);

			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			reg.verifyComponentExistence(reg.ulterioriInformazioniLink);
			Thread.sleep(5000);
			reg.clickComponent(reg.ulterioriInformazioniLink);
			reg.verifyComponentExistence(reg.cosEilProfiloUnico);
			reg.comprareText(reg.cosEilProfiloUnicoText, reg.CosEilProfiloUnicoText, true);
			
			reg.clickComponent(reg.cosEilProfiloUnicoClose);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			
			reg.verifyComponentExistence(reg.credenziali);
			reg.verifyComponentExistence(reg.email);
			reg.verifyComponentExistence(reg.confirmEmail);
			reg.verifyComponentExistence(reg.password);
			reg.verifyComponentExistence(reg.confirmPassword);
			Thread.sleep(5000);
			reg.comprareText(reg.confirmPswText, reg.ConfirmPswText, true);
			
			reg.verifyComponentExistence(reg.datiAnagrafici);
			reg.verifyComponentExistence(reg.nome);
			reg.verifyComponentExistence(reg.cognome);
			
			reg.verifyComponentExistence(reg.condizioniGenerali);
			reg.verifyComponentExistence(reg.checkBox1Text);
			reg.verifyComponentExistence(reg.checkBox2Text);
			reg.verifyComponentExistence(reg.campoObbligatorio);
			reg.verifyComponentExistence(reg.registrationButton);
			
			Thread.sleep(4000);
			reg.jsClickObject(reg.condizioniGenaraliPolicy);
			reg.verifyComponentExistence(reg.condizioniGeneraliTitle);
		//	reg.comprareText(reg.condizioniGeneraliContent, reg.CondizioniGeneraliContent, true);
			reg.clickComponent(reg.condizioniGenaraliPolicyClose);
			Thread.sleep(3000);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			
			Thread.sleep(3000);
			reg.jsClickObject(reg.informativaPrivacy);
			reg.verifyComponentExistence(reg.informativaPrivacyTitle);
			//reg.comprareText(reg.informativaPrivacyContent, reg.InformativaPrivacyContent, true);
			reg.clickComponent(reg.informativaPrivacyClose);
			Thread.sleep(3000);
			reg.comprareText(reg.pageTitle, reg.PageTitle, true);
			reg.comprareText(reg.pageText, reg.PageText, true);
			
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.emailError, reg.ErrorMsg, true);
			reg.comprareText(reg.confirmEmailError, reg.ErrorMsg, true);
			reg.comprareText(reg.passwordError, reg.ErrorMsg, true);
			reg.comprareText(reg.confirmPasswordError, reg.ErrorMsg, true);
			reg.comprareText(reg.nomeError, reg.ErrorMsg, true);
			reg.comprareText(reg.cognomeError, reg.ErrorMsg, true);
			reg.comprareText(reg.cfError, reg.ErrorMsg, true);
			reg.comprareText(reg.checkBox1Error, reg.ErrorMsg, true);
			reg.comprareText(reg.checkBox2Error, reg.ErrorMsg, true);

			String Invalid_Email = "iii@@@";
			reg.enterInputParameters(reg.email, Invalid_Email);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidEmailError, reg.InvalidEmailErrorMsg, true);
			
			reg.enterInputParameters(reg.confirmEmail, Invalid_Email);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidConfirmEmailError, reg.InvalidEmailErrorMsg, true);
			
			reg.enterInputParameters(reg.email, prop.getProperty("WP_USERNAME"));
			reg.enterInputParameters(reg.confirmEmail, Invalid_Email);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidConfirmEmailError, reg.InvalidEmailErrorMsg, true);
			reg.enterInputParameters(reg.confirmEmail, prop.getProperty("WP_USERNAME"));
			reg.clickComponent(reg.registrationButton);

			//reg.checkFieldDisplay(reg.emailError);
			//reg.checkFieldDisplay(reg.confirmEmailError);
			
			String Invalid_Password = "123";
			reg.enterInputParameters(reg.password, Invalid_Password);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.invalidPasswordError, reg.InvalidPswErrorMsg, true);
			
			reg.enterInputParameters(reg.confirmPassword, Invalid_Password);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.confirmPasswordError, reg.InvalidPswErrorMsg, true);
			
			String INVALID_PSW = "123";
			reg.enterInputParameters(reg.password, prop.getProperty("WP_PASSWORD"));
			reg.enterInputParameters(reg.confirmPassword, INVALID_PSW);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.confirmPasswordError, reg.InvalidPswErrorMsg, true);

			reg.enterInputParameters(reg.confirmPassword, prop.getProperty("WP_PASSWORD"));
			reg.clickComponent(reg.registrationButton);
			//reg.checkFieldDisplay(reg.passwordError);
			//reg.checkFieldDisplay(reg.confirmPasswordError);
			
			String Invalid_Name = "123";
			reg.enterInputParameters(reg.nome, Invalid_Name);
			reg.enterInputParameters(reg.cognome, Invalid_Name);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.nomeError, reg.InvalidNomeErrorMsg, true);
			reg.comprareText(reg.cognomeError, reg.InvalidCogNomeErrorMsg, true);
			
			reg.enterInputParameters(reg.nome, prop.getProperty("ACCOUNT_FIRSTNAME"));
			reg.enterInputParameters(reg.cognome, prop.getProperty("ACCOUNT_LASTNAME"));
			reg.clickComponent(reg.registrationButton);
			
			String Invalid_CF = "PTRSLL45R62F101X";
			reg.enterInputParameters(reg.denominazione, Invalid_CF);
			reg.clickComponent(reg.registrationButton);
			//reg.comprareText(reg.cf_error, reg.CFErrorMsg, true);
			
			String Incorrect_CF = "0123456789";
			reg.enterInputParameters(reg.cf_input, Incorrect_CF);
			reg.clickComponent(reg.cf_input);
			reg.clickComponent(reg.registrationButton);
			reg.comprareText(reg.cf_error, reg.CFErrorMsg, true);
			
//			reg.enterInputParameters(reg.piva, Incorrect_CF);
//			reg.clickComponent(reg.registrationButton);
//			reg.comprareText(reg.piva_error, reg.PIVAErrorMsg, true);
			
			reg.enterInputParameters(reg.cf_input, prop.getProperty("PIVA"));
//			reg.enterInputParameters(reg.piva, prop.getProperty("PIVA"));
			reg.enterInputParameters(reg.denominazione, prop.getProperty("COMPANY_NAME"));
			reg.clickComponent(By.id("modal-cf-button-si"));
			reg.clickComponent(reg.infoCheckBox);
			reg.clickComponent(reg.condizioniCheckBox);
			reg.clickComponent(reg.registrationButton);
			
			reg.verifyComponentExistence(reg.registraAccountTitle);
			reg.comprareText(reg.registraAccountPath, reg.RegistraAccountPath, true);
			reg.comprareText(reg.registraAccountTitle, reg.RegistraAccountTitle, true);
			reg.verifyComponentExistence(reg.CompletailtuoprofiloTitle);
			reg.comprareText(reg.completailtuoprofiloText, reg.CompletailtuoprofiloText, true);
			reg.verifyComponentExistence(reg.numeroDiCellulare);
			reg.verifyComponentExistence(reg.prefissoTelefonico);
			reg.comprareText(reg.registraAccountPageText, reg.RegistraAccountPageText, true);
			reg.verifyComponentExistence(reg.continuaButton);
			reg.verifyComponentExistence(reg.saltaButton);
			reg.clickComponent(reg.qui);
			
			String currentHandle ="";
			Set <String> windows = driver.getWindowHandles();
			 currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
			   
		        String pagetitle = driver.getTitle();
			    driver.switchTo().window(winHandle);
			 }
			reg.verifyComponentExistence(reg.faqPagePath);
			reg.containsText(reg.faqPagePath, reg.FaqPagePath);
			reg.comprareText(reg.faqPageTitle, reg.FaqPageTitle, true);
			reg.checkURLAfterRedirection(reg.faqUrl);
			driver.close();
			driver.switchTo().window(currentHandle);

			String Incorrect_Cellulare = "32090901111111111111111111111";
			reg.enterInputParameters(reg.numeroDiCellulare, Incorrect_Cellulare);
			reg.clickComponent(reg.continuaButton);
			reg.verifyComponentExistence(reg.numeroCellulareErrorTitle);
			reg.comprareText(reg.numeroCellulareErrorTitle, reg.NumeroCellulareErrorTitle, true);
			reg.comprareText(reg.numeroCellulareErrorSubTitle, reg.NumeroCellulareErrorSubTitle, true);
			reg.enterInputParameters(reg.numeroDiCellulare, prop.getProperty("MOBILE_NUMBER"));
			reg.jsClickObject(reg.continuaButton);
			Thread.sleep(5000);
			
			reg.verifyComponentExistence(reg.registraAccountPath1);
			reg.verifyComponentExistence(reg.uniqueProfileTitle);
			reg.comprareText(reg.uniqueProfileTitle, reg.UniqueProfileTitle, true);
			reg.verifyComponentExistence(reg.codiceDiSicurezza);
			reg.comprareText(reg.disclaimerOTPText, reg.DisclaimerOTPText, true);
			reg.verifyComponentExistence(reg.SMSPin);
			reg.verifyComponentExistence(reg.OTPContinua);
			reg.verifyComponentExistence(reg.InviaNuovoPIN);
			reg.verifyComponentExistence(reg.otpText);
			reg.verifyComponentExistence(reg.link);
			String otp = APIService.getOTP(prop.getProperty("WP_USERNAME"), prop.getProperty("MOBILE_NUMBER"));
			
			reg.verifyComponentExistence(reg.SMSPin);
			reg.fillInputField(reg.SMSPin, otp);
			reg.clickComponent(reg.OTPContinua);
			reg.verifyComponentExistence(reg.thanksForRegistering);
//			reg.clickComponent(reg.thanksForRegisteringButton);
			
			Thread.sleep(30000);
		
//			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//
//				prop.setProperty("USERNAME",Costanti.utenza_Tp2_crm);
//				prop.setProperty("PASSWORD",Costanti.password_Tp2_crm);
//			}
//			
//			String link = null;			
//			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//	            logger.write("Recupera Link - Start");
//	            GmailQuickStart gm = new GmailQuickStart();
//	    		link = gm.recuperaLink(prop, "NEW_ACCOUNT", false);
//	            logger.write("Recupera Link - Completed");
//	            Thread.sleep(5000);
//	            AccountRegistrationComponent arc = new AccountRegistrationComponent(driver);
//	            arc.launchLink(link);
//	            Thread.sleep(15000);
//	            arc.verifyComponentExistence(arc.profiloUnicoCreatoHeader);
//			}
//			
//			reg.verifyComponentExistence(reg.attivazioneAccount);
//			reg.verifyComponentExistence(reg.profileUnique);
//			reg.comprareText(reg.Ricordacheora, reg.RICORDACHEORA, true);
//			reg.clickComponent(reg.uniqueProfileContinua);
//			
//			By accessLoginPage = null;
//			
//			log.verifyComponentExistence(user);
//			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));
//			
//			log.verifyComponentExistence(pw);
//			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));
//
//			log.verifyComponentExistence(accedi);
//			log.clickComponent(accedi); 
//			
//			
////			try{
////				if (!prop.getProperty("AREA_CLIENTI").equals(""))
////				{
////					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
////					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
////						log.verifyComponentExistence(log.areaClientiCasa);
////						log.clickComponent(log.areaClientiCasa);
////					}
////					else
////					{
////						log.verifyComponentExistence(log.areaClientiImpresa);
////						log.clickComponent(log.areaClientiImpresa);
////					}
////					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
////				} else {
////					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
////				}
////			}catch(Exception e){
////				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
////			}
//			
//			ContinueWithRegistrationComponent cwrc = new ContinueWithRegistrationComponent(driver);
//			
//			cwrc.verifyComponentExistence(cwrc.primoAccessoHeader);
//			cwrc.verifyComponentExistence(cwrc.numeroClienteInputField);
//			cwrc.fillInputField(cwrc.numeroClienteInputField, prop.getProperty("SUPPLY_NUMBER"));
//			cwrc.verifyComponentExistence(cwrc.numeroClienteContinuaButton);
//			cwrc.clickComponent(cwrc.numeroClienteContinuaButton);
//			cwrc.verifyComponentExistence(cwrc.numeroClienteConfermaButton);
//			cwrc.clickComponent(cwrc.numeroClienteConfermaButton);
//			cwrc.verifyComponentExistence(cwrc.numeroClienteFineButton);
//			cwrc.clickComponent(cwrc.numeroClienteFineButton);
//			
//			try{
//				if (!prop.getProperty("AREA_CLIENTI").equals(""))
//				{
//					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
//					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
//						log.verifyComponentExistence(log.areaClientiCasa);
//						log.clickComponent(log.areaClientiCasa);
//					}
//					else
//					{
//						log.verifyComponentExistence(log.areaClientiImpresa);
//						log.clickComponent(log.areaClientiImpresa);
//					}
//					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
//				}
//				if(!prop.getProperty("ACCOUNT_TYPE").equals(""))
//					if(prop.getProperty("ACCOUNT_TYPE").contains("BSN"))
//						if(!prop.getProperty("AREA_CLIENTI").equals(""))
//							if(prop.getProperty("AREA_CLIENTI").equals("CASA"))
//								accessLoginPage=log.loginSuccessful;
//							else
//								accessLoginPage=log.loginBSNSuccessful;
//						else
//							accessLoginPage=log.loginBSNSuccessful;
//					else
//						accessLoginPage=log.loginSuccessful;
//				else
//					accessLoginPage=log.loginSuccessful;
//				Thread.sleep(5000);
//			log.verifyComponentExistence(accessLoginPage);
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
//		
//			PrivateArea_Imprese_HomePageBSNComponent hm =new PrivateArea_Imprese_HomePageBSNComponent(driver);
//			hm.verifyComponentExistence(hm.logout);
//			hm.clickComponent(hm.logout);
//			
//			log.clickComponent(log.iconUser);
//			log.enterLoginParameters(log.username, prop.getProperty("WP_USERNAME"));
//			log.enterLoginParameters(log.password, prop.getProperty("WP_PASSWORD"));
//			log.clickComponent(log.buttonLoginAccedi);
//			
//			try{
//				if (!prop.getProperty("AREA_CLIENTI").equals(""))
//				{
//					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
//					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
//						log.verifyComponentExistence(log.areaClientiCasa);
//						log.clickComponent(log.areaClientiCasa);
//					}
//					else
//					{
//						log.verifyComponentExistence(log.areaClientiImpresa);
//						log.clickComponent(log.areaClientiImpresa);
//					}
//					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
//				} else {
//					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
//				}
//			}catch(Exception e){
//				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
//			}
//			
//			if(!prop.getProperty("ACCOUNT_TYPE").equals(""))
//				if(prop.getProperty("ACCOUNT_TYPE").contains("BSN"))
//					if(!prop.getProperty("AREA_CLIENTI").equals(""))
//						if(prop.getProperty("AREA_CLIENTI").equals("CASA"))
//							accessLoginPage=log.loginSuccessful;
//						else
//							accessLoginPage=log.loginBSNSuccessful;
//					else
//						accessLoginPage=log.loginBSNSuccessful;
//				else
//					accessLoginPage=log.loginSuccessful;
//			else
//				accessLoginPage=log.loginSuccessful;
//			hm.verifyComponentExistence(hm.pageTitle);
//			//Thread.sleep(10000);
//			logger.write("Verifi home page title and details -- Start");
//			hm.comprareText(hm.pageTitle, hm.PageTitle, true);
//		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		
	}
}
