package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BolletteComponent;
import com.nttdata.qa.enel.components.colla.DettaglioBollettaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class DettaglioBolletta_103 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
	
	try {
		
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		prop.setProperty("RESIDENTIAL_MENU", " Forniture | Bollette | Servizi | enelpremia WOW! | Novità | Spazio Video | Account | I tuoi diritti | Supporto | Trova Spazio Enel | Esci ");
		
		BolletteComponent 	bc = new BolletteComponent(driver);	
		
		logger.write("Check for Residential Menu - Start");
		bc.checkForResidentialMenu(prop.getProperty("RESIDENTIAL_MENU"));
		logger.write("Check for Residential Menu - Completed");
		
		logger.write("Click on Bills from Residential Menu - Start");
		bc.clickComponent(bc.bills);
		logger.write("Click on Bills from Residential Menu - Completed");
		
		logger.write("Verification of List of One or more bills assocaited with the supply Luce- Started");
		//bc.verifyComponentExistence(bc.listofBills);
		logger.write("Verification of List of One or more bills assocaited with the supply Luce- Completed");
		
		logger.write("Verification of List of One or more bills assocaited with the supply -gas- Started");
		bc.verifyComponentExistence(bc.listOfBillsGas);
		bc.jsClickObject(bc.plusInGas);
		logger.write("Verification of List of One or more bills assocaited with the supply -gas- Completed");
		
		bc.verifyComponentExistence(bc.moreGas);
				
		logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");
		bc.waitForElementToDisplay(bc.moreGas);
		bc.verifyComponentExistence(bc.moreGas);
		bc.clickComponent(bc.moreGas);
		logger.write("From the box of the single bill select the 3 lateral dots and click on the dots - Start");

		bc.verifyComponentExistence(bc.pagaOnlineGas);		
		bc.verifyComponentExistence(bc.dettaglioBollettaGas);
		bc.verifyComponentExistence(bc.scaricaPDFGas);
				
		logger.write("Click on Dettaglio Bolletta - Start");
		bc.clickComponent(bc.pagaOnlineGas);
		bc.checkURLAfterRedirection(bc.privateAreaOnlinePayment);
		bc.verifyComponentExistence(bc.paymentOnlineHeading);
		bc.comprareText(bc.paymentOnlineHeading, BolletteComponent.PAYMENT_ONLINE_HEADING, true);
		logger.write("Click on Dettaglio Bolletta - Completed");
		
		prop.setProperty("RETURN_VALUE", "OK");

	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

//		prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
	} finally {
		// Store WebDriver Info in properties file
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}
	}
}
