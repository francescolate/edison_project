package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaMarketingConsensus {


	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);

			//STEP 4 Verifica Consensi
			logger.write("checking the menu item Servizi is present within the page - START");
			
			paf.verifyComponentExistence(paf.serviziMenuItem);
			paf.clickComponent(paf.serviziMenuItem);
			
			logger.write("Servizi menu item click  - COMPLETED");

			logger.write("click the card for editing consensus - START");	

			paf.verifyComponentExistence(paf.icon_consensi);
			paf.jsClickComponent(paf.icon_consensi);

			logger.write("click the card for editing consensus - COMPLETED");

			logger.write("click the button for editing consensus - START");	

			By button_change_consensus = paf.button_change_consensus;

			paf.verifyComponentExistence(button_change_consensus);
			paf.clickComponent(button_change_consensus);
			Thread.sleep(30000);

			logger.write("click the button for editing consensus - COMPLETED");	
			logger.write("click the yes radio-button for mobile marketing consensus - START");

			if (prop.getProperty("CONSENSUS_MOBILE").equals("TRUE")) {
				try {
					TimeUnit.SECONDS.sleep(3);
					paf.verifyComponentExistence(paf.input_mobile_marketing);
					paf.verifyAttributeValue(paf.input_mobile_marketing, "checked", "true");

					logger.write("the radio-button for mobile marketing consensus is alrady checked");

				} catch (Exception e) {

					By radiobutton_mobile_marketing_yes = paf.radiobutton_mobile_marketing_yes;

					//paf.verifyComponentExistence(radiobutton_mobile_marketing_yes);
					//paf.clickComponent(radiobutton_mobile_marketing_yes);

				} 
			}
			logger.write("click the yes radio-button for mobile marketing consensus - COMPLETED");

			logger.write("click the yes radio-button for email marketing consensus - START");

			if (prop.getProperty("CONSENSUS_EMAIL").equals("TRUE")) {
				try {

					paf.verifyComponentExistence(paf.input_email_marketing);
					paf.verifyAttributeValue(paf.input_email_marketing, "checked", "true");

					logger.write("the radio-button for email marketing consensus is alrady checked");

				} catch (Exception e) {

					By radiobutton_email_marketing_yes = paf.radiobutton_email_marketing_yes;

					//paf.verifyComponentExistence(radiobutton_email_marketing_yes);
					//paf.clickComponent(radiobutton_email_marketing_yes);

				} 
			}
			
			logger.write("click the yes radio-button for email marketing consensus - COMPLETED");

				logger.write("click the button for saving the modification - START");	

				By button_save_change = paf.button_save_change;
				
				//paf.verifyComponentExistence(button_save_change);
				//paf.clickComponent(button_save_change);

				logger.write("click the button for saving the modification - COMPLETED");
				
				logger.write("click the end button - START");

				Thread.sleep(10000);
				
				By button_end = paf.button_end;

				//paf.verifyComponentExistence(button_end);
				//paf.clickComponent(button_end);
				
				logger.write("click the end button - COMPLETED");
				
				prop.setProperty("RETURN_VALUE", "OK");
				
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
