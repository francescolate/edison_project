package com.nttdata.qa.enel.testqantt.colla;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.FooterPageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.components.colla.OreFreeDettaglioComponent;
import com.nttdata.qa.enel.components.colla.ScriviciComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Publicco_ID_90_Form_Scrivici {
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			FooterPageComponent fpc = new FooterPageComponent(driver);
			
			ScriviciComponent  sc = new ScriviciComponent(driver);
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			Thread.sleep(5000);
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			Thread.sleep(10000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			sc.verifyComponentExistence(sc.supporto);
			sc.clickComponent(sc.supporto);
						
			sc.verifyComponentExistence(sc.contractiModilistica);
			sc.clickComponent(sc.contractiModilistica);
			
			sc.verifyComponentExistence(sc.contatacci);
			sc.clickComponent(sc.contatacci);
			
			fpc.checkUrl(prop.getProperty("SUPPORTOLINK"));
			
			sc.verifyComponentExistence(sc.supportoPath);
			//sc.compareText(sc.supportoPath, ScriviciComponent.SUPPORTO_PATH, true);
			
			sc.verifyComponentExistence(sc.contatacciHeader);
			sc.compareText(sc.contatacciHeader, ScriviciComponent.CONTACCI_HEADER, true);
			
			sc.verifyComponentExistence(sc.contatacciSubHeader);
			sc.compareText(sc.contatacciSubHeader, ScriviciComponent.CONTACCI_SUBHEADER, true);
			
			sc.verifyComponentExistence(sc.vuoiWebQui);
			sc.clickComponent(sc.vuoiWebQui);
			
			fpc.checkUrl(prop.getProperty("QUI_LINK"));
			
			sc.verifyComponentExistence(sc.quiTitle);
			sc.compareText(sc.quiTitle, ScriviciComponent.QUI_TITLE, true);
			
			sc.verifyComponentExistence(sc.quiSubTitle);
			sc.compareText(sc.quiSubTitle, ScriviciComponent.QUI_SUBTITLE, true);
			
			sc.verifyComponentExistence(sc.inviaButton);
			
			sc.verifyComponentExistence(sc.quiSubTitle1);
			sc.compareText(sc.quiSubTitle1, ScriviciComponent.QUI_SUBTITLE1, true);
			
			sc.verifyComponentExistence(sc.cliccaQui);
			sc.clickComponent(sc.cliccaQui);
			
			sc.verifyComponentExistence(sc.conttati);
			sc.compareText(sc.conttati, ScriviciComponent.CONTATTI, true);
						
			sc.verifyComponentExistence(sc.inserisci);
			sc.compareText(sc.inserisci, ScriviciComponent.INSERISCI, true);
			
			sc.verifyComponentExistence(sc.inserisciDati);
			sc.compareText(sc.inserisciDati, ScriviciComponent.INSERISCI_DATI, true);
			
			sc.verifyComponentExistence(sc.informativaPrivacy);
			sc.compareText(sc.informativaPrivacy, ScriviciComponent.INFORMATIVA_PRIVACY, true);
			
			sc.verifyComponentExistence(sc.nomeAssociazioneLabel);
			sc.compareText(sc.nomeAssociazioneLabel, ScriviciComponent.NOMEASSOCIAZIONE_LABEL, true);
			
			sc.verifyComponentExistence(sc.cfAssociazioneLabel);
			sc.compareText(sc.cfAssociazioneLabel, ScriviciComponent.CFASSOCIAZIONE_LABEL, true);
			
			sc.verifyComponentExistence(sc.cfDelagatoLabel);
			sc.compareText(sc.cfDelagatoLabel, ScriviciComponent.CFDELAGATO_LABEL, true);
			
			sc.verifyComponentExistence(sc.nominativoLabel);
			sc.compareText(sc.nominativoLabel, ScriviciComponent.NOMINATIVO_LABEL, true);
			
			sc.verifyComponentExistence(sc.indrizzoMailLabel);
			sc.compareText(sc.indrizzoMailLabel, ScriviciComponent.INDRIZZOMAIL_LABEL, true);
			
			sc.verifyComponentExistence(sc.numeroTelephonoLabel);
			sc.compareText(sc.numeroTelephonoLabel, ScriviciComponent.NUMEROTELEPHONE_LABEL, true);
			
			sc.verifyComponentExistence(sc.nomeLabel);
			sc.compareText(sc.nomeLabel, ScriviciComponent.NOME_LABEL, true);
			
			sc.verifyComponentExistence(sc.cognomeLabel);
			sc.compareText(sc.cognomeLabel, ScriviciComponent.COGNOME_LABEL, true);
			
			sc.verifyComponentExistence(sc.codiceFiscaleLabel);
			sc.compareText(sc.codiceFiscaleLabel, ScriviciComponent.CODICEFISCALE_LABEL, true);
			
			sc.verifyComponentExistence(sc.clienteLabel);
			sc.compareText(sc.clienteLabel, ScriviciComponent.CLIENTE_LABEL, true);
			
			sc.verifyComponentExistence(sc.dicosahaiLabel);
			sc.compareText(sc.dicosahaiLabel, ScriviciComponent.DICOSAHAI_LABEL, true);
			
			sc.verifyComponentExistence(sc.argomentoLabel);
			sc.compareText(sc.argomentoLabel, ScriviciComponent.ARGOMENTO_LABEL, true);
			
			sc.verifyComponentExistence(sc.messagioLabel);
			sc.compareText(sc.messagioLabel, ScriviciComponent.MESSAGIO_LABEL, true);
			
			sc.verifyComponentExistence(sc.accettoLabel);
			sc.compareText(sc.accettoLabel, ScriviciComponent.ACCETTO_LABEL, true);
			
			sc.verifyComponentExistence(sc.nonAccettoLabel);
			sc.compareText(sc.nonAccettoLabel, ScriviciComponent.NONACCETTO_LABEL, true);
			
			String currentHandle ="";
			Set <String> windows = driver.getWindowHandles();
			currentHandle = driver.getWindowHandle();			
			for (String winHandle : windows) {
				driver.switchTo().window(winHandle);
			}
			
			sc.verifyComponentExistence(sc.cliccaquiLink);
			sc.clickComponent(sc.cliccaquiLink);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			dpc.switchToNewTab();
			Thread.sleep(5000);
			driver.switchTo().window(currentHandle);
			
			logger.write("Verify header section - Start");
			Thread.sleep(5000);
			ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("LUCE_E_GAS"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("IMPRESA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("STORIE"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("FUTUR-E"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("MEDIA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");

			mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
			mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);
			Thread.sleep(5000);
			logger.write("Verify footer section - Start");
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
			logger.write("Verify footer section - Completed");		
			
			OreFreeDettaglioComponent ofdf = new OreFreeDettaglioComponent(driver);
			Robot robot = new Robot();
									
				ofdf.clickComponent(ofdf.dropdownClick);
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(500);
						
			sc.enterInput(sc.cfdelagatoInput, prop.getProperty("CFD"));
			sc.enterInput(sc.nominativoInput, prop.getProperty("ND"));
			sc.enterInput(sc.indrizzoMailInput, prop.getProperty("EMAIL"));
			sc.enterInput(sc.numberoTelephonoInput, prop.getProperty("TELEPHONO"));
			sc.enterInput(sc.nomeInput, prop.getProperty("NOME"));
			sc.enterInput(sc.cognomeInput, prop.getProperty("COGNOME"));
			sc.enterInput(sc.codiceFiscaleInput, prop.getProperty("CF"));
			
			for (int i=0; i<=12; i++){
				ofdf.clickComponent(ofdf.ddvaluecontainer);
				robot.keyPress(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_DOWN);
				robot.delay(50);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.delay(50);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(2000);
				}
			
			sc.enterInput(sc.messagioTextbox, prop.getProperty("MESSAGIO"));
			
			sc.verifyComponentExistence(sc.accettoRadio);
			sc.clickComponent(sc.accettoRadio);
			
			sc.verifyComponentExistence(sc.proseguiButton);
			sc.clickComponent(sc.proseguiButton);
			
			sc.verifyComponentExistence(sc.noButton);
			sc.clickComponent(sc.noButton);
			
			sc.verifyComponentExistence(sc.okButtonNew);
			sc.clickComponent(sc.okButtonNew);
			
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
