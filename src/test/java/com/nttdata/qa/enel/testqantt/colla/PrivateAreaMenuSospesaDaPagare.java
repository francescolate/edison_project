package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteMyComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaMenuSospesaDaPagare {
	
		public static void main(String[] args) throws Exception {
			Properties prop = null;
			prop = WebDriverManager.getPropertiesIstance(args[0]);
			QANTTLogger logger = new QANTTLogger(prop);

			try {
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				PrivateAreaBolletteMyComponent pabc = new PrivateAreaBolletteMyComponent(driver);
						
				logger.write("Payment Status and Colour verification on the Card - STARTS");
				pabc.verifyComponentExistence(pabc.paymentStatusDaPagareOnCard);
				pabc.checkColor(pabc.paymentStatusDaPagareOnCard, "Red", "In verifica");
				logger.write("Payment Status and Colour verification on the Card - ENDS");			
				
				logger.write("checking and clicking the button related to the active supply - START");
				pabc.verifyComponentExistence(pabc.gestisciLeOreFree8c);
				pabc.clickComponent(pabc.gestisciLeOreFree8c);
				logger.write("checking and clicking the button related to the active supply  - COMPLETED");
				
				logger.write("checking the text contained within the header of the page - START");
				pabc.comprareText(pabc.supplyHeader, pabc.SUPPLY_HEADER, true);
				logger.write("header text checking - COMPLETED");
									
				logger.write("checking customer supply address - START");
				pabc.verifyComponentExistence(pabc.gassupplyAddress);
				pabc.comprareText(pabc.gassupplyAddress, prop.getProperty("SUPPLY_ADDRESS"), true);
				logger.write("checking customer supply address - COMPLETED");
				
				logger.write("checking customer code - START");
				pabc.verifyComponentExistence(pabc.gasCustomerNumber);
				pabc.comprareText(pabc.gasCustomerNumber, prop.getProperty("CUSTOMER_CODE"), true);
				logger.write("checking customer code - COMPLETED");
				
				logger.write("checking Stato Fornitura Feild - START");
				pabc.verifyComponentExistence(pabc.statoFornituraFeild);
				String s = driver.findElement(pabc.statoFornituraFeild).getText();
				pabc.comprareText(pabc.statoFornituraFeild, prop.getProperty("STATO_FORNITURA_FEILD"), true);
				pabc.checkColor(pabc.statoFornituraValue, "Red", "Sospesa");
				logger.write("checking Stato Fornitura Feild - COMPLETED");
				
				logger.write("checking Stato Pagamento Feild - START");
				pabc.verifyComponentExistence(pabc.statoPagamentoFeild);
				String s1 = driver.findElement(pabc.statoPagamentoFeild).getText();
				pabc.comprareText(pabc.statoPagamentoFeild, prop.getProperty("STATO_PAGAMENTO_FEILD"), true);
				pabc.checkColor(pabc.statoPagamentoValue, "Red", "Da pagare (9)");
				logger.write("checking Stato Pagamento Feild - COMPLETED");
				
				
				
				prop.setProperty("RETURN_VALUE", "OK");
				
			} catch (Throwable e) {
				prop.setProperty("RETURN_VALUE", "KO");

				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: " + errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

//				prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
			}

		}

	}
