package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SFDCBoxSubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class CreaOffertaSubentroId16 {

    public static void main(String[] args) throws Exception {

        Properties prop = WebDriverManager.getPropertiesIstance(args[0]);
        RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
        SFDCBoxSubentroComponent boxSubentro = new SFDCBoxSubentroComponent(driver);
        RiepilogoOffertaComponent offer = new RiepilogoOffertaComponent(driver);
        GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);


        try {
            QANTTLogger logger = new QANTTLogger(prop);
            //BOX Riepilogo offerta
            /*By container_rieoff = boxSubentro.container_rieoff;
            boxSubentro.verifyComponentExistence(container_rieoff);

            //BOX Seleziona Uso Forniture
            By container_suf = boxSubentro.container_suf;
            boxSubentro.verifyComponentExistence(container_suf);
            By suf_pickuplist_uf = boxSubentro.suf_pickuplist_uf;
            boxSubentro.verifyComponentExistence(suf_pickuplist_uf);
            boxSubentro.clickAncestorClickable(boxSubentro.xpathToString(suf_pickuplist_uf), 7);
            By suf_pickuplist_uf_uda = boxSubentro.suf_pickuplist_uf_uda;
            boxSubentro.verifyComponentExistence(suf_pickuplist_uf_uda);
            boxSubentro.clickComponent(suf_pickuplist_uf_uda);
            By suf_button_conferma = boxSubentro.suf_button_conferma;
            boxSubentro.verifyComponentExistence(suf_button_conferma);
            boxSubentro.clickComponent(suf_button_conferma);
            gestione.checkSpinnersSFDC();


            //BOX Commodity
            offer.checkValoreInTable("ELETTRICO");
            offer.clickComponentIfExist(offer.buttonModificaFornitureCommodity);
            gestione.checkSpinnersSFDC();*/
            offer.popolareCommodityBusiness("NO", "ALTRI SERVIZI", "NO", "3297451908", "NO", "SI", "1250");
            offer.clickComponentIfExist(offer.buttonConfermaFornituraCommodity);
            gestione.checkSpinnersSFDC();
            offer.clickComponent(offer.buttonConfermaCommodity);
            gestione.checkSpinnersSFDC();


            //BOX Indirizzo di Residenza/Sede Legale
            By container_irsl = boxSubentro.container_irsl;
            boxSubentro.verifyComponentExistence(container_irsl);
            By irsl_button_conferma = boxSubentro.irsl_button_conferma;
            boxSubentro.clickComponentIfExist(irsl_button_conferma);

            //BOX INDIRIZZO DI FATTURAZIONE
            By container_idf = boxSubentro.container_idf;
            boxSubentro.verifyComponentExistence(container_idf);
            By idf_banner_indirizzo = boxSubentro.idf_banner_indirizzo;
            By idf_button_conferma = boxSubentro.idf_button_conferma;
            if (boxSubentro.getElementExistance(idf_banner_indirizzo)) {
                boxSubentro.clickComponentIfExist(idf_button_conferma);
            }
            By idf_tab_verifica = boxSubentro.idf_tab_verifica;
            boxSubentro.clickComponentIfExist(idf_tab_verifica);
            By idf_first_checkbox_showed = boxSubentro.idf_first_checkbox_showed;
            boxSubentro.clickComponentIfExist(idf_first_checkbox_showed);
            By idf_button_importa = boxSubentro.idf_button_importa;
            boxSubentro.clickComponentIfExist(idf_button_importa);
            By idf_input_regione = boxSubentro.idf_input_regione;
            boxSubentro.verifyComponentExistence(idf_input_regione);
            By idf_input_regione_disabled = boxSubentro.idf_input_regione_disabled;
            boxSubentro.verifyComponentExistence(idf_input_regione_disabled);
            By idf_input_provincia = boxSubentro.idf_input_provincia;
            boxSubentro.verifyComponentExistence(idf_input_provincia);
            By idf_input_comune = boxSubentro.idf_input_comune;
            boxSubentro.verifyComponentExistence(idf_input_comune);
            By idf_input_localita = boxSubentro.idf_input_localita;
            boxSubentro.verifyComponentExistence(idf_input_localita);
            By idf_input_indirizzo = boxSubentro.idf_input_indirizzo;
            boxSubentro.verifyComponentExistence(idf_input_indirizzo);
            By idf_input_ncivico = boxSubentro.idf_input_ncivico;
            boxSubentro.verifyComponentExistence(idf_input_ncivico);
            By idf_input_cap = boxSubentro.idf_input_cap;
            boxSubentro.verifyComponentExistence(idf_input_cap);
            By idf_input_cap_disabled = boxSubentro.idf_input_cap_disabled;
            boxSubentro.verifyComponentExistence(idf_input_cap_disabled);
            By idf_button_verifica = boxSubentro.idf_button_verifica;
            boxSubentro.clickComponentIfExist(idf_button_verifica);
            By idf_indirizzo_verificato = boxSubentro.idf_indirizzo_verificato;
            boxSubentro.verifyComponentExistence(idf_indirizzo_verificato);
            boxSubentro.clickComponentIfExist(idf_button_conferma);


            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            //logger.write("ERROR_DESCRIPTION: " + errors.toString());
            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());

            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}