package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltagePreventintoComponent;
import com.nttdata.qa.enel.components.colla.DettagliocostietempiComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.PowerAndVoltageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACR_MPT_380 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try{
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("apertura del portale web Enel di test - Start");
			PowerAndVoltageComponent pavc = new PowerAndVoltageComponent(driver);
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			DettagliocostietempiComponent dc = new DettagliocostietempiComponent(driver);
			String electricitySupplyDetailPageUrl = prop.getProperty("WP_LINK")+"/it/area-clienti/residenziale/dettaglio_fornitura";
			
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);

			//Step 4
			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");

			//Step 5
			logger.write("Verifying the customer Page-- Start");
			pavc.comprareText(pavc.areaPrivataHeading, PowerAndVoltageComponent.AREA_PRAIVATA_HEADING, true);
			pavc.comprareText(pavc.leFornitureHeading, PowerAndVoltageComponent.LE_FORNITURE_HEADING, true);
			logger.write("Verifying the customer Page-- Ends");
			logger.write("Clicking on the Dettaglio Fornitura button in electrical supply card & verifying the page navigation-- Start");
			dc.clickComponent(dc.detaglioFurnitura_930943292NewUpdated);
			pavc.checkURLAfterRedirection(electricitySupplyDetailPageUrl);
			logger.write("Clicking on the Dettaglio Fornitura button in electrical supply card & verifying the page navigation-- Ends");
			logger.write("Clicking on the Modifica potenca e/o Tenzione & verifying the page navigation-- Start");
			pavc.verifyComponentExistence(pavc.serviziPerLeFornitureHeading);
			Thread.sleep(20000);
			pavc.jsClickElement(pavc.modificaPotenzaTensioneTile);
			//pavc.clickComponent(pavc.modificaPotenzaTensioneTile);
			Thread.sleep(20000);
			
			//Step 6&7
			pavc.verifyComponentExistence(pavc.modificaPotenzaPage);
			pavc.verifyComponentExistence(pavc.modificaPotenzaButton);
			pavc.clickComponent(pavc.modificaPotenzaButton);
			Thread.sleep(15000);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTensioneTileHeading);
			pavc.comprareText(pavc.modificaPotenzaTensioneTileHeading, PowerAndVoltageComponent.MODIFICA_POTENZA_TENZIONE_TILE_HEADING, true);
			pavc.verifyComponentExistence(pavc.modificaPotenzaTenzioneTitle);
			pavc.comprareText(pavc.modificaPotenzaTenzioneTitle, PowerAndVoltageComponent.MODIFICA_POTENZA_TENZIONE_TITLE, true);
			logger.write("Clicking on the Modifica potenca e/o Tenzione & verifying the page navigation-- Ends");
			
			//Step 8
			logger.write("Clicking on the Radio Button-- Start");
			dc.clickComponent(dc.radioIT002E3831521A); 
			logger.write("Clicking on the Radio Button-- Ends");
			
			//Step 9
			logger.write("Clicking on the Modifica Potenza e/o Tenzione-- Start");
			pavc.verifyComponentExistence(pavc.modificaPotenzaTenzioneButton2);
			pavc.clickComponent(pavc.modificaPotenzaTenzioneButton2);
			logger.write("Clicking on the Modifica Potenza e/o Tenzione-- Ends");
			
			//Step 9
			logger.write("Verifying the data on clicking Modifica Potenza e/o Tenzione button-- Start");
			pavc.verifyComponentExistence(pavc.titleMPT);
			pavc.comprareText(pavc.titleMPT, PowerAndVoltageComponent.TITLE_MPT, true);
			dc.comprareText(dc.subTitleMPT, DettagliocostietempiComponent.SUBTITLE_MPT_New, true);
			pavc.checkForoptionsAvailable(prop.getProperty("OPTIONS"));
			pavc.verifyComponentExistence(pavc.powerandVoltageDataEntryHeading);
			pavc.comprareText(pavc.powerandVoltageDataEntryHeading, PowerAndVoltageComponent.POVER_VOLTAGE_DATAENTRY_HEADING, true);
			pavc.verifyComponentExistence(pavc.textNextToTenzione);
			pavc.comprareText(pavc.textNextToTenzione, PowerAndVoltageComponent.TEXT_NEXT_TENZIONE, true);
			pavc.verifyComponentExistence(pavc.noRadioButton);
			pavc.isRadioButtonEnabled(pavc.noRadioButton);
			
			//Step 10
			pavc.comprareText(pavc.potenzalabel, PowerAndVoltageComponent.POTENZA_LABEL, true);
			pavc.validateDropDownValue(pavc.potenzaValue, PowerAndVoltageComponent.POTENZA_VALUE);
		   
			//Step 11
			pavc.comprareText(pavc.tensionelabel, PowerAndVoltageComponent.TENZIONE_LABEL, true);
			pavc.validateDropDownValue(pavc.tensioneValue, PowerAndVoltageComponent.TENZIONE_VALUE);
			
			//Step 12
			pavc.changeDropDownValue(pavc.potenzaValue,pavc.setPotenzaValueNew);
			pavc.verifyComponentExistence(pavc.aggiornamentoTensioneHeading);
			pavc.verifyComponentExistence(pavc.aggiornamentoTensioneSubHeading);
			
			//Step 13
			pavc.verifyComponentExistence(pavc.tensioneValueAfterNewSet);
			
			//Step 14
			pavc.changeDropDownValue(pavc.potenzaValue,pavc.setPotenzaValue);
			
			//Step 15
			pavc.changeDropDownValue(pavc.potenzaValue,pavc.setPotenzaValueNew2);
	
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}