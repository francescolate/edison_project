package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.apache.tika.metadata.Property;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.components.lightning.RichiestaCollaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


import io.qameta.allure.Step;

 public class  VerifyRichestaDetails {

           @Step("Richiesta Details")

      public static void main(String[] args) throws Exception {

          
           Properties prop = null;
           prop = WebDriverManager.getPropertiesIstance(args[0]);
           QANTTLogger logger = new QANTTLogger(prop);
           RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
          
           try

           {
             RichiestaCollaComponent RCC = new RichiestaCollaComponent(driver);
        /*     
              logger.write("Verify the Customer details - Start");
              System.out.println("Verify the Richesta Header");
              RCC.verifyRichestaHeader();
              logger.write("Verify the Customer details - Complete");
              
              logger.write("Verify the Richesta details - Start");
              System.out.println("verify the Richesta Details");
              RCC.verifyRichestaDetails();
              logger.write("Verify the Richesta details - Complete");
                 
              logger.write("Verify and click on Richesta details - Start");
              //String ele = driver.findElement(RCC.RichestaNum).getText();
              //System.out.println(ele);
             // RCC.pressJavascript(RCC.RichestaNum1);
              logger.write("Verify and click on Richesta details - Complete");
          */   
              Thread.sleep(5000);
              RCC.jsScrollRiche();
              logger.write("Verify and click on Richesta header - Start");
              RCC.verifyComponentExistence(RCC.RichestaHeaderLink);
              RCC.pressJavascript(RCC.RichestaHeaderLink);
              logger.write("Verify and click on Richesta header  - Complete");
              
            //  RCC.verifyComponentExistence(By.xpath(RCC.RichestaDetail));
            //  RCC.pressJavascript(By.xpath(RCC.RichestaDetail));
              
              DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
              LocalDateTime now = LocalDateTime.now();
              RCC.verifyComponentExistence(By.xpath(RCC.RichestaDetailDate.replace("$DATE$", dtf.format(now))));
              RCC.pressJavascript(By.xpath(RCC.RichestaDetailDate.replace("$DATE$", dtf.format(now))));
              
            
              logger.write("Verify the status of Richesta details - Start");
              RCC.verifyStatoSottostatoRichiesta(prop.getProperty("STATO"), prop.getProperty("SOTTOSTATO"));
              logger.write("Verify the status of Richesta details - Complete");
              
              prop.setProperty("RETURN_VALUE", "OK");

           } catch (Exception e) {

                 prop.setProperty("RETURN_VALUE", "KO");

                
                 StringWriter errors = new StringWriter();
                 e.printStackTrace(new PrintWriter(errors));
                 errors.toString();
                 logger.write("ERROR_DESCRIPTION: "+errors.toString());

                 prop.setProperty("ERROR_DESCRIPTION", errors.toString());

           if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
             

           }finally

           {

                 //Store WebDriver Info in properties file

                 prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");           

           }

      }

 

 

}