package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_InvioPraticheComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_InvioPSPortale_1OK_ELE {

	@Step("R2D Invio PS a Portale - Esito 1OK -ELE")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				String statopratica=prop.getProperty("STATO_R2D");
				//l'esito RD2 1OK è opzionale e viene dato solo quando lo stato della pratica è AW
				if (statopratica.compareToIgnoreCase("AW")==0){
					//Selezione Menu Code elaborazione dati
					menuBox.selezionaVoceMenuBox("Code elaborazione dati","Invio PS a portale");
					logger.write("Menu Code elaborazione dati");
					//Selezione Voce Report Post Sales
					R2D_InvioPraticheComponent selezionaPratiche = new R2D_InvioPraticheComponent(driver);
					logger.write("Report Post Sales");
					//Selezione distributore
					selezionaPratiche.selezionaDistributore(prop.getProperty("DISTRIBUTORE_R2D_ATTESO_ELE"));
					String tipoOperazione=prop.getProperty("TIPO_OPERAZIONE");
					logger.write("Distributore Selezionato");
					//Seleziona Tipo Lavorazione Coda
					//if (prop.getProperty("MODIFICA_POTENZA_TENSIONE","N").equals("Y")){
					if (tipoOperazione.equals("MODIFICA_POTENZA_TENSIONE")){
						selezionaPratiche.selezionaTipoLavorazioneCodaDoppia(prop.getProperty("TIPO_LAVORAZIONE_CRM_ELE"), prop.getProperty("SOTTO_TIPO_LAVORAZIONE_CRM_ELE"));
					}
					else{
						selezionaPratiche.selezionaTipoLavorazioneCoda(prop.getProperty("TIPO_LAVORAZIONE_CRM_ELE"));
					}
					logger.write("Pratiche selezionate per scodamento");
					//Invio Pratiche
					selezionaPratiche.invioPratiche();
					// Pratica avanzata al 2OK - Stato 'AA'
					logger.write("Pratiche scodate");
					prop.setProperty("STATO_R2D", "AA");
					logger.write("Stato Pratica = 'AA'");
				}
				else{
					Logger.getLogger("").log(Level.INFO, "Pratica in Stato diverso da 'AW', non è necessario dare esito 1OK");
				}

			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
