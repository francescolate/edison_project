package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.SuplyPrivateDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privata_Servizi_ACR_75 {
	
public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
//			prop.setProperty("FORNITURETITLE", "Servizi per le forniture");
//			prop.setProperty("BOLLETTETITLE", "Servizi per le bollette");
//			prop.setProperty("CONTRATTOTITLE", "Servizi per il contratto");
//			prop.setProperty("FORNITURECONTENT", "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture");
//			prop.setProperty("BOLLETTECONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette");
//			prop.setProperty("CONTRATTOCONTENT", "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture");
//			prop.setProperty("MODIFICATITLE", "Modifica Indirizzo di Fatturazione");
//			prop.setProperty("CITTAWARNINGMSG", "Per proseguire è necessario inserire la Città");
//			prop.setProperty("INDRIZZOWARNINGMSG", "Per proseguire è necessario inserire un indirizzo di recapito");
//			prop.setProperty("CIVIWARNINGMSG", "Per proseguire è necessario indicare il numero civico");
						
			SuplyPrivateDetailComponent spdc = new SuplyPrivateDetailComponent(driver);
		//	SuplyPrivateDetailComponent cba = new SuplyPrivateDetailComponent(driver);
			
//			logger.write("Verify the left menu list - Start");
//			spdc.homepageRESIDENTIALMenu(spdc.LeftMenuList);
//			logger.write("Verify the left menu list - Complete");
			
			logger.write("Verify and click the Servizi link - Start");
			spdc.clickComponent(spdc.ServiziLink);
			logger.write("Verify and click the Servizi link - Start");
			
			logger.write("Verify the title - Start");
//			spdc.VerifyBollette(spdc.FornituraTitle);
//			spdc.VerifyBollette(spdc.FornituraTitleContent);
			spdc.VerifyBollette(spdc.BolletteTitle);
			spdc.VerifyBollette(spdc.BolletteTitleContent);
			spdc.VerifyBollette(spdc.ContratoTitle);
			spdc.VerifyBollette(spdc.ContratoTitleContent);
			logger.write("Verify the title  - Complete");
			
			logger.write("Verify the  contents - Start");
			spdc.VerifyForniture(spdc.FornitureContents);
			spdc.VerifyBollette(spdc.BolletteContents);
			logger.write("Verify the  contents - Complete");
			
			logger.write("Click on Modiffica link - Start");
			spdc.clickComponent(spdc.ModifficaIndrizzolink);
			logger.write("Click on Modiffica link - Complete");
			
			logger.write("Verify the title and contents - Start");
			spdc.verifyComponentExistence(spdc.ModificaTitle);
			spdc.verifyComponentExistence(spdc.ModificaContents);
			logger.write("Verify the title and contents - Complete");
			
			logger.write("Click on the checkbox  - Start");
			spdc.clickComponent(spdc.ModificaCheckbox);
			spdc.clickComponent(spdc.ModificaButton);
			logger.write("Click on the checkbox - Complete");
			Thread.sleep(2000);
			logger.write("Enter the input values in the mandatory fields  - Start");		
			spdc.verifyComponentExistence(spdc.CittaInputField176);
			spdc.enterLoginParameters(spdc.CittaInputField176, prop.getProperty("CITTAINPUTVALUE"));
			Thread.sleep(2000);
			//spdc.verifyComponentExistence(spdc.CittaDropDownRoma);
			//spdc.clickComponent(spdc.CittaDropDownRoma);
			
					
			Thread.sleep(2000);
			spdc.verifyComponentExistence(spdc.IndrizzoInput176);	
			spdc.enterText(spdc.IndrizzoInput176, prop.getProperty("INDRIZZOINPUTVALUE"));
			Thread.sleep(1000);
			//spdc.verifyComponentExistence(spdc.IndrizzoDropDownAntica);
			//spdc.clickComponent(spdc.IndrizzoDropDownAntica);
			
			Thread.sleep(1000);
			spdc.verifyComponentExistence(spdc.NumercoCivicInput176);
			spdc.enterLoginParameters(spdc.NumercoCivicInput176, prop.getProperty("NUMERCOCIVIINPUTVALUE"));
			logger.write("Enter the input values in the mandatory fields  - Complete");
			
			Thread.sleep(2000);
			logger.write("Click on the Contiue button - Start");
			spdc.verifyComponentExistence(spdc.ContinuaBtn);
			spdc.clickComponent(spdc.ContinuaBtn);	
			
			logger.write("Click on the Contiue button - Complete");
			
			//spdc.verifyInserimento(spdc.VerifyInserimento3);
			
			/*
			spdc.verifyComponentExistence(spdc.AttenzionePopup);
			spdc.verifyComponentExistence(spdc.AttenzionePopupText);
			spdc.verifyComponentExistence(spdc.AttenzionePopupClose);
			spdc.clickComponent(spdc.AttenzionePopupClose);
		
			//-------------
			
			
			spdc.verifyComponentExistence(spdc.DropdownBivo);
			spdc.clickComponent(spdc.DropdownBivo);
			spdc.verifyComponentExistence(spdc.CapInput);
			spdc.enterLoginParameters(spdc.CapInput, prop.getProperty("CAPINPUTVALUE"));
			spdc.verifyComponentExistence(spdc.CapDropdownValue);
			spdc.clickComponent(spdc.CapDropdownValue);
			spdc.clickComponent(spdc.ContinuaBtn);
			
			spdc.verifyComponentExistence(spdc.IndrizzoInputAfterWarn);
			spdc.enterLoginParameters(spdc.IndrizzoInputAfterWarn, prop.getProperty("INDRIZZOINPUTVALUE"));
			spdc.enterLoginParameters(spdc.NumercoCivicInput, prop.getProperty("NUMERCOCIVIINPUTVALUE"));
			spdc.clickComponent(spdc.ContinuaBtn);
			
			//-------------
			
			*/
			logger.write("Click on the Conferma button - Start");
			Thread.sleep(10000);
			spdc.verifyComponentExistence(spdc.Conferma);
			spdc.clickComponent(spdc.Conferma);
			Thread.sleep(10000);
			logger.write("Click on the Conferma button - Complete");
			
			logger.write("Click on the Conferma details - Start");
//			spdc.verifyComponentExistence(spdc.ConfermaTitle);
//			spdc.verifyComponentExistence(spdc.ConfermaContents);
//			logger.write("Click on the Conferma details - Start");
//			
//			logger.write("Click on the Fine button - Start");
//			//spdc.verifyFinebutton(spdc.Finebutton);
//			spdc.clickComponent(spdc.Finebutton);
//			Thread.sleep(10000);
			logger.write("Click on the Fine button - Complete");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		

	}

}
