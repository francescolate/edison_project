package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class SetPropertyPredPresaGasId5_04 {

    public static void main(String[] args) throws Exception {

        Properties prop = null;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

        		// Lavorazione flusso N01 Esecuzione Lavori
        		prop.setProperty("STATO_R2D_ORDINE", "PRESO IN CARICO");
        		prop.setProperty("STATO_ELEMENTO_ORDINE", "RICHIESTO LAVORO");
        		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "RICHIESTO LAVORO");
        		prop.setProperty("STATO_SAP_SD_ORDINE", "OK");
        		prop.setProperty("STEP_PP", "4");
        		// R2D
        		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Allaccio");
       		    logger.write(" *** STEP_PP=4 ***");
       		    System.out.println("STEP_PP=4");

            }
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}
