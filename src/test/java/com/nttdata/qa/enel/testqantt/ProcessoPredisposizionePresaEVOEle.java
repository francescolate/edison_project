package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestionePredisposizionePresaComponentEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ProcessoPredisposizionePresaEVOEle {

	@Step("Processo Switch Attivo EVO")
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				SeleniumUtilities util = new SeleniumUtilities(driver);

				GestionePredisposizionePresaComponentEVO presa = new GestionePredisposizionePresaComponentEVO(driver);
				String statoUbiest="ON"; //*** qui Ubiest FUNZIONA
	
				// Inserimento USO Fornitura
				logger.write("Selezione Uso Fornitura - Start");
				presa.compilaCampoUsoFornitura(prop.getProperty("USO_FORNITURA"));			
				logger.write("Selezione Uso Fornitura - Completed");
				statoUbiest=Costanti.statusUbiest;
				// Inserimento e Validazione Indirizzo Esecuzione Lavori 
				if(statoUbiest.compareTo("ON")==0){
					logger.write("Inserimento e Validazione Indirizzo Esecuzione lavori - Start");
					presa.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
//					presa.verificaIndirizzo(presa.verificaIndirizzoButton);
					presa.verificaIndirizzo(prop.getProperty("CAP"),presa.verificaIndirizzoButton);
					presa.press(presa.confermaIndirizzo);
					logger.write("Inserimento e Validazione Indirizzo Esecuzione lavori - Completed");
					TimeUnit.SECONDS.sleep(5);
				}
				else if(statoUbiest.compareTo("OFF")==0){
					logger.write("Inserimento e Validazione Indirizzo Esecuzione lavori Forzato - Completed");
					presa.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
							prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
//					presa.verificaIndirizzo(presa.verificaIndirizzoButton);
					presa.verificaIndirizzo(prop.getProperty("CAP"),presa.verificaIndirizzoButton);
					presa.forzaIndirizzoPredisposizionePresaLavori(prop.getProperty("CAP"),presa.confermaIndirizzo);
					presa.press(presa.confermaIndirizzo);
					logger.write("Inserimento Indirizzo Forzato  - Completed"); 
					TimeUnit.SECONDS.sleep(5);
				}
				// FINE Inserimento e Validazione Indirizzo Esecuzione Lavori

				// Dati Fornitura
				TimeUnit.SECONDS.sleep(5);
				logger.write("Seleziona Commodity Fornitura - Start");
				presa.press(presa.radioCommodity);
				logger.write("Seleziona Commodity Fornitura - Completed");
				TimeUnit.SECONDS.sleep(5);	
				logger.write("Selezione Tensione Richiesta - Start");
				presa.compilaTensioneRichiesta(prop.getProperty("TENSIONE_RICHIESTA"));			
				logger.write("Selezione Tensione Richiesta - Completed");
				
				if(!prop.getProperty("POTENZA_RICHIESTA","0").contentEquals("0")) {
					logger.write("Selezione Potenza Richiesta - Start");
					presa.compilaPotenzaRichiesta(prop.getProperty("POTENZA_RICHIESTA"));
					logger.write("Selezione Potenza Richiesta - Completed");
				
					presa.inserisciTelefono(presa.telefono); //inserisce numero telefono se il campo è vuoto
					// Test funzionamento su campo Emissione Fattura Anticipata - Valore "SI" e Valore "NO"
					if (prop.getProperty("TEST_FATTURAZIONE_ANTICIPATA","NO").contentEquals("SI")) {
						logger.write("Test su Campo Fatturazione Anticipata - Start");
						presa.FatturaAnticipata("SI");
						presa.FatturaAnticipata("NO");
						System.out.println("Test su Campo Fatturazione Anticipata: SI/NO - Completed");
						logger.write("Test su Campo Fatturazione Anticipata - Completed");
						}
					// Emissione Fattura Anticipata
					if (prop.getProperty("FATTURAZIONE_ANTICIPATA").contentEquals("SI")) {
						logger.write("Validazione Campo Fatturazione Anticipata a 'SI' - Start");
						presa.FatturaAnticipata(prop.getProperty("FATTURAZIONE_ANTICIPATA"));
						System.out.println("Validazione Campo Fatturazione Anticipata a 'SI' - Completed");
						logger.write("Validazione Campo Fatturazione Anticipata a 'SI' - Completed");
						}
					presa.press(presa.confermaFornitura);
					logger.write("confermaDatiFornitura Dati Fornitura - Start");
					presa.press(presa.confermaDatiFornitura);
					logger.write("confermaDatiFornitura Dati Fornitura - Completed");

					// Annullamento-Crea Offerta in fase di Data Entry
					if(prop.getProperty("ANNULLA_OFFERTA","").contentEquals("Y")){
						logger.write("Annullamento Ordine - Start");
						presa.press(presa.annullaOrdine);
						presa.press(presa.confermaAnnullaOrdine);
						logger.write("Annullamento Ordine - Completed");
					} else {
						
						logger.write("Compila Campi Sezione Modalità firma e Canale Invio - Start");
						presa.compilaCampiModalitaFirmaIndirizzoForzato(prop.getProperty("CAP"), prop.getProperty("CANALE_INVIO_CONTRATTO"));			
						logger.write("Compila Campi Sezione Modalità firma e Canale Invio - Completed");
	
						//Calcolo sysdate
						Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
						String data = simpleDateFormat.format(calendar.getTime()).toString();
	
						if(prop.getProperty("TIPOCLIENTE","").contentEquals("BUSINESS") || (prop.getProperty("TIPOCLIENTE","").contentEquals("PA"))){
							// Sezione CIG e CUP
							logger.write("Compila Campi Sezione CIG e CUP - Start");
							presa.compilaCampiCigCup(prop.getProperty("FLAG_136"),prop.getProperty("CIG"),prop.getProperty("CUP"));			
							logger.write("Compila Campi Sezione CIG e CUP - Completed");
							// Sezione Split Payment
							logger.write("Compila Campi Sezione Split Payment - Start");
							presa.compilaSplitPayment(prop.getProperty("SPLIT_PAYMENT"), data);			
							logger.write("Compila Campi Sezione Split Payment - Completed");
							
							if(prop.getProperty("TIPOCLIENTE","").contentEquals("PA")){
								
								// Sezione Fatturazione Elettronica
								logger.write("Compila Campi Sezione Fatturazione Elettronica - Start");
								presa.compilaFatturazioneElettronica(prop.getProperty("TIPOLOGIA_PUBBL_AMMIN"));			
								System.out.println("Compila Campi Sezione Fatturazione Elettronica - OK");
								logger.write("Compila Campi Sezione Fatturazione Elettronica - Completed");
								
							}
						}
	
	
						// CREA ORDINE
						TimeUnit.SECONDS.sleep(5);
						logger.write("Bottone Crea Ordine - Start");
						presa.press(presa.creaOrdine);
						logger.write("Bottone Crea Ordine - Completed");
					}
				} else {
	                // Controllo Alert Potenza Richiesta = 0 o vuoto
					logger.write("Test Alert Potenza Richiesta = 0 - Start");
					presa.compilaPotenzaRichiesta(prop.getProperty("POTENZA_RICHIESTA"));
					System.out.println("Selezione Potenza Richiesta = 0 - OK");
					logger.write("Test Alert Potenza Richiesta = 0 - Completed");
				}
				
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
