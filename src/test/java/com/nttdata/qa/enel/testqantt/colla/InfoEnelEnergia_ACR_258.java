package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.GDPRComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class InfoEnelEnergia_ACR_258 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			HomeComponent hm = new HomeComponent(driver);
			GDPRComponent gdpr = new GDPRComponent(driver);
			hm.verifyComponentExistence(hm.pageTitleRes);
			Thread.sleep(10000);
			logger.write("Verifi home page title and details -- Start");
			//Thread.sleep(10000);
			hm.comprareText(hm.pageTitleResNew, HomeComponent.PageTitle, true);
			hm.verifyComponentExistence(hm.letueFurniture);
			hm.comprareText(hm.letueFurnitureSubtext, HomeComponent.LetueFurnitureSubtext, true);
			hm.comprareText(hm.pageTitleSubText, HomeComponent.TitleSubText, true);
			logger.write("Verifi home page title and details -- Completed");
			
			logger.write("Click on services -- Start");
			hm.clickComponent(hm.services);
			logger.write("Click on services -- Completed");

			ServicesComponent sc = new ServicesComponent(driver);
//			sc.comprareText(sc.serviceTitle, ServicesComponent.serviziText, true);
//			sc.comprareText(sc.serviceSubText, ServicesComponent.ServiziSubText, true);
//			sc.verifyComponentExistence(sc.serviceTitle);
			sc.verifyComponentExistence(sc.infoEnelEnergia);
			sc.checkForSupplyServices(ServicesComponent.SupplyServices);
			
			logger.write("Click on Info EnelEnergia on services -- Start");
			Thread.sleep(10000);
			sc.clickComponent(sc.infoEnelEnergia);
			logger.write("Click on Info EnelEnergia on services -- Completed");

			InfoEnelEnergiaMyComponent ic =new InfoEnelEnergiaMyComponent(driver);
			logger.write("Verify Info EnelEnergia page title and content -- Start");
			ic.verifyComponentExistence(ic.headingInfoEnergia);
			ic.comprareText(ic.headingInfoEnergia, InfoEnelEnergiaMyComponent.HEADING_INFOENERGIA, true);
			ic.comprareText(ic.titleInfoEnergia, InfoEnelEnergiaMyComponent.TITLE_INFOENERGIA, true);
			ic.comprareText(ic.subtitleInfoEnergia, InfoEnelEnergiaMyComponent.SUBTITLE_INFOENERGIA, true);
			ic.verifyComponentExistence(ic.attivaInfoEnergiaButton);
			ic.verifyComponentExistence(ic.esciButton);
			logger.write("Verify Info EnelEnergia page title and content -- Completed");
			
			/*logger.write("Verify Info EnelEnergia page title and content -- Start");
			ic.clickComponent(ic.attivaInfoEnergiaButton);
			ic.comprareText(ic.heading2InforEnergia, InfoEnelEnergiaMyComponent.AttivaInfoEnelEnergiaTitle, true);
			ic.comprareText(ic.heading2InfoEnel, InfoEnelEnergiaMyComponent.HEADING2_INFOENELENERGIA, true);
			gdpr.comprareText(gdpr.indirizzoDiFornituraRES, GDPRComponent.INDIRIZZO_DELLA_FORNITURA, true);
			gdpr.comprareText(gdpr.numeroClienteRES, GDPRComponent.NUMERO_CLIENTE_RES, true);
			gdpr.verifyComponentExistence(gdpr.numeroClienteRESValue);
			gdpr.isContinuaDisabled(gdpr.buttonContinuaRES);
			ic.verifyComponentExistence(ic.esciRESButton);*/
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
