package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentaleComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaContrattoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;
import javassist.bytecode.analysis.Util;


public class ConfermaPrimaAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {


			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			RiepilogoOffertaComponent check=new RiepilogoOffertaComponent(driver);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			SeleniumUtilities util=new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			String frame="";

		if (!prop.getProperty("REGISTRAZIONE_VOCAL","").contentEquals("N")) {
				check.verifyComponentExistence(check.sezioniConfermate);
				logger.write("Conferma Prima attivazione - Start");
				modalita.pressAndCheckSpinners(modalita.confirmQuoteButton);
//				modalita.clickComponentWithJseAndCheckSpinners(modalita.confirmQuoteButton);
				logger.write("Conferma Prima attivazione - Completed");
					}
				
				//logger.write("Torna all'Offerta - Start");
				modalita.switchFrame();
				logger.write("Corretta apertura della pagina con le sezioni Cliente, Riepilogo offerta, sommario offerta e click sul pulsante 'Torna all'offerta' - Start");
				
				offer.verifyComponentExistence(offer.sezioneCliente);
				offer.verifyComponentExistence(offer.sezioneSommarioOfferta);
				offer.verifyComponentExistence(offer.pageRiepilogoOff);
				if (prop.getProperty("CARICA_DOCUMENTO","").contentEquals("Y")) {
					DocumentiUploadComponent doc=new DocumentiUploadComponent(driver);



					TimeUnit.SECONDS.sleep(3);
					offer.verifyComponentExistence(doc.caricaDocumenti);


					logger.write("Upload Documento di riconoscimento - Start");
					TimeUnit.SECONDS.sleep(10);

					GestioneDocumentaleComponent documentale = new GestioneDocumentaleComponent(driver);
					TimeUnit.SECONDS.sleep(2);



					logger.write("Caricamento documento - Start");

					//il metodo verifica presenza e in caso negativo recupera pdf dalla folder del progetto

					frame = documentale.caricaDocumento(Utility.getDocumentPdf(prop));
					prop.setProperty("frame", frame);
					logger.write("Caricamento documento - Completed");



					logger.write("Upload Documento di riconoscimento - Completed");
					driver.switchTo().defaultContent();
				}
		
			util.getFrameActive();
//			modalita.clickComponentWithJseAndCheckSpinners(modalita.tornaAllOffertaButton);
			modalita.pressAndCheckSpinners(modalita.tornaAllOffertaButton);
			driver.switchTo().defaultContent();
			//logger.write("Torna all'Offerta - Completed");
			logger.write("Corretta apertura della pagina con le sezioni Cliente, Riepilogo offerta, sommario offerta e click sul pulsante 'Torna all'offerta' - Completed");


			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
