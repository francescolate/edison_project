package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.AttivaBollettaWebComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID308Component;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID321Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PRIVATO_Modifica_Bolletta_WEB_ACR_318 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("Correct visualization of the Home Page with central text -- Starts");
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");

			logger.write("Click on services and verify the Supply service section -- Starts");
			HomeComponent hc = new HomeComponent(driver);
			hc.clickComponent(hc.services);
			DettaglioFornitureComponent dfc = new DettaglioFornitureComponent(driver);
			dfc.comprareText(dfc.serviziForniture, DettaglioFornitureComponent.SERVIZI_FORNITURE, true);
			dfc.comprareText(dfc.serviziFornitureContent, DettaglioFornitureComponent.SERVIZIFORNITURE_CONTENT, true);
			logger.write("Click on services and verify the Supply service section -- Ends");
	
			logger.write("Click on BollettaWeb and verify the active supply section presents- Starts");
			AttivaBollettaWebComponent abc = new AttivaBollettaWebComponent(driver);
			abc.verifyComponentExistence(abc.bollettaWebTileNew);
			abc.clickComponent(abc.bollettaWebTileNew);
			PrivatoBollettaWebID308Component atti = new PrivatoBollettaWebID308Component(driver);
		//	prop.setProperty("Bolletta_Web_TITLE", "Bolletta Web");
		//	prop.setProperty("Bolletta_Web_Title_SUBTEXT", "Il servizio Bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica di emissione tramite SMS.");
			logger.write("Checking if Bolletta_Web_TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By Bolletta_Web_Title = atti.Bolletta_Web_Title;
			atti.verifyComponentExistence(Bolletta_Web_Title);
	//		atti.VerifyText(Bolletta_Web_Title, prop.getProperty("Bolletta_Web_TITLE"));
			By Bolletta_Web_Title_Subtext = atti.Bolletta_Web_Title_Subtext;
			atti.verifyComponentExistence(Bolletta_Web_Title_Subtext);
	//		atti.VerifyText(Bolletta_Web_Title_Subtext, prop.getProperty("Bolletta_Web_Title_SUBTEXT"));
			abc.verifyComponentExistence(abc.bollettaWebactivesupply);
			abc.compareText(abc.bollettaWebactivesupply, AttivaBollettaWebComponent.AttivoForText, true);
			abc.verifyComponentExistence(abc.indirizzoLabel);
			abc.compareText(abc.indirizzoLabel, AttivaBollettaWebComponent.IndirizzoLabelText, true);
			abc.verifyComponentExistence(abc.indirizzoValue);
			/*abc.compareText(abc.indirizzoValue, AttivaBollettaWebComponent.IndirizzoValueTextNew, true);
			abc.verifyComponentExistence(abc.emailLabel);
			abc.compareText(abc.emailLabel, AttivaBollettaWebComponent.EmailText, true);
			abc.verifyComponentExistence(abc.emailValue);
			abc.compareText(abc.emailValue, AttivaBollettaWebComponent.EmailValueTextNew, true);*/
			logger.write("Click on BollettaWeb and verify the active supply section presents- Ends");
			
			logger.write("Click on Modifica Indirizzo Email Button and verify the page - Starts");
			PrivatoBollettaWebID321Component bol = new PrivatoBollettaWebID321Component(driver);
			By modificaBollettaWeb_button = bol.modificaBollettaWeb_button;
			bol.verifyComponentExistence(modificaBollettaWeb_button);
			bol.clickComponent(modificaBollettaWeb_button);
			logger.write("Validate for BollettaWebPageTitle and click on modificaBollettaWeb_button - Completed");
			Thread.sleep(40000);
		//	prop.setProperty("Modifica_Bolletta_Web_TITLE", "Modifica Bolletta Web");
		//	prop.setProperty("Modifica_Bolletta_Web_Title_SUBTEXT", "Seleziona una o più forniture su cui intendi modificare il servizio di Bolletta Web.");
			logger.write("Validate for modificaBollettaWeb_Title, modificaBollettaWeb_TitleSubtext, select SUPPLY and click on CONTINUA_Button - Start");
			By modificaBollettaWeb_Title = bol.modificaBollettaWeb_Title;
			bol.verifyComponentExistence(modificaBollettaWeb_Title);
		//	bol.VerifyText(modificaBollettaWeb_Title, prop.getProperty("Modifica_Bolletta_Web_TITLE"));
			By modificaBollettaWeb_TitleSubtext = bol.modificaBollettaWeb_TitleSubtext;
			bol.verifyComponentExistence(modificaBollettaWeb_TitleSubtext);
		//	bol.VerifyText(modificaBollettaWeb_TitleSubtext, prop.getProperty("Modifica_Bolletta_Web_Title_SUBTEXT"));
			Thread.sleep(5000);
			By supplySelect = bol.supplySelect;
			bol.verifyComponentExistence(supplySelect);
			bol.clickComponent(supplySelect);
			logger.write("Click on Modifica Indirizzo Email Button and verify the page - Ends");		
			
			logger.write("Click contunue Button and verify the page - Starts");
			By CONTINUA_Button = bol.CONTINUA_Button;
			bol.verifyComponentExistence(CONTINUA_Button);
			bol.clickComponent(CONTINUA_Button);
			Thread.sleep(5000);
			logger.write("Click contunue Button and verify the page - Ends");
			
			logger.write("Checking presence of DISATTIVAZIONE steps - Start");
			bol.verifyComponentExistence(bol.DisattivazioneSteps);
			logger.write("Checking presence of DISATTIVAZIONE steps - Completed");
			logger.write("Verifying the content of DISATTIVAZIONE steps and Color of step 1 - Start");
			bol.validateDisattivazioneSteps();
			logger.write("Verifying the content of DISATTIVAZIONE steps and Color of step 1 - Completed");
			logger.write("Verify the color of Inserimento_dati - Start");
			bol.checkColor(bol.Inserimento_dati, bol.servicesColor, "Inserimento_dati");
			logger.write("Verify the color of Inserimento_dati - Start");
			
		
			
			logger.write("Set Field Values and click on continuaButton - Start");
			bol.enterInput(bol.emailInputField, prop.getProperty("EMAIL"));
			bol.enterInput(bol.confermaEmailInputField,prop.getProperty("EMAIL"));
			bol.enterInput(bol.cellulareInputField, prop.getProperty("CELLULARE"));
			bol.enterInput(bol.confermaCellulareInputField, prop.getProperty("CELLULARE"));
			By continuaButton = bol.continuaButton;
			bol.verifyComponentExistence(continuaButton);
			bol.clickComponent(continuaButton);
			Thread.sleep(5000);
			logger.write("Set Field Values and click on continuaButton - Completed");
			
			logger.write("Go back and click on continuaButton - Start");
			By backButton = bol.backButton;
			bol.verifyComponentExistence(backButton);
			bol.clickComponent(backButton);
			
			bol.verifyComponentExistence(continuaButton);
			bol.clickComponent(continuaButton);
			logger.write("Go back and click on continuaButton - Start");
	
			logger.write("Checking presence of DISATTIVAZIONE steps - Start");
			bol.verifyComponentExistence(bol.DisattivazioneSteps);
			logger.write("Checking presence of DISATTIVAZIONE steps - Completed");
			logger.write("Verify the color of Inserimento_dati - Start");
			bol.checkColor(bol.Inserimento_dati, bol.successColor, "Inserimento_dati");
			logger.write("Verify the color of Inserimento_dati - Start");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			bol.checkColor(bol.Riepilogo_e_conferma_dati, bol.servicesColor, "Riepilogo_e_conferma_dati");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			
			logger.write("Click on confirm button and verify - Start");
			By confermaButton = bol.confermaButton;
			bol.verifyComponentExistence(confermaButton);
			bol.clickComponent(confermaButton);
			Thread.sleep(10000);
			logger.write("Click on confirm button and verify - Start");
			
			//Footer & Header verify starts
			logger.write("Verification of Header - Start ");
			ModConsensiACR162Component mod = new ModConsensiACR162Component(driver);
			mod.verifyComponentExistence(mod.headerLogo);
			mod.verifyComponentExistence(mod.headerUserLogo);
			logger.write("Verification of Header - End ");
			
			logger.write("Verification of Footer - Start ");		
			//prop.setProperty("BEFORE_FOOTER_TEXT", "© Enel Energia S.p.a.");
			//mod.comprareText(mod.footerBeforeText, prop.getProperty("BEFORE_FOOTER_TEXT"), true);
			//prop.setProperty("TUTTI_DIRITTI_RISERVATI", "Tutti i Diritti Riservati");
			//mod.comprareText(mod.tuttiDiritti, prop.getProperty("TUTTI_DIRITTI_RISERVATI"), true);
			//prop.setProperty("PIVA", "P. IVA 06655971007");
			//prop.setProperty("PIVA", "Gruppo IVA Enel P.IVA 15844561009");
			//mod.comprareText(mod.pIVANew, prop.getProperty("PIVA"), true);
			mod.verifyComponentExistence(mod.footerBeforeText);
			mod.verifyComponentExistence(mod.tuttiDiritti);
			mod.verifyComponentExistence(mod.pIVANew);
			mod.verifyComponentExistence(mod.informazionLegali);
			mod.verifyComponentExistence(mod.privacy);
			mod.verifyComponentExistence(mod.credits);
			mod.verifyComponentExistence(mod.contattacci);
			logger.write("Verification of Footer - End ");
			// Footer & Header verify ends
			
			/*By finebutton = bol.finebutton;
			bol.verifyComponentExistence(finebutton);
			Thread.sleep(5000);
			bol.clickComponent(finebutton);
			
			logger.write("Click on ESCI and Correct visualization of the Home Page with text- Starts");
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Click on ESCI andCorrect visualization of the Home Page with central text -- Ends");
*/
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
