package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Step;
import io.qameta.allure.aspects.StepsAspects;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;

public class RecuperaDatiWorkbenchModificaStatoResidenza {

    private final static int SEC = 120;

    public static void main(String[] args) throws Exception {
        Properties prop = null;

        try {

            prop = WebDriverManager.getPropertiesIstance(args[0]);
            RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
            QANTTLogger logger = new QANTTLogger(prop);

            if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

                LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
                WorkbenchComponent a = new WorkbenchComponent(driver);
                page.navigate(Costanti.salesforceLink);
                page.enterUsername(Costanti.workbenchUser);
                page.enterPassword(Costanti.workbenchPwd);
                page.submitLogin();
                TimeUnit.SECONDS.sleep(5);
                page.navigate(Costanti.workbenchLink);
                a.selezionaEnvironment(Costanti.workbenchSelectionEnv);
                a.pressButton(a.checkAgree);
                a.pressButton(a.buttonLogin);
                TimeUnit.SECONDS.sleep(2);


                while (!driver.getCurrentUrl().startsWith(Costanti.workbenchLink)) {
                    //TODO COUNT TENTATIVI DI LOGIN
                    page.enterUsername(Costanti.workbenchUser);
                    page.enterPassword(Costanti.workbenchPwd);
                    page.submitLogin();
                    TimeUnit.SECONDS.sleep(2);
                }

                a.AbilitaJoin();
                a.inserisciNuovaQuery();
                //RANDOM ERROR ON SOME ELEMENT OF THE FUNCTION inserisciNuovaQuery() , sometimes the click on the item is not made even if the process progresses
                a.insertQuery(prop.getProperty("QUERY"));
                boolean condition = false;
                int tentativi = 5;
                while (!condition && tentativi-- > 0) {
                    a.pressButton(a.submitQuery);
                    condition = a.aspettaRisultati(SEC);
                }
                if (condition) {
                    a.recuperaRisultati(Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE", "1")), prop);
                } else
                    throw new Exception("Impossibile recuperare i risultati dalla query workbench. Sono stati fatti : " + tentativi + "tentativi di recupero");

                a.logoutWorkbench();
            }
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

        } finally {
            //Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }
    }
}