package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ModifyConsents_SF_Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyConsents_SF {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModifyConsents_SF_Component resetComp = new ModifyConsents_SF_Component(driver);
			
			System.out.println("Searching for CF/IVA");
			logger.write("Searching for CF/IVA - Start");
			//driver.manage().window().maximize();
			resetComp.searchCfIva(prop.getProperty("CF"));
			logger.write("Searching for CF/IVA - Completed");
			
			System.out.println("Accessing page for client");
			logger.write("Accessing page for client - Start");
			resetComp.verifyVisibilityThenClick(resetComp.clientNameLink);
			logger.write("Accessing page for client - Completed");
			
			System.out.println("Accessing storico privacy first record");
			logger.write("Accessing storico privacy first record - Start");
			//System.out.println(driver.getWindowHandle());
			Thread.sleep(30000);
			resetComp.accessStoricoPrivacy();
			logger.write("Accessing storico privacy first record - Completed");
			
			System.out.println("Deleting first record");
			logger.write("Deleting first record - Start");
			Thread.sleep(30000);
			resetComp.deleteRecord();
			logger.write("Deleting first record - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
