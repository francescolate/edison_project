package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OCR_Module_Component;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.components.colla.SubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeID31 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			logger.write("verifica presenza dell'offerta 'Ore Free' - Start");
			OreFreeComponent offerta=new OreFreeComponent(driver);
			
			/*By offerta_orefree=offerta.offertaOreFree;
			offerta.verifyComponentExistence(offerta_orefree);
			offerta.scrollComponent(offerta_orefree);*/
			logger.write("verifica presenza dell'offerta 'Ore Free' - Completed");
			
			logger.write("Click sul Link VERIFICA DISPONIBITA' e check sulla popup - Start");
			By verificadisponibilita_Button=offerta.verificadisponibilitaButton;
			offerta.verifyComponentExistence(verificadisponibilita_Button);
			offerta.clickComponent(verificadisponibilita_Button);
			
	    	By inserisci_NumeroPod=offerta.inserisciNumeroPod;
			offerta.verifyComponentExistence(inserisci_NumeroPod);
			
			By testo_popup=offerta.testopopup;
			offerta.verifyComponentExistence(testo_popup);
			
			By campo_CodicePod=offerta.campoCodicePod;
			offerta.verifyComponentExistence(campo_CodicePod);
			
			By conferma_Button=offerta.confermaButton;
			offerta.verifyComponentExistence(conferma_Button);
			
			By link_pod=offerta.linkpod;
			offerta.verifyComponentExistence(link_pod);
			logger.write("Click sul Link VERIFICA DISPONIBITA' e check sulla popup - Completed");
			
			logger.write("Inserire un POD tipo :IT002E0000000A,premere CONFERMA e verificare che si proceda con la navigazione - Start");
			TimeUnit.SECONDS.sleep(2);
			String pod=offerta.generateRandomPodNumberEnergia();
			prop.setProperty("POD", pod);
			TimeUnit.SECONDS.sleep(1);
			offerta.enterPodValue(campo_CodicePod, "IT002E8349229A");
			TimeUnit.SECONDS.sleep(5);
			offerta.clickeaspetta(conferma_Button);
			
			/*	
			TimeUnit.SECONDS.sleep(2);
			By titolo_Page=offerta.titoloPage;
			offerta.verifyComponentExistence(titolo_Page);
			logger.write("Inserire un POD tipo :IT002E8348229A,premere CONFERMA e verificare che si proceda con la navigazione - Completed");
			*/
			
			
			OCR_Module_Component ocr = new OCR_Module_Component(driver);
			Thread.sleep(2000);
			ocr.checkHeader("Scegli Tu Ore Free", driver);
			ocr.checkPage();
			ocr.clickComponent(ocr.compilaManualmenteButton);
			ocr.inserisciDati("", "", "", "", "");
			ocr.checkCampoObbligatorio();
			ocr.inserisciDati("abc", "123", "", "", "");
			ocr.checkFormatoNonCorretto();
			ocr.inserisciDati("abc", "qwerty", "123", "", "");
			ocr.checkFormatoNonCorretto();
			ocr.clickComponent(ocr.calcolaCF);
			ocr.clickComponent(ocr.calcolaButtonCF);
			
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
