package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SupplyFilters_BsnComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SupplyFilters_Bsn {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			System.out.println("Accessing supplies section");
			logger.write("Accessing supplies section - Start");
			SupplyFilters_BsnComponent supFil = new SupplyFilters_BsnComponent(driver);
			supFil.verifyComponentVisibility(supFil.fornitureMenuItem);
			supFil.clickComponent(supFil.fornitureMenuItem);
			logger.write("Accessing supplies section - Completed");
			
			/*System.out.println("Showing and verifying filter elements");
			logger.write("Showing and verifying filter elements - Start");
			supFil.verifyComponentVisibility(supFil.showFiltersBtn);
			supFil.clickComponent(supFil.showFiltersBtn);
			supFil.checkFilterFields();
			logger.write("Showing and verifying filter elements - Completed");
			
			System.out.println("Verifying address filter");
			logger.write("Verifying address filter - Start");
			supFil.checkAddressFilter();
			logger.write("Verifying address filter - Completed");
			
			System.out.println("Verifying city filter");
			logger.write("Verifying address city - Start");
			supFil.checkCityFilter();
			logger.write("Verifying address city - Completed");
			
			System.out.println("Verifying status filter");
			logger.write("Verifying status filter - Start");
			supFil.checkStatusFilter();
			logger.write("Verifying status filter - Completed");
			
			System.out.println("Verifying type filter");
			logger.write("Verifying type filter - Start");
			supFil.checkTypeFilter();
			logger.write("Verifying type filter - Completed");
			
			System.out.println("Verifying client number filter");
			logger.write("Verifying client number filter - Start");
			supFil.checkClientNumberFilter();
			logger.write("Verifying client number filter - Completed");*/
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		
	}

}
