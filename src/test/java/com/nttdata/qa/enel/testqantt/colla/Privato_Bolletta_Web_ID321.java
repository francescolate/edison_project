package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID321Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID321 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivatoBollettaWebID321Component bol = new PrivatoBollettaWebID321Component(driver);
			
			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
                bol.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));
			
			bol.verifyComponentVisibility(bol.homePageCentralText);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			By homePage = bol.homePageCentralText;
			bol.verifyComponentExistence(homePage);
			bol.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			By homepageSubheading = bol.homePageCentralSubText;
			bol.verifyComponentExistence(homepageSubheading);
			bol.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			By sectionTitle = bol.sectionTitle;
			bol.verifyComponentExistence(sectionTitle);
			bol.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			By sectionTitleSubText = bol.sectionTitleSubText;
			bol.verifyComponentExistence(sectionTitleSubText);
			bol.VerifyText(sectionTitleSubText, bol.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			logger.write("Click on 'serviziSelect' menu item - Start");
			By servizi = bol.serviziSelect;
			bol.verifyComponentExistence(servizi);
			bol.clickComponent(servizi);
			logger.write("Click on 'serviziSelect' menu item - Completed");
			Thread.sleep(5000);

			bol.verifyComponentVisibility(bol.serviziPage);
			
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Start");
			By serviziHomePage = bol.serviziPage;
			bol.verifyComponentExistence(serviziHomePage);
			bol.VerifyText(serviziHomePage, prop.getProperty("SERVIZI_HOMEPAGE"));
			logger.write("Checking if user navigated to SERVIZI_HOMEPAGE - Completed");
			
			logger.write("Validate for serviziSectionTitle and click on BollettaWebTile - Start");
			By serviziSectionTitle = bol.serviziSectionTitle;
			bol.verifyComponentExistence(serviziSectionTitle);
			bol.VerifyText(serviziSectionTitle, prop.getProperty("SERVIZI_HOMEPAGE_SECTIONTITLE"));
			
			By BollettaWebTile = bol.BollettaWebTile;
			bol.verifyComponentExistence(BollettaWebTile);
			bol.clickComponent(BollettaWebTile);
			logger.write("Validate for serviziSectionTitle and click on BollettaWebTile - Completed");
			Thread.sleep(5000);
			
			logger.write("Validate for BollettaWebPageTitle and click on modificaBollettaWeb_button - Start");
			By BollettaWebPageTitle = bol.BollettaWebPageTitle;
			bol.verifyComponentExistence(BollettaWebPageTitle);
			bol.VerifyText(BollettaWebPageTitle, prop.getProperty("BOLLETTA_WEBPAGE_TITLE"));
			Thread.sleep(5000);
			
			By modificaBollettaWeb_button = bol.modificaBollettaWeb_button;
			bol.verifyComponentExistence(modificaBollettaWeb_button);
			bol.clickComponent(modificaBollettaWeb_button);
			logger.write("Validate for BollettaWebPageTitle and click on modificaBollettaWeb_button - Completed");
			Thread.sleep(15000);

			logger.write("Validate for modificaBollettaWeb_Title, modificaBollettaWeb_TitleSubtext, select SUPPLY and click on CONTINUA_Button - Start");
			By modificaBollettaWeb_Title = bol.modificaBollettaWeb_Title;
			bol.verifyComponentExistence(modificaBollettaWeb_Title);
			bol.VerifyText(modificaBollettaWeb_Title, prop.getProperty("MODIFICA_BOLLETTA_WEBPAGE_TITLE"));
			
			By modificaBollettaWeb_TitleSubtext = bol.modificaBollettaWeb_TitleSubtext;
			bol.verifyComponentExistence(modificaBollettaWeb_TitleSubtext);
			bol.VerifyText(modificaBollettaWeb_TitleSubtext, prop.getProperty("MODIFICA_BOLLETTA_WEBPAGE_TITLE_SUBTEXT"));
			
			By supplySelect = bol.supplySelect;
			bol.verifyComponentExistence(supplySelect);
			bol.clickComponent(supplySelect);
			
			By CONTINUA_Button = bol.CONTINUA_Button;
			bol.verifyComponentExistence(CONTINUA_Button);
			bol.clickComponent(CONTINUA_Button);
			logger.write("Validate for modificaBollettaWeb_Title, modificaBollettaWeb_TitleSubtext, select SUPPLY and click on CONTINUA_Button - Completed");
			Thread.sleep(5000);
			
			logger.write("Checking presence of DISATTIVAZIONE steps - Start");
			bol.verifyComponentExistence(bol.DisattivazioneSteps);
			logger.write("Checking presence of DISATTIVAZIONE steps - Completed");
			
			logger.write("Verifying the content of DISATTIVAZIONE steps and Color of step 1 - Start");
			bol.validateDisattivazioneSteps();
			logger.write("Verifying the content of DISATTIVAZIONE steps and Color of step 1 - Completed");
			
			logger.write("Verify the color of Inserimento_dati - Start");
			bol.checkColor(bol.Inserimento_dati, bol.servicesColor, "Inserimento_dati");
			logger.write("Verify the color of Inserimento_dati - Start");
			
			logger.write("Set Field Values and click on continuaButton - Start");
			bol.enterInput(bol.emailInputField, prop.getProperty("EMAIL"));
			bol.enterInput(bol.confermaEmailInputField,prop.getProperty("EMAIL"));
			bol.enterInput(bol.cellulareInputField, prop.getProperty("CELLULARE"));
			bol.enterInput(bol.confermaCellulareInputField, prop.getProperty("CELLULARE"));
			
			By continuaButton = bol.continuaButton;
			bol.verifyComponentExistence(continuaButton);
			bol.clickComponent(continuaButton);
			logger.write("Set Field Values and click on continuaButton - Completed");
			Thread.sleep(5000);
			
			logger.write("Checking presence of DISATTIVAZIONE steps - Start");
			bol.verifyComponentExistence(bol.DisattivazioneSteps);
			logger.write("Checking presence of DISATTIVAZIONE steps - Completed");
			
			logger.write("Verify the color of Inserimento_dati - Start");
			bol.checkColor(bol.Inserimento_dati, bol.successColor, "Inserimento_dati");
			logger.write("Verify the color of Inserimento_dati - Start");
			
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			bol.checkColor(bol.Riepilogo_e_conferma_dati, bol.servicesColor, "Riepilogo_e_conferma_dati");
			logger.write("Verify the color of Riepilogo_e_conferma_dati - Start");
			
			logger.write("Click on 'fornitureSelect' menu item - Start");
			By fornitureSelect = bol.fornitureSelect;
			bol.verifyComponentExistence(fornitureSelect);
			bol.clickComponent(fornitureSelect);
			logger.write("Click on 'fornitureSelect' menu item - Completed");
			
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Start");
			bol.verifyComponentExistence(bol.dailogTitle);
			bol.verifyComponentExistence(bol.dailogClose);
			bol.verifyComponentExistence(bol.dailog_No);
			bol.verifyComponentExistence(bol.dailog_Yes);
			
			bol.clickComponent(bol.dailog_No);
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Completed");
			
			logger.write("Check back if  modificaBollettaWeb_Title is displayed - Start");
			bol.verifyComponentExistence(modificaBollettaWeb_Title);
			bol.VerifyText(modificaBollettaWeb_Title, prop.getProperty("MODIFICA_BOLLETTA_WEBPAGE_TITLE"));
			logger.write("Check back if  modificaBollettaWeb_Title is displayed - Completed");
			
			logger.write("Click on 'fornitureSelect' menu item - Start");
			bol.verifyComponentExistence(fornitureSelect);
			bol.clickComponent(fornitureSelect);
			logger.write("Click on 'fornitureSelect' menu item - Completed");
			
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Start");
			bol.verifyComponentExistence(bol.dailogTitle);
			bol.verifyComponentExistence(bol.dailogClose);
			bol.verifyComponentExistence(bol.dailog_No);
			bol.verifyComponentExistence(bol.dailog_Yes);
			
			bol.clickComponent(bol.dailog_Yes);
			logger.write("Verify DailogBox Section then click on 'dailog_No' - Completed");
			
			Thread.sleep(10000);
			
			logger.write("Checking if heading and sub-heading of homePage is displayed - Start");
			bol.verifyComponentExistence(homePage);
			bol.VerifyText(homePage, prop.getProperty("HOMEPAGE_HEADING"));
						
			bol.verifyComponentExistence(homepageSubheading);
			bol.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_SUBHEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Start");
			bol.verifyComponentExistence(sectionTitle);
			bol.VerifyText(sectionTitle, prop.getProperty("SECTION_TITLE"));
			
			bol.verifyComponentExistence(sectionTitleSubText);
			bol.VerifyText(sectionTitleSubText, bol.SectionTitleSubText);
			logger.write("Checking if SECTION-TITLE and SECTION-TITLE-SUBTEXT of homePage is displayed - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}
