package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
/**
 * valorizza le property CODICE_FISCALE, CODICE_FISCALE_CL_USCENTE quando non sono state settate
 * @param CODICE_FISCALE  quando questa property è valorizzata il CF del cliente entrante NON viene estratto mediante query
 * @param CODICE_FISCALE_CL_USCENTE quando questa property è valorizzata il CF del cliente uscente NON viene estratto mediante query
 * @param RIGA_DA_ESTRARRE 
 * @throws Exception
 */
public  class SetPropertyFor_VSA {

	 
	
	static String nomeModulo=SetPropertyFor_VSA.class.getSimpleName();
	
	public static void main(String[] args) throws Exception
	{

		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			

				WorkbenchQuery wb=new WorkbenchQuery();
				wb.Login(args[0]);
				String query=prop.getProperty("QUERY");
				Map<Integer,Map> rows=wb.EseguiQuery(query);



				
				Integer rowIndexCF=Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE","1"));

				String cfEntrante="XXXXXX";
				String cfUscente="";
				String pod="";


				if(prop.getProperty("CODICE_FISCALE","").contentEquals(""))
				{
					logger.write("Avvio estrazione cliente entrante da query");
					cfEntrante=rows.get(rowIndexCF).get("Account.NE__Fiscal_code__c").toString();		
					prop.setProperty("CODICE_FISCALE", cfEntrante);
					logger.write(String.format("Cliente Entrante estratto da quary - CF: %s", cfEntrante));
					rowIndexCF++;
				}
				else
				{
					cfEntrante=prop.getProperty("CODICE_FISCALE");
					logger.write(String.format("il cliente entrante NON estratto da query in quanto già  settato nei property -CF: %s - ",cfEntrante));
				}
				

				if(prop.getProperty("CODICE_FISCALE_CL_USCENTE","").contentEquals(""))
				{
					logger.write("Avvio estrazione cliente Uscente da query");
					Integer rowIndexCF_Uscente=rowIndexCF;
					for(Integer rowIndex:rows.keySet())
					{
						if(rowIndex<=rowIndexCF)
						{
							rowIndexCF_Uscente++;
							continue;
						}

						String cf=rows.get(rowIndex).get("Account.NE__Fiscal_code__c").toString();
						if(cf.contentEquals(cfEntrante))
						{
							rowIndexCF_Uscente++;
							continue;
						}
						else
						{
							cfUscente=cf;
							pod=rows.get(rowIndex).get("NE__Service_Point__c.ITA_IFM_POD__c").toString();
							prop.setProperty("CODICE_FISCALE_CL_USCENTE", cfUscente);
							prop.setProperty("POD", pod);
							logger.write(String.format("Cliente Uscente estratto da query CF: %s, POD: %s", cfUscente,pod));
							rowIndexCF_Uscente++;
							break;

						}

					}
				}


			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}


	
	
	
	public static String recupera_CF_POD_per_Volture_id39=
			"SELECT "
					+ "AccountId, "
					+ "Account.RecordType.Name, "
					+ "Account.NE__Fiscal_code__c, "
					+ "Account.Name, "
					+ "account.ITA_IFM_CustomerType__c,\r\n"
					+ "RecordType.Name, "
					+ "NE__Service_Point__r.ITA_IFM_POD__c, "
					+ "ITA_IFM_Commodity__c, NE__Status__c, "
					+ "ITA_IFM_Resident__c,\r\n"
					+ "NE__Service_Point__r.ITA_IFM_SupplierId__c, "
					+ "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, "
					+ "NE_BillingProf_Type__c\r\n"
					+ "FROM  Asset "
					+ "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\r\n"
					+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
					+ "AND Account.ITA_IFM_CustomerType__c = 'Casa'\r\n"
					+ "AND RecordType.Name = 'Commodity'\r\n"
					+ "AND NE__Status__c = 'Active'\r\n"
					+ "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\r\n"
					+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n"
					+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
					+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
					+ "LIMIT 30";
	
	public static String recupera_CF_POD_per_Volture_id40=
			"SELECT AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c,\r\n"
			+ "RecordType.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Commodity__c, NE__Status__c, ITA_IFM_Resident__c,\r\n"
			+ "NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c\r\n"
			+ "FROM  Asset WHERE CreatedDate > 2020-01-01T00:00:00.000+0000\r\n"
			+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
			+ "AND Account.ITA_IFM_CustomerType__c = 'Casa'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND NE__Status__c = 'Active'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\r\n"
			+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "LIMIT 30";

}
