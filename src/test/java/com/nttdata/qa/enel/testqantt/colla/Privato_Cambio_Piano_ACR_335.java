package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.DirittodiPortabilitaComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.HomePageResidentialComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaMyComponent;
import com.nttdata.qa.enel.components.colla.ModificaInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.Privato336ACRCambioPianoComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.components.colla.SupplyDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Cambio_Piano_ACR_335 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			DirittodiPortabilitaComponent dpc = new DirittodiPortabilitaComponent(driver);
			
			if(driver.findElement(By.xpath("//button[@class='remodal-close popupLogin-close']")).isDisplayed())
                dpc.clickComponent(By.xpath("//button[@class='remodal-close popupLogin-close']"));

			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Correct visualization of the Home Page with central text -- Ends");
		
			logger.write("Correct opening of the supply detail page 310506437 -- Starts");
			DettaglioFornitureComponent dfc= new DettaglioFornitureComponent(driver);
			//Given supply no
			dfc.verifyComponentExistence(dfc.dettaglioForniuturaButton310506437);
			dfc.clickComponent(dfc.dettaglioForniuturaButton310506437); 
//			dfc.verifyComponentExistence(dfc.dettaglioForniuturaButton310522248);
//			dfc.clickComponent(dfc.dettaglioForniuturaButton310522248);
			SupplyDetailComponent supply = new SupplyDetailComponent(driver);
			supply.verifyComponentExistence(supply.RinominaFornitura);
			supply.verifyComponentExistence(supply.VisualizzaBollette);
			supply.verifyComponentExistence(supply.Addressfields);
			supply.verifyComponentExistence(supply.SupplyDetailFieldsNew);
			supply.verifyComponentExistence(supply.Mostradipiu);
			logger.write("Correct opening of the supply detail page 310506437 -- Ends");
			
			logger.write("Correct opening of the page Cambio Piano -- Starts");
			Privato336ACRCambioPianoComponent pcp= new Privato336ACRCambioPianoComponent(driver);
			pcp.verifyComponentExistence(pcp.cambioPianoButton);
			pcp.clickComponent(pcp.cambioPianoButton);
			Thread.sleep(40000);
			pcp.verifyComponentExistence(pcp.iconcheckbox);
//			pcp.verifyComponentExistence(pcp.iconcontainer);
			pcp.verifyComponentExistence(pcp.indrizzo);
			pcp.verifyComponentExistence(pcp.cliente);
			logger.write("Correct opening of the page Cambio Piano -- Ends");
			
			logger.write("correct display of Cambio Piano page and select supply-- Starts");
			dpc.comprareText(dpc.cambioPianoPageDescription, DirittodiPortabilitaComponent.CambioPianoPageDescriptionNew, true);
			//Given supply no
			//pcp.verifyComponentExistence(pcp.radioButton);
			//pcp.clickComponent(pcp.radioButton); 
			//pcp.verifyComponentExistence(pcp.radioButton310522233);
			//pcp.clickComponent(pcp.radioButton310522233); 
			pcp.clickComponent(pcp.iconcheckbox); 
			dpc.verifyComponentExistence(dpc.cambioPianoButton);
			dpc.clickComponent(dpc.cambioPianoButton);
			Thread.sleep(40000);
			logger.write("correct display of Cambio Piano page and select supply-- Ends");
			
			logger.write("correct display of Cambio Piano page and select Plan-- Starts");
			dpc.comprareText(dpc.cambioPianoPlanText1, DirittodiPortabilitaComponent.CambioPianoPlanText1, true);
			dpc.comprareText(dpc.cambioPianoPlanText2, DirittodiPortabilitaComponent.CambioPianoPlanText2, true);
//			dpc.comprareText(dpc.cambioPianoPlanText3, DirittodiPortabilitaComponent.CambioPianoPlanText3, true);
			dpc.comprareText(dpc.cambioPianoPlanText4, DirittodiPortabilitaComponent.CambioPianoPlanText4, true);
			pcp.clickComponent(pcp.Plan_ButtonNew); 
			Thread.sleep(40000);
			logger.write("correct display of Cambio Piano page and select Plan-- Ends");
			
			logger.write("correct display by continue -- Starts");
			dpc.verifyComponentExistence(dpc.cambioPianoContinueButton);
			dpc.clickComponent(dpc.cambioPianoContinueButton);
			dpc.comprareText(dpc.cambioPianoPlanText5, DirittodiPortabilitaComponent.CambioPianoPlanText5New, true);
			dpc.verifyComponentExistence(dpc.cambioPianoConfirmButton);
			dpc.clickComponent(dpc.cambioPianoConfirmButton);
			dpc.comprareText(dpc.cambioPianoPlanText6, DirittodiPortabilitaComponent.CambioPianoPlanText6, true);
			dpc.comprareText(dpc.cambioPianoPlanText7, DirittodiPortabilitaComponent.CambioPianoPlanText7New, true);
			dpc.verifyComponentExistence(dpc.cambioPianoFineButton);
			dpc.clickComponent(dpc.cambioPianoFineButton);
			Thread.sleep(40000);
			logger.write("correct display by continue -- Ends");

			logger.write("Correct visualization of the Home Page with central text -- Starts");		
			dpc.comprareText(dpc.homepageHeading1, DirittodiPortabilitaComponent.HOMEPAGE_HEADING1, true);
			dpc.comprareText(dpc.homepageParagraph1, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH1, true);
			dpc.comprareText(dpc.homepageHeading2, DirittodiPortabilitaComponent.HOMEPAGE_HEADING2, true);
			dpc.comprareText(dpc.homepageParagraph2New, DirittodiPortabilitaComponent.HOMEPAGE_PARAGRAPH2New, true);
			logger.write("Start editing and click on Forniture and Correct visualization of Forniture Page with central text -- Ends");
			
			prop.setProperty("RETURN_VALUE", "OK");			
		} 
		catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} 
		finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
