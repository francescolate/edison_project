package com.nttdata.qa.enel.testqantt.colla;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaPageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ContinueWithRegistration_204 {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		WebDriver driver = WebDriverManager.getNewWebDriver(prop);
		try {
			By accessLoginPage = null;
			
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
            
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			
			By user = log.username;
			log.verifyComponentExistence(user);
			System.out.println(prop.getProperty("WP_USERNAME"));
			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));
			
			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));

			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			
			try{
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Start");
				log.verifyComponentExistence(log.areaClientiCondizioni);
				log.clickComponent(log.areaClientiCondizioni);
				log.verifyComponentExistence(log.areaClientiPrivacy);
				log.clickComponent(log.areaClientiPrivacy);
				log.verifyComponentExistence(log.terminiECondizioniButton);
				log.clickComponent(log.terminiECondizioniButton);
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Completed");
			}catch(Exception e){
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Pagina inesistente. Proseguo.");
			}
			/*
			 
			try{
				if (!prop.getProperty("AREA_CLIENTI").equals(""))
				{
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
						log.verifyComponentExistence(log.areaClientiCasa);
						log.clickComponent(log.areaClientiCasa);
					}
					else
					{
						log.verifyComponentExistence(log.areaClientiImpresa);
						log.clickComponent(log.areaClientiImpresa);
					}
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
				} else {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
				}
			}catch(Exception e){
				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
			}
			
			ContinueWithRegistrationComponent cwrc = new ContinueWithRegistrationComponent(driver);
			
			cwrc.verifyComponentExistence(cwrc.primoAccessoHeader);
			cwrc.verifyComponentExistence(cwrc.numeroClienteInputField);
			cwrc.fillInputField(cwrc.numeroClienteInputField, prop.getProperty("SUPPLY_NUMBER"));
			cwrc.verifyComponentExistence(cwrc.numeroClienteContinuaButton);
			cwrc.clickComponent(cwrc.numeroClienteContinuaButton);
			cwrc.verifyComponentExistence(cwrc.numeroClienteConfermaButton);
			cwrc.clickComponent(cwrc.numeroClienteConfermaButton);
			cwrc.verifyComponentExistence(cwrc.numeroClienteFineButton);
			cwrc.clickComponent(cwrc.numeroClienteFineButton);
			*/
			if(!prop.getProperty("ACCOUNT_TYPE").equals(""))
				if(prop.getProperty("ACCOUNT_TYPE").contains("BSN"))
					if(!prop.getProperty("AREA_CLIENTI").equals(""))
						if(prop.getProperty("AREA_CLIENTI").equals("CASA"))
							accessLoginPage=log.loginSuccessful;
						else
							accessLoginPage=log.loginBSNSuccessful;
					else
						accessLoginPage=log.loginBSNSuccessful;
				else
					accessLoginPage=log.loginSuccessful;
			else
				accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage);
			
//			if(prop.getProperty("ACCOUNT_TYPE").contains("LIKE_RES") || prop.getProperty("ACCOUNT_TYPE").contains("LIKE_BSN"))
//			{
//				boolean shouldVerifySelfCareSelectionPage = Boolean.parseBoolean(prop.getProperty("VERIFY_SC_SELECTION_PAGE", "false"));
//				if (!shouldVerifySelfCareSelectionPage) {
//					accessLoginPage=log.loginSuccessful;
//					log.verifyComponentExistence(accessLoginPage); 
//				}
//			}
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
			
			//Privato_204_ACR_Diritto_di_Portabilità.main(args);
			Thread.sleep(4000);
			PrivateAreaPageComponent pac = new PrivateAreaPageComponent(driver);
			
			logger.write("Verify the privato home page title and contents   - Start");
			
			pac.verifyComponentExistence(pac.BenvenutoTitle);
			pac.verifyComponentText(pac.BenvenutoTitle,PrivateAreaPageComponent.BENVENUTO_TITLE );
			pac.verifyComponentExistence(pac.BenvenutoContent);
			pac.verifyComponentText(pac.BenvenutoContent, PrivateAreaPageComponent.BENVENUTO_CONTENT);
			pac.verifyComponentExistence(pac.fornitureHeader);
			pac.verifyComponentText(pac.fornitureHeader, PrivateAreaPageComponent.FORNITURE_HEADER);
			pac.verifyComponentExistence(pac.fornitureContent);
			pac.verifyComponentText(pac.fornitureContent, PrivateAreaPageComponent.FORNITURE_CONTENT);
			logger.write("Verify the privato home page title and contents   - Complete");
			
			
			logger.write("Verify and click on tuoiDiritti link  - Start");
			pac.verifyComponentExistence(pac.tuoiDirittiLink);
			pac.clickComponent(pac.tuoiDirittiLink);
			logger.write("Verify and click on tuoiDiritti link  - Complete");
			
			logger.write("Verify diPortabilità title and contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitàTitle);
			pac.verifyComponentText(pac.diPortabilitàTitle, PrivateAreaPageComponent.DIPORTABILITA_TITLE);
			pac.verifyComponentExistence(pac.tuoDirittiQues1);
			pac.verifyComponentText(pac.tuoDirittiQues1, PrivateAreaPageComponent.TUOIDIRITTI_QUES1);
			pac.verifyComponentExistence(pac.tuoDirittiAns1);
			pac.verifyComponentText(pac.tuoDirittiAns1, PrivateAreaPageComponent.TUOIDIRITTI_ANS1);
			logger.write("Verify diPortabilità title and contents - Complete");
			
			logger.write("Verify question and answer contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitaLastQues);
			pac.verifyComponentText(pac.diPortabilitaLastQues, PrivateAreaPageComponent.DIPORTABILITA_LASTQUES);
			pac.verifyComponentExistence(pac.diPortabilitaLastAns);
			//pac.verifyComponentText(pac.diPortabilitaLastAns, PrivateAreaPageComponent.DIPORTABILITA_LASTANS);
			logger.write("Verify question and answer contents - Complete");
			
			logger.write("Click on AccediServizo button - Start");
			pac.verifyComponentExistence(pac.accediAlServizio);
			pac.clickComponent(pac.accediAlServizio);
			logger.write("Click on AccediServizo button - Complete");
			
			Thread.sleep(5000);
			
			logger.write("Verify the DiPortabilita contents - Start");
			pac.verifyComponentExistence(pac.diPortabilitaContent);
			pac.verifyComponentText(pac.diPortabilitaContent, PrivateAreaPageComponent.DIPORTBILITTA_CONTENT);
			logger.write("Verify the DiPortabilita contents - Start");
			
			logger.write("Verify the Dirrito contents - Start");
			pac.verifyComponentExistence(pac.dirritoContent1);
			pac.compareText(pac.dirritoContent1, PrivateAreaPageComponent.DIRRITO_CONTENT1, true);
			pac.verifyComponentExistence(pac.dirritoContent2);
			pac.compareText(pac.dirritoContent2, PrivateAreaPageComponent.DIRRITO_CONTENT2, true);
			logger.write("Verify the Dirrito contents - Complete");
			
			logger.write("Verify the Scarica link and title - Start");
			pac.verifyComponentExistence(pac.scaricaLinkTitle);
			pac.compareText(pac.scaricaLinkTitle, PrivateAreaPageComponent.SCARICALINK_TITLE, true);
			pac.verifyComponentExistence(pac.scaricaLink1);
			logger.write("Verify the Scarica link and title - Complete");
			
			
			logger.write("Verify esci and richiedi button - Start");
			pac.verifyComponentExistence(pac.esciButton);
			pac.verifyComponentExistence(pac.richiediButton);
			logger.write("Verify esci and richiedi button - Complete");
			
			logger.write("Click on richiedi button - Start");
			pac.clickComponent(pac.richiediButton);
			Thread.sleep(10000);
			logger.write("Verify esci and richiedi button - Complete");
			
			logger.write("Verify the operazione  title - Start");
			pac.verifyComponentExistence(pac.operazioneTitlte);
			logger.write("Verify the operazione  title - Complete");	
			
			logger.write("Verify the Home page title and contents - Start");
			pac.clickComponent(pac.fineButton);
			
			/*pac.verifyComponentExistence(pac.fornitureHeader);
			pac.compareText(pac.fornitureHeader, PrivateAreaPageComponent.FORNITURE_HEADER, true);
			pac.verifyComponentExistence(pac.fornitureContent);
			pac.compareText(pac.fornitureHeader,PrivateAreaPageComponent.FORNITURE_CONTENT , true);*/
			
			// Text is changed in UI
			
			pac.verifyComponentExistence(pac.bevenutoPrivataTitle);
			pac.compareText(pac.bevenutoPrivataTitle, PrivateAreaPageComponent.BEVENUTO_PRIVATA, true);
			pac.verifyComponentExistence(pac.bevenutoPrivataContent);
			pac.compareText(pac.bevenutoPrivataContent,PrivateAreaPageComponent.BEVENUTOPRIVATA_CONTENT , true);
			
			logger.write("Verify the Home page title and contents - Complete");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			cealc.restoreAvailableEmailAddresses(cealc.getListOfAvailableEmailAddresses(), prop.getProperty("WP_USERNAME"));

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}


}
