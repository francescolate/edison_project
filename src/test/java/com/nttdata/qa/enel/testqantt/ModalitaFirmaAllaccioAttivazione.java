package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.GestioneAppuntamentoComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class ModalitaFirmaAllaccioAttivazione {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		SeleniumUtilities util;

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);
			util=new SeleniumUtilities(driver);
			String tipoUtenza=prop.getProperty("TIPO_UTENZA");
			String modalitaFirma=prop.getProperty("MODALITA_FIRMA");
			String statoUbiest=Costanti.statusUbiest;

			logger.write("Selezione canale Invio - Start ");

			//Controlli se modalità firma è no vocal
			if(modalitaFirma.compareToIgnoreCase("NO VOCAL")==0){


				if (prop.getProperty("CANALE_INVIO").compareTo("EMAIL")==0) {
					modalita.selezionaCanaleInvioAllaccio("EMAIL");
					modalita.sendText("Indirizzo Email", prop.getProperty("EMAIL"));
				}
				
				if (prop.getProperty("CANALE_INVIO").compareTo("STAMPA LOCALE")==0) {
					modalita.selezionaCanaleInvioAllaccio("STAMPA LOCALE");
				}

				if (prop.getProperty("CANALE_INVIO").compareTo("POSTA")==0) {
					modalita.selezionaCanaleInvioAllaccio("POSTA");
					CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
					if(statoUbiest.compareTo("ON")==0){
						logger.write("Selezione Indirizzo Modalità firma e Canale Invio - Start");
						compila.selezionaIndirizzoEsistenteSeNonSelezionato("Modalità firma e Canale Invio");
						logger.write("Selezione Indirizzo Modalità firma e Canale Invio - Completed");
						}
						else if (statoUbiest.compareTo("OFF")==0){
							logger.write("Selezione Fornzata Indirizzo Modalità firma e Canale Invio - Start");
							compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Modalità firma e Canale Invio",prop.getProperty("CAP"),prop.getProperty("CITTA"));
							logger.write("Selezione Fornzata Indirizzo Modalità firma e Canale Invio - Completed");
						}
				}
				logger.write("Selezione canale Invio - Completed");
			}

			else if (modalitaFirma.compareToIgnoreCase("DIGITAL")==0){

				logger.write("Controllo che Modalità Firma sia precompilato su No Vocal - Start ");
				modalita.verificaModalitàFirmaPrecompilato("DIGITAL");
				logger.write("Controllo che Modalità Firma sia precompilato su No Vocal - Completed ");


				// controllo che Canale Invio sia EMAIL
				logger.write("Controllo che Canale Invio sia precompilato su No Vocal - Start ");
				modalita.verificaCanaleInvioPrecompilato("EMAIL");
				logger.write("Controllo che Canale Invio sia precompilato su No Vocal - Completed ");

				// controllo che email sia precompilato
				logger.write("Controllo che email sia precompilato - Start ");
				modalita.verificaEmailPrecompilato();
				logger.write("Controllo che email sia precompilato - Completed ");

			}

			//Click su conferma se abilitato
//			if (modalita.isElementEnabled(modalita.confermaButton)){
				if (util.exists(modalita.confermaButton, 10)){
				modalita.press(modalita.confermaButton);
			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
