package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
//import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
//import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
//import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
//import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Identificazione_Interlocutore_ID21 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
//				SeleniumUtilities util = new SeleniumUtilities(driver);
				/*SceltaProcessoComponent avvioSubentro = new SceltaProcessoComponent(driver);
				logger.write("Selezione processo - Start");
				TimeUnit.SECONDS.sleep(10);
				avvioSubentro.clickAllProcess();
				TimeUnit.SECONDS.sleep(2);
				avvioSubentro.chooseProcessToStart(avvioSubentro.avvioSubentroEVO);
				logger.write("Selezione processo - Completed");*/

				logger.write("Identificazione interlocutore EVO_v1.0 - ID18 - Start");
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
				logger.write("i campi nome, cognome e CF sono disabilitati - Start");
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoNome);
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoCognome);
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoCF);
				logger.write("i campi nome, cognome e CF sono disabilitati - Completed");
				logger.write("Click conferma - Start");
				identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
				logger.write("Click conferma - Completed");
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(3);
				
				
//				By button_conferma_2 = identificaInterlocutore.button_conferma_2;
//				identificaInterlocutore.verifyComponentExistence(button_conferma_2);
//				identificaInterlocutore.clickComponent(button_conferma_2);
//				
				logger.write("Identificazione interlocutore EVO_v1.0 - ID18 - Completed");
				
				
				prop.setProperty("RETURN_VALUE", "OK");
			} catch (Exception e) {
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//				return;
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
			}
		}

			}
