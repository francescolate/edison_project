package com.nttdata.qa.enel.testqantt;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SetCfForGestioneSdd {

	public static void main(String[] args) throws Exception {



		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String query="";
		try {



			String nomeScenario=args[0].replace(".properties","" );

			Integer rigaDaEstrarre=Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE","1"));

			logger.write("Start Settaggio CF e POD per "+nomeScenario);



			switch(nomeScenario) 
			{
			//Cliente Residenziale, con solo una fornitura ELE attiva con Uso Abitativo e Flag Residente a NO, su distributore configurato a portale e senza SR in corso sul cliente
			case "Gestione_Attivazione_SSD_EVO_id1": 

				query=query_gestione_sdd_1;
				break;
			case "Gestione_Attivazione_SSD_EVO_id2": 

				query=query_gestione_sdd_2; //TODO: FR aggiustare query 
				break;
			case "Gestione_Attivazione_SSD_EVO_id3": 

				query=query_gestione_sdd_3; //TODO: FR aggiustare query 
				break;
			case "Gestione_Attivazione_SSD_EVO_id4": 

				query=query_gestione_sdd_4; //TODO: FR aggiustare query 
				break;
			case "Variazione_SDD_EVO_Id5": 

				query=query_variazione_sdd_5;
				break;
			case "Variazione_SDD_EVO_Id6": 

				query=query_variazione_sdd_6;
				break;
			case "Variazione_SDD_EVO_Id7": 

				query=query_variazione_sdd_7;
				break;
			case "Variazione_SDD_EVO_Id8": 

				query=query_variazione_sdd_8;
				break;
			case "Revoca_SDD_EVO_Id9": 

				query=query_revoca_sdd_9;
			case "Revoca_SDD_EVO_Id10": 

				query=query_revoca_sdd_10;
				break;
			case "Gestione_Attivazione_SSD_EVO_id11": 

				query=query_gestione_sdd_1;
				break;
			case "Gestione_Attivazione_SSD_EVO_id12": 

				query=query_gestione_sdd_1;
				break;
			case "Gestione_Attivazione_SSD_EVO_id13": 
				query=query_gestione_sdd_1;
				break;
			default:
				throw new Exception("Nessun nome scenario corrispondente");


			}//FINE SWITCH



			WorkbenchQuery wb=new WorkbenchQuery();
			wb.Login(args[0]);



			Map<Integer,Map> clientiConUnaSolaFornitura=wb.EseguiQuery(query);
			String accountId=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("Account.Id").toString();
			String cf=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("Account.NE__Fiscal_code__c").toString();
			String Pod=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("NE__Service_Point__c.ITA_IFM_POD__c").toString();
			String consuptionNumber=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("ITA_IFM_ConsumptionNumber__c").toString();
			String nome=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("Account.ITA_IFM_First_name__c").toString();
			String cognome=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("Account.ITA_IFM_Last_name__c").toString();
			String indirizzo_Fornitura=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("NE__Service_Point__c.ITA_IFM_Concatenated_Supply_Address__c").toString();
			String modalitaPagamento=clientiConUnaSolaFornitura.get(rigaDaEstrarre).get("NE_BillingProf_Type__c").toString();

			prop.setProperty("CODICE_FISCALE", cf);
			prop.setProperty("POD",Pod );

			prop.setProperty("ATT_SDD_ACCOUNT_ID", accountId);
			if(prop.getProperty("TIPOLOGIA_CLIENTE").equals("RESIDENZIALE"))
			{
				prop.setProperty("ATT_SDD_CF", cf);
				prop.setProperty("ATT_SDD_NOME", nome);
				prop.setProperty("ATT_SDD_COGNOME", cognome);
			}
			else
			{
				query=query_88.replace("@Account_id@", accountId);
				Map<Integer,Map >result=wb.EseguiQuery(query);
				if(result.size()<1) throw new Exception("Impossibile estrarre CF referente per cliente "+cf +"Query: " + query);
				String cfRef=result.get(1).get("Contact.ITA_IFM_CF__c").toString();
				String contactName=result.get(1).get("Contact.Name").toString();
				
				prop.setProperty("ATT_SDD_CF", cfRef);
				prop.setProperty("ATT_SDD_NOME", contactName);
				prop.setProperty("ATT_SDD_RAGIONE_SOCIALE", cognome);
			}
			
			
			prop.setProperty("ATT_SDD_CONSUMPTIONNUMBER_SDD",consuptionNumber );
			prop.setProperty("ATT_SDD_INDIRIZZO_FORNITURA", indirizzo_Fornitura);
			prop.setProperty("ATT_SDD_CONSUMPTIONNUMBER_SDD",consuptionNumber );
			prop.setProperty("ATT_SDD_MOD_PAGAMENTO",consuptionNumber );

			logger.write("MOD_PAGAMENTO: "+modalitaPagamento);
			logger.write("CF: "+cf);
			logger.write("POD: "+Pod);
			logger.write("Fine Settaggio CF e POD per "+nomeScenario);




			prop.setProperty("RETURN_VALUE", "OK");



		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}






	static String query_gestione_sdd_1=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, \r\n"
					+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, \r\n"
					+ "NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
					+ "FROM Asset\r\n"
					+ "WHERE \r\n"
					+ "NE__Status__c = 'Active'\r\n"
					+ "AND RecordType.Name = 'Commodity'\r\n"
					+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
					+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
					+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
					+ "AND NE_BillingProf_Type__c = 'Bollettino Postale'\r\n"
					+ "AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n"
					+ "AND ITA_IFM_Commodity__c = 'ELE'\r\n"
					+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'IT30P0301503200000000246802') \r\n"
					+ "LIMIT 10";
	static String query_gestione_sdd_2=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
					+ "FROM Asset\r\n"
					+ "WHERE NE__Status__c = 'Active'\r\n"
					+ "AND RecordType.Name = 'Commodity'\r\n"
					+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
					+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
					+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
					+ "AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n"
					+ "AND ITA_IFM_Commodity__c = 'ELE'\r\n"
					+ "AND NE_BillingProf_Type__c = 'Bollettino Postale'"
					+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'FR9114508000307445982348I06') \r\n"
					+ "LIMIT 10";
	static String query_gestione_sdd_3=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "AND Account.RecordType.Name = 'Business' \r\n"
			+"AND ITA_IFM_Service_Use__c ='Uso diverso da abitazione'\r\n"
			+"AND ITA_IFM_Commodity__c = 'GAS'\r\n"
			+"AND NE_BillingProf_Type__c = 'Bollettino Postale'\r\n"
			+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'IT30P0301503200000000246802') \r\n	"
			+ "LIMIT 24";
	
	static String query_gestione_sdd_4=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+" AND Account.RecordType.Name = 'Business' \r\n"
			+" AND ITA_IFM_Service_Use__c ='Uso diverso da abitazione' \r\n"
			+" AND NE_BillingProf_Type__c = 'Bollettino Postale' \r\n"
			+" AND ITA_IFM_Commodity__c = 'GAS' \r\n"
			+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'FR9114508000307445982348I06') \r\n"
			+ "LIMIT 10";
	
	static String query_variazione_sdd_5=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c,\r\n "
			+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND Account.RecordType.Name = 'Business'\r\n"
			+ "AND NE_BillingProf_Type__c = 'RID'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso diverso da abitazione'\r\n"
			+ "AND ITA_IFM_Commodity__c = 'GAS'\r\n"
			+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'IT91K0538766280000001405978')\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "LIMIT 1";
	static String query_variazione_sdd_6=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c,\r\n "
			+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND Account.RecordType.Name = 'Business'\r\n"
			+ "AND NE_BillingProf_Type__c = 'RID'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso diverso da abitazione'\r\n"
			+ "AND ITA_IFM_Commodity__c = 'GAS'\r\n"
			+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'FR9510096000404937285628E26')\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "LIMIT 1";
	static String query_variazione_sdd_7=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c,\r\n "
			+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
			+ "AND NE_BillingProf_Type__c = 'RID'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n"
			+ "AND ITA_IFM_Commodity__c = 'ELE'\r\n"
			+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'IT91K0538766280000001405978')\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "LIMIT 1";
	static String query_variazione_sdd_8=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c,\r\n "
			+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
			+ "AND NE_BillingProf_Type__c = 'RID'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n"
			+ "AND ITA_IFM_Commodity__c = 'ELE'\r\n"
			+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'FR9510096000404937285628E26')\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "LIMIT 1";
	
	static String query_revoca_sdd_9=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c,\r\n "
			+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
			+ "AND NE_BillingProf_Type__c = 'RID'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n"
			+ "AND ITA_IFM_Commodity__c = 'ELE'\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "LIMIT 1";
	static String query_revoca_sdd_10=
			"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c,\r\n "
			+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
			+ "FROM Asset\r\n"
			+ "WHERE NE__Status__c = 'Active'\r\n"
			+ "AND RecordType.Name = 'Commodity'\r\n"
			+ "AND Account.RecordType.Name = 'Business'\r\n"
			+ "AND NE_BillingProf_Type__c = 'RID'\r\n"
			+ "AND ITA_IFM_Service_Use__c ='Uso diverso da abitazione'\r\n"
			+ "AND ITA_IFM_Commodity__c = 'GAS'\r\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
			+ "LIMIT 1";

    static String 	query_gestione_sdd_13=
    		"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, \r\n"
					+ "NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, \r\n"
					+ "NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n"
					+ "FROM Asset\r\n"
					+ "WHERE \r\n"
					+ "NE__Status__c = 'Active'\r\n"
					+ "AND RecordType.Name = 'Commodity'\r\n"
					+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
					+ "AND CreatedDate > 2021-03-01T00:00:00.000+0000\r\n"
					+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
					+ "AND NE_BillingProf_Type__c = 'Bollettino Postale'\r\n"
					+ "AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n"
					+ "AND ITA_IFM_Commodity__c = 'ELE'\r\n"
					+ "AND AccountId NOT IN (SELECT NE__Account__c FROM NE__Billing_Profile__c WHERE NE__Iban__c = 'VG06TOQD7689787919143621') \r\n"
					+ "LIMIT 10";
	static String query_88=
			"SELECT Id, IsPrimary, Role, Account.Id, Account.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Contact.Name, Contact.ITA_IFM_CF__c\r\n"
			+ "FROM AccountContactRole\r\n"
			+ "WHERE "
			//+ " isPrimary=true and"
			+ "AccountId = '@Account_id@'";
}
