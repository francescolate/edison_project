package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabPaginaPrincipaleComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.RicercaOffertaComponent;
import com.nttdata.qa.enel.components.lightning.RicercaRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class RicercaOffertaSubentro 
{
	//private Properties prop = null;
	
	@Step("Login Salesforce")
	public static void main(String[] args) throws Exception {
		
		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			RicercaOffertaComponent ricerca = new RicercaOffertaComponent(driver);
           /*
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				
				ricerca.cliccaSwitch(); //potrebbe non essere presente dipende dall'ultimo accesso
				logger.write("Ricerca Richiesta: "+prop.getProperty("NUMERO_RICHIESTA"));
				ricerca.inserisciRichiestaOld(prop.getProperty("NUMERO_RICHIESTA"));
				logger.write("Selezione Richiesta: "+prop.getProperty("NUMERO_RICHIESTA"));
				ricerca.cliccaRisultato(prop.getProperty("NUMERO_RICHIESTA"));
				logger.write("Selezione Offerta - Start");
				ricerca.selezionaOfferta();
				logger.write("Selezione Offerta - Completed");
			}
			*/
			
			
			/*
			*questo metodo risolve il problema del cambio xpath della barra di ricerca, 
			*bisogna NECESSARIAMENTE eseguire loginSalesForce.main(args) prima di eseguire questo metodo
			*M. Lombardi 01-10-2021
			*/
			Thread.sleep(30000);
			AccediTabPaginaPrincipaleComponent tabRichieste = new AccediTabPaginaPrincipaleComponent(driver);
			tabRichieste.openTab(By.xpath(
					"//span[(text()='Richieste' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]"));

			RicercaRichiesteComponent cercaRichieste = new RicercaRichiesteComponent(driver);
			cercaRichieste.searchRichiesta(prop.getProperty("NUMERO_RICHIESTA"));
			logger.write("Selezione Offerta - Start");
			ricerca.selezionaOfferta();
			logger.write("Selezione Offerta - Completed");
		
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
