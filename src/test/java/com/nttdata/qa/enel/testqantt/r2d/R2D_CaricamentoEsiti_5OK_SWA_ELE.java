package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsiti_5OK_SWA_ELE {

	@Step("R2D Caricamento esiti 5OK per SWA - ELE")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				String statopratica=prop.getProperty("STATO_R2D");
				//l'esito RD2 1OK è opzionale e viene dato solo quando lo stato della pratica è AW
				if (statopratica.compareToIgnoreCase("CI")==0)  {
						
					R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
					//Selezione Menu Caricamento Esiti
					logger.write("Code di comunicazione/Caricamento Esiti - Start");
					 menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Esiti");
					logger.write("Code di comunicazione/Caricamento Esiti - Completed");
					R2D_CaricamentoEsitiComponent caricamentoEsiti = new R2D_CaricamentoEsitiComponent(driver);
					//Selezione tipologia Caricamento
					logger.write("Selezione tipologia Caricamento - Start");
					 caricamentoEsiti.selezionaTipoCaricamento("Puntuale SWA");
					logger.write("Selezione tipologia Caricamento - Completed");
					//Indice POD
//					int indice=Integer.parseInt(prop.getProperty("INDICE_POD","1"));
//					if(prop.getProperty("ID_RICHIESTA").isEmpty()){
//						logger.write("inserisci Pod - Start");
//						 caricamentoEsiti.inserisciPod(caricamentoEsiti.inputPOD, prop.getProperty("POD"));
//						logger.write("inserisci Pod - Completed");
//					} else {
						logger.write("inserisci Id_Richiesta - Start");
//						caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA",prop.getProperty("ID_ORDINE")));
						if (!prop.getProperty("ID_RICHIESTA","").equals("")) {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM, prop.getProperty("ID_RICHIESTA"));
						} else {
							caricamentoEsiti.inserisciIdRichiestaCRM(caricamentoEsiti.inputIdRichiestaCRM, prop.getProperty("ID_ORDINE"));
						}
						logger.write("inserisci Id_Richiesta - Completed");
//					}
					//Click Cerca
					logger.write("Selezione pulsante Cerca - Start");
					 caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
					logger.write("Selezione pulsante Cerca - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Click button Azione
					logger.write("Selezione Bottone Azione Pratica - Start");
					 caricamentoEsiti.selezionaTastoAzionePratica();
					logger.write("Selezione Bottone Azione Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Verifica Stato pratica atteso
					logger.write("Verifico Stato Pratica - Start");
					 caricamentoEsiti.verificaStatoPraticaAtteso(caricamentoEsiti.statoPraticaAtteso, "CI");
					logger.write("Verifico Stato Pratica - Completed");
					TimeUnit.SECONDS.sleep(5);
					//Selezione esito 5OK
					logger.write("Caricamento Evento - Start");
					 caricamentoEsiti.selezioneEvento(caricamentoEsiti.selectEvento, prop.getProperty("EVENTO_5OK_ELE"));
					logger.write("Caricamento Evento - Completed");
					//Inserimento esito
					logger.write("Caricamento Esito OK - Start");
					 caricamentoEsiti.selezioneEsito("OK");
					logger.write("Caricamento Esito OK - Completed");
//					//Calcolo sysdate
//					Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
//					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
//					String data = simpleDateFormat.format(calendar.getTime()).toString();
	
					if(prop.getProperty("TIPO_LAVORAZIONE_CRM_ELE").contains("Disdetta standard")) {
						String dataDisdetta = prop.getProperty("DATA_SWITCH_/_DISDETTA");
						logger.write("Caricamento Data Disdetta - Start");
						 caricamentoEsiti.inserimentoDettaglioEsito5OK_DisdettaStandard_ELE(dataDisdetta);
						logger.write("Caricamento Data Disdetta - Completed");
					}
					
					else {
						//Inserimento dettaglio esito 5OK in base alla tipologia processo
						//attribuzione opzione tariffaria
						String opzionetariffaria="";
						logger.write("Caricamento Opzione Tariffaria e Data Switch - Start");
						if (prop.getProperty("USO").compareToIgnoreCase("Uso Abitativo")==0)
							opzionetariffaria="ETAD3M00A1";
						if(prop.getProperty("USO").compareToIgnoreCase("Uso Diverso da Abitazione")==0)
							opzionetariffaria="ETAB1C00C1";
						
						caricamentoEsiti.inserimentoDettaglioEsito5OK_SWA_ELE(prop.getProperty("DATA_SWITCH_/_DISDETTA"),opzionetariffaria);
						prop.setProperty("STATO_R2D", "OK");
						logger.write("Caricamento Opzione Tariffaria e  Data Switch - Completed");
						TimeUnit.SECONDS.sleep(10);
					}
				}

				//Salvataggio stato attuale pratica
//				System.out.println("Caricamento 5OK Stato: "+ prop.getProperty("STATO_PRATICA_R2D"));
//				System.out.println("Caricamento 5OK Stato Pratica: "+ prop.getProperty("STATO_R2D"));
				prop.setProperty("RETURN_VALUE", "OK");

			}
			//
			prop.setProperty("STATUS", "OK");
		} 		
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
