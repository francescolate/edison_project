package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.RilavoraScartiComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RisottomettiScarti {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			if(prop.getProperty("KO_RIPROCESSABILE").compareTo("OK")==0){
				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				SeleniumUtilities util = new SeleniumUtilities(driver);
				driver.switchTo().defaultContent();

				//Click su rilavora scarti
				RilavoraScartiComponent scarti = new RilavoraScartiComponent(driver);
				logger.write("Click pulsante Rilavora scarti - Start");
				scarti.clickRilavoraScarti();
				logger.write("Click pulsante Rilavora scarti - Completed");

				//Inserimento campi mandatory, campi Contratto Cliente, Conto contrattuale SAP, Codice BP SAP

				//scarti.insertTextByChar(scarti.inputContrattoCliente, "0098627231");
				//scarti.insertTextByChar(scarti.inputContoSAP, "009024664592");
				//scarti.insertTextByChar(scarti.inputCodiceBPSAP, "0019299143");
				//scarti.simulateTabKey(scarti.inputCodiceBPSAP);
				
				//CLick su body per prendere focus dagli oggetti

				//Verifica tutti gli indirizzi

				String statoUbiest=Costanti.statusUbiest;

				if(statoUbiest.compareTo("ON")==0){
					scarti.verificaIndirizzi();
				}
				else if (statoUbiest.compareTo("OFF")==0){
					//Viene effettuata la pressione del button 'Verifica' e forzati tutti gli indirizzi presenti in pagina
					scarti.verificaIndirizzi_forzato();
					logger.write("Click Effettua Checklist - Start");
				}

				
				
				//Click su Forza OK
				//scarti.clickRisottometti();
				
				
				//TODO:  cliccare tasto di conferma
				
				//scarti.checkSpinnersSFDC();
			}

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
