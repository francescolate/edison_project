package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSN_RevocoInfoEnelEnergiaComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RevocoRequestInfoEnergia {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			logger.write("Click on Piu Informazioni Link -- Start");
			pa.isDisplayedChatWindow(pa.chatClose);
			pa.clickComponent(pa.piuInformazioniLink);
			logger.write("Click on Piu Informazioni Link -- Completed");
			
			FornitureBSNComponent sc = new FornitureBSNComponent(driver);
			Thread.sleep(10000);
			logger.write("Verify page navigation and page details -- Start");
			pa.verifyComponentExistence(sc.pageTitle);
			Thread.sleep(5000);
			pa.verifyComponentExistence(sc.scopriDiPiuLink);
			logger.write("Verify page navigation and page details -- Completed");

			logger.write("Click on scopri di piu link -- Start");
			sc.clickComponent(sc.scopriDiPiuLink);
			Thread.sleep(10000);
			logger.write("Click on scopri di piu link -- Completed");

			BSN_RevocoInfoEnelEnergiaComponent bric = new BSN_RevocoInfoEnelEnergiaComponent(driver);
			
			try {
				
				if(!(driver.findElement(bric.PROSEGUIbutton).isDisplayed()))
				{
					logger.write("Process of revoco request -- Start");
					bric.verifyComponentExistence(bric.scopriDiPiuLink_Revoco_InfoEnelEnergia);
					bric.clickComponent(bric.scopriDiPiuLink_Revoco_InfoEnelEnergia);
					Thread.sleep(120000);
					
					bric.verifyComponentExistence(bric.disattivazione_ieePageTitle);
					bric.comprareText(bric.disattivazione_ieePageTitle, bric.DISATTIVAZIONE_IEE_PAGE_TITLE, true);
					
					bric.verifyComponentExistence(bric.disattivazione_Prosegui_button);
					bric.clickComponent(bric.disattivazione_Prosegui_button);
					Thread.sleep(40000);
					
					bric.verifyComponentExistence(bric.ieePageTitle);
					bric.comprareText(bric.ieePageTitle, bric.IEE_PAGE_TITLE, true);
					
					bric.verifyComponentExistence(bric.Attiva_Bolletta_Web_button);
					
					bric.verifyComponentExistence(bric.fineButton);
					bric.clickComponent(bric.fineButton);
					Thread.sleep(20000);
					logger.write("Process of revoco request -- Completed");
					
					logger.write("Click on Piu Informazioni Link -- Start");
					pa.isDisplayedChatWindow(pa.chatClose);
					pa.verifyComponentExistence(pa.piuInformazioniLink);
					logger.write("Click on Piu Informazioni Link -- Completed");
				}
				
			} catch (Exception e) {
				logger.write("PROSEGUI button is displayed");
			}
				
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}	

}
