package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CheckPrivateAreaBSNComponent;
import com.nttdata.qa.enel.components.colla.CheckPrivateAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaMenuBSN {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("checking the menu located on private area left side page");
			CheckPrivateAreaBSNComponent cpac = new CheckPrivateAreaBSNComponent(driver);
			cpac.checkDocumentReadyState();
			if(prop.containsKey("ACCOUNT_TYPE"))
				if(prop.getProperty("ACCOUNT_TYPE").equals("BSN"))
					cpac.verifyBSNMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_BSN);
				else
					cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_RES);
			else
				cpac.verifyMenuItemsPresenceAndOrder(cpac.PRIVATE_AREA_MENU_RES);
			logger.write("private area left menu checks: completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}

