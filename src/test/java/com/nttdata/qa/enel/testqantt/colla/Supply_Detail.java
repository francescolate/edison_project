package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.HomePageResidentialComponent;
import com.nttdata.qa.enel.components.colla.SupplyDetailComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.By;

public class Supply_Detail {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
		    prop.setProperty("INNER_TEXT", "Scegli la strada che non costa fatica, passa all’addebito diretto e scopri tutti i vantaggi di questo metodo di pagamento.");
		    prop.setProperty("ADDEBITO_TITLE", "Addebito Diretto");
		    prop.setProperty("ADDEBITO_DESCRIPTION", "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sulla tua Carta di pagamento, sul tuo Conto Corrente o sul tuo conto Paypal.");
		    prop.setProperty("POPUP_TITLE", "Lo sapevi che puoi addebitare l’importo della tua bolletta sulla tua carta di credito o sul tuo conto corrente?");

			SupplyDetailComponent supply = new SupplyDetailComponent(driver);
			
			
			By Addressfields = supply.Addressfields;
			By SupplyDetailFields = supply.SupplyDetailFields;
			By BolletinoPostale = supply.BolletinoPostal;
			By Mostradipiu = supply.Mostradipiu;
						
			supply.supplyAddressDetailsDisplay(Addressfields);
			logger.write("Supply Address Details - Completed");
			
			supply.clickComponent(Mostradipiu);
			logger.write("Bolletino Postale - Verified");
						
			supply.supplyDetailsDisplay(SupplyDetailFields);
			logger.write("Supply Details - Completed");
			
			supply.verifyComponentExistence(supply.RinominaFornitura);
			logger.write("Button RinominaFornitura - Verified");
			
			supply.verifyComponentExistence(supply.VisualizzaBollette);
			logger.write("Button Visualizza Bollette - Verified");
			
			supply.verifyComponentExistence(BolletinoPostale);
			logger.write("Bolletino Postale - Verified");
			
			supply.verifyComponentExistence(supply.IconInfoCircle);
			logger.write("Icon Info Circle - Verified");
			Thread.currentThread().sleep(6000);
			//supply.clickComponent(supply.IconInfoCircle);
			supply.jsClickObject(supply.IconInfoCircle);
			
			//Popup
			supply.VerifyText(supply.PopupTitle, prop.getProperty("POPUP_TITLE"));
			supply.VerifyText(supply.PopupInnerText, prop.getProperty("INNER_TEXT"));
			supply.verifyComponentExistence(supply.IconInfoCircle);
			supply.verifyComponentExistence(supply.ScopriButton);
			
			supply.clickComponent(supply.ScopriButton);
			
			//New page
			
			supply.VerifyText(supply.AddebitoDirettoTitle, prop.getProperty("ADDEBITO_TITLE") );
			supply.VerifyText(supply.AddebitoDirettoDescription, prop.getProperty("ADDEBITO_DESCRIPTION") );
			supply.verifyComponentExistence(supply.EsciButton);
			//supply.clickComponent(supply.EsciButton);
			supply.jsClickObject(supply.EsciButton);
			
			//NewPage
			HomePageResidentialComponent home = new HomePageResidentialComponent(driver);
			home.homepageRESIDENTIALMenu(home.leftMenu);
				
				
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	}

			
			
			


