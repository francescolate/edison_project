package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CVPComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.ConfiguraProdottoComponent;
import com.nttdata.qa.enel.components.lightning.ContattiConsensiComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoClienteComponent;
import com.nttdata.qa.enel.components.lightning.FatturazioneElettronicaComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.ModalitaFirmaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.ScontiEBonusComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class ConfermaSezioniOffertaInBozzaAllaccio {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
            
            Thread.currentThread().sleep(5000);
            
            //Inserito in data 16/09/2020 causa aggiornamento processo
			GestioneFornituraFormComponent fornitura = new GestioneFornituraFormComponent(
					driver);
			logger.write("Selezione commodity - Start");
			fornitura.selezionaFornitura(fornitura.fornitura_ele_allaccio);
			logger.write("Selezione commodity - Completed");
			
			logger.write("Conferma inserimento fornitura - Start");
			fornitura.pressAndCheckSpinners(fornitura.confermaFornitura);
			logger.write("Conferma inserimento fornitura - Completed");
			
			logger.write("Conferma - Start");
			fornitura.clickComponent(fornitura.confermaButtonInsCommodity);
			fornitura.checkSpinnersSFDC();
			logger.write("Conferma - Completed");
            
            compila.confermaIndirizzo("Indirizzo di Residenza/Sede Legale");
            logger.write("Conferma Indirizzo Residenza/Sede Legale - Completed");

            compila.confermaIndirizzo("Indirizzo di Fatturazione");
            logger.write("Conferma Indirizzo di Fatturazione - Completed");
            
            FatturazioneElettronicaComponent fatturazione = new FatturazioneElettronicaComponent(driver);
            fatturazione.pressButton(fatturazione.confermaFatturazioneElettronica);
            logger.write("Conferma Fatturazione Elettronica - Completed");

            
            ScontiEBonusComponent sconti = new ScontiEBonusComponent(driver);
            sconti.pressButton(sconti.confermaScontiEBonusSWAEVO);
            logger.write("Conferma Sconti e Bonus- Completed");
            
            ContattiConsensiComponent contatti = new ContattiConsensiComponent(driver);
		    contatti.press(contatti.confermaButton);
		    logger.write("Click Conferma Consensi e Contatti");
		    
			ModalitaFirmaComponent modalita = new ModalitaFirmaComponent(driver);

			logger.write("Selezione canale Invio - Start ");
			modalita.press(modalita.confermaButton);
            logger.write("Conferma Modalità firma e Canale Invio - Completed");
    
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
