package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


public class RecuperaClientePerAttivazioneBollettaWeb {
	
	/*
	 * 
	 * Il seguent modulo si occupa di effettuare una prima query nel workbench
	 * Salvando i dati della prima (ed unica) colonna, intervallati da virgola
	 * Da dare in pasto come condizione In('valore','valore','valore') 
	 * ed estrarne poi uno risultato da salvare nel properties
	 * 
	 * 
	 */

	private final static int SEC = 120;
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate(Costanti.workbenchLink);
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				a.pressButton(a.checkAgree);
				a.pressButton(a.buttonLogin);
				TimeUnit.SECONDS.sleep(2);
				//System.out.println(driver.getCurrentUrl());
				while (!driver.getCurrentUrl().startsWith(Costanti.workbenchLink)) {
					page.enterUsername(Costanti.workbenchUser);
					page.enterPassword(Costanti.workbenchPwd);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}
				
				prop.setProperty("QUERY_1", Costanti.query_200_1);
				prop.setProperty("QUERY_2", Costanti.query_200_2);
//				prop.setProperty("QUERY_3", Costanti.query_200_3);
//				prop.setProperty("QUERY_4", Costanti.query_200_4);
				prop.setProperty("RIGA_DA_ESTRARRE", "1");

				//Conto il numero di query da effettuare
				int numQuery = 0;
				int eseguiQuery = 0;
				for(int x = 1;prop.containsKey("QUERY_"+x); x++) {
					numQuery = x;
				}
				
				boolean condition = false;
				int tentativi = 2;
				
				for(int x = 1;x<numQuery;x++) {
					eseguiQuery=x;
						a.insertQuery(prop.getProperty("QUERY_"+x));
						condition = false;
						tentativi = 2;
						while (!condition && tentativi-- > 0) {
							a.pressButton(a.submitQuery);
							condition = a.aspettaRisultati(SEC);
						}
						if (condition) {
							String inCondition;
							if (eseguiQuery == 3) {
								inCondition = a.costruisciStringaRisultatiColonna5();
							} else {
								inCondition = a.costruisciStringaRisultatiColonna3();
							}
							
							String newQuery = String.format(prop.getProperty("QUERY_"+(x+1)), inCondition);
							prop.setProperty("QUERY_"+(x+1), newQuery);
						}else
							throw new Exception("Impossibile recuperare i risultati dalla query workbench "+prop.getProperty("QUERY_"+x));
				}
				
					a.insertQuery(prop.getProperty("QUERY_"+numQuery));
					condition = false;
					tentativi = 2;
					while (!condition && tentativi-- > 0) {
						a.pressButton(a.submitQuery);
						condition = a.aspettaRisultati(SEC);
					}
					if(condition) {
						a.recuperaRisultati(Integer.parseInt(prop.getProperty("RIGA_DA_ESTRARRE","1")), prop);
//						String newQuery = "SELECT Id, Email, ITA_IFM_Email_PEC__c, MobilePhone From Contact Where Id = '%s'";
//						newQuery = String.format(newQuery, prop.getProperty("ACCOUNT.ITA_IFM_HOLDERCONTACT__C"));
//						a.insertQuery(newQuery);
//						condition = false;
//						tentativi = 2;
//						while (!condition && tentativi-- > 0) {
//							a.pressButton(a.submitQuery);
//							condition = a.aspettaRisultati(SEC);
							Map<String, String> mappa = new HashMap<String,String>();
							mappa.put("Email", prop.getProperty("INDIRIZZO_EMAIL",prop.getProperty("INDIRIZZO_PEC")));
							mappa.put("ITA_IFM_Email_PEC__c", prop.getProperty("INDIRIZZO_EMAIL",prop.getProperty("INDIRIZZO_PEC")));							
							mappa.put("MobilePhone", prop.getProperty("CELLULARE"));
						    //a.updateRows(mappa);
							a.logoutWorkbench();
							
						}else
							throw new Exception("Impossibile recuperare i risultati dalla query workbench "+prop.getProperty("QUERY_"+numQuery));

						
						

				

			
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}