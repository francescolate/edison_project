package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SalvaEditaOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SalvaOffertaInBozzaRicontrattualizzazione {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		       logger.write("Salva Offerta In Bozza - Start");
				SeleniumUtilities util = new SeleniumUtilities(driver);
				SalvaEditaOffertaComponent salva = new SalvaEditaOffertaComponent(driver);
				salva.clickComponentWithJseAndCheckSpinners(salva.salvaInbozza);
				logger.write("Salva Offerta In Bozza - Completed");
				driver.switchTo().defaultContent();
				VerificaRichiestaComponent verifica = new VerificaRichiestaComponent(driver);
				verifica.attesaCaricamentoSezioneCliente();
				verifica.pressJavascript(verifica.elementiRichiestaSection);
				
				String flag = prop.getProperty("RECUPERA_OI_RICHIESTA","Y");
				if(flag==null || flag.length()==0) flag = "N";
				if(flag.contentEquals("Y")) {
				String id = verifica.recuperaOIRichiestaDaPod(
						prop.getProperty("POD", prop.getProperty("POD_ELE", prop.getProperty("POD_GAS"))));
				prop.setProperty("OI_RICHIESTA", id);
				}
				logger.write("Recupero valore id richiesta - Completed");
		
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
