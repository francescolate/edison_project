package com.nttdata.qa.enel.testqantt;

import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class VerificaMessaggioErroreSelezioneFornituraVolturaConAccollo {

    public static void main(String[] args) throws Exception {

        Properties prop;
        prop = WebDriverManager.getPropertiesIstance(args[0]);
        QANTTLogger logger = new QANTTLogger(prop);

        try {

            RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
            SeleniumUtilities util = new SeleniumUtilities(driver);
            SezioneSelezioneFornituraComponent forn = new SezioneSelezioneFornituraComponent(driver);

            util.getFrameActive();
            logger.write("Verifica messaggio d'errore: start");
           
            forn.verificaMessaggioErroreSelezioneFornitura(forn.messaggioErroreSelFornitura, forn.messaggioErroreTestoAtteso);
            
            logger.write("Verifica messaggio d'errore: messaggio d'errore verificato con successo");
            prop.setProperty("RETURN_VALUE", "OK");
        } catch (Exception e) {
            prop.setProperty("RETURN_VALUE", "KO");
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errors.toString();
            logger.write("ERROR_DESCRIPTION: " + errors.toString());

            prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
            if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
                throw e;
        } finally {
            // Store WebDriver Info in properties file
            prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
        }


    }

}
