package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LuceAndGasSearchSectionComponent;
import com.nttdata.qa.enel.components.colla.OffersLuceComponent;
import com.nttdata.qa.enel.components.colla.OreFreeComponent;
import com.nttdata.qa.enel.components.colla.SubentroComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class OreFreeID29 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			OreFreeComponent offerta=new OreFreeComponent(driver);
			By campo_CodicePod=offerta.campoCodicePod;
			By conferma_Button=offerta.confermaButton;
			
			logger.write("Inserire il valore non valido '"+prop.getProperty("POD")+"' nel campo POD' e click su CONFERMA - Start");
			Thread.currentThread().sleep(2000);
			offerta.enterPodValue(campo_CodicePod, prop.getProperty("POD"));
			Thread.currentThread().sleep(2000);
			offerta.clickComponent(conferma_Button);
			Thread.currentThread().sleep(2000);
			By labelformato_nonvalido=offerta.labelformatononvalido;
			offerta.verifyComponentExistence(labelformato_nonvalido);
			logger.write("Inserire il valore non valido '"+prop.getProperty("POD")+"' nel campo POD' e click su CONFERMA - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
