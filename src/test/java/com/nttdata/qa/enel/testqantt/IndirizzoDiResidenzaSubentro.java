package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class IndirizzoDiResidenzaSubentro {
	
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			RiepilogoOffertaComponent checkCampiSezioneIndirizzi=new RiepilogoOffertaComponent(driver);
			logger.write("check dei campi nella sezione 'Indirizzo Residenza/Sede Legale' e 'Indirizzo di Fatturazione' - Start");
			checkCampiSezioneIndirizzi.verificaCampiIndirizzoResidenza();
			checkCampiSezioneIndirizzi.verificaCampiIndirizzoFatturazione();
			logger.write("check dei campi nella sezione 'Indirizzo Residenza/Sede Legale' e 'Indirizzo di Fatturazione' - Completed");
			CompilaIndirizziComponent compila = new CompilaIndirizziComponent(driver);
			String tipoCliente = prop.getProperty("TIPO_CLIENTE","").toLowerCase();
			SeleniumUtilities util = new SeleniumUtilities(driver);
			String statoUbiest=Costanti.statusUbiest;

			//Se il tasto conferma è abilitato, viene confermata l'intera sezione senza inserire indirizzo
			if(!util.exists(compila.buttonConfermaIndizzoResidenzaDisabilitato,5)){
				
				if (prop.getProperty("RESIDENTE").equals("SI")) {
					logger.write("Verifica Campo \"Cliente fornisce indirizzo?\" ReadOnly - Started");
					if (!checkCampiSezioneIndirizzi.checkDisabled(checkCampiSezioneIndirizzi.fornisceIndirizzo)) {
						logger.write("Verifica Campo \"Cliente fornisce indirizzo?\" ReadOnly - Completed");
						logger.write("Verifica Esistenza \"Indirizzo Verificato\"  - Started");
						if (driver.findElement(checkCampiSezioneIndirizzi.labelIndirizzoResVerificato).isDisplayed()) {
							logger.write("Verifica Esistenza \"Indirizzo Verificato\"  - Completed");
						} else {
							logger.write("Verifica Esistenza \"Indirizzo Verificato\"  - Failed");
							throw new Exception("Il banner \"Indirizzo Verificato\" non è presente nel box");
						}
						
					} else {
						logger.write("Verifica Campo \"Cliente fornisce indirizzo?\" ReadOnly - Failed");
					}
				}
				compila.pressButton(compila.buttonConfermaIndizzoResidenza);
				
			} else {
				String fornitura_indirizzo=prop.getProperty("CLIENTE_FORNISCE_INDIRIZZO");
				if(prop.getProperty("TIPO_CLIENTE").compareTo("Residenziale")==0 || prop.getProperty("TIPO_CLIENTE").compareTo("Casa")==0){
				//Se il campo 'fornisce indirizzo' è abilitato viene popolato
				if(!util.exists(compila.clienteFornisceIndirizzoSelectDisabilitato,5)){
					logger.write("Selezione campo 'cliente fornisce indirizzo?- Start");
					compila.clienteFornisceIndirizzo(fornitura_indirizzo);
					logger.write("Selezione campo 'cliente fornisce indirizzo?- Completed");
				}
					}
						if(statoUbiest.compareTo("ON")==0){
							logger.write("Selezione Indirizzo di Fatturazione - Start");
							compila.selezionaIndirizzoEsistenteSeNonSelezionato("Indirizzo di Residenza/Sede Legale");
							logger.write("Selezione Indirizzo di Residenza/Sede Legalee - Completed");
						}
						else if (statoUbiest.compareTo("OFF")==0){
							logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Start");
							compila.selezionaIndirizzoEsistenteSeNonSelezionatoForzato("Indirizzo di Residenza/Sede Legale",prop.getProperty("CAP_FORZATURA"),prop.getProperty("CITTA_FORZATURA"));
							logger.write("Selezione Indirizzo di Residenza/Sede Legale Forzato - Completed");
						}
			}
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}

}
