// Package HerokuAPIGetUUID_Email_2 per gestione errori

package com.nttdata.qa.enel.testqantt.colla.api;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.qa.enel.components.colla.api.HerokuAPIGetUUID_Component_Err;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class HerokuAPIGetUUID_MAIL_2 {

	/**
	 * Effettuare la chiamata API 
     * Verifica risposta Json Oputput sia contenuto in Args
	 * @throws Exception
	 */
	@Step("API Heroku getuuid eMAIL Scenario_1")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			prop.setProperty("Authorization", "Bearer " + APIService.getToken());
			prop.setProperty("API_URL", prop.getProperty("API_URL") + prop.getProperty("EMAIL"));

			//Da configurare come OutPut
			prop.setProperty("CHANNELKEY","86c74228-d825-4048-b3c9-21c0af2d7cd1");
    		prop.setProperty("TID", UUID.randomUUID().toString());
    		prop.setProperty("SID", UUID.randomUUID().toString()); 
    		prop.setProperty("SOURCECHANNEL","WEB");
    		prop.setProperty("SOURCE_CHANNEL","EE");
    		prop.setProperty("TOUCHPOINT","WEB");
    		prop.setProperty("Content-Type", "application/json");
    		
			logger.write("API Request Start");
		
			HerokuAPIGetUUID_Component_Err HRKComponent = new HerokuAPIGetUUID_Component_Err();
			String dataResponce = HRKComponent.sendPostRequestReturnData(prop);

			if(!dataResponce.contains(prop.getProperty("JSON_DATA_OUTPUT")))
			    throw new Exception("Unexpected data response :" + dataResponce +"\n data response expected: " + prop.getProperty("JSON_DATA_OUTPUT"));		
			
			prop.setProperty("JsonObjectDataResponse", dataResponce);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}

}


