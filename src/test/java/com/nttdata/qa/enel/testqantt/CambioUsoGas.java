package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.server.handler.FindElement;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CambioUsoGas {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
//NUMERO_DOCUMENTO;POD;COMMODITY;CAMBIA_METODO_PAGAMENTO;METODO_PAGAMENTO;
//IBAN;USO_FORNITURA;CATEGORIA_CONSUMO;PROFILO_CONSUMO;CATEGORIA_MARKETING;
//ORDINE_GRANDEZZA;POTENZIALITA;UTILIZZO;CATEGORIA_USO
		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			logger.write("Scelta processo Cambio uso - Start");
			SceltaProcessoComponent sceltaProcesso = new SceltaProcessoComponent(driver);
			IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
			CheckListComponent checklist = new CheckListComponent(driver);
			FornitureComponent forniture = new FornitureComponent(driver);
			CambioUsoComponent cambio = new CambioUsoComponent(driver);
			IntestazioneNuovaRichiestaComponent nuovaRichiesta = new IntestazioneNuovaRichiestaComponent(driver);
			sceltaProcesso.clickAllProcess();
			sceltaProcesso.chooseProcessToStart(sceltaProcesso.cambioUso);
			logger.write("Scelta processo Cambio uso - Completed");
			
			
			
			logger.write("Verifica schermata identificazione interlocutore sia quella attesa - Start");
		
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Nome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Cognome");
			identificaInterlocutore.verifyInputFieldIsNotEnabled("Codice Fiscale");
			
			logger.write("Conferma Identificazione - Start");
			identificaInterlocutore.insertDocumento("b");
			identificaInterlocutore.pressButton(identificaInterlocutore.confirmButton);
			identificaInterlocutore.pressButton(identificaInterlocutore.conferma);
			logger.write("Conferma Identificazione - Completed");
			TimeUnit.SECONDS.sleep(10);
			
			String testoChecklist = checklist.getChecklistText(checklist.checkListBodyText);
			testoChecklist = testoChecklist.replace("\r\n", "").replace("\n", "");
			if(!testoChecklist.contentEquals(checklistMessage)) throw new Exception("Il messaggio della checklist risulta differire da quanto atteso. Verificare");
			
			
			logger.write("Verifica esistenza e click su alert sap - Start");
			cambio.checkandclickAlertSap();
			logger.write("Verifica esistenza e click su alert sap - Start");		
					
			String frame = checklist.clickFrameConferma2();
			
			String numerorichiesta = nuovaRichiesta
			.salvaNumeroRichiesta(frame,nuovaRichiesta.pNRichiestaPaginaNuovaRichiesta);
           //System.out.println(numerorichiesta);
         	prop.setProperty("NUMERO_RICHIESTA", numerorichiesta.trim());
			logger.write("Identificazione Interlocutore e verifica Checklist - Completed");
			
			logger.write("Selezione fornitura - Start");
		
			frame=forniture.cercaECliccaFornituraConFrame(prop.getProperty("POD"),
					forniture.checkboxPrimaFornituraInTabella);
			forniture.clickConfermaFornitura(frame);
			forniture.clickAvanti(frame);
			logger.write("Selezione fornitura - Completed");
			
			logger.write("Inserimento campi Cambio Uso - Start");
			CambioUsoComponent cambioUso = new CambioUsoComponent(driver);
		
			if (prop.getProperty("USO_FORNITURA").contentEquals("Uso Diverso da Abitazione")){
				cambioUso.inserisciNuoviParametriConsumoGasUsoNonAbitativo(frame, prop.getProperty("USO_FORNITURA"), prop.getProperty("CATEGORIA_CONSUMO"), prop.getProperty("PROFILO_CONSUMO"), prop.getProperty("CATEGORIA_MARKETING"), prop.getProperty("ORDINE_GRANDEZZA"),  prop.getProperty("UTILIZZO"), prop.getProperty("CATEGORIA_USO"));
			} else			
			{
			cambioUso.inserisciNuoviParametriConsumoGas(frame, prop.getProperty("USO_FORNITURA"), prop.getProperty("CATEGORIA_CONSUMO"), prop.getProperty("PROFILO_CONSUMO"), prop.getProperty("CATEGORIA_MARKETING"), prop.getProperty("ORDINE_GRANDEZZA"), prop.getProperty("POTENZIALITA"), prop.getProperty("UTILIZZO"), prop.getProperty("CATEGORIA_USO"));
			}
			logger.write("Inserimento campi Cambio Uso - Completed");
			SceltaMetodoPagamentoComponentEVO scelta = new SceltaMetodoPagamentoComponentEVO(driver);
			if(prop.getProperty("CAMBIA_METODO_PAGAMENTO").contentEquals("Y")) {
				if(prop.getProperty("METODO_PAGAMENTO").contentEquals("RID")) {
					
					scelta.selezionaNuovoMetodoPagamento(frame, prop.getProperty("IBAN"), scelta.rid, scelta.nuovoMetodoPagamento2, scelta.copiaDaIntestatario2, scelta.ibanField2, scelta.saveMetod2, scelta.buttonTextAvanti);
					
				}
				if(prop.getProperty("METODO_PAGAMENTO").contentEquals("Bollettino Postale")) {
					
					scelta.selezionaBollettino(frame, scelta.bollettinoPostale4, scelta.depositoCauzionaleBtn4, scelta.buttonTextAvanti,true);
				}
			}
			else scelta.press(frame, scelta.buttonTextAvanti);
			TimeUnit.SECONDS.sleep(60);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
	
	public static String checklistMessage="ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:  POD/PDR/numero utenza.INFORMAZIONI UTILI - Il Cambio Uso della fornitura di energia elettrica ha un costo pari a: 0,00 €.- Il Cambio Uso della fornitura di gas ha un costo pari a: 0,00 €.- Contestualmente alla variazione uso è possibile attivare/modificare/revocare i Servizi VASe il Metodo di pagamento - Per la variazione uso gas, nel caso di modifica dell'uso fornitura, è necessario eseguire il cambio prodotto.Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto."; 
}
