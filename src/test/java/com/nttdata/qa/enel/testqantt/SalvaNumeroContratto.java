package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;


import com.nttdata.qa.enel.components.lightning.VerificaRichiestaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;



public class SalvaNumeroContratto {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			VerificaRichiestaComponent contratto = new VerificaRichiestaComponent(driver);
			
			logger.write("salva il Numero Contratto - Start ");
			String numContratto=contratto.salvaNumeroContratto();
			prop.setProperty("NUMERO_CONTRATTO", numContratto);
			logger.write("salva il Numero Contratto - Completed ");
			
			if(prop.getProperty("TIPO_OPERAZIONE", "").contentEquals("PRIMA_ATTIVAZIONE_EVO") && prop.getProperty("COMMODITY", "").contentEquals("ELE")) {
				
				if(prop.getProperty("STATO_OFFERTA", "").contentEquals("Inviata")) {
					
					driver.navigate().refresh();
					TimeUnit.SECONDS.sleep(10);
					if(prop.getProperty("COMMODITY_DUAL", "").contentEquals("Y")) {
						contratto.checkOfferta("PRIMA_ATTIVAZIONE", "Dual Energy", prop.getProperty("STATO_OFFERTA"));	
					} else if (prop.getProperty("COMMODITY_MULTI_ELE", "").contentEquals("Y")){
						contratto.checkOfferta("PRIMA_ATTIVAZIONE", "Multi Ele", prop.getProperty("STATO_OFFERTA"));
					}
					else {
					contratto.checkOfferta("PRIMA_ATTIVAZIONE", "Elettricità", prop.getProperty("STATO_OFFERTA"));}
				}
				else {
				    contratto.checkOfferta("PRIMA_ATTIVAZIONE", "Elettricità", "Bozza");}
			}
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
}
