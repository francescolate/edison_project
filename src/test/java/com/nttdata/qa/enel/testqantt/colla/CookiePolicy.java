package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.HomePageHeaderComponent;
import com.nttdata.qa.enel.components.colla.CookieBannerComponent;
import com.nttdata.qa.enel.components.colla.EnterprisePageComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;


/**
 * 
 * This module file is used in the scenario WEB_Pubblico_Home_Page_AreaPubblica_14 - Home Page Area Pubblica
 * It tests the correct functionality of the cookie info banner on the bottom of the page.
 * 
 * @author Michele Cammarata
 *
 */

public class CookiePolicy {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			//WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			Thread.sleep(5000);
			//System.out.println("Accessing home page");
			logger.write("Accessing home page - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			LoginLogoutEnelCollaComponent homePage = new LoginLogoutEnelCollaComponent(driver);
			CookieBannerComponent banner = new CookieBannerComponent(driver);/*
			//accessHomePage(homePage, prop);
			Thread.sleep(5000);
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			Thread.sleep(5000);
			logger.write("Accessing home page - End");
			*/ 
			//System.out.println("Accessing enterprise page");
			logger.write("Accessing enterprise page - Start");
			HomePageHeaderComponent header = new HomePageHeaderComponent(driver);
			header.clickComponent(header.enterpriseBtn);
			Thread.sleep(20000);
			EnterprisePageComponent enterprisePage = new EnterprisePageComponent(driver);
			enterprisePage.verifyVisibilityAndText(enterprisePage.pageTitle, prop.getProperty("ENT_PAGE_TITLE"));
			Thread.sleep(20000);
			//checkCookieBanner(banner, prop);
			logger.write("Accessing enterprise page - Completed");
			
			//System.out.println("Accessing cookie policy page");
			logger.write("Accessing cookie policy page - Start");
			Thread.sleep(20000);
			By policyLink = banner.policyLinkNew;
			banner.verifyComponentExistence(policyLink);
			banner.clickComponent(policyLink);
			logger.write("Accessing cookie policy page - Completed");
			
			//System.out.println("Reaccessing home page");
			logger.write("Reaccessing home page - Start");
			driver.manage().deleteAllCookies();
			accessHomePage(homePage, prop);
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("Reaccessing home page - Completed");

			logger.write("Accept banner - Start");
			Thread.sleep(40000);
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			logger.write("Accept banner - Completed");
			
				WebDriverWait wait = new WebDriverWait(driver, 20);
			if (!wait.until(ExpectedConditions.invisibilityOfElementLocated(banner.banner))) {
				throw new Exception("Cookie policy banner has not been dismissed");
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
	private static void accessHomePage(LoginLogoutEnelCollaComponent homePage, Properties prop) throws Exception {
		homePage.launchLink(prop.getProperty("LINK"));
		By logo = homePage.logoEnel;
		homePage.verifyComponentVisibility(logo);
	}
	
	private static void checkCookieBanner(CookieBannerComponent banner, Properties prop) throws Exception {
		banner.verifyVisibilityAndText(banner.title, prop.getProperty("BANNER_TITLE"));
		banner.verifyVisibilityAndText(banner.description, prop.getProperty("BANNER_DESCRIPTION"));
		banner.verifyComponentExistence(banner.policyLink);
		banner.verifyComponentExistence(banner.acceptBtn);
		banner.verifyComponentExistence(banner.closeBtn);
	}
}
