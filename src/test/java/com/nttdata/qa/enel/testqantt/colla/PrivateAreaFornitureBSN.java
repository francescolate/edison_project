package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.PrivateAreaFornitureBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFornitureBSN {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFornitureBSNComponent pafbc = new PrivateAreaFornitureBSNComponent(driver);
			
			logger.write("cliccando sulla voce di menu forniture - START");
			if(prop.getProperty("MENU_ITEM").equals("FORNITURE"))
				pafbc.clickComponent(pafbc.fornitureMenuItem);
			else if(prop.getProperty("MENU_ITEM").equals("HOMEPAGE"))
				pafbc.clickComponent(pafbc.homepageMenuItem);
			logger.write("cliccando sulla voce di menu forniture - COMPLETED");
			
			if(prop.getProperty("MENU_ITEM").equals("FORNITURE")){
				logger.write("scrolling verso 'i tuoi dati' - START");
				Thread.sleep(10000);
				pafbc.verifyComponentExistence(pafbc.iTuoiDatiContainer);
				pafbc.scrollComponent(pafbc.iTuoiDatiContainer);
				logger.write("scrolling verso 'i tuoi dati' - COMPLETED");
	
				/*logger.write("controllando i campi della sezione 'i tuoi dati' - START");
				pafbc.compareText(pafbc.iTuoiDatiContainer, pafbc.iTuoiDatiText.split(";"), true);
				logger.write("controllando i campi della sezione 'i tuoi dati' - COMPLETED");*/
				
				logger.write("scrolling verso 'Le tue bollette' - START");
				pafbc.verifyComponentExistence(pafbc.leTueBolletteContainer);
				pafbc.scrollComponent(pafbc.leTueBolletteContainer);
				logger.write("scrolling verso 'Le tue bollette' - COMPLETED");
	
				/*logger.write("controllando i campi della sezione 'Le tue bollette' - START");
				pafbc.compareText(pafbc.leTueBolletteContainer, pafbc.leTueBolletteText.split(";"), true);
				logger.write("controllando i campi della sezione 'Le tue bollette' - COMPLETED");*/
				
			}else if(prop.getProperty("MENU_ITEM").equals("HOMEPAGE")){
				logger.write("scrolling verso 'Forniture e bollette' - START");
				pafbc.verifyComponentExistence(pafbc.fornitureEBolletteHeader);
				pafbc.scrollComponent(pafbc.fornitureEBolletteHeader);
				logger.write("scrolling verso 'Forniture e bollette' - COMPLETED");
	
				/*logger.write("controllando i campi della sezione 'Forniture e bollette' - START");
				pafbc.compareText(pafbc.panelHeadingIndex, pafbc.panelHeadingIndexText.split(";"), true);
				logger.write("controllando i campi della sezione 'Forniture e bollette' - COMPLETED");
				*/
				logger.write("scrolling verso il dettaglio della fornitura - START");
				pafbc.verifyComponentExistence(pafbc.fornituraInDettaglioContainer);
				pafbc.scrollComponent(pafbc.fornituraInDettaglioContainer);
				logger.write("scrolling verso il dettaglio della fornitura - COMPLETED");
	
				logger.write("controllando i campi del dettaglio della fornitura - START");
				pafbc.compareText(pafbc.fornituraInDettaglioContainer, pafbc.fornituraInDettaglioText.split(";"), true);
				logger.write("controllando i campi del dettaglio della fornitura - COMPLETED");
				
				logger.write("scrolling verso 'Le tue bollette' - START");
				pafbc.verifyComponentExistence(pafbc.leTueBolletteContainer);
				pafbc.scrollComponent(pafbc.leTueBolletteContainer);
				logger.write("scrolling verso 'Le tue bollette' - COMPLETED");
	
				logger.write("controllando i campi della sezione 'Le tue bollette' - START");
				pafbc.compareText(pafbc.leTueBolletteContainer, pafbc.leTueBolletteHomepageText.split(";"), true);
				logger.write("controllando i campi della sezione 'Le tue bollette' - COMPLETED");
			}
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
