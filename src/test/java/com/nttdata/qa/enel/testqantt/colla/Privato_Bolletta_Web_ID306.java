package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivatoBollettaWebID308Component;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Privato_Bolletta_Web_ID306 {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			PrivatoBollettaWebID308Component atti = new PrivatoBollettaWebID308Component(driver);
			
			logger.write("User shoulf be logged in successfully - Start");
			logger.write("Checking if path and heading of homePage is displayed - Start");
			By homePagePath = atti.areaRiservata;
			atti.verifyComponentExistence(homePagePath);
			atti.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			By homepageSubheading = atti.pageHeading;
			atti.verifyComponentExistence(homepageSubheading);
			atti.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			logger.write("User shoulf be logged in successfully - Completed");
			
			logger.write("Click on 'serviziSelect' menu item - Start");
			By servizi = atti.serviziSelectMenu;
			atti.verifyComponentExistence(servizi);
			atti.clickComponent(servizi);
			logger.write("Click on 'serviziSelect' menu item - Completed");
			Thread.sleep(8000);
			
			logger.write("Verify SERVIZI Path - Start");
			By serviziPath = atti.areaRiservata;
			atti.verifyComponentExistence(serviziPath);
			atti.VerifyText(serviziPath, prop.getProperty("SERVIZI_PATH"));
			logger.write("Verify SERVIZI Path - Completed");
			
			logger.write("Verify serviziPageTitle - Start");
			By serviziPageTitle = atti.serviziPageTitle;
			atti.verifyComponentExistence(serviziPageTitle);
			atti.VerifyText(serviziPageTitle, prop.getProperty("SERVIZI_PAGETITLE"));
			logger.write("Verify serviziPageTitle - Completed");
			
			logger.write("Verify serviziPageTitleSubtext - Start");
			By serviziPageTitleSubtext = atti.serviziPageTitleSubtext;
			atti.verifyComponentExistence(serviziPageTitleSubtext);
			atti.VerifyText(serviziPageTitleSubtext, prop.getProperty("SERVIZI_PAGETITLE_SUBTEXT"));
			logger.write("Verify serviziPageTitleSubtext - Completed");
			
			logger.write("Click on Bolletta WEBBOX Tile - Start");
			By Bolletta_Web_box = atti.Bolletta_Web_box;
			atti.verifyComponentExistence(Bolletta_Web_box);
			atti.clickComponent(Bolletta_Web_box);
			logger.write("Click on Bolletta WEBBOX Tile - Completed");
			Thread.sleep(5000);
			
			By Bolletta_Web_PageTitle = atti.Bolletta_Web_PageTitle;
			atti.verifyComponentExistence(Bolletta_Web_PageTitle);
			atti.VerifyText(Bolletta_Web_PageTitle, prop.getProperty("BOLLETTA_WEB_PAGETITLE"));
			
			By Bolletta_Web_PageTitleSubtext = atti.Bolletta_Web_PageTitleSubtext;
			atti.verifyComponentExistence(Bolletta_Web_PageTitleSubtext);
			atti.VerifyText(Bolletta_Web_PageTitleSubtext, prop.getProperty("BOLLETTA_WEB_PAGETITLE_SUBTEXT"));
			
			atti.verifyComponentExistence(atti.supplyActiveStatus);
			atti.VerifyText(atti.supplyActiveStatus, atti.supplyActiveText);
			
			atti.verifyComponentExistence(atti.indrizzoHeading);
			atti.verifyComponentExistence(atti.indrizzoHeadingValue);
			atti.VerifyText(atti.indrizzoHeadingValue, atti.IndrizzoTextValue);
			
			atti.verifyComponentExistence(atti.emailHeading);
			atti.verifyComponentExistence(atti.emailValue);
			atti.VerifyText(atti.emailValue, atti.EmailValue);
			
			atti.verifyComponentExistence(atti.modificaIndrizzoEmailButton);
			atti.verifyComponentExistence(atti.Per_disattivareText);
			atti.VerifyText(atti.Per_disattivareText, atti.per_disattivareText);
			atti.verifyComponentExistence(atti.CLICCA_QUI_Link);
			atti.clickComponent(atti.CLICCA_QUI_Link);
			/*By Attivazione = atti.Attivazione;
			atti.verifyComponentExistence(Attivazione);
			atti.VerifyText(Attivazione, prop.getProperty("ATTIVAZIONE"));
			
			By Modifica = atti.Modifica;
			atti.verifyComponentExistence(Modifica);
			atti.VerifyText(Modifica, prop.getProperty("MODIFICA"));
			
			By Revoca = atti.Revoca;
			atti.verifyComponentExistence(Revoca);
			atti.VerifyText(Revoca, prop.getProperty("REVOCA"));
			
			By Attivazione_scopriDiPiu = atti.Attivazione_scopriDiPiu;
			atti.verifyComponentExistence(Attivazione_scopriDiPiu);
			
			By Modifica_scopriDiPiu = atti.Modifica_scopriDiPiu;
			atti.verifyComponentExistence(Modifica_scopriDiPiu);
			
			By Revoca_scopriDiPiu = atti.Revoca_scopriDiPiu;
			atti.verifyComponentExistence(Revoca_scopriDiPiu);
			
			atti.clickComponent(Revoca_scopriDiPiu);*/
			Thread.sleep(30000);
			
			By disattivazioneBollettaWeb_Title = atti.disattivazioneBollettaWeb_Title;
			atti.verifyComponentExistence(disattivazioneBollettaWeb_Title);
			atti.VerifyText(disattivazioneBollettaWeb_Title, prop.getProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE"));
			
			By disattivazioneBollettaWeb_TitleSubtext = atti.disattivazioneBollettaWeb_TitleSubtext;
			atti.verifyComponentExistence(disattivazioneBollettaWeb_TitleSubtext);
			atti.VerifyText(disattivazioneBollettaWeb_TitleSubtext, prop.getProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE_SUBTEXT"));
			
			atti.validateSupply(atti.numeroClientie_851661167);
			
			By SelezionaButton = atti.SelezionaButton;
			atti.verifyComponentExistence(SelezionaButton);
			atti.clickComponent(SelezionaButton);
			Thread.sleep(8000);
			
			atti.verifyComponentExistence(disattivazioneBollettaWeb_Title);
			atti.VerifyText(disattivazioneBollettaWeb_Title, prop.getProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE"));
			
			By CurrentStep = atti.CurrentStep;
			atti.verifyComponentExistence(CurrentStep);
			atti.VerifyText(CurrentStep, prop.getProperty("CURRENTSTEP1"));
			
			By disattivazioneBollettaWeb_TitleSubtext2 = atti.disattivazioneBollettaWeb_TitleSubtext;
			atti.verifyComponentExistence(disattivazioneBollettaWeb_TitleSubtext2);
			atti.VerifyText(disattivazioneBollettaWeb_TitleSubtext2, atti.DisattivazioneBollettaWeb_TitleSubtext2);
			
			By supplyDeactivateMessage = atti.supplyDeactivateMessage;
			atti.verifyComponentExistence(supplyDeactivateMessage);
			atti.VerifyText(supplyDeactivateMessage, prop.getProperty("SUPPLY_DEACTIVE_MESSAGE"));
			
			atti.confirmSupply(atti.numeroClientie_851661167, atti.Tipo, atti.Numerocliente, atti.IndirizzoForniture);
			
			By supplyDeactivateConfirmingMessage = atti.supplyDeactivateConfirmingMessage;
			atti.verifyComponentExistence(supplyDeactivateConfirmingMessage);
			atti.VerifyText(supplyDeactivateConfirmingMessage, prop.getProperty("SUPPLY_DEACTIVE_CONFIRMING_MESSAGE"));
			
			By Esci = atti.Esci;
			atti.verifyComponentExistence(Esci);
			
			By Indietro = atti.Indietro;
			atti.verifyComponentExistence(Indietro);
			
			By Conferma = atti.Conferma;
			atti.verifyComponentExistence(Conferma);
			
			atti.clickComponent(Indietro);
			
			atti.verifyComponentExistence(disattivazioneBollettaWeb_Title);
			atti.VerifyText(disattivazioneBollettaWeb_Title, prop.getProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE"));
			
			atti.verifyComponentExistence(disattivazioneBollettaWeb_TitleSubtext);
			atti.VerifyText(disattivazioneBollettaWeb_TitleSubtext, prop.getProperty("DISATTIVAZIONE_BOLLETTA_WEB_PAGETITLE_SUBTEXT"));
			
			atti.verifyComponentExistence(SelezionaButton);
			atti.clickComponent(SelezionaButton);
			Thread.sleep(5000);
			
			atti.verifyComponentExistence(Esci);
			atti.clickComponent(Esci);
			
			logger.write("Verify DailogBox Section then click on 'Esci_Button' - Start");
			atti.verifyComponentExistence(atti.dailogTitle);
			atti.VerifyText(atti.dailogTitle, prop.getProperty("DAILOG_TITLE"));
			
			atti.verifyComponentExistence(atti.dailogText);
			atti.VerifyText(atti.dailogText, prop.getProperty("DAILOG_TEXT"));
			
			atti.verifyComponentExistence(atti.Torna_al_processo_Button);
			atti.verifyComponentExistence(atti.Esci_Button);
			
			atti.clickComponent(atti.Esci_Button);
			logger.write("Verify DailogBox Section then click on 'Esci_Button' - Completed");
			Thread.sleep(8000);
			
			logger.write("Checking if path and heading of homePage is displayed - Start");
//			atti.verifyComponentExistence(homePagePath);
//			atti.VerifyText(homePagePath, prop.getProperty("HOMEPAGE_PATH"));
						
			atti.verifyComponentExistence(homepageSubheading);
			atti.VerifyText(homepageSubheading, prop.getProperty("HOMEPAGE_HEADING"));
			logger.write("Checking if heading and sub-heading of homePage is displayed - Completed");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			
			
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}	
			
		

}	