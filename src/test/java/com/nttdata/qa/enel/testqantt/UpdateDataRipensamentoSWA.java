package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;

import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.Step;
import io.qameta.allure.aspects.StepsAspects;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;

public class UpdateDataRipensamentoSWA {

	@Step("UpdateDataRipensamentoSWA")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				page.navigate("https://enelcrmt--uat.cs88.my.salesforce.com/");
				page.enterUsername(Costanti.utenza_admin_salesforce);
				page.enterPassword(Costanti.password_admin_salesforce);
				page.submitLogin();
				////System.out.println("Login ok");
				TimeUnit.SECONDS.sleep(5);
				page.navigate("https://workbench.developerforce.com/query.php");
				WorkbenchComponent a = new WorkbenchComponent(driver);
				a.selezionaEnvironment("Sandbox");
				a.pressButton(a.checkAgree);
				a.pressButton(a.buttonLogin);
				TimeUnit.SECONDS.sleep(2);
				//System.out.println(driver.getCurrentUrl());
				while (!driver.getCurrentUrl().startsWith("https://workbench.developerforce.com/query.php")) {
					page.enterUsername(Costanti.utenza_admin_salesforce);
					page.enterPassword(Costanti.password_admin_salesforce);
					page.submitLogin();
					TimeUnit.SECONDS.sleep(2);
				}
				
				//Calcolo della sysdate-1 da utilizzare nell'update per skippare la data di ripendamento
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				String data = simpleDateFormat.format(calendar.getTime()).toString();
				Logger.getLogger("").log(Level.INFO, "Sysdate:"+data);
				calendar.add(Calendar.DATE,-1);
				simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				data = simpleDateFormat.format(calendar.getTime()).toString();
				prop.setProperty("DATA_RIPENSAMENTO_NEW", data);
				logger.write("Calcolo data ripensamento a sysdate -1");
	
				
				//Gestione per effettuare la stessa update più volte
//				int numeroUpdate=Integer.parseInt(prop.getProperty("NUMERO_UPDATE"));
//				for(int i=1;i<=numeroUpdate;i++){

//					a.insertQuery(prop.getProperty("UPDATE_"+i));
					a.insertQuery(Costanti.aggiorna_data_ripensamento_SWA.replaceAll("@ID_OFFERTA_CRM@",prop.getProperty("ID_OFFERTA")));
					logger.write("Inserimento Query");
					boolean condition = false;
					int tentativi = 2;
					while (!condition && tentativi-- > 0) {
						a.pressButton(a.submitQuery);
						condition = a.aspettaRisultati();
						logger.write("Esecuzione Query");
					}
					if (condition) {
						a.recuperaRisultati(Integer.parseInt("1"), prop);
					} else
						throw new Exception("Impossibile recuperare i risultati dalla query workbench. Sistema lento");
					
					TimeUnit.SECONDS.sleep(5);
					//Effettuo Update
					a.updateRow("ITA_IFM_Reconsideration_EndDate_SWA__c", data);
					logger.write("Update data ripensamento");
					//Torno alla pagina principale se è rischiesto di fare multi update
//					if (numeroUpdate>1) {
//						a.inserisciNuovaQuery();
//					}
//				}
				//Logout
				a.logoutWorkbench();

			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}