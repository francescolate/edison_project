package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class VerificaStatoAttivitaFattoOCdC {
	private final static int SEC = 120;
	
	public static void main(String[] args) throws Exception {
		

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
//			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
//				if(prop.getProperty("ITA_IFM_STATUS__C").compareToIgnoreCase("FATTO")!=0){	
//				if(!prop.getProperty("ITA_IFM_STATUS__C").equals("FATTO")){	
				if(!prop.getProperty("ITA_IFM_STATUS__C").equals("FATTO") && !prop.getProperty("ITA_IFM_STATUS__C").equals("Chiusa da confermare")){	
					throw new Exception("Lo stato dell'attività non corrisponde a quello atteso. Valore attuale:"+prop.getProperty("ITA_IFM_STATUS__C")+" valore atteso:FATTO. Pod riferimento:"+prop.getProperty("POD"));
			}
			logger.write("Verifica stato attività in fatto");

		}

			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
