package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.InfoEnelEnergiaACRComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_268_InfoEnergia {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			InfoEnelEnergiaACRComponent iee = new InfoEnelEnergiaACRComponent(driver);
					
			logger.write("Correct visualization of the Home Page with text- Starts");
			iee.comprareText(iee.homepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.homepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH2, true);
			iee.comprareText(iee.homepageTitle, InfoEnelEnergiaACRComponent.HOMEPAGE_TITLE, true);
			logger.write("Correct visualization of the Home Page with text- Ends");
			
			logger.write("Click on servizi and verify the data- Starts");
			iee.clickComponent(iee.servizi);
			iee.comprareText(iee.serviziHomepagePath1, InfoEnelEnergiaACRComponent.HOMEPAGE_PATH1, true);
			iee.comprareText(iee.serviziHomepagePath2, InfoEnelEnergiaACRComponent.HOMEPAGE_SERVIZI_PATH2, true);
			iee.comprareText(iee.serviziText, InfoEnelEnergiaACRComponent.SERVIZI_TEXT, true);
			logger.write("Click on dettaglio Fornitura and verify the data- Ends");
			
			logger.write(" Click on Info Enel Energia and verify Correct display error page- Starts");
			iee.clickComponent(iee.infoEnelEnergia);
			Thread.sleep(5000);
			iee.clickComponent(iee.proseguiButtonErr);
			Thread.sleep(5000);
			iee.comprareText(iee.errorPageHeadingBSN, InfoEnelEnergiaACRComponent.ERROR_PAGE_HEADING, true);
			logger.write(" Click on Info Enel Energia and verify Correct display error page- Ends");
			
			logger.write("Click on");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
