package com.nttdata.qa.enel.testqantt.r2d;


import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_InvioPraticheComponent;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_InvioPSPredisposizionePresa_1OK_GAS {

	@Step("R2D Invio PS a Portale- Esito 1OK - GAS")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				String statopratica=prop.getProperty("STATO_PRATICA");
				//l'esito RD2 1OK è opzionale e viene dato solo quando lo stato della pratica è AW
				if (statopratica.compareToIgnoreCase("AW")==0){
					//Selezione Menu Interrogazione
					menuBox.selezionaVoceMenuBox("Code di comunicazione","Invio PS a portale");
					//Selezione Voce Report Post Sales
					R2D_InvioPraticheComponent selezionaPratiche = new R2D_InvioPraticheComponent(driver);
					//Selezione distributore
					selezionaPratiche.selezionaDistributore(prop.getProperty("DISTRIBUTORE_R2D_ATTESO_GAS"));
					//Seleziona Tipo Lavorazione Coda
					selezionaPratiche.selezionaTipoLavorazioneCodaPN1(prop.getProperty("TIPO_LAVORAZIONE_CRM_GAS"));
					//Invio Pratiche
					selezionaPratiche.invioPratiche();
				}
				else{
					Logger.getLogger("").log(Level.INFO, "Pratica in stato diverso da AW, non è necessario dare esito 1OK");
				}

			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
