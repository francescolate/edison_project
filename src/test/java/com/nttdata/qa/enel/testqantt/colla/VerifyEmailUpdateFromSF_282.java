package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.lightning.GetAccountFromComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;
public class VerifyEmailUpdateFromSF_282 {

	@Step("Get account from CF")
	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			GetAccountFromComponent gafc = new GetAccountFromComponent(driver);
			
			//prop.setProperty("CF", "CLZGRL86R14D883O");
			//prop.setProperty("CF", "MTTFNC33L21F890Y");
			prop.setProperty("CF", "TMSLGU45B09A766B");
			
			logger.write("typing the user CF - START");	
			gafc.verifyComponentExistence(gafc.searchBar);
			gafc.searchAccount(gafc.searchBar, prop.getProperty("CF"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("checking the data existence - START");	
			Thread.sleep(2000);
			gafc.verifyComponentExistence(gafc.clientiaccountLink);
			gafc.clickComponent(gafc.clientiaccountLink);
			Thread.sleep(2000);
			gafc.verifyComponentExistence(gafc.dettagliLink);
			Thread.sleep(2000);
			//gafc.clickComponent(gafc.dettagliLink);
			Thread.sleep(2000);
			//gafc.verifyComponentExistence(gafc.dettagliEmailClienteLink);
			logger.write("checking the data existence - COMPLETE");
		
		if(prop.getProperty("EMAIL") != null && !prop.getProperty("EMAIL").equals("")){
			logger.write("checking account email - START");	
			//gafc.verifyComponentExistence(By.xpath(gafc.accountEmailNew.replace("$email$", prop.getProperty("EMAIL"))));
			logger.write("checking account email - COMPLETE");
		}
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;	
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}

