package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
/**
 * dopo il primo messaggio d'errore il test chiede di inserire un IBAN estero senza spuntare il flag IBAN ESTERO - ci si aspetta
 * il messaggio d'errore "Iban Italiano non valido, se trattasi di Iban Estero, selezionare il flag “Iban Estero (Area SEPA)"
 * @author RadescaFe
 *
 */
public class ReSetIban_AttivazioneSDD11 {
	
	public static void main(String[] args) throws Exception {
		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		String query="";
		try {

			prop.setProperty("ATT_SDD_CHECK_OBBLIGATORIETA_CAMPI", "N");
			prop.setProperty("ATT_SDD_IBAN", "FR9114508000307445982348I06");
			prop.setProperty("ATT_SDD_IBAN_ESTERO", "N");
			prop.setProperty("ATT_SSD_MSG_VALIDAZIONE_IBAN", "Iban Italiano non valido, se trattasi di Iban Estero, selezionare il flag “Iban Estero (Area SEPA).");
			prop.setProperty("RETURN_VALUE", "OK");



		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y")) throw e;

		} finally {
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
	
	
}
