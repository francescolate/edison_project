package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneClienteUscenteComponent;
import com.nttdata.qa.enel.components.lightning.SezioneSelezioneFornituraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CheckSezioneSelezioneFornituraRicontrattualizzazione {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

				RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
				SeleniumUtilities util = new SeleniumUtilities(driver);
				SezioneSelezioneFornituraComponent forn=new SezioneSelezioneFornituraComponent(driver);
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);

				logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Start");
                By sezione_fornitura=forn.sezioneSelezioneFornitura;
                forn.verifyComponentExistence(sezione_fornitura);
                By campo_CercaFornitura=forn.campoCercaFornitura;
                forn.verifyComponentExistence(campo_CercaFornitura);
                forn.scrollComponent(forn.pagina);
                TimeUnit.SECONDS.sleep(1);
                logger.write("check esistenza sezione 'Selezione Fornitura' e campo 'Cerca Fornitura' - Completed");

				logger.write("inserire nel campo 'Cerca Fornitura' il POD - Start");
				TimeUnit.SECONDS.sleep(1);
				forn.enterPodValue(campo_CercaFornitura, prop.getProperty("POD"));
				TimeUnit.SECONDS.sleep(3);
				logger.write("inserire nel campo 'Cerca Fornitura' il POD - Completed");
				
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: COMMODITY,STATO,NUMERO UTENZA,POD,INDIRIZZO DI FORNITURA,USO FORNITURA,MERCATO DI RIFERIMENTO,PRODOTTO,ATTIVABILITÀ,OFFERTABILITÀ e selezionare la fornitura di interesse - Start");
				forn.verificaColonneTabella(forn.colonneRic);
				forn.scrollComponent(forn.pagina);
				forn.selezionaRecordInTabella(prop.getProperty("POD"));
				logger.write("verifica che nella tabella forniture sono riportate le seguenti informazioni: COMMODITY,STATO,NUMERO UTENZA,POD,INDIRIZZO DI FORNITURA,USO FORNITURA,MERCATO DI RIFERIMENTO,PRODOTTO,ATTIVABILITÀ,OFFERTABILITÀ e selezionare la fornitura di interesse - Completed");
				
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna ATTIVABILITA' sia presente il valore 'Non Attivabile' - Start");
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(5);
				forn.checkValoreInTable(prop.getProperty("ATTIVABILITA"));
				logger.write("verifica che nella tabella forniture in corrispondenza della colonna ATTIVABILITA' sia presente il valore 'Non Attivabile' - Completed");


				if (prop.getProperty("CONFERMA_SELEZIONE_FORNITURA","").contentEquals("Y")) {

					logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Start");
					forn.clickComponentIfExist(forn.buttonConfermaSelezioneFornituraAbilitato);
					logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia abilitato e click su questo - Completed");

					logger.write("click sul pulsante 'Conferma' - Start");
					forn.clickComponentIfExist(forn.buttonConferma);
					logger.write("click sul pulsante 'Conferma' - Completed");

					logger.write("click sul pulsante 'Crea offerta' - Start");
					forn.clickComponentIfExist(forn.buttonCreaOfferta);
					gestione.checkSpinnersSFDC();
					TimeUnit.SECONDS.sleep(2);
					logger.write("se compare la popup 'Proposta Voltura Dual' click su Si, poi su 'Conferma Selezione Fornitura', 'Conferma' e 'Crea Offerta' - Start");
					forn.clickPopupIfExist(forn.popupSi);
					logger.write("se compare la popup 'Proposta Voltura Dual' click su Si, poi su 'Conferma Selezione Fornitura', 'Conferma' e 'Crea Offerta' - Completed");
					gestione.checkSpinnersSFDC();
					TimeUnit.SECONDS.sleep(3);
					logger.write("click sul pulsante 'Crea offerta' - Completed");
				}
				
				else {
					logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia disabilitato - Start");
					forn.verifyComponentExistence(forn.buttonConfermaSelezioneFornitura);
					logger.write("verifica che il pulsante 'Conferma Selezione Fornitura' sia disabilitato - Completed");
					}
				
				
				prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
