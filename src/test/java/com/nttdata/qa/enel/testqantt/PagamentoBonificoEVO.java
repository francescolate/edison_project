
package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class PagamentoBonificoEVO {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				SceltaMetodoPagamentoComponentEVO sceltaMetodoPagamentoComponentEVO = new SceltaMetodoPagamentoComponentEVO(
						driver);
				By bonifico;
				By buttonAvanti;

				// Attribuzione path per Bonifico

				
					String tipoOperazione = prop.getProperty("TIPO_OPERAZIONE");

					if (tipoOperazione.equals("SWITCH_ATTIVO_EVO")) {
						bonifico = sceltaMetodoPagamentoComponentEVO.bonifico2;
					} else if (tipoOperazione.equals("PRIMA_ATTIVAZIONE_EVO")) {
						bonifico = sceltaMetodoPagamentoComponentEVO.bonifico2;
					} else if (tipoOperazione.equals("ALLACCIO_EVO")) {
						bonifico = sceltaMetodoPagamentoComponentEVO.bonifico2;
					} else if (tipoOperazione.equals("SUBENTRO_EVO")) {
						bonifico = sceltaMetodoPagamentoComponentEVO.bonifico2;
					} else {
						throw new Exception(tipoOperazione + " non gestita");
					}

					if (tipoOperazione.contentEquals("SWITCH_ATTIVO_EVO")) {
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonConferma2;
					} else if (tipoOperazione.equals("PRIMA_ATTIVAZIONE_EVO")) {
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonConferma2;
					} else if (tipoOperazione.equals("ALLACCIO_EVO")) {
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonConferma2;
					} else if (tipoOperazione.equals("SUBENTRO_EVO")) {
						buttonAvanti = sceltaMetodoPagamentoComponentEVO.buttonConferma2;
					} else {
						throw new Exception(tipoOperazione + " non gestita");
					}

					// Selezione metodo di pagamento
					if (tipoOperazione.equals("SWITCH_ATTIVO_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBonifico(bonifico, "checked", "checked",
								buttonAvanti, true);
					} else if (tipoOperazione.equals("PRIMA_ATTIVAZIONE_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBonifico(bonifico, "checked", "checked",
								buttonAvanti, true);
					} else if (tipoOperazione.equals("ALLACCIO_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBonifico(bonifico, "checked", "checked",
								buttonAvanti, true);
					} else if (tipoOperazione.equals("SUBENTRO_EVO")) {
						sceltaMetodoPagamentoComponentEVO.selezionaBonifico(bonifico, "checked", "checked",
								buttonAvanti, true);
					} else  {
						throw new Exception(tipoOperazione + " non gestita");
					}

				
				logger.write("Selezione metodo di pagamento");
			}
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
