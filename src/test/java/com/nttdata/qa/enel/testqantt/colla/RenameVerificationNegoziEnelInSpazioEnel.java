package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.RenameVerificationNegoziEnelInSpazioEnelComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RenameVerificationNegoziEnelInSpazioEnel {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			logger.write("Apertura portale colla web - Start");
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			RenameVerificationNegoziEnelInSpazioEnelComponent log = new RenameVerificationNegoziEnelInSpazioEnelComponent(driver);
			log.launchLink(prop.getProperty("LINK"));
			logger.write("Apertura portale colla web - Completed");
			By logo = log.logoEnel;
			log.verifyComponentExistence(logo);// verifica esistenza logo enel

			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
			
			By menu = log.menu;
			log.verifyComponentExistence(menu);
			log.clickComponent(menu);
			
			By trovaLinkSpazioEnel=log.linkSpazioEnel;
			log.verifyComponentExistence(trovaLinkSpazioEnel);
			log.clickComponent(trovaLinkSpazioEnel);  
			
			By linkAggiornato=log.urlSpazioEnel;
			log.verifyComponentExistence(linkAggiornato);
			
			By apertPageSpazioEnel=log.spazioEnelTitle;
			log.verifyComponentExistence(apertPageSpazioEnel);
			log.scrollComponent(log.nellaTuaCittaLabel);
			
			By switchFrame=log.frame;
			log.setframe(switchFrame);
			
			By labelSpazioEnel=log.spazioEnel;
			log.verifyComponentExistence(labelSpazioEnel);
			
			By labelSpazioEnelPartner=log.spazioEnelPartner;
			log.verifyComponentExistence(labelSpazioEnelPartner);
			
			
			//By switchFrame2=log.framesetUbiest;
			log.checkListUbiest(prop.getProperty("DESCRIPTION_UBIEST1"), prop.getProperty("DESCRIPTION_UBIEST2"));
			
		    log.selezionaProvincia(prop.getProperty("PROVINCIA"));
		    
		    By checkObject=log.checkVaiAlloSpazioEnel;
		    log.checkLabel(checkObject,prop.getProperty("CHECK_LABEL"),prop.getProperty("PROVINCIA"));
			
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
