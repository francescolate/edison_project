package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LoginPageComponent;
import com.nttdata.qa.enel.components.colla.ModConsensiACR162Component;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Login_Private_Area {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		By accessLoginPage = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			String loginPageURL = prop.getProperty("WP_LINK")+"it/login";
			logger.write("apertura del portale web Enel di test - Start");
			//Login Page
			LoginPageComponent log1 = new LoginPageComponent(driver);
			
			logger.write("apertura del portale web Enel di test - Start");
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start"); 
			By logo = log.logoEnel;
//			log.verifyComponentExistence(logo);// verifica esistenza logo enel
			logger.write("check sulla presenza del logo Enel - Completed");
			Thread.sleep(5000);
			
			By accettaCookie = log.buttonAccetta;
			log.verifyComponentExistence(accettaCookie);
			log.clickComponent(accettaCookie);
						
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			logger.write("Check login page - completed");
			
			logger.write("Check the user name and password fields - start");
			By user = log.username;
			log.verifyComponentExistence(user);
			By pw = log.password;
			log.verifyComponentExistence(pw);
			logger.write("Check the user name and password fields - completed");
			
			logger.write("Check the Login button - start");
			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			logger.write("Check the Login button - completed");

			By recName = log1.RecUserName;
	        log.verifyComponentExistence(recName);
	        By RecPasswd=log1.RecUserPasswd;
	        log.verifyComponentExistence(RecPasswd);
	        
//	        log.verifyComponentExistence(log1.haiProblemiText);
//	        log1.comprareText(log1.haiProblemiText, LoginPageComponent.HAI_PROBLEMI_TEXT, true);
	    
	        logger.write("Check the google and facebook  - start");
	        By signInFacebook=log1.FacebookSignIn;
	        log.verifyComponentExistence(signInFacebook);
	        By signInGoogle=log1.GoogleSignIn;
	        log.verifyComponentExistence(signInGoogle);
	        logger.write("Check the google and facebook - completed");
	        
	        log.verifyComponentExistence(log1.nonHaiAccount);
	        log1.comprareText(log1.nonHaiAccount, LoginPageComponent.NON_HAI_ACCOUNT, true);
	        
	        logger.write("Check the registrati button  - start");
	        log.verifyComponentExistence(log1.registratiButton);
	        logger.write("Check the registrati button  - ends");
	        
	        log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));
			log.clickComponent(accedi); 
			
			
			try{
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Start");
				log.verifyComponentExistence(log.areaClientiCondizioni);
				log.clickComponent(log.areaClientiCondizioni);
				log.verifyComponentExistence(log.areaClientiPrivacy);
				log.clickComponent(log.areaClientiPrivacy);
				log.verifyComponentExistence(log.terminiECondizioniButton);
				log.clickComponent(log.terminiECondizioniButton);
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Completed");
			}catch(Exception e){
				logger.write("check sull'esistenza degli elementi nella pagina Termini e Condizioni - Pagina inesistente. Proseguo.");
			}
			
			try{
				if (!prop.getProperty("AREA_CLIENTI").equals(""))
				{
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - Start");
					if(prop.getProperty("AREA_CLIENTI").equals("CASA")){
						log.verifyComponentExistence(log.areaClientiCasa);
						log.clickComponent(log.areaClientiCasa);
					}
					else
					{
						log.verifyComponentExistence(log.areaClientiImpresa);
						log.clickComponent(log.areaClientiImpresa);
					}
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - COMPLETED");
				} else {
					logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
				}
			}catch(Exception e){
				logger.write("check sull'esistenza della pagina di scelta tra CASA e IMPRESA - PAGINA INESISTENTE. PROSEGUO.");
			}
			
			if(!prop.getProperty("ACCOUNT_TYPE").equals(""))
				if(prop.getProperty("ACCOUNT_TYPE").contains("BSN"))
					if(!prop.getProperty("AREA_CLIENTI").equals(""))
						if(prop.getProperty("AREA_CLIENTI").equals("CASA"))
							accessLoginPage=log.loginSuccessful;
						else
							accessLoginPage=log.loginBSNSuccessful;
					else
						accessLoginPage=log.loginBSNSuccessful;
				else
					accessLoginPage=log.loginSuccessful;
			else
				accessLoginPage=log.loginSuccessful;
			log.verifyComponentExistence(accessLoginPage);
			
//			if(prop.getProperty("ACCOUNT_TYPE").contains("LIKE_RES") || prop.getProperty("ACCOUNT_TYPE").contains("LIKE_BSN"))
//			{
//				boolean shouldVerifySelfCareSelectionPage = Boolean.parseBoolean(prop.getProperty("VERIFY_SC_SELECTION_PAGE", "false"));
//				if (!shouldVerifySelfCareSelectionPage) {
//					accessLoginPage=log.loginSuccessful;
//					log.verifyComponentExistence(accessLoginPage); 
//				}
//			}
			
			logger.write("check sull'esistenza della pagina di inserimento credenziali e accesso all'area privata - Completed");
		
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
