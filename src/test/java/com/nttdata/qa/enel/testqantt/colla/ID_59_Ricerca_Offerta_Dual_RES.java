package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.LuceEGasComponent;
import com.nttdata.qa.enel.components.colla.PublicAreaComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_59_Ricerca_Offerta_Dual_RES {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);
			logger.write("apertura del portale web Enel di test - Start");
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("WP_LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("controllando l'esistenza del messaggio pubblicitario - Start");
			log.hanldeFullscreenMessage(log.homeFullscreenAlertCloseButton);
			logger.write("controllando l'esistenza del messaggio pubblicitario - Completed");
		
			log.clickComponent(log.buttonAccetta);
			PublicAreaComponent pc = new PublicAreaComponent(driver);
			logger.write("select drop down value - Start");
			Thread.sleep(30000);
			pc.changeDropDownValue(pc.productDropDown, pc.luceEgas);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, pc.primaAttivazione);
			logger.write("select drop down value - Completed");
			
			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			Thread.sleep(30000);
			LuceEGasComponent lc = new LuceEGasComponent(driver);
			pc.comprareText(lc.luceegasInsiemeText, lc.LUCEEGASINSIEMEText, true);
			pc.comprareText(lc.luceEGasInsiemeSubText, lc.LUCEEGASINSIEMESubText, true);
			/*pc.comprareText(lc.trovaLaSoluzioneText, lc.TrovaLaSoluzioneText, true);
			pc.comprareText(lc.trovaLaSoluzioneSubText, lc.TrovaLaSoluzioneSubText, true);
			pc.comprareText(lc.scopriilNegozioLink, lc.ScopriilNegozioLinkText, true);
			lc.clickComponent(lc.scopriilNegozioLink);
			driver.navigate().back();

			pc.comprareText(lc.luceegasInsiemeText, lc.LUCEEGASINSIEMEText, true);
			pc.comprareText(lc.trovaLaSoluzioneText, lc.TrovaLaSoluzioneText, true);
			pc.comprareText(lc.trovaLaSoluzioneSubText, lc.TrovaLaSoluzioneSubText, true);
			pc.comprareText(lc.scopriilNegozioLink, lc.ScopriilNegozioLinkText, true);*/
			
			logger.write("select drop down value - Start");
			//driver.navigate().refresh();
			Thread.sleep(10000);
			pc.changeDropDownValue(pc.productDropDown, pc.luceEgas);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, lc.subentro);
			logger.write("select drop down value - Completed");
			
			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			
			Thread.sleep(10000);
			pc.comprareText(lc.luceegasInsiemeText, lc.LUCEEGASINSIEMEText, true);
			pc.comprareText(lc.luceEGasInsiemeSubText, lc.LUCEEGASINSIEMESubText, true);
			pc.comprareText(lc.luceegasInsiemeText, lc.LUCEEGASINSIEMEText, true);
			/*pc.comprareText(lc.trovaLaSoluzioneText, lc.TrovaLaSoluzioneText, true);
			pc.comprareText(lc.trovaLaSoluzioneSubText, lc.TrovaLaSoluzioneSubText, true);
			pc.comprareText(lc.scopriilNegozioLink, lc.ScopriilNegozioLinkText, true);*/
			
			logger.write("select drop down value - Start");
			pc.changeDropDownValue(pc.productDropDown, pc.luceEgas);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, lc.voltura);
			logger.write("select drop down value - Completed");
			
			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			
			Thread.sleep(10000);
			pc.comprareText(lc.luceegasInsiemeText, lc.LUCEEGASINSIEMEText, true);
			pc.comprareText(lc.luceEGasInsiemeSubText, lc.LUCEEGASINSIEMESubText, true);
			pc.comprareText(lc.luceegasInsiemeText, lc.LUCEEGASINSIEMEText, true);
			/*pc.comprareText(lc.trovaLaSoluzioneText, lc.TrovaLaSoluzioneText, true);
			pc.comprareText(lc.trovaLaSoluzioneSubText, lc.TrovaLaSoluzioneSubText, true);
			pc.comprareText(lc.scopriilNegozioLink, lc.ScopriilNegozioLinkText, true);
			
			*/logger.write("select drop down value - Start");
			pc.changeDropDownValue(pc.productDropDown, pc.luceEgas);
			pc.changeDropDownValue(pc.placeDropDown, pc.casa);
			pc.changeDropDownValue(pc.myselectionDD, lc.cambioFornitore);
			logger.write("select drop down value - Completed");
			
			logger.write("Click on inizia ora - Start");
			pc.clickComponent(pc.iniziaOra);
			logger.write("Click on inizia ora - Completed");
			
			Thread.sleep(15000);
			pc.comprareText(lc.luceEGasInsiemeTitle, lc.LUCEEGASINSIEMEText, true);
			lc.verifyComponentExistence(lc.luceEGasInsiemeText);
			pc.comprareText(lc.luceEGasInsiemeSubText, lc.LUCEEGASINSIEMESubText, true);
			lc.verifyComponentExistence(lc.vediTutteLuce);
			lc.verifyComponentExistence(lc.vediTutteGas);
			logger.write("Click on vedi tutte luce - Start");
			lc.clickComponent(lc.vediTutteLuce);
			logger.write("Click on vedi tutte luce - Completed");

			Thread.sleep(10000);
			lc.verifyComponentExistence(lc.luce);
			lc.verifyComponentExistence(lc.casa);
			lc.verifyComponentExistence(lc.visualizzatutte);
			
			driver.navigate().back();
			
			Thread.sleep(10000);
			pc.comprareText(lc.luceEGasInsiemeTitle, lc.LUCEEGASINSIEMEText, true);
			lc.verifyComponentExistence(lc.luceEGasInsiemeText);
			pc.comprareText(lc.luceEGasInsiemeSubText, lc.LUCEEGASINSIEMESubText, true);
			lc.verifyComponentExistence(lc.vediTutteLuce);
			lc.verifyComponentExistence(lc.vediTutteGas);
			logger.write("Click on vedi tutte gas - Start");
			lc.clickComponent(lc.vediTutteGas);
			logger.write("Click on vedi tutte gas - Completed");

			Thread.sleep(10000);
			lc.verifyComponentExistence(lc.gas);
			lc.verifyComponentExistence(lc.casa);
			lc.verifyComponentExistence(lc.visualizzatutte);
			
			driver.navigate().back();
			
			Thread.sleep(15000);
			pc.comprareText(lc.luceEGasInsiemeTitle, lc.LUCEEGASINSIEMEText, true);
			lc.verifyComponentExistence(lc.luceEGasInsiemeText);
			pc.comprareText(lc.luceEGasInsiemeSubText, lc.LUCEEGASINSIEMESubText, true);
			lc.verifyComponentExistence(lc.vediTutteLuce);
			lc.verifyComponentExistence(lc.vediTutteGas);
			
//			logger.write("Click on attiva ora E Light Bioraria + E Light Gas - Start");
//			lc.clickComponent(lc.attivaOraELightBioraria);
//			logger.write("Click on attiva ora E Light Bioraria + E Light Gas - Completed");
			
			logger.write("Click on E Light Bioraria + E Light Gas - Start");
			lc.clickComponent(lc.eLightBioraria_eLightGas);
			lc.clickComponent(lc.attivaOra);
			logger.write("Click on E Light Bioraria + E Light Gas - Completed");

		//	Thread.sleep(5000);
//			lc.verifyComponentExistence(lc.biorariaWeb);
//			lc.verifyComponentExistence(lc.gasWeb);

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
