package com.nttdata.qa.enel.testqantt;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CercaClienteComponentEVO;
import com.nttdata.qa.enel.components.lightning.CreaNuovaInterazioneComponentEVO;
import com.nttdata.qa.enel.components.lightning.SelezionaClienteAttivoComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class  CercaClientePerNuovaInterazionePerReferenteEVO  {


	@Step("Cerca Cliente Per Nuova Iterazione")
	public static void main (String[] args) throws Exception
	{

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {


			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			if(prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				CreaNuovaInterazioneComponentEVO startIterationPage = new CreaNuovaInterazioneComponentEVO(driver);
				Thread.sleep(5000);
				startIterationPage.createNewiteration();
				logger.write("Creazione nuova Interazione");
				CercaClienteComponentEVO searchClientPage =  new CercaClienteComponentEVO(driver);
				searchClientPage.fillCfAndSearch2(prop.getProperty("CODICE_FISCALE"));
				logger.write("GetProp: CODICE FISCALE");

				SelezionaClienteAttivoComponentEVO selectCliente = new SelezionaClienteAttivoComponentEVO(driver);

				if(prop.getProperty("SCELTA_TIPO_CLIENTE","N").equals("Y")) {
					selectCliente.fillCfAndChooseClientType(prop.getProperty("TIPO_CLIENTE"),prop.getProperty("COMPLETE_NAME"));
				}
				else {
					try {
						Thread.sleep(10000);
						selectCliente.fillCfAndSearch3();
					}catch (Exception e) {
						// TODO: handle exception
					}
				}

				if (prop.getProperty("CODICE_FISCALE_REFERENTE","").equals("")){
					selectCliente.setInterlocutoreConReferente(0,prop.getProperty("CODICE_FISCALE_REFERENTE"));
				}
				else {
					selectCliente.setInterlocutore(0);
				}

			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());
			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
