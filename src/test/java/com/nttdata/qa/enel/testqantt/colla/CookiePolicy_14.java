package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.nttdata.qa.enel.components.colla.BolleteBSNComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CookiePolicy_14 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			logger.write("apertura del portale web Enel di test - Start");
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			log.launchLink(prop.getProperty("LINK"));
			logger.write("apertura del portale web Enel di test - Completed");
			
			logger.write("check sulla presenza del logo Enel - Start");
			log.verifyComponentExistence(log.logoEnel);
			logger.write("check sulla presenza del logo Enel - Completed");
			
			log.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));
			
			log.verifyComponentText(log.trusteConsentText, log.textCookie);
			log.verifyComponentExistence(log.buttonAccetta);
			log.verifyComponentExistence(log.buttonImpostazioni);
			log.clickComponent(log.trusteCookieButton);
			Thread.sleep(2000);
			Set <String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			for (String winHandle : windows) {
				   // Switch to child window
					System.out.println("switched to "+driver.getTitle()+"  Window");
			        String pagetitle = driver.getTitle();
				    driver.switchTo().window(winHandle);
				 }
			log.checkUrl2("https://www.enel.it/it/supporto/faq/cookie-policy");
			
			log.verifyComponentText(log.trusteConsentText, log.textCookie);
			log.verifyComponentExistence(log.buttonAccetta);
			log.verifyComponentExistence(log.buttonImpostazioni);
			log.clickComponent(log.buttonAccetta);
			

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
