package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByAll;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.ModificaOpzioniBollettaDiSintesiComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaOpzioniBollettaDiSintesi {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModificaOpzioniBollettaDiSintesiComponent mocc = new ModificaOpzioniBollettaDiSintesiComponent(driver);
			
			//STEP 4
			//Click servizi
			
			By button_servizi = mocc.button_servizi;
			mocc.verifyComponentExistence(button_servizi);
			mocc.clickComponentWithJse(button_servizi);
		
//			//STEP 5
//			//Modifica tipologia bolletta
			TimeUnit.SECONDS.sleep(10);
			By opzioni_bolletta_card = mocc.opzioni_bolletta_card;
			mocc.verifyComponentExistence(opzioni_bolletta_card);
			mocc.clickComponent(opzioni_bolletta_card);
			
			//Corretta apertura della pagina
//			By text_path = mocc.text_path;
			
//			mocc.verifyComponentText(text_path, "AREA RISERVATA FORNITURE MODIFICA TIPOLOGIA BOLLETTA");
			
			By text_titolo = mocc.text_titolo;
			mocc.verifyComponentText(text_titolo, "Opzioni Bolletta");
			
			By text_lead = mocc.text_lead;			
			mocc.verifyComponentText(text_lead, "Con Opzioni Bolletta puoi scegliere di ricevere un bolletta sintetica o dettagliata.");
			

			By tipo_boll_title = mocc.tipo_boll_title;
			mocc.verifyComponentText(tipo_boll_title, "Tipologia bolletta");
			
			Thread.sleep(10000);
			
			//By tipo_boll = mocc.tipo_boll;
			//mocc.verifyComponentExistence(tipo_boll);
			//mocc.verifyComponentText(tipo_boll, "Dettagliata");
			
			By desc = mocc.desc;
			mocc.verifyComponentExistence(desc);
					
			//STEP 6
			//Click sul button Modifica
			
			By button_modifica = mocc.button_modifica;
			mocc.verifyComponentExistence(button_modifica);
			mocc.clickComponent(button_modifica);
			
			//Corretta apertura della pagina
			
		//	mocc.verifyComponentExistence(text_path);
		//	mocc.verifyComponentText(text_path, "AREA RISERVATAFORNITUREMODIFICA TIPOLOGIA BOLLETTA");
			
		//	mocc.verifyComponentText(text_titolo, "Modifica Tipologia Bolletta");
					
		//	mocc.verifyComponentText(text_lead, "Vuoi modificare la tipologia di bolletta anche per altre forniture?");
			
			By step_text_0 = mocc.step_text_0;
			By step_num_0 = mocc.step_num_0;
			By step_text_1 = mocc.step_text_1;
			By step_num_1 = mocc.step_num_1;
			By step_text_2 = mocc.step_text_2;
			By step_num_2 = mocc.step_num_2;
			By step_text_3 = mocc.step_text_3;
			By step_num_3 = mocc.step_num_3;
		/*	
			mocc.verifyComponentText(step_text_0, "Multifornitura");
			mocc.verifyComponentText(step_num_0, "1");
			mocc.verifyComponentText(step_text_1, "Scelta tipologia bolletta");
			mocc.verifyComponentText(step_num_1, "2");
			mocc.verifyComponentText(step_text_2, "Riepilogo e conferma dati");
			mocc.verifyComponentText(step_num_2, "3");
			mocc.verifyComponentText(step_text_3, "Esito");
			mocc.verifyComponentText(step_num_3, "4");
			
			By search_box = mocc.search_box;
			mocc.verifyComponentExistence(search_box);
			
			By table_responsive = mocc.search_box;
			mocc.verifyComponentExistence(table_responsive);
			
			By tabele_first_row = mocc.tabele_first_row;
			mocc.verifyComponentExistence(tabele_first_row);
			
			By table_first_row_3th_cell = mocc.table_first_row_3th_cell;
			mocc.verifyComponentText(table_first_row_3th_cell, "Tipo");
			By table_first_row_4th_cell = mocc.table_first_row_4th_cell;
			mocc.verifyComponentText(table_first_row_4th_cell, "Numero cliente / Alias");
			By table_first_row_5th_cell = mocc.table_first_row_5th_cell;
			mocc.verifyComponentText(table_first_row_5th_cell, "Indirizzo fornitura");
			
			By button_indietro = mocc.button_indietro;
			By button_prosegui = mocc.button_prosegui;
			mocc.verifyComponentExistence(button_indietro);
			mocc.verifyComponentExistence(button_prosegui);
		*/	
//			//Click homepage button
//			By button_homepage = mocc.button_homepage;
//			mocc.verifyComponentExistence(button_homepage);
//			mocc.clickComponentWithJse(button_homepage);
//			
//			//Check popup
//			
//			By popup_title = mocc.popup_title;
//			By popup_desc = mocc.popup_desc;
//			
//			mocc.verifyComponentText(popup_title, "Attenzione");
//			mocc.verifyComponentText(popup_desc, "Se uscirai dal processo perderai le modifiche effettuate.");
//			
//			By button_popup_torna = mocc.button_popup_torna;
//			mocc.verifyComponentExistence(button_popup_torna);
//			
//			//Click esci
//			By button_popup_esci = mocc.button_popup_esci;
//			mocc.verifyComponentExistence(button_popup_esci);
//			mocc.clickComponentWithJse(button_popup_esci);	
			prop.setProperty("RETURN_VALUE", "OK");

		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
