package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.CreateEmailAddressesListComponent;
import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class GetUserForRegistration_38 {
	@Step("Recupero Pod da Workbench")
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			int counter = 1, limit = 50;
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			WorkbenchComponent wbc = new WorkbenchComponent(driver);
			LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
			CreateEmailAddressesListComponent cealc = new CreateEmailAddressesListComponent(driver);
			wbc.launchLink(Costanti.workbenchLink);
			wbc.selezionaEnvironment("Sandbox");
			logger.write("Seleziona Environment Sandbox");
			wbc.pressButton(wbc.checkAgree);
			logger.write("Click button Agree");
			wbc.pressButton(wbc.buttonLogin);
			logger.write("Click button Login");
			page.enterUsername(Costanti.utenza_admin_salesforce);
			page.enterPassword(Costanti.password_admin_salesforce);
			page.submitLogin();

			if(prop.getProperty("TIPOCLIENTE").equals("RESIDENZIALE")){
				wbc.insertQuery(Costanti.get_data_for_registration_res);
				wbc.pressButton(wbc.submitQuery);
				wbc.aspettaRisultati(60);
				List<String> res = null;
				while(counter<=limit){
					Thread.currentThread().sleep(100);
					res = wbc.recuperaRisultatiLista(counter);
					String cf = res.get(3);
					if(!APIService.accountExists("cf", cf)){
						String email = cealc.getEmailAddress();
						prop.setProperty("WP_USERNAME", email);
						prop.setProperty("CF", res.get(3));
						prop.setProperty("SUPPLY_NUMBER", res.get(0));
						prop.setProperty("ACCOUNT_FIRSTNAME", res.get(4));
						prop.setProperty("ACCOUNT_LASTNAME", res.get(5));
						prop.store(new FileOutputStream(args[0]), null);
						//System.out.println(res.get(0));
						counter=limit+1;
					}else
						counter++;
				}
			}else{
				wbc.insertQuery(Costanti.get_data_for_registration_bsn_38);
				wbc.pressButton(wbc.submitQuery);
				wbc.aspettaRisultati(60);
				List<String> res = null;
				while(counter<=limit){
					Thread.currentThread().sleep(100);
					res = wbc.recuperaRisultatiLista(counter);
					if(!APIService.accountExists("personalId", res.get(3))){
						String email = cealc.getEmailAddress();
						while(APIService.accountExists("email", email))
							email = cealc.getEmailAddress();
						prop.setProperty("WP_USERNAME", email);
						System.out.println("email"+prop.getProperty("WP_USERNAME"));
						prop.setProperty("SUPPLY_NUMBER", res.get(0));
						System.out.println("SUPPLY_NUMBER"+prop.getProperty("SUPPLY_NUMBER"));
						prop.setProperty("ACCOUNT_ID", res.get(1));
						System.out.println("AccountId"+prop.getProperty("ACCOUNT_ID"));
						prop.setProperty("CF", res.get(3));
						System.out.println("cf"+prop.getProperty("CF"));
						prop.setProperty("PARTITA_IVA", res.get(4));
						prop.setProperty("DENOMINAZIONE", res.get(5));
						prop.setProperty("ACCOUNT_FIRSTNAME", res.get(6));
						System.out.println("ACCOUNT_FIRSTNAME"+prop.getProperty("ACCOUNT_FIRSTNAME"));
						prop.setProperty("ACCOUNT_LASTNAME", res.get(7));
						System.out.println("ACCOUNT_LASTNAME"+prop.getProperty("ACCOUNT_LASTNAME"));
						counter=limit+1;
					}
				
			}
			}
			
			wbc.logoutWorkbench("https://workbench.developerforce.com/logout.php");
			driver.close();
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
