package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.OffertaComponent;
import com.nttdata.qa.enel.components.colla.OffertaComponent110;
import com.nttdata.qa.enel.components.colla.PubblicoID61ProcessoAResSwaEleComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_110_RES_SWA_ELE_ERR {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			OffertaComponent oc =new OffertaComponent(driver);
			OffertaComponent110 oc1 = new OffertaComponent110(driver);
			logger.write("Verify page navigation to Offerta page and page details -- Strat");
			driver.navigate().to(oc.offertaUrl);
			
			oc.verifyComponentExistence(oc.aderisci);
			oc.checkURLAfterRedirection(oc.offertaUrl);
			oc.verifyComponentExistence(oc.pageTitle);
			oc.comprareText(oc.pageText, oc.PageText, true);
			oc.verifyComponentExistence(oc.inserisciITuoiDati);
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.verifyComponentExistence(oc.pagamentiEBollette);
			oc.verifyComponentExistence(oc.consensi);
			oc.verifyComponentExistence(oc.caricaBolletta);
			oc.verifyComponentExistence(oc.compilaManualmente);
			logger.write("Verify page navigation to Offerta page and page details -- Completed");

			oc.clickComponent(oc.compilaManualmente);
			
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Strat");
			oc.enterInputParameters(oc.nome, prop.getProperty("NOME"));
			oc.enterInputParameters(oc.cognome, prop.getProperty("COGNOME"));
			oc.enterInputParameters(oc.cf, prop.getProperty("CF"));
			Thread.sleep(5000);
			oc.enterInputParameters(oc.cellulare, prop.getProperty("CELLULARE"));
			oc.enterInputParameters(oc.email, prop.getProperty("EMAIL"));
			oc.enterInputParameters(oc.emailConferma, prop.getProperty("EMAIL"));
			Thread.sleep(5000);
			oc.clickComponent(oc.checkBox);		
			oc.clickComponent(oc.prosegui1);
			Thread.sleep(25000);
			logger.write("Enter the values in inserisciITuoiDati section and click on continue-- Completed");

			logger.write("Verify and Enter the values in informazioniFornitura section-- Strat");
			oc.verifyComponentExistence(oc.informazioniFornitura);
			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);
			oc.enterInputParameters(oc.pod, prop.getProperty("POD"));
			oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
			oc.clickComponent(oc.capdropdown);
			oc.comprareText(oc.privacyText, oc.PrivacyText, true);
			oc.verifyComponentExistence(oc.prosegui1);
			oc.verifyComponentExistence(oc.inseriscilodopo);
			oc.clickComponent(oc.prosegui1);
			
			oc1.comprareText(oc1.popUpTitle, oc1.POPUP_TITLE, true);
			oc1.comprareText(oc1.popUpText, oc1.POPUP_TEXT, true);
			oc1.clickComponent(oc1.proseguipopupbutton);
			
			
			oc1.comprareText(oc1.valoreLucePlus, oc1.VALORE_LUCE_PLUS, true);
			oc1.comprareText(oc1.valoreLucePlusText, oc1.VALORE_LUCE_PLUS_TEXT, true);
			
			
//			oc.jsClickComponent(oc1.checkboX);
//			oc.clickComponent(oc1.proseguicust);
//			oc.verifyComponentExistence(oc.informazioniFornitura);
//			oc.comprareText(oc.informazioneText1, oc.InformazioneText1, true);
//			oc.verifyComponentExistence(oc.pod);
//			oc.verifyComponentExistence(oc.cap);
//			oc.clickComponent(oc.capdropdown);
//			oc.comprareText(oc.privacyText, oc.PrivacyText, true);
//			oc.verifyComponentExistence(oc.prosegui1);
//			oc.verifyComponentExistence(oc.inseriscilodopo);
//			
//			
//			oc.enterInputParameters(oc.pod, prop.getProperty("POD"));
//			oc.enterInputParameters(oc.cap, prop.getProperty("CAP"));
//			oc.clickComponent(oc.prosegui1);
//			
//			oc.comprareText(oc1.popUpText2, oc1.POPUP2_TEXT, true);
			
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}
}
