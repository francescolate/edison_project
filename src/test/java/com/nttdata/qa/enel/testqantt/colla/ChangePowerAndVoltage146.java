package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltageComponent;
import com.nttdata.qa.enel.components.colla.ChangePowerAndVoltagePreventintoComponent;
import com.nttdata.qa.enel.components.colla.DettaglioFornitureComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ChangePowerAndVoltage146 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
											
			home.verifyComponentExistence(home.enelLogo);
								
			home.clickComponent(home.detaglioFurnitura);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			
			logger.write("Check For page title-- start");
			
			dc.verifyComponentExistence(dc.pageTitle);
			
			logger.write("Check For page title-- Completed");
			
			logger.write("Click on Change Power And Voltage-- start");

			dc.verifyComponentExistence(dc.serviziPerleforniture);
			
			Thread.sleep(10000);
			
			dc.verifyComponentExistence(dc.modificaPotenzo);
			
			dc.jsClickObject(dc.modificaPotenzo);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			logger.write("Click on button Change Power And Voltage Button -- Completed");

			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.ModificaSubText, true);
			
			changePV.verifyComponentExistence(changePV.radioButton);
			
			changePV.verifyComponentExistence(changePV.flashIcon);
			
			logger.write("Check for Test Data-- Start");
			
			changePV.checkForData(changePV.address,prop.getProperty("ADDRESS"));
			
			changePV.checkForData(changePV.pod,prop.getProperty("POD"));
			
			changePV.checkForData(changePV.powerVoltageValue,prop.getProperty("PVVALUE"));

			logger.write("Check for Test Data-- Completed");
			
			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton);
			
			logger.write("Click on radio button-- Completed");
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.TitleSubText, true);			
			
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");
			
			changePV.comprareText(changePV.formLine1, changePV.FormLine1, true);
			
			logger.write("Check for Power Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.powerLable);
			
			logger.write("Check for Power Lable -- Completed");
			
			logger.write("Check for Default Power value -- Start");
			
			changePV.checkForDefaultValue(changePV.powerValue,prop.getProperty("POWERDEFAULT"));
			
			logger.write("Check for Default Power value -- Completed");
			
			logger.write("Check for Voltage Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.voltageLable);
			
			logger.write("Check for Voltage Lable -- Completed");
			
			logger.write("Check for Default Voltage value -- Start");
			
			changePV.checkForDefaultValue(changePV.voltageValue,prop.getProperty("VOLTAGEFAULT"));
			
			logger.write("Check for Default Voltage value -- Completed");
			
			changePV.comprareText(changePV.formLine2, changePV.FormLine2, true);
			
			changePV.checkButtonIsEnable(changePV.radioNo);
			
			changePV.comprareText(changePV.formLine3, changePV.FormLine3, true);
			
			changePV.changeDropDownValue(changePV.powerValue, prop.getProperty("POWER_INPUT"));
			
			changePV.checkFieldValue(changePV.emailInput, prop.getProperty("WP_USERNAME"));
						
			Thread.sleep(50000);
			changePV.clearField(changePV.cellular);
			
			changePV.enterInputtoField(changePV.cellular, prop.getProperty("MOBILE"));
			Thread.sleep(1000);
			changePV.enterInputtoField(changePV.confirmCellular, prop.getProperty("MOBILE"));
			
			changePV.clickComponent(changePV.calculateQuote);
			
			logger.write("Verify Pop up -- Start");

			changePV.verifyComponentExistence(changePV.attendiPopUpTitle);
			Thread.sleep(10000);
			changePV.comprareText(changePV.attendiPopUp, changePV.Attendiqualchesecondo, true);
			
			logger.write("Verify Pop up -- Completed");

			ChangePowerAndVoltagePreventintoComponent cp = new ChangePowerAndVoltagePreventintoComponent(driver);
			Thread.currentThread().sleep(30000);
			
			logger.write("Verify Preventito Page Content -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			cp.waitForElementToDisplay(cp.preventintoText);
			cp.verifyComponentExistence(cp.preventintoText);
	//		cp.comprareText(cp.preventintoText, cp.provinica_text, true);
			cp.verifyComponentExistence(cp.potenzoLabel);
			cp.checkData(cp.potenzoValue, prop.getProperty("POWER_INPUT"));
			cp.verifyComponentExistence(cp.tensionLabel);
			cp.checkData(cp.tensionValue, prop.getProperty("TENZION"));
//			cp.comprareText(cp.detagliocostiTitle, cp.dettaglio_costi, true);
//			cp.comprareText(cp.modalitàdiPagamentoText, cp.ModalitadiPagamento, true);
			cp.verifyComponentExistence(cp.emailInput);
			cp.verifyComponentExistence(cp.cellularInput);
			cp.verifyComponentExistence(cp.supplySection);
//			cp.comprareText(cp.preventivoText3, cp.PreventivoText3, true);
			
			logger.write("Verify Preventito Page Content -- Completed");

			cp.verifyComponentExistence(cp.conferma);
			cp.verifyComponentExistence(cp.indietro);
			
			logger.write("Click on Indietro -- Start");

			cp.clickComponent(cp.indietro);

			logger.write("Click on Indietro -- Completed");

			changePV.verifyComponentExistence(changePV.powerLable);
			prop.setProperty("RETURN_VALUE", "OK");
			
		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}
}
