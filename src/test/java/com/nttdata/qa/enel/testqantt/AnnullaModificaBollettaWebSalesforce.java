package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AccountSalesforceComponent;
import com.nttdata.qa.enel.components.colla.CaseSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.CaseItemDetailsComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.SbloccoTabComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class AnnullaModificaBollettaWebSalesforce {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			SeleniumUtilities util = new SeleniumUtilities(driver);
			SpinnerManager spinner = new SpinnerManager(driver);

			Thread.sleep(15000);
			
			AccountSalesforceComponent asc = new AccountSalesforceComponent(driver);
			
			logger.write("Click on last Richieste Aperte - START");
			asc.containsText(asc.firstCaseVariazioneVas, "VARIAZIONE VAS", true);
			asc.containsText(asc.firstCaseVariazioneVas, "IN LAVORAZIONE", true);
			asc.verifyComponentExistence(asc.linkFirstCaseVariazioneVas);
			asc.jsClickComponent(asc.linkFirstCaseVariazioneVas);
			logger.write("Click on last Richieste Aperte - COMPLETED");
			
//			Thread.sleep(5000);
//			SbloccoTabComponent sblocco = new SbloccoTabComponent(WebDriverManager.getDriverInstance(prop));
//			sblocco.chiudiVecchieInterazioni3();
			
			CaseSalesforceComponent csc = new CaseSalesforceComponent(driver);
			Thread.sleep(5000);
			logger.write("Click on Elemento Richiesta - START");
			csc.verifyComponentExistence(By.xpath("//*[@id='relatedListsTab__item']"));
			csc.jsClickComponent(By.xpath("//*[@id='relatedListsTab__item']"));
			csc.verifyComponentExistence(csc.elementoRichiesta);
			csc.jsClickComponent(csc.elementoRichiesta);
			logger.write("Click on Elemento Richiesta - COMPLETED");
			
			CaseItemDetailsComponent cidc = new CaseItemDetailsComponent(driver);   

			logger.write("Click Annullment - START");
	        cidc.clickComponent(cidc.annulmentButton);	        
	        Thread.sleep(2000);        
	        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@name='ITA_IFM_Case_Items__c.Annulment']")));        
	        cidc.clickComponent(cidc.dropDown);
	        Thread.sleep(500);
	        cidc.clickComponent(cidc.dropDownOption);	        
	        cidc.clickComponent(cidc.confermaAnnullament);
	        driver.switchTo().defaultContent();
	        spinner.waitForSpinnerBySldsHide(cidc.spinnerAfterAnnullament);
	        logger.write("Click Annullment - COMPLETED");
			
			
			
			
			
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
