package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CambioUsoComponent;
import com.nttdata.qa.enel.components.lightning.CheckListComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponent;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaMetodoPagamentoComponentEVO;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class CompilaDatiFornituraGasPrimaAttivazione {
	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			CompilaIndirizziComponent pod = new CompilaIndirizziComponent(driver);
			SeleniumUtilities util = new SeleniumUtilities(driver);
				if(Costanti.statusUbiest.compareTo("ON")==0){
				logger.write("Inserimento e Validazione Indirizzo - Start");
				pod.inserisciIndirizzo(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				logger.write("Inserimento e Validazione Indirizzo - Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			else if(Costanti.statusUbiest.compareTo("OFF")==0){
				logger.write("Inserimento Indirizzo Forzato - Start");
				pod.inserisciIndirizzoDaForzare(prop.getProperty("PROVINCIA"), prop.getProperty("CITTA"),
						prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
				pod.verificaIndirizzo(pod.buttonVerifica);
				pod.forzaIndirizzoPrimaAttivazione(prop.getProperty("CAP_FORZATURA"),prop.getProperty("CITTA_FORZATURA"));
				logger.write("Inserimento Indirizzo Forzato  - Completed");
				TimeUnit.SECONDS.sleep(5);
			}
			Thread.currentThread().sleep(5000);
			
			if(prop.getProperty("MATRICOLA_CONTATORE","").contentEquals("Y")) {
				String matricola=util.randomNumeric(11);
				pod.popolareCampo(matricola, pod.matricola);
				prop.setProperty("MATRICOLA_CONTATORE", matricola);
			}
			GestioneFornituraFormComponent compila = new GestioneFornituraFormComponent(
					driver);
			
//		    compila.clickComponentWithJseAndCheckSpinners(compila.confermaButton);
		    compila.press(compila.confermaButtonPrimaAttivazione);
			compila.clickComponentWithJseAndCheckSpinners(compila.creaOffertaMainButton);
			Thread.currentThread().sleep(15000);
			
			
			logger.write("Selezione Uso Fornitura - Start");
//			compila.selezionaUsoFornitura(prop.getProperty("USO_FORNITURA"));
			compila.selezionaLigtheningValue("Uso Fornitura", prop.getProperty("USO_FORNITURA"));
			compila.clickComponent(compila.buttonConfermaSelezioneUsoFornitura);
			logger.write("Selezione Uso Fornitura - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}

	}
	
	
	public static String checklistMessage="ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:  POD/PDR/numero utenza.INFORMAZIONI UTILI - Il Cambio Uso della fornitura di energia elettrica ha un costo pari a: 0,00 €.- Il Cambio Uso della fornitura di gas ha un costo pari a: 0,00 €.- Contestualmente alla variazione uso è possibile attivare/modificare/revocare i Servizi VASe il Metodo di pagamento - Per la variazione uso gas, nel caso di modifica dell'uso fornitura, è necessario eseguire il cambio prodotto.Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto."; 
}
