package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class AnnullamentoSubentro {

	public static void main(String[] args) throws Exception {

		Properties prop;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			By button_conferma = gestione.button_conferma;
			By button_annulla = gestione.button_annulla;
			By button_close_annullament = gestione.button_close_annullament;
			By button_confirm_annullament = gestione.button_confirm_annullament;
			By check_stato_by_text = By.xpath(gestione.check_stato_by_text.replaceFirst("##", "Chiuso"));
			By check_statodc_by_text = By.xpath(gestione.check_statodc_by_text.replaceFirst("##", "Chiuso"));
			By check_sstatodc_by_text = By.xpath(gestione.check_statodc_by_text.replaceFirst("##", "ANNULLATO"));
			//By check_sstatodc_by_text_quote = By.xpath(gestione.check_statodc_by_text.replaceFirst("##", "Offer - Annullata"));
			
			Thread.sleep(20000);
			logger.write("Verifica esito Offertabilita - Start");

			gestione.verifyComponentExistence(button_conferma);
			//gestione.clickComponent(button_conferma);
			gestione.verifyComponentExistence(button_annulla);
			gestione.clickComponent(button_annulla);
			gestione.verifyComponentExistence(button_close_annullament);
			gestione.clickComponent(button_close_annullament);
			gestione.verifyComponentExistence(button_annulla);
			gestione.clickComponent(button_annulla);
			gestione.verifyComponentExistence(button_confirm_annullament);
			gestione.clickComponent(button_confirm_annullament);
			gestione.verifyComponentExistence(check_stato_by_text);
			gestione.verifyComponentExistence(check_statodc_by_text);
			gestione.verifyComponentExistence(check_sstatodc_by_text);
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());
			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}
}
