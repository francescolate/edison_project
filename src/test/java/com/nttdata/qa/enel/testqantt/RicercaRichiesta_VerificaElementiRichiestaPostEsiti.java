package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabPaginaPrincipaleComponent;
import com.nttdata.qa.enel.components.lightning.RecuperaElementiOrdineComponent;
import com.nttdata.qa.enel.components.lightning.RicercaRichiesteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

/**
 * @author CapuanoTo Il seguente modulo si occupa della ricerca di una
 *         Richiesta accedendo prima alla sezione Richieste dal menu a tendina
 *         del primo tab disponibile
 */
public class RicercaRichiesta_VerificaElementiRichiestaPostEsiti {

	@Step("Ricerca Verifica Richiesta- Controllo provisioning Elementi della Richiesta")
	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				AccediTabPaginaPrincipaleComponent tabRichieste = new AccediTabPaginaPrincipaleComponent(driver);
				tabRichieste.openTab(By.xpath(
						"//span[(text()='Richieste' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]"));

				RicercaRichiesteComponent cercaRichieste = new RicercaRichiesteComponent(driver);
				cercaRichieste.searchRichiesta(prop.getProperty("NUMERO_RICHIESTA"));
				boolean statoRichiesta=cercaRichieste.verificaStatoRichiesta(prop.getProperty("EXPECTEDSTATUS_RICHIESTA_FINALE", "Chiuso"));
				boolean statoSottoRichiesta=cercaRichieste.verificaSottostatoRichiesta(prop.getProperty("EXPECTED_SOTTO_STATUS_RICHIESTA_FINALE", "Espletato"));
				if(!statoRichiesta || !statoSottoRichiesta){
					RecuperaElementiOrdineComponent elementiOrdine = new RecuperaElementiOrdineComponent(driver);
					String esiti="";
					if(prop.getProperty("COMMODITY").equals("ELE")){
						esiti="POD ELE:";
						esiti=esiti+elementiOrdine.dettaglioEsitiRichiesta(prop.getProperty("OI_RICHIESTA"));
					}
					if(prop.getProperty("COMMODITY").equals("GAS")){
						esiti="POD GAS:";
						esiti=esiti+elementiOrdine.dettaglioEsitiRichiesta(prop.getProperty("OI_RICHIESTA"));
					}
					throw new Exception("Stato richiesta diverso da:"+prop.getProperty("EXPECTEDSTATUS_RICHIESTA_FINALE", "Chiuso")
					+" oppure sottostato richiesta diverso da:"+prop.getProperty("EXPECTED_SOTTO_STATUS_RICHIESTA_FINALE", "Espletato")
					+" dettaglio esito provisionig:"+esiti);
					
				}
			}
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
