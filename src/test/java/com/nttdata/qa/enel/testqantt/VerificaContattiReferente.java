package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.AccediTabClientiComponent;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziReferenteComponent;
import com.nttdata.qa.enel.components.lightning.CreaNuovoReferenteComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;


public class VerificaContattiReferente {

	public static void main(String[] args) throws Exception {

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				CreaNuovoReferenteComponent crea = new CreaNuovoReferenteComponent(driver);
				CompilaIndirizziReferenteComponent indirizzi = new CompilaIndirizziReferenteComponent(driver);
				String frameName;
				AccediTabClientiComponent tabClienti = new AccediTabClientiComponent(driver);
				logger.write("Accesso al Tab Clienti - Start");
				tabClienti.accediTabClienti();
				logger.write("Accesso al Tab Clienti - Completed");
				TimeUnit.SECONDS.sleep(5);
				logger.write("Pressione pulsante Nuovo Referente - Start");
				crea.nuovoReferente();
				logger.write("Pressione pulsante Nuovo referente - Completed");
				TimeUnit.SECONDS.sleep(6);
				logger.write("Pressione pulsante Lente - Start");
				crea.clickbtnLente();
				logger.write("Pressione pulsante Lente - Completed");
				TimeUnit.SECONDS.sleep(6);
				logger.write("Pressione pulsante Nuovo - Start");
				crea.clickbtnNuovo();
				logger.write("Pressione pulsante Nuovo - Completed");
				TimeUnit.SECONDS.sleep(6);
				frameName = new SeleniumUtilities(driver).getFrameByIndex(0);
				logger.write("Pressione pulsante Calcolo Codice Fiscale - Start");
				crea.setFrameName(frameName);
				crea.verificaCalcoloCodiceFiscale(prop);
				logger.write("Pressione pulsante Calcolo Codice Fiscale - Completed");
				TimeUnit.SECONDS.sleep(6);
				logger.write("Anagrafica Referente - Start");
				crea.compilaAnagraficaReferenteConRuolo(prop);
				logger.write("Anagrafica Referente - Completed");
				TimeUnit.SECONDS.sleep(6);
				logger.write("Verifica obbligatorietà campi Contatti - Start");
				crea.verificaObbligatorietaCampiContatti();
				logger.write("Verifica obbligatorietà campi Contatti - Completed");
				TimeUnit.SECONDS.sleep(6);
				logger.write("Bottone Annulla - Start");
				crea.annullaCreazione();
//				crea.confermaCreazione();
				logger.write("Bottone Annulla - Completed");
					
			}

			logger.write("Verifica Cotatti OK");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());

//			String filePath = "src/test/resources/nuovireferenti.txt";
//			Files.write(Paths.get(filePath), ("REFERENTE "+"Id4R_Verifica Obbligatorietà Campi Contatti"+errors+"\n").getBytes(), StandardOpenOption.APPEND);
			
			if(prop.getProperty("RUN_LOCALLY","N").contentEquals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}

	}
}
