package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;


import org.openqa.selenium.remote.RemoteWebDriver;


import com.nttdata.qa.enel.components.colla.Privato331ACBAddebitoDirettoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyPodDetails331 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
		try 
		{
			Privato331ACBAddebitoDirettoComponent adb = new Privato331ACBAddebitoDirettoComponent(driver);
			
			
			/*logger.write("typing the user POD - START");	
			adb.verifyComponentExistence(adb.searchBar);
			adb.searchPOD(adb.searchBar, prop.getProperty("POD1_IT002E5928147A"), true);
			logger.write("typing the user POD - COMPLETED");
			
			logger.write("Validate and click on POD - START");	
			adb.verifyComponentExistence(adb.selectPod_IT002E5928147A);
//			adc.clickComponent(adc.autoSuggestSelectPod_02660026599153);
			
			adb.clickComponent(adb.selectPod_IT002E5928147A);
			logger.write("Validate and click on POD - COMPLETED");	
			
			logger.write("Validate and click on SERVICE REQUEST LINK - START");
			adb.click_Service_Requests_Link(adb.Service_Requests_Link,"Service Requests");
			logger.write("Validate and click on SERVICE REQUEST LINK - COMPLETED");
			
			logger.write("Verify and select latest date SERVICE REQUEST - START");
			adb.Validate_Service_Request_Section();
			logger.write("Verify and select latest date SERVICE REQUEST - COMPLETED");
			
			logger.write("Store the SR number - START");
			adb.storeServiceRequestId(adb.Service_Requests_RecordID);
			logger.write("Store the SR number - COMPLETED");*/
			
			logger.write("typing the user POD - START");	
			adb.verifyComponentExistence(adb.searchBar);
			adb.searchPOD(adb.searchBar, prop.getProperty("POD1_IT002E5928147A"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("Validate and click on POD - START");
			adb.verifyComponentExistence(adb.selectPod_IT002E5928147A);
			
			adb.clickComponent(adb.selectPod_IT002E5928147A);
			logger.write("Validate and click on POD - COMPLETED");
			
			logger.write("Validate and click on ELEMENTI OPERZIONE LINK - START");
			adb.click_Elementi_Operazione_Link(adb.Elementi_Operazione_Link,"Elementi Operazione");
			logger.write("Validate and click on ELEMENTI OPERZIONE LINK - COMPLETED");
			
			logger.write("Verify and select latest date ORDER_ID or CASE ITEM - START");
			adb.Validate_Elementi_Operazione_Section();
			logger.write("Verify and select latest date ORDER_ID or CASE ITEM - COMPLETED");
			
			/*logger.write("Verify Dati Di Process Section - START");
			adb.datiDiProcessSectionPod1(prop.getProperty("PROCESSO"),prop.getProperty("STATO"),prop.getProperty("METODO_DI_PAGAMENTO_HEADING"),prop.getProperty("NUOVO_METODO_DI_PAGAMENTO"),prop.getProperty("NUOVO_TITOLARE_DEL_CONTO_BANCARIO"),prop.getProperty("CF_DEL_NUOVO_TITOLARE_DEL_CONTO_BANCARIO"),prop.getProperty("IBAN_VALUE"));
			logger.write("Verify Dati Di Process Section - COMPLETED");*/
			
			logger.write("SWITCH to UDB Platform - START");
			adb.switchToUDBwindow_ClickSubmit(adb.idBpmVALUE, "KO",prop.getProperty("LINK_UDB"));
			logger.write("SWITCH to UDB Platform - COMPLETED");
			adb.refreshBrowser();
			
			/*logger.write("SWITCH to WB to update SR status - START");
			adb.switchToWb_UpdateServiceRequest(prop.getProperty("SERVICEREQUEST_LINK"),adb.ServiceRequestId);
			logger.write("SWITCH to WB to update SR status - COMPLETED");*/
			
			adb.refreshBrowser();
			
			logger.write("Click on ANNULMENT BUTTON and perform Annulment - START");
			adb.verifyComponentExistence(adb.AnnulmentButton);
			adb.clickComponent(adb.AnnulmentButton);
			
			adb.switchFrame("ITA_IFM_Case_Items__c.Annulment");
			
//			adb.verifyComponentExistence(adb.AnnulmentDialogTitle);
//			adb.VerifyText(adb.AnnulmentDialogTitle, "Annullamento Richiesta");
			
			adb.selectRinunciaCliente(adb.Rinuncia_cliente_Dropdown);
			logger.write("Click on ANNULMENT BUTTON and perform Annulment - COMPLETED");
			Thread.sleep(3000);
			adb.refreshBrowser();
			
			logger.write("Verify the ANNULMENT STATUS - START");
			adb.verifyComponentExistence(adb.Stato_FieldValue);
			adb.VerifyText(adb.Stato_FieldValue, "Annullato");
			logger.write("Verify the ANNULMENT STATUS - COMPLETED");
			
			/////////////////////////////////////////////////////////////////////////
			
			/*logger.write("typing the user POD - START");	
			adb.verifyComponentExistence(adb.searchBar);
			adb.searchPOD(adb.searchBar, prop.getProperty("POD2_IT002E3401567A"), true);
			logger.write("typing the user POD - COMPLETED");
			
			logger.write("Validate and click on POD - START");	
			adb.verifyComponentExistence(adb.selectPod_IT002E3401567A);
			
			adb.clickComponent(adb.selectPod_IT002E3401567A);
			logger.write("Validate and click on POD - COMPLETED");	
			
			logger.write("Validate and click on SERVICE REQUEST LINK - START");
			adb.click_Service_Requests_Link(adb.Service_Requests_Link,"Service Requests");
			logger.write("Validate and click on SERVICE REQUEST LINK - COMPLETED");
			
			logger.write("Verify and select latest date SERVICE REQUEST - START");
			adb.Validate_Service_Request_Section();
			logger.write("Verify and select latest date SERVICE REQUEST - COMPLETED");
			
			logger.write("Store the SR number - START");
			adb.storeServiceRequestId(adb.Service_Requests_RecordID);
			logger.write("Store the SR number - COMPLETED");*/
			
			logger.write("typing the user POD - START");	
			adb.verifyComponentExistence(adb.searchBar);
			adb.searchPOD(adb.searchBar, prop.getProperty("POD2_IT002E3401567A"), true);
			logger.write("typing the user CF - COMPLETED");
			
			logger.write("Validate and click on POD - START");
			adb.verifyComponentExistence(adb.selectPod_IT002E3401567A);
			
			adb.clickComponent(adb.selectPod_IT002E3401567A);
			logger.write("Validate and click on POD - COMPLETED");
			
			logger.write("Validate and click on ELEMENTI OPERZIONE LINK - START");
			adb.click_Elementi_Operazione_Link(adb.Elementi_Operazione_Link,"Elementi Operazione");
			logger.write("Validate and click on ELEMENTI OPERZIONE LINK - COMPLETED");
			
			logger.write("Verify and select latest date ORDER_ID or CASE ITEM - START");
			adb.Validate_Elementi_Operazione_Section();
			logger.write("Verify and select latest date ORDER_ID or CASE ITEM - COMPLETED");
			
			/*logger.write("Verify Dati Di Process Section - START");
			adb.datiDiProcessSectionPod2(prop.getProperty("PROCESSO"),prop.getProperty("STATO"),prop.getProperty("METODO_DI_PAGAMENTO_HEADING"),prop.getProperty("NUOVO_METODO_DI_PAGAMENTO"),prop.getProperty("NUOVO_TITOLARE_DEL_CONTO_BANCARIO"),prop.getProperty("CF_DEL_NUOVO_TITOLARE_DEL_CONTO_BANCARIO"),prop.getProperty("IBAN_VALUE"));
			logger.write("Verify Dati Di Process Section - COMPLETED");*/
			
			logger.write("SWITCH to UDB Platform - START");
			adb.switchToUDBwindow_ClickSubmit(adb.idBpmVALUE_POD2, "KO",prop.getProperty("LINK_UDB"));
			logger.write("SWITCH to UDB Platform - COMPLETED");
			
			adb.refreshBrowser();
			
			/*logger.write("SWITCH to WB to update SR status - START");
			adb.switchToWb_UpdateServiceRequest(prop.getProperty("SERVICEREQUEST_LINK"),adb.ServiceRequestId);
			logger.write("SWITCH to WB to update SR status - COMPLETED");*/
			
			adb.refreshBrowser();
			
			logger.write("Click on ANNULMENT BUTTON and perform Annulment - START");
			adb.verifyComponentExistence(adb.AnnulmentButton);
			adb.clickComponent(adb.AnnulmentButton);
			
			adb.switchFrame("ITA_IFM_Case_Items__c.Annulment");
			
//			adb.verifyComponentExistence(adb.AnnulmentDialogTitle);
//			adb.VerifyText(adb.AnnulmentDialogTitle, "Annullamento Richiesta");
			
			adb.selectRinunciaCliente(adb.Rinuncia_cliente_Dropdown);
			logger.write("Click on ANNULMENT BUTTON and perform Annulment - COMPLETED");
			Thread.sleep(3000);
			adb.refreshBrowser();
			
			logger.write("Verify the ANNULMENT STATUS - START");
			adb.verifyComponentExistence(adb.Stato_FieldValue);
			adb.VerifyText(adb.Stato_FieldValue, "Annullato");
			logger.write("Verify the ANNULMENT STATUS - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}

}