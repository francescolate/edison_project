package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.FibraDiMelitaComponent;
import com.nttdata.qa.enel.components.colla.HamburgerMenuComponent;
import com.nttdata.qa.enel.components.colla.LeftMenuComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ServiziComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerificaCoperturaKOFibraDiMelita {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			
			HamburgerMenuComponent hamburger=new HamburgerMenuComponent(driver);
			hamburger.clickComponent(hamburger.fibraDiMelitaLink);
			
			FibraDiMelitaComponent fibra = new FibraDiMelitaComponent(driver);
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Start");
			fibra.checkHeaderPathLinks(fibra.pathLinks1);
			fibra.verifyComponentExistence(fibra.headerTitle);
			fibra.verifyComponentExistence(fibra.headerSubTitle);
			fibra.checkNavBarLinks();
			logger.write("Verifica atterraggio pagina Fibra di Melita e check Voci navbar - Completed");
			
			logger.write("Voce di Puntamento Verifica Copertura e check campi - Start");
			fibra.clickComponent(fibra.verificaCoperturaAnchor);
			//fibra.clickComponent(fibra.closeChatHeading);
			fibra.verifyComponentExistence(fibra.comuneField);
			fibra.verifyComponentExistence(fibra.indirizzoField);
			fibra.verifyComponentExistence(fibra.civicoField);
			fibra.verifyComponentExistence(fibra.verificaLaCoperturaButton);
			fibra.verifyComponentExistence(fibra.nonTroviIlTuoIndirizzoLink);
			logger.write("Voce di Puntamento Verifica Copertura e check campi - Completed");
			
			logger.write("Verifica Copertura Esito KO - Start");
			fibra.inserisciIndirizzo(prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
			fibra.clickComponent(fibra.verificaLaCoperturaButton);
			fibra.verifyComponentExistence(fibra.headerTitle_2);
			fibra.verifyComponentExistence(fibra.headerSubTitleKO);
			fibra.checkElementWithText(fibra.mainContactFiberLabel, "Restiamo in contatto!");
			fibra.verificaMancataCoperturaIndirizzo(prop.getProperty("COMUNE"), prop.getProperty("INDIRIZZO"), prop.getProperty("CIVICO"));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Nome*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Cognome*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Codice Fiscale*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Telefono Cellulare*")));
			fibra.verifyComponentExistence(By.xpath(fibra.inputFibra.replace("#", "Email*")));
			fibra.verifyComponentExistence(By.xpath(fibra.genericLabel.replace("#", "Ho preso visione")));
			fibra.verifyComponentExistence(By.xpath(fibra.genericLabel.replace("#", "testo marketing")));
			fibra.verifyComponentExistence(fibra.proseguiButton);
			fibra.verifyComponentExistence(fibra.tornaHomeButton);
			logger.write("Verifica Copertura Esito KO - Completed");
			
			logger.write("Click sul tasto <Torna alla home> - Start");
            fibra.clickComponent(fibra.tornaHomeButton);
            fibra.checkUrl(prop.getProperty("LINK"));
			logger.write("Click sul tasto <Torna alla home> - Completed");
            prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
