package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.AreaRiservataHomePageComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Area_Riservata_HomePage_ACR_12 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		
		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			AreaRiservataHomePageComponent home = new AreaRiservataHomePageComponent(driver);
									
		    prop.setProperty("HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("HOMEPAGE_TITLE2", "HOMEPAGE");
		    prop.setProperty("HOMEPAGE_TITLE_DESCRIPTION", "Benvenuto nella tua area dedicata Business");
		    prop.setProperty("SUPPLY_BILL_TITLE", "Forniture e bollette");
//		    prop.setProperty("CUSTOMER_LIGHT_SUPPLY", "prova alias bsn\nFornitura Luce Cliente N °: 636727539");
		    prop.setProperty("SUPPLY_DETAIL_HEADING", "La tua fornitura nel dettaglio.");
		    prop.setProperty("SUPPLY_DETAIL_TITLE1", "Tutto quello che c'è da sapere sulla tua fornitura");
		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION1", "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.");
		    prop.setProperty("SUPPLY_HOMEPAGE_TITLE1", "AREA RISERVATA");
		    prop.setProperty("SUPPLY_HOMEPAGE_TITLE2", "FORNITURA");
		    prop.setProperty("SUPPLY_DETAILS1", "Fornitura Luce Cliente N°: " + prop.getProperty("CUSTOMER_NUM"));
		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION2", "Se cerchi una bolletta in particolare usa i filtri per trovarla più facilmente.");
		    prop.setProperty("SUPPLY_DETAIL_DESCRIPTION3", "Ci sono bollette scadute. Pagale comodamente on line. Se richiesto, in seguito puoi inviare il dimostrato pagamento cliccando su \"invia documenti\".");
		    prop.setProperty("SUPPLY2_TITLE", "Le tue Bollette");
		    prop.setProperty("BILL_DETAILS", "Le tue bollette");
		    prop.store(new FileOutputStream(args[0]), "Set New Value");
			
			logger.write("Home Page Logo, Title and Description Verification- Starts");
			home.verifyComponentExistence(home.homePageLogo);
			home.VerifyText(home.homePageTitle1,prop.getProperty("HOMEPAGE_TITLE1"));
			home.VerifyText(home.homePageTitle2,prop.getProperty("HOMEPAGE_TITLE2"));
			home.VerifyText(home.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"));
			logger.write("Home Page Log,Title and Description Verification- Ends");
			
			logger.write("Home Page Left Menu Items Verification- Starts");
			home.leftMenuItemsDisplay(home.leftMenuItems);
			logger.write("Home Page Left Menu Items Verification- Ends");
			
			logger.write("Home Page Supply Detail Verification- Starts");
			home.VerifyText(home.supplyBillTitle,prop.getProperty("SUPPLY_BILL_TITLE"));
			home.VerifyText(home.supplyHeading, prop.getProperty("SUPPLY_DETAIL_HEADING"));
//			home.supplyDetailsDisplay(home.supplyDetails);
		    logger.write("Home Page Supply Detail Verification- Ends");	
		    
		    home.clickComponent(home.piuInformazionlink);
		    logger.write("AREA RISERVATA/FORNITURA Verification- Starts");
		    Area_Riservata_More_Information.main(args);
		    logger.write("AREA RISERVATA/FORNITURA Verification- Ends");
		    
		    logger.write("Bill Details Verification- Starts");
		    home.VerifyText(home.billDetails, prop.getProperty("BILL_DETAILS"));
		    home.supplyBillDetailsDisplay(home.billTable);
		    logger.write("Bill Details Verification- Ends");
		    
		    logger.write("VAI ALL'ELENCO BOLLETTE Verification - Starts");
		    home.verifyComponentExistence(home.vaiAllelencolink);
		    home.clickComponent(home.vaiAllelencolink);
		    home.VerifyText(home.SupplyTitle2,prop.getProperty("SUPPLY2_TITLE"));
			home.VerifyText(home.supplyDescription2,prop.getProperty("SUPPLY_DETAIL_DESCRIPTION2"));
//			home.VerifyText(home.supplyDescription3,prop.getProperty("SUPPLY_DETAIL_DESCRIPTION3"));
			home.verifyComponentExistence(home.inviaDocumenti);
			home.verifyComponentExistence(home.mostraFiltri);
			home.verifyComponentExistence(home.esportainExcel);
		    logger.write("VAI ALL'ELENCO BOLLETTE- Ends");
		    
		    //back browser
		    logger.write("Arrival Page Verification- Starts");
		    home.backBrowser(home.homePageLogo);
		    logger.write("Arrival Page Verification- Ends");
		    prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}

			
			
