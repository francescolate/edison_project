package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.RecoverPasswordComponent;
import com.nttdata.qa.enel.components.colla.RecoverUsernameComponent;
import com.nttdata.qa.enel.components.colla.RegistrationTypeChoiceComponent;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class VerifyLoginPage {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getNewWebDriver(prop);
			
			//Start Set dati verifiche
			String BASE_LINK = prop.getProperty("LINK");
			//** END Set dati verifiche
			
			LoginLogoutEnelCollaComponent loginPage = new LoginLogoutEnelCollaComponent(driver);
			
			System.out.println("Accessing login page");
			logger.write("Accessing login page - Start");
			loginPage.launchLink(prop.getProperty("LINK"));
			By logo = loginPage.logoEnel;
			loginPage.verifyComponentExistence(logo);
			//Click chusura bunner pubblicitario se esistente
			loginPage.clickComponentIfExist( By.xpath("//div[@id='fsa-close-button']"));

			By acceptCookieBtn = loginPage.buttonAccetta;
			loginPage.verifyComponentExistence(acceptCookieBtn);
			loginPage.jsClickComponent(acceptCookieBtn);
			By icon = loginPage.iconUser;
			loginPage.verifyComponentExistence(icon);
			loginPage.clickComponent(icon); 
			//loginPage.jsClickComponent(icon);
			logger.write("Accessing login page - Completed");
			
			System.out.println("Verifying username and password fields");
			logger.write("Verifying username and password fields - Start");
			By usernameField = loginPage.username;
			loginPage.verifyComponentVisibility(usernameField);
			By passwordField = loginPage.password;
			loginPage.verifyComponentVisibility(passwordField);
			logger.write("Verifying username and password fields - Completed");
			
			System.out.println("Verifying access button");
			logger.write("Verifying access button - Start");
			//By accessButton = loginPage.buttonLoginAccedi2;
			By accessButton = loginPage.buttonLoginAccedi;
			loginPage.verifyComponentVisibility(accessButton);
			logger.write("Verifying access button - Completed");
			
			System.out.println("Verifying recovery password and username links");
			logger.write("Verifying recovery password and username links - Start");
			By recoveryPassword = loginPage.recoverPasswordBtn;
			loginPage.verifyComponentVisibility(recoveryPassword);
			By recoveryUsername = loginPage.recoverUsernameBtn;
			loginPage.verifyComponentVisibility(recoveryUsername);
			logger.write("Verifying recovery password and username links - Completed");
			
			System.out.println("Verifying access problems link");
			logger.write("Verifying access problems link - Start");
			By accessProblemsLabel = loginPage.accessProblemsBtn;
			loginPage.verifyComponentVisibility(accessProblemsLabel);
			//loginPage.verifyComponentText(accessProblemsLabel, prop.getProperty("ACCESS_PROBLEMS_STRING"));
			By accessProblemsButton = loginPage.accessProblemsBtn;
			loginPage.verifyComponentVisibility(accessProblemsButton);
			loginPage.jsClickComponent(accessProblemsButton);
			Thread.sleep(10000);
			verifySelfCareFaqLanding(driver, prop.getProperty("LINK_FAQ"), prop.getProperty("TITLE_FAQ"));
			driver.navigate().back();
			logger.write("Verifying access problems link - Completed");
			
			System.out.println("Verifying social access buttons");
			logger.write("Verifying social access buttons - Start");
			By googleAccessButton = loginPage.googleAccessBtn;
			loginPage.verifyComponentVisibility(googleAccessButton);
			By facebookAccessButton = loginPage.facebookAccessBtn;
			loginPage.verifyComponentVisibility(facebookAccessButton);
			logger.write("Verifying social access buttons - Completed");
			
			System.out.println("Verifying registration elements");
			logger.write("Verifying registration elements - Start");
			By noAccountLabel = loginPage.noAccountLabelNew;
			loginPage.verifyComponentVisibility(noAccountLabel);
			By registerButton = loginPage.registerBtn;
			loginPage.verifyComponentVisibility(registerButton);
//			By accessProblemsLink = loginPage.accessProblemsLink;
//			loginPage.verifyComponentVisibility(accessProblemsLink);
			logger.write("Verifying registration elements - Completed");
			
//			System.out.println("Verifying recovery password");
			logger.write("Verifying recovery password - Start");
			loginPage.jsClickComponent(recoveryPassword);
			RecoverPasswordComponent recoverPasswordPage = new RecoverPasswordComponent(driver);
			By recoverPasswordHeader = recoverPasswordPage.pageHeader;
			recoverPasswordPage.verifyComponentVisibility(recoverPasswordHeader);
			recoverPasswordPage.verifyComponentText(recoverPasswordHeader, prop.getProperty("RECOVERY_PWD_STRING_1"));
			By recoverPasswordSubHeader = recoverPasswordPage.pageSubHeader;
			recoverPasswordPage.verifyComponentVisibility(recoverPasswordSubHeader);
			recoverPasswordPage.verifyComponentText(recoverPasswordSubHeader, prop.getProperty("RECOVERY_PWD_STRING_2"));
			By recPwdEmailField = recoverPasswordPage.emailField;
			recoverPasswordPage.verifyComponentVisibility(recPwdEmailField);
			By recPwdSendButton = recoverPasswordPage.sendBtn;
			recoverPasswordPage.verifyComponentVisibility(recPwdSendButton);
			driver.navigate().back();
			logger.write("Verifying recovery password - Completed");
			
			System.out.println("Verifying recovery username");
			logger.write("Verifying recovery username - Start");
			loginPage.clickComponent(recoveryUsername);
			RecoverUsernameComponent recUsrPage = new RecoverUsernameComponent(driver);
			By recUsrHeader = recUsrPage.pageHeader;
			recUsrPage.verifyComponentVisibility(recUsrHeader);
			recUsrPage.verifyComponentText(recUsrHeader, prop.getProperty("RECOVERY_USRNM_STRING_1"));
			By recUsrSubHeader = recUsrPage.pageSubHeader;
			recUsrPage.verifyComponentVisibility(recUsrSubHeader);
			recUsrPage.verifyComponentText(recUsrSubHeader, prop.getProperty("RECOVERY_USRNM_STRING_2"));
			By recUsrCfField = recUsrPage.cfField;
			recUsrPage.verifyComponentVisibility(recUsrCfField);
			By recUsrSendButton = recUsrPage.sendBtn;
			recUsrPage.verifyComponentVisibility(recUsrSendButton);
			driver.navigate().back();
			logger.write("Verifying recovery username - Completed");
			
			System.out.println("Verifying registration page");
			logger.write("Verifying registration page - Start");
			loginPage.clickComponent(registerButton);
			RegistrationTypeChoiceComponent regSelectionPage = new RegistrationTypeChoiceComponent(driver);
			By regWelcomeMessage = regSelectionPage.welcomeMessageLabel;
			regSelectionPage.verifyComponentVisibility(regWelcomeMessage);
			regSelectionPage.verifyComponentText(regWelcomeMessage, prop.getProperty("REGISTER_STRING_1"));
			By regHeader = regSelectionPage.pageHeader;
			regSelectionPage.verifyComponentVisibility(regHeader);
			regSelectionPage.verifyComponentText(regHeader, prop.getProperty("REGISTER_STRING_2"));
			By privateSelector = regSelectionPage.privateSelector;
			regSelectionPage.verifyComponentVisibility(privateSelector);
			By businessSelector = regSelectionPage.businessSelector;
			regSelectionPage.verifyComponentVisibility(businessSelector);
			By continueButton = regSelectionPage.continueBtn;
			regSelectionPage.verifyComponentVisibility(continueButton);
//			regSelectionPage.checkContinueButtonIsDisabled();
			driver.navigate().back();
			logger.write("Verifying registration page - Completed");

			System.out.println("Verifying empty fields access attempt error");
			logger.write("Verifying empty fields access attempt error - Start");
			Thread.sleep(2000);
			//loginPage.jsClickComponent(accessButton);
			loginPage.clickComponent(accessButton);
			Thread.sleep(2000);
			By obligatoryUsername = loginPage.obligatoryUsernameLabel;
			loginPage.verifyComponentVisibility(obligatoryUsername);
			loginPage.verifyComponentText(obligatoryUsername, prop.getProperty("ACCESS_ERR_STRING_1"));
			By obligatoryPassword = loginPage.obligatoryPasswordLabel;
			loginPage.verifyComponentVisibility(obligatoryPassword);
			loginPage.verifyComponentText(obligatoryPassword, prop.getProperty("ACCESS_ERR_STRING_2"));
			logger.write("Verifying empty fields access attempt error - Completed");
			Thread.sleep(5000);
			System.out.println("User access");
			logger.write("User access - Start");
			loginPage.enterLoginParameters(usernameField, prop.getProperty("USERNAME"));
			loginPage.enterLoginParameters(passwordField, prop.getProperty("PASSWORD"));
			Thread.sleep(3000);
			//loginPage.jsClickComponent(accessButton);
			loginPage.clickComponent(accessButton);
			loginPage.enterLoginParameters(passwordField, prop.getProperty("PASSWORD"));
			loginPage.clickComponent(accessButton);
			Thread.sleep(5000);
			logger.write("User access - Completed");

			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
	
	private static void verifySelfCareFaqLanding(WebDriver driver, String pageUrl, String pageTitle) throws Exception {
		String title = driver.getTitle();
		String url = driver.getCurrentUrl();
//		System.out.println("Title: " + title + " vs requested: " + pageTitle);
//		System.out.println("Url: " + url + " vs requested: " + pageUrl);
		if (!title.equals(pageTitle) || !url.equals(pageUrl)) {
			throw new Exception("Clicking on the access problems button did not lead to the selfcare FAQ page");
		}
	}

}
