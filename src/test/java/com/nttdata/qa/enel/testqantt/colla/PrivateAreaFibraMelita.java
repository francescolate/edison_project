package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaFibraComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaFibraMelita {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaFibraComponent paf = new PrivateAreaFibraComponent(driver);
			
			//STEP 5 Seleziona Banner Melita
			logger.write("click the button Show Offer in the card with FIBRA-MELITA offer - START");
			
			By button_show_offer_fibra = paf.button_show_offer_fibra;
			
			paf.verifyComponentExistence(button_show_offer_fibra);
			paf.clickComponent(button_show_offer_fibra);
			
			logger.write("click the button Show Offer in the card with FIBRA-MELITA offer - COMPLETED");
			
			//STEP 6 Tasto continua
			logger.write("click the Continue button - START");
			
			By button_continue_fibra = paf.button_continue_fibra;
			
			paf.verifyComponentExistence(button_continue_fibra);
			paf.clickComponent(button_continue_fibra);
			
			logger.write("click the Continue button - COMPLETED");
						
			logger.write("chiudere la popup 'Verifica copertura non disponibile' se presente - START");
			TimeUnit.SECONDS.sleep(10);
            By id_frame = paf.id_frame;
			paf.setframe(id_frame);
			TimeUnit.SECONDS.sleep(5);
//			paf.clickIfExist(paf.closePopup);
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(5);
			logger.write("chiudere la popup 'Verifica copertura non disponibile' se presente - COMPLETED");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
