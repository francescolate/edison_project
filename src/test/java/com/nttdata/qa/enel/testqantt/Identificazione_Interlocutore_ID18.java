package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
//import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
//import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
//import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.util.QANTTLogger;
//import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class Identificazione_Interlocutore_ID18 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
//				SeleniumUtilities util = new SeleniumUtilities(driver);
				/*SceltaProcessoComponent avvioSubentro = new SceltaProcessoComponent(driver);
				logger.write("Selezione processo - Start");
				TimeUnit.SECONDS.sleep(10);
				avvioSubentro.clickAllProcess();
				TimeUnit.SECONDS.sleep(2);
				avvioSubentro.chooseProcessToStart(avvioSubentro.avvioSubentroEVO);
				logger.write("Selezione processo - Completed");*/

				logger.write("Identificazione interlocutore EVO_v1.0 - ID18 - Start");
				IdentificazioneInterlocutoreComponentEVO identificaInterlocutore = new IdentificazioneInterlocutoreComponentEVO(driver);
				logger.write("i campi nome, cognome e CF sono disabilitati - Start");
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoNome);
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoCognome);
				identificaInterlocutore.verifyComponentExistence(identificaInterlocutore.campoCF);
				
				if(prop.getProperty("SALVA_DATI_DELEGATO", "").contentEquals("Y")) {
					String nome=identificaInterlocutore.salvaNome();
					String cognome=identificaInterlocutore.salvaCognome();
					String cf=identificaInterlocutore.salvaCF();
					prop.setProperty("NOME_DELEGATO", nome);
					prop.setProperty("COGNOME_DELEGATO", cognome);
					prop.setProperty("CF_DELEGATO", cf);
				}
				logger.write("i campi nome, cognome e CF sono disabilitati - Completed");
				logger.write("Inserimento Documento di identità del titolare - Start");

				By input_tipo_documento = identificaInterlocutore.input_tipo_documento;
				identificaInterlocutore.verifyComponentExistence(input_tipo_documento);
				identificaInterlocutore.clickComponent(input_tipo_documento);
				
				By pickuplist_tipo_documento = identificaInterlocutore.pickuplist_tipo_documento;
				identificaInterlocutore.verifyComponentExistence(pickuplist_tipo_documento);
				identificaInterlocutore.clickComponent(pickuplist_tipo_documento);
				
				By input_numero_documento = identificaInterlocutore.input_numero_documento;
				identificaInterlocutore.verifyComponentExistence(input_numero_documento);
				identificaInterlocutore.clickComponent(input_numero_documento);
				driver.findElement(input_numero_documento).sendKeys(prop.getProperty("NUMERO_DOCUMENTO","12321"));
//				identificaInterlocutore.insertTextByChar(input_numero_documento, "00000");
				
				By input_rilasciato_il = identificaInterlocutore.input_rilasciato_il;
				identificaInterlocutore.verifyComponentExistence(input_rilasciato_il);
				identificaInterlocutore.clickComponent(input_rilasciato_il);
				
				if(prop.getProperty("RILASCIATO_IL","").contains("SYSDATE")){
					
					By button_sysdate = identificaInterlocutore.button_sysdate;
					identificaInterlocutore.verifyComponentExistence(button_sysdate);
					identificaInterlocutore.clickComponent(button_sysdate);
					
				}else{
					identificaInterlocutore.clickComponent(input_rilasciato_il);
					driver.findElement(input_rilasciato_il).sendKeys(prop.getProperty("RILASCIATO_IL","06/07/2019"));
				}
				
				By input_rilasciato_da = identificaInterlocutore.input_rilasciato_da;
				identificaInterlocutore.verifyComponentExistence(input_rilasciato_da);
				identificaInterlocutore.clickComponent(input_rilasciato_da);
				driver.findElement(input_rilasciato_da).sendKeys(prop.getProperty("RILASCIATO_DA","mctc"));
//				identificaInterlocutore.insertTextByChar(input_rilasciato_da, "ccccc");
				
				
							
				By button_conferma = identificaInterlocutore.button_conferma;
				identificaInterlocutore.verifyComponentExistence(button_conferma);
				identificaInterlocutore.clickComponent(button_conferma);
				logger.write("Inserimento Documento di identità del titolare - Completed");
				logger.write("Selezione tipo delega e click su pulsante Conferma e poi di nuovo su Conferma - Start");
				DelegationManagementComponentEVO delega = new DelegationManagementComponentEVO(driver);
				delega.selezionaDelega( "Delega light");
				TimeUnit.SECONDS.sleep(2);
				delega.valorizzaDelegato();
				TimeUnit.SECONDS.sleep(1);
				delega.conferma(delega.confermaButton);
				delega.verifyComponentExistence(delega.inputTipoDelega);
				delega.verificaValoreTipoDelega("Delega light");
				identificaInterlocutore.pressButton(identificaInterlocutore.confermaFinaleInBasso);
				logger.write("Selezione tipo delega e click su pulsante Conferma e poi di nuovo su Conferma - Completed");
				GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
				gestione.checkSpinnersSFDC();
				TimeUnit.SECONDS.sleep(3);
				
				
//				By button_conferma_2 = identificaInterlocutore.button_conferma_2;
//				identificaInterlocutore.verifyComponentExistence(button_conferma_2);
//				identificaInterlocutore.clickComponent(button_conferma_2);
//				
				logger.write("Identificazione interlocutore EVO_v1.0 - ID18 - Completed");
				
				
				prop.setProperty("RETURN_VALUE", "OK");
			} catch (Exception e) {
				prop.setProperty("RETURN_VALUE", "KO");
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				errors.toString();
				logger.write("ERROR_DESCRIPTION: "+errors.toString());

				prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//				return;
				if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
					throw e;

			} finally {
				// Store WebDriver Info in properties file
				prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
			}
		}

			}
