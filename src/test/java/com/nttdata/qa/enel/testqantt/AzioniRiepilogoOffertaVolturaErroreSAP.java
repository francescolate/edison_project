package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.CheckListComponentEVO;
import com.nttdata.qa.enel.components.lightning.CompilaIndirizziComponentEVO;
import com.nttdata.qa.enel.components.lightning.DelegationManagementComponentEVO;
import com.nttdata.qa.enel.components.lightning.FornitureComponent;
import com.nttdata.qa.enel.components.lightning.GestioneDocumentiIdentitaComponentEVO;
import com.nttdata.qa.enel.components.lightning.GestioneFornituraFormComponent;
import com.nttdata.qa.enel.components.lightning.GestionePODperSWAEVO;
import com.nttdata.qa.enel.components.lightning.GestionePaginoneSwaEvo;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponent;
import com.nttdata.qa.enel.components.lightning.IdentificazioneInterlocutoreComponentEVO;
import com.nttdata.qa.enel.components.lightning.InserimentoFornitureSubentroComponent;
import com.nttdata.qa.enel.components.lightning.IntestazioneNuovaRichiestaComponentEVO;
import com.nttdata.qa.enel.components.lightning.PrecheckComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.SceltaProcessoComponent;
import com.nttdata.qa.enel.components.lightning.SezioneMercatoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class AzioniRiepilogoOffertaVolturaErroreSAP {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);

			SeleniumUtilities util = new SeleniumUtilities(driver);
			RiepilogoOffertaComponent offer=new RiepilogoOffertaComponent(driver);
			GestioneFornituraFormComponent gestione = new GestioneFornituraFormComponent(driver);
			
			logger.write("click su pulsante 'Conferma' presente nella sezione 'Riepilogo offerta' - Start");
			offer.clickComponent(offer.confermaRiepilogoOfferta);
			
			gestione.checkSpinnersSFDC();
			logger.write("click su pulsante 'Conferma' presente nella sezione 'Riepilogo offerta' - Completed");
			
			logger.write("verifica esistenza pulsante 'Modifica' nella sezione 'Riepilogo offerta' - Start");
			offer.verifyComponentExistence(offer.buttonModificaRiepilogoOfferta);
			logger.write("verifica esistenza pulsante 'Modifica' nella sezione 'Riepilogo offerta' - Completed");
			
			logger.write("Dalla sezione 'Selezione Uso Fornitura' selezionare dalla pick list '"+prop.getProperty("USO")+"' e premere conferma - Start");
			offer.verifyComponentExistence(offer.sezioneSelezioneUsoFornitura);
			offer.scrollComponent(offer.sezioneSelezioneUsoFornitura);
			TimeUnit.SECONDS.sleep(1);
			
			offer.selezionaUsoFornitura(prop.getProperty("USO"));
			offer.verifyComponentExistence(offer.buttonConfermaSezioneUsoFornitura);
			offer.clickComponent(offer.buttonConfermaSezioneUsoFornitura);
			gestione.checkSpinnersSFDC();
			TimeUnit.SECONDS.sleep(3);
			logger.write("Dalla sezione 'Selezione Uso Fornitura' selezionare dalla pick list '"+prop.getProperty("USO")+"' e premere conferma - Completed");
			
			logger.write("verifica esistenza pulsante 'Modifica' nella sezione 'Selezione Uso Fornitura' - Start");
			offer.verifyComponentExistence(offer.buttonModificaUsoFornitura);
			logger.write("verifica esistenza pulsante 'Modifica' nella sezione 'Selezione Uso Fornitura' - Completed");
			
			logger.write("valorizzare i campi Categoria Merciologica SAP con 'Abitazioni Private'; Titolarietà con 'Proprità o Usufrutto'; Consumo Annuo con un valore a piacere; Flag Residente con '"+prop.getProperty("FLAG_RESIDENTE")+"' - Start");
			offer.popolareCampiDatiFornitura(prop.getProperty("CATEGORIA"), prop.getProperty("TITOLARITA"),  prop.getProperty("CONSUMO"), prop.getProperty("FLAG_RESIDENTE"));
			logger.write("valorizzare i campi Categoria Merciologica SAP con 'Abitazioni Private'; Titolarietà con 'Proprità o Usufrutto'; Consumo Annuo con un valore a piacere; Flag Residente con '"+prop.getProperty("FLAG_RESIDENTE")+"' - Completed");
			
			logger.write("click sul pulsante 'Conferma fornitura' - Start");
			offer.clickComponent(offer.buttonConfermaFornitura);
			
			gestione.checkSpinnersSFDC();
			logger.write("click sul pulsante 'Conferma fornitura' - Completed");
			
			
				logger.write("Verifica Messaggio Errore SAP - Start");
				
				TimeUnit.SECONDS.sleep(10);
				offer.verificaMessaggioErroreSAP(prop.getProperty("POD"));
				gestione.checkSpinnersSFDC();
				logger.write("Verifica Messaggio Errore SAP - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
//			return;
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	}

}
