package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.PrivateAreaBolletteMyComponent;
import com.nttdata.qa.enel.components.colla.PrivateAreaMenuLogoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class PrivateAreaMenuLogo {

	public static void main(String[] args)throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateAreaMenuLogoComponent pabc = new PrivateAreaMenuLogoComponent(driver);
							
			logger.write("checking and clicking on the Logo - START");
			pabc.verifyComponentExistence(pabc.homePageLogo);
			pabc.clickComponent(pabc.homePageLogo);
			logger.write("checking and clicking on the Logo - ENDS");
			
			logger.write("checking the Menu Display - START");
			pabc.verifyComponentExistence(pabc.modificaLink);
			pabc.verifyComponentExistence(pabc.datiDiContatto);
			pabc.verifyComponentExistence(pabc.ilTuoProfilo);
			logger.write("checking the Menu Display - ENDS");
			
			logger.write("checking the opening of popup with correct text - START");
			pabc.clickComponent(pabc.modificaLink);
			pabc.comprareText(pabc.popupText, PrivateAreaBolletteMyComponent.POPUP_TEXT, true);
			logger.write("checking the opening of popup with correct text - ENDS");
			
			pabc.verifyComponentExistence(pabc.seleziona);
			pabc.clickComponentJs(pabc.seleziona);
			//pabc.UploadDocumentsComponent(pabc.seleziona);
			prop.setProperty("RETURN_VALUE", "OK");
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}

	}

}
