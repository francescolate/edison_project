package com.nttdata.qa.enel.testqantt.r2d;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.r2d.R2D_CaricamentoEsitiDelibera40Component;
import com.nttdata.qa.enel.components.r2d.R2D_MenuBoxComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

import io.qameta.allure.Step;

public class R2D_CaricamentoEsitiDelibera40Istruttoria_GAS {

	public static void main(String[] args) throws Exception {
		Properties prop = null;

		try {

			prop = WebDriverManager.getPropertiesIstance(args[0]);
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			QANTTLogger logger = new QANTTLogger(prop);

			if (prop.getProperty("MODULE_ENABLED", "Y").equals("Y")) {
				R2D_MenuBoxComponent menuBox = new R2D_MenuBoxComponent(driver);
				//Selezione Opzione dal nenù a sinistra
				menuBox.selezionaVoceMenuBox("Code di comunicazione","Caricamento Avanzamenti PS e Sospensioni PS");
				R2D_CaricamentoEsitiDelibera40Component caricamentoEsiti = new R2D_CaricamentoEsitiDelibera40Component(driver);
				//Selezione tipologia Caricamento Avanzata
				caricamentoEsiti.selezionaTipoCaricamentoAvanzato("Avanzamenti delibera 40");
				//Inserisci Pdr
				caricamentoEsiti.inserisciPdr(caricamentoEsiti.inputPRD, prop.getProperty("POD_GAS", prop.getProperty("POD")));
				//Inserisci ID Richiesta CRM
				caricamentoEsiti.inserisciNumeroOrdineCRM(caricamentoEsiti.inputNumeroOrdineCRM, prop.getProperty("OI_RICERCA"));
				//Cerca
				caricamentoEsiti.cercaPod(caricamentoEsiti.buttonCerca);
				//Click button Azione
				caricamentoEsiti.selezionaTastoAggiornaPratica();
				//Selezione avanzamento Istruttoria
				caricamentoEsiti.selezioneAvanzamento(caricamentoEsiti.selectAvanzamento, prop.getProperty("ESITO_DELIBERA40_ISTRUTTORIA"));
				
			}
			//
			prop.setProperty("RETURN_VALUE", "OK");
		}
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
}
