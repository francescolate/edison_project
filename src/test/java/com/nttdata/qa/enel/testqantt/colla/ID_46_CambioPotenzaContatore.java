package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SupportoCambioPotenzaContatore;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ID_46_CambioPotenzaContatore {

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			SupportoCambioPotenzaContatore sup = new SupportoCambioPotenzaContatore(driver);
			String supportoURL = prop.getProperty("WP_LINK")+"it/supporto.html";
			String supportoPotenzaETensioneURL = prop.getProperty("WP_LINK")+"it/supporto/faq/cambio-potenza-contatore";
			
			sup.containsText(sup.areaRiservataHomepagePath, sup.AREA_RISERVATA_HOMEPAGE_PATH, true);
			sup.containsText(sup.areRiservataHomepageHeading, sup.AREA_RISERVATA_HOMEPAGE_HEADING, true);
			
			logger.write("Clicking on Supporto HEader and verification of data-Started");
			sup.clickComponent(sup.supportoHeader);
			
			sup.checkURLAfterRedirection(supportoURL);
			sup.containsText(sup.supportoHomePagePath1, sup.SUPPORTO_HOMEPAGE_PATH1, true);
			sup.containsText(sup.supportoHomePagePath2, sup.SUPPORTO_HOMEPAGE_PATH2, true);
			sup.containsText(sup.supportoHeading, sup.SUPPORTO_HEADING, true);
			//sup.containsText(sup.supportoTitleText, SupportoCambioPotenzaContatore.SUPPORTO_TITLE_TEXT, true);
			logger.write("Clicking on Supporto HEader and verification of data-Completed");
			
			//5
			logger.write("Clicking on contratti e modulistica and verification of menu-Started");
			Thread.sleep(5000);
			sup.clickComponent(sup.contrattiemodulistica);
			sup.verifyComponentExistence(sup.sosLuceeGas);
			sup.containsText(sup.sosLuceeGas, sup.SOS_LUCE_E_GAS, true);
			
			sup.verifyComponentExistence(sup.autolettura);
			sup.containsText(sup.autolettura, sup.AUTOLETTURA, true);
			
			sup.verifyComponentExistence(sup.appEnel);
			sup.containsText(sup.appEnel, sup.APP_ENEL, true);
			
			sup.verifyComponentExistence(sup.gestisciITuoiDati);
			sup.containsText(sup.gestisciITuoiDati, sup.GESTISCI_I_TUOI_DATI, true);
			
			sup.verifyComponentExistence(sup.modificaGagliaAConsumi);
			sup.containsText(sup.modificaGagliaAConsumi, sup.MODIFICA_TAGLIA_A_CONSUMI, true);
			
			sup.verifyComponentExistence(sup.guidaAiServizieTutorial);
			sup.containsText(sup.guidaAiServizieTutorial, sup.GUIDA_AI_SERVIZI_E_TUTORIAL, true);
			
			sup.verifyComponentExistence(sup.serviziSMS);
			sup.containsText(sup.serviziSMS, sup.SERVIZI_SMS, true);
			
			sup.verifyComponentExistence(sup.modulistica);
			sup.containsText(sup.modulistica, sup.MODULISTICA, true);
			
			sup.verifyComponentExistence(sup.subentro);
			sup.containsText(sup.subentro, sup.SUBENTRO, true);
			
			sup.verifyComponentExistence(sup.volutra);
			sup.containsText(sup.volutra, sup.VOLUTRA, true);
			
			sup.verifyComponentExistence(sup.primaAttivazione);
			sup.containsText(sup.primaAttivazione, sup.PRIMA_ATTIVAZIONE, true);
			
			sup.verifyComponentExistence(sup.modificaPotenzaeTensione);
			sup.containsText(sup.modificaPotenzaeTensione, sup.MODIFICA_POTENZA_TENSIONE, true);
			
			sup.verifyComponentExistence(sup.disAttivaZione);
			sup.containsText(sup.disAttivaZione, sup.DISATTIVAZIONE, true);
			
			sup.verifyComponentExistence(sup.caricaDocumenti);
			sup.containsText(sup.caricaDocumenti, sup.CARICA_DOCUMENTI, true);
			
			sup.verifyComponentExistence(sup.pianoSalvaBlackOut);
			sup.containsText(sup.pianoSalvaBlackOut, sup.PIANO_SALVA_BLACK_OUT, true);
			
			sup.verifyComponentExistence(sup.faqAreaclienti);
			sup.containsText(sup.faqAreaclienti, sup.FAQ_AREA_CLIENTI, true);
			
			sup.verifyComponentExistence(sup.assicurazioneClientiFinale);
			sup.containsText(sup.assicurazioneClientiFinale, sup.ASSICURAZIONE_CLIENTI_FINALE, true);
			
			sup.verifyComponentExistence(sup.contattaci);
			sup.containsText(sup.contattaci, sup.CONTATTACI, true);
			
			sup.verifyComponentExistence(sup.conciliazionieRisoluzioneDelleControversie);
			sup.containsText(sup.conciliazionieRisoluzioneDelleControversie, sup.CONCILIAZIONI_E_RESOLUZIONE, true);
			
			sup.verifyComponentExistence(sup.guidaAllaBollettaLuceGas);
			sup.containsText(sup.guidaAllaBollettaLuceGas, sup.GUIDA_ALLA_BOLLETTA_LUCE_E_GAS, true);
			logger.write("Clicking on contratti e modulistica and verification of menu-Completed");
			
			//6
			logger.write("Clicking on modifica Potenza e Tensione and verification of Data-Started");
			sup.clickComponent(sup.modificaPotenzaeTensione);
			
			sup.checkURLAfterRedirection(supportoPotenzaETensioneURL);
			sup.containsText(sup.supportoPotenzoTensionePath, sup.SUPPORTO_POTENZAETENZIONE_PATH, true);
			sup.containsText(sup.PotenzaeTenzioneTitle, sup.POTENZA_E_TENZIONE_TITLE, true);
			sup.containsText(sup.potenzaETenzioneText, sup.POTENZA_E_TENZIONE_TEXT, true);
			
			sup.containsText(sup.potenzaContent1, sup.POTENZA_CONTENT1, true);
			sup.containsText(sup.potenzaContent2, sup.POTENZA_CONTENT2, true);
			sup.containsText(sup.potenzaContent3, sup.POTENZA_CONTENT3, true);
			sup.containsText(sup.potenzaContent4, sup.POTENZA_CONTENT4, true);
			sup.verifyComponentExistence(sup.email);
			sup.verifyComponentExistence(sup.password);
			logger.write("Clicking on modifica Potenza e Tensione and verification of Data-Completed");
			
			logger.write(" Verification of non availability of accedi and VaiallareaClient Button -Started");
			sup.containsText(sup.serviziodisponsibleText, sup.SERVIZIO_DISPONSIBLE_TEXT, true);
			
			sup.isElementNotPresent(sup.VaiallareaclientiButton);
			logger.write(" Verification of non availability of accedi and VaiallareaClient Button -Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}


	}

}
