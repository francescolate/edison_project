package com.nttdata.qa.enel.testqantt;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.lightning.DocumentiUploadComponent;
import com.nttdata.qa.enel.components.lightning.RiepilogoOffertaComponent;
import com.nttdata.qa.enel.components.lightning.VerificaContrattoComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.Utility;
import com.nttdata.qa.enel.util.WebDriverManager;

public class RiepilogoOffertaWebPrivatoPrimaAttivazione {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {

			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		
				SeleniumUtilities util = new SeleniumUtilities(driver);
				VerificaContrattoComponent offer=new VerificaContrattoComponent(driver);
				logger.write("corretta apertura della pagina riepilogo offerta e visualizzazione della sezione Metodo di Pagamento - Start");
				
				TimeUnit.SECONDS.sleep(60);
				offer.clickComponent(offer.accettaCookie);
				TimeUnit.SECONDS.sleep(5);
				offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
			    offer.verifyComponentExistence(offer.riepilogoOffertaLabel);
			    offer.verifyComponentExistence(offer.labelCompletaAttivazione);
			    offer.verifyComponentExistence(offer.pagineProceduraAttivazione);
			    offer.verifyComponentExistence(offer.sezioneDatiPagamento);


				driver.switchTo().defaultContent();
				driver.switchTo().frame("iframeSF");
				
			    offer.clickComponentWithJse(offer.metodoPagamentoBollettinoPostale);
			    logger.write("corretta apertura della pagina riepilogo offerta e visualizzazione della sezione Metodo di Pagamento - Completed");
			  
			    logger.write("click sul pulsante Continua - Start");
			    offer.clickComponent(offer.buttonContinua);
			    TimeUnit.SECONDS.sleep(10);
			    logger.write("click sul pulsante Continua - Completed");
			    
			    logger.write("corretta apertura della pagina ' Seleziona l'indirizzo di fatturazione' con i possibili indirizzi da selezionare e corretta visualizzazione della sezione 'Oppure vuoi comunicarci un nuovo indirizzo di fatturazione?' con i check 'Si' e 'No' - Start");
			    offer.verifyComponentExistence(offer.labelIndirizzoFatt);
			    offer.verifyComponentExistence(offer.radioIndirizziFatturazione);
			    offer.verifyComponentExistence(offer.labelVuoiComunicarci);
			    offer.verifyComponentExistence(offer.checkSi);
			    offer.verifyComponentExistence(offer.checkNO);
			    offer.clickComponentWithJse(offer.checkSi);
			    TimeUnit.SECONDS.sleep(3);
			    offer.sezioneNuovoIndirizzo();
			    offer.clickComponent(offer.radioPrimoIndirizziFatturazione);
			    logger.write("corretta apertura della pagina ' Seleziona l'indirizzo di fatturazione' con i possibili indirizzi da selezionare e corretta visualizzazione della sezione 'Oppure vuoi comunicarci un nuovo indirizzo di fatturazione?' con i check 'Si' e 'No' - Completed");
			    logger.write("click sul pulsante Continua - Start");
			    offer.clickComponent(offer.buttonContinua);
			    TimeUnit.SECONDS.sleep(10);
			    logger.write("click sul pulsante Continua - Completed");
			    
			    logger.write("corretta apertura della pagina ' Indirizzo di residenza' con label 'Sei residente all'indirizzo per il quale stai richiedendo la fornitura?' con check 'Si' e 'No' - Start");
			    offer.verifyComponentExistence(offer.labelIndirizzoResidenza);
			    offer.verifyComponentExistence(offer.labelSeiResidente);
			    offer.verifyComponentExistence(offer.pagine3ProceduraAttivazione);
			    offer.verifyComponentExistence(offer.checkSiSeiResidente);
			    offer.verifyComponentExistence(offer.checkNoSeiResidente);
			    
			    logger.write("corretta apertura della pagina ' Indirizzo di residenza' con label 'Sei residente all'indirizzo per il quale stai richiedendo la fornitura?' con check 'Si' e 'No' - Completed");
			    
			    logger.write("click sul pulsante Continua - Start");
			    offer.clickComponent(offer.buttonContinua);
			    TimeUnit.SECONDS.sleep(10);
			    logger.write("click sul pulsante Continua - Completed");
				
				
				logger.write("corretta apertura della pagina 'Consensi per attività di marketing', selezione dei radio button e click sul pulsante Continua - Start");
				 offer.verifyComponentExistence(offer.labelConsensiAttMarketing);
				 offer.verifyComponentExistence(offer.pagine4ProceduraAttivazione);
				 offer.clickComponentWithJse(offer.checkSiMarketingEnelEnergiaCellulare);
				 TimeUnit.SECONDS.sleep(3);
				 offer.clickComponentWithJse(offer.checkSiMarketingTerziCellulare);
				 TimeUnit.SECONDS.sleep(3);
				 offer.clickComponentWithJse(offer.checkSiMarketingProfilazioneEnergiaCellulare);
				 TimeUnit.SECONDS.sleep(3);
				 offer.clickComponent(offer.buttonContinua);
				 TimeUnit.SECONDS.sleep(10);
				logger.write("corretta apertura della pagina 'Consensi per attività di marketing', selezione dei radio button e click sul pulsante Continua - Completed");
			
				logger.write("corretta apertura della pagina di  'RIEPILOGO' con le sezioni compilate in precedenza e click su Informativa - Start");
				offer.verifyComponentExistence(offer.sezioneRiepilogo);
				offer.verifyComponentExistence(offer.sezioneDatiPagamento);
				offer.verifyComponentExistence(offer.sezioneIndirizzoFatturazione);
			    offer.verifyComponentExistence(offer.sezioneIndirizzoResidenza);
			    offer.verifyComponentExistence(offer.sezioneConsensi);
			    offer.checkTestoInformativa();
			    offer.clickComponentWithJse(offer.informativa);
				logger.write("corretta apertura della pagina di  'RIEPILOGO' con le sezioni compilate in precedenza e click su Informativa- Completed");
				
				logger.write("click su CONFERMA - Start");
				offer.clickComponent(offer.buttonConferma);
				 TimeUnit.SECONDS.sleep(10);
				logger.write("click su CONFERMA - Completed");
				
				logger.write("click su PROSEGUI sulla popup 'Ci siamo quasi' - Start");
				driver.switchTo().defaultContent();
				TimeUnit.SECONDS.sleep(15);
				
				offer.clickComponent(offer.pulsanteProsegui);
				logger.write("click su PROSEGUI sulla popup 'Ci siamo quasi' - Completed");
                TimeUnit.SECONDS.sleep(20);
				
				offer.verifyFrameAvailableAndSwitchToIt(offer.frame);
				
				logger.write("corretta apertura della pagina Compilazione Documenti, check di controllo sul testo presente nella pagina, selezione della voce 'nessun provvedimento o comunicazione perché non richiesti dalla normativa vigente in materia' e della voce 'Locazione/Comodato (Atto già registrato o in corso di registrazione)' - Start");
				  offer.verifyComponentExistence(offer.pageCompilazioneDocumenti);
				  offer.checkTesto(offer.dettaglioCompilazioneDocumenti, offer.testo2);
				  offer.verifyComponentExistence(offer.sezioneDichiarazioneragolaritaUrbanistica);
				  offer.verifyComponentExistence(offer.testo1DichiarazioneragolaritaUrbanistica);
				  offer.verifyComponentExistence(offer.testo2DichiarazioneragolaritaUrbanistica);
				  offer.verifyComponentExistence(offer.testo3DichiarazioneragolaritaUrbanistica);
				  offer.verifyComponentExistence(offer.voceNessunProvvedimento);
				  offer.verifyComponentExistence(offer.titleSezioneDocumenti2);
				  offer.verifyComponentExistence(offer.testo1DichiarazioneRegolarePossesso);
				  offer.verifyComponentExistence(offer.selezioneComodato);
				  offer.clickComponentWithJse(offer.voceNessunProvvedimento);
				  TimeUnit.SECONDS.sleep(3);
				  offer.clickComponentWithJse(offer.selezioneComodato);
				  TimeUnit.SECONDS.sleep(3);
				
				 logger.write("corretta apertura della pagina Compilazione Documenti, check di controllo sul testo presente nella pagina, selezione della voce 'nessun provvedimento o comunicazione perché non richiesti dalla normativa vigente in materia' e della voce 'Locazione/Comodato (Atto già registrato o in corso di registrazione)' - Completed");
				 logger.write("click su Prosegui - Start");
				 offer.clickComponentWithJse(offer.prosegui);
				 TimeUnit.SECONDS.sleep(30);
				 logger.write("click su Prosegui - Completed");	
				
				logger.write("corretto accesso alla pagina di carica documenti - Start");
				
				offer.verifyComponentExistence(offer.riepilogoOffertaLabel);
				TimeUnit.SECONDS.sleep(3);
				offer.checkTesto(offer.dettaglioCompilazioneDocumenti, offer.testo3);
				offer.verifyComponentExistence(offer.titolo1PageInviaDocumentazione);
				offer.verifyComponentExistence(offer.testo2PageInviaDocumentazione);
				offer.verifyComponentExistence(offer.titolo2PageInviaDocumentazione);
				offer.checkTesto(offer.testoSmartPhone, offer.testo4);
				offer.verifyComponentExistence(offer.testo3PageInviaDocumentazione);
				offer.verifyComponentExistence(offer.testo4PageInviaDocumentazione);
				logger.write("corretto accesso alla pagina di carica documenti - Completed");
				DocumentiUploadComponent doc=new DocumentiUploadComponent(driver);
			  
				
				 offer.clickWithJS(offer.buttonCaricaDocumentoRiconoscimento);
				 TimeUnit.SECONDS.sleep(3);
			    offer.verifyComponentExistence(doc.fileUpload3);
			   
			 
			    logger.write("Upload Documento di riconoscimento - Start");
			    TimeUnit.SECONDS.sleep(15);
				doc.uploadGenericFile(
						Utility.getDocumentPdf(prop),doc.fileUpload3
						);
				
				logger.write("Upload Documento di riconoscimento - Completed");
				
				TimeUnit.SECONDS.sleep(15);
				 offer.clickWithJS(offer.buttonCaricaIstanza);
				 TimeUnit.SECONDS.sleep(3);
			    offer.verifyComponentExistence(doc.fileUpload4);
			   
			 
			    logger.write("Upload Documento istanza - Start");
			    TimeUnit.SECONDS.sleep(15);
				doc.uploadGenericFile(
						Utility.getDocumentPdf(prop),doc.fileUpload4
						);
				
				logger.write("Upload Documento istanza - Completed");
				logger.write("Click su pulsante Invia documenti - Start");
				TimeUnit.SECONDS.sleep(15);
				offer.clickWithJS(offer.buttonInviaDocumenti);
				logger.write("Click su pulsante Invia documenti - Completed");
				
				TimeUnit.SECONDS.sleep(15);
				offer.verifyComponentExistence(offer.labelCaricoRichiesta);
				
				driver.switchTo().defaultContent();
			    prop.setProperty("RETURN_VALUE", "OK");
		} catch (Exception e) {
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:" + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");
		}
	

	}

}
