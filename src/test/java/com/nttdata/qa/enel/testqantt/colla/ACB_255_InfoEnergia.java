package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNFornitureComponent;
import com.nttdata.qa.enel.components.colla.BSNServiziComponent;
import com.nttdata.qa.enel.components.colla.HomeComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.components.colla.ServicesComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ACB_255_InfoEnergia {

	public static void main(String[] args) throws Exception {
	Properties prop = null;
	prop = WebDriverManager.getPropertiesIstance(args[0]);
	QANTTLogger logger = new QANTTLogger(prop);

	try {
		RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
		PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
		logger.write("Click on servizi from left menu -- Strat");
		pa.clickComponent(pa.servizi);
		logger.write("Click on servizi from left menu -- Completed");

		BSNServiziComponent bs =new BSNServiziComponent(driver);
		Thread.sleep(10000);
		logger.write("Verify Servii page title and path -- Strat");
		bs.verifyComponentExistence(bs.pageTitle);
		bs.comprareText(bs.pageText, bs.PageText, true);
		bs.comprareText(bs.pagePath, bs.Path, true);
		logger.write("Verify Servii page title and path -- Completed");
		logger.write("Click on info enel energia -- Strat");
		bs.clickComponent(bs.infoEnelEnergia);
		logger.write("Click on info enel energia -- Completed");

		BSNFornitureComponent fc = new BSNFornitureComponent(driver);
		Thread.sleep(10000);
		fc.verifyComponentExistence(fc.pageTitle);
	//	fc.comprareText(fc.pagePath, fc.Path, true);
		fc.comprareText(fc.pageText, fc.PageText, true);
	//	fc.comprareText(fc.pageText1, fc.PageText1, true);
						
		logger.write("Verify header section - Start");
		prop.setProperty("LUCE_E_GAS", "Luce e gas");
		fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("LUCE_E_GAS"));
		prop.setProperty("IMPRESA", "Imprese");
		fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("IMPRESA"));
		//prop.setProperty("INSIEME A TE", "Insieme a te");
		//fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("INSIEME A TE"));
		prop.setProperty("STORIE", "Storie");
		fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("STORIE"));
		prop.setProperty("FUTUR-E", "Futur-e");
		fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("FUTUR-E"));
		prop.setProperty("MEDIA", "Media");
		fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("MEDIA"));
		prop.setProperty("SUPPORTO", "Supporto");
		fc.verifyHeaderVoice(fc.headerVoice,prop.getProperty("SUPPORTO"));
		logger.write("Verify header section - Completed");
		
		logger.write("Verify footer section - Start");
		fc.comprareText(fc.footerText, fc.FooterTextNew, true);
		prop.setProperty("INFORMAZIONI_LEGALI", "Informazioni Legali");
		fc.verifyFooterLink(fc.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
		prop.setProperty("CREDITS", "Credits");
		fc.verifyFooterLink(fc.footerLink, prop.getProperty("CREDITS"));
		prop.setProperty("PRIVACY", "Privacy");
		fc.verifyFooterLink(fc.footerLink, prop.getProperty("PRIVACY"));
		prop.setProperty("CONTATTACI", "Contattaci");
		fc.verifyFooterLink(fc.footerLink, prop.getProperty("CONTATTACI"));
		logger.write("Verify footer section - Completed");
		prop.setProperty("RETURN_VALUE", "OK");
		
	} catch (Throwable e) {
		prop.setProperty("RETURN_VALUE", "KO");

		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		errors.toString();
		logger.write("ERROR_DESCRIPTION: " + errors.toString());

		prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
		if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
			throw e;

	} finally {
		prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
	}

}
}
