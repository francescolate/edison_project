package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.ChangePowerVoltageSectionComponent;
import com.nttdata.qa.enel.components.colla.ContattaciComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModuloReclamiComponent;
import com.nttdata.qa.enel.components.colla.ServiziOnlineCaricaDocumentiComponent;
import com.nttdata.qa.enel.components.colla.SupportFaqComponent;
import com.nttdata.qa.enel.components.colla.SupportPrivacyComponent;
import com.nttdata.qa.enel.components.colla.SupportoSectionComponent;
import com.nttdata.qa.enel.components.colla.TopMenuComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
public class SupportoToContattaciHeaderAndFooter {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
			
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			ModuloReclamiComponent mod = new ModuloReclamiComponent(driver);
			
			logger.write("Verify header section - Start");
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("LUCE_E_GAS"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("IMPRESA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("STORIE"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("FUTUR-E"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("MEDIA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");

			mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
			mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);
			Thread.sleep(5000);
			logger.write("Verify footer section - Start");
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
			logger.write("Verify footer section - Completed");
			
			ServiziOnlineCaricaDocumentiComponent sodc = new ServiziOnlineCaricaDocumentiComponent(driver);
			sodc.comprareText(sodc.text1By, sodc.text1, true);
			sodc.comprareText(sodc.textSub1By, sodc.textSub1, true);
			sodc.comprareText(sodc.text2By, sodc.text2, true);
			sodc.comprareText(sodc.buttonMsg, sodc.buttonText, true);
			
			sodc.clickComponent(sodc.buttonMsg);
			
			logger.write("Check Servizi Online Carica Documenti - START");
			sodc.checkURLAfterRedirection(prop.get("LINK_DOCUMENTI").toString());
			Thread.sleep(5000);
			sodc.verifyItems();
			sodc.comprareText(sodc.informativaTextBy, sodc.informativaText, true);
			logger.write("Check Servizi Online Carica Documenti - END");

			logger.write("Verify header section - Start");
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("LUCE_E_GAS"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("IMPRESA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("STORIE"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("FUTUR-E"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("MEDIA"));
			mod.verifyHeaderVoice(mod.headerVoice, prop.getProperty("SUPPORTO"));
			logger.write("Verify header section - Completed");

			mod.comprareText(mod.enelItalia, mod.EnelItalia, true);
			mod.comprareText(mod.enelEnergia, mod.EnelEnergia, true);
			Thread.sleep(5000);
			logger.write("Verify footer section - Start");
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("INFORMAZIONI_LEGALI"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("CREDITS"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PRIVACY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("COOKIE_POLICY"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("REMIT"));
			mod.verifyFooterLink(mod.footerLink, prop.getProperty("PREFERENZE_COOKIE"));
			logger.write("Verify footer section - Completed");
			
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}

