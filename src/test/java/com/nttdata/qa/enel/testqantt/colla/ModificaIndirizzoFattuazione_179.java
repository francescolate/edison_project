package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.AreaRiservataHomePageComponent;
import com.nttdata.qa.enel.components.colla.LoginLogoutEnelCollaComponent;
import com.nttdata.qa.enel.components.colla.ModificaIndirizzoFattuazioneComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModificaIndirizzoFattuazione_179 {

	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			WebDriver driver = WebDriverManager.getDriverInstance(prop);
			ModificaIndirizzoFattuazioneComponent mif = new ModificaIndirizzoFattuazioneComponent(driver);
			AreaRiservataHomePageComponent home = new AreaRiservataHomePageComponent(driver);
			LoginLogoutEnelCollaComponent log = new LoginLogoutEnelCollaComponent(driver);
			String profileurl = "https://www-coll1.enel.it/it/modifica-profilo";
			String fornitureurl = "https://www-coll1.enel.it/it/area-clienti/imprese/forniture";
			String indirizzoFornituraUrl = "https://www-coll1.enel.it/it/area-clienti/imprese/forniture/indirizzo_fatturazione";
			String dispositivaUrl = "https://www-coll1.enel.it/it/area-clienti/imprese/forniture/indirizzo_fatturazione/dispositiva";
			
			logger.write("Home Page Logo, Title and Description Verification- Starts");
			home.verifyComponentExistence(home.homePageLogo);
			home.comprareText(home.homePageTitle1,prop.getProperty("HOMEPAGE_TITLE1"), true);
			home.comprareText(home.homePageTitle2,prop.getProperty("HOMEPAGE_TITLE2"),true);
			home.comprareText(home.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"), true);
			logger.write("Home Page Log,Title and Description Verification- Ends");
			
			logger.write("Click on the little man at the top of the home page-Starts ");
			Thread.sleep(5000);
			home.clickComponent(home.littlemanHomepage);
			Thread.sleep(5000);
//			home.checkUrl(profileurl);
			home.clickComponent(home.esciButton);
			logger.write("Click on the little man at the top of the home page-Ends ");
			
			logger.write("click su icona utente - Start");
			By icon = log.iconUser;
			log.verifyComponentExistence(icon);
			Thread.sleep(5000);
			log.clickComponent(icon); 
			logger.write("click su icona utente - Completed");
			
			logger.write("Click on little man and enter the username and password to enter into Private Area - Start");
			By pageLogin = log.loginPage;
			log.verifyComponentExistence(pageLogin);
			
			By user = log.username;
			log.verifyComponentExistence(user);
			log.enterLoginParameters(user, prop.getProperty("WP_USERNAME"));
			
			By pw = log.password;
			log.verifyComponentExistence(pw);
			log.enterLoginParameters(pw, prop.getProperty("WP_PASSWORD"));

			By accedi = log.buttonLoginAccedi;
			log.verifyComponentExistence(accedi);
			log.clickComponent(accedi); 
			logger.write("Click on little man and enter the username and password to enter into Private Area - Start");
			
			logger.write("La tua fornitura nel dettaglio click on più informazioni-Starts");
			home.comprareText(home.homepageSectionHeadingBSN, AreaRiservataHomePageComponent.HOMEPAGE_SECTIONHEADING_BSN, true);
			home.clickComponent(home.piuInformazionlink);
			Thread.sleep(5000);
			home.checkUrl(fornitureurl);
			logger.write("La tua fornitura nel dettaglio click on più informazioni-Ends");
			
			logger.write("Click on Modifica From the Il tuo indirizzo di fatturazione Section- Starts");
			home.comprareText(home.ilTuoIndirizzoDiFatturazione, AreaRiservataHomePageComponent.IL_TUO_INDIRIZZO_DI_FATTURAZIONE, true);
			home.clickComponent(home.modificaButton);
			Thread.sleep(5000);
			home.checkUrl(indirizzoFornituraUrl);
			logger.write("Click on Modifica From the Il tuo indirizzo di fatturazione Section- Ends");
			
			logger.write("In the section Il tuo indirizzo di fatturazione, click on Modifica- Starts");
			home.comprareText(home.indirizzoDiFatturazioneFornitura, AreaRiservataHomePageComponent.INDIRIZZO_DI_FATTURAZIONE_FORNITURA, true);
			home.comprareText(home.ilTuoIndirizzoDiFatturazione1, AreaRiservataHomePageComponent.IL_TUO_INDIRIZZO_DI_FATTURAZIONE, true);
			home.clickComponent(home.modificaButton2);
			Thread.sleep(10000);
			home.checkUrl(dispositivaUrl);
			logger.write("In the section Il tuo indirizzo di fatturazione, click on Modifica- Ends");
			
			logger.write("Press the Esci button located at the bottom left and verify the popup - Starts");
			home.clickComponent(home.esciButton2);
			home.comprareText(home.popupHeading, AreaRiservataHomePageComponent.POPUP_HEADING, true);
			home.comprareText(home.popupParagraph, AreaRiservataHomePageComponent.POPUP_PARAGRAPH, true);
			home.verifyComponentExistence(home.tornaAlProcessoButton);
			home.verifyComponentExistence(home.popupEsciButton);
			logger.write("Press the Esci button located at the bottom left and verify the popup - Ends");
			
			logger.write("Click on Torna Al Processo Button and verify the navigation- Starts");
			home.clickComponent(home.tornaAlProcessoButton);
			home.checkUrl(dispositivaUrl);
			logger.write("Click on Torna Al Processo Button and verify the navigation- Ends");
			
			logger.write("Press the Esci button located at the bottom left and verify the popup - Starts");
			home.clickComponent(home.esciButton2);
			home.comprareText(home.popupHeading, AreaRiservataHomePageComponent.POPUP_HEADING, true);
			home.comprareText(home.popupParagraph, AreaRiservataHomePageComponent.POPUP_PARAGRAPH, true);
			home.verifyComponentExistence(home.tornaAlProcessoButton);
			home.verifyComponentExistence(home.popupEsciButton);
			logger.write("Press the Esci button located at the bottom left and verify the popup - Ends");
			
			logger.write("Click on Torna Al Processo Button and verify the navigation- Starts");
			home.clickComponent(home.popupEsciButton);
			Thread.sleep(5000);
			home.verifyComponentExistence(home.homePageLogo);
			home.comprareText(home.homePageTitleDescriptiom,prop.getProperty("HOMEPAGE_TITLE_DESCRIPTION"), true);
			logger.write("Click on Torna Al Processo Button and verify the navigation- Ends");
			
            prop.setProperty("RETURN_VALUE", "OK");
			} 
			catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}

}
