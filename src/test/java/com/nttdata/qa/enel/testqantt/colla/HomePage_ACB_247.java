package com.nttdata.qa.enel.testqantt.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.BSNFornitureComponent;
import com.nttdata.qa.enel.components.colla.BolleteBSNComponent;
import com.nttdata.qa.enel.components.colla.FornitureBSNComponent;
import com.nttdata.qa.enel.components.colla.PrivateArea_Imprese_HomePageBSNComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class HomePage_ACB_247 {
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			PrivateArea_Imprese_HomePageBSNComponent pa = new PrivateArea_Imprese_HomePageBSNComponent(driver);
			pa.verifyComponentExistence(pa.fornitureebollette);
			Thread.sleep(30000);
			//pa.checkForErrorPopup();
			pa.verifyComponentExistence(pa.latuaFornituranelDettaglio);
			logger.write("Verify default supply details -- Start ");
			pa.checkDefaultSupply(pa.defaultSupply);
			logger.write("Verify default supply details -- Completed ");
			pa.verifyComponentExistence(pa.piuInformazioniLink);
			logger.write("Click on piuInformazioniLink and verify page navigation -- Start ");
			pa.clickComponent(pa.piuInformazioniLink);			
			FornitureBSNComponent fc = new FornitureBSNComponent(driver);
			//fc.comprareText(fc.pageTitle, fc.PageTitle, true);
//			fc.comprareText(fc.path, fc.PagePath, true);
			//fc.comprareText(fc.titleSubText, fc.TitleSubText, true);
//			fc.verifyComponentExistence(fc.fornituraLuceClienti);
//			fc.comprareText(fc.modificaNomeFurnitura, fc.ModificaNomeFurnitura, true);
			logger.write("Click on piuInformazioniLink and verify page navigation -- Completed ");

			logger.write("Navigate back and verify le tue bollete section -- Start ");
			driver.navigate().back();
			pa.verifyComponentExistence(pa.letuebollete);
			pa.verifyComponentExistence(pa.numero);
			pa.verifyComponentExistence(pa.importo);
			pa.verifyComponentExistence(pa.stato);
			pa.verifyComponentExistence(pa.scadenza);
			//pa.verifyComponentExistence(pa.bill1);
//			pa.verifyComponentExistence(pa.bill2);
//			pa.verifyComponentExistence(pa.bill3);
			pa.verifyComponentExistence(pa.vaiAllElencoBollette1);
			logger.write("Navigate back and verify le tue bollete section -- Completed ");
			logger.write("Click on vaiAllElencoBollette and verify page navigation,page details -- Start");
			pa.clickComponent(pa.vaiAllElencoBollette1);
			
			BolleteBSNComponent bc = new BolleteBSNComponent(driver);
			bc.verifyComponentExistence(bc.path);
			bc.verifyComponentExistence(bc.pageTitle);
			Thread.sleep(5000);
		//	bc.comprareText(bc.titleSubText, bc.TitleSubText, true);
			/*bc.comprareText(bc.pageText, bc.PageText, true);
			bc.verifyComponentExistence(bc.inviaDocumenti);
	*/		bc.verifyComponentExistence(bc.mostraFiltri);
			bc.verifyComponentExistence(bc.EsportaInExcel);
			logger.write("Click on vaiAllElencoBollette and verify page navigation,page details -- Completed");

			driver.navigate().back();
			logger.write("Verify page navigation to home page -- Start");
			pa.verifyComponentExistence(pa.fornitureebollette);
			pa.verifyComponentExistence(pa.latuaFornituranelDettaglio);
			logger.write("Verify page navigation to home page -- Completed");

			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

		} finally {
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
	}
}
