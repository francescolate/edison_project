package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CambiaProdottoResidenziale;
import com.nttdata.qa.enel.testqantt.CambioUsoGas;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiCambioUso;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.DataConfiguration;
import com.nttdata.qa.enel.testqantt.FinalizzaCambioUso;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaDatiInnestatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyCF_CompleteName_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyNAMECFPOD;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CRM_T_CAMBIO_USO_3 {
	Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
//	Claudio: Scenario di riferimento: ID.1 GAS
	//Lo scenario si occupa di effettuare una modifica del cambio uso da Uso Diverso da Abitazione a Uso Abitativo
	//per una commodity GAS lasciando intatto il metodo di pagamento originario
	//Modalità firma No Vocal con caricamento documenti e validazione
	
	@Before
	public void setUp() throws Exception {
       this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
//		prop.setProperty("QUERY_PROPERTIES", "QUERY_1;QUERY_2");
//		prop.setProperty("QUERY_REFERENCES", "query_202_1;query_202_2");
		prop.setProperty("QUERY_PROPERTIES", "QUERY_1");
		prop.setProperty("QUERY_REFERENCES", "query_202");
		prop.setProperty("NUMERO_DOCUMENTO", "AASjwqje");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("CATEGORIA_CONSUMO", "Uso Cucina");
		prop.setProperty("PROFILO_CONSUMO", "COTTURA PRODUZIONE ACQUA CALDA");
		prop.setProperty("CATEGORIA_MARKETING", "RESIDENZIALE");
		prop.setProperty("ORDINE_GRANDEZZA", "USO CUCINA");
		prop.setProperty("POTENZIALITA", "5000");
		prop.setProperty("UTILIZZO", "CIVILE");
		prop.setProperty("CATEGORIA_USO", "C2");
		prop.setProperty("PRODOTTO", "Speciale Gas 60");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("DELIVERY_CHANNEL", "Email");
		prop.setProperty("CAMBIA_METODO_PAGAMENTO", "N");
		prop.setProperty("EMAIL", "testtest@test.it");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\Doc.pdf");
//		prop.setProperty("PATH_DOCUMENTO", "C:\\Users\\galdieroro\\Downloads\\a.pdf");
		prop.setProperty("CAUSALE_CONTATTO_ATTIVITA","Contratto");
		prop.setProperty("DESCRIZIONE_ATTIVITA","Nuovo Contratto");
		prop.setProperty("SPECIFICA_ATTIVITA","N/A");
		prop.setProperty("STATO_ATTIVITA", "FATTO");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		
		// Dati R2d
				prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
				prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
				prop.setProperty("TIPO_OPERAZIONE", "CAMBIO_USO");
				//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
				// Dati Verifiche pod iniziali
				prop.setProperty("SKIP_POD", "N");
				prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS", "ITALGAS RETI SPA");
				//prop.setProperty("STATO_R2D", "AW");
				// Dati 1OK
				prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS", "Variazione Uso SII");
				// Dati 3OK
				prop.setProperty("INDICE_POD", "1");
				prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
				prop.setProperty("CP_GESTORE", "12345");
				prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Addebito diretto su conto corrente;C.T.E.;Documento di riconoscimento;ML_MdaSingleRes_No_Vocal");
		prop.setProperty("RUN_LOCALLY", "Y");
		return prop;
	}



	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
				
		DataConfiguration.main(args);
		RecuperaDatiInnestatiWorkbench.main(args);
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("CODICE_FISCALE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
//		prop.setProperty("COMPLETE_NAME", prop.getProperty("QUERY_ACCOUNT.NAME"));
////		prop.setProperty("COMMODITY", prop.getProperty("QUERY_ITA_IFM_COMMODITY__C"));
//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
//		prop.store(new FileOutputStream(nomeScenario), null);
		
		SetPropertyCF_CompleteName_POD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		CambioUsoGas.main(args);
		
		CambiaProdottoResidenziale.main(args);
		FinalizzaCambioUso.main(args);
		CaricaEValidaDocumentiCambioUso.main(args);
		VerificheRichiestaDaPod.main(args);
		RecuperaOrderIDDaPod.main(args);

		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);  
		
		LoginR2D_GAS.main(args);
	//	TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_GAS.main(args);
	//	TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_Standard_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);
		
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("STATO_GLOBALE_RICHIESTA", "CHIUSO");
//		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ESPLETATO");
//		prop.setProperty("STATO_RICHIESTA", "ESPLETATO");
//		prop.setProperty("STATO_R2D", "OK");
//		prop.setProperty("STATO_SAP", "OK");
//		prop.setProperty("STATO_SEMPRE", "OK");
//		prop.store(new FileOutputStream(nomeScenario), null);


		//Da eseguire dopo qualche ora
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args); 

	}
	
    @After
 public void fineTest() throws Exception{
         String args[] = {nomeScenario};
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
    }

}

