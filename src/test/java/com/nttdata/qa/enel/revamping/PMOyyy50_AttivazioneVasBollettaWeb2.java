package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoAttivazioneVasBollettaWeb;
import com.nttdata.qa.enel.testqantt.RecuperaClientePerAttivazioneBollettaWeb;
import com.nttdata.qa.enel.testqantt.RecuperaClientePerAttivazioneBollettaWeb2;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaServizioVasBolletta;
import com.nttdata.qa.enel.testqantt.SetPropertyACCOUNTCFNAME;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class PMOyyy50_AttivazioneVasBollettaWeb2 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() {
		//WindowsProcessKiller.killChrome();
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("NUMERO_DOCUMENTO", "AF12345");
		prop.setProperty("SERVIZIO_VAS","Bolletta Web");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("INDIRIZZO_EMAIL","testing.crm.automation@gmail.com"); //sdesantis@key-biz.it
		prop.setProperty("CELLULARE", "3351295559");
		//Solo per esecuzione locale, NO QANTT
		// query spostate nella classe: RecuperaClientePerAttivazioneBollettaWeb2
//		prop.setProperty("QUERY_1", Costanti.query_200_1);
//		prop.setProperty("QUERY_2", Costanti.query_200_2);
//		prop.setProperty("QUERY_3", Costanti.query_200_3);
//		prop.setProperty("QUERY_4", Costanti.query_200_4);
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "RICEVUTO");
		prop.setProperty("STATO_RICHIESTA", "ESPLETATO");
		prop.setProperty("STATO_R2D", "NON PREVISTO");
		prop.setProperty("STATO_SAP", "OK");
		prop.setProperty("STATO_SEMPRE", "NON PREVISTO");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		// dati cliente pre-impostati
//		prop.setProperty("CODICE_FISCALE", "FREROE02B54H501E");
//		prop.setProperty("CODICE_FISCALE", "TMTKMJ86C04F839I");
		prop.setProperty("CODICE_FISCALE", "SLVCIA40B69H501G");
		
//		prop.setProperty("COMPLETE_NAME", "FREEI ORE");
		prop.setProperty("COMPLETE_NAME", "maillorenzo nrtgas");
		 
		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();
	@Test
	@DisplayName("Attivazione VAS - Attivazione Bolletta WEB")
	@Description("Si accede al tab Interazioni per la creazione di una richiesta di Avvio Attivazione VAS per un cliente RES(Business).Utenza accesso:pe")
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);	
/*
		//RecuperaClientePerAttivazioneBollettaWeb.main(args);
		RecuperaClientePerAttivazioneBollettaWeb2.main(args);

		SetPropertyACCOUNTCFNAME.main(args);
		
		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazione.main(args);
		
 		ProcessoAttivazioneVasBollettaWeb.main(args);
		
		SelezionaServizioVasBolletta.main(args);

		// Attendere 5 minuti
		TimeUnit.SECONDS.sleep(300);
*/LoginSalesForce.main(args);		
		VerificheRichiesta.main(args);

	}
	
	@After
	public void tearDown() throws Exception{
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	              }


}
