package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CambiaProdottoNonResidenziale;
import com.nttdata.qa.enel.testqantt.CambioUsoEle;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.FinalizzaCambioUso;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyCF_CompleteName_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetPropertyVariazione_Indirizzo_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetProperty_Stati_Finali_CambioUso;
import com.nttdata.qa.enel.testqantt.SetQueryCambioUso_204_New;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_CambioUso_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CRM_T_CAMBIO_USO_5 {
	Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
      this.prop = conf();
	}
	
	//Lo scenario si occupa di effettuare una modifica del cambio uso da Uso Abitativo a Uso Diverso da Abitazione
	//per una commodity Ele lasciando intatto il metodo di pagamento originario Bollettino Postale
		//Modalità firma Documenti Validi
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		//modifica query 07/10/2020
		prop.setProperty("QUERY", Costanti.query_204_new);
		prop.setProperty("NUMERO_DOCUMENTO", "AASjwqje");
		prop.setProperty("RIGA_DA_ESTRARRE", "2");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
		prop.setProperty("PRODOTTO", "New_Trend Sicuro Energia Impresa Mono");
		prop.setProperty("OPZIONE_KAM", "BASE");		
		prop.setProperty("MODALITA_FIRMA", "Documenti Validi");
		prop.setProperty("DELIVERY_CHANNEL", "Posta");
		prop.setProperty("CAMBIA_METODO_PAGAMENTO", "N");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		
		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
		prop.setProperty("TIPO_OPERAZIONE", "CAMBIO_USO");
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE","Areti");
		//prop.setProperty("STATO_R2D", "AW");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "CAMBIO USO FORNITURA");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE","Esito Ammissibilità - Esito Ammissibilità Richiesta CUF");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esito Cambio Uso Fornitura");
		
		
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}



	@Test
	public void test() throws Exception {

			prop.store(new FileOutputStream(nomeScenario), null);
		Properties prop = new Properties();
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
			SetQueryCambioUso_204_New.main(args);
		RecuperaDatiWorkbench.main(args);
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//			prop.setProperty("CODICE_FISCALE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
//		prop.setProperty("COMPLETE_NAME", prop.getProperty("QUERY_ACCOUNT.NAME"));
//		//prop.setProperty("COMMODITY", prop.getProperty("QUERY_ITA_IFM_COMMODITY__C"));
//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
//		prop.store(new FileOutputStream(nomeScenario), null);
		
		SetPropertyCF_CompleteName_POD.main(args);
		LoginSalesForce.main(args);
		//Thread.currentThread().sleep(60);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		CambioUsoEle.main(args);
		CambiaProdottoNonResidenziale.main(args);
		FinalizzaCambioUso.main(args);
		VerificheRichiestaDaPod.main(args);
		RecuperaOrderIDDaPod.main(args);

		
		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);  
		
		LoginR2D_ELE.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_CambioUso_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("STATO_GLOBALE_RICHIESTA", "CHIUSO");
//		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ESPLETATO");
//		prop.setProperty("STATO_RICHIESTA", "ESPLETATO");
//		prop.setProperty("STATO_R2D", "OK");
//		prop.setProperty("STATO_SAP", "OK");
//		prop.setProperty("STATO_SEMPRE", "OK");
//		prop.store(new FileOutputStream(nomeScenario), null);


		//Da eseguire dopo qualche ora
		SetProperty_Stati_Finali_CambioUso.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args); 

	}
	
    @After
 public void fineTest() throws Exception{
         String args[] = {nomeScenario};
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
    }

}

