package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CambiaProdottoNonResidenziale;
import com.nttdata.qa.enel.testqantt.CambiaProdottoResidenziale;
import com.nttdata.qa.enel.testqantt.CambioUsoGas;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.FinalizzaCambioUso;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyCF_CompleteName_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetQueryCambioUso_203_Items;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheOrdinePostQuery;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CRM_T_CAMBIO_USO_8 {
	Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
       this.prop = conf();
	}

	//Lo scenario si occupa di effettuare una modifica del cambio uso da Uso Abitativo a Uso Diverso da Abitazione
		//per una commodity Ele passando da un RID bancario ad un nuovo RID
			//Modalità firma Documenti Validi
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		//modifica query 07/10/2020
		prop.setProperty("QUERY", Costanti.query_203_items);
		prop.setProperty("NUMERO_DOCUMENTO", "AASjwqje");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
		prop.setProperty("CATEGORIA_CONSUMO", "Ristoranti");
		prop.setProperty("PROFILO_CONSUMO", "RISCALDAMENTO E PRODUZIONE");
		prop.setProperty("CATEGORIA_MARKETING", "RISTORANTI/TRATTORIE");
		prop.setProperty("ORDINE_GRANDEZZA", "COPERTI TRA 50 E 100");
		//prop.setProperty("POTENZIALITA", "5000");
		prop.setProperty("UTILIZZO", "CIVILE");
		prop.setProperty("CATEGORIA_USO", "C3");
		prop.setProperty("PRODOTTO", "New_Soluzione Gas Impresa Business");
		prop.setProperty("OPZIONE_KAM", "CORPORATE SUPER");
		prop.setProperty("MODALITA_FIRMA", "Documenti Validi");
		prop.setProperty("DELIVERY_CHANNEL", "Posta");
		prop.setProperty("CAMBIA_METODO_PAGAMENTO", "Y");
		prop.setProperty("METODO_PAGAMENTO", "RID");
		prop.setProperty("IBAN", "IT69W0306909400100000008461");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("LINK_UDB", "http://dtcmmind-bw-01.risorse.enel:8887/");
		prop.setProperty("ESITO_UDB", "OK");
		
		// Dati R2d
				prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
				prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
				prop.setProperty("PASSWORD_R2D", "FR4$Enel2020");
				//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
				prop.setProperty("TIPO_OPERAZIONE", "CAMBIO_USO");
				// Dati Verifiche pod iniziali
				prop.setProperty("SKIP_POD", "N");
				prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS", "LD RETI S.R.L.");
				//prop.setProperty("STATO_R2D", "AW");
				// Dati 1OK
				prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS", "Variazione Uso SII");
				// Dati 3OK
				prop.setProperty("INDICE_POD", "1");
				prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
				prop.setProperty("CP_GESTORE", "12345");
				prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");

				
		prop.setProperty("RUN_LOCALLY", "Y");
		return prop;
	}



	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		Properties prop = new Properties();
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		SetQueryCambioUso_203_Items.main(args);	
		RecuperaDatiWorkbench.main(args);
		
		
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		
//		//modifica parametri recuperati da query 08/10/2020
//		prop.setProperty("CODICE_FISCALE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
//		prop.setProperty("COMPLETE_NAME", prop.getProperty("QUERY_ACCOUNT.NAME"));
//		//prop.setProperty("COMMODITY", prop.getProperty("QUERY_ITA_IFM_COMMODITY__C"));
//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
//		prop.store(new FileOutputStream(nomeScenario), null);
		
		SetPropertyCF_CompleteName_POD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		CambioUsoGas.main(args);

		CambiaProdottoNonResidenziale.main(args);		
		FinalizzaCambioUso.main(args);
		VerificheRichiestaDaPod.main(args);
		RecuperaOrderIDDaPod.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusUDBCreazioneOrdine.main(args);
		InvioEsitoUBD.main(args);

		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);
		
		LoginR2D_GAS.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_GAS.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_Standard_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);
				
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("STATO_CASE", "CHIUSO");
//		prop.setProperty("SOTTOSTATO_CASE", "RICEVUTO");
//		prop.setProperty("STATO_ORDINE", "ESPLETATO");
//		prop.setProperty("STATO_R2D", "OK");
//		prop.setProperty("STATO_SAP", "OK");
//		prop.setProperty("STATO_SEMPRE", "OK");
//		prop.store(new FileOutputStream(nomeScenario), null);
		
		//Da eseguire dopo qualche ora
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusUDBAttivazPod.main(args);			
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);
		VerificheOrdinePostQuery.main(args);

	}

	
    @After
 public void fineTest() throws Exception{
       /*   String args[] = {nomeScenario};
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);*/
    }

}

