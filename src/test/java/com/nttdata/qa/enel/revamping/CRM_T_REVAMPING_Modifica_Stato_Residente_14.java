package com.nttdata.qa.enel.revamping;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CRM_T_REVAMPING_Modifica_Stato_Residente_14 {
	/*
 		Cliente Residenziale, con almeno due forniture ELE attive con Uso Abitativo e Flag Residente a SI, 
 		su distributore configurato a portale e senza SR in corso sul cliente
	 */
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		//prop.setProperty("QUERY", Costanti.msr_id2); FR 15.07.2021 non più utilizzata 
		prop.setProperty("SCELTA_TIPO_CLIENTE", "Y");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		//STATO RESIDENZA
		prop.setProperty("PROCESSO", "Avvio Modifica Stato Residenza");
		prop.setProperty("STATO_RESIDENZA", "NO");
		prop.getProperty("VERIFICA_POP_UP_MSR","N");
		
		prop.setProperty("MODALITA_FIRMA", "Documenti da firmare");
		prop.setProperty("CANALE_INVIO", "Email");
		prop.setProperty("EMAIL", "test@testtest.it");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESA IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "NON PREVISTO");
		prop.setProperty("RUN_LOCALLY", "Y");
		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("TIPO_OPERAZIONE", "MODIFICA_STATO_RESIDENTE");
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
		prop.setProperty("STATO_R2D", "AW");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "MODIFICA STATO RESIDENTE SII");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Esito Ammissibilità Variazione SII");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito Richiesta Variazione SII");

		//IMPOSTAZIONI MODULO SETTAGGIO CF e POD	
		prop.setProperty("CODICE_FISCALE", "GNIMTN64A01E506I");
		prop.setProperty("POD", "IT012E34908864"); //impostare un pod con stato residenza a Y
	}

	;

	@Test
	public void eseguiTest() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));
		
		/*
 		//R3 - per modifica stato residenza vengono utilizzati dati prodotti da DP
		SetQueryModificaStatoResidente14Query.main(args);
		RecuperaDatiWorkbenchModificaStatoResidenza.main(args);
		SetPropertyModificaStatoResidenzilaleCF.main(args);
			
		SetQueryModificaStatoResidente14Query2.main(args);
		RecuperaDatiWorkbenchModificaStatoResidenza.main(args);
		SetPropertyModificaStatoResidenzilalePOD.main(args);
		RecuperaDatiWorkbenchModificaStatoResidenza.main(args);
		SetPropertyModificaStatoResidenzilalePOD.main(args);
		*/

		//SetPropertyModificaStatoResidenzilalePOD.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID10.main(args);
		ProcessoModificaStatoResidenza.main(args);
		CaricaEValidaDocumenti.main(args);
		VerificheRichiestaDaPod.main(args);
		SetPropertyStatoRD2AW.main(args);

		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args);
		//Lo stato R2D dopo il 10K deve essere AA - R2D_CaricamentoEsiti_3OK_ELE va a leggere la property STATO_R2D che non viene aggiornata dopo il 1OK
		SetPropertyStatoR2DtoAA.main(args);
		R2D_CaricamentoEsiti_3OK_ELE.main(args);
		TimeUnit.SECONDS.sleep(40);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		SetProperty_Stato_Ordine_Espletato_SEMPRE_Non_Previsto.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
	}


	@After
	public void fineTest() throws Exception {
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	}

	;

}
