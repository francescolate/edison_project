package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AnnullaOrdine;
import com.nttdata.qa.enel.testqantt.CambiaProdottoResidenziale;
import com.nttdata.qa.enel.testqantt.CambioUsoGas;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiCambioUso;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.DataConfiguration;
import com.nttdata.qa.enel.testqantt.FinalizzaCambioUso;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaDatiInnestatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyCF_CompleteName_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyCambioUso_Azione_Eseguita_Annullamento;
import com.nttdata.qa.enel.testqantt.SetPropertyCambioUso_Stato_Globale_Richiesta_InLavorazione;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.VerificaRichiestaAnnullamento;
import com.nttdata.qa.enel.testqantt.VerificaStatoConfigurationItem;
import com.nttdata.qa.enel.testqantt.VerificaStatoOfferta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CRM_T_CAMBIO_USO_4 {
	Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
       this.prop = conf();
	}

	//Lo scenario si occupa di effettuare una modifica del cambio uso da Uso Diverso da Abitazione a Uso Abitativo
	//per una commodity GAS lasciando intatto il metodo di pagamento originario
		//Modalità firma No Vocal con caricamento documenti e validazione
	//Arrivati al 3OK su R2D viene avanzata la richiesta di Annullamento integrato su salesforce
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		//prop.setProperty("QUERY_1", Costanti.query_202_1);
		//prop.setProperty("QUERY_2", Costanti.query_202_2);
		prop.setProperty("QUERY_PROPERTIES", "QUERY_1");
		prop.setProperty("QUERY_REFERENCES", "query_202");
		prop.setProperty("NUMERO_DOCUMENTO", "AASjwqje");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("CATEGORIA_CONSUMO", "Uso Cucina");
		prop.setProperty("PROFILO_CONSUMO", "COTTURA PRODUZIONE ACQUA CALDA");
		prop.setProperty("CATEGORIA_MARKETING", "RESIDENZIALE");
		prop.setProperty("ORDINE_GRANDEZZA", "USO CUCINA");
		prop.setProperty("POTENZIALITA", "5000");
		prop.setProperty("UTILIZZO", "CIVILE");
		prop.setProperty("CATEGORIA_USO", "C2");
		prop.setProperty("PRODOTTO", "Speciale Gas 60");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("DELIVERY_CHANNEL", "Email");
		prop.setProperty("CAMBIA_METODO_PAGAMENTO", "N");
		prop.setProperty("EMAIL", "testtest@test.it");
//		prop.setProperty("PATH_DOCUMENTO", "C:\\Users\\galdieroro\\Downloads\\a.pdf");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\Doc.pdf");
		prop.setProperty("CAUSALE_CONTATTO_ATTIVITA","Contratto");
		prop.setProperty("DESCRIZIONE_ATTIVITA","Nuovo Contratto");
		prop.setProperty("SPECIFICA_ATTIVITA","N/A");
		prop.setProperty("STATO_ATTIVITA", "FATTO");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		
		// Dati R2d
				prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
				prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
				//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
				prop.setProperty("TIPO_OPERAZIONE", "CAMBIO_USO");
				// Dati Verifiche pod iniziali
				prop.setProperty("SKIP_POD", "N");
				prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS", "ITALGAS RETI SPA");
			//	prop.setProperty("STATO_R2D", "AW");
				// Dati 1OK
				prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS", "Variazione Uso SII");
				// Dati 3OK
				prop.setProperty("INDICE_POD", "1");
				prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
				prop.setProperty("CP_GESTORE", "12345");
				prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Addebito diretto su conto corrente;C.T.E.;Documento di riconoscimento;ML_MdaSingleRes_No_Vocal");
		prop.setProperty("RUN_LOCALLY", "Y");
		return prop;
	}


	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		Properties prop = new Properties();
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		//aggiunta 02/10/2020
		DataConfiguration.main(args);
		RecuperaDatiInnestatiWorkbench.main(args);
		
//		
//		prop.setProperty("CODICE_FISCALE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
//		prop.setProperty("COMPLETE_NAME", prop.getProperty("QUERY_ACCOUNT.NAME"));
////		prop.setProperty("COMMODITY", prop.getProperty("QUERY_ITA_IFM_COMMODITY__C"));
//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
		
//		prop.store(new FileOutputStream(nomeScenario), null);

		SetPropertyCF_CompleteName_POD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		CambioUsoGas.main(args);  
		CambiaProdottoResidenziale.main(args);
		FinalizzaCambioUso.main(args);
		CaricaEValidaDocumentiCambioUso.main(args);
		VerificheRichiestaDaPod.main(args);
		RecuperaOrderIDDaPod.main(args);

		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);
		
		LoginR2D_GAS.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_GAS.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		
//		in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
//		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
//		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
//		prop.setProperty("STATO_R2D", "INVIATO AL DISTRIBUTORE");
//		prop.setProperty("STATO_SAP", "BOZZA");
//		prop.setProperty("STATO_SEMPRE", "BOZZA");
//		prop.setProperty("RECUPERA_OI_RICHIESTA", "N");
//		
//		prop.store(new FileOutputStream(nomeScenario), null);

		
		SetPropertyCambioUso_Stato_Globale_Richiesta_InLavorazione.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);
	//	VerificheStatoRichiesta.main(args);
		AnnullaOrdine.main(args);
	
		// AGGIUNTO 09/10/2020 IN SEGUITO A MODIFICA DELLA TL
		
//		prop.setProperty("STATO_RICHIESTA", "In attesa");
//		prop.setProperty("STATO_R2D", "INVIATO AL DISTRIBUTORE");
//		prop.setProperty("AZIONE_ESEGUITA", "ANNULLAMENTO");
//		prop.setProperty("STATO_OFFERTA", "Chiusa");
//		prop.setProperty("DETTAGLIO_AZIONE_ESEGUITA", "Annullamento Distributore");
//		prop.store(new FileOutputStream(nomeScenario), null);				
		
		SetPropertyCambioUso_Azione_Eseguita_Annullamento.main(args);
		VerificaStatoOfferta.main(args);
		VerificaRichiestaAnnullamento.main(args);
		VerificaStatoConfigurationItem.main(args);

		

	}
	
    @After
 public void fineTest() throws Exception{
          String args[] = {nomeScenario};
          InputStream in = new FileInputStream(nomeScenario);
          prop.load(in);
          this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
          ReportUtility.reportToServer(this.prop);
    }

}

