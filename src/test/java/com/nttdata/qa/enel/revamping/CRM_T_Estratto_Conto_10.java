package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioEstrattoConto;
import com.nttdata.qa.enel.testqantt.ProcessoEstrattoConto;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerificaStatiRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheAvvioEstrattoConto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

//Corrisponde all'ID2 della TL Estratto Conto - EMAIL	

public class CRM_T_Estratto_Conto_10 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		
		this.prop = conf();
	}



	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "DNNMMM64A17Z330A");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "DNNMMM64A17Z330A");
		prop.setProperty("POD_PDR", "03081000782206"); // 03081000782206 - IT001E51365704
		prop.getProperty("NUMEROFATTURA","012345678");
		prop.setProperty("STATOPAGAMENTICONSUMI", "Aperte");
		prop.setProperty("INSERIMENTODATEFATTURA", "N");
//		prop.setProperty("CANALE_INVIO","Posta");
		prop.setProperty("CANALE_INVIO","Email");
		prop.setProperty("PRESSO", "AUTOMATION");
		prop.setProperty("PROVINCIA", "NAPOLI");
		prop.setProperty("COMUNE", "NAPOLI");
		prop.setProperty("LOCALITA", "CAPODICHINO");
		prop.setProperty("INDIRIZZO", "ORESTE SALOMONE");
		prop.setProperty("CIVICO", "2");
		prop.setProperty("SCALA", "B");
		prop.setProperty("PIANO", "7");
		prop.setProperty("INTERNO", "3");
		prop.setProperty("INDIRIZZO_EMAIL","testing.crm.automation@gmail.com");
		prop.setProperty("MESSAGGIO_ESITO","Gentile cliente la sua richiesta è stata presa in carico");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "RICEVUTO");
		prop.setProperty("DESCRIZIONEATTIVITA","Richiesta Estratto Conto");
		prop.setProperty("STATOATTIVITA","FATTO");
		prop.setProperty("MODELLODOCUMENTO","Plico");
//		prop.setProperty("DESCRIZIONIDOCUMENTICREATI","Plico;Estratto Conto;ML_LettReinvioDoc");
		prop.setProperty("DESCRIZIONIDOCUMENTICREATI","Plico;Estratto Conto;Oggetto Email Generico;Corpo Email Generico");
//		prop.setProperty("NUMERODOCUMENTIATTESI", "3");
		prop.setProperty("NUMERODOCUMENTIATTESI", "4");
		prop.setProperty("STATODOCUMENTO","In Lavorazione");
		prop.setProperty("MODULE_ENABLED","Y");
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
//	@DisplayName("Creazione Estratto Conto con canale invio Posta")
	@DisplayName("Creazione Estratto Conto con canale invio Email")
	@Description("Si accede al tab Interazioni per la creazione di una richiesta di Estratto Conto con canale invio Posta")
	public void test() throws Exception {
		
//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*	
		LoginSalesForce.main(args);

		SbloccaTab.main(args);

		CercaClientePerNuovaInterazione.main(args);

		ProcessoAvvioEstrattoConto.main(args);

		ProcessoEstrattoConto.main(args);

		VerificheAvvioEstrattoConto.main(args);
*/
		}
	
	@After
	public void tearDown() throws Exception{
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	              }

}
