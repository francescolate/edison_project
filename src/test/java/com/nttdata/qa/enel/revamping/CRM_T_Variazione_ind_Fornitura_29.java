package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;

import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSEMPRE;
import com.nttdata.qa.enel.testqantt.CambiaIndirizzoConfermaLavorazione;
import com.nttdata.qa.enel.testqantt.CambiaIndirizzoFornituraConfermaLavorazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CercaFornituraPerCambioIndirizzoFornitura;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID10;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_Forzatura;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_ForzaturaScarti;
import com.nttdata.qa.enel.testqantt.ProcessoVariazioneIndirizzoFornitura;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer_Moge;
import com.nttdata.qa.enel.testqantt.RicercaAttivita_Forzatura;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RilavoraScartiVariazioneIndirizzoFornitura;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SbloccaTab_Forzatura;
import com.nttdata.qa.enel.testqantt.SetPropertyPODCF;
import com.nttdata.qa.enel.testqantt.SetPropertyStatoRD2AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetPropertyVariazione_Indirizzo_Stato_Richiesta_Closed;
import com.nttdata.qa.enel.testqantt.SetPropertyVariazione_Indirizzo_Stato_Richiesta_Gestione_Multipla;
import com.nttdata.qa.enel.testqantt.SetQueryVariazione_Ind_Fornitura_28;
import com.nttdata.qa.enel.testqantt.SetQueryVariazione_Ind_Fornitura_29;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SEMPRE;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CRM_T_Variazione_ind_Fornitura_29 {
	Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		//		this.driver=ManageDriver.startDriver();
		//		// initialize utilities
		//		this.util = new SeleniumUtilities(driver);
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("INDIRIZZO", "VIA ROMA");
		prop.setProperty("PROVINCIA", "VERONA");
		prop.setProperty("CITTA", "VERONA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "DA INVIARE");
		prop.setProperty("STATO_SEMPRE", "DA INVIARE");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("CAP", "00198");
		prop.setProperty("RUN_LOCALLY", "Y");

		//Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("TIPO_OPERAZIONE", "VARIAZIONE_INDIRIZZO_FORNITURA");
		//Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		//		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS","MEGARETI SPA");
		//Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Variazione Indirizzo Fornitura SII");
		//Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("CP_GESTORE", "12345");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");

		return prop;
	}

	@After
	public void fineTest() throws Exception{
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);


		SetQueryVariazione_Ind_Fornitura_29.main(args);
		RecuperaDatiWorkbench.main(args);
		SetPropertyPODCF.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoVariazioneIndirizzoFornitura.main(args);
		Identificazione_Interlocutore_ID10.main(args);
		CercaFornituraPerCambioIndirizzoFornitura.main(args);
		CambiaIndirizzoFornituraConfermaLavorazione.main(args);
		VerificheRichiestaDaPod.main(args);

		SetPropertyStatoRD2AW.main(args);

		LoginR2D_GAS.main(args);
		TimeUnit.SECONDS.sleep(60);
		R2D_VerifichePodIniziali_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_Standard_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);

		RecuperaStatusCase_and_Offer_Moge.main(args);

		//Verifica se un eventuale KO SEMPRE è riprocessabile
		VerificaSeForzabile_KO_SEMPRE.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SEMPRE riprocessabile
		AssegnaAttivitaScartiSEMPRE.main(args);
		LoginSalesForce_ForzaturaScarti.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		RilavoraScartiVariazioneIndirizzoFornitura.main(args);
		//Modulo che verifica lo stato della richiesta
		SetPropertyVariazione_Indirizzo_Stato_Richiesta_Gestione_Multipla.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);

		VerificheRichiestaDaPod.main(args);



	}

}
