package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioEstrattoConto;
import com.nttdata.qa.enel.testqantt.ProcessoEstrattoConto;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.VerificheAvvioEstrattoConto;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;


// Corrisponde all'ID1 della TL Estratto Conto - POSTA	

public class CRM_T_Estratto_Conto_9 {

	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}



	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
//		prop.setProperty("CODICE_FISCALE", "SLSGPL51H13B009N"); //IT001E08617397 
		//OK - 25-05-2021 
		prop.setProperty("CODICE_FISCALE", "BRRFNC65A45G273L"); //OK - 25-05-2021 
		// NVRMRA74H28C933F - pod: IT001E22279680 - 
		// BNLGNN37T21F241W - pod: 01611625000592 - IT001E32704847
	// Verificare il POD associato al cliente altrimenti ricerca fallisce	
		prop.setProperty("POD_PDR", "IT001E17415671");   // 11610000032724 -  IT001E76980680 -  00880000806816 - IT001E08616279
		prop.getProperty("NUMEROFATTURA");
		prop.setProperty("STATOPAGAMENTICONSUMI", "Aperte");
		prop.setProperty("INSERIMENTODATEFATTURA", "N");
		prop.setProperty("STATOPAGAMENTICONSUMI", "Aperte");
		prop.setProperty("INSERIMENTODATEFATTURA", "N");
		prop.setProperty("ESTRATTO_CANONE_TV", "N");
//		prop.setProperty("CANALE_INVIO","Email");
		prop.setProperty("CANALE_INVIO","Posta");
		prop.setProperty("PRESSO", "AUTOMATION");
		prop.setProperty("PROVINCIA", "NAPOLI");
		prop.setProperty("COMUNE", "NAPOLI");
		prop.setProperty("LOCALITA", "CAPODICHINO");
		prop.setProperty("INDIRIZZO", "ORESTE SALOMONE");
		prop.setProperty("CIVICO", "2");
		prop.setProperty("SCALA", "B");
		prop.setProperty("PIANO", "7");
		prop.setProperty("INTERNO", "3");
		prop.setProperty("INDIRIZZO_EMAIL","testing.crm.automation@gmail.com");
		prop.setProperty("MESSAGGIO_ESITO","Gentile cliente la sua richiesta è stata presa in carico");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "CHIUSO");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "RICEVUTO");
		prop.setProperty("DESCRIZIONEATTIVITA","Richiesta Estratto Conto");
		prop.setProperty("STATOATTIVITA","FATTO");
		prop.setProperty("MODELLODOCUMENTO","Plico");
//		prop.setProperty("DESCRIZIONIDOCUMENTICREATI","Plico;Estratto Conto;Oggetto Email Generico;Corpo Email Generico");
		prop.setProperty("DESCRIZIONIDOCUMENTICREATI","Plico;Estratto Conto;ML_LettReinvioDoc");
//		prop.setProperty("NUMERODOCUMENTIATTESI", "4");
		prop.setProperty("NUMERODOCUMENTIATTESI", "3");
		prop.setProperty("STATODOCUMENTO","Inviato");
		prop.setProperty("MODULE_ENABLED","Y");
		prop.setProperty("RUN_LOCALLY", "Y");
		

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*		
		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);

		CercaClientePerNuovaInterazione.main(args);
		
		ProcessoAvvioEstrattoConto.main(args);

		ProcessoEstrattoConto.main(args);
*/
		VerificheAvvioEstrattoConto.main(args);

	}
	
	@After
	public void tearDown() throws Exception{
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	              }


}
