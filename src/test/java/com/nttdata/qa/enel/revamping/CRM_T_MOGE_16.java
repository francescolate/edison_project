package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;

import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSEMPRE;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_Forzatura;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_ForzaturaScarti;
import com.nttdata.qa.enel.testqantt.ModificaAnagraficaCondominio;
import com.nttdata.qa.enel.testqantt.RecuperaDatiInnestatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer_Moge;
import com.nttdata.qa.enel.testqantt.RicercaAttivita_Forzatura;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RilavoraScartiMoge;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SbloccaTab_Forzatura;
import com.nttdata.qa.enel.testqantt.SetPropertyCFRAGIONESOCIALEPODELE;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetPropertyVariazione_Indirizzo_Stato_Richiesta_Closed;
import com.nttdata.qa.enel.testqantt.SetPropertyVariazione_Indirizzo_Stato_Richiesta_Gestione_Multipla;
import com.nttdata.qa.enel.testqantt.SetQueryMOGE_16;
import com.nttdata.qa.enel.testqantt.SetValuePODfromPod_ELE;
import com.nttdata.qa.enel.testqantt.VerificaClienteWorkbenchDaCaseNumber;
import com.nttdata.qa.enel.testqantt.VerificaCrezioneAttivita;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SEMPRE;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;


public class CRM_T_MOGE_16 {
	Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {

		this.prop = conf();
	}

	public static Properties conf() throws Exception {
		Properties prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("SCELTA_TIPO_CLIENTE", "Y");
		prop.setProperty("TIPO_CLIENTE", "Condominio");
//		prop.setProperty("QUERY", Costanti.recupera_cliente_senza_operazioni_in_corso4
//				.replaceAll("@TIPO_CLIENTE@", "Condominio").replaceAll("@FAMIGLIA_POD@", "IT002%A"));

		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("NUMERO_DOCUMENTO", "NAisadu");
		prop.setProperty("MODALITA_ACCETTAZIONE", "Documenti Validi");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESA IN CARICO");
		prop.setProperty("STATO_SAP", "DA INVIARE");
		prop.setProperty("STATO_SEMPRE", "DA INVIARE");
		prop.setProperty("TIPO_ATTIVITA","Processo");
		prop.setProperty("CAUSALE_CONTATTO_ATTIVITA","Gestione Cliente");
		prop.setProperty("DESCRIZIONE_ATTIVITA","Modifica");
		prop.setProperty("SPECIFICA_ATTIVITA","Anagrafica");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("COMMODITY", "ELE");
		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("TIPO_OPERAZIONE", "MOGE");
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "VARIAZIONE ANAGRAFICA SII");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Esito Ammissibilità Variazione SII");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito Richiesta Variazione SII");

		
		
		return prop;
	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@After
	public void tearDown() throws Exception {
	/*	InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);*/
	}

	@Test

	public void test() throws Exception {

	prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
	SetQueryMOGE_16.main(args);
		
		RecuperaDatiInnestatiWorkbench.main(args);

		SetPropertyCFRAGIONESOCIALEPODELE.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		CercaClientePerNuovaInterazione.main(args);
		ModificaAnagraficaCondominio.main(args);
		
		VerificheRichiestaDaPod.main(args);
		VerificaCrezioneAttivita.main(args);
		VerificaClienteWorkbenchDaCaseNumber.main(args);

		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(15);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_Standard_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);

		SetPropertyVSA_Stato_Ordine_Espletato.main(args);

		SetValuePODfromPod_ELE.main(args);
		RecuperaStatusCase_and_Offer_Moge.main(args);
		
		//Verifica se un eventuale KO SEMPRE è riprocessabile
		VerificaSeForzabile_KO_SEMPRE.main(args);
		//I seguenti moduli vengono eseguiti nel caso di un KO SEMPRE riprocessabile
		AssegnaAttivitaScartiSEMPRE.main(args);
		LoginSalesForce_ForzaturaScarti.main(args);
		SbloccaTab_Forzatura.main(args);
		RicercaAttivita_Forzatura.main(args);
		//RilavoraScarti.main(args);
		RilavoraScartiMoge.main(args);

		//Modulo che verifica lo stato della richiesta
		SetPropertyVariazione_Indirizzo_Stato_Richiesta_Gestione_Multipla.main(args);
		
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
				
		VerificheRichiestaDaPod.main(args);

	}

}
