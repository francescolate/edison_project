package com.nttdata.qa.enel.revamping;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaPODPerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CertificaPecBollettaWeb;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoAttivazioneVasBollettaWeb;
import com.nttdata.qa.enel.testqantt.RecuperaClientePerAttivazioneBollettaWeb;
import com.nttdata.qa.enel.testqantt.RecuperaClientePerAttivazioneBollettaWeb2;
import com.nttdata.qa.enel.testqantt.RecuperaDatiInnestatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaServizioVasBolletta;
import com.nttdata.qa.enel.testqantt.SetClienteP;
import com.nttdata.qa.enel.testqantt.SetPropertyACCOUNTCFNAME;
import com.nttdata.qa.enel.testqantt.SetPropertyBollettaPec;
import com.nttdata.qa.enel.testqantt.VerificaRichiestaVas;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.colla.HeaderLogin;
import com.nttdata.qa.enel.testqantt.colla.Login_Public_Area;
import com.nttdata.qa.enel.testqantt.colla.Support;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WindowsProcessKiller;

import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;

public class PMOyyy50_AttivazioneVasBollettaPec2 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() {
		//WindowsProcessKiller.killChrome();
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("NUMERO_DOCUMENTO", "AF12345");
		prop.setProperty("SERVIZIO_VAS","Bolletta PEC");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CELLULARE", "3351295559");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("SOTTOSTATO_RICHIESTA", "INVIATO");
		prop.setProperty("CANALE_INVIO","PEC");
		prop.setProperty("INDIRIZZO_EMAIL","cliente4@pec.enel.it");
		prop.setProperty("INDIRIZZO_PEC","cliente4@pec.enel.it");
		
		prop.setProperty("STATO_R2D", "NON PREVISTO");
		prop.setProperty("STATO_SAP", "DA INVIARE");
		prop.setProperty("STATO_SEMPRE", "NON PREVISTO");
		// query spostate nella classe: RecuperaClientePerAttivazioneBollettaWeb2
//		prop.setProperty("QUERY_1", Costanti.query_200_1);
//		prop.setProperty("QUERY_2", Costanti.query_200_2);
//		prop.setProperty("QUERY_3", Costanti.query_200_3);
//		prop.setProperty("QUERY_4", Costanti.query_200_4);
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("RUN_LOCALLY","Y");
		// se lanciato senza query estrazione dati usare righe sotto
		prop.setProperty("CODICE_FISCALE", "LNDSTL74B02A704P"); // MCCSDY85A70H501X
		prop.setProperty("COMPLETE_NAME", "MACCARELLI SANDY");
//		SPCMHL72B42C543W - SPECIALEGAS MICHELA
//		prop.setProperty("SPLIT_NAME", "MARIAGRAZIA DE ROBERTIS");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	@DisplayName("Attivazione VAS - Attivazione Bolletta PEC")
	@Description("Si accede al tab Interazioni per la creazione di una richiesta di Avvio Attivazione VAS per un cliente RES(Business).Utenza accesso:pe")
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*
		//RecuperaClientePerAttivazioneBollettaWeb.main(args);
		RecuperaClientePerAttivazioneBollettaWeb2.main(args);

		SetPropertyACCOUNTCFNAME.main(args);
	
		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);

		CercaClientePerNuovaInterazione.main(args);	

		ProcessoAttivazioneVasBollettaWeb.main(args);
		
		SelezionaServizioVasBolletta.main(args);
		
		VerificaRichiestaVas.main(args);
		
// *******************  FUORI AMBITO CERTIFICAZIONE PEC ma Funziona 15/07/2021 Claudio**************************************		
		// Click sul LINK nell'email Certificata per chiudere il processo
		SetClienteP.main(args);
		CertificaPecBollettaWeb.main(args);
		
		SetPropertyBollettaPec.main(args);
		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
	
		RicercaRichiesta.main(args);
// ******************* FINE FUORI AMBITO CERTIFICAZIONE PEC **************************************		
*/
		VerificheRichiesta.main(args);

	}
	
	@After
	public void tearDown() throws Exception{
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	              }


}
