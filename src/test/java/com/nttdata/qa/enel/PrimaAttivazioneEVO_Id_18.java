package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaDocumentiSenzaValidazione;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumentiMultiELE;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CheckSezioneContattiConsensi;
import com.nttdata.qa.enel.testqantt.CheckSezioneDelegatoSuRichiesta;
import com.nttdata.qa.enel.testqantt.CheckTouchPoint;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_APPEZZOTTA;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID18;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ModificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2PrimaAttivazione;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Voltura;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SalvaNumeroContratto;
import com.nttdata.qa.enel.testqantt.SalvaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_18 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "MLDFPP24A28M088A");
		prop.setProperty("SALVA_DATI_DELEGATO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("TIPO_DELEGA", "Delega light");
		prop.setProperty("NUMERO_DOCUMENTO", "80380000");
		prop.setProperty("INTERLOCUTORE_NON_TITOLARE", "Y");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "00100");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("RESIDENTE", "NO");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "NO");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("TELEFONO", "3394675542");
		prop.setProperty("CELLULARE", "3394675542");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("STATO_OFFERTA", "Inviata");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("TIPOCLIENTE","RESIDENZIALE");
		prop.setProperty("RECUPERA_OI_RICHIESTA","Y");
		prop.setProperty("RUN_LOCALLY","Y");

	};

	@Test
	public void eseguiTest() throws Exception {
		
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		//CercaClientePerNuovaInterazione.main(args);
		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID18.main(args);
		//Identificazione_Interlocutore_ID15.main(args);
	    SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);
		CompilaDatiFornituraElePrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		CommodityEleResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		AzioniSezioneCarrello.main(args);
		GestioneCVPPrimaAttivazione.main(args);
		CheckSezioneContattiConsensi.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);	
		ModalitaFirmaPrimaAttivazione.main(args);	
		ConfermaPrimaAttivazione.main(args);
		
		VerificheRichiestaDaPod.main(args);
		SalvaNumeroContratto.main(args);
		CaricaDocumentiSenzaValidazione.main(args);
		CheckSezioneDelegatoSuRichiesta.main(args);
	};

	@After
    public void fineTest() throws Exception{
       String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

}
