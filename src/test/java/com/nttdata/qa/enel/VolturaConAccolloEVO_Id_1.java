package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckSezioneClienteUscenteAndInsertPodVolturaConAccollo;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornituraNonAttivabile;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID22;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaConAccollo;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaConAccolloEVO_Id_1 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void inizioTest() throws Exception{

		this.prop = new Properties();
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("CODICE_FISCALE", "RSSPLA61L28E522S");
		prop.setProperty("CODICE_FISCALE_CL_USCENTE", "CRSFNC89M49H769B");
		prop.setProperty("POD", "IT002E5552102A");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("CAUSALE", "Mortis Causa");
		prop.setProperty("PROCESSO", "Avvio Voltura con accollo EVO");
		prop.setProperty("ATTIVABILITA","Non Attivabile");
		prop.setProperty("RUN_LOCALLY","Y");
	}

	@Test
    public void eseguiTest() throws Exception{

		String args[] = {nomeScenario};
		prop.store(new FileOutputStream(nomeScenario), null);
		prop.load(new FileInputStream(nomeScenario));
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID22.main(args);
		SelezioneMercatoVolturaConAccollo.main(args);
		CheckSezioneClienteUscenteAndInsertPodVolturaConAccollo.main(args); 
		CheckSezioneSelezioneFornitura.main(args);
	}
	
	@After
    public void fineTest() throws Exception{
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	}
}
