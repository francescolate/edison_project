package com.nttdata.qa.enel.udb;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;
import io.qameta.allure.Step;

public class InvioEsitoUBD 
{

	public static void main(String[] args) throws Exception {
		
		Properties prop = null;
		//Istanzia oggetto per link a propertiesFile
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);
		try {
	
			//Istanzia oggetto Selenium WebDriver
			WebDriver driver = WebDriverManager.getNewWebDriver(prop);

				LoginSalesforceComponent page = new LoginSalesforceComponent(driver);
				driver.get(prop.getProperty("LINK_UDB"));
				logger.write("Apertura portale UDB");
				driver.findElement(By.xpath("//input[@name='idBpm']")).sendKeys(prop.getProperty("ID_BPM"));
				logger.write("Inserimento Id BPM");
				Select select = new Select(driver.findElement(By.xpath("//select[@name='esito']")));
				select.selectByVisibleText(prop.getProperty("ESITO_UDB"));
				logger.write("Selezione esito");
				driver.findElement(By.xpath("//input[@value='Submit']")).click();
				logger.write("Invio evento");
				TimeUnit.SECONDS.sleep(10);
				String esito=driver.findElement(By.xpath("//ul/li[5]/label")).getText();
				if(!esito.contains("Risposta del server: generato esito: OK per l'idbpm:")){
					throw new Exception("Invio dell'esito non avvenuto correttamente");
				}
				TimeUnit.SECONDS.sleep(5);
		
			prop.setProperty("RETURN_VALUE", "OK");
			
		} catch (Throwable e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: "+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;
			
//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		}
		finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");			
		}
	}
	

	

}
