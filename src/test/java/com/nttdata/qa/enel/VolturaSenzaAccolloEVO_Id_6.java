package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;

import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrelloBusiness;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaBSN;
import com.nttdata.qa.enel.testqantt.CompilaSezioneIndirizziVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ContattiEConsensiVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura3_Finalizzazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.MetodoDiPagamentoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.ModalitàFirmaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyPODCF;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_CF_Uscente_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID6;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID8;
import com.nttdata.qa.enel.testqantt.SezioneCVPVoltura;
import com.nttdata.qa.enel.testqantt.SezioneFatturazioneElettronicaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVOVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_6 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);

		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("CODICE_FISCALE", "80400350585");
		//	prop.setProperty("CODICE_FISCALE_CL_USCENTE", "96069530580");
		//	prop.setProperty("POD", "IT002E3918761A");
		prop.setProperty("NUMERO_DOCUMENTO", "0000");
		prop.setProperty("RILASCIATO_DA", "cccc");
		prop.setProperty("RILASCIATO_IL", "01/01/2020");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Diverso da Abitazione");
		prop.setProperty("CATEGORIA","ALTRI SERVIZI");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");

		prop.setProperty("SCEGLI_METODO_PAGAMENTO","BONIFICO");

		prop.setProperty("FLAG_RESIDENTE","NO");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("CAP", "00198");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");

		prop.setProperty("PRODOTTO","New_Soluzione Energia Impresa_Mono Business");
		prop.setProperty("POPOLA_CIG_CUP","Y");
		prop.setProperty("CUP","CUP358111111101");
		prop.setProperty("CIG", "CIG3581101");
		prop.setProperty("FLAG_136", "SI");
		prop.setProperty("POPOLA_SPLIT_PAYMENT","Y");
		prop.setProperty("CODICE_UFFICIO", "YIEOE4");
		prop.setProperty("PUBBLICA_AMMINISTRAZIONE","Locale");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("OPZIONE_KAM_AGCOR", "BASE");
		prop.setProperty("MODALITA_FIRMA","NO VOCAL");
		prop.setProperty("CANALE","EMAIL");
		prop.setProperty("EMAIL","d.calabresi@reply.it");
		prop.setProperty("EFFETTUARE_CONFERMA","Y");	
		prop.setProperty("STATUS", "OFF");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Documento di riconoscimento");

		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
		prop.setProperty("TIPO_OPERAZIONE", "VOLTURA_SENZA_ACCOLLO");
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE","Areti");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Voltura senza accollo");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE","Esito Ammissibilità - Ammissibilità Voltura VT1 AU");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esito Richiesta Voltura VT1 AU");
		prop.setProperty("CODICE_CAUSALE_5OK_ELE","1234");
		prop.setProperty("RIGA_DA_ESTRARRE","2");			



		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	};

	@Test
	public void eseguiTest() throws Exception{//			prop.store(new FileOutputStream(nomeScenario), null);

		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		SetQueryRecupera_CF_POD_VSA_ID6.main(args);
		RecuperaDatiWorkbench.main(args);
		SetPropertyVSA_CF_Uscente_POD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args); 
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID14.main(args);

		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);
		CheckSezioneSelezioneFornitura.main(args); 

		RiepilogoOffertaVoltura.main(args);

		ConfermaRiepilogoOfferta.main(args);
		SelezioneUsoFornitura.main(args);
		ReferenteSwaEVO.main(args);

		CompilaDatiFornituraVolturaBSN.main(args);
		MetodoDiPagamentoOffertaVoltura.main(args);
		ConfermaScontiBonusEVO.main(args);

		CompilaSezioneIndirizziVolturaSenzaAccollo.main(args);
		AzioniSezioneCarrelloBusiness.main(args);

		CigCupEVO.main(args);
		SplitPaymentEVOVolturaSenzaAccollo.main(args);
		SezioneCVPVoltura.main(args);
		ContattiEConsensiVolturaSenzaAccollo.main(args);

		SezioneFatturazioneElettronicaVolturaSenzaAccollo.main(args);
		ModalitàFirmaVolturaSenzaAccollo.main(args);
		CheckRiepilogoOfferta.main(args); 

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOfferta.main(args); 
		CaricaEValidaDocumenti.main(args);

		//VerificaDocumento.main(args);

		VerificheRichiestaDaPod.main(args);
		VerificheStatoRichiesta.main(args); 
		RecuperaOrderIDDaPod.main(args);

		//RecuperaStatusCase_and_Order.main(args);
		//SalvaIdOrdine.main(args);
		//SalvaIdBPM.main(args);
		//VerificaStatusUDBCreazioneOrdine.main(args);
		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);

		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args); 
		R2D_CaricamentoEsiti_5OK_ELE.main(args); 
		R2D_VerifichePodFinali_ELE.main(args);


		//TODO
		//Da eseguire dopo qualche ora
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);
		LoginSalesForce.main(args);

		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args); 
		//VerificheStatoRichiesta.main(args);


	};

	@After
	public void fineTest() throws Exception{

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
