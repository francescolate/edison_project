package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaCheckListAttivazionSDD;
import com.nttdata.qa.enel.testqantt.CreaNuovoSdd;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID12;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaGestioneSdd;
import com.nttdata.qa.enel.testqantt.ProcessoGestioneSDD;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.AttivazioneSDD_SetPropertiesPerVerificaCase;
import com.nttdata.qa.enel.testqantt.AttivazioneSDD_VerificaStatoCase;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaFornituraAttivazioneSDD;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetCfForGestioneSdd;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamento;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamentoRIDAsset;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

/**
'Profilo: S2S
Cliente: Non Residenziale
Commodity: GAS

Descrizione scenario
Avviare il processo di Attivazione SDD da profilo S2S e verificarne il corretto espletamento e2e.
In fase di valorizzazione IBAN inserire un IBAN italiano.

 */
public class Gestione_Attivazione_SSD_EVO_id3 {

	// RICHIESTA 215659983

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("TIPOLOGIA_CLIENTE", "BUSINESS");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "");
		prop.setProperty("POD", "");

		prop.setProperty("ESITO_COMPATIBILITA", "Attivabile");

		// IBAN
		prop.setProperty("ATT_SDD_IBAN", "IT30P0301503200000000246802");
		prop.setProperty("ATT_SDD_IBAN_ESTERO", "NO");
		prop.setProperty("ATT_SDD_BANCA", "FINECOBANK SPA");
		prop.setProperty("ATT_SDD_AGENZIA", "SEDE DI ROMA");
		prop.setProperty("ATT_SDD_STATO", "SDD");
		// MODALITA FIRMA
		prop.setProperty("ATT_SDD_MOD_FIRMA", "DOCUMENTI VALIDI");
		prop.setProperty("ATT_SDD_CANALE_FIRMA", "STAMPA LOCALE");

		prop.setProperty("ATT_SDD_STATO_ATTESO_CASE", "IN LAVORAZIONE");
		prop.setProperty("ATT_SDD_SOTTOSTATO_ATTESO_CASE", "INVIATO");
		prop.setProperty("ATT_SDD_STATO_ATTESO_SAP", "DA INVIARE");
		prop.setProperty("ATT_SDD_STATO_ATTESO_UDB", "IN ATTESA");

		prop.setProperty("LINK_UDB", "http://dtcmmind-bw-01.risorse.enel:8887");
		prop.setProperty("ESITO_UDB", "OK");

		prop.setProperty("RUN_LOCALLY", "Y");

	};

	@Test
	public void eseguiTest() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		SetCfForGestioneSdd.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoGestioneSDD.main(args);
		Identificazione_Interlocutore_ID12.main(args);
		ConfermaCheckListAttivazionSDD.main(args);
		SelezionaFornituraAttivazioneSDD.main(args);
		CreaNuovoSdd.main(args);
		ModalitaFirmaGestioneSdd.main(args);
		// VERIFICHE POST EMISSIONE
		AttivazioneSDD_VerificaStatoCase.main(args);
		InvioEsitoUBD.main(args);
		AttivazioneSDD_SetPropertiesPerVerificaCase.main(args);
		AttivazioneSDD_VerificaStatoCase.main(args);
		VerificaMetodoPagamento.main(args);
	};

	@After
	public void fineTest() throws Exception {
		 String args[] = {nomeScenario};
		 InputStream in = new FileInputStream(nomeScenario);
		 prop.load(in);
		 this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 ReportUtility.reportToServer(this.prop);
	};

}
