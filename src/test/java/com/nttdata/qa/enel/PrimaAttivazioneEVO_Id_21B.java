package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumentiMultiELE;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CheckSezioneContattiConsensi;
import com.nttdata.qa.enel.testqantt.CheckSezioneDelegatoSuRichiesta;
import com.nttdata.qa.enel.testqantt.CheckTouchPoint;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CommodityMultiEleResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GestioneReferentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_APPEZZOTTA;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID18;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ModificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PagamentoBonificoEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneMULTIEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoMULTIEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2PrimaAttivazione;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Voltura;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaPodMultiNonEsistenti;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaNumeroContratto;
import com.nttdata.qa.enel.testqantt.SalvaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetCaseStatusPostChiusura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetOrderLineItemCommoditytoRetreive;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneQuotePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheOrdinePostQuery;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_21B {
	Properties prop;
	
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_DELEGA","Nessuna delega");
		prop.setProperty("CODICE_FISCALE","DSNSLV98B54H501V");
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "00198");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("RESIDENTE", "NO");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");		
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		//Carrello
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("ELIMINA_VAS", "Y");
		//prop.setProperty("PRODOTTO", "Speciale Luce 60");
		
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("TELEFONO", "3394675542");
		prop.setProperty("COMMODITY_MULTI_ELE","Y");
		prop.setProperty("CELLULARE", "3394675542");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "POSTA");
		prop.setProperty("MODALITA_FIRMA", "VOCAL ORDER");
		prop.setProperty("REGISTRAZIONE_VOCAL","N");
		prop.setProperty("STATO_OFFERTA", "Inviata");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("TIPO_CLIENTE", "Residenziale");
		prop.setProperty("DOCUMENTI_DA_VALIDARE",
				"Documento di riconoscimento;ML_MdaMultiRes_No_Vocal;C.T.E.;ML_MdaMultiRes_No_Vocal");

		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
		prop.setProperty("STATO_R2D", "AW");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
		prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
		prop.setProperty("POTENZA_FRANCHIGIA", "220");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");
		prop.setProperty("TIPOCLIENTE","RESIDENZIALE");
		prop.setProperty("RUN_LOCALLY","Y");
       

	};

	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
			
		RecuperaPodMultiNonEsistenti.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazione.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID17.main(args); 
		SelezioneMercatoPrimaAttivazioneEVO.main(args);		
		ProcessoPrimaAttivazioneMULTIEVOResidenziale.main(args);		
		VerificaCreazioneOffertaPrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		SetPODtoRetreiveELE_Multi_Pod1.main(args);	
		CommodityMultiEleResidenzialePrimaAttivazione.main(args);	
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoElettricoResidenziale.main(args);
		GestioneCVPPrimaAttivazione.main(args);	
		CheckSezioneContattiConsensi.main(args);	
		ConsensiEContattiPrimaAttivazione.main(args);	
		ModalitaFirmaPrimaAttivazione.main(args);			
		ConfermaPrimaAttivazione.main(args);	
		SalvaNumeroContratto.main(args);
		//CaricaVerificaValidaDocumentiMultiELE.main(args);
		CaricaEValidaDocumenti.main(args);
		//Recupera stato Offerta
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args);
			
		Thread.currentThread().sleep(18000); 
		//Esitazione 5-OK POD_2 ELE
		//Recupero order da POD 1
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		SalvaIdOrdine.main(args);	
		// Set primo ordine in chiave ricerca OI_RICERCA
		SetOIRicercaDaOIOrdine.main(args);
		//Set POD_1 in POD
		SetPODtoRetreiveELE_Multi_Pod1.main(args);
		//R2D	
		LoginR2D_ELE.main(args);	
		R2D_VerifichePodIniziali_ELE.main(args);	
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		
		Thread.currentThread().sleep(5000); 
		
		//Esitazione 5-OK POD_2 ELE
		//Recupero order da POD 2
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		SalvaIdOrdine.main(args);
		// Set secondo ordine in chiave ricerca OI_RICERCA
		SetOIRicercaDaOIOrdine.main(args);
		//Set POD_2 in POD
		SetPODtoRetreiveELE_Multi_Pod2.main(args);
		//R2D	
		LoginR2D_ELE.main(args);	
		R2D_VerifichePodIniziali_ELE.main(args);	
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
			
		//Verifiche post esiti R2D
	
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);
		
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);

		
		//Lanciare dopo 4 ore
		
		//Verifiche finali 
		SetPODtoRetreiveELE_Multi_Pod1.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		VerificheAttivazioneRichiesta.main(args);
		
		Thread.currentThread().sleep(3000); 
			
		SetPODtoRetreiveELE_Multi_Pod2.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		VerificheAttivazioneRichiesta.main(args);
			
	};

	@After
	public void fineTest() throws Exception {
		String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
       ReportUtility.reportToServer(this.prop);
	};

}
