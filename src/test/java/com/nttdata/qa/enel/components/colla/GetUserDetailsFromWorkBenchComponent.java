package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GetUserDetailsFromWorkBenchComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By addressField = By.xpath("//*[@id='query_results']/tbody/tr[5]/td[6]");
	public By cf = By.xpath("//*[@id='query_results']/tbody/tr[5]/td[8]");
	public By firstName = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	public By lastName = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	
	public GetUserDetailsFromWorkBenchComponent (WebDriver driver) throws Exception{
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
}
