package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CheckListComponentEVO {
    WebDriver driver;
    SeleniumUtilities util;

    public final By builderChecklistOkButton = By.xpath("//div[@id='modalHeaderChecklist']"
            + "/div[@id='checklistFooter']" + "/button[text()='Conferma']");


    //	final By swaChekclistOkButton = By.xpath("//button[text()='OK' and contains(@onclick,'close')]");
    //Claudio provo con bottone Conferma
//	final By swaChekclistOkButton = By.xpath("//button[text()='Conferma' and contains(@onclick,'close')]");
    final By swaChekclistOkButton = By.xpath("//*[@id='brandBand_7']/div/div[3]/*[@id='modalChecklist']/section/*[@id='modalHeaderChecklist']/div[@id='checklistFooter']/button[text()='Conferma']");
    final By attendiChkList = By.xpath("//h2[contains(text(),'Checklist')]");


    public CheckListComponentEVO(WebDriver driver) {
        this.driver = driver;
        this.util = new SeleniumUtilities(this.driver);
        PageFactory.initElements(driver, this);
    }

    // @FindBy(xpath="//iframe[@title = 'accessibility title'")
    List<WebElement> possibleFrames;

    public String clickFrameConferma2() throws Exception {
        String frame = util.getFrameByIndex(1);
        util.frameManager(frame, builderChecklistOkButton, util.scrollAndClick);
        return frame;
    }


    public void checkChecklistText(String text) throws Exception {
        if (util.verifyExistence(By.xpath("//div[contains(@id,'modalBody')]"), 30)) {
            Thread.currentThread().sleep(10000);
            String checklistText = util.getElementText(By.xpath("//div[contains(@id,'modalBody')]"));
            String checkListActual = checklistText.replace("\r\n", "").replace("\r", "");
            if (!checkListActual.contains(text))
                throw new Exception("Il testo della checklist non risulta quello atteso. Fare riferimento alla documentazione.");
        } else throw new Exception("Impossibile trovare la Checklist. Verificare.");
    }

    public void clickConferma() throws Exception {
//    	Thread.sleep(30000);
//        if (util.exists(By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma'] | //section[@aria-expanded='true']//div[@role='dialog']//button[text()='Conferma'] | //section[@data-id='checklistModal']//button[text()='Conferma'] | //button[@name='Confirm']"), 120))
//            util.objectManager(By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma'] | //section[@aria-expanded='true']//div[@role='dialog']//button[text()='Conferma'] | //section[@data-id='checklistModal']//button[text()='Conferma'] | //button[@name='Confirm']"), util.scrollAndClick);
//        if (util.exists(By.xpath("//button[@name='checklistConferma']"), 90))
//            util.objectManager(By.xpath("//button[@name='checklistConferma']"), util.scrollAndClick);
    	Thread.sleep(10000);
        if (util.exists(By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma'] | //section[@aria-expanded='true']//div[@role='dialog']//button[text()='Conferma'] | //section[@data-id='checklistModal']//button[text()='Conferma'] | //button[@name='Confirm']"), 120)) {
            util.objectManager(By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma'] | //section[@aria-expanded='true']//div[@role='dialog']//button[text()='Conferma'] | //section[@data-id='checklistModal']//button[text()='Conferma'] | //button[@name='Confirm']"), util.scrollAndClick);
    	} else {
        	if (util.exists(By.xpath("//button[@name='checklistConferma']"), 90))
        		util.objectManager(By.xpath("//button[@name='checklistConferma']"), util.scrollAndClick);
        }
    }


    public String clickOk() throws Exception {
//		String frame = util.getFrameByIndex(1); //con 1 va in errore provo con 0
        String frame = util.getFrameByIndex(0);
        util.frameManager(frame, swaChekclistOkButton, util.scrollAndClick);
        return frame;

    }

    public String clickConfermaEVO() throws Exception {
//		String frame = util.getFrameByIndex(1); //con 1 va in errore provo con 0
        int sizeFrame;
        sizeFrame = driver.findElements(By.tagName("iframe")).size();
        //driver.switchTo().frame(0);
        String frame = util.getFrameByIndex(0);
        driver.switchTo().frame(1);
        String frame1 = util.getFrameByIndex(1);
        util.frameManager(frame1, swaChekclistOkButton, util.scrollAndClick);
        return frame;

    }

    public void clickConferma(By xpathConferma, int idxFrame) throws Exception {
        String frame = util.getFrameByIndex(idxFrame);

        WebElement frameToSw = driver.findElement(
                By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']")
        );
        driver.switchTo().frame(frameToSw);
        clickConferma(xpathConferma);
        driver.switchTo().defaultContent();
    }

    public void clickConferma(By xpathConferma) throws Exception {
        util.waitUntilIsDisplayed(xpathConferma);
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(xpathConferma).click();
        TimeUnit.SECONDS.sleep(5);
    }

    public void attendiChecklist() throws Exception {

        WebElement element = util.waitAndGetElement(attendiChkList, 200, 5);

    }

}
