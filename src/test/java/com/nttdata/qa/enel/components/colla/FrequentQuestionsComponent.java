package com.nttdata.qa.enel.components.colla;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class FrequentQuestionsComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	
	public FrequentQuestionsComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public By headerPathLinks = By.xpath("//ul[@class='breadcrumbs component']/li/a");
	public By headerTitle = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_title') and text()='Domande frequenti']");
	public By headerTitleContatti = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_title') and text()='Entra in contatto con noi']");
	public By headerTitleLeggereBolletta = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_title') and text()='Come leggere la bolletta luce Enel Energia']");
	public By headerSubTitleContatti = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_detail') and text()='Scopri tutti i modi per comunicare con Enel.']");
	public By showFilterButton = By.xpath("//form[@id='filter']//button[text()='Mostra filtri']");
	public By hideFilterButton = By.xpath("//form[@id='filter']//button[text()='Nascondi filtri']");

	public By orderFilterSelected = By.xpath("//form[@id='filter']//label[text()='Ordina per:']/..//select/option[@selected and text()='Più recente']");
	public By orderFilter = By.xpath("//form[@id='filter']//label[text()='Ordina per:']/..//select");
	public String pathLinks1 [] = {"HOME","SUPPORTO","DOMANDE FREQUENTI"};
	public String pathLinks2 [] = {"HOME","ENTRA IN CONTATTO CON NOI"};
	public String pathLinks3 [] = {"HOME","SUPPORTO","COME LEGGERE LA BOLLETTA DELLA LUCE ENEL ENERGIA"}; 
	public String question1 [] = {"INFO UTILI-Confronta le offerte gas di Enel Energia","INFO UTILI-Come risparmiare sull’illuminazione di casa","SERVIZI-Tutto quello che devi sapere su Area Clienti e Profilo Unico","INFO UTILI-Costo energia elettrica kWh: il prezzo dell’elettricità in Italia","INFO UTILI-Con enelpremia WOW! vinci Hollywood","INFO UTILI-La storia della nuova Area Clienti di Enel Energia","INFO UTILI-Lo Spazio delle Risposte: i tutorial di Enel Energia"};
    public String question4 [] = {"INFO UTILI-Vivete il Giro d'Italia insieme a noi con le vostre storie","SERVIZI-Il senso di Enel Energia per il social caring","INFO UTILI-Brand Loyalty Awards, il successo di enelpremia 3.0: secondo posto al Grand Prix e il Premio Gamification","OFFERTE-Le nostre offerte per le piccole imprese","INFO UTILI-La rete vendita Enel Energia per il business: competenza e territorio","SERVIZI-Enel Energia per le aziende: un canale per ogni esigenza","INFO UTILI-Enel Energia arriva su LinkedIn"};
	public String question5 [] = {"SERVIZI-Cambio contatori gas: cosa sono e come funzionano","SERVIZI-Enel Energia più vicina alla tua impresa con la nuova Area Clienti Business","SERVIZI-Nuova app Enel Energia: entra in un mondo di vantaggi","INFO UTILI-Tariffa monoraria e bioraria, che differenza c’è?","INFO UTILI-Che cosa sono le fasce orarie di consumo dell’energia elettrica","INFO UTILI-Ecco come funziona il bonus luce e gas 2020","INFO UTILI-Offerte luce per la seconda casa"};
    public String question6 [] = {"INFO UTILI-Live Chat Enel Energia, scopri come richiedere assistenza immediata","BOLLETTA-Condizioni generali di fornitura per clienti residenziali","INFO UTILI-Come leggere la bolletta luce Enel Energia","INFO UTILI-Come calcolare la bolletta del gas"};
	public String question7 [] = {"INFO UTILI-Come leggere la bolletta gas Enel Energia","INFO UTILI-Bollette energia elettrica e gas: cambiano i tempi di prescrizione","INFO UTILI-Privacy","-Maggiori informazioni sul tuo POD/PDR"};
    //public String questionOrderedByRelevance [] = {"INFO UTILI-Open Energy: l’offerta per le imprese trasparente, facile e chiara","SERVIZI-Cambio contatori gas: cosa sono e come funzionano","INFO UTILI-Vivete il Giro d'Italia insieme a noi con le vostre storie","INFO UTILI-ufirst, l’app salva coda per gli Spazio Enel","SERVIZI-Tutto quello che devi sapere su Area Clienti e Profilo Unico","INFO UTILI-Tariffa monoraria e bioraria, che differenza c’è?","NORMATIVA-PrivacyRiders of icarus"};
    public String questionOrderedByRelevance [] = {"SERVIZI-Cambio contatori gas: cosa sono e come funzionano","INFO UTILI-Vivete il Giro d'Italia insieme a noi con le vostre storie","INFO UTILI-ufirst, l’app salva coda per gli Spazio Enel","SERVIZI-Tutto quello che devi sapere su Area Clienti e Profilo Unico","INFO UTILI-Tariffa monoraria e bioraria, che differenza c’è?"};

    public String questionOrderedLessRecent [] = {"-Maggiori informazioni sul tuo POD/PDR","NORMATIVA-Privacy","INFO UTILI-Bollette energia elettrica e gas: cambiano i tempi di prescrizione"};
    public String questionOfferteFilter [] =    {"CONTATORI-Come risparmiare sulla bolletta con ORE FREE NOW","OFFERTE-Le nostre offerte per le piccole imprese"};
    //public String questionContrattoFilter [] =    {"INFO UTILI-Confronta le offerte gas di Enel Energia"};
    public String questionContrattoFilter [] =    {"CONTRATTO-Come fare una voltura"};
    public String questionBollettaContatoriFilters [] = {"BOLLETTA-Condizioni generali di fornitura per clienti residenziali","CONTATORI-Come risparmiare sulla bolletta con ORE FREE NOW", "BOLLETTA-Guida alla bolletta"};
    public By disabledprevpage = By.xpath("//div[@id='page_navigation']//li[@role='button' and @class='disabled']/span[text()='Prev']");
    public By disablednextpage = By.xpath("//div[@id='page_navigation']//li[@role='button' and @class='disabled']/span[text()='Next']");
    public By prevpage = By.xpath("//div[@role='navigation' and @data-pagination='simplerow']//li[@role='button']//a[@class='page-link prev']");
    public By nextpage = By.xpath("//div[@role='navigation' and @data-pagination='simplerow']//li[@role='button']//a[@class='page-link next']");
	public String [] groupFilters1 = {"Contratto"};
	public String [] groupFilters2 = {"Bolletta","Spese-contrattuali","Servizi","Contatori","Pagamento","Offerte","Normativa","Modulistica","Strumenti"};
	public String [] groupFilters3 = {"Ultima settimana","Ultimo mese","Ultimo anno"};
	public String [] groupFilters4 = {"Da","A"};
	public By annullaFilterButton = By.xpath("//button[text()='Annulla']");
	public By applicaFilterButton = By.xpath("//button[text()='Applica']");
	public String filterGroup1 = "//form[@id='filter']//div[@class='filter__groups']//h3[@class='filter__group__heading' and text()='#']/..//label";
	public String filterGroup2 = "//form[@id='filter']//div[@class='filter__groups']//h3[@class='filter__group__heading']//label[text()='#']/../..//div[@class='labels']/label";
    public String genericQuestion = "//ul[@class='resultList']/li[@style='display: block;']/a/p[text()='#']";
    public By haiBisognodiAiutoLabel = By.xpath("//h2[text()='Hai bisogno di aiuto?']");
    public By contattaciButton = By.xpath("//a[contains(text(),'CONTATTACI')]");
    
    
    
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
//		TimeUnit.SECONDS.sleep(5);
		util.objectManager(clickableObject, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		
	}
	
	
	public void checkFilters(String xpath,String section, String [] filters) throws Exception{

		List<WebElement> el = util.waitAndGetElements(By.xpath(xpath.replace("#", section)));
		if(el.size()==0) throw new Exception("Nella sezione filtri delle domande frequenti non e' stata riscontrata la sezione "+section);
		this.checkFilters(el, filters);
	}	
	
	private void checkFilters_OLD(List<WebElement> elements,String [] checkfilters) throws Exception{
		for(int x = 0;x<checkfilters.length;x++) {
			if(x>elements.size()) throw new Exception("Nella sezione filtri delle domande frequenti non risulta presente il filtro "+checkfilters[x]);
			if(!elements.get(x).getAttribute("innerText").equalsIgnoreCase(checkfilters[x])) throw new Exception("Nella sezione filtri delle domande, il filtro "+checkfilters[x]+" non è presente oppure non si trova nella corretta sequenza "+checkfilters.toString());
		}
	}
	private void checkFilters(List<WebElement> elements,String [] checkfilters) throws Exception{
		for(int x = 0;x<checkfilters.length;x++) {
			if(x>elements.size()) throw new Exception("Nella sezione filtri delle domande frequenti non risulta presente il filtro "+checkfilters[x]);
			boolean trovato = false;
			for(int y = 0;y<elements.size();y++) {
				if(elements.get(y).getAttribute("innerText").equalsIgnoreCase(checkfilters[x]))
					trovato = true;
			}
			if (!trovato)
			{
				 throw new Exception("Nella sezione filtri delle domande, il filtro "+checkfilters[x]+" non è presente oppure non si trova nella corretta sequenza "+checkfilters.toString());
			}
		}
	}
	
	public void clickFilter(String xpath,String section, String filter) throws Exception{
		
		List<WebElement> el = util.waitAndGetElements(By.xpath(xpath.replace("#", section)));
		if(el.size()==0) throw new Exception("Nella sezione filtri delle domande frequenti non e' stata riscontrata la sezione "+section);
		for(WebElement e : el) {
			if(e.getAttribute("innerText").equals(filter)) e.click();
		}
	}	
	
	
	public void changePage(String page) throws Exception{
		util.objectManager(By.xpath("//div[@role='navigation' and @data-pagination='simplerow']//li[@role='button']//a[@href='#page-"+page+"']"), util.scrollAndClick);
		
	}
	
	public boolean searchQuestionInPage(By object) throws Exception{
		boolean found = false;
		while(!found) {
			if(util.verifyExistence(object, 2)) return true;
			if(util.verifyExistence(nextpage, 2)) util.objectManager(nextpage, util.click);
			else break;
			
		}
		return found;
	}
	
	public void selectFilter(String filtro) throws Exception{
		util.objectManager(orderFilter, util.select,filtro);
		TimeUnit.SECONDS.sleep(3);
	}
	
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
		if(!colore.contentEquals(color)) throw new Exception("Il colore dell'oggetto "+objectName+" non risulta essere "+color+". Colore trovato : "+colore);
	}
	
//	public void checkQuestions(String [] questions) throws Exception {
//		List<WebElement> q = util.waitAndGetElements(By.xpath("//ul[@class='resultList']/li[@style='display: block;']/a"));
//		
//		for(int x = 0;x<questions.length;x++) {
//			if(x<=q.size()) {
//				String type = questions[x].split("-")[0];
//				String quest = questions[x].split("-")[1];
//				
//				if(type.length()>0) {
//					String span = util.waitAndGetElement(q.get(x),By.xpath("./span")).getAttribute("innerText");
//					if(!span.toLowerCase().contentEquals(type.toLowerCase())) throw new Exception("Nella pagina Domande Frequenti non risulta presente la domanda "+questions[x]+" nell'ordine atteso alla "+x+" posizione");
//				}
//				String p = util.waitAndGetElement(q.get(x),By.xpath("./p")).getAttribute("innerText");
//				if(!quest.toLowerCase().contentEquals(quest.toLowerCase())) throw new Exception("Nella pagina Domande Frequenti, per la tipologia "+type+" impossibile trovare la domanda "+quest);
//
//				
//				
//				
//			}
//			else throw new Exception("Nella pagina Domande Frequenti impossibile trovare la domanda "+questions[x]);
//			
//		}
//	}
	
	public void checkQuestions(String [] questions) throws Exception {
		//Prelevo tutti i blocchi di domande presenti in pagina comprensive di titolo intestazione e descrizione
		List<WebElement> q = util.waitAndGetElements(By.xpath("//ul[@class='resultList']/li/a"));
		
		//Creo una mappa di domande
		Map<String,String> mappaDomande = new HashMap<String,String>();
		String span,p;
		for(WebElement li : q) {
			//Se le domande hanno una intestazione SPAN la salvo, altrimenti stringa vuota
			List<WebElement> spans = util.waitAndGetElements(li,By.xpath("./span"));
			if(spans.size()>0) span = spans.get(0).getAttribute("innerText");
			else span = "";
			//Salvo dettaglio descrizione della domanda
			p = util.waitAndGetElement(li,By.xpath("./p"),true).getAttribute("innerText");
			mappaDomande.put(p,span);
		}
		
		//Verifico la presenza delle domande in input a partire dalla sua descrizione, per poi controllarne anche l'eventuale intestazione
		for(String question : questions) {
			String type = question.split("-")[0];
			String quest = question.split("-")[1];
			if(mappaDomande.containsKey(quest)) {
				String intestazione = mappaDomande.get(quest);
				if(!intestazione.toLowerCase().contentEquals(type.toLowerCase())) throw new Exception("E' presente la domanda : "+quest+" ma la sua intestazione e' diversa da "+type);
				//else//System.out.println(question +" OK!");
			}
			else throw new Exception("Non risulta presente la domanda "+question);
		}
	
	}
	
	public void checkNumOfQuestions(int num) throws Exception {
		List<WebElement> q = util.waitAndGetElements(By.xpath("//ul[@class='resultList']/li[@style='display: block;']/a"),10,2);
	    if(q.size()!=num) throw new Exception("Nella sezione Domande Frequenti erano attese "+num+ "domande mentre ne sono state riscontrate "+q.size());
	}
	
	
	public void checkHeaderPathLinks(String links []) throws Exception{
		
		List<WebElement> webLinks = util.waitAndGetElements(headerPathLinks);
		
		for(int x = 0;x<=links.length-1;x++) {
			if(!webLinks.get(x).getAttribute("innerText").trim().toLowerCase().contains(links[x].trim().toLowerCase())) throw new Exception("In Domande Frequenti Header section, the path does not contains link "+links[x]);
		}
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("L'oggetto identificato dal seguente xpath " + existingObject + " non esiste.");
	}
	
	
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	
}
