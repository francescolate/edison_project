package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OffersGasComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By titoliListaOfferte=By.xpath("//div[@class='plan-promo_inner container plan-promo_custom plan-promo-wrapper manage-block']//div[@class='plan-promo_plan-holder']//li//h3");
	public By labelGasPerLeImprese=By.xpath("//div[@class='plan-promo_content' and @id='promo-offert_results']//span[@class='eyebrow' and text()='Gas per le imprese']");
	public By titolotesto=By.xpath("//div[@class='plan-promo_content' and @id='promo-offert_results']//h2[@class='title' and text()='Diamo gas al tuo business']");
	public By sottotitolotesto=By.xpath("//div[@class='plan-promo_content' and @id='promo-offert_results']//p[@class='description' and text()='Scegli tra le soluzioni gas di Enel Energia.  La sicurezza delle nostre offerte e tanti vantaggi per la tua attività.  Vai a tutto gas con la tua impresa!']");
	public By linkVedituttegas=By.xpath("//a[@href='https://www-coll1.enel.it/it/luce-e-gas/gas/negozio-ufficio?search=non_specificato&sortType=undefined']//span[text()=' Vedi tutte gas']");
	public By frecciaLink=By.xpath("//a[@href='https://www-coll1.enel.it/it/luce-e-gas/gas/negozio-ufficio?search=non_specificato&sortType=undefined']//span[@class='ico']");
	public By dettaglioOfferta=By.xpath("//li[@class='offert-item']//div[@class='content-dxBottom-newLayoutCard']//button[@type='button' and normalize-space(text())='Dettaglio Offerta']");
	public By listaAttivaOra=By.xpath("//li[@class='offert-item']//div[@class='pricing-details']//a[@href='/it/adesione' and normalize-space(text())='Attiva ora']");
	public String linkLUCE_E_GAS_CON_FILTRO="it/luce-e-gas/gas/negozio-ufficio?nec=Subentro";
	
	public OffersGasComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto con xpath " + existentObject + " non esiste.");
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void verifyOfferList (By listOffer)throws Exception {
		String description="";
		String offersListfound="";
		
		  List <WebElement> list = driver.findElements(listOffer);
		   
			for (int i = 1; i <= list.size(); i++) {
			 description = driver.findElement(By.xpath("//div[@class='plan-promo_inner container plan-promo_custom plan-promo-wrapper manage-block']//div[@class='plan-promo_plan-holder']//li["+i+"]//h3")).getText();
		//System.out.println(description);
		     
			 if (!description.contentEquals("GiustaPerTe Impresa Gas") && !description.contentEquals("Enel Energia PLACET Variabile Gas Business") && !description.contentEquals("E-Light Gas Impresa")&& !description.contentEquals("Anno Sicuro Gas Impresa")&& !description.contentEquals("Enel Energia PLACET Fissa Gas Business"))
			 offersListfound="NO";
	
			 
			 if (offersListfound.contentEquals("NO"))
			        throw new Exception("le offerte ricercate per il motorino di ricerca impostato a 'Gas, Negozio-Ufficio e Subentro' non sono presenti nella sezione offerte gas nell'ordine seguente:GiustaPerTe Impresa Gas, Enel Energia PLACET Variabile Gas Business,E-Light Gas Impresa,Anno Sicuro Gas Impresa, Enel Energia PLACET Fissa Gas Business");    
				
			}
			
  }
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void verificaDettaglioOfferta(By existentObject) throws Exception {
		  List <WebElement> listdettaglioOfferta = util.waitAndGetElements(existentObject);
		 
		   
			for (int i = 1; i <= listdettaglioOfferta.size(); i++) {			
	         	if (!util.exists(By.xpath("//li[@class='offert-item']["+i+"]//button[@type='button' and normalize-space(text())='Dettaglio Offerta']"), 15))
			       throw new Exception("ul pulsante web 'DETTAGLIO OFFERTA' per ogni offerta gas con xpath " + existentObject + " non esiste.");
			}
	}
	
	public void verificaAttivaOra(By existentObject) throws Exception {
		  List <WebElement> listdettaglioOfferta = driver.findElements(existentObject);
		   
			for (int i = 1; i <= listdettaglioOfferta.size(); i++) {	
				checkColor(By.xpath("//li[@class='offert-item']["+i+"]//div[@class='pricing-details']//a[@href='/it/adesione' and normalize-space(text())='Attiva ora']"),"DeepPink","link 'ATTIVA ORA'");
	         	if (!util.exists(By.xpath("//li[@class='offert-item']["+i+"]//div[@class='pricing-details']//a[@href='/it/adesione' and normalize-space(text())='Attiva ora']"), 15))
			       throw new Exception("l'oggetto link 'ATTIVA ORA' per ogni offerta gas con xpath " + existentObject + " non esiste.");
			}
	}
}
