package com.nttdata.qa.enel.components.colla;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DetaglioFornitura_GestiscileOreFree67_Component {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By oreFreeTitle = By.xpath("//div[@class='forniture-info-block oreFree-oggi_container']//h2[@class='forniture-title']");
	//public By modifica = By.xpath("//div[@data-info-date='11 Giugno']//a[@class='forniture-link modify-daily']");
	public By modifica = By.xpath("//div[@data-info-date][2]//a[@class='forniture-link modify-daily']");
	public By setYourBand = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3");
	public By setBandSubText = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//p");
	public By dalleOre = By.xpath("//div[@class='cm-col cm-col-large cm-custom-select']/label");
	public By alleOre = By.xpath("//div[@class='cm-col cm-col-small cm-custom-select']/label");
	public By haiImpostatoLafascia = By.xpath("//div[@class='cm-col cm-col-medium print-new-settings' and contains(text(),'Hai impostato')]");
	public By startTime = By.xpath("//span[@id='startingDailySelectBoxIt']");
	//public By endTime = By.xpath("//span[@class='selectboxit  selectboxit-disabled selectboxit-btn' and @name='end-time']");
	public By endTime = By.xpath("//span[@class='selectboxit selectboxit-disabled selectboxit-btn' and @name='end-time']");                                             
	public By startTimeList = By.xpath("//ul[@id='startingDailySelectBoxItOptions']//li");
	public By conferma = By.xpath("//button[@class='button_first xh-highlight']//span[contains(text(),'Conferma')]");
	public By popUpText = By.xpath("");
	public By chiudi = By.xpath("");
	public By currentDate = By.xpath("//div[@class='single-info-box']//div[@class='infos ']//div[@class='infos-inner']//div[@class='info-value']");
	public By haiImpostato = By.xpath("");
	
	public DetaglioFornitura_GestiscileOreFree67_Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
 
 public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
 public void switchToDefault(){
		driver.switchTo().defaultContent();
	}
 
 public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
    
    public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
    
    public void scrolltoElement(By checkObject) throws Exception{
    	util.scrollToElement(driver.findElement(checkObject));
    	util.waitAndGetElement(checkObject);
    }
    
    public String getDate() throws ParseException {
    	SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMMM",Locale.ITALIAN);
    	Date today = new Date();
        String date = DATE_FORMAT.format(today);
        date = date.replace("-", " ");
      //  DateTimeFormatter fIn = DateTimeFormatter.ofPattern( date , Locale.ITALIAN ); 
        System.out.println("Date "+date);
        return date;
	}
    
    public void checkForDate() throws Exception{
    	
    	scrolltoElement(currentDate);
    	String today = getDate();
    	if(today.equals(driver.findElement(currentDate).getText())){
    		clickComponent(modifica);
    	}
    }
    
    public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	

	public void checkFieldStatus(By checkObject) throws Exception {
		
		if(!driver.findElement(checkObject).isEnabled())
		throw new Exception(checkObject+" Field is enable");
	}
	
	public void checkFieldValue() throws Exception {		
		List<WebElement> list =driver.findElements(startTimeList);
		System.out.println("size "+list.size());
		for(int i=0;i<list.size();i++){					
			String value = list.get(i).getText();
			System.out.println(value);

			 if(!Arrays.asList(startTimeValue).contains(value))
	                throw new Exception ("This page does not has the following Bill Details:'Data Emissione','Data Scadenza','Stato Pagamento','Data Pagamento','Modalità di Pagamento','Attestazione di Pagamento'");
		}		
		clickComponent(startTime);
	}
	
	public void changeDropDownValue(By checkObject,String property) throws Exception {
		
		clickComponent(startTime);
		List<WebElement> list =driver.findElements(checkObject);
		System.out.println("size "+list.size());
		for(int i=0;i<list.size();i++){					
			String value = list.get(i).getText();
			System.out.println(value);
			 if(property.equals(value)){
				
				 util.scrollToElement(driver.findElement(By.xpath("//ul[@id='startingDailySelectBoxItOptions']//li[@data-val='"+value+"']")));
				 clickComponent(By.xpath("//ul[@id='startingDailySelectBoxItOptions']//li[@data-val='"+value+"']"));
			 }
		}			
	}

	public static final String setYourBandText ="Scegli le Ore Free più adatte alle tue esigenze di consumo.";
	public static final String [] startTimeValue = {"00:00","01:00","02:00","03:00","04:00","05:00","06:00","07:00","08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00"};
	public static final String haiImpostatoLafasciaText = "Hai impostato la fascia 20:00 - 23:00";
	public static final String confermaPopupText = "Ricordati che puoi cambiare ogni volta che vuoi la tua Fascia Giornaliera per poter ottenere il massimo del risparmio secondo le tue abitudini di consumo quotidiane.";
}
