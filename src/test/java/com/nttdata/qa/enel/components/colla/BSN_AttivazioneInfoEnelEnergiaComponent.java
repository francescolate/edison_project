package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSN_AttivazioneInfoEnelEnergiaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By pagePath = By.xpath("//ol[@class='breadcrumb']");
	public By pageTitle = By.xpath("//h1[contains(text(),'Attivazione')]");
	public By pageTitleNew = By.xpath("//h1[contains(text(),'Modifica')]");
	public By pageSubText = By.xpath("//p[contains(text(),'Seleziona e compila')]");
	public By menuOne = By.xpath("//span[text()='Notifiche e dati']");
	public By menuOneDescription = By.xpath("//p[contains(text(),'Scegli su')]");
	public By avvisoComunicaLettura = By.xpath("//b[text()='Avviso comunica lettura']");
	public By avvisoComunicaLetturaSMS = By.xpath("//b[text()='Avviso comunica lettura']//following::span[1]");
	public By avvisoComunicaLetturaEmail = By.xpath("//b[text()='Avviso comunica lettura']//following::span[2]");
	public By avvisoEmissioneBolletta = By.xpath("//b[text()='Avviso emissione bolletta']");
	public By avvisoEmissioneBollettaSMS = By.xpath("//b[text()='Avviso emissione bolletta']//following::span[1]");
	public By avvisoEmissioneBollettaEmail = By.xpath("//b[text()='Avviso emissione bolletta']//following::span[2]");
	public By avvenutoPagamento = By.xpath("//b[text()='Avvenuto Pagamento']");
	public By avvenutoPagamentoSMS = By.xpath("//b[text()='Avvenuto Pagamento']//following::span[1]");
	public By avvenutoPagamentoEmail = By.xpath("//b[text()='Avvenuto Pagamento']//following::span[2]");
	public By avvisoProssimaScadenza = By.xpath("//b[text()='Avviso prossima scadenza']");
	public By avvisoProssimaScadenzaSMS = By.xpath("//b[text()='Avviso prossima scadenza']//following::span[1]");
	public By avvisoProssimaScadenzaEmail = By.xpath("//b[text()='Avviso prossima scadenza']//following::span[2]");
	public By avvisoSollecitoPagamento = By.xpath("//b[text()='Avviso sollecito pagamento']");
	public By avvisoSollecitoPagamentoSMS = By.xpath("//b[text()='Avviso sollecito pagamento']//following::span[1]");
	public By avvisoSollecitoPagamentoEmail = By.xpath("//b[text()='Avviso sollecito pagamento']//following::span[2]");
	public By avvisoConsumi = By.xpath("//b[text()='Avviso Consumi']");
	public By avvisoConsumiSMS = By.xpath("//b[text()='Avviso Consumi']//following::span[1]");
	public By avvisoConsumiEmail = By.xpath("//b[text()='Avviso Consumi']//following::span[2]");
	public By sectionTitle = By.xpath("//p[contains(text(),'Inserisci i tuoi')]");
	public By prosegui = By.xpath("//button[@class='btn btn-primary']"); 
	public By fine = By.xpath("//a[@class='btn btn-primary']");
	public By emailError = By.xpath("//label[contains(text(),'Email')]//following::label[1]");
	public By email = By.xpath("//label[contains(text(),'Email')]//following::input[1]");
	public By confirmEmail = By.xpath("//label[contains(text(),'Conferma email')]/following::input[1]");
	public By numeroDiCellulare = By.xpath("//label[contains(text(),'Numero di Cellulare')]/following::input[1]");
	public By confermaCellulare = By.xpath("//label[contains(text(),'Conferma cellulare')]/following::input[1]");
	public By aggiornaIDatiDiContatto = By.xpath("//span[contains(text(),'Aggiorna i dati di contatto')]");
	public By aggiornaIDatiDiContattoCheck = By.xpath("//span[contains(text(),'Aggiorna i dati di contatto')]");
	public By esci = By.xpath("//a[text()='Esci']");
	public By popupesci = By.xpath("//div[@id='sendAbort']");
	public By homepage = By.xpath("//a[text()='Homepage']");
	public By indietro = By.xpath("//a[text()='Indietro']");
	public By cellulare = By.xpath("//label[contains(text(),'Numero di Cellulare')]/following::input[1]");
	public By menuTwo = By.xpath("//span[text()='Riepilogo e conferma dati']");
	public By titleSubText = By.xpath("//p[contains(text(),'Controlla che i')]");
	public By menuTwoTitle = By.xpath("//p[contains(text(),'Sms o mail?')]");
	public By supplyDataText = By.xpath("//h5[contains(text(),'Il servizio')]");
	public By tipo = By.xpath("//th/b[text()='Tipo']");
	public By numeroCliente = By.xpath("//th/b[text()='Numero Cliente / Alias']");
	public By address = By.xpath("//th/b[text()='Indirizzo di fornitura']");
	public By tipoValue = By.xpath("//th/b[text()='Tipo']/following::td[1]");
	public By numeroClientiValue = By.xpath("//th/b[text()='Tipo']/following::td[2]");
	public By addressValue = By.xpath("//th/b[text()='Tipo']/following::td[3]");
	public By cellulareError = By.xpath("//label[text()='Numero di Cellulare*']//following::label[1]");
	public By iIcon = By.xpath("//span[@class='custom-ico custom-ico-inline infotooltip']");
	public By iIconText = By.xpath("//p[contains(text(),'Selezionando la funzione')]");  
	public By popupheadertext = By.xpath("//h4[text()='Attenzione']"); 
	public By popupsubtext = By.xpath("//p[contains(text(),'Se uscirai dal processo perderai le modifiche effettuate.')]"); 
	public By step3Text = By.xpath("//p[contains(text(),'La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve.')]"); 
	
	public By emailCheckBoxes = By.xpath("//div[@class='row check-block']//label[@class='checkbox-inline'][2]//span");
	public By certificationEmailTitle = By.xpath("//section[@id='OkMessage']");
	
	public BSN_AttivazioneInfoEnelEnergiaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponent(By clickObject) throws Exception {
			util.objectManager(clickObject, util.scrollToVisibility, false);
			util.objectManager(clickObject, util.scrollAndClick);
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void enterInputToField(By checkElement,String input){
			
			util.objectManager(checkElement, util.scrollAndSendKeys,input);
		}
		
		public void selectCheckBox(By checkObject){
			if(!driver.findElement(checkObject).isSelected()){
				
				try {
					Thread.sleep(5000);
					clickComponent(checkObject);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		public void verifyContentNotDispaying(By checkObject) throws Exception{
			Boolean flag = driver.findElements(checkObject).size() > 0;
			if(flag.equals("true"))
				throw new Exception(checkObject+"Field is displaying");
		}

		public void verifyFieldEnable(By checkObject) throws Exception{
		
			Boolean isEnable = driver.findElement(checkObject).isEnabled();
			System.out.println(isEnable+"Status");
			if(!isEnable.toString().equals("false"))
				throw new Exception("Input Field is Enable");
		}
		
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}

		public void selectEmailCheckBox(){
			List<WebElement> emailElements = driver.findElements(emailCheckBoxes);
			System.out.println(emailElements.size());
			for(WebElement ele:emailElements)
			{
				ele.click();
				/*try {
					emailElements = driver.findElements(emailCheckBoxes);
					emailElements.get(i).click();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
		}
		public static final String PageTitle = "Attivazione Info Enel Energia";
		public static final String PageSubText = "Seleziona e compila i campi sottostanti e poi prosegui."; 
		public static final String MenuOneDescription = "Scegli su cosa vuoi essere informato.";
		public static final String TitleSubText = "Controlla che i dati inseriti siano corretti e poi conferma.";
		public static final String MenuTwoTitle = "Sms o mail? Ecco dove preferisci ricevere le tue notifiche.";
		public static final String SupplyDataText = "Il servizio verrà attivato sulla seguente fornitura:";
		public static final String TipoValue = "Luce";
		public static final String Tipo = "Tipo";
		public static final String NumeroClientiValue_260 ="592594359";
		public static final String AddressValue_260 = "Via Santa Sofia 105d 95123 Catania Catania Ct";

}
