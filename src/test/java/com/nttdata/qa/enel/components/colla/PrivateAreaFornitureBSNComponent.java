package com.nttdata.qa.enel.components.colla;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaFornitureBSNComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;

	public By fornitureMenuItem = By.xpath("//a[text()='forniture']");
	public By homepageMenuItem = By.xpath("//a[text()='HomePage']");
	public By iTuoiDatiContainer = By.xpath("//*[text()='I tuoi dati']/ancestor::div[@class='panel-body gray']");
	public By leTueBolletteContainer = By.xpath("//*[text()='Le tue bollette']/ancestor::div[@class='panel-body gray'][1]");
	public By fornitureEBolletteHeader = By.xpath("//h2[text()='Forniture e bollette']");
	public By panelHeadingIndex = By.xpath("//div[@id='heading-Index']");
	public By fornituraInDettaglioContainer = By.xpath("//*[contains(text(),'La tua fornitura nel dettaglio')]/ancestor::div[@class='panel-body gray']");
	
    public PrivateAreaFornitureBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public void compareText(By by, String[] text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		for(String s : text){
			System.out.println(s);
			if(!weText.contains(s))
				throw new Exception("The webpage container does not contain the text '"+s+"'");
		}
	}
	
	public void compareTextDisabledInput(String xpath, String textToCheck) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		if(!s.equals(textToCheck))
			throw new Exception("Il testo cercato:\n\t"+textToCheck+"\nè diverso da quello in pagina:\n\t"+s);
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}

	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
	}
	
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public void checkBolletteTableRows(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		List<WebElement> l = driver.findElements(by);
		if(l.size()<1)
			throw new Exception("Non ci sono bollette da pagare!");
	}
	
	public final String iTuoiDatiText = "I tuoi dati;Cliente;Numero cliente;Tipologia;Modalità di pagamento;Codice fiscale;Indirizzo fornitura;Modalità di ricezione;Dati tecnici fornitura;POD;Potenza contrattuale;Tensione;Contratto;Offerta;Stato;Data di attivazione";
	public final String leTueBolletteText = "Le tue bollette;Di seguito trovi l'elenco delle tue bollette. Clicca sul dettaglio per maggiori informazioni.;Numero;Importo;Stato;Scadenza";
	public final String leTueBolletteHomepageText = "Le tue bollette;Numero;Importo;Stato;Scadenza";
	public final String fornituraInDettaglioText = "La tua fornitura nel dettaglio;Luogo;Tipologia;Luce;Offerta;Stato;Attiva;Più informazioni";
	//public final String panelHeadingIndexText = "Prova BSN;Fornitura;Luce;Cliente N;636727539";
	
	public final String panelHeadingIndexText = "Prova2BSN;Fornitura;Luce;Cliente N °: 636727539";
}
