package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PaginaAdesionePodComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By testoconpod=By.xpath("//div[@id='adesione_standard']//p");
	//public By ipod=By.xpath("//a[@class='openPOD icon3-line-info2' and @aria-label=\"Icona informativa. Cos'è il POD?\"]");
	public By ipod=By.xpath("//a[@class='openPOD icon3-line-info2']");
	public By titoloIpodPopup=By.xpath("//div[@data-remodal-id='modalInfoPod']//div[@class='remodal-content']//h4[@class='modal-title']");
	public By testoipod=By.xpath("//div[@data-remodal-id='modalInfoPod']//div[@class='remodal-content']//div[@class='modal-body modale_cf']");
	public By chiudiipod=By.xpath("//button[@aria-label='Chiudi modale info pod' and @class='remodal-close close-AEM_SF internal-focus']");
	public By chiudiAdesione=By.xpath("//button[@aria-label='Chiudi Adesione']");
	public By escisenzasalvarebutton=By.xpath("//button[@id='esciSenzaSalvareNoFlag']");
	public By pulsanteInformativoDatiImmobile=By.xpath("//a[@class='openDatiC icon3-line-info2' and @aria-label=\"Informazioni dati dell'immobile\"]");
	public By titoloDatiImmobilePopup=By.xpath("//div[@data-remodal-id='modalInfoDatiC']//div[@class='modal-header']//h4[@class='modal-title']");
	public By testoDatiImmobile=By.xpath("//div[@data-remodal-id='modalInfoDatiC']//div[@class='modal-body']");
	public By chiudiPopupDatiImmobile=By.xpath("//button[@aria-label='Chiudi modale info dati immobile' and @class='remodal-close close-AEM_SF internal-focus']");
	public String testoconIconaInformativaPod1="Ricorda che per completare l'adesione devi avere a portata di mano: Codice fiscale / POD / Iban  L'offerta che hai scelto è valida solo per forniture ad uso abitativo.";
	public String testoconIconaInformativaPod2="Ricorda che per completare l'adesione devi avere a portata di mano: Codice fiscale / POD / Iban se devi fare un Subentro, una Prima attivazione o una Voltura servono anche: dati dell'immobile / Carta d'identità  L'offerta che hai scelto è valida solo per forniture ad uso abitativo.";
	public String titoloPod="COS'E' IL POD";
	public String descrizionePod="Il POD (Point of Delivery) è il codice identificativo della fornitura elettrica e serve ad individuare il punto di prelievo dell'energia su tutto il territorio nazionale. Il POD: inizia sempre con IT è un codice alfanumerico composto da 14 caratteri potrebbe essere composto da 15 caratteri, ma è sufficiente considerare i primi 14 non cambia anche se cambi fornitore di energia elettrica Se la fornitura è già stata attiva lo trovi nella bolletta. L'EnelTel è un codice numerico specifico che identifica il contatore che il distributore ha installato. Genericamente di 8-9 cifre, viene visualizzato sul display dell'apparecchio di misura alla voce \"Numero Cliente\".";
	public String titoloDatiImmobile="Dati dell'immobile";
	public String descrizioneDatiImmobile="Per procedere con la tua attivazione, tieni a portata di mano le seguenti informazioni: Dati sul regolare possesso e detenzione dell' immobile. Uno tra queste informazioni: Proprietà/Usufrutto Locazione/Comodato Altro diritto sull'immobile";
	
	public PaginaAdesionePodComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}
	
	public void clickcomponentjavascript(By object) throws Exception {
		WebElement button=util.waitAndGetElement(object);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void verificaTesto (By object,String testo) throws Exception {
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
		//System.out.println(text);
		if (!text.contentEquals(testo)) 
        	throw new Exception("la label di testo con descrizione "+testo+" non e' presente");
	
	}
	
    public void clickWithJS(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
        util.jsClickElement(oggettocliccabile);
    }
}
