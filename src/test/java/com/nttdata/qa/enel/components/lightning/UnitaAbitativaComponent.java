package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class UnitaAbitativaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	
	public UnitaAbitativaComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	public By confermaUnitaAbitativaSWAEVO = By.xpath("//div[contains(@aria-label,'Abitativa')]/ancestor::div[@role='region']//button[text()='Conferma']");
	public By unitaAbitativa = By.xpath("//label[contains(text(),'abitativa')]/parent::div//input[@role='listbox']");
	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	public void selezionaUnitaAbitativa(String tipo) throws Exception{
//		util.selectLighningValue("Unità abitativa", tipo);
		
		util.objectManager(unitaAbitativa, util.scrollAndClick);
		
		By scelta=By.xpath("//label[contains(text(),'abitativa')]/parent::div//span[text()='"+tipo+"']");
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(scelta, util.click);
		spinner.checkSpinners();
	}
	
}
