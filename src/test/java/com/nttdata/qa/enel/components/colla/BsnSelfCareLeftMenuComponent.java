package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BsnSelfCareLeftMenuComponent extends BaseComponent{
	public By avatarLogo = By.id("avatarUtente");
	private final static String menuItem = "//div[@id='sticky-wrapper']/nav/ul/div/li/a[text()='#']";
	
	public BsnSelfCareLeftMenuComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyMenuItem(String itemName) throws Exception {
		By item = locateMenuItemWithName(itemName);
		if (!util.verifyVisibility(item, 15)) {
			throw new Exception("The menu item '" + itemName.toUpperCase() + " is not displayed.");
		}
	}
	
	public void verifyMenuItems() throws Exception {
		for (int i = 0; i < menuItemsStrings.length; i++) {
			String lowerCaseName = menuItemsStrings[i].toLowerCase();
			verifyMenuItem(lowerCaseName);
		}
	}
	
	public void selectMenuItem(String itemName) throws Exception{
		By item = locateMenuItemWithName(itemName);
		util.objectManager(item, util.click);
	}
	
	private By locateMenuItemWithName (String itemName) {
		if (itemName.equals("homepage")) {
			itemName = "HomePage";
		}
		String xpathString = menuItem.replace("#", itemName);
		return By.xpath(xpathString);
	}
	
	//----- Constants -----
	
//	private String[] menuItemsStrings = {
//			"HOMEPAGE",
//			"BOLLETTE",
//			"FORNITURE",
//			"SERVIZI",
//			"MESSAGGI",
//			"STATO RICHIESTE",
//			"CARICA DOCUMENTI",
//			"CONTATTI",
//			"LOGOUT"
//	};
	private String[] menuItemsStrings = {
			"HOMEPAGE",
			"BOLLETTE",
			"FORNITURE",
			"SERVIZI",
			"MESSAGGI",
			"STATO RICHIESTE",
			"CARICA DOCUMENTI",
			"LOGOUT"
	};
}
