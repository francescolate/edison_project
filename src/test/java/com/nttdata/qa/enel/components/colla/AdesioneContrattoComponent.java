package com.nttdata.qa.enel.components.colla;

import java.awt.AWTException;
import java.awt.List;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class AdesioneContrattoComponent extends BaseComponent{
	
	WebDriver driver;
	
	public By formContainer = By.xpath("//*[@id='form-container']");
	public By richiediCodiceButton = By.xpath("//button[text()='RICHIEDI CODICE DI VERIFICA']");
	public By verifyCode = By.xpath("//*[@id='verify-code-TP']");
	public By inputCodiceVerifica = By.xpath("//*[@id='tp-codiceverificaIframe']");
	public By verificaCodiceButton = By.xpath("//*[@id='verify-nextIframe']");
	public By codeInvalid = By.id("ITA_COD_VER_cc_errmsg");
	
	
	public AdesioneContrattoComponent(WebDriver driver){
		super(driver);
	}
	
	public void checkText(String nome) throws Exception{
		String title = "Bentornato " + nome;
		String text = "Per accedere al link e procedere con l'adesione all'offerta devi inserire il codice di sicurezza che ti verrá inviato al numero di telefono cellulare";
		containsText(formContainer, title);
		containsText(formContainer, text);
		verifyComponentVisibility(richiediCodiceButton);
	}
	public void checkVerifyCode() throws Exception{
		String text1 = "Ti abbiamo inviato un codice di verifica al numero di cellulare sopra indicato";
		String text2 = "Inserisci il codice e clicca su Verifica per accedere.";
		containsText(verifyCode, text1);
		containsText(verifyCode, text2);
		verifyComponentExistence(inputCodiceVerifica);
		verifyComponentExistence(verificaCodiceButton);
	}
	
	public void insertCodeAndVerify(String text) throws Exception{
		clearAndSendKeys(inputCodiceVerifica, text);
		clickComponent(verificaCodiceButton);
	}
	
	public String verifyError(WebDriver driver) throws Exception{
		verifyComponentExistence(codeInvalid);
		String error = driver.findElement(codeInvalid).getAttribute("innerHTML");
		return error;
	}
	
}
