package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;

public class SupplyFilters_BsnComponent extends BaseComponent {
	public enum FilterType {
		ADDRESS,
		CITY,
		STATUS,
		TYPE,
		CLIENT_NUMBER
	}
	
	public By fornitureMenuItem = By.cssSelector("#menuForn > a");
	public By statusLbl = By.xpath("//section[@id=\"fornDetailPannel\"]//div[@data-context='forn-detail']//span[contains(@class,'status')]/text()");
	public By showFiltersBtn = By.name("Filtri");
	public By addressField = By.cssSelector("#filter > div > div.row > div:nth-child(1) > div > div > div > a");
	public By cityField = By.cssSelector("#filter > div > div.row > div:nth-child(2) > div > div > div > a");
	public By statusField = By.cssSelector("#filter > div > div.row > div:nth-child(3) > div > div > div > a > span");
	public By typeField = By.cssSelector("#filter > div > div.row > div:nth-child(4) > div > div > div > a > span");
	public By clientNumberField = By.cssSelector("#filter > div > div.row > div:nth-child(5) > div > div > div > a > span");
	public By addressFirstOption = By.xpath("//*[@id=\"filter\"]/div/div[1]/div[1]/div/div/div/div/ul/li[1]/label/span");
	public By cityFirstOption = By.xpath("//*[@id=\"filter\"]/div/div[1]/div[2]/div/div/div/div/ul/li[1]/label/span");
	public By statusFirstOption = By.xpath("//*[@id=\"filter\"]/div/div[1]/div[3]/div/div/div/div/ul/li[1]/label/span");
	public By typeFirstOption = By.xpath("//*[@id=\"filter\"]/div/div[1]/div[4]/div/div/div/div/ul/li[1]/label/span");
	public By clientNumberFirstOption = By.xpath("//*[@id=\"filter\"]/div/div[1]/div[5]/div/div/div/div/ul/li[1]/label/span");
	public By table = By.xpath("//*[@id=\"table-wrapper\"]/table");
	public By applyBtn = By.id("filterSubmit");
	public By cancelBtn = By.id("filterReset");
	
	public SupplyFilters_BsnComponent(WebDriver driver) {
		super(driver);
	}

	public void checkSupplyStatus() throws Exception {
		verifyComponentVisibility(statusLbl);
		verifyComponentText(statusLbl, activeStatus);
		Color greenColour = Color.fromString("#55be5a");
		verifyElementColor(statusLbl, greenColour);
	}
	
	public void checkFilterFields() throws Exception {
		By[] locators = {addressField, cityField, statusField, typeField, clientNumberField};
		for (int i = 0; i < locators.length; i++) {
			verifyComponentVisibility(locators[i]);
		}
	}
	
	private void checkFilter(FilterType filterType) throws Exception {
		By fieldLocator = null, optionLocator = null;
		String columnIndex = null;
		switch (filterType) {
		case ADDRESS:
			fieldLocator = addressField;
			optionLocator = addressFirstOption;
			columnIndex = "4";
			break;
		case CITY:
			fieldLocator = cityField;
			optionLocator = cityFirstOption;
			columnIndex = "4";
			break;
		case STATUS:
			fieldLocator = statusField;
			optionLocator = statusFirstOption;
			columnIndex = "2";
			break;
		case TYPE:
			fieldLocator = typeField;
			optionLocator = typeFirstOption;
			columnIndex = "1";
			break;
		case CLIENT_NUMBER:
			fieldLocator = clientNumberField;
			optionLocator = clientNumberFirstOption;
			columnIndex = "3";
			break;
		}
		clickComponent(fieldLocator);
		verifyComponentVisibility(optionLocator);
		String selection = getElementTextString(optionLocator);
		String[] selectionArray = { selection };
		clickComponent(optionLocator);
		verifyComponentVisibility(applyBtn);
		clickComponent(applyBtn);
		verifyComponentVisibility(table);
		int rowsNumber = driver.findElements(By.xpath(shownTableRows)).size();
		for (int i = 1; i <= rowsNumber; i++) {
			String xpath = tableElementXpath.replace("#row#", String.valueOf(i)).replace("#column#", columnIndex);
			By tableElement = By.xpath(xpath);
			verifyComponentTextContainsSubstrings(tableElement, selectionArray);
		}
		verifyComponentVisibility(cancelBtn);
		clickComponent(cancelBtn);
		clickComponent(applyBtn);
	}
	
	public void checkAddressFilter() throws Exception {
		checkFilter(FilterType.ADDRESS);
	}
	
	public void checkCityFilter() throws Exception {
		checkFilter(FilterType.CITY);
	}
	
	public void checkStatusFilter() throws Exception {
		checkFilter(FilterType.STATUS);
	}
	
	public void checkTypeFilter() throws Exception {
		checkFilter(FilterType.TYPE);
	}
	
	public void checkClientNumberFilter() throws Exception {
		checkFilter(FilterType.CLIENT_NUMBER);
	}
	
	//----- Constants -----
	
	private final String activeStatus = "Attivata";
	private final String tableElementXpath = "//*[@id=\"table-wrapper\"]/table/tbody/tr[not(contains(@class,'hidden'))][#row#]/td[#column#]";
	private final String shownTableRows = "//*[@id=\"table-wrapper\"]/table/tbody/tr[not(contains(@class,'hidden'))]";
	
}
