package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SpazioEnelMyComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By supportoHeader = By.xpath("//a[.='Supporto']");
	public By contattaci = By.xpath("//div[@id='e_glossary_title']/div[@class='enelmia-deals gloss-link']/div[@class='setting']/div[@class='glossary_two-column']/a[.='Contattaci']");
	public By supportoContattaciPath = By.xpath("//main[@id='main']//div/nav[@class='image-hero_breadcrumbs module--initialized']");
	public By contattaciTitle = By.xpath("//main[@id='main']//descendant::h1[contains(text(),'Tanti')]");
	public By contattaciText = By.xpath("//main[@id='main']//descendant::p[contains(text(),'Enel')]");
	public By recandotiPhrase = By.xpath("//main[@id='main']//descendant::ul/li[contains(text(),'recandoti')]");
	public By recandotiPhraseCliccaQui = By.xpath("//main[@id='main']//descendant::ul/li[contains(text(),'recandoti')]/a/b/span");
	public By spazioEnelPath = By.xpath("//section[@id='externalHero']//descendant::nav");
	public By spazioEnelText = By.xpath("//section[@id='externalHero']//span[contains(text(),'Trova')]");
	public By luceEGasHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[1]/a");
	public By impreseHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[2]/a");
	public By insiemeATeHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[3]/a");
	public By storieHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[4]/a");
	public By futurEHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[5]/a");
	public By mediaHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[6]/a");
	public By abruzzoList = By.xpath("//div[@id='es_index']//div[@class='item-regione'][1]");
	public By napoli = By.xpath("//div[@id='es_index']//div[@class='list-region']/div[@class='item-regione'][4]/div/a[2]");
	public By napoliPath = By.xpath("//section[@id='externalHero']//descendant::nav");
	public By napoliText = By.xpath("//section[@id='externalHero']//span[contains(text(),'Trova')]");
	public By firstElementSpazioNapoli = By.xpath("//div[@id='eswrap']/div[@id='es_index_prov']//div[1]/div[@class='row']");
	public By vaiAlloSpazioEnelButton = By.xpath("//div[@id='es_index_prov']//div[1]/div[@class='row']/div[2]//a");
	public By spazioEnelNapoliPath = By.xpath("//section[@id='externalHero']//descendant::nav");
	public By spazioEnelnapoliText = By.xpath("//section[@id='externalHero']//span[contains(text(),'Trova')]");
	public By lodi = By.xpath("//div[@id='es_index']//div[@class='list-region']/div[@class='item-regione'][9]/div/a[7]");
	public By lodiPath = By.xpath("//section[@id='externalHero']//descendant::nav");
	public By lodiText = By.xpath("//section[@id='externalHero']//span[contains(text(),'Trova')]");
	public By messina = By.xpath("//div[@id='es_index']//div[@class='list-region']/div[@class='item-regione'][15]/div/a[5]");
	public By messinaPath = By.xpath("//section[@id='externalHero']//descendant::nav");
	public By messinaText = By.xpath("//section[@id='externalHero']//span[contains(text(),'Trova')]");
	public SpazioEnelMyComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	  public void clickComponent(By clickableObject) throws Exception {
		  	util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	  
	  public void waitForElementToDisplayAndClick(By clickObject) throws Exception{
		  
		  /*WebDriverWait wait = new WebDriverWait(driver, 15);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(clickObject));
		   wait.until(ExpectedConditions.elementToBeClickable(clickObject));*/
		  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(clickObject));
		   	Thread.sleep(500);
		  	driver.findElement(clickObject).click();
	  }
	  
	  
	  public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	  
	  public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
	  
	  public void  isElementPresent(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(existentObject));
			String elementFound = "NO";
			
			try{
			WebElement we =driver.findElement(By.xpath("//div[@class='card forniture regolare']//*[contains(text(), '11:00 - 14:00')]"));
			we.isDisplayed();
			elementFound = "YES";
			} 
	        catch (Exception e) {

	            System.out.println("Element Not found trying again - " );
	            e.printStackTrace();}
	         }
	  
	  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void checkForCityValue(By checkObject, String Field[], String Value[]) throws Exception{
				List<WebElement> cityList = driver.findElements(By.xpath("//div[@id='es_index']//div[@class='list-region']/div[@class='item-regione']/div/a"));
				List<WebElement> stateList=	driver.findElements(By.xpath("//div[@id='es_index']//div[@class='list-region']/div[@class='item-regione']/h4"));
				List<String> expectedCityList = Arrays.asList(Value);
				List<String> expectedStateList = Arrays.asList(Field);
				
				List<String> newCityList = new ArrayList<String>();
				List<String> newStateList = new ArrayList<String>();
				for(WebElement city : cityList) {
					newCityList.add(city.getText());
				System.out.println(city.getText());}
				for(WebElement state : stateList) {
					newStateList.add(state.getText());
				System.out.println(state.getText());}
				
				Collections.sort(newCityList);
				Collections.sort(newStateList);
				Collections.sort(expectedCityList);
				Collections.sort(expectedStateList);
				if(!newCityList.equals(expectedCityList))
		    		throw new Exception ("The  details are not correctly displayed");
				if(!newStateList.equals(expectedStateList))
		    		throw new Exception ("The  details are not correctly displayed");
		}
		
		public void navigateBack(){
			
			driver.navigate().back();
		}
		
		public static final String CONTATTACI_PATH = "Home/supporto/Come entrare in contatto con Enel Energia posizione attuale";
		public static final String CONTATTACI_TITLE = "Tanti modi per entrare in contatto con noi";
		public static final String CONTATTACI_TEXT = "Enel Energia è sempre in contatto con te, se sei cliente e se vuoi diventarlo.";
		public static final String RECANDOTI_TEXT = "recandoti allo Spazio Enel più vicino a te. Clicca qui per scoprire le nostre sedi.";
		public static final String SPAZIO_ENEL_PATH = "HomeSpazio Enel";
		public static final String SPAZIO_ENEL_TEXT = "Trova lo Spazio Enel più vicino a te";
		public static final String NAPOLI_PATH = "HomeSpazio EnelNAPOLI";
		public static final String NAPOLI_TEXT = "Trova lo Spazio Enel più vicino a te";
		public static final String LODI_PATH = "HomeSpazio EnelLODI";
		public static final String LODI_TEXT = "Trova lo Spazio Enel più vicino a te";
		public static final String MESSINA_PATH = "HomeSpazio EnelMESSINA";
		public static final String MESSINA_TEXT = "Trova lo Spazio Enel più vicino a te";
		public static final String SPAZIOENEL_NAPOLI_PATH = "HomeSpazio EnelNAPOLIAL.BA SRL";
		public static final String SPAZIOENEL_NAPOLI_TEXT = "Trova lo Spazio Enel più vicino a te";
		public static final String FIELD[] = {"Abruzzo","Basilicata","Calabria","Campania","Emilia Romagna","Friuli-Venezia Giulia","Lazio","Liguria","Lombardia","Marche","Molise",
												"Piemonte","Puglia","Sardegna","Sicilia","Toscana","Umbria","Valle D'aosta","Veneto"};
		public static final String VALUES[] = {"TERAMO","PESCARA","L'AQUILA","CHIETI","POTENZA","MATERA","VIBO VALENTIA","REGGIO CALABRIA","CROTONE","COSENZA","CATANZARO","SALERNO","NAPOLI","CASERTA","BENEVENTO","AVELLINO","RIMINI","REGGIO EMILIA","RAVENNA","PIACENZA","PARMA","MODENA","FORLI-CESENA","FERRARA","BOLOGNA","UDINE","TRIESTE","PORDENONE","GORIZIA","VITERBO","ROMA","RIETI","LATINA","FROSINONE","SAVONA","LA SPEZIA","IMPERIA","GENOVA","VARESE","SONDRIO","PAVIA","MONZA E BRIANZA","MILANO","MANTOVA","LODI","LECCO","CREMONA","COMO","BRESCIA","BERGAMO","PESARO E URBINO","MACERATA","FERMO","ASCOLI PICENO","ANCONA","ISERNIA","CAMPOBASSO","VERCELLI","VERBANO-CUSIO-OSSOLA","TORINO","NOVARA","CUNEO","BIELLA","ASTI","ALESSANDRIA","TARANTO","LECCE","FOGGIA","BRINDISI","BARLETTA-ANDRIA-TRANI","BARI","SASSARI","ORISTANO","OLBIA-TEMPIO","OGLIASTRA","NUORO","MEDIO CAMPIDANO","CARBONIA-IGLESIAS","CAGLIARI","TRAPANI","SIRACUSA","RAGUSA","PALERMO","MESSINA","ENNA","CATANIA","CALTANISSETTA","AGRIGENTO","SIENA","PRATO","PISTOIA","PISA","MASSA-CARRARA","LUCCA","LIVORNO","GROSSETO","FIRENZE","AREZZO","TERNI","PERUGIA","AOSTA","VICENZA","VERONA","VENEZIA","TREVISO","ROVIGO","PADOVA","BELLUNO"};
			/*{"TERAMO",
				"PESCARA",
				"L'AQUILA",
				"CHIETI",
				"POTENZA",
				"MATERA",
				"VIBO VALENTIA",
				"REGGIO CALABRIA",
				"CROTONE",
				"COSENZA",
				"CATANZARO",
				"SALERNO",
				"NAPOLI",
				"CASERTA",
				"BENEVENTO",
				"AVELLINO",
				"RIMINI",
				"REGGIO EMILIA",
				"RAVENNA",
				"PIACENZA",
				"PARMA",
				"MODENA",
				"FORLÌ-CESENA",
				"FERRARA",
				"BOLOGNA",
				"UDINE",
				"TRIESTE",
				"PORDENONE",
				"GORIZIA",
				"VITERBO",
				"ROMA",
				"RIETI",
				"LATINA",
				"FROSINONE",
				"SAVONA",
				"LA SPEZIA",
				"IMPERIA",
				"GENOVA",
				"VARESE",
				"SONDRIO",
				"PAVIA",
				"MONZA E BRIANZA",
				"MILANO",
				"MANTOVA",
				"LODI",
				"LECCO",
				"CREMONA",
				"COMO",
				"BRESCIA",
				"BERGAMO",
				"PESARO E URBINO",
				"MACERATA",
				"FERMO",
				"ASCOLI PICENO",
				"ANCONA",
				"ISERNIA",
				"CAMPOBASSO",
				"VERCELLI",
				"VERBANO-CUSIO-OSSOLA",
				"TORINO",
				"NOVARA",
				"CUNEO",
				"BIELLA",
				"ASTI",
				"ALESSANDRIA",
				"TARANTO",
				"LECCE",
				"FOGGIA",
				"BRINDISI",
				"BARLETTA-ANDRIA-TRANI",
				"BARI",
				"SASSARI",
				"ORISTANO",
				"OLBIA-TEMPIO",
				"OGLIASTRA",
				"NUORO",
				"MEDIO CAMPIDANO",
				"CARBONIA-IGLESIAS",
				"CAGLIARI",
				"TRAPANI",
				"SIRACUSA",
				"RAGUSA",
				"PALERMO",
				"MESSINA",
				"ENNA",
				"CATANIA",
				"CALTANISSETTA",
				"AGRIGENTO",
				"SIENA",
				"PRATO",
				"PISTOIA",
				"PISA",
				"MASSA-CARRARA",
				"LUCCA",
				"LIVORNO",
				"GROSSETO",
				"FIRENZE",
				"AREZZO",
				"TERNI",
				"PERUGIA",
				"AOSTA",
				"VICENZA",
				"VERONA",
				"VENEZIA",
				"TREVISO",
				"ROVIGO",
				"PADOVA",
				"BELLUNO"

};*/
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		public static final String ABRUZZO_ELEMENTS[][] = {{"Abruzzo","TERAMO","PESCARA","L'AQUILA","CHIETI"}, {"Abruzzo","TERAMO","PESCARA","L'AQUILA","CHIETI"}};
}
