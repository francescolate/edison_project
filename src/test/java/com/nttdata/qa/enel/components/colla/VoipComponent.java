package com.nttdata.qa.enel.components.colla;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VoipComponent{
	
	WebDriver driver;
	SeleniumUtilities util;
	public int baseTimeoutInterval = 30;
    public int inexTimeoutInterval = 5;
    public int extTimeoutInterval = 15;
	
	public String url = "https://www-coll1.enel.it/it/supporto/faq/faq-voip.html";
	public By urlBy = By.xpath("//link[@href=https://www-coll1.enel.it/it/supporto/faq/faq-voip'");
	public By pathBy = By.xpath("//*[@data-module='navigation-breadcrumbs']");
	public String path = "HOMESUPPORTOCOME FUNZIONA UNA CHIAMATA SU INTERNET?";
	public By title = By.xpath("//h1[text()='Tutto quello che ti serve sapere per sfruttare la tecnologia VoIP']");
	public By dropbase = By.xpath("//div[@class='dropdown parbase']");
	public String text1 = "Con VOIP si intende “Voice Over IP”, ossia “Voce tramite protocollo Internet”, una tecnologia che ti consente di telefonare a chiunque in tempo reale. Per farlo, il VOIP sfrutta la connessione a Internet o una rete dedicata con un protocollo IP (per esempio, la LAN aziendale). Puoi chiamarci attraverso il servizio voip direttamente dal tuo PC, dal tuo smartphone o dal tuo tablet.";
	public String text2 = "Per sfruttare tutte le potenzialità del Voip occorrono:Connessione Internet tramite Wi-fi o connessione dati;Installare l’ultima versione di Chrome, Safari o Firefox;Auricolare o altoparlanti;Microfono.";
	public String text3 = "Se ricevi la telefonata mentre sei al pc, ti verrà chiesto di autorizzare l'app ad accedere al microfono e agli altoparlanti. La qualità audio può variare in base al software audio, all'hardware, al sistema operativo e alla connessione Internet in uso. È consigliabile utilizzare una cuffia USB collegata direttamente al computer e sistemare il microfono a distanza da altri altoparlanti o dispositivi per evitare rumore di ritorno. Se, invece, usi uno smartphone è necessario consentire l’uso del microfono.";
	public String text4 = "No, se si utilizza il wi-fi o la connessione dati.";
	public String text5 = "Il Voip non ha alcun confine. E’ sufficiente avere una buona connessione.";
	public By text1By = By.xpath("//button[text()='Cos’è il VoIP?']//parent::section//p");
	public By text2By = By.xpath("//button[text()='Come posso utilizzare il VoIP?']//parent::section//div");
	public By text3By = By.xpath("//button[text()='Come faccio ad attivare il microfono?']//parent::section//p");
	public By text4By = By.xpath("//button[text()='E’ un servizio a pagamento?']//parent::section//p");
	public By text5By = By.xpath("//button[text()='Posso utilizzare Voip anche se mi trovo all’estero?']//parent::section//p");
	public By button1 = By.xpath("//button[text()='Cos’è il VoIP?']");
	public By button2 = By.xpath("//button[text()='Come posso utilizzare il VoIP?']");
	public By button3 = By.xpath("//button[text()='Come faccio ad attivare il microfono?']");
	public By button4 = By.xpath("//button[text()='E’ un servizio a pagamento?']");
	public By button5 = By.xpath("//button[text()='Posso utilizzare Voip anche se mi trovo all’estero?']");
							
	
	public VoipComponent (WebDriver driver) throws Exception{
        this.driver = driver;
        util = new SeleniumUtilities(this.driver);
	}
	
	public void verifyDropbase() throws Exception{
		containsText(dropbase, "Cos’è il VoIP?");
		containsText(dropbase, "Come posso utilizzare il VoIP?");
		containsText(dropbase, "Come faccio ad attivare il microfono?");
		containsText(dropbase, "E’ un servizio a pagamento?");
		containsText(dropbase, "Posso utilizzare Voip anche se mi trovo all’estero?");	
		System.out.println("Dropbase OK");
	}
	
	public void verifyTextDropbase() throws Exception{
		clickComponent(button1);
		compareText(text1By, text1, true);
		clickComponent(button2);
		compareText(text2By, text2, true);
		clickComponent(button3);
		compareText(text3By, text3, true);
		clickComponent(button4);
		compareText(text4By, text4, true);
		clickComponent(button5);
		compareText(text5By, text5, true);
		System.out.println("Dropbase Text OK");
		
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = we.getText();		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
    public void confirmUrl(String theUrl) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, baseTimeoutInterval);
        if (!wait.until(ExpectedConditions.urlToBe(theUrl))) {
            String current = driver.getCurrentUrl();
            throw new Exception("Browser did not load required url: " + theUrl + "\nCurrent url: " + current);
        }
    }
    
    public void verifyComponentExistence(By oggettoEsistente) throws Exception {
        if (!util.verifyExistence(oggettoEsistente, extTimeoutInterval)) {
            throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
        }
    }
    
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, true);
		util.objectManager(clickableObject, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		
	}
	


		

}
