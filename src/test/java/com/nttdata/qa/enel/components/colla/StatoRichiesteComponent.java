package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class StatoRichiesteComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By pagePath = By.xpath("//div[@class='panel-body']//ol");
	public By pageTitle = By.xpath("//h1[text()='Stato Richieste']");
	public By subText = By.xpath("//h1[text()='Stato Richieste']//following::p[1]");
	
	
	public By statoRichieste = By.xpath("//a[text()='stato richieste']");
	public By contratti = By.xpath("//a[text()='Contratti']");
	public By servizi = By.xpath("//li[@id='servizi']/a[text()='Servizi']");
	public By modificheContatore = By.xpath("//a[text()='Modifiche contatore']"); 
	public By mostra = By.xpath("//a[@name='Filtri']"); 
	
	public By openEnergyDigital = By.xpath("//h4[text()='Open_Energy Digital']"); 
	public By tipo = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Tipo:']"); 
	public By tipoValue = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span[text()='  Luce ']"); 
	public By nomeOfferta = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Numero Offerta:']"); 
	public By nomeOffertaValue = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span[text()='  SG3235321 ']");
	public By pod = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='  POD  ']"); 
	public By podValue = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/span[text()='IT004E32838292']");
	public By dataRichiesta = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Data richiesta:']"); 
	public By aataAttivazione = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Data richiesta:']");
	public By inviata = By.xpath("//li[@class='step important past'][1]");
	public By collapseIcon = By.xpath("//span[@class='collapse-icon'][1]");
	
	public By numeroUtente = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Numero utente:']");
	public By indirizzo = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Indirizzo di fornitura:']");
	public By tipodiattivazione = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Tipo di attivazione:']");
	public By descrizionetipodiattivazione = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Descrizione tipo di attivazione:']");
	public By stato = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Stato:']");
	
	public By descrizionestato = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Descrizione stato:']");
	public By visualizzacontrattofirmato = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Visualizza contratto firmato:']");
	public By documentazionecontrattuale = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Documentazione contrattuale:']");
	
	public By infoenelenergia = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div/div/h4");
	public By infoenelenergiaTipo = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div/following-sibling::div/div/span/b[text()='Tipo:']");
	public By infoenelenergiaNumerocliente  = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div/following-sibling::div/div/span/b[text()='Numero cliente:']");
	public By infoenelenergiaTipoperazione = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div/following-sibling::div/div/following-sibling::div/span/b[text()='Tipo operazione:']");
	public By infoenelenergiaDatarichiesta = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div/following-sibling::div/div/following-sibling::div/span/b[text()='Data richiesta:']");
	
	public By inlavorazione = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div[@id='pratica--0']/div/div/ul/li/span[text()='In lavorazione']");
	public By eSITO = By.xpath("//div[@class='nav-wrap']/following-sibling::div/div/div/div[@id='pratica--0']/div/div/ul/li/span[text()='Esito']");
	
	public By tipo1 = By.xpath("//div[@id='servizi']/div[@class='panel-body gray'][1]/div[@class='row'][2]/div[@class='col-xxs-12 col-xs-6'][1]/span[1]");
	public By numeroCliente = By.xpath("//div[@id='servizi']/div[@class='panel-body gray'][1]/div[@class='row'][2]/div[@class='col-xxs-12 col-xs-6'][1]/span[2]");
	public By tipoOperazione = By.xpath("//div[@id='servizi']/div[@class='panel-body gray'][1]/div[@class='row'][2]/div[@class='col-xxs-12 col-xs-6'][2]/span[1]");
	public By dataRichiesta1 = By.xpath("//div[@id='servizi']/div[@class='panel-body gray'][1]/div[@class='row'][2]/div[@class='col-xxs-12 col-xs-6'][2]/span[2]");
	public By collapseIcon1 = By.xpath("//div[@id='pratica--0']/div[@class='item-wrapper']/a//span[@class='collapse-icon']");
	public By dettaglioOperazione = By.xpath("//div[@id='dettaglio-0']/div/div[@class='row'][1]/div[@class='col-xs-12']/h5");
	public By potenzoRichiesta = By.xpath("//div[@id='dettaglio-0']//div[@class='col-xxs-12 col-xs-6'][1]/p/span[1]/b");
	public By tensioneRichiesta = By.xpath("//div[@id='dettaglio-0']//div[@class='col-xxs-12 col-xs-6'][1]/p/span[2]/b");
	public By Npreventivo = By.xpath("//div[@id='dettaglio-0']//div[@class='col-xxs-12 col-xs-6'][2]/p/span[1]/b");
	
	public StatoRichiesteComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public static final String Path = "Area riservataStato Richieste";
	public static final String SubText = "In questa pagina puoi visionare lo stato di avanzamento delle tue richieste, in ogni momento.";
	public static final String TIPO ="Tipo:  Luce";
	public static final String NUMERO_CLIENTE ="Numero cliente: 310492674";
	public static final String TIPO_OPERAZIONE ="Tipo operazione: Modifica";
	public static final String DATA_RICHIESTA ="Data richiesta: 03/12/2020";
}
