package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModificaAddebitoDirettoComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[text()='Modifica Addebito Diretto']");
	public By titleSubText = By.xpath("//h2[contains(text(),'Seleziona una o')]");
	public By addressLable = By.xpath("//span[text()='Indirizzo della fornitura']");
	public By numerloClienteLable = By.xpath("//span[text()='Numero Cliente']");
	public By modalitaAddebitoDiretto = By.xpath("//span[text()='Modalità Addebito Diretto']");
	public By modalitaAddebitoDirettoValue = By.xpath("//span[text()='Modalità Addebito Diretto']/following-sibling::span");
	public By address = By.xpath("//span[text()='Indirizzo della fornitura']/following-sibling::span");
	public By numerloCliente = By.xpath("//span[text()='Numero Cliente']/following-sibling::span");
	public By ESCI = By.xpath("//span[text()='ESCI']");
	public By modificaAddebito = By.xpath("//span[text()='MODIFICA ADDEBITO']");
	public By radioButton = By.xpath("//span[@id='checkbox-item-0']");
	public By titleSubText1 = By.xpath("//h2[contains(text(),'Stai effettuando')]");
	public By inserimentoDati = By.xpath("//span[text()='Inserimento dati']");
	public By riepilogoEConfermaDati = By.xpath("//span[text()='Riepilogo e conferma dati']");
	public By Esito = By.xpath("//span[text()='Esito']");
	
	public By menuOneDescription = By.xpath("//h4[contains(text(),'Inserisci le')]");
	public By cognome = By.xpath("//input[@id='cognome-intestatarioInput']");
	public By nome = By.xpath("//input[@id='nome-intestatarioInput']");
	public By CF = By.xpath("//input[@id='cf-intestatarioInput']");
	public By IBAN = By.xpath("//input[@id='iban-intestatarioInput']");
	public By agenzia = By.xpath("//input[@id='bank-agencyInput']");
	public By nomeBanca = By.xpath("//input[@id='bank-nameInput']");
	public By INDIETRO = By.xpath("//span[text()='INDIETRO']");
	public By CONTINUA = By.xpath("//span[text()='CONTINUA']");
	public By IBANError = By.xpath("//span[@id='iban-intestatarioError']");
	public String agenziaString = "//input[@id='bank-agencyInput']";
	public String nomeBancaString = "//input[@id='bank-nameInput']";
	public String cognomeString = "//input[@id='cognome-intestatarioInput']";
	public String nomeString = "//input[@id='nome-intestatarioInput']";
	public String CFString = "//input[@id='cf-intestatarioInput']";
	
	public By menuTwoDescription = By.xpath("//h4[contains(text(),'Verifica la')]");
	public By ragione = By.xpath("//dt[contains(text(),'Cognome/Ragione sociale Intestatario C/C')]");
	public By ragioneValue = By.xpath("//dt[contains(text(),'Cognome/Ragione sociale Intestatario C/C')]/following-sibling::dd");
	public By nomeIntestatario = By.xpath("//dt[contains(text(),'Nome Intestatario C/C')]");
	public By nomeIntestatarioValue = By.xpath("//dt[contains(text(),'Nome Intestatario C/C')]/following-sibling::dd");
	public By CF1 = By.xpath("//dt[contains(text(),'Codice Fiscale')]");
	public By CF1Value = By.xpath("//dt[contains(text(),'Codice Fiscale')]/following-sibling::dd");
	public By IBAN1 = By.xpath("//dt[contains(text(),'IBAN')]");
	public By IBAN1Value = By.xpath("//dt[contains(text(),'IBAN')]/following-sibling::dd");
	public By Agenzia1 = By.xpath("//dt[contains(text(),'Agenzia')]");
	public By Agenzia1Value = By.xpath("//dt[contains(text(),'Agenzia')]/following-sibling::dd");
	public By nomeBanca1 = By.xpath("//dt[contains(text(),'Nome banca')]");
	public By nomeBanca1Value = By.xpath("//dt[contains(text(),'Nome banca')]/following-sibling::dd");
	public By menuTwoText = By.xpath("//h4[contains(text(),'La modalit')]");
	public By menuTwoText1 = By.xpath("//h4[contains(text(),'Inserisci la tua')]");
	public By email = By.xpath("//input[@id='email-intestatarioInput']");
	public By campiObbligatori = By.xpath("//span[contains(text(),'campi obbligatori')]");
	public By confirmEmail = By.xpath("//input[@id='email-intestatarioConfirmInput']");
	public By numeroClienteLable = By.xpath("//span[text()='Numero cliente']");
	public By numeroCliente = By.xpath("//span[text()='Numero cliente']/following-sibling::span");
	public By conferma = By.xpath("//span[text()='CONFERMA']");
	
	public By menuThreeDescription = By.xpath("//h3[@id='id_title']");
	public By menuThreeText = By.xpath("//p[contains(text(),'La richiesta di')]");
	public By fine = By.xpath("//span[text()='FINE']");
	
	public ModificaAddebitoDirettoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
	}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	 }
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
	public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
	
	public void verifyFieldIsNotEditable(By checkElement) throws Exception{
		if(driver.findElement(checkElement).isEnabled())
			throw new Exception("Field is editable");
	}
	
	public void enterInputToField(By checkElement,String input){
		
		util.objectManager(checkElement, util.scrollAndSendKeys,input);
	}
	
	 public void checkPrePopulatedValue(String xpath,String Input) throws Exception{         
         WebDriverWait wait = new WebDriverWait(this.driver, 50);
             wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
             JavascriptExecutor js = (JavascriptExecutor) driver;
             String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
             System.out.println(value+"Value");
             if(! value.equals(Input))throw new Exception(value +" Pre populated field is not matching with the actual data "+Input);
     }
	
	public static final String TitleSubText = "Seleziona una o più forniture su cui intendi modificare il servizio di addebito diretto della bolletta sul tuo conto corrente.";
	public static final String Address = "Salita Motto 30 13836 Cossato Cossato Bi";
	public static final String NumerloCliente = "293798389";
	public static final String ModalitaAddebitoDirettoValue = "Conto Corrente";
	public static final String TitleSubText1 = "Stai effettuando la modifica del servizio di addebito diretto sul tuo conto corrente.";
	public static final String MenuOneDescription = "Inserisci le informazioni del conto";
	public static final String IBANERROR = "IBAN vuoto";
	public static String IBANInput = "IT75W0103001200010101010101";
	public String AGENCIA = "FILIALE DI AOSTA";
	public String NOMEBANCA="BANCA MONTE DEI PASCHI DI SIENA S.P.A.";
	public static final String MenuTwoDescription ="Verifica la correttezza delle informazioni inserite:";
	public static final String MenuTwoText = "La modalità di pagamento verrà aggiornata su:";
	public static final String MenuTwoText1 = "Inserisci la tua email per ricevere informazioni sullo stato della tua richiesta:";
	public static final String RagioneValue = "HORNIG";
	public static final String NomeIntestatarioValue = "MONIKA";
	public static final String CFValue = "HRNMNK46H63Z112Q";
	public static final String MenuThreeText = "La richiesta di modifica del conto corrente per l'addebito diretto è stata inoltrata al tuo istituto di credito che dovrà autorizzarla. Puoi verificare lo stato della richiesta navigando nel dettaglio della tua fornitura.Ricordati che puoi modificare l'Addebito Diretto anche dall'APP di Enel Energia. Se ancora non ce l'hai, scaricala gratuitamente e scopri tutti i servizi e i vantaggi dedicati a te.";
	public static final String MenuThreeDescrition = "Operazione eseguita correttamente";
	
	
}
