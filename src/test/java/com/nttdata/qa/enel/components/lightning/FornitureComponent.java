package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class FornitureComponent extends BaseComponent{
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public By barraRicerca = By.xpath("//div[@id='labelDatagridB']//input");
	public By checkboxPrimaFornituraInTabella = By.xpath("//table//*[text()='COMMODITY']/ancestor::table[1]//input[@type='radio']/..");
	public By checkboxPrimaFornituraInTabellaFatturazione = By.xpath("//table[@id='ServiceAddressChange_Table']/tbody/tr//div[1]//label");
	public By btnConfermaIndirizzoFornitura	= By.xpath("//button[text()='Conferma Selezione Fornitura']");
	public By checkboxPrimaFornituraInTabellaModificaPotenzaTensione = 
			By.xpath("//table[@id='selectcommodityMPT_Table']/tbody/tr[1]//div[2]/label") ;
	public By avantiButton = By.xpath("//button[text()='Avanti']");
	
	
	
	
	public FornitureComponent(WebDriver driver)
	{
		super(driver);
		this.driver =driver ;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	public void cercaInTabella(String valoreRicerca) throws Exception{
		util.waitAndGetElement(barraRicerca)
		.sendKeys(valoreRicerca);
	}
	
	
	public void cercaInTabella(String frame,String valoreRicerca) throws Exception{
		util.frameManager(frame, barraRicerca, util.sendKeys, valoreRicerca);
	
	}

	public void cliccaPrimoElementoInTabella(By by) throws Exception{
		util.waitAndGetElement(by)
		.click();
	}
	
	public void cliccaPrimoElementoInTabella(String frame,By by) throws Exception{
	
		util.frameManager(frame, by, util.scrollAndClick);
	}
	
	public void cercaECliccaFornitura(String valoreRicerca,By by) throws Exception{
		cercaInTabella(valoreRicerca);
		spinner.checkSpinners();
		cliccaPrimoElementoInTabella(by);
		spinner.checkSpinners();
	}
	
	public String cercaECliccaFornituraConFrame(String valoreRicerca,By by) throws Exception{
		String frame = util.getFrameByIndex(1);
		spinner.checkSpinners(frame);
		spinner.checkSpinners();
		cercaInTabella(frame,valoreRicerca);
		spinner.checkSpinners(frame);
		TimeUnit.SECONDS.sleep(10);
		cliccaPrimoElementoInTabella(frame,by);
		spinner.checkSpinners(frame);
		return frame;
	}
	
	public void clickConfermaFornitura () throws Exception{
		util.objectManager(btnConfermaIndirizzoFornitura, util.scrollAndClick);
	}
	
	
	public void clickConfermaFornitura (String frame) throws Exception{
		util.frameManager(frame, btnConfermaIndirizzoFornitura, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}
	
	public void clickAvanti (String frame) throws Exception{
		util.frameManager(frame, avantiButton, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}
	
}
