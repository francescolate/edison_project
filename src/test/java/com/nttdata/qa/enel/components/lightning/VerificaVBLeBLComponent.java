package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaVBLeBLComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public VerificaVBLeBLComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  


	public final By clickCheckCliente = By.xpath("//button[@id='checkVBLButton']");
	public final By EsitoVBL = By.xpath("//label[text()='Esito VBL']/../div/img");
	public final By EsitoBLContatto = By.xpath("//label[text()='Esito Check Contatto']/../div/img");
	public final By ClickAvanti = By.xpath("//input[@value='Avanti' and contains(@id, 'principalForm')]");
	public final By ClickAvanti2 = By.xpath("//div/h3/button[text()='Quality check']/../..//input[@value='Avanti' and contains(@id, 'principalForm')]");

	public final By CheckOffertabilita = By.xpath("//button[text()='Check Offertabilità']");
	public final By ClickPrecheck = By.xpath("//button[text()='Precheck']");
	public final By SelectCheck = By.xpath("//option[text()='QC OK senza Nuova Registrazione']/..");
	public final By ClickConfermaEsito = By.xpath("//input[@value='Conferma Esito' and contains(@id, 'principalForm')]");
	public final By testoVBLKOAllaccio = By.xpath("//h2[text()='VBL KO']/ancestor::div[1]//span/p");
	public final By testoVBLKOPrimaAttivazione = By.xpath("//h2[text()='VBL KO']/ancestor::div[1]//span/p[contains(text(),'Gentile')]");
	public final By ClickChiudi = By.xpath("//button[text()='Chiudi']");
	public final String testoAllaccio = "Spettabile Cliente, da procedura è previsto un controllo credito per verificare se emergono condizioni ostative alla sottoscrizione del contratto: pertanto la preghiamo di attendere qualche istante.\n\nCliente: Va bene attendo in linea.\n\n"+
"Gentile Cliente a seguito della sua  richiesta e delle verifiche effettuate, le comunichiamo che:\n"+
"da un'analisi dei nostri archivi risulta una morosità di euro 1.472,67 alla date del 22-06-2018 riferita a precedente-i contratto-i a lei intestato-i, in virtù della quale non è intenzione di Enel Energia procedere all'attivazione della nuova fornitura (ELE O GAS).\n\n"+
"possibile obiezione del cliente -\nma come vi ho chiesto un contratto e non volete attivarmi?\n\n"+
"al quale l'operatore risponde:\n\n"+
"Sono spiacente ma Enel Energia non è obbligata a dar corso alle richieste di contratto di fornitura presentate dal cliente sul Mercato Libero. Se avesse smarrito le fatture ancora da pagare le fornisco immediatamente il saldo da versare e la modalità a Lei più congeniale per il pagamento:\n"+
"- con il bollettino allegato alla-e fattura-e in suo possesso con versamento sul c/c postale numero 76245836 o anche presso le ricevitorie del Lotto, le tabaccherie e i bar collegati alla rete Lottomatica Servizi e Sisal e i punti vendita Coop abilitati\n"+
"- a mezzo bonifico bancario IBAN IT18Y0306905020051014470177 indicando nella causale il numero della fattura, il numero cliente e-o il suo Codice Fiscale o Partita Iva\n\n"+
"La invitiamo ad inviarci una copia della documentazione dell'avvenuto versamento al numero di fax 800.903.727 e a ricontattarci per verificare la possibilità di sottoscrivere un nuovo contratto.\n\n"+
"Restiamo a disposizione per chiarimenti\n";
	
	public void clickCheckOffertabilita() throws Exception{
		
		driver.switchTo().defaultContent();

		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		WebElement table = null;
		WebElement temp = null;
		for (WebElement webElement : els) {
			driver.switchTo().frame(webElement);
			
			try {
				//ci sono più tabelle con questo id quindi verifico che ci sia una con almeno un risultato
				table = driver.findElement(By.xpath("//table[contains(@id, 'principalForm:insertedSuppliesTable')]/tbody/tr/ancestor::table"));
				temp = webElement;
				break;
			}catch(Exception e) {
				driver.switchTo().defaultContent();
			}
		}
		
		util.objectManager(ClickPrecheck, util.scrollToVisibility, false);
		driver.findElement(By.xpath("//input[@type='radio']/..")).click();
		
		TimeUnit.SECONDS.sleep(5);
		press(CheckOffertabilita);
		TimeUnit.SECONDS.sleep(30);
		if(util.exists(By.xpath("(//input[@type='radio']/..)[2]"), 5)) {
		
			driver.findElement(By.xpath("(//input[@type='radio']/..)[2]")).click();
			
			TimeUnit.SECONDS.sleep(5);
			press(CheckOffertabilita);
			TimeUnit.SECONDS.sleep(30);
		}
	}
	
	public void ClickPrecheck() throws Exception{
		
		driver.switchTo().defaultContent();

		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		WebElement table = null;
		for (WebElement webElement : els) {
			driver.switchTo().frame(webElement);
			try {
				//ci sono più tabelle con questo id quindi verifico che ci sia una con almeno un risultato
				table = driver.findElement(By.xpath("//table[contains(@id, 'principalForm:insertedSuppliesTable')]/tbody/tr/ancestor::table"));
			 break;
			}catch(Exception e) {
				driver.switchTo().defaultContent();
			}
		}
		
		driver.findElement(By.xpath("//input[@type='radio']/..")).click();
		
		TimeUnit.SECONDS.sleep(5);
		press(ClickPrecheck);
		TimeUnit.SECONDS.sleep(30);
			
		if(util.exists(By.xpath("(//input[@type='radio']/..)[2]"), 5)) {
		driver.findElement(By.xpath("(//input[@type='radio']/..)[2]")).click();
		
		TimeUnit.SECONDS.sleep(5);
		press(ClickPrecheck);
		TimeUnit.SECONDS.sleep(30);
		}
			
		
			
	}
	
	
	public void checkCliente() throws Exception{
		
		
//		driver.switchTo().defaultContent();
//
//		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//		boolean flag = false;
//		for (WebElement webElement : els) {
//			
//			
//			if(webElement.findElement(By.xpath("..")).isDisplayed()) {
//				driver.switchTo().frame(webElement);
//				break;
//			}	
//			
//		}
		
		util.getFrameActive();
		
		press(clickCheckCliente);
		
		
		
		/*
		String x ="";
		boolean flag = false;
		driver.switchTo().defaultContent();
		try {
			driver.findElement(clickCheckCliente).click();
		}catch(Exception e) {
			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
			for (WebElement webElement : els) {
				try {
				 driver.switchTo().defaultContent();
				 x = webElement.getAttribute("name");
				}catch(Exception e2) {continue;}
				if(!x.equals("")) {
					driver.switchTo().frame(webElement);
					try {
						driver.findElement(clickCheckCliente).click();
						flag = true;
						break;
					}catch(Exception e1) {}
				}
			}
			if(!flag) {
				throw new Exception ("Pulsante Check Cliente non trovato.");
			}
		}
		*/
	}
	
	public void clickAvanti() throws Exception{
		
//		driver.switchTo().defaultContent();
//
//		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//		boolean flag = false;
//		for (WebElement webElement : els) {
//			
//			
//			if(webElement.findElement(By.xpath("..")).isDisplayed()) {
//				driver.switchTo().frame(webElement);
//				break;
//			}	
//			
//		}
		util.getFrameActive();
		press(ClickAvanti);


		/*
		String x ="";
		boolean flag = false;
		driver.switchTo().defaultContent();
		try {
			driver.findElement(ClickAvanti).click();
		}catch(Exception e) {
			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
			for (WebElement webElement : els) {
				try {
				 driver.switchTo().defaultContent();
				 x = webElement.getAttribute("name");
				}catch(Exception e2) {continue;}
				if(!x.equals("")) {
					driver.switchTo().frame(webElement);
					try {
						driver.findElement(ClickAvanti).click();
						flag = true;
						break;
					}catch(Exception e1) {}
				}
			}
			if(!flag) {
				throw new Exception ("Pulsante Avanti non trovato.");
			}
		}
		*/
	}
	
	public void clickAvanti2() throws Exception{
		
		util.getFrameActive();
		press(ClickAvanti2);
/*
		String x ="";
		boolean flag = false;
		driver.switchTo().defaultContent();
		try {
			driver.findElement(ClickAvanti2).click();
		}catch(Exception e) {
			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
			for (WebElement webElement : els) {
				try {
				 driver.switchTo().defaultContent();
				 x = webElement.getAttribute("name");
				}catch(Exception e2) {continue;}
				if(!x.equals("")) {
					driver.switchTo().frame(webElement);
					try {
						driver.findElement(ClickAvanti2).click();
						flag = true;
						break;
					}catch(Exception e1) {}
				}
			}
			if(!flag) {
				throw new Exception ("Pulsante Avanti non trovato.");
			}
		}
		*/
	}
	
	public void clickConfermaEsito() throws Exception{
		
//		WebElement button=driver.findElement(ClickConfermaEsito);
//		JavascriptExecutor js = (JavascriptExecutor)driver;
//		js.executeScript("arguments[0].click();", button);
		util.getFrameActive();
		press(ClickConfermaEsito);
		
	}
	
	
	
	public void selezionaEsitoQC() throws Exception{
		driver.switchTo().defaultContent();
//		try {
//			WebElement select = driver.findElement(SelectCheck);
//		}catch(Exception e) {
//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			for (WebElement webElement : els) {
//				driver.switchTo().defaultContent();
//					driver.switchTo().frame(webElement);
//					try {
//						WebElement select = driver.findElement(SelectCheck);
//						break;
//					}catch(Exception e1) {}
//				
//			}
//		}
		util.getFrameActive();
		
		WebElement select = driver.findElement(SelectCheck);

		((JavascriptExecutor)driver).executeScript("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", select, "QC OK senza Nuova Registrazione");
		
		
		//Select se = new Select(driver.findElement(SelectCheck));
		//se.selectByVisibleText("QC OK senza Nuova Registrazione");
	}
	
	public void verificaEsitoVBL() throws Exception{
	
			
		
			String esito = driver.findElement(EsitoVBL).getAttribute("src");
			
			if(!esito.contains("/greenlight.png")) {
				throw new Exception ("Esito VBL atteso: OK, riscontrato: "+esito);
			}
			
			 esito = driver.findElement(EsitoBLContatto).getAttribute("src");
			
			if(!esito.contains("/greenlight.png")) {
				throw new Exception ("Esito BL Contatto atteso: OK, riscontrato: "+esito);
			}
			TimeUnit.SECONDS.sleep(5);
			
	}

	public void VerificaEsitiPrecheckOff() throws Exception{
		
	
		
//		driver.switchTo().defaultContent();
//
//		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//		boolean flag = false;
//		for (WebElement webElement : els) {
//			
//			
//			if(webElement.findElement(By.xpath("..")).isDisplayed()) {
//				driver.switchTo().frame(webElement);
//				break;
//			}	
//			
//		}
		util.getFrameActive();
		WebElement table = null;
		//ci sono più tabelle con questo id quindi verifico che ci sia una con almeno un risultato
		table = driver.findElement(By.xpath("//table[contains(@id, 'principalForm:insertedSuppliesTable')]/tbody/tr/ancestor::table"));

		/*driver.switchTo().defaultContent();

		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		WebElement table = null;
		for (WebElement webElement : els) {
			driver.switchTo().frame(webElement);
			try {
				//ci sono più tabelle con questo id quindi verifico che ci sia una con almeno un risultato
				table = driver.findElement(By.xpath("//table[contains(@id, 'principalForm:insertedSuppliesTable')]/tbody/tr/ancestor::table"));
			 break;
			}catch(Exception e) {
				driver.switchTo().defaultContent();
			}
		}
		
			*/
			
			String colName = "Esito Offertabilità";
			String xpathHeader = "/thead/tr/th/div";
			int count = util.getTableRowCount(table);
			
			for (int i = 1; i<=count;i++) {
				String out = util.getTableCellDataWithoutDiv(table, i, colName, "span");
				if(!out.equals("OK")) {
					throw new Exception("Esito Offertabilità Atteso:OK riscontrato: "+out+" alla riga: "+i);
				}
			}
			
			colName = "Esito Precheck";
			for (int i = 1; i<=count;i++) {
				String out = util.getTableCellDataWithoutDiv(table, i, colName, "span");
//				if(!out.equals("OK") && !out.equals("NA")) {
				if(out.equals("KO")) {
					throw new Exception("Esito Precheck Atteso:OK riscontrato: "+out+" alla riga: "+i);
				}
			}

	}

	
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void verificaVBL_KO_Ceazione_Offerta(By oggetto, String testoAtteso) throws Exception{
		util.waitAndGetElement(oggetto);
		String testo=driver.findElement(oggetto).getText();
		System.out.println("Testo visualizzato:"+testo);
		if(testo.compareTo(testoAtteso)!=0){
			throw new Exception("Il messaggio visualizzato non è quello atteso.Messaggio visualizzato:\n"+testo+"\n Messaggio atteso:/n"+testoAtteso);
		}
	}
	
	public void verificaVBL_KO_Ceazione_Offerta(By oggetto) throws Exception{
	if(!util.exists(oggetto, 180)){
		throw new Exception("La pop-up di verifica BL KO con relativo messaggio non è stata visualizzata");
	}
}
	
	
	
}
