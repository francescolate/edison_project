package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SelfCareSelectionComponent extends BaseComponent {
	public By titleLabel = By.cssSelector("body > div.remodal-wrapper.bsnBlue.remodal-is-opened > div > div.content.bsnBlue > div > div > div > div > div:nth-child(1) > h2");
	public By homeSelfCareLabel = By.xpath("//img[contains(@src,'priv_ico.svg')]//following-sibling::h4");
	public By businessSelfCareLabel = By.xpath("//img[contains(@src,'business_ico.svg')]//following-sibling::h4");
	public By homeSelfCareBtn = By.xpath("//a[@href='/it/area-clienti/residenziale']");
	public By privatoBtn = By.id("private");
	public By continuaBtn = By.id("continua-button-initial-modal");

	
	private By[] locators = {titleLabel, homeSelfCareLabel, businessSelfCareLabel};
	
	public SelfCareSelectionComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyElementsStrings() throws Exception {
		verifyElementsArrayText(locators, componentElementsStrings);
	}
	
	//----- Constants -----
	
	private String[] componentElementsStrings = {
			"Scegli come accedere alla tua area riservata.",
			"Area Clienti Casa",
			"Area Clienti Impresa"
	};
	
}
