package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Privato170ModIndFatturazioneACRComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public static String ITA_IFM_BillingAddress__c = "";
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By visualizzaLeBollette_button = By.xpath("(//dd[text()='Attiva']/ancestor::*/following-sibling::div/a[contains(text(),'Visualizza le bollette')])[1]");
	public By dettaglioFornitura_button = By.xpath("(//dd[text()='Attiva']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')])[1]");
	
	//servizi
	
	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
	public By serviziPage = By.xpath("//h2[text()='Servizi per le forniture']");
	public By serviziPageSubText = By.xpath("//*[text()='Servizi per le forniture']/parent::div/p[contains(text(),'Di seguito potrai visualizzare')]");
	public By modificaIndirizzoFatturazione_button = By.xpath("//h3[text()='Modifica Indirizzo Fatturazione']/parent::div");
	public By modificaDiIndirizzoFatturazione_button = By.xpath("//span[text()='Modifica indirizzo di fatturazione']/parent::button");
	
	public By modificaDiIndirizzoFatturazioneTitle = By.xpath("//h1[text()='Modifica Indirizzo di Fatturazione']");
	public By modificaDiIndirizzoFatturazioneSubTextDescription = By.xpath("//h2[contains(text(),'Seleziona una o')]");
	
	public By supplySelect = By.xpath("//span[text()='699869481']/ancestor::span/span[@id='checkbox-item-0']");
	public By numeroClienti = By.xpath("//span[text()='699869481']/ancestor::span/preceding-sibling::span[text()='Numero Cliente']");
	public By numeroClientiNumber = By.xpath("//span[text()='699869481']");
	
	public By supplySelect_310491181 = By.xpath("//span[text()='310491181']/ancestor::span/span[contains(@id,'checkbox-item')]");
	public By supplyGasIcon_310491181 = By.xpath("//span[text()='310491181']/ancestor::span/preceding-sibling::span/span[@class='icon-line-flame']");
	
	public By IndirizzoDella_310491181_Heading = By.xpath("(//span[text()='310491181']/ancestor::span/preceding-sibling::span)[3]/span[1]");
	public By IndirizzoDella_310491181_Value = By.xpath("(//span[text()='310491181']/ancestor::span/preceding-sibling::span)[3]/span[2]");
	
	public By numeroClienti_310491181 = By.xpath("(//span[text()='310491181']/ancestor::span/preceding-sibling::span)[4]");
	public By numeroClientiNumber_310491181 = By.xpath("//span[text()='310491181']");
	
	public By supplyGasIcon_310491181_Confirm = By.xpath("//span[text()='310491181']/ancestor::span/preceding-sibling::span[@class='icon-line-flame']");
	public By numeroClienti_310491181_Confirm = By.xpath("//span[text()='310491181']/preceding-sibling::span");
	
	
	// Supply - 300001597
	public By supplySelect_300001597 = By.xpath("//span[text()='300001597']/ancestor::span/span[contains(@id,'checkbox-item')]");
	public By supplyGasIcon_300001597 = By.xpath("//span[text()='300001597']/ancestor::span/preceding-sibling::span/span[@class='icon-line-flame']");
	
	public By IndirizzoDella_300001597_Heading = By.xpath("(//span[text()='300001597']/ancestor::span/preceding-sibling::span)[3]/span[1]");
	public By IndirizzoDella_300001597_Value = By.xpath("(//span[text()='300001597']/ancestor::span/preceding-sibling::span)[3]/span[2]");
	
	public By numeroClienti_300001597 = By.xpath("(//span[text()='300001597']/ancestor::span/preceding-sibling::span)[4]");
	public By numeroClientiNumber_300001597 = By.xpath("//span[text()='300001597']");
	
	public By supplyGasIcon_300001597_Confirm = By.xpath("//span[text()='300001597']/ancestor::span/preceding-sibling::span[@class='icon-line-flame']");
	public By numeroClienti_300001597_Confirm = By.xpath("//span[text()='300001597']/preceding-sibling::span");
	
	// Supply - 300001594
	public By supplySelect_300001594 = By.xpath("//span[text()='300001594']/ancestor::span/span[contains(@id,'checkbox-item')]");
	public By supplyGasIcon_300001594 = By.xpath("//span[text()='300001594']/ancestor::span/preceding-sibling::span/span[@class='icon-line-electricity']");
	
	public By IndirizzoDella_300001594_Heading = By.xpath("(//span[text()='300001594']/ancestor::span/preceding-sibling::span)[3]/span[1]");
	public By IndirizzoDella_300001594_Value = By.xpath("(//span[text()='300001594']/ancestor::span/preceding-sibling::span)[3]/span[2]");
	
	public By numeroClienti_300001594 = By.xpath("(//span[text()='300001594']/ancestor::span/preceding-sibling::span)[4]");
	public By numeroClientiNumber_300001594 = By.xpath("//span[text()='300001594']");
	
	public By supplyGasIcon_300001594_Confirm = By.xpath("//span[text()='300001594']/ancestor::span/preceding-sibling::span[@class='icon-line-electricity']");
	public By numeroClienti_300001594_Confirm = By.xpath("//span[text()='300001594']/preceding-sibling::span");
	//
	
	
	public By modificaDellIndirizzoFatturazioneTitle = By.xpath("//h1[contains(text(),'Modifica')]");
	
	public By inserimentoDati = By.xpath("//li[@class='step1']/span[@class='step-label active']");
	public By riepilogo_e_ConfermaDati = By.xpath("//li[@class='step2']/span[@class='step-label active']");
	
	public By riepilogo_e_ConfermaDati_Section1 = By.xpath("//h4[text()='Verifica la correttezza delle informazioni inserite:']");
	
	public By selectDropdown = By.xpath("//Select[@id='scegli-indirizzo']");
	
	public By cittaField = By.xpath("(//label[text()='Città'])[1]");
//	public By cittaFieldValue = By.xpath("(//label[contains(text(),'Città')])[1]/following-sibling::input");
	public By cittaFieldValue = By.xpath("(//label[contains(text(),'Città')])[2]/following-sibling::input");
	
//	-Toponomastica (optional field)  -  FROS
	public By toponomasticaField = By.xpath("//label[text()='Toponomastica']");
	public By toponomasticaFieldValue = By.xpath("//label[text()='Toponomastica']/following-sibling::select");
	
	//	-Presso (optional field)  -  FROS
	public By pressoField = By.xpath("//label[text()='Presso']");
	public By pressoFieldValue = By.xpath("//label[text()='Presso']/following-sibling::input");
	//	-Provincia (Required field) - FROSINONE 	 
	public By provinciaField = By.xpath("//label[text()='*Provincia']");
	public By provinciaFieldValue = By.xpath("//label[contains(text(),'Provincia')]/following-sibling::input");
	public By provinciaExpectedFieldValue = By.xpath("//dt[text()='Provincia']/following-sibling::dd");
	//	-Comune (Required field)  -  CASSINO
	public By comuneField = By.xpath("//label[text()='*Comune']");
	public By comuneFieldValue = By.xpath("//label[contains(text(),'Comune')]/following-sibling::input");
	//	-Località (optional field)  -  CASSINO
	public By localitaField = By.xpath("//label[text()='Località']");
	public By localitaFieldValue = By.xpath("//label[text()='Località']/following-sibling::input");
	public By localitaExpectedFieldValue = By.xpath("//dt[text()='Località']/following-sibling::dd");
	//	-Indirizzo (Required field)  - VIA MANTEGNA
	public By indirizzoField = By.xpath("(//label[text()='Indirizzo'])[1]");
	public By indirizzoFieldValue = By.xpath("(//label[contains(text(),'Indirizzo')])[1]");
	public By indirizzoExpectedFieldValue = By.xpath("//dt[contains(text(),'Indirizzo')]/following-sibling::dd");
	//	-Numero civico (Required field) - 1
	public By numeroCivicoField = By.xpath("//label[text()='Numero Civico*']");
	public By numeroCivicoFieldValue = By.xpath("//label[text()='Numero Civico*']/following-sibling::input");
	public By numeroCivicoExpectedFieldValue = By.xpath("//label[contains(text(),'Numero Civico')]/following-sibling::input");
	//	-CAP (field that will fill in itself following the enhancement of city and address)
	public By CAPField = By.xpath("//label[contains(text(),'CAP')]");
	public By CAPFieldValue = By.xpath("//label[contains(text(),'CAP')]/following-sibling::input");
	public By CAPFieldSelect = By.xpath("//li//div/span[text()='ROMA']");
	public By CAPExpectedFieldValue = By.xpath("//dt[text()='CAP']/following-sibling::dd");
	
	public By continuaButton = By.xpath("//span[text()='Continua']/parent::button");
	public By indietroButton = By.xpath("//span[text()='INDIETRO']/parent::button");
	public By confermaButton = By.xpath("//span[text()='Conferma']/parent::button");
	public By esciButton = By.xpath("//span[text()='Esci']/parent::button");
	
	//WB
	public By ITA_IFM_ConsumptionNumber__c_Column = By.xpath("//th[text()='ITA_IFM_ConsumptionNumber__c']");
	public By ITA_IFM_ConsumptionNumber__c_Value = By.xpath("//th[text()='ITA_IFM_ConsumptionNumber__c']/parent::tr[1]/following-sibling::tr/td[3]");
	
	public By ITA_IFM_BillingAddress__c_Column = By.xpath("//th[text()='ITA_IFM_BillingAddress__c']");
	public By ITA_IFM_BillingAddress__c_Value = By.xpath("//th[text()='ITA_IFM_BillingAddress__c']/parent::tr[1]/following-sibling::tr/td[4]");
	
	public By dailogTitle = By.xpath("//h3[@id='titleModal']");
	public By dailogClose = By.xpath("(//button[@onclick='focusCloseModale()'])[1]");
	
	
	public Privato170ModIndFatturazioneACRComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}
	
		public void VerifyTextNotMatch(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String textfield_found="YES";
			
			WebElement element = driver.findElement(checkObject);
			
			String actualtext = element.getText().replaceAll("(\r\n|\n)", "").replaceAll(",", "").replaceAll(" ", "");
			
			
			if (!(actualtext.contentEquals(Value)))
			textfield_found="NO";
							
			if (textfield_found.contentEquals("YES"))
				throw new Exception("The expected text" + Value + " is found:");
			}
	
	public void ValidatePrepopulatedFields() throws Exception
	{
		
		verifyComponentExistence(cittaFieldValue);
		checkPrePopulatedValueAndCompare(prepopulatedCittaFieldValue,CittaFieldValue);

		verifyComponentExistence(indirizzoFieldValue);
		checkPrePopulatedValueAndCompare(prepopulatedIndirizzoFieldValue,IndirizzoFieldValue);
		
		verifyComponentExistence(numeroCivicoFieldValue);
		checkPrePopulatedValueAndCompare(prepopulatedNumeroCivicoFieldValue,NumeroCivicoFieldValue);
		
		verifyComponentExistence(CAPFieldValue);
		checkPrePopulatedValueAndCompare(prepopulatedCAPboxValue,CAPboxValue);
		
	}
	
	public void ValidatePostpopulatedFields() throws Exception
	{
		
		verifyComponentExistence(provinciaExpectedFieldValue);
		VerifyText(provinciaExpectedFieldValue, ProvinciaFieldValue);
		
		verifyComponentExistence(localitaExpectedFieldValue);
		VerifyText(localitaExpectedFieldValue, LocalitaFieldValue);
		
		verifyComponentExistence(indirizzoExpectedFieldValue);
		VerifyText(indirizzoExpectedFieldValue, IndirizzoExpectedFieldValue);

		verifyComponentExistence(CAPExpectedFieldValue);
		VerifyText(CAPExpectedFieldValue, CAPboxValue);
		
	}
	
	public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
        
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        if(value.equals(""))throw new Exception("Field is not pre filled");
        
        String match = "FAIL";
        if(value.contentEquals(expectedValue))
        	match = "PASS";
        if(match.contentEquals("FAIL"))
        	throw new Exception("PrePopulatedValue and Expected values are not matching");
    }
		
	
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void verifyCheckboxnotSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (!element.isSelected()){
		checkbox="YES";
		}			
		if (element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is selected");
				}
	}
	
	
	public void verifyCheckboxSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (element.isSelected()){
		checkbox="YES";
		}			
		if (!element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is not selected");
				}
	}
	
	/*public void validateSupplySelection() throws Exception
	{
		
		verifyComponentExistence(supplySelect_310491181);
		verifyCheckboxnotSelected(supplySelect_310491181);
		
		verifyComponentExistence(supplyGasIcon_310491181);
		
		verifyComponentExistence(IndirizzoDella_310491181_Heading);
		VerifyText(IndirizzoDella_310491181_Heading, IndirizzoDella_310491181_HeadingText);
		
		verifyComponentExistence(IndirizzoDella_310491181_Value);
		VerifyText(IndirizzoDella_310491181_Value, IndirizzoDella_310491181_ValueText);
		
		verifyComponentExistence(numeroClienti_310491181);
		
		verifyComponentExistence(numeroClientiNumber_310491181);
		VerifyText(numeroClientiNumber_310491181, numeroClienti_310491181_Value);
		
	}*/
	/*public void validateSupplySelection() throws Exception
	{
		
		verifyComponentExistence(supplySelect_300001597);
		verifyCheckboxnotSelected(supplySelect_300001597);
		
		verifyComponentExistence(supplyGasIcon_300001597);
		
		verifyComponentExistence(IndirizzoDella_300001597_Heading);
		VerifyText(IndirizzoDella_300001597_Heading, IndirizzoDella_300001597_HeadingText);
		
		verifyComponentExistence(IndirizzoDella_300001597_Value);
		VerifyText(IndirizzoDella_300001597_Value, IndirizzoDella_300001597_ValueText);
		
		verifyComponentExistence(numeroClienti_300001597);
		
		verifyComponentExistence(numeroClientiNumber_300001597);
		VerifyText(numeroClientiNumber_300001597, numeroClienti_300001597_Value);
		
	}*/
	
	public void validateSupplySelection() throws Exception
	{
		
		verifyComponentExistence(supplySelect_300001594);
		verifyCheckboxnotSelected(supplySelect_300001594);
		
		verifyComponentExistence(supplyGasIcon_300001594);
		
		verifyComponentExistence(IndirizzoDella_300001594_Heading);
		VerifyText(IndirizzoDella_300001594_Heading, IndirizzoDella_300001594_HeadingText);
		
		verifyComponentExistence(IndirizzoDella_300001594_Value);
		VerifyText(IndirizzoDella_300001594_Value, IndirizzoDella_300001594_ValueText);
		
		verifyComponentExistence(numeroClienti_300001594);
		
		verifyComponentExistence(numeroClientiNumber_300001594);
		VerifyText(numeroClientiNumber_300001594, numeroClienti_300001594_Value);
		
	}
	
	public void setFieldValues(String capField, String indirizzoField, String numeroCivicoField) throws Exception
	{
		/*verifyComponentExistence(CAPFieldValue);
		enterInput(CAPFieldValue, capField);
		verifyComponentExistence(CAPFieldSelect);
		clickComponent(CAPFieldSelect);*/
		verifyComponentExistence(indirizzoFieldValue);
		enterInput(indirizzoFieldValue, indirizzoField);
		verifyComponentExistence(numeroCivicoFieldValue);
		enterInput(numeroCivicoFieldValue, numeroCivicoField);
//		ExtractBillingAddress();  // Billing address
	}
	
	/*public void confirmSupplySelection() throws Exception
	{
		
		verifyComponentExistence(supplyGasIcon_310491181_Confirm);
		
		verifyComponentExistence(IndirizzoDella_310491181_Heading);
		VerifyText(IndirizzoDella_310491181_Heading, IndirizzoDella_310491181_HeadingText);
		
		verifyComponentExistence(IndirizzoDella_310491181_Value);
		VerifyText(IndirizzoDella_310491181_Value, IndirizzoDella_310491181_ValueText);
		
		verifyComponentExistence(numeroClienti_310491181_Confirm);
		
		verifyComponentExistence(numeroClientiNumber_310491181);
		VerifyText(numeroClientiNumber_310491181, numeroClienti_310491181_Value);
		
	}*/
	/*public void confirmSupplySelection() throws Exception
	{
		
		verifyComponentExistence(supplyGasIcon_300001597_Confirm);
		
		verifyComponentExistence(IndirizzoDella_300001597_Heading);
		VerifyText(IndirizzoDella_300001597_Heading, IndirizzoDella_300001597_HeadingText);
		
		verifyComponentExistence(IndirizzoDella_300001597_Value);
		VerifyText(IndirizzoDella_300001597_Value, IndirizzoDella_300001597_ValueText);
		
		verifyComponentExistence(numeroClienti_300001597_Confirm);
		
		verifyComponentExistence(numeroClientiNumber_300001597);
		VerifyText(numeroClientiNumber_300001597, numeroClienti_300001597_Value);
		
	}*/
	
	public void confirmSupplySelection() throws Exception
	{
		
		verifyComponentExistence(supplyGasIcon_300001594_Confirm);
		
		verifyComponentExistence(IndirizzoDella_300001594_Heading);
		VerifyText(IndirizzoDella_300001594_Heading, IndirizzoDella_300001594_HeadingText);
		
		verifyComponentExistence(IndirizzoDella_300001594_Value);
		VerifyText(IndirizzoDella_300001594_Value, IndirizzoDella_300001594_ValueText);
		
		verifyComponentExistence(numeroClienti_300001594_Confirm);
		
		verifyComponentExistence(numeroClientiNumber_300001594);
		VerifyText(numeroClientiNumber_300001594, numeroClienti_300001594_Value);
		
	}
	
	public void ValidateDilogPopup()
	{
		if(driver.findElement(dailogTitle).isDisplayed())
			driver.findElement(dailogClose).click();
		
	}
	
	
	public void selectValue(By checkboxObject,String value) throws Exception {
		
		WebElement dropdown = driver.findElement(checkboxObject);
		Select select = new Select(dropdown); 
		select.selectByValue(value);
		ITA_IFM_BillingAddress__c =  select.getFirstSelectedOption().getText().replaceAll("(\r\n|\n)", "").replaceAll(" ", "");
		System.out.println(ITA_IFM_BillingAddress__c);
		}
	
	public void ExtractBillingAddress() throws Exception {
		
		Select select = new Select(driver.findElement(toponomasticaFieldValue));
		String toponomasticaValue =  select.getFirstSelectedOption().getText().replaceAll("(\r\n|\n)", "").replaceAll(" ", "");
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String indirizzoValue = (String)js.executeScript("return (document.evaluate(\""+indirizzo_Xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		String numeroCivicoValue = (String)js.executeScript("return (document.evaluate(\""+numero_Xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		String CAPValue = (String)js.executeScript("return (document.evaluate(\""+CAP_Xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		String cittaValue = (String)js.executeScript("return (document.evaluate(\""+citta_Xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");

		ITA_IFM_BillingAddress__c =  toponomasticaValue.concat(indirizzoValue).concat(numeroCivicoValue).concat(CAPValue).concat(cittaValue).replaceAll("(\r\n|\n)", "").replaceAll(" ", "");
		System.out.println(ITA_IFM_BillingAddress__c);
			
		}
	
	
	public static final String ModificaIndirizzoFatturazioneSubTextDescription = "Seleziona una o più forniture su cui intendi modificare l'indirizzo di fatturazione. Qualora non fosse attivo il servizio di Bolletta Web, in caso di modifica, verrà utilizzato come indirizzo di recapito delle bollette e di eventuali comunicazioni.";

	public static final String prepopulatedCittaFieldValue = "(//label[contains(text(),'Città')])[1]/following-sibling::input";
	public static final String prepopulatedIndirizzoFieldValue = "(//label[contains(text(),'Indirizzo')])[1]/following-sibling::input";
	public static final String prepopulatedNumeroCivicoFieldValue = "//label[contains(text(),'Numero Civico*')]/following-sibling::input";
	public static final String prepopulatedCAPboxValue = "//label[contains(text(),'CAP')]/following-sibling::input";
	
	
	public static final String postpopulatedProvinciaFieldValue = "//dt[text()='Provincia']/following-sibling::dd";
	public static final String postpopulatedLocalitaFieldValue = "//dt[text()='Località']/following-sibling::dd";
	public static final String postpopulatedIndirizzoFieldValue = "//dt[text()='Indirizzo']/following-sibling::dd";
	public static final String postpopulatedCAPboxValue = "//dt[text()='CAP']/following-sibling::dd";
	
	public static final String PressoFieldValue = "";
//	public static final String ProvinciaFieldValue = "BARI";
	public static final String ProvinciaFieldValue = "ROMA";
//	public static final String CittaFieldValue = "GIOVINAZZO (BARI) PUGLIA";
//	public static final String CittaFieldValue = "";
//	public static final String LocalitaFieldValue = "GIOVINAZZO";
	public static final String LocalitaFieldValue = "MENTANA";
//	public static final String IndirizzoFieldValue = "GROSSIS";
//	public static final String IndirizzoExpectedFieldValue = "Vicolo Grossis,17";
	public static final String IndirizzoExpectedFieldValue = "Via Lazio,14";
//	public static final String NumeroCivicoFieldValue = "17";
//	public static final String CAPboxValue = "70054";
	
	public static final String CittaFieldValue = "MENTANA (RM) LAZIO";
	public static final String CAPboxValue = "00013";
	public static final String IndirizzoFieldValue = "VIA LAZIO";
	public static final String NumeroCivicoFieldValue = "14";
	
	public static final String numeroClienti_310491181_Value = "310491181"; 
	public static final String IndirizzoDella_310491181_HeadingText = "Indirizzo della fornitura"; 
	public static final String IndirizzoDella_310491181_ValueText  =  "Via Alberico Da Barbiano 1 20161 Milano Milano Mi";
	
	public static final String numeroClienti_300001597_Value = "300001597"; 
	public static final String IndirizzoDella_300001597_HeadingText = "Indirizzo della fornitura"; 
	public static final String IndirizzoDella_300001597_ValueText  =  "Via Virginia 12 A 40017 San Matteo Della Decima San Giovanni In Persiceto Bo";
	
	public static final String numeroClienti_300001594_Value = "300001594"; 
	public static final String IndirizzoDella_300001594_HeadingText = "Indirizzo della fornitura"; 
	public static final String IndirizzoDella_300001594_ValueText  =  "Via Virginia 12 A 40017 San Matteo Della Decima San Giovanni In Persiceto Bo";
	
	public static final String citta_Xpath = "(//label[contains(text(),'Città')])[2]/following-sibling::input";
	public static final String indirizzo_Xpath = "(//label[contains(text(),'Indirizzo*')])/following-sibling::input";
	public static final String numero_Xpath = "//label[text()='Numero Civico*']/following-sibling::input";
	public static final String CAP_Xpath = "//label[contains(text(),'CAP')]/following-sibling::input";
	public String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
}










