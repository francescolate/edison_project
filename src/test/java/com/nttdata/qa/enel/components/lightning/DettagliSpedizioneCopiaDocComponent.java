package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.text.SimpleDateFormat;
import java.util.*;


public class DettagliSpedizioneCopiaDocComponent extends BaseComponent {

    WebDriver driver;
    SeleniumUtilities util;

    public By buttonConfermaDettagliSpedizione = By.xpath("//button[@name='Conferma' and text()='Conferma']");
    public final By buttonConferma = new By.ByXPath("//button[text()='Conferma']");
    public final By numero_documento = new By.ByXPath("//p[text()='Numero caso']/ancestor::div[1]//lightning-formatted-text");
    public final By stato_documento = new By.ByXPath("//p[text()='Stato']/ancestor::div[1]//lightning-formatted-text");
    public final By sottostato_documento = new By.ByXPath("//span[text()='Sottostato']/..//div/span");
    public final By richiesta_documento = new By.ByXPath("//h1[text()='Copia documenti']//ancestor::div[@class='slds-media__body']/h2[@class='slds-text-heading--label']");
    public final By indirizzo_mail = new By.ByXPath("//*[contains(text(),'Indirizzo Email')]//ancestor::div[@role='none']//input[contains(@id, 'Indirizzo Email')]");
    public  By indirizzo_mail1 =  By.xpath("//label[text()='Indirizzo Email']/..//input");
    public final By indirizzo_pec = new By.ByXPath("//*[contains(text(),'Indirizzo Pec')]//ancestor::div[@role='none']//input[contains(@id, 'Indirizzo Pec')]");
    public  By indirizzo_pec1 =  By.xpath("//label[text()='Indirizzo Pec']/..//input");
    public final By indirizzo_fax = new By.ByXPath("//*[contains(text(),'Numero Fax')]//ancestor::div[@role='none']//input[contains(@id, 'Numero Fax')]");
    public  By indirizzo_fax1 =  By.xpath("//label[text()='Numero Fax']/..//input");
    
    //da utilizzare per scenario 2 copia doc
    public final By button_NO_popup = new By.ByXPath("//button[text()='NO']");
    public final By tipologia_DMS = new By.ByXPath("//label[contains(text(),'Tipologia DMS')]//ancestor::div[@role='none']//input");
    public final By buttonCercaInbound = new By.ByXPath("//button[@name='Cerca']");
    public final By buttonRicercaInbound = new By.ByXPath("//span[text()='Inbound']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public String colonneSezioneInbound[] = {"Id Documento", "Numero Attività", "Tipologia DMS", "Data Ricezione", "Commento"};
    public final By radioButtonSezioneInbound = new By.ByXPath("//span[text()='Inbound']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public By buttonConfermaSezioneInbound = By.xpath("//span[text()='Inbound']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public final By buttonRicercaOubound = new By.ByXPath("//span[text()='Outbound']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public String colonneSezioneOutbound[] = {"Id Documento", "Modello", "Data Inoltro", "Processo"};
    public final By radioButtonSezioneOutbound = new By.ByXPath("//span[text()='Outbound']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public By buttonConfermaSezioneOutbound = By.xpath("//span[text()='Outbound']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public final By buttonRicercaDimPagamento = new By.ByXPath("//span[text()='Dimostrato Pagamento']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public final By buttonRicercaFatture = new By.ByXPath("//span[text()='Fatture']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public final By buttonRicercaPianoDiRientro = new By.ByXPath("//span[text()='Piano Di Rientro']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public final By buttonRicercaEstrattoConto = new By.ByXPath("//span[text()='Estratto Conto']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public final By buttonRicercaPreventivi = new By.ByXPath("//span[text()='Preventivi']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public String colonneSezionePianoDiRientro[] = {"Numero PDR", "Rate", "Importo", "Data Inizio", "Data Fine"};
    public String colonneSezionePreventivi[] = {"Id Protocollo", "Numero Attività", "Causale Trasporto", "Importo", "Data Richiesta"};
    public String colonneSezioneEstrattoConto[] = {"Id Documento", "Tipologia", "Id Richiesta", "Data Richiesta", "Data Inizio", "Data Fine", "Stato Pagamento"};
    public String colonneSezioneFatture[] = {"Numero Fattura", "Scadenza", "Emissione", "Pagamento", "Importo"};
    public String colonneSezioneDimPagamento[] = {"Id Documento", "Data Pagamento", "Modalità Pagamento", "Importo Pagato", "Numero Fattura"};
    public final By radioButtonSezioneDimPagamento = new By.ByXPath("//span[text()='Dimostrato Pagamento']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public final By radioButtonSezioneFatture = new By.ByXPath("//span[text()='Fatture']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public final By radioButtonSezionePianoDiRientro = new By.ByXPath("//span[text()='Piano Di Rientro']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public final By radioButtonSezionePreventivi = new By.ByXPath("//span[text()='Preventivi']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public final By radioButtonSezioneEstrattoConto = new By.ByXPath("//span[text()='Estratto Conto']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public By buttonConfermaSezioneFatture = By.xpath("//span[text()='Fatture']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By buttonConfermaSezioneDimPagamento = By.xpath("//span[text()='Dimostrato Pagamento']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By buttonConfermaPianoDiRientro = By.xpath("//span[text()='Piano Di Rientro']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By buttonConfermaPreventivi = By.xpath("//span[text()='Preventivi']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By buttonConfermaEstrattoConto = By.xpath("//span[text()='Estratto Conto']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By indirizzoVerificato = By.xpath("//h2//span[text()='Dettagli Spedizione']/ancestor::div[@role='region']//label[text()='Indirizzo Verificato']");
    public By buttonConfermaIndirizzi = By.xpath("//h2//span[text()='Dettagli Spedizione']/ancestor::div[@role='region']//button[text()='Conferma']");


    public String salvaNumeroDocumento() throws Exception {
        String numerocontratto = util.waitAndGetElement(numero_documento, true).getAttribute("value");
        return numerocontratto;
    }

    public void enterDMSType(String tipologia_DMS) throws Exception {
        util.selectLighningValue("Tipologia DMS", tipologia_DMS);
    }

    public void enterModelType(String modello) throws Exception {
        util.selectLighningValue("Modello", modello);
    }

    public void enterDeliveryChannel(By insertObject, String valore) throws Exception {
        util.objectManager(insertObject, util.sendKeys, valore);
    }

    public void checkNumeroRighe(int riga, Properties prop) throws Exception {
        List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
        if (riga != rows.size())
            throw new Exception("Numero di righe ottenuto diverso da numero di righe atteso: righe restituite dalla query (" + rows.size() + ")");
    }

    public String getStatoDocumento() throws Exception {
        String statodocumento = util.waitAndGetElement(stato_documento, true).getAttribute("value");
        return statodocumento;
    }

    public String getSottoStatoDocumento() throws Exception {
        String sottostatodocumento = util.waitAndGetElement(sottostato_documento, true).getAttribute("title");
        return sottostatodocumento;
    }

    public DettagliSpedizioneCopiaDocComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
    }

    public void inserisciCanaleInvio(String canale_invio) throws Exception {
        util.selectLighningValue("Canale Invio", canale_invio);
    }


    public void verificaStatoSottostatoDocumento(String stato, String sottostato) throws Exception {
        String statDoc = getStatoDocumento();
        String sottostatDoc = getSottoStatoDocumento();
        if (!stato.toLowerCase().trim().contentEquals(statDoc.trim().toLowerCase()))
            throw new Exception("Lo stato della Richiesta risulta diverso da : " + stato + ". Stato riscontrato: " + statDoc);
        if (!sottostato.toLowerCase().trim().contentEquals(sottostatDoc.trim().toLowerCase()))
            throw new Exception("Il sottostato della Richiesta risulta diverso da : " + sottostato + ". Stato riscontrato: " + sottostatDoc);
    }

    public void checkModalitàDuplicato(String modalità) throws Exception {
        String modalitàText = util.waitAndGetElement(By.xpath("//input[@role='listbox']"), true).getAttribute("value");
        if (modalitàText.compareTo(modalità) != 0)
            throw new Exception("Modalità Duplicato è diverso da: " + modalità + "valore mostrato: " + modalitàText);
    }


    public void verifyComponentExistence(By existingObject) throws Exception {
        if (!util.exists(existingObject, 15))
            throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
    }

    public void clickComponent(By clickableObject) throws Exception {
        util.objectManager(clickableObject, util.scrollToVisibility, false);
        Thread.sleep(5000);
        util.objectManager(clickableObject, util.scrollAndClick);
    }
    // 2020-09-10

    public String getSysdateDoc() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String data = simpleDateFormat.format(calendar.getTime()).toString();
        return data;
    }
}
