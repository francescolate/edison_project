package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Privato180ModIndFatturazioneACBComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By areaRiservata = By.xpath("//ol[@class='breadcrumb']");
	public By pageHeading = By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
	
	public By serviziSelect = By.xpath("//li/a[text()='servizi']");
	public By serviziHeading = By.xpath("//h1[text()='Servizi']");
	public By serviziPageSubText = By.xpath("//h4[text()='In questa pagina trovi tutti i servizi.']");

	public By modificaIndirizzoDiFatturazione_button = By.xpath("//p[contains(text(),'Modifica Indirizzo di Fatturazione')]/parent::div");
	public By indirizzoDiFatturazionePageTitle = By.xpath("//h1[contains(text(),'indirizzo di fatturazione della')]");
	public By indirizzoDiFatturazionePageSubSectionHeadings = By.xpath("//div[@class='panel-body']//h2");
	public By modificaButton = By.xpath("//a[text()='Modifica']");

	public By ModificaIndirizzoDiFatturazioneHeading = By.xpath("//h1[contains(text(),'Modifica Indirizzo di Fatturazione')]");

	public By inserimentoDati = By.xpath("//ul[@class='steps']/li[1]/span[2]");
	public By riepilogo_e_ConfermaDati = By.xpath("//ul[@class='steps']/li[2]/span[2]");
	
	public By customerSelect = By.xpath("//div[@class='customselect-container']");
	public By customerSelectDropdownFeild = By.xpath("(//ul[@class='select2-results__options']/li)[1]");
	
	public By pressoField = By.xpath("//label[text()='Presso']");
	public By pressoFieldValue = By.xpath("//label[text()='Presso']/following-sibling::input");
 	 
	public By provinciaField = By.xpath("//label[text()='*Provincia']");
	public By provinciaFieldValue = By.xpath("//label[contains(text(),'Provincia')]/following-sibling::input");

	public By comuneField = By.xpath("//label[text()='*Comune']");
	public By comuneFieldValue = By.xpath("//label[contains(text(),'Comune')]/following-sibling::input");

	public By localitaField = By.xpath("//label[text()='Località']");
	public By localitaFieldValue = By.xpath("//label[text()='Località']/following-sibling::input");

	public By indirizzoField = By.xpath("(//label[text()='*Indirizzo'])[1]");
	public By indirizzoFieldValue = By.xpath("(//label[contains(text(),'Indirizzo')])[1]/following-sibling::input");

	public By numeroCivicoField = By.xpath("//label[text()='*Numero Civico']");
	public By numeroCivicoFieldValue = By.xpath("//label[text()='*Numero Civico']/following-sibling::div/input");
	public By numeroCivicoExpectedFieldValue = By.xpath("//label[contains(text(),'Numero Civico')]/following-sibling::input");

	public By CAPField = By.xpath("//label[text()='CAP']");
	public By CAPFieldValue = By.xpath("//label[text()='CAP']/following-sibling::input");
	public By CAPFieldSelect = By.xpath("(//span[text()='ROMA'])[2]");
	
	public By ToponomasticaFieldSelect = By.xpath("//span[text()='Toponomastica']/parent::label/following-sibling::select");

	public By esciButton = By.xpath("//a[@title='Esci']");
	public By indietroButton = By.xpath("//button[@title='Prosegui']/preceding-sibling::a[text()='Indietro']");
	public By proseguiButton = By.xpath("//button[@title='Prosegui']");
	
	public By errorMessage = By.xpath("//p[contains(text(),'Correggi')]");
	public By errorLink = By.xpath("//p[contains(text(),'Correggi')]/a");
	
	public By ConfirmSupply = By.xpath("(//table[@class='table'])[2]");
	
	
	public Privato180ModIndFatturazioneACBComponent(WebDriver driver)
	{
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
        
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        if(value.equals(""))throw new Exception("Field is not pre filled");
        
        String match = "FAIL";
        if(value.contentEquals(expectedValue))
        	match = "PASS";
        if(match.contentEquals("FAIL"))
        	throw new Exception("PrePopulatedValue and Expected values are not matching");
    }
	
	public void validateIndirizzoDiFatturazionePageSections(By checkObject) throws Exception
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		List<WebElement> sectionHeadings = driver.findElements(checkObject);
		
		String textfield_found="NO";
		
		for (WebElement section : sectionHeadings) {
			
			String sectionTextValue=section.getText();
			
			if (sectionTextValue.contentEquals("I tuoi dati")) {
				textfield_found="YES";
			}else if (sectionTextValue.contentEquals("Il tuo indirizzo di fatturazione")) {
				textfield_found="YES";
			}else if (sectionTextValue.contentEquals("Le specifiche tecniche della tua fornitura")) {
				textfield_found="YES";
			}else if (sectionTextValue.contentEquals("Il tuo contratto")) {
				textfield_found="YES";
			}  
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception(" Mismatch with 'Indirizzo Di Fatturazione' SECTION HEADING text");
		
		}
	}
	public void selectDropDownByValue(By checkObject, String Value )throws Exception {
		
		WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(checkObject));
        
        String valueFound = "NO";
        
		Select select = new Select(driver.findElement(checkObject));
		
		List<WebElement> options = select.getOptions();
	    for (WebElement we : options) {
	        if (we.getText().equals(Value)) {
	        	valueFound = "Yes";
	        	select.selectByValue(Value);
	                
	                }
	    }
	    if (valueFound.contentEquals("NO"))
			throw new Exception("Option not found to select");
	}
		
	public void validateFields() throws Exception
	{
		verifyComponentExistence(pressoField);
		VerifyText(pressoField, "Presso");
		
		verifyComponentExistence(pressoFieldValue);
		
		verifyComponentExistence(provinciaField);
		VerifyText(provinciaField, "*Provincia");
		
		verifyComponentExistence(provinciaFieldValue);
		
		verifyComponentExistence(comuneField);
		VerifyText(comuneField, "*Comune");
		
		verifyComponentExistence(comuneFieldValue);
		
		verifyComponentExistence(localitaField);
		VerifyText(localitaField, "Località");
		
		verifyComponentExistence(localitaFieldValue);
		
		verifyComponentExistence(indirizzoField);
		VerifyText(indirizzoField, "*Indirizzo");
		
		verifyComponentExistence(indirizzoFieldValue);
		
		verifyComponentExistence(numeroCivicoField);
		VerifyText(numeroCivicoField, "*Numero Civico");
		
		verifyComponentExistence(numeroCivicoFieldValue);
		
		verifyComponentExistence(CAPField);
		VerifyText(CAPField, "CAP");
		
		verifyComponentExistence(CAPFieldValue);
		
	}
	
	public void ValidatePrepopulatedFields(By checkObject, String ExpectedValue, String PopulatedValue) throws Exception
	{
		
		verifyComponentExistence(provinciaFieldValue);
		checkPrePopulatedValueAndCompare(populatedProvinciaFieldValue,ProvinciaFieldValue);
		
		verifyComponentExistence(comuneFieldValue);
		checkPrePopulatedValueAndCompare(populatedComuneFieldValue,ComuneFieldValue);
		
		verifyComponentExistence(localitaFieldValue);
		checkPrePopulatedValueAndCompare(populatedLocalitaFieldValue,LocalitaFieldValue);
		
		verifyComponentExistence(indirizzoFieldValue);
		checkPrePopulatedValueAndCompare(populatedIndirizzoFieldValue,ExpectedValue);
		
		verifyComponentExistence(checkObject);
		checkPrePopulatedValueAndCompare(PopulatedValue,NumeroCivicoFieldValue);
		
		verifyComponentExistence(CAPFieldValue);
		checkPrePopulatedValueAndCompare(populatedCAPboxValue,CAPboxValue);
		
	}
	
	public void ConfirmSupply(By checkObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("(//table[@class='table'])[2]/thead/tr//b"));
		List<WebElement> value = driver.findElements(By.xpath("(//table[@class='table'])[2]/tbody/tr/td"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("((//table[@class='table'])[2]/thead/tr//b)["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("(//table[@class='table'])[2]/tbody/tr/td["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Tipo") && descriptionValue.contentEquals("Luce")) {
				textfield_found="YES";
			}else if (description.contentEquals("Numero Cliente / Alias") && descriptionValue.contentEquals("Prova BSN")) {       //descriptionValue.contentEquals("981836202")
				textfield_found="YES";
			}else if (description.contentEquals("Indirizzo di fornitura") && descriptionValue.contentEquals("Via Padule 10a 25035 Ospitaletto Ospitaletto Bs")) {  //descriptionValue.contentEquals("Via Cellini 2 03043 Cassino Cassino Fr")
				textfield_found="YES";
			} 
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Supply details are not matching for index :"+i);
			
		}
		
	}
	
	
	public String errorMessageText = "Correggi l’indirizzo e clicca su \"Prosegui\" oppure CLICCA QUI";
	public String errorMessageLink = "CLICCA QUI";
	
	public String populatedProvinciaFieldValue = "//label[contains(text(),'Provincia')]/following-sibling::input";
	public String populatedComuneFieldValue = "//label[contains(text(),'Comune')]/following-sibling::input";
	public String populatedLocalitaFieldValue = "//label[text()='Località']/following-sibling::input";
	public String populatedIndirizzoFieldValue = "(//label[contains(text(),'Indirizzo')])[1]/following-sibling::input";
	public String populatedNumeroCivicoFieldValue = "//label[text()='*Numero Civico']/following-sibling::div/input";
	public String populatedNumeroCivicoExpectedFieldValue = "//label[contains(text(),'Numero Civico')]/following-sibling::input";
	public String populatedCAPboxValue = "//label[text()='CAP']/following-sibling::input";
	
	public String ProvinciaFieldValue = "ROMA"; //"FROSINONE";
	public String ComuneFieldValue = "ROMA";//"CASSINO";
	public String LocalitaFieldValue = "ROMA";//"CASSINO";
	public String IndirizzoFieldValue = "Via Umberto Smaila";//"VIA MANTEGNA";
	public String IndirizzoExpectedFieldValue = "VIA VIA UMBERTO SMAILA";//"VIA ANDREA MANTEGNA";
	public String NumeroCivicoFieldValue = "1";
	public String CAPboxValue = "00198";//"03043";
}