package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class IdentificazioneInterlocutoreComponentEVO extends BaseComponent implements SFDCBox {

    WebDriver driver;
    SeleniumUtilities util;
    Actions actions;
    SpinnerManager spinner;

    public By podInput = new By.ByXPath("//span[text()='POD/PDR']/parent::*/following-sibling::div/input");
    public By indirizzoDiFornituraInput = new By.ByXPath("//div[@id='callerIdentBody']"
            + "/..//span[text()='Indirizzo di fornitura']"
            + "/../following-sibling::div/input");
    public By documentoIdentita = new By.ByXPath("//label[contains(text(),'Documento')]/../div/input");
    public By NumeroCliente = By.xpath("//span[text()='Numero Cliente']/../..//input");
    public By confirmButton = new By.ByXPath("//h2[text()='Identificazione interlocutore']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By confirmButton2 = new By.ByXPath("//h2[text()='Documento di identità del titolare']/ancestor::div[@role='main']//button[text()='Conferma']");
    public By confirmButton3 = new By.ByXPath("//h2[text()='Delegato']/ancestor::div[@role='main']//button[text()='Conferma']");
    public By buttonChecklis = By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma']");
    public By ImportoUltimaFattura = By.xpath("//span[text()='Importo ultima fattura']/../..//input");
    public By conferma = By.xpath("//div[@class='slds-card']//button[@name='Confirm']");
    public By conferma1 = By.xpath("//div[@class='slds-button slds-button_brand']//button[@name='Conferma']");
    public By confermaFinaleInBasso = By.xpath("//div[@class='slds-p-top_small']//button[text()='Conferma' and @name='Confirm']");

    public By confermaDocumento = By.xpath("//h2[text()='Documento di identità del titolare']/ancestor::div[@role='region']//button[text()='Conferma']");
    
    public By confermaDocumentoIdentità = By.xpath("//div[@class='slds-button slds-button_brand']//button[text()='Conferma']");

    public By campoNome = By.xpath("//label[text()='Nome']/following-sibling::div/input[@disabled]");
    public By campoCognome = By.xpath("//label[text()='Cognome']/following-sibling::div/input[@disabled]");
    public By campoCF = By.xpath("//label[text()='Codice Fiscale']/following-sibling::div/input[@disabled]");
    public By confermaLogin = By.xpath("//button[@name='checklistConferma']");

    //INDENTIFICAZIONE INTERLOCUTORE ID16
    public By input_tipo_documento = By.xpath("//label[text()='Tipo documento']/ancestor::slot[1]//input[@role='listbox']/parent::*");
    /* */
    
    /* */
    
    public By pickuplist_tipo_documento = By.xpath("//label[text()='Tipo documento']/ancestor::slot[1]//*[contains(text(),\"Carta\")]");
    
    public By pickuplist_tipo_documento_patente = By.xpath("//label[text()='Tipo documento']/ancestor::slot[1]//*[contains(text(),\"Patente\")]");

    public By input_numero_documento = By.xpath("//label[text()='Numero documento']/ancestor::slot[1]//input/parent::*");
    public By input_rilasciato_da = By.xpath("//label[text()='Rilasciato da']/ancestor::slot[1]//input/parent::*");
    public By input_rilasciato_il = By.xpath("//label[text()='Rilasciato il']/ancestor::slot[1]//input/parent::*");
    public By button_sysdate = By.xpath("//label[text()='Rilasciato il']/ancestor::slot[1]//button[text()='Oggi']");
    public By button_conferma = By.xpath("//h2[text()='Documento di identità del titolare']/ancestor::div[@role='region']//button[text()='Conferma']");
    
    
    		
    
    public By button_conferma_2 = By.xpath("//button[text()='Conferma']/ancestor::div[@class='slds-card' and not(contains(.,'Documento di identità del titolare'))]//button[text()='Conferma'][not(@disabled)]");
    public By confermaPagina = By.xpath("//lightning-button[@data-id='Confirm']");

    public IdentificazioneInterlocutoreComponentEVO(WebDriver driver) {
        super(driver);
        this.driver = driver;
        util = new SeleniumUtilities(driver);
        actions = new Actions(driver);
        spinner = new SpinnerManager(driver);

    }

    public void insertPod(String pod) throws Exception {
        //TimeUnit.SECONDS.sleep(20);
        spinner.checkSpinners();
        WebElement el = util.waitAndGetElement(this.podInput);
        util.scrollDown();
        actions.moveToElement(el).click().perform();
        TimeUnit.SECONDS.sleep(5);
        el.sendKeys(pod);


    }


    public void insertDocumento(String documento) throws Exception {
        //TimeUnit.SECONDS.sleep(20);
        spinner.checkSpinners();

        if (util.exists(this.confermaLogin, 5)) {
            util.objectManager(this.confermaLogin, util.scrollAndClick);
        }

        WebElement el = util.waitAndGetElement(this.documentoIdentita);
        util.scrollDown();
        actions.moveToElement(el).click().perform();
        TimeUnit.SECONDS.sleep(5);
        el.sendKeys(documento);
        spinner.checkSpinners();

    }

    public void insertDocumentoNew(String documento) throws Exception {

        WebElement el = util.waitAndGetElement(this.documentoIdentita);
        util.scrollDown();
        actions.moveToElement(el).click().perform();
        TimeUnit.SECONDS.sleep(5);
        el.sendKeys(documento);

    }

    public void inserisciCodiceCliente(String codiceCliente) throws Exception {
        //TimeUnit.SECONDS.sleep(20);
        spinner.checkSpinners();
        WebElement el = util.waitAndGetElement(this.NumeroCliente);
        util.scrollDown();
        actions.moveToElement(el).click().perform();
        TimeUnit.SECONDS.sleep(5);
        el.sendKeys(codiceCliente);

    }

    public void pressButton(By button) throws Exception {
        TimeUnit.SECONDS.sleep(2);
//		WebElement el = util.waitAndGetElement(button);
////		util.scrollToElement(el);
//		el.click();
        util.objectManager(button, util.scrollAndClick);
//claudio		spinner.checkSpinners();
        TimeUnit.SECONDS.sleep(10);  //prova con sleep perché spinner dura troppo
    }

    public void insertIndirizzoFornitura(By input, String address) throws Exception {
        //TimeUnit.SECONDS.sleep(10);
        spinner.checkSpinners();
        util.scrollToElement(driver.findElement(input));
        util.waitAndGetElement(input, 20, 1).click();
        util.waitAndGetElement(input).sendKeys(address);
    }

    public void insertImportoUltimaFattura(String documento) throws Exception {
        //TimeUnit.SECONDS.sleep(20);
        spinner.checkSpinners();
        WebElement el = util.waitAndGetElement(this.ImportoUltimaFattura);
        util.scrollDown();
        actions.moveToElement(el).click().perform();
        TimeUnit.SECONDS.sleep(5);
        el.sendKeys(documento);
        spinner.checkSpinners();

    }

    public String salvaNome() throws Exception {
        String nome = util.waitAndGetElement(campoNome, true).getAttribute("value");
        return nome;
    }

    public String salvaCognome() throws Exception {
        String cognome = util.waitAndGetElement(campoCognome, true).getAttribute("value");
        return cognome;
    }

    public String salvaCF() throws Exception {
        String cf = util.waitAndGetElement(campoCF, true).getAttribute("value");
        return cf;
    }

}
