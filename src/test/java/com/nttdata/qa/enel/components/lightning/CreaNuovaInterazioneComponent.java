package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CreaNuovaInterazioneComponent {
    private WebDriver driver;
    
    public CreaNuovaInterazioneComponent(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
    
    
    @FindBy(xpath = "//span[(text()='Interazioni' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]")
    private WebElement iterationSpan;

//    public static final String X_DROPDOWN = "//button[@class='slds-button slds-p-horizontal__"
//    		+ "xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon[1]" ;
    
    
    public static final String X_DROPDOWN ="//button[contains(@class,'slds-button_icon-small slds-button_icon-container')]/lightning-primitive-icon[1]";

    @FindBy(xpath = X_DROPDOWN)
    private WebElement dropdwonButton;
    
    @FindBy(xpath = "//div[@title='Nuovo' and (text() = 'Nuovo')]")
    private WebElement newIterationButton;

	
	public void createNewiteration() throws Exception{
		TimeUnit.SECONDS.sleep(7);
		dropdwonButton.click();
//		//System.out.println("Dropdown clicked");
		iterationSpan.click();
//		//System.out.println(" New interationicked");
		TimeUnit.SECONDS.sleep(7);
		newIterationButton.click();
	}
	
	
}
