package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InfoEnelEnergiaComponent extends BaseComponent {
	
	public By serviziItem = By.xpath("//nav[@id='mainNav']//span[text()='Servizi']");
	public By serviziFornitureLbl = By.cssSelector("#forniture-id0 > div > h2");
	public By infoEnelEnergiaBtn = By.xpath("//a[contains(@href,'/it/area-clienti/residenziale/infoenelenergia')]");
	public By activateInfoEnelEnergiaBtn = By.xpath("//button/span[text()='ATTIVA INFOENELENERGIA']");
	public By modInfoEnelEnergiaBtn = By.xpath("//button/span[text()='MODIFICA INFOENELENERGIA']");
	public By esciBtn = By.xpath("//button/span[text()='Esci']");
	//public By clientNumber = By.xpath("//span[contains(@class,'flex') and not(contains(@class,'dsd'))]/span[@class='cliente']/span[@class='valore']/span/span");
	public By clientNumber = By.xpath("//span[@class='valore']/span/span");
	public By firstSupplyCheckBox = By.xpath("//span[contains(@class,'flex') and not(contains(@class,'dsd'))]/span[@class='icon-ckbox']");
	public By continueBtn = By.xpath("//span[text()='CONTINUA']/..");
	
	public InfoEnelEnergiaComponent(WebDriver driver) {
		super(driver);
	}
	
	public void accessServiziForniture() throws Exception {
		clickComponent(serviziItem);
		verifyVisibilityAndText(serviziFornitureLbl, serviziFornitureString);
	}
	
	//----- Constants -----
	
	public final String serviziFornitureString = "Servizi per le forniture"; 

}
