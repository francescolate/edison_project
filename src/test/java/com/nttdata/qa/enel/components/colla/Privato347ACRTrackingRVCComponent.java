package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Privato347ACRTrackingRVCComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By dettaglioFornitura_button_621855744 = By.xpath("//span/dd[text()='Attiva']/parent::span/following-sibling::span/dd[text()='621855744']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
	public By dettaglioFornitura_button_310488559 = By.xpath("//span/dd[text()='Attiva']/parent::span/following-sibling::span/dd[text()='310488559']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
	
	//Dettaglio Forniture HomePage
	public By supplyHeading = By.xpath("//h1[@id='fornitura01']");
	public By supplyHeadingDetails = By.xpath("//h1[@id='fornitura01']/parent::div/dl/span[1]");
	public By supplyNumeroCliente = By.xpath("//h1[@id='fornitura01']/parent::div/dl/span[3]/dt");
	public By supplyNumeroClienteNumber = By.xpath("//h1[@id='fornitura01']/parent::div/dl/span[3]/dd");
	
	public By supplyStatusHeading = By.xpath("//div[@class='supply details-container']/dl[@aria-labelledby='fornitura01']/span[1]/dt");
	public By supplyStatus = By.xpath("//div[@class='supply details-container']/dl[@aria-labelledby='fornitura01']/span[1]/dd");
	
	public By section1Heading = By.xpath("//h2[text()='Cambio Offerta']");
	public By section1Subtext = By.xpath("//h2[text()='Cambio Offerta']/following-sibling::p");
	
	public By CambioOffertaSteps = By.xpath("//li[contains(@class,'track-item')]/h3");
	public By Documentazione_d_restituire_Step_Subtext = By.xpath("//li[contains(@class,'track-item')][3]/h3/following-sibling::p");
	public By Richiesta_annullata_Step_Subtext = By.xpath("//li[contains(@class,'track-item')][8]/h3/following-sibling::p");
	
	ColorUtils c = new ColorUtils();
	
	public Privato347ACRTrackingRVCComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}
	
		public void VerifyTextNotMatch(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String textfield_found="YES";
			
			WebElement element = driver.findElement(checkObject);
			
			String actualtext = element.getText().replaceAll("(\r\n|\n)", "").replaceAll(" ", "");
			
			
			if (!(actualtext.contentEquals(Value)))
			textfield_found="NO";
							
			if (textfield_found.contentEquals("YES"))
				throw new Exception("The expected text" + Value + " is found:");
			}
		
		
		public void verifyCambioOffertaStepsRVC_Progress(By checkObject,String objectColor,By SubText,String SubTextValue) throws Exception {
			
			String[] cambioOffertaStepsText = {"Hai scelto l'offerta EnergiaX65 Luce","Richiesta avviata","Documentazione da restituire","Documentazione ricevuta","In attesa","Richiesta in lavorazione","Fornitura attivata","Richiesta completata"};
			List<WebElement> cambioOffertaSteps = driver.findElements(checkObject); //CambioOffertaSteps

			String step_found="NO";
			String textHighlight="NO";
			
			for (int i = 0; i < cambioOffertaSteps.size(); i++) {
				
				String cambioOffertaStep = driver.findElement(By.xpath("//li[contains(@class,'track-item')]["+(i+1)+"]/h3")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				String stepColor = c.getColorNameFormRgbString(util.waitAndGetElement(By.xpath("//li[contains(@class,'track-item')]["+(i+1)+"]/h3"),true).getCssValue("color"));  
				
				// Validating 1,2,3 highlighted step
				if (i<3 && cambioOffertaStep.contentEquals(cambioOffertaStepsText[(i)])) {
					
					step_found="YES";
					// Validating the highlighted step
					if(stepColor.contentEquals(objectColor))
					{
						textHighlight="Yes";
					}
					
					if(textHighlight.contentEquals("NO"))
						throw new Exception("Text not highlighted for step: "+i);
					
				} else if(cambioOffertaStep.contentEquals(cambioOffertaStepsText[i])){
					
					step_found="YES";
					// Validating the step not highlighted
					if(!(stepColor.contentEquals(objectColor)))
					{
						textHighlight="NO";
					}
					
					if(textHighlight.contentEquals("YES"))
						throw new Exception("Text highlighted for wrong step: "+i);
				}
				 
				
				if(cambioOffertaStep.contentEquals(cambioOffertaStepsText[2]))
				{
					step_found="YES";
					try {
						
						VerifyText(SubText, SubTextValue);  //Documentazione_d_restituire_Step_Subtext //Documentazione_d_restituire_Step_SubtextValue
						
					} catch (Exception e) {
						
						e.printStackTrace();
						throw new Exception("SubText Mismatch/NOT FOUND for step: ' "+cambioOffertaStep+" '");
					}
				}
				
				if(step_found.contentEquals("NO"))
					throw new Exception("Step Mismatch/NOT FOUND for index: "+i);

			}
			
		}
		
		public void verifyCambioOffertaStepsRVC_Cancelled(By checkObject,String objectColor,By SubText,String SubTextValue) throws Exception {
			
			String[] cambioOffertaStepsText = {"Hai scelto l'offerta Energia Pura Bioraria Extra","Richiesta avviata","Documentazione da restituire","Documentazione ricevuta","In attesa","Richiesta in lavorazione","Fornitura attivata","Richiesta annullata"};
			List<WebElement> cambioOffertaSteps = driver.findElements(checkObject); //CambioOffertaSteps

			String step_found="NO";
			String textHighlight="NO";
			
			for (int i = 0; i < cambioOffertaSteps.size(); i++) {
				
				String cambioOffertaStep = driver.findElement(By.xpath("//li[contains(@class,'track-item')]["+(i+1)+"]/h3")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				String stepColor = c.getColorNameFormRgbString(util.waitAndGetElement(By.xpath("//li[contains(@class,'track-item')]["+(i+1)+"]/h3"),true).getCssValue("color"));  
				
				// Validating 1,2,3 highlighted step
				if (i==7 && cambioOffertaStep.contentEquals(cambioOffertaStepsText[(i)])) {
					
					step_found="YES";
					// Validating the highlighted step
					if(stepColor.contentEquals(objectColor))
					{
						textHighlight="Yes";
					}
					
					if(textHighlight.contentEquals("NO"))
						throw new Exception("Text not highlighted for step: "+i);
					
				} else if(cambioOffertaStep.contentEquals(cambioOffertaStepsText[i])){
					
					step_found="YES";
					// Validating the step not highlighted
					if(!(stepColor.contentEquals(objectColor)))
					{
						textHighlight="NO";
					}
					
					if(textHighlight.contentEquals("YES"))
						throw new Exception("Text highlighted for wrong step: "+i);
				}
				 
				
				if(cambioOffertaStep.contentEquals(cambioOffertaStepsText[7]))
				{
					step_found="YES";
					try {
						
						VerifyText(SubText, SubTextValue);  //Documentazione_d_restituire_Step_Subtext //Documentazione_d_restituire_Step_SubtextValue
						
					} catch (Exception e) {
						
						e.printStackTrace();
						throw new Exception("SubText Mismatch/NOT FOUND for step: ' "+cambioOffertaStep+" '");
					}
				}
				
				if(step_found.contentEquals("NO"))
					throw new Exception("Step Mismatch/NOT FOUND for index: "+i);

			}
			
		}
		
	
	public static final String NumeroClienteNumber310488559= "310488559";	
	public static final String NumeroClienteNumber621855744= "621855744";
	public static final String Documentazione_d_restituire_Step_SubtextValue = "Completa la tua richiesta di adesione all'offerta compilando e restituendo la documentazione indicata nel contratto.";
	public static final String Richiesta_annullata_Step_SubtextValue = "La richiesta di attivazione della fornitura è stata annullata. Per maggiori informazioni chiedi al nostro supporto.";
	
	public static final String stepColor= "OrangeRed";
	public static final String singleStepColor= "Red";
	
	public String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
}
