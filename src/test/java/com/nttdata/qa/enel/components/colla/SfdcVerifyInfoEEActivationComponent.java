package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SfdcVerifyInfoEEActivationComponent extends BaseComponent {
	
	public By searchField = By.xpath("//input[@role='combobox' and contains(@title,'Cerca')]");
	public By clientNameLink = By.xpath("//a[text()='Clienti']/ancestor::div[contains(@class,'resultsItem slds-col slds-no-flex slds-m-bottom_small')]//table/tbody/tr/th/span/a");
	public By richiesteBtn = By.xpath("//h2[@class='slds-card__header-title']/a/span[text()='Richieste']");
	public String attivazioneVasXpath = "//section[@aria-expanded='true']//a[contains(@title,'ATTIVAZIONE VAS')]";
	public By correlatoBtn = By.xpath("//a[@data-label='Correlato']");
	public By storicoCertificazioneBtn = By.xpath("//span[@title='Storico Certificazione']");
	public By firstStatoLbl = By.xpath("(//span[text()='CERTIFICATA'])[1]");
	
	public SfdcVerifyInfoEEActivationComponent(WebDriver driver) {
		super(driver);
	}
	
	public void searchCfIva(String cfIva) throws Exception {
		verifyVisibilityThenClick(searchField);
		insertTextAndReturn(searchField, cfIva);
	}
	
	public void accessRichieste() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, baseTimeoutInterval);
		util.scrollToElement(wait.until(ExpectedConditions.presenceOfElementLocated(richiesteBtn)));
		verifyComponentVisibility(richiesteBtn);
		clickComponentWithJse(richiesteBtn);
	}
	
	public void selectLatestAttivazioneVas() throws Exception {
		List<WebElement> attVasElements = driver.findElements(By.xpath(attivazioneVasXpath));
		if (attVasElements.size() > 1) {
			//Check if the elements are in ascending or descending order
			SimpleDateFormat sdFormat = new SimpleDateFormat("dd/MM/yyyyy HH.mm");
			String dateXpath = "(" + attivazioneVasXpath + "/ancestor::tr/td[6]/span/span)";
			String firstElementDateString = driver.findElement(By.xpath(dateXpath + "[1]")).getText();
			String lastElementDateString = driver.findElement(By.xpath(dateXpath + "[" + attVasElements.size() + "]")).getText();
			Date firstElementDate = sdFormat.parse(firstElementDateString);
			Date lastElementDate = sdFormat.parse(lastElementDateString);
			if (firstElementDate.after(lastElementDate)) {
				System.out.println("Discendente");
				clickComponent(By.xpath("(" + attivazioneVasXpath + ")[1]"));
			} else {
				System.out.println("Ascendente");
				clickComponent(By.xpath("(" + attivazioneVasXpath + ")[" + attVasElements.size() + "]"));
			}
		} else {
			attVasElements.get(0).click();
		}
	}
	
	public void checkActivationStatus() throws Exception {
		verifyVisibilityThenClick(correlatoBtn);
		verifyVisibilityThenJsClick(storicoCertificazioneBtn);
		//We identify the element by it's text, so if that's wrong we'll get an exception
		verifyComponentVisibility(firstStatoLbl);
	}

}
