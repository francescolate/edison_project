package com.nttdata.qa.enel.components.colla;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModificaIndirizzoFattuazioneComponent {
	WebDriver driver;
	SeleniumUtilities util;
	Properties DPQ = new Properties(); // Data Preparation Query
	
	
	public By serivzi = By.xpath("//li[@id='menuService']/a");
	public By bollette = By.xpath("//li[@id='menuBoll']/a");
	public By forniture = By.xpath("//li[@id='menuForn']/a");
	public By HomepagePath = By.xpath("//main[@id='main']/section[@class='acb']//section[@class='content']//div[@class='panel-body']/ol[@class='breadcrumb']");
	public By serviziPath = By.xpath("//main[@id='main']/section[@class='acb']//section[@class='content']//div[@class='panel-body']/ol[@class='breadcrumb']");
	public By serviziiPath = By.xpath("//main[@id='main']//section[@class='content']//div/ol[@class='breadcrumb']/li[@class ='active']");
	public By serviziTitle = By.xpath("//section[@class='content']/div[@class='panel panel-default panel-clear']/div[@class='panel-body']/h1");
	public By serviziText = By.xpath("//section[@class='content']/div/div[@class='text parbase']/div[@class='heading']/h4");
	public By addebitoDiretto = By.xpath("//div[@class='single-service'][1]/a[@class='panel panel-default panel-primary']//p[1]");
	public By bollettaWeb = By.xpath("//div[@class='single-service'][2]/a[@class='panel panel-default panel-primary']//p[1]");
	public By opzioneGreen = By.xpath("//div[@class='single-service'][3]/a[@class='panel panel-default panel-primary']//p[1]");
	public By modificaIndirizzodiFatturazione = By.xpath("//div/p[contains(text(),'Modifica Indirizzo di Fatturazione')]/ancestor::a");//div[@class='single-service'][3]/a[@class='panel panel-default panel-primary']//p[1]
	//public By modificaIndirizzodiFatturazione =  By.xpath("//div[@class='single-service'][4]/a[@class='panel panel-default panel-primary']//p[1]");
	public By modificaPotenzaeTensione =  By.xpath("//div[@class='single-service'][5]/a[@class='panel panel-default panel-primary']//p[1]");
	public By autolettura = By.xpath("//div[@class='single-service'][6]/a[@class='panel panel-default panel-primary']//p[1]");
	public By infoEnelEnergia = By.xpath("//div[@class='single-service'][7]/a[@class='panel panel-default panel-primary']//p[1]");
	public By opzioneBolletta = By.xpath("//div[@class='single-service'][8]/a[@class='panel panel-default panel-primary']//p[1]");
	public By rateizzazione = By.xpath("//div[@class='single-service'][9]/a[@class='panel panel-default panel-primary']//p[1]");
    public By indirizzoDiFatturazionePath = By.xpath("//main[@id='main']/section[@class='acb']//section[@class='content']//div[@class='panel-body']/ol[@class='breadcrumb']");
    public By indirizzoDiFatturazioneTitle = By.xpath("//main[@id='main']/section[@class='acb']//section[@class='content']//div[@class='panel-body']/h1");
    public By sectionData = By.xpath("//div[@class='panel panel-default'][1]//h2");
    public By sectionBillingAddress = By.xpath("//div[@class='panel panel-default'][2]//h2");
    public By sectionTechnicalSpec = By.xpath("//div[@class='panel panel-default'][3]//h2");
    public By sectionContract = By.xpath("//div[@class='panel panel-default'][4]//h2");
	public By clientNumber = By.xpath("//div[@id='headingOne']/h4/b");
	public By modificaButton = By.xpath("//div/span[2]/a[@name='Modifica']");
	public By supplyBox = By.xpath("//td[text()='310488488']/preceding-sibling::td/div/label");
	public By modificaButton2 = By.xpath("//a[@name='MODIFICA']");
	public By provincia = By.xpath("//div[@id='ProvinciapanelInputId']/input");
	public By comune = By.xpath("//div[@id='ComunepanelInputId']/input");
	public By indrizzo = By.xpath("//div[@id='IndirizzopanelInputId']/input");
	public By numerocivico = By.xpath("//div/input[@id='input-2']");
	public By localita = By.xpath("//div[@class='form-group']/input[@id='Località']");
	public By cap = By.xpath("//div[@class='form-group']/input[@id='CAP']");
	public By cap1 = By.xpath("//input[@id ='495:16;a']");
	public By regione = By.xpath("//div[@class='form-group']/input[@id='Regione']");
	public By provincia177 = By.xpath("//div[@id='ProvinciapanelInputId']/input");
	public By comune177 = By.xpath("//div[@id='ComunepanelInputId']/input");
	public By indrizzo177 = By.xpath("//div[@id='IndirizzopanelInputId']/input");
	public By numerocivico177= By.xpath("//input[@id='input-2']");
	public By localita177 = By.xpath("//div[@id='LocalitàpanelInputId']/input");
	public By cap177 = By.xpath("//div[@class='form-group']/input[@id='CAP']");
	public By regione177 = By.xpath("//div[@class='form-group']/input[@id='Regione']");
	public By proseguiButton = By.xpath("//button[.='Prosegui']");
	public By selectCap = By.xpath("//span[.='ALICE CASTELLO']");
	public By cliccaQui = By.xpath("//a[contains(text(),'CLIC')]");
    public By campoRichiestoProvinica = By.xpath("//div[@id='ProvinciapanelInputId']/span");
    public By CampoRichiestaComune = By.xpath("//div[@id='ComunepanelInputId']/span[@class='slds-form-element__help']");
  // public By CampoRichiestaComune = By.xpath("div[@id='ComunepanelInputId']/input[@id='331:16;a']/following-sibling::span[1]");
   public By CampoRichiestaIndirizzo = By.xpath("//div[@id='IndirizzopanelInputId']/span[@class='slds-form-element__help']");
       //81
    public By supplyCheckBox = By.xpath("//div[@class='table-responsive']/table/tbody/tr[1]/td[1]/div");
    public By esciButton = By.xpath("//div[@class='row-btns row-btns-closing']/a[@class='btn btn-primary btn-bordered pull-left']");
    public By popupHeading = By.xpath("//div[@id='modal-boxed-attenzione']//div/h4");
    public By popupTitle = By.xpath("//div[@id='modal-boxed-attenzione']//div/p");
    public By tornaAlProcessoButton = By.xpath("//div[@id='modal-boxed-attenzione']//button[@id='closeAbort']");
    public By esciPopupButton = By.xpath("//div[@id='modal-boxed-attenzione']//div[@id='sendAbort']");
    public By popupClose = By.xpath("//div[@id='modal-boxed-attenzione']//button[@class='modal-close']/span");
    public By popCloseinfoicon = By.xpath("//div[@id='modalAlert']//span[@class='dsc-icon-close-rounded']");
    public By indietroButton = By.xpath("//section[@id='sc']//div[@class='row-btns row-btns-closing']/a[.='Indietro']");
        
    public By indirizziGiaVerificati = By.xpath("//div[@class='customselect']/div[@class='customselect-container']/a[@class='customselect-choice']");
    public By addressOption1 = By.xpath("//li[@id='select2-scegli-indirizzo-result-nl9q-option6'][1]");
    //public By riepilogoeConfermaDati = By.xpath("//div[@class='panel-body']/ul[@class='steps']/li[@class='current']");
    public By riepilogoeConfermaDati = By.xpath("//section[@id='sc']/div/div[1]/div/ul/li[2]/span[2]");
    public By savedAddress = By.xpath("//table[@class='table']/tbody/tr/td[3]/a[@class='ellipsis']");
    public By confermaButton = By.xpath("//div[@class='row-btns row-btns-closing']/button[@class='btn btn-primary']");
    public By esito = By.xpath("//main[@id='main']//div/ul/li[@class='done'][3]");
    public By messageOk = By.xpath("//p[@id='messageOK']");
    public By fineButton = By.xpath("//div[@class='row-btns']/a[@class='btn btn-primary']");
    public By homepageHeading = By.xpath("//section[@id='content-home']/div[@class='panel panel-default panel-primary']//div/h2");
    
    public By homepageHeadingRES = By.xpath("//div[@id='mainContentWrapper']//descendant::h1");
    public By homepageParagraphRES = By.xpath("//div[@id='mainContentWrapper']//descendant::p[contains(text(),'In questa se')]");
    public By homepageHeading2RES = By.xpath("//section[@id='lista-forniture']/div[@class='section-heading']/h2");
    public By homepageParagraph2RES = By.xpath("//section[@id='lista-forniture']//p[2]");
    public By attiva  = By.xpath("//div[@id='fornitura961728208']");
    public By attiva1  = By.xpath("//div[@class='card forniture regolare']");
    public By attiva2  = By.xpath("//dd[@id='fornitura799813220']");
    public By attiva2New  = By.xpath("//dd[@id='fornitura310494829']");
    public By visualizzalLeBollette = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare']/div[@class='button-container']/a[1]");
    public By dettaglioFornitura = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare']/div[@class='button-container']/a[2]");
    public By attiva174 = By.xpath("//div[@id='fornitura300001594']");
    public By attiva174Sub = By.xpath("//dd[@id='fornitura300001594']");
    public By visualizzalLeBollette174 = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][2]/div[@class='button-container']/a[1]");
    public By dettaglioFornitura174 = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][2]/div[@class='button-container']/a[2]");
    public By servizipageHeadingRES = By.xpath("//section[@id='forniture-id0']//h2");
    public By servizipageParagraphRES = By.xpath("//section[@id='forniture-id0']//p[2]");
    public By mifHeading = By.xpath("//h1[@id='h1_mif']");
    public By mifHeading2 = By.xpath("//section[@id='landing-text']/div[@id='sc']//descendant::h2");
    public By infoIconPopupHeading = By.xpath("//h3[@id='titleModal']");
    public By infoIconPopupText = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
    public By fornitureRES = By.xpath("//a[@id='idFornitura']/span");
    public By popupHeadingRES = By.xpath("//h3[@id='titleModal']");
	public By popupParagraph = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
	public By popupNoButton = By.xpath("//button[@id='overlayNoButton']");
	public By popupSIButton = By.xpath("//button[@id='overlayYesButton']");
	public By supplyDetail = By.xpath("//label[@id='row-0-label']/span");
	public By indirizzoDiFatturazione = By.xpath("//div[@id='form_mifRecap']//h4[@class='plan-main-head']");
	public By addressValue = By.xpath("//span[@class='indirizzo']/span[@class='valore']");
    public By numeroCliente = By.xpath("//span[@class='cliente']/span[@class='valore']");
    public By infoIcon = By.xpath("//label[@id='row-0-label']//a[@id='icon-info-circle-0']/span[@id='0']");
    public By confermaButtonRES = By.xpath("//div[@id='sc']//button[.='Conferma']");
    public By esitoRES = By.xpath("//div[@id='sc']//span[.='Esito']");
    public By esitoRESHeading = By.xpath("//h3[@id='id_title']");
    public By esitoRESParagraph = By.xpath("//div[@id='sc']//p");
    public By fineButtonRES = By.xpath("//div[@id='sc']//button[.='Fine']");
    
    public By cliccaQua = By.xpath("//a[contains(text(), ' CLICCA QUI')]");
    public By indirizzofornitura = By.xpath("//b[contains(text(), 'Indirizzo fornitura:')]");
    public By indirizzoFornituraValue = By.xpath("//main[@id='main']/section/div/div/section/div[3]/div/div[2]/div/div/div/div/div[2]/span[2]");
    public String provinciaxpath = "//label[contains(text(), 'Provincia')]/following-sibling::input";
    public String comunexpath = "//input[@name='Comune']";
    public String indrizzoxpath = "//input[@name='Indirizzo']";
    public String numerocivicoxpath = "//input[@name='Numero Civico']";
    public String localitaxpath ="//input[@name='Località']";
    public String localitta = "//input[@name='CAP']/";
    public String capxpath = "//label[contains(text(), 'CAP')]/following-sibling::input";
    public String regionexpath = "//input[@id='Regione']";
            
    public String provinciaBeforeSave = "//div[@id='ProvinciapanelInputId']/input";
	public String comuneBeforeSave ="//div[@id='ComunepanelInputId']/input";
	public String indrizzoBeforeSave ="//div[@id='IndirizzopanelInputId']/input";
	public String numerocivicoBeforeSave = "//div/input[@id='input-2']";
	public String localitaBeforeSave = "//div[@class='form-group']/input[@id='Località']";
	

	
	 public ModificaIndirizzoFattuazioneComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
//			util.objectManager(clickableObject, util.scrollAndClick);
			util.jsClickElement(clickableObject);
			//System.out.println("selected option: "+ clickableObject);
		}
	 
	 public void checkMenuPath(String path, By existingObject) throws Exception {
			WebElement obj = util.waitAndGetElement(existingObject);
			String pathDescription=obj.getText(); 
			pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	

			if (!pathDescription.equalsIgnoreCase(path))
				throw new Exception("object path with xpath " + path + " is not exist.");
		}
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkForSupplyServices(String property) throws Exception {
			
			if(! property.contains(driver.findElement(addebitoDiretto).getText()) && property.contains(driver.findElement(bollettaWeb).getText()) && property.contains(driver.findElement(opzioneGreen).getText()) && property.contains(driver.findElement(modificaIndirizzodiFatturazione).getText())
					&& property.contains(driver.findElement(modificaPotenzaeTensione).getText()) && property.contains(driver.findElement(autolettura).getText())  && property.contains(driver.findElement(infoEnelEnergia).getText())
					 && property.contains(driver.findElement(opzioneBolletta).getText()) && property.contains(driver.findElement(rateizzazione).getText()))
							
				throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
		}
		
		public void clickProseguiButton( By clickObject, By expObject, String exptext) throws Exception 
		  {  
		   //driver.findElement(existingObject).sendKeys(prop);
		   WebDriverWait wait = new WebDriverWait(driver, 15);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(clickObject));
		   wait.until(ExpectedConditions.elementToBeClickable(clickObject));
		  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(clickObject));
		   	Thread.currentThread().sleep(2000);
		   /*Actions actions = new Actions(driver);
		   actions.moveToElement(driver.findElement(clickObject));
		   actions.click();
		   actions.perform();*/
		   //driver.findElement(clickObject).click();
		   util.jsClickElement(clickObject);
		  if(! wait.until(ExpectedConditions.textToBePresentInElementLocated(expObject, exptext)))
			  throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
			  
		  }
		 public void enterinputParameters(By insertObject, String valore) throws Exception {
				util.objectManager(insertObject, util.sendKeys, valore);
			}
		 
		 public void clearInputFromField(By checkObject) throws Exception {
				driver.findElement(checkObject).clear();
				
		}	
		 
		 public void verifyComponentExistence(By existingObject) throws Exception {
				if (!util.exists(existingObject, 15))
					throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
			}
		 
		 public void verifySupplyComponentExistence(By existingObject, By Object1, By Object2) throws Exception {
				 String attiva = driver.findElement(existingObject).getText(), attivaExp = "Attiva";
				 
				 if(attiva.equalsIgnoreCase(attivaExp)){
					 driver.findElement(Object1).isDisplayed();
				 	 	driver.findElement(Object2).isDisplayed();}
				 	 	else throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
			}
		 
		 public void verifyPreviousPageData(By Object) throws Exception {
			 WebDriverWait wait = new WebDriverWait(driver, 15);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(Object));
			 WebElement checkbox = driver.findElement(supplyCheckBox);
			 if(!checkbox.isEnabled())
			 throw new Exception("In the page 'https://www-colla.enel.it/' Perviously filled data is not matching");
					
			}
		 
		 public void checkColor(By object, String color, String objectName) throws Exception{
				ColorUtils c = new ColorUtils();
				System.out.println(util.waitAndGetElement(object,true).getCssValue("color"));
				String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("border-color"));
				System.out.println(colore);
				if(!colore.contentEquals(color)) 
					throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
			}
		 
		 public void checkURLAfterRedirection(String url) throws Exception{
				if(!url.equals(driver.getCurrentUrl()))
					throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
			}
		 
		 public void verifyFeildAttribute(String xpath, String text) throws Exception {
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			String actualText = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
				if (!actualText.equalsIgnoreCase(text))
					throw new Exception("object with text " + text + " is not exist. actual text = "+actualText);
			}
		 
		 public void waitForElementToDisplay (By checkObject) throws Exception{
				
				WebDriverWait wait = new WebDriverWait (driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				
			}
		 
		 public void changeDropDownValue(By checkObject,String property) throws Exception {
			 	WebDriverWait wait = new WebDriverWait (driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	           	Select dropdown = new Select(driver.findElement(checkObject));
	            dropdown.selectByVisibleText(property);
	           
	        } 
		 
		 public void verifySupplyDisplay(By checkObject) throws Exception {
			  WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
					WebElement mytable = driver.findElement(By.xpath("//table[@class='table']"));
					List < WebElement > rows_table = mytable.findElements(By.xpath("//table[@class='table']/tbody"));
					int rows_count = rows_table.size();
												
					for (int row = 0; row < rows_count; row++)
						{
							String description = rows_table.get(row).getText();
							if(!description.contentEquals("Gas ATTIVO 778186188\nVia Lamarmora 31 20122 Milano Mi\nVEDI BOLLETTE"))
							throw new Exception("The supply details displayed are incorrect");
						}
					 }
		 public void jsClickElement(By xpath){
				String itemXpath = xpath.toString().replace("By.xpath: ", "");
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("(document.evaluate(\""+itemXpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
			}
		 
		 public void refreshPage(){
			  driver.navigate().refresh();
		  }
		 
		 public void verifyAddressDetails(By checkObject, String[] Feild, String[] Value) throws Exception{
				
				HashMap<String, String> hm = new HashMap<String, String>();
				HashMap<String, String> hmActual = new HashMap<String, String>();
				String key, val ;
				for (int i=1;i<=3;i++)
					{
					key = driver.findElement(By.xpath("//div[@id='form_mifRecap']/div[@class='content-group']/div[@class='details-container']/dl/span["+ i +"]/dt")).getText();
					val = driver.findElement(By.xpath("//div[@id='form_mifRecap']/div[@class='content-group']/div[@class='details-container']/dl/span["+ i +"]/dd")).getText();
					hm.put(key, val);
					}
				for (int i=0; i<Feild.length;i++){
				hmActual.put(Feild[i], Value[i]);}
				if(!hm.equals(hmActual))
		    		throw new Exception ("The address details are not correctly displayed");
				}
		 
		  public void enterAddDetails(String text) throws Exception{
	             
			  ((WebElement) cap).sendKeys(text);
			  System.out.println("Cap enterred");
       }
        
		
public static final String SERVIZI_PATH = "AREA RISERVATA SERVIZI";
public static final String BOLLETTE_PATH = "AREA RISERVATA / BOLLETTE";
public static final String HOMEPAGE_PATH = "AREA RISERVATA HOMEPAGE";
public static final String SERVIZII_PATH = "SERVIZI";
public static final String SERVIZI_TITLE = "Servizi";
public static final String SERVIZI_TEXT = "In questa pagina trovi tutti i servizi.";
public static final String INDIRIZZO_PATH = "AREA RISERVATA FORNITURA INDIRIZZO DI FATTURAZIONE";
public static final String INDIRIZZO_TITLE = "L'indirizzo di fatturazione della tua fornitura";
public static final String SECTION_DATA = "I tuoi dati";
public static final String SECTION_BILLING_ADDRESS= "Il tuo indirizzo di fatturazione";
public static final String SECTION_TECH_SPECIFICATION = "Le specifiche tecniche della tua fornitura";
public static final String SECTION_CONTRACT = "Il tuo contratto";
public static final String CAMPO_RICHIESTA = "Campo richiesto";
public static final String POPUP_HEADING = "Attenzione";
public static final String POPUP_HEADING_INFO= "Attenzione!";
public static final String POPUP_TITLE = "Se uscirai dal processo perderai le modifiche effettuate.";
public static final String POPUP_PARAGRAPH = "Sei sicuro di voler uscire da questa sezione?";
public static final String POPUP_PARAGRAPH_INFOICON = "Siamo spiacenti, ma non è possibile avviare la richiesta poiché risulta un'altra operazione in corso. Non appena conclusa sarà possibile effettuare una nuova richiesta. Per maggiori informazioni chiedi al nostro supporto in chat.";
public static final String REIPILOGO_DATI ="Riepilogo e conferma dati";
public static final String SAVED_ADDRESS = "Via Gramsci 29 7 13040 Palazzolo Vercellese Palazzolo Vercellese Vc";
public static final String MESSAGE_OK = "La tua richiesta è stata presa in carico. Riceverai le prossime comunicazioni cartacee al tuo nuovo indirizzo";
public static final String HOMEPAGE_HEADING = "Forniture e bollette";
public static final String HOMEPAGE_PATH_TEXT = "Area riservataHomepage";
public static final String HOMEPAGE_HEADING_RES = "Benvenuto nella tua area privata";
public static final String HOMEPAGE_PARAGRAPH_RES = "In questa sezione potrai gestire le tue forniture";
public static final String HOMEPAGE_HEADING2_RES = "Le tue forniture";
public static final String HOMEPAGESS_VALUE_174 = "Via Virginia 12 A 40017 San Matteo Della Decima San Giovanni In Persiceto Bo";
public static final String NUMERO__PARAGRAPH2_RES = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
public static final String SERVIZIPAGE_HEADING_RES = "Servizi per le forniture";
public static final String SERVIZIPAGE_PARAGRAPH_RES = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
public static final String INDIRIZZO_DI_FATTURAZIONE = "L'indirizzo di fatturazione verrà aggiornato su:";
public static final String ADDRESS_VALUE = "Piazza Negri 1 26851 Borgo San Giovanni Borgo San Giovanni Lo";
public static final String ADDRECLIENTE ="799813220";
public static final String NUMERO_CLIENTE_174 ="300001597";
public static final String MIF_HEADING = "Modifica Indirizzo di Fatturazione";
public static final String MIF_PARAGRAPH = "Seleziona una o più forniture su cui intendi modificare l'indirizzo di fatturazione. Qualora non fosse attivo il servizio di Bolletta Web, in caso di modifica, verrà utilizzato come indirizzo di recapito delle bollette e di eventuali comunicazioni.";
public static final String ESITO_RES = "Esito";
public static final String ESITO_RES_HEADING = "La tua richiesta è stata presa in carico";
public static final String ESITO_RES_HEADINGNew = "L'operazione non è andata a buon fine";
public static final String ESITO_RES_PARAGRAPH = "Riceverai le prossime comunicazioni cartacee, relative alle forniture selezionate, al tuo nuovo indirizzo.";

public static final String[] INDIRIZO_FIELDS= {"Indirizzo","Località","Provincia"};
public  final String[] INDIRIZO_VALUES= {"Via Appia Antica,1","ROMA","ROMA"};

//
public static final String INDIRIZZO_FORNITURA = "Indirizzo fornitura: Via Nizza 152 00198 Roma Rm";

}
