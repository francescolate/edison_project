package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivatoBollettaWebID316Component {

	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainProspect']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By areaRiservata = By.xpath("//ol[@class='breadcrumb']");
	public By pageHeading = By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
	
	public By leftMenu = By.xpath("//ul[@id='internal-menu']/li/a[not(@style='display:none')]");
	
//	public By sectionSubheading = By.xpath("//h2[contains(text(),'Vantaggi per il tuo business')]");
//	
//	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
//	public By serviziSelectMenu = By.xpath("//li/a[text()='servizi']");
//	public By serviziPageBolletteSection = By.xpath("//h2[text()='Servizi per le bollette']");
//	public By Bolletta_Web_box = By.xpath("//p[contains(text(),'Bolletta Web')]/ancestor::div[@class='panel-body']");
//	public By modificaBollettaWeb_button = By.xpath("//div[@id='bollWebAttivo']//a[@name='Modifica']");
//	public By modificaBollettaWeb_Title = By.xpath("//h1[contains(text(),'Modifica')]");
//	public By modificaBollettaWeb_TitleSubtext = By.xpath("//h1[contains(text(),'Modifica')]/following-sibling::p");
//	
//	public By supplyActiveStatus = By.xpath("//b[text()='è attivo']/parent::p");
//	public By supplyNonActiveStatus = By.xpath("//b[text()='non è attivo']/parent::p");
//	
//	public By fornitureSelect = By.xpath("//li/a[text()='fornitura']");
//	public By dailogTitle = By.xpath("//h3[text()='Attenzione']");
//	public By dailog_No = By.xpath("//button[@id='overlayNoButton']");
//	public By dailog_Yes = By.xpath("//button[@id='overlayYesButton']");
//	public By dailogClose = By.xpath("(//button[@onClick='focusCloseModale()'])[1]");
//	
//	public By dettaglioFornitura = By.xpath("//dd[text()='Attiva']/ancestor::*/preceding-sibling::*//span[@class='icon-line-electricity']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
//	public By dettaglioFornituraGas = By.xpath("//dd[text()='Attiva']/ancestor::*/preceding-sibling::*//span[@class='icon-line-flame']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
//	public By Visualizza_le_bollette_button = By.xpath("(//dd[text()='Attiva']/ancestor::div[@class='details-container']/following-sibling::div/a[contains(text(),'Visualizza le bollette')])[1]");
//	
//	public By Bolletta_Web_box_Tile = By.xpath("//li[@class='tile']//h3[text()='Bolletta Web']/parent::div");
//	public By Bolletta_Web_Title = By.xpath("//div[@class='section-heading']//h2"); 
//	public By Bolletta_Web_Title_Subtext = By.xpath("//div[@class='section-heading']//p[contains(text(),'Il servizio Bolletta Web consente')]"); 
//	
	
	
	
	public PrivatoBollettaWebID316Component(WebDriver driver)
	{
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void verify_Item_NotDisplayed(String item) throws Exception
	{
		
		String found = "YES";
		
		if (!(driver.getPageSource().contains(item))) {
			found = "NO";
		}

		if (found.contentEquals("YES"))
			throw new Exception(item+" is displayed which was not expected");
		
	}
	
	public void verify_SERVIZI_menuNotDisplayed (By checkObject, String menuItem) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String item_found="YES";
		List<WebElement> leftMenu = driver.findElements(checkObject);
		
		for (int i=0; i<leftMenu.size(); i++)
		{

			String description = driver.findElement(By.xpath("(//ul[@id='internal-menu']/li/a[not(@style='display:none')])["+(i+1)+"]")).getText();
			description= description.replaceAll("(\r\n|\n)", " ");

			if (!(description.contentEquals(menuItem))) 
				item_found="NO";

			if (item_found.contentEquals("YES"))
				throw new Exception(menuItem+" is button is present which is not expected");
		}
		
//		verify_Item_NotDisplayed(menuItem);
		
	}
	
}
