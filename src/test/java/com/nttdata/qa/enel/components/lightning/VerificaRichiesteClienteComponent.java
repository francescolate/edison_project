package com.nttdata.qa.enel.components.lightning;

import java.sql.Time;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaRichiesteClienteComponent {

	WebDriver driver;
	public SeleniumUtilities util;
	Actions actions;
	JavascriptExecutor jse;
	SpinnerManager spinner;

	public VerificaRichiesteClienteComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		actions = new Actions(driver);
		jse = (JavascriptExecutor) driver;
		spinner = new SpinnerManager(driver);
	}

	public By attivitaSectionSpan = By.xpath("//span[@title='Attività']");
	public By richiesteSectionSpan = By.xpath("//span[@title='Richieste']");
	public By richiesteTable = By.xpath("//section//table");
	private By iconaRicerca = By.xpath("//button[@title='Mostra filtri veloci']");
	private By inputOggetto=By.xpath("//input[contains(@name,'Case-Subject')]");
	private By buttonApplica = By.xpath("//button[@title='Applica']");
	private By buttonChiudiRicerca = By.xpath("//button[@title='Chiudi filtri']");


	public void press(By el) throws Exception{
		util.objectManager(el, util.scrollAndClick);

	}


	public void cercaUltimaRichiesta() throws Exception{

		WebElement table = util.waitAndGetElement(richiesteTable);

		//		int x = util.getTableColumnIndexByName(table, "OGGETTO","./thead/tr/th/div/a/span[2]");
		util.clickTableElementFromColumnData(table, "OGGETTO", "AUTOLETTURA", "./thead/tr/th/div/a/span[2]", "./tbody/tr[%d]", "//td//a",0,true);


	}

	public void ricercaRichieste(String oggetto) throws Exception{
		//Click su icona imbuto
		util.objectManager(iconaRicerca, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//Inserisco campo oggetto
		util.objectManager(inputOggetto, util.scrollAndSendKeys,oggetto);
		//Click su Applica
		util.objectManager(buttonApplica, util.scrollAndClick);
		//Chiusura form di ricerca
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(buttonChiudiRicerca, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);

	}

	public String verificaRichiestaSwitchPassivo(String stato,String sottoStato,String pod) throws Exception{

		WebElement table = util.waitAndGetElement(richiesteTable);
		//Recupero numero righe
		int numRighe=driver.findElements(By.xpath("//section//table/tbody/tr")).size();
		if(numRighe==0){
			throw new Exception("Nella sono presenti richieste in tabella");
		}
		Logger.getLogger("").log(Level.INFO, "Numero Righe Tabella Richiesta:"+numRighe);
		//Ricerca attivita
		String nomeRichiesta="";
		String statoRichiesta="";
		String sottoStatoRichiesta="";
		boolean podTrovato=false;
		boolean verificaStato=false;
		boolean verificaSottoStato=false;
		for(int i=1;i<=numRighe;i++){
			table = util.waitAndGetElement(richiesteTable);
			//Recupero id richiesta, stato e sottostato
			nomeRichiesta=util.getTableCellText(table, i, "Richiesta", "./thead/tr/th/div/a/span[2]", "./tbody/tr["+i+"]", "/th/span/a", 0);
			Logger.getLogger("").log(Level.INFO,"id richiesta:"+nomeRichiesta);
//			//			statoRichiesta=util.getTableCellText(table, i, "Stato", "./thead/tr/th/div/a/span[2]", "./tbody/tr["+i+"]", "/td[%d]/span", 0);/td[5]/span/span
//			statoRichiesta=util.getTableCellText(table, i, "Stato", "./thead/tr/th/div/a/span[2]", "./tbody/tr["+i+"]", "/td[5]/span/span", 0);//td[5]/span/span
//			Logger.getLogger("").log(Level.INFO,"Stato richiesta:"+statoRichiesta);
//			//			sottoStatoRichiesta=util.getTableCellText(table, i, "Stato Secondario", "./thead/tr/th/div/a/span[2]", "./tbody/tr["+i+"]", "/td[%d]/span", 0);
//			sottoStatoRichiesta=util.getTableCellText(table, i, "Stato Secondario", "./thead/tr/th/div/a/span[2]", "./tbody/tr["+i+"]", "/td[7]/span/span", 0);
//			Logger.getLogger("").log(Level.INFO,"Sottostato richiesta:"+sottoStatoRichiesta);
//			if(statoRichiesta.compareTo(stato)==0 && sottoStatoRichiesta.compareTo(sottoStato)==0){
				//Se stato e sottostato sono quelli che si si aspetta, si clicca sul dettaglio per verificare se è relativa al pod da cercare
				util.clickTableElementFromColumnData(table, "Richiesta", nomeRichiesta, "./thead/tr/th/div/a/span[2]", "./tbody/tr["+i+"]", "/th/span/a",0,true);
				spinner.checkSpinners();
				//Verifica dettaglio POD
				podTrovato=verificaPODDettaglioRichiesta(pod);
				verificaStato=verificaStatoDettaglioRichiesta(stato);
				verificaSottoStato=verificaSottoStatoDettaglioRichiesta(sottoStato);
				//Chiusura tab Richiesta
				By buttonChiudiDettaglio = By.xpath("//button[@title='Chiudi "+nomeRichiesta+"']");
				util.objectManager(buttonChiudiDettaglio, util.scrollAndClick);
				if(podTrovato==true){
					break;
				}
			}
//		}
		//Viene lanciata eccezione se non è viene trovato il POD nei dettagli delle attività
		if(podTrovato==false){
			throw new Exception("Il POD:"+pod+" non è presente nei dettagli delle Richieste.");
		}
		else if(verificaStato==false | verificaSottoStato==false){
			throw new Exception("Il POD:"+pod+" è presente nel dettaglio ma non ha Stato:"+stato+" e SottoStato:"+sottoStato);
		}

		return nomeRichiesta;


	}
	private boolean verificaPODDettaglioRichiesta(String podAtteso) throws Exception{
		By labelPod = By.xpath("//span[text()='"+podAtteso+"']");
		if(util.exists(labelPod, 15)){
			Logger.getLogger("").log(Level.INFO,"POD:"+podAtteso+" trovato");
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean verificaStatoDettaglioRichiesta(String stato) throws Exception{
		By labelStato = By.xpath("//span[@title='"+stato+"']");
		if(util.exists(labelStato, 5)){
			Logger.getLogger("").log(Level.INFO,"Richiesta in stato:"+stato+" trovata");
			return true;
		}
		else{
			return false;
		}
	}
	private boolean verificaSottoStatoDettaglioRichiesta(String sottoStato) throws Exception{
		By labelSottoStato = By.xpath("//span[contains(@title,'"+sottoStato+"')]");
		if(util.exists(labelSottoStato, 5)){
			Logger.getLogger("").log(Level.INFO,"Richiesta con sottostato::"+sottoStato+" trovato");
			return true;
		}
		else{
			return false;
		}
	}


}
