package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class VerificaDocumentoComponent extends BaseComponent {

    WebDriver driver;
    SeleniumUtilities util;
    private SpinnerManager spinnerManager;

    public By sezioneDocumenti = By.xpath("//a//span[text()='Documenti']/ancestor::a/span[2]");
    public By linkVisualizzaTutto = By.xpath("//a[contains(@href,'/lightning/r') and (contains(@href,'/related/Documents__r/view') or contains(@href,'/related/Documents1__r/view')) ]//span[text()='Visualizza tutto']");
    public By linkVisualizzaTuttoTouchPoint = By.xpath("//a[contains(@href,'/lightning/r') and contains(@href,'/related/Touch_Point__r/view')]//span[text()='Visualizza tutto']");
    public By buttonChiudiDocumenti = By.xpath("//button[@title='Chiudi Documenti']");
    public By buttonChiudiTouchPoint = By.xpath("//button[@title='Chiudi Touch Point']");
    public By sezioneTouchPoint = By.xpath("//a//span[text()='Touch Point' and @title='Touch Point']");
    public String colonne[] = {"Document Ref", "Tipo di record", "Canale di Invio", "Direzione", "Modello", "Link", "Stato", "Richiesta", "Case Item", "Data Creazione"};
    public By ordineVolture = By.xpath("//*[contains(text(),'Ordine')]/ancestor::span/a");
//  public By inAttesa = By.xpath("//table//*[text()=\"Stato dell'Ordine\"]/ancestor::table//td//*[text()='In attesa']");
    public By inAttesa = By.xpath("//span[text()='Configurations (Order Header)']/ancestor::lst-common-list-internal//table//*[text()=\"Stato dell'Ordine\"]/ancestor::table//td//*[text()='In attesa']");

//  public By Completato = By.xpath("//table//span[text()=\"Stato dell'Ordine\"]/ancestor::table//td[@data-label=\"Stato dell'Ordine\"]");
    public By Completato = By.xpath("//span[text()='Configurations (Order Header)']/ancestor::div[@class='listWrapper']//table//span[contains(text(),'Stato') and contains(text(),'Ordine')]/ancestor::table//td[contains(@data-label,'Stato') and contains(@data-label,'Ordine')]");  
    public By chiudiOR = By.xpath("//button[contains(@title,'Chiudi ORH-')]");
    public By tabCorrelato = By.xpath("//a[@role='tab' and text()='Correlato']");
    public By tabDettagli = By.xpath("//a[@data-label='Dettagli' and @id='detailTab__item' and @tabindex='-1']");
    public By sezioneDocumento = By.xpath("//span[@title='Documenti']");
    public By sezioneAllegati = By.xpath("//*[text() = 'Allegati']");

    /**
     *
     * @param driver
     */
    public VerificaDocumentoComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
        spinnerManager = new SpinnerManager(driver);
    }


    /**
     * caricaSezioneDocumenti
     */
    public void caricaSezioneDocumenti() {
        FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 180).ignoring(WebDriverException.class).pollingEvery(1, TimeUnit.SECONDS).withMessage("Attesa sezione Documento");
        wait.until(ExpectedConditions.visibilityOfElementLocated(sezioneDocumento));
    }


    /**
     *
     * @param object
     * @param testo
     * @throws Exception
     */
    public void verificaSezioneDocumentiNonVuota(By object, String testo) throws Exception {
        String text = util.waitAndGetElement(object).getText();
        text = text.replaceAll("(\r\n|\n)", " ");
        if (text.contains(testo))
            throw new Exception("la sezione documenti è priva di documenti");
    }


    /**
     *
     * @param col
     * @throws Exception
     */
    public void verificaColonneTabella(String[] col) throws Exception {
        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//div[@data-aura-class='forceBrandBand']//table/thead/tr/th[contains(@class,'initialSortAsc') or contains(@class,'initialSortDesc')]//div//a//span[@class='slds-truncate']"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i < col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//div[@data-aura-class='forceBrandBand']//table/thead/tr/th[contains(@class,'initialSortAsc') or contains(@class,'initialSortDesc')][" + (i + 1) + "]//div//a//span[@class='slds-truncate']"));
                String columnName = c.getText();
                //	System.out.println(columnName);
                if (!columnName.contentEquals(col[i].trim()))
                    throw new Exception("la tabella contenente la lista dei documenti non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        } else throw new Exception("la tabella contenente la lista dei documenti non e' correttamente visualizzata ");
    }


    /**
     *
     * @param modello
     * @throws Exception
     */
    
    public boolean verificaValoreInTabella(String modello) throws Exception {
        boolean modelloPresente = false;
        for (int i = 0; i < 5; i++) {
            if (!util.verifyExistence(By.xpath("//div[@data-aura-class='forceBrandBand']//table/tbody/tr/td//span//span[text()='" + modello + "']"), 3)) {
                driver.findElement(By.xpath("//a[@class='toggle slds-th__action slds-text-link--reset ']//span[text()='Modello']"));
                checkSpinnersSFDC();
            } else {
                modelloPresente = true;
                break;
            }
        }
        if (!modelloPresente) {
            throw new Exception("il documento con descrizione " + modello + " non e' presente in tabella");
        }
		return modelloPresente;
    }


    /**
     *
     * @throws Exception
     */
    public void contaAttesa() throws Exception {
        if (!util.verifyExistence(By.xpath("//span[text()='Configurations (Order Header)']//ancestor::a//span[text()='(2)']"), 3)) {
            throw new Exception("nella tabella 'Order Header/Configuration non sono presenti due Item");
        }
        List<WebElement> stato = driver.findElements(inAttesa);
        if (stato.size() != 2) {
            throw new Exception("lo stato della richiesta per il cliente uscente e entrante è 'IN ATTESA'");
        }
    }


    /**
     *
     * @throws Exception
     */
    public void contaCompletato() throws Exception {
        if (!util.verifyExistence(By.xpath("//span[text()='Configurations (Order Header)']//ancestor::a//span[text()='(2)']"), 3)) {
            throw new Exception("nella tabella 'Order Header/Configuration non sono presenti due Item");
        }
        List<WebElement> stato = driver.findElements(Completato);
        System.out.println(stato.size());
        if (stato.size() != 2) {
            throw new Exception("lo stato della richiesta per il cliente uscente e entrante è 'Completato'");
        }
    }


    /**
     *
     * @param touchpoint
     * @param stato
     * @throws Exception
     */
    public void verificaValoreInTabellaTouchPoint(String touchpoint, String stato) throws Exception {
        String touchpointPresente = "NO";
        for (int i = 0; i < 5; i++) {
            if (stato.contentEquals("Creato")) {
                if (!util.verifyExistence(By.xpath("//div[@data-aura-class='forceBrandBand']//table/tbody/tr/td//span//span[contains(text(),'" + touchpoint + "')]//ancestor::tr[1]//td//span//span[text()='" + stato + "']"), 3)) {
                    driver.findElement(By.xpath("//a[@class='toggle slds-th__action slds-text-link--reset ']//span[text()='Tipo e Sezione']")).click();
                    checkSpinnersSFDC();
                } else {
                    touchpointPresente = "SI";
                    break;
                }
            } else {
                if (!util.verifyExistence(By.xpath("//div[@data-aura-class='forceBrandBand']//table/tbody/tr/td//span//span[contains(text(),'" + touchpoint + "')]//ancestor::tr[1]//td//span//span[text()='" + stato + "']"), 3)) {
                    driver.findElement(By.xpath("//a[@class='toggle slds-th__action slds-text-link--reset ']//span[text()='Tipo e Sezione']")).click();
                    checkSpinnersSFDC();
                } else {
                    touchpointPresente = "SI";
                    break;
                }
            }
        }
        if (touchpointPresente.contentEquals("NO"))
            throw new Exception("il touch point con descrizione " + touchpoint + " non e' presente in tabella");
    }
}
