package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

import org.junit.Assert;

public class Precheck2GComponents extends BaseComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public Precheck2GComponents(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public By esitoPrecheck = By.xpath("//div[@class='iconHolder']//img");
	public By pod = By.xpath("//lightning-layout-item[@class='slds-p-around_small slds-col slds-size_9-of-12']//p");

	public void verificaPrecheckOK() {
		try {
			WebElement esito = driver.findElement(esitoPrecheck);
			esito.getAttribute("title");
			Assert.assertEquals("Esito Precheck 2G OK", esito.getAttribute("title"));
			System.out.println("Esito Precheck 2G OK!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

