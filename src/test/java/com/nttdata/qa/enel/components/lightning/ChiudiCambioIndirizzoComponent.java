package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ChiudiCambioIndirizzoComponent extends BaseComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public By btnConfermaCambio = By.xpath("//div[@class='oneWorkspaceTabWrapper']"
			+ "//div[@class='slds-page-header header-fixed']//button[text()='Conferma'][1]");

	public By btnOkModal = By.id("theModalCmpBtn_A");
	public By buttonOk = By.xpath("//*[contains(@id,'theModalCmp') and text()='OK']");
	public By modalBolletta = By.xpath("//div[@id='theModalCmp_B']//p[contains(text(),'Il cliente è interessato all’attivazione della Bolletta in formato elettronico')]");
    public By buttonNo = By.xpath("//div[@id='theModalCmp_B']//button[text()='No']");
    public By buttonSi = By.xpath("//div[@id='theModalCmp_B']//button[text()='Sì']");
    
    
	//pop-up Operazione eseguita con successo
	public By popUpOperazioneOK = By.xpath("//span[text()='Operazione Conclusa con Successo']");
	public By popUpOperazioneOKTesto = By.xpath("//span[text()='Operazione Conclusa con Successo']/ancestor::div[2]//p");
	public By buttonOKpopUpOperazioneOK = By.xpath("//span[text()='Operazione Conclusa con Successo']/ancestor::div[2]//button[text()='OK']");
	public String testopopUpOperazioneOK="L' operazione è stata eseguita correttamente";
	
	
	public ChiudiCambioIndirizzoComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	public void press(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void confermaCambioIndirizzoFornitura() throws Exception {
		util.objectManager(btnConfermaCambio, util.scrollAndClick);
		spinner.checkSpinners();

	}
	
	public void verificaPopUpconTestoOpzionale(By obj, By objText,By button,String popupTestoAtteso) throws Exception{
		WebElement webElement = driver.findElement(By.xpath("//body"));
		webElement.sendKeys(Keys.TAB);
		spinner.checkSpinners();	
		if(util.exists(obj, 60)){
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);
		}
	}
	
	
	public void verificaPopUpconTesto(By obj, By objText,By button,String popupTestoAtteso) throws Exception{
		if(!util.verifyExistence(obj, 60)) throw new Exception("Non è stata visualizzata la pop up con testo: "+popupTestoAtteso);
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);

	}
	
	
	
	public void confermaAnnullaVas(By sino) throws Exception{
		
		if(!util.verifyExistence(modalBolletta, 20)) throw new Exception("Dopo aver confermato l'operatività non viene fuori la modale con la richiesta attivazione VAS bolletta web");
		else {
			util.objectManager(sino, util.scrollAndClick);
			spinner.checkSpinners();
		}
	}
	
}
