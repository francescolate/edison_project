package com.nttdata.qa.enel.components.r2d;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_SimulatoreComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public R2D_SimulatoreComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	public By selectServizio = By.xpath("//select[@id='idservizio']");
	public By selectFlusso = By.xpath("//select[@id='flussoId']");
	public By inputPOD = By.xpath("//input[@name='pdr']");
	public By buttonCerca = By.xpath("//img[@alt='Cerca'] | //*[contains(text(),'Cerca')]");
	public By inputEsito = By.xpath("//td[text()='esito ']/ancestor::tr[1]//input");
	public By inputStatoFornitura = By.xpath("//td[text()='stato fornitura ']/ancestor::tr[1]//input");
	public By buttonCarica= By.xpath("//img[@alt='Carica'] | //a[contains(text(),'Carica')]");
	
	

//	public void cercaFlusso(String servizio,String flusso, String pod, String matricola) throws Exception{
		public void cercaFlusso(String servizio,String flusso, String pod) throws Exception{
		//Selezione Servizio
		util.objectManager(selectServizio,util.scrollAndSelectByValue,servizio);
		TimeUnit.SECONDS.sleep(2);
		//Selezione Flusso
		util.objectManager(selectFlusso,util.scrollAndSelectByValue,flusso);
		TimeUnit.SECONDS.sleep(2);
		//Inserimento POD
		util.objectManager(inputPOD,util.scrollAndSendKeys,pod);
		TimeUnit.SECONDS.sleep(2);
		//Click su cerca
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
		
		public void configutaSimulatoreFlussoCKPOD(String esito,String statoFornitura) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		//Selezione esito
		util.objectManager(inputEsito,util.scrollAndSendKeys,esito);
		TimeUnit.SECONDS.sleep(2);
		//Selezione stato fornitura
		util.objectManager(inputStatoFornitura,util.scrollAndSendKeys,statoFornitura);
		TimeUnit.SECONDS.sleep(2);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		driver.switchTo().alert().accept();

	}
	
}
