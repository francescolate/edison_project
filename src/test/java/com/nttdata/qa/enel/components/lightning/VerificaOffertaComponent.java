package com.nttdata.qa.enel.components.lightning;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaOffertaComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public VerificaOffertaComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
	  
    public final By elementiOffertaSection = By.xpath("//div[contains(text(),'Elementi') and contains(text(),'Offerta')]/parent::section");
    public final By oiOfferta = By.xpath("//div[contains(text(),'Elementi') and contains(text(),'Offerta')]/ancestor::div[@class='slds-form-element']//a[contains(text(),'OI-')]");
	  
	  
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	
}
