package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GetUserDetailFromWorkBenchComponent {

	WebDriver driver;
	SeleniumUtilities util;
	Properties prop = null;
	ModificaInfoEnelEnergiaComponent mc = new ModificaInfoEnelEnergiaComponent(driver);
	
	public By queryResultFields = By.xpath("//*[@id='query_results']/tbody/tr/th");
	public By queryInputField = By.xpath("//textarea[@id='soql_query_textarea']");
	public By supplyNum_277 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By supplyNum_278 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By supplyNum_259 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By caseNum_263a = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By caseNum_263b = By.xpath("//table[@id='query_results']/tbody/tr[2]");
	public By caseNum_264a = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By caseNum_264b = By.xpath("//table[@id='query_results']/tbody/tr[2]");
	public By supplyNum_276 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By supplyNum_261 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By supplyNum_174 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By supplyNum_174a = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By status_318_Annullato = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");  
	public By status_318_AnnullatoNew = By.xpath("//*[text()='Annullato']");
	public By status_318_Inlavorazione = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]"); 
	public By status_318_InlavorazioneNew = By.xpath("//*[text()='In attesa']"); 
	public By date_260 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By email_260 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By cellulare_260 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	public By staus_260 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	public By CF_260 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	
	public By status_260B = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	public By subStatus_260B = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	public By email_260B = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	public By operationType_260B = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	public By Id_260B = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[2]");
	
	public By AccountType_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[2]");
	public By Subject_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By POD_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By R2DStatus_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	public By R2RStatus_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	public By SAPStatus_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	public By SAPFICAStatus_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	public By SAPMM_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[9]");
	public By SAPSD_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[10]");
	public By SEMPRE_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[11]");
	public By UDBStatus_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[12]");
	public By NECStatus_260C = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[13]");
	
	//338 a
	public By description_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	public By activityStatus_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By CF_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	public By category_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	public By IFM_Causale_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	public By cabDescription_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	public By email_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[9]");
	public By Descrizione_Tripletta_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[10]");
	public By IBAN_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[11]");
	public By MMP_Reason_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[12]");
	public By MMP_Reason_SDD_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[13]");
	public By operationType_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[14]");
	//338 b
	public By acoountId_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]//a");
	public By description_338b = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By Current_Method_Of_Payment_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	public By Method_Of_Payment_Previous_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	public By New_IBAN_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	public By New_Method_Of_Payment_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	public By New_Method_Of_Payment_Name_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[9]");
	public By POD_PDR_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[10]");
	public By R2D_Status_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[11]");
	public By SAP_Status_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[12]");
	public By Service_Use_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[13]");
	public By IFM_Status_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[14]");
	public By IFM_Subject_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[15]");
	public By UDB_Status_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[16]");
	
	public By operationStatus_338 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By IFM_Status_338c = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");

	public GetUserDetailFromWorkBenchComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
		}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	 
	public void verifyAccountDetails(By checkObject, String[] Feild, String[] Value) throws Exception{
		
		util.exists(checkObject, 10);
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmActual = new HashMap<String, String>();
		String key, val ;
		List <WebElement> list  = driver.findElements(checkObject);
		for (int i=1; i<=list.size();i++)
		{
			key = driver.findElement(By.xpath("//*[@id='query_results']/tbody/tr/th["+ i +"]")).getText();
			val = driver.findElement(By.xpath("//*[@id='query_results']/tbody/tr/td["+ i +"]")).getText();
			hm.put(key, val);
			}
		for (int i=0; i<Value.length;i++){
		/*hmActual.put(Feild[i], Value[i]);
		if(!hm.equals(hmActual))
    		throw new Exception ("The account details are not correctly displayed");*/
			/*System.out.println(Value.toString());
			System.out.println(hm.get(i));*/
			if(!Arrays.asList(Value).contains(hm.get(Feild[i])))
				System.out.println(Arrays.asList(Value));
				System.out.println(hm.get(Feild[i]));
				throw new Exception(hm.get(i)+" Displaying value is not mathcing with the actual result "+ Value);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public static final String[] QUERY_FIELDS_295 = {" ","CaseNumber","Status","ITA_IFM_Email__c","ITA_IFM_MMP_Reason__c","ITA_IFM_Operation_Type__c","ITA_IFM_PEC__c","ContactMobile","ContactPhone","Origin"};
	public static final String[] QUERY_VALUES_295 = {"1","0010Y00000F8ifXQAR","BANCA MONTE DEI PASCHI DI","FATTO","HRNMNK46H63Z112Q","Processo","Gestione Cliente","FILIALE DI AOSTA","raffae.ll.aquomo@gmail.com","Modifica Modalità Pagamento","IT75W0103001200010101010101","VARIAZIONE RID","Variazione SDD","MODIFICA"};
	public static final String[] QUERY_FIELDS_338a = {" ","AccountId","ITA_IFM_ABI_Description__c","ITA_IFM_ActivityStatus__c","ITA_IFM_Bank_Account_Holder_FiscalCode__c","ITA_IFM_Category__c","ITA_IFM_Causale__c","ITA_IFM_CAB_Description__c","ITA_IFM_CommunicationEmailAddress__c","ITA_IFM_Descrizione_Tripletta__c","ITA_IFM_IBAN__c","ITA_IFM_MMP_Reason__c","ITA_IFM_MMP_Reason_SDD__c","ITA_IFM_Operation_Type__c"};
	public static final String[] QUERY_VALUES_338a = {"1","0010Y00000F8ifXQAR","BANCA MONTE DEI PASCHI DI","FATTO","HRNMNK46H63Z112Q","Processo","Gestione Cliente","FILIALE DI AOSTA","raffae.ll.aquomo@gmail.com","Modifica Modalità Pagamento","IT75W0103001200010101010101","VARIAZIONE RID","Variazione SDD","MODIFICA"};
	public static final String Status_260 = "CERTIFICATA";
	public static final String Status_260B = "Chiuso";
	public static final String SubStatus_260B = "Ricevuto";
	public static final String OperationTYpe_260B = "ATTIVAZIONE";
	public static final String AccountType260C = "NON RESIDENZIALE"; 
	public static final String Subject260C = "ATTIVAZIONE VAS"; 
	public static final String POD260C = "IT001E04040599"; 
	public static final String Status_260C = "NON PREVISTO"; 
	public static final String NECStatus260C = "OK"; 
	public static final String Description_338 = "BANCA MONTE DEI PASCHI DI SIENA S.P.A.";
	public static final String ActivityStatus_338 = "FATTO";
	public static final String Catergory_338 = "Processo";
	public static final String Causale__c_338 = "Gestione Cliente";
	public static final String CABDescription_338 = "FILIALE DI AOSTA";
	public static final String DescrizioneTripletta_338 = "Modifica Modalità Pagamento";
	public static final String MMPReason_338 = "VARIAZIONE RID";
	public static final String MMPReasonSDD_338 = "Variazione SDD";
	public static final String Reason_SDD_338 = "Variazione SDD";
	public static final String OperationType_338 = "MODIFICA";
	public static final String Method_Of_Payment_338 = "SDD";
	public static final String New_IBANValue_338 = "IT75W0103001200010101010101";
	public static final String New_MethodOfPayment_338 = "RID";
	public static final String SAPStatus_338 = "DA INVIARE";
	public static final String IFM_Service_Use_338 = "Uso Abitativo";
	public static final String IFMStatus_338 = "In attesa";
	public static final String IFMSubject_338 = "VARIAZIONE SDD";
	public static final String UDBStatus_338 = "IN ATTESA";
	public static final String IFMStatus_338c = "Working";
	public static final String OperationStatus_338 = "In attesa";

}
