package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ModifyConsents_SF_Component extends BaseComponent {

	public By searchField = By.xpath("//input[@role='combobox' and contains(@title,'Cerca')]");
	public By searchFieldClassic = By.id("phSearchInput");
	public By searchBtn = By.id("phSearchButton");
	public By clientNameLink = By.xpath("//a[text()='Clienti']/ancestor::div[contains(@class,'resultsItem slds-col slds-no-flex slds-m-bottom_small')]//table/tbody/tr/th/span/a");
	public By clientNameLinkClassic = By.cssSelector("#Account_body > table > tbody > tr.dataRow.even.last.first > th > a");
	public By visualizzaTuttoPrivacyBtn = By.cssSelector("#tab-15 > slot > flexipage-component2:nth-child(2) > slot > flexipage-aura-wrapper > div > div > div > div:nth-child(8) > article > a > div > span");
	public By dettagliBtn = By.xpath("//a[contains(@class,'optionItem efpDetailsView')]/span[text()='Dettagli']");
	public By storicoPrivacyBtn = By.xpath("//h2[@class='slds-card__header-title']/a/span[text()='Storico Privacy']");
	public By storicoPrivacyBtnClassic = By.xpath("//span[@class='listTitle' and text()='Storico Privacy']");
	public By firstRowDate = By.xpath("((//div[contains(@id, 'brandBand_')]//table/tbody/tr[1]/td[3]/span/span)[last()]");
	public By secondRowDate = By.xpath("((//div[contains(@id, 'brandBand_')]//table/tbody/tr[2]/td[3]/span/span)[last()]");
	public By dateOrderBtn = By.xpath("//span[@title='Data di Creazione']");
	public By firstPrivacyName = By.xpath("(//a[contains(@title,'Privacy Name')])[1]");
	public By firstPrivacyNameClassic = By.xpath("//tbody/tr/td[2]//tr[contains(@class,'first')]/th[@scope='row']/a[contains(text(),'Privacy Name')]");
	public By deleteBtn = By.xpath("//button[@name='Delete']");
	public By[] delPopupLocators = {
			By.xpath("//h2[contains(@class,'title') and text()='Elimina Storico Privacy']"),
			By.xpath("//div[contains(@class,'modal-body')]/div[contains(@class,'detail')]")
	};
	public By cancelBtn = By.xpath("//div[contains(@class,'modal-footer')]/div/button/span[contains(@dir,'ltr') and text()='Annulla']");
	public By confirmDelBtn = By.xpath("//div[contains(@class,'modal-footer')]/div/button/span[contains(@dir,'ltr') and text()='Elimina']");
	public By creationDateDescending = By.xpath("//span[text()='Data di Creazione']/ancestor::th[@class='initialSortDesc sortable descending slds-is-sortable  slds-is-resizable']");
	
	public ModifyConsents_SF_Component (WebDriver driver) {
		super(driver);
	}
	
	public void prova() throws Exception {
		util.salesForceClassicModePlease();
	}
	
	public void searchCfIva(String cfIva) throws Exception {
		verifyVisibilityThenClick(searchField);
		insertTextAndReturn(searchField, cfIva);
	}
	
	public void accessStoricoPrivacy() throws Exception {
//		verifyVisibilityThenClick(dettagliBtn);
//		verifyVisibilityThenClick(storicoPrivacyBtn);
//		verifyVisibilityThenClick(firstPrivacyName);
		
		WebDriverWait wait = new WebDriverWait(driver, baseTimeoutInterval);
		Thread.sleep(30000);
		util.scrollToElement(wait.until(ExpectedConditions.presenceOfElementLocated(storicoPrivacyBtn)));
		verifyComponentVisibility(storicoPrivacyBtn);
		
		clickComponentWithJse(storicoPrivacyBtn);
		//System.out.println(driver.getWindowHandle());
		util.waitAndGetElement(this.creationDateDescending);
		Thread.sleep(2000);
		verifyVisibilityThenClick(firstPrivacyName);
	}
	
	private boolean checkDatesSortingDescending() throws Exception {
		By[] locators = {firstRowDate, secondRowDate};
		verifyElementsArrayVisibility(locators);
		SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyyy HH.mm");
		String dateString1 = getElementTextString(firstRowDate);
		String dateString2 = getElementTextString(secondRowDate);
		System.out.println("Prima data: " + dateString1);
		System.out.println("Seconda data: " + dateString2);
		Date date1 = sdformat.parse(dateString1);
		Date date2 = sdformat.parse(dateString2);
		if (date1.after(date2)) {
			System.out.println("Discendente");
			return true;
		} else {
			//Let's change the sorting order
			System.out.println("Ascendente");
			verifyVisibilityThenClick(dateOrderBtn);
			return false;
		}
	}
	
	public void deleteRecord() throws Exception {
		verifyVisibilityThenClick(deleteBtn);
		verifyElementsArrayText(delPopupLocators, delPopupStrings);
		verifyComponentVisibility(cancelBtn);
		verifyVisibilityThenClick(confirmDelBtn);
	}

	//----- Constants -----
	
	private String[] delPopupStrings = {
			"Elimina Storico Privacy",
			"Eliminare questo elemento Storico Privacy?"
	};
}

