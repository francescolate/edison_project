package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OreFree157Component {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By haiBisognoDiAiutoHeading =  By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-block next-day-orefree'][2]//h2");
	public By haiBisognoDiAiutoLink = By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-block next-day-orefree'][2]//a[1]");
	public By domandeFrequentiHeading = By.xpath("//h3[@id='faq-disattivazione']");
	public By questionOne = By.xpath("//section[@id='landing-accordion']/ul/li[1]/h4");
	public By plusOne = By.xpath("//button[@id='accordion-button-item-accordion-0']/span");
	public By answerOne = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-0']");
	public By questionTwo = By.xpath("//section[@id='landing-accordion']/ul/li[2]/h4");
	public By plusTwo = By.xpath("//button[@id='accordion-button-item-accordion-1']/span");
	public By answerTwo = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-1']/p");
	public By questionThree = By.xpath("//section[@id='landing-accordion']/ul/li[3]/h4");
	public By plusThree = By.xpath("//button[@id='accordion-button-item-accordion-2']/span");
	public By answerThree = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-2']");
	public By questionFour = By.xpath("//section[@id='landing-accordion']/ul/li[4]/h4");
	public By plusFour = By.xpath("//button[@id='accordion-button-item-accordion-3']/span");
	public By answerFour = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-3']/p");
	public By questionFive = By.xpath("//section[@id='landing-accordion']/ul/li[5]/h4");
	public By plusFive = By.xpath("//button[@id='accordion-button-item-accordion-4']/span");
	public By answerFive = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-4']");
	public By questionSix = By.xpath("//section[@id='landing-accordion']/ul/li[6]/h4");
	public By plusSix = By.xpath("//button[@id='accordion-button-item-accordion-5']/span");
	public By answerSix = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-5']");
	public By questionSeven = By.xpath("//section[@id='landing-accordion']/ul/li[7]/h4");
	public By plusSeven = By.xpath("//button[@id='accordion-button-item-accordion-6']/span");
	public By answerSeven = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-6']/p");
	public By questionEight = By.xpath("//section[@id='landing-accordion']/ul/li[8]/h4");
	public By plusEight= By.xpath("//button[@id='accordion-button-item-accordion-7']/span");
	public By answerEight = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-7']/p");
	public By questionNine = By.xpath("//section[@id='landing-accordion']/ul/li[9]/h4");
	public By plusNine= By.xpath("//button[@id='accordion-button-item-accordion-8']/span");
	public By answerNine = By.xpath("//section[@id='landing-accordion']//div[@id='accordion-panel-item-accordion-8']/p");
	
	
    public OreFree157Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
    
    public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
    
    public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
    
    public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
		
			}
	public static final String HAIBISOGNODIAIUTO_HEADING = "Hai bisogno di aiuto?";
	public static final String Domandefrequenti_Heading = "Domande frequenti";
	public static final String QUESTION_ONE = "Come si compone la spesa relativa all'offerta Scegli Tu Ore Free?";
	public static final String ANSWER_ONE = "L'offerta prevede un prezzo della componente energia monorario fisso e invariabile per 12 mesi decorrenti dalla Data di attivazione della fornitura. Nella fascia delle 3 Ore Free scelta dal cliente tale prezzo sarà scontato del 100% e quindi pari a 0 €/kWh, IVA e imposte escluse.Le altre voci di spesa, ovvero dispacciamento, oneri di commercializzazione e vendita, corrispettivo di sbilanciamento, spesa per oneri di sistema, spesa per il trasporto e la gestione del contatore, imposte ed IVA, saranno regolarmente applicate nella misura e secondo le modalità definite all'interno delle Condizioni Tecnico Economiche e delle condizioni Generali di Fornitura.";
	public static final String QUESTION_TWO = "Che cosa si intende per Fascia di Ore Free?";
	public static final String ANSWER_TWO = "E' la fascia oraria giornaliera consistente in 3 ore consecutive per le quali il prezzo della componente energia, applicato anche alle relative perdite di rete, sarà scontato del 100% e quindi pari a 0 €/kWh,IVA e imposte escluse.";
	public static final String QUESTION_THREE = "Come è possibile modificare la fascia scelta?";
	public static final String ANSWER_THREE = "È possibile modificare gratuitamente la fascia scelta (o la fascia preimpostata) in ogni momento della giornata, tramite l’APP, l’Area Clienti di Enel Energia ed anche con un comando vocale tramite Alexa, secondo le modalità previste:- Cambio fascia giornaliera: scegliendo una nuova fascia solo per ciascuno dei 7 giorni successivi- Cambio fascia base: scegliendo una nuova fascia \"base\", che sarà applicata a tutti i giorni successivi, andando a sostituire la fascia prescelta in fase di adesione e sovrascrivendo qualunque eventuale modifica effettuata in precedenza.E' inoltre possibile modoficare anche la fascia del giorno in corso, fino a 15 minuti prima dell'orario di inizio di quella già in vigore.";
	//public static final String ANSWER_THREE = "È possibile modificare gratuitamente la fascia scelta (o la fascia preimpostata) in ogni momento della giornata, tramite l'App di Enel Energia e nell'area privata secondo le modalità previste:- Cambio fascia giornaliera: scegliendo una nuova fascia solo per ciascuno dei 7 giorni successivi- Cambio fascia base: scegliendo una nuova fascia \"base\", che sarà applicata a tutti i giorni successivi, andando a sostituire la fascia prescelta in fase di adesione e sovrascrivendo qualunque eventuale modifica effettuata in precedenza.E' inoltre possibile modoficare anche la fascia del giorno in corso, fino a 15 minuti prima dell'orario di inizio di quella già in vigore.";
	public static final String QUESTION_FOUR = "Quante volte è possibile modificare fascia?";
	public static final String ANSWER_FOUR = "Non esiste un numero massimo di volte entro cui è possibile effettuare il cambio fascia: sarà quindi possibile procedere al cambio in ogni momento e fino a 15 minuti prima dell'orario di inizio della fascia già in vigore (Esempio: se è in vigore la fascia 14:00 - 17:00 sarà possibile cambiare la fascia entro massimo le ore 13:45).Verrà ritenuta valida solamente l'ultima modifica effettuata in ordine di tempo, insistente sullo stesso giorno.";
	public static final String QUESTION_FIVE = "Non riesco a cambiare la fascia di Ore Free per oggi, come mai?";
	public static final String ANSWER_FIVE = "Il cambio della fascia giornaliera di ore free è consentito solo fino a 15 minuti prima dell'orario di inizio della fascia già in vigore. Dopo tale periodo, in caso di cambio fascia giornaliera, il giorno odierno non sarà più selezionabile e verrà mostrato direttamente il giorno successivo, su cui potrà essere effettuata la modifica della fascia.In caso invece di cambio fascia base, la modifca verrà effettuata a partire dal giorno successivo.";
	public static final String QUESTION_SIX = "Ho sbagliato a impostare le mie fasce giornaliere, come posso tornare indietro?";
	public static final String ANSWER_SIX = "Puoi reimpostare la fascia base già definita, attraverso la funzionalità di ripristino della fascia base: in questo modo verrà applicata per tutti i giorni la fascia base già scelta precedentemente.";
	public static final String QUESTION_SEVEN = "Come vengono calcolati i corrispettivi previsti dall'offerta?";
	public static final String ANSWER_SEVEN = "La modalità di calcolo dei corrispettivi avverrà sulla base di quanto indicato all'art.6 delle Condizioni Generali di Fornitura. In particolare il calcolo dei consumi periodici sarà basato sui dati per fasce (totalizzatori) resi disponibili dal Distributore, mentre la definizione del consumo attribuibile alla Ore Free per calcolare l'importo dello sconto, sarà determinato sulla base delle curve di consumo giornaliere, rese disponibili dal Distributore. In caso di assenza di dati reali, quanto precedentemente indicato, verrà calcolato sulla base dei consumi stimati. L'utilizzo di dati stimati potrà determinare ricalcoli qualora vengano resi successivamente disponibili dati di consumo reali";
	public static final String QUESTION_EIGHT = "Dove posso visualizzare i miei consumi giornalieri e le Ore Free scelte?";
	public static final String ANSWER_EIGHT = "È possibile visualizzarli accedendo all'APP di Enel Energia o all'Area Privata, all'interno del Dettaglio Fornitura.Per ogni giorno passato (con uno storico massimo di 6 mesi) in cui sono disponibili i dati di consumo, verrà visualizzato un grafico che riporterà l'andamento dei consumi con dettaglio orario e le informazioni in merito a:";
	public static final String QUESTION_NINE = "Perché non riesco a vedere i consumi in tempo reale?";
	public static final String ANSWER_NINE = "L’infrastruttura del contatore elettronico di seconda generazione è in grado di fornire e validare dati di misura ogni quarto d’ora, che vengono però messi a disposizione di Enel Energia solamente a partire dalle successive 24 ore.";
	
	
}

