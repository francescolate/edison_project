package com.nttdata.qa.enel.components.lightning;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.*;

import org.eclipse.persistence.internal.oxm.record.deferred.StartCDATAEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class RecordingObjectRuntime {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public RecordingObjectRuntime(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	 
	  public void start() {

			JFrame f=new JFrame();//creating instance of JFrame  
			JTextField t1 = new JTextField();  
			JTextField t2 = new JTextField();  
			JButton b=new JButton("Find");//creating instance of JButton 
			JLabel label = new JLabel("evento: ");
			JLabel labelClick = new JLabel("click: ");
			JLabel labelInput = new JLabel("input: ");
			JLabel labelXpath = new JLabel("xpath: ");
			JLabel labelIframe = new JLabel("Iframe: ");
			JRadioButton EventClick = new JRadioButton("click");
			JRadioButton EventInput = new JRadioButton("Input");
			JButton EventSwitch = new JButton("Switch");
			 ButtonGroup group = new ButtonGroup();
			    group.add(EventClick);
			    group.add(EventInput);
			b.setBounds(130,100,100, 40);//x axis, y axis, width, height  
			label.setBounds(30, 130, 50, 80);
			labelClick.setBounds(100, 130, 50, 80);
			labelInput.setBounds(150, 130, 50, 80);
			EventClick.setBounds(130, 160, 20, 20);
			EventInput.setBounds(180, 160, 20, 20);
			t1.setBounds(30, 40, 300, 50);
			t2.setBounds(80, 210, 30, 30);
			labelIframe.setBounds(15, 210, 50, 30);
			labelXpath.setBounds(15, 15, 50, 30);
			EventSwitch.setBounds(120, 210, 80, 30);
			f.add(b);//adding button in JFrame  
			f.add(label);
			f.add(labelClick);
			f.add(labelInput);
			f.add(EventClick);
			f.add(EventInput);
			f.add(labelIframe);
			f.add(EventSwitch);
			f.add(t1);
			f.add(t2);
			f.add(labelXpath);
			f.setSize(400,500);//400 width and 500 height  
			f.setLayout(null);//using no layout managers  
			f.setVisible(true);//making the frame visible  
			 
			
		    
		    // add the listener to the jbutton to handle the "pressed" event
		    b.addActionListener(new ActionListener()
		    {
		      public void actionPerformed(ActionEvent e)
		      {
		        // display/center the jdialog when the button is pressed
		    	  JOptionPane.showMessageDialog(null, "ok");
		    	  if(EventClick.isSelected()) {
		    		  try {
						press(By.xpath(t1.getText()));
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "xpath non trovato o non raggiungibile");
					}
		    	  }else if (EventInput.isSelected()) {
		    		  driver.findElement(By.xpath(t1.getText())).sendKeys("Oggetto Raggiungo");
		    	  }else {
		    		  JOptionPane.showMessageDialog(null, "Errore non hai selezionato un evento");
		    	  }
		      }
		    });
		    
		    EventSwitch.addActionListener(new ActionListener()
		    {
			      public void actionPerformed(ActionEvent e)
			      {
			        // display/center the jdialog when the button is pressed
			    	  try {
						String frameName = new SeleniumUtilities(driver).getFrameByIndex(Integer.parseInt(t2.getText()));
						JOptionPane.showMessageDialog(null, frameName);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    	  
			      }
			    });
	  }
	  
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	
}
