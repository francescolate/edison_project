package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RecoverUsernameComponent extends BaseComponent {
	public By pageHeader = By.xpath("//div[@id='recovery-username-step1']/div/h1[@class='cmPageTitle cmTitle1']");
	public By pageSubHeader = By.cssSelector("#recovery-username-step1 > div.cmPageSubtitleCont.media > p");
	public By cfField = By.xpath("//div[@id='formRecoverUsernameCF']//input[@id='rec-username-fiscal-code']");
	public By sendBtn = By.id("rec-username-button");
	
	public RecoverUsernameComponent(WebDriver driver) {
		super(driver);
	}
	
}
