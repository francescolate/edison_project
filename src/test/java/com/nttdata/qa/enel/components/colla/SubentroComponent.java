package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SubentroComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By titoloPagesubentro=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[@class='image-hero_title text--page-heading' and text()='Come fare un subentro']");
	public By sottotitoloPagesubentro=By.xpath("//div[@class='image-hero_inner image-hero-container container']//p[@class='image-hero_detail text--detail' and text()='Riattivare un contatore in semplici passaggi']");
	public String linkSUBENTRO="it/supporto/faq/subentro";
	
	
	public SubentroComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
}
