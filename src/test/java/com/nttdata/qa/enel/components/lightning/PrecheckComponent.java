package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

import junit.framework.Assert;

public class PrecheckComponent extends BaseComponent{
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public PrecheckComponent(WebDriver driver) {
		  super(driver);
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
	  
   
//    public final By precheckButton = By.xpath("//button[@alt='Precheck']");
    public final By precheckButton = By.xpath("//button[@name='Precheck']"); 
//    public final By precheckButton = By.xpath("(//button[@name='Precheck'])[2]"); // 23/03/2021
    
  
    public final By precheckButton2 = By.xpath("//button[@name='Esegui precheck']");
//    public final By precheckButtonPODELE = By.xpath("//td[text()='ELETTRICO']/ancestor::tr/td/input[@type='checkbox']/ancestor::td/label/span");
    public final By precheckButtonPODELE = By.xpath("//div[text()='ELETTRICO']/ancestor::tr//input[@type='radio']/ancestor::div[1]/label[@class='slds-radio__label']/span");
//    public final By precheckButtonPODGAS = By.xpath("//td[text()='GAS']/ancestor::tr/td/input[@type='checkbox']/ancestor::td/label/span");
    public final By precheckButtonPODGAS = By.xpath("//div[text()='GAS']/ancestor::tr//input[@type='radio']/ancestor::div[1]/label[@class='slds-radio__label']/span");
//    public final By precheckOKPODELE = By.xpath("//td[text()='ELETTRICO']/ancestor::tr/td//span[contains(@id,'principalForm:insertedSuppliesTable')]");
    public final By precheckOKPODELE = By.xpath("//div[text()='ELETTRICO']/ancestor::tr[@class='slds-hint-parent']/td[5]/div/div");
//    public final By precheckOKPODGAS = By.xpath("//td[text()='GAS']/ancestor::tr/td//span[contains(@id,'principalForm:insertedSuppliesTable')]");
    public final By precheckOKPODGAS = By.xpath("//div[text()='GAS']/ancestor::tr[@class='slds-hint-parent']/td[5]/div/div");
//    public final String precheckPODMulti = "//td[text()='#COMMODITY#']/ancestor::tr/td[contains(text(),'#POD#')]/ancestor::tr//span[contains(@id,'principalForm:insertedSuppliesTable')]";
    public final String precheckPODMulti = "//*[text()='#COMMODITY#']/ancestor::tr//div/div[contains(text(),'#POD#')]/ancestor::tr//td[5]/div/div";
  
    //    public final String selezionaFornituraPodMulti = "//td[text()='#COMMODITY#']/ancestor::tr/td[contains(text(),'#POD#')]/ancestor::tr/td/input[@type='checkbox']/ancestor::td/label/span";
    public final String selezionaFornituraPodMulti = "//*[text()='#COMMODITY#']/ancestor::tr//div/div[contains(text(),'#POD#')]/ancestor::tr//input[@type='radio']/ancestor::td//label/span";
  
//    public final By EsitoPrecheck = By.xpath("//label[text()='Codice Esito Precheck']/..//span");
    public final By EsitoPrecheck = By.xpath("//label[text()='Esito Codice Precheck']/..//div/input");
  
//    public final By CodiceMotivazione = By.xpath("//label[text()='Codice Motivazione']/..//span");
    public final By CodiceMotivazione = By.xpath("//label[text()='Reason Codice Precheck']/..//div/input");
  
    public By codiceIstat=By.xpath("//div[@class='slds-modal__container']//tr[1]//label/span[@class='slds-radio_faux']");
//    public String codice_istat_by_name = "//div[@id='prompt-message-wrapper']//div[@data-label='##']/label";
    public String codice_istat_by_name = "//div[@id='modalIstatBody']//div[@data-label='CAPANNELLE']/label";
    public String codice_istat_Roma_xpath ="//*[@id=\"prompt-message-wrapper\"]/c-ita_ifm_lwc037_datatable/lightning-layout/slot/table/tbody/tr[3]/td[1]/c-ita_ifm_lwc045_input/lightning-layout/slot/lightning-layout-item/slot/div/div/div/div/label/span[1]";
    public By codice_istat_by_name_cla=By.xpath("//div[@data-label='CAPANNELLE']/label/span");
  
    public By tabellaSelezioneIstat=By.xpath("//h2[text()='Selezione Istat']");
    public By buttonConfermaIstat=By.xpath("//button[@name='Conferma Istat']");
//    public final By confermaPrecheck = By.xpath("//div[@id='mainId']//button[text()='Conferma']");
    public final By confermaPrecheck = By.xpath("//*[@name='Confirm']"); 
//    public final By confermaPrecheck = By.xpath("(//*[@name='Confirm'])[2]"); // 23/03/2021
    
  
//    public final By annullaPrecheck = By.xpath("//div[@id='mainId']//button[text()='Annulla']");
    public final By annullaPrecheck = By.xpath("//button[@name='Cancel']");
  
    public By insertPOD = By.xpath("//label[text()='POD/PDR']/..//input");
	public By insertCAP = By.xpath("//label[text()='CAP']/..//input");
	public By insertCAP_2 = By.xpath("(//label[text()='CAP'])[2]/..//input");

	public By RichiestaSupporto = By.xpath("//button[text()='Richiesta Supporto']");
//	public By Modifica = By.xpath("//input[@value='Modifica Offerta']");
	public By Modifica = By.xpath("//button[@name='Modify']");
	
	public By Avanti1 = By.xpath("//input[contains(@id, 'principalForm:idNextOfferSummary')]");
	public By Avanti2 = By.xpath("//input[contains(@id, 'principalForm:validate')]");
	public By Avanti3 = By.xpath("//button[contains(@id, 'nextOnResidential')]");
	public By annullaPopup = By.xpath("//*[@id='SWApopupKoPck']//button[text()='Annulla']");
//	public By confermaAnnullaOfferta = By.xpath("//*[@id='confirmCancellationButton']");
	public By confermaAnnullaOfferta = By.xpath("//*[@name='ConfirmAnnullmentQuote']");
	//non arrivo fino alla fine in quanto non richiesto
	public By descrizioneEsitoPrecheck = By.xpath("//span[text()='Descrizione esito precheck']/following::span");
	
	public By indirizzo_forzato_flag = By.xpath("//*[@id='toggle-desc']//span[contains(@id,'toggle-description-')]");
	public By first_address_in_list_cap = By.xpath("//div[@id='DivmodalModifyAddressABC']//li[@data-record='0']");
	public By indirizzo_forzato_flag_sezione_indirizzo_fatturazione = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@class='slds-card ']//*[@id='toggle-desc']//span[contains(@id,'toggle-description-')]");
	
    public final By popUpIstat = By.xpath("//h2[text()='Selezione Istat']");
    public final By primoIstat=By.xpath("//table[@role='table']//tr[1]/td[1]//label/span[1]");
    public final By confermaIstat=By.xpath("//button[@name='Conferma Istat']");
    
	//pop-up Errore Precheck Prima attivazione
	public By popUpPrecheck = By.xpath("//h2[text()='Attenzione - Errore Conferma']");
	public By popUpPrecheckTesto = By.xpath("//h2[text()='Attenzione - Errore Conferma']/ancestor::div[1]//p");
	public By buttonOKpopUpPrecheck = By.xpath("//h2[text()='Attenzione - Errore Conferma']/ancestor::div[1]//button[text()='OK']");
	public String testopopUpPrecheck="Lo stato del POD non è congruente con l'operazione di PRIMA ATTIVAZIONE";
	
	/* aggiunti da Mattia */
	public By ripetiPrecheck = By.xpath("//button[@name='Ripeti precheck']");
	public By modificaPrecheck = By.xpath("//button[@name='Modifica precheck']");
	public By istatElementoUno = By.xpath("//tbody//..//input[@aria-posinset='1']");
	
	
	
	public void press(By oggetto) throws Exception{
		util.getFrameActive();
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		driver.switchTo().defaultContent();
//		spinner.checkSpinners();
	}
	
	
	
	public void press(String frame,By oggetto) throws Exception{
		util.frameManager(frame,oggetto, util.scrollToVisibility, false);
		util.frameManager(frame,oggetto, util.scrollAndClick);
		Thread.currentThread().sleep(5000);
		//spinner.checkSpinners(frame);
	}
	
	public void verificaEsitoOffertabilita(String esito) throws Exception{		
		
		//table[contains(@id,'fornitureInserite')]//tr[contains(@class,'slds-is-selected')]//td[5]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']
		//	WebElement offertabilita = util.waitAndGetElement(By.xpath("//div[@title='Tipo Commodity']/ancestor::table//tbody//td[5]/div"));
			WebElement offertabilita = util.waitAndGetElement(By.xpath("//div[text()='Esito Offertabilità']/ancestor::table//tbody//tr[1]//td[5]/div"));
			if(!offertabilita.getText().contentEquals(esito)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+esito);
		
	}
	public void verificaEsitoOffertabilitaCla(String esito) throws Exception{		
		
		//table[contains(@id,'fornitureInserite')]//tr[contains(@class,'slds-is-selected')]//td[5]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']
		//	WebElement offertabilita = util.waitAndGetElement(By.xpath("//div[@title='Tipo Commodity']/ancestor::table//tbody//td[5]/div"));
			WebElement offertabilita = util.waitAndGetElement(By.xpath("//div[text()='Esito Offertabilità']/ancestor::table//tbody//tr[1]//td[5]/div"));
			if(!offertabilita.getText().contentEquals(esito))  {
				offertabilita = util.waitAndGetElement(By.xpath("//div[text()='Esito Offertabilità']/ancestor::table//tbody//tr[2]//td[5]/div"));
				if(!offertabilita.getText().contentEquals(esito)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+esito);
			}
		
	}
	
	public String recuperaOffertabilità() throws Exception {
		WebElement offertabilita = util.waitAndGetElement(By.xpath("//div[text()='Esito Offertabilità']/ancestor::table//tbody//tr[1]//td[5]/div"));
		String esito = offertabilita.getText();
		return esito;
	}
	
public void verificaEsitoOffertabilita2(String esito, String pod) throws Exception{		
		   String p="//div[text()='Esito Offertabilità']/ancestor::table//tbody//td//div//div[text()='#']/ancestor::tr//td[5]/div";
		   String xpathString=  p.replace("#", pod);
		   By item = By.xpath(xpathString);
		   WebElement offertabilita = util.waitAndGetElement(item);
		   if(!offertabilita.getText().contentEquals(esito)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+esito);
		
	}
	
	public String recuperaEsitoPrecheck() throws Exception{
//		Thread.currentThread().sleep(30000);
		Thread.currentThread().sleep(5000);
//		driver.switchTo().frame(1);
		util.getFrameActive();
//		String returned = driver.findElement(EsitoPrecheck).getText();
		WebElement TxtBoxContent = driver.findElement(EsitoPrecheck);
		String returned = TxtBoxContent.getAttribute("value");

		return returned;
	}
	
	public String recuperaCodiceMotivazione() throws Exception{
//		driver.switchTo().defaultContent();
//		driver.switchTo().frame(1);
		util.getFrameActive();
//		String returned = driver.findElement(CodiceMotivazione).getText();
		WebElement TxtBoxContent = driver.findElement(CodiceMotivazione);
		String returned = TxtBoxContent.getAttribute("value");
//		driver.switchTo().defaultContent();
		return returned;
	}
	
	public String recuperaDescrizionePrecheck() throws Exception{
		driver.switchTo().defaultContent();
		
		String returned = util.waitAndGetElement(descrizioneEsitoPrecheck,100,1).getAttribute("innerText");
	
		return returned;
	}
	
	public void effettuaPrecheck() throws Exception{
//		util.getFrameActive();
		press(precheckButton);
	}
	public String effettuaPrecheck(int i) throws Exception{
		driver.switchTo().defaultContent();
		String frame = util.getFrameByIndex(i);
		press(frame,precheckButton);
		driver.switchTo().defaultContent();
		Thread.currentThread().sleep(30000);
		return frame;
	}
	public void clickPrecheck() throws Exception{
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		press(precheckButton);
		driver.switchTo().defaultContent();
		
	}
	
	public String selezionaFortinuta(By oggetto) throws Exception{
		driver.switchTo().defaultContent();
		String frame = util.getFrameByIndex(1);
		press(frame,oggetto);
		driver.switchTo().defaultContent();
		return frame;
	}
	
	public void selezionaFornitura(By oggetto) throws Exception{
		util.getFrameActive();
		press(oggetto);
	}
	
	public String selezionaFortinutaMulti(String pod,String commodity) throws Exception{
		String pathFornitura = this.selezionaFornituraPodMulti.replaceAll("#COMMODITY#", commodity).replaceAll("#POD#", pod);
		By fornitura=By.xpath(pathFornitura);
//		driver.switchTo().defaultContent();
//		String frame = util.getFrameByIndex(1);
///////		util.getFrameActive();
		press(fornitura);
		driver.switchTo().defaultContent();
		return "";
	}
	
	public String generateRandomPodNumberEnergia() throws Exception {
		return util.generateRandomPodNumberEnergia();
	}
	
	public String generateRandomPodNumberGas() throws Exception {
		return util.generateRandomPodNumberGAS3();
	}
	
	public void verificaEsitoOKPrecheck(String frame) throws Exception{
	   WebElement x = util.commonFrameManagement(frame,By.xpath("//span[contains(@id,'principalForm:insertedSuppliesTable')]"));
       String esito = x.getText();
       if(!esito.contentEquals("OK")&& !esito.contentEquals("NA")) throw new Exception("L'esito del precheck risulta essere diverso da OK/NA. Esito: "+esito);
       driver.switchTo().defaultContent();
		
	}
	
	
	public String leggiEsitoPrecheck() throws Exception{
		   WebElement x = util.commonFrameManagement(By.xpath("//tr[@class='slds-hint-parent']/td[5]/div/div"));
		   String esito = x.getText();
	       return esito;
		}

	public void verificaEsitoOKPrecheck() throws Exception{
//		   WebElement x = util.commonFrameManagement(By.xpath("//span[contains(@id,'principalForm:insertedSuppliesTable')]"));
//		   WebElement x = util.commonFrameManagement(By.xpath("//*[@data-id='table']//table/tbody"));
		   WebElement x = util.commonFrameManagement(By.xpath("//tr[@class='slds-hint-parent']/td[5]/div/div"));
		   
		   String esito = x.getText();
	       if(!esito.contentEquals("OK")&& !esito.contentEquals("NA")) throw new Exception("L'esito del precheck risulta essere diverso da OK/NA. Esito: "+esito);
//	       driver.switchTo().defaultContent();
			
		}
	public void verificaEsitoKOPrecheck(String frame) throws Exception{
//		   WebElement x = util.commonFrameManagement(frame,By.xpath("//span[contains(@id,'principalForm:insertedSuppliesTable')]"));
		   WebElement x = util.commonFrameManagement(frame,By.xpath("//table[@role='table']/tbody/tr/td[5]/div/div"));
		   String esito = x.getText();
	       if(!(esito.contentEquals("KO"))) throw new Exception("L'esito del precheck risulta essere diverso da KO. Esito: "+esito);
	       driver.switchTo().defaultContent();
			
		}

	public void verificaEsitoKOPrecheck() throws Exception{
		   WebElement x = util.commonFrameManagement(By.xpath("//table[@role='table']/tbody/tr/td[5]/div/div"));
		   String esito = x.getText();
	       if(!(esito.contentEquals("KO"))) throw new Exception("L'esito del precheck risulta essere diverso da KO. Esito: "+esito);
	       driver.switchTo().defaultContent();
			
		}

	public void verificaEsitoOKPrecheck(String frame, By oggetto) throws Exception{
		   WebElement x = util.commonFrameManagement(frame,oggetto);
	       String esito = x.getText();
	       if(!esito.contentEquals("OK") && !esito.contentEquals("NON CENSITO") && !esito.contentEquals("NA")) throw new Exception("L'esito del precheck risulta essere diverso da OK/NA. Esito: "+esito);
	       driver.switchTo().defaultContent();
			
		}
	public void verificaEsitoOKPrecheck( By oggetto) throws Exception{
		   WebElement x = util.commonFrameManagement(oggetto);
	       String esito = x.getText();
	       if(!esito.contentEquals("OK") && !esito.contentEquals("NON CENSITO") && !esito.contentEquals("NA")) throw new Exception("L'esito del precheck risulta essere diverso da OK/NA. Esito: "+esito);
	       driver.switchTo().defaultContent();
			
		}

	public void verificaEsitoOKPrecheckMulti( String pod, String commodity) throws Exception{
		   String pathEsitoVerifica = this.precheckPODMulti.replaceAll("#COMMODITY#", commodity).replaceAll("#POD#", pod);
		   By esitoVerifica=By.xpath(pathEsitoVerifica);
		   WebElement x = util.commonFrameManagement(esitoVerifica);
	       String esito = x.getText();
//	       if(!esito.contentEquals("OK") && !esito.contentEquals("NON CENSITO")) throw new Exception("L'esito del precheck risulta essere diverso da OK. Esito: "+esito);
	       if(!esito.contentEquals("OK") && !esito.contentEquals("NON CENSITO") && !esito.contentEquals("NA")) throw new Exception("L'esito del precheck risulta essere diverso da: 'OK' - 'NON CENSITO' - 'NA'. Esito: "+esito);
	       driver.switchTo().defaultContent();
			
		}

	public void annullaPopup(String frame) throws Exception{
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		press(annullaPopup);
		driver.switchTo().defaultContent();
			
		}
	
	public void selezionaIstat(By tabellaIstat, By primoIstat, By buttonConfirm) throws Exception{
		if (util.exists(tabellaIstat, 30)){
			util.objectManager(primoIstat, util.scrollAndClick);
			util.objectManager(buttonConfirm, util.scrollAndClick);
			spinner.checkSpinners();
		}
	}
	
	public void selezionaIstatConComune(By tabellaIstat, By buttonConfirm, String citta) throws Exception{
		if (util.exists(tabellaIstat, 30)){
			util.objectManager(By.xpath("//table[@role='table']//tr/td[3]//div[text()='"+citta+"']/ancestor::tr/td//label/span[1]"), util.scrollAndClick);
			util.objectManager(buttonConfirm, util.scrollAndClick);
			spinner.checkSpinners();
		}
	}
	
	public void verificaPopUpconTesto(By obj, By objText,By button,String popupTestoAtteso) throws Exception{
		if(!util.verifyExistence(obj, 60)) throw new Exception("Non è stata visualizzata la pop up con testo: "+popupTestoAtteso);
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);

	}
	
	public void verificaPOD(String pod) throws Exception {
       WebElement POD = driver.findElement(By.xpath("//div[@role='table']//div[text()='"+pod+"']"));
       if(!POD.getText().equals(pod)) {
    	   throw new Exception("Il pod nella tabella non corrisponde al pod inserito. Pod visualizzato " + POD.getText() + " pod atteso: " + pod);
       }
	}

}
