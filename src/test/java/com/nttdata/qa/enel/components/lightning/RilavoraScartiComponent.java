package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RilavoraScartiComponent extends BaseComponent {
	WebDriver driver;
	SeleniumUtilities util;
	private SpinnerManager spinnerManager;

	public final By buttonRilavoraScarti = By.xpath("//button[text()='Rilavora Scarti']");
	public final By buttonVerifica = By.xpath("//button[text()='Verifica']");
	public final By inputCap = By.xpath("//label[contains(text(),'CAP')]/ancestor::div/input");
	public final By buttonForzaIndirizzo = By.xpath("//button[text()='Forza Indirizzo']");
	public final By switchIndirizzoForzato = By.xpath("//span[text()='Indirizzo Non forzato']/ancestor::span");
	public final By inputContrattoCliente = By.xpath("//label[text()='Contratto Cliente']/parent::div//input");
	public final By inputContoSAP = By.xpath("//label[text()='Conto contrattuale SAP']/parent::div//input");
	public final By inputCodiceBPSAP = By.xpath("//label[text()='Codice BP SAP']/parent::div//input");
	public final By clickForzaturaOK = By.xpath("//button[text()='Forzatura OK']");
	public final By clickRisottometti = By.xpath("//button[text()='Risottometti']");
	public final By suggerimentoCAP = By.xpath("//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='ROMA']");
	public final By forzaturaSi = By.xpath("//h2[text()='Conferma Forzatura OK']/ancestor::div[@class='slds-modal__container']//button[text()='Sì']");

	public RilavoraScartiComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}

	public void clickRilavoraScarti() throws Exception{
		util.objectManager(buttonRilavoraScarti, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void clickForzaturaOK() throws Exception{
		util.objectManager(clickForzaturaOK, util.scrollAndClick);
		spinnerManager.checkSpinners();
		//Click sulla pop-up Si
		util.objectManager(forzaturaSi, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}
	
	public void clickRisottometti() throws Exception{
		util.objectManager(clickRisottometti, util.scrollAndClick);
		spinnerManager.checkSpinners();
		//Click sulla pop-up Si
		util.objectManager(forzaturaSi, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void verificaIndirizzi_forzato() throws Exception{
		List<WebElement> list_of_verifica = driver.findElements(buttonVerifica);
		List<WebElement> list_of_cap = driver.findElements(inputCap);
		List<WebElement> list_of_forza_indirizzo= driver.findElements(buttonForzaIndirizzo);
		List<WebElement> list_of_switch_forza_indirizzo= driver.findElements(switchIndirizzoForzato);
		
		//Viene effettuato click sul body per prendere il focus della pagina
		spinnerManager.checkSpinners();
		
		//Vengono cliccati tutti i button verifica richiesta e forzati gli indirizzi
		for(int i=0;i<list_of_verifica.size();i++){
			//Click su Verifica
			util.objectManager(list_of_verifica.get(i), util.scrollAndClick);
			//Seleziona Switch
			util.objectManager(list_of_switch_forza_indirizzo.get(i), util.scrollAndClick);
			//Inserimento Cap
			util.scrollToElement(list_of_cap.get(i));
			list_of_cap.get(i).sendKeys("00198");
			//Selezione suggerimento
			List<WebElement> list_of_suggerimentoCAP= driver.findElements(suggerimentoCAP);
			util.objectManager(list_of_suggerimentoCAP.get(i), util.scrollAndClick);
			//Click su forza indirizzo
			util.objectManager(list_of_forza_indirizzo.get(i), util.scrollAndClick);
			Thread.sleep(60000);
			spinnerManager.checkSpinners();


		}
	}
	
	public void verificaIndirizzi() throws Exception{
		List<WebElement> list_of_verifica = driver.findElements(buttonVerifica);
		
		//Viene effettuato click sul body per prendere il focus della pagina
		spinnerManager.checkSpinners();
		
		//Vengono cliccati tutti i button verifica richiesta e forzati gli indirizzi
		for(int i=0;i<list_of_verifica.size();i++){
			//Click su Verifica
			util.objectManager(list_of_verifica.get(i), util.scrollAndClick);
			//Seleziona Switch
			spinnerManager.checkSpinners();


		}
	}

}
