package com.nttdata.qa.enel.components.colla;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class AccountRegistrationComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By homeFullscreenAlert = By.xpath("//div[@id='home-fullscreen-alert']");
	public By homeFullscreenAlertCloseButton = By.xpath("//div[@id='fsa-close-button']");
	public By logoEnel = By.xpath("//img[contains(@class,'logoimg')]");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By registratiButton = By.xpath("//a[text()='Registrati']");
	public By registrationHeader = By.xpath("//h1[@class='cmPageTitle cmTitle2']");
	//public By privatoDivButton = By.xpath("//div[@id='private']");
	public By privatoDivButton = By.xpath("//button[@id='private']");
	public By businessDivButton = By.xpath("//button[@id='business']");
	//public By continuaButton = By.xpath("//a[@id='continua-button-initial-modal']");
	public By continuaButton = By.xpath("//button[@id='continua-button-initial-modal']");
	//public By credentialsFormHeader = By.xpath("//div[@class='cmText2 cmMarginYlg']");
	public By credentialsFormHeader = By.xpath("//h2[@class='cmText2 cmMarginYlg']");
	public By emailInputField = By.xpath("//input[@id='email']");
	public By confirmEmailInputField = By.xpath("//input[@id='confirm-email']");
	public By passwordInputField = By.xpath("//input[@id='password']");
	public By confirmPasswordInputField = By.xpath("//input[@id='confirm-password']");
	public By nomeInputField = By.xpath("//input[@id='nome']");
	public By cognomeInputField = By.xpath("//input[@id='cognome']");
	public By cfInputField = By.xpath("//input[@id='cf']");
	public By cfAziendaleInputField = By.xpath("//input[@id='cfaziendale']");
	public By denominazioneInputField = By.xpath("//*[@id='denominazione']");
	public By informativaPrivacyCheckbox = By.xpath("//*[contains(text(),'Informativa')]/../preceding-sibling::label");
	public By condizioniGeneraliCheckbox = By.xpath("//*[contains(text(),'Condizioni')]/../preceding-sibling::label");
	public By registratiAllAreaRiservataButton = By.xpath("//a[@id='registration-form-button']");
	public By mobileNumberInputField = By.xpath("//input[@id='complete-profile-mobile-num']");
	public By continuaWithOTPButton = By.xpath("//button[text()='Continua']");
	public By otpHeader = By.xpath("//h1[contains(text(), 'sicurezza')]");
	public By otpInputField =By.xpath("//input[@id='complete-profile-otp']");
	public By confermaOTPButton = By.xpath("//button[@id='check-otp-step2-form-button']");
	public By thanksForRegistering = By.xpath("//h1[text()='Grazie per esserti registrato']");
	public By thanksForRegisteringButton = By.xpath("//a[@id='continua-button-thank-you']");
	public By profiloUnicoCreatoHeader = By.xpath("//h1[text()='Il tuo Profilo Unico è stato creato']");
	
	public By regHomePagePath1 =  By.xpath("//section[@id='general_hero']//a[.='Home']");
	public By regHomePagePath2 =  By.xpath("//section[@id='general_hero']//a[.='Registra Account']");
	public By cosEilProfiloUnicoHeaing = By.xpath("//h4[@id='modalRegistrationInfo-title']");
	public By calcolaOra = By.xpath("//a[@id='open-modalCF']");
	public By c_nome = By.xpath("//input[@id='nomeCF']");
	public By c_cognome = By.xpath("//input[@id='cognomeCF']");
	public By dataDiNascita = By.xpath("//form[@id='form-container']//label[.='Data di Nascita*']");
	public By giorno = By.xpath("//span[@id='daysSelectBoxIt']");
	public By mese = By.xpath("//span[@id='monthsSelectBoxItText']");
	public By anno = By.xpath("//span[@id='yearsSelectBoxItText']");
	public By c_sesso = By.xpath("//form[@id='form-container']//label[.='Sesso*']");
	public By femmina = By.xpath("//form[@id='form-container']//label[.='Femmina']");
	public By maschio = By.xpath("//form[@id='form-container']//label[.='Maschio']");
	public By si = By.xpath("//form[@id='form-container']//label[.='Si']");
	public By no = By.xpath("//form[@id='form-container']//label[.='No']");
	public By seiNato = By.xpath("//form[@id='form-container']//label[.='Sei nato in Italia?*']");
	public By calcolaButton = By.xpath("//button[@id='calcolaBtn']");
	public By nome_campoObb = By.xpath("//span[@id='nomeCF_errmsg']");
	public By cognome_campObb = By.xpath("//span[@id='cognomeCF_errmsg']");
	public By giorno_camObb = By.xpath("//span[@id = 'days_errmsg']");
	public By mese_camObb = By.xpath("//span[@id = 'months_errmsg']");
	public By ano_camObb = By.xpath("//span[@id = 'years_errmsg']");
	public By calcola_close = By.xpath("//div[@class='modal-header']/button[@class='remodal-close close-AEM_SF']");
	public By cf_errmsg = By.xpath("//div[@id='errore_fiscal-code']/span[@class='errorMsg']");
	public By cell_errmsg = By.xpath("");
	
	public By thanksForRegSecHeading = By.xpath("//main[@id ='main']//div[.='Scopri il mondo di servizi pensati per te']");
	public By emailMessage = By.xpath("//*[@id ='add-email-message']");
	public By emailMessage2 = By.xpath("//div[@id ='disclaimerTYP']/p[1]");
	
	
    public AccountRegistrationComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			System.out.println(url);
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
		if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is better to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		*/
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			//System.out.println(message);
			throw new Exception(message);
		}
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
			
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void fillInputField(By by, String s) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		we.clear();
		Thread.sleep(200);
		we.sendKeys(s);
	}
	
	public void enterInputtoField(By checkObject, String property) throws Exception {			
		util.objectManager(checkObject, util.sendKeys, property);
}
	
	public static final String REG_HOMEPAGE_PATH1 = "Home";
	public static final String REG_HOMEPAGE_PATH2 = "Registra Account";
	public static final String PROFILO_UNIOC_HEADING = "Cos'è il Profilo Unico?";
	public static final String CF_ERR_MSG ="Il codice fiscale non corrisponde al nominativo inserito";
	public static final String CELL_ERR_MSG = "";
	public static final String THANKS_FOR_REG_SEC_HEADING ="Scopri il mondo di servizi pensati per te";
	public static final String THANKS_FOR_REGISTERING ="Grazie per esserti registrato";
	public static final String EMAIL_MESSAGE = "Abbiamo inviato un messaggio all'indirizzo testingcrm.a.u.t.o.m.a.t.i.on@gmail.com";
	public static final String EMAIL_MESSAGE2 ="Controlla l’email e clicca sul link di conferma per attivare il tuo profilo unico e accedere a tutti i servizi digitali dell’Area Clienti e dell’App di Enel Energia. Se non hai ricevuto l'email ti invitiamo a controllare nella cartella spam.";
}
