package com.nttdata.qa.enel.components.lightning;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class FirmaDigitalComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public FirmaDigitalComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver,this);
		util=new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

//	public final By confermaButton = By.xpath("//h2[text()='Modalità firma digital']/ancestor::lightning-card//button[text()='Conferma']");
	public final By confermaButton = By.xpath("//h2[text()='Modalità firma digital']/ancestor::div[@role='region']//button[text()='Conferma']");
	

	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}


}

