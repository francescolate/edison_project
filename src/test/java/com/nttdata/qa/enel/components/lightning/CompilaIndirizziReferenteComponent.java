package com.nttdata.qa.enel.components.lightning;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CompilaIndirizziReferenteComponent {
	public  final By spanVerificaIndirizzo = By.id("spanFocusYServiceAddressChange");
	final public By buttonVerificaAddress = By.xpath("//div[@id='DivmodalModifyAddressABC']//button[text()='Verifica']");
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	//Logger //LOGGER = Logger.getLogger("");

	//	final By tabBillingAddress = new By.ByXPath("//div[@id='j_id0:formId:billingAddress']");
	//	final By tabResidentialAddress = new By.ByXPath("//div[@id='ResidentialAddressSection']");

	final public By verificaInd = new By.ByXPath("//button[@id='componentAddress4:verifyAddrBtn']");
	final public By verificaInd2 = new By.ByXPath("//button[@id='componentAddress2:verifyAddrBtn']");
	final public By indirizziEsistenti = new By.ByXPath("//a[text()='Verifica Indirizzi Esistenti']");
	final public By indirizziEsistentiTabBill = new By.ByXPath("//div[@id='j_id0:formId:billingAddress']//a[@role='tab' "
			+ "and text()='Verifica Indirizzi Esistenti']");

	final public By indirizziEsistentiTabResidenza = new By.ByXPath("//div[@id='ResidentialAddressSection']"
			+ "//a[@role='tab' and text()='Verifica Indirizzi Esistenti']");

	final public By checkBoxIndirizzoResidenza = new By.ByXPath("//div[@aria-labelledby='existingAddresses__item']"
			+ "//label[@class='slds-checkbox'][1]");

	//div[@aria-labelledby='existingAddresses__item']//label[@class='slds-checkbox'][1]

	final public By importaBtn = new By.ByXPath("//button[text()='Importa']");
	final public By verificaBtn =  new By.ByXPath("//button[text()='Verifica']");
	final public By verificaBtnBill = new By.ByXPath("//div[@aria-labelledby='newAddress__item']"
			+ "//button[text()='Verifica']");

	final public By verificaBtnRes = new By.ByXPath("//div[@id='ResidentialAddressSection']"
			+ "//button[text() = 'Verifica']");

	final public By verificaBtnAddresKit = new By.ByXPath("//div[@id='addressComponentSendKit']"
			+ "//button[text() = 'Verifica']");
	final public By verificaBtnSedeLegale=new By.ByXPath("//div[@id='addressComponentResidential']//button[text() = 'Verifica']");

	final public By confermaIndirizzoVerificatoBill = new By.ById("spanFocusYaddressComponentBilling");

	final public By spanIndirizzoConfermato =  spanVerificaIndirizzo;

	final public By confermaIndirizzoVerificatoRes = new By.ById("spanFocusYResidentialAddressSection");

	final public By confermaIndirizzoVerificatoKit = new By.ById("spanFocusYaddressComponentSendKit");
	final public By confermaIndirizzoVerificatoSedeLegale = new By.ById("spanFocusYaddressComponentResidential");
	final public By conferma = By.xpath("//button[text()='Conferma']");
	final public By avantiIndirizzoVerificatoBill = new By.ById("billingAddressNextBtn");

	final public By avantiIndirizzoVerificatoRes = new By.ById("checkResAddressButton");

	final public By avantiIndirizzoVerificatoKit = new By.ById("envAddNext");

	final public By avantiIndirizzoVerificatoKitVoltura = new By.ById("nextKitShipping_Button");
	final public By avantiIndirizzoVerificatoVoltura = new By.ById("nextResidence_Button");
	final public By avantiIndirizzoVerificatoVolturaAccollo = By.xpath("//button[text()='Avanti' and @id='confirmModeSignButton']");

	public By avantiButton = By.xpath("//div[text()='Indirizzo di Fatturazione']/parent::div//button[@alt='Avanti' and contains(@id,'next')]");
	public By avantiButton2 = By.xpath("//button[@id='buttonNext']");
	public By avantiButton3 = By.xpath("//button[contains(@id,'next')]");


	final public By selezioneFornisceIndirizzoSINO = new By.ById("j_id0:formId:providedAddressId");
	final public By selezioneClienteFornisceIndirizzo = new By.ByXPath("//span[contains(text(),'Cliente fornisce') and contains(text(),'indirizzo')]/ancestor::div[1]//select");
	final public By selezioneDichiarazioneResidenza = new By.ByXPath("//label[text()='Cliente Dichiara Residenza']/parent::span//select");

	final public By regioneDomicilio = new By.ById("componentAddress1:regionInput");
	final public By capDomicilio = new By.ById("componentAddress1:zipCodeInput");
	final public By regioneDomicilio2 = new By.ById("componentAddress2:regionInput");
	final public By capResidenza = new By.ById("componentAddress2:zipCodeInput");

	final public By street = new By.ById("precheckDetailAddressComponent:streetNameInput");
	final public By province = new By.ById("precheckDetailAddressComponent:provinceInput");
	final public By civico = new By.ById("precheckDetailAddressComponent:streetNumberInput");
	final public By city = new By.ById("precheckDetailAddressComponent:cityInput");
	final public By street2 = new By.ByXPath("//input[contains(@id,'streetNameInput') and @type='text']");
	final public By province2 = new By.ByXPath("//input[contains(@id,'provinceInput') and @type='text']");
	final public By civico2 = new By.ByXPath("//input[contains(@id,'streetNumberInput') and @type='text']");
	final public By city2 = new By.ByXPath("//input[contains(@id,'cityInput') and @type='text']");
	final public By addressCheckBtn = new By.ById("precheckDetailAddressComponent:verifyAddrBtn");

	private final String provinciaXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'provinceInput') and @type='text']";
	private final String comuneXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'cityInput') and @type='text']";
	private final String indirizzoXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'streetNameInput') and @type='text']";
	private final String civicoXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'streetNumberInput') and @type='text']";
	private final String telefonoClienteXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//input[contains(@id,'telefonoCliente') and @type='text']";

	private final String verificaIndirizzoButtonXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//button[contains(@id,'verifyAddrBtn')][1]";
	private final String verificaIndirizzoButtonXPath2 = "//button[contains(@id,'componentAddress2:verifyAddrBtn')]";
	private final String indirizzoVerificato1XPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//fieldset/label[text()='Domicilio']/..//div[contains(@id,'verifiedAddrTick')]";

	private final String indirizzoVerificato2XPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//fieldset/label[text()='Residenza']/..//div[contains(@id,'verifiedAddrTick')]";
	private final String indirizzoVerificato3XPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//fieldset/label/..//div[contains(@id,'verifiedAddrTick')]";

	private final String indirizzoVerificatoSedeLegaleXpath = "//h2[text()='Sede Legale e Contatti']/ancestor::div[@class='slds-card']//div[contains(@id,'verifiedAddrTick')]";
	//private final By indirizzoVerificato2 = By.id("componentAddress2:verifiedAddrTick");
	private final By copiaIndirizzo = By.xpath("//input[@id='CopiaIndirizzo']/following-sibling::*");
	public final By copiaDaIndirizzoDiResidenzaLink = By.xpath("//a[@class='acceleretorLinkResidentialAddress' and contains(text(),'residenza')]");
	public final By copiaDaIndirizzoDiResidenzaLink2 = By.xpath("//a[@class='acceleretorLinkBillingAddress' and contains(text(),'residenza')]");
	public final By copiaDaIndirizzoDiFornituraLink = By.xpath("//a[contains(text(),'indirizzo di fornitura')]");
	public final By copiaDaIndirizzoSedeLegale = By.xpath("//a[contains(text(),'sede legale')]");
	public By inputPresso=By.xpath("//label[contains(text(),'Presso')]/../input");
//	public By inputProvincia=By.xpath("//label[contains(text(),'Provincia')]/../input");
	public By inputProvincia=By.xpath("//*[@id='componentAddress4:provinceInput']");
	
	public By inputComune=By.xpath("//label[contains(text(),'Comune')]/../input");
	public By inputLocalita=By.xpath("//label[contains(text(),'Localit')]/../input");
	public By inputIndirizzo=By.xpath("//label[contains(text(),'Indirizzo')]/../input");
	public By inputCivico=By.xpath("//label[contains(text(),'Numero Civico')]/../div/input[@name='civic']");
	public By inputCivico2=By.xpath("//span[contains(text(),'Numero Civico')]/ancestor::div[1]/input");
	public By inputScala=By.xpath("//label[contains(text(),'Scala')]/../div/input[@name='stairNr']");
	public By inputPiano=By.xpath("//label[contains(text(),'Piano')]/../div/input[@name='floorNr']");
	public By inputInterno=By.xpath("//label[contains(text(),'Interno')]/../div/input[@name='apartmentNr']");
	public By buttonVerifica=By.xpath("//button[contains(text(),'Verifica')]");
	public By buttonVerificaVolturaAccollo=By.xpath("//div[@id='focusOnThisaddressComponentSendKit']/..//button[contains(text(),'Verifica')]");
	public By spanEsitoVerificaIndirizzo=By.xpath("//span[@id='spanFocusYaddressDefaultChannel']");

	public By simulaAvantiButton = By.xpath("//button[@alt='Avanti' and contains(@onclick,'simulaclick')]");
	public By avantiComponentlightningButton = By.xpath("//button[text()='Avanti' and contains(@onclick,'clickButton')]");

	public By inputResidentialProvince = By.xpath("//input[@id='residentialAddress:provinceInput']");
	public By inputResidentialCity = By.id("residentialAddress:cityInput");
	public By inputResidentialStreet = By.id("residentialAddress:streetNameInput");
	public By inputResidentialStreetNumber = By.id("residentialAddress:streetNumberInput");

	public By TabverificaIndirizziEsistenti = By.xpath("//li[@title='Verifica Indirizzi Esistenti']/a");

	public By primoIndirizzoEsistente = By.xpath("//div[@aria-labelledby='existingAddresses__item']"
			+ "//label[@class='slds-checkbox'][1]");

	public By btnImporta = By.xpath("//button[text()='Importa']");

	public By btnConfermaCoordinateGeografiche=By.xpath("//span[text()='Coordinate geografiche con TP']/ancestor::div[2]//button");

	public By spanIndirizzoVerificato = By.xpath("//span[text()='Indirizzo verificato']");

	public final By buttonVerificaIndirizzo = By.xpath("//button[text()='Verifica Indirizzo']");

	public final By tabellaIstat = By.xpath("//table[@id='districtDisambiguation_Table']");
	public final By primoIstat=By.xpath("//table[@id='districtDisambiguation_Table']/tbody/tr[1]/td[1]//div/label[@class='slds-radio']");
	public final By confermaIstat=By.xpath("//table[@id='districtDisambiguation_Table']/ancestor::div[contains(@id,'modalBuilderSelezione Localit')]//button[text()='OK']");

	private final By provinciaInput = By.xpath("//label[@id='addressComponentBillingProvincia']/following-sibling::input");
	private final By provinciaDropDownMenu = By.xpath("//span[contains(text(), 'SALERNO')]/../../../../..");
	private final By comuneInput = By.xpath("//label[@id='addressComponentBillingComune']/following-sibling::input");
	private final By viaInput = By.xpath("//label[@id='addressComponentBillingIndirizzo']/following-sibling::input");
	private final By viaDropDownMenu = By.xpath("//span[contains(text(), 'VIA QUASIMODO')]/..");
	private final By civicoInput = By.xpath("//label[contains(text(), 'Numero Civico')]/following-sibling::div/input");
	private final By verifica = By.xpath("//div[@id='focusOnThisaddressComponentBilling']/following-sibling::div/div/button[contains(text(), 'Verifica')]");
	private final By avanti = By.xpath("//button[@id='nextBillingAddress_Button']");

	public CompilaIndirizziReferenteComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	private boolean checkAlertPresence(WebElement alertBar) throws Exception{
		boolean exist = false;
		String style = alertBar.getAttribute("style");
		if(style.contentEquals("display: none;")) exist = false;
		else exist = true;
		return exist;
	}

	private boolean checkAlertMessage(String message) throws Exception {
		WebElement errors = util.waitAndGetElement(By.xpath("//span[@id='RedAlertsErrors']"));
		if(errors.getText().contains(message)) return true;
		else return false;
	}



	public void forzaindirizzoResidenza(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress2";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath2.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		//class <> slds-hide
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);

		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, il sistema non mostra alcun messaggio di errore");
		else {

			if(!checkAlertMessage("Indirizzo non riconosciuto")) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, al verifica indirizzo non viene visualizzato il messaggio di indirizzo non riconosciuto.");
			By closeBar = By.xpath("//div[@id='RedAlerts']//button");
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capResidenza, util.scrollAndSendKeys, "80014");
			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}


		}

		this.driver.switchTo().defaultContent();

	}


	public void forzaindirizzoDomicilio(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		//class <> slds-hide
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);

		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, il sistema non mostra alcun messaggio di errore");
		else {

			if(!checkAlertMessage("Indirizzo non riconosciuto")) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, al verifica indirizzo non viene visualizzato il messaggio di indirizzo non riconosciuto.");
			By closeBar = By.xpath("//div[@id='RedAlerts']//button");
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capDomicilio, util.scrollAndSendKeys, "80014");
			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}


		}

		this.driver.switchTo().defaultContent();

	}


	public void verificaObbligatorietaIndirizzoDomicilio(String frameName) throws Exception {
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.FrameSwitcher(By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		if(util.waitAndGetElement(regioneDomicilio,true).isEnabled()) throw new Exception ("Il campo Regione nell' indirizzo domicilio non risulta disabilitato");
		if(util.waitAndGetElement(capDomicilio,true).isEnabled()) throw new Exception ("Il campo CAP nell' indirizzo domicilio non risulta disabilitato");
		util.objectManager(verificaInd, util.scrollAndClick);
// *** ATTENZIONE IF sotto commentata perchè non vengono segnalati i campi indirizzo obbligatori
//		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo indirizzo domicilio risulta obbligatorio, impossibile trovare la Red Alert");
//		else {

//			if(!checkAlertMessage("Il campo Provincia è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Provincia risulta non essere obbligatorio");
//			if(!checkAlertMessage("Il campo Comune è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Comune risulta non essere obbligatorio");
//			if(!checkAlertMessage("Il campo Indirizzo è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Indirizzo risulta non essere obbligatorio");
//			if(!checkAlertMessage("Il campo Numero Civico è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Numero Civico risulta non essere obbligatorio");

//			util.objectManager(closeBar, util.scrollAndClick);

		util.objectManager(By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"ROMA");
			util.objectManager(By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"ROMA");
			util.objectManager(By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NIZZA");
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"5");
			util.objectManager(verificaInd, util.scrollAndClick);
			if(util.exists(verificaInd, 5)) util.objectManager(verificaInd, util.scrollAndClick);

			WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));

			if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");

			WebElement capElement = util.waitAndGetElement(capDomicilio,true);
			if(!capElement.getAttribute("value").equals("80144")) throw new Exception("Nella sezione indirizzo del domicilio, dopo la verifica indirizzo non viene popolato in automatico il campo CAP");

			driver.switchTo().defaultContent();

// *** ATTENZIONE riga sotto "}"commentata perchè non vengono segnalati i campi indirizzo obbligatori

//		}


	}



	public void verificaObbligatorietaIndirizzoResidenza(String frameName) throws Exception {
		String section = "Indirizzi";
		String domresid = "componentAddress2";
		util.FrameSwitcher(By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		if(util.waitAndGetElement(regioneDomicilio2,true).isEnabled()) throw new Exception ("Il campo Regione nell' indirizzo residenza non risulta disabilitato");
		if(util.waitAndGetElement(capResidenza,true).isEnabled()) throw new Exception ("Il campo CAP nell' indirizzo residenza non risulta disabilitato");
		util.objectManager(verificaInd2, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo indirizzo residenza risulta obbligatorio, impossibile trovare la Red Alert");
		else {

			if(!checkAlertMessage("Il campo Provincia è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Provincia risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Comune è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Comune risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Indirizzo è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Indirizzo risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Numero Civico è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Numero Civico risulta non essere obbligatorio");

			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NAPOLI");
			util.objectManager(By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NAPOLI");
			util.objectManager(By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"VIA ROMA NAPOLI");
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"5");
			util.objectManager(verificaInd2, util.scrollAndClick);
			if(util.exists(verificaInd2, 5)) util.objectManager(verificaInd2, util.scrollAndClick);

			WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato2XPath.replaceAll("#", section)));

			if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo residenza del nuovo cliente non e' verificato. Inserire un indirizzo corretto");

			WebElement capElement = util.waitAndGetElement(capResidenza,true);
			if(!capElement.getAttribute("value").equals("80144")) throw new Exception("Nella sezione indirizzo di residenza, dopo la verifica indirizzo non viene popolato in automatico il campo CAP");

			driver.switchTo().defaultContent();

		}


	}

	public void selezionaSceltaIndirizzoCliente(String frame, String scelta) throws Exception {
		if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.frameManager(frame, selezioneFornisceIndirizzoSINO,util.select,scelta);
		util.frameManager(frame, avantiIndirizzoVerificatoRes,util.scrollToVisibility,true);
		util.frameManager(frame, avantiIndirizzoVerificatoRes,util.click);
	}

	public void selezionaSceltaIndirizzoCliente(String frame, By selectDichiaraResidenza,String scelta,By avantiIndirizzo) throws Exception {
		if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.frameManager(frame, selectDichiaraResidenza,util.select,scelta);
		util.frameManager(frame, avantiIndirizzo,util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame, spinner.loadingSpinner);

	}

	public void selezionaSceltaIndirizzoCliente(By selectDichiaraResidenza,String scelta) throws Exception {
		if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.objectManager(selectDichiaraResidenza, util.scrollAndSelect, scelta);

	}

	public void selezionaSceltaIndirizzoClientePerValore(By selectDichiaraResidenza,String value) throws Exception {
		//if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.objectManager(selectDichiaraResidenza, util.scrollAndSelectByValue, value);

	}

	public void importaIndirizzoEsistente() throws Exception{

		util.objectManager(indirizziEsistenti,util.scrollAndClick);

		util.objectManager(checkBoxIndirizzoResidenza,util.scrollAndClick);

		util.objectManager(importaBtn, util.scrollAndClick);

		spinner.checkSpinners();
		util.objectManager(verificaBtn, util.scrollAndClick);

		util.scrollToBottom();
		spinner.checkSpinners();
		util.objectManager(conferma,util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void fillCustomerData(Properties prop, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		//this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.provinciaInput, util.scrollAndSendKeys,prop.getProperty("PROVINCIA"));
		util.objectManager(this.comuneInput, util.scrollAndSendKeys,prop.getProperty("COMUNE"));
		util.objectManager(this.viaInput, util.scrollAndSendKeys,prop.getProperty("VIA"));
		util.objectManager(this.viaDropDownMenu, util.click);
		util.objectManager(this.civicoInput, util.scrollAndSendKeys,prop.getProperty("NUMERO_CIVICO"));
		util.objectManager(this.verifica, util.click);
		TimeUnit.SECONDS.sleep(5);
		spinner.checkSpinners();
		util.objectManager(this.avanti, util.click);
		spinner.checkSpinners();
	}

	public void fillComune(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.comuneInput, util.scrollAndSendKeys,s);
	}

	public void fillProvincia(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.provinciaInput, util.scrollAndSendKeys,s);
	}

	public void fillIndirizzo(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.viaInput, util.scrollAndSendKeys,s);
	}

	public void fillCivico(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.civicoInput, util.scrollAndSendKeys,s);
	}

	public void selectVia() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.viaDropDownMenu, util.click);
	}

	public void verificaClick() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.verifica, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void avantiClick() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.avanti, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void fillComune(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		//this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.comuneInput, util.scrollAndSendKeys,s);
	}

	public void fillProvincia(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.provinciaInput, util.scrollAndSendKeys,s);
	}

	public void fillIndirizzo(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.viaInput, util.scrollAndSendKeys,s);
	}

	public void fillCivico(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.civicoInput, util.scrollAndSendKeys,s);
	}

	public void selectVia(String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.viaDropDownMenu, util.click);
	}

	public void verificaClick(String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.verifica, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void avantiClick(String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.avanti, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void importaIndirizzoEsistenteBilling(String frame) throws Exception{

		util.frameManager(frame, indirizziEsistentiTabBill,util.scrollAndClick);

		util.frameManager(frame, checkBoxIndirizzoResidenza,util.scrollAndClick);

		util.frameManager(frame, importaBtn, util.scrollAndClick);

		spinner.waitForSpinnerByStyleDiplayNone(frame);

		util.frameManager(frame, verificaBtnBill, util.scrollAndClick);

		util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoBill,120);
		util.frameManager(frame, avantiIndirizzoVerificatoBill,util.scrollAndClick);
		spinner.checkSpinners(frame);
	}

	public void validaIndirizzo(String frame,By verificaBtn,By spanIndirizzoverificato,By buttonAvanti) throws Exception{

		util.frameManager(frame, verificaBtn, util.scrollAndClick);
		spinner.checkSpinners(frame);
		util.verifyExistenceInFrame(frame,spanIndirizzoverificato,120);
		util.frameManager(frame, buttonAvanti,util.scrollAndClick);
		spinner.checkSpinners(frame);

	}

	public void importaIndirizzoEsistenteResidenziale(String frame) throws Exception{

		util.frameManager(frame, verificaBtnRes, util.scrollAndClick);
		util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoRes,120);
		util.scrollDownDocument();
		util.frameManager(frame, avantiIndirizzoVerificatoRes,util.scrollAndClick);
		spinner.checkSpinners();

	}

	public void copiaDaIndirizzo(String frame,By link) throws Exception{
		util.frameManager(frame, link, util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}


	public void importaIndirizzoEsistenteKit(String frame) throws Exception{

		util.frameManager(frame, verificaBtnAddresKit, util.scrollAndClick);
		util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoKit,120);
		util.frameManager(frame, avantiIndirizzoVerificatoKit,util.scrollAndClick);

	}

	public void importaIndirizzoVolturaKit(String frame) throws Exception{

		if(util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoKit,120)){
			util.frameManager(frame, avantiIndirizzoVerificatoKitVoltura,util.scrollAndClick);
		}
		else{
			util.frameManager(frame, verificaBtnAddresKit, util.scrollAndClick);
			util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoKit,120);
			util.frameManager(frame, avantiIndirizzoVerificatoKitVoltura,util.scrollAndClick);
		}

	}

	public void compileAddressIfAvailable(String frame,
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy,
			By addressCheckBtnBy) throws Exception
	{
		if(util.exists(frame, addressCheckBtnBy, 5)) 
		{
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			WebElement el = driver.findElement(addressCheckBtnBy);
			if(!el.getAttribute("class").contains("slds-hide")) 
			{
				compileAddressInfo(provincia,provinciaBy,city,cityBy,address,addressBy,civico,civicoBy,addressCheckBtnBy) ;
			}
		}		
		driver.switchTo().defaultContent();
		spinner.checkSpinners(frame);

	}

	public void clearAddress(String frame) throws Exception
	{

		util.frameManager(frame, inputProvincia, util.scrollAndClick);
		util.frameManager(frame, inputProvincia, util.clear);
		util.frameManager(frame, inputProvincia, util.sendKeys, "");
		util.frameManager(frame, inputComune, util.scrollAndClick);
		util.frameManager(frame, inputComune, util.clear);		
		util.frameManager(frame, inputComune, util.sendKeys, "");		
		util.frameManager(frame, inputLocalita, util.scrollAndClick);
		util.frameManager(frame, inputLocalita, util.clear);
		util.frameManager(frame, inputLocalita, util.sendKeys, "");
		util.frameManager(frame, inputIndirizzo, util.scrollAndClick);
		util.frameManager(frame, inputIndirizzo, util.clear);
		util.frameManager(frame, inputIndirizzo, util.sendKeys, "");
		util.frameManager(frame, inputCivico, util.scrollAndClick);
		util.frameManager(frame, inputCivico, util.clear);
		util.frameManager(frame, inputCivico, util.sendKeys, "");	
		util.frameManager(frame, inputScala, util.scrollAndClick);
		util.frameManager(frame, inputScala, util.clear);
		util.frameManager(frame, inputScala, util.sendKeys, "");
		util.frameManager(frame, inputPiano, util.scrollAndClick);
		util.frameManager(frame, inputPiano, util.clear);
		util.frameManager(frame, inputPiano, util.sendKeys, "");
		util.frameManager(frame, inputInterno, util.scrollAndClick);
		util.frameManager(frame, inputInterno, util.clear);
		util.frameManager(frame, inputInterno, util.sendKeys, "");
	}

	/**
	 * Sbianca tutti i campi dell'indirizzo e ricompila
	 * Utilizza gli oggetti By della classe
	 * @param frame
	 * @param provincia
	 * @param city
	 * @param localita
	 * @param address
	 * @param civico
	 * @param scala
	 * @param piano
	 * @param interno
	 * @param addressCheckBtnBy
	 * @throws Exception
	 */
	public void compileAddressInfo(String frame,
			String provincia,
			String city,
			String localita,
			String address,
			String civico,
			String scala,
			String piano,
			String interno,
			By buttonVerificaInput) throws Exception
	{	
		//Compila anche il campo Località
		//Campo localita
		clearAddress(frame);

		//inserimento campi
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//campo provincia
		util.frameManager(frame, inputProvincia,util.sendKeysCharByChar, provincia);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Comune
		util.frameManager(frame, inputComune,util.sendKeysCharByChar,city);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Località
		util.frameManager(frame, inputLocalita,util.sendKeysCharByChar,localita);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Indirizzo
		util.frameManager(frame, inputIndirizzo,util.sendKeysCharByChar,address);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Civico
		util.frameManager(frame, inputCivico,util.sendKeysCharByChar,civico);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//Inserisco Scala
		util.frameManager(frame, inputScala,util.sendKeysCharByChar, scala);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//Inserisco Piano
		util.frameManager(frame, inputPiano,util.sendKeysCharByChar,piano);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//Inserisco Interno
		util.frameManager(frame, inputInterno,util.sendKeysCharByChar,interno);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);

		util.frameManager(frame, buttonVerificaInput, util.scrollAndClick);

		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void compileAddressInfo(String frame,
			String provincia,
			String city,
			String address,
			String civico,
			By buttonVerificaInput) throws Exception
	{	
		//Compila anche il campo Località
		//Campo localita
		clearAddress(frame);

		//inserimento campi
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//campo provincia
		util.frameManager(frame, inputProvincia,util.sendKeysCharByChar, provincia);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Comune
		util.frameManager(frame, inputComune,util.sendKeysCharByChar,city);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Indirizzo
		util.frameManager(frame, inputIndirizzo,util.sendKeysCharByChar,address);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Civico
		util.frameManager(frame, inputCivico,util.sendKeysCharByChar,civico);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);

		util.frameManager(frame, buttonVerificaInput, util.scrollAndClick);

		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void compileAddressInfo(String frame,
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy,
			By addressCheckBtnBy) throws Exception{


		//LOGGER.log(Level.INFO ,"Entrato in compileAddressInfo");
		compileAddress(frame, provincia, provinciaBy, city, cityBy, address, addressBy, civico, civicoBy);

		util.frameManager(frame, addressCheckBtnBy, util.scrollAndClick);
		//		//System.out.println("cliccato address check");

		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void compileAddressInfo(String provincia,By provinciaBy,String city,By cityBy,String address,By addressBy,String civico,By civicoBy,By addressCheckBtnBy) throws Exception{
		compileAddress(provincia, provinciaBy, city, cityBy, address, addressBy, civico, civicoBy);
		util.objectManager(addressCheckBtnBy, util.scrollAndClick);
		//		//System.out.println("cliccato address check");
		spinner.checkSpinners();
	}


	public void compileAddress(String frame,
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy) throws Exception{


		//LOGGER.log(Level.INFO ,"Entrato in compileAddress ");
		util.frameManager(frame, provinciaBy, util.sendKeys,provincia);
		//LOGGER.log(Level.INFO ,"Inserita provincia");
		//		TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, civicoBy, util.scrollAndClick);
		//LOGGER.log(Level.INFO ,"Click Civico");
		//		TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, cityBy, util.sendKeys,city);
		//		TimeUnit.SECONDS.sleep(1);
		util.frameManager(frame, provinciaBy, util.scrollAndClick);
		//		TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, addressBy, util.sendKeys,address);	
		//		TimeUnit.SECONDS.sleep(1);
		util.frameManager(frame, provinciaBy, util.scrollAndClick);
		//TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, civicoBy, util.sendKeys,civico);
		//		TimeUnit.SECONDS.sleep(1);
		util.frameManager(frame, provinciaBy, util.scrollAndClick);
		//TimeUnit.SECONDS.sleep(2);

	}

	public void compileAddress(String provincia,By provinciaBy,String city,By cityBy,String address,By addressBy,String civico,By civicoBy) throws Exception{

		util.objectManager(provinciaBy,util.scrollAndSendKeys, provincia);
		//		driver.findElement(By.xpath("//body")).click();
		util.objectManager(civicoBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		//		driver.findElement(cityBy).sendKeys(city);
		util.objectManager(cityBy,util.scrollAndSendKeys, city);
		//		driver.findElement(By.xpath("//body")).click();
		util.objectManager(provinciaBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		//		driver.findElement(addressBy).sendKeys(address);
		util.objectManager(addressBy,util.scrollAndSendKeys, address);
		//		driver.findElement(By.xpath("//body")).click();
		util.objectManager(provinciaBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		//		driver.findElement(civicoBy).sendKeys(civico);
		util.objectManager(civicoBy,util.scrollAndSendKeys, civico);
		//		driver.findElement(By.xpath("//body")).click();
		util.objectManager(provinciaBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);

	}


	//Questo modulo sbianca tutti i campi dell'indirizzo pre-caricato e ne inserisce uno nuovo
	public void reinserisciIndirizzo(String frame,By presso,By provincia, By localita,By comune, By indirizzo, By civico,By interno, By piano, By scala,String vpresso,String vprovincia, String vlocalita,String vcomune,String vindirizzo,String vcivico,String vinterno,String vpiano,String vscala) throws Exception{
		//sbiancamento campi
		util.frameManager(frame, scala,util.clear);
		util.frameManager(frame, piano,util.clear);
		util.frameManager(frame, interno,util.clear);
		util.frameManager(frame, civico,util.clear);
		util.frameManager(frame, civico,util.clear);//occorre riperetere 2 volte il clear per sbiancare campo
		util.frameManager(frame, indirizzo,util.clear);
		util.frameManager(frame, comune,util.clear);
		util.frameManager(frame, localita,util.clear);
		util.frameManager(frame, provincia,util.clear);
		util.frameManager(frame, presso,util.clear);

		//inserimento campi
		//campo presso
		util.frameManager(frame, presso,util.sendKeysCharByChar,vpresso);
		TimeUnit.SECONDS.sleep(5);
		//campo provincia
		util.frameManager(frame, provincia,util.sendKeysCharByChar,vprovincia);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Comune
		util.frameManager(frame, comune,util.sendKeysCharByChar,vcomune);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Località
		util.frameManager(frame, localita,util.sendKeysCharByChar,vlocalita);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Indirizzo
		util.frameManager(frame, indirizzo,util.sendKeysCharByChar,vindirizzo);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Civico
		util.frameManager(frame, civico,util.sendKeysCharByChar,vcivico);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//Inserisco Scala
		util.frameManager(frame, scala,util.sendKeysCharByChar,vscala);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//Inserisco Piano
		util.frameManager(frame, piano,util.sendKeysCharByChar,vpiano);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//Inserisco Interno
		util.frameManager(frame, interno,util.sendKeysCharByChar,vinterno);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
	}

	public void compilaIndirizzi(String section,String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato3XPath.replaceAll("#", section)));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo nella sezione "+section+" non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}

	public void compilaIndirizziResidenziale(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.findElement(copiaIndirizzo).click();
		el = util.waitAndGetElement(By.xpath(indirizzoVerificato2XPath.replaceAll("#", section)));
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo di Residenza nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}
	public void compilaDomicilio(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.objectManager(inputProvincia,util.sendKeys,provincia);
		util.frameManager(frameName, inputProvincia,util.sendKeys,provincia);
		util.frameManager(frameName, inputComune,util.sendKeys,comune);
		util.frameManager(frameName, inputIndirizzo,util.sendKeys,indirizzo);
		util.frameManager(frameName, inputCivico,util.sendKeys,civico);
//		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}


	public void compilaIndirizziResidenzialeDomicilio(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}



	public void compilaSedeLegale(String frameName,String provincia, String comune, String indirizzo, String civico, String telefono) throws Exception{
		String domresid = "componentAddress1";
		String section = "Sede Legale e Contatti";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(telefonoClienteXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,telefono);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section).replaceAll("$", domresid)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificatoSedeLegaleXpath));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo Sede Legale nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}

	public void verificaIndirizzo(String frame,By verifica,By esitoVerificaIndirizzo) throws Exception{
		//Click su verifica e aspetta fin quando non compare l'esito della verifica
		util.frameManager(frame, verifica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement mess=null;
		mess=util.waitAndGetElement(esitoVerificaIndirizzo, 100, 2);
		if (mess==null){
			throw new Exception("L'indirizzo inserito risulta essere non validato");
		}
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}

	public void verificaIndirizzo(By buttonVerifica,By esitoVerificaIndirizzo)throws Exception{
		if(!util.exists(esitoVerificaIndirizzo, 10)){
			util.objectManager(buttonVerifica, util.scrollAndClick);
		}
	}

	public void avanti(String frame,By avantiButton) throws Exception{
		util.frameManager(frame, avantiButton, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}

	public void avanti(By avantiButton) throws Exception{
		util.objectManager(avantiButton, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void verificaPopUpCoordinateGeografiche(By buttonConfirmPopUp) throws Exception{
		TimeUnit.SECONDS.sleep(5);

		if(util.exists(buttonConfirmPopUp, 30)&& driver.findElement(buttonConfirmPopUp).isDisplayed()){
			util.objectManager(buttonConfirmPopUp, util.scrollAndClick);
			spinner.checkSpinners();
			// Avanti su Punto su Mappa
			avanti(avantiButton2);
		}
		//		else{
		//			throw new Exception("Pop up di conferma Coordinate Geografiche non visualizzata");}

	}
	public void selezionaIstat(By tabellaIstat, By primoIstat, By buttonConfirm) throws Exception{
		if (util.exists(tabellaIstat, 15)){
			util.objectManager(primoIstat, util.scrollAndClick);
			util.objectManager(buttonConfirm, util.scrollAndClick);
			spinner.checkSpinners();
		}
	}

	public void compilaNuovoIndirizzoDiFornitura() throws Exception {

		compilaNuovoIndirizzoDiFornitura(spanVerificaIndirizzo);
	}

	public void compilaNuovoIndirizzoDiFornitura(By verificaBy) throws Exception {

		util.scrollAndClick.accept(
				util.waitAndGetElement(TabverificaIndirizziEsistenti));

		util.scrollAndClick.accept(
				util.waitAndGetElement( primoIndirizzoEsistente));


		util.scrollAndClick.accept(
				util.waitAndGetElement(btnImporta
						));

		util.waitAndGetElement(
				buttonVerificaAddress)
		.click();

		util.waitAndGetElement(
				verificaBy);
	}


	//	public void inserisciIndirizzoECivicoSeModificabile(String frame,By provinciaBy, String provincia, By comuneBy, String comune, By indirizzoBy, String indirizzo, By civicoBy, String civico) throws Exception{
	//		if(util.exists(frame, buttonVerificaIndirizzo, 5)) {
	//    		driver.switchTo().frame(frame);
	//    		WebElement el = driver.findElement(buttonVerificaIndirizzo);
	//    		if(!el.getAttribute("class").contains("slds-hide")) {
	//    			util.objectManager(provinciaBy, util.sendKeysAndEnter, provincia);
	//    			util.objectManager(comuneBy, util.sendKeysAndEnter, comune);
	//    			util.objectManager(indirizzoBy, util.sendKeysAndEnter, indirizzo);
	//    			util.objectManager(civicoBy, util.sendKeysAndEnter, civico);
	//    			spinner.checkSpinners();
	//    			el.click();
	//    		}
	//    		driver.switchTo().defaultContent();
	//    	    spinner.checkSpinners(frame);
	//    	}
	//	
	//
	//	}

	/**
	 * Verifica esistenza oggetto con descrizione "Indirizzo verificato", 
	 * se presente l'indirizzo è già stato inserito/verificato 
	 * @param frame
	 * @return true --> Indirizzo da Inserire - false --> Indirizzo già verificato
	 * @throws Exception
	 */
	public boolean verificaInserimentoIndirizzo(String frame) throws Exception
	{
		//Oggetto descrizione  "Indirizzo verificato"
		//Presente a video solo se l'indirizzo è già stato verificato
		By objectIndirizzoVerificato = By.xpath("//span[@id='spanFocusYaddressComponentResidential']");
		boolean returnValue = false;

		//		driver.switchTo().frame(frame);

		//	List<WebElement> el = driver.findElements(objectIndirizzoVerificato);
		//	if (el.size() == 1)
		if	(!util.exists(frame, objectIndirizzoVerificato, 5))
		{
			returnValue = true;
		}

		//driver.switchTo().defaultContent();

		return returnValue;

	}

	public By getButtonVerifica() {
		return buttonVerifica;
	}

	public void setButtonVerifica(By buttonVerifica) {
		this.buttonVerifica = buttonVerifica;
	}

}
