package com.nttdata.qa.enel.components.lightning;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ConfermaFornituraComponent extends BaseComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	public final By confermaFornituraButton = By.xpath("//button[text()='Conferma Fornitura']");
	public final By confirmButton = By.xpath("//lightning-button[@data-id='confirmButton']");
	 public By buttonChiudi = By.xpath("//button[contains(text(),'Chiudi')]");
	public By inputCategoria=By.xpath("//label[text()='Categoria Merceologica SAP']/ancestor::div[1]//input");
	public By inputTitolarita = By.xpath("//label[text()='Titolarita’']/ancestor::div[1]//input");
	public By selezionaPrimaFornitura = By.xpath("//*[text()='Commodity']/ancestor::div[@role='region']//input[@type='radio']/..");
	public By selezionaReferente = By.xpath("//*[text()='Referente']/ancestor::div[@role='region']//input[@type='radio']/..//label//span");
	public By selezionaPrimaFornituraRes = By.xpath("//*[text()='Commodity']/ancestor::div[@role='region']//input[@type='radio']/..//label//span");
	public By selezionaSecondaFornitura = By.xpath("//*[text()='Commodity']/ancestor::div[@role='region']//input[@aria-posinset='2']//..//label");
	public By button_conferma_in_referente_container = By.xpath("//*[text()='Referente']/ancestor::div[@role='region']//button");	
	public By erroreMesi = By.xpath("//h2[text()='Errore']//..//p");
	public By alertAscensore = By.xpath("//h2[text()='Attenzione']//..//p");
	public By alertAscensore2 = By.xpath("//h2[text()='Attenzione']//..//p[text()='La potenza richiesta/disponibile non è sufficiente rispetto ai dati ascensore. La potenza suggerita è 75 vuole continuare?']");
	public By alertAscensoreOk = By.xpath("//slot[@name='footer']//..//button[text()='OK']");
	public By alertAscensoreSi = By.xpath("//slot[@name='footer']//..//button[text()='SI']");
	public By calcolaAscensore = By.xpath("//button[text()='Calcola Ascensore']");
	public By bannerResidenza = By.xpath("//p[text()='Il cliente risulta residente presso un altro indirizzo di fornitura. Se prosegui verrà effettuata una \"Modifica Stato Residente\"']");
	public By chiudBannerResidenza = By.xpath("//button[@name='ChiudiAsset']");
	public By chiudiAlert = By.xpath("//button[text()='Chiudi']");
	
	  public ConfermaFornituraComponent(WebDriver driver) {
		  super(driver);
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
	  public void pressButton(By button) throws Exception {
	        util.objectManager(button, util.scrollAndClick);
	    }
	  
	
	  public void confermaCommodity(String pod) throws Exception{
			//String commodityRadio = "//div[text()='"+pod+"']/ancestor::table//label[contains(@class,'radio')]";
			String commodityRadio = "//div[text()='"+pod+"']/ancestor::tr//label[contains(@class,'radio')]";

			if(!util.verifyExistence(By.xpath(commodityRadio), 30)) throw new Exception("Nella sezione Commodity impossibile cliccare sul radiobutton nella tabella per conferma fornitura");
			util.objectManager(By.xpath(commodityRadio),util.scrollAndClick);
			//util.objectManager(confirmButton,util.scrollAndClick);
			
		}
	  
	  
	  public void inserisciResidente(String residente) throws Exception{
		  util.selectLighningValue("Residente", residente);
	  }
	  
	  public void inserisciTitolarita(String titolarita) throws Exception{
		  util.selectLighningValue("Titolarita’", titolarita);
	  }
	  
	  public void inserisciOrdineFittizio(String ordineFittizio) throws Exception{
		  util.selectLighningValue("Ordine Fittizio", ordineFittizio);
	  }
	  
	  public void inserisciTelefonoDistributore(String distributore) throws Exception{
		  sendText("Telefono Distributore", distributore);
	  }
	  
	  public void inserisciMatricolaContatore(String valore) throws Exception{
		  sendText("Matricola Contatore", valore);
	  }

	  public void inserisciCategoriaConsumo(String valore) throws Exception{
		  util.selectLighningValue("Categoria di Consumo", valore);
	  }
	  
	  public void inserisciOrdineGrandezza(String valore) throws Exception{
		  util.selectLighningValue("Ordine di Grandezza", valore);
	  }
	  
	  public void inserisciProfiloConsumo(String valore) throws Exception{
		  util.selectLighningValue("Profilo di Consumo", valore);
	  }
	  
	  public void inserisciPotenzialitaRichiesta(String valore) throws Exception{
		  sendText("Potenzialita’ Richiesta", valore);
	  }
	  	  
	  public void inserisciClassePrelievo(String valore) throws Exception{
		  util.selectLighningValue("Classe di Prelievo", valore);
	  }
	  
	  public void inserisciCategoriaUso(String valore) throws Exception{
		  util.selectLighningValue("Categoria Uso", valore);
	  }
	  
	  public void inserisciAscensore(String ascensore) throws Exception{
		  util.selectLighningValue("Ascensore", ascensore);
	  }
	  
	  public void inserisciDisalimentabilita(String disalimentabilita) throws Exception{
		  util.selectLighningValue("Disalimentabilita’", disalimentabilita);
	  }
	  
	  public void inserisciConsumoAnnuo(String consumoAnnuo) throws Exception{
		  sendText("Consumo Annuo", consumoAnnuo);
	  }
	  
	  public void inserisciCategoriaMerceologica(String categoriaMerceologica) throws Exception{
		  util.selectLighningValue("Categoria Merceologica SAP", categoriaMerceologica);
		  
	  }
	  
	  public void inserisciTipologiaAscensore(String tipologiaAscensore) throws Exception{
		  util.selectLighningValue("Tipologia ascensore", tipologiaAscensore);
		  
	  }
	  
	  public void inserisciCorrenteDiRegime(String inserisciCorrenteDiRegime) throws Exception{
		  sendText("Corrente di regime", inserisciCorrenteDiRegime);
		  
	  }
	  
	  public void verificaValoreCategoriaMerceologica(String categoria) throws Exception{
			 TimeUnit.SECONDS.sleep(3);
			String p=driver.findElement(inputCategoria).getAttribute("value");
			
				for (int i=0;i<8;i++) {
					TimeUnit.SECONDS.sleep(10);
					if (p.contentEquals(categoria))
						break;
				}
	  }
	  public void verificaCampiSezioneCommodityGas(String pod) throws Exception{
			String pdr = util.waitAndGetElement(By.xpath("//label[text()='PDR']/ancestor::div[1]//input"),true).getAttribute("value");
			By matricola_contatore = By.xpath("//label[text()='Matricola Contatore']/ancestor::div[1]//input");
			By potenzialita=By.xpath("//label[text()='Potenzialita’']/ancestor::div[1]//input");
			By categoriaConsumo=By.xpath("//label[text()='Categoria di Consumo']/ancestor::div[1]//input");
			By categoriaMarketing=By.xpath("//label[text()='Categoria di Marketing']/ancestor::div[1]//input");
			By ordineGrandezza=By.xpath("//label[text()='Ordine di Grandezza']/ancestor::div[1]//input");
			By profiloConsumo=By.xpath("//label[text()='Profilo di Consumo']/ancestor::div[1]//input");
			By potenzialitaRichiesta=By.xpath("//label[text()='Potenzialita’ Richiesta']/ancestor::div[1]//input");
			By utilizzo=By.xpath("//label[text()='Utilizzo']/ancestor::div[1]//input");
			By categoriaUso=By.xpath("//label[text()='Categoria Uso']/ancestor::div[1]//input");
			By classePrelievo=By.xpath("//label[text()='Classe di Prelievo']/ancestor::div[1]//input");
			By prelieviConsumiAnno=By.xpath("//label[text()='Prelievi Consumi/Anno']/ancestor::div[1]//input");
			By titolarita=By.xpath("//label[text()='Titolarita’']/ancestor::div[1]//input");
			By delibera40=By.xpath("//label[text()='Delibera 40']/ancestor::div[1]//input");
			By telefonoSms=By.xpath("//label[text()='Telefono SMS DEL. 40']/ancestor::div[1]//input");
			By telefonoDistributore=By.xpath("//label[text()='Telefono Distributore']/ancestor::div[1]//input");
			By note=By.xpath("//label[text()='Note']/ancestor::div[1]//input");
			By ordineFittizio=By.xpath("//label[text()='Ordine Fittizio']/ancestor::div[1]//input");
			By invioDocumentazioni=By.xpath("//label[text()='Invio Documentazione']/ancestor::div[1]//input");

			if(!pdr.contentEquals(pod)) throw new Exception("il campo pdr non risulta prepopolato con il valore del pod");
			verifyComponentExistence(matricola_contatore);
			verifyComponentExistence(potenzialita);
			verifyComponentExistence(categoriaConsumo);
			verifyComponentExistence(categoriaMarketing);
			verifyComponentExistence(ordineGrandezza);
			verifyComponentExistence(profiloConsumo);
			verifyComponentExistence(potenzialitaRichiesta);
			verifyComponentExistence(utilizzo);
			verifyComponentExistence(categoriaUso);
			verifyComponentExistence(classePrelievo);
			verifyComponentExistence(prelieviConsumiAnno);
			verifyComponentExistence(titolarita);
			verifyComponentExistence(delibera40);
			verifyComponentExistence(telefonoSms);
			verifyComponentExistence(telefonoDistributore);
			verifyComponentExistence(ordineFittizio);
			verifyComponentExistence(invioDocumentazioni);
			verifyComponentExistence(note);
			
		}
	  
	  public void verificaCampiSezioneCommodityGasBusiness(String pod) throws Exception{
			String pdr = util.waitAndGetElement(By.xpath("//label[text()='PDR']/ancestor::div[1]//input"),true).getAttribute("value");
			By matricola_contatore = By.xpath("//label[text()='Matricola Contatore']/ancestor::div[1]//input");
			By potenzialita=By.xpath("//label[text()='Potenzialita’']/ancestor::div[1]//input");
			By categoriaConsumo=By.xpath("//label[text()='Categoria di Consumo']/ancestor::div[1]//input");
			By categoriaMarketing=By.xpath("//label[text()='Categoria di Marketing']/ancestor::div[1]//input");
			By ordineGrandezza=By.xpath("//label[text()='Ordine di Grandezza']/ancestor::div[1]//input");
			By profiloConsumo=By.xpath("//label[text()='Profilo di Consumo']/ancestor::div[1]//input");
			By potenzialitaRichiesta=By.xpath("//label[text()='Potenzialita’ Richiesta']/ancestor::div[1]//input");
			By utilizzo=By.xpath("//label[text()='Utilizzo']/ancestor::div[1]//input");
			By categoriaUso=By.xpath("//label[text()='Categoria Uso']/ancestor::div[1]//input");
			By classePrelievo=By.xpath("//label[text()='Classe di Prelievo']/ancestor::div[1]//input");
			By prelieviConsumiAnno=By.xpath("//label[text()='Prelievi Consumi/Anno']/ancestor::div[1]//input");
			By titolarita=By.xpath("//label[text()='Titolarita’']/ancestor::div[1]//input");
			By telefonoDistributore=By.xpath("//label[text()='Telefono Distributore']/ancestor::div[1]//input");
			By note=By.xpath("//label[text()='Note']/ancestor::div[1]//input");
			By ordineFittizio=By.xpath("//label[text()='Ordine Fittizio']/ancestor::div[1]//input");
			By invioDocumentazioni=By.xpath("//label[text()='Invio Documentazione']/ancestor::div[1]//input");

			if(!pdr.contentEquals(pod)) throw new Exception("il campo pdr non risulta prepopolato con il valore del pod");
			verifyComponentExistence(matricola_contatore);
			verifyComponentExistence(potenzialita);
			verifyComponentExistence(categoriaConsumo);
			verifyComponentExistence(categoriaMarketing);
			verifyComponentExistence(ordineGrandezza);
			verifyComponentExistence(profiloConsumo);
			verifyComponentExistence(potenzialitaRichiesta);
			verifyComponentExistence(utilizzo);
			verifyComponentExistence(categoriaUso);
			verifyComponentExistence(classePrelievo);
			verifyComponentExistence(prelieviConsumiAnno);
			verifyComponentExistence(titolarita);
			verifyComponentExistence(telefonoDistributore);
			verifyComponentExistence(ordineFittizio);
			verifyComponentExistence(invioDocumentazioni);
			verifyComponentExistence(note);
			
		}
	  
	  public void verificaCampiSezioneCommodityGasSubentro(String pod) throws Exception{
			String pdr = util.waitAndGetElement(By.xpath("//label[text()='PDR']/ancestor::div[1]//input"),true).getAttribute("value");
			By matricola_contatore = By.xpath("//label[text()='Matricola Contatore']/ancestor::div[1]//input");
			By potenzialita=By.xpath("//label[text()='Potenzialita’']/ancestor::div[1]//input");
			By categoriaConsumo=By.xpath("//label[text()='Categoria di Consumo']/ancestor::div[1]//input");
			By categoriaMarketing=By.xpath("//label[text()='Categoria di Marketing']/ancestor::div[1]//input");
			By ordineGrandezza=By.xpath("//label[text()='Ordine di Grandezza']/ancestor::div[1]//input");
			By profiloConsumo=By.xpath("//label[text()='Profilo di Consumo']/ancestor::div[1]//input");
			By potenzialitaRichiesta=By.xpath("//label[text()='Potenzialita’ Richiesta']/ancestor::div[1]//input");
			By utilizzo=By.xpath("//label[text()='Utilizzo']/ancestor::div[1]//input");
			By categoriaUso=By.xpath("//label[text()='Categoria Uso']/ancestor::div[1]//input");
			By classePrelievo=By.xpath("//label[text()='Classe di Prelievo']/ancestor::div[1]//input");
			By prelieviConsumiAnno=By.xpath("//label[text()='Prelievi Consumi/Anno']/ancestor::div[1]//input");
			By titolarita=By.xpath("//label[text()='Titolarita’']/ancestor::div[1]//input");
			By telefonoDistributore=By.xpath("//label[text()='Telefono Distributore']/ancestor::div[1]//input");
			By note=By.xpath("//label[text()='Note']/ancestor::div[1]//input");
			By ordineFittizio=By.xpath("//label[text()='Ordine Fittizio']/ancestor::div[1]//input");
			By invioDocumentazioni=By.xpath("//label[text()='Invio Documentazione']/ancestor::div[1]//input");

			if(!pdr.contentEquals(pod)) throw new Exception("il campo pdr non risulta prepopolato con il valore del pod");
			verifyComponentExistence(matricola_contatore);
			verifyComponentExistence(potenzialita);
			verifyComponentExistence(categoriaConsumo);
			verifyComponentExistence(categoriaMarketing);
			verifyComponentExistence(ordineGrandezza);
			verifyComponentExistence(profiloConsumo);
			verifyComponentExistence(potenzialitaRichiesta);
			verifyComponentExistence(utilizzo);
			verifyComponentExistence(categoriaUso);
			verifyComponentExistence(classePrelievo);
			verifyComponentExistence(prelieviConsumiAnno);
			verifyComponentExistence(titolarita);
			verifyComponentExistence(telefonoDistributore);
			//verifyComponentExistence(ordineFittizio);
			//verifyComponentExistence(invioDocumentazioni);
			//verifyComponentExistence(note);
			
		}

	  public void verificaCampiSezioneCommodityVolturaConAccolloResGas(String pod) throws Exception{
			String pdr = util.waitAndGetElement(By.xpath("//label[text()='PDR']/ancestor::div[1]//input"),true).getAttribute("value");
			By matricola_contatore = By.xpath("//label[text()='Matricola Contatore']/ancestor::div[1]//input");
			By potenzialita=By.xpath("//label[text()='Potenzialita’']/ancestor::div[1]//input");
			By categoriaConsumo=By.xpath("//label[text()='Categoria di Consumo']/ancestor::div[1]//input");
			By categoriaMarketing=By.xpath("//label[text()='Categoria di Marketing']/ancestor::div[1]//input");
			By ordineGrandezza=By.xpath("//label[text()='Ordine di Grandezza']/ancestor::div[1]//input");
			By profiloConsumo=By.xpath("//label[text()='Profilo di Consumo']/ancestor::div[1]//input");

			By invioDocumentazioni=By.xpath("//label[text()='Invio Documentazione']/ancestor::div[1]//input");

			if(!pdr.contentEquals(pod)) throw new Exception("il campo pdr non risulta prepopolato con il valore del pod");
			verifyComponentExistence(matricola_contatore);
			verifyFieldIsNotEmpty(matricola_contatore);
			verifyComponentExistence(potenzialita);
		//	verifyFieldIsNotEmpty(potenzialita);
			verifyComponentExistence(categoriaConsumo);
		//	verifyFieldIsNotEmpty(categoriaConsumo);
			verifyComponentExistence(categoriaMarketing);
		//	verifyFieldIsNotEmpty(categoriaMarketing);
			verifyComponentExistence(ordineGrandezza);
		//	verifyFieldIsNotEmpty(ordineGrandezza);
			verifyComponentExistence(profiloConsumo);
		//	verifyFieldIsNotEmpty(profiloConsumo);
			verifyComponentExistence(invioDocumentazioni);
			verifyFieldIsNotEmpty(invioDocumentazioni);
		}

	  public void verificaCampiSezioneCommodityEle(String pod) throws Exception{
			String numero_pod = util.waitAndGetElement(By.xpath("//label[text()='POD']/ancestor::div[1]//input"),true).getAttribute("value");
			By tensioneConsegna = By.xpath("//label[text()='Tensione Di Consegna']/ancestor::div[1]//input");
			By potenzaDisponibile=By.xpath("//label[text()='Potenza Disponibile']/ancestor::div[1]//input");
			By potenzaContrattuale=By.xpath("//label[text()='Potenza Contrattuale']/ancestor::div[1]//input");
			By potenzaFranchigia=By.xpath("//label[text()='Potenza In Franchigia']/ancestor::div[1]//input");
			By tipoPotenza=By.xpath("//label[text()='Tipo Potenza']/ancestor::div[1]//input");
			By tensioneRichiesta=By.xpath("//label[text()='Tensione Richiesta']/ancestor::div[1]//input");
			By livelloDiTensione=By.xpath("//label[text()='Livello Di Tensione']/ancestor::div[1]//input");
			By potenzaRichiesta=By.xpath("//label[text()='Potenza Richiesta']/ancestor::div[1]//input");
			By tipoPotenzaRichiesta=By.xpath("//label[text()='Tipo Potenza Richiesta']/ancestor::div[1]//input");
			By franchigia=By.xpath("//label[text()='Franchigia']/ancestor::div[1]//input");
			By emissioneFatturaAnticipata=By.xpath("//label[text()='Emissione Fattura Anticipata']/ancestor::div[1]//input");
			By disalimentabilita=By.xpath("//label[text()='Disalimentabilita’']/ancestor::div[1]//input");
			By tipologiaDisalimentabilita=By.xpath("//label[text()='Tipologia Disalimentabilita’']/ancestor::div[1]//input");
			By telefonoDisalimentabilita=By.xpath("//label[text()='Telefono Disalimentabilita’']/ancestor::div[1]//input");
			By categoriaMerceologicaSAP=By.xpath("//label[text()='Categoria Merceologica SAP']/ancestor::div[1]//input");
			By residente=By.xpath("//label[text()='Residente']/ancestor::div[1]//input");
			By titolarita=By.xpath("//label[text()='Titolarita’']/ancestor::div[1]//input");
			By ordineFittizio=By.xpath("//label[text()='Ordine Fittizio']/ancestor::div[1]//input");
			By invioDocumentazione=By.xpath("//label[text()='Invio Documentazione']/ancestor::div[1]//input");
			By telefonoDistributore=By.xpath("//label[text()='Telefono Distributore']/ancestor::div[1]//input");
			By noteAlDistributore=By.xpath("//label[text()='Note al Distributore']/ancestor::div[1]//input");
			By ascensore=By.xpath("//label[text()='Ascensore']/ancestor::div[1]//input");
			By tipologiaAscensore=By.xpath("//label[text()='Tipologia ascensore']/ancestor::div[1]//input");
			By correnteRegime=By.xpath("//label[text()='Corrente di regime']/ancestor::div[1]//input");
			By correnteAvviamento=By.xpath("//label[text()='Corrente di avviamento']/ancestor::div[1]//input");
			By potenzaSuggerita=By.xpath("//label[text()='Potenza suggerita']/ancestor::div[1]//input");
			By tipoMisuratore=By.xpath("//label[text()='Tipo Misuratore']/ancestor::div[1]//input");
			By consumoAnnuo=By.xpath("//label[text()='Consumo Annuo']/ancestor::div[1]//input");
			
			if(!numero_pod.contentEquals(pod)) throw new Exception("il campo pod non risulta prepopolato con il valore del pod");
			verifyComponentExistence(tensioneConsegna);
			verifyComponentExistence(potenzaDisponibile);
			verifyComponentExistence(potenzaContrattuale);
			verifyComponentExistence(potenzaFranchigia);
			verifyComponentExistence(tipoPotenza);
			verifyComponentExistence(tensioneRichiesta);
			verifyComponentExistence(livelloDiTensione);
			verifyComponentExistence(potenzaRichiesta);
			verifyComponentExistence(tipoPotenzaRichiesta);
			verifyComponentExistence(franchigia);
			verifyComponentExistence(emissioneFatturaAnticipata);
			verifyComponentExistence(titolarita);
			verifyComponentExistence(disalimentabilita);
			verifyComponentExistence(tipologiaDisalimentabilita);
			verifyComponentExistence(telefonoDistributore);
			verifyComponentExistence(ordineFittizio);
			verifyComponentExistence(telefonoDisalimentabilita);
			verifyComponentExistence(categoriaMerceologicaSAP);
			verifyComponentExistence(residente);
			verifyComponentExistence(invioDocumentazione);
			verifyComponentExistence(noteAlDistributore);
			verifyComponentExistence(ascensore);
			verifyComponentExistence(tipologiaAscensore);
			verifyComponentExistence(correnteRegime);
			verifyComponentExistence(correnteAvviamento);
			verifyComponentExistence(potenzaSuggerita);
			verifyComponentExistence(tipoMisuratore);
			verifyComponentExistence(consumoAnnuo);
			
		}
	  public void verificaCampiSezioneCommodityEleRes(String pod) throws Exception{
			String numero_pod = util.waitAndGetElement(By.xpath("//label[text()='POD']/ancestor::div[1]//input"),true).getAttribute("value");
			By tensioneConsegna = By.xpath("//label[text()='Tensione Di Consegna']/ancestor::div[1]//input");
			By potenzaDisponibile=By.xpath("//label[text()='Potenza Disponibile']/ancestor::div[1]//input");
			By potenzaContrattuale=By.xpath("//label[text()='Potenza Contrattuale']/ancestor::div[1]//input");
			By potenzaFranchigia=By.xpath("//label[text()='Potenza In Franchigia']/ancestor::div[1]//input");
			By tipoPotenza=By.xpath("//label[text()='Tipo Potenza']/ancestor::div[1]//input");
			By tensioneRichiesta=By.xpath("//label[text()='Tensione Richiesta']/ancestor::div[1]//input");
			By livelloDiTensione=By.xpath("//label[text()='Livello Di Tensione']/ancestor::div[1]//input");
			By potenzaRichiesta=By.xpath("//label[text()='Potenza Richiesta']/ancestor::div[1]//input");
			By tipoPotenzaRichiesta=By.xpath("//label[text()='Tipo Potenza Richiesta']/ancestor::div[1]//input");
			By franchigia=By.xpath("//label[text()='Franchigia']/ancestor::div[1]//input");
			By emissioneFatturaAnticipata=By.xpath("//label[text()='Emissione Fattura Anticipata']/ancestor::div[1]//input");
			By disalimentabilita=By.xpath("//label[text()='Disalimentabilita’']/ancestor::div[1]//input");
			By tipologiaDisalimentabilita=By.xpath("//label[text()='Tipologia Disalimentabilita’']/ancestor::div[1]//input");
			By telefonoDisalimentabilita=By.xpath("//label[text()='Telefono Disalimentabilita’']/ancestor::div[1]//input");
			By categoriaMerceologicaSAP=By.xpath("//label[text()='Categoria Merceologica SAP']/ancestor::div[1]//input");
			By residente=By.xpath("//label[text()='Residente']/ancestor::div[1]//input");
			By titolarita=By.xpath("//label[text()='Titolarita’']/ancestor::div[1]//input");
			By ordineFittizio=By.xpath("//label[text()='Ordine Fittizio']/ancestor::div[1]//input");
			By invioDocumentazione=By.xpath("//label[text()='Invio Documentazione']/ancestor::div[1]//input");
			By telefonoDistributore=By.xpath("//label[text()='Telefono Distributore']/ancestor::div[1]//input");
			By noteAlDistributore=By.xpath("//label[text()='Note al Distributore']/ancestor::div[1]//input");
			By ascensore=By.xpath("//label[text()='Ascensore']/ancestor::div[1]//input");
			By tipologiaAscensore=By.xpath("//label[text()='Tipologia ascensore']/ancestor::div[1]//input");
			By correnteRegime=By.xpath("//label[text()='Corrente di regime']/ancestor::div[1]//input");
			By correnteAvviamento=By.xpath("//label[text()='Corrente di avviamento']/ancestor::div[1]//input");
			By potenzaSuggerita=By.xpath("//label[text()='Potenza suggerita']/ancestor::div[1]//input");
			By tipoMisuratore=By.xpath("//label[text()='Tipo Misuratore']/ancestor::div[1]//input");
			By consumoAnnuo=By.xpath("//label[text()='Consumo Annuo']/ancestor::div[1]//input");
			
//			if(!numero_pod.contentEquals(pod)) throw new Exception("il campo pod non risulta prepopolato con il valore del pod");
			verifyComponentExistence(tensioneConsegna);
			verifyComponentExistence(potenzaDisponibile);
			verifyComponentExistence(potenzaContrattuale);
			verifyComponentExistence(potenzaFranchigia);
			verifyComponentExistence(tipoPotenza);
			verifyComponentExistence(tensioneRichiesta);
			verifyComponentExistence(livelloDiTensione);
			verifyComponentExistence(potenzaRichiesta);
			verifyComponentExistence(tipoPotenzaRichiesta);
			verifyComponentExistence(franchigia);
			verifyComponentExistence(emissioneFatturaAnticipata);
			verifyComponentExistence(titolarita);
			verifyComponentExistence(disalimentabilita);
			verifyComponentExistence(tipologiaDisalimentabilita);
			verifyComponentExistence(telefonoDistributore);
			verifyComponentExistence(ordineFittizio);
			verifyComponentExistence(telefonoDisalimentabilita);
			verifyComponentExistence(categoriaMerceologicaSAP);
			verifyComponentExistence(residente);
			verifyComponentExistence(invioDocumentazione);
			verifyComponentExistence(noteAlDistributore);
			verifyComponentExistence(ascensore);
			verifyComponentExistence(tipologiaAscensore);
			verifyComponentExistence(correnteRegime);
			verifyComponentExistence(correnteAvviamento);
			verifyComponentExistence(potenzaSuggerita);
			verifyComponentExistence(tipoMisuratore);
			verifyComponentExistence(consumoAnnuo);
			
		}

	  public void verificaCampoUsoFornituraDiversoAbitazioneGas(String uso_fornitura) throws Exception{
		By uso=By.xpath("//div[text()='GAS']/ancestor::tr//div[text()='"+uso_fornitura+"']");
		verifyComponentExistence(uso);
		//div[text()='GAS']/ancestor::tr//div[text()='Uso Diverso da Abitazione']
	  }		
	  
		public void inserisciCampiMesi(String valoreMesi) throws Exception {
			ArrayList<By> mesi = new ArrayList();
			mesi.add(By.xpath("//label[text()=\"Gennaio\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Febbraio\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Marzo\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Aprile\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Maggio\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Giugno\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Luglio\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Agosto\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Settembre\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Ottobre\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Novembre\"]//..//input"));
			mesi.add(By.xpath("//label[text()=\"Dicembre\"]//..//input"));

			for (int i = 0; i < 12; i++) {
				clearAndSendKeys(mesi.get(i), valoreMesi);
			}
		}
		
		public void verifyCampiAscensore() throws Exception {
			By tipologiaAscensore=By.xpath("//label[text()='Tipologia ascensore']//..//input");
			By correnteRegime=By.xpath("//label[text()='Corrente di regime']//..//input");			
			verifyComponentExistence(tipologiaAscensore); 
			verifyComponentExistence(correnteRegime);
		}
}



