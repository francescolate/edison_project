package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ContattaciComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By contattaciLink=By.xpath("//a[text()='Contattaci' and @href='/it/supporto/faq/contattaci']");
	public By contattaciPath=By.xpath("//ul[@class='breadcrumbs component']");
    public By contattaciTitle=By.xpath("//h1[@class='image-hero_title text--page-heading'and text()='Tanti modi per entrare in contatto con noi']");
    public By contattaciSubtitle=By.xpath("//p[@class='image-hero_detail text--detail'and text()='Enel Energia è sempre in contatto con te, se sei cliente e se vuoi diventarlo.']");
    public By contattacitest1Title=By.xpath("//section[@data-module='article-text-container']//h3[text()='Per le nostre offerte']");
    public By textSection=By.xpath("//section[@data-module='article-text-container']");
  //  public By clickQuiLink=By.xpath("//section[@data-module='article-text-container']//ul//a[@href='https://www-colla.enel.it/spazio-enel/']//span[text()='Clicca qui']");
    public String click_QuiLink="//section[@data-module='article-text-container']//ul//a[@href='#spazio-enel/']//span[text()='Clicca qui']";
    public By contattacitest2Title=By.xpath("//section[@data-module='article-text-container']//h3[text()='Per i nostri clienti']");
    public By paragraph=By.xpath("//section[@data-module='article-text-container']//p");
    public By facebookLogin=By.id("loginbutton");
    public By twitterSearch=By.xpath("//div//input[@placeholder='Search Twitter' and @data-testid='SearchBox_Search_Input']");
    public By telegramImage=By.xpath("//a[@class='tl_main_logo']//div[@class='tl_main_logo_title_image' and @alt='Telegram']");
    public By documentiTitle=By.xpath("//h3[normalize-space(text())='Invio documenti: login']");
    public By pdfPage=By.xpath("//body//embed[@type='application/pdf']");
    public By leggicomefunzionaLink=By.xpath("//section[@data-module='article-text-container']//ul//li//a[@href='https://corporate-qual.enel.it/it/megamenu/media/news/2018/04/enel-lancia-app-pedius-per-comunicazione-non-udenti' and text()='leggi come funziona']");
    public By clickVuoiScriverciViaWeb = By.xpath("//a[@href='https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON']");
    
    public String text1= "Parla con un esperto Parla con uno dei nostri consulenti per scoprire tutti i vantaggi delle nostre offerte, puoi ricevere tutte le informazioni che desideri utilizzando la chat o anche: chiamando il numero verde 800900860. Il nostro servizio clienti è a disposizione dalle 7:00 alle 22:00 tutti i giorni dal Lunedì alla Domenica (escluse le festività nazionali). Il numero verde è gratuito, anche da cellulare. recandoti allo Spazio Enel più vicino a te. Clicca qui per scoprire le nostre sedi.";
    //public String text2= "Per le nostre offerte Parla con un esperto Parla con uno dei nostri consulenti per scoprire tutti i vantaggi delle nostre offerte, puoi ricevere tutte le informazioni che desideri utilizzando la chat o anche: chiamando il numero verde 800900860. Il nostro servizio clienti è a disposizione dalle 7:00 alle 22:00 tutti i giorni dal Lunedì alla Domenica (escluse le festività nazionali). Il numero verde è gratuito, anche da cellulare. recandoti allo Spazio Enel più vicino a te. Clicca qui per scoprire le nostre sedi. contattando lo Spazio Enel a te più vicino anche telefonicamente. Clicca qui per trovare il numero dello Spazio Enel della tua città. Le nostre agenzie di vendita hanno ripreso l’attività e potranno farlo anche attraverso un contatto telefonico. Clicca qui per verificare se il numero che ti ha contattato appartiene ad una nostra agenzia autorizzata. Per i nostri clienti  Vuoi parlarci via web? Entra in Area Clienti e contattaci in chat.  Vuoi scriverci via web? Invia un messaggio cliccando qui.  Vuoi scriverci su Facebook? Inviaci un messaggio qui.  Vuoi scriverci via Twitter? Scrivici clicca qui.  Vuoi scoprire il nuovo modo di comunicare con noi?  Scopri @EnelEnergiaBot, il nuovo modo semplice e veloce per effettuare in autonomia: Autolettura Verifica stato pagamento bolletta Verifica stato attivazione contratto Ricerca del punto Enel più vicino a te Iscrizione a BollettaWeb Aggiornamento dei dati di contatto Per utilizzarlo basta: scaricare Telegram sul tuo smartphone ed iscriverti al chatbot @EnelEnergiaBot; andare su Facebook Messenger alla pagina ufficiale di Enel Energia; oppure scriverci su . Vuoi inviare un documento via web? Invia documenti o ricevute di pagamento, clicca qui. Vuoi trovare lo Spazio Enel più vicino? Trova quello più vicino a te. Clicca qui. Vuoi chiamarci? Contatta il Servizio clienti. Servizio clienti dall'Italia: 800900860 (il numero verde è gratuito, anche da cellulare). Servizio clienti dall'estero: +39 0664511012. Utilizza l’app Pedius. L’app permette alle persone sorde di telefonare utilizzando tecnologie di riconoscimento e sintesi vocale, leggi come funziona. Comunica l'Autolettura: 800900837 Vuoi inviarci un fax? Consulta l'elenco dei numeri di fax, clicca qui.";
    public String text2= "Per le nostre offerte Parla con un esperto Parla con uno dei nostri consulenti per scoprire tutti i vantaggi delle nostre offerte, puoi ricevere tutte le informazioni che desideri utilizzando la chat o anche: chiamando il numero verde 800900860. Il nostro servizio clienti è a disposizione dalle 7:00 alle 22:00 tutti i giorni dal Lunedì alla Domenica (escluse le festività nazionali). Il numero verde è gratuito, anche da cellulare. recandoti allo Spazio Enel più vicino a te. Clicca qui per scoprire le nostre sedi. contattando lo Spazio Enel a te più vicino anche telefonicamente. Clicca qui per trovare il numero dello Spazio Enel della tua città. Per presentarti le nostre offerte potrai ricevere anche un contatto telefonico: call center – dai numeri 02/39623792 o 081/18826792 agenzie di vendita – Clicca qui per verificare se il numero che ti ha contattato appartiene ad una nostra agenzia autorizzata. Per i nostri clienti  Vuoi parlarci via web? Entra in Area Clienti e contattaci in chat.  Vuoi scriverci via web? Invia un messaggio cliccando qui.  Vuoi scriverci su Facebook? Inviaci un messaggio qui.  Vuoi scriverci via Twitter? Scrivici clicca qui.  Vuoi scoprire il nuovo modo di comunicare con noi?  Scopri @EnelEnergiaBot, il nuovo modo semplice e veloce per effettuare in autonomia: Autolettura Verifica stato pagamento bolletta Verifica stato attivazione contratto Ricerca del punto Enel più vicino a te Iscrizione a BollettaWeb Aggiornamento dei dati di contatto Per utilizzarlo basta: scaricare Telegram sul tuo smartphone ed iscriverti al chatbot @EnelEnergiaBot; andare su Facebook Messenger alla pagina ufficiale di Enel Energia; oppure scriverci su . Vuoi inviare un documento via web? Invia documenti o ricevute di pagamento, clicca qui. Vuoi trovare lo Spazio Enel più vicino? Trova quello più vicino a te. Clicca qui. Vuoi chiamarci? Contatta il Servizio clienti. Servizio clienti dall'Italia: 800900860 (il numero verde è gratuito, anche da cellulare). Servizio clienti dall'estero: +39 0664511012. Utilizza l’app Pedius. L’app permette alle persone sorde di telefonare utilizzando tecnologie di riconoscimento e sintesi vocale, leggi come funziona. Comunica l'Autolettura: 800900837 Vuoi inviarci un fax? Consulta l'elenco dei numeri di fax, clicca qui.";
    public String paragrafotesto="Vuoi parlarci via web? Entra in Area Clienti e contattaci in chat.";
    public String paragrafotesto2="Vuoi scriverci via web? Invia un messaggio cliccando qui.";
    public String paragrafotesto3="Vuoi scriverci su Facebook? Inviaci un messaggio qui.";
    public String paragrafotesto4="Vuoi scriverci via Twitter? Scrivici clicca qui.";
    public String paragrafotesto5="scaricare Telegram sul tuo smartphone ed iscriverti al chatbot @EnelEnergiaBot;";
    public String paragrafotesto6="Vuoi inviare un documento via web? Invia documenti o ricevute di pagamento, clicca qui.";
    public String paragrafotesto7="Vuoi trovare lo Spazio Enel più vicino? Trova quello più vicino a te. Clicca qui.";
    public String paragrafotesto8="Vuoi inviarci un fax? Consulta l'elenco dei numeri di fax, clicca qui.";
    public String text3= "Scopri @EnelEnergiaBot, il nuovo modo semplice e veloce per effettuare in autonomia: Autolettura Verifica stato pagamento bolletta Verifica stato attivazione contratto Ricerca del punto Enel più vicino a te Iscrizione a BollettaWeb Aggiornamento dei dati di contatto Per utilizzarlo basta scaricare Telegram sul tuo smartphone ed iscriverti al chatbot @EnelEnergiaBot. Vuoi inviare un documento via web? Invia documenti o ricevute di pagamento, clicca qui. Vuoi trovare lo Spazio Enel più vicino? Trova quello più vicino a te. Clicca qui. Vuoi chiamarci? Contatta il Servizio clienti. Servizio clienti dall'Italia: 800900860 (il numero verde è gratuito, anche da cellulare). Servizio clienti dall'estero: +39 0664511012. Utilizza l’app Pedius. L’app permette alle persone sorde di telefonare utilizzando tecnologie di riconoscimento e sintesi vocale, leggi come funziona. Comunica l'Autolettura: 800900837 Vuoi inviarci un fax? Consulta l'elenco dei numeri di fax, clicca qui.";
    public String textpedius= "Utilizza l’app Pedius. L’app permette alle persone sorde di telefonare utilizzando tecnologie di riconoscimento e sintesi vocale, leggi come funziona.";
    public String linkCONTATTACI="it/supporto/faq/contattaci";
    public String linkCARICA_DOCUMENTI2="it/servizi-online/carica-documenti?ta=AP";
    
	public ContattaciComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("oggetto web con xpath " + existingObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void replaceLinkCollaInXpathEClickObject(String link) throws Exception {
		util.objectManager(By.xpath(click_QuiLink.replace("#", link)), util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(By.xpath(click_QuiLink.replace("#", link)), util.click);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkHeaderPageContattaci(By path,By title, By subtitle) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(path));
		String pathDescription=driver.findElement(path).getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	
				
		//if (!pathDescription.contentEquals("HOME / SUPPORTO / CONTATTACI"))
		if (!pathDescription.contains("HOME / SUPPORTO / COME ENTRARE IN CONTATTO CON ENEL ENERGIA"))
			throw new Exception("sulla pagina web 'Contattaci' l'oggetto web con xpath " + path + " non esiste");
		if (!util.exists(title, 15))
			throw new Exception("sulla pagina web 'Contattaci' il titolo con xpath " + title + " non esiste");
		if (!util.exists(subtitle, 15))
			throw new Exception("sulla pagina web 'Contattaci' il sottotitolo " + subtitle + " non esiste");	
	}
		
	public void checkTextPageContattaci(By checkObject, String text) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String description=driver.findElement(checkObject).getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		System.out.println("description --> " +description);
	//	System.out.println(description);
		
		if(!description.contains(text))
			throw new Exception("sulla pagina web 'contattaci' non e' presente il testo: "+text);
		
	}
	
	public void clickLinkParagraphViaWeb(By paragraph, String textParagraph) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(paragraph));
		String paragraphpresent="NO";
		
		List<WebElement> paragraphList = driver.findElements(paragraph);
		 
		for (int i = 1; i <= paragraphList.size(); i++) { 
			String description=driver.findElement(By.xpath("//section[@data-module='article-text-container']//p["+i+"]")).getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			//System.out.println(description);
			if (description.contains(textParagraph)) {
				paragraphpresent="SI";
				util.objectManager(By.xpath("//section[@data-module='article-text-container']//p["+i+"]/span[2]/a"), util.scrollToVisibility, false);
				util.objectManager(By.xpath("//section[@data-module='article-text-container']//p["+i+"]/span[2]/a"), util.scrollAndClick);
				break;
			  }
			else paragraphpresent="NO";
			} 
		if (paragraphpresent.contentEquals("NO"))
	    	throw new Exception("sulla pagina web 'contattaci' nella sezione di testo 'Per i nostri clienti' non e' presente il paragrafo "+textParagraph+" per cui non e' possibile cliccare sul link 'qui'"); 
	}
	
	public void clickLinkParagraph(By paragraph, String textParagraph) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(paragraph));
		String paragraphpresent="NO";
		
		List<WebElement> paragraphList = driver.findElements(paragraph);
		 
		for (int i = 1; i <= paragraphList.size(); i++) { 
			String description=driver.findElement(By.xpath("//section[@data-module='article-text-container']//p["+i+"]")).getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			//System.out.println(description);
			if (description.contains(textParagraph)) {
				paragraphpresent="SI";
				util.objectManager(By.xpath("//section[@data-module='article-text-container']//p["+i+"]//span"), util.scrollToVisibility, false);
				util.objectManager(By.xpath("//section[@data-module='article-text-container']//p["+i+"]//a"), util.scrollAndClick);
				break;
			  }
			else paragraphpresent="NO";
			} 
		if (paragraphpresent.contentEquals("NO"))
	    	throw new Exception("sulla pagina web 'contattaci' nella sezione di testo 'Per i nostri clienti' non e' presente il paragrafo "+textParagraph+" per cui non e' possibile cliccare sul link 'qui'"); 
	}
	public void clickComeFunzionaLink(By paragraph, By link,String text) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(paragraph));
		String paragraphpresent="NO";
		String description=driver.findElement(paragraph).getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		//System.out.println(description);
		
		if (description.contains(text)) {
			paragraphpresent="SI";
			util.objectManager(link, util.scrollToVisibility, false);
		    util.objectManager(link, util.scrollAndClick);
		}
		else paragraphpresent="NO";
		if (paragraphpresent.contentEquals("NO"))
	    	throw new Exception("sulla pagina web 'contattaci' nella sezione di testo 'Per i nostri clienti' non e' presente il paragrafo  "+text+" per cui non e' possibile cliccare sul link 'Leggi come funziona'"); 
		
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");

  }
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
    public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
}
