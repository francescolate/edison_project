package com.nttdata.qa.enel.components.lightning;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CaseItemDetailsComponent {
	WebDriver driver;
	public SeleniumUtilities util;
	Actions actions;
	JavascriptExecutor jse;
	SpinnerManager spinner;
	
	public By elementoRichiesta = By.xpath("//h2[@id='header_5334:0']/a[@class='slds-card__header-link baseCard__header-title-container']");
	public By preventivoInviato= By.xpath("//span[contains(text(),'PREVENTIVO INVIATO')]");
	public By pod = By.xpath("//span[contains(text(),'IT001E73277736')]");
	//public By numeroUtenza = By.xpath("//span[contains(text(),'843517154')]");
	public String datadiprocessoText ="//lightning-formatted-text[contains(text(),'$VALUE$')]";
	public String datadiprocessoTextSafe = "//span[text()='$VALUE$']/../following-sibling::div/span/slot/slot/lightning-formatted-text";
	public String datadiprocessoNumber ="//lightning-formatted-number[contains(text(),'$VALUE$')]";
	public String datadiprocessoNumberSafe ="//span[text()='$VALUE$']/../following-sibling::div/span/slot/slot/lightning-formatted-number";
	public By datadiprocessoField = By.xpath("//div[@class='slds-form']//parent::div[@id='sectionContent-314']//child::div[@class='slds-form-element slds-hint-parent test-id__output-root slds-form-element_readonly slds-form-element_stacked']");
	public By numeroUtenza = By.xpath("//span[contains(text(),'843517154')]");
	public By attivaHeader=By.xpath("(//span[text()='(2)']/preceding::span[text()='Attività'])[1]");
	public By annulmentButton = By.xpath("//button[contains(text(),'Annulment')]");
	public By confermaAnnullament = By.xpath("//*[@id='btnConferma2']");
	public By spinnerAfterAnnullament = By.xpath("//*[@id='spinner_container']");
	
	//public By dropDown = By.xpath("//select[@id='j_id0:j_id4:j_id6:j_id7:cancellationReasonsPicklist']");
	public By dropDown = By.xpath("//select[contains(@id,'cancellationReasonsPicklist')]");
	public By dropDownOption = By.xpath("//*[contains(@id,'cancellationReasonsPicklist')]/option[2]");
	public By annullamentoDropdown = By.xpath("//*[contains(text(),'Annullamento Richiesta')]/following::select");
	public By annulla = By.xpath("//button[text()='Annulla']");
	public By conferma = By.xpath("//button[@id='btnConferma2']");
	public String rinunciaCliente ="Rinuncia cliente";
	public By Chiuso = By.xpath("//span[text()='Chiuso']");
	public By annullo = By.xpath("//lightning-formatted-text[text()='Annullato']");
	public By processo = By.xpath("//ancestor::div[@class='secondaryFields']//lightning-formatted-text[text()='Modifica Potenza e Tensione']");
	public By richestaHeader = By.xpath("//h1//div[text()='Richiesta']");
	public By variazioneAnagrafica = By.xpath("//h1//lightning-formatted-text[text()='VARIAZIONE ANAGRAFICA']");
	public By dettagli = By.xpath("//a[text()='Correlato']/ancestor::li/preceding-sibling::li//a[text()='Dettagli']");
	
	
	
	public String annulatoFrontOffice = "Annullato da Front Office";
	public String iframeAnnullment = "ITA_IFM_Case_Items__c.Annulment";
	
	public Select cancellationReasonsPicklist;
	
	public CaseItemDetailsComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		actions = new Actions(driver);
		jse = (JavascriptExecutor) driver;
		spinner = new SpinnerManager(driver);
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(10000);
			util.objectManager(clickableObject, util.scrollAndClick);
			WebElement button=driver.findElement(clickableObject);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", button);
			
	}
	 
	 public void clickDettagli(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(10000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
			 List<WebElement> button = driver.findElements(clickableObject);
			
			for (WebElement ele : button) {
				ele.click();
			}
	}
	 
	 public void switchFrame(String frame) throws Exception{
			util.switchToFrame(frame);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	 public void checkDataDiProcessoFields(String field,String property) throws Exception{
		 By fieldValue = By.xpath(field.replace("$VALUE$", property));
		 util.scrollToElement(driver.findElement(fieldValue));
		 String value = driver.findElement(fieldValue).getText();
		/* System.out.println(driver.findElement(fieldValue).getText());
		 System.out.println(Arrays.asList(dataDiProcesso));*/
			 if(!Arrays.asList(dataDiProcesso).contains((value)))
				 throw new Exception("Field is not present in the Data di processo");
		 
	 }
	 
	 public void checkDatiDiProcesso() throws Exception {
		 for (int i = 0; i < datiDiProcessoTextIdentifiers.length; i++) {
			 String xpath = datadiprocessoTextSafe.replace("$VALUE$", datiDiProcessoTextIdentifiers[i]);
			 checkProcessDataField(xpath, datiDiProcessoTextIdentifiers[i], datiDiProcessoTextValues[i]);
		 }
		 for (int i = 0; i < datiDiProcessoNumberIdentifiers.length; i++) {
			 String xpath = datadiprocessoNumberSafe.replace("$VALUE$", datiDiProcessoNumberIdentifiers[i]);
			 checkProcessDataField(xpath, datiDiProcessoNumberIdentifiers[i], datiDiProcessoNumberValues[i]);
		}
	 }
	 
	 public void checkDatiDiProcesso2() throws Exception {
		 for (int i = 0; i < datiDiProcessoTextIdentifiers2.length; i++) {
			 String xpath = datadiprocessoTextSafe.replace("$VALUE$", datiDiProcessoTextIdentifiers2[i]);
			 checkProcessDataField(xpath, datiDiProcessoTextIdentifiers2[i], datiDiProcessoTextValues2[i]);
		 }
		 for (int i = 0; i < datiDiProcessoNumberIdentifiers.length; i++) {
			 String xpath = datadiprocessoNumberSafe.replace("$VALUE$", datiDiProcessoNumberIdentifiers[i]);
			 checkProcessDataField(xpath, datiDiProcessoNumberIdentifiers[i], datiDiProcessoNumberValues[i]);
		}
	 }

	 public void checkProcessDataField(String fieldXpath, String fieldId, String valueToCheck) throws Exception {
		 By fieldLocator = By.xpath(fieldXpath);
		 WebElement fieldElement = driver.findElement(fieldLocator);
		 util.scrollToElement(fieldElement);
		 String text = fieldElement.getText();
		 if (!text.contains(valueToCheck)) {
			 throw new Exception("Field " + fieldId + " does not contain text: " + valueToCheck);
		 }
	 }
	 
	 public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}
	 
	 public void selectRinunciaCliente(String checkobject) throws Exception{
	
				TimeUnit.SECONDS.sleep(3);
				//switchFrame("ITA_IFM_Case_Items__c.Annulment_Disdetta");
				switchFrame("ITA_IFM_Case_Items__c.Annulment");
				Thread.sleep(5000);
				Select select = new Select(util.waitAndGetElement(annullamentoDropdown));
				select.selectByValue(checkobject);
				TimeUnit.SECONDS.sleep(5);
				util.objectManager(conferma, util.scrollToVisibility,true);
				Thread.sleep(5000);
				util.scrollToElement(driver.findElement(conferma));
				jsClickObject(conferma);
				Thread.sleep(5000);
			//	util.objectManager(annulla, util.click);
			//	spinner.checkSpinners();
			    driver.switchTo().defaultContent();

	 }
	 public void selectAnnulmentRinunciaCliente(String checkobject) throws Exception{
			
			TimeUnit.SECONDS.sleep(3);
			switchFrame("ITA_IFM_Case_Items__c.Annulment_Disdetta");
			Thread.sleep(5000);
			Select select = new Select(util.waitAndGetElement(annullamentoDropdown));
			select.selectByValue(checkobject);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(conferma, util.scrollToVisibility,true);
			Thread.sleep(5000);
			util.scrollToElement(driver.findElement(conferma));
			jsClickObject(conferma);
			Thread.sleep(5000);
		//	util.objectManager(annulla, util.click);
		//	spinner.checkSpinners();
		    driver.switchTo().defaultContent();

}
	 
	 public void checkData(By checkObject, String property) throws Exception {
			String value = driver.findElement(checkObject).getText();
			if(!property.equals(value))
				throw new Exception(value +" Field value is not matching with expected value "+property);
		}
	 
	 public static final String[] dataDiProcesso ={"IR020","3,5","NO","3,85","LIMITATA","220","Bassa Tensione"};

	 //----- TC135 Constants -----
	 
	 private final String[] datiDiProcessoTextIdentifiers = {
			 "Fase",
			 "Charge on the bill",
			 "Emissione fattura anticipata",
			 "Tipo potenza",
			 "Tensione Disponibile",
			 "Livello di Tensione di consegna"
	 };
	 
	 private final String[] datiDiProcessoNumberIdentifiers = {
			 "Potenza Richiesta"
	 };
	 
	 private final String[] datiDiProcessoTextValues = {
			 "DI200",
			 "NO",
			 "NO",
			 "LIMITATA",
			 "380",
			 "Bassa Tensione"
	 };
	 
	 private final String[] datiDiProcessoNumberValues = {
			 "3"
	 };
	 
	 private final String[] datiDiProcessoTextIdentifiers2 = {
			 "Fase",
			 "Tensione Disponibile",
	 };
	 
	 private final String[] datiDiProcessoTextValues2 = {
			 "IR010",
			 "380"
	 };
}
