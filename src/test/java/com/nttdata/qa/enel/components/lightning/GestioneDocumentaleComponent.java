package com.nttdata.qa.enel.components.lightning;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.Utility;

public class GestioneDocumentaleComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	String frame;

	
	public GestioneDocumentaleComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	final By caricaDocumentoButton = By.xpath("//button[@aria-label='Carica documento'] | //button[@label='Carica']");
	final By caricaDocumento = By.xpath("//input[@id='inputFile']");
//	public final By confermaDocumentoButton = By.xpath("//button[contains(@onclick,'confirm') and contains(@id,'ooter')]");
	public final By confermaDocumentoButton = By.xpath("//button[@id='confermaOffertaButtonHeader']");
	final public By caricaDocumenti=By.xpath("//button[@id='load' and @title='carica documento']");
	public final By generaDocumentoButton = By.xpath("//button[@aria-label='Genera documento']");
	
	
	private void chargeDocument(String filePath) throws Exception{
		File file = new File(filePath);
		TimeUnit.SECONDS.sleep(2);
//		//System.out.println("inizio a caricare");
		util.getFrameActive();
		String uploadId = getUploadId();
//		//System.out.println("UPLOAD ID TROVATO :"+uploadId);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("j$(\"input[id*='inputFileParentId']\").val('"+uploadId+"');");
		//js.executeScript("j$(\"input[id*='inputFile']\").click();");
		js.executeScript("document.getElementById('inputFileParentId').val='"+uploadId+"';");
		js.executeScript("document.getElementById('inputFileParentId').value='"+uploadId+"';");
		
		
		driver.findElement(caricaDocumento).sendKeys(file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(20);
		
	}
	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void confermaDocumentale(By button) throws Exception{
//		driver.switchTo().defaultContent();
//		String frame = util.getFrameByIndex(1);
//		
//		WebElement frameToSw = this.driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"']")
//				);
		util.getFrameActive();
//		util.frameManager(frame,button, util.scrollAndClick);
		util.objectManager(button, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
	}
	
	public String caricaDocumentoInDocumentale(String filePath) throws Exception{
//		driver.switchTo().defaultContent();
//		this.frame = util.getFrameByIndex(0);
//		WebElement frameToSw = this.driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"']")
//				);
//		driver.switchTo().frame(frameToSw);
		TimeUnit.SECONDS.sleep(20);
		util.getFrameActive();
		chargeDocument(filePath);
		TimeUnit.SECONDS.sleep(20);
		Alert alert = driver.switchTo().alert();
		alert.accept();
		TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
		return frame;
	}
	
	
	public String getUploadId() throws Exception{
		WebElement el = util.waitAndGetElement(caricaDocumentoButton);
		String mydata = el.getAttribute("onclick");
        Pattern pattern = Pattern.compile("'(.*?)'");
        Matcher matcher = pattern.matcher(mydata);
        if (matcher.find()) mydata=matcher.group(1);
        else throw new Exception("Impossibile recuperare l'id del documento da caricare. Riprovare");
		return mydata;
	}
	private void chargeDocument2(String filePath) throws Exception{
		File file = new File(filePath);
		TimeUnit.SECONDS.sleep(2);
//		//System.out.println("inizio a caricare");
		String uploadId = getUploadId2();
//		//System.out.println("UPLOAD ID TROVATO :"+uploadId);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("j$(\"input[id*='inputFileParentId']\").val('"+uploadId+"');");
		//js.executeScript("j$(\"input[id*='inputFile']\").click();");
		js.executeScript("document.getElementById('inputFileParentId').val='"+uploadId+"';");
		js.executeScript("document.getElementById('inputFileParentId').value='"+uploadId+"';");
		
		
		driver.findElement(caricaDocumento).sendKeys(file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(20);
		
	}
	public String getUploadId2() throws Exception{
		WebElement el = util.waitAndGetElement(caricaDocumenti);
		String mydata = el.getAttribute("onclick");
        Pattern pattern = Pattern.compile("'(.*?)'");
        Matcher matcher = pattern.matcher(mydata);
        if (matcher.find()) mydata=matcher.group(1);
        else throw new Exception("Impossibile recuperare l'id del documento da caricare. Riprovare");
		return mydata;
	}
	
	public String caricaDocumento(String filePath) throws Exception{
		driver.switchTo().defaultContent();
		this.frame = util.getFrameByIndex(1);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		chargeDocument2(filePath);
		TimeUnit.SECONDS.sleep(20);
	
		return frame;
	}
}
