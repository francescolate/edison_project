package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

/**
 * @author ChianteseRo
 * Seleziona  Commodity checkbox 
 * N.B. testato al momento solo per processo di attivazione VAS
 */
public class SelezionaCommodityComponent {

	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinner;
	private By searchField = By.xpath("//div[@id='labelDatagridB']//input");
	private By commodityCheckbox = By.xpath("//span[contains(text(), 'Elemento non selezionato')]/../../label[@class='slds-checkbox']");
	private By spanCheckCommodity = By.xpath("//span[@class='slds-checkbox--faux']");
	private By spanCommodity = By.xpath("//span[@class='slds-radio--faux']");
	private By searchBtn = By.xpath("//button[text()='Conferma Selezione Fornitura']");
//	private By tableHeaderBy = new By.ByXPath("//table[@aria-describedby='PCtableCI_info']");
//	private By tableHeaderBy = new By.ByXPath("//div[@class='dataTables_scrollBody']//table[@aria-describedby='PCtableCI_info']");
	private By tableHeaderBy = new By.ByXPath("//div[@class='dataTables_scrollHead']//table[@aria-describedby='PCtableCI_info']");
	private By tableRowBy = new By.ByXPath("//table[@aria-describedby='PCtableCI_info']");
	private By tableByDoc = new By.ByXPath("//table[@title='tabella dei risultati cliccabili']");
	private By btnConfermaIndirizzoFornitura = By.xpath("//button[text()='Conferma Selezione Fornitura']");
	private By btnCreaOfferta = new By.ByXPath("//div[@id='j_id0:principalForm:footerId']/div/div[@class='slds-col slds-no-flex slds-grid slds-align-bottom']/button[contains(text(), 'Crea')]");
//	public By checkBox = new By.ByXPath("//div[@class='dataTables_scrollBody']//table[@aria-describedby='PCtableCI_info']//tbody//input");
	public By checkBox = new By.ByXPath("//div[@class='dataTables_scroll']//table[@id='PCtableCI']//tbody[@id='tb']//tr//td//label//span");
	public By checkList = new By.ByXPath("//div[@class='slds-modal__footer slds-theme--default']//button[@onclick='filterCommodityPopup(); return false;']");
	
	public SelezionaCommodityComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(driver);
	}

	private void swichToDefault () throws Exception{
		this.driver.switchTo().defaultContent();
	}
	
	public void searchPOD(String pod) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.searchField, util.scrollAndSendKeys,pod);
	}
	
	public void searchPOD(By by, String pod) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, by, util.scrollAndSendKeys,pod);
	}
	
	public void selectCommodityCheckbox() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.commodityCheckbox, util.click);
		spinner.checkSpinners();
	}
	
	public void selectCommodityCheckbox(By by) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, by, util.click);
	}

	public void selectCommodityCheckbox1(By by) throws Exception{
//		util.frameManager(by, util.click);
//		util.getFrameActive();
		util.objectManager(by, util.scrollAndClick);
		util.objectManager(searchBtn, util.scrollAndClick);
//		driver.switchTo().defaultContent();
	}

	public void clickConfermaFornitura () throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.btnConfermaIndirizzoFornitura, util.click);
		spinner.checkSpinners();
	}
	
	public void clickConfermaFornitura(By by) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, by, util.click);
	}
	
	/**
	 * Ricerca e Seleziona la commodity passata nel parametro.
	 * @param commodityInput
	 * @throws Exception
	 */
	public void setCommodity(String commodityInput) throws Exception
	{
		TimeUnit.SECONDS.sleep(5);
//		String frame = util.getFrameByIndex(1);
//		//
//		//driver.findElement(by)
//		WebElement frameToSw = driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
//				);
//		driver.switchTo().frame(frameToSw);
		List<WebElement> table = driver.findElements(tableHeaderBy);
		WebElement tableHeader = table.get(0);
		WebElement tableRow = table.get(1);

		tableHeader = util.waitAndGetElement(tableHeaderBy);
		util.scrollToElement(tableRow);
		if(util.getTableRowCount(tableRow) < 1) 
			throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");
			
		int rowNum = util.getTableRowFromColumnData(tableHeader, tableRow, "COMMODITY", commodityInput, "./thead/tr/th", "./tbody/tr", "/td"); //div/span
		
		SpinnerManager spinManager = new SpinnerManager(driver);
		spinManager.checkSpinners();
		
		//System.out.println("Riga in tabella numero "+rowNum);
		
		List<WebElement> rowElement = util.waitAndGetElements(tableRow, spanCheckCommodity);
		rowElement.get(rowNum-1).click();
		
//		swichToDefault();
		
		//util.frameManager(frame, searchBtn, util.scrollAndClick);
		util.objectManager(searchBtn, util.scrollAndClick);
	}
	

	/**  MAAAAAAX
	 * Ricerca e Seleziona il POD passata nel parametro per il caso Copia Documenti.
	 * @param commodityInput
	 * @throws Exception
	 */
	public void setCommodityByValueForDoc(String columnName, String searchValue, int idxFrame,  int columnOffset) throws Exception
	{
		TimeUnit.SECONDS.sleep(5);
		String frame = "";
		if (idxFrame > -1)
		{
			frame = util.getFrameByIndex(idxFrame);
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		}
		
		List<WebElement> table = driver.findElements(tableHeaderBy);
		WebElement tableHeader = table.get(0);
		WebElement tableRow = null;
		if (tableRowBy == null)
		{
			tableRow = tableHeader;
		}
		else
		{
			tableRow = driver.findElement(tableRowBy);
		}
		

		tableHeader = util.waitAndGetElement(tableHeaderBy);
		util.scrollToElement(tableRow);
		if(util.getTableRowCount(tableRow) < 1) 
			throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

		int rowNum = util.getTableRowFromColumnData(tableHeader, columnName, searchValue, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset);
		SpinnerManager spinManager = new SpinnerManager(driver);
//		System.out.println("Riga in tabella numero "+rowNum);
		Logger.getLogger("").log(Level.INFO,"Riga in tabella numero "+rowNum);
		List<WebElement> rowElement = util.waitAndGetElements(tableRow, this.getSpanCommodity());
		int row = rowNum -1;
		rowElement.get(row).click();
		TimeUnit.SECONDS.sleep(5);
		
		spinManager.checkSpinners();
		//TimeUnit.SECONDS.sleep(15);


		//Ripristino Frame e click su Conferma Seleziona Fornitura
		if (idxFrame > -1)
		{
			swichToDefault();	
			spinManager.checkSpinners(frame);
			util.frameManager(frame, searchBtn, util.scrollAndClick);	
			spinManager.checkSpinners(frame);
		}
		else
		{
			util.objectManager(searchBtn, util.scrollAndClick);
			spinManager.checkSpinners();
		}
	}
	
	
	/**
	 * Ricerca e Seleziona la commodity identifica dai parametri passati.
	 * @param columnName  -> Nome della colanna della tabella in cui cercare 
	 * @param searchValue -> Valore da ricercare
	 * @param idxFrame    -> Indice del IFrame contenitore degli oggetti (-1 = No IFrame) 
	 * @param columnOffset  -> Offset da aggiungere al numero colonna
	 * @throws Exception
	 */
	public void setCommodityByValue(String columnName, String searchValue, int idxFrame,  int columnOffset) throws Exception
	{
		TimeUnit.SECONDS.sleep(5);
		String frame = "";
		if (idxFrame > -1)
		{
			frame = util.getFrameByIndex(idxFrame);
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		}
		
		List<WebElement> table = driver.findElements(tableHeaderBy);
		WebElement tableHeader = table.get(0);
		WebElement tableRow = null;
		if (tableRowBy == null)
		{
			tableRow = tableHeader;
		}
		else
		{
			tableRow = driver.findElement(tableRowBy);
		}
		

		tableHeader = util.waitAndGetElement(tableHeaderBy);
		util.scrollToElement(tableRow);
		if(util.getTableRowCount(tableRow) < 1) 
			throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

		int rowNum = util.getTableRowFromColumnData(tableHeader, columnName, searchValue, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset);
		SpinnerManager spinManager = new SpinnerManager(driver);
//		System.out.println("Riga in tabella numero "+rowNum);
		Logger.getLogger("").log(Level.INFO,"Riga in tabella numero "+rowNum);
		List<WebElement> rowElement = util.waitAndGetElements(tableRow, this.getSpanCommodity());
		int row = rowNum -1;
		rowElement.get(row).click();
		TimeUnit.SECONDS.sleep(5);
		//Verifica se il POD risulta non attivabile verificando il campo ATTIVABILITÀ
		try
		{
			util.getTableRowFromColumnData(tableHeader, "ATTIVABILITÀ", "Attivabile", "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset);
		}
		catch (Exception e)
		{
			throw new Exception("Impossibile selezionare la commodity, POD/PDR Non Attivabile!");
		}
		
		TimeUnit.SECONDS.sleep(5);
		
		spinManager.checkSpinners();
		//TimeUnit.SECONDS.sleep(15);


		//Ripristino Frame e click su Conferma Seleziona Fornitura
		if (idxFrame > -1)
		{
			swichToDefault();	
			spinManager.checkSpinners(frame);
			util.frameManager(frame, searchBtn, util.scrollAndClick);	
			spinManager.checkSpinners(frame);
		}
		else
		{
			util.objectManager(searchBtn, util.scrollAndClick);
			spinManager.checkSpinners();
		}
	}
	
	
	/**
	 * Ricerca e Seleziona la commodity identifica dai parametri passati.
	 * @param columnName  -> Nome della colanna della tabella in cui cercare 
	 * @param searchValue -> Valore da ricercare
	 * @param idxFrame    -> Indice del IFrame contenitore degli oggetti (-1 = No IFrame) 
	 * @param columnOffset  -> Offset da aggiungere al numero colonna
	 * @throws Exception
	 */
	public void setAllCommodity(int idxFrame,  int columnOffset) throws Exception
	{
		SpinnerManager spinManager = new SpinnerManager(driver);
		TimeUnit.SECONDS.sleep(5);
		String frame = "";
		if (idxFrame > -1)
		{
			frame = util.getFrameByIndex(idxFrame);
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		}
		
		List<WebElement> table = driver.findElements(tableHeaderBy);
		WebElement tableHeader = table.get(0);
		WebElement tableRow = null;
		if (tableRowBy == null)
		{
			tableRow = tableHeader;
		}
		else
		{
			tableRow = driver.findElement(tableRowBy);
		}
		

		tableHeader = util.waitAndGetElement(tableHeaderBy);
		util.scrollToElement(tableRow);
		if(util.getTableRowCount(tableRow) < 1) 
			throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

//		int rowNum = util.getTableRowFromColumnData(tableHeader, columnName, searchValue, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset);
		int rowNum =util.getTableRowCount(tableRow);
		
		Logger.getLogger("").log(Level.INFO,"Righe in tabella numero "+rowNum);
		List<WebElement> rowElement = util.waitAndGetElements(tableRow, this.getSpanCommodity());
		
		for (int idx=0; idx< rowNum; idx++)
		{
			//Aggiorno proprietà element ad ogni ciclo.
			rowElement = util.waitAndGetElements(tableRow, this.getSpanCommodity());
			rowElement.get(idx).click();
			spinManager.checkSpinners();
			//Verifica se il POD risulta non attivabile verificando il campo ATTIVABILITÀ
			int colNum = util.getTableRowFromColumnData(tableHeader, "ATTIVABILITÀ", "Non Attivabile", "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset, false);
			if (colNum > 0)
			{
				throw new Exception("Impossibile selezionare la commodity, POD/PDR Non Attivabile!");
			}
			
			TimeUnit.SECONDS.sleep(5);
//			spinManager.checkSpinners(frame);
			//TimeUnit.SECONDS.sleep(15);
		}

		//Ripristino Frame e click su Conferma Seleziona Fornitura
		if (idxFrame > -1)
		{
			swichToDefault();	
			spinManager.checkSpinners(frame);
			util.frameManager(frame, searchBtn, util.scrollAndClick);	
		}
		else
		{
			util.objectManager(searchBtn, util.scrollAndClick);	
		}
	}
	
	
	/**
	 * Ricerca e Seleziona la commodity identifica dai parametri passati.
	 * @param columnName  -> Nome della colanna della tabella in cui cercare 
	 * @param searchValue -> Valore da ricercare
	 * @param idxFrame    -> Indice del IFrame contenitore degli oggetti (-1 = No IFrame) 
	 * @throws Exception
	 */
	public void setCommodityByValue(String columnName, String searchValue, int idxFrame) throws Exception
	{
		setCommodityByValue(columnName, searchValue, idxFrame,  1);
	}
	
	public void clickAvanti(int idxFrame) throws Exception
	{
		String frame = util.getFrameByIndex(idxFrame);

		SpinnerManager spinManager = new SpinnerManager(driver);
		spinManager.checkSpinners(frame);

		util.frameManager(frame, By.xpath("//button[@id='nextDelegate_Button' and text()='Avanti']"), util.scrollAndClick);

		spinManager.checkSpinners(frame);

	}
	
	public void clickSelezionaReferente(int idxFrame) throws Exception
	{
		String frame = util.getFrameByIndex(idxFrame);

		SpinnerManager spinManager = new SpinnerManager(driver);
		spinManager.checkSpinners(frame);

		util.frameManager(frame, By.xpath("//button[@id='nextReferent_Button' and text()='Avanti']"), util.scrollAndClick);

		spinManager.checkSpinners(frame);

	}
	
	public void clickCreaOfferta(int idxFrame) throws Exception
	{
		String frame = util.getFrameByIndex(idxFrame);
		util.frameManager(frame, this.btnCreaOfferta, util.scrollAndClick);
	}
	
	public void clickCreaOfferta(int idxFrame, By by) throws Exception
	{
		String frame = util.getFrameByIndex(idxFrame);
		util.frameManager(frame, by, util.scrollAndClick);
	}
	
	public void clickCreaOfferta() throws Exception
	{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.btnCreaOfferta, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void clickCreaOfferta(By by) throws Exception
	{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, by, util.scrollAndClick);
	}
	
	/**
	 * Seleziona e COnfigura tutte le commodity identifiche dai parametri passati.
	 * @param idxFrame    -> Indice del IFrame contenitore degli oggetti (-1 = No IFrame) 
	 * @param columnOffset  -> Offset da aggiungere al numero colonna
	 * @throws Exception
	 */
	public void configureAllCommodity(Properties prop, int idxFrame,  int columnOffset) throws Exception
	{
		SpinnerManager spinManager = new SpinnerManager(driver);
		TimeUnit.SECONDS.sleep(5);
		String frame = "";
		if (idxFrame > -1)
		{
			frame = util.getFrameByIndex(idxFrame);
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		}
		
		List<WebElement> table = driver.findElements(tableHeaderBy);
		WebElement tableHeader = table.get(0);
		WebElement tableRow = null;
		if (tableRowBy == null)
		{
			tableRow = tableHeader;
		}
		else
		{
			tableRow = driver.findElement(tableRowBy);
		}
		

		tableHeader = util.waitAndGetElement(tableHeaderBy);
		util.scrollToElement(tableRow);
		if(util.getTableRowCount(tableRow) < 1) 
			throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

//		int rowNum = util.getTableRowFromColumnData(tableHeader, columnName, searchValue, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset);
		int rowNum =util.getTableRowCount(tableRow);
		
		Logger.getLogger("").log(Level.INFO,"Righe in tabella numero "+rowNum);
//		List<WebElement> rowElement = util.waitAndGetElements(tableRow, this.getSpanCommodity());
		List<WebElement> rowsCommodity;
		List<WebElement> elementCommodity;
		
		for (int idx=0; idx< rowNum; idx++)
		{
			//Aggiorno proprietà element ad ogni ciclo.
			tableRow = util.waitAndGetElement(tableHeaderBy);
			util.scrollToElement(tableRow);
//			rowElement = util.waitAndGetElements(tableRow, this.getSpanCommodity());
//			rowElement.get(idx).click();
//			spinManager.checkSpinners();
//			//Verifica se il POD risulta non attivabile verificando il campo ATTIVABILITÀ
//			int colNum = util.getTableRowFromColumnData(tableHeader, "ATTIVABILITÀ", "Non Attivabile", "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", columnOffset, false);
//			if (colNum > 0)
//			{
//				throw new Exception("Impossibile selezionare la commodity, POD/PDR Non Attivabile!");
//			}
			TimeUnit.SECONDS.sleep(5);
		    rowsCommodity = util.waitAndGetElements(tableRow, By.xpath("./tbody/tr")); 
			elementCommodity = util.waitAndGetElements(rowsCommodity.get(idx), By.xpath("./td"));//["+idx+"]/td[2]"));
			String tipoCommodity = elementCommodity.get(1).getText();
			if (tipoCommodity.equalsIgnoreCase("ELE"))
			{
				//Inserimento dati fornitura
				DatiFornituraComponent datiFornitura=new DatiFornituraComponent(driver);
				//Seleziono campo categoria merceologica SAP
				datiFornitura.setSelectCategoriaMerceologicaSAP(By.xpath("//label[contains(text(),'Categoria Merc SAP')]/../span/select"));
				util.objectManager(datiFornitura.selectCategoriaMerceologicaSAP, util.scrollAndSelect, prop.getProperty("CATEGORIA_MERCEOLOGICA_SAP"));
//				datiFornitura.selezionaCategoriaMercologiaSAP(prop.getProperty("interactionFrame"),datiFornitura.selectCategoriaMerceologicaSAP, prop.getProperty("CATEGORIA_MERCEOLOGICA_SAP"));

				// Codice Ateco
				datiFornitura.setSelectCodiceAteco(By.xpath("//label[contains(text(),'Codice Ateco')]/../span/span/select"));
				util.objectManager(datiFornitura.selectCodiceAteco, util.scrollAndSelect, prop.getProperty("CODICE_ATECO"));
//				datiFornitura.selezionaCodiceAteco(prop.getProperty("interactionFrame"),datiFornitura.selectCodiceAteco, prop.getProperty("CODICE_ATECO"));

				//Click su avanti
				datiFornitura.setAvantiButton(By.xpath("//button[@id='nextBillingData_Button']"));
				util.objectManager(datiFornitura.avantiButton, util.scrollAndClick);
//				datiFornitura.ClickButtonAvanti(prop.getProperty("interactionFrame"), datiFornitura.avantiButton);

				spinManager.checkSpinners();
			}
			else if (tipoCommodity.equalsIgnoreCase("GAS"))
			{
				//Inserimento dati fornitura
				DatiFornituraComponent datiFornitura=new DatiFornituraComponent(driver);
				//Seleziono campo categoria merceologica SAP
				datiFornitura.setSelectCategoriaMerceologicaSAP(By.xpath("//label[contains(text(),'Categoria Merc SAP')]/../span/select"));
				util.objectManager(datiFornitura.selectCategoriaMerceologicaSAP, util.scrollAndSelect, prop.getProperty("CATEGORIA_MERCEOLOGICA_SAP"));
//				datiFornitura.selezionaCategoriaMercologiaSAP(prop.getProperty("interactionFrame"),datiFornitura.selectCategoriaMerceologicaSAP, prop.getProperty("CATEGORIA_MERCEOLOGICA_SAP"));

				// Codice Ateco
				datiFornitura.setSelectCodiceAteco(By.xpath("//label[contains(text(),'Codice Ateco')]/../span/span/select"));
				util.objectManager(datiFornitura.selectCodiceAteco, util.scrollAndSelect, prop.getProperty("CODICE_ATECO"));
//				datiFornitura.selezionaCodiceAteco(prop.getProperty("interactionFrame"),datiFornitura.selectCodiceAteco, prop.getProperty("CODICE_ATECO"));

				if(prop.getProperty("TIPO_FATTURAZIONE","RESIDENZIALE").compareTo("BUSINESS")==0){
				//Seleziona Categoria COnsumo
				datiFornitura.setSelectCategoriaConsumo(By.xpath("//label[text()='Categoria di Consumo']/../div//select"));
				util.objectManager(datiFornitura.selectCategoriaConsumo, util.scrollAndSelect, prop.getProperty("CATEGORIA_CONSUMO"));
				spinManager.checkSpinners();
//				datiFornitura.selezionaCategoriaConsumo(prop.getProperty("interactionFrame"),datiFornitura.selectCategoriaConsumo, prop.getProperty("CATEGORIA_CONSUMO"));
				//Seleziona Profilo Consumo
				datiFornitura.setSelectProfiloConsumo(By.xpath("//label[text()='Profilo di Consumo']/../div//select"));
				util.objectManager(datiFornitura.selectProfiloConsumo, util.scrollAndSelect, prop.getProperty("PROFILO_CONSUMO"));
				spinManager.checkSpinners();
//				datiFornitura.selezionaPrimoProfiloConsumo(prop.getProperty("interactionFrame"),datiFornitura.selectProfiloConsumo, prop.getProperty("PROFILO_CONSUMO"));
//				//Seleziona Ordine Grandezza
				datiFornitura.setSelectOrdineGrandezza(By.xpath("//label[text()='Ordine di Grandezza']/../div//select"));
				util.objectManager(datiFornitura.selectOrdineGrandezza, util.scrollAndSelect, prop.getProperty("ORDINE_GRANDEZZA"));
				spinManager.checkSpinners();
////				datiFornitura.selezionaPrimoOrdineGrandezza(prop.getProperty("interactionFrame"),datiFornitura.selectOrdineGrandezza, prop.getProperty("ORDINE_GRANDEZZA"));
				}
				//Click su avanti
				datiFornitura.setAvantiButton(By.xpath("//button[@id='nextBillingData_Button']"));
				util.objectManager(datiFornitura.avantiButton, util.scrollAndClick);
//				datiFornitura.ClickButtonAvanti(prop.getProperty("interactionFrame"), datiFornitura.avantiButton);

				spinManager.checkSpinners();
			}
//			spinManager.checkSpinners(frame);
			//TimeUnit.SECONDS.sleep(15);
		}

		//Ripristino Frame e click su Conferma Seleziona Fornitura
		if (idxFrame > -1)
		{
			swichToDefault();	
//			util.frameManager(frame, searchBtn, util.click);	
		}
//		else
//		{
//			util.objectManager(searchBtn, util.click);	
//		}
	}

	public void setTableHeaderBy(By tableHeaderBy) {
		this.tableHeaderBy = tableHeaderBy;
	}

	public void setTableRowBy(By tableRowBy) {
		this.tableRowBy = tableRowBy;
	}

	public By getSpanCommodity() {
		return spanCommodity;
	}

	public void setSpanCommodity(By spanCommodity) {
		this.spanCommodity = spanCommodity;
	}


}
