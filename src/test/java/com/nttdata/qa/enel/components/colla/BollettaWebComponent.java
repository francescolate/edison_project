package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BollettaWebComponent extends BaseComponent{

	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTextBW = By.xpath("//p[contains(text(),'Il servizio bolletta Web consente')]");
	public By pageSubTextBW = By.xpath("//div[@id='bollWebAttivo']//p[contains(text(),'Il servizio Bolletta Web')]");
	public By addText = By.xpath("//div[@id='bollWebAttivo']//b[contains(text(),'Indirizzo della fornitura')]");
	public By clientNumberText= By.xpath("//b[contains(text(),'Numero cliente')]");
	
	public By attivaBolletaWeb = By.xpath("//div[@id='webNonAtt']/div[2]/div/div/a");
	public By supCheckbox = By.xpath("//span[@id='checkbox-item-0']");
	//public By supplyContinuaButton = By.xpath("//span[text()='CONTINUA']/parent::button[@class='button_first']");
	
	public By bollettaEmailInput = By.xpath("//label[@id='emailField']/following-sibling::div[@class='input-container']/child::input[@id='emailFieldInput']");
	public By bollettaConfermaEmail = By.xpath("//label[@id='emailFieldConfirm']/following-sibling::div[@class='input-container']/child::input[@id='emailFieldConfirmInput']");
	public By bollettaNumeroInput = By.xpath("label[@id='mobileField']/following-sibling::div[@class='input-container']/child::input[@id='mobileFieldInput']");
	public By bollettaNumeroConferma = By.xpath("//label[@id='mobileFieldConfirm']/following-sibling::div[@class='input-container']/child::input[@id='mobileFieldConfirmInput']");
	
	public By statusContinuaButton = By.xpath("//span[text()='CONTINUA']/parent::button[@class='button_first']");
	public By confirmButton = By.xpath("//span[text()='Conferma']/parent::button");
	public By fineButton = By.xpath("//span[text()='FINE']/parent::button[@class='button_first']");
	public By modificaIndirizzoEmailButton = By.xpath("//*[text()='MODIFICA INDIRIZZO EMAIL']");
	
	public By supplyActiveStatus = By.xpath("//b[text()='è attivo']/parent::p");

	public static final String PageTextBW = "Il servizio bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica tramite SMS.";
	public static final String PageSubTextBW = "Il servizio Bolletta Web è attivo sulla fornitura:";
	public static final String AddText = "Indirizzo della fornitura";
	public static final String ClientNumberText = "Numero cliente";
	
	
	public BollettaWebComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		//System.out.println("existingObject: "+existingObject);
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		//Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
    	System.out.println("By -> "+by);
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		System.out.println("without Normalized:\n"+weText);
		System.out.println("without Normalized we.getText():\n"+we.getText());
		if(nestedTags)
		{
			weText = normalizeInnerHTML(weText);
		System.out.println("Normalized:\n"+weText);
		}
		else
			weText = we.getText();
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
 
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}

}
