package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaBolletteComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;

	public By serviziMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Servizi']/parent::a");
	public By bolletteMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Bollette']/parent::a");
	public By bolletteHeader = By.xpath("//h1[text()='Le tue bollette']");
	public By bolletteHeaderText = By.xpath("//div[@class='heading']//p[contains(text(), 'Qui potrai')]");
	public By tilesHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public String orderedList = "//*[text()='$tileHeader$']/ancestor::section//li[contains(@class,'tile') and not(@style)]//h3/ancestor::a";
	public String tile = "//li[contains(@class,'tile')]//h3[text()='$tileName$']/ancestor::a";
	public By bollettaSintesi = By.xpath("//li[contains(@class,'tile')]//h3[text()='Bolletta di Sintesi o di Dettaglio']/ancestor::a");
	public By bollettaSintesiHeaderText = By.xpath("//div[@class='section-heading']");
	public By accountAddressLuce = By.xpath("//span[@class='icon-line-electricity']/../following-sibling::span[@class='indirizzo']/span[@class='valore']");
	public By customerNumberLuce = By.xpath("//span[@class='icon-line-electricity']/../following-sibling::span/span/span[@class='valore']");
	public String tipologiaBollettaButton = "//button/span[text()='$tipologiaBollettaButton$']";
	public By buttonEsci = By.xpath("//button[text()='ESCI']");
	public By buttonDettaglioFornitura = By.xpath("//span[@class='icon-line-flame']/ancestor::div[@class='details-container']//dd[text()='Attiva']/ancestor::div[@class='card forniture regolare']//a[contains(text(),'Dettaglio fornitura')]");
	public By buttonDettaglioFornituraUpdated = By.xpath("//span[@class='icon-line-flame']/ancestor::div[@class='details-container']//dd[text()='Attiva']/ancestor::div[@class='card forniture regolare']//a[contains(text(),'Gestisci fornitura')]");
	public By buttonDettaglioFornituraNew = By.xpath("//span[@class='icon-line-flame']/ancestor::div[@class='details-container']/ancestor::div[@class='card forniture regolare']//a[contains(text(),'Dettaglio fornitura')]");
	public By serviziPerLeBolletteHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public By serviziPerLeBolletteHeaderText = By.xpath("//h2[text()='Servizi per le bollette']/ancestor::div[@class='section-heading']");
	public By accountAddressGas = By.xpath("//span[@class='icon-line-flame']/../following-sibling::span[@class='indirizzo']/span[@class='valore']");
	public By customerNumberGas = By.xpath("//span[@class='icon-line-flame']/../following-sibling::span/span/span[@class='valore']");
	
    public PrivateAreaBolletteComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
//		System.out.println("Normalized:\n"+weText);
//		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text '"+text+ "' is not equal to the one within bolletteHeaderText '"+weText+ "'");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyMenuItemsPresenceAndOrder(String menuItemsString, String tileHeader) throws Exception {
		util.verifyVisibility(By.xpath("//*[@id='spinner'])"), 20, true);
		populateMenuItemsVector(menuItemsString);
		By orderedListXPath = By.xpath(this.orderedList.replace("$tileHeader$", tileHeader));
		List<WebElement> orderedWebElements = util.waitAndGetElements(orderedListXPath);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
		if(orderedWebElements.size()!=menuItemsList.size())
			throw new Exception("actual size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
		for(int i=0;i<orderedWebElements.size();i++){
			if(!orderedWebElements.get(i).equals(menuItemsList.get(i)))
					throw new Exception("the item you're looking for is different from the one in the list at position "+i+".");
		}
	}
	
	private void populateMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(tile.replace("$tileName$", item))));
	}
	
	public static final String BOLLETTE_HEADER = "Le tue bollette";
	public static final String BOLLETTE_HEADER_TEXT = "Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.";
	public static final String BOLLETTE_HEADER_TEXT_NEW = "Qui potrai visualizzare bollette e/o rate delle tue forniture.";
	public static final String BOLLETTE_HEADER_TEXT_GAS = "Servizi per le bollette Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette";
	public static final String SERVIZI_BOLLETTE = "Bolletta Web;Pagamento Online;Rettifica Bolletta;Rateizzazione;Bolletta di Sintesi o di Dettaglio;Invio Attestazione di Pagamento";
	public static final String TILE_HEADER = "Servizi per le bollette";
	public static final String BOLLETTA_SINTESI_HEADER_TEXT = "Modifica Tipologia Bolletta Scegli se ricevere la Bolletta di Sintesi dove troverai le principali voci di spesa ed i consumi oppure la Bolletta di Dettaglio dove sarà possibile approfondire le singole voci elementari di spesa. La modifica riguarderà solo la ricezione della bolletta via mail oppure cartacea se non hai attiva la bolletta web, mentre scaricando il PDF visualizzerai comunque la versione completa della tua bolletta, comprensiva di sintesi e dettaglio.";
}
