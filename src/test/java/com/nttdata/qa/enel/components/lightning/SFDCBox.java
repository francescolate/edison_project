package com.nttdata.qa.enel.components.lightning;

public interface SFDCBox {
    String sdfc_box_by_title_text = "//h2//*[text()='##']/ancestor::div[@role='main' and not(@class='split-right')]";
    String sdfc_box_by_title_text_2 = "//h2//*[text()='##']/ancestor::div[contains(@class , 'lightning-card')]";
    String sfdc_banner_error_by_button_x = "//button/ancestor::slot//*[@data-id='toast']";
    String inputCap = "//label[text()='##']/../input";
    //Common button by text
    String button_conferma = "//button[text()='Conferma']";
    String button_salva = "//button[text()='Salva']";
    String button_checkout = "//button[text()='Checkout']";
    String button_verifica = "//button[text()='Verifica']";
    String button_forza = "//button[text()='Forza Indirizzo']";
    String button_importa = "//button[text()='Importa']";
    String button_conferma_fornitura = "//button[text()='Conferma Fornitura']";
}
