package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ServiziComponent {
	

	WebDriver driver;
	SeleniumUtilities util;
	
	//public static final String HomePagePathSer = "AREA RISERVATA HOMEPAGE";
	public static final String HomePagePathSer = "Area riservataHomepage";
	public static final String HomePageTitleSer = "Benvenuto nella tua area dedicata Business";
	public By fotterSection = By.xpath("//footer//div[@class='footer-info-container']/ul/li");
	public By esci = By.xpath("//span[text()='ESCI']");
	public By comunicalettura = By.xpath("//span[text()='COMUNICA LETTURA']");
	public By informationX = By.xpath("//a[@id='iconLink']");
	public By serviceTitle = By.xpath("//div[@class='section-heading']//h2[text()='Servizi per le forniture']");
	public By AUTOLETTURA= By.xpath("//h3[text()='Autolettura']");
	public By headingautolettura = By.xpath("//h1[text()='Comunica la lettura del contatore']");
	public By subtitleautolettura = By.xpath("//h1[text()='Comunica la lettura del contatore']/following-sibling::h2");
	public By titleautolettura = By.xpath("//h1[text()='Comunica la lettura del contatore']/following-sibling::h4");
	public By Indirizzodellafornitura = By.xpath("//span[text()='Indirizzo della fornitura']");
	public By NumeroCliente = By.xpath("//span[text()='Numero Cliente']");
	public By linkStoricoLetture = By.xpath("//a[text()='Storico Letture']");

	
	public By Seleziona = By.xpath("//ol[@class='breadcrumb']");
	public By Prima = By.xpath("//*[@id='labelAtt']");
	public By nome = By.xpath("//span[@class='icon-info-circle']");
	
	public By homepageDetail1 = By.xpath("//div[@id='mainContentWrapper']//descendant::h1");
	public By homepageheadingdetail = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='In questa sezione potrai gestire le tue forniture']");
	public By homepageheadingdetail2 = By.xpath("//div[@id='mainContentWrapper']//descendant::h2[.='Le tue forniture']");
	public By homepageParagraph2 = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.']");
	public By homepageParagraphtext = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.']");
	public By iTuoiDiritti = By.xpath("//a[@id='iTuoiDiritti']/span");
	public By servicestext = By.xpath("//a[@id='servizi']//span[text()='Servizi']");
	//public static final String PAGEPATH = "AREA RISERVATA SERVIZI";
	public static final String PAGEPATH = "Area riservata Servizi";
	public static final String PAGETITLE = "Servizi";
	public static final String PAGETEXT = "In questa pagina trovi tutti i servizi.";
	
	//public static final String PagePathBW = "AREA RISERVATA SERVIZI BOLLETTA WEB";
	public static final String PagePathBW = "Area riservataServiziBolletta Web";
	public static final String PagePathBWFinal = "AREA RISERVATA BOLLETTA WEB";
	public static final String PageTitleBW = "Bolletta Web";
	public static final String PageTextBW = "E la bolletta diventa digitale";
	public static final String PageSubTitleBW = "Che cos'è ?";
	public static final String PageSubTextBW = "Bolletta Web è il servizio che ti permette di ricevere le tue bollette direttamente via mail: " 
	+"tu risparmi i costi di postalizzazione e l'ambiente ci guadagna. Durante l'attivazione ti chiederemo indirizzo mail e numero di cellulare,"
			+" nel caso non fossero già presenti nei nostri sistemi.";

	public static final String ACTIVATION = "Attivazione Vuoi attivare Bolletta Web sulle tue forniture?SCOPRI DI PIU";
	public static final String EDIT = "Modifica Vuoi modificare Bolletta Web?SCOPRI DI PIU";
	public static final String REVOCATION = "Modifica Vuoi modificare Bolletta Web?SCOPRI DI PIU";
	public By closePopUp = By.xpath("//div[@id='modalAlert']//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	

	public By popUp = By.xpath("//h3[@id='titleModal']");
	public By popUptext = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
	//public static final String PagePathSuppl = "AREA RISERVATAATTIVAZIONE BOLLETTA WEB";
	
	
	public static final String SELEZIONA = "Seleziona la fornitura attiva su cui effettuare l'autolettura.";
	public static final String PRIMA = "Prima di effettuare la lettura ti consigliamo di controllare lo storico.";
	
	public static final String INFORMATIONX = "Il servizio di autolettura consente di avere un quadro costantemente aggiornato dei propri consumi. L'autolettura consiste nel rilevare autonomamente i dati di consumo delle tue utenze gas e luce e comunicarcele.";
	public static final String PagePathSupply = "Area riservataAttivazione Bolletta Web";
	public static final String PageTitleSupply = "Attivazione Bolletta Web";
	public static final String PageTextSupply = "Ecco le forniture su cui stai per attivare Bolletta Web.";
	public static final String PageTextActive = "Completa i campi sottostanti e clicca sul tasto prosegui.";
	public static final String PageTextProgress = "Verifica i campi sottostanti e clicca su conferma.";
	public static final String PageTextConfirmBW = "Operazione eseguita correttamente. La tua richiesta è stata presa in carico. Clicca sul link presente nella email e nell'SMS che ti abbiamo inviato per completare la richiesta, qualora i contatti inseriti non risultino già confermati precedentemente. La mancata conferma del link comporta l'annullamento della richiesta. Segui l'avanzamento della tua richiesta nella sezione STATO RICHIESTE..";
	
	//public static final String PageTextCross = "Per la fornitura selezionata non è possibile avviare la richiesta perché ne risulta un'altra in corso" + "( sotto la lista di delle fonriture visualizzate )";
	public static final String PageTextCross = "Per la fornitura selezionata non è possibile avviare la richiesta perché ne risulta un'altra in corso";

	public final static String service = "//h3[text()='#']";
	
	public static final String textBollettaWeb = "Il servizio bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica tramite SMS.";
	
	public static final String servizioBollettaWeb = "Il servizio Bolletta Web non è attivo sulla fornitura:";
	
	public String stepsValue = "1I tuoi dati2Riepilogo e conferma dati3Esito";
	
	public By services = By.xpath("//li[@id='menuService']/a");	
	public By homePageLogoServizi = By.xpath("//img[@id='avatarUtente']");
	public By homePagePathServizi = By.xpath("//ol[@class='breadcrumb']");
	public By textBollettaWebBy = By.xpath("//*[@id='labelAtt']");
	//public By homePagePathServizi = By.xpath("//*[@id='monoForn']/div[1]/div/ol");
	public By servizioBollettaWebBy = By.xpath("//*[@id='webNonAtt']/div[1]/div/p");
	
	public By homePageTitleServizi = By.xpath("//section[@id='content-home']/div[1]/div/h1");
		
	public By pagePathServizi = By.xpath("//main[@id='main']/section/div/div/section/div[1]/div/ol");
	public By pageTitleServizi = By.xpath("//main[@id='main']/section/div/div/section/div[1]/div/h1");
	public By pagetextServizi = By.xpath("//main[@id='main']/section/div/div/section/div[2]/div[1]/div/h4");
	
	
	public By bollettaWeb = By.xpath("//p[contains(text(), 'Bolletta Web')]");
	public By bollettaWeb_v2 = By.xpath("//p[contains(text(), 'Bolletta Web')]//ancestor::a");
//	public By bollettaWeb = By.xpath("//main[@id='main']/section/div/div/section/div[2]/div[2]/ul/div[2]/a/div/div[1]/i");
	
//	public By pagePathBW = By.xpath("//ol[@class='breadcrumb']");
	//public By pageTitleBW = By.xpath("//section[@id='monoForn']/div[1]/div/h1");
	public By pageTitleBW = By.xpath("//h1[text()='Bolletta Web']");	
	//public By pageTextBW = By.xpath("//section[@id='multyForn']/div[1]/div/p");
	public By pageTextBW = By.xpath("//div[@class='ty-bollettaWeb parbase']//child::p");
	public By pageSubTitleBW = By.xpath("//section[@id='multyForn']/div[2]/div/div[1]/h2");
	public By pageSubTextBW = By.xpath("//section[@id='multyForn']/div[2]/div/div[2]");
	
	public By activation = By.xpath("//section[@id='multyForn']/div[3]/div/div/div/div/div/div/div/div[1]/div/table/tbody/tr[1]");
	public By edit = By.xpath("//section[@id='multyForn']/div[3]/div/div/div/div/div/div/div/div[1]/div/table/tbody/tr[2]");
	public By revocation = By.xpath("//section[@id='multyForn']/div[3]/div/div/div/div/div/div/div/div[1]/div/table/tbody/tr[3]");
	public By findMore = By.xpath("//section[@id='multyForn']/div[3]/div/div/div/div/div/div/div/div[1]/div/table/tbody/tr[1]/td[3]/a");
	
	public By checkBoxForSupply1 = By.xpath("//table/tbody/tr[1]/td[1]/div");
	public By checkBoxForSupply2 = By.xpath("//table/tbody/tr[3]/td[1]/div");
	public By checkBoxForSupply3 = By.xpath("//table/tbody/tr[5]/td[1]/div");
	public By checkBoxForSupply4 = By.xpath("//table/tbody/tr[7]/td[1]/div");
	public By checkBoxForSupply5 = By.xpath("//table/tbody/tr[9]/td[1]/div");
	public By avanti = By.xpath("//span[text()='Avanti']");
	
	public By crossForSupply1 = By.xpath("//table/tbody/tr[1]/td[2]/span");
	public By crossForSupply2 = By.xpath("//table/tbody/tr[3]/td[2]/span");
	public By crossForSupply3 = By.xpath("//table/tbody/tr[5]/td[2]/span");
	public By crossForSupply4 = By.xpath("//table/tbody/tr[7]/td[2]/span");
	public By crossForSupply5 = By.xpath("//table/tbody/tr[9]/td[2]/span");
	
	//public By checkbox1 = By.xpath("//span[@id='checkbox-item-0']");
	
	public By activeButton = By.xpath("//section[@id='sc']/div/div[2]/div/div/div/div/div[3]/a[2]");
	public By attivaBollettaWebButton = By.xpath("//*[@title='Attiva' and text()='Attiva Bolletta Web']");
	
//	public By pagePathSupply = By.xpath("//ol[@class='breadcrumb']");
	public By pageTitleSupply = By.xpath("//section[@id='sc']/div/div[1]/div/h1");
	public By pageTextSupply = By.xpath("//section[@id='sc']/div/div[1]/div/p");
	
	//public By bollettaEmailInput = By.xpath("//label[@id='emailField']/following-sibling::div[@class='input-container']/child::input[@id='emailFieldInput']");
	public By bollettaEmailInput = By.xpath("//*[text()='Email*']//following-sibling::input");
	//public By bollettaConfermaEmail = By.xpath("//label[@id='emailFieldConfirm']/following-sibling::div[@class='input-container']/child::input[@id='emailFieldConfirmInput']");
	public By bollettaConfermaEmail = By.xpath("//*[text()='Conferma email*']//following-sibling::input");
	public By bollettaNumeroInput = By.xpath("label[@id='mobileField']/following-sibling::div[@class='input-container']/child::input[@id='mobileFieldInput']");
	public By bollettaNumeroConferma = By.xpath("//label[@id='mobileFieldConfirm']/following-sibling::div[@class='input-container']/child::input[@id='mobileFieldConfirmInput']");
	public By cellulareInput = By.xpath("//*[text()='Numero di Cellulare*']//following-sibling::input");
	public By cellulareConferma = By.xpath("//*[text()='Conferma cellulare*']//following-sibling::input");
	
	//public By updateCheckbox = By.xpath("//input[@id='checkBoxAggiorna i dati anagrafici']");
	//public By updateCheckboxInput = By.xpath("//input[@id='checkBoxAggiorna i dati anagrafici']/following-sibling::label[contains(text(),'Aggiorna i miei')]");
	public By aggiornadati = By.xpath("//input[@id='aggiornadati']");
	
	public By supplyContinuaButton = By.xpath("//span[text()='CONTINUA']/parent::button[@class='button_first']");
	public By continuaButton = By.xpath("//span[text()='Continua']/parent::button[@class='button_first']");
	public By indietroButton = By.xpath("//span[text()='INDIETRO']/parent::button[@class='button_second']");
	public By proseguiButton = By.xpath("//button[text()='Prosegui']");
	
	//public By confirmButton = By.xpath("//span[text()='Conferma']/parent::button");
	public By confirmButton = By.xpath("//button[text()='Conferma']");
	//public By fineButton = By.xpath("//span[text()='FINE']/parent::button[@class='button_first']");
	public By fineButton = By.xpath("//a[text()='FINE']");
	
	public By crossIcon = By.xpath("");
	public By pageTextCross = By.xpath("//*[contains(text(),'Per la fornitura selezionata non è possibile avviare la richiesta')]");
	
	public By webNonAtt = By.xpath("//*[@id='webNonAtt']");
	
	public By operazioneNonRiuscita = By.xpath("//p[text()='Operazione non riuscita']");
	public By steps = By.xpath("//ul[@class='steps']");
	public By erroreGenerico = By.xpath("//p[@class='leadbig' and contains(text(),'Errore generico')]");
	public By esciButton = By.xpath("//a[@title='Esci' and @name='Esci']");
	public static final String HOMEPAGEDETAIL = "Benvenuto nella tua area privata";
	public static final String HOMEPAGEHEADINGDETAIL = "In questa sezione potrai gestire le tue forniture";
	public static final String HOMEPAGEHEADINGDETAIL2 = "Le tue forniture";
	public static final String HOMEPAGE_PARAGRAPH2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String HOMEPAGE_PARAGRAPHTEXT = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	
	//public By homepageDetail1 = By.xpath("//div[@id='mainContentWrapper']//descendant::h1");
	//public By homepageheadingdetail = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='In questa sezione potrai gestire le tue forniture']");
	//public By homepageheadingdetail2
	
	public static final String HEADING_autolettura = "Comunica la lettura del contatore";
	public static final String SUBTITLEautolettura = "Seleziona la fornitura attiva su cui effettuare l'autolettura.";
	public static final String TITLE_autolettura = "Prima di effettuare la lettura ti consigliamo di controllare lo storico.";

	public ServiziComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		System.out.println("existingObject: "+existingObject);
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		//Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	    
 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		System.out.println("without Normalized:\n"+weText);
		System.out.println("without Normalized we.getText():\n"+we.getText());
		if(nestedTags)
		{
			weText = normalizeInnerHTML(weText);
		System.out.println("Normalized:\n"+weText);
		}
		else
			weText = we.getText();
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
 
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	// to be deleted
	public void verifyCheckboxSelect(By CheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(CheckboxObject));
		
		String checkbox="NO";
		WebElement element = driver.findElement(CheckboxObject);
		element.getText();
		
		if (element.isSelected()){
		checkbox="YES";
		}	
		if (!element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is not selected");
		}
	}
	
	
public void verifyFooterVisibility() throws Exception {
		
		String[] footerValue = {"© Enel Energia S.p.a.","Tutti i Diritti Riservati","Gruppo IVA Enel P.IVA 15844561009","Informazioni Legali","Privacy","Credits","Contattaci"};
		List<WebElement> footer = driver.findElements(fotterSection);
		String footerText [] = new String[footer.size()];
		 
		for (int i = 0; i < footer.size(); i++) {
			
			footerText[i] = driver.findElement(By.xpath("(//footer//div[@class='footer-info-container']/ul/li)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
			
			if(!footerText[i].equalsIgnoreCase(footerValue[i])){
				throw new Exception("Footer values for "+i+" is not matching")	;		
				} 
		}
		
	}
	

	public void selectSupplyType(By checkBoxForSupply) {
		
		 WebElement option1 = driver.findElement(checkBoxForSupply);							

	        // This will Toggle the Check box 	
		 System.out.println("Checkbox option1 is: "+ option1);
		 option1.click();			

	        // Check whether the Check box is toggled on 		
	        if (option1.isSelected()) {					
	            System.out.println("Checkbox is Toggled On");					

	        } else {			
	            System.out.println("Checkbox is Toggled Off");					
	        }		
		
	}
	
	public void selectSupplyTypeList(List<By> checkBoxForSupply, int i) throws Exception {
		WebElement option1 = driver.findElement(checkBoxForSupply.get(i));
		System.out.println("option1 --> " + option1);		
		option1.click();
		while (option1.isSelected()) {		
			i=i+1;
			selectSupplyTypeList(listForniture(),i);
		}
	}
	/*
		for(int i=0;i<n;i++){
			WebElement option1 = driver.findElement(checkBoxForSupply.get(i));
	        // This will Toggle the Check box 	
			 System.out.println("Checkbox option1 is: "+ option1);			 
			 option1.click();
			 Thread.sleep(2000);
		     // Check whether the Check box is toggled on 		
		     if (option1.isSelected()) {					
		         System.out.println("Checkbox is Toggled On");
		         i=n;
		         break;
		     }
		}
		

	}
	*/
	
	public List<By> listForniture(){
		List<By> listForniture = new ArrayList<By>();
		listForniture.add(checkBoxForSupply1);
		listForniture.add(checkBoxForSupply2);
		listForniture.add(checkBoxForSupply3);
		listForniture.add(checkBoxForSupply4);	
		listForniture.add(checkBoxForSupply5);	
	return listForniture;
	}
	
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void sceltaServizio(String servizio) throws Exception{
		util.objectManager(By.xpath(service.replace("#", servizio)), util.click);
	}
	
	public void jsClickComponent(By by) throws Exception{
		util.jsClickElement(by);
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = normalizeInnerHTML(weText);		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}

}
