package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModConsensiACR162Component {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By homePageHeading1 = By.xpath("//div[@id='mainContentWrapper']//h1[contains(text(),'Benven')]");
	public By homePageParagraph1 = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'In questa')]");
	public By homePageHeading2 = By.xpath("//div[@id='mainContentWrapper']//h2[contains(text(),'Le tue forniture')]");
	public By homePageParagraph2 = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'Visualizza')]");
	public By servizii = By.xpath("//a[@id='servizi']");
	public By sectionHeading1 = By.xpath("//section[@id='forniture-id0']//h2[contains(text(),'Servizi per le forniture')]");
	public By sectionParagraph1 = By.xpath("//section[@id='forniture-id0']//p[contains(text(),'Di se')]");
	public By modificaContattieConsensi = By.xpath("//section[@id='forniture-id0']//h3[contains(text(),'Contatti')]");
	public By modificaSectionHeading = By.xpath("//h1[@id='profile-contact']");
	public By modificaSectionParagraph = By.xpath("//main/div[@id='mainContentWrapper']//p[contains(text(),'Consulta e aggio')]");
	public By modificaConsesiHeading = By.xpath("//div[@id='mainContentWrapper']//h2[contains(text(),'Consensi')]");
	public By modificaConsensiParagraph = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'Comunica')]");
	public By modificaConsensiButton = By.xpath("//div[@id='mainContentWrapper']//descendant::button[2]");
	public By datiRegHeading = By.xpath("//div[@id='mainContentWrapper']//h2[contains(text(),'Dati')]");
	public By datiRegParagraph = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'Visualizza')]");
	public By modificaDatiRegButton = By.xpath("//div[@id='mainContentWrapper']//descendant::button[3]");
	public By consensiHeading = By.xpath("//h1[@id='h1_consensi']");
	public By consensiText = By.xpath("//div[@id='sc']//div[@class='consensi-list-heading']");
	public By footerBeforeText = By.xpath("//footer[@id='footer']//li[contains(text(),'Enel')]");
	public By tuttiDiritti = By.xpath("//footer[@id='footer']//li[contains(text(),'Tutti')]");
	public By pIVA = By.xpath("//footer[@id='footer']//li[contains(text(),'P.IVA')]");
	public By pIVANew = By.xpath("//footer[@id='footer']//li[contains(text(),'P.IVA')]");
	public By informazionLegali = By.xpath("//footer[@id='footer']//li/a[contains(text(),'Informazioni Legali')]");
	public By privacy = By.xpath("//footer[@id='footer']//li/a[contains(text(),'Privacy')]");
	public By credits = By.xpath("//footer[@id='footer']//li/a[contains(text(),'Credits')]");
	public By contattacci = By.xpath("//footer[@id='footer']//li/a[contains(text(),'Contattaci')]");
	public By informativaSullaPrivacy = By.xpath("//a[@id='info-privacy']");
	public By informativaSullarPrivacyContent = By.xpath("//div[@id='modalAlert']/div");
	public By closeButton = By.xpath("//div[@id='modalAlert']//span[@class='dsc-icon-close-rounded']");
	public By numeroDiCellulare = By.xpath("//section[@class='consensi']//span[contains(text(),'Numero')]");
	public By cellulareCensensi1 = By.xpath("//div[@class='form-group-container simply-border'][1]//child::fieldset[@class='form-group inline-align'][1]/legend[@class='label-appearance']");
	public By cellulareConsensi1SIRadio = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By cellulareConsensi1NORadio = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By cellulareConsensi2 = By.xpath("//div[@class='form-group-container simply-border'][1]//child::fieldset[@class='form-group inline-align'][2]/legend[@class='label-appearance']");
	public By cellulareConsensi2SIRadio = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By cellulareConsensi2NORadio = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By telefonoFisso = By.xpath("//section[@class='consensi']//span[contains(text(),'Tele')]");
	public By telefonoFissoCensensi1 = By.xpath("//div[@class='form-group-container simply-border'][2]//child::fieldset[@class='form-group inline-align'][1]/legend[@class='label-appearance']");
	public By telefonoFissoConsensi1SIRadio = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By telefonoFissoConsensi1NORadio = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By telefonoFissoConsensi2 = By.xpath("//div[@class='form-group-container simply-border'][2]//child::fieldset[@class='form-group inline-align'][2]/legend[@class='label-appearance']");
	public By telefonoFissoConsensi2SIRadio = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By telefonoFissoConsensi2NORadio = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By email = By.xpath("//section[@class='consensi']//span[contains(text(),'Email')]");
	public By emailCensensi1 = By.xpath("//div[@class='form-group-container simply-border'][3]//child::fieldset[@class='form-group inline-align'][1]/legend[@class='label-appearance']");
	public By emailConsensi1SIRadio = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By emailConsensi1NORadio = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By emailConsensi2 = By.xpath("//div[@class='form-group-container simply-border'][3]//child::fieldset[@class='form-group inline-align'][2]/legend[@class='label-appearance']");
	public By emailConsensi2SIRadio = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By emailConsensi2NORadio = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By consensoProfilazione = By.xpath("//section[@class='consensi']/div[@class='form-group-container']/fieldset[@class='form-group inline-align']/legend[@class='label-appearance']");
	public By consensoProfilazioneSIRadio = By.xpath("//section[@class='consensi']/div[@class='form-group-container']/fieldset[@class='form-group inline-align']/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By consensoProfilazioneNORadio = By.xpath("//section[@class='consensi']/div[@class='form-group-container']/fieldset[@class='form-group inline-align']/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By header = By.xpath("//header[@id='globalHedaer']/div[@class='dotcom-header__main']");
	public By salvaModificheButton = By.xpath("//div[@id='sc']//button[@class='button_first']");
	public By headerLogo = By.xpath("//a[@class='brand href-sf-prevent-default']");
	public By headerUserLogo = By.xpath("//span[@id='welcomeBox']");
	
	public ModConsensiACR162Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
  
  public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
  
  public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
  
  public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
  
  public void rodioIsNotSelected(By existingObject) throws Exception {
	  	if(driver.findElement(existingObject).isSelected())
		  throw new Exception("object with xpath " + existingObject + " is selected.");
  }
  
  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void  isElementNotPresent(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			
			try{
				WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
			} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        		            
	            }
	         }
	 
	 public void  isRadioSelected(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			for(int i=1;i<=3;i++)
			{
			try{
				driver.findElement(By.xpath("//div[@class='form-group-container simply-border']["+ i +"]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']")).isSelected();
				i++;
				} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        		            
	         }
			}
	      }
	 
	 
public static final String HOME_PAGE_HEADING1 = "Benvenuto nella tua area privata";
public static final String HOME_PAGE_PARAGRAPH1 = "In questa sezione potrai gestire le tue forniture";
public static final String HOME_PAGE_HEADING2 = "Le tue forniture";
public static final String HOME_PAGE_PARAGRAPH2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
public static final String SECTION_PAGE_HEADING1 = "Servizi per le forniture";
public static final String SECTION_PAGE_PARAGPAH1 = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
public static final String MODIFICA_SECTION_HEADING = "Dati e Contatti";
public static final String MODIFICA_SECTION_PARAGRAPH = "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione";
public static final String MODIFICA_CONSENSI_HEADING = "Consensi";
public static final String MODIFICA_CONSENSI_PARAGRAPH = "Comunica o aggiorna i consensi a Enel Energia e ai suoi partner";
public static final String DATI_REG_HEADING = "Dati Registrazione";
public static final String DATI_REG_PARAGRAPH = "Visualizza o modifica le tue credenziali di accesso a enel.it";
public static final String CONSENSI_HEADING = "Consensi";
public static final String CONSENSI_TEXT = "ConsensiComunica o aggiorna i consensi a Enel Energia e ai suoi partnerLeggi l’Informativa sulla Privacy";
public static final String INFORMATIVA_CONTENT ="Informativa Privacy Chiudi Lei potrà liberamente decidere se acconsentire all’utilizzo dei suoi Dati Personali per finalità di marketing e profilazione. Il consenso per le predette finalità è facoltativo e non impedisce la possibilità di fruire dei prodotti o servizi offerti da Enel Energia. In particolare, lei potrà liberamente decidere se acconsentire all’utilizzo dei suoi dati di contatto:Consenso a Enel Energia - per il compimento da parte di Enel Energia S.p.A. di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing e per l’invio di materiale pubblicitario effettuato sia mediante modalità automatizzate di contatto (es. SMS, e-mail, telefonate senza operatore) che mediante modalità tradizionali di contatto (es. posta cartacea, telefonate con operatore) Consenso a Terzi - per le finalità descritte al punto precedente perseguite da società del Gruppo Enel, società controllanti, controllate, collegate o a partner commerciali di Enel Energia sia mediante modalità automatizzate di contatto (es. SMS, e-mail, telefonate senza operatore) che mediante modalità tradizionali di contatto (es. posta cartacea, telefonate con operatore) Consenso Profilazione - da parte di Enel Energia o altre Società del Gruppo Enel, società controllanti, controllate, collegate o partner commerciali di Enel Energia, per attività di profilazione basata sulle sue abitudini di consumo, sui suoi dati personali e le informazioni acquisite osservando l’utilizzo dei prodotti o servizi offerti, al fine di proporle offerte personalizzate Il consenso da lei eventualmente prestato al trattamento dei suoi dati personali per la predetta finalità di marketing effettuato mediante l’utilizzo di sistemi automatizzati di contatto (Es. e-mail), si intenderà riferito anche all’utilizzo delle modalità tradizionali di contatto (es. Telefonata con operatore). Parimenti, l’eventuale diritto di opposizione al trattamento dei suoi dati personali per la medesima finalità di marketing, effettuato mediante l’utilizzo di sistemi automatizzati di contatto, si estenderà anche all’utilizzo di modalità tradizionali di contatto, fatta salva la possibilità, a lei riconosciuta, di esercitare tale diritto solo in parte.";
public static final String NUMERO_DI_CELLULARE = "Numero di Cellulare";
public static final String TELEFONO_FISSO = "Telefono Fisso";
public static final String EMAIL = "Email";
public static final String CONSENSO_PROFILAZIONE = "Consenso profilazione";
}
