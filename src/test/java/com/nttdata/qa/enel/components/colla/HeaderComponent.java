package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class HeaderComponent {
	    WebDriver driver;
	    SeleniumUtilities util;
	    public By luceAndGasTab=By.xpath("//li//a[text()='Luce e gas']");
	    public By impreseTab=By.xpath("//li//a[text()='Imprese']");
	    public By insiemeateTab=By.xpath("//li//a[text()='Insieme a te']");
	    public By storieTab=By.xpath("//li//a[text()='Storie']");
	    public By futureTab=By.xpath("//li//a[text()='Futur-e']");
	    public By mediaTab=By.xpath("//li//a[text()='Media']");
	    public By supportoTab=By.xpath("//li//a[text()='Supporto']");
	    public By magnifyingglassIcon=By.xpath("//span[@class='icon-search-small']");
	    public By iconUser = By.xpath("//span[@class='icon-user']");
	    
	    public HeaderComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	    
	    public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}

		public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponentIfExist(By existObject) throws Exception {
			if (util.exists(existObject, 15)) {
				util.objectManager(existObject, util.scrollToVisibility, false);
			    util.objectManager(existObject, util.scrollAndClick);
			}
		}
		
		public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}
		
		public void checkHeaderVisibility() throws Exception {
			By[] locators = {
					luceAndGasTab,
					impreseTab,
//					insiemeateTab,
					storieTab,
					futureTab,
					mediaTab,
					supportoTab,
					magnifyingglassIcon,
					iconUser
			};
			for (int i = 0; i < locators.length; i++) {
				verifyComponentVisibility(locators[i]);
			}
		}
}
