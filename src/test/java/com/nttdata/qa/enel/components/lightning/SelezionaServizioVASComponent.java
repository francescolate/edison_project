package com.nttdata.qa.enel.components.lightning;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

import io.qameta.allure.Step;


/**
 * @author ChianteseRo
 * Seleziona prima il servizio VAS e poi la commodity su cui attivare
 */
public class SelezionaServizioVASComponent {

	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinner;
	private String interactionFrame;
	public By titoloPopup=By.xpath("//div[@class='slds-modal__container slds-modal--prompt']//h2[text()='Checklist']");
	public By testopopup1=By.xpath("//div[@class='scriptDiv']//span//p[1]");
	public String text="ATTENZIONE PRIMA DI PROCEDERE VERIFICARE  1. Il richiedente è in possesso dei dati necessari per l’inserimento della richiesta:    -    Identificativi delle forniture\\Email\\PEC\\Cellulare.  INFORMAZIONI UTILI   1. In caso di certificazione obbligatoria, informare il cliente che dovrà certificare i suoi dati, cliccando sul link che invieremo tramite mail e sms; 2. In caso di selezione multipla, le forniture dovranno avere stessa mail e cellulare.";
	
	
	public SelezionaServizioVASComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(this.driver);
		this.spinner = new SpinnerManager(driver);
	}

	final By selectServizioVas = new By.ById("j_id0:formId:vasValue");

	private By confermaBtn = By.xpath("//div[@id='popup_document']//button[contains(text(), 'Conferma')]");
	
	public void fillServizioVas(String servizioVas) throws Exception{
		util.objectManager(selectServizioVas, util.select, servizioVas);
		//spinner.waitForSpinnerByStyleDiplayNone(interactionFrame);
		spinner.checkSpinners();
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(confermaBtn, util.scrollAndClick);
	}
	
	public void fillServizioVas2(String servizioVas) throws Exception{
		util.getFrameActive();
		util.objectManager(selectServizioVas, util.select, servizioVas);
//		spinner.checkSpinners();
		TimeUnit.SECONDS.sleep(5);
		verificaTestoPopup(titoloPopup,testopopup1, text);
		util.objectManager(confermaBtn, util.scrollAndClick);
//		driver.switchTo().defaultContent();
	}
	
	public void verificaTestoPopup (By titolo,By object,String testo) throws Exception {
		if (!util.exists(titoloPopup, 15)) throw new Exception("l'oggetto web con xpath " + titoloPopup + " non esiste.");
		String text=util.waitAndGetElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
	//	System.out.println(text);
		if (!text.contentEquals(testo)) throw new Exception("la popup non contiene il testo con descrizione "+testo);
	
	}
}
