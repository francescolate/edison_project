package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class InfoEnelEnergiaACRComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By homepagePath1 = By.xpath("//section[@id='content-home']//descendant::li[1]/a[.='Area riservata']");
	public By homepagePath2 = By.xpath("//section[@id='content-home']//descendant::li[.='Homepage']");
	public By homepageLogo = By.xpath("//img[@id='avatarUtente']");
	public By homepageTitle = By.xpath("//section[@id='content-home']//h1[contains(text(),'Benvenuto')]");
	public By servizi = By.xpath("//li[@id='menuService']/a[.='servizi']");
	public By serviziLeftMenu = By.xpath("//a[@id='servizi']");
	public By serviziHomepagePath1 = By.xpath("//main[@id='main']//descendant::li[1][.='Area riservata']");
	public By serviziHomepagePath2 = By.xpath("//main[@id='main']//descendant::li[.='Servizi']");
	public By fornitureHomepagePath2 = By.xpath("//main[@id='main']//descendant::li[.='Forniture']");
	public By serviziTitle = By.xpath("//main[@id='main']//descendant::h1[text()='Servizi']");
	public By serviziText = By.xpath("//main[@id='main']//descendant::h4[contains(text(),'In questa')]");
	public By infoEnelEnergia = By.xpath("//p[contains(text(),'InfoEnelEnergia')]");
	public By infoEnelEnergiaNew = By.xpath("//div/p[contains(text(),'InfoEnelEnergia')]/ancestor::a");
	public By infoEnelEnergiaTitle = By.xpath("//section[@id='active']//descendant::h1[.='Info Enel Energia']");
	public By infoEnelEnergiaTitleNew = By.xpath("(//h1[.='Info Enel Energia'])[1]");
	public By infoEnelEnergiaText = By.xpath("//section[@id='active']//descendant::p[contains(text(),'Per essere')]");
	public By infoEnelEnergiaTextNew = By.xpath("(//h1[.='Info Enel Energia'])[1]/following-sibling::p");
	public By clientNumber = By.xpath("//section[@id='active']//div[@id='headingOne']/h4[@class='panel-title']/b");
	public By notificheText = By.xpath("//section[@id='active']//descendant::h5[contains(text(),'Indirizzo mail')]");
	public By email = By.xpath("//table[@class='table']/thead/tr/th[1]");
	public By cellulare = By.xpath("//table[@class='table']/thead/tr/th[2]");
	public By modifica = By.xpath("//table[@class='table']/tbody/tr[1]/td[1]/b[.='Modifica']");
	public By modificaText = By.xpath("//table[@class='table']/tbody/tr[1]/td[contains(text(),'Se cambi email')]");
	public By scopriDiPIUModifica = By.xpath("//table[@class='table']/tbody/tr[1]/td[@class='text-right']");
	public By revoca = By.xpath("//table[@class='table']/tbody/tr[2]/td[1]/b[.='Revoca']");
	public By revocaText = By.xpath("//tbody/tr[2]/td[contains(text(),'Sei sicuro')]");
	public By scopriDiPIURevoca = By.xpath("//table[@class='table']/tbody/tr[2]/td[@class='text-right']");
	public By homepageHeadingRES = By.xpath("//div[@id='mainContentWrapper']//descendant::h1[contains(text(),'Benvenuto')]");
	public By homepageParagraphRES = By.xpath("//div[@id='mainContentWrapper']//descendant::p[contains(text(),'In questa')]");
	public By homepageSectionHeadingRES = By.xpath("//section[@id='lista-forniture']//descendant::h2[contains(text(),'Le')]");
	public By homepageSectionParagraphRES =  By.xpath("//section[@id='lista-forniture']//p[contains(text(),'Visualizza')]");
	public By dettaglioFornitura = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][11]//a[2]");
	public By dettaglioFornitura263 = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][4]/div[@class='button-container']/a[2]");
	public By dettaglioFornitura259 = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div[@class='button-container']/a[2]");
	public By dettaglioFornituracat259 = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][7]/div[@class='button-container']/a[2]");
	public By dettaglioFornitura267 = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare']/div[@class='button-container']/a[2]");
	public By gestisciLeOreFree = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div[@class='button-container']/a[2]");
	public By autolettura = By.xpath("//h3[text()='Autolettura']");
	public By infoEnelEnergiaTile = By.xpath("//li[@class='tile']//h3[text()='InfoEnelEnergia']");
	public By modificaIndirizzoFatturazione = By.xpath("//h3[text()='Modifica Indirizzo Fatturazione']");
	public By modificaPotenzaeoTensione = By.xpath("//h3[text()='Modifica Potenza e/o Tensione']");
	public By modificaContattieConsensi = By.xpath("//li[@class='tile']//h3[text()='Modifica Contatti e Consensi']");
	public By serviziPerLeFornitura = By.xpath("//section[@id='forniture-id0']//descendant::h2[contains(text(),'Servizi')]");
	public By infoEnelEnergiaHeadingRES = By.xpath("//section[@id='landing-text']//descendant::h2[.='InfoEnelEnergia']");
	public By infoEnelEnergiaParagraphRES = By.xpath("//section[@id='landing-text']//p[contains(text(),'Il servizio InfoEnelEnergia ti permette ')]");
	public By ieenonAttivoParagraph = By.xpath("//section[@id='landing-text']/div[@class='ingaggio-info-energia parbase']//div[1]/p");
	public By attivaInfoEnelEnergiaButton = By.xpath("//section[@id='landing-text']//descendant::span[.='ATTIVA INFOENELENERGIA']");
	public By esciButton = By.xpath("//div[@id='landing-dispositiva']/button[.='ESCI']");
	public By attivaInfoEnelEnergiaHeading  = By.xpath("//h1[@id='titolo-dispositiva']");
	public By attivaInfoEnelEnergiaHeading2 = By.xpath("//div[@id='sc']//descendant::h2[contains(text(),'Il servizio InfoEnelEnergia ti ')]");
	public By esciButtonAttivaIEE = By.xpath("//div[@id='sc']//descendant::span[.='Esci']");
	public By nonAttivoSupplyCheckBox = By.xpath("(//span[contains(@id,'checkbox-item-')])[1]");
	public By continuaButton = By.xpath("//div[@id='sc']//span[.='CONTINUA']");
	public By attivaInfoEnelEnergiaParagraph = By.xpath("//div[@id='sc']//descendant::h2[contains(text(),'Stai effettuando')]");
	public By inserminatoDati = By.xpath("//div[@id='sc']//descendant::span[.='Inserimento dati']");
	public By selezionaText = By.xpath("//div[@id='infoenelenergia']/h4[.='Seleziona la tipologia di avviso']");
	public By comunicaLettura = By.xpath("//div[@id='checkboxGroup0']//descendant::span[.='Comunica Lettura']");
	public By smsCL = By.xpath("//input[@id='shallReadSMS']/parent::div");
	public By emailCL = By.xpath("//input[@id='shallReadEmail']");
	public By emissioneBolletta =  By.xpath("//div[@id='checkboxGroup1']//span[.='Emissione Bolletta']");
	public By smsEB = By.xpath("//input[@id='issueBillSMS']");
	public By emailEB = By.xpath("//input[@id='issueBillEmail']/parent::div");
	public By AvvenutoPagamento = By.xpath("//div[@id='checkboxGroup2']//span[.='Avvenuto Pagamento']");
	public By smsAP = By.xpath("//input[@id='paymentNoticeSMS']/parent::div");
	public By emailAP = By.xpath("//input[@id='paymentNoticeEmail']");
	public By scadenzaProssimaBolletta = By.xpath("//div[@id='checkboxGroup3']//span[.='Scadenza Prossima Bolletta']");
	public By smsSPB = By.xpath("//input[@id='nextDeadlineSMS']");
	public By emailSPB = By.xpath("//input[@id='nextDeadlineEmail']/parent::div");
	public By sollecitoPagamento = By.xpath("//div[@id='checkboxGroup4']//span[.='Sollecito Pagamento']");
	public By smsSP = By.xpath("//input[@id='requestPaymentSMS']/parent::div");
	public By emailSP = By.xpath("//input[@id='requestPaymentEmail']");
	public By avvisoConsumi = By.xpath("//div[@id='checkboxGroup5']//span[.='Avviso Consumi']");
	public By smsAC = By.xpath("//input[@id='consumptionsSMS']");
	public By emailAC = By.xpath("//input[@id='consumptionsEmail']/parent::div");
	public By InserisciiTuoiDati = By.xpath("//div[@id='infoenelenergia']//h4[.='Inserisci i tuoi dati']");
	public By emailInserisciiTuoiDatiLabel = By.xpath("//label[@id='emailField']");
	public By EmailInserisciiTuoiDatiInput = By.xpath("//input[@id='emailFieldInput']");
	public By ConfirmEmailInserisciiTuoiDatiInput = By.xpath("//input[@id='emailFieldConfirmInput']");
	public By cellulareInserisciiTuoiDatiLabel = By.xpath("//label[@id='mobileField']");
	public By cellulareInserisciiTuoiDatiInput = By.xpath("//input[@id='mobileFieldInput']");
	public By ConfermaCellulareInserisciiTuoiDatiInput = By.xpath("//input[@id='mobileFieldConfirmInput']");
	public By aggiornaDatiAnagrafici = By.xpath("//input[@id ='checkBoxAggiorna i dati anagrafici']/parent::div");
	public By continuaIEEButton = By.xpath("//div[@id='sc']//descendant::button[.='Continua']");
	public By errorPageHeading = By.xpath("//div[@id='mainContentWrapper']//descendant::h1");
	public By errorPageHeadingBSN = By.xpath("//main[@id='main']//h1");
	public By reipilogoeConfermaDati = By.xpath("//div[@id='sc']//descendant::span[.='Riepilogo e conferma dati']");
	public By tipologiaDiAvviso = By.xpath("//div[@id='sc']//descendant::h4[.='Tipologia di avviso']");
	public By comunicaLetturaRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Comunica Lettura']");
	public By SoloSMS = By.xpath("//div[@id='sc']//descendant::dd[.='Solo SMS']");
	public By SoloEMail = By.xpath("//div[@id='sc']//descendant::dd[.='Solo Email']");
	public By emissioneBollettaRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Emissione Bolletta']");
	public By avvenutoPagamentoRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Avvenuto Pagamento']");
	public By prossimaScadenzoRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Prossima Scadenza']");
	public By sollecitoPagamentoRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Sollecito Pagamento']");
	public By avvisoConsumiRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Avviso Consumi']");
	public By iTuoiDati = By.xpath("//div[@id='sc']//descendant::h4[.='I tuoi dati']");
	public By emailRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Email']");
	public By emailValueRCD = By.xpath("//div[@id='sc']//descendant::dd[.='catasta@yopmail.com']");
	public By cellulareRCD = By.xpath("//div[@id='sc']//descendant::dt[.='Cellulare']");
	public By cellulareValueRCD = By.xpath("//div[@id='sc']//descendant::dd[.='3459089078']");
	public By IEEAttivoSu = By.xpath("//div[@id='sc']//h4[.='InfoEnelEnergia verrà attivato su:']");
	public By indirizzoDellaFornitura = By.xpath("//div[@id='sc']//div[@class='right-main']/div[2]//span[@class='indirizzo']/span[@class='label']");
	public By numeroCliente = By.xpath("//section[@id='landing-text']/div[@id='sc']//div/span[@class='flex']/span[@class='cliente']/span[@class='label']");
	public By supplyCheckBox = By.xpath("//span[@id='checkbox-item-0']");
	public By confermaButton = By.xpath("//div[@id='sc']//button[.='Conferma']");
	public By esito = By.xpath("//div[@id='sc']//span[.='Esito']");
	public By esitoText = By.xpath("//div[@id='sc']//descendant::p[contains(text(),'La tua')]");
	public By proseguiButtonErr = By.xpath("//section[@id='nonInscritto']//a[@class='btn btn-primary']");
	public By OperazioneTitle = By.xpath("//h3[@id='id_title']");
	public By backButton = By.xpath("//button[text()='INDIETRO']");
	public By checkBox = By.xpath("//span[starts-with(@id,'checkbox-item-')]");
	public By finebutton = By.xpath("//div[@id='sc']//button[.='Fine']");
	public By ConfermaEmailInserisciiTuoiDatiInput = By.xpath("//input[@id='emailFieldConfirmInput']");
	public By Ilservizioverr = By.xpath("//h4[text()='Il servizio verrà disattivato sulla fornitura:']");
	public By esciLeftMenu = By.xpath("//a[@id = 'tr_disconnetti']");
	public By esciLeftMenuNew = By.xpath("//a[@id = 'disconnetti']");
	
	//339
	public By forniture  = By.xpath("//li[@id='menuForn']/a[.='forniture']");
	public By mostraFiltri  = By.xpath("//a[@name='Filtri']");
	public By mostraFiltriNew  = By.xpath("//div[@class='row row-btns row-filter']/div/a/span");
	public By numeroCliente339  = By.xpath("//label[text()='Numero cliente']/following-sibling::div/div/div/div/input");
	public By numeroCliente339New  = By.xpath("//label[text()='Numero cliente']/following-sibling::div/div/a/span");
	public By numeroCliente310492564  = By.xpath("//label[text()='Numero cliente']/following-sibling::div/div/div/ul");
	public By numeroCliente310492564New  = By.xpath("//label[text()='Numero cliente']/following-sibling::div/div/div/ul/li/label/span[text()='310492564']");
	public By buttonApplica  = By.xpath("//button[text()='Applica']");
	public By vaiAlDetataglio  = By.xpath("//a[@name='310492564']");
	public By modificaTittle = By.xpath("//h1[text()='Modifica Addebito Diretto']");
	public By modificaMultifornitura = By.xpath("//span[text()='Multifornitura']");
	public By modificaItuoidati = By.xpath("//span[text()='I tuoi dati']");
	public By modificaItuoidatiNew = By.xpath("//span[text()='I Tuoi Dati']");
	public By modificaRiepilogoeconfermadati = By.xpath("//span[text()='Riepilogo e conferma dati']");
	public By modificaRiepilogoeconfermadatiNew = By.xpath("//span[text()='Riepilogo e Conferma Dati']");
	public By modificaMultiEsito = By.xpath("//span[text()='Esito']");
	public By serviziHomepagePath1New = By.xpath("//main[@id='main']//descendant::li[1][.='AREA RISERVATA']");
	public By fornitureHomepagePath2New = By.xpath("//main[@id='main']//descendant::li[.='FORNITURE']");
	public By modificaTittleNew = By.xpath("//main[@id='main']//descendant::li[.='MODIFICA ADDEBITO DIRETTO']");
	public By inserisciText = By.xpath("//p[text()='Inserisci la tua mail per ricevere aggiornamenti sulla richiesta.']");
	
	public InfoEnelEnergiaACRComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponent(By clickObject) throws Exception {
			util.objectManager(clickObject, util.scrollToVisibility, false);
			util.objectManager(clickObject, util.scrollAndClick);
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkForSupplyServices(String property) throws Exception {
			if(! property.contains(driver.findElement(autolettura).getText()) && property.contains(driver.findElement(infoEnelEnergiaTile).getText()) && property.contains(driver.findElement(modificaIndirizzoFatturazione).getText())
					&& property.contains(driver.findElement(modificaPotenzaeoTensione).getText()) && property.contains(driver.findElement(modificaContattieConsensi).getText()))
				throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
		}
		
		public void isButtonEnabled(By existentObject) throws Exception{
			if (!driver.findElement(existentObject).isEnabled())
				throw new Exception("The button is Enabled");
				}
		
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}

		public void enterLoginParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
				
		public static final String HOMEPAGE_PATH1= "Area riservata";
		public static final String HOMEPAGE_PATH1New= "AREA RISERVATA";
		public static final String HOMEPAGE_PATH2= "Homepage";
		public static final String HOMEPAGE_SERVIZI_PATH2= "Servizi";
		public static final String HOMEPAGE_FORNITURE_PATH2= "Forniture";
		public static final String HOMEPAGE_FORNITURE_PATH2New= "FORNITURE";
		public static final String MODIFICA_TITTLE = "Modifica Addebito Diretto";
		public static final String ModificaMultifornitura= "Multifornitura";
		public static final String ModificaItuoidati= "I tuoi dati";
		public static final String ModificaItuoidatiNew= "I Tuoi Dati";
		public static final String ModificaRiepilogoeconfermadati= "Riepilogo e conferma dati";
		public static final String ModificaRiepilogoeconfermadatiNew= "Riepilogo e Conferma Dati";
		public static final String ModificaMultiEsito= "Esito";
		public static final String HOMEPAGE_TITLE= "Benvenuto nella tua area dedicata Business";
		public static final String SERVIZI_TITLE = "Servizi";
		public static final String SERVIZI_TEXT = "In questa pagina trovi tutti i servizi.";
		public static final String INFO_ENEL_ENERGIA_TITLE = "Info Enel Energia";
		public static final String INFO_ENEL_ENERGIA_TEXT = "Per essere sempre informato sulla tua fornitura.Su questa fornitura, Info Enel Energia non è ancora attivo.";
		public static final String NOTIFICHE_TEXT = "Indirizzo mail e cellulare sui cui ricevi le notifiche";
		public static final String MODIFICA = "Modifica";
		public static final String MODIFICA_TEXT= "Se cambi email o numero di cellulare, aggiorna i tuoi dati.";
		public static final String REVOCA = "Revoca";
		public static final String REVOCA_TEXT = "Sei sicuro di voler rinunciare alla comodità delle notifiche di Info Enel Energia?";
		public static final String HOMEPAGE_HEADING_RES = "Benvenuto nella tua area privata";
		public static final String HOMEPAGE_PARAGRAPH_RES = "In questa sezione potrai gestire le tue forniture";
		public static final String HOMEPAGE_SECTION_HEADING_RES = "Le tue forniture";
		public static final String HOMEPAGE_SECTION_PARAGRAPH_RES = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
		public static final String SUPPLY_SERVICES = "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ";
		public static final String SERVIZI_PER_LE_FORNITURE ="Servizi per le forniture";
		public static final String INFOENELENERGIA_HEADING_RES ="InfoEnelEnergia";
		public static final String INFOENELENERGIA_PARAGRAPH_RES = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
		public static final String INFOENELENERGIA_PARAGRAPH_NONATTIVO ="Il servizio InfoEnelEnergia non è attivo sulla fornitura:";
		public static final String ATTIVA_IEE_HEADING1 ="Attiva InfoEnelEnergia";
		public static final String ATTIVA_IEE_HEADING2 ="Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
		public static final String ATTIVA_IEE_PARAGRAPH = "Stai effettuando l'attivazione del servizio di InfoEnelEnergia";
		public static final String INSERIMENTO_DATI = "Inserimento dati";
		public static final String SELEZIONA_TIPOLOGIA = "Seleziona la tipologia di avviso";
		public static final String COMUNICA_LETTURA = "Comunica Lettura";
		public static final String EMISSIONE_BOLLETTA = "Emissione Bolletta";
		public static final String AVVENUTO_PAGAMENTO = "Avvenuto Pagamento";
		public static final String SCADENZA_PROSSIMA_BOLLETTA= "Scadenza Prossima Bolletta";
		public static final String PROSSIMA_SCADENZA = "Prossima Scadenza";
		public static final String SOLLECITO_PAGAMENTO = "Sollecito Pagamento";
		public static final String AVVISO_CONSUMI = "Avviso Consumi";
		public static final String INSERISCI_TUAI_DATI = "Inserisci i tuoi dati";
		public static final String ERROR_PAGE_HEADING = "Errore nella richiesta";
		public static final String REIPILOGO_CONFERMA_DATI = "Riepilogo e conferma dati";
		public static final String TIPOLOGIA_DI_AVVISO = "Tipologia di avviso";
		public static final String I_TUOI_DATI = "I tuoi dati";
		public static final String EMAIL_RCD= "Email";
		public static final String EMAIL_VALUE_RCD_263 ="collaudoloyaltycolazz.o202.0@gmail.com";
		public static final String CELLULARE_RCD ="Cellulare";
		public static final String CELLULARE_INPUT_RCD ="3459089078";
		public static final String IEE_ATTIVO_SU = "InfoEnelEnergia verrà attivato su:";
		public static final String INDIRIZZO_DELLA_FORNITURA = "Indirizzo della fornitura";
		public static final String NUMERO_CLIENTE = "Numero cliente";
		public static final String ESITO ="Esito";
		public static final String ESITO_TEXT ="La tua richiesta è stata accolta. Per completare l'attivazione del servizio clicca sul link dell'SMS e/o della mail che ti abbiamo inviato.";
		public static final String ESITO_TEXT_259 ="La tua richiesta è stata presa in carico, il servizio sarà attivo a breve.";
		public static final String OPERAZIONE_HEADER = "Operazione eseguita correttamente";
		public static final String IlservizioverrText = "Il servizio verrà disattivato sulla fornitura:";
		public static final String MODIFICA_TITTLENEW = "MODIFICA ADDEBITO DIRETTO";
		public static final String InserisciText = "Inserisci la tua mail per ricevere aggiornamenti sulla richiesta.";
		
}

