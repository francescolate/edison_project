package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CaricaDoccumentiComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By path = By.xpath("//section[@class='content']//ol");
	public By pageTitle = By.xpath("//h1[contains(text(),'In questa')]");
	public String caricaDoccumentiUrl = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=EM&azione=CONT";
	public String caricaDoccumentiUrlNew = "https://www-coll1.enel.it/it/servizi-online/carica-documenti.html?ta=EM&azione=CONT";
	public By nome = By.xpath("//input[@id='inputNome']");
	public By cogNome = By.xpath("//input[@id='inputCognome']");
	
	public By cf = By.xpath("//input[@id='inputUsername']");
	public By cellulare = By.xpath("//input[@id='inputCellulare']");
	public By email = By.xpath("//input[@id='inputEmail']");
	public By indrizzoMailInputNew = By.xpath("//input[@id='inputIndirizzo']");
	public By confermaEmail = By.xpath("//input[@id='inputConfermaEmail']");
	public By seiGiaCliente = By.xpath("//label[contains(text(),'Sei già cliente?')]");
	public By radioButtonSi = By.xpath("//label[@id='inputCliente_si_label']");
	public By radioButtonNo = By.xpath("//label[@id='inputCliente_no_label']");
	public By numeroClienti = By.xpath("//input[@id='inputFornitura']");
	public By messagio = By.xpath("//textarea[@id='inputNote']");
	public By privacyLabel = By.xpath("//label[@id='testo_privacy_label']");
	public By informativaPrivacySection = By.xpath("//textarea[@id='testo_privacy']");
	public By accetto = By.xpath("//label[@id='inputprivacy_si_label']");
	public By nonAccetto = By.xpath("//label[@id='inputprivacy_no_label']");
	public By prosegui = By.xpath("//button[@id='accediupl']");
	public By guideDocument = By.xpath("//div[@class='guide-document']//a");
	public By popUpBody = By.xpath("(//div[@class='modal-body'])[2]");	
	public By popUpCross = By.xpath("(//button[@class='close'])[2]");	
	public By popUpOK = By.xpath("(//button[text()='Ok'])[2]");	
	
	public By privacyError = By.xpath("//div[@id='errorPrivacy']");
	public By nomeError = By.xpath("//input[@id='inputNome']//following::div[@class='error_message'][1]");
	public By cogNomeError = By.xpath("//input[@id='inputCognome']//following::div[@class='error_message'][1]");
	public By cfError = By.xpath("//input[@id='inputUsername']//following::div[@class='error_message'][1]");
	public By cellulareError = By.xpath("//input[@id='inputCellulare']//following::div[@class='error_message'][1]");
	public By emailError = By.xpath("//input[@id='inputEmail']//following::div[@class='error_message'][1]");
	public By confermaEmailError = By.xpath("//input[@id='inputConfermaEmail']//following::div[@class='error_message'][1]");
	public By messagioError = By.xpath("//div[@id='error_message_note_id']");
//	public By nomeError = By.xpath("//input[@id='inputNome']//following::div[@class='error_message'][1]");
	
	public By successMsgTitle = By.xpath("//h3[contains(text(),'MESSAGGIO')]");
	public By successMsg = By.xpath("//div[@class='upload-info']//label");
	public By indietro = By.xpath("//button[contains(text(),'Indietro')]");
	
	public CaricaDoccumentiComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	public void verifyPlaceholderText(By element,String text) throws Exception{
		String actualText = driver.findElement(element).getAttribute("placeholder");
		if(!text.equals(actualText))
			throw new Exception("Placeholder value is not matching with "+ text);
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void enterInputParameters(By insertObject, String value) throws Exception {
		util.objectManager(insertObject, util.sendKeys, value);
	}
	
	public static final String nometext = "Area riservataCarica documenti";
	public static final String cognometext = "In questa sezione puoi inviare i tuoi documenti";
	public static final String codefiscaletext = "Inserisci qui il tuo Nome"; //
	public static final String Cellulareetext = "Inserisci qui il tuo Nome";
	public static final String numeroClientietext = "Inserisci qui il tuo Nome";
	
	
	public static final String Path = "Area riservataCarica documenti";
	public static final String PageTitle = "In questa sezione puoi inviare i tuoi documenti";
	public static final String NomePlaceholder = "Inserisci qui il tuo Nome";
	public static final String CognomePlaceholder = "Inserisci Cognome";
	public static final String CFPlaceholder = "Inserisci Codice Fiscale / Partita IVA";
	public static final String CellularePlaceholder = "Inserisci qui il tuo numero di cellulare";
	public static final String EmailPlaceholder = "Inserisci qui la tua email";
	public static final String ConfermaEmailPlaceholder = "Conferma qui la tua email";
	public static final String NumeroClientiPlaceholder = "Inserisci il tuo numero cliente";
	public static final String InformativaPrivacySection = "INFORMATIVA AI SENSI DELL' ART. 13 del Regolamento UE 2016/679 (\"GDPR\")La informiamo che i dati personali ivi indicati sono raccolti e trattati per dare seguito alla sua segnalazione. L'informativa completa è disponibile sul sito www.enel.it. Titolare del trattamento è Enel Energia S.p.A., con sede legale in Viale Regina Margherita 125 - 00198 Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007. Per esercitare i diritti previsti agli articoli 15-22 del GDPR è possibile inviare una comunicazione alla casella di posta dedicata: privacy.enelenergia@enel.com.";
	public static final String PopUpBody = "Se sei cliente inserisci anche il numero cliente!";
	public static final String PrivacyError = "Per poter procedere è necessario accettare l'informativa sulla privacy";
	public static final String FieldError = "Il campo è obbligatorio";
	public static final String CFError = "Il Codice Fiscale/Partita IVA è obbligatorio";
	public static final String CellulareError = "Il numero non è formalmente valido";
	public static final String ConfermaEmailError = "Digitare nuovamente l'indirizzo e-mail";
	public static final String CFError1 = "Il Codice Fiscale/Partita IVA deve essere formalmente valido e non può essere uguale a quello del delegato";
	public static final String CellulareError1 = "Il numero non è formalmente valido";
	public static final String EmailError = "Il valore inserito non è valido";
	public static final String ConfermaEmailError1 = "L'indirizzo e-mail non corrisponde a quello digitato in precedenza";
	public static final String SuccessMsg = "Grazie per averci contattato. Ti risponderemo nel più breve tempo possibile";

}
