package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ImpreseComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public String impresePageUrl = "https://www-coll1.enel.it/it/imprese";
	public String impresePageUrlCredential = "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/it/imprese";
	public By impreseTitle = By.xpath("//nav[@class='image-hero_breadcrumbs module--initialized']//following::h1");
	public By impreseSubText = By.xpath("//nav[@class='image-hero_breadcrumbs module--initialized']//following::p[@class='image-hero_detail text--detail']");
	public By scopridipui = By.xpath("//div[@class='swiper-slide']//span[text()='Scopri di più']");
	public By searchIcon = By.xpath("//span[@class='icon-search-small']");
	public By gestisci = By.xpath("//a[text()='Gestisci la tua fornitura']");
	
	public ImpreseComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);			
	}
	 
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public static final String ImpresePageTitle ="L'energia non è mai stata così semplice";
	public static final String ImpresePageTitle_2 ="L'energia non è mai stata così semplice";
	public static final String ImpresePageSubText ="Solo aderendo fino al 29 luglio se scegli Open Energy hai i primi due mesi di abbonamento gratuiti.";
	public static final String ImpresePageSubText_2 ="Solo fino al 29 luglio se scegli Open Energy hai i primi due mesi di abbonamento gratuiti.";

}
