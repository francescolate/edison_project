package com.nttdata.qa.enel.components.r2d;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_PreventivoComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public By inputCodiceRichiestaDistributore = By.xpath("//input[@id='codiceRichiestaDt']");
	public By buttonCerca=By.xpath("//a[contains(text(),'Cerca')]");
	public By selectTipoCaricamento=By.xpath("//select[@id='tipoCaricamento']");
	public By campoDataRicezione= By.xpath("//td[contains(text(),'Data Ricezione')]/ancestor::tr[1]/td[2]/input");
	public By campoNumeroPreventivo= By.xpath("//td[contains(text(),'Numero preventivo')]/ancestor::tr[1]/td[2]/input");
	public By campoDataPreventivo= By.xpath("//td[contains(text(),'Data del preventivo')]/ancestor::tr[1]/td[2]/input");
	public By campoCodiceRintracciabilita= By.xpath("//td[contains(text(),'Codice rintracciabilit')]/ancestor::tr[1]/td[2]/input");
	public By campoCodiceRichiestaDt= By.xpath("//td[contains(text(),'Codice Richiesta Dt')]/ancestor::tr[1]/td[2]/input");
	public By campoDataScandenzaPreventivo= By.xpath("//td[contains(text(),'Data scadenza preventivo')]/ancestor::tr[1]/td[2]/input");
	public By campoTempoMassimoEsecuzioneLavori= By.xpath("//td[contains(text(),'Tempo Massimo Esecuzione Lavori')]/ancestor::tr[1]/td[2]/input");
	public By buttonAggiungiImporto=By.xpath("//img[@title='Aggiungi Voce di costo']");
	public By selectVoceCosto=By.xpath("//select[@name='voceCosto']");
	public By inputImporto = By.xpath("//input[@name='importoVoce']");
	public By buttonConferma=By.xpath("//a[contains(text(),'Conferma')]");
	public By caricamentoPreventivo=By.xpath("//div[contains(text(),'Operazione Conclusa con Successo')]");

	public R2D_PreventivoComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	public void InserisciCodiceRichiestaDistributore(By inputCodiceRichiestaDistributore, String valore) throws Exception{
		util.objectManager(inputCodiceRichiestaDistributore,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}


	public void cercaPratica(By buttonCerca) throws Exception{
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}

	public void selezionaTastoAzionePratica() throws Exception{
		By	buttonAzione=new By.ByXPath("//table//tr/td/img[@title='Modifica']");
		int risultatoRicerca=driver.findElements(buttonAzione).size();
		Logger.getLogger("").log(Level.INFO, "Risultato ricerca:"+risultatoRicerca);
		if(risultatoRicerca==0){
			throw new Exception("Nessun risultato restituito");
		}
		else if(risultatoRicerca!=1){
			throw new Exception("La ricerca restituisce più di un risultato.Verificare");
		}
		else{
			util.objectManager(buttonAzione,util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);}
	}

	public void caricaPreventivoAllaccio(String data,String numeroPreventivo,String codiceRintracciabilita,String idCrm, String data30, String tempoEsecuzioneLavori, String voceDiCosto, String importo) throws Exception{
		//selezione tipo caricamento
		util.objectManager(selectTipoCaricamento, util.scrollAndSelect,"Preventivo");
		TimeUnit.SECONDS.sleep(3);
		//Inserimento Data Ricezione
		util.objectManager(campoDataRicezione, util.scrollAndSendKeys,data);
		//Inserimento Numero Preventivo
		util.objectManager(campoNumeroPreventivo, util.scrollAndSendKeys,numeroPreventivo);
		//Inserimento data del preventivo
		util.objectManager(campoDataPreventivo, util.scrollAndSendKeys,data);
		//Inserimento codice di rintracciabilità
		util.objectManager(campoCodiceRintracciabilita, util.scrollAndSendKeys,codiceRintracciabilita);
		//Inserimento Codice Richiesta Dt
//		util.objectManager(campoCodiceRichiestaDt, util.scrollAndSendKeys,idCrm);
		//Inserimemento data scandenza del preventivo
		util.objectManager(campoDataScandenzaPreventivo, util.scrollAndSendKeys,data30);
		//Inserimento tempo Massimo Esecuzione lavori
		util.objectManager(campoTempoMassimoEsecuzioneLavori, util.scrollAndSendKeys,tempoEsecuzioneLavori);
		//Aggiunta importo Preventivo
		util.objectManager(buttonAggiungiImporto, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
		//Selezione voce di costo
		util.objectManager(selectVoceCosto, util.scrollAndSelect,voceDiCosto);
		//Inserimento importo
		util.objectManager(inputImporto, util.scrollAndSendKeys,importo);
		//Click su conferma buttonConferma
		util.objectManager(buttonConferma, util.scrollAndClick);
		//Gestione pop.up conferma
		accettaPopUp();
		
		//Verifica avventuta caricamento preventivo
		if(!util.exists(caricamentoPreventivo, 30)){
			throw new Exception("Messaggio 'Operazione Conclusa con Successo' non visualizzato. Possibili problemi nel caricamento del preventivo. Verificare");
		}
		
	}
	
	public void caricaPreventivoSubentroCombinato(String data,String numeroPreventivo,String codiceRintracciabilita,String idCrm, String data30, String tempoEsecuzioneLavori, String voceDiCosto1, String importo1,String voceDiCosto2,String importo2) throws Exception{
		//selezione tipo caricamento
		util.objectManager(selectTipoCaricamento, util.scrollAndSelect,"Preventivo");
		TimeUnit.SECONDS.sleep(3);
		//Inserimento Data Ricezione
		util.objectManager(campoDataRicezione, util.scrollAndSendKeys,data);
		//Inserimento Numero Preventivo
		util.objectManager(campoNumeroPreventivo, util.scrollAndSendKeys,numeroPreventivo);
		//Inserimento data del preventivo
		util.objectManager(campoDataPreventivo, util.scrollAndSendKeys,data);
		//Inserimento codice di rintracciabilità
		util.objectManager(campoCodiceRintracciabilita, util.scrollAndSendKeys,codiceRintracciabilita);
		//Inserimento Codice Richiesta Dt
//		util.objectManager(campoCodiceRichiestaDt, util.scrollAndSendKeys,idCrm);
		//Inserimemento data scandenza del preventivo
		util.objectManager(campoDataScandenzaPreventivo, util.scrollAndSendKeys,data30);
		//Inserimento tempo Massimo Esecuzione lavori
		util.objectManager(campoTempoMassimoEsecuzioneLavori, util.scrollAndSendKeys,tempoEsecuzioneLavori);
		//Aggiunta importo Preventivo n°1
		util.objectManager(buttonAggiungiImporto, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
		List<WebElement> vociCosto=driver.findElements(selectVoceCosto);
		List<WebElement> importi=driver.findElements(inputImporto);
		
		//inserimento importo 1
		util.scrollToElement(vociCosto.get(0));
		//Selezione Voce Costo 1
		Select select = new Select(vociCosto.get(0));
		select.selectByVisibleText(voceDiCosto1);
		//Selezione Importo 1
		importi.get(0).sendKeys(importo1);
		
		//Aggiunta importo Preventivo n°2
		util.objectManager(buttonAggiungiImporto, util.scrollAndClick);
		
		 TimeUnit.SECONDS.sleep(3);
		 vociCosto=driver.findElements(selectVoceCosto);
		 importi=driver.findElements(inputImporto);
		
		//inserimento importo 2
		util.scrollToElement(vociCosto.get(1));
		//Selezione Voce Costo 2
		Select select2 = new Select(vociCosto.get(1));
		select2.selectByVisibleText(voceDiCosto2);
		//Selezione Importo 2
		importi.get(1).sendKeys(importo2);
		
		//Click su conferma buttonConferma
		util.objectManager(buttonConferma, util.scrollAndClick);
		//Gestione pop.up conferma
		accettaPopUp();
		
		//Verifica avventuta caricamento preventivo
		if(!util.exists(caricamentoPreventivo, 30)){
			throw new Exception("Messaggio 'Operazione Conclusa con Successo' non visualizzato. Possibili problemi nel caricamento del preventivo. Verificare");
		}
		
	}





	private void accettaPopUp() throws Exception{
		TimeUnit.SECONDS.sleep(10);
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(3);

	}

}
