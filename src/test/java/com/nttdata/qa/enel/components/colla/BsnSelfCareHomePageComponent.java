package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BsnSelfCareHomePageComponent extends BaseComponent {
	public By userLogo = By.id("avatarUtente");
	public By titleLabel = By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
	public By pathLbl1 = By.cssSelector("#content-home > div:nth-child(1) > div > ol > li:nth-child(1) > a");
	public By pathLbl2 = By.cssSelector("#content-home > div:nth-child(1) > div > ol > li.active");
	
	public BsnSelfCareHomePageComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkHomePageLanding() throws Exception {
		By[] locators = {userLogo, titleLabel};
		verifyElementsArrayVisibility(locators);
		By[] locators2 = {pathLbl1, pathLbl2};
		verifyElementsArrayText(locators2, pathStrings);
	}
	
	//----- Constants -----
	
	private String[] pathStrings = {
			"AREA RISERVATA",
			"HOMEPAGE"
	};
}
