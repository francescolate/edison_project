package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BodyCheckerComponent {
	
	WebDriver driver;
    SeleniumUtilities util;

    public By whatKindOfContract = By.xpath("//label[contains(text(), 'What kind of contract?')]");
    public By where = By.xpath("//label[contains(text(), 'Where?')]");
    public By whatFor = By.xpath("//label[contains(text(), 'What for?')]");
    public By bodyContainer = By.xpath("//*[@id='promo-offert_noresults']");
    
    public String bodyContent = "POWER AND GAS TOGETHER FOR YOUR HOMEFind the right solution for your home with Enel Energia dealsIf you did not find what you were looking for, you can visit the specific section or contact a dedicated consultant Find the nearest Enel store!";
    public String bodyContentBusiness = "Power and Gas togheter for businessesPower and Gas togheter for businessesCurrently there are no combined energy and gas solutions for businesses. We recommend visiting the specific luce and gas section for your business sector or contacting a dedicated consultant See all power See all gas CONTACT A CONSULTANT, NO OBLIGATIONS";
   
    
    public BodyCheckerComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
    
    public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");

  }
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
}
