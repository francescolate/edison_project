package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class UsefulLinksItaComponent extends BaseComponent {
	public By sectionTitle = By.cssSelector("#main > div.wrapper-sec > div > section > div > h3");
	public By sosLuceGasLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(1) > a");
	public By mixCombustibii = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(2) > a");
	public By pesselink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(3) > a");
	public By servizioSalvaguardiaLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(4) > a");
	public By defaultDistribuzioneLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(5) > a");
	public By conciliazioniControversieLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(6) > a");
	public By informazioniUtiliLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(1) > a");
	public By regoleProtezioneDatiLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(2) > a");
	public By contattaciLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(3) > a");
	public By partnerLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(4) > a");
	public By sponsorizzazioniLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(5) > a");
	public By guidaBollettaLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(6) > a");
	public By prescrizioneImportiLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(7) > a");
	public By modulisticaLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(1) > a");
	public By modulisticaReclamiLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(2) > a");
	public By remitLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(3) > a");
	public By negoziazioneLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(4) > a");
	public By evoluzioneMercatiLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(5) > a");
	public By servizioTutelaGasLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(6) > a");
	
	public By[] sectionElements = {
			sectionTitle,
			sosLuceGasLink,
			mixCombustibii,
			pesselink,
			servizioSalvaguardiaLink,
			defaultDistribuzioneLink,
			conciliazioniControversieLink,
			informazioniUtiliLink,
			regoleProtezioneDatiLink,
			contattaciLink,
			partnerLink,
			sponsorizzazioniLink,
			guidaBollettaLink,
			prescrizioneImportiLink,
			modulisticaLink,
			modulisticaReclamiLink,
			remitLink,
			negoziazioneLink,
			evoluzioneMercatiLink,
			servizioTutelaGasLink
	};
	
	public UsefulLinksItaComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifySectionStrings() throws Exception {
		verifyElementsArrayText(sectionElements, sectionStrings);
	}
	
	// -----------------  Constants -----------------------
	
	public final String[] sectionStrings = {
			"Link utili",
			"SOS luce e gas",
			"Mix Combustibili",
			"Piano salva Black out (PESSE)",
			"Servizio di salvaguardia",
			"Servizio default di distribuzione",
			"Conciliazioni e risoluzione delle controversie",
			"Informazioni utili",
			"Nuove regole europee per la protezione dei dati",
			"Contattaci",
			"Diventa nostro partner",
			"Accedi alle sponsorizzazioni",
			"Guida alla bolletta luce e gas",
			"Prescrizione degli importi fatturati per la fornitura di energia elettrica e gas",
			"Modulistica",
			"Modulistica reclami",
			"Remit",
			"Negoziazione paritetica",
			"Evoluzione mercati al dettaglio",
			"Offerta Servizio Tutela Gas"
	};
}
