package com.nttdata.qa.enel.components.lightning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

//Il seguente componente si occupa di effettuare l'accesso alla sezione clienti. 
//Dopo aver effettuato la Login a Salesforce clicca sul menu a tendina del primo tab aperto per accedere all sezione Clienti
public class WorkbenchComponent {
	private WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public WorkbenchComponent(RemoteWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	
	public void launchLink(String url) throws Exception {
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}

	private final By selectEnvironment = By.id("oauth_env");
	public final By checkAgree = By.id("termsAccepted");
	public final By buttonLogin = By.id("loginBtn");
	private final By sqlArea = By.id("soql_query_textarea");
	public final By submitQuery = By.name("querySubmit");
	private final By buttonUpdate = By.xpath("//input[@value='Update']");
	private final By buttonConfirmUpdate = By.xpath("//input[@value='Confirm Update']");
	private final By queryResultTable = By.id("query_results");
	private final int dly = 1;
	public By queryInputField = By.xpath("//textarea[@id='soql_query_textarea']");


	public void selezionaEnvironment(String environment) throws Exception{
		util.objectManager(selectEnvironment, util.select, environment);
	}

	public void pressButton(By element) throws Exception{
		util.objectManager(element, util.scrollAndClick);
	}

	public void insertQuery(String query) throws Exception {
		util.objectManager(sqlArea, util.clear);
		util.objectManager(sqlArea, util.sendKeys, query);
	}
	public void clearText(By by) throws Exception{
		driver.findElement(by).clear();
	}


	public boolean aspettaRisultati() throws Exception {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 60);
		boolean exist = false;
		try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='query_results']")));
			exist = true;
		}finally{
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		}

		return exist;
	}
	
	public boolean aspettaRisultati(int sec) throws Exception {
		driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
		FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 60);
		boolean exist = false;
		try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@id='query_results']")));
			exist = true;
		}
		catch(Exception e){
			exist = false;
		}
		return exist;
	}

	public boolean verificaNoRecordFound() throws Exception {
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 60);
		boolean noRecordFound = false;
		try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//p[text()='Sorry, no records returned.']")));
			noRecordFound = true;
		}
		catch (Exception e){}

		finally{
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		}

		return noRecordFound;
	}



	//p[text()='Sorry, no records returned.']


	public void recuperaRisultati(int riga, Properties prop) throws Exception{
		List<WebElement> cols = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(riga < 1 | riga > rows.size()-1) throw new Exception("Numero di riga inserito ("+riga+") minore di 1 oppure maggiore del numero di righe restituite dalla query ("+rows.size()+")");
		//System.out.println(cols.size());
		prop.setProperty("ROW_COUNT", ""+(rows.size()-1));//va sottratto l'header
		for(int c = 1;c<cols.size();c++) {
			String nomeColonna = cols.get(c).getText();
			String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(riga+1)+"]/td["+(c+1)+"]")).getText();
			prop.setProperty(nomeColonna.toUpperCase(), valore);
			prop.setProperty("QUERY_"+nomeColonna.toUpperCase(), valore);
		}
	}
	
	public void recuperaStatus(int riga, Properties prop) throws Exception{
		List<WebElement> cols = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(riga < 1 | riga > rows.size()) throw new Exception("Numero di riga inserito ("+riga+") minore di 1 oppure maggiore del numero di righe restituite dalla query ("+rows.size()+")");
		//System.out.println(cols.size());
		for(int c = 1;c<cols.size();c++) {
			String nomeColonna = cols.get(c).getText();
			String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(riga+1)+"]/td["+(c+1)+"]")).getText();
//			if (nomeColonna.equals("ITA_IFM_Status__c")||(nomeColonna.equals("Id"))) {
			if (nomeColonna.equals("ITA_IFM_Status__c")) {
				prop.setProperty(nomeColonna.toUpperCase(), valore);
				break;
			}
		}
	}
	
	public void recuperaId(int riga, Properties prop) throws Exception{
		List<WebElement> cols = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(riga < 1 | riga > rows.size()) throw new Exception("Numero di riga inserito ("+riga+") minore di 1 oppure maggiore del numero di righe restituite dalla query ("+rows.size()+")");
		//System.out.println(cols.size());
		for(int c = 1;c<cols.size();c++) {
			String nomeColonna = cols.get(c).getText();
			String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(riga+1)+"]/td["+(c+1)+"]")).getText();
//			if (nomeColonna.equals("ITA_IFM_Status__c")||(nomeColonna.equals("Id"))) {
			if (nomeColonna.equals("Id")) {
				prop.setProperty(nomeColonna.toUpperCase(), valore);
				break;
			}
		}
	}
	
	public Map recuperaTuttiIRisultati() throws Exception{
		List<WebElement> cols = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(rows.size()<1) throw new Exception("La query non ha estratto dati");
		Map<Integer,Map> mappaRighe = new HashMap<Integer,Map>();
		for(int x = 1;x<rows.size();x++) {
			Map<String,String> mappaValori = new HashMap<String,String>();
			for(int c = 1;c<cols.size();c++) {
				String nomeColonna = cols.get(c).getText();
				String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(x+1)+"]/td["+(c+1)+"]")).getText();
				mappaValori.put(nomeColonna, valore);
			}
			mappaRighe.put(x, mappaValori);
		}
		
		return mappaRighe;
	
	}
	
	/*
	 * Per una query che ha un unico campo in SELECT costruisce uno stringone con i valori di tutte le righe, intervallati da virgola
	 * Da dare in pasto come condizione IN ad una query successiva
	 * 
	 * Es select nome from tabella
	 * 
	 * restituirà ('Roberto','Rosario','Giuseppe')
	 */
	public String costruisciStringaRisultati() throws Exception{
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(rows.size()<1) throw new Exception("La query non ha estratto dati");
		String result = "";
		String formatter = "'%s'";
		for(int x = 2;x<=rows.size();x++) {
			    int c = 2;
				String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(x)+"]/td["+(c)+"]")).getText();
				result = result + String.format(formatter, valore);
			    if(x!=rows.size()) result = result + ",";
		}
		return "(" + result + ")";
		
	
	}
	
	
	public static String GeneraInClause(Map<Integer,Map> rows, String colName) throws Exception
	{
		//Da utilizzare dopo aver chiamato il metodo RecuperaTuttiIRisultati
		if(rows.isEmpty()) 	throw new Exception("Nessun record da estrarre");

		String inCondition="";
		//GENERO IN PER DA INSERIRE IN SECONDA QUERY
		for(Integer r :rows.keySet())
		{
			if(!rows.get(r).containsKey(colName)) throw new Exception("Impossibile trovare la colonna "+colName);
			inCondition+=(inCondition.equals("")?"":",")+"'"+rows.get(r).get(colName)+"'";
		}


		return inCondition;
	}
	

	public String costruisciStringaRisultatiColonna2() throws Exception{
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(rows.size()<1) throw new Exception("La query non ha estratto dati");
		String result = "";
		String formatter = "'%s'";
		for(int x = 2;x<=rows.size();x++) {
			    int c = 2; // prendo la colonna 2
				String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(x)+"]/td["+(c)+"]")).getText();
				result = result + String.format(formatter, valore);
			    if(x!=rows.size()) result = result + ",";
		}
		return "(" + result + ")";
	}
	
	public String costruisciStringaRisultatiColonna3() throws Exception{
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(rows.size()<1) throw new Exception("La query non ha estratto dati");
		String result = "";
		String formatter = "'%s'";
		for(int x = 2;x<=rows.size();x++) {
			    int c = 3; // prendo la colonna 3
				String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(x)+"]/td["+(c)+"]")).getText();
				result = result + String.format(formatter, valore);
			    if(x!=rows.size()) result = result + ",";
		}
		return "(" + result + ")";
		
	
	}

	public String costruisciStringaRisultatiColonna5() throws Exception{
		List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
		if(rows.size()<1) throw new Exception("La query non ha estratto dati");
		String result = "";
		String formatter = "'%s'";
		for(int x = 2;x<=rows.size();x++) {
			    int c = 5; // prendo la colonna 5
				String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(x)+"]/td["+(c)+"]")).getText();
				result = result + String.format(formatter, valore);
			    if(x!=rows.size()) result = result + ",";
		}
		return "(" + result + ")";
		
	
	}

	
	public void costruisciQueryDaMappa(String query,Map<String,String> mappaValori,String columnNames) throws Exception{
		//String st = "akkk %s la,ala %s "; 
		String result = String.format(query, "First Val", "Second Val");
		
	}

	public void updateRow(String campo, String nuovoValoreCampo) throws Exception{
		//Calcolo indice del campo id
		List<WebElement> rowsInt = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
		boolean trovato=false;
		int i=0;
		for(i = 1;i<=rowsInt.size();i++) {
			String nomeColonna = rowsInt.get(i).getText();
			if(nomeColonna.compareTo("Id")==0){
				trovato=true;
				break;

			}
		}
		if(!trovato){
			throw new Exception("Non è stata trovata la colonna 'Id'");

		}
		Logger.getLogger("").log(Level.INFO, "Indice colonna 'Id'"+i+1);
		//click iperlink elemento
		try{
		By elemToClick = By.xpath("//table[@id='query_results']/tbody/tr/td["+(i+1)+"]/a");
		driver.findElement(elemToClick).click();
		}
		catch (Exception e){
			throw new Exception("Non è possibile cliccare sull'elemento cercato");

		}
		//Click button update
		util.objectManager(buttonUpdate, util.click);
		//Inserimento nuovo valore campo
		By inputToUpdate = By.xpath("//input[@name='"+campo+"']");
		util.objectManager(inputToUpdate, util.sendKeys,nuovoValoreCampo);
		//Click conferma
		util.objectManager(buttonConfirmUpdate, util.click);
		
		//Verifica esito update
		
		List<WebElement> rowsIntRis = util.waitAndGetElements(By.xpath("//table/tbody/tr/th"));
		boolean trovatoRis=false;
		int j=0;
		for(j = 1;j<=rowsIntRis.size();j++) {
			String nomeColonna = rowsIntRis.get(j).getText();
			if(nomeColonna.compareTo("Result")==0){
				trovatoRis=true;
				break;

			}
		}
		if(!trovatoRis){
			throw new Exception("Non è stata trovata la colonna 'Result'");

		}
		
		//Verifica contenuto campo Status

		By elemToVerify = By.xpath("//table[@class='dataTable']/tbody/tr/td["+(j+1)+"]");
		util.waitAndGetElements(elemToVerify);
		String stato=driver.findElement(elemToVerify).getText();
		if(stato.compareTo("Success")!=0){
			throw new Exception("La colonna Result ha stato diverso da Success. Assume valore:"+stato);
		}

		Logger.getLogger("").log(Level.INFO, "Indice colonna 'Result'"+j);
		Logger.getLogger("").log(Level.INFO, "Valore colonna 'Result'"+stato);
		
		
	}
	
	
	
	public void updateRows(Map<String,String> campi) throws Exception{
		//Calcolo indice del campo id
		List<WebElement> rowsInt = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
		boolean trovato=false;
		int i=0;
		for(i = 1;i<=rowsInt.size();i++) {
			String nomeColonna = rowsInt.get(i).getText();
			if(nomeColonna.compareTo("Id")==0){
				trovato=true;
				break;

			}
		}
		if(!trovato){
			throw new Exception("Non è stata trovata la colonna 'Id'");

		}
		Logger.getLogger("").log(Level.INFO, "Indice colonna 'Id'"+i+1);
		//click iperlink elemento
		try{
		By elemToClick = By.xpath("//table[@id='query_results']/tbody/tr/td["+(i+1)+"]/a");
		driver.findElement(elemToClick).click();
		}
		catch (Exception e){
			throw new Exception("Non è possibile cliccare sull'elemento cercato");

		}
		//Click button update
		util.objectManager(buttonUpdate, util.click);
		
		for (Map.Entry<String, String> entry : campi.entrySet()) {
		  //Inserimento nuovo valore campo
			By inputToUpdate = By.xpath("//input[@name='"+entry.getKey()+"']");
			util.objectManager(inputToUpdate, util.sendKeys,entry.getValue());
		}
		
		
		//Click conferma
		util.objectManager(buttonConfirmUpdate, util.click);
		
		//Verifica esito update
		
		List<WebElement> rowsIntRis = util.waitAndGetElements(By.xpath("//table/tbody/tr/th"));
		boolean trovatoRis=false;
		int j=0;
		for(j = 1;j<=rowsIntRis.size();j++) {
			String nomeColonna = rowsIntRis.get(j).getText();
			if(nomeColonna.compareTo("Result")==0){
				trovatoRis=true;
				break;

			}
		}
		if(!trovatoRis){
			throw new Exception("Non è stata trovata la colonna 'Result'");

		}
		
		//Verifica contenuto campo Status

		By elemToVerify = By.xpath("//table[@class='dataTable']/tbody/tr/td["+(j+1)+"]");
		util.waitAndGetElements(elemToVerify);
		String stato=driver.findElement(elemToVerify).getText();
		if(stato.compareTo("Success")!=0){
			throw new Exception("La colonna Result ha stato diverso da Success. Assume valore:"+stato);
		}

		Logger.getLogger("").log(Level.INFO, "Indice colonna 'Result'"+j);
		Logger.getLogger("").log(Level.INFO, "Valore colonna 'Result'"+stato);
		
		
	}

	
	
	public void logoutWorkbench(String url) throws Exception{
		driver.navigate().to(url);
	}
	
	public void logoutWorkbench() throws Exception{
		try {
			util.exists(By.xpath("//li[1]//span[@class='down']"), dly);
			util.objectManager(By.xpath("//li[1]//span[@class='down']"), util.scrollAndClick);
			util.exists(By.xpath("//a[@href='logout.php']"), dly);
			util.objectManager(By.xpath("//a[@href='logout.php']"), util.scrollAndClick);
		}catch (Exception e){
			new Exception("Errore in fase di Logout");
		}finally {
			driver.close();
		}


	}
	
	public void AbilitaJoin() throws Exception{
		util.exists(By.xpath("//li[1]//span[@class='down']"), dly);
		util.objectManager(By.xpath("//li[1]//span[@class='down']"), util.scrollAndClick);
		util.exists(By.xpath("//a[@href='settings.php']"), dly);
		util.objectManager(By.xpath("//a[@href='settings.php']"), util.scrollAndClick);
		util.exists(By.xpath("//input[@id='allowParentRelationshipQueries']"), dly);
		util.objectManager(By.xpath("//input[@id='allowParentRelationshipQueries']"), util.scrollAndClick);
		util.exists(By.xpath("//input[@name='submitConfigSetter']"), dly);
		util.objectManager(By.xpath("//input[@name='submitConfigSetter']"), util.scrollAndClick);
		
	}
	
	public void inserisciNuovaQuery() throws Exception{
		util.exists(By.xpath("//li//span[text()='queries']"), dly);
		util.objectManager(By.xpath("//li//span[text()='queries']"), util.scrollAndClick);
		util.exists(By.xpath("//a[@href='query.php']"), dly);
		util.objectManager(By.xpath("//a[@href='query.php']"), util.scrollAndClick);

	}

	
	
	public List<String> recuperaRisultatiLista(int riga) throws Exception{
		try {
			List<WebElement> cols = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr/th"));
			List<WebElement> rows = util.waitAndGetElements(By.xpath("//table[@id='query_results']/tbody/tr"));
			if(riga < 1 | riga > rows.size()) throw new Exception("Numero di riga inserito ("+riga+") minore di 1 oppure maggiore del numero di righe restituite dalla query ("+rows.size()+")");
			//System.out.println(cols.size());
			List<String> valori = new ArrayList<String>();
			for(int c = 1;c<cols.size();c++) {
				String valore = util.waitAndGetElement(By.xpath("//table[@id='query_results']/tbody/tr["+(riga+1)+"]/td["+(c+1)+"]")).getText();
				valori.add(valore);
			}
			return valori;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
