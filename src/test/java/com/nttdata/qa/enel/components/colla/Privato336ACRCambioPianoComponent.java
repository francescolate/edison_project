package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Privato336ACRCambioPianoComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	//servizi
	
	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
	
	public By serviziPage = By.xpath("//h2[text()='Servizi per le forniture']");
	
	public By serviziSectionTitle = By.xpath("//h2[text()='Servizi per il contratto']");
	
    public By cambioPianoButton = By.xpath("//h3[text()='Cambio Piano']/ancestor::a");
    public By cambioPianoPageTitle = By.xpath("//h1[text()='Cambio Piano']");
    public By cambioPianoPageDescription1 = By.xpath("//h1[text()='Cambio Piano']/following-sibling::p[2]");
    public By cambioPianoPageDescription2 = By.xpath("//h1[text()='Cambio Piano']/following-sibling::p[4]");
    
    public By luceIcon_310491837 = By.xpath("//*[text()='310491837']/ancestor::span/preceding-sibling::span[contains(@class,'icon-container')]/span[@class='icon-line-electricity']");
    public By LightDetails = By.xpath("(//span[@class='flex dsd'])[2]/span[not(contains(@class,'icon'))]");
    
    public By ESCI_Button = By.xpath("//button[text()='ESCI']"); 
    
    public By radioButton = By.xpath("//span[text()='310506437']/parent::span/parent::span/parent::span/preceding-sibling::span"); 
    
    public By radioButton310522248 = By.xpath("//span[text()='310522248']/parent::span/parent::span/parent::span/preceding-sibling::span");
	
    public By radioButton310522233 = By.xpath("//span[text()='310522233']/parent::span/parent::span/parent::span/preceding-sibling::span");
	
    public By Plan_Button = By.xpath("//div[@id='Senza Orari']");
    public By Plan_ButtonNew = By.xpath("//div[@class='tab-info-container']/fieldset/div[1]");
    public By iconcheckbox = By.xpath("//span[@class='icon-ckbox']"); 
    public By iconcontainer = By.xpath("//span[@class='icon-container']"); 
    public By indrizzo = By.xpath("//span[@class='indirizzo']"); 
    public By cliente = By.xpath("//span[text()='Numero cliente']"); 
    
	public Privato336ACRCambioPianoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
public void verifyCheckboxnotSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (!element.isSelected()){
		checkbox="YES";
		}			
		if (element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is selected");
				}
	}
	
	
	public void verifyCheckboxSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (element.isSelected()){
		checkbox="YES";
		}			
		if (!element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is not selected");
				}
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText().replaceAll("(\r\n|\n)", " ");;
		Value = Value.replaceAll("(\r\n|\n)", " ");
		
		System.out.println(actualtext);
		System.out.println(Value);
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}

	public void luceSupplyDetailsDisplay(By checkObject) throws Exception {
			
			verifyComponentExistence(luceIcon_310491837);
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
							
			String textfield_found="NO";
						
			List<WebElement> field = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='flex dsd'])[2]/span[not(contains(@class,'icon'))]/span[1]"));
			List<WebElement> value = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='flex dsd'])[2]/span[not(contains(@class,'icon'))]/span[2]"));
			
			for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
				
				WebElement element = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='flex dsd'])[2]/span[not(contains(@class,'icon'))]/span[1])["+i+"]"));
				String description=element.getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				WebElement element1 = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='flex dsd'])[2]/span[not(contains(@class,'icon'))]/span[2])["+j+"]"));
				String descriptionValue=element1.getText();
				descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
				
				if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("Via Aldo Lusardi 76 20122 Milano Milano Mi")) {
					textfield_found="YES";
				}else if (description.contentEquals("Numero Cliente") && descriptionValue.contentEquals("310491837")) {
					textfield_found="YES";
				}else if (description.contentEquals("Piano Attuale") && descriptionValue.contentEquals("Bioraria")) {
					textfield_found="YES";
				} 
				
				if (textfield_found.contentEquals("NO"))
					throw new Exception("ELE Supply details are not matching for index :"+i);
				
			}
			
		}

	public void confirmGasSupply(By checkObject) throws Exception {
		
//		verifyComponentExistence(IconFlame_300001597);
		
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='flex'])[1]/span[not(contains(@class,'icon'))]/span[1]"));
		List<WebElement> value = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='flex'])[1]/span[not(contains(@class,'icon'))]/span[2]"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='flex'])[1]/span[not(contains(@class,'icon'))]/span[1])["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='flex'])[1]/span[not(contains(@class,'icon'))]/span[2])["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("Via Virginia 12 A 40017 San Matteo Della Decima San Giovanni In Persiceto Bo")) {
				textfield_found="YES";
			}else if (description.contentEquals("Numero cliente") && descriptionValue.contentEquals("300001597")) {
				textfield_found="YES";
			}
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Confirm Gas Supply details are not matching for index :"+i);
			
		}
		
	}
	
	public String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public String CambioPianoPageDescription1 = "Con le offerte flessibili della gamma Scegli Tu e Scegli Oggi puoi cambiare il tuo piano tariffario gratuitamente e quando vuoi, in base alle tue abitudini di consumo. Ricorda, durante gli ultimi 2 mesi che precedono il rinnovo delle condizioni economiche, non sarà possibile effettuare il cambio piano.";
	public String CambioPianoPageDescription2 = "Per conoscere il prezzo attualmente applicato sul tuo piano e i prezzi degli altri piani avvia la richiesta di Cambio Piano.";
}
