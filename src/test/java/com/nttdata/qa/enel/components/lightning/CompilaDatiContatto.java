package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

/**
 * @author ChianteseRo
 * Compila i Email e Cellulare per il form "Dati COntatto per Attivazione"
 */
public class CompilaDatiContatto {
	WebDriver driver ;
	SeleniumUtilities util;
	SpinnerManager spinner;
	private By mail = By.xpath("//input[@id='j_id0:formId:email']");
	private By confermaMail = By.xpath("//input[@id='j_id0:formId:confEmail']");
	private By cellulare = By.xpath("//input[@id='j_id0:formId:mobile']");
	private By confermacellulare = By.xpath("//input[@id='j_id0:formId:confMob']");
	private By smsCheckAvviso = By.xpath("//input[@id='j_id0:formId:chooseSMSInEmissioneInfoEnelEnergia']");
	private By emailCheckAvviso = By.xpath("//input[@id='j_id0:formId:chooseEmailEmissioneInfoEnelEnergia']");
	//Per processo Tracking Fatturazione
	private By emailInEmissione = By.xpath("//input[@id='j_id0:formId:chooseEmailInEmissione']");
	private By emailEmissione = By.xpath("//input[@id='j_id0:formId:chooseEmailEmissione']");
	private By emailSpedizione = By.xpath("//input[@id='j_id0:formId:chooseEmailSpedizione']");
	private By emailInConsegna = By.xpath("//input[@id='j_id0:formId:chooseEmailConsegna']");
	private By emailInScadenza = By.xpath("//input[@id='j_id0:formId:chooseEmailInScadenza']");
   
	public CompilaDatiContatto(WebDriver driver){
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	public void fillDati(String mailInput, String cellulareInput) throws Exception 
	{
//R		String frame = util.getFrameByIndex(1);
//		WebDriverWait wait = new WebDriverWait(driver, 120);
		//R	util.frameManager(frame, mail, util.sendKeys, mailInput);
		//R	util.frameManager(frame, confermaMail, util.sendKeys, mailInput);
		util.objectManager(mail, util.sendKeys, mailInput);
		util.objectManager(confermaMail, util.sendKeys, mailInput);
		if (cellulareInput.length() > 0)
		{
			util.objectManager(cellulare, util.sendKeys, cellulareInput);
//			util.frameManager(frame, confermacellulare, util.sendKeys, cellulareInput);
		}
	}
	
	public void fillDatiInfoEnergia(String mailInput, String cellulareInput) throws Exception 
	{
		util.objectManager(mail, util.sendKeys, mailInput);
		util.objectManager(confermaMail, util.sendKeys, mailInput);
		if (cellulareInput.length() > 0)
		{
			util.objectManager(cellulare, util.sendKeys, cellulareInput);
			util.objectManager(confermacellulare, util.sendKeys, cellulareInput);
		}
	}
	
	public void fillDatiPEC(String mailInput, String cellulareInput) throws Exception 
	{
		mail = By.xpath("//input[@id='j_id0:formId:pec']");
		confermaMail = By.xpath("//input[@id='j_id0:formId:confPec']");

//		String frame = util.getFrameByIndex(1);
//		WebDriverWait wait = new WebDriverWait(driver, 120);
		util.objectManager(mail, util.sendKeys, mailInput);
		util.objectManager(confermaMail, util.sendKeys, mailInput);
		if (cellulareInput.length() > 0)
		{
			util.objectManager(cellulare, util.sendKeys, cellulareInput);
//			util.objectManager(confermacellulare, util.sendKeys, cellulareInput);
		}
	}
	public void fillAvvisiEnelEnergia() throws Exception 
	{
//		String frame = util.getFrameByIndex(1);
		TimeUnit.SECONDS.sleep(2);
		util.objectManager(smsCheckAvviso, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		util.objectManager(emailCheckAvviso, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);

	}
	
	public void fillAvvisiTracking() throws Exception 
	{
		
//		String frame = util.getFrameByIndex(1);
//		WebDriverWait wait = new WebDriverWait(driver, 120);
		TimeUnit.SECONDS.sleep(2);
		util.objectManager(emailInEmissione, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(emailEmissione, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(emailSpedizione, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(emailInConsegna, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(emailInScadenza, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);

	}
}
