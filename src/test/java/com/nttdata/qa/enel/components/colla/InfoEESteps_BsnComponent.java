package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;

public class InfoEESteps_BsnComponent extends BaseComponent {
	enum OperationType {
		ACTIVATION,
		MODIFICATION
	}
	
	public By[] changingTxtLocators = {
			By.cssSelector("#sc > div > div.panel.panel-default.panel-clear.panel-steps > div > ul > li.current > span.text"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div:nth-child(1) > div > p")
	};
	public By[] step1TxtLocators = {
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div:nth-child(3) > form > div > div > p"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div:nth-child(3) > form > div > div > div:nth-child(6) > div > label"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div:nth-child(3) > form > div > div > div:nth-child(7) > div > label"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div:nth-child(3) > form > div > div > div:nth-child(8) > div > label"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div:nth-child(3) > form > div > div > div:nth-child(9) > div > label"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div:nth-child(3) > form > div > div > div.form-group.check-group.checkbox-small.cITA_IFM_LCP405SelfCareCheckbox > label > span")
	};
	public By[] step2TxtLocators = {
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div.row > div:nth-child(1) > div > label"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > div.row > div:nth-child(2) > div > label"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > section > div > div > div:nth-child(1) > div > h5"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > section > div > div > div:nth-child(3) > div > div > table > thead > tr > th:nth-child(1) > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > section > div > div > div:nth-child(3) > div > div > table > thead > tr > th:nth-child(2) > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > section > div > div > div:nth-child(3) > div > div > table > thead > tr > th:nth-child(3) > b")
	};
	public By[] commonLocators = {
			By.cssSelector("#sc > div > div.panel.panel-default.panel-clear.panel-steps > div > ol > li:nth-child(1) > a"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-clear.panel-steps > div > ol > li:nth-child(2) > a"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div.row.check-block > div:nth-child(2) > div > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div.row.check-block > div:nth-child(3) > div > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div.row.check-block > div:nth-child(4) > div > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div.row.check-block > div:nth-child(5) > div > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div.row.check-block > div:nth-child(6) > div > b"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-primary > div > div > div > div > form > div.row.check-block > div:nth-child(7) > div > b"),
	};
	public By[] actModLocators = {
			By.cssSelector("#sc > div > div.panel.panel-default.panel-clear.panel-steps > div > ol > li:nth-child(3)"),
			By.cssSelector("#sc > div > div.panel.panel-default.panel-clear.panel-steps > div > h1"),
	};
	public By avvisoComunicaSmsChk = By.xpath("//input[@id='shallReadSMS']/../span");
	public By avvisoComunicaMailChk = By.xpath("//input[@id='shallReadEmail']/../span");
	public By avvisoEmissioneSmsChk = By.xpath("//input[@id='issueBillSMS']/../span");
	public By avvisoEmissioneMailChk = By.xpath("//input[@id='issueBillEmail']/../span");
	public By avvenutoPagamentoSmsChk = By.xpath("//input[@id='paymentNoticeSMS']/../span");
	public By avvenutoPagamentoMailChk = By.xpath("//input[@id='paymentNoticeEmail']/../span");
	public By prossimaScadenzaSmsChk = By.xpath("//input[@id='nextDeadlineSMS']/../span");
	public By prossimaScadenzaMailChk = By.xpath("//input[@id='nextDeadlineEmail']/../span");
	public By sollecitoSmsChk = By.xpath("//input[@id='requestPaymentSMS']/../span");
	public By sollecitoMailChk = By.xpath("//input[@id='requestPaymentEmail']/../span");
	public By consumiSmsChk = By.xpath("//input[@id='consumptionsSMS']/../span");
	public By attivabutton = By.xpath("//button[contains(text(),'ATTIVA')]");
	public By numberconfirmField = By.xpath("");
	public By consumiMailChk = By.xpath("//input[@id='consumptionsEmail']/../span");
	public By emailField = By.xpath("//label[starts-with(text(),'Email')]/following-sibling::input[@class='form-control']");
	public By confirmEmailField = By.xpath("//label[starts-with(text(),'Conferma email')]/following-sibling::input[@class='form-control']");
	public By phoneField = By.xpath("//label[starts-with(text(),'Numero di Cellulare')]/following-sibling::input[@class='form-control']");
	public By confirmPhoneField = By.xpath("//label[starts-with(text(),'Conferma cellulare')]/following-sibling::input[@class='form-control']");
	public By updateChk = By.xpath("//input[@id='aggiornadati']/../span");
	public By exitBtn = By.name("Esci");
	public By backBtn = By.name("Indietro");
	public By proseguiBtn = By.name("Prosegui");
	public By numeroInput = By.xpath("(//input[@name='field-mono'])[3]");
	public By numeroConfirmInput = By.xpath("(//input[@name='field-mono'])[4]");
	public By numeroConfirmInputNew = By.xpath("(//input[@name='field-mono'])[4]");
	public By confermaBtn = By.xpath("//button[@type='submit' and text()='Conferma']");
	public By bolletteMenuItem = By.cssSelector("#menuBoll > a");
	public By requiredFieldPhone = By.xpath("//label[starts-with(text(),'Numero di Cellulare')]/following-sibling::label[@class='has-error']");
	public By requiredFieldPhoneConfirm = By.xpath("//label[starts-with(text(),'Conferma cellulare')]/following-sibling::label[@class='has-error']");
	public By requiredFieldMail = By.xpath("//label[starts-with(text(),'Email')]/following-sibling::label[@class='has-error']");
	public By requiredFieldMailConfirm = By.xpath("//label[starts-with(text(),'Conferma email')]/following-sibling::label[@class='has-error']");
	public By step3OkMessage = By.id("messageOK");
	public By step3EndBtn = By.name("Fine");
	
	private By[] commonElements = {
			avvisoComunicaSmsChk,
			avvisoComunicaMailChk,
			avvisoEmissioneSmsChk,
			avvisoEmissioneMailChk,
			avvenutoPagamentoSmsChk,
			avvenutoPagamentoMailChk, 
			prossimaScadenzaSmsChk,
			prossimaScadenzaMailChk,
			sollecitoSmsChk,
			sollecitoMailChk,
			consumiSmsChk,
			consumiMailChk,
			emailField,
			phoneField,
			exitBtn,
			backBtn
	};
	private By[] step1Elements = {
			confirmEmailField,
			confirmPhoneField,
			updateChk,
			proseguiBtn
	};
	private By[] step2Elements = {
			confermaBtn
	};

	public InfoEESteps_BsnComponent(WebDriver driver) {
		super(driver);
	}
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	private By[] concatenateLocators(By[] firstArray, By[] secondArray) {
		int len1 = firstArray.length;
		int len2 = secondArray.length;
		int totLen = len1 +len2;
		By[] locators = new By[totLen];
		System.arraycopy(firstArray, 0, locators, 0, len1);
		System.arraycopy(secondArray, 0, locators, len1, len2);
		
		return locators;
	}
	
	public void checkStep1Elements() throws Exception {
		checkStep1(OperationType.MODIFICATION);
	}
	
	public void checkStep1ActivationElements() throws Exception {
		checkStep1(OperationType.ACTIVATION);
	}
	public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	private void checkStep1(OperationType opType) throws Exception {
//		verifyElementsArrayText(commonLocators, commonStrings);
		switch (opType) {
		case ACTIVATION:
			verifyElementsArrayText(commonLocators, commonStrings);
			verifyElementsArrayText(actModLocators, activationStrings);
			break;
		case MODIFICATION:
			verifyElementsArrayText(commonLocators, commonStringsModification);
			verifyElementsArrayText(actModLocators, modificationStrings);
			break;
		}
		By[] locators = concatenateLocators(changingTxtLocators, step1TxtLocators);
		verifyElementsArrayText(locators, step1Strings);
		for (int i = 0; i < step1Elements.length; i++) {
			verifyComponentVisibility(step1Elements[i]);
		}
		for (int i = 0; i < commonElements.length; i++) {
			verifyComponentVisibility(commonElements[i]);
		}
	}
	
	public void checkStep2Elements() throws Exception {
//		verifyElementsArrayText(commonLocators, commonStrings);
		By[] locators = concatenateLocators(changingTxtLocators, step2TxtLocators);
		verifyElementsArrayText(locators, step2Strings);
		for (int i = 0; i < step2Elements.length; i++) {
			verifyComponentVisibility(step2Elements[i]);
		}
		for (int i = 0; i < commonElements.length; i++) {
			verifyComponentVisibility(commonElements[i]);
		}
	}
	
	public void checkPhoneRequiredFields() throws Exception {
		By[] locators = {requiredFieldPhone, requiredFieldPhoneConfirm};
		checkRequiredFields(locators);
	}
	
	public void checkMailRequiredFields() throws Exception {
		By[] locators = {requiredFieldMail, requiredFieldMailConfirm};
		checkRequiredFields(locators);
	}
	
	private void checkRequiredFields(By[] locators) throws Exception {
		Color redColor = Color.fromString("#ff0f64");
		for (int i = 0; i < locators.length; i++) {
			verifyComponentVisibility(locators[i]);
			verifyComponentText(locators[i], requiredFieldString);
			verifyElementColor(locators[i], redColor);
		}
	}
	
	//----- Constants -----
	
	public final String[] step1Strings = {
			"Notifiche e dati",
			"Scegli su cosa vuoi essere informato.",
			"Inserisci i tuoi dati e i contatti su cui ricevere le notifiche.",
			"Email",
			"Conferma email",
			"Numero di Cellulare",
			"Conferma cellulare",
			"Aggiorna i dati di contatto"
	};
	public final String[] step2Strings = {
			"Riepilogo e conferma dati",
			"Sms o mail? Ecco dove preferisci ricevere le tue notifiche.",
			"Email",
			"Numero di Cellulare",
			"Il servizio verrà modificato sulla seguente fornitura:",
			"Tipo",
			"Numero Cliente / Alias",
			"Indirizzo di fornitura"
	};
	public final String[] commonStrings = {
			"AREA RISERVATA",
			"FORNITURE",
			"Avviso comunica lettura",
			"Avviso emissione bolletta",
			"Avvenuto Pagamento",
			"Avviso prossima scadenza",
			"Avviso sollecito pagamento",
			"Avviso Consumi"
	};
	
	
	
	public final String[] commonStringsModification = {
			"AREA RISERVATA",
			"FORNITURA",
			"Avviso comunica lettura",
			"Avviso emissione bolletta",
			"Avvenuto Pagamento",
			"Avviso prossima scadenza",
			"Avviso sollecito pagamento",
			"Avviso Consumi"
	};
	public final String requiredFieldString = "Campo Richiesto";
	public final String[] modificationStrings = {
			"MODIFICA INFO ENEL ENERGIA",
			"Modifica Info Enel Energia"
	};
	public final String[] activationStrings = {
			"ATTIVAZIONE INFO ENEL ENERGIA",
			"Attivazione Info Enel Energia"
	};
}
