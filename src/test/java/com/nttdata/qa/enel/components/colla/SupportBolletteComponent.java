package com.nttdata.qa.enel.components.colla;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupportBolletteComponent extends BaseComponent{
	WebDriver driver;
	SeleniumUtilities util;
	public String currentHandle ="";
	
	public By logoEnel = By.xpath("//img[@class='logoimg']");
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By supporto = By.xpath("//*[text()='Supporto']/ancestor::li");
	public By bolletteWeb = By.xpath("//div[@class='plan-overview_questions']/descendant::span[text()='I vantaggi della Bolletta web']");
	public By supportoPath = By.xpath(" //a[text()='Home']/ancestor::ul[@class='breadcrumbs component']");
	public By supportoTitle1 = By.xpath("//main[@id='main']/descendant::h1[contains(text(),'vantaggi della')]");
	public By supportoTitle2 = By.xpath("//div[@class='image-hero_content-wrapper text-center']/following-sibling::p[contains(text(),'La soluzione che')]");
	public By attivaBolleta = By.xpath("//div[@class='dropdown parbase']/parent::div[@class='article-content_inner']//button[contains(text(),'Come si attiva')]");
	public By attivaBolletaContent = By.xpath("//section[@class='anchor']/descendant::p[contains(text(),'Per attivare subito')]");
	public By cliccaqui = By.xpath("//div[@class='item-data']/descendant::a[text()='clicca qui']");
	public By numeroClienteField = By.xpath("//input[@id='Numero-cliente']");
	public By numeroClienteLabel = By.xpath("//input[@id='Numero-cliente']/preceding-sibling::label[text()='Numero cliente *']");
	public By codiceFiscaleInput = By.xpath("//input[@id='Codice-fiscale']");
	public By codiceFiscaleLabel = By.xpath("//input[@id='Codice-fiscale']/preceding-sibling::label[text()='Codice fiscale o Partita IVA *']");
	public By attivaButton = By.xpath("//div[@class='form-wrapper']/descendant::button[text()='Attiva']");
	public By numeroClientelFieldMsg = By.xpath("//input[@id='Numero-cliente']/following-sibling::span[contains(text(),'Campo obbligatorio')]");
	public By codiceFiscaleMsg = By.xpath("//input[@id='Codice-fiscale']/following-sibling::span[contains(text(),'Campo obbligatorio')]");
	public By numeroTelefonoInput = By.xpath("//*[@id='Numero-telefono']");
	public By inviaCodiceButton = By.xpath("//*[@id='sendCodeBtn']");
	
	
	public SupportBolletteComponent(WebDriver driver) throws Exception {
//		this.driver = driver;
//		util = new SeleniumUtilities(this.driver);
		super(driver);
		
	}
	
	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
//	public void waitForElementToDisplay (By checkObject) throws Exception{
//		WebDriverWait wait = new WebDriverWait (driver, 30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
//		
//	}
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
		
	}
	
//	 public void clickComponent(By clickableObject) throws Exception {
//			util.objectManager(clickableObject, util.scrollToVisibility, false);
//			util.objectManager(clickableObject, util.scrollAndClick);
//		}
	 
//	 public void verifyComponentExistence(By existingObject) throws Exception {
//			if (!util.exists(existingObject, 15))
//				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
//		}
	 
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.equalsIgnoreCase(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
				}
		
	public void checkForText(By checkObject, String property) throws Exception {
		
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);
		
	}
	
	public void SwitchTabNewWindow() throws Exception
	{
	   Set <String> windows = driver.getWindowHandles();
		 currentHandle = driver.getWindowHandle();
		
		for (String winHandle : windows) {
		   // Switch to child window
		//System.out.println("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
		 			
	}
	public static final String SUPPORTO_PATH = "Home/supporto/I vantaggi della Bolletta web";
	public static final String SUPPORTO_TITLE1 = "I vantaggi della bolletta Web";
	public static final String SUPPORTO_TITLE2 = "La soluzione che ti fa risparmiare tempo e denaro";
	public String ATTIVA_BOLLETTA_CONTENT = "Per attivare subito Bolletta Webclicca qui.Devi inserire solo il Codice Fiscale / Partita IVA e il numero cliente che trovi in alto a sinistra nella tua bolletta. Dopo"
											+ " l’attivazione, ti invieremo una e-mail; dovrai cliccare sul link per confermare l’indirizzo di posta elettronica.Controlla la tua cartella di posta indesiderata/spam, il"
											+ " nostro indirizzo mail potrebbe non essere ancora riconosciuto dal tuo account.";
	public static final String NUMEROCLIENTE_LABEL = "Numero cliente *";
	public static final String CODICEFISCALE_LABEL = "Codice fiscale o Partita IVA *";
	public static final String NUMEROCLIENTE_MSG = "Campo obbligatorio";
	public static final String CODICEFISCA_MSG = "Campo obbligatorio";
}
