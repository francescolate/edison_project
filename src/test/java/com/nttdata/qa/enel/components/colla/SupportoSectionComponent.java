package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupportoSectionComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By headerDescriptionSupporto=By.xpath("//div[@class='image-hero_content-wrapper']");
	public By ContrattiAndModulisticaLink=By.xpath("//div[@class='e-glossary-title parbase']//a[text()='Contratti e modulistica']");
	public By linksOfContrattiAndModulistica=By.xpath("//div[@class='vertical-tabs__content__inner article-content']");
    public By homeSupportoPath=By.xpath("//ul[@class='breadcrumbs component']");
    public By supportoTitle=By.xpath("//h1[@class='hero_title text--page-heading'][text()='Hai bisogno di aiuto?']");
    public By supportoSubtitle=By.xpath("//p[@class='image-hero_detail text--detail'][text()='Consulta le domande più frequenti e  scopri tutti i vantaggi dei servizi di Enel Energia.']");
    public By leftOperazioniVelociLabel=By.xpath("//div[@class='plan-overview_rate left-plan_rate']//h3[text()='operazioni veloci']");
    public By leftVisualizzaBollettaLabel=By.xpath("//div[@class='plan-overview_rate left-plan_rate']//p");
    public By guardaSubitoButton=By.xpath("//div[@class='plan-overview_rate left-plan_rate']//a//span[text()='guarda subito']");
    public By centralOperazioniVelociLabel=By.xpath("//div[@class='plan-overview_rate center-plan_rate']//h3[text()='operazioni veloci']");
    public By centralPagaBollettaLabel=By.xpath("//div[@class='plan-overview_rate center-plan_rate']//p");
    public By pagaSubitoButton=By.xpath("//div[@class='plan-overview_rate center-plan_rate']//a//span[text()='paga subito']");
    public By rightOperazioniVelociLabel=By.xpath("//div[@class='plan-overview_rate right-plan_rate']//h3[text()='operazioni veloci']");
    public By rightComunicaLabel=By.xpath("//div[@class='plan-overview_rate right-plan_rate']//p");
    public By comunicaButton=By.xpath("//div[@class='plan-overview_rate right-plan_rate']//a//span[text()='comunica adesso']");
    public By domandefrequentiLabel=By.xpath("//div[@class='plan-overview_questions']//h2[text()='Le domande più frequenti']");
    public By questionsList=By.xpath("//div[@class='plan-overview_questions']//ul//li");
    public By leggiFaqButton=By.xpath("//a[@href='/it/supporto/faq/faq-enel-energia?zoneid=supporto-hero']");
    public By serviziSectionTitle=By.xpath("//div[@class='vertical-tabs__nav__inner']//h2[text()='servizi']");
    public By serviziList=By.xpath("//div[@class='vertical-tabs__nav__inner']//ul//div//li");
    public By gestioneEnelSection=By.xpath("//div[@id='promo_offert_results']");
    public By gestioneEnelimage=By.xpath("//img[@src='/content/enel-it/it/supporto/_jcr_content/client-par/promo_offert_copy/promo-par/image.img.jpg/1607099726297.jpg']");
    public By storiedisuccessoSection=By.xpath("//section[@data-module='related-content']/p");
    public By sezionestoriedisuccesso=By.xpath("//section[@data-module='related-content']//a");
    public By pageTitle = By.xpath("//h1[text()='Hai bisogno di aiuto?']");
    public By titleSubText = By.xpath("//p[contains(text(),'Consulta le domande')]");
	public By searchIcon = By.xpath("//span[@class='icon-search-small']");
	public By searchEntry = By.xpath("//input[@type='search']");
	public By submit = By.xpath("//div[@class='dotcom-search-form__submit__wrap']//button[@type='submit']");
	public By leggiLeFAQ = By.xpath("//main[@id='main']//div[@class='btn-cta__wrapper']/a[@class='btn-cta btn-cta--clear']");
	public By contattaci = By.xpath("//a[contains(text(),'Contattaci')]");
	
    public String domande []={"STRUMENTI Come entrare in contatto con Enel Energia","NORMATIVA A cosa serve la App Enel Energia","STRUMENTI Come funziona la fattura elettronica","STRUMENTI Conciliazioni e risoluzione delle controversie","STRUMENTI Come fare l'autolettura","BOLLETTA I vantaggi della Bolletta web","BOLLETTA Come pagare la bolletta","INFORMAZIONI Variazione prezzi luce e gas: cosa cambia in bolletta?","PAGAMENTI Crollo ponte Morandi Genova","NORMATIVA Indagine sulla qualità dei call center 2018","INFORMAZIONI Sisma: informazioni e aggiornamenti"};
//    public String domande []={"A cosa serve la App Enel Energia","Come funziona la fattura elettronica","Conciliazioni e risoluzione delle controversie","Come fare l'autolettura","I vantaggi della Bolletta web","Come pagare la bolletta","Variazione prezzi luce e gas: cosa cambia in bolletta?","Crollo ponte Morandi Genova","Indagine sulla qualità dei call center 2018","Sisma: informazioni e aggiornamenti"};
    public String servizi []={"Bolletta e pagamenti","Contratti e modulistica","Normativa e comunicazioni","Scopri le novità","Strumenti e servizi digitali","Scopri le novità di Enel Energia"}; //NORMATIVA A cosa serve la App Enel Energia
//    public String storie []={"Bolletta Web","My Energy","Come fare l'autolettura"};
  public String storie []={"Bolletta Web","","Come fare l'autolettura"};
    public String  linkSUPPORTO="it/supporto";
	public static final String PageTitle = "Hai bisogno di aiuto?";
	public static final String PageTitleSubText = "Consulta le domande più frequenti e scopri tutti i vantaggi dei servizi di Enel Energia.";

	public static final String PageUrl = "https://www-coll1.enel.it/it/supporto";
    
    public SupportoSectionComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
    
    public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto web con xpath " + existingObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
	}
	 
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkHeaderPageSupporto(By checkObject, By path,By title, By subtitle) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		String pathDescription=driver.findElement(path).getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	
		
		
		if (!pathDescription.contains("HOME / SUPPORTO"))
			throw new Exception("l'oggetto web path con xpath " + path + " non esiste.");
		if (!util.exists(title, 15))
			throw new Exception("l'oggetto web titolo con xpath " + title + " non esiste.");
		if (!util.exists(subtitle, 15))
			throw new Exception("l'oggetto web sottotitolo con xpath " + subtitle + " non esiste.");	
	}
	
	public void checklinksOnContrattiAndModulisticaSection(By checkObject)throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	    String description=driver.findElement(checkObject).getText();
	    description= description.replaceAll("(\r\n|\n)", " ");	
	   
	    if (!description.contains("SOS luce e gas Autolettura App Enel Gestisci i tuoi dati Modifica taglia a consumi Guida ai servizi e tutorial Servizi sms Modulistica Subentro Voltura Prima attivazione Modifica potenza e tensione Disattivazione Carica documenti Piano salva black out Faq Area clienti Assicurazione clienti finale Contattaci Conciliazioni e risoluzione delle controversie Guida alla bolletta luce e gas"))
	    	throw new Exception("sulla pagina di supporto nella sezione 'Contratti e Modulistica' non sono presenti i links:SOS luce e gas; Autolettura; App Enel; Gestisci i tuoi dati; Modifica taglia a consumi; Guida ai servizi e tutorial; Servizi sms; Modulistica; Subentro; Voltura; Prima attivazione; Modifica potenza e tensione; Disattivazione; Carica documenti; Piano salva black out; Faq Area clienti; Assicurazione clienti finale; Contattaci; Conciliazioni e risoluzione delle controversie; Guida alla bolletta luce e gas");
	}
	
	public void checkOperazioniVelociSections(By headerlabel,By text,By button,String description) throws Exception {
		util.objectManager(button, util.scrollToVisibility, false);
		String textDescription=driver.findElement(text).getText();
		if (!util.exists(headerlabel, 15))
			throw new Exception("la label nella sezione 'Operazioni veloci' con xpath " + headerlabel + " non esiste.");
		if (!util.exists(button, 15))
			throw new Exception("l'oggetto web button  nella sezione 'Operazioni veloci' con xpath " + button + " non esiste.");
		if (!textDescription.contentEquals(description))
			throw new Exception("l'oggetto web label con xpath " + text + " e testo "+description+" non esiste.");
	}
	
	
	public void checkFrequentQuestions(String [] domande, By checkobject) throws Exception {
		List<WebElement> q = util.waitAndGetElements(checkobject);
		
		for(int x = 0;x<domande.length;x++) {
					
					String a = util.waitAndGetElement(q.get(x),By.xpath("./a")).getAttribute("innerText"); 
					a= a.replaceAll("(\r\n|\n)", " ");
					
			        if(!a.contentEquals(domande[x].trim()))
			        	throw new Exception("Nella pagina di 'supporto' nella sezione 'Domande Frequenti' e' cambiato il testo di una delle domande: il testo della domanda visualizzato e' "+a+"; quello atteso e' "+domande[x]);
		}
	}
	
	
	public void checkServiziList(String [] servizi,By checkObject) throws Exception {
		List<WebElement> servicesList = util.waitAndGetElements(checkObject);
		for(int x = 0;x<servizi.length;x++) {
			
			String a = util.waitAndGetElement(servicesList.get(x),By.xpath("./a")).getAttribute("innerText"); 
			a= a.replaceAll("(\r\n|\n)", " ");
			
	        if(!a.contentEquals(servizi[x].trim()))
	        	throw new Exception("Nella pagina di 'supporto' nella sezione 'Servizi' e' cambiato il testo di uno dei servizi: il testo del servizio visualizzato e' "+a+"; quello atteso e' "+servizi[x]);
            }
	
	}
	
	
	
	public void checkgestioneEnelSection(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
				String description=driver.findElement(checkObject).getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				 if (!description.contains("GESTIONE ENEL Gestisci la tua fornitura con l'app di Enel Con l'app di Enel puoi gestire il tuo contratto luce e gas da smartphone o tablet. Inoltre, grazie alla Skill ufficiale di Enel Energia per Alexa, potrai gestire le tue offerte e tutti i servizi di Enel Energia in modo molto più intuitivo, potrai ad esempio cambiare piano per ENEL ONE o monitorare i tuoi consumi usando semplicemente la tua voce! Scaricala ora"))
						throw new Exception("sulla pagina web supporto non e' presente la sezione GESTIONE ENEL con testo di descrizione: 'Gestisci la tua fornitura con l'app di Enel','Con l'app di Enel puoi gestire il tuo contratto luce e gas da smartphone o tablet. Scaricala ora!'");
					
	}
	
	
	public void checkstorieDiSuccessoSection(By checkObject,String [] storie) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		String titleSection=driver.findElement(checkObject).getText();
		
		List<WebElement> storieDiSuccessosectionCards = util.waitAndGetElements(By.xpath("//section[@data-module='related-content']//a//div[@class='related-content-card_inner']"));
		
		if (!titleSection.contentEquals("Storie di successo"))
			throw new Exception("nella pagina di supporto NON e' presente una sezione con titolo 'Storie di successo'");
		
		if (storieDiSuccessosectionCards.size()==storie.length) {
			
			for (int i = 0; i < storie.length; i++) { 
				String description=driver.findElement(By.xpath("//section[@data-module='related-content']//a["+(i+1)+"]//div[@class='related-content-card_inner']")).getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				 if(!description.contentEquals(storie[i].trim()))
			       	throw new Exception("Nella pagina di 'supporto' nella sezione 'Storie di successo' e' cambiato la descrizione di una delle Cards: il testo della card visualizzato e' "+description+"; quello atteso e' "+storie[i]);
		          
			     }
		}
		else throw new Exception("il numero della cards presenti nella pagina di supporto nella sezione 'Storie di successo' e' cambiato: le card attese sono "+storie.length+"; quelle presenti in pagina sono "+storieDiSuccessosectionCards.size());
	}

   }
