package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class LoginLogoutEnelCollaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By logoEnel = By.xpath("//img[@alt='enel logo']");
	public By logoEnel_old = By.xpath("//img[@class='logoimg']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By iconUserButton = By.xpath("//span[@class='icon-user']/parent::button[@class='dotcom-header__btn btn-user-open']");
	public By closeIcon = By.xpath("//div[@id='fsa-close-button']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonLoginAccedi2 = By.xpath("//*[contains(text(), 'ACCEDI')]");
	public By buttonAcceptCookie= By.xpath("//button[@id='truste-consent-button']");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By buttonImpostazioni = By.xpath("//*[@id='truste-show-consent']");
	public By trusteConsentText = By.xpath("//*[@id='truste-consent-text']");
	public By trusteCookieButton = By.xpath("//a[@id='truste-cookie-button']");
	public By buttonAccettaEng = By.xpath("//*[@id='accept-btn']");
	public By registratiButton = By.xpath("//a[text()='Registrati']");
    public By loginSuccessful= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
    public By loginBSNSuccessful= By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
    // public By buttonPopupLogin=By.xpath("//button[@class='remodal-close popupLogin-close']");
    public By logout=By.id("disconnetti");
    public By accountButton=By.xpath("//button[@id='user']");
    public By esciLink=By.xpath("//a[@id='tr_disconnetti']");
    public By recoverPasswordBtn = By.xpath("//*[text()='Recupera Password']");
    public By recoverUsernameBtn = By.cssSelector("#formlogin > div.login_recovers > div > span:nth-child(2) > a");
    public By changetext = By.xpath("//*[@id='formlogin']/div[contains(@class, 'help-message')]");
    public By accessProblemsBtn = By.xpath("//*[@id='formlogin']//a[@href='/it/supporto/faq/faq-area-clienti']");
    public By googleAccessBtn = By.cssSelector("#formlogin > div.login-social-btn.border-top-bottom-gray > div:nth-child(1) > button");
    public By facebookAccessBtn = By.cssSelector("#formlogin > div.login-social-btn.border-top-bottom-gray > div:nth-child(2) > button");
    public By noAccountLabel = By.xpath("//p[text()='Non hai ancora un account?']");
    public By noAccountLabelNew = By.xpath("//p[text()='Non hai ancora creato il tuo Profilo Unico?']");
    public By registerBtn = By.xpath("//a[@href='/it/registrazione']");
    //public By accessProblemsLink = By.xpath("//a[text()='Problemi di accesso?']");
    public By accessProblemsLink = By.xpath("//p[text()='Se hai problemi di accesso ']");
    public By obligatoryUsernameLabel = By.xpath("//p[text()='Username obbligatoria']");
    public By obligatoryPasswordLabel = By.xpath("//p[text()='Password obbligatoria']");
    public By areaClientiCasa = By.xpath("//*[contains(text(), 'Area Clienti Casa')]/following-sibling::*//a[text()='Entra']");
    public By areaClientiImpresa = By.xpath("//*[contains(text(), 'Area Clienti Impresa')]/following-sibling::*//a[text()='Entra']");
    public By areaClientiCondizioni = By.xpath("//label[contains(@data-title, 'Condizioni')]/preceding-sibling::label[@class='uIdLabel custom-label']");
    public By areaClientiPrivacy = By.xpath("//label[contains(@data-title, 'Informativa')]/preceding-sibling::label[@class='uIdLabel custom-label']");
    public By terminiECondizioniButton = By.xpath("//button[text()='Continua']");
    public By loginPopcloseicon=By.xpath("//button[@class='remodal-close popupLogin-close']");
    public By loginPopupText = By.xpath("//div[@id='imgUnica']/p");
    //public By accediAMyEnelLabel=By.xpath("//div[@class='login-details']//h1[@aria-label='accedi a myenel']");
    public By entr_LIKE_RES = By.xpath("//div[@class='item item01']/div[2]//li[2]//a");
    public By accediAMyEnelLabel=By.xpath("//div[@class='login-details']//h1[@class='login-details-title']");
	public By resEntra = By.xpath("//li[@class='col-sm-6 margin-bottom-30'][1]/div[@class='panel-group']//div[@class='panel-heading text-center bsnBlue']/p[@class='margin-top-60']/a[@class='acb btn btn-primary btn-white']");		
	public By homeFullscreenAlert = By.xpath("//div[@id='home-fullscreen-alert']");
	public By homeFullscreenAlertCloseButton = By.xpath("//div[@id='fsa-close-button']");
	public By testoCentrale=By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
    public By testoSezioneForniture=By.xpath("//p[.='Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.']");
    
    public By popupLoginCloseButton = By.xpath("//button[@class='remodal-close popupLogin-close']");
    
    public By cookiePolicyAcceptButton = By.xpath("//*[@id='accept-btn']");
    
    public String textCookie = "Usiamo cookies e web tracker per migliorare la tua esperienza online sul nostro sito, consentirti di salvare informazioni, fornire funzionalità di social media (Facebook, Instagram ecc.), personalizzare i contenuti e gli annunci che vedi in base ai tuoi interessi (sul nostro sito e su quello dei partners). I cookies possono, inoltre, aiutarci a comprendere le modalità di utilizzo del nostro sito e migliorarne la funzionalità.";
    public By loginPageTitle = By.xpath("//form[@id='formlogin']//h1");
    public By cliccaQui = By.xpath("//a[text()='clicca qui']");
    
    public LoginLogoutEnelCollaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			System.out.println(url);
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	
	public void launchCorporateLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals("")){
//			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+java.net.URLEncoder.encode(Costanti.WP_Corporate_BasicAuth_Password)+"@");
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");

			System.out.println(url);
		}
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.click);
		
	}
	
	public void jsClickComponent(By clickableObject) throws Exception {
		util.jsClickElement(clickableObject);
		
	}
	
	public boolean verifyComponentExistence(By by, int seconds) throws Exception{
		return util.verifyExistence(by, seconds);
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//		if (!util.exists(oggettoEsistente, 15))
//			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		*/
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			//System.out.println(message);
			throw new Exception(message);
		}
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
			
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found." + " Text found= " +  actualtext);
	}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		if (url.contains("https://"))
			url = url.substring(8);
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
	
	public void checkUrl2(String url) throws Exception{
		String link=driver.getCurrentUrl();
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
}
