package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InfoEEModBsnExitPopUpComponent extends BaseComponent {
	public By title = By.cssSelector("#modal-boxed-attenzione > div > div > div > h4");
	public By subtitle = By.cssSelector("#modal-boxed-attenzione > div > div > div > p");
	public By cancelXBtn = By.cssSelector("#modal-boxed-attenzione > div > div > button");
	public By tornaProcessoBtn = By.cssSelector("#closeAbort");
	public By exitBtn = By.cssSelector("#sendAbort");
	
	public By bolletePath = By.xpath("(//ol[@class='breadcrumb'])[1]");
	public By bolletePageTitle = By.xpath("(//ol[@class='breadcrumb'])[1]/following-sibling::h1[text()='Bollette']");

	public InfoEEModBsnExitPopUpComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkStringsAndElements() throws Exception {
		By[] stringsLocators = {title, subtitle};
		verifyElementsArrayText(stringsLocators, popupStrings);
		verifyComponentVisibility(cancelXBtn);
		verifyComponentVisibility(tornaProcessoBtn);
		verifyComponentVisibility(exitBtn);
	}
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		/*System.out.println("Normalized:\n"+weText);
		System.out.println(text);*/
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within displaying Text item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	//----- Constants ------
	
	private final String[] popupStrings = {
			"Attenzione",
			"Se uscirai dal processo perderai le modifiche effettuate."
	};


	
}
