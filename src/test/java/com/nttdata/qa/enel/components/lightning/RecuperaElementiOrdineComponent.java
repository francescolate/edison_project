package com.nttdata.qa.enel.components.lightning;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class RecuperaElementiOrdineComponent {
	WebDriver driver;
	SeleniumUtilities util;

	//	public final By IdCrmCommodityELE = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/../../..//li/strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'ELETTRICO')]/ancestor::ul/li[1]/a");
	//	public final By IdCrmCommodityELE = By.xpath("div/div[2]/div/ul/li/strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'ELETTRICO')]/ancestor::ul/li[1]/a");
	//	public final By IdCrmCommodityELE = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'ELETTRICO')]/ancestor::ul/li[1]/a");
	//	public final By IdCrmCommodityGAS = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'GAS')]/ancestor::ul/li[1]/a");
	//	public final By labelPOD=By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//span[contains(text(),'POD')]/ancestor::li/strong");
	//	public final By IdCrmCommodityGAS = By.xpath("div/div[2]/div/ul/li/strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'GAS')]/ancestor::ul/li[1]/a");
	//	public final By spanElementiOrdine = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]");
	public final By spanElementiOrdine = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]");
	public final String apriSezioneElemento="div/div/section/div/lightning-icon[contains(@class,'show')]";
//	public final By aORHOrder = By.xpath("//a[contains(text(),'ORH')]");
	//	public final By spanElementiOrdine = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]");
	public RecuperaElementiOrdineComponent(WebDriver driver ) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
	}
	public void aspettaCreazioneOrdini(By spanElementiOrdine,int numOrdiniAttesi) throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		int tentativi = 8;
		int secondi = 30;
		int time = tentativi * secondi;
		boolean trovato=false;
		List<WebElement> listOfElements = driver.findElements(spanElementiOrdine);
		//		while(listOfElements.size()!=numOrdiniAttesi && tentativi<=0){
		while(!trovato && tentativi-- > 0){
			Logger.getLogger("").log(Level.INFO, "Numero elementi:"+listOfElements.size()+" Tentativo n°"+tentativi);
			if(listOfElements.size()==numOrdiniAttesi){
				trovato=true;
			}
			else{
				driver.navigate().refresh();
				TimeUnit.SECONDS.sleep(secondi);
				listOfElements = driver.findElements(spanElementiOrdine);
			}
		}
		if(!trovato){
			throw new Exception("Dopo aver atteso "+time+" il numero di ordini in pagina risultano essere:"+listOfElements.size()+". Il numero atteso è':"+numOrdiniAttesi);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}
	
	public void pressJavascript(By oggetto) throws Exception {
		WebElement element = util.waitAndGetElement(oggetto);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}
	
	public void pressJavascript(WebElement el,By oggetto) throws Exception {
		WebElement element = util.waitAndGetElement(el,oggetto);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	//	public String recuperaIdRichiestaCrm(By spanElementiOrdine,By commodity,int indice) throws Exception {
	//		//Verificare se sestire il salvataggio di POD multipli
	//		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	//		List<WebElement> listOfElements = driver.findElements(spanElementiOrdine);
	////		Logger.getLogger("").log(Level.INFO, "Numero elementi:"+listOfElements.size());
	//		WebElement elem=listOfElements.get(indice);
	//		String style=elem.findElement(By.xpath("div/div[2]")).getAttribute("class");
	////		String style=driver.findElement(spanElementiOrdine).findElement(By.xpath("div/div[2]")).getAttribute("class");
	//		if(style.contains("hide")){
	//			util.objectManager(spanElementiOrdine,util.scrollAndClick);
	//		}
	//		String idRichiesta=elem.findElement(commodity).getText();
	//		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	//		return idRichiesta;
	//
	//	}

	public String recuperaIdRichiestaCrm(String pod,String pratica) throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Elementi Ordine' in corrispondenza del pod cercato
		By spanElemOrdine= By.xpath("//strong[@title='"+pod+"']/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiOrdine=util.waitAndGetElement(spanElemOrdine);
		TimeUnit.SECONDS.sleep(5);
		//Apertura sezione se chiusa
		String style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("hide")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiOrdine);
			TimeUnit.SECONDS.sleep(2);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}

		//recupero dell'OI
//		By labelIdRichiestaCRM = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'"+pod+"')]/ancestor::ul/li[1]/a");
		By labelIdRichiestaCRM = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//*[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'"+pod+"')]//ancestor::ul/li/strong[contains(text(),'"+pratica+"')]/ancestor::ul/li[1]/a");
		TimeUnit.SECONDS.sleep(5);
		util.scrollToElement(driver.findElement(labelIdRichiestaCRM));
		TimeUnit.SECONDS.sleep(2);
		String idRichiesta=driver.findElement(labelIdRichiestaCRM).getText();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);	
		
		//Chiusura sezione se aperta
		style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("show")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}
		
		return idRichiesta;

	}
	public String recuperaIdRichiestaCrmAllaccio() throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Elementi Ordine' in corrispondenza del pod cercato
		By spanElemOrdine= By.xpath("//div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiOrdine=util.waitAndGetElement(spanElemOrdine);
		//Apertura sezione se chiusa
		String style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("hide")){
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}

		//recupero dell'OI
		By labelIdRichiestaCRM = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li[1]/a");
		TimeUnit.SECONDS.sleep(5);
		util.scrollToElement(driver.findElement(labelIdRichiestaCRM));
		String idRichiesta=driver.findElement(labelIdRichiestaCRM).getText();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);	
		
		//Chiusura sezione se aperta
		style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("show")){
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}
		
		return idRichiesta;

	}
	
	public String recuperaIdRichiestaCrmSwitchAttivo(String pod) throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Elementi Ordine' in corrispondenza del pod cercato
		By spanElemOrdine= By.xpath("//strong[@title='"+pod+"']/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiOrdine=util.waitAndGetElement(spanElemOrdine);
		//Apertura sezione se chiusa
		String style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("hide")){
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}

		//recupero dell'OI
		By labelIdRichiestaCRM = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'"+pod+"')]/ancestor::ul/li[1]/a");
		TimeUnit.SECONDS.sleep(5);
		util.scrollToElement(driver.findElement(labelIdRichiestaCRM));
		String idRichiesta=driver.findElement(labelIdRichiestaCRM).getText();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);	
		
		//Chiusura sezione se aperta
		style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("show")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}
		
		return idRichiesta;

	}
	

	public String dettaglioEsitiOrdine(String idRichiestaCRM,String pratica) throws Exception {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Elementi Ordine' in corrispondenza dell'ordine passato in input
		By spanElemOrdine= By.xpath("//a[contains(text(),'"+idRichiestaCRM+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiOrdine=util.waitAndGetElement(spanElemOrdine);
		String style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("hide")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}

		//recupero gli esiti di R2D,SAP,SEMPRE
		String esiti="";
		By labelStatoR2D = By.xpath("//a[contains(text(),'"+idRichiestaCRM+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/*[contains(text(),'"+pratica+"')]/ancestor::ul/li/span[contains(text(),'Stato R2D')]/ancestor::li/*[@title]");
		By labelStatoSapIsu = By.xpath("//a[contains(text(),'"+idRichiestaCRM+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/*[contains(text(),'"+pratica+"')]/ancestor::ul/li/span[contains(text(),'Stato SAP-ISU')]/ancestor::li/*[@title]");
		By labelStatoUDB = By.xpath("//a[contains(text(),'"+idRichiestaCRM+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/*[contains(text(),'"+pratica+"')]/ancestor::ul/li/span[contains(text(),'Stato UDB')]/ancestor::li/*[@title]");
		By labelStatoSapSd = By.xpath("//a[contains(text(),'"+idRichiestaCRM+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/*[contains(text(),'"+pratica+"')]/ancestor::ul/li/span[contains(text(),'Stato SAP-SD')]/ancestor::li/*[@title]");
		By labelStatoSempre = By.xpath("//a[contains(text(),'"+idRichiestaCRM+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/*[contains(text(),'"+pratica+"')]/ancestor::ul/li/span[contains(text(),'SEMPRE')]/ancestor::li/*[@title]");
		String statoR2D=driver.findElement(labelStatoR2D).getText();
		String statoSapIsu=driver.findElement(labelStatoSapIsu).getText();
		String statoUDB=driver.findElement(labelStatoUDB).getText();
		String statoSapSD=driver.findElement(labelStatoSapSd).getText();
		String statoSempre=driver.findElement(labelStatoSempre).getText();

		esiti="Dettaglio Esiti provisioning per Ordine OI:"+idRichiestaCRM+":\n"
				+ "Stato R2D:"+statoR2D+" \n"
				+ " Stato SAP-ISU:"+statoSapIsu+"\n"
				+ " Stato UBD:"+statoUDB+"\n"
				+ " Stato SAP-SD:"+statoSapSD+"\n"
				+ " Stato Sempre:"+statoSempre+".";
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		//Chiusura sezione se aperta
		style=elemElementiOrdine.findElement(By.xpath("div/div[2]")).getAttribute("class");
		if(style.contains("show")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiOrdine);
			elemElementiOrdine.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}
		
		return esiti;

	}
	
	
	public String recuperaPodDaTabellaRichiesta() throws Exception{
		By spanPod= By.xpath("//*[contains(text(),'Elemento Richiesta')]/ancestor::article//table//td[2]/span");
		WebElement elemElementiRichiesta=util.waitAndGetElement(spanPod);
		return elemElementiRichiesta.getText().trim();
	}
	
	
	public String recuperaIdRichiestaDaTabellaConPiuRichieste(String pod) throws Exception{
		util.objectManager(By.xpath("//span[contains(text(),'Navigazione processo')]/ancestor::article//div[contains(text(),'Elementi della Richiesta')]"), util.scrollAndClick);
		WebElement elemElementiRichiesta=util.waitAndGetElement(By.xpath("//span[contains(text(),'Navigazione processo')]/ancestor::article//*[contains(text(),'"+pod+"')]/ancestor::ul//a[contains(text(),'OI')]"));
		return elemElementiRichiesta.getText().trim();
	}
	
	public String recuperaIdRichiesta(String pod) throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Elementi Richiesta' in corrispondenza del pod cercato
		By spanElemRichiesta= By.xpath("//strong[@title='"+pod+"']/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiRichiesta=util.waitAndGetElement(spanElemRichiesta);
		//click per prendere focus su 'Elementi Richiesta'
		util.scrollToElement(elemElementiRichiesta);
		pressJavascript(elemElementiRichiesta,By.xpath(apriSezioneElemento));
		//Apertura sezione se chiusa
		String style=elemElementiRichiesta.findElement(By.xpath("div/div[contains(@class,'slds-p-around-medium slds')]")).getAttribute("class");
		if(style.contains("hide")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiRichiesta);
			TimeUnit.SECONDS.sleep(2);
			pressJavascript(elemElementiRichiesta,By.xpath(apriSezioneElemento));
			TimeUnit.SECONDS.sleep(5);
		}

		//recupero dell'OI
//		By labelIdRichiestaCRM = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'"+pod+"')]//ancestor::ul/li/strong[contains(text(),'"+pratica+"')]/ancestor::ul/li[1]/a");
		By labelIdRichiestaCRM = By.xpath("//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'"+pod+"')]//ancestor::ul/li/strong[contains(text(),'')]/ancestor::ul/li[1]/a");
		TimeUnit.SECONDS.sleep(5);
		util.scrollToElement(driver.findElement(labelIdRichiestaCRM));
		String idRichiesta=driver.findElement(labelIdRichiestaCRM).getText();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);	
		
		//Chiusura sezione se aperta
		style=elemElementiRichiesta.findElement(By.xpath("div/div[contains(@class,'slds-p-around-medium slds')]")).getAttribute("class");
		if(style.contains("show")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiRichiesta);
			pressJavascript(elemElementiRichiesta,By.xpath(apriSezioneElemento));
			TimeUnit.SECONDS.sleep(5);
		}
		
		return idRichiesta;

	}
	
	public String dettaglioEsitiRichiesta(String idRichiesta) throws Exception {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Elementi Richiesta' in corrispondenza dell'ordine passato in input
		By spanElemRichiesta= By.xpath("//a[contains(text(),'"+idRichiesta+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiRichiesta=util.waitAndGetElement(spanElemRichiesta);
		util.scrollToElement(elemElementiRichiesta);
		//elemElementiRichiesta.findElement(By.xpath(apriSezioneElemento)).click();
		pressJavascript(elemElementiRichiesta,By.xpath(apriSezioneElemento));
		//Apertura sezione se chiusa
		String style=elemElementiRichiesta.findElement(By.xpath("div/div[contains(@class,'slds-p-around-medium slds')]")).getAttribute("class");
		if(style.contains("hide")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiRichiesta);
			//elemElementiRichiesta.findElement(By.xpath(apriSezioneElemento)).click();
			pressJavascript(elemElementiRichiesta,By.xpath(apriSezioneElemento));

			TimeUnit.SECONDS.sleep(5);
		}

		//recupero gli esiti di R2D,SAP,SEMPRE
		String esiti="";
		By labelStatoR2D = By.xpath("//a[contains(text(),'"+idRichiesta+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]//span[contains(text(),'Stato R2D')]/ancestor::li/*[@title]");
		By labelStatoSapIsu = By.xpath("//a[contains(text(),'"+idRichiesta+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]//span[contains(text(),'Stato SAP-ISU')]/ancestor::li/*[@title]");
		By labelStatoUDB = By.xpath("//a[contains(text(),'"+idRichiesta+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]//span[contains(text(),'Stato UDB')]/ancestor::li/*[@title]");
		By labelStatoSapSd = By.xpath("//a[contains(text(),'"+idRichiesta+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]//span[contains(text(),'Stato SAP-SD')]/ancestor::li/*[@title]");
		By labelStatoSempre = By.xpath("//a[contains(text(),'"+idRichiesta+"')]/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-tree__item-label slds-truncate slds-m-top_small']/ancestor::div[@class='slds-card__body'][1]//span[contains(text(),'SEMPRE')]/ancestor::li/*[@title]");
		String statoR2D=driver.findElement(labelStatoR2D).getText();
		String statoSapIsu=driver.findElement(labelStatoSapIsu).getText();
		String statoUDB=driver.findElement(labelStatoUDB).getText();
		String statoSapSD=driver.findElement(labelStatoSapSd).getText();
		String statoSempre=driver.findElement(labelStatoSempre).getText();

		esiti="Dettaglio Esiti provisioning per Richiesta OI:"+idRichiesta+":\n"
				+ "Stato R2D:"+statoR2D+" \n"
				+ " Stato SAP-ISU:"+statoSapIsu+"\n"
				+ " Stato UBD:"+statoUDB+"\n"
				+ " Stato SAP-SD:"+statoSapSD+"\n"
				+ " Stato Sempre:"+statoSempre+".";
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
		//Chiusura sezione se aperta
		style=elemElementiRichiesta.findElement(By.xpath("div/div[contains(@class,'slds-p-around-medium slds')]")).getAttribute("class");
		if(style.contains("show")){
//			util.objectManager(spanElemOrdine, util.scrollAndClick);
			util.scrollToElement(elemElementiRichiesta);
			//elemElementiRichiesta.findElement(By.xpath(apriSezioneElemento)).click();
			pressJavascript(elemElementiRichiesta,By.xpath(apriSezioneElemento));
			TimeUnit.SECONDS.sleep(5);
		}
		
		return esiti;

	}
	public String recuperaIdOffertaCrm(String pod,String pratica) throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		//Viene aperto il dettaglio 'Offerta' in corrispondenza del pod cercato
		By spanElemOfferta= By.xpath("//strong[@title='"+pod+"']/ancestor::div[@class='slds-form-element']//div[contains(text(),'Elementi dell')and contains(text(),'Offerta')]/ancestor::div[@class='slds-card__body'][1]");
		WebElement elemElementiOfferta=util.waitAndGetElement(spanElemOfferta);
		TimeUnit.SECONDS.sleep(5);
		//Apertura sezione se chiusa
		String style=elemElementiOfferta.findElement(By.xpath("div[2]/div[2]")).getAttribute("class");
//		System.out.println(style);
		if(style.contains("hide")){
			util.scrollToElement(elemElementiOfferta);
			TimeUnit.SECONDS.sleep(2);
			elemElementiOfferta.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}

		//recupero dell'OI
		By labelIdOffertaCRM = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Offerta')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'Commodity')]/ancestor::ul/li/strong[contains(text(),'"+pod+"')]//ancestor::ul/li/strong[contains(text(),'"+pratica+"')]/ancestor::ul/li[1]/a");
		TimeUnit.SECONDS.sleep(5);
		util.scrollToElement(driver.findElement(labelIdOffertaCRM));
		TimeUnit.SECONDS.sleep(2);
		String idOfferta=driver.findElement(labelIdOffertaCRM).getText();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);	
		
		//Chiusura sezione se aperta
		style=elemElementiOfferta.findElement(By.xpath("div[2]/div[2]")).getAttribute("class");
		if(style.contains("show")){
			util.scrollToElement(elemElementiOfferta);
			elemElementiOfferta.findElement(By.xpath(apriSezioneElemento)).click();
			TimeUnit.SECONDS.sleep(5);
		}
		
		return idOfferta;

	}
	
	public String recuperaORHOrdine(String pod) throws Exception {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 By aORHOrder = By.xpath("//div[contains(text(),'Elementi dell')and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']/ancestor::div[@class='slds-card__body'][1]//strong[contains(text(),'"+pod+"')]/ancestor::article[1]//a[contains(text(),'ORH')]");
		//Viene recuperato l'ORH dell'ordine
		String ORHOrdine=driver.findElement(aORHOrder).getText();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		return ORHOrdine;
	}
}
