package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class SezioneMercatoComponent extends BaseComponent {

    WebDriver driver;
    SeleniumUtilities util;

    public By buttonConfermaSezioneMercato = By.xpath("//h2[text()='Selezione Mercato']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonConfermaSezioneCausale = By.xpath("//h2[text()='Selezione Causale']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonAnnullaSezioneMercato = By.xpath("//h1[text()='Subentro']/ancestor::div[@role='tabpanel']//button[text()='Annulla' and (@name='Cancel')][1]");
    public By TextPopupAnnullaSezioneMercato = By.xpath("//h2[text()='Modifiche non Salvate']");
    public By popupButtonConferma = By.xpath("//button[text()='Conferma'][1]");
    public By paragrafoPopupAnnullaSezioneMercato = By.xpath("//slot[@name='body']//p");
    public By labelSelezioneMercato = By.xpath("//div[@aria-label='Selezione Mercato']//h2[text()='Selezione Mercato']");
    public By labelSelezioneCausale = By.xpath("//div[@aria-label='Selezione Causale']//h2[text()='Selezione Causale']");
    public By tabSubentro = By.xpath("//span[text()='Subentro']");
    public By labelSubentro = By.xpath("//h1[text()='Subentro' and @title='Subentro']");
    public By labelAttivazione = By.xpath("//h1[text()='Prima Attivazione' and @title='Prima Attivazione']");
    public By checkContatto = By.xpath("//span[text()='Check contatto']//ancestor::header//img[@title='Esito Check contatto OK']");
    public By checkContattoKO = By.xpath("//span[text()='Check contatto']//ancestor::header//img[@title='Esito Check contatto KO']");
    public By checkCf = By.xpath("//span[text()='Check CF']//ancestor::header//img[@title='Esito Check CF Non disponibile']");
    public By campoMercato = By.xpath("//label[contains(text(),'Mercato di riferimento prodotto')]/..//input");
    public By campoCausale = By.xpath("//label[contains(text(),'Seleziona la causale della Voltura con accollo')]/..//input");
    public By buttonModificaSezioneMercato = By.xpath("//h2[text()='Selezione Causale']/ancestor::div[@role='region']//button[text()='Modifica']");
    public By popup_bl_ko = By.xpath("//h2[text()='KO BL']/ancestor::div[@class='slds-modal__container']//p[text()='Comunica al Cliente di recarsi presso uno Spazio Enel']/ancestor::div[@class='slds-modal__container']//button[text()='Conferma e Annulla']");
    public By popup_ko = By.xpath("//h2[text()='KO BL']/ancestor::div[@class='slds-modal__container']//button[text()='Conferma e Annulla']");
    public By inputSezioneMercato = By.xpath("//h2[text()='Selezione Mercato']/ancestor::div[@role='region']//input");
    public By articleNavigazioneProcesso = By.xpath("//h2//span[text()='Navigazione processo']");
    public By statoNavigazioneProcesso = By.xpath("//div//span[text()='Chiuso']");
    public By sottostatoNavigazioneProcesso = By.xpath("//div//span[text()='ANNULLATO']");
    public By inputPodPdr = By.xpath("//div[@role='none']//input[@maxlength='14']");
    public By inputCap = By.xpath("//div[@role='none']//input[@maxlength='5']");
    public By buttonEseguiPrecheck = By.xpath("//div[@role='region']//button[@name='Esegui precheck']");
    public By offertabilità = By.xpath(" //div[text()='OK']");
    public By ripetiPrecheck = By.xpath("//div[@role='region']//button[@name='Ripeti precheck']");
    public By modificaPrecheck = By.xpath("//div[@role='region']//button[@name='Modifica precheck']");
    public By esitoPrecheck = By.xpath(" //div[text()='KO']");
    public By popupErrorePrecheck = By.xpath("//div[@class='slds-modal__content slds-p-around_large']//slot[@name='body']//p[text()='Attenzione! Per Mercato Riferimento Prodotto = Salvaguardia è possibile inserire solo siti elettrici'][1]");
    //public By popupCodiceIstat = By.xpath("//h2[text()='Selezione Istat']/ancestor::div[@role='dialog']");
    public By aggiungiFornitura = By.xpath("//div[@role='region']//button[@name='Aggiungi fornitura']");
    public By rimuoviFornitura = By.xpath("//div[@role='region']//button[@name='Rimuovi fornitura']");
    public By popupEliminaFornitura = By.xpath("//h2[text()='Attenzione - Conferma eliminazione']/ancestor::div[@role='alertdialog']");
    public By textpopupEliminaFornitura = By.xpath("//h2[text()='Attenzione - Conferma eliminazione']/ancestor::div[@role='alertdialog']//p");
    public By buttonEliminaFornitura = By.xpath("//h2[text()='Attenzione - Conferma eliminazione']/ancestor::div[@role='alertdialog']//button[text()='Conferma']");
    public By popupLimiteFornitura = By.xpath("//h2[text()='Attenzione - Limite forniture']/ancestor::div[@role='alertdialog']");
    public By testoPopupLimiteFornitura = By.xpath("//h2[text()='Attenzione - Limite forniture']/ancestor::div[@role='alertdialog']//p");
    public By testoNonSeiAbilitato = By.xpath("//div[@data-id='errorMexId']//..//p");
    
    public By popupCodiceIstat = By.xpath("//div[@id='prompt-message-wrapper']");
    


    /**
     * @param driver
     */
    public SezioneMercatoComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
    }


    /**
     * @param existingObject
     * @throws Exception
     * previous 15
     */
    public void verifyComponentExistence(By existingObject) throws Exception {
        if (!util.exists(existingObject, 30))
            throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
    }


    /**
     * @param existingObject
     * @return true if element exist
     * @throws Exception
     */
    public boolean verifyExistenceComponent(By existingObject) throws Exception {
        if (!util.exists(existingObject, 30))
            return false;
        return true;
    }


    /**
     * @param clickableObject
     * @throws Exception
     */
    public void clickComponent(By clickableObject) throws Exception {
        util.objectManager(clickableObject, util.scrollToVisibility, false);
        util.objectManager(clickableObject, util.scrollAndClick);
    }


    /**
     * @throws Exception
     */
    public void verificaSezioneMercato() throws Exception {
        util.waitAndGetElement(campoMercato).click();
        TimeUnit.SECONDS.sleep(1);
        String mercato = "";
        List<WebElement> m = util.waitAndGetElements(By.xpath("//div[@role='listbox']//lightning-base-combobox-item"));
        for (int i = 1; i <= m.size(); i++) {
            mercato = util.waitAndGetElement(By.xpath("//div[@role='listbox']//lightning-base-combobox-item[" + i + "]//span[@class='slds-truncate']")).getText();
            if (!mercato.contentEquals("Libero") && !mercato.contentEquals("Salvaguardia") && !mercato.contentEquals("FUI"))
                throw new Exception("dalla combobox 'Mercato di riferimento del prodotto' non sono presenti i valori selezionabili 'Libero','Salvaguardia' e 'FUI'");
        }
    }


    /**
     * @throws Exception
     */
    public void verificaSezioneCausale() throws Exception {
        util.waitAndGetElement(campoCausale).click();
        TimeUnit.SECONDS.sleep(1);
        String mercato = "";
        List<WebElement> m = util.waitAndGetElements(By.xpath("//h2[text()='Selezione Causale']/ancestor::div[@role='region']//span[@class='slds-media__body']"));
        for (int i = 0; i < m.size(); i++) {
            mercato = m.get(i).getText();
            System.out.println(mercato);
            if (!mercato.contentEquals("Mortis Causa") && !mercato.contentEquals("Separazione/Divorzio"))
                throw new Exception("dalla combobox 'Causale voltura con accollo' non sono presenti i valori selezionabili 'Mortis Causa' e 'Separazione/Divorzio'");
        }
    }


    /**
     * @throws Exception
     */
    public void verificaSezioneCausaleBSN() throws Exception {
        util.waitAndGetElement(campoCausale).click();
        TimeUnit.SECONDS.sleep(1);
        String mercato = "";
        List<WebElement> m = util.waitAndGetElements(By.xpath("//h2[text()='Selezione Causale']/ancestor::div[@role='region']//span[@class='slds-media__body']"));
        for (int i = 0; i < m.size(); i++) {
            mercato = m.get(i).getText();
            System.out.println(mercato);
            if (!mercato.contentEquals("Trasformazione/Fusione Azienda") && !mercato.contentEquals("Cessione Ramo Azienda"))
                throw new Exception("dalla combobox 'Causale voltura con accollo' non sono presenti i valori selezionabili 'Trasformazione/Fusione Azienda' e 'Cessione Ramo Azienda'");
        }
    }


    /**
     * @param mercato
     * @throws Exception
     */
    public void selezionaMercato(String mercato) throws Exception {
        WebElement element = util.waitAndGetElement(campoMercato, 90, 1);
        element.sendKeys(mercato);
        element.sendKeys(Keys.ENTER);
    }


    /**
     * @param causale
     * @throws Exception
     */
    public void selezionaCusale(String causale) throws Exception {
        WebElement element = util.waitAndGetElement(campoCausale, 90, 1);
        element.sendKeys(causale);
        element.sendKeys(Keys.ENTER);
    }
}
