package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ConfermaButtonComponent {
	WebDriver driver ;
	SeleniumUtilities util;
	SpinnerManager spinner;
	private By confermaButton = By.xpath("//span[@id='j_id0:formId:buttonSaveDown']//button[text()='Conferma']");
    public By titoloPopup=By.xpath("//h2[@id='completedTitle' and  text()='Operazione Conclusa con Successo']");
	public By confermaSave = By.xpath("//span[@id='j_id0:formId:buttonSaveDown']//button[text()='Conferma']");
//    public By ricorda = By.xpath("//div[contains(text(),'Ricorda al cliente') and contains(text(),'una Email/SMS')]/..//button[text()='OK']");
    public By ricordaOK = By.xpath("//*[@id='popup_completed']/div[1]/div/div[7]/button[1]");
    public By ricordaNO = By.xpath("//*[@id='popup_completed']/div[1]/div/div[7]/button[2]");
      

    
    public ConfermaButtonComponent(WebDriver driver){
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	public void clickConferma(String xpathButton) throws Exception 
	{
		confermaButton = By.xpath(xpathButton);
//		String frame = util.getFrameByIndex(1);
//		WebDriverWait wait = new WebDriverWait(driver, 120);
		util.objectManager(confermaButton, util.scrollAndClick);
	}
	
	public void press(By xpathButton) throws Exception 
	{

//		util.objectManager(xpathButton, util.scrollAndClick);
		WebElement element = util.waitAndGetElement(xpathButton);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Thread.currentThread().sleep(10000); 
		spinner.checkSpinners();
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		util.getFrameActive();
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	
	public boolean verifyExistence(By existentObject) throws Exception {
		return util.verifyExistence(existentObject, 10);
	}
		
	
	
}
