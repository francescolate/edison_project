package com.nttdata.qa.enel.components.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class ModifyPowerAndVoltage {
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try{
		
			RemoteWebDriver driver = WebDriverManager.getDriverInstance(prop);
			
			logger.write("apertura del portale web Enel di test - Start");
			
			HomeComponent home = new HomeComponent(driver);
								
			home.clickComponent(home.detaglioFurnituraES);
			
			DettaglioFornitureComponent dc = new DettaglioFornitureComponent(driver);
			
			logger.write("Check For page title-- start");
			
			Thread.currentThread().sleep(5000);
			dc.verifyComponentExistence(dc.pageTitle);
			
			logger.write("Click on Change Power And Voltage-- Start");

			Thread.sleep(5000);
			dc.verifyComponentExistence(dc.modificaPotenzo);
			
			dc.jsClickObject(dc.modificaPotenzo);
			
			logger.write("Click on Change Power And Voltage-- Completed");
			
			ChangePowerAndVoltageComponent changePV = new ChangePowerAndVoltageComponent(driver);
			
			logger.write("Click on change power and voltage button -- Start");
			
			changePV.clickComponent(changePV.changePowerAndVoltageButton);
			
			logger.write("Click on change power and voltage button -- Completed");
			
			changePV.waitForElementToDisplay(changePV.pageTitle);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Start");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
			
			changePV.comprareText(changePV.titleSubText, changePV.ModificaSubText, true);
			
			logger.write("Check for Change Power And Voltage Title and subtext -- Completed");
			
			logger.write("Click on radio button-- Start");

			changePV.clickComponent(changePV.radioButton149);
						
			logger.write("Click on radio button-- Completed");
			
			logger.write("Click on Change Power and voltage button-- Start");

			changePV.clickComponent(changePV.changePowerOrVoltageButton);
			
			logger.write("Click on Change Power and voltage button-- Completed");
			
//			changePV.selectSupply();

			changePV.verifyComponentExistence(changePV.pageTitle);
						
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");
			
			changePV.verifyComponentExistence(changePV.pageTitle);
						
			logger.write("Check for menu item -- Start");

			changePV.checkForMenuItem(changePV.menuItem);
			
			logger.write("Check for menu item -- Completed");
			
			logger.write("Check for Power Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.powerLable);
			
			logger.write("Check for Power Lable -- Completed");
			
			logger.write("Check for Default Power value -- Start");
			
			changePV.checkForDefaultValue(changePV.powerValue,prop.getProperty("POWERDEFAULT"));
			
			logger.write("Check for Default Power value -- Completed");
			
			logger.write("Check for Voltage Lable -- Start");
			
			changePV.verifyComponentExistence(changePV.voltageLable);
			
			logger.write("Check for Voltage Lable -- Completed");
			
			logger.write("Check for Default Voltage value -- Start");
			
			changePV.checkForDefaultValue(changePV.voltageValue,prop.getProperty("VOLTAGEFAULT"));
			
			logger.write("Check for Default Voltage value -- Completed");
			
			changePV.comprareText(changePV.formLine2, changePV.FormLine2, true);
			
			changePV.checkButtonIsEnable(changePV.radioNo);
			
			changePV.comprareText(changePV.formLine3, changePV.FormLine3, true);
			changePV.checkFieldValue(changePV.emailInput, prop.getProperty("WP_USERNAME"));
		//	changePV.checkFieldValue(changePV.cellulare, prop.getProperty("CELLULAR"));

			
			changePV.changeDropDownValue(changePV.powerValue, prop.getProperty("POWER_INPUT"));

			logger.write("Click on radio button Yes  -- Start");
			
			changePV.clickComponent(changePV.radioYes);
			
			logger.write("Click on radio button Yes  -- Completed");
			
	
			changePV.changeDropDownValue(changePV.liftTypeDropdown,prop.getProperty("ARGANO"));

			changePV.enterInputtoField(changePV.startingCurrentInput,prop.getProperty("STARTINGCURRENTINPUT"));
			changePV.enterInputtoField(changePV.runningCurrentInput,prop.getProperty("RUNNINGCURRENTINPUT"));
			
			changePV.clickComponent(changePV.calculateQuote);
			
		
			
			changePV.enterInputtoField(changePV.cellular, prop.getProperty("CELLULAR"));
			changePV.clickComponent(changePV.calculateQuote);
			changePV.enterInputtoField(changePV.confirmCellular, prop.getProperty("CELLULAR"));

			changePV.clickComponent(changePV.calculateQuote);
			
			logger.write("Verify Pop up -- Start");

			changePV.verifyComponentExistence(changePV.attendiPopUpTitle);
			changePV.comprareText(changePV.attendiPopUp, changePV.Attendiqualchesecondo, true);
			
			/*Thread.sleep(50000);
			changePV.verifyComponentExistence(changePV.sopralluogo);
			changePV.comprareText(changePV.sopralluogoText1, changePV.SopralluogoText1, true);
			changePV.comprareText(changePV.sopralluogoText2, changePV.SopralluogoText2, true);
			changePV.checkForData(changePV.potenzaInput, prop.getProperty("POTENZA"));
			changePV.checkForData(changePV.tensionInput, prop.getProperty("TENSIONE"));
			changePV.checkForData(changePV.emailIp, prop.getProperty("WP_USERNAME"));
			changePV.checkForData(changePV.cellularIp, prop.getProperty("CELLULAR"));
			changePV.comprareText(changePV.sopralluogoText3, changePV.SopralluogoText3, true);
			changePV.comprareText(changePV.sopralluogoText2, changePV.SopralluogoText2, true);
			changePV.verifyComponentExistence(changePV.luceIcon);
		//	changePV.checkForData(changePV.luceAdd, prop.getProperty("ADDRESS"));
			changePV.verifyComponentExistence(changePV.indietro);
			changePV.verifyComponentExistence(changePV.conferma);
			changePV.clickComponent(changePV.conferma);
			
			ChangePowerAndVoltagePreventintoComponent cc = new ChangePowerAndVoltagePreventintoComponent(driver);
			
			Thread.sleep(30000);
			cc.comprareText(cc.richesta, cc.confirmText, true);
			cc.clickComponent(cc.fineButton);

			logger.write("Verify Pop up -- Completed");
			
			home.checkForResidentialMenu(home.RESIDENTIALMENU);*/
						
			prop.setProperty("RETURN_VALUE", "OK");			

		}catch (Throwable e) {
			prop.setProperty("RETURN_VALUE", "KO");

			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: " + errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR: " + errors.toString());
			if (prop.getProperty("RUN_LOCALLY", "N").equals("Y"))
				throw e;

//			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+e.getMessage());
		} finally {
			// Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set WebDriver Info");
		}
		}

}
