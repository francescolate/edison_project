package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ContractChoiceComponent extends BaseComponent {
	//final static String optionPath = "//ul[@aria-hidden='false']//*[text()='#']/ancestor::li";
	final static String optionPath = "//ul//*[text()='#']/ancestor::li";
	public By contractTypeSelect = By.xpath("//span[@id='productSelectBoxIt']");
//	public By contractTypeSelect = By.xpath("//select[@id='product']");
	public By placeSelect = By.xpath("//span[@id='placeSelectBoxIt']");
//	public By placeSelect = By.xpath("//select[@id='place']");
	public By requirementsSelect = By.xpath("//span[@id='myselectSelectBoxIt']");
//	public By requirementsSelect = By.xpath("//select[@id='myselect']");
	public By continueButton = By.id("ofertButton");
	
	public ContractChoiceComponent(WebDriver driver) {
		super(driver);
	}
	
	public void selectMenuOption(By menu, String option) throws Exception {
		verifyComponentVisibility(menu);
		WebElement menuElement = driver.findElement(menu);
		menuElement.click();
		Thread.sleep(2000);
		By requiredOption = By.xpath(optionPath.replace("#", option));
		verifyComponentVisibility(requiredOption);
		WebElement optionElement = driver.findElement(requiredOption);
		optionElement.click();
	}
	
}
