package com.nttdata.qa.enel.components.colla;

import java.awt.AWTException;
import java.awt.List;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OCR_Module_Component extends BaseComponent{
	
	WebDriver driver;
    JavascriptExecutor js = (JavascriptExecutor) driver;

	
	//Form OCR Detail
	public By attivaOraButton = By.xpath("//*[@id='priceHeaderID' and text()='Attiva ora']");
	
	public By compilaManualmenteBtn = By.xpath("//button[text()='COMPILA MANUALMENTE']");
	
	public By chatCloseButton = By.xpath("//span[@class='icon-close track-gdpr']");
	public By offerDetailsButton = By.xpath("//h3[text()='E-Light Bioraria']/ancestor::a");
	public By offerDetailsButtonGas = By.xpath("//h3[text()='E-light Gas']/ancestor::a");
	public By offerActivationButton = By.xpath("//a[@role='button' and contains(text(), 'attiva')]");
	public By description = By.xpath("//p[@class='description_container']");
	public By caricaBollettaButton = By.xpath("//button[@type='button' and text()='CARICA BOLLETTA']");
	public By compilaManualmenteButton = By.xpath("//button[text()='COMPILA MANUALMENTE']");
	public By firstnameInputField = By.xpath("//input[@id='nameId']");
	public By lastnameInputField = By.xpath("//input[@id='surnameId']");
	public By fiscalCodeInputField = By.xpath("//input[@id='cfId']");
	public By mobileInputField = By.xpath("//input[@id='phoneId']");
	public By emailInputField = By.xpath("//input[@id='emailId']");
	public By emailConfirmationInputField = By.xpath("//input[@id='emailConfirmId']");
	public By privacyCheckbox = By.xpath("//input[@id='checkId']/parent::span");
	public By mobileErrorMessage = By.xpath("//p[@id='phoneId-helper-text']");
	public By continueButton = By.xpath("//button[text()='PROSEGUI']");
	public By continueButton1 = By.xpath("(//button[text()='PROSEGUI'])[1]");
	public By continueButton2 = By.xpath("(//button[text()='PROSEGUI'])[2]");
	public By salvaContinuaDopoButton = By.xpath("(//button[text()='SALVA E CONTINUA DOPO'])[1]");
	public By salvaContinuaDopoButton_2 = By.xpath("(//button[text()='SALVA E CONTINUA DOPO'])[2]");
	public By continueButton3 = By.xpath("(//button[text()='PROSEGUI'])[3]");
	public By continueButton4 = By.xpath("(//button[text()='PROSEGUI'])[4]");
	public By inserisciDati = By.xpath("//h3[@class='sub_header' and text()='Inserisci i tuoi dati']");
	public By informazioniFornitura = By.xpath("//h3[@class='sub_header' and text()='Informazioni fornitura']");
	public By pagamentiBollette = By.xpath("//h3[@class='sub_header' and text()='Pagamenti e bollette']");
	public By consensi = By.xpath("//h3[@class='sub_header' and text()='Consensi']");
	public By mandantiConsensi = By.xpath("//h3[@class='sub_header' and text()='Mandati e Consensi']");
	public By legend = By.xpath("//legend[text()='Di cosa hai bisogno ?']");
	public By checkbox1 = By.xpath("(//span[@class='MuiIconButton-label'])[1]");
	public By privacy = By.xpath("//p[@class='privacy_text']");
	public By indirizzoFornitura = By.xpath("//p[text()='Indirizzo di fornitura']");
	public By inputCittà = By.xpath("//input[@id='citySupply']");
	public By inputCityLuce = By.xpath("//input[@id='addressLuce-city']");
	public By inputIndirizzo = By.xpath("//input[@id='addressSupply']");
	public By inputAddressLuce = By.xpath("//input[@id='addressLuce-address']");
	public By inputCivico = By.xpath("//input[@id='houseNumberSupply']");
	public By inputCivicLuce = By.xpath("//input[@id='addressLuce-civic-number']");
	public By inputCap = By.xpath("//input[@id='postalCodeSupply']");
	public By inputCapLuce = By.xpath("//input[@id='addressLuce-cap']");
	public By inputAttualeFornitore = By.xpath("//input[@id='currentSupplier']");
	public By inputAttualeFornitoreLuce = By.xpath("//input[@id='supplier-luce']");
	public By inputAttualeFornitoreGas = By.xpath("//input[@id='supplier-gas']");
	public By recapiti = By.xpath("//p[@class='upper_text' and text()='Recapiti']");
	public By residente = By.xpath("//*[ text()='Sei residente presso la fornitura?']");
	public By residentePressoFornituraSi = By.xpath("//legend[text()='Sei residente presso la fornitura?']//ancestor::fieldset[@class='MuiFormControl-root']//input[@value='ELE']//following-sibling::div");
	public By residentePressoFornituraNo = By.xpath("//legend[text()='Sei residente presso la fornitura?']//ancestor::fieldset[@class='MuiFormControl-root']//input[@value='NA']//following-sibling::div");
	public By inputSi = By.xpath("(//span[text()='SI']//ancestor::label//span[1]/input[@value='true'])[1]");
	public By inputNo = By.xpath("(//span[text()='NO']//ancestor::label//span[1]/input[@value='false'])[1]");
	public By comunicazioni = By.xpath("//p[text()='Dove vuoi ricevere le comunicazioni relative alla tua fornitura?']");
	public By stessoIndirizzo = By.xpath("//span[text()='Allo stesso indirizzo della fornitura']");
	public By diversoIndirizzo = By.xpath("//span[text()='Ad un indirizzo diverso da quello della fornitura']");
	public By metodoPagamento = By.xpath("//p[contains(text(),'Metodo di Pagamento')]");
	//public By metodoPagamentoInput = By.xpath("((//div[@aria-label='scegli il metodo di pagamento']//parent::div)[1]//span)[1]");
	public By metodoPagamentoInput = By.xpath("//input[@id='choosePayment']//parent::span//parent::div");
	public By inputIbanStraniero = By.xpath("//span[text()='IBAN estero']//ancestor::label//span//input[@name='ibanForeign' and @type='checkbox']");
	public By inputIbanEstero = By.xpath("//input[@name='ibanForeign']//parent::span");
	public By codiceIban = By.xpath("//label[contains(text(),'Codice IBAN')]");
	public By inputCodiceIban = By.xpath("//*[@id='iban-text-field' and @name='ibanCode']");
	public By inputIntestatario= By.xpath("//span[text()=\"Sono l'intestatario del conto corrente\"]//ancestor::label//input//parent::span");
	public By inputAltroIntestatario= By.xpath("//span[text()=\"L'intestatario del conto corrente è un altro\"]//ancestor::label//input//parent::span");
	public By modalitàRicezioneBolletta = By.xpath("//p[contains(text(),'Modalità di ricezione della bolletta')]");
	public By bollettaEmail = By.xpath("//span[text()=\"Voglio ricevere la bolletta via email e le notifiche relative all'immissione della bolletta ai seguenti canali di contatto\"]//ancestor::label//input//parent::span");
	public By bollettaCartacea = By.xpath("//span[text()='Voglio ricevere la fattura in forma cartacea tramite posta ordinaria']//ancestor::label//input//parent::span");
	public By inputEmail = By.xpath("//input[@name='email']");
	public By inputPhone = By.xpath("//input[@name='phone']");
	public By codicePromozionale = By.xpath("//label[text()='Codice Promozionale']");
	public By codicePromozionaleText = By.xpath("//p[text()='Se sei in possesso di un codice promozionale inseriscilo nel campo seguente']");
	public By promo = By.xpath("//input[@id='idPromo']");
	public By chiudiLogo = By.xpath("//span[@class='closeLogo']");
	public By esciSenzaSalvare = By.xpath("//button[@class='exit_button' and text()='Esci senza salvare']");
	
	public By header = By.xpath("//h1[@class='header']");
	public By header2 = By.xpath("//h1[@class='header'][2]");
	public By infoCap = By.xpath("//p[@class='infoCap']");
	public String infoCapText = "Conferma il CAP cliccando sul campo e selezionando dall’elenco la voce corretta.";
	public By podPdr = By.xpath("//input[@name='podPdr']");
	public By podInput = By.xpath("//input[@name='pod']");
	public By pdrInput = By.xpath("//input[@name='pdr']");
	public By podCap = By.xpath("//input[@id='pod-cap']");
	public By pdrCap = By.xpath("//input[@id='pdr-cap']");
	public By capDropdown = By.xpath("//div[@label='00136-ROMA']");
	public By proceedButton_Step2 = By.xpath("//p[@class='infoCap']/parent::div//button[text()='PROSEGUI']");
	public By inserisciloDopo = By.xpath("//button[text()='INSERISCILO DOPO']");
	public By infoPodButton = By.xpath("//button[@class='info_icon_pod']");
	public By infoPdrButton = By.xpath("(//button[@class='info_icon_pod'])[2]");

	
	//campi obbligatori
	public By obbligatorioNome = By.xpath("//p[@id='nameId-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioCognome = By.xpath("//p[@id='surnameId-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioCF = By.xpath("//p[@id='cfId-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioCellulare = By.xpath("//p[@id='phoneId-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioEmail = By.xpath("//p[@id='emailId-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioConfermaEmail = By.xpath("//p[@id='emailConfirmId' and text()='Campo Obbligatorio']");
	
	//formato non corretto
	public By formatoNome = By.xpath("//p[@id='nameId-helper-text' and text()='Formato non corretto']");
	public By formatoCognome = By.xpath("//p[@id='surnameId-helper-text' and text()='Formato non corretto']");
	public By formatoCF = By.xpath("//p[@id='cfId-helper-text' and text()='Formato non corretto']");
	public By formatoCellulare = By.xpath("//p[@id='phoneId-helper-text' and text()='Formato non corretto']");
	public By formatoEmail = By.xpath("//p[@id='emailId-helper-text' and text()='Formato non corretto']");
	public By formatoConfermaEmail = By.xpath("//p[@id='emailConfirmId' and text()='Formato non corretto']");
	
	//Calcolo CF
	public By calcolaCF = By.xpath("//div[@class='cf_modal_link']");
	public By nomeCF = By.xpath("//input[@id='name-text-field-cf']");
	public By cognomeCF = By.xpath("//input[@id='surname-text-field-cf']");
	public By giornoCF = By.xpath("//input[@id='cf_select_day']");
	public By meseCF = By.xpath("//input[@id='cf_select_month']");
	public By annoCF = By.xpath("//input[@id='cf_select_year']");
	public By calcolaButtonCF = By.xpath("//button[@type='submit' and @class='exit_button' and contains(text(),'Calcola')]");
	
	//campi obbligatori CF
	public By obbligatorioNomeCF = By.xpath("//*[@id='name-text-field-cf-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioCognomeCF = By.xpath("//*[@id='surname-text-field-cf-helper-text' and text()='Campo Obbligatorio']");
	public By obbligatorioGiornoCF = By.xpath("//*[@id='error_select_day_cf' and text()='Campo Obbligatorio']");
	public By obbligatorioMeseCF = By.xpath("//*[@id='error_select_month_cf' and text()='Campo Obbligatorio']");
	public By obbligatorioAnnoCF = By.xpath("//*[@id='error_select_year_cf' and text()='Campo Obbligatorio']");
	public By obbligatorioSessoCF = By.xpath("//*[@id='error-radio-gender' and text()='Campo Obbligatorio']");
	public By obbligatorioLuogoCF = By.xpath("//*[@id='error-radio-italy-born' and text()='Campo Obbligatorio']");
	
	//Mandanti e Consensi
	public By mandatiConsensiBy1 = By.xpath("//*[@class='textarea_appearance consent_box'][1]");
	public String mandatiConsensiText1 = "Dichiaro di voler conferire ad Enel Energia:" + "mandato irrevocabile senza rappresentanza per lo svolgimento presso il distributore competente delle attività di gestione della connessione dei punti di prelievo (es. aumenti di potenza, spostamenti di gruppi di misura, etc), mantenendo la titolarità di ogni rapporto giuridico con il distributore competente inerente la connessione alla rete dei siti ed impianti. Tale mandato è a titolo oneroso ed obbliga la corrispondere al Fornitore gli importi necessari per l’esecuzione del mandato e per l’adempimento delle obbligazioni che a tal fine il Fornitore ha contratte in proprio nome;" + "mandato irrevocabile con rappresentanza per la sottoscrizione del Contratto per il servizio di connessione alla rete elettrica allegate al contratto per il servizio di trasporto dell'energia elettrica, del cui contenuto il Cliente ha preso atto anche in quanto disponibile sul sito www.enel.it, consapevole che l’accettazione ed il rispetto delle stesse è condizione necessaria per l’attivazione ed il mantenimento del servizio di trasporto.";
	public By inputMandatiConsensi = By.xpath("//div[@class='input_wrapper']");
	public String inputMandatiConsensiText = "Ho preso visione dell' informativa privacy riguardo come tratterete i miei dati personali." +
											"Ho preso visione delle note legali ai sensi del D.Lgs 70/2003 e le informazioni precontrattuali ai sensi dell'art 49 del D.Lgs 21/2014 - Codice del Consumo." +
											"Voglio recedere dal contratto attualmente in essere con";

	
	
	public By consensi1 = By.xpath("//input[@name='viewPersonalData']//parent::span");
	public By consensi2 = By.xpath("//input[@name='viewLegalNote']//parent::span");
	public By consensi3 = By.xpath("//input[@name='thirdPartyContract']//parent::span");
	public By consensi3_ele = By.xpath("//input[@name='thirdPartyContractEle']//parent::span");
	public By consensi3_gas = By.xpath("//input[@name='thirdPartyContractGas']//parent::span");
	public By esecuzioneAnticipataSI = By.xpath("//input[@id='radioValueYes']//parent::span");
	public By marketing = By.xpath("//input[@name='marketingConsent' and @value='true']//parent::span");
	public By marketingThird = By.xpath("//input[@name='marketingConsentThird' and @value='true']//parent::span");
	public By marketingProfiling = By.xpath("//input[@name='marketingProfiling' and @value='true']//parent::span");
	
	public By richiestaAnticipataTitle = By.xpath("//*[text()='Richiesta di esecuzione anticipata del contratto']");
	public By richiestaAnticipata = By.xpath("//*[@class='request_execution']");
	public By maggioriInfo = By.xpath("//*[@class='more_info']");
	public By siAttivaSubito = By.xpath("//span[text()='Si, voglio avviarlo subito']//ancestor::label//child::span[@aria-checked='false']");
	public By noAttendi = By.xpath("//span[text()='No, voglio attendere 14 giorni']//ancestor::label//child::span[@aria-checked='false']");
	public By modalBody = By.xpath("//div[@class='modal_body']");
	public By modalCloseButton = By.xpath("//button[@class='modal_close_icon']");
	
	public By completaAdesione = By.xpath("//button[text()='Completa Adesione']");
	
	public By passoAdesione = By.xpath("//h1[text()=\"Sei ad un passo dal completare l'adesione!\"]");
	public By passoAdesioneBody = By.xpath("//p[@class='hero_msg1']");
	
	public By caricaBollettaTitle = By.xpath("//h3[@class='carica_text' and text()='Carica la tua bolletta']");
	public By dropContainer = By.xpath("//div[@class='drop_container']");
	public By descriptionContainer = By.xpath("//div[@class='lower_text']//p[@class='description_container']");
	

	public String text = "Vuoi procedere velocemente con l'attivazione?Puoi caricare la tua attuale bolletta in formato digitale e penseremo noi a compilare i tuoi dati.Dovrai solamente verificarli e completarli con alcune informazioni aggiuntive.In alternativa puoi compilare manualmente le informazioni richieste.";
	public String cambioFornitore = "CAMBIO FORNITOREHo gia un contratto con un altro fornitore ma vorrei passare ad Enel";
	public String primaAttivazione = "PRIMA ATTIVAZIONEVoglio attivare un nuovo contatore";
	public String subentro = "SUBENTROVoglio attivare un contatore disattivato";
	public String voltura = "VOLTURAVoglio cambiare intestatario a un contratto di fornitura";
	public String privacyText = "Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l’adesione.";
	public String privacyText2 = "Ti informiamo che la fornitura verrà attivata con i medesimi requisiti tecnici (potenza e tensione) attualmente in esercizio. Se lo desideri potrai effettuare una variazionedi potenza dopo l'attivazione del contratto direttamente dalla tua area clienti.";
	public String maggioriInfoText = "Il Codice del Consumo prevede 14 giorni di tempo per consentire al cliente di esercitare il diritto ripensamento dopo la conclusione di un contratto. Scegliendo la \"Richiesta di esecuzione anticipata del contratto\", il Cliente richiede espressamente che le procedure per dar corso all'attivazione vengano avviate subito, ossia prima che sia decorso il termine per il ripensamento. Anche nel caso di \"Richiesta di esecuzione anticipata del contratto\", per il Cliente sará sempre possibile esercitare il diritto di ripensamento. Qualora sia possibile annullare la richiesta di attivazione, il Cliente continuerá ad essere fornito dal precedente Fornitore oppure saranno attivati i Servizi di ultima istanza gas o il Servizio di maggior tutela per il settore elettrico. Qualora, invece, non sia possibile interrompere la richiesta di attivazione verso il distributore, il Cliente verrá fornito da Enel Energia e potrá individuare successivamente un altro Fornitore o procedere alla richiesta di chiusura del Punto di fornitura, facendone espressa richiesta. In questo caso il Cliente sará tenuto al pagamento dei corrispettivi previsti dal contratto fino all'avvenuta cessazione della fornitura.";
	public String passoAdesioneText = "Ti abbiamo inviato un messaggio all'indirizzo $ Controlla l'email e conferma il contratto della tua fornitura energia. Se non dovessi riceverla, verifica la casella della posta indesiderata";
	public String passoAdesioneText_v2 = "Ti abbiamo inviato un messaggio all'indirizzo $Controlla l'email e conferma il contratto della tua fornitura energia.";
	public String dropContainerText = "Trascina e rilascia qui la tua bolletta (formato digitale max 20MB)AltrimentiCLICCA QUI per scegliere un file.(formato digitale max 20MB)";
	public String descriptionContainerText = "Caricando la tua bolletta in formato digitale compileremo alcune delle informazioni necessarie all'invio della richiesta. Dovrai integrare i dati che non è possibile estrarre dalla tua bolletta (Es. Email, Cellulare, ...)Ti invitiamo, infine a verificare l'esattezza dei dati compilati.";
	
	//Salva e continua dopo
	public By esciButton = By.xpath("//*[@id='scroll-dialog-title']/button");
	public By checkOffers = By.xpath("//*[@id='otherOffersId' and @type ='radio']//parent::span");
	public By checkTime = By.xpath("//*[@id='noTimeId' and @type ='radio']//parent::span");
	public By checkInfo = By.xpath("//*[@id='noInfoId' and @type ='radio']//parent::span");
	public By checkAltro = By.xpath("//*[@id='otherId' and @type ='radio']//parent::span");
	public By areaAltro = By.xpath("//*[@id='standard-required'and @name='description']");
	public By salvaEsciButton = By.xpath("//*[@id='scroll-dialog-description']//button[text()='Salva ed esci']");
	public By esciSenzaSalvareButton = By.xpath("//*[@id='scroll-dialog-description']//button[text()='Esci senza salvare']");
	public By finalText = By.xpath("//*[@id='save-landing-page']//h1");
	public String finalText1 = "Ti abbiamo";
	public String finalText2 = "inviato un link per riprendere il processo di adesione";
	public By backToHomeButton = By.xpath("//*[@id='back-to-home']");

	
	
	public OCR_Module_Component(WebDriver driver){
		super(driver);
	}
	
	//Check Title
	public void checkHeader(String s, WebDriver driver) throws Exception{
		if (util.verifyVisibility(header, 1)){
			this.driver = driver;
			String a = driver.findElement(header).getText();
			if (!s.contentEquals(a))
				throw new Exception("Il titolo della pagina non è corretto. È presente il titolo " + a);
		} else throw new Exception("L'header non è visibile");
	}
	
	public void checkHeader2(String s, WebDriver driver) throws Exception{
		if (util.verifyVisibility(header2, 1)){
			this.driver = driver;
			String a = driver.findElement(header2).getText();
			if (!s.contentEquals(a))
				throw new Exception("Il titolo della pagina non è corretto. È presente il titolo " + a);
		} else throw new Exception("L'header non è visibile");
	}
	
	public String getElementHref(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		return we.getAttribute("href");
	}
	
	//Check first description and the button
	public void checkPage() throws Exception{
		compareText(description, text, true);
		verifyComponentExistence(caricaBollettaButton);
		verifyComponentExistence(compilaManualmenteButton);	
	}
	
	public void checkCaricaBolletta() throws Exception{
		verifyComponentExistence(caricaBollettaTitle);
		verifyComponentExistence(dropContainer);	
		verifyComponentExistence(descriptionContainer);
		compareText(dropContainer, dropContainerText, true);
		compareText(descriptionContainer, descriptionContainerText, true);
	}
	
	//Check and insert data
	public void inserisciDati(String nome, String cognome, String cf, String cell, String email) throws Exception{
		verifyComponentExistence(inserisciDati);
		clearAndSendKeys(firstnameInputField, nome);Thread.sleep(200);
		clearAndSendKeys(lastnameInputField, cognome);Thread.sleep(200);
		clearAndSendKeys(fiscalCodeInputField, cf);Thread.sleep(200);
		clearAndSendKeys(mobileInputField, cell);Thread.sleep(200);
		clearAndSendKeys(emailInputField, email);Thread.sleep(200);
		clearAndSendKeys(emailConfirmationInputField, email);Thread.sleep(200);
		clickComponent(privacyCheckbox);Thread.sleep(200);
		clickComponent(continueButton1);Thread.sleep(200);
	}
	
	public void inserisciDatiBusiness(String formaGiuridica, String cfSocieta, String regioneSociale, String piva, String prefSocieta, String tel, String email, String pec, String tipo, String nome, String cognome, String cf, String pref, String cel, Boolean check, WebDriver driver) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//driver.switchTo().frame("iframeSF");
		//Thread.sleep(3000);
		clickComponent(By.xpath("//div[@id='forma-giuridica']"));
		Thread.sleep(2000);
//		int z=1;
//		switch(formaGiuridica) {
//			case "S.A.S.":
//			z=8;
//		}
//		for(int i=0; i<z; i++)
//			simulateDownArrowKey(By.xpath("//div[@id='forma-giuridica']"));
//		simulateEnterKey(By.xpath("//div[@id='forma-giuridica']"));
		clickComponent(By.xpath("//ul[@role='listbox']//li[text()='" + formaGiuridica + "']"));

		clickComponent(By.id("fiscalCodeBusinessId"));
		insertText(By.id("fiscalCodeBusinessId"), cfSocieta);
		insertText(By.id("regioneSocialeId"), regioneSociale);
		insertText(By.id("partitaIvaId"), piva);
		insertText(By.id("phoneId"), tel);
		insertText(By.id("emailId"), email);
		insertText(By.id("pecId"), pec);
		Thread.sleep(1000);
		//util.objectManager(By.id("tipo-referente"), util.scrollToVisibility, false);
		js.executeScript("window.scrollBy(0,250)"); //Scroll vertically down by 1000 pixels
		//scrollComponent(By.id("referenceLastName"));
		clickComponent(By.xpath("//div[@id='tipo-referente']"));
		Thread.sleep(1000);
		clickComponent(By.xpath("//ul[@role='listbox']//li[text()='" + tipo + "']"));
//		Thread.sleep(1000);
//		int y=0;
//		switch(tipo) {
//			case "Rappresentante legale":
//			y=5;
//		}
//		for(int i=0; i<y; i++)
//			simulateDownArrowKey(By.xpath("//span[@id='tipo-referente']"));
//		simulateEnterKey(By.xpath("//span[@id='tipo-referente']"));
		insertText(By.id("name"), nome);
		insertText(By.id("surname"), cognome);
		insertText(By.name("fiscalCode"), cf);
		insertText(By.name("phoneReferent"), cel);
		if (check)
			clickComponent(By.xpath("(//input[@type='radio' and @value='true']//parent::span)[1]"));
		else {
			//js.executeScript("window.scrollBy(0,1000)"); //Scroll vertically down by 1000 pixels
			//util.objectManager(By.id("label-privacy-consent"), util.scrollElement);
			clickComponent(By.xpath("(//input[@type='radio' and @value='false']//parent::span)[1]"));
		}
		clickComponent(By.xpath("//*[@id='checkId']//parent::span"));
		clickComponent(By.xpath("(//button[text()='PROSEGUI'])[1]"));
	}
	
	public void insertInformazioniFornitura(String type, String pod, String cap, String city, String cityFull, String address, String civic, String potenza, String fornitore, Boolean sameAddress, Boolean sameAddressSupply) throws Exception{
		//clickComponent(By.xpath("//SPAN[(text()='"+type+"')]//ancestor::span//preceding-sibling::span//span[1]"));
		clickComponent(By.xpath("//SPAN[(text()='"+type+"')]"));
		insertText(By.id("pod-input"), pod);
		insertText(By.id("pod-cap"), cap);
		clickComponent(By.xpath("//*[text()='"+cap+"-"+city+"']"));
		//clickComponent(By.id("verifyPOD"));
		clickComponent(By.xpath("(//button[text()='PROSEGUI'])[1]"));
		Thread.sleep(5000);
		insertText(By.id("addressLuce-city"), cityFull);				//città
		Thread.sleep(2000);
		simulateEnterKey(By.id("addressLuce-city"));
		insertText(By.id("addressLuce-address"), address);				//indirizzo
		Thread.sleep(2000);
		simulateEnterKey(By.id("addressLuce-address"));
		insertText(By.id("addressLuce-civic-number"), civic);			//numero civico
		Thread.sleep(2000);
		simulateEnterKey(By.id("addressLuce-civic-number"));
		insertText(By.id("supplier-luce"), fornitore);					//attuale fornitore
		Thread.sleep(2000);
		simulateEnterKey(By.id("supplier-luce"));
		util.objectManager(By.xpath("(//button[text()='PROSEGUI'])[1]"), util.scrollElement);
		Thread.sleep(1000);
		if (sameAddress)
			clickComponent(By.xpath("(//input[@type='radio' and @value='ELE']//parent::span)[1]"));
		else{
			clickComponent(By.xpath("(//input[@type='radio' and @value='NA']//parent::span)[1]"));
			insertRecapiti();
		}
		if (sameAddressSupply)
			clickComponent(By.xpath("(//input[@type='radio' and @value='ELE']//parent::span)[2]"));
		else
			clickComponent(By.xpath("(//input[@type='radio' and @value='NA']//parent::span)[2]"));
		Thread.sleep(1000);
		clickComponent(By.xpath("(//button[text()='PROSEGUI'])[1]"));

	}
	
	public void insertPagamentiBollette(String mop, Boolean ricezione, String promo) throws Exception{
		switch(mop) {
			case "Bollettino Postale":
				clickComponent(metodoPagamentoInput);
				//simulateDownArrowKey(metodoPagamentoInput);
				//simulateDownArrowKey(By.id("choosePayment_list_1"));
				clickComponent(By.xpath("(//*[text()='Bollettino Postale'])[2]"));
				//simulateEnterKey(metodoPagamentoInput);
			break;
			case "Addebito su carta di pagamento":
				clickComponent(metodoPagamentoInput);
				simulateDownArrowKey(metodoPagamentoInput);
				simulateDownArrowKey(metodoPagamentoInput);
				simulateEnterKey(metodoPagamentoInput);
			break;
		}
		if (!ricezione)
			clickComponent(By.xpath("//input[@value='fromPoste']//parent::span"));
		insertText(this.promo, promo);	
		clickComponent(continueButton);
	}
	
	public void checkIbanEstero(){
		
	}
	
	
	public void insertRecapiti(){
		//add params
	}
		
	public void checkCampoObbligatorio() throws Exception{
		ArrayList<By> listObb = new ArrayList<By>();
		String all = "";
		if (!util.verifyVisibility(obbligatorioNome, 2))
			all = all+"Nome ";
		if (!util.verifyVisibility(obbligatorioCognome, 2))
			all = all+"Cognome ";
		if (!util.verifyVisibility(obbligatorioCF, 2))
			all = all+"Codice fiscale ";
		if (!util.verifyVisibility(obbligatorioCellulare, 2))
			all = all+"Cellulare ";
		if (!util.verifyVisibility(obbligatorioEmail, 2))
			all = all+"Email ";
		if (!util.verifyVisibility(obbligatorioConfermaEmail, 2))
			all = all+"Conferma Email ";
		System.out.println(all);
		if (all.equals(""))
			throw new Exception("Non ci sono messaggi di errore");
	}
	
	public void checkFormatoNonCorretto() throws Exception{
		ArrayList<By> listObb = new ArrayList<By>();
		String all = "";
		if (!util.verifyVisibility(formatoNome, 2))
			all = all+"Nome ";
		if (!util.verifyVisibility(formatoCognome, 2))
			all = all+"Cognome ";
		if (!util.verifyVisibility(formatoCF, 2))
			all = all+"Codice fiscale ";
		if (!util.verifyVisibility(formatoCellulare, 2))
			all = all+"Cellulare ";
		if (!util.verifyVisibility(formatoEmail, 2))
			all = all+"Email ";
		if (!util.verifyVisibility(formatoConfermaEmail, 2))
			all = all+"Conferma Email ";
		System.out.println(all);
		if (all.equals(""))
			throw new Exception("Non ci sono messaggi Formato non corretto");
	}

	public void checkBisogno() throws Exception{
		String[] bisogno = {cambioFornitore, primaAttivazione, subentro, voltura};
		By selection;
		verifyComponentExistence(legend);
		String xpath = "(//label[@class='MuiFormControlLabel-root'])[#]";
		for (int i=1; i==4; i++){
			xpath.replace("#",String.valueOf(i));
			selection = By.xpath(xpath);
			compareText(selection, bisogno[i-1], true);		
		}
	}
	
	public void selectBisogno(String bisogno) throws Exception{
		switch(bisogno) {
			case "CAMBIO FORNITORE":
				clickComponent(By.xpath("//input[@name='whatYouNeed' and @value='SWA']//ancestor::span[1]"));
				break;
		  case "PRIMA ATTIVAZIONE":
			  clickComponent(By.xpath("//input[@name='whatYouNeed' and @value='PRIMA_ATTIVAZIONE']//ancestor::span[1]"));
			  break;
		  case "SUBENTRO":
			  clickComponent(By.xpath("//input[@name='whatYouNeed' and @value='SUBENTRO']//ancestor::span[1]"));
			  break;
		  case "VOLTURA":
			  clickComponent(By.xpath("//input[@name='whatYouNeed' and @value='VOLTURA']//ancestor::span[1]"));
			  break;
		}
	}
	
	//Check informazioni fornitura
	public void informazioniFornitura() throws Exception{
		String[] bisogno = {cambioFornitore, primaAttivazione, subentro, voltura};
		By selection;
		verifyComponentExistence(legend);
		String xpath = "(//label[@class='MuiFormControlLabel-root'])[#]";
		for (int i=1; i==4; i++){
			xpath.replace("#",String.valueOf(i));
			selection = By.xpath(xpath);
			compareText(selection, bisogno[i-1], true);		
		}
		verifyComponentExistence(infoCap);
		compareText(infoCap, infoCapText, true);
		verifyComponentExistence(podPdr);
		verifyComponentExistence(podCap);
		compareText(privacy, privacyText, true);
	}
	
	public void informazioniFornituraLuce() throws Exception{
		verifyComponentVisibility(informazioniFornitura);
		checkBisogno();
		verifyComponentVisibility(infoCap);
		compareText(infoCap, infoCapText, true);
		verifyComponentVisibility(podInput);
		verifyComponentExistence(podCap);
		compareText(privacy, privacyText, true);
		verifyComponentVisibility(continueButton1);
		verifyComponentVisibility(inserisciloDopo);
	}
	
	public void informazioniFornituraGas() throws Exception{
		verifyComponentVisibility(informazioniFornitura);
		verifyComponentVisibility(infoCap);
		compareText(infoCap, infoCapText, true);
		verifyComponentVisibility(pdrInput);
		verifyComponentExistence(podCap);
		verifyComponentVisibility(continueButton1);
		verifyComponentVisibility(inserisciloDopo);
	}
	
	public void informazioniFornituraLuceGas() throws Exception{
		verifyComponentVisibility(informazioniFornitura);
		verifyComponentVisibility(infoCap);
		compareText(infoCap, infoCapText, true);
		verifyComponentVisibility(podInput);
		verifyComponentVisibility(pdrInput);
		verifyComponentExistence(podCap);
		verifyComponentExistence(pdrCap);
		verifyComponentVisibility(continueButton1);
		verifyComponentVisibility(inserisciloDopo);
		checkInfoPod();
		Thread.sleep(2);
		checkInfoPdr();
	}
	
	public void checkInfoPod() throws Exception{
		clickComponent(infoPodButton);
		String text = "è stata aperta la modale Cos'e' il POD Il POD (Point of Delivery) è il codice identificativo della fornitura elettrica e serve ad individuare il punto di prelievo dell'energia su tutto il territorio nazionaleIl POD:inizia sempre con ITè un codice alfanumerico composto da 14 caratteripotrebbe essere composto da 15 caratteri, ma è sufficiente considerare i primi 14non cambia anche se cambi fornitore di energia elettricaSe la fornitura è gia attiva lo trovi nella bolletta. L'EnelTel e' un codice numerico specifico che identifica il contatore che il distributore ha installato. Genericamente di 8-9 cifre, viene visualizzato sul display dell'apparecchio di misura alla voce 'Numero Cliente'";
		containsText(By.xpath("//*[@class='modal_content']"), text, true);
		clickComponent(By.xpath("//button[@class='modal_close_icon']"));
	}
	
	public void checkInfoPdr() throws Exception{
		clickComponent(infoPdrButton);
		String text = "è stata aperta la modale Cos'e' il PDR Il PDR (Punto di Riconsegna) è il codice che identifica la fornitura di gas.Il PDR:si compone di 14 caratteri numericinon cambia anche se cambi il fornitre di gasSe la fornitura è gia attiva lo trovi nella bolletta. La matricola contatore è il codice univoco che identifica il contatore. Genericamente composta da 6-8 cifre, è sempre segnata sul contatore nei dati di targa.";
		containsText(By.xpath("//*[@class='modal_content']"), text, true);
		clickComponent(By.xpath("//button[@class='modal_close_icon']"));
	}
	
	public void checkFornituraLuceGas() throws Exception{
		verifyComponentVisibility(By.xpath("//h4[text()='Fornitura Luce']"));
		verifyComponentVisibility(By.xpath("//h5[text()='Indirizzo di fornitura']"));
		verifyComponentExistence(inputCityLuce);
		verifyComponentExistence(inputAddressLuce);
		verifyComponentExistence(inputCivicLuce);
		verifyComponentExistence(inputCapLuce);
		String text = "Ti informiamo che la fornitura verrà attivata con i medesimi requisiti tecnici (potenza e tensione) attualmente in esercizio. Se lo desideri potrai effettuare una variazionedi potenza dopo l'attivazione del contratto direttamente dalla tua area clienti.";
		compareText(By.xpath("//*[@class='InsertSupply_info__2dK9a']"), text, false);
		verifyComponentExistence(inputAttualeFornitoreLuce);
		verifyComponentVisibility(By.xpath("//h4[text()='Fornitura Gas']"));
		compareText(By.xpath("//*[@id='gas-same-as-luce']"), "L'indirizzo di fornitura Luce corisponde all'indirizzo di fornitura Gas?", false);
		verifyComponentExistence(inputAttualeFornitoreGas);
		verifyComponentVisibility(By.xpath("//h4[text()='Recapiti']"));
		compareText(By.xpath("//*[@id='residence-address-same-descr']"), "Sei residente presso la fornitura?", false);
		compareText(By.xpath("//*[@id='postal-address-same-dual-descr']"), "Dove vuoi ricevere le comunicazioni relative alla tua fornitura?", false);
		verifyComponentExistence(continueButton1);
		verifyComponentExistence(salvaContinuaDopoButton);
	}
	
	public void insertFornituraLuceGas(String city, String region, String address, String civic, String supplierLuce, String supplierGas) throws Exception{
		Thread.sleep(3000);
		insertText(inputCityLuce, city);
		Thread.sleep(3000);
		clickComponent(By.xpath("//*[contains(text(), '" + region + "')]"));
		insertText(inputAddressLuce, address);
		Thread.sleep(3000);
		clickComponent(By.xpath("//*[contains(text(), '" + address + "')]"));
		insertText(inputCivicLuce, civic);
		clickComponent(inputCapLuce);
		insertText(inputAttualeFornitoreLuce, supplierLuce);
		clickComponent(By.xpath("//*[contains(text(), '" + supplierLuce + "')]"));
		insertText(inputAttualeFornitoreGas, supplierGas);
		clickComponent(By.xpath("//*[contains(text(), '" + supplierGas + "')]"));
		clickComponent(residentePressoFornituraSi);
		clickComponent(stessoIndirizzo);
		clickComponent(continueButton);

		
		
	}
	
	//Check and insert data Indirizzo di fornitura
	public void indirizzoFornitura(String città, String indirizzo, String civico, String cap, String fornitore, Boolean residente, Boolean comunicazioni) throws Exception{
		//By selectCap = By.xpath("//div[text()='"+cap+"']");
		By selectCittà = By.xpath("//div[@class='ant-select-item-option-content' and contains(text(), '"+città+"')]");
		By selectIndirizzo = By.xpath("//div[@class='ant-select-item-option-content' and text()='"+"VIA ABBADIA"+"']");
		//By selectIndirizzo = By.xpath("(//div[text()='"+indirizzo+"'])[2]");
		By selectFornitore = By.xpath("(//div[text()='"+fornitore+"'])[2]");
		By inputCap = By.xpath("//input[@id='postalCodeSupply' and @value ='"+cap.substring(0,5)+"'");
		verifyComponentExistence(indirizzoFornitura);
		compareText(privacy, privacyText2, true);
		insertText(inputCittà, città);
		Thread.sleep(3000);
		clickComponent(selectCittà);
		Thread.sleep(3000);
		insertText(inputIndirizzo, indirizzo.substring(0, indirizzo.length()-2));
		Thread.sleep(3000);
		clickComponent(selectIndirizzo);	
		Thread.sleep(3000);
		insertText(inputCivico, civico);
		Thread.sleep(3000);		
		insertText(inputAttualeFornitore, fornitore);
		//verifyComponentExistence(inputCap);
		clickComponent(selectFornitore);
		verifyComponentExistence(inputSi);
		verifyComponentExistence(inputNo);
		if (!residente) clickComponent(inputNo);
		verifyComponentExistence(this.comunicazioni);
		verifyComponentExistence(stessoIndirizzo);
		verifyComponentExistence(diversoIndirizzo);
		if (!comunicazioni){
			clickComponent(diversoIndirizzo);
			// add other input
		}
		clickComponent(inputCivico);
		Thread.sleep(10000);
		clickComponent(continueButton1);
		clickComponentIfExist(continueButton1);
		
	}
	
	//Enter pod and cap on Informazioni fornitura
	public void enterPodCap(String pod, String cap, String city) throws Exception{
		By selectCap = By.xpath("//div[@class='ant-select-item-option-content' and contains(text(),'"+cap+"')]");
		insertText(podInput, pod);
		Thread.sleep(1000);
		insertText(this.podCap, cap);
		Thread.sleep(1000);
//		clickComponent(selectCap);
//		Thread.sleep(1000);
		clickComponent(By.xpath("//*[contains(text(), '" + city + "')]"));
		clickComponent(continueButton1);
	}
	
	public void enterPdrCap(String pdr, String cap, String city) throws Exception{
		By selectCap = By.xpath("//div[@class='ant-select-item-option-content' and contains(text(),'"+cap+"')]");
		insertText(pdrInput, pdr);
		Thread.sleep(1000);
		insertText(this.pdrCap, cap);
		Thread.sleep(1000);
		//clickComponent(selectCap);
		clickComponent(By.xpath("//*[contains(text(), '" + city + "')]"));
		clickComponent(continueButton1);
	}
	
	//Check and insert pagamenti e bollette
	public void pagamentiBollette() throws Exception{
		verifyComponentVisibility(pagamentiBollette);
		verifyComponentVisibility(metodoPagamento);
		containsText(metodoPagamentoInput, "Addebito su conto corrente");
		verifyComponentVisibility(inputIbanStraniero);
		verifyComponentVisibility(codiceIban);
		verifyComponentVisibility(inputCodiceIban);
		verifyComponentVisibility(inputIntestatario);
		verifyComponentVisibility(inputAltroIntestatario);
		verifyComponentVisibility(modalitàRicezioneBolletta);
		verifyComponentVisibility(bollettaEmail);
		verifyComponentVisibility(bollettaCartacea);
		verifyComponentVisibility(inputEmail);
		verifyComponentVisibility(inputPhone);
		verifyComponentVisibility(codicePromozionale);
		verifyComponentVisibility(codicePromozionaleText);
		verifyComponentVisibility(promo);
	}
	
	public void pagamentiBollette(String email, String cel) throws Exception{
		verifyComponentVisibility(pagamentiBollette);
		verifyComponentVisibility(metodoPagamento);
		containsText(metodoPagamentoInput, "Addebito su conto corrente");
		verifyComponentExistence(inputIbanStraniero);
		verifyComponentVisibility(codiceIban);
		verifyComponentVisibility(inputCodiceIban);
		verifyComponentVisibility(inputIntestatario);
		verifyComponentVisibility(inputAltroIntestatario);
		verifyComponentVisibility(modalitàRicezioneBolletta);
		verifyComponentVisibility(bollettaEmail);
		verifyComponentVisibility(By.xpath("//input[@name='email' and @value = '" + email + "']"));
		verifyComponentVisibility(By.xpath("//input[@name='phone' and @value = '" + cel + "']"));
		verifyComponentVisibility(codicePromozionale);
		verifyComponentVisibility(codicePromozionaleText);
		verifyComponentVisibility(promo);
		verifyComponentExistence(continueButton1);
		verifyComponentExistence(salvaContinuaDopoButton);
	}
	
	public void selectMOP(String mop) throws Exception{
		switch(mop){
			case "Bollettino Postale":
				 clickComponent(metodoPagamentoInput);
				 clickComponent(By.xpath("//div[@class='ant-select-item-option-content' and text()='Bollettino Postale']"));
				 verifyComponentInvisibility(inputIbanStraniero);
				 verifyComponentInvisibility(codiceIban);
				 verifyComponentInvisibility(inputCodiceIban);
				 verifyComponentInvisibility(inputIntestatario);
				 verifyComponentInvisibility(inputAltroIntestatario);
		}
	}
	
	public void insertIbanAndContinue(Boolean estero, String iban) throws Exception{
		if (estero)
			clickComponent(inputIbanEstero);
		insertText(inputCodiceIban, iban);
		clickComponent(continueButton);
	}
	
	public void checkMandatiConsensi() throws Exception{
		verifyComponentExistence(richiestaAnticipataTitle);
		verifyComponentExistence(richiestaAnticipata);
		verifyComponentExistence(maggioriInfo);
		verifyComponentExistence(siAttivaSubito);
		verifyComponentExistence(noAttendi);
		clickComponent(maggioriInfo);
		clickComponent(By.xpath("(//button[@aria-label='chiudi'])[2]"));
		verifyComponentVisibility(modalBody);
		compareText(modalBody, maggioriInfoText, true);
		clickComponent(modalCloseButton);
	}
	
	public void checkMandatiConsensiLuce() throws Exception{
		verifyComponentVisibility(mandantiConsensi);
		verifyComponentExistence(mandatiConsensiBy1);
		compareText(mandatiConsensiBy1, mandatiConsensiText1, true);
		verifyComponentExistence(inputMandatiConsensi);
		compareText(inputMandatiConsensi, inputMandatiConsensiText, true);
		verifyComponentExistence(richiestaAnticipataTitle);
		verifyComponentExistence(richiestaAnticipata);
		verifyComponentExistence(maggioriInfo);
		verifyComponentExistence(siAttivaSubito);
		verifyComponentExistence(noAttendi);
		clickComponent(maggioriInfo);
		verifyComponentVisibility(modalBody);
		compareText(modalBody, maggioriInfoText, true);
		clickComponent(modalCloseButton);
	}
	
	public void insertMandatiConsensi() throws Exception{
		clickWithJS(consensi1);
		clickWithJS(consensi2);
		clickWithJS(consensi3);
		clickWithJS(esecuzioneAnticipataSI);
		clickWithJS(marketing);
		clickWithJS(marketingThird);
		clickWithJS(marketingProfiling);		
	}
	
	public void insertMandatiConsensiLuce() throws Exception{
		clickWithJS(consensi1);
		clickWithJS(consensi2);
		clickWithJS(consensi3_ele);
		clickWithJS(esecuzioneAnticipataSI);
		Thread.sleep(1);
		clickWithJS(By.xpath("//div[@role='dialog']//button[@aria-label='chiudi']"));
		Thread.sleep(1);
		clickWithJS(marketing);
		clickWithJS(marketingThird);
		clickWithJS(marketingProfiling);	
		clickWithJS(completaAdesione);		
	}
	
	public void insertMandatiConsensiLuceGas() throws Exception{
		clickWithJS(consensi1);
		clickWithJS(consensi2);
		clickWithJS(consensi3_ele);
		clickWithJS(consensi3_gas);
		clickWithJS(esecuzioneAnticipataSI);
		Thread.sleep(1);
		clickWithJS(By.xpath("//div[@role='dialog']//button[@aria-label='chiudi']"));
		Thread.sleep(1);
		clickWithJS(marketing);
		clickWithJS(marketingThird);
		clickWithJS(marketingProfiling);	
		clickWithJS(completaAdesione);	
	}
		
	public void checkSalvaOraContinuaDopo(String nome, String cognome, String email, String cell, String cf, WebDriver driver) throws Exception{
		//By esciButton = By.xpath("//*[@id='scroll-dialog-title']/button");
		By salvaContinuaTitle = By.xpath("//*[@class='title' and text()='Salva ora, continua dopo']");
		By salvaContinuaTextBy = By.xpath("//div[@role='dialog']//div[@class='message']");
		String salvaContinuaText = "Inserisci i tuoi dati e riceverai un link, con cui riprendere il processo di adesione al contratto dal punto in cui lo ha lasciato. I dati da te rilasciati saranno conservati da Enel Energia per 5 giorni solari, trascorsi i quali il link non sarà più accessibile e i dati saranno cancellati.";
		By dialogMain = By.xpath("//div[@role='dialog']//div[@class='main']");
		By question = By.xpath("//*[@id='scroll-dialog-description']//p[text()='Puoi indicarci il motivo per cui stai uscendo dal processo?']");
		By check1 = By.xpath("//*[@id='otherOffersId' and @type ='radio']//ancestor::label");
		By check2 = By.xpath("//*[@id='noTimeId' and @type ='radio']//ancestor::label");
		By check3 = By.xpath("//*[@id='noInfoId' and @type ='radio']//ancestor::label");
		By check4 = By.xpath("//*[@id='otherId' and @type ='radio']//ancestor::label");
		//By salvaEsciButton = By.xpath("//*[@id='scroll-dialog-description']//button[text()='Salva ed esci']");
		//By esciSenzaSalvareButton = By.xpath("//*[@id='scroll-dialog-description']//button[text()='Esci senza salvare']");
		By finalTextBy = By.xpath("//*[@id='scroll-dialog-description']/div[5]");
		String finalText = "Il Titolare del trattamento dei dati è Enel Energia che li tratterà nel rispetto della vigente normativa in materia di privacy e protezione dei dati. Informativa privacy completa disponibile sul sito www.enel.it";

		verifyComponentVisibility(esciButton);
		verifyComponentVisibility(salvaContinuaTitle);
		verifyComponentVisibility(salvaContinuaTextBy);
		containsText(salvaContinuaTextBy, salvaContinuaText);
		containsText(dialogMain, "NOME*");
		containsText(dialogMain, "COGNOME*");
		containsText(dialogMain, "EMAIL*");
		containsText(dialogMain, "CELLULARE*");
		containsText(dialogMain, "CODICE FISCALE*");
		Thread.sleep(1000);
		driver.findElement(By.xpath("(//div[@role='dialog']//div[@class='main']//input)[1]")).getAttribute("value").equals(nome);
		driver.findElement(By.xpath("(//div[@role='dialog']//div[@class='main']//input)[2]")).getAttribute("value").equals(cognome);
		driver.findElement(By.xpath("(//div[@role='dialog']//div[@class='main']//input)[3]")).getAttribute("value").equals(email);
		driver.findElement(By.xpath("(//div[@role='dialog']//div[@class='main']//input)[5]")).getAttribute("value").equals(cell);
		driver.findElement(By.xpath("(//div[@role='dialog']//div[@class='main']//input)[6]")).getAttribute("value").equals(cf);
		verifyComponentVisibility(question);
		compareText(check1,"Voglio vedere altre offerte", true);
		compareText(check2,"Sono interessato ma adesso non ho tempo", true);
		compareText(check3,"Non ho le informazioni richieste", true);
		compareText(check4,"Altro", true);
		verifyComponentVisibility(salvaEsciButton);
		verifyComponentVisibility(esciSenzaSalvareButton);
		compareText(finalTextBy, finalText, true);
	}
	
	public void checkFinalText(String email) throws Exception{
		verifyComponentVisibility(passoAdesione);
		containsText(passoAdesioneBody, passoAdesioneText_v2.replace("$", email), true);
	}
	
}
