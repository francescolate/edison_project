package com.nttdata.qa.enel.components.lightning;


import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * @author ChianteseRo
 * Seleziona il Cliente Attivo Dopo Ricerca POD su Nuova Interazione
 */

public class SelezionaClienteAttivoComponentEVO {
    private WebDriver driver;
    private SeleniumUtilities util;

    public SelezionaClienteAttivoComponentEVO(WebDriver driver) {
        this.driver = driver;
        this.util = new SeleniumUtilities(this.driver);
    }


    private By spanClienteAttivo = By.xpath("//span[@class='slds-radio--faux']");

    private By searchBtn = By.xpath(".//button[@id='searchAccountBtn']");

    private By frameBy = By.xpath("//iframe[@title = 'accessibility title']");

    public By tableBy = new By.ByXPath("//table[@id ='clientsTable2']");

    private By buttonConferma = By.xpath("(//*[contains(text(),'Conferma')])[2]");
    

    private void swichToDefault() throws Exception {
        this.driver.switchTo().defaultContent();
    }


    @Deprecated
    public void fillCfAndSearch(String param) throws Exception {
        TimeUnit.SECONDS.sleep(60);
        String x = util.frameSearcher(
                frameBy,
                spanClienteAttivo,
                r -> {
                    r.sendKeys(param);
                }
        );


        WebElement frameToSw = driver.findElement(
                By.xpath("//iframe[@name='" + x + "'] | //frame[@name='" + x + "']")
        );
        driver.switchTo().frame(frameToSw).findElement(searchBtn).click();
        this.swichToDefault();

    }

    /**
     * select last client for Cf
     */
    public void fillCfAndSearchLastElementinTable() {

        util.getFrameActive();

        try {

            WebElement table;
            table = util.waitAndGetElement(tableBy);
            util.scrollToElement(table);

            if (util.getTableRowCount(table) < 1)
                throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");
            if (util.getTableRowCount(table) > 1) {
                List<WebElement> rowElement = util.waitAndGetElements(table, spanClienteAttivo);
                rowElement.get(rowElement.size() - 2).click();
            }

            this.driver.switchTo().defaultContent();
        } catch (Exception e) {
        	
            System.out.println("qui non si apre la popup interlocutore");
        }
        this.driver.switchTo().defaultContent();
    }


    /**
     * select client for Cf
     */
    public void fillCfAndSearch3() throws Exception {
        TimeUnit.SECONDS.sleep(5);//era 20
        util.getFrameActive();

        try {
            WebElement table;
            table = util.waitAndGetElement(tableBy);
            util.scrollToElement(table);
            if (util.getTableRowCount(table) < 1)
                throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");
            if (util.getTableRowCount(table) > 1) {
                List<WebElement> rowElement = util.waitAndGetElements(table, spanClienteAttivo);
                rowElement.get(1).click();
            }

            TimeUnit.SECONDS.sleep(2);
            this.driver.switchTo().defaultContent();
        } catch (Exception e) {
            // nessuno messaggio di errore perchè c'è un solo Referente/Interlocutore
        	
            System.out.println("qui non si apre la popup interlocutore");
        }

        TimeUnit.SECONDS.sleep(2);
        this.driver.switchTo().defaultContent();
    }
    
    public void fillCfAndSearch4() throws Exception {
        TimeUnit.SECONDS.sleep(5);//era 20
        util.getFrameActive();

        try {
            WebElement table;
            table = util.waitAndGetElement(tableBy);
            util.scrollToElement(table);
            if (util.getTableRowCount(table) < 1)
                throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");
            if (util.getTableRowCount(table) > 1) {
                List<WebElement> rowElement = util.waitAndGetElements(table, spanClienteAttivo);
                rowElement.get(2).click();
            }

            TimeUnit.SECONDS.sleep(2);
            this.driver.switchTo().defaultContent();
        } catch (Exception e) {
            // nessuno messaggio di errore perchè c'è un solo Referente/Interlocutore
        	
            System.out.println("qui non si apre la popup interlocutore");
        }

        TimeUnit.SECONDS.sleep(2);
        this.driver.switchTo().defaultContent();
    }

    public void fillCfAndChooseClientType(String type, String name) throws Exception {
        TimeUnit.SECONDS.sleep(5);//era 10
        String frame = util.getFrameByIndex(0);
        //
        //driver.findElement(by)
        WebElement frametoSw = driver.findElement(By.xpath(
                "//iframe[@name='" +
                        frame
                        + "']"));
        driver.switchTo().frame(frametoSw);
        WebElement table = driver.findElement(tableBy);

        table = util.waitAndGetElement(tableBy);
        util.scrollToElement(table);
        if (util.getTableRowCount(table) < 1)
            throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

        if (util.getTableRowCount(table) > 1) {
            int rowNum = util.getTableRowFromColumnData(table, "Nome Cliente", name, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]");

            String tipologia = util.getTableCellText(table, rowNum, "Tipologia Cliente", "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", 0);

            if (!tipologia.equals(type))
                throw new Exception("Il Cliente " + name + " non risulta di tipologia " + type + ". Tipologia riscontrata " + tipologia + ". Impossibile proseguire");

            //System.out.println("Riga in tabella numero "+rowNum);

            List<WebElement> rowElement = util.waitAndGetElements(table, spanClienteAttivo);
            rowElement.get((rowNum - 1)).click();
        }

        TimeUnit.SECONDS.sleep(2);
        this.driver.switchTo().defaultContent();
    }
    
    public void fillCfAndChooseClientTypeNew(String type, String name) throws Exception {
    	
   	 WebElement table = driver.findElement(tableBy);

        table = util.waitAndGetElement(tableBy);
        util.scrollToElement(table);
        if (util.getTableRowCount(table) < 1)
            throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

        if (util.getTableRowCount(table) > 1) {
            int rowNum = util.getTableRowFromColumnData(table, "Nome Cliente", name, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]");

            String tipologia = util.getTableCellText(table, rowNum, "Tipologia Cliente", "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", 0);

            if (!tipologia.equals(type))
                throw new Exception("Il Cliente " + name + " non risulta di tipologia " + type + ". Tipologia riscontrata " + tipologia + ". Impossibile proseguire");

            //System.out.println("Riga in tabella numero "+rowNum);

            List<WebElement> rowElement = util.waitAndGetElements(table, spanClienteAttivo);
            rowElement.get((rowNum - 1)).click();
        }

        TimeUnit.SECONDS.sleep(2);
   	
   }
    
    public void fillCfAndChooseClientType1(String type, String name) throws Exception {
        TimeUnit.SECONDS.sleep(5);//era 10
        String frame = util.getFrameByIndex(0);
        //
        //driver.findElement(by)
        WebElement frametoSw = driver.findElement(By.xpath(
                "//iframe[@name='" +
                        frame
                        + "']"));
        driver.switchTo().frame(frametoSw);
        WebElement table = driver.findElement(tableBy);

        table = util.waitAndGetElement(tableBy);
        util.scrollToElement(table);
        if (util.getTableRowCount(table) < 1)
            throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");

        if (util.getTableRowCount(table) > 1) {
            int rowNum = util.getTableRowFromColumnData(table, "Nome Cliente", name, "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]");

            String tipologia = util.getTableCellText(table, rowNum, "Tipologia Cliente", "./thead/tr/th", "./tbody/tr[%d]", "/td[%d]", 0);

            if (!tipologia.equals(type))
                throw new Exception("Il Cliente " + name + " non risulta di tipologia " + type + ". Tipologia riscontrata " + tipologia + ". Impossibile proseguire");

            System.out.println("Riga in tabella numero "+rowNum);

            List<WebElement> rowElement = util.waitAndGetElements(table, spanClienteAttivo);
            rowElement.get(3).click();
        }

        TimeUnit.SECONDS.sleep(2);
        this.driver.switchTo().defaultContent();
    }


    /**
     * WorkAround per gestire popup in presenza di n interlocutori associati al cliente
     *
     * @param idxFrame --> Frame della pagina della interazione
     * @throws Exception
     */
    public void setInterlocutore(int idxFrame) throws Exception {
        //click su Tab precedente della interazione creata per verificare popup Ricerca interlocutore
        WebElement tabElement = driver.findElement(By.xpath("//span[@title='ITA_IFM_Interaction__c']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(1);
        String frame = util.getFrameByIndex(idxFrame);
        WebElement frametoSw = driver.findElement(By.xpath(
                "//iframe[@name='" +
                        frame
                        + "']"));
        driver.switchTo().frame(frametoSw);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        try {
            //verificare Esistenza popup Ricerca interlocutore
            WebElement popUpElement = driver.findElement(By.xpath("//h2[@id='genericModalTitle_A' and text()='Ricerca Interlocutore']"));
            if (popUpElement.isDisplayed()) {
                WebElement interlocutoreElement = driver.findElement(By.xpath("//tr/th/a[@class='recordItem']"));
                interlocutoreElement.click();
                TimeUnit.SECONDS.sleep(2);
            }

        } catch (Exception e) {
            System.out.println("qui non si apre la popup interlocutore"+e.getLocalizedMessage());
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);//era 100

        this.driver.switchTo().defaultContent();
        TimeUnit.SECONDS.sleep(5);//era 20
        //click su Tab Account per passare nuovamente al TAB per selezionare "Tutti i processi"
        tabElement = driver.findElement(By.xpath("//a/span[@title='Account']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(5);//era 10
    }

    public void setInterlocutoreNonTitolareNew(int idxFrame) throws Exception {
        //click su Tab precedente della interazione creata per verificare popup Ricerca interlocutore
        WebElement tabElement = driver.findElement(By.xpath("//span[@title='ITA_IFM_Interaction__c']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(1);
        String frame = util.getFrameByIndex(idxFrame);
        WebElement frametoSw = driver.findElement(By.xpath(
                "//iframe[@name='" +
                        frame
                        + "']"));
        driver.switchTo().frame(frametoSw);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        try {
            //verificare Esistenza popup Ricerca interlocutore
            WebElement popUpElement = driver.findElement(By.xpath("//h2[@id='genericModalTitle_A' and text()='Ricerca Interlocutore']"));
            if (popUpElement.isDisplayed()) {
                WebElement interlocutoreElement = driver.findElement(By.xpath("(//tr/th/a[@class='recordItem'])[2]"));
                interlocutoreElement.click();
                TimeUnit.SECONDS.sleep(2);
            }

        } catch (Exception e) {
            System.out.println("qui non si apre la popup interlocutore"+e.getLocalizedMessage());
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);//era 100

        this.driver.switchTo().defaultContent();
        TimeUnit.SECONDS.sleep(5);//era 20
        //click su Tab Account per passare nuovamente al TAB per selezionare "Tutti i processi"
        tabElement = driver.findElement(By.xpath("//a/span[@title='Account']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(5);//era 10
    }

    public void setInterlocutoreNonTitolare(int idxFrame, String codFisc) throws Exception {
        //click su Tab precedente della interazione creata per verificare popup Ricerca interlocutore
        WebElement tabElement = driver.findElement(By.xpath("//span[@title='ITA_IFM_Interaction__c']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(1);
        String frame = util.getFrameByIndex(idxFrame);
        WebElement frametoSw = driver.findElement(By.xpath(
                "//iframe[@name='" +
                        frame
                        + "']"));
        driver.switchTo().frame(frametoSw);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        try {
            //verificare Esistenza popup Ricerca interlocutore
            WebElement popUpElement = driver.findElement(By.xpath("//h2[@id='genericModalTitle_A' and text()='Ricerca Interlocutore']"));
            WebElement interlocutoreElement = driver.findElement(By.xpath("//tr/th[text()!='" + codFisc + "']/ancestor::tr/th/a[@class='recordItem']"));
            interlocutoreElement.click();
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            System.out.println("qui non si apre la popup interlocutore");
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);// era 100

        this.driver.switchTo().defaultContent();
        //click su Tab Account per passare nuovamente al TAB per selezionare "Tutti i processi"
        tabElement = driver.findElement(By.xpath("//a/span[@title='Account']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(2);
    }

    public void setInterlocutoreConReferente(int idxFrame, String codFisc) throws Exception {
        //click su Tab precedente della interazione creata per verificare popup Ricerca interlocutore
        WebElement tabElement = driver.findElement(By.xpath("//span[@title='ITA_IFM_Interaction__c']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(1);
        String frame = util.getFrameByIndex(idxFrame);
        WebElement frametoSw = driver.findElement(By.xpath(
                "//iframe[@name='" +
                        frame
                        + "']"));
        driver.switchTo().frame(frametoSw);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        try {
            //verificare Esistenza popup Ricerca interlocutore
            WebElement popUpElement = driver.findElement(By.xpath("//h2[@id='genericModalTitle_A' and text()='Ricerca Interlocutore']"));
            WebElement interlocutoreElement = driver.findElement(By.xpath("//tr/th[text()='" + codFisc + "']/ancestor::tr/th/a[@class='recordItem']"));
            interlocutoreElement.click();
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            System.out.println("qui non si apre la popup interlocutore");
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);// era 100

        this.driver.switchTo().defaultContent();
        //click su Tab Account per passare nuovamente al TAB per selezionare "Tutti i processi"
        tabElement = driver.findElement(By.xpath("//a/span[@title='Account']"));
        tabElement.click();
        TimeUnit.SECONDS.sleep(2);
    }

}
