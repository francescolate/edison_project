package com.nttdata.qa.enel.components.lightning;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class RichiestaCollaComponent {
	
	private WebDriver driver;
    private SeleniumUtilities util;
    SpinnerManager spinner;
    JavascriptExecutor jse;
	
	public RichiestaCollaComponent(WebDriver driver)

    {

         this.driver = driver;
         util = new SeleniumUtilities(driver);
         this.spinner = new SpinnerManager(driver);
         PageFactory.initElements(driver, this);

    }

    private By searchBy = By.xpath("//input[@name='Case-search-input']");
    public final By tabCorrelato=By.xpath("//a[@title='Correlato'] and @aria-selected='true']");
    public final By elementoRichiestaPreventivo=By.xpath("//td/span[text()='PREVENTIVO INVIATO']/ancestor::tr//a[contains(text(),'OI-')]");
    public final By buttonGestionePreventivo=By.xpath("//button[text()='Gestione Preventivo']");
    public static final String X_DROPDOWN = "//button[@class='slds-button slds-p-horizontal__"

               + "xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon[1]" ;
    public By RichestaNumber = By.xpath("//div[@class='slds-card__body']/div[@class='slds-card__body slds-card__body_inner slds-scrollable slds-m-top_small']/div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][1]/span[@class='slds-text-heading_small']/a");
    public final By status = By.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//span[text()='Stato']/following::*/span");
	public final By sottostatus = By.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//span[text()='Sottostato']/following::*/span");
	
	public By RichestaNum = By.xpath("//div[2]/div/div[@class='previewMode SMALL forceRelatedListPreview']/div/div[@class='slds-card__body']/ul[@class='uiAbstractList']/li[@class='slds-var-p-horizontal_medium slds-var-p-vertical_xx-small desktop forceImageRelatedListStencil forceRecordLayout'][1]/div[@class='listItemBody withActions']/h3[@class='primaryField']/div[@class='outputLookupContainer forceOutputLookupWithPreview']/a");
	public By RichestaNum1 = By.xpath("//div[@class='container']/div[@class='container forceRelatedListSingleContainer'][2]/article[@class='slds-card slds-card_boundary headerBottomBorder forceRelatedListCardDesktop']/div[2]/div/div[@class='previewMode SMALL forceRelatedListPreview']/div/div[@class='slds-card__body']/ul[@class='uiAbstractList']/li[@class='slds-var-p-horizontal_medium slds-var-p-vertical_xx-small desktop forceImageRelatedListStencil forceRecordLayout'][1]/div[@class='listItemBody withActions']/h3[@class='primaryField']/div[@class='outputLookupContainer forceOutputLookupWithPreview']/a");
	public final By elementiRichiestaSection = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]");

	public final By elementiOrdineSection = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]");
    public By RichestaDetails = By.xpath("//*[@id='tab-15']/slot/flexipage-component2[2]/slot/flexipage-aura-wrapper/div/div/div/div[2]/article/div[2]/div/div/div/div/ul/li[1]");
   
   
    public By richestaHeader1 = By.xpath("(//div[@class='container forceRelatedListSingleContainer']//span[contains(text(), 'Richieste')])[1]");
//    public By RichestaHeader  = By.xpath("//div[@class='container forceRelatedListSingleContainer']//span[contains(text(), 'Richieste')]");
    public By RichestaHeader  = By.xpath("//span[contains(text(),'+')]/preceding-sibling::span[contains(text(), 'Richieste')]");
    public By RichestaHeaderLink = By.xpath("//div[@class='slds-media__body slds-align-middle']//span[contains(text(), 'Richieste')]");
    public String RichestaDetail = "//span[contains(text(), '12/06/2020')]/ancestor::td/preceding-sibling::td//a[text()='GESTIONE PRIVACY']/ancestor::td/preceding-sibling::th//a";
    public String RichestaDetailDate = "//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//a[text()='GESTIONE PRIVACY']/ancestor::td/preceding-sibling::th//a";
    
    public By richestaNumero = By.xpath("//span[@class='slds-grid slds-grid--align-spread']/child::a[@class='slds-truncate outputLookupLink slds-truncate outputLookupLink-5001l000005RYSHAA4-354:4349;a forceOutputLookup']");
    
    public String richestaNumertoday = "//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//a[text()='DISDETTA CON SUGGELLO']/ancestor::td/preceding-sibling::th//a";
    
    public String richestaAttivazioneToday1 = "(//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//a[text()='ATTIVAZIONE SDD']/ancestor::td/preceding-sibling::th//a)[2]";
    public String richestaAttivazioneToday = "//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//a[text()='ATTIVAZIONE SDD']/ancestor::td/preceding-sibling::th//a";
    public By itemName = By.xpath("//span[text()='Item Name']/ancestor::table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit']//span//a");
//    public String richestaVariazioneToday_VAS = "(//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//a[text()='VARIAZIONE VAS']/ancestor::td/preceding-sibling::th//a)[1]";
    public String richestaVariazioneToday_VAS = "//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//span[text()='In Lavorazione']/parent::span/parent::td/preceding-sibling::td//a[text()='VARIAZIONE VAS']/ancestor::td/preceding-sibling::th//a";
    public String richestaVariazioneToday = "//span[contains(text(), '$DATE$')]/ancestor::td/preceding-sibling::td//span[text()='In Lavorazione']/parent::span/parent::td/preceding-sibling::td//a[text()='VARIAZIONE SDD']/ancestor::td/preceding-sibling::th//a";
 
    public By elementiOperazioneHeader = By.xpath("//div[@class='listWrapper']//span[contains(text(),'Elementi Operazione')]");
    public By elementiOperazione_date = By.xpath("//span[contains(text(), '06/11/2020')]/ancestor::tbody//td[@class='slds-cell-edit cellContainer slds-has-focus']//span[contains(text(),'Attivazione RID')]/ancestor::td/preceding-sibling::th//a");
    public String elementiOperazion_today = "//span[contains(text(), '$DATE$')]/ancestor::tbody//td[@class='slds-cell-edit cellContainer slds-has-focus']//span[contains(text(),'Attivazione RID')]/ancestor::td/preceding-sibling::th//a";
    public By pod_IT002E8969300A =By.xpath("//span[text()='IT002E8969300A']/ancestor::tr/child::th[@class='slds-cell-edit cellContainer']/child::span//a[text()='IT002E8969300A']");
    public By pod_IT002E6365412A = By.xpath("//span[text()='IT002E6365412A']/ancestor::tr/child::th[@class='slds-cell-edit cellContainer']/child::span//a[text()='IT002E6365412A']");
    public By elementi_todaydate = By.xpath("//*[contains(text(),'02/09/2020')]/ancestor::div[@class='slds-media__body']/descendant::div[@class='slds-grid']/child::a//span[@id='window']");
    public By bpmId = By.xpath("//span[text()='ID BPM']/ancestor::div[@class='slds-grid slds-size_1-of-1 label-stacked']/descendant::div[@class='slds-form-element__control']//lightning-formatted-text");
    public By bpm_id = By.xpath("//span[text()='ID BPM']/parent::div/following-sibling::div//lightning-formatted-text");
    public String bpm = "//span[text()='ID BPM']/ancestor::div[@class='slds-grid slds-size_1-of-1 label-stacked']/descendant::div[@class='slds-form-element__control']//lightning-formatted-text";
    public By inputBpm = By.xpath("//input[@name='idBpm']");
    public By EsitoDropDown = By.xpath("//select[@name='esito']");
    public By submitButton = By.xpath("//input[@type='submit']");
    
    public By iDbmpValue = By.xpath("//*[contains(text(),'pv0:')]");
    public String richestaVariazioneTodaySDD = "(//span[text()='In Lavorazione']/ancestor::td/preceding-sibling::td//a[text()='VARIAZIONE SDD']/ancestor::td/preceding-sibling::th//a)";
    public By iDbmpValueInput = By.xpath("//input[@name='idBpm']");
    public By esitoCombo = By.xpath("//select[@name='esito']");
    public By esitoOptionKO = By.xpath("//select[@name='esito']/option[2]");
    public By submitButton2 = By.xpath("//input[@type='submit']");
    
    public String request_338 = "(//span[contains(text(),'$Date$')]/ancestor::td/following-sibling::td//span[text()='INVIATO']/ancestor::td/preceding-sibling::td//span//a[text()='VARIAZIONE SDD']//ancestor::td/preceding-sibling::th//span//a)[2]";
    @FindBy(xpath = X_DROPDOWN)

    private WebElement dropdwonButton;

    /**

    * @param richiestaInput

    * @throws Exception

    */
    public void searchRichiesta(String richiestaInput) throws Exception
    {
         //String frame = util.getFrameByIndex(0);

         TimeUnit.SECONDS.sleep(2);

         //         dropdwonButton.click();

         //         serachObject = xpathTab;

         //         util.frameManager(frame, searchObject, util.sendKeys, richiestaInput);

         //         WebElement searchObject = driver.findElement(searchBy);

         //          searchObject.sendKeys(richiestaInput);

         util.objectManager(searchBy, util.sendKeysCharByChar, richiestaInput);

         TimeUnit.SECONDS.sleep(10);

         //         util.frameManager(frame, searchObject, util.sendKeys, Keys.ENTER.toString());

         //          searchObject.sendKeys(Keys.ENTER);

    //    util.objectManager(searchBy, util.sendKeysCharByChar, Keys.ENTER.toString());

    util.objectManager(By.xpath("//span[contains(text(),' Sandbox: UAT')]"), util.scrollAndClick);

         //span[contains(text(),' Sandbox: UAT')]

         TimeUnit.SECONDS.sleep(10);

         String xPathElement = "//a[@title='"+richiestaInput+"']";

         By element = By.xpath(xPathElement);

         if (!util.verifyExistence(element, 30))

         {

               throw new Exception("Errore! Il codice richiesta " + richiestaInput + " non presente a sistema!");

         }

         //    util.frameManager(frame, element, util.scrollAndClick);

         //Istanzio oggetto link della Richiesta

         //         searchObject = driver.findElement(element);

         //         searchObject.click();

         util.objectManager(element, util.scrollAndClick);
    }

    public void verifySottostatoRichiesta(String sottoStatoInput) throws Exception
    {
         //         String frame = util.getFrameByIndex(0);

         TimeUnit.SECONDS.sleep(2);
         String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Sottostato']/../div/span[@title='"+sottoStatoInput+"']";
         By element = By.xpath(byElement);
         if (!util.verifyExistence(element, 10))
         {
              throw new Exception("Errore! Il sottostato richiesta non è " + sottoStatoInput);
         }                    

    }

    public boolean verificaSottostatoRichiesta(String sottoStatoInput) throws Exception
    {
    //         String frame = util.getFrameByIndex(0);

         TimeUnit.SECONDS.sleep(2);
         String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Sottostato']/../div/span[@title='"+sottoStatoInput+"']";
         By element = By.xpath(byElement);
//       if (!util.verifyExistence(element, 10))
         if (!util.exists(element, 20))
         {
               return false;
         }

         else{

               return true;
         }

    }

    public void verifyStatoRichiesta(String statoInput) throws Exception
    {
         //String frame = util.getFrameByIndex(0);
         TimeUnit.SECONDS.sleep(2);
         String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Stato']/../div/span[@title='"+statoInput+"']";
         By element = By.xpath(byElement);
         if (!util.verifyExistence(element, 10))
         {

               throw new Exception("Errore! Stato richiesta non è " + statoInput);
         }                    

    }

    public boolean verificaStatoRichiesta(String statoInput) throws Exception
    {

         TimeUnit.SECONDS.sleep(2);
         String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Stato']/../div/span[@title='"+statoInput+"']";
         By element = By.xpath(byElement);
//       if (!util.verifyExistence(element, 10))

         if (!util.exists(element, 20))
         {
               return false;
         }
         else{

               return true;
         }

    }

    public void verifyStatoOfferta(String statoInput) throws Exception
    {
         //String frame = util.getFrameByIndex(0);
         TimeUnit.SECONDS.sleep(2);
         String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Stato']/../div/span[@title='"+statoInput+"']";
         By element = By.xpath(byElement);
         if (!util.verifyExistence(element, 10))
         {
               throw new Exception("Errore! Stato richiesta non è " + statoInput);
         }                    

    }
    public String getBPMId(By BpmObject){
    	
    	String BPM = driver.findElement(BpmObject).getText();
    	System.out.println(BPM);
    	return BPM;
    
    }
    
    public void CancelActivate(String BPM,String url) throws Exception{
    	//Current Window 
    	String MainWindowHandle = driver.getWindowHandle();
    	
    	// Initialize the robot class object
        Robot robot = new Robot();
        // Press and hold Control and N keys
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_N);
        // Release Control and N keys
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_N);

        // Setting focus to the newly opened browser window
        ArrayList <String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size()-1));
        for (String windowHandle : driver.getWindowHandles()) {
            driver.switchTo().window(windowHandle);
        }

        // Continue actions in the new browser window
        //driver.get(url);
        driver.manage().window().maximize();
		driver.navigate().to(url);
		Thread.sleep(10000);
		driver.findElement(By.xpath("//input[@name='idBpm']")).sendKeys(BPM);
		WebElement dropdown = driver.findElement(By.xpath("//select[@name='esito']"));
		Select sel = new Select(dropdown);
		sel.selectByValue("KO");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		Thread.sleep(20000);
		driver.close();
		driver.switchTo().window(MainWindowHandle);
			        
        }
    
    public void enableActivate(String BPM,String url) throws Exception{
    	//Current Window 
    	String MainWindowHandle = driver.getWindowHandle();
    	
    	// Initialize the robot class object
        Robot robot = new Robot();
        // Press and hold Control and N keys
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_N);
        // Release Control and N keys
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_N);

        // Setting focus to the newly opened browser window
        ArrayList <String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size()-1));
        for (String windowHandle : driver.getWindowHandles()) {
            driver.switchTo().window(windowHandle);
        }

        // Continue actions in the new browser window
        //driver.get(url);
        driver.manage().window().maximize();
		driver.navigate().to(url);
		Thread.sleep(10000);
		driver.findElement(By.xpath("//input[@name='idBpm']")).sendKeys(BPM);
		WebElement dropdown = driver.findElement(By.xpath("//select[@name='esito']"));
		Select sel = new Select(dropdown);
		sel.selectByValue("OK");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		Thread.sleep(20000);
		driver.close();
		driver.switchTo().window(MainWindowHandle);
			        
        }
    
    public String annulaActivate()throws Exception {
    	WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bpm)));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String value = (String)js.executeScript("return (document.evaluate(\""+bpm+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        return value;
       // if(value.equals("")) throw new Exception("Field is not pre filled");
    }
    	
    public void clickTabCorrelato(By tab) throws Exception {

         util.objectManager(tab, util.click);

         spinner.checkSpinners();
    }

    public void clickElementoRichiestaPreventivo(By linkOI) throws Exception {
         try{
         util.objectManager(linkOI, util.click);
         }
         catch(Exception e){

               throw new Exception("Lo stato della preventivo risulta diverso da 'PREVENTIVO INVIATO'");
         }
         spinner.checkSpinners();
    }

    public void clickButtonGestionePreventivo(By buttonGestionePreventivo) throws Exception {
          util.objectManager(buttonGestionePreventivo, util.click);
         spinner.checkSpinners();
    }

    public void espandiElementiRichiesta() throws Exception{

    util.objectManager(By.xpath("//span[contains(text(),'Navigazione processo')]/ancestor::article//div[contains(text(),'Elementi della Richiesta')]"), util.scrollAndClick);

    }

    public void verifyRichestaHeader() throws Exception{
         Thread.sleep(3000);
         
         List<WebElement> li = driver.findElements(By.xpath("//slot[@class='slds-grid slds-page-header__detail-row']/*[contains(@role,'listitem')]"));

         for(WebElement element : li){
             String details=element.getText();
             details= details.replaceAll("(\r\n|\n)", " ");
             System.out.println(details);
         }
    }
    
    public void verifyRichestaDetails() throws Exception {
    	Thread.sleep(3000);
        
        List<WebElement> li = driver.findElements(By.xpath("//div[@class='container']/div[@class='container forceRelatedListSingleContainer'][2]/article[@class='slds-card slds-card_boundary headerBottomBorder forceRelatedListCardDesktop']/div[2]/div/div[@class='previewMode SMALL forceRelatedListPreview']/div/div[@class='slds-card__body']/ul[@class='uiAbstractList']/li[@class='slds-var-p-horizontal_medium slds-var-p-vertical_xx-small desktop forceImageRelatedListStencil forceRecordLayout'][1]"));

        for(WebElement element : li){
            String details=element.getText();
            details= details.replaceAll("(\r\n|\n)", " ");
            //System.out.println(details);
        }
    	
    }

        public void pressJavascript(By clickObject) throws Exception {
		WebElement element = util.waitAndGetElement(clickObject);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}
    public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	  public void scrollComponent(By object) throws Exception {
			util.objectManager(object, util.scrollToVisibility, false);
		}
	  public void scrollDown() {
	   		jse.executeScript("scroll(0, 1000)");
	    }
	  public void scrollToElement(WebElement element) {

	        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
	                + "var elementTop = arguments[0].getBoundingClientRect().top;"
	                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

	        ((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);


	    }
	  public void jsScroll() throws Exception {
	        JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("window.scrollTo(0, 1100);");
	    }
	  public void jsScrollRiche() throws Exception {
	        JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("window.scrollTo(0, 400);");
	    }
	public void verifyStatoSottostatoRichiesta(String stato,String sottostato) throws Exception{
		String state= util.waitAndGetElement(status).getText();
		String subState= util.waitAndGetElement(sottostatus).getText();
		if(!stato.toLowerCase().trim().contentEquals(state.trim().toLowerCase())) throw new Exception("Lo stato della Richiesta risulta diverso da : "+stato+". Stato riscontrato: "+state);
		if(!sottostato.toLowerCase().trim().contentEquals(subState.trim().toLowerCase())) throw new Exception("Il sottostato della Richiesta risulta diverso da : "+sottostato+". Stato riscontrato: "+subState);

	}

    public String recuperaInfoDaIdRichiesta(String info, String id) throws Exception {

          System.out.println("//div[contains(@data-component-id,'ProcessExplorer')]//a[contains(text(),'"+id+"')]/ancestor::ul//span[text()='"+info+"']/../strong");

         List<WebElement> list = util.waitAndGetElements(By.xpath("//div[contains(@data-component-id,'ProcessExplorer')]//a[contains(text(),'"+id+"')]/ancestor::ul//span[text()='"+info+"']/../strong"));

         if(list.size()==0) 
         throw new Exception("Impossibile recuperare il valore di "+info+" a partire dall id richiesta "+id);

         return list.get(0).getText();
 
    }
    public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
    public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
}
