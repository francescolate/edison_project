package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OffertaComponent110 {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By popUpText = By.xpath("//p[@class='accept_new_line']");
	public By popUpTitle = By.xpath("//div[@class='modal_content']/h2[@class='modal_title']");
	public By proseguipopupbutton = By.xpath("//div[@class='jss1'][2]/div[@class='jss2']/div[@class='modal_content']/div[@class='modal_cta']/button[@class='exit_button']");
	public By valoreLucePlus = By.xpath("//div[@id='adesione_standard']/h1[@class='image-hero_title text--page-heading']");
	public By valoreLucePlusText = By.xpath("//div[@id='adesione_standard']/p[@class='image-hero_detail text--detail']");
	public By popUpText2 = By.xpath("//div[@class='remodal-content']/div[@class='modal-body']/p[@class='message']");
	public By proseguicust = By.xpath("//button[@id='customer-data-next']");
	public By checkboX = By.xpath("//input[@id='privacy-consent']");
	
	
	public OffertaComponent110(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
	 
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void jsClickComponent(By by) throws Exception{
			util.jsClickElement(by);
		}
		
		public void clickTab(By Object) throws Exception {
			driver.findElement(Object).sendKeys(Keys.TAB);;
		}
		
		public void verifyDefaultValue(By checkObject, String Value ) throws Exception
		{
			String textFound = "NO";
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String actualValue = driver.findElement(checkObject).getText();
			
			if (actualValue.contentEquals(Value))
				textFound="YES";
								
			if (textFound.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					
		}
		
		public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
	        
	        WebDriverWait wait = new WebDriverWait(this.driver, 50);
	            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
	        if(value.equals(""))throw new Exception("Field is not pre filled");
	        
	        String match = "FAIL";
	        if(value.contentEquals(expectedValue))
	        	match = "PASS";
	        if(match.contentEquals("FAIL"))
	        	throw new Exception("PrePopulatedValue and Expected values are not matching");
	    }
		
		public static final String POPUP_TEXT= "Gentile cliente, ti informiamo che l’operazione che hai selezionato non può essere effettuata su questa fornitura.L’operazione adatta alla tua fornitura è Voltura senza AccolloSe hai bisogno di supporto contatta il nostro servizio clienti in chat sul sito enel.it per tutte le informazioni di dettaglio.";
		public static final String POPUP_TITLE= "Cambio operazione";
		public static final String VALORE_LUCE_PLUS ="Valore Luce Plus";
		public static final String VALORE_LUCE_PLUS_TEXT ="Ricorda che per completare l'adesione devi avere a portata di mano: Codice fiscale / POD / Iban L'offerta che hai scelto è valida solo per forniture ad uso abitativo.";
		public static final String POPUP2_TEXT ="";

}
