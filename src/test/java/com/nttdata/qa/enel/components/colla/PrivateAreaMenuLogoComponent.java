package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaMenuLogoComponent {

	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;
	
	public By addressList = By.xpath("//div[@class='row']/div[@class='col-xs-12']/div[@class='form-group form-btn-right margin-bottom-15']/div/div[@class='customselect']/div[@class='customselect-container']/div[@class='customselect-drop']/ul[@class='customselect-results']/li/label/span");
	//38
	public By homePageLogo = By.xpath("//div[@id='openModalUserBtn']");
	public By modificaLink = By.xpath("//div[@id='modalUser']/div[@class='modal-dialog']/div[@class='modal-content']/div[@class='modal-body']/p");
	public By datiDiContatto = By.xpath("//a[@id='modificaProfiloLink']");
	public By ilTuoProfilo = By.xpath("//div[@id='modalUser']/div[@class='modal-dialog']/div[@class='modal-content']/div[@class='modal-body']/div[@class='row row-btns']/a[@class='btn btn-primary btn-bordered btn-fuchsia #cta_profilo']");
	public By popupText = By.xpath("//form[@id='dragprofile']/div[@class='dz-default dz-message']/span");
	//public By seleziona = By.xpath("//div[@class='upload-btn-wrapper']/input[@id='myFile']");
	public By seleziona = By.xpath("//button[text()='Seleziona']");
	public By selezionaNew =By.xpath("//div[@class='upload-btn-wrapper']/button[text()='Seleziona']");
	//39
	public By contactValues = By.xpath("//div[@class='row']/div/div/input");
	public By contactFeilds = By.xpath("//div[@class='row']/div/div/label");
	public By editContactPath = By.xpath("//div[@class='panel-body']/ol");
	public By editContactTitle = By.xpath("//div[@class='panel-body']/h1");
	public By editContactSubTitle = By.xpath("//div[@class='panel-body']/p");
	public By sectionTitle = By.xpath("//form[@class='formacb']/div/div[1]");
	public By contactDetailValues = By.xpath("//div[@class='row']/div/div[@class='form-group']/input");
	public By contactDetailsFeilds = By.xpath("//div[@class='row']/div/div[@class='form-group']/label");
	public By sectionDescription = By.xpath("//div[@class='row']/div[@class='col-xs-12'][2]/p[@class='lead']");
	public By buttonModifica = By.xpath("//div[@class='row-btns']/a[@class='btn btn-primary href-sf-prevent-default']");
	public By sectionTitle1 = By.xpath("//div[@class='row']/div/p[@class='leadbig margin-top-15 margin-bottom-30']");
	public By sectionTitle2 = By.xpath("//div[@class='row']/div[@class='col-xs-12']/p[2]");
	//40
	public By credenzialiDiAccessoHeading = By.xpath("//h3[@id='credenziali-accesso']");
	public By email = By.xpath("//div[@class='profile details-container']/dl/span/dt");
	public By emailValue = By.xpath("//div[@class='profile details-container']/dl/span/dd[@id='emailUsername']");
	public By customerDataFields = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div/label");
	public By customerDataValues = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div/input");
	public By infoIconCircleCellulare = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div[3]/span[@class='dsc-icon-info-circle']");
	public By popUpHeader = By.xpath("//div[@class='modal-dialog']/div[@class='modal-header']/h3/p");
	public By popUpTitle = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/p[a]");
	public By popUpclose = By.xpath("//div[@class='modal-header']/span[@class='dsc-icon-close']");
	public By anagraficaHeading = By.xpath("//form[@id='modificaAnagrafica-form']/div[@id='anagraficaAziendaTitle']");
	public By companyDataFields = By.xpath("//section[@id='profileSectionBusiness']//dl/span/dt");
	public By acceptTerm = By.xpath("//div[@id='generatedConsents']/div[1]/div[@class='uIdcheckbox-container']/div");
	public By acceptTerm2 = By.xpath("//div[@id='generatedConsents']/div[2]/div[@class='uIdcheckbox-container']/div");
	public By XAPTH_CHKBOX1 =By.xpath("//div[@id='generatedConsents']/div[1]/div[@class='uIdcheckbox-container']/div/input[@type='checkbox']");
	public By XPATH_CHKBOX2=By.xpath("//div[@id='generatedConsents']/div[2]/div[@class='uIdcheckbox-container']/div/input[@type='checkbox']");
	public By informativaPrivacyLink = By.xpath("//div[@class='informative-new'][1]//label[2]/button[@id='onlytext']");
	public By condizioniLink = By.xpath("//div[@class='informative-new'][2]//label[2]/button[@id='onlytext']");
	public By infoPopupHeading = By.xpath("//div[@class='row']/div/div[1]/div");
	public By infoPopupHeading1 = By.xpath("//div[@class='cmModalText cmText5']/h2[1]/b");
	public By infoPopupHeading2 = By.xpath("//div[@class='cmModalText cmText5']/h2[2]/b");
	public By infoPopupHeading3 = By.xpath("//div[@class='cmModalText cmText5']/h2[3]/b");
	//public By largerText = By.xpath("//div[@class='cmModalText cmText5']/h2[3]/b");
	
	public By infopriPopupHeading = By.xpath("//div[@id='cmModalInfoBoxes']//div[1]/div[@class='cmModalTitle cmTitle1']");
	public By largerText =By.xpath("//div[@class='cmModalTitle cmTitle1']/ancestor::div[@class='container']");
	public By infoPriPopupClose = By.xpath("//button[@class='btn-close pull-right']");
	public By condizionPopupHeading = By.xpath("//div[@id='cmModalInfoBoxes']//div[1]/div[@class='cmModalTitle cmTitle1']");
	public By largerTextCondizion = By.xpath("//div[@id='cmModalInfoBoxes']/div[@class='modal-dialog info-privacy']/div[@class='modal-content']/div[@class='modal-body']/div[@class='row']/div[@class='container']");
	public By condizionPopupClose = By.xpath("//button[@class='btn-close pull-right']");
	
	public By serviziMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Servizi']/parent::a");
	public By bolletteMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Bollette']/parent::a");
	public By bolletteHeader = By.xpath("//h1[text()='Le tue bollette']");
	public By bolletteHeaderText = By.xpath("//div[@class='heading']//p[contains(text(), 'Qui potrai')]");
	public By tilesHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public String orderedList = "//*[text()='$tileHeader$']/ancestor::section//li[contains(@class,'tile') and not(@style)]//h3/ancestor::a";
	public String tile = "//li[contains(@class,'tile')]//h3[text()='$tileName$']/ancestor::a";
	public By bollettaSintesi = By.xpath("//li[contains(@class,'tile')]//h3[text()='Bolletta di Sintesi o di Dettaglio']/ancestor::a");
	public By bollettaSintesiHeaderText = By.xpath("//div[@class='section-heading']");
	public By accountAddressLuce = By.xpath("//span[@class='icon-line-electricity']/../following-sibling::span[@class='indirizzo']/span[@class='valore']");
	public By customerNumberLuce = By.xpath("//span[@class='icon-line-electricity']/../following-sibling::span/span/span[@class='valore']");
	public String tipologiaBollettaButton = "//button/span[text()='$tipologiaBollettaButton$']";
	public By buttonEsci = By.xpath("//button[text()='ESCI']");
	public By buttonDettaglioFornitura = By.xpath("//span[@class='icon-line-flame']/ancestor::div[@class='details-container']//div[text()='Attiva']/ancestor::div[@class='card forniture regolare']//a[text()='Dettaglio fornitura']");
	public By serviziPerLeBolletteHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public By serviziPerLeBolletteHeaderText = By.xpath("//h2[text()='Servizi per le bollette']/ancestor::div[@class='section-heading']");
	public By accountAddressGas = By.xpath("//span[@class='icon-line-flame']/../following-sibling::span[@class='indirizzo']/span[@class='valore']");
	public By customerNumberGas = By.xpath("//span[@class='icon-line-flame']/../following-sibling::span/span/span[@class='valore']");
	
    public PrivateAreaMenuLogoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void clickComponentJs(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.jsClickElement(clickObject);
	}

	
	public void click(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyMenuItemsPresenceAndOrder(String menuItemsString, String tileHeader) throws Exception {
		util.verifyVisibility(By.xpath("//*[@id='spinner'])"), 10, true);
		populateMenuItemsVector(menuItemsString);
		By orderedListXPath = By.xpath(this.orderedList.replace("$tileHeader$", tileHeader));
		List<WebElement> orderedWebElements = util.waitAndGetElements(orderedListXPath);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
		if(orderedWebElements.size()!=menuItemsList.size())
			throw new Exception("actual size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
		for(int i=0;i<orderedWebElements.size();i++){
			if(!orderedWebElements.get(i).equals(menuItemsList.get(i)))
					throw new Exception("the item you're looking for is different from the one in the list at position "+i+".");
		}
	}
	
	private void populateMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(tile.replace("$tileName$", item))));
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		System.out.println(util.waitAndGetElement(object,true).getCssValue("color"));
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
		System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}	
	
	public void checkMenuPath(String path, By existingObject) throws Exception {
		WebElement obj = util.waitAndGetElement(existingObject);
		String pathDescription=obj.getText(); 
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	

		if (!pathDescription.equalsIgnoreCase(path))
			throw new Exception("object path with xpath " + path + " is not exist.");
	}
	
	public void verifyFeildAttribute(By existingObject, String text) throws Exception {
		String actualText = driver.findElement(By.xpath("//div[@class='customselect-search']/input")).getAttribute("placeholder");
		if (!actualText.equalsIgnoreCase(text))
			throw new Exception("object with text " + text + " is not exist.");
	}
	
	public void enterInputtoField(By checkObject, String property) throws Exception {			
				util.objectManager(checkObject, util.sendKeys, property);
	}
	
	public void clearInputFromField(By checkObject) throws Exception {
		driver.findElement(checkObject).clear();
		
}
			
	  	
	public void checkForLiftTypeDropDownValue(By checkObject) throws Exception{
		String[] exp = {"Via Lamarmora 31 20122 Milano Mi","Via Lamarmora Alfonso 31 20122 Milano Mi","mia fornitura preferita","778186188","780414414"};
		driver.findElement(By.xpath("//div[@class='customselect']")).click();
		List <WebElement> list  = driver.findElements(addressList);
			for (WebElement we : list) {
	    	System.out.println(we.getText());
	    	if(!Arrays.asList(exp).contains(we.getText()))
	    		throw new Exception ("The list of addresses are not available to the customer");
	       	    } 
	}
	
	public void verifyContactDetails(By checkObject, String[] Feild, String[] Value) throws Exception{
		
			List <WebElement> list  = driver.findElements(contactDetailsFeilds);
					
			HashMap<String, String> hm = new HashMap<String, String>();
			HashMap<String, String> hmActual = new HashMap<String, String>();
			String key, val ;
			
			for (int i=1; i<=list.size();i++)
			{
				key = driver.findElement(By.xpath("//div[@class='row']/div[@class='col-xxs-12 col-xs-6']["+ i +"]/div[@class='form-group']/label")).getText();
				val = driver.findElement(By.xpath("//div[@class='row']/div[@class='col-xxs-12 col-xs-6']["+ i +"]/div[@class='form-group']/input")).getAttribute("value");
				hm.put(key, val);
				}
			for (int i=0; i<Feild.length;i++)
			{	
				hmActual.put(Feild[i], Value[i]);
			}
			if(!hm.equals(hmActual))
	    		throw new Exception ("The actual contact details "+hmActual.toString()+" are not match with "+hm);
			}
	
	public void verifyEmailData(By checkObjectFeild,String xpath, String Feild, String Value) throws Exception{
		 WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        
      	HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmExpctd = new HashMap<String, String>();
		hmExpctd.put(Feild, Value);
		
			String key, expValue ;
			key = driver.findElement(checkObjectFeild).getText();
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        expValue = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		    //expValue = driver.findElement(By.xpath(xpath)).getAttribute("value");
			hm.put(key, expValue);
		
		if(!hm.equals(hmExpctd))
   		throw new Exception ("The actual contact details "+hm+" are not match with "+hmExpctd);
		}	
	
	public void verifyCustomerData(By checkObjectFeild,String xpath, String Feild[], String Value[]) throws Exception{
		 WebDriverWait wait = new WebDriverWait(this.driver, 50);
         wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
         
        List <WebElement> list  = driver.findElements(checkObjectFeild);
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmExpctd = new HashMap<String, String>();
		
		for (int i=0; i<Feild.length;i++){			
		hmExpctd.put(Feild[i], Value[i]);}
		
		for (int i=1; i<=list.size();i++)
		{
		String key, expValue ;
			key = driver.findElement(By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div["+ i +"]/label")).getText();
			xpath = "//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div["+ i +"]/input";
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        expValue = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			hm.put(key, expValue);
		}	
		if(!hm.equals(hmExpctd))
    		throw new Exception ("The contact details are not correctly displayed");
		}	
	
	public void verifyCompanyData(By checkObjectFeild,String xpath, String Feild[], String Value[]) throws Exception{
		 WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        
       List <WebElement> list  = driver.findElements(checkObjectFeild);
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmExpctd = new HashMap<String, String>();
		
		for (int i=0; i<Feild.length;i++){			
		hmExpctd.put(Feild[i], Value[i]);}
		
		for (int i=1; i<=list.size();i++)
		{
		String key, expValue ;
			key = driver.findElement(By.xpath("//section[@id='profileSectionBusiness']//dl/span["+ i +"]/dt")).getText();
			xpath = driver.findElement(By.xpath("//section[@id='profileSectionBusiness']//dl/span["+ i +"]/dd")).getText();
			//JavascriptExecutor js = (JavascriptExecutor) driver;
	        //expValue = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			hm.put(key, xpath);
		}	
		if(!hm.equals(hmExpctd))
   		throw new Exception ("The contact details are not correctly displayed");
		}
	public void verifyCheckBoxIsChecked(By checkObject, String text, By Xpath ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		WebElement chkbxtxt = driver.findElement(checkObject);
		WebElement chkbox = driver.findElement(Xpath);
		String description = chkbxtxt.getText();
		if(!(chkbox.isSelected())&& description.contentEquals(text))
		throw new Exception ("The check box is not checked and the correct text is not displayed");
		
	}
	
	public void clickOnTheInputFeild(By checkObject) throws Exception{
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		driver.findElement(By.xpath("//div[@class='customselect-search']/input")).click();
	}
	
  public void verifySupplyDisplay(By checkObject) throws Exception {
  WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		WebElement mytable = driver.findElement(By.xpath("//table[@class='table']"));
		List < WebElement > rows_table = mytable.findElements(By.xpath("//table[@class='table']/tbody"));
		int rows_count = rows_table.size();
									
		for (int row = 0; row < rows_count; row++)
			{
				String description = rows_table.get(row).getText();
				if(!description.contentEquals("Gas ATTIVO 778186188\nVia Lamarmora 31 20122 Milano Mi\nVEDI BOLLETTE"))
				throw new Exception("The supply details displayed are incorrect");
			}
		 }
  
  public void UploadDocumentsComponent(By checkObject) throws Exception {
	  WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		WebElement upload = driver.findElement(By.xpath("//div[@class='upload-btn-wrapper']/input[@id='myFile']"));
		upload.sendKeys("C:/Users/Public/Pictures/Image.JPG");
		
	}
    
	public static final String SUPPLY_HEADER = "Gas";
	public static final String SUPPLY_HEADER_LUCE = "Luce";
	public static final String BOLLETTEPATH = "AREA RISERVATA BOLLETTE";
	public static final String ERROR_TEXT_MESSAGE = "Per effettuare una ricerca ricordati di inserire il tuo numero cliente o un indirizzo di fornitura.";
	public static final String POPUP_TEXT = "Seleziona o trascina qui i file dal tuo computer tramite le cartelle del tuo pc";
	public static final String text = "Inserisci N° cliente/indirizzo di fornitura";
	public static final String BILL_TITLE = "Se cerchi una bolletta in particolare usa i filtri per trovarla più facilmente.";
	public static final String BILL_SUBTITLE = "Ci sono bollette scadute. Pagale comodamente on line. Se richiesto, in seguito puoi inviare il dimostrato pagamento cliccando su\ninvia documenti\n.";
	public static final String[] expCustValue = {"IDEAL COLOR SNC","03430350177","0818890123","fabiana.manzo@nttdata.com","idealcolorsnc@comunicapec.it"};
	public static final String[] expCustFeild = {"Titolare","Codice fiscale","Telefono","Email","Email PEC"};
	public static final String EDIT_CONTACT_PATH = "AREA RISERVATA MODIFICA DATI";
	public static final String EDIT_CONTACT_TITLE ="Modifica dati di contatto";
	public static final String EDIT_CONTACT_SUBTITLE ="In questa sezione sono riportati i dati di contatto che ci hai comunicato. Se desideri modificarli, integrarli o sostituirli, inserisci le nuove informazioni nei campi corrispondenti.";
	public static final String SECTION_TITLE = "Aggiorna i tuoi dati";
	public static final String SECTION_DESCRIPTION = "Di seguito sono riportati i tuoi dati di contatto. Per aggiornarli clicca su \"modifica\".";
	public static final String SECTION_TITLE1 = "Comunica il tuo consenso";
	public static final String SECtiON_TITLE2 = "Vuoi essere sempre informato sulle nuove offerte di Enel Energia e dei suoi partner? Lascia il tuo consenso per scoprire tutti i vantaggi a te riservati!";
	public static final String ACCESSO_HEADING ="Credenziali di accesso";
	public static final String EMAIL = "Indirizzo e-mail";
	public static final String EMAIL_VALUE = "immilezell-1212@yopmail.com";
	public static final String[] expCustDataFeild = {"Nome*","Cognome*","Cellulare*","Codice Fiscale*"};
	public static final String[] expCustDataValue ={"test","collaudo","3394956806","03430350177"};
	public static final String POPUP_HEADING = "Modifica numero di cellulare";
	public static final String POPUP_TITLE = "Potrai modificare in ogni momento il numero di cellulare inserito seguendo le indicazioni riportate qui";
	public String XPATH_EMAIL_VALUE = "//div[@class='row'][1]/div[@class='form-group col-xs-12 col-sm-12']/input[@id='email']";
	public String XPATH_CUSTOMER_DATA_VALUES = "//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div/input";
	public static final String ANAGRAFICA_HEADING = "Anagrafica aziendaAnagrafica azienda";
	public static final String[] anagraficaExpCustDataField = {"Denominazione","Partita IVA","Codice fiscale"};
	public static final String[] anagraficaExpCustDataValue ={"IDEAL COLOR SNC","03430350177","03430350177"};
	public String XPATH_COMPANY_DATA_VALUES = "//section[@id='profileSectionBusiness']//dl/span/dd";
	public static final String ACCEPT_TERMS_TEXT= "Ho letto e accettato: Informativa privacy *";
	public static final String ACCEPT_TERM_TEXT2 ="Ho letto e accettato: Condizioni generali dei servizi offerti *";
	public static final String INFOPRIV_HEADING = "Informativa privacy";
	public static final String INFOPRIV_HEADING1 = "Portale ENEL ENERGIA";
	public static final String INFOPRIV_HEADING2 = "INFORMATIVA PRIVACY";
	public static final String INFOPRIV_HEADING3 = "AI SENSI DELL’ART. 13 DEL REGOLAMENTO UE 2016/679 (\"GDPR\")";
	public static final String LARGER_TEXT = "";
	
	
	public static final String INFOPRIV_POPUP_HEADING = "Informativa privacy";
	//public static final String INFOPRIV_POPUP_LARGER_TEXT = "Portale ENEL ENERGIAINFORMATIVA PRIVACY AI SENSI DELL’ART. 13 DEL REGOLAMENTO UE 2016/679 (\"GDPR\") TITOLARE E RESPONSABILE DEL TRATTAMENTO DEI DATI PERSONALI Enel Energia S.p.A., con sede legale in viale Regina Margherita n. 125, 00198, Roma, P. IVA 06655971007 (di seguito \"Enel Energia\" o \"Titolare\"), in qualità di Titolare del trattamento, tratterà i suoi dati personali forniti tramite il sito www.enel.it (di seguito \"Sito\") in conformità a quanto stabilito dalla normativa applicabile in materia di protezione dei dati personali e dalla presente Informativa. RESPONSABILE DELLA PROTEZIONE DEI DATI PERSONALI (RPD) Enel Energia ha nominato un Responsabile della Protezione dei Dati personali (RPD) che può essere contattato al seguente indirizzo e-mail: dpo.enelenergia@enel.com. OGGETTO E MODALITÀ DEL TRATTAMENTO Enel Energia tratterà i dati personali da lei comunicati o legittimamente raccolti (\"Dati Personali\"). In particolare, possono essere oggetto di trattamento i seguenti Dati Personali: Dati Identificativi: dati che permettono l‘identificazione diretta, come i dati anagrafici (quali ad esempio nome, cognome, codice fiscale, Partita IVA, indirizzo, ecc.), conferiti al Titolare e trattati ai fini della sottoscrizione e della gestione del contratto. Dati di consumo: dati relativi alla fornitura e ai livelli di consumo registrati, raccolti ed elaborati nel corso della durata del contratto. Dati Economici e Finanziari: dati necessari per i pagamenti (es. IBAN) o che provano l’esecuzione dei pagamenti (estremi identificativi dei pagamenti) e ogni altro dato relativo alla solvibilità e puntualità del cliente;Dati di contatto: informazioni di contatto quali ad esempio numeri di telefono, fisso e/mobile, indirizzo email, forniti al Titolare in fase di sottoscrizione o nel corso della durata del contratto o acquisiti dal Titolare stesso, che consentono di contattarla ai fini della gestione dei rapporto contrattuale e/o per fornirle servizi adeguati alle Sue esigenze.Dati dell’area riservata: dati richiesti in fase di registrazione, ove effettuata, e necessari per la fruizione dei relativi servizi offerti dalle società del Gruppo Enel accessibili da web tramite il Profilo Unico Enel. Al momento della creazione del Profilo Unico Enel, le sarà richiesto di fornire alcuni Dati Personali quali: nome, cognome, indirizzo email, numero di cellulare, codice fiscale e codice utenza. Enel Energia le consente inoltre di accedere all’area riservata anche tramite l’APP o mediante l’utilizzo di credenziali fornite da un social network (c.d. “social login”), in particolare utilizzando le identità create su Facebook e Google+;Dati di navigazione: i sistemi informatici e telematici e le procedure software preposte al funzionamento del Sito, ovvero dedicati al funzionamento e utilizzo delle APP rese disponibili da Enel Energia, acquisiscono, nel corso del loro normale esercizio, alcuni dati (es. la data e l’ora dell’accesso, le pagine visitate, il nome dell’Internet Service Provider e l’indirizzo del Protocollo Internet (IP) attraverso il quale Lei accede a Internet, l’indirizzo Internet dal quale lei si è collegato al Sito, ecc.), la cui trasmissione è implicita nell’uso dei protocolli di comunicazione web o è utile per la migliore gestione e ottimizzazione del sistema di invio di dati ed e-mail; Dati acquisiti dal Servizio Clienti: dati forniti in occasione di interazioni con il Servizio Clienti di Enel Energia, ivi incluse le registrazioni delle telefonate, qualora lei abbia acconsentito alla loro registrazione ovvero nei casi in cui la registrazione venga realizzata in forza di obbligo di legge o regolamento. Per trattamento di Dati Personali ai fini della presente informativa è da intendersi qualsiasi operazione o insieme di operazioni, compiute con l’ausilio di processi automatizzati e applicate ai Dati Personali, come la raccolta, la registrazione, l’organizzazione, la strutturazione, la conservazione, l’adattamento o la modifica, l’estrazione, la consultazione, l’uso, la comunicazione mediante trasmissione, diffusione o qualsiasi altra forma di messa a disposizione, il raffronto o l’interconnessione, la limitazione, la cancellazione o la distruzione. La informiamo che tali Dati Personali verranno trattati manualmente e/o con il supporto di mezzi informatici o telematici. FINALITÀ E BASE GIURIDICA DEL TRATTAMENTOEnel Energia tratterà i suoi Dati Personali per il conseguimento di finalità precise delle quali sarà di volta in volta informato e solo in presenza di una specifica base giuridica prevista dalla legge applicabile in materia di protezione dei dati personali. Nello specifico, Enel Energia tratterà i suoi Dati Personali solo quando ricorra una o più delle seguenti basi giuridiche:Lei ha prestato il suo consenso libero, specifico, informato, inequivocabile ed espresso al trattamento; Il trattamento è necessario all’esecuzione di un contratto di cui lei è parte od all’esecuzione di misure precontrattuali adottate su sua richiesta;In presenza di un legittimo interesse di Enel Energia a condizione che non prevalgano gli interessi o i diritti e le libertà fondamentali dell’interessato che richiedono la protezione dei dati personali; In forza di un obbligo di legge. Nella seguente tabella sono elencate le finalità per cui i suoi Dati Personali sono trattati da Enel Energia e la base giuridica su cui si basa il trattamento. Finalità del trattamento Base giuridica Permettere l’utilizzo di tutte le funzionalità del Sito.Esecuzione di un contratto Controllare il corretto funzionamento del Sito. Esecuzione di un contratto Consentire la registrazione e/o l’accesso all’area riservata tramite il profilo unicoEsecuzione di misure precontrattuali adottate su richiesta dell’interessato Esecuzione di un contratto Accertare le responsabilità in caso di reati informatici ai danni del Sito. Legittimo interesse Dare riscontro a un quesito dell’interessato. Esecuzione di misure precontrattuali adottate su richiesta dell’interessato Esecuzione di un contratto Consentire l’utilizzo delle APP rese disponibili da Enel Energia e fruizione dei relativi servizi. Esecuzione di misure precontrattuali adottate su richiesta dell’interessato Esecuzione di un contratto Dare esecuzione ai contratti sottoscritti con Enel Energia (es. aventi ad oggetto la fornitura di energia elettrica, gas naturale, applicazioni per la rilevazione del consumo dell’energia, ecc.) e di tutte le conseguenti operazioni di interesse del cliente. Esecuzione di un contratto Consentire l’accesso alle funzionalità del \"Salva e Continua\" disponibile sul sito web di Enel Energia in occasione della conclusione di contratti via web. Nell’eventualità lei non porti a termine la procedura di adesione al contratto con Enel Energia o la procedura di registrazione al sito, i suoi dati saranno utilizzati per contattarla al fine di agevolare la conclusione del processo. Enel Energia potrà accedere al formulario di adesione in cui sono presenti i dati da lei già inseriti nella prima fase di compilazione, proseguire nella compilazione mediante contatto telefonico e consentirle di finalizzare il processo di adesione mediante accesso a un link, che Enel Energia invierà alla sua casella di posta elettronica. In alternativa l’accesso alla compilazione potrà essere realizzato attraverso l’invio di una \"One Time Password\" (cd. \"OTP\") al numero di cellulare indicato in fase di adesione. Decorso il termine di cinque (5) giorni solari dall’invio dell’e-mail, il link non sarà più utilizzabile, con conseguente cancellazione dei dati. Esecuzione di misure precontrattuali adottate su richiesta dell’interessato Gestire gli eventuali ordini di acquisto e delle connesse attività amministrative. Esecuzione di misure precontrattuali adottate su richiesta dell’interessato Esecuzione di un contratto Nell’eventualità lei abbia richiesto la domiciliazione bancaria o postale dei pagamenti, per verificare che i dati bancari/postali forniti siano corretti. A tale fine Enel Energia potrà contattare la Banca /Poste Italiane oppure potrà contattarla direttamente telefonicamente o tramite i canali digitali da lei resi disponibili a tal fine. Esecuzione di misure precontrattuali adottate su richiesta dell’interessato Esecuzione di un contratto Consentire i controlli di sicurezza richiesti dalla legge. Obbligo di legge Legittimo interesse Permettere a Enel Energia di adempiere agli obblighi di legge, regolamento o previsti dalla normativa comunitaria. Obbligo di legge Predisporre misure per la tutela delle ragioni di credito e per le attività di prevenzione contro le frodi connesse ai prodotti eservizi prestati e per valutare l’affidabilità economica e/o solvibilità del cliente, anteriormente o nel corso del rapporto contrattuale. A tale scopo potrebbero essere acquisite informazioni attraverso l’accesso ai sistemi informativi del Titolare, a fonti pubbliche e/o a banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Legittimo interesse Cessione dei diritti di credito verso i clienti, derivanti dalla fornitura dei prodotti e servizi forniti dal Titolare; Esecuzione di un contratto Rilevare, prevenire, mitigare e accertare attività fraudolente o illegali in relazione ai servizi forniti sul Sito. Legittimo interesse Obbligo di legge Svolgere attività mirate al miglioramento della qualità dei servizi offerti collegati al rapporto contrattuale (per esempio, mediante sondaggi per valutare il gradimento dei clienti).Legittimo interesse Obbligo di legge Consentire ad Enel Energia lo svolgimento di ricerche di mercato, vendite dirette, anche telefoniche, il collocamento di prodotti o servizi, comunicazioni commerciali o attività di marketing e l’invio di materiale pubblicitario. Tali attività potranno essere eseguite mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail).Consenso dell’interessato Utilizzare l’indirizzo di posta elettronica fornito dall’inte...";
	public static final String CONDIZION_POPUP_HEADING = "Condizioni generali dei servizi offerti";
	//public static final String CONDIZION_POPUP_LARGER_TEXT = "";
	
	
	
	public static final String BOLLETTE_HEADER = "Le tue bollette";
	public static final String BOLLETTE_HEADER_TEXT = "Qui potrai visualizzare bollette e/o rate delle tue forniture.";
	public static final String SERVIZI_BOLLETTE = "Bolletta Web;Pagamento Online;Rettifica Bolletta;Rateizzazione;Bolletta di Sintesi o di Dettaglio;Invio Attestazione di Pagamento";
	public static final String TILE_HEADER = "Servizi per le bollette";
	public static final String BOLLETTA_SINTESI_HEADER_TEXT = "Modifica Tipologia Bolletta Scegli se ricevere la Bolletta di Sintesi dove troverai le principali voci di spesa ed i consumi oppure la Bolletta di Dettaglio dove sarà possibile approfondire le singole voci elementari di spesa. La modifica riguarderà solo la ricezione della bolletta via mail oppure cartacea se non hai attiva la bolletta web, mentre scaricando il PDF visualizzerai comunque la versione completa della tua bolletta, comprensiva di sintesi e dettaglio.";
	
	
	     
}

