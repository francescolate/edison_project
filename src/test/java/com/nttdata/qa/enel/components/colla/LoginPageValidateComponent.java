package com.nttdata.qa.enel.components.colla;

import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LoginPageValidateComponent extends BaseComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By logoEnel = By.xpath("//img[contains(@class,'logoimg')]");
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By buttonAcceptCookie= By.xpath("//button[@id='truste-consent-button']");
    public By loginSuccessful= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
    public By loginBSNSuccessful= By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
   // public By buttonPopupLogin=By.xpath("//button[@class='remodal-close popupLogin-close']");
    public By logout=By.id("disconnetti");
    public By accountButton=By.xpath("//button[@id='user']");
    public By esciLink=By.xpath("//a[@id='tr_disconnetti']");
    public By recoverPasswordBtn = By.cssSelector("#formlogin > div.login_recovers > div > span:nth-child(1) > a");
    public By recoverUsernameBtn = By.cssSelector("#formlogin > div.login_recovers > div > span:nth-child(2) > a");
    public By accessProblemsLabel = By.xpath("//*[@id='formlogin']/div[contains(@class,'help-message')]/p[1]");
    public By accessProblemsBtn = By.xpath("//*[@id='formlogin']/div[contains(@class,'help-message')]//a");
    public By googleAccessBtn = By.cssSelector("#formlogin > div.login-social-btn.border-top-bottom-gray > div:nth-child(1) > button");
    public By facebookAccessBtn = By.cssSelector("#formlogin > div.login-social-btn.border-top-bottom-gray > div:nth-child(2) > button");
    public By noAccountLabel = By.xpath("//p[contains(text(),'Non hai ancora creato il tuo Profilo Unico?')]");
    public By registerBtn = By.xpath("//a[@href='/it/registrazione']");
    //public By accessProblemsLink = By.xpath("//a[text()='Problemi di accesso?']");
    public By accessProblemsLink = By.xpath("//p[text()='Se hai problemi di accesso ']");
    public By obligatoryUsernameLabel = By.xpath("//p[text()='Username obbligatoria']");
    public By obligatoryPasswordLabel = By.xpath("//p[text()='Password obbligatoria']");
    public By areaClientiCasa = By.xpath("//*[contains(text(), 'Area Clienti Casa')]/following-sibling::*//a[text()='Entra']");
    public By areaClientiImpresa = By.xpath("//*[contains(text(), 'Area Clienti Impresa')]/following-sibling::*//a[text()='Entra']");
    public By areaClientiCondizioni = By.xpath("//label[contains(@data-title, 'Condizioni')]/preceding-sibling::label[@class='uIdLabel custom-label']");
    public By areaClientiPrivacy = By.xpath("//label[contains(@data-title, 'Informativa')]/preceding-sibling::label[@class='uIdLabel custom-label']");
    public By terminiECondizioniButton = By.xpath("//button[text()='Continua']");
    public By loginPopcloseicon=By.xpath("//button[@class='remodal-close popupLogin-close']");
    public By loginPopupText = By.xpath("//div[@id='imgUnica']/p");
    //public By accediAMyEnelLabel=By.xpath("//div[@class='login-details']//h1[@aria-label='accedi a myenel']");
    
    public By entr_LIKE_RES = By.xpath("//div[@class='item item01']/div[2]//li[2]//a");
    public By accediAMyEnelLabel=By.xpath("//div[@class='login-details']//h1[@aria-label='accedi a myenel']");
	public By resEntra = By.xpath("//li[@class='col-sm-6 margin-bottom-30'][1]/div[@class='panel-group']//div[@class='panel-heading text-center bsnBlue']/p[@class='margin-top-60']/a[@class='acb btn btn-primary btn-white']");		
	public By homeFullscreenAlert = By.xpath("//div[@id='home-fullscreen-alert']");
	public By homeFullscreenAlertCloseButton = By.xpath("//div[@id='fsa-close-button']");
	public By testoCentrale=By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
    public By testoSezioneForniture=By.xpath("//section[@id='lista-forniture']//p[text()='Visualizza le informazioni principali delle tue forniture e consulta le tue bollette']");
    
    
    public By usernameValidate = By.xpath("//input[@id='txtLoginUsername']/following-sibling::p[contains(text(),'Username obbligatoria')]");
    public By passwordValidate = By.xpath("//input[@id='txtLoginPassword']/following-sibling::p[contains(text(),'Password obbligatoria')]");
    public By googleButton = By.xpath("//div[@class='login-social-btn border-top-bottom-gray']/ancestor::div[@class='login-details']//button[contains(text(),'Accedi con Google')]");
    public By facebookButton = By.xpath("//div[@class='login-social-btn border-top-bottom-gray']/ancestor::div[@class='login-details']//button[contains(text(),'Accedi con Facebook')]");
    public By usernameInvalid = By.xpath("//input[@id='txtLoginUsername']/following-sibling::p[contains(text(),'Username non valida')]");
    public By passwordInvalid = By.xpath("//button[@id='login-btn']/parent::div[@class='login-btn login-btn-login']/following::p[@class='login-error-msg']");
    public By loginError = By.xpath("//div[@id='loginError']");
    public By header1 = By.xpath("//div[@id='mainContentWrapper']//h1[contains(text(),'Benvenuto nella tua area privata')]");
    public By header2 = By.xpath("//p[contains(text(),'In questa sezione potrai gestire le tue forniture')]");
    public By title1 = By.xpath("//h2[contains(text(),'Le tue forniture')]");
    public By title2 = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue fo')]");
    
    public By logoutLink = By.xpath("//span[@id='welcomeBox']//following::span[@class='dsc-icon-chevron-down']");
    public By esci = By.xpath("//a[@id='tr_disconnetti']/child::span[@class='linktext'][contains(text(),'Esci')]");
   //54
    public By loginProblem = By.xpath("//*[@id='formlogin']/div[contains(@class,'help-message')]");
    public By rucuperaPassword = By.xpath("//div[@class='login_recovers']/descendant::a[text()='Recupera Password']");
    public By recuperaUsername = By.xpath("//div[@class='login_recovers']/descendant::a[text()='Recupera Username']");
    public By recuperaHeader = By.xpath("//div[@id='recovery-password']/descendant::h1[contains(text(),'Recupera')]");
    public By recuperaHeader1 = By.xpath("//div[@class='cmPageTitleCont']/following-sibling::div[@class='cmPageSubtitleCont media']//p[contains(text(),'Inserisci')]");
    public By emaiLabel = By.xpath("//input[@id='rec-email']/preceding-sibling::label[contains(text(),'Email')]");
    public By emailInput = By.xpath("//div[@id='formRecoverEmail']/descendant::input[@id='rec-email']");
    public By inviaBtn = By.xpath("//div[@id='formRecoverEmail']/descendant::a[text()='Invia']");
    public By emailInputError = By.xpath("//div[@id='formRecoverEmail']/descendant::span[text()='Campo obbligatorio']");
    public By passwordChangeEmail = By.xpath("//div[@id='recovery-password']/descendant::h1[contains(text(),'Grazie! Ti abbiamo')]");
    public By emailAddress = By.xpath("//div[@id='recovery-password']/descendant::p[contains(text(),'.com')]");
    public By PasswordChangeContent = By.xpath("//div[@id='recovery-password']/descendant::p[contains(text(),'Segui le indicazioni')]");
    public By gmailUserField = By.xpath("//input[@id='identifierId']");
    public By userNextButton = By.xpath("//div[@id='identifierNext']/descendant::span[contains(text(),'Next')]");
    public By gmailPasswordField = By.xpath("//input[@name='password']");
    public By passwordNextButton = By.xpath("//div[@id='passwordNext']/descendant::span[contains(text(),'Next')]");
    public By gmailSignIn = By.xpath("//ul[@class='h-c-header__cta-list header__nav--ltr']/child::li/a[contains(text(),'Sign in')]");
    public By continuaButton = By.xpath("//a[@id='rec-email-button' and text()='Continua']");
    public By nuovaPasswordInput = By.xpath("//input[@id='rec-password']");
    public By nuovaConfermaPassword = By.xpath("//input[@id='rec-password-new']");
    public By cofermaButton = By.xpath("//a[@id='rec-password-button' and text()='CONFERMA']");
    public By otpInput = By.xpath("//input[@id='otp-step3Email']");
    public By otpcontinuaButton = By.xpath("(//button[contains(text(),'INVIA NUOVO CODICE')]/parent::div/child::button[@id='confirm-PINotp'])[1]");
    public By accediButton = By.xpath("//a[contains(text(),'ACCEDI') and @class='btn btn-white btn-auto']");
    
    public LoginPageValidateComponent(WebDriver driver) throws Exception {
		super(driver);
	}

    public void launchLink(String url) throws Exception {
    	System.out.println(Costanti.WP_BasicAuth_Password);
		
		if(!Costanti.WP_BasicAuth_Username.equals("")){
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		}
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}

	
	public void jsClickComponent(By clickableObject) throws Exception {
		util.jsClickElement(clickableObject);
	}


	public void SwitchTabToCustomerArea() throws Exception
	{
		
	   
		Set <String> windows = driver.getWindowHandles();
		String currentHandle = driver.getWindowHandle();
		System.out.println(currentHandle);
		
		for (String winHandle : windows) {
			System.out.println(winHandle);
			System.out.println("switched to "+driver.getTitle()+"  Window");
			System.out.println(driver.getCurrentUrl());
			
	        String pagetitle = driver.getTitle();
	        driver.switchTo().window(winHandle);
	        System.out.println(winHandle);
	        System.out.println("After switch"+ driver.getCurrentUrl());
	        System.out.println(driver.getTitle());
		    
		 }
	}
	
	   
		
	public void SwitchToWindow() throws Exception
	{
		Set<String> windows = driver.getWindowHandles();
		String currentHandle = driver.getWindowHandle();
		
		for (String winHandle : windows) {
		/*	System.out.println(winHandle);
			System.out.println("switched to "+driver.getTitle()+"  Window");
			System.out.println(driver.getCurrentUrl());
		*/	
	       // String pagetitle = driver.getTitle();
	        		driver.switchTo().window(winHandle);
	   /*     System.out.println(winHandle);
	        System.out.println("After switch"+ driver.getCurrentUrl());
	        System.out.println(driver.getTitle());
		*/    
		 }
	}
//	public void verifyComponentVisibility(By visibleObject) throws Exception {
//		if (!util.verifyVisibility(visibleObject, 15)) {
//			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
//		}
//	}
	
//	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
//		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
//		if (!util.checkElementText(objectWithText, textToCheck)) {
//			WebElement problemElement = driver.findElement(objectWithText);
//			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
//			if (elementText.length() < 1) {
//				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
//			}
//			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
//			//System.out.println(message);
//			throw new Exception(message);
//		}
//	}
	public void verifyContent(By object,String text) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
		String value = driver.findElement(object).getText();
		value = value.replaceAll("(\r\n|\n,)","");
		if(!text.contentEquals(value))
			throw new Exception("Value is not matching with expected");
		
	}


		public void waitForElementToDisplay (By checkObject) throws Exception{
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
		public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}
			
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void verifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		String textfield_found="NO";
		WebElement element = driver.findElement(checkObject);
		String actualtext = element.getText();
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void backBrowser(By checkObject) throws Exception {
	    try {
	    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    	driver.navigate().back();
	    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                
	    } catch (Exception e) {
	        throw new Exception("Previous Page not displayed");
	    }
	}

	public  void verifyLandingPage(WebDriver driver, String pageUrl, String pageTitle) throws Exception {
		
		String title = driver.getTitle();
		String url = driver.getCurrentUrl();
		System.out.println("Title: " + title + " vs requested: " + pageTitle);
		System.out.println("Url: " + url + " vs requested: " + pageUrl);
		if (!title.equals(pageTitle) || !url.equals(pageUrl)) {
			throw new Exception("Clicking on the access problems button did not lead to the selfcare FAQ page");
		}
	}
	public final static String INVALID_PASSWORD = "Username o password errata. Ricordati di inserire il prefisso se accedi con il numero di cellulare oppure, se non l'hai già fatto, di accedere con l'e-mail e completare il profilo inserendo il cellulare";
	public final static String INVALID = "Siamo spiacenti:Le credenziali che hai fornito non sono corrette.";
	public final static String USER_VALIDATE = "Username obbligatoria";
	public final static String PASSWD_VALIDATE = "Password obbligatoria";
	public final static String HOMEPAGE_HEADER1 = "Benvenuto nella tua area privata";
	public final static String HOMEPAGE_HEADER2 = "In questa sezione potrai gestire le tue forniture";
	public final static String HOMEPAGE_TITLE1 = "Le tue forniture";
	public final static String HOMEPAGE_TITLE2  = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public final static String LOGIN_PROBLEM = "Se non riesci adaccedereoppure vuoi ricevere informazioni sul Profilo Unicoclicca qui";
	public final static String RUCUPERAPASSWORD_HEADER = "Recupera password";
	public final static String RUCUPERAPASSWORD_HEADER1 = "Inserisci lausernameche hai usato per registrarti a Enel Energia per procedere con l'impostazione di una nuova password.";
	public final static String RUCUPERAPASSWORD1_HEADER1 = "Inserisci lausernameche hai usato per registrarti a Enel Energia per procedere con l'impostazione di una nuova password.";
	public final static String RUCUPERAPASSWORD2_HEADER1 = "Inserisci la username che hai usato per registrarti a Enel Energia per procedere con l'impostazione di una nuova password.";

	public final static String EMAIL_LABEL = "Email";
	public final static String PASSWORD_EMAIL = "Grazie! Ti abbiamo inviato una mail all'indirizzo";
	public final static String PASSWORDEMAIL_CONTENT = "Segui le indicazioni contenute nella mail per completare la tua richiesta! Non hai ricevuto la mail? Verifica che l'indirizzo sia corretto oppure controlla nella cartella Spam.";

}

