package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BolletteComponent {

	WebDriver driver;
	SeleniumUtilities util;
	public By pageTitle = By.xpath("//div[@class='heading']/h1");
	public By services = By.xpath("//a[@id='servizi']//span[text()='Servizi']");
	public By supplies = By.xpath("//a[@id='idFornitura']");
	public By enelPremiaWOW = By.xpath("//a[@id='enelPremia']/span[2]");
	public By bills = By.xpath("//a[@id='bollette']");
	public By news = By.xpath("//a[@id='novitaSelfcare']");
	public By videoSpace = By.xpath("//a[@id='tutorialSelfcare']");
	public By yourRights = By.xpath("//a[@id='iTuoiDiritti']");
	public By account = By.xpath("//a[@id='accountSelfcare']");
	public By support = By.xpath("//a[@id='supporto']");
	public By findEnelSpace = By.xpath("//a[@id='negozioEnel']");
	public By exit = By.xpath("//a[@id='disconnetti']");
	public By listofBills = By.xpath("//ul[@id='fornitura_0']/li/dl/div");
	public By listOfBillsGas = By.xpath("//ul[@id='fornitura_1']/li/dl/div");
	public By oneOfBillList = By.xpath("//ul[@id='fornitura_0']/li[1]/dl/div");
	public By dettaglioBolletta = By.xpath("//ul[@id='fornitura_0']/li[1]//li[1]/a");
	public By sacricaPDF = By.xpath("//ul[@id='fornitura_0']/li[1]//li[2]/a");
	public By luceDettaglioFornitura = By.xpath("//ul[@id='fornitura_1']/li[4]/dl/ul[@class='azioni-pagamento show-azioni']/li[1]/a");
	public By luceSacricaPDF = By.xpath("//ul[@id='fornitura_1']/li[4]/dl/ul[@class='azioni-pagamento show-azioni']/li[2]");
	public By more = By.xpath("//div[@id='bill-list-panel-0']//ul[@id='fornitura_0']//li[1]//dl//button[@class='btnAppearanceRemover open-menu-azioni bolletteModel']");
	public By plusInGas = By.xpath("//div[2]/div/button[@id='bill-list-button-1']");
	//public By plusInLuce = By.xpath("//div[2]/div/button[@id='bill-list-button-1']");
	public By plusInLuce = By.xpath("//button[@id='bill-list-button-1']");
	public By moreGas = By.xpath("//div[@id='bill-list-panel-1']/ul[@id='fornitura_1']/li[1]/dl/button");
	public By pagaOnlineGas = By.xpath("//ul[@id='fornitura_1']/li[1]/dl/ul/li[1]/a");
	public By dettaglioBollettaGas = By.xpath("//ul[@id='fornitura_1']/li[1]/dl/ul/li[2]/a");
	public By scaricaPDFGas = By.xpath("//ul[@id='fornitura_1']/li[1]/dl/ul/li[3]/a");
	public By paymentOnlineHeading = By.xpath("//div[@id='pagamentiOnlineStep2']/div[@class='list-form-container']/h1");
	public By moreLuce = By.xpath("//ul[@id='fornitura_1']/li[4]/dl/button[@class='btnAppearanceRemover open-menu-azioni bolletteModel']");
	public By moreLuce6100000274 = By.xpath("//ul[@id='fornitura_0']/li[2]//button");
	public By moreLuce4000007572 = By.xpath("//ul[@id='fornitura_1']/li[1]//button");
	public By visualizzaTutte = By.xpath("//div[@id='bill-list-panel-1']/div[@class='open-tab-list']/a[1]");
//	public By dettaglioBolletta6100000274 = By.xpath("//ul[@id='fornitura_0']/li[2]//a[.='Dettaglio bolletta']");
	public By dettaglioBolletta6100000274 = By.xpath("//ul[@class='azioni-pagamento show-azioni']/li[2]//a[.='Dettaglio bolletta']");
	public By scaricaPDF4000007572 = By.xpath("//ul[@id='fornitura_1']/li[1]//a[.='Scarica PDF Bolletta']");
	
	public By bollettaWebTitle = By.xpath("//h2[text()='Bolletta Web']");
	public By bollettaWebSubText = By.xpath("//h2[text()='Bolletta Web']//following::p[2]");
	public By ModificaIndirizzoEmail = By.xpath("//span[text()='MODIFICA INDIRIZZO EMAIL']");
	
	public BolletteComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	 
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	    
	    public void scrollComponent(By object) throws Exception {
			util.objectManager(object, util.scrollToVisibility, false);
		}
	    
	    public void checkForResidentialMenu(String property) throws Exception{
	    	
	    	util.waitAndGetElement(supplies);
	    	if(! property.contains(driver.findElement(supplies).getText()) && property.contains(driver.findElement(services).getText()) && property.contains(driver.findElement(enelPremiaWOW).getText())
	    		&& property.contains(driver.findElement(bills).getText()) && property.contains(driver.findElement(news).getText())
	    		&& property.contains(driver.findElement(videoSpace).getText()) && property.contains(driver.findElement(yourRights).getText())
	    		&& property.contains(driver.findElement(support).getText()) && property.contains(driver.findElement(findEnelSpace).getText())
	    		&& property.contains(driver.findElement(exit).getText()) && property.contains(driver.findElement(account).getText()))
	    	
	    		throw new Exception ("In the page 'https://www-colla.enel.it/' Residential menu item is not displaying");
	    }
	    
	    public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
		
	    public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL is different from the one saved before.");
		}
	    
		public void checkSupplyStatus(By checkElement,String property) throws Exception{
			
			String supply = driver.findElement(checkElement).getText();
			
			if(!property.equals(supply)) 
				throw new Exception (" Supply staus"+ supply+" is not matching with the actual supply status "+property);
		}
		
		public void verifyCustomerNumber(By checkElement,String property) throws Exception{
			
			String customerNum = driver.findElement(checkElement).getText();
			
			if(! property.equals(customerNum))
				throw new Exception (" Supply staus"+ customerNum+" is not matching with the actual supply status "+property);

		}
		
		public void checkForSupplyColour(By checkElement) throws Exception{
			
			String color =  driver.findElement(checkElement).getCssValue("color");
			
			System.out.println("Colur "+color);
			
		}
		
		public void checkFieldValue(By checkElement,String property) throws Exception{
			
			String supply = driver.findElement(checkElement).getText();
			
			if(!property.equals(supply)) 
				throw new Exception (" Supply staus"+ supply+" is not matching with the actual supply status "+property);
			
		}
		
		public void billToBePaid(By checkElement , String property) throws Exception
		{
			String bill = driver.findElement(checkElement).getText();
			
			String [] array = bill.split("\\s");
		
				if(!array[array.length-1].contains(property))
					throw new Exception ("Bills to be paid is not equal to "+property);
			
		}

		public void navigateBack(By checkObject) throws Exception {
			
			driver.navigate().back();
			util.waitAndGetElement(checkObject);
			
		}

		public void checkForContent(By header,By text,String property) throws Exception {
			
			String title = driver.findElement(header).getText();
			String Subtext = driver.findElement(text).getText();
			
			if(! property.contains(title) && property.contains(Subtext))
				throw new Exception (" Text is not matching with actual data"+property);
			
		}

		public void checkFieldDisplay(By checkObject) throws Exception {
			
			Boolean isPresent = driver.findElements(checkObject).size() > 0;
			if(isPresent.equals("true"))
				throw new Exception(checkObject+"Field is displaying");
		}
		
		public boolean isElementExist(By locator)
	    {
	        try {
	            driver.findElement(locator);
	        } catch (NoSuchElementException e) {
	            return false;
	        }

	        return true;
	    }
		
		public void SwitchTabToCustomerArea() throws Exception
		{
		   
			Set <String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
			   // Switch to child window
				System.out.println("switched to "+driver.getTitle()+"  Window");
		        String pagetitle = driver.getTitle();
			    driver.switchTo().window(winHandle);
			 }
			verifyComponentExistence(pageTitle);
			
			driver.close();
			driver.switchTo().window(currentHandle);
		}

		public void verifyFilterValues(By checkObject,String property) throws Exception{
			
			List<WebElement> web =   driver.findElements(checkObject);
			
			ArrayList<String> list = new ArrayList<String>();
			for(int i=0;i<web.size();i++){
				
				String checkId = web.get(i).getAttribute("id");
				Thread.sleep(100);
				String filterValue = driver.findElement(By.xpath("//label[@for='"+checkId+"']")).getText();
				list.add(filterValue);
			}	
				for(int j=0;j<list.size();j++){
				
					if(!property.contains(list.get(j)))
					throw new Exception(list.get(j)+" Filter value is not matching with the expected value "+property);
				}
			

			
		}

		public void checkForText(By checkObject, String property) throws Exception {
			
			String actualText = driver.findElement(checkObject).getText().trim();
			String value = actualText.replaceAll("\\s+", " ");
			String result = "NO";
			if(value.equals(property)){
				result="YES";
			}
			if(result.equals("NO"))
				throw new Exception(value+" Text is not matching with "+property);
			
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			//System.out.println("Normalized:\n"+weText);
			//System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}
		
		public static final String PAYMENT_ONLINE_HEADING = "Pagamento online";
		public static final String privateAreaOnlinePayment = "https://www-coll1.enel.it/it/area-clienti/residenziale/servizi/pagamenti-online?fattura=0000008097017297";
		
}


