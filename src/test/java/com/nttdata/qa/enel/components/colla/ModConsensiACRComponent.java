package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModConsensiACRComponent {
	public static String RB1Label = null;
	WebDriver driver;
	SeleniumUtilities util;
	Properties prop = null;
	
	public static ArrayList<String> list = new ArrayList<String>();
	//public static Object[] objects = list.toArray();
	 
	 
	public By account = By.xpath("//a[@id='accountSelfcare']");
	public By accountHeading = By.xpath("//h1[@id='profile-contact']");
	public By accountParagraph = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'Consulta e aggiorna')]");
	public By consesiHeading = By.xpath("//div[@id='mainContentWrapper']//h2[.='Consensi']");
	public By consesiParagraph = By.xpath("//div[@id='mainContentWrapper']//p[2][contains(text(),'Comunica')]");
	public By modificaConsensiButton = By.xpath("//div[@id='mainContentWrapper']//button/span[.='MODIFICA CONSENSI']");
	public By consensiSectionHeading = By.xpath("//section[@class='consensi']/div[@class='consensi-list-heading']");
	public By salvaModificheButton = By.xpath("//div[@id='sc']//button[@class='button_first']");
	public By confirmationHeading  = By.xpath("//h3[@id='h1_consensi']");
	public By confirmationParagraph  = By.xpath("//div[@id='sc']//p");
	public By fineButton = By.xpath("//div[@id='sc']//span[contains(text(),'Fine')]");
	public By indietroButton = By.xpath("//div[@id='sc']//span[.='Indietro']");
	public By contatti = By.xpath("//li[@id='contattiAct']/a");
	public By contattiPath1 = By.xpath("//main[@id='main']//li[1][.='Area riservata']");
	public By contattiPath2 = By.xpath("//main[@id='main']//li[2][.='Richiesta contatto']");
	public By contattiHeading = By.xpath("//main[@id='main']//h1");
	public By contattiParagraph = By.xpath("//main[@id='main']//p[@class='lead']");
	public By seiInteressato = By.xpath("//div[@id='prefrichiesta_key']//a[@class='customselect-choice']");
	public By seiInteressatoList = By.xpath("//div[@id='prefrichiesta_key']//li/label/span");
	public By tipologiaOfferta = By.xpath("//div[@id='prefofferta_key']//a[@class='customselect-choice']");
	public By tipologiaOffertaList = By.xpath("//div[@id='prefofferta_key']//li/label/span");
	public By provincia = By.xpath("//input[@id='Provincia']");
	public By roma = By.xpath("//ul/li/div[.='ROMA']");
	public By milano = By.xpath("//ul/li/div[.='MILANO']");
	public By cesena = By.xpath("//ul//div[contains(text(),'CESENA')]");
	public By canaleContattoPreferito = By.xpath("//div[@id='prefcontact_key']//a");
	public By email = By.xpath("//input[@id='email']");
	public By telefonFisso = By.xpath("//input[@id='Telefono']");
	public By cellulare = By.xpath("//input[@id='Cellulare']");
	public By informativaHeading = By.xpath("//div[@class='col-xs-12'][1]/h5");
	public By informativaParagraph = By.xpath("//div[@class='col-xs-12'][1]/p[@class='margin-bottom-25']");
	public By accetto = By.xpath("//label[@class='radio-block inline-block'][1]/span");
	public By nonAccetto = By.xpath("//label[@class='radio-block inline-block'][2]/span");
	public By esciButton = By.xpath("//a[.='Esci']");
	public By logo = By.xpath("//div[@id='openModalUserBtn']");
	public By homepagePath1 = By.xpath("//main[@id='main']//li[1][.='Area riservata']");
	public By homepagePath2 = By.xpath("//main[@id='main']//li[2][.='Homepage']");
	public By homepageHeading = By.xpath("//section[@id='content-home']//h1");
	public By homePageHeading1 = By.xpath("//div[@id='mainContentWrapper']//h1[contains(text(),'Benven')]");
	public By homePageParagraph1 = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'In questa')]");
	public By homePageHeading2 = By.xpath("//div[@id='mainContentWrapper']//h2[contains(text(),'Le tue forniture')]");
	public By homePageParagraph2 = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'Visualizza')]");
	public By telefonoFissoConsensi1SIRadio = By.xpath("//input[@id ='consenso-enel-si--3']");
	public By telefonoFissoConsensi1NORadio = By.xpath("//input[@id ='consenso-enel-no--3']");
	public By telefonoFissoConsensi1SIRadioLabel = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By telefonoFissoConsensi1NORadioLabel = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By telefonoFissoConsensi2SIRadioLabel = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By telefonoFissoConsensi2NORadioLabel = By.xpath("//div[@class='form-group-container simply-border'][2]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By telefonoFissoConsensi2 = By.xpath("//div[@class='form-group-container simply-border'][2]//child::fieldset[@class='form-group inline-align'][2]/legend[@class='label-appearance']");
	public By telefonoFissoConsensi2SIRadio = By.xpath("//input[@id ='consenso-terzi-si--3']");
	public By telefonoFissoConsensi2NORadio = By.xpath("//input[@id ='consenso-terzi-no--3']");
	public By emailCensensi1 = By.xpath("//div[@class='form-group-container simply-border'][3]//child::fieldset[@class='form-group inline-align'][1]/legend[@class='label-appearance']");
	public By emailConsensi1SIRadio = By.xpath("//input[@id ='consenso-enel-si--1']");
	public By emailConsensi1NORadio = By.xpath("//input[@id ='consenso-enel-no--1']");
	public By emailConsensi2 = By.xpath("//div[@class='form-group-container simply-border'][3]//child::fieldset[@class='form-group inline-align'][2]/legend[@class='label-appearance']");
	public By emailConsensi2SIRadio = By.xpath("//input[@id ='consenso-terzi-si--1']");
	public By emailConsensi2NORadio = By.xpath("//input[@id ='consenso-terzi-no--1']");
	public By emailConsensi1SIRadioLabel = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By emailConsensi1NORadioLabel = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By emailConsensi2SIRadioLabel = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By emailConsensi2NORadioLabel = By.xpath("//div[@class='form-group-container simply-border'][3]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By numeroDiCellulareCensensi1 = By.xpath("//div[@class='form-group-container simply-border'][1]//child::fieldset[@class='form-group inline-align'][1]/legend[@class='label-appearance']");
	public By numeroDiCellulareConsensi1SIRadio = By.xpath("//input[@id ='consenso-enel-si--2']");
	public By numeroDiCellulareConsensi1NORadio = By.xpath("//input[@id ='consenso-enel-no--2']");
	public By numeroDiCellulareCensensi12 = By.xpath("//div[@class='form-group-container simply-border'][1]//child::fieldset[@class='form-group inline-align'][2]/legend[@class='label-appearance']");
	public By numeroDiCellulareConsensi2SIRadio = By.xpath("//input[@id ='consenso-terzi-si--2']");
	public By numeroDiCellulareConsensi2NORadio = By.xpath("//input[@id ='consenso-terzi-no--2']");
	public By numeroDiCellulareConsensi1SIRadioLabel = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By numeroDiCellulareConsensi1NORadioLabel = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By numeroDiCellulareConsensi2SIRadioLabel = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][1]/label[@class='label-radio']");
	public By numeroDiCellulareConsensi2NORadioLabel = By.xpath("//div[@class='form-group-container simply-border'][1]/fieldset[@class='form-group inline-align'][2]/div[@class='radio-container'][2]/label[@class='label-radio']");
	public By profilazoneSIRadio = By.xpath("//input[@id ='consenso-profilazione-si']");
	public By profilazoneNORadio = By.xpath("//input[@id ='consenso-profilazione-no']");
	public By profilazoneSIRadioLabel = By.xpath("//div[@id='sc']//section[@class='consensi']/div[@class='form-group-container']//div[@class='radio-container'][1]/label");
	public By profilazoneNORadioLabel = By.xpath("//div[@id='sc']//section[@class='consensi']/div[@class='form-group-container']//div[@class='radio-container'][2]/label");
	
	
	
	public ModConsensiACRComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
  
  public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
  
  public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
  
  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void  isElementNotPresent(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			
			try{
				WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
			} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        		            
	            }
	         }
	 
	 public void  isRadioSelected(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			for(int i=1;i<=3;i++)
			{
			try{
				driver.findElement(By.xpath("//div[@class='form-group-container simply-border']["+ i +"]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']")).isSelected();
				i++;
				} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        		            
	         }
			}
	      }
	 
	 public void verifyDropDownValue(By checkObject, By checkValues, String[] exp) throws Exception{
				List <WebElement> list  = driver.findElements(checkValues);
				for (WebElement we : list) {
		    	//System.out.println(we.getText());
		    	if(!Arrays.asList(exp).contains(we.getText() ))
		    		throw new Exception ("The list of values are not availabl");
		       	    } 
		}
	 
	 public void waitForElementToDisplay (By checkObject) throws Exception{
         
         WebDriverWait wait = new WebDriverWait (driver, 20);
         wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
        
     }
	 
	 public void radiobuttonStatus(By checkObject1, By checkObject2, By checkObject1Label, By checkObject2Label ){
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 30);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject1));
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject2));
			
			WebElement RB1 = driver.findElement(checkObject1);
					
			if (!RB1.isSelected()){
				String itemXpath = checkObject1.toString().replace("By.xpath: ", "");
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("(document.evaluate(\""+itemXpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
				RB1Label = driver.findElement(checkObject1Label).getText();
				list.add(RB1Label);
			}	
			else {
				String itemXpath = checkObject2.toString().replace("By.xpath: ", "");
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("(document.evaluate(\""+itemXpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
				String RB2Label = driver.findElement(checkObject2Label).getText();
				list.add(RB2Label);
			}
			
		}
	 public static final String ACCOUNT_HEADING = "Dati e Contatti";
	 public static final String ACCOUNT_PARAGRAPH = "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione";
	 public static final String CONSESI_HEADING = "Consensi";
	 public static final String CONSESI_PARAGRAPH = "Comunica o aggiorna i consensi a Enel Energia e ai suoi partner";
	 public static final String CONSENSI_SECTION_HEADING = "ConsensiComunica o aggiorna i consensi a Enel Energia e ai suoi partnerLeggi l’Informativa sulla Privacy";
	 public static final String CONFIRMATION_HEADING ="L'operazione è andata a buon fine";
	 public static final String CONFIRMATION_PARAGRAPH ="L'operazione stata eseguita correttamente. I tuoi consensi sono stati aggiornati. Ricordati che puoi modificare i tuoi consensi anche dall''APP di Enel Energia. Se ancora non ce l'hai, scaricala gratuitamente e scopri tutti i servizi e i vantaggi dedicati a te.";
	 public static final String CONTATTI_PATH1 = "Area riservata";
	 public static final String CONTATTI_PATH2 = "Richiesta contatto";
	 public static final String CONTATTI_HEADING = "Richiesta contatto";
	 public static final String CONTATTI_PARAGRAPH = "Gentile cliente, compilando il modulo sottostante la richiesta verrà inoltrata ad un nostro consulente. Verrai ricontattato al più presto.";
	 public static final String[] INTERESSATO_VALUES = {"Nuova attivazione","Modifica servizio","Revoca servizio","Altro","Annulla selezione"};
	 public static final String[] TIPOLOGIA_OFFERTA_VALUES = {"Luce","Gas","Annulla selezione"};
	 public static final String INForMATIVA_HEADING = "Informativa sulla privacy";
	 public static final String INForMATIVA_PARAGRAPH = "Ti ricordiamo che Enel Energia tratterà i tuoi dati personali nel rispetto delle disposizioni previste dal Regolamento UE 2016/679 (GDPR) per le finalità e con le modalità indicate nell'Informativa Privacy. Titolare del trattamento è Enel Energia S.p.A. Informativa privacy completa disponibile sul sito https://www.enel.it";
	 public static final String HOMEPAGE_PATH1 = "Area riservata";
	 public static final String HOMEPAGE_PATH2 = "Homepage";
	 public static final String HOMEPAGE_HEADING = "Benvenuto nella tua area dedicata Business";
	 public static final String HOME_PAGE_HEADING1 = "Benvenuto nella tua area privata";
	 public static final String HOME_PAGE_PARAGRAPH1 = "In questa sezione potrai gestire le tue forniture";
	 public static final String HOME_PAGE_HEADING2 = "Le tue forniture";
	 public static final String HOME_PAGE_PARAGRAPH2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
}
