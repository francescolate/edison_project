package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class DatiFornituraComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public By selectCategoriaMerceologicaSAP = By.xpath("//label[contains(text(),'Categoria Merceologica SAP')]/parent::div//select");
	public By selectTitolarita= By.xpath("//label[contains(text(),'Titolarit')]/parent::div//select");
	public By selectResidenza= By.xpath("//label[text()='Flag Residente']/parent::div//select");
	public By inputConsumoAnnuo= By.xpath("//label[text()='Consumo Annuo']/parent::div//input");
	public By avantiButton = By.xpath("//button[@id='nextCommodityData_Button']");
	public By flaDisalimentabilita = By.xpath("//label[text()='Disalimentabilità']/../span/select");
	//GAS
	public By selectCodiceAteco=By.xpath("//label[text()='Codice Ateco']/parent::div//select");
	public By selectCodiceAteco2=By.xpath("//label[text()='Codice ATECO']/parent::div//select");
	public By selectCategoriaConsumo=By.xpath("//label[text()='Categoria di Consumo']/parent::div//select");
	public By selectCategoriaMarketing=By.xpath("//label[text()='Categoria Marketing']/parent::div//select");
	public By selectOrdineGrandezza=By.xpath("//label[text()='Ordine di Grandezza']/parent::div//select");
	public By selectProfiloConsumo=By.xpath("//label[text()='Profilo di consumo']/parent::div//select");



	public By selectPotenzialita=By.xpath("//label[contains(text(),'Potenzialit')]/parent::div//select");
	public By selectUtilizzo=By.xpath("//label[text()='Utilizzo']/parent::div//select");
	public By selectCategoriaUso=By.xpath("//label[text()='Categoria Uso']/parent::div//select");
	public By selectClassePrelievo=By.xpath("//label[text()='Classe di prelievo']/parent::div//select");
	public By inputLetturaMisuratore= By.xpath("//label[text()='Lettura misuratore']/parent::div//input");
	public By buttonValidaLettura= By.xpath("//input[contains(@value,'Valida lettura')]");
	public By h3GasCommodity=By.xpath("//h3[contains(text(),'Gas Commodity')]");
	//Allaccio
	public By selectTensioneRichiesta=By.xpath("//span[contains(text(),'Tensione Richiesta')]/ancestor::div[1]//select[@name='tensioneRichiesta']");
	public By inputPotenzaRichiesta= By.xpath("//span[contains(text(),'Potenza Richiesta')]/ancestor::div[1]//input");
	public By selectCategoriaMerceologicaSAP2=By.xpath("//span[contains(text(),'Categoria Merceologica SAP')]/ancestor::div[1]//select[@name='categoriaMerceologicaSAP']");
	public By inputConsumoAnnuo2= By.xpath("//label[text()='Consumo Annuo']/ancestor::div[1]//input[@name='consumoAnnuo']");
	public By inputTelefonoDistributore= By.xpath("//span[contains(text(),'Telefono Distributore')]/ancestor::div[1]//input");
	public By selectTitolarita2= By.xpath("//span[contains(text(),'Titolarit')]/ancestor::div[1]//select[@name='titolarita']");
	public By selectTipoMisuratore= By.xpath("//span[contains(text(),'Tipo Misuratore')]/ancestor::div[1]//select[@name='tipoMisuratore']");
	public By avantiButton2 = By.xpath("//button[@id='buttonNext']");
	public By alertUsoStraodinario=By.xpath("//div[@id='theModalCmpDescr_A']/p[contains(text(),'Per forniture straordinarie che alimentano giostre ricorda di selezionare Ascensore = Si')]");
	public By buttonOkAlertUsoStraodinario=By.xpath("//button[@id='theModalCmpBtn_A']");
	public By buttonCloseInfo = new By.ById("closeButtonInfo");

	public DatiFornituraComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}


	public void selezionaCategoriaMercologiaSAP(String frame, By categoria, String valore) throws Exception{
		util.frameManager(frame, categoria, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}

	public void selezionaCategoriaMercologiaSAP(By categoria, String valore) throws Exception{
		util.objectManager(categoria, util.scrollAndSelect, valore);
		TimeUnit.SECONDS.sleep(5);
	}
	
	public void selezionaCategoriaMercologiaSAPAllaccio (String valore) throws Exception{
		 
		By divMenu = By.xpath("//label[text()='Categoria Merceologica SAP']/..//input");
		util.objectManager(divMenu, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		By selezionaValue = By.xpath("//label[text()='Categoria Merceologica SAP']/..//span[contains(text(),'"+valore+"')]");
		util.objectManager(selezionaValue, util.scrollAndClick);
	
	}

	public void selezionaTitolarita(String frame, By titolarita, String valore) throws Exception{
		util.frameManager(frame, titolarita, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	
	
	public void selezionaTitolarita(By titolarita, String valore) throws Exception{
		util.objectManager(titolarita, util.scrollAndSelect, valore);
		TimeUnit.SECONDS.sleep(5);
	}

	public void inserisciConsumoAnnuo(String frame, By consumoAnnuo, String valore) throws Exception{
		util.frameManager(frame, consumoAnnuo, util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void inserisciConsumoAnnuo(By consumoAnnuo, String valore) throws Exception{
		util.objectManager(consumoAnnuo, util.scrollAndSendKeys, valore);
		TimeUnit.SECONDS.sleep(5);
	}

	public void selezionaResidenza(String frame, By residenza, String valore) throws Exception{
		util.frameManager(frame, residenza, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaCodiceAteco(String frame, By codiceAteco, String valore) throws Exception{
		util.frameManager(frame, codiceAteco, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaCategoriaConsumo(String frame, By categoriaConsumo, String valore) throws Exception{
		util.frameManager(frame, categoriaConsumo, util.scrollAndSelect,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}

	public void selezionaCategoriaMarketing(String frame, By categoriaMarketing, String valore) throws Exception{
		util.frameManager(frame, categoriaMarketing, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}

	public void selezionaPrimoOrdineGrandezza(String frame, By ordineGrandezza, String valore) throws Exception{
		util.frameManager(frame, ordineGrandezza, util.selectFirstElemet,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaOrdineGrandezza(String frame, By ordineGrandezza, String valore) throws Exception{
		util.frameManager(frame, ordineGrandezza, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	
	
	public void selezionaPrimoProfiloConsumo(String frame, By profiloConsumo, String valore) throws Exception{
		util.frameManager(frame, profiloConsumo, util.selectFirstElemet,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	public void selezionaProfiloConsumo(String frame, By profiloConsumo, String valore) throws Exception{
		util.frameManager(frame, profiloConsumo, util.scrollAndSelect,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	public void selezionaPotenzialita(String frame, By potenzialita, String valore) throws Exception{
		util.frameManager(frame, potenzialita, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaUtilizzo(String frame, By utilizzo, String valore) throws Exception{
		util.frameManager(frame, utilizzo, util.scrollAndSelect,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	public void selezionaCategoriaUso(String frame, By categoriaUso, String valore) throws Exception{
		util.frameManager(frame, categoriaUso, util.scrollAndSelect,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	public void selezionaClassePrelievo(String frame, By classePrelievo, String valore) throws Exception{
		util.frameManager(frame, classePrelievo, util.scrollAndSelect,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	public void inserisciLetturaMisuratore(String frame, By misuratore, String valore) throws Exception{
		util.frameManager(frame, misuratore, util.sendKeys,valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void validaLettura(String frame, By buttonValida) throws Exception{
		util.frameManager(frame, buttonValida, util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	public void selezionaTensioneRichiesta(By tensione,String valore)throws Exception{
		util.objectManager(tensione, util.scrollAndSelect, valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void inserisciPotenzaRichiesta(By potenza,String valore)throws Exception{
		util.objectManager(potenza, util.scrollAndSendKeys, valore);
		driver.findElement(By.xpath("//body")).click();
		spinner.checkSpinners();
	}
	public void inserisciTelefonoDistributore(By telefono,String valore)throws Exception{
		util.objectManager(telefono, util.scrollAndSendKeys, valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaTipoMisuratore(By misuratore,String valore)throws Exception{
		util.objectManager(misuratore, util.scrollAndSelect, valore);
		TimeUnit.SECONDS.sleep(5);
	}
	public void ClickButtonAvanti(String frame, By buttonAvanti)throws Exception{
		util.frameManager(frame, buttonAvanti, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}

	public void clickButtonAvanti(By buttonAvanti)throws Exception{
		util.objectManager(buttonAvanti, util.scrollAndClick);
		spinner.checkSpinners();
		TimeUnit.SECONDS.sleep(5);
	}

	public void selezionaDisalimentabilita(String frame, By objectSelect, String valore) throws Exception{
		util.frameManager(frame, objectSelect, util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(5);
	}

	public By getSelectCategoriaMerceologicaSAP() {
		return selectCategoriaMerceologicaSAP;
	}


	public void setSelectCategoriaMerceologicaSAP(By selectCategoriaMerceologicaSAP) {
		this.selectCategoriaMerceologicaSAP = selectCategoriaMerceologicaSAP;
	}


	public By getSelectResidenza() {
		return selectResidenza;
	}


	public void setSelectResidenza(By selectResidenza) {
		this.selectResidenza = selectResidenza;
	}


	public By getAvantiButton() {
		return avantiButton;
	}


	public void setAvantiButton(By avantiButton) {
		this.avantiButton = avantiButton;
	}

	public void verificaPresenzaAlertUsoStraordinaria(By alert, By buttonConfirm)throws Exception{
		if(util.exists(alert, 20)){
			if(driver.findElement(alert).isDisplayed()){
				util.objectManager(buttonConfirm, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(5);
			}
			else{
				System.out.println("Alert per uso Straordinaria non presente");}
		}
		driver.switchTo().defaultContent();
	}


	public By getSelectCategoriaConsumo() {
		return selectCategoriaConsumo;
	}


	public void setSelectCategoriaConsumo(By selectCategoriaConsumo) {
		this.selectCategoriaConsumo = selectCategoriaConsumo;
	}

	public void setSelectOrdineGrandezza(By selectOrdineGrandezza) {
		this.selectOrdineGrandezza = selectOrdineGrandezza;
	}
	
	public By getSelectProfiloConsumo() {
		return selectProfiloConsumo;
	}


	public void setSelectProfiloConsumo(By selectProfiloConsumo) {
		this.selectProfiloConsumo = selectProfiloConsumo;
	}

	public void chiudiModalDialog(String frame,By x) throws Exception {
		util.frameManager(frame, x, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}
	public By getSelectCodiceAteco() {
		return selectCodiceAteco;
	}


	public void setSelectCodiceAteco(By selectCodiceAteco) {
		this.selectCodiceAteco = selectCodiceAteco;
	}

}
