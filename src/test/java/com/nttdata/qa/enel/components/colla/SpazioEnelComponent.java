package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SpazioEnelComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By spazioEnelTitle = By.xpath("//span[text()='Trova lo Spazio Enel più vicino a te' and @class='image-hero_title text--page-heading']");
	public By errorMessage=By.xpath("//div[@id='es-header']//span[text()='Not Found']");
	public String  linkSPAZIO_ENEL="spazio-enel/";
	public By spazioEnelPath=By.xpath("//ul[@class='breadcrumbs component']");
	public SpazioEnelComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkHeaderPageSpazioEnel(By path,By title) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(path));
		String pathDescription=driver.findElement(path).getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	
			//System.out.println(pathDescription);	
		if (!pathDescription.contentEquals("HOME SPAZIO ENEL"))
			throw new Exception("sulla pagina web 'Spazio Enel' l'oggetto web con xpath " + path + " non esiste");
		if (!util.exists(title, 15))
			throw new Exception("sulla pagina web 'Spazio Enel' il titolo con xpath " + title + " non esiste");
		
	}
	
	public void checkError(By existingObject) throws Exception {
		if (util.exists(existingObject, 15))
			throw new Exception("sulla pagina web 'spazio-enel' l'oggetto con xpath " + existingObject + " NON e' presente");
	}
}
