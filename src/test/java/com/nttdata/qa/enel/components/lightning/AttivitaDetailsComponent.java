package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class AttivitaDetailsComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public AttivitaDetailsComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public By attivaOne = By.xpath("//h1[text()='Attività']/ancestor::div[@class='oneWorkspaceTabWrapper']//span[text()='FATTO']//ancestor::td/preceding-sibling::td//span[contains(text(),'N/A')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Modifica Potenza')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Gestione Lavori')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Processo')]");
	public By attivaSecond = By.xpath("//h1[text()='Attività']/ancestor::div[@class='oneWorkspaceTabWrapper']//span[text()='FATTO']//ancestor::td/preceding-sibling::td//span[contains(text(),'Modifica Potenza')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Predeterminabile')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Preventivo')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Emissione preventivo')]"); 
	public String dataApertura1 = "//span[text()='N/A']/ancestor::td/following-sibling::td//span[text()='FATTO']/ancestor::td/following-sibling::td[1]//span[text()='$DATE$']";
	public String dataChiusura1 ="//span[text()='N/A']/ancestor::td/following-sibling::td//span[text()='FATTO']/ancestor::td/following-sibling::td[2]//span[text()='$DATE$']";
	public String dataApertura2 = "//h1[text()='Attività']/ancestor::div[@class='oneWorkspaceTabWrapper']//ancestor::td/following-sibling::td//span[text()='Predeterminabile']/ancestor::td/following-sibling::td//span[text()='FATTO']/ancestor::td/following-sibling::td[1]//span[text()='$DATE$']";
	public String dataChiusura2 = "//h1[text()='Attività']/ancestor::div[@class='oneWorkspaceTabWrapper']//ancestor::td/following-sibling::td//span[text()='Predeterminabile']/ancestor::td/following-sibling::td//span[text()='FATTO']/ancestor::td/following-sibling::td[2]//span[text()='$DATE$']";
	public By attivaChangeVoltage = By.xpath("//h1[text()='Attività']/ancestor::div[@class='oneWorkspaceTabWrapper']//span[text()='FATTO']//ancestor::td/preceding-sibling::td//span[contains(text(),'N/A')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Modifica Tensione')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Gestione Lavori')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Processo')]");
	public By attivaEmissionePreventivo = By.xpath("//h1[text()='Attività']/ancestor::div[@class='oneWorkspaceTabWrapper']//span[text()='FATTO']//ancestor::td/preceding-sibling::td//span[contains(text(),'Modifica Tensione')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Non Predeterminabile')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Preventivo')]//ancestor::td/preceding-sibling::td//span[contains(text(),'Emiss. automatica preventivo')]");
	
	public void verifyAttivaDetails(By existingObject) throws Exception{	
		util.waitAndGetElement(existingObject);
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void verifyAttivitaDetails(By checkObject,String[] value) throws Exception{
		String actualValue = driver.findElement(checkObject).getText();
		if(!value.toString().contains(actualValue))
		throw new Exception("Details is not matching with data");
	}
	
	public static final String[] attivitaTwo = {"Emissione preventivo","Contatto Preventivo","Predeterminabile","Modifica Potenza","FATTO"};
}
