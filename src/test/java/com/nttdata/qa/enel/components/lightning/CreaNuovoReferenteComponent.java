package com.nttdata.qa.enel.components.lightning;

import java.sql.Time;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.testqantt.VerificaIndirizziDomicilio;
import com.nttdata.qa.enel.util.SeleniumUtilities;

//Il seguente componente si occupa della gestione della creazione di un nuovo Referente dal tab Clienti
public class CreaNuovoReferenteComponent {
    private WebDriver driver;
    private SeleniumUtilities util;
    String frameName;

    public CreaNuovoReferenteComponent(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
    util=new SeleniumUtilities(driver);

    
  }
 
//    @FindBy(xpath = "//*[@id='split-left']/div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr/th/span/a")
    @FindBy(xpath = "//div/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr/th/span/a")
    private WebElement selezionaCliente;
   
    @FindBy(xpath = "//div[@title='Nuovo' and (text() = 'Nuovo')]")
    private WebElement nuovoReferenteButton;
    
 
  
    
    public void setFrameName(String frame) throws Exception{
    	this.frameName = frame;
    }
    
    public final By selectTipologiaReferente = By.xpath("//div[@id='tipologiaReferente']//label[text()='Tipologia Referente']/..//select[contains(@id,'tipologiaReferente')]");
    public final By radioButtonResidenziale = By.xpath("//label[@for='ITA_IFM_Residential']/span[@class='slds-radio--faux']");
    public final By radioButtonImpresaIndividuale = By.xpath("//label[@for='ITA_IFM_ImpresaIndividuale']/span[@class='slds-radio--faux']");
    public final By radioButtonCondominio = By.xpath("//label[@for='ITA_IFM_Condominio']/span[@class='slds-radio--faux']");
    public final By radioButtonBusiness = By.xpath("//label[@for='ITA_IFM_Business']/span[@class='slds-radio--faux']");
    private final By confirmButton = By.xpath("//button[@id='modalNewAccountNext']");
    public final By nuovoReferenteDaInterazioniButton = By.xpath("//div[@id='AccountSearchCard']//button[text()='Nuovo Referente' and contains(@class,'newAccountBtn')]");

   private final By cellulareField = By.xpath("//input[contains(@id,'valorecellulare')]");
   private final By descrizioneCellulareField = By.xpath("//select[contains(@id,'descrizionecellulare')]");
   
   private final By inputNome = By.xpath("//input[contains(@id,'FirstName')]");
   private final By inputCognome = By.xpath("//input[contains(@id,'LastName')]");
   private final By inputSesso = By.xpath("//select[contains(@id,'sesso')]");
   private final By inputCF = By.xpath("//input[contains(@id,'CodiceFiscale')]");
   private final By inputDataNascita = By.xpath("//input[contains(@id,'datePicker')]");
   private final By inputComuneNascita = By.xpath("//input[contains(@id,'lookupComuneDiNascita')]");
   private final By inputPartitaIva = By.xpath("//input[contains(@id,'partitaIVA')]");
   private final By inputRagioneSociale = By.xpath("//input[contains(@id,'nomeReferenteRagioneSociale')]");
   private final By inputAccount = By.xpath("//input[contains(@id,'codiceFiscaleAccount')]");
   private final By selectTipoFormaGiuridica = By.xpath("//select[contains(@id,'TipoFormaGiuridica')]");
   private final By selectRuoloReferente = By.xpath("//select[contains(@id,'ruolo')]");
   public final By selectRuoloReferente2 = By.xpath("//select[contains(@id,'ruoloContact')]");
   
   public final By checkDipendenteEnel = By.xpath("//legend[text()='Dipendente Enel']/../..//label[@class='slds-checkbox']");
		   
   private final By calcolaCodiceFiscaleButton = By.xpath("//button[text()='Calcola Codice Fiscale']");

   private final By confermaButton = By.xpath("//input[contains(@id,'btnConferma') | //button[@value='Conferma' and contains(@class,'saveRecord')]]");
   
   private final By conferma = By.xpath("//p[contains(text(),'Nuovo Referente')]/ancestor::div[@role='banner']//*[@value='Conferma']");
   private final By confermaOld = By.xpath("//p[contains(text(),'Nuovo Referente')]/ancestor::div[@role='banner']//button[@value='Conferma']");
   private final By annulla = By.xpath("//p[contains(text(),'Nuovo Referente')]/ancestor::div[@role='banner']//button[text()='Annulla']");
   private final By continua = By.id("buttonConfirmID_A");

   private final By confermaComuneNascita = By.xpath("//div[@id='lookupItemId']");  
   
	private By  tuttiIProcessi = new By.ByXPath("//a[@title = 'Tutti i processi']");
	
	private By  nuovoReferente = new By.ByXPath("//a[@title = 'Nuovo Referente']");

	public By tendina = By.xpath("//div[@id='lookupItemId']//ul//a");
	public By ReferenteField = By.id("lookupContattoAccount");

	public By inputProvincia=By.xpath("//*[@id='componentAddress4:provinceInput']");
	public By inputComune=By.xpath("//*[@id='componentAddress4:cityInput']");
	public By inputIndirizzo=By.xpath("//*[@id='componentAddress4:streetNameInput']");
	public By inputCivico=By.xpath("//*[@id='componentAddress4:streetNumberInput']");
	public By inputScala=By.xpath("//*[@id='componentAddress4:stairNumberInput']");
	public By inputPiano=By.xpath("//*[@id='componentAddress4:floorNumberInput']");
	public By inputInterno=By.xpath("//*[@id='componentAddress4:apartmentNumberInput']");
	
	public By bottoneVerificaIndirizzo=By.xpath("//*[@id='componentAddress4:verifyAddrBtn']");
	public By indirizzoVerificato=By.xpath("//*[@id='componentAddress4:verifiedAddrTick']");
	public By modificaIndirizzo=By.xpath("//*[@id='componentAddress4:editAddrBtn']");
	public By btnFormatoStampa=By.xpath("//*[@title='Formato stampa']");
	
	private By twitterAccount = By.xpath("//input[contains(@id,'form:twitterAccount')]");
	private By linkedinAccount = By.xpath("//input[contains(@id,'LinkedInAccount')]");
	private By fbAccount = By.xpath("//input[contains(@id,'FBAccount')]");
	private By webSite = By.xpath("//input[contains(@id,'form:Website')]");
	
	private By titoloStudio = By.xpath("//select[contains(@id,'form:TitoloDiStudio')]");
	private By professione = By.xpath("//select[contains(@id,'form:Professione')]");

	private By btnLente = By.xpath("//*[@class='slds-input__icon slds-icon-text-default']");
	private By btnNuovo = By.xpath("//button[text()='Nuovo']");
	private By referente = By.xpath("//label[contains(text(),'Referente')]/..//input");
	private By verifica = By.xpath("//*[contains(text(),'Nome e cognome')]");
		


		
	
	
	private void pulisciCampiAnagraficaMod(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception {
		util.frameManager(this.frameName, inputComuneNascita,util.sendKeys,comune);
		TimeUnit.SECONDS.sleep(10);
//		util.frameManager(this.frameName, confermaComuneNascita, util.scrollAndClick);
		util.frameManager(this.frameName, inputDataNascita,util.scrollAndSendKeys,dataNascita + Keys.RETURN);
		util.frameManager(this.frameName, inputNome,util.scrollAndSendKeys,nome);
		util.frameManager(this.frameName, inputCognome,util.scrollAndSendKeys,cognome);
		util.frameManager(this.frameName, inputSesso, util.scrollAndSelect,sesso);
		util.frameManager(this.frameName, calcolaCodiceFiscaleButton, util.scrollAndClick);
		
	}
	private void popolaCampiAnagrafica(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception {
		util.frameManager(this.frameName, inputComuneNascita,util.sendKeys,comune);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(this.frameName, confermaComuneNascita, util.scrollAndClick);
		util.frameManager(this.frameName, inputDataNascita,util.scrollAndSendKeys,dataNascita + Keys.RETURN);
		util.frameManager(this.frameName, inputNome,util.scrollAndSendKeys,nome);
		util.frameManager(this.frameName, inputCognome,util.scrollAndSendKeys,cognome);
		util.frameManager(this.frameName, inputSesso, util.scrollAndSelect,sesso);
		util.frameManager(this.frameName, calcolaCodiceFiscaleButton, util.scrollAndClick);
		
//		//prova domicilio
////		util.objectManager(inputProvincia,util.sendKeys,provincia);
//		util.frameManager(this.frameName, inputProvincia,util.sendKeys,provincia);
		
		
	}
	public void popolaCampiDomicilio(String provincia, String comune, String indirizzo, String civico) throws Exception {
// prima clicco bottone Verifica (ma i campi non sono obbligatori) e successivamente compilo i campi
		util.frameManager(this.frameName, bottoneVerificaIndirizzo, util.scrollAndClick);
// Compilo i campi dopo la verifica obbligatorietà		
		util.frameManager(this.frameName, inputProvincia,util.sendKeys,provincia);
		util.frameManager(this.frameName, inputComune,util.sendKeys,comune);
		util.frameManager(this.frameName, inputIndirizzo,util.sendKeys,indirizzo);
		util.frameManager(this.frameName, inputCivico,util.sendKeys,civico);
		util.frameManager(this.frameName, bottoneVerificaIndirizzo, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);

		if(!util.exists(this.frameName, indirizzoVerificato , 5)) throw new Exception("Non esiste la conferma 'Indirizzo Verificato'");
		
	}
	
	public void forzaDomicilio(String provincia, String comune, String indirizzo, String civico) throws Exception {
		// Compilo i campi 	
		util.frameManager(this.frameName, inputProvincia,util.sendKeys,provincia);
		util.frameManager(this.frameName, inputComune,util.sendKeys,comune);
		util.frameManager(this.frameName, inputIndirizzo,util.sendKeys,indirizzo);
		util.frameManager(this.frameName, inputCivico,util.sendKeys,civico);
		util.frameManager(this.frameName, bottoneVerificaIndirizzo, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(this.frameName, By.xpath("//input[@id='componentAddress4:forceAddrChk']/ancestor::label"), util.scrollAndClick);
		util.frameManager(this.frameName, By.xpath("//input[@id='componentAddress4:zipCodeInput']"), util.scrollAndSendKeys, "20121");
		util.frameManager(this.frameName, By.xpath("//td[text()='20121']/ancestor::table//tr[2]/td"), util.scrollAndClick);
		util.frameManager(this.frameName, By.xpath("//button[@id='componentAddress4:forceAddrBtn']"), util.scrollAndClick);
		if(!util.exists(this.frameName, By.xpath("//button[@id='componentAddress4:editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) 
		{
			throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");
		}
	}
	
	public void modificaIndirizzo() throws Exception {
		if(!util.exists(this.frameName, indirizzoVerificato , 5)) throw new Exception("Non esiste la conferma 'Indirizzo Verificato'");
		util.frameManager(this.frameName, modificaIndirizzo , util.scrollAndClick);
		util.frameManager(this.frameName, inputScala,util.sendKeys,"A");
		util.frameManager(this.frameName, inputPiano,util.sendKeys,"3");
		util.frameManager(this.frameName, inputInterno,util.sendKeys,"8");
		util.frameManager(this.frameName, bottoneVerificaIndirizzo, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);

	}
	
	public void forzaModificaIndirizzo() throws Exception {
		if(!util.exists(this.frameName, indirizzoVerificato , 5)) throw new Exception("Non esiste la conferma 'Indirizzo Verificato'");
		util.frameManager(this.frameName, modificaIndirizzo , util.scrollAndClick);
		util.frameManager(this.frameName, inputScala,util.sendKeys,"A");
		util.frameManager(this.frameName, inputPiano,util.sendKeys,"3");
		util.frameManager(this.frameName, inputInterno,util.sendKeys,"8");
		TimeUnit.SECONDS.sleep(2);
		util.frameManager(this.frameName, By.xpath("//button[@id='componentAddress4:forceAddrBtn']"), util.scrollAndClick);

	}

	public void verificaCalcoloCodiceFiscale(Properties prop) throws Exception {
		String nomeRef = prop.getProperty("NOME").trim();
		if (nomeRef.equalsIgnoreCase("RANDOM"))
		{
			nomeRef = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME", nomeRef);
		}
		this.popolaCampiAnagrafica(nomeRef,prop.getProperty("COGNOME").trim(),prop.getProperty("SESSO").trim(),prop.getProperty("DATA_NASCITA").trim(),prop.getProperty("COMUNE_NASCITA").trim());
		//this.driver.switchTo().frame(this.frameName);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		String cf = util.waitAndGetElement(inputCF).getAttribute("value");
		prop.setProperty("CODICE_FISCALE", cf);
		if(cf.length()<16) throw new Exception("Dopo la pressione del pulsante Calcola Codice Fiscale, il campo codice fiscale non viene popolato con un codice corretto (" +cf +")");
		this.driver.switchTo().defaultContent();
	}
	
		   
	private String compilaAnagraficaReferenteResidenziale(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception{
		//this.driver.switchTo().frame(this.frameName);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		String cf = util.waitAndGetElement(inputCF).getAttribute("value");
		this.driver.switchTo().defaultContent();
		return cf;
	}
	
	
	private boolean checkAlertPresence(WebElement alertBar) throws Exception{
		boolean exist = false;
		String style = alertBar.getAttribute("style");
		if(style.contentEquals("display: none;")) exist = false;
		else exist = true;
		return exist;
	}
	
	private boolean checkAlertMessage(String message) throws Exception {
		WebElement errors = util.waitAndGetElement(By.xpath("//span[@id='RedAlertsErrors']"));
		if(errors.getText().contains(message)) return true;
		else return false;
	}
	
	
	public void verificaReferenteGiaPresente() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un Referente gia' presenta a sistema non viene mostrato alcun messaggio di errore");
		else {
			if(!checkAlertMessage("Il codice fiscale inserito è già presente nel sistema")) throw new Exception("Dopo aver inserito un Referente già presente a sistema, vien rilevato un messaggio di errore diverso da : Il codice fiscale inserito è già presente nel sistema");

		}
		this.driver.switchTo().defaultContent();
		
	}
	
	public void verificaObbligatorietaCampiAnagraficaImpresa() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");
		if(!util.exists(checkDipendenteEnel, 5)) throw new Exception("Impossibile trovare la checkbox Dipendente Enel nella sezione Informazioni Anagrafiche Titolare");
		util.objectManager(calcolaCodiceFiscaleButton, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Nome")) throw new Exception("Il campo Nome risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Cognome")) throw new Exception("Il campo Cognome risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Sesso")) throw new Exception("Il campo Sesso risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Data di nascita")) throw new Exception("Il campo Data di nascita risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Comune di nascita")) throw new Exception("Il campo Comune di nascita risulta non essere obbligatorio");
            
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(inputComuneNascita,util.sendKeys,"NAPOLI");
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(confermaComuneNascita, util.scrollAndClick);
			util.objectManager(inputDataNascita,util.scrollAndSendKeys,"04/03/1985" + Keys.RETURN);
			util.objectManager(inputNome,util.scrollAndSendKeys,"TEST");
			util.objectManager(inputCognome,util.scrollAndSendKeys,"TEST");
			util.objectManager(inputSesso, util.scrollAndSelect,"M");
			util.objectManager(conferma, util.scrollAndClick);
			if(!checkAlertMessage("Errore: Il campo codice fiscale è obbligatorio")) throw new Exception("Il campo Codice Fiscale risulta non essere obbligatorio");
			util.objectManager(closeBar, util.scrollAndClick);
			driver.switchTo().defaultContent();
		
		}
		
		
	}
	
	
	public void verificaObbligatorietaCampiAnagraficaResidenziale() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");
		if(!util.exists(checkDipendenteEnel, 5)) throw new Exception("Impossibile trovare la checkbox Dipendente Enel nella sezione Informazioni Anagrafiche Titolare");
		util.objectManager(calcolaCodiceFiscaleButton, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Nome")) throw new Exception("Il campo Nome risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Cognome")) throw new Exception("Il campo Cognome risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Sesso")) throw new Exception("Il campo Sesso risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Data di nascita")) throw new Exception("Il campo Data di nascita risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Comune di nascita")) throw new Exception("Il campo Comune di nascita risulta non essere obbligatorio");
            
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(inputComuneNascita,util.sendKeys,"Milano");
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(confermaComuneNascita, util.scrollAndClick);
			util.objectManager(inputDataNascita,util.scrollAndSendKeys,"04/03/1985" + Keys.RETURN);
			util.objectManager(inputNome,util.scrollAndSendKeys,"MARIO");
			util.objectManager(inputCognome,util.scrollAndSendKeys,"RASI");
			util.objectManager(inputSesso, util.scrollAndSelect,"M");
			util.objectManager(conferma, util.scrollAndClick);
			if(!checkAlertMessage("Errore: Il campo codice fiscale è obbligatorio")) throw new Exception("Il campo Codice Fiscale risulta non essere obbligatorio");
			util.objectManager(closeBar, util.scrollAndClick);
			driver.switchTo().defaultContent();
		
		}
		
		
	}
	
	public void verificaObbligatorietaCampiContatti() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");
		util.objectManager(conferma, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Almeno un contatto telefonico è obbligatorio")) throw new Exception("Durante la creazione di un nuovo Referente senza inserire alcun contatto il sistema non restituisce il messaggio di errore : Almeno un contatto telefonico e' obbligatorio");
		 
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(cellulareField, util.sendKeys,"3958854741");
			util.objectManager(descrizioneCellulareField, util.select,"Privato");

			util.objectManager(inputNome, util.clear);

//			this.confermaCreazione();		
		}
		
		
	}
	
	public void verificaTipologiaReferente(String tipologia) throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		Select s = new Select(util.waitAndGetElement(this.selectTipologiaReferente, true));
		WebElement option = s.getFirstSelectedOption();
		String defaultItem = option.getText();
		if(!defaultItem.trim().contentEquals(tipologia)) throw new Exception("La tipologia Referente presente all'interno del campo Tipologia Referente di default non risulta: "+tipologia+". Valore riscontrato: "+defaultItem);
		driver.switchTo().defaultContent();
	}
	
	
	private void compilaAnagraficaReferenteMOD(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception{
		this.compilaAnagraficaReferenteResidenziale(nome,cognome,sesso,dataNascita,comune);
	}
	
//	private void compilaAnagraficaReferenteConRuolo(String nome, String cognome, String sesso, String dataNascita, String comune,String ruolo) throws Exception{
	public void compilaAnagraficaReferenteConRuolo(Properties prop) throws Exception{
		util.frameManager(this.frameName, selectRuoloReferente2,util.select,prop.getProperty("RUOLO"));
	}
	
	private void compilaAnagraficaReferenteConRuoloMOD(String nome, String cognome, String sesso, String dataNascita, String comune,String ruolo) throws Exception{
		util.frameManager(this.frameName, selectRuoloReferente,util.select,ruolo);
		this.compilaAnagraficaReferenteResidenziale(nome,cognome,sesso,dataNascita,comune);
	}

	public void compilaContatti(String frameName,String cellulare,String descrizioneCellulare) throws Exception {
		util.frameManager(frameName, cellulareField, util.sendKeys,cellulare);
		util.frameManager(frameName, descrizioneCellulareField, util.select,descrizioneCellulare);
		
	}
	public void confermaCreazione() throws Exception {
		util.frameManager(this.frameName, conferma, util.scrollAndClick);
	}
	
	public void verificaConfermaCreazione() throws Exception {
		if(!util.exists(verifica, 5)) throw new Exception("La Conferma referente non è andata a buon fine! Verificare.");
	}
	
	public void annullaCreazione() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		util.objectManager(annulla, util.scrollAndClick);
		util.objectManager(continua, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		if(!util.exists(By.xpath("//div[@title='Nuovo' and (text() = 'Nuovo')]"), 5)) throw new Exception("Dopo aver annullato il processo di creazione Referente, il sistema non torna alla schermata Clienti");
//		if(util.exists(nuovoReferenteButton,5)) {}
		driver.switchTo().defaultContent();
	}
	
	
	
	public void vaiADettagliReferente() throws Exception{
		util.objectManager(By.xpath("//a[@data-refid='recordId' and contains(@class,'textUnderline')]"), util.scrollAndClick);
	}
	
	public void verificaCreazione() throws Exception {
		
		try {
			this.driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(10);
			util.waitAndGetElement(tuttiIProcessi,30,1);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Impossibile creare la nuova anagrafica. Verificare i dati inseriti");
		}
	}
	
	public void ripetiReferente() throws Exception {
		
		try {
			this.driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(10);
			util.waitAndGetElement(nuovoReferente,30,1);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Impossibile rientrare nell'anagrafica. Verificare i dati inseriti");
		}
	}
	
	
	public void selezionaReferenteEsistente(String Referente) throws Exception{
		util.frameManager(this.frameName, ReferenteField, util.sendKeys,Referente);
		util.frameManager(frameName, tendina, util.scrollAndClick);
		
	}
	
	
	private void compilaAnagraficaImpresaIndividuale(String piva, String ragioneSociale) throws Exception{
		   util.frameManager(this.frameName, inputPartitaIva, util.sendKeys,piva);
		   util.frameManager(this.frameName, inputRagioneSociale, util.sendKeys,ragioneSociale);
	}
	
	private void compilaAnagraficaCondominio(String cf,String piva, String ragioneSociale) throws Exception{
		util.frameManager(this.frameName, inputAccount, util.sendKeys,cf);
		this.compilaAnagraficaImpresaIndividuale(piva, ragioneSociale);
	}
	
	private void compilaAnagraficaBusiness(String cf, String piva, String ragioneSociale, String tipoFormaGiuridica) throws Exception {
		this.compilaAnagraficaCondominio(cf, piva, ragioneSociale);
		util.frameManager(this.frameName, selectTipoFormaGiuridica, util.select,tipoFormaGiuridica);
	}
	
	public String selezionaTipologiaReferente(By tipologia) throws Exception{
		String frame = util.getFrameByIndex(0);
		this.frameName = frame;
		util.frameManager(frame, tipologia, util.scrollAndClick);
		util.frameManager(frame, confirmButton, util.scrollAndClick);
		return frame;
		
	}
	
	public String selezionaTipologiaReferente(int frameId,By tipologia) throws Exception{
		String frame = util.getFrameByIndex(frameId);
		this.frameName = frame;
		util.frameManager(frame, tipologia, util.scrollAndClick);
		util.frameManager(frame, confirmButton, util.scrollAndClick);
		return frame;
		
	}

	public void selezionaTipologia(By tipologia) throws Exception{
		util.frameManager(frameName, tipologia, util.scrollAndClick);
		util.frameManager(frameName, confirmButton, util.scrollAndClick);	
	}
	public void nuovoReferente_Veloce() throws Exception{
//		TimeUnit.SECONDS.sleep(3);
//		selezionaCliente.click();
//		TimeUnit.SECONDS.sleep(3);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		SceltaProcessoComponent referente = new SceltaProcessoComponent(driver);
		referente.clickReferente();
		TimeUnit.SECONDS.sleep(5);
	}
	
	public void nuovoReferente() throws Exception{
		TimeUnit.SECONDS.sleep(3);
		selezionaCliente.click();
		TimeUnit.SECONDS.sleep(3);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		SceltaProcessoComponent referente = new SceltaProcessoComponent(driver);
		referente.clickReferente();
		TimeUnit.SECONDS.sleep(5);
	}
	public void nuovoReferenteDaInterazioni() throws Exception{
		TimeUnit.SECONDS.sleep(3);
		SeleniumUtilities util = new SeleniumUtilities(driver);
		SceltaProcessoComponent referente = new SceltaProcessoComponent(driver);
		referente.clickReferente();
//		TimeUnit.SECONDS.sleep(5);
	
	}
	
	

	
	public void selezionaRuolo(By el,String ruolo) throws Exception{
		util.frameManager(frameName, el, util.select, ruolo);
	}
	
	public void compilaReferente() throws Exception{
		TimeUnit.SECONDS.sleep(3);
		String frame = util.getFrameByIndex(0);
		this.frameName = frame;
		util.frameManager(frameName, referente,util.sendKeys,"ProvaRef");
		TimeUnit.SECONDS.sleep(5);
	}
	
	
	public void clickbtnLente() throws Exception{
		//TimeUnit.SECONDS.sleep(5);
//		String frame = util.getFrameByIndex(0);
		util.getFrameActive();
//		this.frameName = frame;
//		util.frameManager(frame, btnLente, util.scrollAndClick);
		util.objectManager(btnLente, util.scrollAndClick);
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}

	public void clickbtnNuovo() throws Exception{
		//TimeUnit.SECONDS.sleep(5);
//		String frame = util.getFrameByIndex(0);
//		this.frameName = frame;
		util.getFrameActive();
//		util.frameManager(frame, btnNuovo, util.scrollAndClick);
//		TimeUnit.SECONDS.sleep(5);
		util.objectManager(btnNuovo, util.scrollAndClick);
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}
	
	public void verificaEsistenzaCampiSocialeStudi(String frame) throws Exception {
		//Verifica esistenza campi sezione Social e Web
		if(!util.exists(frame, twitterAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Twitter");
		if(!util.exists(frame, linkedinAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Linkedin");
		if(!util.exists(frame, fbAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Facebook");
		if(!util.exists(frame, webSite, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Sito Web");

		//Verifica esistenza campi sezione Studi e Professione
		if(!util.exists(frame, titoloStudio, 5)) throw new Exception("Nella sezione Studi e Professione impossibile trovare la select Titolo di Studio");
		if(!util.exists(frame, professione, 5)) throw new Exception("Nella sezione Studi e Professione impossibile trovare il campo Professione");

	}
	public void verificaEsistenzaCampiSocialeStudiReferente(String frame) throws Exception {
		//Verifica esistenza campi sezione Social e Web
		if(!util.exists(frame, twitterAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Twitter");
		if(!util.exists(frame, linkedinAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Linkedin");
		if(!util.exists(frame, fbAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Facebook");
		if(!util.exists(frame, webSite, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Sito Web");

		//Verifica esistenza campi sezione Studi e Professione
		if(!util.exists(frame, titoloStudio, 5)) throw new Exception("Nella sezione Studi e Professione impossibile trovare la select Titolo di Studio");
		if(!util.exists(frame, professione, 5)) throw new Exception("Nella sezione Studi e Professione impossibile trovare il campo Professione");

	}
	
}
