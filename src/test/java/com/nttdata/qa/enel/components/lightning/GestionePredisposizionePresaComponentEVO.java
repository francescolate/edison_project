package com.nttdata.qa.enel.components.lightning;

import java.awt.List;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.concurrent.TimeUnit;

import javax.swing.text.Document;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

//Il seguente componente si occupa di effettuare l'inserimento del POD per lo SWA-EVO
//specificando 

public class GestionePredisposizionePresaComponentEVO {
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;

	public GestionePredisposizionePresaComponentEVO(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}

	By insertPOD = By.xpath(
			"//label[text()='POD/PDR']/../div/input|//div[@class='active hasFixedFooter oneWorkspace']//section[@class='tabContent active oneConsoleTab']//input");
	// By selectTipoDocumento = By.xpath("//label[contains(text(),'Tipo
	// documento')]/..//select");
	// By insertRegione = By.xpath("//label[contains(text(),'Numero
	// documento')]/..//input");
	By insertProvincia = By.xpath("//label[contains(text(),'*Provincia')]/..//input");
	By insertComune = By.xpath("//label[contains(text(),'*Comune')]/..//input");
	By insertIndirizzo = By.xpath("//label[contains(text(),'*Indirizzo')]/..//input");
	By insertCivico = By.xpath("//label[contains(text(),'*Numero Civico')]/..//input");

	By insertUso = By.xpath("//label[contains(text(),'Uso')]/..//input");
	By insertResidente = By.xpath("//label[contains(text(),'Residente')]/..//input");
	By insertSocietaVendita = By.xpath("//label[contains(text(),'Attuale società di vendita')]/..//input");
	By insertTitolarita = By.xpath("//label[contains(text(),'Titolarità')]/..//input");
	By consumoAnnuo = By.xpath("//label[contains(text(),'Consumo annuo')]/..//input");
	public By aggiungiFornitura = By.xpath("//button[text()='Aggiungi fornitura']");
    By rimuoviFornitura = By.xpath("//button[text()='Rimuovi fornitura']");
    // GAS
    By matricolaContatore = By.xpath("//label[contains(text(),'Matricola contatore')]/..//input");
    By tipoInstallazione = By.xpath("//label[contains(text(),'Tipo installazione')]/..//input");
  
    By categoriaConsumo = By.xpath("//label[contains(text(),'Categoria di Consumo')]/..//input");
  
    By categoriaMarketing = By.xpath("//label[contains(text(),'Categoria di Marketing')]/..//input");
  
    By ordineGrandezza = By.xpath("//label[contains(text(),'Ordine di Grandezza')]/..//input");
  
    By profiloConsumo = By.xpath("//label[contains(text(),'Profilo di Consumo')]/..//input");
  
    By potenzialita = By.xpath("//label[contains(text(),'Potenzialità')]/..//input");
  
    By inputUtilizzo = By.xpath("//label[contains(text(),'Utilizzo')]/..//input");
    
	public By confermaPodButton = By.xpath("//button[contains(text(),'Conferma')]");

	public By confermaPodButton1 = By.xpath(
			"/html/body/div[4]/div[1]/div[2]/div[2]/div/div[2]/div/section[2]/div/div[2]/div/div/article/div[1]/div[4]/article/div[1]/div/div[1]/div/div/div[1]/article/div[2]/div/button");
	public By confermaDatiSitoButton = By.xpath("//button[text()='Conferma Dati Sito'][1]");
	public By confermaFornitureButton = By
			.xpath("//section[@class='tabContent active oneConsoleTab']//div//button[text()='Conferma Forniture']");
	// public By creaOffertaButton = By.xpath("//section[@class='tabContent active
	// oneConsoleTab']//div//button[text()='Crea offerta']");
	public By creaOffertaButton = By.xpath(
//			"//section[@class='tabContent active oneConsoleTab']//div[@class='oneWorkspaceTabWrapper'] //article[@class='slds-card']//div[@class='slds-col_bump-left slds-size_10-of-12 slds-align-middle']//div//button[text()='Crea offerta']");
//			"//section[@class='tabContent active oneConsoleTab']//div[@class='oneWorkspaceTabWrapper']//article[@class='slds-card']//button[text()='Crea offerta']//div//button[text()='Crea offerta']");
			"//div//button[text()='Crea offerta']");
	// public By okPopupButton =
	// By.xpath("//*[@id='modalButtons-839']/lightning-button/button");
	public By okPopupButton = By.xpath("//button[contains(text(),'OK')]");
	public By modificaButton = By
			.xpath("//button[text()='Modifica Indirizzo']/parent::div[not(contains(@class,'slds-hide'))]");
	
	public By mercatodiprovenienza = By.xpath("//label[text()='Mercato di provenienza']/..//input");
	
	// Nuovi xpath
	public By verificaIndirizzoButton = By.xpath("//button[text()='Verifica'][1]");
	public By confermaIndirizzo = By.xpath("//*[contains(text(),'Indirizzo Esecuzioni Lavori')]/../../../../..//button[@name='Conferma']");
	public By radioCommodity = By.xpath("//*[@class='slds-radio_faux']");
	public By numeroTipoPDRTipo1 = By.xpath("//*[contains(text(),'Numero PDR Tipo 1')]/..//input");
	public By numeroTipoPDRTipo2 = By.xpath("//*[contains(text(),'Numero PDR Tipo 2')]/..//input");
	public By numeroTipoPDRTipo3 = By.xpath("//*[contains(text(),'Numero PDR Tipo 3')]/..//input");
	public By potenzialitaMaxTipoPDRTipo1 = By.xpath("//*[contains(text(),'Potenzialità Massima PDR Tipo 1')]/..//input");
	public By potenzialitaMaxTipoPDRTipo2 = By.xpath("//*[contains(text(),'Potenzialità Massima PDR Tipo 2')]/..//input");
	public By potenzialitaMaxTipoPDRTipo3 = By.xpath("//*[contains(text(),'Potenzialità Massima PDR Tipo 3')]/..//input");
	public By confermaFornitura = By.xpath("//button[contains(text(),'Conferma Fornitura')]");
	public By confermaDatiFornitura = By.xpath("//lightning-button[@data-id='confirmButton']//button");
	public By annullaOrdine = By.xpath("//lightning-button[@data-id='Cancel']//button");
	public By creaOrdine = By.xpath("//lightning-button[@data-id='CreateOrder']//button");
	public By confermaAnnullaOrdine = By.xpath("//button[@name='ConfirmAnnullmentCase']");
//	public By confermaModalitaFirma = By.xpath("//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='main']//button[@name='Conferma']");
	public By confermaModalitaFirma = By.xpath("//span[text()='Modalità firma e Canale Invio']/../../../../..//button[@name='Conferma']");
	
//	public By confermaSplitPayment = By.xpath("//h2[text()='Split Payment']/ancestor::div[@role='main']//button[@name='Conferma']");
	public By confermaSplitPayment = By.xpath("//h2[text()='Split Payment']/../../../../../../../..//button[@name='Conferma']");
//	public By confermaCigCup = By.xpath("//h2[text()='CIG e CUP']/ancestor::div[@role='main']//button[@name='Conferma']");
	public By confermaCigCup = By.xpath("//h2[text()='CIG e CUP']/../../../../../../../..//button[@name='Conferma']");
	
//	public By confermaUsoFornitura = By.xpath("//h2[text()='Selezione Uso Fornitura']/ancestor::div[@role='main']//button[@name='Conferma']");
	public By confermaUsoFornitura = By.xpath("//h2[text()='Selezione Uso Fornitura']/../../../../../../../..//button[@name='Conferma']");
	
//	public By email = By.xpath("//*[contains(@id,'Indirizzo Email-')]/..//input");
	public By email = By.xpath("//*[contains(@id,'Indirizzo-')]/..//input");
//	public By telefono = By.xpath("//*[contains(@id,'Telefono per Distributore')]/..//input");
	public By telefono = By.xpath("//*[contains(text(),'Telefono per Distributore')]/..//input");
	public By inizioValidita = By.xpath("//*[contains(text(),'Inizio Validità')]/..//input");
	public By fineValidita = By.xpath("//*[contains(text(),'Fine Validità')]/..//input");
	public By flag136 = By.xpath("//label[text()='Flag 136']/..//input");
	public By cig = By.xpath("//label[text()='CIG']/..//input");
	public By cup = By.xpath("//label[text()='CUP']/..//input");
//	public By confermaFatturazioneElettronica = By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@role='main']//button[@name='Conferma']");
	public By confermaFatturazioneElettronica = By.xpath("//h2[text()='Fatturazione Elettronica']/../../../../../../../..//button[@name='Conferma']");
	public By alertPotenzaChiudi = By.xpath("//h2[text()='Errore']/ancestor::div[@role='main']//button[@name='powerNewValue']");
	public By alertPotenzaNulla = By.xpath("//h2[text()='Errore']/ancestor::div[@role='main']//*[text()='Attenzione: la potenza non può essere nulla']");
// Nuovi xPath per Ubiest
	public By inputProvincia=By.xpath("//label[contains(text(),'Provincia')]/../input");
	public By inputCap=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//label[contains(text(),'CAP')]/../input");
	public By inputCapEsecuzioneLavori = By.xpath("//label[contains(text(),'CAP')]/../input");

	public By inputCapRes=By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[text()='CAP']/../input");
    public By capSpanRes=By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']");
	
	public By indirizzoForzatoButtonEsecuzioneLavori = By.xpath("//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']");

	public By indirizzoForzatoButtonRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"); 
	public By forzaIndirizzoButton = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
    public By forzaIndirizzoButtonRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
	public By forzaIndirizzoEsecuzioneLavoriButton = By.xpath("//button[text()='Forza Indirizzo']");
	public By indirizzoNonVerificato = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//*[@id='spanFocusX']");
	public By buttonIndirizzoNonForzato = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//div[@class='slds-visible slds-p-around--small']//span[text()='Indirizzo Non forzato']/ancestor::span");
	public By inputCap2=By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/ancestor::div/input");
	public By indirizzoForzatoButton = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"); 
//	public By selezionaCap = By.xpath("//*[@id='DivmodalModifyAddressABC']//*[contains(text(),'ROMA')]"); 
//	String nomeCitta="NAPOLI";
//	public By selezionaCap = By.xpath("//*[@id='DivmodalModifyAddressABC']//*[contains(text(),'"+nomeCitta+"')]");
	public By forzaIndirizzoButtonInserimentoFornitura = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
	public By nomeComune = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'*Comune')]/..//input"); 
	public By inputCapLavori = By.xpath("//h2//span[text()='Indirizzo Esecuzioni Lavori']/../../../../..//label[contains(text(),'CAP')]/../input");
	public By nomeComuneLavori = By.xpath("//h2//span[text()='Indirizzo Esecuzioni Lavori']/ancestor::div[@role='region']//label[contains(text(),'*Comune')]/..//input"); 
	public By forzaIndirizzoButtonInserimentoLavori = By.xpath("//h2//span[text()='Indirizzo Esecuzioni Lavori']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
	public By buttonIndirizzoNonForzatoLavori = By.xpath("//h2//span[text()='Indirizzo Esecuzioni Lavori']/ancestor::div[@role='region']//div[@class='slds-visible slds-p-around--small']//span[text()='Indirizzo Non forzato']/ancestor::span");
	public By capSpan=By.xpath("//label[contains(text(),'CAP')]/../following::li[@data-record='0']");
	public By capSpanModFirma=By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']");
	// Gestione ISTAT
	public By codiceIstat=By.xpath("//div[@class='slds-modal__container']//tr[1]//label/span[@class='slds-radio_faux']");
	//  public String codice_istat_by_name = "//div[@id='prompt-message-wrapper']//div[@data-label='##']/label";
	public String codice_istat_by_name = "//div[@id='modalIstatBody']//div[@data-label='##']/label";
	public By tabellaSelezioneIstat=By.xpath("//h2[text()='Selezione Istat']");
	public By buttonConfermaIstat=By.xpath("//button[@name='Conferma Istat']");
	public By radioIstat=By.xpath("//*[@class='cITA_IFM_LCP630_DatagridBody']//td//lightning-input");
	
	 
	
	public void inserisciIndirizzo(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {

		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
			// logger.info("Ho trovato il campo *Provincia e inserisco il valore " +
			// Provincia);
		
			util.objectManager(insertComune, util.scrollAndSendKeysAndTab,Comune);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(insertCivico, util.scrollAndSendKeys,Civico);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(insertProvincia, util.scrollAndSendKeysAndTab,Provincia);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(insertIndirizzo, util.scrollAndSendKeysAndTab,Indirizzo);
			TimeUnit.SECONDS.sleep(5);
		}
	}
		public void inserisciDatiFornitura1(String NumeroPDRTipo1, String Potenzialita) throws Exception {

				util.objectManager(numeroTipoPDRTipo1, util.scrollAndSendKeys,NumeroPDRTipo1);
				TimeUnit.SECONDS.sleep(5);
				util.objectManager(potenzialitaMaxTipoPDRTipo1, util.scrollAndSendKeys,Potenzialita);
				TimeUnit.SECONDS.sleep(5);
				
			}

		public void inserisciDatiFornitura2(String NumeroPDRTipo2, String Potenzialita) throws Exception {

			util.objectManager(numeroTipoPDRTipo2, util.scrollAndSendKeys,NumeroPDRTipo2);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(potenzialitaMaxTipoPDRTipo2, util.scrollAndSendKeys,Potenzialita);
			TimeUnit.SECONDS.sleep(5);
			
		}

		public void inserisciDatiFornitura3(String NumeroPDRTipo3, String Potenzialita) throws Exception {

			util.objectManager(numeroTipoPDRTipo3, util.scrollAndSendKeys,NumeroPDRTipo3);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(potenzialitaMaxTipoPDRTipo3, util.scrollAndSendKeys,Potenzialita);
			TimeUnit.SECONDS.sleep(5);
			
		}

		public void FatturaAnticipata(String Valore) throws Exception{
			util.selectLighningValue("Emissione Fattura Anticipata",Valore);
		}

		
		public void inserisciTelefono(By telefono) throws Exception {

			WebElement TxtBoxContent = driver.findElement(telefono);
			String tel = TxtBoxContent.getAttribute("value");
			
			if(tel.isEmpty()) {
				util.objectManager(telefono, util.scrollAndSendKeys,"3345655567");
			}

			TimeUnit.SECONDS.sleep(5);
			
		}
		
		public void press(By element) throws Exception{
			util.objectManager(element, util.scrollAndClick);
			spinnerManager.checkSpinners();
		}
	
	
		public void forzaIndirizzoPredisposizionePresaLavori(String Cap, By element) throws Exception{
			WebElement prov = util.waitAndGetElement(inputProvincia,true);
			if (prov.isEnabled()) {
				// Salva il CAP
				WebElement element1 = util.waitAndGetElement(inputCapLavori,true);
				String leggiCap = element1.getAttribute("value");
				// Salva il nome del Comune
				element1 = util.waitAndGetElement(nomeComuneLavori,true);
				String nomeCitta = element1.getAttribute("value");
				
				//Click sullo Switch indirizzo non forzato
				util.objectManager(buttonIndirizzoNonForzatoLavori, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(3);

				//Inserimento CAP
				if(leggiCap.isEmpty()) {
					util.objectManager(inputCapLavori, util.scrollAndSendKeys, Cap);
				} else {
					util.objectManager(inputCapLavori, util.scrollAndSendKeys, leggiCap);
				}
				TimeUnit.SECONDS.sleep(5);
				//			util.objectManager(By.xpath("//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);
//				util.objectManager(capSpan, util.scrollAndClick);

				pressJavascript(capSpan);

				//Forza indirizzo
				util.objectManager(forzaIndirizzoButtonInserimentoLavori, util.scrollAndClick);
				spinnerManager.checkSpinners();
//				// *** E' apparsa nuova schermata Seleziona ISTAT ***
//				WebElement istat = util.waitAndGetElement(codiceIstat,true);
//				if (istat.isDisplayed()) {
//					util.objectManager(radioIstat, util.scrollAndClick);
//					TimeUnit.SECONDS.sleep(5);
//					util.objectManager(buttonConfermaIstat, util.scrollAndClick);
//				}
			} else {
				util.waitAndGetElement(modificaButton, 20, 1); // verifica se esiste il bottone "Modifica Indirizzo"
			}

						
//			util.objectManager(element, util.scrollAndClick);
//			spinnerManager.checkSpinners();
		}
	
	
	public void inserisciPOD(String frame, String POD) throws Exception {
		TimeUnit.SECONDS.sleep(7);
		util.frameManager(frame, insertPOD, util.sendKeys, POD);
	}
	
	
	public void verificaPulsantiFornituraDisabilitati() throws Exception {
		util.checkElementDisabled(aggiungiFornitura);
		util.checkElementDisabled(rimuoviFornitura);
	}
	
	public void aggiungiFornitura() throws Exception {
		TimeUnit.SECONDS.sleep(30);
		util.objectManager(aggiungiFornitura, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void inserisciPOD(String POD) throws Exception {
		TimeUnit.SECONDS.sleep(3);
		util.waitAndGetElement(insertPOD).sendKeys(POD);
	}


	

	public void inserisciDettaglioSito(String Uso, String SocietaVendita, String Residente, String Titolarita, String Mercato)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		util.selectLighningValue("Residente", Residente);
		util.selectLighningValue("Titolarità", Titolarita);
		util.checkElementValue(mercatodiprovenienza, Mercato);

	}
	
	public void inserisciDettaglioSitoPod2(String Uso, String Residente, String Potenza, 
			String Tensione, String SocietaVendita, String TipoMisuratore, String Mercato, String Titolarita)
			throws Exception {
		
//		util.selectLighningValue("Uso",Uso);
		util.selectLighningValue("Residente", Residente);
//		util.selectLighningValue("Potenza",Potenza);
//		util.selectLighningValue("Tensione",Tensione);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
//		util.selectLighningValue("Tipo Misuratore", TipoMisuratore);
		util.selectLighningValue("Titolarità", Titolarita);
//		util.selectLighningValue("Mercato di Provenienza", Mercato);

	}
	
	public void inserisciDettaglioSito(String Uso, String Residente, String Potenza, 
			String Tensione, String SocietaVendita, String TipoMisuratore, String Mercato, String Titolarita)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		util.selectLighningValue("Residente", Residente);
//		util.selectLighningValue("Potenza",Potenza);
//		util.selectLighningValue("Tensione",Tensione);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(6);
	
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
		util.selectLighningValue("Titolarità", Titolarita);
//		util.selectLighningValue("Mercato di Provenienza", Mercato);
//		util.selectLighningValue("Tipo Misuratore", TipoMisuratore);

	}
	
	public void inserisciDettaglioSito(String SocietaVendita, String Residente, String Titolarita, String Mercato)
			throws Exception {
		
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(6);
		
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
		util.selectLighningValue("Residente", Residente);
		util.selectLighningValue("Titolarità", Titolarita);
		util.checkElementValue(mercatodiprovenienza, Mercato);

	}
	
	public void inserisciDettaglioSito2(String Uso, String SocietaVendita, String Titolarita, String Mercato)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(6);
		
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
		util.selectLighningValue("Titolarità", Titolarita);

	}
	
	
	public void inserisciDettaglioSito2(String SocietaVendita, String Titolarita,String Mercato)
			throws Exception {
		
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(6);
		
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
		util.selectLighningValue("Titolarità", Titolarita);

	}
	
	public void inserisciDettaglioSitoNonResidenziale(String Uso, String SocietaVendita, String Mercato, String Categoria, String Potenza, String Tensione, String Misuratore,String Consumo)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(5);
	
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);

	/*	Robot r = new Robot();
		
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
	*/	
	//	Actions act = new Actions(driver);
	//	act.sendKeys(driver.findElement(insertSocietaVendita), Keys.ARROW_DOWN).sendKeys(driver.findElement(insertSocietaVendita),Keys.ENTER).build().perform();
		util.selectLighningValue("Categoria Merceologica", Categoria);
		util.selectListOptionValue("Potenza Contrattuale", Potenza);
		util.selectLighningValue("Tensione disponibile", Tensione);
		util.selectLighningValue("Tipo misuratore", Misuratore);
		util.checkElementValue(mercatodiprovenienza, Mercato);
		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);

	}
	

	public void inserisciDettaglioSitoNonResidenzialePod2(String Uso, String SocietaVendita, String Mercato, String Categoria, String Potenza, String Tensione, String Misuratore,String Consumo)
			throws Exception {
		
//		util.selectLighningValue("Uso",Uso);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.selectLighningValue("Categoria Merceologica", Categoria);
		util.selectListOptionValue("Potenza Contrattuale", Potenza);
		util.selectLighningValue("Tensione disponibile", Tensione);
		util.selectLighningValue("Tipo misuratore", Misuratore);
		util.checkElementValue(mercatodiprovenienza, Mercato);
		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);

	}
	public void inserisciDettaglioSitoNonResidenzialeGas(String Uso, String SocietaVendita, String Mercato, 
			String Matricola, String TipoInstallazione, String CatConsumo, String CatMarketing,String OrdineGrandezza,
			String Profilo,String Potenzialita,String Utilizzo,String Consumo)
			throws Exception {

		//label[text()='Uso']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Tipo installazione']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Categoria di Consumo']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Categoria di Marketing']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Ordine di Grandezza']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Profilo di Consumo']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Potenzialità']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Utilizzo']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		
		
		//label[text()='Uso']/parent::lightning-combobox//span[text()='Uso Diverso da Abitazione']]
		util.selectLighningValue("Uso",Uso);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectLighningValue("Tipo installazione",TipoInstallazione);
// con Agriturismo funziona mentre Industria non lo setta
		util.selectLighningValue("Categoria di Consumo",CatConsumo);
		util.selectLighningValue("Categoria di Marketing",CatMarketing);
		util.selectLighningValue("Ordine di Grandezza",OrdineGrandezza);
		util.selectLighningValue("Profilo di Consumo",Profilo);
		util.selectLighningValue("Potenzialità",Potenzialita);
		util.selectLighningValue("Utilizzo",Utilizzo);
		//Aggiunto da Capuano Tommaso in data 12/05/2020
//		util.waitAndGetElement(matricolaContatore).sendKeys(Matricola);
		String matricola=util.randomNumeric(10);
		util.waitAndGetElement(matricolaContatore).sendKeys(matricola);
		
//		util.objectManager(consumoAnnuo, util.scrollAndSendKeys,Consumo);
//		util.objectManager(matricolaContatore, util.scrollAndSendKeys,Matricola);
		
//		TimeUnit.SECONDS.sleep(3);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);
//		util.waitAndGetElement(categoriaConsumo).sendKeys(CatConsumo);
//		util.waitAndGetElement(categoriaMarketing).sendKeys(CatMarketing);
//		util.waitAndGetElement(ordineGrandezza).sendKeys(OrdineGrandezza);
//		util.waitAndGetElement(profiloConsumo).sendKeys(Profilo);
//		util.waitAndGetElement(potenzialita).sendKeys(Potenzialita);
//		util.waitAndGetElement(inputUtilizzo).sendKeys(Utilizzo);
	
	}
	
	public void inserisciDettaglioSitoNonResidenzialeGas2(String SocietaVendita, String Mercato, 
			String Matricola, String TipoInstallazione, String CatConsumo, String CatMarketing,String OrdineGrandezza,
			String Profilo,String Potenzialita,String Utilizzo,String Consumo)
			throws Exception {

		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectLighningValue("Tipo installazione",TipoInstallazione);
// con Agriturismo funziona mentre Industria non lo setta
		util.selectLighningValue("Categoria di Consumo",CatConsumo);
		util.selectLighningValue("Categoria di Marketing",CatMarketing);
		util.selectLighningValue("Ordine di Grandezza",OrdineGrandezza);
		util.selectLighningValue("Profilo di Consumo",Profilo);
		util.selectLighningValue("Potenzialità",Potenzialita);
		util.selectLighningValue("Utilizzo",Utilizzo);
		//Aggiunto da Capuano Tommaso in data 12/05/2020
//		util.waitAndGetElement(matricolaContatore).sendKeys(Matricola);
		String matricola=util.randomNumeric(10);
		util.waitAndGetElement(matricolaContatore).sendKeys(matricola);
		
//		util.objectManager(consumoAnnuo, util.scrollAndSendKeys,Consumo);
//		util.objectManager(matricolaContatore, util.scrollAndSendKeys,Matricola);
		
//		TimeUnit.SECONDS.sleep(3);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);
//		util.waitAndGetElement(categoriaConsumo).sendKeys(CatConsumo);
//		util.waitAndGetElement(categoriaMarketing).sendKeys(CatMarketing);
//		util.waitAndGetElement(ordineGrandezza).sendKeys(OrdineGrandezza);
//		util.waitAndGetElement(profiloConsumo).sendKeys(Profilo);
//		util.waitAndGetElement(potenzialita).sendKeys(Potenzialita);
//		util.waitAndGetElement(inputUtilizzo).sendKeys(Utilizzo);
	
	}
	public void inserisciDettaglioSitoNonResidenzialeGas(String SocietaVendita,
			String Matricola, String TipoInstallazione, String CatConsumo, String CatMarketing,String OrdineGrandezza,
			String Profilo,String Potenzialita,String Utilizzo,String Consumo,String Mercato)
			throws Exception {
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		TimeUnit.SECONDS.sleep(5);
		
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectLighningValue("Tipo installazione",TipoInstallazione);
		util.selectLighningValue("Categoria di Consumo",CatConsumo);
		util.selectLighningValue("Categoria di Marketing",CatMarketing);
		util.selectLighningValue("Ordine di Grandezza",OrdineGrandezza);
		util.selectLighningValue("Profilo di Consumo",Profilo);
		util.selectLighningValue("Potenzialità",Potenzialita);
		util.selectLighningValue("Utilizzo",Utilizzo);
		//Aggiunto da Capuano Tommaso in data 12/05/2020
		String matricola=util.randomNumeric(10);
		util.waitAndGetElement(matricolaContatore).sendKeys(matricola);
	
	}

	public void confermaDatiSito(By confermaDatiSito) throws Exception {
		util.objectManager(confermaDatiSito, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}
	
	
	
	public void verificaTabellaFornitureInserite(String commodity,String pod, String indirizzo, String istat, String off) throws Exception{
		
		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[2]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[4]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement podIstat = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[5]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[6]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
        
		if(!tipoCommodity.getText().contentEquals(commodity)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il tipo Commodity risulta diverso da "+commodity);
		if(!numeroFornitura.getText().contentEquals(pod)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il pod risulta diverso da "+pod+". Numero fornitura trovato : "+numeroFornitura.getText());
	    if(!indirizzofornitura.getText().contains(indirizzo)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite l'indirizzo risulta diverso da "+indirizzo);
		if(!podIstat.getText().contentEquals(istat)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di POD ISTAT risulta diverso da "+istat);
		if(!offertabilita.getText().contentEquals(off)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+off);
	}
	
	public void verificaTabellaFornitureInseriteGas(String commodity,String pod, String indirizzo, String istat, String off) throws Exception{
		
		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[2]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[4]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[6]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
        
		if(!tipoCommodity.getText().contentEquals(commodity)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il tipo Commodity risulta diverso da "+commodity);
		if(!numeroFornitura.getText().contentEquals(pod)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il pod risulta diverso da "+pod+". Numero fornitura trovato : "+numeroFornitura.getText());
	    if(!indirizzofornitura.getText().contains(indirizzo)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite l'indirizzo risulta diverso da "+indirizzo);
		if(!offertabilita.getText().contentEquals(off)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+off);
	}
	
//	public void verificaTabellaFornitureInseriteMulti(String commodity,String pod, String indirizzo, String istat, String off) throws Exception{
//		
//		try {
//			util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		} catch (Exception e) {
//			throw new Exception("Il pod:"+pod+" non esiste nella tabella delle forniture");
//		}
//		System.out.println("POD attuale:"+pod);
//		
//		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[2]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
////		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[4]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[6]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//        
//		if(!tipoCommodity.getText().contentEquals(commodity)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il tipo Commodity risulta diverso da "+commodity);
////		if(!numeroFornitura.getText().contentEquals(pod)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il pod risulta diverso da "+pod+". Numero fornitura trovato : "+numeroFornitura.getText());
//	    if(!indirizzofornitura.getText().contains(indirizzo)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite l'indirizzo risulta diverso da "+indirizzo);
//		if(!offertabilita.getText().contentEquals(off)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+off);
//	}

	public void confermaPod(By confermaPod) throws Exception {
		util.objectManager(confermaPod, util.scrollAndClick);
		//util.waitAndGetElement(confermaPod).click();
		TimeUnit.SECONDS.sleep(10);
		spinnerManager.checkSpinners();
	}

	public void verificaIndirizzo(String Cap, By verifica) throws Exception {
		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
			util.objectManager(verifica,util.scrollAndClick);
			spinnerManager.checkSpinners();
		}

		try {
//			util.waitAndGetElement(modificaButton, 20, 1);
			forzaIndirizzoPredisposizionePresaLavori(Cap, verifica);
		} catch (Exception e) {
			throw new Exception("L'indirizzo di fornitura non è stato forzato/validato");
		}
	}

	public void confermaForniture(By conferma) throws Exception {
		util.objectManager(conferma, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void creaOfferta(By crea) throws Exception {
		util.objectManager(crea,util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void okPopup(By ok) throws Exception {
		util.waitAndGetElement(ok).click();
		spinnerManager.checkSpinners();
	}

	public void compilaCampiCigCup(String valore, String Cig, String Cup) throws Exception{
		if(valore.equals("NO")) {
			util.selectLighningValue("Flag 136", valore);
		} else {
			// valorizzare i campi CIG e CUP
			util.objectManager(cig, util.scrollAndSendKeys,Cig);
			util.objectManager(cup, util.scrollAndSendKeys,Cup);
		}
		this.press(confermaCigCup);
	}

	public void compilaFatturazioneElettronica(String valore) throws Exception{
		util.selectLighningValue("Tipologia Pubblica Amministrazione", valore);
//		if(valore.equals("SI")) {
//			util.objectManager(inizioValidita, util.scrollAndSendKeys,data);
//			util.objectManager(fineValidita, util.scrollAndSendKeys,"31/12/2999");
//		}
		this.press(confermaFatturazioneElettronica);
	}
	public void compilaSplitPayment(String valore, String data) throws Exception{
		util.selectLighningValue("Split Payment", valore);
		if(valore.equals("SI")) {
			util.objectManager(inizioValidita, util.scrollAndSendKeys,data);
			util.objectManager(fineValidita, util.scrollAndSendKeys,"31/12/2999");
		}
		this.press(confermaSplitPayment);
	}

	public void compilaPotenzaRichiesta(String valore) throws Exception{
		if(valore.equals("0")) {
			util.selectLighningValueAlert("Potenza Richiesta", "Tensione Richiesta");
			if(util.exists(alertPotenzaNulla, 5)) 
				press(alertPotenzaChiudi);
				System.out.println("Alert Potenza Richiesta - OK");
		} else {
			util.selectLighningValue("Potenza Richiesta", valore);
			spinnerManager.checkSpinners();
		}
	}


	public void compilaTensioneRichiesta(String valore) throws Exception{
		util.selectLighningValue("Tensione Richiesta", valore);
		spinnerManager.checkSpinners();
	}

	public void compilaCampoUsoFornitura(String uso) throws Exception{
		util.selectLighningValue("Uso Fornitura", uso);
		this.press(confermaUsoFornitura);
	}

	public void compilaCampiModalitaFirma(String canale) throws Exception{
		util.selectLighningValue("Canale Invio", canale);
		if (canale.equals("Email")) {
			util.objectManager(email, util.scrollAndSendKeys,"testing.crm.automation@gmail.com");
		}
		// Break per problema indirizzo Ubiest
		this.press(confermaModalitaFirma);
	}
	
	public void compilaCampiModalitaFirma(String canale, String modalitafirma, String emailTP8) throws Exception{
		util.selectLighningValue("Canale Invio", canale);
		util.objectManager(email, util.scrollAndSendKeys, emailTP8);
		util.selectLighningValue("Modalità Firma", modalitafirma);
		this.press(confermaModalitaFirma);
	}

	public void compilaCampiModalitaFirmaIndirizzoForzato(String Cap, String canale) throws Exception {

		util.selectLighningValue("Canale Invio", canale);
		if (canale.equals("EMAIL")) {
			util.objectManager(email, util.scrollAndSendKeys,"testing.crm.automation@gmail.com");
		}
		// Salva il CAP
		WebElement element = util.waitAndGetElement(inputCap2,true);
		String leggiCap = element.getAttribute("value");
		// Salva il nome del Comune
		element = util.waitAndGetElement(nomeComune,true);
		String nomeCitta = element.getAttribute("value");
		// Gestione problema indirizzo Ubiest con Forzatura Indirizzo
		WebElement indNonVerificato = util.waitAndGetElement(indirizzoNonVerificato,true);
		if (indNonVerificato.isDisplayed()) {
			//Click sullo Switch indirizzo non forzato
			util.objectManager(buttonIndirizzoNonForzato, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);
			if(leggiCap.isEmpty()) {
				util.objectManager(inputCap2, util.scrollAndSendKeys, Cap);
			} else {
				util.objectManager(inputCap2, util.scrollAndSendKeys, leggiCap);
			}
			//			util.objectManager(By.xpath("//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);
//			util.objectManager(capSpan, util.scrollAndSendKeys, leggiCap);
			TimeUnit.SECONDS.sleep(3);
//			util.objectManager(selezionaCap, util.scrollAndClick);
			pressJavascript(capSpanModFirma);
			TimeUnit.SECONDS.sleep(3);
			//Forza indirizzo
			util.objectManager(forzaIndirizzoButtonInserimentoFornitura, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(5);

		}
		// Bottone Conferma
		this.press(confermaModalitaFirma);

	}
	public void pressJavascript(By oggetto) throws Exception{
		WebElement element = util.waitAndGetElement(oggetto);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}

}
