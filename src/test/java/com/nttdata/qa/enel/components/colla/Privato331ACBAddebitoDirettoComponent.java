package com.nttdata.qa.enel.components.colla;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.lightning.LoginSalesforceComponent;
import com.nttdata.qa.enel.components.lightning.WorkbenchComponent;
import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;


public class Privato331ACBAddebitoDirettoComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	public static String currentHandle = "";
	public static String ServiceRequestId = "";
	
	public By areaRiservata = By.xpath("//ol[@class='breadcrumb']");
	public By pageHeading = By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
	
	public By sectionSubheading = By.xpath("//h2[contains(text(),'Vantaggi per il tuo business')]");
	
	public By fornitureSelect = By.xpath("//li/a[text()='forniture']");
	public By fornitureHeading = By.xpath("//h1[text()='Tutte le tue forniture']");
	public By mostraFiltri = By.xpath("//a[@name='Filtri']");
	public By inputNumeroCliente = By.xpath("//label[text()='Numero cliente']/parent::div//div[@class='customselect-container']/a[@class='customselect-choice']");
	public By selectNumeroCliente_310512806 = By.xpath("//ul/li//span[text()='310512806']");
	public By selectNumeroCliente_310514464 = By.xpath("//ul/li//span[text()='310514464']");
	public By applicaButton = By.xpath("//button[text()='Applica']");
	public By Vai_al_dettaglio_LINK = By.xpath("//tr[@class='fornItem']/td//a[text()='VAI AL DETTAGLIO']");
	
	public By serviziSelect = By.xpath("//li/a[text()='servizi']");
	public By serviziHeading = By.xpath("//h1[text()='Servizi']");
	public By serviziPageSubText = By.xpath("//h4[text()='In questa pagina trovi tutti i servizi.']");
	public By AddebitoDiretto_button = By.xpath("//p[contains(text(),'Addebito Diretto')]/parent::div");
	
	public By AddebitoDiretto_Link = By.xpath("//h2[contains(text(),'Vantaggi per il tuo business')]/parent::div/following-sibling::div//h3[text()='Addebito Diretto']");
	public By AddebitoDirettoPath = By.xpath("(//ol[@class='breadcrumb'])[2]");
	public By AddebitoDiretto_PageTitle = By.xpath("//h1[@class='title']");
	public By AddebitoDiretto_WebTile = By.xpath("//p[contains(text(),'Addebito Diretto')]/parent::div");
	
	public By AddebitoDiretto_SubText = By.xpath("//h1[@class='title']/following-sibling::p");
	public By AddebitoDiretto_Status = By.xpath("//*[text()='non è attivo']/parent::p");
	public By AttivaAddebitoDiretto_Supply = By.xpath("//div[@id='mmpNonAttivo']//div[@class='row']");
	public By AttivaAddebitoDiretto_button = By.xpath("//div/a[@title='Attiva']");
	
//	public By AttivazioneAddebitoDiretto_Subtext = By.xpath("//p[text()='Compila i campi sottostanti con i tuoi dati e poi prosegui']");
	public By AttivazioneAddebitoDiretto_Subtext = By.xpath("//p[text()='Il servizio automatico che paga le tue bollette.']");
	public By AttivazioneAddebitoDiretto_Subtext2 = By.xpath("//p[text()='Verifica i campi sottostanti e clicca su conferma']");
	public By AttivazioneAddebitoDiretto_ConfirmSupply = By.xpath("(//div[@class='row']//table)[2]");
	
	public By AttivazioneAddebitoDiretto_Num1 = By.xpath("(//span[@class='num'])[1]");
	public By AttivazioneAddebitoDiretto_Num2 = By.xpath("(//span[@class='num'])[2]");
	public By AttivazioneAddebitoDiretto_Num3 = By.xpath("(//span[@class='num'])[3]");
	public By AttivazioneAddebitoDiretto_Num4 = By.xpath("(//span[@class='num'])[4]");
	
	public By checos = By.xpath("//div[@class='title']//h2");
	public By checosContent = By.xpath("//div[@class='title']/following-sibling::div/p");
	
	public By SupplyInfo = By.xpath("//h5[text()='Ecco le forniture su cui modifichermo Addebito Diretto']");
	public By EmailInfo = By.xpath("//p[text()='Inserisci la tua mail per ricevere aggiornamenti sulla richiesta.']");
	public By EmailInputField = By.xpath("//label[contains(text(),'Email')]/following-sibling::input");
	
	public By Cognome_Ragione_sociale_Intestatario_C_C_FieldValue = By.xpath("//label[contains(text(),'Cognome/Ragione sociale Intestatario C/C')]/following-sibling::input");
	public By Nome_Intestatario_C_C_FieldValue = By.xpath("//label[contains(text(),'Nome Intestatario C/C')]/following-sibling::input");
	public By Codice_fiscale_Avente_poteri_di_firma_FieldValue = By.xpath("//label[contains(text(),'Codice fiscale (Avente poteri di firma)')]/following-sibling::input");
	public By IBAN_FieldValue = By.xpath("//label[contains(text(),'IBAN')]/following-sibling::input");
	public By IBAN_ESTERO_Checkbox = By.xpath("//span[contains(text(),'IBAN ESTERO')]/following-sibling::input");
	public By Agenzia_FieldValue = By.xpath("//label[contains(text(),'Agenzia')]/following-sibling::input");
	public By Nome_banca_FieldValue = By.xpath("//label[contains(text(),'Nome banca')]/following-sibling::input");
	
	public By RequestInfo = By.xpath("//p[contains(text(),'La tua richiesta')]");
	public By AttentionInfo = By.xpath("//p[contains(text(),'Attenzione, il servizio')]");
	
	public By PROSEGUIButton = By.xpath("//span[text()='PROSEGUI']/parent::button");
	public By CONFERMAButton = By.xpath("//span[text()='CONFERMA']/parent::button");
	public By FINEButton = By.xpath("//a[text()='FINE']");
	
	public By searchBar = By.xpath("//div[contains(@class, 'forceSearchInputDesktopPillWrapper')]/following-sibling::div//input");
	public By autoSuggestSelectPod_02660026599153 = By.xpath("//li//div[@class='slds-icon_container']/following-sibling::span[contains(text(),'02660026599153')]");
	public By selectPod_IT002E5928147A = By.xpath("//th[@scope='row']//a[text()='IT002E5928147A']");
	public By selectPod_IT002E3401567A = By.xpath("//span[text()='IT002E3401567A']/ancestor::tr//th//a");
	
	public By Service_Requests_Link = By.xpath("//span[contains(text(),'+')]/preceding-sibling::span[@title='Service Requests']");
//	public By Service_Requests_Link2 = By.xpath("(//span[@title='Service Requests'])[3]");
	public By Service_Requests_RecordID = By.xpath("//li//span[text()='Record ID']");
	public By service_Request_ID_Heading = By.xpath("//span[text()='ID del record']");
	public By service_Request_ID_Value = By.xpath("//p[@class='slds-p-horizontal_small']");
	
	public By Elementi_Operazione_Link = By.xpath("//span[contains(text(),'+')]/preceding-sibling::span[@title='Elementi Operazione']");
//	public By Elementi_Operazione_Link2 = By.xpath("(//span[text()='Elementi Operazione'])[3]");
	
	public By Processo_FieldValue = By.xpath("//span[text()='Processo']/parent::div/following-sibling::div/span//lightning-formatted-text");
	public By Stato_FieldValue = By.xpath("//span[text()='Stato']/parent::div/following-sibling::div/span//lightning-formatted-text");
	
	public By Metodo_di_pagamento_SectionHeading = By.xpath("//span[text()='Metodo di pagamento']");
	
	public By Nuovo_Metodo_di_pagamento_FieldValue = By.xpath("//span[text()='Nuovo Metodo di pagamento']/parent::div/following-sibling::div/span//lightning-formatted-text");
	public By Nuovo_titolare_del_conto_bancario_FieldValue = By.xpath("//span[text()='Nuovo titolare del conto bancario']/parent::div/following-sibling::div/span//lightning-formatted-text");
	public By CF_del_nuovo_titolare_del_conto_bancario_FieldValue = By.xpath("//span[text()='CF del nuovo titolare del conto bancario']/parent::div/following-sibling::div/span//lightning-formatted-text");
	public By Nuovo_IBAN_FieldValue = By.xpath("//span[text()='Nuovo IBAN']/parent::div/following-sibling::div/span//lightning-formatted-text");
	public By idBpmVALUE = By.xpath("//span[text()='ID BPM']/parent::div/following-sibling::div//lightning-formatted-text");
	public By idBpmVALUE_POD2 = By.xpath("(//span[text()='ID BPM']/parent::div/following-sibling::div//lightning-formatted-text)[2]");
	
	public By AnnulmentButton = By.xpath("//button[text()='Annulment']");
	
	public By AnnulmentDialogTitle = By.xpath("//h2[text()='Annullamento Richiesta']");
	public By annullamentoDropdown = By.xpath("//h2[contains(text(),'Annullamento Richiesta')]/following::select");  //Rinuncia cliente
	public By annulla = By.xpath("//button[text()='Annulla']");
	public By conferma = By.xpath("//button[@id='btnConferma2']");
	public By close = By.xpath("//button[@title='Chiudi questa finestra']/lightning-primitive-icon");
	public By AnnulmentDialogTitle_annullaButton = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Annulla']");
	
	public By switchToLightening = By.xpath("//a[@class='switch-to-lightning']");
	
	// UDB
	public By idBpm = By.xpath("//input[@name='idBpm']");
	public By esito = By.xpath("//select[@name='esito']");
	public By submit = By.xpath("//input[@type='submit']");
	public By idBpmStatus = By.xpath("//label[contains(text(),'Risposta del server:')]");
	
	// WB
	private final By selectEnvironment = By.id("oauth_env");
	public final By checkAgree = By.id("termsAccepted");
	public final By buttonLogin = By.id("loginBtn");
	
	public By data = By.xpath("//a/span[text()='data']");
	public By update = By.xpath("//li/a[text()='Update']");
	public By objectType = By.xpath("//select[@id='default_object']");
	public By singleRecord = By.xpath("//input[@id='sourceType_singleRecord']");
	public By singleRecordInput = By.xpath("//input[contains(@onfocus,'sourceType_singleRecord')]");
	public By nextButton = By.xpath("//input[@value='Next']");
	
	public By ITA_IFM_OperationStatus__c = By.xpath("//input[@name='ITA_IFM_OperationStatus__c']");
	public By ITA_IFM_Status__c = By.xpath("//input[@name='ITA_IFM_Status__c']");
	public By ITA_IFM_CancelStatus__c = By.xpath("//input[@name='ITA_IFM_CancelStatus__c']");
	public By ConfirmUpdateButton = By.xpath("//input[@value='Confirm Update']");

	public By downloadResult = By.xpath("//input[@value='Download Full Results']");
	
	ColorUtils c = new ColorUtils();
	
	public Privato331ACBAddebitoDirettoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	
	public void click_Service_Requests_Link(By clickableObject,String Value) throws Exception {
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,700)");
		Thread.sleep(3000);
		verifyComponentExistence(clickableObject);
		VerifyText(clickableObject, Value);
		
		WebElement button=driver.findElement(clickableObject);
		js.executeScript("arguments[0].click();", button);		
	}
	
	public void click_Elementi_Operazione_Link(By clickableObject,String Value) throws Exception {
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,500)");
		Thread.sleep(3000);
		verifyComponentExistence(clickableObject);
		VerifyText(clickableObject, Value);
		
		WebElement button=driver.findElement(clickableObject);
		js.executeScript("arguments[0].click();", button);		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		System.out.println(actualtext);
		System.out.println(Value);
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}
	
	public void udbSubmit(By checkObject)
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 5);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		WebElement element = driver.findElement(checkObject);
		element.submit();
	}
	
	public void ContainsText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		
		if (actualtext.contains(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}
	
	public void Validate_Elementi_Operazione_Section() throws Exception
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
        LocalDateTime now = LocalDateTime.now();  
        String TODAYS_DATE = dtf.format(now);
        System.out.println(TODAYS_DATE);
        
        String item_found = "NO";
        
        int table = driver.findElements(By.xpath("(//table)")).size();				// table size
        
        demo:
        for (int k = 1; k <= table; k++) {                                           
        	
        	if (driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[1]")).getText().contentEquals("Item Name") && driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[2]")).getText().contentEquals("Data creazione")) {
				
        		int row = driver.findElements(By.xpath("(//table)["+k+"]/tbody/tr")).size();     // row size
        		
        		for (int i = 1; i <= row; i++) {                                    
        			
        			if (driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[1]")).isDisplayed() && driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[2]")).isDisplayed()) {
						
        				String TableHeading_Data_creazione = driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[2]")).getText();
        				String Table_Data_creazione_Value = driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[2]")).getText();
        				String TableHeading_Tipo_Operazione = driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[3]")).getText();
        				String Table_Tipo_Operazione_Value = driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[3]")).getText();
        				String TableHeading_Tipo_di_record = driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[4]")).getText();
        				String Table_Tipo_di_record_Value = driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[4]")).getText();
        				String TableHeading_Stato = driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[5]")).getText();
        				String Table_Stato_Value = driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[5]")).getText();
        				
        				
        				if (TableHeading_Data_creazione.contentEquals("Data creazione") && Table_Data_creazione_Value.contains(TODAYS_DATE)) {
        					
        					if (TableHeading_Tipo_Operazione.contentEquals("Tipo Operazione") && Table_Tipo_Operazione_Value.contentEquals("Attivazione RID")) {
        						
        						if (TableHeading_Tipo_di_record.contentEquals("Tipo di record") && Table_Tipo_di_record_Value.contentEquals("Gestione RID")) {
        						
        							if (TableHeading_Stato.contentEquals("Stato") && Table_Stato_Value.contentEquals("In attesa")) {
        								
        								verifyComponentExistence(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[1]"));
        								VerifyText(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[1]"), "Item Name");
        								
        								verifyComponentExistence(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[1]"));
        								clickComponent(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[1]"));
        								
        								item_found = "YES";
        								break demo;
        								
        							}
        							
        						}
        					}
        				}
        				
        				
					} 
        		}
        		
			} 
        }
        
       
        if (item_found.contentEquals("NO"))
			throw new Exception("The expected Item Name is not found:");
	
	}
	
	
	public void Validate_Service_Request_Section() throws Exception
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
        LocalDateTime now = LocalDateTime.now();  
        String TODAYS_DATE = dtf.format(now);
        System.out.println(TODAYS_DATE);
        
        String item_found = "NO";
        
        int table = driver.findElements(By.xpath("(//table)")).size();
        
        
        demo:
        for (int k = 1; k <= table; k++) {                                           // table size
			
        	
        	if(driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[3]")).isDisplayed()) {
        		
        		//check if table heading present in table
            	String TableHeading = driver.findElement(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[3]")).getText();
        		
            	if (TableHeading.contentEquals("Data creazione")){
            		
            		int row = driver.findElements(By.xpath("(//table)["+k+"]/tbody/tr")).size();
    				
    				for (int i = 1; i <= row; i++) {                                     // row size
    					
    					if (driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[3]")).isDisplayed()) {
							
    						String TableValue = driver.findElement(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[3]")).getText();
    						
    						if (TableHeading.contentEquals("Data creazione") && TableValue.contains(TODAYS_DATE)) {
    							
								verifyComponentExistence(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[1]"));
								VerifyText(By.xpath("(((//table)["+k+"]/thead/tr/th/div/a/span)[position() mod 2 = 0])[1]"), "Name");
								
								verifyComponentExistence(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[1]"));
								clickComponent(By.xpath("((//table)["+k+"]/tbody/tr["+i+"]/td/span/span[not(contains(@class,'checkbox'))] | (//table)["+k+"]/tbody/tr["+i+"]/th/span/a)[1]"));
								
								item_found = "YES";
								
								break demo;
										
    						}
						}
    				}
    			}
            	
        	}
        }
        
        if (item_found.contentEquals("NO"))
			throw new Exception("The expected Item Name is not found:");
	
	}
	
	public void storeServiceRequestId(By checkObject) throws Exception
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		verifyComponentExistence(checkObject);
		clickComponent(checkObject);
		Thread.sleep(3000);
		
		verifyComponentExistence(service_Request_ID_Heading);
		VerifyText(service_Request_ID_Heading, "ID del record");
		
		ServiceRequestId = driver.findElement(service_Request_ID_Value).getText();
		clickComponent(checkObject);
	}
	
	public void datiDiProcessSectionPod2(String Processo, String Stato, String pagamento_heading, String Metodo_di_pagamento, String titolare_del_conto_bancario,String CF_del_nuovo, String nuovo_iban) throws Exception
	{
			verifyComponentExistence(By.xpath("(//span[text()='Processo']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"));
			VerifyText(By.xpath("(//span[text()='Processo']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"), Processo);
			
			verifyComponentExistence(By.xpath("(//span[text()='Stato']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"));
			VerifyText(By.xpath("(//span[text()='Stato']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"), Stato);
			
			verifyComponentExistence(By.xpath("(//span[text()='Metodo di pagamento'])[2]"));
			VerifyText(By.xpath("(//span[text()='Metodo di pagamento'])[2]"), pagamento_heading);
			
			verifyComponentExistence(By.xpath("(//span[text()='Nuovo Metodo di pagamento']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"));
			VerifyText(By.xpath("(//span[text()='Nuovo Metodo di pagamento']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"), Metodo_di_pagamento);
			
			verifyComponentExistence(By.xpath("(//span[text()='Nuovo titolare del conto bancario']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"));
			VerifyText(By.xpath("(//span[text()='Nuovo titolare del conto bancario']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"), titolare_del_conto_bancario);
			
			verifyComponentExistence(By.xpath("(//span[text()='CF del nuovo titolare del conto bancario']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"));
			VerifyText(By.xpath("(//span[text()='CF del nuovo titolare del conto bancario']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"), CF_del_nuovo);
			
			verifyComponentExistence(By.xpath("(//span[text()='Nuovo IBAN']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"));
			VerifyText(By.xpath("(//span[text()='Nuovo IBAN']/parent::div/following-sibling::div/span//lightning-formatted-text)[2]"), nuovo_iban);
			
		
	}
	public void datiDiProcessSectionPod1(String Processo, String Stato, String pagamento_heading, String Metodo_di_pagamento, String titolare_del_conto_bancario,String CF_del_nuovo, String nuovo_iban) throws Exception
	{
			verifyComponentExistence(Processo_FieldValue);
			VerifyText(Processo_FieldValue, Processo);
			
			verifyComponentExistence(Stato_FieldValue);
			VerifyText(Stato_FieldValue, Stato);
			
			verifyComponentExistence(Metodo_di_pagamento_SectionHeading);
			VerifyText(Metodo_di_pagamento_SectionHeading, pagamento_heading);
			
			verifyComponentExistence(Nuovo_Metodo_di_pagamento_FieldValue);
			VerifyText(Nuovo_Metodo_di_pagamento_FieldValue, Metodo_di_pagamento);
			
			verifyComponentExistence(Nuovo_titolare_del_conto_bancario_FieldValue);
			VerifyText(Nuovo_titolare_del_conto_bancario_FieldValue, titolare_del_conto_bancario);
			
			verifyComponentExistence(CF_del_nuovo_titolare_del_conto_bancario_FieldValue);
			VerifyText(CF_del_nuovo_titolare_del_conto_bancario_FieldValue, CF_del_nuovo);
			
			verifyComponentExistence(Nuovo_IBAN_FieldValue);
			VerifyText(Nuovo_IBAN_FieldValue, nuovo_iban);

		
		
	}
	public void validateSupply(By checkObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("//div[@id='mmpNonAttivo']//div[@class='row']/div/div[1]"));
		List<WebElement> value = driver.findElements(By.xpath("//div[@id='mmpNonAttivo']//div[@class='row']/div/div[2]"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("(//div[@id='mmpNonAttivo']//div[@class='row']/div/div[1])["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("(//div[@id='mmpNonAttivo']//div[@class='row']/div/div[2])["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("VIA NIZZA 148 - 00198 ROMA RM")) {
				textfield_found="YES";
			}else if (description.contentEquals("Numero cliente") && descriptionValue.contentEquals("310492540")) {
				textfield_found="YES";
			}
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Confirm Light Supply details are not matching for index :"+i);
			
		}
		
	}
	
	public void validateSupply(By checkObject, String numeroCliente) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("//div[@id='mmpNonAttivo']//div[@class='row']/div/div[1]"));
		List<WebElement> value = driver.findElements(By.xpath("//div[@id='mmpNonAttivo']//div[@class='row']/div/div[2]"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("(//div[@id='mmpNonAttivo']//div[@class='row']/div/div[1])["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("(//div[@id='mmpNonAttivo']//div[@class='row']/div/div[2])["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("VIA NIZZA 152 - 00198 ROMA RM")) {
				textfield_found="YES";
			}else if (description.contentEquals("Numero cliente") && descriptionValue.contentEquals(numeroCliente)) {
				textfield_found="YES";
			}
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Confirm Light Supply details are not matching for index :"+i);
			
		}
		
	}
	
	public void confirmSupply(By checkObject, String numeroCliente1, String numeroCliente2) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
		
		String numeroCliente [] = {numeroCliente1,numeroCliente2};
		
		for (int k = 0; k < numeroCliente.length; k++) {
			
			List<WebElement> field = driver.findElements(By.xpath("(//table)[2]//thead/descendant::th"));
			List<WebElement> value = driver.findElements(By.xpath("(//table)[2]//tbody//td[text()='"+numeroCliente[k]+"']/parent::tr/td"));
			
			for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
				
				WebElement element = driver.findElement(By.xpath("(//table)[2]//thead/descendant::th["+i+"]"));
				String description=element.getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				WebElement element1 = driver.findElement(By.xpath("(//table)[2]//tbody//td[text()='"+numeroCliente[k]+"']/parent::tr/td["+j+"]"));
				String descriptionValue=element1.getText();
				descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
				
				if (description.contentEquals("Tipo") && descriptionValue.contentEquals("Luce")) {
					textfield_found="YES";
				}else if (description.contentEquals("Numero Cliente / Alias") && descriptionValue.contentEquals(numeroCliente[k])) {
					textfield_found="YES";
				}else if (description.contentEquals("Indirizzo di fornitura") && descriptionValue.contentEquals("Via Nizza 152 00198 Roma Roma Rm")) {
					textfield_found="YES";
				}else if (description.contentEquals("Attuale modalità di pagamento") && descriptionValue.contentEquals("Bollettino Postale")) {
					textfield_found="YES";
				}
				
				if (textfield_found.contentEquals("NO"))
					throw new Exception("Confirm Light Supply details are not matching for index :"+i);
				
			}
			
		}
					
		
		
	}
	public void clickTab(By Object) throws Exception {
		driver.findElement(Object).sendKeys(Keys.TAB);;
	}
	
	public void validateAttivazioneSteps() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//ul[contains(@class,'step')]/li"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 I tuoi dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
	}
	
	public void validateMultiAttivazioneSteps() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//ul[contains(@class,'step')]/li"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 Multifornitura")) {
				stepFound = "YES";
			}else if (stepDescription.contains("2 I tuoi dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("4 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
	}
	
	public void validateAttivazioneFields() throws Exception
	{
		verifyComponentExistence(Cognome_Ragione_sociale_Intestatario_C_C_FieldValue);
		verifyComponentExistence(Nome_Intestatario_C_C_FieldValue);
		verifyComponentExistence(Codice_fiscale_Avente_poteri_di_firma_FieldValue);
		verifyComponentExistence(IBAN_FieldValue);
		verifyComponentExistence(IBAN_ESTERO_Checkbox);
		verifyComponentExistence(Agenzia_FieldValue);
		verifyComponentExistence(Nome_banca_FieldValue);
	}
	
	public void setAttivazioneFieldValues() throws Exception
	{
		enterInput(IBAN_FieldValue, iban_FieldValue);
		clickTab(IBAN_FieldValue);
	}
	
	public void validateAttivazionePrepopulatedFields() throws Exception
	{
		
		verifyComponentExistence(Cognome_Ragione_sociale_Intestatario_C_C_FieldValue);
		checkPrePopulatedValueAndCompare(Cognome_Ragione_sociale_Intestatario_C_C,cognome_Ragione_sociale_Intestatario_C_C_FieldValue);
		
		verifyComponentExistence(Codice_fiscale_Avente_poteri_di_firma_FieldValue);
		checkPrePopulatedValueAndCompare(Codice_fiscale_Avente_poteri_di_firma,codice_fiscale_Avente_poteri_di_firma_FieldValue);
		
		setAttivazioneFieldValues();
		Thread.sleep(5000);
		
		verifyComponentExistence(Agenzia_FieldValue);
		checkPrePopulatedValueAndCompare(Agenzia,AgenziaPopulatedValue);
		
		verifyComponentExistence(Nome_banca_FieldValue);
		checkPrePopulatedValueAndCompare(Nome_banca,Nome_banca_PopulatedValue);
		
	}
	
	public void validateAttivazionePostpopulatedFields() throws Exception
	{
		
		verifyComponentExistence(Cognome_Ragione_sociale_Intestatario_C_C_FieldValue);
		checkPrePopulatedValueAndCompare(Cognome_Ragione_sociale_Intestatario_C_C,cognome_Ragione_sociale_Intestatario_C_C_FieldValue);
		
//		verifyComponentExistence(Nome_Intestatario_C_C_FieldValue);
//		checkPrePopulatedValueAndCompare(Nome_Intestatario_C_C,nome_Intestatario_C_C_FieldValue);
		
		verifyComponentExistence(Codice_fiscale_Avente_poteri_di_firma_FieldValue);
		checkPrePopulatedValueAndCompare(Codice_fiscale_Avente_poteri_di_firma,codice_fiscale_Avente_poteri_di_firma_FieldValue);
		
		verifyComponentExistence(IBAN_FieldValue);
		checkPrePopulatedValueAndCompare(IBAN,iban_FieldValue);
		
		verifyComponentExistence(Agenzia_FieldValue);
		checkPrePopulatedValueAndCompare(Agenzia,AgenziaPopulatedValue);
		
		verifyComponentExistence(Nome_banca_FieldValue);
		checkPrePopulatedValueAndCompare(Nome_banca,Nome_banca_PopulatedValue);
			
	}
	
	public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
        
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        if(value.equals(""))throw new Exception("Field is not pre filled");
        
        String match = "FAIL";
        if(value.contentEquals(expectedValue))
        	match = "PASS";
        if(match.contentEquals("FAIL"))
        	throw new Exception("PrePopulatedValue and Expected values are not matching");
    }
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{

		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));

		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
    }

	public void verifyCheckboxSelected(By checkboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(checkboxObject);
		
		if (element.isSelected()){
		checkbox="YES";
		}			
		if (!element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is not selected");
				}
	}
	
	public void verifyCheckboxnotSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="YES";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (!element.isSelected()){
		checkbox="NO";
		}			
		if (element.isSelected()){
		checkbox = "YES";
			throw new Exception("The checkbox is selected");
				}
	}
	
	public void validateAttivazioneAddebitoDirettoSupply(String numeroCliente1,String numeroCliente2) throws Exception {
		
		/*WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));*/
						
		String textfield_found="NO";
		
		String numeroCliente [] = {numeroCliente1,numeroCliente2};
		
		for (int k = 0; k < numeroCliente.length; k++) {
			
			List<WebElement> field = driver.findElements(By.xpath("(//table//th)[position()>2 and position()< last()]"));
			List<WebElement> value = driver.findElements(By.xpath("//td[text()='"+numeroCliente[k]+"']/parent::tr/td[position()>2]"));
			
			for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
				
				WebElement element = driver.findElement(By.xpath("(//table//th)[position()>2 and position()< last()]["+i+"]"));
				String description=element.getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				WebElement element1 = driver.findElement(By.xpath("(//td[text()='"+numeroCliente[k]+"']/parent::tr/td[position()>2])["+j+"]"));
				String descriptionValue=element1.getText();
				descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
				
				if (description.contentEquals("Tipo") && descriptionValue.contentEquals("Luce")) {
					textfield_found="YES";
				}else if (description.contentEquals("Numero cliente / Alias") && descriptionValue.contentEquals(numeroCliente[k])) {
					textfield_found="YES";
				}else if (description.contentEquals("Indirizzo fornitura") && descriptionValue.contentEquals("Via Nizza 152 00198 Roma Roma Rm")) {
					textfield_found="YES";
				}
				
				/*if (numeroCliente[k].contentEquals(numeroCliente2)) {
					
					verifyComponentExistence(By.xpath("//td[text()='"+numeroCliente[k]+"']/parent::tr/td[1]"));
					clickComponent(By.xpath("//td[text()='"+numeroCliente[k]+"']/parent::tr/td[1]"));
					Thread.sleep(5000);
				}*/
				
				
				if (textfield_found.contentEquals("NO"))
					throw new Exception("LUCE Supply details are not matching for index :"+numeroCliente[k]);
				
			}
			
		}
		
		verifyComponentExistence(By.xpath("//td[text()='"+numeroCliente2+"']/parent::tr/td[1]"));
		clickComponent(By.xpath("//td[text()='"+numeroCliente2+"']/parent::tr/td[1]"));
		Thread.sleep(5000);
		
		
		verifyComponentExistence(By.xpath("//td[text()='"+numeroCliente1+"']/parent::tr/td[1]"));
//		verifyCheckboxSelected(By.xpath("//td[text()='"+numeroCliente1+"']/parent::tr/td[1]"));
		verifyComponentExistence(By.xpath("//td[text()='"+numeroCliente1+"']/parent::tr/td[2]"));
//		checkColor(By.xpath("//td[text()='"+numeroCliente1+"']/parent::tr/td[2]"), successColor, "checkbox_"+numeroCliente1+"");
		
		verifyComponentExistence(By.xpath("//td[text()='"+numeroCliente2+"']/parent::tr/td[1]"));
//		verifyCheckboxSelected(By.xpath("//td[text()='"+numeroCliente2+"']/parent::tr/td[1]"));
		verifyComponentExistence(By.xpath("//td[text()='"+numeroCliente2+"']/parent::tr/td[2]"));
//		checkColor(By.xpath("//td[text()='"+numeroCliente2+"']/parent::tr/td[2]"), successColor, "checkbox_"+numeroCliente2+"");
		
		
	}

	public void searchPOD(By by, String filter) throws Exception{
		WebElement we = driver.findElement(by);
		we.sendKeys(filter);
	}
	
	public void searchPOD(By by, String filter, boolean newLine) throws Exception{
		Thread.sleep(5000);
		WebElement we = driver.findElement(by);
		if(newLine){
			we.clear();
			we.sendKeys(filter+"\n");
		}
		else{
			we.sendKeys(filter);
		}
	}
	
	public void switchToUDBwindow_ClickSubmit(By Checkobject, String value2, String url) throws Exception
	{
		String value1 = driver.findElement(Checkobject).getText();
		
		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.open('"+url+"','_blank');");
		Thread.sleep(5000);
		
		String idbpm_SucessStatus = "Risposta del server: generato esito: KO per l'idbpm: "+value1+"";
		
		
		Set<String> wh = driver.getWindowHandles();
		
		for (String sWH : wh) {
			System.out.println(sWH);
			System.out.println(driver.getTitle());
			driver.switchTo().window(sWH);
			if (!(driver.getTitle().contains("Salesforce"))) {   //Entra nel Mercato Libero: Offerte Luce e Gas | Enel Energia
				
				String idbpm_Message = driver.findElement(idBpmStatus).getText();
				do {
					
					enterInput(idBpm,value1);
					
					Select select = new Select(util.waitAndGetElement(esito));
					select.selectByValue(value2);
					
					TimeUnit.SECONDS.sleep(5);
					util.objectManager(submit, util.scrollToVisibility,true);
					Thread.sleep(5000);
					util.scrollToElement(driver.findElement(submit));
//					jsClickObject(submit);
//					clickComponent(submit);
					udbSubmit(submit);
					Thread.sleep(10000);
					
					idbpm_Message = driver.findElement(idBpmStatus).getText();
					System.out.println("Status :"+idbpm_Message);
					
				} while (!(idbpm_Message.equals(idbpm_SucessStatus)));
				
				driver.close();
			}
		}
		driver.switchTo().window(mainWindow);
//		System.out.println("Title Main Window"+driver.getTitle());
	}
	
	public void refreshBrowser() throws InterruptedException
	{
		driver.navigate().refresh();
		Thread.sleep(8000);
	}
	
	public void selezionaEnvironment(String environment) throws Exception{
		util.objectManager(selectEnvironment, util.select, environment);
	}

	public void pressButton(By element) throws Exception{
		util.objectManager(element, util.scrollAndClick);
	}
	
	public void switchToWb_UpdateServiceRequest(String url,String reqNumber) throws Exception
	{
		
		String mainWindow = driver.getWindowHandle();
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.open('"+url+"','_blank');");
		Thread.sleep(5000);
		
		Set<String> wh = driver.getWindowHandles();
		
		for (String sWH : wh) {
			
			driver.switchTo().window(sWH);
			
			if (driver.getTitle().contentEquals("Workbench")) {
				
				selezionaEnvironment("Sandbox");
				pressButton(checkAgree);
				pressButton(buttonLogin);
				Thread.sleep(5000);
				
				Actions act = new Actions(driver);
				act.moveToElement(driver.findElement(data)).build().perform();
				act.click(driver.findElement(update)).perform();
//				action.moveToElement(driver.findElement(update)).click();
				
				Select sel = new Select(driver.findElement(objectType));
				sel.selectByValue("ITA_IFM_ServiceRequest__c");
				
				util.objectManager(singleRecord, util.scrollToVisibility,true);
				util.scrollToElement(driver.findElement(singleRecord));
				jsClickObject(singleRecord);
				enterInput(singleRecordInput, reqNumber);
				
				TimeUnit.SECONDS.sleep(5);
				util.objectManager(nextButton, util.scrollToVisibility,true);
				Thread.sleep(5000);
				util.scrollToElement(driver.findElement(nextButton));
				jsClickObject(nextButton);
				Thread.sleep(7000);
				
				enterInput(ITA_IFM_OperationStatus__c, "Annullato");
				enterInput(ITA_IFM_Status__c, "Canceled");
				enterInput(ITA_IFM_CancelStatus__c, "Non previsto");
				util.scrollToElement(driver.findElement(ConfirmUpdateButton));
				jsClickObject(ConfirmUpdateButton);
				Thread.sleep(5000);
				
				validate_ServiceId_UpdateStatus(reqNumber);
				
				driver.close();
			}
		}
		driver.switchTo().window(mainWindow);
//		System.out.println("Title Main Window"+driver.getTitle());
	}
	
	public void validate_ServiceId_UpdateStatus(String salesforceId)
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(downloadResult));
		
		String stepFound="NO";
		for (int i = 1; i <4; i++) {
			
			String tableHeading = driver.findElement(By.xpath("//table[@class='dataTable']//tr/th["+(i+1)+"]")).getText();
			String tableValue = driver.findElement(By.xpath("//table[@class='dataTable']//tr/td["+(i+1)+"]")).getText();
			
			if (tableHeading.contentEquals("Salesforce Id") && tableValue.contentEquals(salesforceId)) {
				stepFound = "YES";
			}else if (tableHeading.contentEquals("Result") && tableValue.contentEquals("Success")) {
				stepFound = "YES";
			}else if (tableHeading.contentEquals("Status") && tableValue.contentEquals("Updated")) {
				stepFound = "YES";
			}
			
		}
	}
	
	public void selectRinunciaCliente(String checkobject) throws Exception{
		
		TimeUnit.SECONDS.sleep(3);
		
		Thread.sleep(5000);
		Select select = new Select(util.waitAndGetElement(annullamentoDropdown));
		select.selectByValue(checkobject);
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(conferma, util.scrollToVisibility,true);
		Thread.sleep(5000);
		util.scrollToElement(driver.findElement(conferma));
		jsClickObject(conferma);
		Thread.sleep(5000);
	//	util.objectManager(annulla, util.click);
	//	spinner.checkSpinners();
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		util.scrollToElement(driver.findElement(close));
		jsClickObject(close);
	//  jsClickObject(AnnulmentDialogTitle_annullaButton);
	  
	}
	
	public void SwitchTabNewWindow() throws Exception
    {
       Set <String> windows = driver.getWindowHandles();
         currentHandle = driver.getWindowHandle();
       
        for (String winHandle : windows) {
           // Switch to child window
        //System.out.println("switched to "+driver.getTitle()+"  Window");
            String pagetitle = driver.getTitle();
            driver.switchTo().window(winHandle);
         }
                    
    }
	
	public void switchFrame(String frame) throws Exception{
		util.switchToFrame(frame);
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	
	
	public static final String servicesColor = "Crimson";
	public static final String serviceColor = "DeepPink";
	public static final String errorColor = "Red";
	public static final String successColor = "Teal";
	
	public static final String Cognome_Ragione_sociale_Intestatario_C_C = "//label[contains(text(),'Cognome/Ragione sociale Intestatario C/C')]/following-sibling::input";
	public static final String Nome_Intestatario_C_C = "//label[contains(text(),'Nome Intestatario C/C')]/following-sibling::input";
	public static final String Codice_fiscale_Avente_poteri_di_firma = "//label[contains(text(),'Codice fiscale (Avente poteri di firma)')]/following-sibling::input";
	public static final String IBAN = "//label[contains(text(),'IBAN')]/following-sibling::input";
	public static final String Agenzia = "//label[contains(text(),'Agenzia')]/following-sibling::input";
	public static final String Nome_banca = "//label[contains(text(),'Nome banca')]/following-sibling::input";
	
	public static final String cognome_Ragione_sociale_Intestatario_C_C_FieldValue = "IMPRESA MAIRA OPEN ENERGY";
//	public static final String nome_Intestatario_C_C_FieldValue = "CARLO";
	public static final String codice_fiscale_Avente_poteri_di_firma_FieldValue = "07351980565";
	public static final String iban_FieldValue = "IT30P0301503200000000246802";
	public static final String AgenziaPopulatedValue = "SEDE DI ROMA";
	public static final String Nome_banca_PopulatedValue = "FINECOBANK SPA";
	
	public static final String emailInputField = "//label[contains(text(),'Email')]/following-sibling::input";
	public static final String emailInputFieldValue = "web.testing.automat.ion@gmail.com";
	
	public static final String requestInfo = "La tua richiesta è stata presa in carico ed è stata inoltrata al tuo istituto di credito che dovrà autorizzarla. Segui l'avanzamento della tua richiesta nella sezione STATO RICHIESTE";
	public static final String attentionInfo = "Attenzione, il servizio verrà attivato non appena l'istituto di credito confermerà l'effettivo addebito diretto";
	
	public static final String Rinuncia_cliente_Dropdown = "Rinuncia cliente";
	
	public static final String SelectRecentOrderCaseItem = "((//*[contains(text(),'Data creazione')])/following-sibling::dd[1]//lightning-formatted-text[contains(text(),'$DATE$')])[1]/ancestor::div[@class='slds-media__body']//span[contains(text(),'OI-')]/parent::a";
	public static final String Data_creazioneFieldValue = "((//*[contains(text(),'Data creazione')])/following-sibling::dd[1]//lightning-formatted-text[contains(text(),'$DATE$')])[1]";
	public static final String Tipo_OperazioneFieldValue = "((//*[contains(text(),'Data creazione')])/following-sibling::dd[1]//lightning-formatted-text[contains(text(),'$DATE$')])[1]/ancestor::dl/*[contains(text(),'Tipo Operazione')]/following-sibling::dd[1]//lightning-formatted-text";
	public static final String Tipo_di_recordFieldValue = "((//*[contains(text(),'Data creazione')])/following-sibling::dd[1]//lightning-formatted-text[contains(text(),'$DATE$')])[1]/ancestor::dl/*[contains(text(),'Tipo di record')]/following-sibling::dd[1]//lightning-formatted-rich-text/span";
	
	public static final String ChecosContent = "L'Addebito Diretto, chiamato anche SDD (SEPA Direct-Debit), è il metodo più facile e veloce per pagare le tue bollette. Attivando il servizio, l'importo della bolletta verrà addebitato direttamente sul tuo Conto Corrente il giorno stesso della scadenza, così non dovrai più preoccuparti di ricordartelo. Per la corretta attivazione del servizio, ti chiederemo indirizzo mail e numero di cellulare, nel caso non fossero già presenti nei nostri sistemi.";
}


