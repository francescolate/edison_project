package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaPageComponent {
	
WebDriver driver;
SeleniumUtilities util;
Properties prop = null;
public List<WebElement> menuItemsList = null;

public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
public By logoEnel = By.xpath("//img[@class='logoimg']");
public By iconUser = By.xpath("//span[@class='icon-user']");
public By loginPage = By.xpath("//form[@id='formlogin']");
public By username = By.id("txtLoginUsername");
public By password = By.id("txtLoginPassword");
public By buttonLoginAccedi = By.id("login-btn");
public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
public By loginSuccessful= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
// public By buttonPopupLogin=By.xpath("//button[@class='remodal-close popupLogin-close']");
public By loginBSNSuccessful= By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
public By logout=By.id("disconnetti");
public By accountButton=By.xpath("//button[@id='user']");
//public By esciLink=By.xpath("//a[@id='tr_disconnetti']");
public By recoveryPassword = By.xpath("//div[@class='login-btn login-btn-login']/following::a[text()='Recupera Password']");
public By recoveryUsername = By.xpath("//div[@class='login-btn login-btn-login']/following::a[text()='Recupera Username']");
public By accessProblemsLabel = By.xpath("//div[@class='login-btn login-btn-login']/following::p[contains(text(),'Se hai problemi')]");
public By googleAccessButton = By.xpath("//div[@class='login-btn']/child::button[contains(text(),'Accedi con Google')]");
public By facebookAccessButton = By.xpath("//div[@class='login-btn']/child::button[contains(text(),'Accedi con Facebook')]");
public By noAccountLabel = By.xpath("//div[@class='login-block']/descendant::p[contains(text(),'Non hai ancora')]");
public By registerButton = By.xpath("//div[@class='login-block']/descendant::a[text()='Registrati']");
public By accessProblemsLink = By.xpath("//div[@class='login-btn login-btn-login']/following::a[text()='clicca qui']");

public By BenvenutoTitle = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Benvenuto nella')]");
public By BenvenutoContent = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'In questa')]");

public By bevenutoPrivataTitle = By.xpath("//div[@id='mainProspect']/descendant::h1[contains(text(),'Benvenuto nella')]");
public By bevenutoPrivataContent = By.xpath("//div[@id='mainProspect']/descendant::p[contains(text(),'Entra nel mondo')]");

public By tuoiq1 = By.xpath("//section[@id='landing-text']/descendant::h1[contains(text(),'Come posso esercitare')]");
public By tuoia1 = By.xpath("//section[@id='landing-text']/descendant::h2[contains(text(),'Se vuoi esercitare')]");

public By tuoiq2 = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Informazione')]");
public By tuoia2a = By.xpath("//p[contains(text(),'È possibile consultare le ')]/preceding-sibling::h4[contains(text(),'Come faccio a visualizzare')]");
public By tuoia2b = By.xpath("//section[@id='landing-text']/descendant::h4[contains(text(),'Come faccio')]/following-sibling::p[contains(text(),'È possibile consultare le policy')]");
public By tuoiq3  = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Accesso')]");
public By tuoia3 = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'È possibile visualizzare')]");
public By tuoiq4 = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Cancellazione')]");
public By tuoia4a = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'Hai il diritto di ottenere')]");
public By tuoia4b = By.xpath("//section[@id='landing-text']/descendant::li[contains(text(),'revoca il consenso')]/parent::ol");
public By tuoia4c = By.xpath("//h3[text()='Diritto di Cancellazione']/parent::div[@class='faq-appearance-group']/descendant::p[contains(text(),'Per esercitare il')]");
public By tuoiq5 = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Limitazione')]");
public By tuoia5a = By.xpath("//section[@id='landing-text']/descendant::h4[contains(text(),'È possibile limitare')]");
public By tuoia5b = By.xpath("//section[@id='landing-text']/descendant::h4[contains(text(),'È possibile limitare')]/following-sibling::p[contains(text(),'Hai il diritto')]");
public By tuoia5c = By.xpath("//section[@id='landing-text']/descendant::h4[contains(text(),'È possibile limitare')]/following-sibling::ol");
public By tuoia5d = By.xpath("//section[@id='landing-text']/descendant::h4[contains(text(),'È possibile limitare')]/following-sibling::p[contains(text(),'Per esercitare')]");
public By tuoiq6 = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Opposizione')]");
public By tuoia6a = By.xpath("//div[@class='faq-element parbase']/descendant::h4[contains(text(),'Come faccio a visualizzare')]");
public By tuoia6b = By.xpath("//div[@class='faq-element parbase']/descendant::p[contains(text(),'È possibile consultare')]");
public By tuoiq7 = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Rettifica')]");
public By tuoia7 = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'Nella')]");
public By tuoiq8 = By.xpath("//section[@id='landing-text']/descendant::h3[contains(text(),'Diritto di Portabilità')]");
public By tuoia8 = By.xpath("//h3[text()='Diritto di Portabilità']/parent::div[@class='faq-appearance-group']/descendant::p[contains(text(),'È possibile richiedere')]");

public By supportoHomeLoc = By.xpath("//a[@class='breadcrumbs_link']/ancestor::ul[@class='breadcrumbs component']");
public By novitaHeading = By.xpath("//div[@id='mainContentWrapper']/descendant::div[@class='section-heading']//h2[contains(text(),'Novità')]");
public By novitaContent = By.xpath("//div[@id='mainContentWrapper']/descendant::div[@class='section-heading']//p[contains(text(),'In questa sezione')]");

public By datiContattiHeading = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Dati e Contatti')]");
public By datiContattiContent = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'Consulta e aggiorna')]");
public By spazioHome = By.xpath("//span[text()='Home']/ancestor::ul[@class='breadcrumbs component']");
public By spazioTitle = By.xpath("//span[@class='image-hero_title text--page-heading']");
public By homepageLogoLabel = By.xpath("//div[@class='dotcom-header__logo']/child::span[contains(text(),'Enel Energia per il mercato libero')]");



public static final String TUOI_Q1 = "Come posso esercitare i diritti sui miei dati personali in base a quanto stabilito dal General Data Protection Regulation (GDPR)?";
public static final String TUOI_A1 = "Se vuoi esercitare uno o più diritti riconosciuti dal GDPR puoi farlo seguendo le indicazioni riportate di seguito.";
public static final String TUOI_Q2 = "Diritto di Informazione";
public static final String TUOI_A2a = "Come faccio a visualizzare le informative sulla privacy e consultare la politica sui cookie?";
public static final String TUOI_A2b = "È possibile consultare le policy tramite i link Privacy Policye Cookie Policy";
public static final String TUOI_Q3 = "Diritto di Accesso";
public static final String TUOI_A3 = "È possibile visualizzare i tuoi dati personali e i dati di contatto nellaSezione Account."
									+"Per poter accedere ai dati di pagamento puoi consultare il servizio diAddebito Direttoqualora fosse attivo su almeno una delle tue forniture."
									+ "Per consultare i dati della tua fornitura è possibile trovare queste informazioniselezionando una tua fornituraed accedendo al relativo dettaglio. Qualora avessi forniture in attivazione potrai vedere le informazioni sullo stato di avanzamento della fornitura in lavorazione.";
public static final String TUOI_Q4 = "Diritto di Cancellazione";
public static final String TUOI_A4a = "Hai il diritto di ottenere dal titolare del trattamento la cancellazione dei dati personali che ti riguardano senza ingiustificato ritardo e il titolare del trattamento ha l’obbligo di cancellare senza ingiustificato ritardo i dati personali, se sussiste uno dei motivi seguenti:";
public static final String TUOI_A4b = "1. I dati personali non sono più necessari rispetto alle finalità per le quali sono stati raccolti o altrimenti trattati;"
									+ "2. L’interessato revoca il consenso su cui si basa il trattamento.";
public static final String TUOI_A4c = "Per esercitare il diritto puoi scrivere tramite email al seguente indirizzo:privacy.enelenergia@enel.com";

public static final String TUOI_Q5 = "Diritto di Limitazione del Trattamento";
public static final String TUOI_A5a = "È possibile limitare l’utilizzo dei miei dati conservati da Enel Energia?";
public static final String TUOI_A5b = "Hai il diritto di ottenere dal titolare la limitazione del trattamento quando ricorre una delle seguenti casistiche:";
public static final String TUOI_A5c = "L’interessato contesta l’esattezza dei dati personali;Il trattamento è illecito e l’interessato si oppone alla cancellazione dei dati personali e chiede invece che ne sia limitato l’utilizzo;I dati personali sono necessari all’interessato per l’accertamento, l’esercizio o la difesa di un diritto in sede giudiziaria.";
public static final String TUOI_A5d = "Per esercitare il diritto puoi scrivere tramite email al seguente indirizzo:privacy.enelenergia@enel.com";
public static final String TUOI_Q6 = "Diritto di Opposizione";
public static final String TUOI_A6a = "Come faccio a visualizzare le informative sulla privacy e consultare la politica sui cookie?";
public static final String TUOI_A6b = "È possibile consultare le policy tramite i link Privacy Policye Cookie Policy";
public static final String TUOI_Q7 = "Diritto di Rettifica";
public static final String TUOI_A7 = "NellaSezione Accountè possibile consultare i tuoi dati personali e modificare i tuoi consensi, i tuoi dati di contatto e dati di registrazione.Se hai attivo l’Addebito Direttopuoi consultare e modificare i dati.";
public static final String TUOI_Q8 = "Diritto di Portabilità";
public static final String TUOI_A8 = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailmajdouline_182004@yahoo.it.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
public static final String TUOI_A8b = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailfabiana.manzo1@yopmail.com.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
public static final String TUOI_A8a = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailrita.mastellone@accenture.com.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";

public By TravoSpazioHome = By.xpath("//span[text()='Home']/ancestor::ul[@class='breadcrumbs component']");
public By TravoSpazioTitle = By.xpath("//span[@class='image-hero_title text--page-heading']");
public By profileFields = By.xpath("//div[@id='profile']/dl[@class='test']/span/dt");


//LeftMenu Links
public By fornitureLink = By.xpath("//a[@id='idFornitura']/span");
//public By enelPremiaLink  = By.xpath("//a[@id='enelPremia']//span[contains(text(),'enelpremia WOW!')]");
public By enelPremiaLink  = By.xpath("//a[@id='enelPremia']//span[contains(text(),'enelpremia WOW!')]");
public By novitaLink = By.xpath("//a[contains(@id,'novitaSelfcare')]//span[text()='Novità']");
public By accountLink = By.xpath("//a[contains(@id,'accountSelfcare')]//span[text()='Account']");
public By tuoiDirittiLink = By.xpath("//a[@id='iTuoiDiritti']/ancestor::ul[@id='internal-menu']//span[text()='I tuoi diritti']");
public By supportoLink = By.xpath("//a[contains(@id,'supporto')]/child::span[text()='Supporto']");
public By travoSpazioLink = By.xpath("//a[contains(@id,'negozioEnel')]/child::span[text()='Trova Spazio Enel.']");
public By esciLink = By.xpath("//span[@class='dsc-icon-line-download']/ancestor::nav[@id='mainNav']/descendant::span[contains(text(),'Esci')]");
public By spazioVideoLink = By.xpath("//nav[@id='mainNav']/descendant::span[contains(text(),'Spazio Video')]");


public By fornitureHeader = By.xpath("//section[@id='lista-forniture']/descendant::h2[contains(text(),'Le tue forniture')]");
public By fornitureContent = By.xpath("//section[@id='lista-forniture']/descendant::p[contains(text(),'Visualizza le informazioni')]");
public By accediAlServizio = By.xpath("//div[@id='landing-dispositiva']/child::button[text()='ACCEDI AL SERVIZIO']");
public By fornitureHeading = By.xpath("//div[@id='mainProspect']//h1");
public By fornitureParagraph = By.xpath("//div[@id='mainProspect']//p[2]");

public By indrizzolabel = By.xpath("//div[@class='label-container gas']/descendant::span[@class='indirizzo']/child::span[contains(text(),'Indirizzo della')]");
public By numeroLabel = By.xpath("//div[@class='label-container gas']/descendant::span[contains(text(),'Numero Cliente')]");
public By indrizzoValue = By.xpath("//div[@class='label-container gas']/descendant::span[@class='indirizzo']/child::span[contains(text(),'Via Archimede 57 95018 Catania Riposto Ct')]");
public By numeroValue = By.xpath("//div[@class='label-container gas']/descendant::span[contains(text(),'708692905')]");
public By dirrittoCheckbox = By.xpath("//input[@type='checkbox']/following::span[@class='icon-ckbox']");
public By scaricaLink = By.xpath("//div[@class='label-container gas']/descendant::a[contains(text(),'Scarica file')]");
public By scaricaLink1 = By.xpath("//a[contains(text(),'scarica il file')]");

public By luceIndrizzoLabel = By.xpath("//div[@class='label-container luce']/descendant::span[@class='indirizzo']/child::span[contains(text(),'Indirizzo della')]");
public By luceNumeroLabel = By.xpath("//div[@class='label-container luce']/descendant::span[contains(text(),'Numero Cliente')]");
public By luceIndrizzoValue = By.xpath("//div[@class='label-container luce']/descendant::span[@class='indirizzo']/child::span[contains(text(),'Via Foro Romano 2 00186 Roma Roma Rm')]");
public By luceNumeroValue = By.xpath("//div[@class='label-container luce']/descendant::span[contains(text(),'310494829')]");
public By luceEyeIcon = By.xpath("//div[@class='label-container luce']/descendant::span[@class='icon-info-circle']");

public By gasIndrizzoLabel = By.xpath("//div[@class='label-container gas']/descendant::span[@class='indirizzo']/child::span[contains(text(),'Indirizzo della')]");
public By gasNumeroLabel = By.xpath("//div[@class='label-container gas']/descendant::span[contains(text(),'Numero Cliente')]");
public By gasIndrizzoValue = By.xpath("//div[@class='label-container gas']/descendant::span[@class='indirizzo']/child::span[contains(text(),'Via Treves 38 D 20090 Trezzano Sul Naviglio T')]");
public By gasNumeroValue = By.xpath("//div[@class='label-container gas']/descendant::span[contains(text(),'665344836')]");

public By gasEyeIcon = By.xpath("//div[@class='label-container gas']/descendant::span[@class='icon-info-circle']");
public By gasEyeIcon1 = By.xpath("//div[@class='label-container gas']/descendant::span[@class='icon-ckbox']");

public By eyeIconPopupHeader = By.xpath("//div[@id='modalAlert']/descendant::h3[contains(text(),'Diritto di Portabilità')]");
public By eyeIconPopupContent = By.xpath("//div[@id='modalAlert']/descendant::p[contains(text(),'Hai già effettuato')]");
public By eyeIconPopupClosebtn = By.xpath("//div[@id='modalAlert']/descendant::button[@class='modal-close inline-icon-link']");


public By esciButton = By.xpath("//span[text()='ESCI']/parent::button[@class='button_second']");
public By richiediButton = By.xpath("//span[text()='RICHIEDI I TUOI DATI']/parent::button[@class='button_first ciaooo']");
public By operazioneTitlte = By.xpath("//div[@class='resultBox-container']/descendant::h1[contains(text(),'Operazione eseguita')]");
public By operazioneContent = By.xpath("");
public By fineButton = By.xpath("//span[text()='FINE']/parent::button[@class='button_first']");

public By popupAttenzione = By.xpath("//div[@class='modal-dialog']/descendant::h3[text()='Attenzione']");
public By popupAttenzioneTitle = By.xpath("//div[@class='modal-dialog']/descendant::p[contains(text(),'Sei sicuro di')]");
public By popupNoButton = By.xpath("//span[text()='NO']/ancestor::div[@class='button-container']/child::button[@class='button_second']");
public By popupSIButton = By.xpath("//span[text()='SI']/ancestor::div[@class='button-container']/child::button[@class='button_first']");




public By tuoDirittiQues1 = By.xpath("//section[@id='landing-text']/descendant::h1[contains(text(),'Come posso esercitare')]");
public By tuoDirittiAns1 = By.xpath("//section[@id='landing-text']/descendant::h2[contains(text(),'Se vuoi esercitare uno')]");
public By diPortabilitàTitle = By.xpath("//section[@id='landing-text']/descendant::div[@class='faq-element parbase']//h3[contains(text(),'Diritto di Portabilità')]");
public By diPortabilitaLastAns = By.xpath("//h3[text()='Diritto di Portabilità']/following-sibling::p[contains(text(),'possibile richiedere')]");
public By diPortabilitaLastQues = By.xpath("//h3[text()='Diritto di Portabilità']/following-sibling::h4[contains(text(),'Come posso')]");
public By diPortabilitaContent = By.xpath("//div[@class='list-form-container']/child::h2[contains(text(),'Seleziona una')]");

public By serviziLink = By.xpath("//a[@id='servizi']/ancestor::ul[@id='internal-menu']//span[text()='Servizi']");
public By bolletteLink = By.xpath("//a[@id='servizi']/ancestor::ul[@id='internal-menu']//span[text()='Bollette']");
public By dirritoContent1 = By.xpath("//span[@class='icon-info-circle']/parent::div[@class='dsc-icon-container']/following::p[contains(text(),'Non hai ancora')]");
public By dirritoContent2 = By.xpath("//div[@class='magenta alert-box blue-alert']/child::p[contains(text(),'Se hai già')]");
public By scaricaLinkTitle = By.xpath("//div[@class='list-form-container']/child::span[contains(text(),'Se vuoi salvare')]");
public By richiediTitle = By.xpath("//div[@class='resultBox-container']/descendant::h1[contains(text(),'Operazione eseguita')]");
public By richiediContent1 = By.xpath("//div[@class='resultBox-container']/descendant::p[contains(text(),'Riceverai entro')]");
public By richiediContent2 = By.xpath("//div[@class='resultBox-container']/descendant::p[contains(text(),'Quando disponibili')]");

public By homePageTitle = By.xpath("//div[@id='mainProspect']/descendant::h1[contains(text(),'Benvenuto nella')]");
public By homePageContent = By.xpath("//div[@id='mainProspect']/descendant::p[contains(text(),'Entra nel mondo')]");

public By homepageBollette = By.xpath("//div[@id='mainContentWrapper']/child::div[@class='text parbase']");
public By benvenutoHeader = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Benvenuto nella tua area privata')]");
public By benvenutoContent1 = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'In questa sezione')]");
public static final String BENVENUTO_CONTENT1 = "In questa sezione potrai gestire le tue forniture";
public By LetuFornitureHeader = By.xpath("//section[@id='lista-forniture']/descendant::h2[contains(text(),'Le tue forniture')]");
public static final String LETUEFORNITURE_HEADER = "Le tue forniture";
public By LetueFornitureContent = By.xpath("//section[@id='lista-forniture']/descendant::p[contains(text(),'Visualizza le')]");
public static final String LETUEFORNITURE_CONTENT = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette";
public static final String LETUEFORNITURE_CONTENT1 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
//Private Area Page
public By FornitureBollette=By.xpath("//h2[contains(text(),'Forniture e bollette')]");
public By ProvaAliasBSN=By.xpath("//b[contains(text(),'prova alias bsn')]");
public By FornituraLuceCliente=By.xpath("//div[@class='panel panel-default panel-primary']//h4[1]");
public By Dettaglio=By.xpath("//h5[contains(text(),'La tua fornitura nel dettaglio.')]");
public By Luogo = By.xpath("//*[@id='collapseOne']/div[1]/div[2]/div[1]/span[1]");
public By Tiplogia = By.xpath("//*[@id='collapseOne']/div[1]/div[2]/div[1]/span[1]");
public By Offerata=By.xpath("//*[@id='collapseOne']/div[1]/div[2]/div[2]/span[1]");
public By Stato=By.xpath("//*[@id='collapseOne']/div[1]/div[2]/div[2]/span[2]");
public By BolletteArrow=By.xpath("//*[@id='tue_bollette']/div[4]/div/a");


//Bills Details Page

public By LeTueBollette=By.xpath("//h1[contains(text(),'Le tue Bollette')]");
public By BolletteContent = By.xpath("//p[contains(text(),'Se cerchi una bolletta in particolare usa i filtri')]");
public By BolletteContent2=By.xpath("//p[@id='inviaDocumentiLab']");
public By InviaDocumentiBtn=By.xpath("//*[@id='inviaDocumenti']/a");
public By MostraFiltriBtn=By.xpath("//a[@name='Filtri']");
public By EsportaExcelBtn=By.xpath("//*[@id='excel-export']");

public By accediAMyEnelLabel=By.xpath("//div[@class='login-details']//h1[@aria-label='accedi a myenel']");		

//LeftMenu List
public By LeftMenuList = By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]");
public By BusinLeftMenuList = By.xpath("//div[@id='sticky-wrapper']/nav/ul/div/li");
public By BusinessHomePage = By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a");

public By orderedList = By.xpath("//a[contains(@class,'href-sf-prevent-default inline-icon') and (not(@style) or @style='')]");
public String itemXpath = "//ul[@id='internal-menu']//span[text()='$itemName$']/parent::a";
public By fornitureCard1 = By.xpath("//section[@id='lista-fornitureProspect']//div[@class='card prospect'][1]/div[@class='card-nuovaFornitura']");
public By fornitureCard2 = By.xpath("//section[@id='lista-fornitureProspect']//div[@class='card prospect'][2]/div[@class='card-nuovaFornitura']");
public By fornitureCard3 = By.xpath("//section[@id='lista-fornitureProspect']//div[@class='card prospect'][3]/div[@class='card-nuovaFornitura']");
public By novitaSection1 = By.xpath("//li[@id='appPromo']/span[2]/h4[text()='È arrivato il Black Friday']");
public By novitaSection2 = By.xpath("//div[@class='preposition-generic-module parbase'][2]/li[@id='appPromo']/span[2]/a");
public By novitaSection3 = By.xpath("//div[@class='preposition-generic-module parbase'][3]/li[@id='appPromo']/span[2]/a");
public By novitaSection4 = By.xpath("//div[@class='preposition-generic-module parbase'][4]/li[@id='appPromo']/span[2]/a");
public By supportoRegLink = By.xpath("//span[@class='dsc-icon-line-solar']/following-sibling::span[text()='Supporto']");

public By autenticazone = By.xpath("//div[@id='autenticazioneForStep1']/child::h1[contains(text(),'Autenticazione')]");
public By staiEffettuando = By.xpath("//div[@id='autenticazioneForStep1']/child::h2[contains(text(),'Stai effettuando')]");
public By travoSpazioEnel = By.xpath("//span[@class='dsc-icon-line-pin']/following-sibling::span[contains(text(),'Trova Spazio Enel')]");
public By enelPremia = By.xpath("//span[@class='dsc-icon-enelpremiawow']/following-sibling::span[contains(text(),'enelpremia WOW!')]");
public By esciMenuLink = By.xpath("//a[@id='disconnetti']/descendant::span[text()='Esci']");

public static final String AUTENTICAZONe = "Autenticazione";
public static final String STAIEFFETTUANDO = "Stai effettuando il primo accesso in area privata";


public PrivateAreaPageComponent(WebDriver driver) throws Exception {
	this.driver = driver;
	util = new SeleniumUtilities(this.driver);
	
	}

	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	public void SwitchToWindow() throws Exception
	{
		Set<String> windows = driver.getWindowHandles();
		
		
		for (String winHandle : windows) {
		/*	System.out.println(winHandle);
			System.out.println("switched to "+driver.getTitle()+"  Window");
			System.out.println(driver.getCurrentUrl());
		*/	
	       // String pagetitle = driver.getTitle();
	        driver.switchTo().window(winHandle);
	   /*     System.out.println(winHandle);
	        System.out.println("After switch"+ driver.getCurrentUrl());
	        System.out.println(driver.getTitle());
		*/    
		 }
	}
	
	 public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 100);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
		
	}
	public void verifyButtonDisabled(By checkObject) throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		 boolean b = false;
		
		 if(!driver.findElement(checkObject).isEnabled()) {
			 b=true;
			//System.out.println(b) ;
			 }
		 else
			 throw new Exception("Button is enabled");
		 
	}

	
	public void compareLabelValue(By label, String property) throws Exception {
		String lab = driver.findElement(label).getText().trim();
		lab = lab.replaceAll("\\s+", " ");
		//System.out.println(lab);
		String result = "NO";
		if(lab.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(lab+" Text is not matching with "+property);
		
	}

	public void homepageRESIDENTIALMenu(By checkObject) throws Exception {
	
    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
                   
    String item_found="NO";
    List<WebElement> leftMenu = driver.findElements(By.xpath("//div[@id='sticky-wrapper']/nav/ul/div/li"));
   
    for(WebElement element : leftMenu){
    String description=element.getText();
    description= description.replaceAll("(\r\n|\n)", " ");
       
    if (description.contentEquals("Forniture"))
        item_found="YES";
   
    else if(description.contentEquals("Bollette"))
        item_found="YES";
    else if (description.contentEquals("Servizi"))
        item_found="YES";
    else if (description.contentEquals("enelpremia WOW!"))
        item_found="YES";
    else if (description.contentEquals("Novità"))
        item_found="YES";
    else if (description.contentEquals("Spazio Video"))
        item_found="YES";
    else if (description.contentEquals("Account"))
        item_found="YES";
    else if (description.contentEquals("I tuoi diritti"))
        item_found="YES";
    else if (description.contentEquals("Supporto"))
        item_found="YES";
    else if (description.contentEquals("Trova Spazio Enel"))
        item_found="YES";
    else if (description.contentEquals("Esci"))
    	item_found="YES";
   
        if (item_found.contentEquals("NO"))
        throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'Forniture', 'Bollette','Servizi','enelpremia WOW!','Novità','Spazio Video','Account','I tuoi diritti','Supporto','Trova Spazio Enel','Esci'");
        
        
            }
    
        }
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			System.out.println(message);
			throw new Exception(message);
		}
	}

	public void verifyComponentVisibility(By visibleObject) throws Exception {
	if (!util.verifyVisibility(visibleObject, 15)) {
		throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}


	public void clickComponent(By clickableObject) throws Exception {
	util.objectManager(clickableObject, util.scrollToVisibility, false);
	util.objectManager(clickableObject, util.scrollAndClick);
	
	}

	public void BusinessHomePageMenu(By BusinessObject) throws Exception {
	
    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(BusinessObject));
                   
    String item_found="NO";
    List<WebElement> BusiLeftMenu = driver.findElements(By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a"));
   
    for(WebElement element : BusiLeftMenu){
    String description=element.getText();
    description= description.replaceAll("(\r\n|\n)", " ");
       
    if (description.contentEquals("HOMEPAGE"))
        item_found="YES";
   
    else if(description.contentEquals("BOLLETTE"))
        item_found="YES";
    else if (description.contentEquals("FORNITURE"))
        item_found="YES";
    else if (description.contentEquals("SERVIZI"))
        item_found="YES";
    else if (description.contentEquals("MESSAGGI"))
        item_found="YES";
    else if (description.contentEquals("STATO RICHIESTE"))
        item_found="YES";
    else if (description.contentEquals("REPORT BILLING MANAGEMENT"))
        item_found="YES";
    else if (description.contentEquals("CARICA DOCUMENTI"))
        item_found="YES";
    else if (description.contentEquals("CONTATTI"))
        item_found="YES";
    else if (description.contentEquals("LOGOUT"))
    	item_found="YES";
   		   
        if (item_found.contentEquals("NO"))
        throw new Exception("on the 'https://www-colla.enel.it/ Business left menu section are not present with description 'HomePage', 'FORNITURE','SERVIZI','MESSAGGI','STATO RICHIESTE','REPORT BILLING MANAGEMENT','CARICA DOCUMENTI','CONTATTI','LOGOUT'");
        
        
            }
    
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	
	String textfield_found="NO";
	
	WebElement element = driver.findElement(checkObject);
	
	String actualtext = element.getText();
	
	
	if (actualtext.contentEquals(Value))
	textfield_found="YES";
					
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected text" + Value + " is not found:");
			}


	public void verifyComponentExistence(By existingObject) throws Exception {
	if (!util.exists(existingObject, 20))
		throw new Exception("object with xpath " + existingObject + " is not exist.");
	}

	public void clickComponentIfExist(By existObject) throws Exception {
	if (util.exists(existObject, 15)) {
		util.objectManager(existObject, util.scrollToVisibility, false);
	    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
		
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
	util.objectManager(insertObject, util.sendKeys, valore);
	}

	public void backBrowser(By checkObject) throws Exception {
	    try {
	    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    	driver.navigate().back();
	    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                
	    } catch (Exception e) {
	        throw new Exception("Previous Page not displayed");
	    }
	}
	
	public void verifyMenuItemsPresenceAndOrder(String menuItemsString) throws Exception {
		populateMenuItemsVector(menuItemsString);
		List<WebElement> orderedWebElements = util.waitAndGetElements(this.orderedList);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
				for(WebElement we : menuItemsList){
			if(!orderedWebElements.contains(we))
				throw new Exception("La voce di menù "+we.getText()+" non è contenuta nel menù.");
		}
	}
	
	private void populateMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(itemXpath.replace("$itemName$", item))));
	}
	
	public void switchToNewTab() throws Exception {
		Set <String> windows = driver.getWindowHandles();
		for (String winHandle : windows) {
			driver.switchTo().window(winHandle);
		}
	}
	
	public void switchToOriginaTab(String currentWindowHandle) throws Exception {
		
		driver.switchTo().window(currentWindowHandle);
	}
	
	public void verifyAccountDetails(By checkObject, String[] Feild, String[] Value) throws Exception{
		
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmActual = new HashMap<String, String>();
		String key, val ;
		List <WebElement> list  = driver.findElements(profileFields);
		for (int i=1; i<=list.size();i++)
		{
			key = driver.findElement(By.xpath("//div[@id='profile']/dl[@class='test']/span["+ i +"]/dt")).getText();
			val = driver.findElement(By.xpath("//div[@id='profile']/dl[@class='test']/span["+ i +"]/dd")).getText();
			hm.put(key, val);
			}
		for (int i=0; i<Feild.length;i++){
		hmActual.put(Feild[i], Value[i]);}
		if(!hm.equals(hmActual))
    		throw new Exception ("The account details are not correctly displayed");
		}
	
	public static final String BENVENUTO_TITLE = "Benvenuto nella tua area privata";
	public static final String BENVENUTO_CONTENT = "In questa sezione potrai gestire le tue forniture";

	public static final String FORNITURE_HEADER = "Le tue forniture";
	public static final String FORNITURE_CONTENT = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette";

	public static final String TUOIDIRITTI_QUES1 = "Come posso esercitare i diritti sui miei dati personali in base a quanto stabilito dal General Data Protection Regulation (GDPR)?";
	public static final String TUOIDIRITTI_ANS1 = "Se vuoi esercitare uno o più diritti riconosciuti dal GDPR puoi farlo seguendo le indicazioni riportate di seguito.";
	
	public static final String DIPORTABILITA_TITLE = "Diritto di Portabilità";
	public static final String DIPORTABILITA_LASTQUES = "Come posso richiedere una copia dei miei dati forniti ad Enel Energia?";
	public static final String DIPORTABILITA_LASTANS1 = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato."
													+"Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno"
													+" inviati alla email fabiana.manzo2@yopmail.com."
													+ "Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Account prima di procedere.";
	public static final String DIPORTABILITA_LASTANS = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato."
			+"Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno"
			+" inviati alla email majdouline_182004@yahoo.it."
			+ "Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Account prima di procedere.";

	public static final String LUCEDIPORTABILITA_LASTANS1 = "Come posso richiedere una copia dei miei dati forniti ad Enel Energia? È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato. Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla email non presente. Se desideri aggiornarla o l'email risulta non presente vai alla Sezione Account prima di procedere.";
	public static final String LUCEDIPORTABILITA_LASTANS = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla email\"non presente\".Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String GASDIPORTABILITA_LASTANS = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailperftest002@gmail.com.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String GASDIPORTABILITA_LASTANS1 = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla email#email#.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String DIPORTBILITTA_CONTENT = "Seleziona una o più forniture sulla quale intendi richiedere una copia dei tuoi dati.";
	public static final String HOMEPAGE_BOLLETTE = "Le tue bollette"
													+" Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.";

	public static final String DIRRITO_CONTENT1 = "Non hai ancora forniture attive, puoi comunque richiedere una copia dei tuoi dati.";
	public static final String DIRRITO_CONTENT2 = "Se hai già effettuato la richiesta, riceverai una email contenente una copia dei tuoi dati. Quando disponibili i tuoi dati saranno scaricabili in questa sezione.";
	public static final String SCARICALINK_TITLE = "Se vuoi salvare la copia dei tuoi dati già richiesti scarica il file";
	public static final String ATTENZIONE = "Attenzione";
	public static final String ATTENZIONE_TITLE = "Sei sicuro di voler uscire da questa sezione?";
	public static final String DATICONTATTIHEADING = "Dati e Contatti";
	public static final String DATICONTATTICONTENT = "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione";

	public static final String NOVITA_HEADING = "Novità";
	public static final String NOVITA_CONTENT = "In questa sezione potrai visualizzare tutte le offerte e i vantaggi offerti da Enel Energia";

	public static final String BEVENUTO_PRIVATA = "Benvenuto nella tua area privata";
	public static final String BEVENUTOPRIVATA_CONTENT = "Entra nel mondo di Enel Energia e scopri le nostre offerte.";
	public static final String TRAVOSPAZIO_HOME = "HomeSpazio Enel";
	public static final String TRAVOSPAZIO_HOMENew = "HomeSpazio Enel"; 
	public static final String TRAVOSPAZIO_TITLE = "Trova lo Spazio Enel più vicino a te";
	public static final String SERVIZI_TITLE = "Servizi per le forniture";
	public static final String SERVIZI_CONTENT = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
	public By serviziTitle = By.xpath("//section[@class='tile-container']/descendant::h2[contains(text(),'Servizi per le forniture')]");
	public By serviziContent = By.xpath("//h2[text()='Servizi per le forniture']/following-sibling::p[contains(text(),'Di seguito potrai')]");
	public static final String EYEICONPOPUP_HEADER = "Diritto di Portabilità";
	public static final String EYICONPOPUP_CONTENT = "Hai già effettuato la richiesta. Riceverai una email contenente una copia dei tuoi dati. Quando disponibili i tuoi dati saranno anche scaricabili in questa sezione e per effettuare una nuova richiesta dovrai attendere qualche giorno.";
	
	public static final String PRIVATE_AREA_MENU_RES = "Forniture;enelpremia WOW!;Novità;Chiamaci;Account;I tuoi diritti;Supporto;Trova Spazio Enel.;Esci";
	public static final String FORNITURE_HEADING = "Benvenuto nella tua area privata";
	public static final String FORNITURE_PARAGRAPH = "Entra nel mondo di Enel Energia e scopri le nostre offerte.";
	public static final String FORNITURE_CARD1 = "Visualizza le offerte Luce e Gas Luce e Gas insieme per la tua casa";
	public static final String FORNITURE_CARD2 = "Visualizza le offerte Luce Dai più luce alla tua casa con Enel Energia";
	public static final String FORNITURE_CARD3 = "Visualizza le offerte Gas Riscalda la tua casa con Enel Energia";
	
	public static final String[] PROFILE_FIELDS = {"Titolare","Codice Fiscale","Telefono Fisso","Numero di Cellulare","Email","PEC","Canale di contatto preferito"};
	public  final String[] PROFILE_VALUES = {"Rita Casa","CSARTI80A41H501D","3920033003","+39 3920033003","rita.mastellone@accenture.com","Non presente","Email"};
	
}
