package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaOffertaInviataComponent extends BaseComponent{
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;
	
	public By sezioneNavigazioneProcesso = By.xpath("//span[text()='Navigazione processo']");
	public By sezioneNavigazioneProcessoTipo = By.xpath("//div[@role='list']//span[text()='Tipo']");
	public By sezioneNavigazioneProcessoTipoValue = By.xpath("//div[@role='list']//span[@title='VOLTURA CON ACCOLLO']");
	public By sezioneNavigazioneProcessoCreatoIl = By.xpath("//span[text()='Creata il']");
	public By sezioneNavigazioneProcessoStato = By.xpath("//div[@class='slds-grid slds-gutters_small slds-align_absolute-center']//span[text()='Stato']");
	public By sezioneNavigazioneProcessoStatoValue = By.xpath("//div[@role='list']//span[@title='Chiuso']");
	
	    public VerificaOffertaInviataComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}	
	public void verificaElement(By path, String value) throws Exception{
		
		String text = util.waitAndGetElement(path).getText();
				//util.waitAndGetElement(path,true).getAttribute("value");
		//if(!textModal.contentEquals(popupText)) throw new Exception("Dopo aver confermato il processo viene mostrata una popup modale ma non contiene il testo : "+popupText);
		if(text.compareTo(value)!=0)  throw new Exception("Il valore atteso: " +value +"non corrisponde al valore mostrato: " +text);	
	}
	
	public void verificaElementExist(By path) throws Exception{
		
		if(!util.verifyExistence(path, baseTimeoutInterval)){ 
			throw new Exception("Impossibile trovare l'elemento: " +path);
	}	
	}
			 
}
