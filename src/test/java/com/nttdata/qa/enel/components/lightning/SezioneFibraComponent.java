package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SezioneFibraComponent extends BaseComponent{
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;
    
 public By sezioneFibra= By.xpath("//h2//span[text()='Fibra']");
 public By ProvinciaSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input");
 public By ComuneSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input");
 public By IndirizzoSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input");
 public By CivicoSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']");
 public By consensiSezioneFibraNo = By.xpath("//*[text()='Fibra']/ancestor::div[@role='region']//span[text()='NO']/ancestor::label/span[@class='slds-radio_faux']");
 public By ConsensTrattDatiPersonSezFibra = By.xpath("//*[text()='Fibra']/ancestor::div[@role='region']//span[text()='Consenso al trattamento dei dati personali da parte di terzi']/ancestor::label/span[@class='slds-checkbox_faux']");
 public By CellulareSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='Cellulare']");
 public By ConfermaCellulareSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='Conferma Cellulare']//..//input");
 public By EmailSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//label[text()='Email']"); 
 public By VerificaButtonSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//button[text()='Verifica']");
 public By ConfermaButtonSezioneFibra = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//button[text()='Conferma']");
 public By IndirizzoVerificatoButton = By.xpath("//h2/span[text()='Fibra']/ancestor::div[@role='region']//span[text()='Indirizzo verificato']");
 public By bannerCopertura = By.xpath("//div[@id='coverageBannerIndirizzo di Fornitura Fibra']");
 public By rinunciaFibra = By.xpath("//div[@data-label='without_fiber']/..//input");
 public By AccettoConsensiSezioneFibra = By.xpath("//*[text()='Fibra']/ancestor::div[@role='region']//span[text()='NO']/ancestor::label/span[@class='slds-radio_faux']");
 
 public SezioneFibraComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}

	
		public void popolaCellulareSezioneFibra(String value) throws Exception {
			String valore = util.waitAndGetElement(CellulareSezioneFibra,true).getAttribute("value");
			if (valore.contentEquals(""))	 {
				this.popolareCampo(value, CellulareSezioneFibra);	
			}
		}
		
		public void popolaConfermaCellulareSezioneFibra(String value) throws Exception {
			String valore = util.waitAndGetElement(ConfermaCellulareSezioneFibra,true).getAttribute("value");
			if (valore.contentEquals(""))	 {
				this.popolareCampo(value, ConfermaCellulareSezioneFibra);	
			}
		}
		
		
		public void popolaEmailSezioneFibra(String mail) throws Exception {
			String valore = util.waitAndGetElement(EmailSezioneFibra,true).getAttribute("value");
			if (valore.contentEquals(""))	 {
				this.popolareCampo(mail, EmailSezioneFibra);	
			}
		}
		 
		 public void popolareCampo(String valore, By object) throws Exception{
				TimeUnit.SECONDS.sleep(1);
				WebElement element = util.waitAndGetElement(object,60,2);
				util.objectManager(object, util.sendKeys, valore);		
				TimeUnit.SECONDS.sleep(1);
				element.sendKeys(Keys.ENTER);
		 }
		 
		 public void verificaBanner(By object, String value) throws Exception {
			  util.checkElementText(object, value);
		 }
}
