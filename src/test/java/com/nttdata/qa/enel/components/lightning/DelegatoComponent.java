package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class DelegatoComponent extends BaseComponent{
	WebDriver driver;
	SeleniumUtilities util;
	private SpinnerManager spinnerManager;
	
	
    public By tipologiaDelegato=By.xpath("//span[text()='Tipologia delegato']/ancestor::div[2]//lightning-formatted-text[text()='Altro']");
    public By tipologiaDelega=By.xpath("//span[text()='Tipologia delega']/ancestor::div[2]//lightning-formatted-text[text()='Delega light']");
    public String nomeDelegato="//span[text()='Nome delegato']/ancestor::div[2]//lightning-formatted-text[text()='#']";
    public String cognomeDelegato="//span[text()='Cognome delegato']/ancestor::div[2]//lightning-formatted-text[text()='#']";
    public String cf_delegato="//span[text()='Codice fiscale delegato']/ancestor::div[2]//lightning-formatted-text[text()='#']";
    public String carta_Identita="//span[text()='Numero carta di identità delegato']/ancestor::div[2]//lightning-formatted-text[text()='#']";
    public By telefono=By.xpath("//span[text()='Telefono fisso delegato']");
    public By cellulare=By.xpath("//span[text()='Celluare delegato']");
	
	
	public DelegatoComponent(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}
	
	public void checkSezioneDelegato(String n,String c, String codicefiscale, String cartaIdentita)throws Exception {
		verifyComponentExistence(tipologiaDelegato);
		verifyComponentExistence(tipologiaDelega);
		verifyComponentExistence(telefono);
		verifyComponentExistence(cellulare);
		
		
		String xpathString1 = nomeDelegato.replace("#", n);
		By item1 = By.xpath(xpathString1);
		if (!util.verifyExistence(item1, 150)) {
			throw new Exception("l'oggetto con xpath " + item1 + " non esiste.");
		   }
		
		String xpathString2 = cognomeDelegato.replace("#", c);
		By item2 = By.xpath(xpathString2);
		if (!util.verifyExistence(item2, 150)) {
			throw new Exception("l'oggetto con xpath " + item2 + " non esiste.");
		   }
		
		String xpathString3 = cf_delegato.replace("#", codicefiscale);
		By item3 = By.xpath(xpathString3);
		if (!util.verifyExistence(item3, 150)) {
			throw new Exception("l'oggetto con xpath " + item3 + " non esiste.");
		   }
		String xpathString4 = carta_Identita.replace("#", cartaIdentita);
		By item4 = By.xpath(xpathString4);
		if (!util.verifyExistence(item4, 150)) {
			throw new Exception("l'oggetto con xpath " + item4 + " non esiste.");
		   }
	}
}
