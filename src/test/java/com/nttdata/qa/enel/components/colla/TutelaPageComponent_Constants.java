package com.nttdata.qa.enel.components.colla;

public class TutelaPageComponent_Constants {
	
	public static final String[] pagePresentationStrings = {
			"HOME",
			"SUPPORT SERVICES",
			"GAS PROTECTION SERVICE DEAL",
			"Tutela gas service cost and advantages",
			"Economic conditions for the supply of gas"
	};
	public static final String whatItIs = "What it is";
	public static final String economicConditions = "Economic conditions";
	public static final String price  = "Price";
	public static final String documents = "Documents";
	public static final String[] pageContentStrings = {
			"What is the gas protection service?",
			"The gas protection service deal provides for the application of the economic and contractual terms and conditions for the supply of gas regulated by the Authority for Electricity, Gas and the Water System, pursuant to resolution 64/09 and resolution 229/01",
			"Who can subscribe to the service?",
			"The following are entitled to gas protection (regardless of their supplier and market):",
			"all domestic customers",
			"apartment buildings with domestic use and annual consumption of less than 200,000 scm",
			"Gas supply tarif details according to location",
			"Select the location to see the gas tariff details."
	};
	public static final String[] locationStrings = {
			"Location",
			"Mandatory fields *"
	};
	public static final String errorMessage = "Mandatory fields *";
	public static final String condizEconomGasTitle = "Condizioni economiche di fornitura gas naturale ai clienti finali";
	public static final String location = "Location:";
	public static final String condizEconomBody = "Le presenti condizioni economiche, in conformità al TESTO INTEGRATO DELLE ATTIVITA DI VENDITA AL DETTAGLIO DI GAS NATURALE E GAS DIVERSI DA GAS NATURALE DISTRIBUITI A MEZZO RETI URBANE (TIVG), approvato dalla delibera ARG/GAS 64/09 dell'Autorità per l'energia elettrica e il gas e successive modifiche ed integrazioni, si applicano ai Clienti finali secondo l'articolo 2, comma 3, del medesimo provvedimento.\n" + 
			"\n" + 
			"Le condizioni economiche di fornitura gas che l'esercente la vendita deve offrire ai clienti del servizio di tutela sono date dalla somma delle seguenti componenti:\n" + 
			"\n" + 
			"a) componenti relative al servizio di distribuzione, misura e relativa commercializzazione, e oneri di sistema;\n" + 
			"\n" + 
			"b) componente relativa al servizio di trasporto (QTI);\n" + 
			"\n" + 
			"c) componenti relative agli Oneri di Gradualità e componente CPR;\n" + 
			"\n" + 
			"d) componente relativa alla commercializzazione all'ingrosso (Materia Prima Gas);\n" + 
			"\n" + 
			"e) componente relativa alla commercializzazione della vendita al dettaglio (QVD);\n" + 
			"\n" + 
			"f) = a) + b) + c) + d) + e)";
	public static final String fasceScaglioni = "Scaglioni di consumo (fasce)";
	public static final String scaglioniConsumo ="Scaglioni a consumo";
	public static final String[][] scaglioniTableStrings= {
			{"Fasce", "da Smc/anno", "a Smc/anno"},
			{"1", "0", "120"},
			{"2", "121", "480"},
			{"3", "481", "1.560"},
			{"4", "1.561", "5.000"},
			{"5", "5.001", "80.000"},
			{"6", "80.001", "200.000"},
			{"7", "200.001", "1.000.000"},
			{"8", "1.000.001", "oltre"}
	};
	public static final String scaglioniTableHeaderElements = "//*[@id=\"table1\"]/table/tbody/tr[1]/th[#]";
	public static final String scaglioniTableBodyElements = "//*[@id=\"table1\"]/table/tbody/tr[#]/td[§]";
	public static final String componenteTariffaria = "Componente tariffaria della distribuzione misura e relativa commercializzazione: a)";
	public static final String[][] compTarifTableStrings_Roma = {
			{"Fasce", "Quote fisse", "Quota variable (Tau3)", "UG1,UG2,UG3,RE,RS,GS"},
			{"", "Euro/cliente/anno", "Euro/Smc",	"Euro/Smc"},
			{"1", "49,04", "0,000000", "0,024881"},
			{"2", "49,04", "0,143577", "0,069981"},
			{"3", "49,04", "0,131412", "0,051081"},
			{"4", "49,04", "0,131965", "0,045881"},
			{"5", "49,04", "0,098605", "0,039581"},
			{"6", "49,04", "0,049948", "0,030381"},
			{"7", "49,04", "0,024513", "0,013311"},
			{"8", "49,04", "0,006819", "0,013311"}
	};
	public static final String[][] compTarifTableStrings_Milano = {
			{"Fasce", "Quote fisse", "Quota variable (Tau3)", "UG1,UG2,UG3,RE,RS,GS"},
			{"", "Euro/cliente/anno", "Euro/Smc", "Euro/Smc"},
			{"1", "28,74", "0,000000", "0,025361"},
			{"2", "28,74", "0,060022", "0,069961"},
			{"3", "28,74", "0,054936", "0,051061"},
			{"4", "28,74", "0,055167", "0,045861"},
			{"5", "28,74", "0,041221", "0,039561"},
			{"6", "28,74", "0,020880", "0,030361"},
			{"7", "28,74", "0,010248", "0,016011"},
			{"8", "28,74", "0,002851", "0,016011"}
	};
	public static final String[][] compTarifTableStrings_Napoli = {
			{"Fasce", "Quote fisse", "Quota variable (Tau3)", "UG1,UG2,UG3,RE,RS,GS"},
			{"", "Euro/cliente/anno", "Euro/Smc", "Euro/Smc"},
			{"1", "46,01", "0,000000", "0,025361"},
			{"2", "46,01", "0,139945", "0,069961"},
			{"3", "46,01", "0,128088", "0,051061"},
			{"4", "46,01", "0,128627", "0,045861"},
			{"5", "46,01", "0,096111", "0,039561"},
			{"6", "46,01", "0,048684", "0,030361"},
			{"7", "46,01", "0,023893", "0,016011"},
			{"8", "46,01", "0,006647", "0,016011"}
	};
	public static final String compTarifHeader1Elements = "//*[@id=\"table2\"]/table/tbody/tr[1]/th[#]";
	public static final String compTarifHeader2Elements = "//*[@id=\"table2\"]/table/tbody/tr[2]/th[#]";
	public static final String compTarifBodyElements = "//*[@id=\"table2\"]/table/tbody/tr[#]/td[§]"; 
	public static final String altreComponenti = "Altre componenti: b), c), d), e)";
	public static final String tariffaRisultante = "Tariffa risultante al netto delle imposte: f)";
	public static final String[][] altreComponentiTableStrings_Roma = {
			{"Fasce", "b)", "c))", "d)", "e)", "f)"},
			{"", "Trasporto", "Oneri di Gradualità", "Materia Prima Gas", "Vendita al dettaglio", "Tariffa di fornitura"},
			{"", "Euro/Smc", "Euro/Smc", "Euro/Smc", "Euro/Smc", "Euro/Smc"},
			{"1", "0,043835", "0,000000", "0,208274", "0,007946", "0,284936"},
			{"2", "0,043835", "0,000000", "0,208274", "0,007946", "0,473613"},
			{"3", "0,043835", "0,000000", "0,208274", "0,007946", "0,442548"},
			{"4", "0,043835", "0,000000", "0,208274", "0,007946", "0,437901"},
			{"5", "0,043835", "0,000000", "0,208274", "0,007946", "0,398241"},
			{"6", "0,043835", "0,000000", "0,208274", "0,007946", "0,340384"},
			{"7", "0,043835", "0,000000", "0,208274", "0,007946", "0,297880"},
			{"8", "0,043835", "0,000000", "0,208274", "0,007946", "0,280186"}
	};
	public static final String[][] altreComponentiTableStrings_Milano = {
			{"Fasce", "b)", "c))", "d)", "e)", "f)"},
			{"", "Trasporto", "Oneri di Gradualità", "Materia Prima Gas", "Vendita al dettaglio", "Tariffa di fornitura"},
			{"", "Euro/Smc", "Euro/Smc", "Euro/Smc", "Euro/Smc", "Euro/Smc"},
			{"1", "0,043591", "0,000000", "0,178150", "0,007946", "0,255048"},
			{"2", "0,043591", "0,000000", "0,178150", "0,007946", "0,359670"},
			{"3", "0,043591", "0,000000", "0,178150", "0,007946", "0,335684"},
			{"4", "0,043591", "0,000000", "0,178150", "0,007946", "0,330715"},
			{"5", "0,043591", "0,000000", "0,178150", "0,007946", "0,310469"},
			{"6", "0,043591", "0,000000", "0,178150", "0,007946", "0,280928"},
			{"7", "0,043591", "0,000000", "0,178150", "0,007946", "0,255946"},
			{"8", "0,043591", "0,000000", "0,178150", "0,007946", "0,248549"}
	};
	public static final String[][] altreComponentiTableStrings_Napoli = {
			{"Fasce", "b)", "c))", "d)", "e)", "f)"},
			{"", "Trasporto", "Oneri di Gradualità", "Materia Prima Gas", "Vendita al dettaglio", "Tariffa di fornitura"},
			{"", "Euro/Smc", "Euro/Smc", "Euro/Smc", "Euro/Smc", "Euro/Smc"},
			{"1", "0,044636", "0,000000", "0,182419", "0,007946", "0,260361"},
			{"2", "0,044636", "0,000000", "0,182419", "0,007946", "0,444906"},
			{"3", "0,044636", "0,000000", "0,182419", "0,007946", "0,414149"},
			{"4", "0,044636", "0,000000", "0,182419", "0,007946", "0,409488"},
			{"5", "0,044636", "0,000000", "0,182419", "0,007946", "0,370672"},
			{"6", "0,044636", "0,000000", "0,182419", "0,007946", "0,314045"},
			{"7", "0,044636", "0,000000", "0,182419", "0,007946", "0,274904"},
			{"8", "0,044636", "0,000000", "0,182419", "0,007946", "0,257658"}

	};
	public static final String altreCompHeader1Elements = "//*[@id=\"table3\"]/table/tbody/tr[1]/th[#]";
	public static final String altreCompHeader2Elements = "//*[@id=\"table3\"]/table/tbody/tr[2]/th[#]";
	public static final String altreCompHeader3Elements = "//*[@id=\"table3\"]/table/tbody/tr[3]/th[#]";
	public static final String altreCompBodyElements = "//*[@id=\"table3\"]/table/tbody/tr[#]/td[§]";
	public static final String nota = "Nota\n" + 
			"\n" + 
			"Per quanto riguarda la componente tariffaria della vendita, ai sensi del TIVG della AEEG, in aggiunta alla Tariffa di fornitura si applica una quota fissa pari a 58,83 Euro/cliente/anno per clienti domestici e una quota fissa pari a 77,26 Euro/cliente/anno per i condomini con uso domestico con consumo annuo non superiore a 200.000 smc, le utenze relative ad attività di servizio pubblico e i clienti non domestici con consumo annuo non superiore a 50.000 smc.\n" + 
			"Le componenti della quota fissa relativa ai servizi di Distribuzione e Misura variano in funzione della classe del contatore; pertanto quelle esposte in precedenza, riferite alle classi G4-G6, possono variare per i contatori di classi superiori.\n" + 
			"La componente GS della tariffa di distribuzione e misura, di cui all'art. 35.3 lett. d) Allegato A della delibera AEEG ARG/gas n.159/08 (RTDG), si applica a tutte le utenze di gas naturale diverse da quelle nella titolarità di clienti domestici, così come individuati ai sensi dell'articolo 2.3 lett.a della delibera ARG/gas n. 64/09 (TIVG).\n" + 
			"Le condizioni economiche sopra indicate sono applicate a tutti i clienti e sono comunque da considerarsi ancora provvisorie 'salvo conguaglio', tenuto conto degli aggiornamenti periodici secondo i criteri definiti dall'Autorità per l'energia elettrica e il gas.\n" + 
			"In relazione a tali aggiornamenti verranno applicate le condizioni economiche di fornitura risultanti, effettuando conseguentemente i dovuti conguagli.\n" + 
			"\n" + 
			"Per maggiori informazioni, consulta anche il sito dall'Autorità per l'energia elettrica e il gas.";
	public static final String areraRedirectUrl = "https://www.autorita.energia.it/it/redirect.htm?/it/index.htm";
	public static final String[] economicConditionsStrings = {
			"Economic conditions",
			"The economic conditions applied to supplies under the gas protection service are fixed periodically by ARERA based on the provisions of the integrated text for retail gas sales activities (Annex A to Resolution ARERA ARG/gas 64/09).",
			"The economic conditions envisage the application of fees for:",
			"Sales services",
			"Gas Raw Material",
			"QVD - Retail sales quota",
			"QOA - Fee for additional charges",
			"Fee covering scaling charges",
			"Network services",
			"QTI - Costs and charges for transport services",
			"Distribution fee",
			"Gas supply tarif details according to location",
			"Select the location to see the gas tariff details."
	};
	public static final String[] priceStrings = {
			"Gas sale price",
			"The gas price components are the tariff, excise duties and taxes.\n" + 
			"\n" + 
			"The tariff is set by ARERA and covers the costs for purchase of the gas, transportation and storage, and distribution and sales activities.\n" + 
			"\n" + 
			"The calculation of the price for end customers depends on taxation (excise duties and taxes):",
			"excise duty: this is expressed in €/scm and is divided into different rates, according to annual consumption classes and geographical area",
			"regional supplement (only if established by the Region): this is expressed in €/scm and divided into annual consumption classes. It is not applied in the regions with special statue",
			"VAT: this is expressed as a percentage and is applied to the total price, inclusive of excise duty and regional supplement.",
			"VAT differs according to the gas consumption class:",
			"10% for payments of the first 480 scm consumed in a calendar year",
			"22% for payments of consumptions in excess of 480 scm",
			"Note: 1 scm (standard cubic metre) refers to gas at a temperature of 15°C and a constant pressure of 1.01325 bar.",
			"Gas supply tarif details according to location",
			"Select the location to see the gas tariff details."
	};
	public static final String[] downloadStrings = {
			"Tutta la documentazione per approfondire",
			"Condizioni tecnico economiche Tutela gas",
			"Pdf size 0.22 mb",
			"Informazioni precontrattuali Tutela gas",
			"Pdf size 0.59 mb",
			"Modulo di adesione Tutela gas clienti residenziali",
			"Pdf size 0.16 mb",
			"Modulo di adesione Tutela Gas Condomini",
			"Pdf size 0.71 mb",
			"Modulo esercizio diritto di ripensamento",
			"Pdf size 0.07 mb",
			"Modulo reclami tutela gas",
			"Pdf size 0.1 mb",
			"Condizioni generali di fornitura clienti residenziali",
			"Pdf size 0.17 mb",
			"Condizioni generali di fornitura",
			"Pdf size 0.15 mb",
			"Gas supply tarif details according to location",
			"Select the location to see the gas tariff details."
	};
	public static final String[] downloadSubtitleStrings = {
			"I dettagli sui termini dell'offerta, il costo del gas e i servizi aggiuntivi",
			"L'offerta riassunta in 12 semplici punti"
	};
	public static final String[] downloadLinks = {
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Condizioni%20tecnico%20economiche%20tutela%20gas.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Informazioni%20precontrattuali.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20di%20adesione%20Tutela%20Gas%20res.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20di%20adesione%20Tutela%20Gas%20Condomini.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20esercizio%20diritto%20di%20ripensamento%20Tutela%20Gas.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20reclami%20tutela%20gas.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Condizioni%20generali%20di%20fornitura%20per%20clienti%20Casa.pdf",
//			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Condizioni%20generali%20di%20fornitura%20business.pdf"
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/TutelaGas_cte.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/TutelaGas_cte.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Informazioni%20precontrattuali.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20di%20adesione%20Tutela%20Gas%20res.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20di%20adesione%20Tutela%20Gas%20Condomini.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20esercizio%20diritto%20di%20ripensamento%20Tutela%20Gas.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Modulo%20reclami%20tutela%20gas.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Condizioni%20generali%20di%20fornitura%20per%20clienti%20Casa.pdf",
			"https://www-coll1.enel.it/content/dam/enel-it/documenti-supporto/offerta-tutela-gas/Condizioni%20generali%20di%20fornitura%20business.pdf"
	};
}
