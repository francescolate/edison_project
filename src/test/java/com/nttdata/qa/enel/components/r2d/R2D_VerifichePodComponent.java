package com.nttdata.qa.enel.components.r2d;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_VerifichePodComponent{
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public R2D_VerifichePodComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	public By inputPOD = By.xpath("//input[@name='pod']");
	public By inputPDR = By.xpath("//input[@name='pdr']");
	public By selectCodiceServizioDt = By.xpath("//select[@name='tipologiaRichiestaDt']");
	public By inputValidazionePreventivo = By.xpath("//input[@name='accettazionePreventivo']");
	public By inputIdRichiestaCRM = By.xpath("//input[@name='idRichiestaCrm']");
	public By buttonCerca = By.xpath("//img[@alt='Cerca'] | //*[contains(text(),'Cerca')]");
	public By buttonDettaglioPratiche = By.xpath("//img[@alt='Visualizza Dettaglio']");
//	public By rigaPratica = By.xpath("//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[2]");
	public By rigaPratica = By.xpath("//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[2]/td[1]");
	public String righeXpath = "//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr";
	public String distributorRigaPratica = "//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[$ROW_NUMBER$]/td[1]";
	public String tipoLavoroRigaPratica = "//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[$ROW_NUMBER$]/td[3]";
	public String statusRigaPratica = "//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[$ROW_NUMBER$]/td[5]";
	public String dateRigaPratica = "//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[$ROW_NUMBER$]/td[8]";
    public By flagFittizioSi=By.xpath("//input[@name='fittizio' and @value='Y' and @type='radio']");
    public By distribure = By.xpath("//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[2]/td[1]");

	public void inserisciPod(By inputPOD, String valore) throws Exception{
		util.objectManager(inputPOD,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void inserisciPdr(By inputPDR, String valore) throws Exception{
		util.objectManager(inputPDR,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void inserisciIdRichiestaCRM(By inputIdRichiestaCRM, String valore) throws Exception{
		util.objectManager(inputIdRichiestaCRM,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	public void selezionaCodiceServizioDt(By selectCodiceServizio,String valore) throws Exception{
		util.objectManager(selectCodiceServizio,util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	public void inserisciAccettazionePreventivo(By inputAccettazionePreventivo,String valore) throws Exception{
		util.objectManager(inputAccettazionePreventivo,util.sendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}

	public void cercaPod(By buttonCerca) throws Exception{
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
	public void dettaglioPratiche(By buttonDettaglio) throws Exception{
		util.objectManager(buttonDettaglio,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
	public void selezionaPratica(By elemento) throws Exception{
		util.objectManager(elemento,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
	}
	
	public String recuperaDistributore(By elemento) throws Exception{
		return driver.findElement(elemento).getText();
	}
	
	// lettura e gestione Dettaglio Pratica per GAS
	public void verificaDettaglioPratica(Properties prop,ArrayList<String> campiDaVerificare,ArrayList<String> campiDaSalvare) throws Exception{
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(5);
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		Logger.getLogger("").log(Level.INFO, "campi da verificare:");
		//Elementi da Verificare
		String campo,valoreCampo,valoreCampoAtteso="";
		for(int i=0;i<campiDaVerificare.size();i++){
			String[] elem =campiDaVerificare.get(i).split(";");
			campo=elem[0];
			valoreCampoAtteso=elem[1];
			Logger.getLogger("").log(Level.INFO, "campo:"+campo);
			try{
//				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
				valoreCampo=driver.findElement(By.xpath("//table//td[text()='"+campo+"']/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo);
			valoreCampoAtteso=elem[1];
			//cambio in contains il compare
			if(!valoreCampo.contains(valoreCampoAtteso)){
				throw new Exception("Il valore del campo: "+campo+" non corrisponde a quello atteso.Valore visualizzato: "+valoreCampo+" valore atteso: "+valoreCampoAtteso);
			}
		}
		Logger.getLogger("").log(Level.INFO, "campi da salvare:");
		//Elementi da salvare
		for(int i=0;i<campiDaSalvare.size();i++){
			campo=campiDaSalvare.get(i);
			try{
				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			campo=campo.replaceAll(" ", "_");
			Logger.getLogger("").log(Level.INFO, "campo:"+campo.toUpperCase());
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo.toUpperCase());
			prop.setProperty(campo.toUpperCase(), valoreCampo.toUpperCase());
			System.out.println("Verifica Nome Campo salvato: "+ campo.toUpperCase()+ " - Valore campo:"+valoreCampo.toUpperCase());
		}
		
//		if(prop.containsKey("CODICE_ROW-ID_CRM")) prop.setProperty("ID_RICHIESTA", prop.getProperty("CODICE_ROW-ID_CRM",prop.getProperty("OI_RICHIESTA")));	
		if (!prop.getProperty("CODICE_ROW-ID_CRM","").equals("")) prop.setProperty("ID_RICHIESTA", prop.getProperty("CODICE_ROW-ID_CRM",prop.getProperty("OI_RICHIESTA")));
//		if(prop.containsKey("DATA_INVIO_RECESSO")) prop.setProperty("DATA_RECESSO", prop.getProperty("DATA_INVIO_RECESSO").substring(0, 10));
		if (!prop.getProperty("DATA_INVIO_RECESSO","").equals("")) prop.setProperty("DATA_RECESSO", prop.getProperty("DATA_INVIO_RECESSO").substring(0, 10));
//		if(prop.containsKey("STATO_PRATICA_R2D")) prop.setProperty("STATO_R2D", prop.getProperty("STATO_PRATICA_R2D").substring(0, 2));
		if (!prop.getProperty("STATO_PRATICA_R2D","").equals("")) prop.setProperty("STATO_R2D", prop.getProperty("STATO_PRATICA_R2D").substring(0, 2));
		if (prop.getProperty("TIPO_OPERAZIONE","").equals("Richiesta N01 Allaccio")) {
			prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
//			prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Richiesta N01 Allaccio");
		}
		//Chiusura pagina attuale
		driver.close();
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		TimeUnit.SECONDS.sleep(3);
	}

	// lettura e gestione Dettaglio Pratica per GAS
	public void verificaDettaglioPratica_GAS(Properties prop,ArrayList<String> campiDaVerificare,ArrayList<String> campiDaSalvare) throws Exception{
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(5);
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		Logger.getLogger("").log(Level.INFO, "campi da verificare:");
		//Elementi da Verificare
		String campo,valoreCampo,valoreCampoAtteso="";
		for(int i=0;i<campiDaVerificare.size();i++){
			String[] elem =campiDaVerificare.get(i).split(";");
			campo=elem[0];
			valoreCampoAtteso=elem[1];
			Logger.getLogger("").log(Level.INFO, "campo:"+campo);
			try{
//				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
				valoreCampo=driver.findElement(By.xpath("//table//td[text()='"+campo+"']/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo);
			valoreCampoAtteso=elem[1];
			//cambio in contains il compare
			if(!valoreCampo.contains(valoreCampoAtteso)){
				throw new Exception("Il valore del campo:"+campo+"non corrisponde a quello atteso.Valore visualizzato:"+valoreCampo+" valore atteso:"+valoreCampoAtteso);
			}
		}
		Logger.getLogger("").log(Level.INFO, "campi da salvare:");
		//Elementi da salvare
		for(int i=0;i<campiDaSalvare.size();i++){
			campo=campiDaSalvare.get(i);
			try{
				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			campo=campo.replaceAll(" ", "_");
			Logger.getLogger("").log(Level.INFO, "campo:"+campo.toUpperCase());
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo.toUpperCase());
			prop.setProperty(campo.toUpperCase(), valoreCampo.toUpperCase());
			System.out.println("Verifica Nome Campo salvato: "+ campo.toUpperCase()+ " - Valore campo:"+valoreCampo.toUpperCase());
		}
		// aggiunti da claudio
		prop.setProperty("ID_RICHIESTA", prop.getProperty("NUMERO_ORDINE_CRM"));
		prop.setProperty("DATA_RECESSO", prop.getProperty("DATA_INVIO_RECESSO"));
		prop.setProperty("STATO_R2D", prop.getProperty("STATO_PRATICA").substring(0, 2));
//		campiDaSalvare.add("Stato Pratica");
		
		System.out.println("Valore campo Codice ROW-ID CRM: "+ prop.getProperty("ID_RICHIESTA"));
		System.out.println("Valore campo DATA_INVIO_RECESSO: "+ prop.getProperty("DATA_RECESSO"));
		System.out.println("Valore campo STATO_R2D: "+ prop.getProperty("STATO_R2D"));
		//		By 	elementoTabella=new By.ByXPath("//table//td[contains(text(),'"+elemento+"')]");
		//		By	valoreElementoTabella=new By.ByXPath("//table//td[contains(text(),'"+elemento+"')]/parent::tr/td[2]");
		//Chiusura pagina attuale
		driver.close();
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	//----- TC135 ACR Methods and Constants -----
	
	public String findPraticaRowNumber(String date) throws Exception {
		By rowsLocator = By.xpath(righeXpath);
		int rowsNumber = driver.findElements(rowsLocator).size();
		String rowAsString = null;
		//First row is for headers, so we start with index = 2
		for (int i = 2; i < rowsNumber; i++) {
			rowAsString = String.valueOf(i);
			String tipoLavoroString = driver.findElement(By.xpath(tipoLavoroRigaPratica.replace("$ROW_NUMBER$", rowAsString))).getText();
			String dateAsString = driver.findElement(By.xpath(dateRigaPratica.replace("$ROW_NUMBER$", rowAsString))).getText();
			if (tipoLavoroString.equals(tipoLavoro) && dateAsString.equals(date)) {
				break;
			}
		}
		if (rowAsString != null) {
			
			return rowAsString;
		} else {
			throw new Exception("Can't find pratica");
		}
	}
	
	public void checkPraticaStatus(By praticaLocator, String statusString) throws Exception {
		String status = driver.findElement(praticaLocator).getText();
		if (!status.contains(statusString)) {
			throw new Exception("Pratica has wrong status");
		}
	}
	
	public String getDistributor() throws Exception {
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(5);
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		String valoreCampo = null;
		try{
			valoreCampo = driver.findElement(By.xpath("//table//td[contains(text(),'Distributore e Partita IVA')]/parent::tr/td[2]")).getText();
		}
		catch (Exception e){
			throw new Exception("Il campo 'Distributore e Partita IVA' non è presente nel dettaglio");
		}
		//Chiusura pagina attuale
		driver.close();
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		TimeUnit.SECONDS.sleep(3);
		
		return valoreCampo;
	}
	
	public boolean verificaSeEsiste(By elemento) throws Exception{
		if(util.exists(elemento, 10)){
			return true;
		}else return false;
	}

	
	private String tipoLavoro = "MODIFICA TENSIONE";
	public String status_OK = "OK";
	public String status_AW = "AW";
	public String status_AA = "AA";
	public String status_CI = "CI";
}
