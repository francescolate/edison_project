package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class FibraDiMelitaComponent {
	//RemoteWebDriver driver;
	SeleniumUtilities util;
	WebDriver driver;
	
	/*
	public FibraDiMelitaComponent(RemoteWebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	*/
	public FibraDiMelitaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public By headerPathLinks = By.xpath("//ul[@class='breadcrumbs component']/li/a");
	public By path = By.xpath("//ul[@class='breadcrumbs component']");
	public By title = By.xpath("//*[text()='In casa è arrivata una nuova energia']");
	public By subTitle = By.xpath("//*[text()='FIRA DI MELITA a 27€ al mese (IVA inclusa). Senza costi di attivazione e con modem WiFi incluso!']");
	public By parbase = By.xpath("//*[@class='text parbase']");
	public By headerTitle = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_title') and text()='Scegli la velocità della fibra di Melita!']");
	public By headerTitle_2 = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_title') and text()='Melita offre la fibra ai clienti Enel Energia']");
	public By headerSubTitle = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_detail') and contains(text(),'Verifica la copertura nel tuo comune e naviga fino a 1 Gigabit/s in')]");
	public By headerSubTitleNew = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_detail') and contains(text(),'In promozione, tre mesi di canone in regalo!')]");
	public By headerSubTitleKO = By.xpath("//section[@data-module='image-hero']//*[contains(@class,'hero_detail') and contains(text(),'Restiamo in contatto')]");
	public String pathLinks1 [] = {"HOME","FIBRA DI MELITA"};
	public String pathLinks2 [] = {"HOME","FIBRA COPERTURA CONFERMATA"};
    public String linkNavbarXpath = "//nav[@data-module='hub-secondary-nav']//a[text()='#']";
    public String [] navBarLinks = {"Vantaggi","Caratteristiche","Verifica copertura","Offerta","FAQ"};
	public By verificaCoperturaAnchor = By.xpath("//nav[@data-module='hub-secondary-nav']//a[text()='Verifica copertura' and @href='#ver']");
    public By closeChatHeading = By.xpath("//section[contains(@class,'open')]//div[@class='chat-heading']//span[contains(@class,'icon-close')]");
	public By comuneField = By.xpath("//div[@id='mainFibra']//label[text()='Comune']/..//input[@id='comune']");
	public By indirizzoField = By.xpath("//div[@id='mainFibra']//label[text()='Indirizzo']/..//input[@id='indirizzo']");
	public By civicoField = By.xpath("//div[@id='mainFibra']//label[text()='Numero civico']/..//input[@id='numeroCivico']");
	public By verificaLaCoperturaButton = By.xpath("//div[@id='mainFibra']//button[contains(text(),'Verifica la copertura')]");
	public By nonTroviIlTuoIndirizzoLink = By.xpath("//div[@id='mainFibra']//a/span[contains(text(),'Non trovi il tuo indirizzo?')]");
    public By fibraDiMelitaArrivataLabel = By.xpath("//div[@class='article-content_inner']//h2[text()='La fibra di Melita è arrivata a casa tua!']");
    public By fibraMelitaArrivataLabel = By.xpath("//h3[text()='La fibra di Melita è arrivata a casa tua!']");
	public By articleh3strongelement = By.xpath("//section[@data-module='article-text-container']/h3/strong");
	public By articlepstrongelement = By.xpath("//section[@data-module='article-text-container']/h3/..//p");
	public By articledivstrongelement = By.xpath("//section[@data-module='article-text-container']/h3/..//div");
    public By linkAccediEnel = By.xpath("//a[contains(text(),'Area Clienti MyEnel')]");
    public By mainContactFiberLabel = By.xpath("//div[@id='mainContactFiber']//h1");
    public String inputFibra = "//div[@class='customerData']//label[text()='#']/..//input[contains(@class,'form-control')]";
    public String errorMissedFields = "//label[text()='#']/../span[text()='Si prega di inserire un valore']";
    public String genericLabel ="//label[text()='#']";
    public By proseguiButton = By.xpath("//button[contains(text(),'PROSEGUI')]");
    public By tornaHomeButton= By.xpath("//div[@class='container-button-group']//button[contains(text(),'TORNA ALLA HOME')]");
	String mandatoryfields [] = {"Nome*","Cognome*","Codice Fiscale*","Telefono Cellulare*","Email*","Comune*","Indirizzo*","Civico*","Ho preso visione","testo marketing"};
    public By modalMainText = By.xpath("//div[@data-remodal-id]//h3");
    public By modalSecondaryText = By.xpath("//div[@data-remodal-id]//p[not(@class)]");
    public By modalTornaAllaHomePageButton = By.xpath("//div[@data-remodal-id]//button[text()='TORNA ALLA HOMEPAGE']");
    public By containterOperatore = By.xpath("//div[@class='wrapper-content']");
    public By chiamaAdessoButton = By.xpath("//button[@id='js-call-btn']");
    public By maggioriInfo = By.xpath("//a[@title='info']");
    
    public String indirizzoCopertoText = "La fibra di Melita è arrivata a casa tua!L'indirizzoè coperto.Scegli una delle offerte luce e gas abbinate alla FIBRA DI MELITA.Se hai bisogno di supporto, puoi entrare in contatto con un nostro operatore che ti saprà fornire l'aiuto di cui hai bisogno.";
	
	public void checkNavBarLinks() throws Exception{
    	for(int x =0;x<navBarLinks.length;x++) {
    		if(!util.exists(By.xpath(linkNavbarXpath.replace("#", navBarLinks[x])), 2)) throw new Exception("Nella sezione Fibra Di Melita non e' presente nella navbar in alto la voce "+navBarLinks[x]);
    	}
    }
	
	
	public void verificaCoperturaIndirizzo(String comune, String indirizzo, String civico) throws Exception{
		WebElement el = util.waitAndGetElement(By.xpath("//div[@class='article-content_inner']//p[text()=concat('L',\"'\",'indirizzo ')]"));
	    String address = el.getText();
	    String checkAddress = "L'indirizzo "+indirizzo+", "+civico+" "+comune.split(",")[0]+" è coperto.";
	    if(!address.contentEquals(checkAddress)) throw new Exception("Dopo la verifica della copertura non compare a video il messaggio "+checkAddress);
		
	}
	
	public void verificaMancataCoperturaIndirizzo(String comune, String indirizzo, String civico) throws Exception{
		WebElement el = util.waitAndGetElement(By.xpath("//div[@id='messageTitleLeadB']"));
	    String address = el.getText().replace("\r\n", "").replace("\r", "").replace("\n", "");
	    String checkAddress = "L'indirizzo "+indirizzo+", "+civico+" "+comune.split(",")[0]+" non è ancora raggiunto dal servizio fibra di Melita.Se vuoi sapere quando il servizio fibra di Melita sarà disponibile presso il tuo indirizzo di fornitura, compila il form e ti ricontatteremo. Se vuoi provare con un altro indirizzo, torna alla pagina precedente per la verifica della copertura.";
	   
	    if(!address.contentEquals(checkAddress)) throw new Exception("Dopo la verifica della copertura non compare a video il messaggio "+checkAddress);
	}
	
	
	
	public void checkMandatoryFields() throws Exception {
		ArrayList<String> array = new ArrayList<String>(Arrays.asList(mandatoryfields));
		clickComponent(proseguiButton);
		for(String a : array) {
			if(util.waitAndGetElement(By.xpath(errorMissedFields.replace("#", a))).getAttribute("class").contains("hide")) throw new Exception("Dopo aver cliccato prosegui senza aver inserito il campo "+a+" il sistema non mostra il messaggio di obbligatorietà");
		}
	}
	
	public void checkMandatoryFields(List<String> removeFields) throws Exception {
		ArrayList<String> array = new ArrayList<String>(Arrays.asList(mandatoryfields));
		for(String x : removeFields) array.remove(x);
		clickComponent(proseguiButton);
		for(String a : array) {
				if(util.waitAndGetElement(By.xpath(errorMissedFields.replace("#", a))).getAttribute("class").contains("hide")) throw new Exception("Dopo aver cliccato prosegui senza aver inserito il campo "+a+" il sistema non mostra il messaggio di obbligatorietà");
		}
	}
	
	public void checkMandatoryFieldsAfterInputField(String field, String value, List<String> removeFields, boolean clear) throws Exception {
		ArrayList<String> array = new ArrayList<String>(Arrays.asList(mandatoryfields));
		array.remove(field);
		for(String x : removeFields) array.remove(x);
		util.objectManager(By.xpath(inputFibra.replace("#", field)), util.scrollAndSendKeys,value);
		clickComponent(proseguiButton);
		for(String a : array) {
			if(util.waitAndGetElement(By.xpath(errorMissedFields.replace("#", a))).getAttribute("class").contains("hide")) throw new Exception("Dopo aver cliccato prosegui senza aver inserito il campo "+a+" il sistema non mostra il messaggio di obbligatorietà");
		}
		if(clear)util.objectManager(By.xpath(inputFibra.replace("#", field)), util.clear);

	}
	
	public void checkMandatoryFieldsAfterInputField(String field, String value, boolean clear) throws Exception {
		ArrayList<String> array = new ArrayList<String>(Arrays.asList(mandatoryfields));
		array.remove(field);
		util.objectManager(By.xpath(inputFibra.replace("#", field)), util.scrollAndSendKeys,value);
		clickComponent(proseguiButton);
		for(String a : array) {
			if(util.waitAndGetElement(By.xpath(errorMissedFields.replace("#", a))).getAttribute("class").contains("hide")) throw new Exception("Dopo aver cliccato prosegui senza aver inserito il campo "+a+" il sistema non mostra il messaggio di obbligatorietà");
		}
		if(clear)util.objectManager(By.xpath(inputFibra.replace("#", field)), util.clear);

	}
	
	public void checkMandatoryFieldsAfterSelectedCheckbox(String field) throws Exception {
		ArrayList<String> array = new ArrayList<String>(Arrays.asList(mandatoryfields));
		array.remove(field);
		util.objectManager(By.xpath(genericLabel.replace("#", field)), util.click);
		clickComponent(proseguiButton);
		for(String a : array) {
			if(util.waitAndGetElement(By.xpath(errorMissedFields.replace("#", a))).getAttribute("class").contains("hide")) throw new Exception("Dopo aver cliccato prosegui senza aver inserito il campo "+a+" il sistema non mostra il messaggio di obbligatorietà");
		}
		util.objectManager(By.xpath(genericLabel.replace("#", field)), util.click);
	}
	
	
	public void checkElementWithText(By element, String text) throws Exception{
		if(!util.checkElementText(element, text)) throw new Exception("Impossibile trovare a video l'elemento identificato dal seguente testo "+text);
	}
	
	public void inserisciIndirizzo(String comune, String indirizzo, String civico) throws Exception{
		util.objectManager(comuneField, util.sendKeysCharByChar, comune);
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(By.xpath("//ul//a[@role='option' and contains(text(),'"+comune+"')]"), util.scrollAndClick);
		util.objectManager(indirizzoField, util.sendKeysCharByChar, indirizzo);
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(By.xpath("//ul//a[@role='option' and contains(text(),'"+indirizzo+"')]"), util.scrollAndClick);
		util.objectManager(civicoField, util.sendKeysCharByChar, civico);
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(By.xpath("//ul//a[@role='option' and contains(text(),'"+civico+"')]"), util.scrollAndClick);
		
	}
	
	
	public void inputField(By xpath, String valore) throws Exception {
		util.objectManager(xpath, util.scrollAndSendKeys, valore);
	}
	
	public void sendKeyAndSelectValue(By field, String value) throws Exception{
		util.objectManager(field, util.scrollAndSendKeys, value);
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(By.xpath("(//ul//a[@role='option' and contains(text(),'"+value+"')])[1]"), util.scrollAndClick);
	}
    
    
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, true);
		util.objectManager(clickableObject, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		
	}
	
	
	public void clickIfExist(By clickableObject) throws Exception {
		if (util.exists(clickableObject, 2)) {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
		}
	}
	

	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
		if(!colore.contentEquals(color)) throw new Exception("Il colore dell'oggetto "+objectName+" non risulta essere "+color+". Colore trovato : "+colore);
	}
	
	
	public void checkHeaderPathLinks(String links []) throws Exception{
		
		List<WebElement> webLinks = util.waitAndGetElements(headerPathLinks);
		
		for(int x = 0;x<=links.length-1;x++) {
			if(!webLinks.get(x).getAttribute("innerText").trim().toLowerCase().contentEquals(links[x].trim().toLowerCase())) throw new Exception("In Domande Frequenti Header section, the path does not contains link "+links[x]);
		}
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("L'oggetto identificato dal seguente xpath " + existingObject + " non esiste.");
	}
	
	
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
	
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = we.getText();		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	public void checkHeaderPage() throws Exception{
		verifyComponentExistence(path);
		verifyComponentExistence(title);
		verifyComponentExistence(subTitle);
	}
	
	public void checkTextPageOK() throws Exception{	
		verifyComponentExistence(fibraMelitaArrivataLabel);
		verifyComponentExistence(parbase);
		compareText(parbase, indirizzoCopertoText, true);		
	}
	
	public void checkTextOperatore() throws Exception{
		verifyComponentExistence(containterOperatore);
		containsText(containterOperatore, "Vuoi contattare un nostro operatore?");
		//containsText(containterOperatore, "Un operatore sarà a tua disposizione per offrirti il nostro supporto e darti informazioni sui nostri servizi.Ti informiamo che questo servizio è attivo dal lunedì al venerdì dalle 9:00 alle 23:00.");
		//containsText(containterOperatore, "Ti ricordiamo che puoi metteri in contatto con noi attraverso i canali");
		containsText(containterOperatore, "Whatsapp");
		containsText(containterOperatore, "Facebook Messenger");
		containsText(containterOperatore, "Telegram");
		//containsText(containterOperatore, "Ti informiamo che questo servizio è attivo dal lunedì al venerdì dalle 9:00 alle 23:00 ed è disponibile solo per i seguenti browser: Chrome, Firefox e Safari.");
		containsText(containterOperatore, "MAGGIORI INFORMAZIONI");
		verifyComponentExistence(chiamaAdessoButton);

	}
	
	
}
