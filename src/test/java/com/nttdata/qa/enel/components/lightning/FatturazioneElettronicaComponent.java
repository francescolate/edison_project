package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class FatturazioneElettronicaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	
	public FatturazioneElettronicaComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	public By confermaFatturazioneElettronicaSWAEVO = By.xpath("//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By confermaFatturazioneElettronica = By.xpath("//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By codiceufficio = By.xpath("//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//label[text()='Codice Ufficio']/..//input");
	public By pecFatturazioneElettronica = By.xpath("//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//label[text()='PEC']/..//input");
	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	public void selezionaCanaleInvio(String canale) throws Exception{
		util.selectLighningValue("Canale Invio", canale);
		spinner.checkSpinners();
	}
	
	public void inserisciCodiceUfficio(String codice) throws Exception{
		util.objectManager(codiceufficio, util.scrollAndSendKeys, codice);
	}
	
	public void selezionaCodiceUfficio(String codice) throws Exception{
		String valorePredefinito=driver.findElement(By.xpath("//label[text()='Codice Ufficio']/following::input[1]")).getAttribute("value");
		if(valorePredefinito.length()<1){
			util.selectLighningValue("Codice Ufficio", codice);
		}
	}
	
	public void selezionaTipologiaPA(String tipo) throws Exception{
		util.selectLighningValue("Tipologia pubblica amministrazione", tipo);
	}
	
	public void selezioCodiceUfficio(String tipo) throws Exception{
		util.selectLighningValue("Codice Ufficio'", tipo);
	}
	public String salvaValoreMail(By path) throws Exception{
		String mailValue = util.waitAndGetElement(pecFatturazioneElettronica,true).getAttribute("value");
		return mailValue;		
	}
	
	public void verificaCampiConfigurazioneB2C() throws Exception{
		
		String canaleInvio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Canale Invio']/..//input"),true).getAttribute("value");
		String codiceUfficio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Codice Ufficio']/..//input"),true).getAttribute("value");
		String pec = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='PEC']/..//input"),true).getAttribute("value");
		String dataInizioValidità = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Data Inizio Validità']/..//input"),true).getAttribute("value");
		String dataFineValidità = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Data Fine Validità']/..//input"),true).getAttribute("value");
		String tipoInvio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Tipo Invio']/..//input"),true).getAttribute("value");

	
		if((canaleInvio.length()>0) )  throw new Exception("Canale Invio risulta prepopolato o diverso dal valore ' '");
		if((codiceUfficio.length()<6) || (codiceUfficio.compareTo("0000000")!=0))  throw new Exception("codiceUfficio non risulta prepopolato correttamente, diverso dal valore '000000'");
		if(pec.length()>0) throw new Exception("Pec risulta prepopolato o è diverso dal valore ' '");

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String data = simpleDateFormat.format(calendar.getTime()).toString();
		
		Logger.getLogger("").log(Level.INFO, "Sysdate:"+data);
		simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		data = simpleDateFormat.format(calendar.getTime()).toString();
		if((dataInizioValidità.length()<2) || (dataInizioValidità.compareTo(data)!=0)) throw new Exception("Data inizio validità non risulta prepopolato o diversa da:"+data);
		if((dataFineValidità.length()<2) || (dataFineValidità.compareTo("31/12/2999")!=0)) throw new Exception("Data fine validità non risulta prepopolato o diversa da:31/12/9999 ");
		if((tipoInvio.length()<2) || (tipoInvio.compareTo("DOPPIO")!=0))  throw new Exception("tipoInvio non risulta prepopolato correttamente, diverso dal valore 'DOPPIO'");

	 }
	
public void verificaCampiFatturazioneElettronica() throws Exception{
		
		String canaleInvio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Canale Invio']/..//input"),true).getAttribute("value");
		String codiceUfficio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Codice Ufficio']/..//input"),true).getAttribute("value");
		String pec = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='PEC']/..//input"),true).getAttribute("value");
		String dataInizioValidità = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Data Inizio Validità']/..//input"),true).getAttribute("value");
		String dataFineValidità = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Data Fine Validità']/..//input"),true).getAttribute("value");
		String tipoInvio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Tipo Invio']/..//input"),true).getAttribute("value");

	
		if((canaleInvio.length()<0))  throw new Exception("Canale Invio non risulta prepopolato o diverso dal valore ' '");
		if((codiceUfficio.length()<2) || (codiceUfficio.compareTo("0000000")!=0))  throw new Exception("codiceUfficio non risulta prepopolato correttamente, diverso dal valore '000000'");
		if(pec.length()<0) throw new Exception("Pec non risulta prepopolato o è diverso dal valore ' '");

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String data = simpleDateFormat.format(calendar.getTime()).toString();
		
		Logger.getLogger("").log(Level.INFO, "Sysdate:"+data);
		simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		data = simpleDateFormat.format(calendar.getTime()).toString();
		if((dataInizioValidità.length()<2) || (dataInizioValidità.compareTo(data)!=0)) throw new Exception("Data inizio validità non risulta prepopolato o diversa da:"+data);
		if((dataFineValidità.length()<2) || (dataFineValidità.compareTo("31/12/2999")!=0)) throw new Exception("Data fine validità non risulta prepopolato o diversa da:31/12/9999 ");
		if((tipoInvio.length()<2))  throw new Exception("tipoInvio non risulta prepopolato correttamente, diverso dal valore 'DOPPIO'");

	 }

	public void verificaCampiConfigurazioneB2B() throws Exception{
		
		String codiceUfficio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Codice Ufficio']/..//input"),true).getAttribute("value");
		String pec = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='PEC']/..//input"),true).getAttribute("value");
		String dataInizioValidità = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Data Inizio Validità']/..//input"),true).getAttribute("value");
		String dataFineValidità = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Data Fine Validità']/..//input"),true).getAttribute("value");
		String tipoInvio = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Tipo Invio']/..//input"),true).getAttribute("value");

	
		if((codiceUfficio.length()<6) || (codiceUfficio.compareTo("0000000")!=0))  throw new Exception("codiceUfficio non risulta prepopolato correttamente, diverso dal valore '000000'");
		if(pec.length()>0) throw new Exception("Pec risulta prepopolato o è diverso dal valore ' '");

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String data = simpleDateFormat.format(calendar.getTime()).toString();
		
		Logger.getLogger("").log(Level.INFO, "Sysdate:"+data);
		simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		data = simpleDateFormat.format(calendar.getTime()).toString();
		if((dataInizioValidità.length()<2) || (dataInizioValidità.compareTo(data)!=0)) throw new Exception("Data inizio validità non risulta prepopolato o diversa da:"+data);
		if((dataFineValidità.length()<2) || (dataFineValidità.compareTo("31/12/2999")!=0)) throw new Exception("Data fine validità non risulta prepopolato o diversa da:31/12/9999 ");
		if((tipoInvio.length()<2) || (tipoInvio.compareTo("DOPPIO")!=0))  throw new Exception("tipoInvio non risulta prepopolato correttamente, diverso dal valore 'DOPPIO'");

	 }
	
	public boolean verificaAllFiledReadOnly() throws Exception{
		//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//input
		List<WebElement> inputElements = driver.findElements(By.xpath("//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//input"));
//		System.out.println("Numero Elementi:"+inputElements.size());
		Boolean flag=true;
		for(int i=0;i<inputElements.size();i++){
			String disabled=inputElements.get(i).getAttribute("disabled");
//			System.out.println("Disable input "+i+":"+disabled);
//			System.out.println("Disable tag size "+i+":"+disabled.length());
			if (disabled.compareTo("false")==0){
				flag=false;
				break;
			}
			
		}
		return flag;
	}
	
}
