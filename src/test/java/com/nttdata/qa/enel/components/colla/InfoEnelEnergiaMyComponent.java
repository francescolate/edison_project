package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
public class InfoEnelEnergiaMyComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;
	
	public By serviziItem = By.xpath("//nav[@id='mainNav']//span[text()='Servizi']");
	public By ituoidirtti = By.xpath("//a[@id='iTuoiDiritti']");
	public By account = By.xpath("//a[@id='accountSelfcare']/span[2]");
	public By serviziFornitureLbl = By.xpath("//section[@id='forniture-id0']/div/h2");
	public String tile ="//section[@id='forniture-id0']/ul/li/a/div[2]/h3";
	public By autolettura = By.xpath("//li[@class='tile'][1]//h3[text()='Autolettura']");
	public By infoEnelEnergia = By.xpath("//li[@class='tile']//h3[text()='InfoEnelEnergia']");
	public By modificaIndirizzoFatturazione = By.xpath("//h3[text()='Modifica Indirizzo Fatturazione']");
	public By modificaPotenzaeoTensione = By.xpath("//h3[text()='Modifica Potenza e/o Tensione']");
	public By modificaContattieConsensi = By.xpath("//li[@class='tile']//h3[text()='Modifica Contatti e Consensi']");
	public By infoEnergiaIcon = By.xpath("//section[@id='forniture-id0']//li[@class='tile'][2]/a/div[2]/h3");
	public By infoEnergiaIconUpdated = By.xpath("//ul[@class='cm-services-row']/div[6]/a");
	public By headingInfoEnergia = By.xpath("//section[@id='landing-text']/div/div/h2");
	public By heading2InforEnergia = By.xpath("//h1[@id='titolo-dispositiva']");
	public By titleInfoEnergia = By.xpath("//section[@id='landing-text']/div/div/p[2]");
	public By heading2InfoEnel = By.xpath("//div[@id='sc']//h2");
	public By subtitleInfoEnergia = By.xpath("//section/div[@class='ingaggi-list-container']/div[1]/p");
	public By subtitleInfoEnergiaNew = By.xpath("//div[@data-id='fornitura-attiva']/p");
	public By addressInfoEEservice = By.xpath("//div[@class='list-container ingaggiModel'][1]/div//span[@class='valore']");
	public By clientNumber = By.xpath("//div[@class='list-container ingaggiModel'][1]/div//span[3]");
	public By attivaInfoEnergiaButton = By.xpath("//div[@class='button-container'][1]/button[@class='button_first']");
	public By esciButton = By.xpath("//div[@id='landing-dispositiva']/button[@class='button_second']");
	public By esciRESButton = By.xpath("//div[@id='sc']//span[.='Esci']");
	public By popUpHeading = By.xpath("//h3[@id='titleModal']");
	public By popUpInnerText = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
	public By noButton = By.xpath("//button[@id='overlayNoButton']");
	public By siButton = By.xpath("//button[@id='overlayYesButton']");
	public By closePopUp = By.xpath("//div[@id='modalAlert']//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By accountTitle = By.xpath("//h1[@id='profile-contact']");
	public By accountSubTitle = By.xpath("//div[@id='mainContentWrapper']//div[@class='heading faq-appearance-group section-heading']/p");
	public By modificaContattiButton = By.xpath("//div[@id='mainContentWrapper']/div[@class='button-landing parbase']//span");
	public By consensiTitle = By.xpath("//div[@class='static-page-component parbase'][1]//h2");
	public By consensiSubTitle = By.xpath("//div[@class='static-page-component parbase'][1]//p[2]");
	public By modificaConsensiButton = By.xpath("//div[@class='static-page-component parbase'][1]//a//span");
	public By datiRegistrazioneTitle = By.xpath("//div[@class='static-page-component parbase'][2]//h2");
	public By datiRegistrazionesubTitle = By.xpath("//div[@class='static-page-component parbase'][2]//p[2]");
	public By modificaDatiButton = By.xpath("//div[@class='static-page-component parbase'][2]//a//span");
	public By modificaInfoEnergiaButton = By.xpath("//div[@class='button-container'][1]/button[@class='button_first']/span[text()='MODIFICA INFOENELENERGIA']");
	public By seVoiDisattivareilServizioText = By.xpath("//p[contains(text(),'Se vuoi')]");
	public By subTitle2InfoEnergia = By.xpath("//div[@data-id='fornitura-attiva']//p");
	public By attivaInfoEnelTitle = By.xpath("//h1[contains(text(),'Attiva')]");
	public By iIcon = By.xpath("//span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By Iicon274 = By.xpath("//a[@id='icon-info-circle-1']/span[@id='1']");
	public By popupContent = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-header']//p");
//	public By popUpClose = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//button");
	public By popUpClose = By.xpath("//h3[@id='titleModal']/following-sibling::button/span[1]");
	public By popUpTitle = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-header']//h3");
	public By attivaContinueButton = By.xpath("//button[@class='button button-first button-center change-tab cITA_IFM_LCP405_SelfCareButton']");
	public By attivaEsciButton = By.xpath("//button[@class='button button-first button-center change-tab cITA_IFM_LCP405_SelfCareButton']/preceding-sibling::button");
	public By attivaSubText = By.xpath("//h2[contains(text(),'Il servizio InfoEnelEnergia')]");
	public By modificabutton = By.xpath("//span[text()='MODIFICA INFOENELENERGIA']/parent::button[@class='button_first']");	
	public By avvialarichiesta = By.xpath("//a[text()='avvia la richiesta']"); 
	public By headingInfoEnergia1 = By.xpath("//div[@class='list-form-container']/h1"); 
	public By headingInfoEnergia2 = By.xpath("//div[@class='list-form-container']/h2");
	public By attivaInfoEnergia = By.xpath("//span[contains(text(),'ATTIVA INFOENELENERGIA')]/parent::button[@class='button_first']");
	
	
	public InfoEnelEnergiaMyComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponent(By clickObject) throws Exception {
			util.objectManager(clickObject, util.scrollToVisibility, false);
			util.jsClickElement(clickObject);
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
				
		public void checkForSupplyServices(String property) throws Exception {
			
			if(! property.contains(driver.findElement(autolettura).getText()) && property.contains(driver.findElement(infoEnelEnergia).getText()) && property.contains(driver.findElement(modificaIndirizzoFatturazione).getText())
					&& property.contains(driver.findElement(modificaPotenzaeoTensione).getText()) && property.contains(driver.findElement(modificaContattieConsensi).getText()))
				
				throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
			
			
		}
		
		public void isPopUpDisplayed(By checkObject) throws InterruptedException
		{
			
			if (driver.findElement(checkObject).isDisplayed()) 
			{
				driver.findElement(checkObject).click();
				Thread.sleep(10000);
			} 
			
			return;
			
		}
	
		
		public static final String SERVIZI_FORNTURE_LABEL= "Servizi per le forniture";
		public static final String[] SERVICES_SUPPLIES  = {"Autolettura","InfoEnelEnergia","Modifica Indirizzo Fatturazione","Modifica Potenza e/o Tensione","Modifica Contatti e Consensi"};
		public static final String HEADING_INFOENERGIA ="InfoEnelEnergia"; 
		public static final String TITLE_INFOENERGIA ="Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
		public static final String HEADING2_INFOENELENERGIA ="Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
		public static final String SUBTITLE_INFOENERGIA ="Il servizio InfoEnelEnergia non è attivo sulla fornitura:";
		public static final String POPUP_HEADING ="Attenzione";
		public static final String POPUP_INNERTEXT ="Sei sicuro di voler uscire da questa sezione?";
		public static final String ACCOUNT_TITLE = "Dati e Contatti";
		public static final String ACCOUNT_SUBTITLE ="Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione";
		public static final String CONSENSI_TITLE = "Consensi";
		public static final String CONSENSI_SUBTITLE = "Comunica o aggiorna i consensi a Enel Energia e ai suoi partner";
		public static final String DATI_REG_TITLE ="Dati Registrazione";
		public static final String DATI_REG_SUBTITLE ="Visualizza o modifica le tue credenziali di accesso a enel.it";
		public static final String SUBTITLE2INFOENERGIA = "Il servizio InfoEnelEnergia è attivo sulla fornitura:";
		public static final String SEVOIDISATTIVAREILSERVIZIOTEXT = "Se vuoi disattivare il servizio, avvia la richiesta";
		public static final String AttivaInfoEnelEnergiaTitle = "Attiva InfoEnelEnergia";
		public static final String PopUpContent = "Non è possibile avviare la richiesta poiché risulta un'altra operazione in corso. Non appena conclusa sarà possibile effettuare una nuova richiesta. Per maggiori informazioni chiedi al nostro supporto in chat.";
		public static final String HeadingInfoEnergia1 = "Disattiva InfoEnelEnergia";
		public static final String HeadingInfoEnergia2 = "Seleziona una o più forniture su cui intendi revocare il servizio di InfoEnelEnergia.";

}

