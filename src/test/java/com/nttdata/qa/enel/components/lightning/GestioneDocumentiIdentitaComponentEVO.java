package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

//Il seguente componente si occupa di effettuare l'inserimento dei documenti di idendità
//specificando tipologia, numero, rilasciatoda e data

public class GestioneDocumentiIdentitaComponentEVO {
    private WebDriver driver;
    private SeleniumUtilities util;
    private SpinnerManager spinnerManager;
    
    public GestioneDocumentiIdentitaComponentEVO(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
    util = new SeleniumUtilities(driver);
    spinnerManager = new SpinnerManager(driver);
  }
    
    
    By selectTipoDocumento = By.xpath("//label[contains(text(),'Tipo documento')]/..//input");
    By insertNumeroDocumento = By.xpath("//label[contains(text(),'Numero documento')]/..//input");
    By insertRilasciatoDa = By.xpath("//label[contains(text(),'Rilasciato da')]/..//input");
    By insertRilasciatoIl = By.xpath("//label[contains(text(),'Rilasciato il')]/..//input");
    public By confermaButton = By.xpath("//h2[text()='Documento di identità del titolare']/ancestor::div[@role='main']//div[@data-id='modificaButton']/..//button[text()='Conferma']");


	public void inserisciDocumento(String frame, String tipoDocumento, String numDoc, String rilasciatoDa, String rilasciatoIl) throws Exception{
		TimeUnit.SECONDS.sleep(7);
		util.frameManager(frame, selectTipoDocumento, util.select, tipoDocumento);
		util.frameManager(frame, insertNumeroDocumento, util.sendKeys, numDoc);
		util.frameManager(frame, insertRilasciatoDa, util.sendKeys, rilasciatoDa);
		util.frameManager(frame, insertRilasciatoIl, util.sendKeys, rilasciatoIl);
	}
	
	public void inserisciDocumento(String tipoDocumento, String numDoc, String rilasciatoDa, String rilasciatoIl) throws Exception{
		TimeUnit.SECONDS.sleep(3);
//		WebElement firstName = driver.findElement(By.cssSelector("input[name='first_name']"));
		WebElement tipoDoc = driver.findElement(selectTipoDocumento);
		
		tipoDoc.sendKeys(tipoDocumento);
		
		TimeUnit.SECONDS.sleep(3);
		
		tipoDoc.sendKeys(Keys.ENTER);

		TimeUnit.SECONDS.sleep(3);
		util.waitAndGetElement(insertNumeroDocumento).sendKeys(numDoc);
		util.waitAndGetElement(insertRilasciatoDa).sendKeys(rilasciatoDa);
		util.waitAndGetElement(insertRilasciatoIl).sendKeys(rilasciatoIl);
		
		tipoDoc.sendKeys(Keys.TAB);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void conferma(By conferma) throws Exception{
		WebElement el = util.waitAndGetElement(conferma);
		util.scrollToElement(el);
		el.click();
		//spinnerManager.waitForSpinnerBySldsHide(spinnerManager.dotSpinner);
		TimeUnit.SECONDS.sleep(5);
//		spinnerManager.checkSpinners();
	}
	
	
	
	
}
