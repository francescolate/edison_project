package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class LeftMenuComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	public final static String voceMenu = "//nav[@id='mainNav']//span[text()='#']";
	public By enelLogo = By.cssSelector("#header > div > a.brand.href-sf-prevent-default > img");
    
	public LeftMenuComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
		
	}

	public void sceltaMenu(String voce) throws Exception{
		util.objectManager(By.xpath(voceMenu.replace("#", voce)), util.click);
		spinner.checkMainCollaSpinner();
	}
	
	public void verifyMenuItem(String itemName) throws Exception {
		String xpathString = voceMenu.replace("#", itemName);
		By item = By.xpath(xpathString);
		if (!util.verifyVisibility(item, 15)) {
			throw new Exception("The menu item '" + itemName.toUpperCase() + " is not displayed.");
		}
	}
	
	public void verifyMenuItems(boolean isLikeRes) throws Exception {
		for (int i = 0; i < menuItemsString.length; i++) {
			verifyMenuItem(menuItemsString[i]);
		}
		if (isLikeRes) {
			verifyMenuItem(areaClientiImpresa);
		}
	}
	
	public By locateMenuItem(String itemName) {
		return By.xpath(voceMenu.replace("#", itemName));
	}
	
	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//		if (!util.exists(oggettoEsistente, 15))
//			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		if (!util.verifyExistence(oggettoEsistente, 15)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		*/
	}
		
	//----- CONSTANTS ------
	
	private final String[] menuItemsString = {
			"Forniture",
			"Bollette",
			"Servizi",
			"Novità",
			"Spazio Video",
			"Account",
			"I tuoi diritti",
			//"Supporto",
			"Trova Spazio Enel.",
			"Esci"
	};
	private final String areaClientiImpresa = "Area Clienti Impresa";

}
