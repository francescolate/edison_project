package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BolleteBSNComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By path = By.xpath("//div[@class='panel panel-default panel-clear']//ol");
	public By pageTitle = By.xpath("//h1[text()='Le tue Bollette']");
	public By titleSubText = By.xpath("//h1[text()='Le tue Bollette']//following::p[1]");
	public By pageText = By.xpath("//p[@id='inviaDocumentiLab']");
	public By bolleteDaPagareTitle = By.xpath("//section[@class='content']//h1[text()='Bollette da pagare']");
	public By bolleteDaPagarePath = By.xpath("//section[@class='content']//ol");
	public By inviaDocumenti = By.xpath("//a[contains(text(),'Invia documenti')]");
	public By mostraFiltri = By.xpath("//a[@name='Filtri']");
	public By EsportaInExcel = By.xpath("//a[@name='Esporta in Excel']");
	public By Bollete = By.xpath("//section[@id='multiFornSelect']//div[@class='title']//h2");
	public By tipo = By.xpath("//table//tr//th[1]");
	public By nBollete = By.xpath("//table//tr//th[2]");
	public By scadenza = By.xpath("//table//tr//th[3]");
	public By stato = By.xpath("//table//tr//th[4]");
	public By importo = By.xpath("//table//tr//th[5]");
	public By indirizzoFornitura = By.xpath("//table//tr//th[6]");
	public By plusIcon = By.xpath("//tbody//tr[1]/td[8]//a");
	public By dettagliBolletta = By.xpath("//tr//h5[text()='Dettagli bolletta']");
	public By invoiceDetails = By.xpath("//tr//h5[text()='Dettagli bolletta']//following::div[@class='row'][1]//span//b");
	public By homeMenu =By.xpath("//li[@id='homeMenu']//a");

	public BolleteBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);			
	}
	 
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		/*System.out.println("Normalized:\n"+weText);
		System.out.println(text);*/
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void verifyInvoiceField(By checkObject,String[] fields) throws Exception{
		
		List<String> listField = Arrays.asList(fields);
		List<WebElement> web = driver.findElements(checkObject);
		//System.out.println(web.size());
		for(int i=0;i<web.size();i++){
			//System.out.println(web.get(i).getText());
		if(!listField.get(i).contains(web.get(i).getText())) throw new Exception("Invoice details is not matching with the expected value" + listField);
		}
	}
	
	public static final String PageText = "Ci sono bollette scadute. Pagale comodamente on line. Se richiesto, in seguito puoi inviare il dimostrato pagamento cliccando su \"invia documenti\".";
	public static final String TitleSubText = "Se cerchi una bolletta in particolare usa i filtri per trovarla più facilmente.";
	public static final String Tipo = "Tipo";
	public static final String NBolletta = "N. Bolletta";
	public static final String Scadenza = "Scadenza";
	public static final String Stato = "Stato";
	public static final String Importo = "Importo";
	public static final String Indirizzo_fornitura = "Indirizzo fornitura";
	public static final String[] InvoiceDetails = {"Importo: ","Da pagare: ","Stato: ","Periodo: ","Attuale modalità di pagamento: ","Canale di pagamento: ","Data di pagamento: ","Tipo: "};
}
