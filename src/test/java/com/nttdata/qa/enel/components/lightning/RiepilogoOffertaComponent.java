package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RiepilogoOffertaComponent extends BaseComponent {

    private WebDriver driver;
    private SeleniumUtilities util;
    private SpinnerManager spinnerManager;


    public By modalitaFirma = By.xpath("//label[text()='Modalità Firma']/..//input");
    public By confermaRiepilogoOfferta = By.xpath("//*[text()='Riepilogo offerta']/ancestor::div[@role='region']//button[text()='Conferma']");
    //    public By email = By.xpath("//label[text()='Email']/..//input");
    public By email = By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[@role='region']//label[text()='Email']/..//input");
    public By ConfermaTelefonica = By.xpath("//*[text()='Riepilogo offerta']/ancestor::div[@role='region']//input[@value='fisso']/../label");
    public By modalitaConferma = By.xpath("//label[text()='Modalità Conferma']/..//input");
    public By pageRiepilogoOfferta = By.xpath("//*[text()='Riepilogo offerta']");
    public By sezioneSelezioneUsoFornitura = By.xpath("//h2[text()='Selezione Uso Fornitura']");
    public By campoUsoFornitura = By.xpath("//label[contains(text(),'Uso Fornitura')]/..//input");
    public By campoUsoFornituraDisabled = By.xpath("//label[contains(text(),'Uso Fornitura')]/..//input[@disabled]");
    public By buttonConfermaSezioneUsoFornitura = By.xpath("//label[contains(text(),'Uso Fornitura')]/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonModificaSezioneUsoFornitura = By.xpath("//label[contains(text(),'Uso Fornitura')]/ancestor::div[@role='region']//button[text()='Modifica']");
    public By sezioneContattiEConsensi = By.xpath("//h2//*[text()='Contatti e consensi']");
    public By messaggioErroreEmailNonValida = By.xpath("//h6[text()='Inserire un indirizzo email valido']");
    public By messaggioErroreCampoObbl = By.xpath("//span[text()='Accettazione Condizioni Contrattuali']/ancestor::div[@role='region']//h6[text()='Questo campo è obbligatorio']");
    public By messaggioErroreSAP = By.xpath("//div[@class='slds-notify slds-notify_alert slds-theme_alert-texture slds-notify_container slds-theme_error']/h2");
    public By buttonInformativa = By.xpath("//slot[text()='Esecuzione Anticipata – Promemoria per l’operatore (DA LEGGERE OBBLIGATORIAMENTE)']//button[@title='Diritto di Ripensamento']");
    public By titoloInformativa = By.xpath("//h2[text()='Diritto di ripensamento: cosa avviene in caso di esecuzione anticipata.']");
    public By testoInformativa = By.xpath("//div[contains  (@role, none) and contains (@class,'slds-popover__body slds-p-around_large')]//slot//p");
    public By buttonAnnulla = By.xpath("//div[@class='slds-card__header slds-grid']//button[text()='Annulla']");
   
  
    public By sezioneDatiFornitura = By.xpath("//h2//*[text()='Dati fornitura']");
    public By sezioneMetodoPagamento = By.xpath("//h2//*[text()='Metodo di pagamento']");
    public By selezioneMetodoPagamento = By.xpath("//span[text()='Metodo di pagamento']/ancestor::div[contains(@class,'slds-card')]//table//label/span[@class='slds-radio_faux']");
    public By primoMetodoPagamento = By.xpath("//span[text()='Metodo di pagamento']/ancestor::div[contains(@class,'slds-card')]//table//tr[1]//label/span[@class='slds-radio_faux']");
    public By sezioneScontiBonus = By.xpath("//h2//*[text()='Sconti e Bonus']");
    public By sezioneCarrello = By.xpath("//h2//*[text()='Carrello']");
    public By buttonPopupAnnullaChiudi = By.xpath("//*[text()='Annulla']/ancestor::div[@data-id='modal']//p[text()=\"Confermi di voler annullare l'Offerta?\"]/ancestor::div[@data-id='modal']//button[text()='Chiudi']");
    public By buttonPopupAnnullaConferma = By.xpath("//*[text()='Annulla']/ancestor::div[@data-id='modal']//p[text()=\"Confermi di voler annullare l'Offerta?\"]/ancestor::div[@data-id='modal']//button[text()='Conferma']");
    public By buttonModificaRiepilogoOfferta = By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[@role='region']//button[text()='Modifica']");
    public By buttonModificaUsoFornitura = By.xpath("//h2//*[text()='Selezione Uso Fornitura']/ancestor::div[@role='region']//button[text()='Modifica']");
    public By buttonConfermaFornitura = By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[@role='region']//button[text()='Conferma fornitura']");
    public By buttonConfermaAnnullamentoRichiesta = By.xpath("//div[@class='slds-align--absolute-center']//button[contains(text(),'Conferma')]");
    public By buttonChiudiAnnullamentoRichiesta = By.xpath("//div[@class='slds-align--absolute-center']//button[text()='Chiudi']");
    public By buttonConfermaDatiForn = By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonNuovo = By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div[@role='region']//button[text()='Nuovo']");
    public By buttonConfermaMetodoPagamento = By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonConfermaScontiBonus = By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    public By buttonVerificaIndirizzoFat = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']");
    public By buttonVerificaIndUltimaFat = By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']");
    public By fornisceIndirizzo = By.xpath("//label[text()='Cliente fornisce l’indirizzo?']//..//input");
    public By labelIndirizzoVerificato = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo verificato']");
    public By labelIndirizzoUltFattVerificato = By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo verificato']");
    public By buttonConfermaIndirizzoFat = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    public By buttonVerificaIndirizzoRes = By.xpath("//h2/span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']");
    public By labelIndirizzoResVerificato = By.xpath("//h2/span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo verificato']");
    public By labelIndirizzoResNonVerificato = By.xpath("//h2/span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo non verificato:']");
    public By buttonConfermaIndirizzoRes = By.xpath("//h2/span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    public By buttonConfermaIndUltimaFat = By.xpath("//h2/span[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    public By buttonModificaIndirizzoFatturazione = By.xpath("//h2/span[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Modifica Indirizzo']");
    public By buttonConfermaIndirizzoResDis = By.xpath("//h2/span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma' and @disabled]");
    public By buttonModificaIndirizzoRes = By.xpath("//h2/span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Modifica Indirizzo']");
    public By buttonConfermaIndirizzoBuc = By.xpath("//span[text()='Indirizzo BUC Unico']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    // public By buttonConfermaFornituraCommodity=By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma Fornitura']");
    // public By buttonModificaFornitureCommodity=By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//button[text()='Modifica Fornitura']");
    // public By buttonConfermaCommodity=By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    //public By sezioneIndFatt=By.xpath("//h2/span[text()='Indirizzo di Fatturazione']");
    // public By buttonConfermaFatElettronica=By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    //public By checkBoxSiContattiConsensi=By.xpath("//*[text()='Contatti e consensi']/ancestor::div[@role='region']//p/b[text()='Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?']/ancestor::div[@role='region']//span[text()='SI']/ancestor::label/span[@class='slds-radio_faux']");
    // public By checkBoxSiContattiConsensiVolture=By.xpath("//*[text()='Contatti e consensi']/ancestor::div[@role='region']//span[text()='SI']/ancestor::label/span[@class='slds-radio_faux']");
    //public By buttonConfermaContattiConsensi=By.xpath("//*[text()='Contatti e consensi']/ancestor::div[@role='region']//button[text()='Conferma']");
    // public By buttonConfermaModFirmaCanale=By.xpath("//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonModificaIndirizzoModFirmaCanale = By.xpath("//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Modifica Indirizzo']");
    // public By buttonConfermaCvp=By.xpath("//*[text()='CVP']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonConfermaFornituraCommodity = By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma Fornitura']");
    public By buttonModificaFornitureCommodity = By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//button[text()='Modifica Fornitura']");
    public By buttonConfermaCommodity = By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    public By sezioneIndFatt = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']");
    public By buttonConfermaFatElettronica = By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']");
    public By checkBoxSiContattiConsensi = By.xpath("//h2//*[text()='Contatti e consensi']/ancestor::div[@role='region']//p/b[text()='Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?']/ancestor::div[@role='region']//span[text()='SI']/ancestor::label/span[@class='slds-radio_faux']");
    public By checkBoxSiContattiConsensiVolture = By.xpath("//h2//*[text()='Contatti e consensi']/ancestor::div[@role='region']//span[text()='SI']/ancestor::label/span[@class='slds-radio_faux']");
   // public By checkBoxSiContattiConsensiVolture = By.xpath("//h2//*[text()='Contatti e consensi']/ancestor::div[@role='region']//div[@data-label='ITA_IFM_Conditions_Accepted__c']//span[@class='slds-checkbox_faux']");
    public By buttonConfermaContattiConsensi = By.xpath("//h2//*[text()='Contatti e consensi']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonConfermaModFirmaCanale = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonConfermaCvp = By.xpath("//h2//*[text()='CVP']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By sezioniConfermate = By.xpath("//div[text()='Tutte le sezioni confermate']");
    public By buttonConferma = By.xpath("//div[@class='slds-card']//button[@name='ConfirmQuote']");
    public By sezioneSommarioOfferta = By.xpath("//span[text()='Sommario Offerta']");
    public By sezioneCliente = By.xpath("//span[text()='Cliente']");
    public By buttonTornaOfferta = By.xpath("//button[@id='confirmHeaderBtn']");
    public By campoModalitaFirma = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']//input");
    public By campoCanale = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Canale Invio']/ancestor::div[@role='none']//input");
    public By campoEmail = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Indirizzo Email']/ancestor::div[@role='none']//input");
    public By campoPEC = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Indirizzo Pec']/ancestor::div[@role='none']//input");
    public By campoCellulare = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Cellulare']/ancestor::div[@role='none']//input");
    
    
    public By pageRiepilogoOff = By.xpath("//h1/span[contains(text(),'Riepilogo Offerte')]");
    public By offerta = By.xpath("//table[@title='tabella riepilogo offerte']//a");
    public By riepilogoOfferte = By.xpath("//div//table[@title='tabella riepilogo offerte']//tbody");
    //per cliente busness
    public By sezioneReferente = By.xpath("//h2//*[text()='Referente']");
    public By campiSelezioneInSezioneReferente = By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='main']//label/span[@class='slds-radio_faux']");
    public By buttonConfermaReferente = By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By ricercaReferente = By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='main']//label[text()='Ricerca']/ancestor::div[1]//input");
    public By nomeReferente = By.xpath("//table[@role='table']//div[@class='slds-assistive-text' and text()='Nome referente']");
    public By cognomeReferente = By.xpath("//table[@role='table']//div[@class='slds-assistive-text' and text()='Cognome referente']");
    public By tipoReferente = By.xpath("//table[@role='table']//div[@class='slds-assistive-text' and text()='Tipo referente']");
    public By codFiscaleReferente = By.xpath("//table[@role='table']//div[@class='slds-assistive-text' and text()='Codice fiscale referente']");
    public By primoCampoSelezioneInSezioneReferente = By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='main']//table//tr[1]//label/span[@class='slds-radio_faux']/parent::*");
    public By buttonConfermaCigCup = By.xpath("//h2//*[text()='CIG e CUP']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By buttonConfermaSplitPayment = By.xpath("//h2//*[text()='Split Payment']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By campoSplitPayment = By.xpath("//h2//*[text()='Split Payment']/ancestor::div[@role='main']//label[text()='Split Payment']/ancestor::div[1]//input");
    public By campoflag136 = By.xpath("//h2//*[text()='CIG e CUP']/ancestor::div[@role='main']//label[text()='Flag 136']/ancestor::div[1]//input");
    public By checkboxContattiConsensi = By.xpath("//span[text()='Accettazione Condizioni Contrattuali']/ancestor::div[contains(@class,'checkbox')]/input[@aria-checked='true']");
    public By checkboxContattiConsensiClick = By.xpath("//span[text()='Accettazione Condizioni Contrattuali']/ancestor::div[contains(@class,'checkbox')]//span[@class='slds-checkbox_faux']");
    public By cercaMetodoPagamento = By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ricerca']/ancestor::div[1]//input");
    public By salvaInBozza = By.xpath("//button[@name='SaveAsDraft']");
    public By modifica = By.xpath("//button[contains(text(),'Modifica') and contains(@onclick,'editNEQuote')]");
    public By span_sezioni_nonConfermate = By.xpath("//span[contains(text(),'sezioni non confermate')]");
    public By span_sezioni_nonConfermate2 = By.xpath("//a//span[contains(text(),'sezioni confermate - Fase completa')]");
    public By labelErrorCodiceUfficio = By.xpath("//h6[text()='Il valore inserito è troppo corto']");
    public By testo1ContattiConsensi = By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[contains(@class,'slds-card')]//slot//lightning-layout-item//slot//p[text()='Gentile cliente, la informo che può richiedere l’esecuzione anticipata del suo contratto di fornitura con Enel Energia affinché le procedure di attivazione vengano avviate prima che sia decorso il termine per il diritto di ripensamento che potrà essere esercitato entro 14 giorni dalla conclusione del contratto secondo quanto previsto dalla normativa vigente. La richiesta di esecuzione anticipata non pregiudicherà la sua possibilità di esercitare il diritto di ripensamento. In questo caso, qualora non sia possibile annullare la richiesta di attivazione verso il distributore, verrà fornito da Enel Energia per il tempo necessario a trovare un altro Fornitore e sarà tenuto al pagamento dei corrispettivi previsti dal contratto fino all’avvenuta cessazione. In alternativa potrà richiedere espressamente la chiusura del punto di fornitura. Qualora, invece, sia possibile annullare la sua richiesta di esecuzione anticipata continuerà ad essere servito dal precedente Fornitore altrimenti saranno attivati i Servizi di Ultima Istanza Gas o Servizio di maggior Tutela per il settore elettrico. Se decide di non richiedere l’esecuzione anticipata del contratto, procederemo con l’attivazione della fornitura, solamente decorso il termine previsto per l’esercizio del diritto di ripensamento (14 giorni).']");
    public By testo2ContattiConsensi = By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[contains(@class,'slds-card')]//slot//lightning-layout-item//slot//p[text()='Le ricordo che in ogni caso potrà recedere, in qualsiasi momento, dal contratto cui sta aderendo.']");
    public By testo3ContattiConsensi = By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[contains(@class,'slds-card')]//slot//lightning-layout-item//slot//p//b[text()='Ha compreso le condizioni e desidera anticipare le procedure di attivazione del contratto con Enel Energia?']");
    public By testo4ContattiConsensi = By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[contains(@class,'slds-card')]//slot//lightning-layout-item//slot//p//b[text()='Gentile Cliente, le confermo che il contratto sarà attivo non prima dei 14 giorni previsti e solo dopo aver restituito la documentazione obbligatoria.']");
    public By DataRichiestaSottoscrizione = By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[@role='region']//label[text()='Data Richiesta Sottoscrizione']/ancestor::div[@role='none']//input");

 
    //Xpath per i campi che vengono visualizzati dopo aver cliccato torna ad offerta
    public By HeaderTipoLavorazione=By.xpath("//div[h1/div[text()='Offerta']]/parent::div//span");
    public By HeaderQuote=By.xpath("//div[p[text()='Quote']]//lightning-formatted-text");
    public By HeaderNumeroContratto=By.xpath("//div[p[text()='Numero Contratto']]//lightning-formatted-text");
    public By HeaderTipoOfferta=By.xpath("//div[p[text()='Tipo Offerta']]//lightning-formatted-text");
    public By HeaderDataAdesione=By.xpath("//div[p[text()='Data Adesione']]//lightning-formatted-text");
    public By HeaderStato=By.xpath("//div[p[text()='Stato']]//lightning-formatted-text");
    
    
    public RiepilogoOffertaComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
        spinnerManager = new SpinnerManager(driver);
    }

    public void pressButton(By button) throws Exception {
        util.objectManager(button, util.scrollAndClick);
    }


    public void enterReferente(By insertObject, String valore) throws Exception {
        util.objectManager(insertObject, util.sendKeys, valore);
    }

    public void verificaCampiPrepopolati() throws Exception {
        String canale = util.waitAndGetElement(By.xpath("//label[text()='Canale']/following::input"), true).getAttribute("value");
        String sottocanale = util.waitAndGetElement(By.xpath("//label[text()='Sottocanale']/following::input"), true).getAttribute("value");
        String incaricato = util.waitAndGetElement(By.xpath("//label[contains(text(),'Codice Incaricato')]/following::input"), true).getAttribute("value");
        String dataAdesione = util.waitAndGetElement(By.xpath("//label[contains(text(),'Data Adesione')]/following::input"), true).getAttribute("value");
        String numcontratto = util.waitAndGetElement(By.xpath("//label[contains(text(),'Numero Contratto')]/following::input"), true).getAttribute("value");

        if (canale.length() < 2) throw new Exception("Canale non risulta prepopolato");
        if (sottocanale.length() < 2) throw new Exception("Sottocanale non risulta prepopolato");
        if (incaricato.length() < 2) throw new Exception("Incaricato non risulta prepopolato");
        if (dataAdesione.length() < 2) throw new Exception("Data Adesione non risulta prepopolato");
        if (numcontratto.length() < 2) throw new Exception("Numero contratto non risulta prepopolato");


    }

    public void verificaCampiPrepopolatiVolturaPE() throws Exception {
        String canale = util.waitAndGetElement(By.xpath("//label[text()='Canale']/following::input"), true).getAttribute("value");
        String sottocanale = util.waitAndGetElement(By.xpath("//label[text()='Sottocanale']/following::input"), true).getAttribute("value");
        String codiceIncaricato = util.waitAndGetElement(By.xpath("//label[text()='Codice Incaricato']/following::input"), true).getAttribute("value");
        String dataAdesione = util.waitAndGetElement(By.xpath("//label[contains(text(),'Data Adesione')]/following::input"), true).getAttribute("value");
        String numcontratto = util.waitAndGetElement(By.xpath("//label[contains(text(),'Numero Contratto')]/following::input"), true).getAttribute("value");

//		System.out.println(canale);
        if ((canale.length() < 2) || (canale.compareTo("Punti Enel") != 0))
            throw new Exception("Canale non risulta prepopolato o diverso dal valore 'Punti Enel'");
//		System.out.println(sottocanale);
        if ((sottocanale.length() < 2) || (sottocanale.compareTo("Agenzia") != 0))
            throw new Exception("Sottocanale non risulta prepopolato o diverso dal valore 'Agenzia'");
//		System.out.println(codiceIncaricato);
        if (codiceIncaricato.length() < 2) throw new Exception("Incaricato non risulta prepopolato");
//		System.out.println(dataAdesione);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String data = simpleDateFormat.format(calendar.getTime()).toString();
        Logger.getLogger("").log(Level.INFO, "Sysdate:" + data);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data = simpleDateFormat.format(calendar.getTime()).toString();
        if ((dataAdesione.length() < 2) || (dataAdesione.compareTo(data) != 0))
            throw new Exception("Data Adesione non risulta prepopolato o diversa da:" + data);
//		System.out.println(numcontratto);
        if (numcontratto.length() < 2) throw new Exception("Numero contratto non risulta prepopolato");


    }

    public void verificaCampiPrepopolatiAllaccioPE() throws Exception {
        String canale = util.waitAndGetElement(By.xpath("//label[text()='Canale']/following::input"), true).getAttribute("value");
        String sottocanale = util.waitAndGetElement(By.xpath("//label[text()='Sottocanale']/following::input"), true).getAttribute("value");
        String matricola = util.waitAndGetElement(By.xpath("//label[contains(text(),'Matricola Utente')]/following::input"), true).getAttribute("value");
        String dataAdesione = util.waitAndGetElement(By.xpath("//label[contains(text(),'Data Adesione')]/following::input"), true).getAttribute("value");
        String numcontratto = util.waitAndGetElement(By.xpath("//label[contains(text(),'Numero Contratto')]/following::input"), true).getAttribute("value");

//		System.out.println(canale);
        if ((canale.length() < 2) || (canale.compareTo("Punti Enel") != 0))
            throw new Exception("Canale non risulta prepopolato o diverso dal valore 'Punti Enel'");
//		System.out.println(sottocanale);
        if ((sottocanale.length() < 2) || (sottocanale.compareTo("Agenzia") != 0))
            throw new Exception("Sottocanale non risulta prepopolato o diverso dal valore 'Agenzia'");
//		System.out.println(matricola);
        if (matricola.length() < 2) throw new Exception("Incaricato non risulta prepopolato");
//		System.out.println(dataAdesione);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String data = simpleDateFormat.format(calendar.getTime()).toString();
        Logger.getLogger("").log(Level.INFO, "Sysdate:" + data);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data = simpleDateFormat.format(calendar.getTime()).toString();
        if ((dataAdesione.length() < 2) || (dataAdesione.compareTo(data) != 0))
            throw new Exception("Data Adesione non risulta prepopolato o diversa da:" + data);
//		System.out.println(numcontratto);
        if (numcontratto.length() < 2) throw new Exception("Numero contratto non risulta prepopolato");


    }

    public void verificaCampiPrepopolatiAllaccioS2S() throws Exception {
        String canale = util.waitAndGetElement(By.xpath("//label[text()='Canale']/following::input"), true).getAttribute("value");
        String sottocanale = util.waitAndGetElement(By.xpath("//label[text()='Sottocanale']/following::input"), true).getAttribute("value");
        String matricola = util.waitAndGetElement(By.xpath("//label[contains(text(),'Matricola Utente')]/following::input"), true).getAttribute("value");
        String dataAdesione = util.waitAndGetElement(By.xpath("//label[contains(text(),'Data Adesione')]/following::input"), true).getAttribute("value");
        String numcontratto = util.waitAndGetElement(By.xpath("//label[contains(text(),'Numero Contratto')]/following::input"), true).getAttribute("value");

//		System.out.println(canale);
        if ((canale.length() < 2) || (canale.compareTo("CC Energia") != 0))
            throw new Exception("Canale non risulta prepopolato o diverso dal valore 'CC Energia'");
//		System.out.println(sottocanale);
        if ((sottocanale.length() < 2) || (sottocanale.compareTo("OUT_TELEPERFORMANCE_TA_S2S") != 0))
            throw new Exception("Sottocanale non risulta prepopolato o diverso dal valore 'OUT_TELEPERFORMANCE_TA_S2S'");
//		System.out.println(matricola);
        if (matricola.length() < 2) throw new Exception("Incaricato non risulta prepopolato");
//		System.out.println(dataAdesione);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String data = simpleDateFormat.format(calendar.getTime()).toString();
        Logger.getLogger("").log(Level.INFO, "Sysdate:" + data);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data = simpleDateFormat.format(calendar.getTime()).toString();
        if ((dataAdesione.length() < 2) || (dataAdesione.compareTo(data) != 0))
            throw new Exception("Data Adesione non risulta prepopolato o diversa da:" + data);
//		System.out.println(numcontratto);
        if (numcontratto.length() < 2) throw new Exception("Numero contratto non risulta prepopolato");


    }

    public void verificaCampiPrepopolatiVolturaS2S() throws Exception {
        String canale = util.waitAndGetElement(By.xpath("//label[text()='Canale']/following::input"), true).getAttribute("value");
        String sottocanale = util.waitAndGetElement(By.xpath("//label[text()='Sottocanale']/following::input"), true).getAttribute("value");
        String codiceIncaricato = util.waitAndGetElement(By.xpath("//label[text()='Codice Incaricato']/following::input"), true).getAttribute("value");
        String dataAdesione = util.waitAndGetElement(By.xpath("//label[contains(text(),'Data Adesione')]/following::input"), true).getAttribute("value");
        String numcontratto = util.waitAndGetElement(By.xpath("//label[contains(text(),'Numero Contratto')]/following::input"), true).getAttribute("value");


        if ((canale.length() < 2) || (canale.compareTo("CC Energia") != 0))
            throw new Exception("Canale non risulta prepopolato o diverso dal valore 'CC Energia'");

        if ((sottocanale.length() < 2) || (sottocanale.compareTo("OUT_TELEPERFORMANCE_TA_S2S") != 0))
            throw new Exception("Sottocanale non risulta prepopolato o diverso dal valore 'OUT_TELEPERFORMANCE_TA_S2S'");

        if (codiceIncaricato.length() < 2) throw new Exception("Incaricato non risulta prepopolato");

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String data = simpleDateFormat.format(calendar.getTime()).toString();
        Logger.getLogger("").log(Level.INFO, "Sysdate:" + data);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data = simpleDateFormat.format(calendar.getTime()).toString();
        if ((dataAdesione.length() < 2) || (dataAdesione.compareTo(data) != 0))
            throw new Exception("Data Adesione non risulta prepopolato o diversa da:" + data);
        if (numcontratto.length() < 2) throw new Exception("Numero contratto non risulta prepopolato");
    }

    public void verificaCampiPrepopolatiAllaccioPENP() throws Exception {
        String canale = util.waitAndGetElement(By.xpath("//label[text()='Canale']/following::input"), true).getAttribute("value");
        String sottocanale = util.waitAndGetElement(By.xpath("//label[text()='Sottocanale']/following::input"), true).getAttribute("value");
        String matricola = util.waitAndGetElement(By.xpath("//label[contains(text(),'Matricola Utente')]/following::input"), true).getAttribute("value");
        String dataAdesione = util.waitAndGetElement(By.xpath("//label[contains(text(),'Data Adesione')]/following::input"), true).getAttribute("value");
        String numcontratto = util.waitAndGetElement(By.xpath("//label[contains(text(),'Numero Contratto')]/following::input"), true).getAttribute("value");

//		System.out.println(canale);
        if ((canale.length() < 2) || (canale.compareTo("Punto Enel Negozio Partner") != 0))
            throw new Exception("Canale non risulta prepopolato o diverso dal valore 'Punto Enel Negozio Partner'");
//		System.out.println(sottocanale);
        if ((sottocanale.length() < 2) || (sottocanale.compareTo("KINEMA ENERGIE PF VIMERCATE") != 0))
            throw new Exception("Sottocanale non risulta prepopolato o diverso dal valore 'KINEMA ENERGIE PF VIMERCATE'");
//		System.out.println(matricola);
        if (matricola.length() < 2) throw new Exception("Incaricato non risulta prepopolato");
//		System.out.println(dataAdesione);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String data = simpleDateFormat.format(calendar.getTime()).toString();
        Logger.getLogger("").log(Level.INFO, "Sysdate:" + data);
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data = simpleDateFormat.format(calendar.getTime()).toString();
        if ((dataAdesione.length() < 2) || (dataAdesione.compareTo(data) != 0))
            throw new Exception("Data Adesione non risulta prepopolato o diversa da:" + data);
//		System.out.println(numcontratto);
        if (numcontratto.length() < 2) throw new Exception("Numero contratto non risulta prepopolato");


    }

    public void verificaMessaggioErroreSAP(String pod) throws Exception {
        String mess = util.waitAndGetElement(this.messaggioErroreSAP, true).getText();

        System.out.println(mess);
        if (!(mess.contains("La Categoria merceologica SAP non è congruente con l'Uso Fornitura del POD: " + pod)))
            throw new Exception("Messaggio d'errore non risulta popolato o non contiene: La Categoria merceologica SAP non è congruente con l'Uso Fornitura del POD " + pod);
    }

    public void compilaCampiRiepilogoOffertaSWA(String canale, String modalitafirma, String tipoutenza, boolean isPenp) throws Exception {

        util.selectLighningValue("Canale Invio Contratto", canale);
        if (canale.contentEquals("EMAIL")) {
            if (tipoutenza.contentEquals("S2S")) {
                util.objectManager(email, util.scrollAndSendKeys, "testing.crm.automation@gmail.com");
            }
            util.selectLighningValue("Modalità Firma", modalitafirma);
        } else {
            util.checkElementValue(modalitaFirma, modalitafirma);
        }

        if (util.exists(ConfermaTelefonica, 6)) {
            this.pressButton(ConfermaTelefonica);
        }

        if (isPenp) {
            util.selectLighningValue("Luogo/Modalità di sottoscrizione contratto", "Visita in negozio");
        } else if(tipoutenza.equals("PE")) {
            util.selectLighningValue("Luogo/Modalità di sottoscrizione contratto", "Visita in negozio");
        }

        this.pressButton(confermaRiepilogoOfferta);
        spinnerManager.checkSpinners();
    }

    public void compilaCampiRiepilogoOffertaSWA(String canale, String modalitafirma, String tipoutenza,
                                                String modalitaconferma, String digital) throws Exception {

        util.selectLighningValue("Canale Invio Contratto", canale);
        if (canale.contentEquals("EMAIL")) {
            if (tipoutenza.contentEquals("S2S")) {
                util.objectManager(email, util.scrollAndSendKeys, "testing.crm.automation@gmail.com");
            }
//			WebElement element = util.waitAndGetElement(email);
//		    String emailContenuto=element.getText();
//			if(emailContenuto.contentEquals("")) {
//				util.objectManager(email, util.scrollAndSendKeys,"testing.crm.automation@gmail.com");
//			}			

            util.selectLighningValue("Modalità Firma", modalitafirma);
        } else {
            util.checkElementValue(modalitaFirma, modalitafirma);
        }
        //Claudio aggiunta modalità conferma Digital
        if (digital.contentEquals("Y")) {
            util.selectLighningValue("Modalità conferma", modalitaconferma);
        }
//		if (tipoutenza.contentEquals("PE")) {
        if(tipoutenza.equals("PE") || tipoutenza.equals("PENP")) {
        	util.selectLighningValue("Luogo/Modalità di sottoscrizione contratto", "Visita in negozio");
		}
        this.pressButton(confermaRiepilogoOfferta);
    }

    public void compilaCampiRiepilogoOffertaDigitalSWA(String canale, String modalitafirma, String tipoutenza,
            String modalitaconferma, String digital) throws Exception {

		util.selectLighningValue("Canale Invio Contratto", canale);
		if (canale.contentEquals("EMAIL")) {
			if (tipoutenza.contentEquals("S2S")) {
				util.objectManager(email, util.scrollAndSendKeys, "testing.crm.automation@gmail.com");
			}
			util.selectLighningValue("Modalità Firma", modalitafirma);
		} else {
			util.checkElementValue(modalitaFirma, modalitafirma);
			}
		//Claudio aggiunta modalità conferma Digital
		  // righe commentate da modifica del 26 agosto 2021
//		if (digital.contentEquals("Y")) {
//			util.selectLighningValue("Modalità conferma", modalitaconferma);
//		}
		this.pressButton(confermaRiepilogoOfferta);
	}

    public void compilaCampiRiepilogoOffertaFibraSWA(String canale, String modalitafirma, String tipoutenza,
            String modalitaconferma, String indirizzoFibraOk) throws Exception {
		util.selectLighningValue("Canale Invio Contratto", canale);
		if (canale.contentEquals("EMAIL")) {
			if (!indirizzoFibraOk.contentEquals("SI")) {
				if (tipoutenza.contentEquals("S2S")) {
					util.objectManager(email, util.scrollAndSendKeys, "testing.crm.automation@gmail.com");
				}
			}
			util.selectLighningValue("Modalità Firma", modalitafirma);
		} else {
			util.checkElementValue(modalitaFirma, modalitafirma);
		}
//		//Claudio aggiunta modalità conferma Digital
//		if (digital.contentEquals("Y")) {
//			util.selectLighningValue("Modalità conferma", modalitaconferma);
//		}
//		if (tipoutenza.contentEquals("PE")) {
        if(tipoutenza.equals("PE") || tipoutenza.equals("PENP")) {
            util.selectLighningValue("Luogo/Modalità di sottoscrizione contratto", "Visita in negozio");
		}
		this.pressButton(confermaRiepilogoOfferta);
}
    
    
    public void scrollComponent(By object) throws Exception {
        util.objectManager(object, util.scrollToVisibility, false);
    }

    public void selezionaUsoFornitura(String uso_forn) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        WebElement element = util.waitAndGetElement(campoUsoFornitura, 60, 2);
        element.sendKeys(uso_forn);
        TimeUnit.SECONDS.sleep(3);
        element.sendKeys(Keys.ENTER);

    }

    public void verificaEsistenzaAnnulla() throws Exception {
        List<WebElement> button = driver.findElements(buttonAnnulla);
        if (button.size() != 2) {
            throw new Exception("in pagina il pulsante Annulla non e' presente due volte");
        }
    }

    public void verificaCampiSezioneRiepilogoOfferta() throws Exception {
// Claudio: righe commentate perchè trovava 10 campi anzichè 5 (lo fa solo alcune volte che duplica i campi)
        List<WebElement> campi = driver.findElements(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//label/ancestor::div[@role='none']//input"));
        if (campi.size() != 5) {
            throw new Exception("nella sezione 'Riepilogo offerta' i campi presenti sono in numero diverso da quello atteso, i campi sono: " + campi.size());
        }
        if (!util.exists(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//label[text()='Canale']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Riepilogo offerta' il campo 'Canale' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//label[text()='Sottocanale']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Riepilogo offerta' il campo 'Sottocanale' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Incaricato']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Riepilogo offerta' il campo 'Codice Incaricato' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//label[text()='Data Adesione']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Riepilogo offerta' il campo 'Data Adesione' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//label[text()='Numero Contratto']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Riepilogo offerta' il campo 'Numero Contratto' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Riepilogo offerta']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']"), 15))
            throw new Exception("nella sezione 'Riepilogo offerta' il pulsante 'Conferma' non esiste.");
    }

    public void verificaCampiFatturazioneElettronica() throws Exception {

        if (!util.exists(By.xpath("//h2//*[text()='Fatturazione Elettronica']"), 15))
            throw new Exception("la sezione Fatturazione Elettronica non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']"), 15))
            throw new Exception("nella sezione 'Fatturazione Elettronica' il tasto 'Conferma' non esiste.");
    }


    public void verificaCampiContattieConsensi() throws Exception {

        if (!util.exists(By.xpath("//h2//*[text()='Contatti e consensi']"), 15))
            throw new Exception("la sezione Contatti e consensi non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Contatti e consensi']/ancestor::div[@role='region']//button[text()='Conferma']"), 15))
            throw new Exception("nella sezione 'Contatti e consensi' il tasto 'Conferma' non esiste.");
    }

    public void verificaCampiModalitàFirma() throws Exception {

        if (!util.exists(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']"), 15))
            throw new Exception("la sezione Contatti e consensi non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']"), 15))
            throw new Exception("nella sezione 'Contatti e consensi' il tasto 'Conferma' non esiste.");
    }

    public void verificaCampiSezioneDatiFornitura() throws Exception {
	/*	 List<WebElement> campi = driver.findElements(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label/ancestor::div[@role='none']//input"));
		 if (campi.size()!=12) {
				throw new Exception("nella sezione 'Dati Fornitura' i campi presenti sono in numero diverso da quello atteso ");
		 }*/
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='POD']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'POD' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Misuratore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Misuratore' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Tensione di Consegna']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Tensione di Consegna' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Merceologica SAP' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Ateco']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Codice Ateco' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Titolarita' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenza contrattuale']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Potenza contrattuale' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenza disponibile']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Potenza disponibile' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Note al Distributore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Note al Distributore' non esiste.");
        //if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 15))
        //	throw new Exception("nella sezione 'Dati Fornitura' il campo 'Consumo Annuo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Disalimentabilita' non esiste.");
        // if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Flag Residente']/ancestor::div[@role='none']//input"), 15))
        //		throw new Exception("nella sezione 'Dati Fornitura' il campo 'Flag Residente' non esiste.");

    }

    public void verificaCampiSezioneCommodity() throws Exception {

        if (!util.exists(By.xpath("//h2[text()='Commodity']"), 15))
            throw new Exception("La sezione Commodity' non esiste.");
        if (!util.exists(By.xpath("//table[@role='table']//div[text()='Commodity']"), 15))
            throw new Exception("la sezione Commodity non esiste.");
        if (!util.exists(By.xpath("//table[@role='table']//div[text()='Indirizzo']"), 15))
            throw new Exception("La sezione Indirizzo non esiste.");
        if (!util.exists(By.xpath("//table[@role='table']//div[text()='Fornitura']"), 15))
            throw new Exception("La sezione Fornitura non esiste.");
        if (!util.exists(By.xpath("//table[@role='table']//div[text()='Uso Fornitura']"), 15))
            throw new Exception("La sezione Uso Fornitura non esiste.");
        if (!util.exists(By.xpath("//table[@role='table']//div[text()='Confermata']"), 15))
            throw new Exception("La sezione Confermata non esiste.");
        if (!util.exists(By.xpath("//h2[text()='Commodity']//ancestor::div[@role='main']//button[text()='Conferma']"), 15))
            throw new Exception("Nella sezione Commodity il tasto Conferma non esiste.");
    }


    public void verificaCampiSezioneReferente() throws Exception {

        if (!util.exists(sezioneReferente, 15))
            throw new Exception("La sezione 'Referente' non esiste.");
        if (!util.exists(buttonConfermaReferente, 15))
            throw new Exception("Nella sezione Referente il tasto Conferma non esiste.");
        if (!util.exists(nomeReferente, 15))
            throw new Exception("La sezione 'Nome referente' non esiste.");
        if (!util.exists(cognomeReferente, 15))
            throw new Exception("La sezione 'Cognome referente' non esiste.");
        if (!util.exists(tipoReferente, 15))
            throw new Exception("La sezione 'tipo referente' non esiste.");
        if (!util.exists(codFiscaleReferente, 15))
            throw new Exception("La sezione 'codice fiscale referente' non esiste.");
        if (!util.exists(campiSelezioneInSezioneReferente, 15))
            throw new Exception("Nella sezione Referente il check selezionabile non esiste.");
    }

    public void verificaCampiSezioneDatiFornituraGas() throws Exception {

        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='POD']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'POD' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Matricola contatore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Matricola contatore' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria di Consumo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria di Consumo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Merceologica SAP' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Ateco']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Codice Ateco' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Marketing']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Marketing' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine di Grandezza']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'rdine di Grandezza' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Note al Distributore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Note al Distributore' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Consumo Annuo' non esiste.");

        // if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Flag Residente']/ancestor::div[@role='none']//input"), 15))
        //throw new Exception("nella sezione 'Dati Fornitura' il campo 'Flag Residente' non esiste.");

        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenzialita']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Potenzialita' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Utilizzo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Utilizzo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Uso']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Uso' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Classe di prelievo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Classe di prelievo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Titolarita' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Lettura misuratore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Lettura misuratore' non esiste.");

    }

    public void verificaCampiSezioneDatiFornituraGasBus() throws Exception {

        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='POD']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'POD' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Matricola contatore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Matricola contatore' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria di Consumo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria di Consumo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Merceologica SAP' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Ateco']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Codice Ateco' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Marketing']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Marketing' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine di Grandezza']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'rdine di Grandezza' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Note al Distributore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Note al Distributore' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Consumo Annuo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenzialita']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Potenzialita' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Utilizzo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Utilizzo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Uso']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Categoria Uso' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Classe di prelievo']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Classe di prelievo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Titolarita' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Lettura misuratore']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Dati Fornitura' il campo 'Lettura misuratore' non esiste.");

    }

    public void verificaCampiSezioneScontiBonus() throws Exception {
        List<WebElement> campi = driver.findElements(By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//label/ancestor::div[@role='none']//input"));
        if (campi.size() != 3) {
            throw new Exception("nella sezione 'Sconti e Bonus' i campi presenti sono in numero diverso da quello atteso ");
        }
        if (!util.exists(By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Campagna']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Sconti e Bonus' il campo 'Codice Campagna' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//label[text()='Partnership']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Sconti e Bonus' il campo 'Partnership' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Amico']/ancestor::div[@role='none']//input"), 15))
            throw new Exception("nella sezione 'Sconti e Bonus' il campo 'Codice Amico' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']"), 15))
            throw new Exception("nella sezione 'Sconti e Bonus' il pulsante 'Conferma' non esiste.");
    }

    public void verificaCampiIndirizzoFatturazione() throws Exception {
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Presso']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Presso' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Regione']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Regione' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Provincia']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Provincia' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Comune']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Comune' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Località']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Località' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//div[@id='IndirizzopanelInputId']//label[text()='*Indirizzo']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Indizzo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Numero Civico']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Numero Civico' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Scala']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Scala' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Piano']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Piano' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Interno']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'Interno' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='CAP']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il campo 'CAP' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Fatturazione' il pulsante 'Verifica' non esiste.");

    }

    public void verificaCampiIndirizzoResidenza() throws Exception {
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Regione']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Regione' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Provincia']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Provincia' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Comune']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Comune' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Località']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Località' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//div[@id='IndirizzopanelInputId']//label[text()='*Indirizzo']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Indizzo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Numero Civico']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Numero Civico' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Scala']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Scala' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Piano']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Piano' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Interno']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'Interno' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='CAP']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il campo 'CAP' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']"), 15))
            throw new Exception("nella sezione 'Indirizzo di Residenza/Sede Legale' il pulsante 'Verifica' non esiste.");

    }

    public void verificaCampiIndirizzoUltimaFattura() throws Exception {
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Regione']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Regione' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Provincia']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Provincia' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Comune']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Comune' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Località']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Località' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//div[@id='IndirizzopanelInputId']//label[text()='*Indirizzo']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Indizzo' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='*Numero Civico']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Numero Civico' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Scala']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Scala' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Piano']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Piano' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='Interno']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'Interno' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//input/ancestor::div[@id='DivmodalModifyAddressABC']//label[text()='CAP']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il campo 'CAP' non esiste.");
        if (!util.exists(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']"), 15))
            throw new Exception("nella sezione 'Indirizzo Ultima Fattura' il pulsante 'Verifica' non esiste.");

    }

    public void compilaCampiRiepilogoOffertaSWATP8(String canale, String modalitafirma, String emailTP8, boolean isPenp) throws Exception {

        util.selectLighningValue("Canale Invio Contratto", canale);
        if (canale.contentEquals("EMAIL")) {
            util.objectManager(email, util.scrollAndSendKeys, emailTP8);
            util.selectLighningValue("Modalità Firma", modalitafirma);
        } else {
            util.checkElementValue(modalitaFirma, modalitafirma);
        }

        if (util.exists(ConfermaTelefonica, 6)) {
            this.pressButton(ConfermaTelefonica);
        }

        if (isPenp) {
            util.selectLighningValue("Luogo/Modalità di sottoscrizione contratto", "Visita in negozio");
        }
        this.pressButton(confermaRiepilogoOfferta);
        spinnerManager.checkSpinners();
    }

    public void verificaValore(By Object, String val) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        String valore = driver.findElement(Object).getAttribute("value");

        for (int i = 0; i < 8; i++) {
            TimeUnit.SECONDS.sleep(10);
            if (valore.contentEquals(val))
                break;
            else {
                WebElement element = util.waitAndGetElement(Object, 60, 2);
                element.sendKeys(val);
                //util.objectManager(Object, util.sendKeys, val);

            }
        }

        if (!valore.contentEquals(val))
            throw new Exception("il valore nel campo è diverso da quello atteso:il valore visualizzato e' " + valore + "; quello atteso e' " + val);

    }

    public void popolaCampiSezioneContattiConsensiSeVuoti(String consenso) throws Exception {
        TimeUnit.SECONDS.sleep(1);

        if (!util.verifyExistence(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"), baseTimeoutInterval))
            throw new Exception("il campo '" + consenso + "' non esiste");
        if (!util.verifyExistence(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input[@disabled]"), baseTimeoutInterval)) {
            String valore = driver.findElement(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input")).getAttribute("value");
            if (valore.contentEquals("ACQUISITO")) {

            } else if (valore.contentEquals("NON ESPRESSO")) {
					/*	clickComponentWithJse(By.xpath("//label[contains(text(),'"+consenso+"')]/ancestor::div[@role='none']//input"));
					    TimeUnit.SECONDS.sleep(1);
						clickComponent(By.xpath("//span[@title='ACQUISITO']/ancestor::c-ita_ifm_lwc047_combobox_item"));
						TimeUnit.SECONDS.sleep(1);	*/

                WebElement element = util.waitAndGetElement(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"), 5, 2);
                element.sendKeys("ACQUISITO");
                TimeUnit.SECONDS.sleep(1);

                element.sendKeys(Keys.ENTER);
                TimeUnit.SECONDS.sleep(1);
            } else if (valore.contentEquals("NO")) {
                clickComponentWithJse(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"));
                TimeUnit.SECONDS.sleep(3);
                if (util.verifyExistence(By.xpath("//span[@title='CONFERMATO']/ancestor::c-ita_ifm_lwc047_combobox_item"), 3)) {
                    clickComponentWithJse(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"));
                    TimeUnit.SECONDS.sleep(1);

                    WebElement element = util.waitAndGetElement(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"), 5, 2);
                    element.sendKeys("CONFERMATO");
                    TimeUnit.SECONDS.sleep(1);
                    //util.objectManager(By.xpath("//label[contains(text(),'"+consenso+"')]/ancestor::div[@role='none']//input"), util.sendKeys, "CONFERMATO");
                    element.sendKeys(Keys.ENTER);
                    TimeUnit.SECONDS.sleep(1);
                } else {

                    clickComponent(By.xpath("//span[@title='ACQUISITO']/ancestor::c-ita_ifm_lwc047_combobox_item"));
                    TimeUnit.SECONDS.sleep(1);
                }
            } else if (valore.contentEquals("SI")) {

                TimeUnit.SECONDS.sleep(1);

                WebElement element = util.waitAndGetElement(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"), 5, 2);
                element.sendKeys("CONFERMATO");
                TimeUnit.SECONDS.sleep(1);
                element.sendKeys(Keys.ENTER);
                TimeUnit.SECONDS.sleep(1);
            } else if (valore.contentEquals("CONFERMATO")) {
                WebElement element = util.waitAndGetElement(By.xpath("//label[contains(text(),'" + consenso + "')]/ancestor::div[@role='none']//input"), 5, 2);
                element.sendKeys("CONFERMATO");
                TimeUnit.SECONDS.sleep(1);
                element.sendKeys(Keys.ENTER);
                TimeUnit.SECONDS.sleep(1);
            }
        }


    }

    public void popolareCampiDatiFornituraGas(String categoriaMerceologicaSAP, String CategoriadiConsumo, String CategoriaMarketing, String flag, String OrdinediGrandezza,
                                              String Potenzialita, String Utilizzo, String CategoriaUso, String Classediprelievo, String Titolarita, String Profilodiconsumo) throws Exception {

        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+categoriaMerceologicaSAP+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + categoriaMerceologicaSAP + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria di Consumo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria di Consumo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+CategoriadiConsumo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));

        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + CategoriadiConsumo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Marketing']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Marketing']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+CategoriaMarketing+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + CategoriaMarketing + "']"));


				TimeUnit.SECONDS.sleep(1);
			    element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Flag Residente']/ancestor::div[@role='none']//input"),60,2);
			    clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Flag Residente']/ancestor::div[@role='none']//input"));
				TimeUnit.SECONDS.sleep(1);
				clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='"+flag+"']"));
				TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine di Grandezza']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine di Grandezza']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+OrdinediGrandezza+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + OrdinediGrandezza + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Profilo di consumo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Profilo di consumo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Profilodiconsumo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Profilodiconsumo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenzialita']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenzialita']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Potenzialita+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Potenzialita + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Utilizzo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Utilizzo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Utilizzo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Utilizzo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Uso']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Uso']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+CategoriaUso+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + CategoriaUso + "']"));

        TimeUnit.SECONDS.sleep(1);

        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Classe di prelievo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Classe di prelievo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Classediprelievo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Classediprelievo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Titolarita+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Titolarita + "']"));

        TimeUnit.SECONDS.sleep(1);


        util.objectManager(By.xpath("//span[text()='Lettura non fornita']/../.."), util.scrollAndClick);

        TimeUnit.SECONDS.sleep(1);
    }

    public void popolareCampiDatiFornituraBSNGas(String categoriaMerceologicaSAP, String CategoriadiConsumo, String CategoriaMarketing, String flag, String OrdinediGrandezza,
                                                 String Potenzialita, String Utilizzo, String CategoriaUso, String Classediprelievo, String Titolarita, String Profilodiconsumo) throws Exception {

        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+categoriaMerceologicaSAP+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + categoriaMerceologicaSAP + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria di Consumo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria di Consumo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+CategoriadiConsumo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));

        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + CategoriadiConsumo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Marketing']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Marketing']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+CategoriaMarketing+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + CategoriaMarketing + "']"));


        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Flag Residente']/ancestor::div[@role='none']//input"), 60, 2);
        element.sendKeys(flag);
        TimeUnit.SECONDS.sleep(1);

        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine di Grandezza']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine di Grandezza']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+OrdinediGrandezza+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + OrdinediGrandezza + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Profilo di consumo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Profilo di consumo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Profilodiconsumo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Profilodiconsumo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenzialita']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Potenzialita']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Potenzialita+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Potenzialita + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Utilizzo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Utilizzo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Utilizzo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Utilizzo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Uso']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Uso']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+CategoriaUso+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + CategoriaUso + "']"));

        TimeUnit.SECONDS.sleep(1);

        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Classe di prelievo']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Classe di prelievo']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Classediprelievo+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Classediprelievo + "']"));

        TimeUnit.SECONDS.sleep(1);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
//				clickComponent(By.xpath("//span[@title='"+Titolarita+"']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + Titolarita + "']"));

        TimeUnit.SECONDS.sleep(1);


        util.objectManager(By.xpath("//span[text()='Lettura non fornita']"), util.scrollAndClick);

        TimeUnit.SECONDS.sleep(1);
    }

    public void popolareCampiDatiFornitura(String categoria, String titolarita, String consumo, String flag) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + categoria + "']"));
        TimeUnit.SECONDS.sleep(1);

        verificaValore(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), categoria);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + titolarita + "']"));
        TimeUnit.SECONDS.sleep(1);

        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), util.sendKeys, consumo);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
       
        //Modifica per gestione flag residente non presente
        if (util.exists(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div//label[text()='Flag Residente']/ancestor::div[@role='none']//input"), 6)) {
           
        	 element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div//label[text()='Flag Residente']/ancestor::div[@role='none']//input"), 60, 2);
             element.sendKeys(flag);
             TimeUnit.SECONDS.sleep(1);
             element.sendKeys(Keys.ENTER);
             TimeUnit.SECONDS.sleep(5);
        	
        }
                        

        if (util.exists(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input[@aria-checked='true']//ancestor::div[1]//span[@class='slds-checkbox_faux']"), 6)) {
            util.objectManager(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input[@aria-checked='true']//ancestor::div[1]//span[@class='slds-checkbox_faux']"), util.scrollAndClick);
        }

    }

    public void popolareCampiDatiFornituraRES(String categoria, String titolarita, String consumo, String flag) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + categoria + "']"));
        TimeUnit.SECONDS.sleep(1);

        verificaValore(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), categoria);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + titolarita + "']"));
        TimeUnit.SECONDS.sleep(1);

        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), util.sendKeys, consumo);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div//label[text()='Flag Residente']/ancestor::div[@role='none']//input"), 60, 2);
        element.sendKeys(flag);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);
        TimeUnit.SECONDS.sleep(5);

        if (util.exists(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input[@aria-checked='true']//ancestor::div[1]//span[@class='slds-checkbox_faux']"), 6)) {
            util.objectManager(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input[@aria-checked='true']//ancestor::div[1]//span[@class='slds-checkbox_faux']"), util.scrollAndClick);
        }

    }

    public void popolareCampiDatiFornituraOrdineFittizio(String categoria, String titolarita, String consumo, String flag, String OrdineFit, String InvioDocumentazione) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), 60, 2);
        clickComponentWithJse(By.xpath("//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        clickComponent(By.xpath("//h2[text()='Dati fornitura']/ancestor::div[@role='region']//span[@class='slds-media__body']/span[text()='" + categoria + "']"));
        TimeUnit.SECONDS.sleep(1);

        verificaValore(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), categoria);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita']/ancestor::div[@role='none']//input"), 60, 2);
        element.sendKeys(titolarita);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), util.sendKeys, consumo);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
//			    element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div//label[text()='Flag Residente']/ancestor::div[@role='none']//input"),60,2);
//				element.sendKeys(flag);		
//				TimeUnit.SECONDS.sleep(1);
//				element.sendKeys(Keys.ENTER);
        TimeUnit.SECONDS.sleep(5);
//				if(util.exists(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input[@aria-checked='true']//ancestor::div[1]//span[@class='slds-checkbox_faux']"), 6)) {
//				util.objectManager(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input"), util.scrollAndClick);
//					}
        driver.findElement(By.xpath("//div[@data-label='ITA_IFM_Fittizio_Order_flag__c']//input")).sendKeys(Keys.SPACE);


        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Dati fornitura']/ancestor::div//label[text()='Invio Documentazione']/ancestor::div[@role='none']//input"), 60, 2);
        element.sendKeys(InvioDocumentazione);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);
    }

    public void popolaCampiIndirizzoFatturazioneSeVuoti(String provinciaComune, String indirizzo, String civico) throws Exception {
        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Provincia']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Comune']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), indirizzo);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Indirizzo']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), civico);
        }
    }

    public void popolaCampiIndirizzoUltimaFatturaSeVuoti(String provinciaComune, String indirizzo, String civico) throws Exception {
        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Provincia']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Comune']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), indirizzo);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Indirizzo']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo Ultima Fattura']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), civico);
        }
    }

    public void popolaCampiIndirizzoFatturazioneNonNormalizzatoSeVuoti(String provinciaComune, String indirizzo, String civico) throws Exception {
        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Provincia']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Comune']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), indirizzo);
            TimeUnit.SECONDS.sleep(1);
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), civico);
        }
    }

    public void popolaCampiIndirizzoResidenzaSeVuoti(String provinciaComune, String indirizzo, String civico) throws Exception {
        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Provincia']/ancestor::div[@id='ProvinciapanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Provincia']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Comune']/ancestor::div[@id='ComunepanelInputId']//input"), provinciaComune);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Comune']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Indirizzo']/ancestor::div[@id='IndirizzopanelInputId']//input"), indirizzo);
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='*Indirizzo']/../input/ancestor::div[@class='cAddressFieldAutocomplete']//li[@data-record='0']"));
        }

        if (util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), true).getAttribute("value").length() < 1) {
            insertTextByChar(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='*Numero Civico']/ancestor::lightning-input[1]//input[@name='civic']"), civico);
        }
    }


    public void checkValoreInTable(String commodity) throws Exception {
        TimeUnit.SECONDS.sleep(1);
				/*
				 WebElement valore=util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[@role='region']//div[text()='" + commodity + "']/ancestor::tbody//label"));
			//	System.out.println(valore);
				if (valore == null)
					throw new Exception("nella tabella Commodity in corrispondenza della colonna Commodity' non e' presente il valore: "+commodity+"; ma il valore "+valore);
			
				 */
//				String valore=util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[@role='region']//div[text()='ELETTRICO']/ancestor::tbody/tr/td[2]")).getText();
        //	System.out.println(valore);
        String valore = util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[@role='region']//div[text()='" + commodity + "']/ancestor::tbody/tr/td[2]")).getText();
        if (!valore.contentEquals(commodity))
            throw new Exception("nella tabella Commodity in corrispondenza della colonna Commodity' non e' presente il valore: " + commodity + "; ma il valore " + valore);
        else {
            util.objectManager(By.xpath("//h2//*[text()='Commodity']/ancestor::div[@role='region']//div[text()='" + commodity + "']/ancestor::tbody//label"), util.scrollToVisibility, false);
            util.objectManager(By.xpath("//h2//*[text()='Commodity']/ancestor::div[@role='region']//div[text()='" + commodity + "']/ancestor::tbody//label"), util.scrollAndClick);
        }

    }

    public void checkConfermata(String commodity) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        String valore = util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th[2]/div//*[text()='Commodity']/ancestor::table/tbody/tr/td[7]/div/div")).getText();
        //	System.out.println(valore);
        if (!valore.contentEquals(commodity))
            throw new Exception("nella tabella Commodity in corrispondenza della colonna Confermata non e' presente il valore: " + commodity + "; ma il valore " + valore);

    }

    public void checkConfermataDUAL(String pod) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        String p = "//div[text()='Confermata']/ancestor::table//tbody//td//div//div[text()='#']/ancestor::tr//td[7]/div";
        String xpathString = p.replace("#", pod);
        By item = By.xpath(xpathString);
        WebElement confermata = util.waitAndGetElement(item);
        if (!confermata.getText().contentEquals("SI"))
            throw new Exception("nella tabella Commodity in corrispondenza della colonna Confermata non e' presente il valore: 'SI'");

    }

    public void popolareCommodity(String residente, String titolarita, String ordine, String telefono, String ascensore, String disalimentabilita, String consumo) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Residente']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Residente']/ancestor::div[@role='none']//input"), util.sendKeys, residente);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita’']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Titolarita’']/ancestor::div[@role='none']//input"), util.sendKeys, titolarita);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine Fittizio']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine Fittizio']/ancestor::div[@role='none']//input"), util.sendKeys, ordine);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Telefono Distributore']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Telefono Distributore']/ancestor::div[@role='none']//input"), util.sendKeys, telefono);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ascensore']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ascensore']/ancestor::div[@role='none']//input"), util.sendKeys, ascensore);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita’']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita’']/ancestor::div[@role='none']//input"), util.sendKeys, disalimentabilita);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), util.sendKeys, consumo);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);
    }

    public void popolareUsoFornitura(String uso) throws Exception {
 
        //Uso Fornitura
		util.selectLighningValue("Uso Fornitura",uso);
//		util.verifyExistence(uso, 10).sendKeys(Keys.ENTER);
  	
    }

    public void popolareCommodityCla(String categoriaMerceologica, String titolarita, String ordine, String telefono, String ascensore, String disalimentabilita, String consumo) throws Exception {
        WebElement element;

        //gestire Catgoria Merceologica
		util.selectLighningValue("Categoria Merceologica SAP",categoriaMerceologica);
		
        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine Fittizio']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine Fittizio']/ancestor::div[@role='none']//input"), util.sendKeys, ordine);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Telefono Distributore']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Telefono Distributore']/ancestor::div[@role='none']//input"), util.sendKeys, telefono);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ascensore']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ascensore']/ancestor::div[@role='none']//input"), util.sendKeys, ascensore);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita’']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita’']/ancestor::div[@role='none']//input"), util.sendKeys, disalimentabilita);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), util.sendKeys, consumo);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);
    }

    
    public void popolareCommodityBusiness(String residente, String categoria, String ordine, String telefono, String ascensore, String disalimentabilita, String consumo) throws Exception {
        TimeUnit.SECONDS.sleep(10);

        String val = driver.findElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Residente']/ancestor::div[@role='none']//input")).getAttribute("value");

        if (!val.contentEquals(residente))
            throw new Exception("il valore nel campo 'Residente' è diverso da quello atteso:il valore visualizzato e' " + val + "; quello atteso e' " + residente);

        TimeUnit.SECONDS.sleep(10);

        WebElement element;

        clickComponentWithJse(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(10);

        clickComponent(By.xpath("//span[@title='" + categoria + "']/ancestor::c-ita_ifm_lwc047_combobox_item"));
        TimeUnit.SECONDS.sleep(10);

        verificaValore(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//input"), categoria);


        TimeUnit.SECONDS.sleep(10);
        element = util.waitAndGetElement(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine Fittizio']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ordine Fittizio']/ancestor::div[@role='none']//input"), util.sendKeys, ordine);
        TimeUnit.SECONDS.sleep(10);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(10);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Telefono Distributore']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Telefono Distributore']/ancestor::div[@role='none']//input"), util.sendKeys, telefono);
        TimeUnit.SECONDS.sleep(10);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(10);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ascensore']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Ascensore']/ancestor::div[@role='none']//input"), util.sendKeys, ascensore);
        TimeUnit.SECONDS.sleep(10);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(10);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita’']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Disalimentabilita’']/ancestor::div[@role='none']//input"), util.sendKeys, disalimentabilita);
        TimeUnit.SECONDS.sleep(10);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(10);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Commodity']/ancestor::div[contains(@class,'slds-card')]//label[text()='Consumo Annuo']/ancestor::div[@role='none']//input"), util.sendKeys, consumo);
        TimeUnit.SECONDS.sleep(10);
        element.sendKeys(Keys.ENTER);
    }

    public void popolareInd(String indirizzo) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='Cliente fornisce l’indirizzo?']/ancestor::div[@class='boxInput']//select"), 60, 2);
        String valore = driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='Cliente fornisce l’indirizzo?']/ancestor::div[@class='boxInput']//select")).getAttribute("value");
        if (!valore.contentEquals("NO")) {
            driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='Cliente fornisce l’indirizzo?']/ancestor::div[@class='boxInput']//select")).click();
            TimeUnit.SECONDS.sleep(1);
            driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='Cliente fornisce l’indirizzo?']/ancestor::div[@class='boxInput']//select//option[1]")).click();
        } else {
            clickComponent(buttonConfermaIndirizzoRes);
            checkSpinnersSFDC();
        }
    }

    public void popolaIndirizzoFatt() throws Exception {
        if (driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma' and @disabled='true']")).isDisplayed()) {
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//a[text()='Verifica Indirizzi Esistenti']"));
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//div[@aria-labelledby='existingAddresses__item']//h3/a[text()='Indirizzo di Residenza']/ancestor::ul//li[@class='slds-item'][2]//label/span[@class='slds-checkbox--faux']"));
            TimeUnit.SECONDS.sleep(3);
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Importa']"));
            verificaCampiIndirizzoFatturazione();
            checkCampiIndirizzoFattDis();

            checkSpinnersSFDC();
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']"));
            checkSpinnersSFDC();
            if (driver.findElement(By.xpath("//h2/span[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//div[@title='Indirizzo non verificato:']")).isDisplayed()) {
                clickComponent(By.xpath("//h2/span[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"));
                TimeUnit.SECONDS.sleep(1);
                util.objectManager(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='CAP']/ancestor::div[@class='inputSpinner']/input"), util.sendKeys, "00178");
                TimeUnit.SECONDS.sleep(1);
                clickComponent(By.xpath("//label[text()='CAP']/../input/ancestor::div[@class='slds-form-element slds-lookup slds-is-open cAddressFieldAutocompleteZip']//li[@data-record='0']"));
                TimeUnit.SECONDS.sleep(1);
                clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Forza Indirizzo']"));
                TimeUnit.SECONDS.sleep(1);
            }
            if (driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo verificato']")).isDisplayed()) {
                clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']"));
                checkSpinnersSFDC();
            }
        }

    }

    public void popolaIndirizzoFattCla() throws Exception {
        if (driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma' and @disabled='true']")).isDisplayed()) {
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//a[text()='Verifica Indirizzi Esistenti']"));
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//div[@aria-labelledby='existingAddresses__item']//h3/a[text()='Indirizzo di Residenza']/ancestor::ul//li[@class='slds-item'][2]//label/span[@class='slds-checkbox--faux']"));
            TimeUnit.SECONDS.sleep(3);
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Importa']"));
            verificaCampiIndirizzoFatturazione();
            checkCampiIndirizzoFattDis();
            Thread.sleep(5000);
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Verifica']"));
            Thread.sleep(5000);

            By msgIndirizzo = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo verificato']");
    		WebElement el = driver.findElement(msgIndirizzo);
    		String testo = el.getText();
    		if(!testo.contentEquals("Indirizzo verificato")) {
    			System.out.println("Indirizzo Verificato");
                clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']"));
                checkSpinnersSFDC();
    		} else {
    			System.out.println("Indirizzo non verificato");
                clickComponent(By.xpath("//h2/span[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"));
                TimeUnit.SECONDS.sleep(1);
                util.objectManager(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[text()='CAP']/ancestor::div[@class='inputSpinner']/input"), util.sendKeys, "00178");
                TimeUnit.SECONDS.sleep(1);
                clickComponent(By.xpath("//label[text()='CAP']/../input/ancestor::div[@class='slds-form-element slds-lookup slds-is-open cAddressFieldAutocompleteZip']//li[@data-record='0']"));
                TimeUnit.SECONDS.sleep(1);
                clickComponent(By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//button[text()='Forza Indirizzo']"));
                TimeUnit.SECONDS.sleep(1);
    		}
        }

    }

    
    public void popolaIndirizzoRes() throws Exception {
        if (driver.findElement(labelIndirizzoResNonVerificato).isDisplayed()) {
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"));
            TimeUnit.SECONDS.sleep(1);
            util.objectManager(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//label[text()='CAP']/ancestor::div[@class='inputSpinner']/input"), util.sendKeys, "00178");
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//label[text()='CAP']/../input/ancestor::div[@class='slds-form-element slds-lookup slds-is-open cAddressFieldAutocompleteZip']//li[@data-record='0']"));
            TimeUnit.SECONDS.sleep(1);
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Forza Indirizzo']"));
            TimeUnit.SECONDS.sleep(1);
        }
        if (driver.findElement(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//span[text()='Indirizzo verificato']")).isDisplayed()) {
            clickComponent(By.xpath("//h2//*[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[contains(@class,'slds-card')]//button[text()='Conferma']"));
            checkSpinnersSFDC();
        }
    }

    public void checkCampiIndirizzoFattDis() throws Exception {
        verifyComponentExistence(By.xpath("//h2/span[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Regione')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2/span[text()='Indirizzo di Fatturazione']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'CAP')]/following::input[1][@disabled]"));
    }

    public void checkCampiFatturazioneElettronica() throws Exception {
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Canale Invio')]/following::input[1]"));
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Codice Ufficio')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'PEC')]/following::input[1][@disabled]"));

        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Tipo Invio')]/following::input[1][@disabled]"));
    }

    public void checkCampiFatturazioneElettronicaBusiness() throws Exception {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/YYYY");
        Calendar calendar = new GregorianCalendar();
        fmt.setCalendar(calendar);
        String dateFormatted = fmt.format(calendar.getTime());

        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Tipo Invio')]/following::input[1][@disabled]"));
        String inizio = driver.findElement(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]")).getAttribute("value");
        if (!inizio.contentEquals(dateFormatted))
            throw new Exception("il valore nel campo 'inizio validita'' è diverso da quello atteso:il valore visualizzato e' " + inizio + "; quello atteso e' dateFormatted");
        String fine = driver.findElement(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]")).getAttribute("value");
        if (!fine.contentEquals("31/12/2999"))
            throw new Exception("il valore nel campo 'fine validita'' è diverso da quello atteso:il valore visualizzato e' " + fine + "; quello atteso e' 31/12/2999");
        String tipo = driver.findElement(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Tipo Invio')]/following::input[1][@disabled]")).getAttribute("value");
        if (!tipo.contentEquals("DOPPIO"))
            throw new Exception("il valore nel campo 'tipo invio' è diverso da quello atteso:il valore visualizzato e' " + tipo + "; quello atteso e' DOPPIO");

    }

    public void checkCampiFatturazioneElettronicaBusiness2() throws Exception {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/YYYY");
        Calendar calendar = new GregorianCalendar();
        fmt.setCalendar(calendar);
        String dateFormatted = fmt.format(calendar.getTime());

        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]"));

        String inizio = driver.findElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]")).getAttribute("value");
        if (!inizio.contentEquals(dateFormatted))
            throw new Exception("il valore nel campo 'inizio validita'' è diverso da quello atteso:il valore visualizzato e' " + inizio + "; quello atteso e' dateFormatted");
        String fine = driver.findElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]")).getAttribute("value");
        if (!fine.contentEquals("31/12/2999"))
            throw new Exception("il valore nel campo 'fine validita'' è diverso da quello atteso:il valore visualizzato e' " + fine + "; quello atteso e' 31/12/2999");
        By codiceUfficio = By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Codice Ufficio')]/following::input[1]");
        verifyComponentExistence(codiceUfficio);
        By pec = By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'PEC')]/following::input[1][@disabled]");
        verifyComponentExistence(pec);

    }

    public void checkCampiFatturazioneElettronicaConPA() throws Exception {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/YYYY");
        Calendar calendar = new GregorianCalendar();
        fmt.setCalendar(calendar);
        String dateFormatted = fmt.format(calendar.getTime());

        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]"));
        verifyComponentExistence(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]"));

        String inizio = driver.findElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Inizio Validità')]/following::input[1][@disabled]")).getAttribute("value");
        if (!inizio.contentEquals(dateFormatted))
            throw new Exception("il valore nel campo 'inizio validita'' è diverso da quello atteso:il valore visualizzato e' " + inizio + "; quello atteso e' dateFormatted");
        String fine = driver.findElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Data Fine Validità')]/following::input[1][@disabled]")).getAttribute("value");
        if (!fine.contentEquals("31/12/2999"))
            throw new Exception("il valore nel campo 'fine validita'' è diverso da quello atteso:il valore visualizzato e' " + fine + "; quello atteso e' 31/12/2999");
        By codiceUfficio = By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Codice Ufficio')]/following::input[1]");
        verifyComponentExistence(codiceUfficio);
        By codiceUfficioRichiesto = By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Codice Ufficio Richiesto')]/following::input[1]");
        verifyComponentExistence(codiceUfficioRichiesto);
        By pubblicaAmministrazione = By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Tipologia pubblica amministrazione')]/following::input[1]");
        verifyComponentExistence(pubblicaAmministrazione);

    }

    public void popolaCampiSplitPayment() throws Exception {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/YYYY");
        Calendar calendar = new GregorianCalendar();
        fmt.setCalendar(calendar);
        String dateFormatted = fmt.format(calendar.getTime());
        By inizioValidita = By.xpath("//h2[text()='Split Payment']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Inizio Validità')]/following::input[1]");
        //By fineValidita=By.xpath("//h2[text()='Split Payment']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Fine Validità')]/following::input[1]");
        util.objectManager(inizioValidita, util.sendKeys, dateFormatted);
        //util.objectManager(fineValidita, util.sendKeys,"31/12/2999");
    }

    public void popolaCampiFatturazioneElettronica(String canale, String codiceUfficio) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Canale Invio')]/following::input[1]"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Canale Invio')]/following::input[1]"), util.sendKeys, canale);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Codice Ufficio')]/following::input[1]"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Fatturazione Elettronica']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Codice Ufficio')]/following::input[1]"), util.sendKeys, codiceUfficio);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);
    }

    public void checkValoreInTable2AndClosePopup(String metodoPag) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        util.objectManager(cercaMetodoPagamento, util.sendKeys, metodoPag);
        TimeUnit.SECONDS.sleep(3);

        //String valore=util.waitAndGetElement(By.xpath("//h2/span[text()='Metodo di pagamento']/ancestor::div[contains(@class,'slds-card')]//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th[2]/div/div/div[text()='Stato']/ancestor::table/tbody/tr/td[2]/div/div")).getText()
        String valore = util.waitAndGetElement(By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th[2]//div[text()='Stato']/ancestor::table/tbody/tr/td[2]/div/div")).getText();
        //	System.out.println(valore);
        if (!valore.contentEquals(metodoPag))
            throw new Exception("nella tabella metodo di pagamento in corrispondenza della colonna stato non e' presente il valore: " + metodoPag + "; ma il valore " + valore);
        else {
            util.objectManager(By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th[2]//div[text()='Stato']/ancestor::table/tbody/tr/td//span[@class='slds-radio_faux']"), util.scrollToVisibility, false);
            util.objectManager(By.xpath("//h2//*[text()='Metodo di pagamento']/ancestor::div//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th[2]//div[text()='Stato']/ancestor::table/tbody/tr/td//span[@class='slds-radio_faux']"), util.scrollAndClick);
            util.objectManager(By.xpath("//h2[text()='Deposito Cauzionale']/ancestor::div[@data-id='modal']//p[text()='Ricorda che il metodo di pagamento “Bollettino Postale” comporta l’addebito del deposito cauzionale.']/ancestor::div[@data-id='modal']//footer//button[text()='Ok']"), util.scrollToVisibility, false);
            util.objectManager(By.xpath("//h2[text()='Deposito Cauzionale']/ancestor::div[@data-id='modal']//p[text()='Ricorda che il metodo di pagamento “Bollettino Postale” comporta l’addebito del deposito cauzionale.']/ancestor::div[@data-id='modal']//footer//button[text()='Ok']"), util.scrollAndClick);
        }

    }

    public void verificaValoreCodiceCampagna() throws Exception {
        TimeUnit.SECONDS.sleep(3);
        String c = driver.findElement(By.xpath("//h2//*[text()='Sconti e Bonus']/ancestor::div[contains(@class,'slds-card')]//label[text()='Codice Campagna']/ancestor::div[1]//input")).getAttribute("value");

        if (c.contentEquals("")) throw new Exception("il campo 'Codice Campagna' non e' popolato ");

    }

    public void popolareCampiModFirma(String firma, String canale) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(By.xpath("//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']//input"), util.sendKeys, firma);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

        TimeUnit.SECONDS.sleep(1);
        element = util.waitAndGetElement(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Canale Invio']/ancestor::div[@role='none']//input"), 60, 2);
        util.objectManager(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Canale Invio']/ancestor::div[@role='none']//input"), util.sendKeys, canale);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);

    }

    public void checkCampiModFirma() throws Exception {
        TimeUnit.SECONDS.sleep(1);

        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Canale Invio']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Indirizzo Email']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Cellulare']/ancestor::div[@role='none']//input"));

    }

    public void checkCampiModFirmaNoVocal() throws Exception {
        TimeUnit.SECONDS.sleep(1);

        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Canale Invio']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);
        verifyComponentExistence(By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Indirizzo Email']/ancestor::div[@role='none']//input"));
        TimeUnit.SECONDS.sleep(1);

    }

    public void popolareCampo(String valore, By object) throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElement element = util.waitAndGetElement(object, 60, 2);
        util.objectManager(object, util.sendKeys, valore);
        TimeUnit.SECONDS.sleep(1);
        element.sendKeys(Keys.ENTER);
    }

    public void verificaCvp() throws Exception {
        TimeUnit.SECONDS.sleep(3);
        String cvpLight = driver.findElement(By.xpath("//label[text()='Esito CVP Light']/ancestor::div[1]//input[contains(@id,'Esito CVP Light')]")).getAttribute("value");
        if (!cvpLight.contentEquals("OK")) throw new Exception("il campo 'Esito CVP Light' non e' popolato con OK ");
        String cvpMind = driver.findElement(By.xpath("//label[text()='Presa in Carico MIND Light']/ancestor::div[1]//input[contains(@id,'Presa in Carico MIND Light')]")).getAttribute("value");
        if (!cvpMind.contentEquals("OK"))
            throw new Exception("il campo 'Presa in Carico MIND Light' non e' popolato con OK ");
        String cvpVbl = driver.findElement(By.xpath("//label[text()='Esito VBL']/ancestor::div[1]//input[contains(@id,'Esito VBL')]")).getAttribute("value");
        if (!cvpVbl.contentEquals("OK")) throw new Exception("il campo 'Esito VBL' non e' popolato con OK ");

    }

    public void verificaCampiPrepopolatiModalitaFirmaCanale() throws Exception {
        String modalitaFirma = util.waitAndGetElement(campoModalitaFirma, true).getAttribute("value");
        String canaleInvio = util.waitAndGetElement(campoCanale, true).getAttribute("value");
        String indirizzoEmail = util.waitAndGetElement(campoEmail, true).getAttribute("value");
        String cellulare = util.waitAndGetElement(campoCellulare, true).getAttribute("value");

        if (!modalitaFirma.contentEquals("DIGITAL"))
            throw new Exception("il campo modalita firma non risulta correttamente prepopolato");
        if (!canaleInvio.contentEquals("EMAIL"))
            throw new Exception("il campo canale non risulta correttamente prepopolato");
        if (!indirizzoEmail.contentEquals("testing.crm.automation@gmail.com"))
            throw new Exception("il campo Indirizzo Email non risulta correttamente prepopolato");
        if (!cellulare.contentEquals("3929926614"))
            throw new Exception("il campo Cellulare non risulta correttamente prepopolato");


    }

    public boolean verificaCampiEMailPopolato() throws Exception {

        String indirizzoEmail = util.waitAndGetElement(campoEmail, true).getAttribute("value");
        boolean flag = false;
        if (indirizzoEmail.length() > 0)
            flag = true;
        return flag;

    }

    // AnnullamentoOfferta
    public By popUpAnnullaTitle = By.xpath("//h2[text()='Annulla']");
    public By popUpAnnullaTesto = By.xpath("//h2[text()='Annulla']/ancestor::div[1]//p");
    public By popUpAnnullaButtonChiudi = By.xpath("//h2[text()='Annulla']/ancestor::div[1]//button[text()='Chiudi']");
    public By popUpAnnullaButtonConferma = By.xpath("//h2[text()='Annulla']/ancestor::div[1]//button[text()='Conferma']");

    public String popUpAnnullaTestoAtteso = "Confermi di voler annullare l'Offerta?";

    public By popUpAnnullaSceltaMotivoNoteAdd = By.xpath("//textarea[@class='slds-textarea']");
    public By popUpAnnullaSceltaMotivo = By.xpath("//label[contains(text(),'Causale di annullamento')]/following::select[@name='cancellationReasonsPicklist']");
    public By popUpAnnullaSceltaMotivoEsito = By.xpath("//h2[text()='Annullamento Richiesta']/ancestor::div[1]//p[@id='annulmentResultAlert']");
    public By popUpAnnullaSceltaButtonChiudi = By.xpath("//h2[text()='Annullamento Richiesta']/ancestor::div[1]//button[text()='Chiudi']");
    public By popUpAnnullaSceltaButtonConferma = By.xpath("//h2[text()='Annullamento Richiesta']/ancestor::div[1]//button[text()='Conferma']");
    private By ordineFittizio = By.xpath("//span[contains(text(),'Flag Ordine Fittizio')]");


    public void verificaPopUpconTesto(By obj, By objText, By button, String popupTestoAtteso) throws Exception {
        if (!util.verifyExistence(obj, 60))
            throw new Exception("Non è stata visualizzata la pop up con testo: " + popupTestoAtteso);
        String textModal = util.waitAndGetElement(objText, false).getText();
//				System.out.println("Messaggio visualizzato	:"+textModal);
//				System.out.println("Messaggio atteso		:"+popupTestoAtteso);
        if (textModal.compareTo(popupTestoAtteso) != 0)
            throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: " + textModal + " testo atteso:" + popupTestoAtteso);
        util.objectManager(button, util.scrollAndClick);

    }


    public void popUpSceltaAnnullamento() throws Exception {

        // scelta annullamento
        util.objectManager(popUpAnnullaSceltaMotivo, util.select, "Ripensamento");

        //scrivi note
        util.objectManager(popUpAnnullaSceltaMotivoNoteAdd, util.clear);
        util.objectManager(popUpAnnullaSceltaMotivoNoteAdd, util.sendKeys, "Annulla richiesta crea offerta");

        // premi conferma
        util.objectManager(popUpAnnullaSceltaButtonConferma, util.scrollAndClick);

    }

    public void popUpSceltaAnnullamentoEsito(By obj) throws Exception {

        if (!util.verifyExistence(obj, 60)) throw new Exception("Non è stata visualizzato l'esito");
        util.objectManager(popUpAnnullaSceltaButtonChiudi, util.scrollAndClick);
    }

    public void verificaAssenzaOrdineFittizio() throws Exception {
        if (util.verifyExistence(this.ordineFittizio, 10))
            throw new Exception("Il campo Ordine fittizio è presente quando non dovrebbe");
       // util.objectManager(popUpAnnullaSceltaButtonChiudi, util.scrollAndClick);

    }

    public void click(By xpath) {
        driver.findElement(xpath).click();
    }

    public void inserisciDataRichiestaSottoscrizione() throws InterruptedException {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"), Locale.ITALY);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String data = simpleDateFormat.format(calendar.getTime()).toString();

        driver.findElement(DataRichiestaSottoscrizione).click();
        TimeUnit.SECONDS.sleep(2);

        driver.findElement(DataRichiestaSottoscrizione).clear();
        driver.findElement(DataRichiestaSottoscrizione).sendKeys(data);

        try {
            spinnerManager.checkSpinners();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
    
    public String verificaPopUpRipensamento(String titoloPopUp, String testoPopUp) {
         String titolo = driver.findElement(titoloInformativa).getText();
         String testo = driver.findElement(testoInformativa).getText();
         String result = null;
         
         if((titoloPopUp.equals(titolo)) && (testoPopUp.equals(testo))) {
        	  result = "Verifica sul popUp ripensamento riuscita: Il popup è presente e il testo è corretto";
         } else {
        	 result = "Controllo sul popUp ripensamento non riuscito";
         }
		
		return result;
    }
    
    
}
