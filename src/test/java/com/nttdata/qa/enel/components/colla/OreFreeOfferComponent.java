package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OreFreeOfferComponent extends BaseComponent {	
	//public By checkOrefreeAvailabilityBtn = By.xpath("//div[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-orefree']//a[@href='/it/adesione']");
	public By checkOrefreeAvailabilityBtn = By.xpath("//div[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-orefree']//a");
	public By titleLabel = By.cssSelector("#code-insert-modal > div > h6");
	public By subtitleLabel = By.cssSelector("#code-insert-modal > div > p");
	public By podField = By.xpath("//input[@placeholder='Codice POD']");
	public By submitBtn = By.xpath("//button[@id='scegli-tu-orefree']");
	public By infoLink = By.cssSelector("#code-insert-modal > div > div.infoLink > p > a");
	public By errorLabel = By.xpath("//span[@id='error-label']");
	public By faqPageTitle = By.cssSelector("#main > section.hero-detail.image-hero.image-hero-custom.module.image-hero-wrapper.vs-textcolor-default > div > div > h1");
	public By faqPageSubtitle = By.cssSelector("#main > section.hero-detail.image-hero.image-hero-custom.module.image-hero-wrapper.vs-textcolor-default > div > p");
	public By wrongPodErrorLabel = By.xpath("//span[@id='error-label']");
	public By acceptancePageTitleLabel = By.cssSelector("#adesione_standard > h1");
	public By adesioneContrattoTitle = By.xpath("//h1[@class='header']");
	public By oreFree = By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/scegli-tu-orefree']");
	
	public OreFreeOfferComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyPageUrl(String pageUrl) throws Exception {
		String theUrl = driver.getCurrentUrl();
		if (!theUrl.equals(pageUrl)) {
			throw new Exception("Page URL (" + theUrl + ") is different from expected: " + pageUrl);
		}
	}
	
	public void insertPod (String pod) throws Exception {
		verifyComponentVisibility(podField);
		WebElement thePodField = driver.findElement(podField);
		thePodField.clear();
		thePodField.sendKeys(pod);
		Thread.sleep(1000);
		clickComponent(submitBtn);
	}
	
	public void checkWrongPodMessage(String errorMesage) throws Exception {
		verifyComponentVisibility(wrongPodErrorLabel);
		verifyComponentText(wrongPodErrorLabel, errorMesage);
	}
}
