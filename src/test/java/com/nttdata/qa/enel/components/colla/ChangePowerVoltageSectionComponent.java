package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ChangePowerVoltageSectionComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By modificaPotenzaETensioneButton=By.xpath("//a[text()='Modifica potenza e tensione']");
	public By headerDescriptionCambioPotenzaContatorePage=By.xpath("//div[@class='image-hero_inner image-hero-container container']");	
    public By textCambioPotenzaContatorePage=By.xpath("//section[@data-module='article-text-container']");	
	public By accediAreaRiservataLabel=By.xpath("//h3[text()='Accedi alla tua Area Riservata']");	
	public By emailLabel=By.xpath("//div[@class='cessazione-form-group']//label[text()='Email']");
	public By emailInputtext=By.id("txtUsernameCes");
	public By passwordLabel=By.xpath("//div[@class='cessazione-form-group']//label[text()='Password']");
	public By passwordInputtext=By.id("txtPasswordCes");
    public By accediButton=By.id("accessBtnCes");
    public By notregisteredLabel= By.xpath("//p[text()='Non sei registrato?']");
    public By registratiLink=By.xpath("//a//span[text()='Registrati']");
    public By headerDescriptionRegistrazionePage=By.xpath("//section[@class='cmMainContent cmPaddingYlg text-center']//div[@class='container']");
    public By continuaButton=By.id("continua-button-initial-modal");
    public By vaiAreaClientiButton=By.xpath("//a[@class='btn-cta--pink ']");
    public By questionsList=By.xpath("//div[@class='dropdown parbase']");
    public By questionsDetails=By.xpath("//section//section//button[@aria-expanded='false']");
    public By plusImage=By.xpath("//section[@data-accordion]");
    public By textOnVoltagePowerPage=By.xpath("//section[@id='landing-text']");
    public By homePotenzaTensionePath=By.xpath("//ul[@class='breadcrumbs component']");
    public By potenzaTensioneTitle=By.xpath("//h1[@class='image-hero_title text--page-heading'][text()='Vuoi modificare la potenza del tuo contatore?']");
    public By potenzaTensioneSubtitle=By.xpath("//p[@class='image-hero_detail text--detail'][text()='Tutto quello che devi sapere per richiedere una modifica della potenza e/o tensione relative alla tua fornitura']");
    public By modificapotenzatensioneTitle=By.xpath("//div[@class='heading']//h1[text()='Modifica potenza e/o tensione']");
    public String domande []={"Cosa si intende per potenza e tensione?","Quando è necessario modificare la potenza?","Quali sono i tempi per l'esecuzione della modifica di potenza o tensione?","Cosa si intende per sopralluogo tecnico?","Quali sono i costi dell'operazione?","Come posso pagare il preventivo?"};
    public String risposte[]={"La potenza è la quantità di energia elettrica massima erogata dal contatore e viene misurata in kW. La tensione è la differenza di potenziale elettrico che c’è tra due conduttori elettrici (ad esempio due cavi elettrici in rame). Può essere monofase (220V) oppure trifase (380V).","L'utilizzo di numerosi elettrodomestici può comportare il superamento della potenza disponibile sul tuo contatore e, nel caso di forniture con limitazione della potenza, alla conseguente interruzione nell'erogazione di energia elettrica. Quando un evento del genere capita frequentemente, ti consigliamo di considerare una richiesta di aumento di potenza della tua fornitura. Viceversa, nel caso in cui ritieni che la potenza attuale del contatore sia sovradimensionata rispetto alle tue esigenze, puoi considerare una richiesta di riduzione di potenza. Tieni presente che, per richieste di potenza fino a 15 kW avrai a disposizione una potenza massima disponibile pari al valore di potenza richiesto aumentato del 10% (ad esempio, un contatore con potenza contrattuale richiesta di 3 kW garantisce una potenza disponibile pari a 3,3 kW).","La tempistica varia a seconda se sia necessario o meno effettuare un sopralluogo da parte di un tecnico del tuo distributore, il quale ti comunicherà i tempi di esecuzione del lavoro. Se non è necessario il sopralluogo l'operazione viene effettuata mediamente tra 2 e 5 giorni lavorativi.","Se la modifica di potenza e/o tensione non è eseguibile direttamente dal tuo distributore di energia elettrica in telegestione, è necessario il sopralluogo di un tecnico del distributore per determinare l'entità dell'intervento da effettuare. In questi casi sarà lo stesso distributore a contattare il cliente per accordarsi sulla data di esecuzione del sopralluogo.","L’operazione di modifica della potenza e/o tensione comporta un costo, che ti verrà comunicato nel preventivo. Tale costo dipende della variazioni di potenza e/o tensione richieste, e si compone di una quota relativa alle attività sulla rete di distribuzione e di una quota dipendente dalla condizioni del tuo contratto di fornitura con Enel Energia. Nel preventivo che ti invieremo troverai, inoltre, una nostra stima degli impatti in bolletta della modifica richiesta: infatti, i servizi di rete (trasporto, distribuzione dell'energia e gestione del contatore), gli oneri generali di sistema e l’ammontare delle accise applicate in bolletta variano in base alla potenza del tuo contatore. I costi indicati nel preventivo sono da considerarsi al netto dell'IVA.","Se l'importo del preventivo è inferiore a 1000 euro potrai scegliere se addebitare il costo in bolletta oppure pagare online con carta di credito o, in alternativa, tramite bollettino. Se l'importo è superiore a 1000 euro non sarà possibile richiedere l'addebito in bolletta, ma è necessario effettuare il pagamento online con carta di credito o tramite bollettino. In tutti i casi, se hai la domiciliazione attiva, il costo verrà automaticamente addebitato in bolletta e non dovrai fare nulla. Il sistema ti guiderà nella scelta della modalità di pagamento che preferisci tra quelle disponibili."};
    public String linkCAMBIO_POTENZA="it/supporto/faq/cambio-potenza-contatore";
    public String linkREGISTRATI="it/registrazione";
    public ChangePowerVoltageSectionComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
    public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
    
    public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
    
    public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
    
    public void checkHeaderCambioPotenzaContatorePage(By checkObject, By path,By title, By subtitle) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String pathDescription=driver.findElement(path).getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	
				
		if (!pathDescription.contains("HOME / SUPPORTO / POTENZA E TENSIONE"))
			throw new Exception("sulla pagina web POTENZA E TENSIONE l'oggetto path con xpath " + path + " non esiste.");
		if (!util.exists(title, 15))
			throw new Exception("sulla pagina web POTENZA E TENSIONE l'oggetto titolo con xpath " + title + " non esiste.");
		if (!util.exists(subtitle, 15))
			throw new Exception("sulla pagina web POTENZA E TENSIONE l'oggetto sottotitolo con xpath " + subtitle + " non esiste.");	
    }
	
	public void checkTextCambioPotenzaContatorePage(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		util.objectManager(checkObject, util.scrollToVisibility, false);
	    String description=driver.findElement(checkObject).getText();
	    description= description.replaceAll("(\r\n|\n)", " ");	
	    
	    if (!description.contains("Modifica potenza e tensione Se la potenza disponibile del tuo contatore risulta insufficiente alle tue esigenze di consumo, puoi prendere in considerazione di effettuare una richiesta di aumento di potenza. Viceversa, qualora ritieni che la potenza disponibile del tuo contatore sia sovradimensionata rispetto alle tue esigenze, potrai richiedere una diminuzione della potenza. La modifica della potenza (ed, eventualmente, della tensione) del tuo contatore comporta variazioni alle condizioni economiche e, qualora necessario, anche variazioni alle condizioni tecniche della fornitura (ad esempio: una variazione della tensione, un cambio del contatore etc.). Effettua in maniera semplice e veloce l’operazione dalla tua area riservata. Otterrai subito un preventivo di spesa che potrai accettare o annullare comodamente. Questa funzionalità è disponibile solamente per forniture ad uso abitativo."))
			throw new Exception("sulla pagina web '/it/supporto/faq/cambio-potenza-contatore' non e' presente il testo 'Modifica potenza e tensione Se la potenza disponibile del tuo contatore risulta insufficiente alle tue esigenze di consumo, puoi prendere in considerazione di effettuare una richiesta di aumento di potenza. Viceversa, qualora ritieni che la potenza disponibile del tuo contatore sia sovradimensionata rispetto alle tue esigenze, potrai richiedere una diminuzione della potenza. La modifica della potenza (ed, eventualmente, della tensione) del tuo contatore comporta variazioni alle condizioni economiche e, qualora necessario, anche variazioni alle condizioni tecniche della fornitura (ad esempio: una variazione della tensione, un cambio del contatore etc.). Effettua in maniera semplice e veloce l’operazione dalla tua area riservata. Otterrai subito un preventivo di spesa che potrai accettare o annullare comodamente. Questa funzionalità è disponibile solamente per forniture ad uso abitativo.'");
	}
	
	public void checkHeaderRegistratiPage(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	    String description=driver.findElement(checkObject).getText();
	    description= description.replaceAll("(\r\n|\n)", " ");	
	  
	    if (!description.contains("SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE Registrati nell'area riservata Privato Impresa CONTINUA"))
			throw new Exception("sulla pagina web '/it/registrazione' non sono presenti le label con testo 'SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE - Registrati nell'area riservata - Privato - Impresa - CONTINUA'");	
	}
	
	 public void notclickableVerify (By checkObject, String url) throws Exception {
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			driver.findElement(checkObject).click();
			checkUrl(url);
			
	 }
	 
	
	 public void checkfrequentlyquestionssection(By checkObject) throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		 String description=driver.findElement(checkObject).getText();
		 description= description.replaceAll("(\r\n|\n)", " ");
				 
		 if (!description.contentEquals("Domande Frequenti Cosa si intende per potenza e tensione?  Quando è necessario modificare la potenza? Quali sono i tempi per l'esecuzione della modifica di potenza o tensione?   Cosa si intende per sopralluogo tecnico?  Quali sono i costi dell'operazione?  Come posso pagare il preventivo?"))
			 throw new Exception("sulla pagina web '/it/supporto/faq/cambio-potenza-contatore' non e' presente la sezione 'Domande frequenti' con domande: Cosa si intende per potenza e tensione?  Quando è necessario modificare la potenza? Quali sono i tempi per l'esecuzione della modifica di potenza o tensione?   Cosa si intende per sopralluogo tecnico?  Quali sono i costi dell'operazione?  Come posso pagare il preventivo?");
		 
	 }
	 
	 public void checkPlusImage(By checkObject,String linkColla) throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
//		 String plus=linkColla+"etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/plus.png";
		 String plus="plus.png";
		 List<WebElement> questionsList = driver.findElements(checkObject);
	
			for (int i = 1; i <= questionsList.size(); i++) {
				if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-accordion]["+i+"]"),plus)!=true)
					throw new Exception("sulla pagina web '/it/supporto/faq/cambio-potenza-contatore' le domande frequenti non sono affincate dall'immagine +");
				 }
		    }
			
	 public void checkQuestionsDetails(By checkObject,String [] questions, String [] answer ) throws Exception {
		
		 List<WebElement> questionsList = util.waitAndGetElements(checkObject);
		 if (questionsList.size()==questions.length) {
			for (int i = 0; i < questions.length; i++) { 
				WebElement d= util.waitAndGetElement(By.xpath("//section//section["+(i+1)+"]//button[@aria-expanded='false']"));
				String description=d.getText();
				//System.out.println(description); 
				 driver.findElement(By.xpath("//section//section["+(i+1)+"]//button[@aria-expanded='false']")).click();
				 TimeUnit.SECONDS.sleep(1);
				 WebElement el= util.waitAndGetElement(By.xpath("//section//section["+(i+1)+"]//button[@aria-expanded='true']"));
				 WebElement el2=util.waitAndGetElement(By.xpath("//section//section["+(i+1)+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']"));
			     String text=el2.getText();
			     text= text.replaceAll("(\r\n|\n)", " ");
			   // //System.out.println(text);
			     TimeUnit.SECONDS.sleep(1);
					     if(!description.contentEquals(questions[i].trim()))
						       	throw new Exception("sulla pagina web '/it/supporto/faq/cambio-potenza-contatore' nella sezione 'Domande Frequenti' e' cambiato la descrizione di una delle domande: il testo della domanda visualizzato e' "+description+"; quello atteso e' "+questions[i]);  
					     if(!text.contentEquals(answer[i].trim()))
						       	throw new Exception("sulla pagina web '/it/supporto/faq/cambio-potenza-contatore' nella sezione 'Domande Frequenti' e' cambiato il testo di risposta alla domanda "+description+": il testo e' "+text+"; quello atteso e' "+answer[i]);  
							  		    	 
			     el.click();
			     TimeUnit.SECONDS.sleep(1);
			     d= util.waitAndGetElement(By.xpath("//section//section["+(i+1)+"]//button[@aria-expanded='false']"));
			
				}
		 }
		 else throw new Exception("sulla pagina web '/it/supporto/faq/cambio-potenza-contatore' nella sezione 'Domande Frequenti' il numero atteso delle domande e' cambiato ");
	 }
	 
	 public void enterLoginParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	 
	 public void checkTextOnVoltagePowerPage(By title,By checkObject) throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		 String description=driver.findElement(checkObject).getText();
		 description= description.replaceAll("(\r\n|\n)", " ");
		 if (!util.exists(title, 15))
				throw new Exception("l'oggetto web titolo con xpath " + title + " non esiste.");	
		 if (!description.contains("Se la potenza disponibile del tuo contatore risulta insufficiente alle tue esigenze di consumo, puoi prendere in considerazione di effettuare una richiesta di aumento di potenza. Viceversa, qualora ritieni che la potenza disponibile del tuo contatore sia sovradimensionata rispetto alle tue esigenze, potrai richiedere una riduzione della potenza. La modifica della potenza (ed, eventualmente, della tensione) del tuo contatore comporta variazioni alle condizioni economiche e, qualora necessario, anche variazioni alle condizioni tecniche della fornitura (ad esempio: una variazione della tensione, un cambio del contatore etc.). Effettua in maniera semplice e veloce l’operazione dalla tua area riservata. Otterrai subito un preventivo di spesa che potrai accettare o annullare comodamente. Questa funzionalità è disponibile solamente per forniture ad uso abitativo."))
			 throw new Exception("sulla pagina web '/it/area-clienti/residenziale/tensione_potenza#!' non e' presente il testo con descrizione: Modifica potenza e/o tensione,Se la potenza disponibile del tuo contatore risulta insufficiente alle tue esigenze di consumo, puoi prendere in considerazione di effettuare una richiesta di aumento di potenza. Viceversa, qualora ritieni che la potenza disponibile del tuo contatore sia sovradimensionata rispetto alle tue esigenze, potrai richiedere una riduzione della potenza. La modifica della potenza (ed, eventualmente, della tensione) del tuo contatore comporta variazioni alle condizioni economiche e, qualora necessario, anche variazioni alle condizioni tecniche della fornitura (ad esempio: una variazione della tensione, un cambio del contatore etc.). Effettua in maniera semplice e veloce l’operazione dalla tua area riservata. Otterrai subito un preventivo di spesa che potrai accettare o annullare comodamente. Questa funzionalità è disponibile solamente per forniture ad uso abitativo."); 
	 }
    
}
