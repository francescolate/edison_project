package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class FornitureBSNComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By path = By.xpath("//section[@id='fornDetailPannel']//ol[@class='breadcrumb']");
	public By pageTitle = By.xpath("//h1[contains(text(),'Tutto quello che')]");
	public By titleSubText = By.xpath("//p[contains(text(),'In questa sezione visualizzi i dettagli della tua')]");
	public By forniture = By.xpath("//h2[text()='Forniture']");
	public By gestisciletueForniture = By.xpath("//h5[text()='Gestisci le tue forniture']");
	public By infoEnelEnergiaText = By.xpath("//table//tr[@class='INFO_ENELENERGIA']");
	public By scopriDiPiuLink = By.xpath("//table//tr[@class='INFO_ENELENERGIA']//td[@class='text-right']//a");
	public By fornituraLuceClienti = By.xpath("//div[@data-context='header-info']//div[@class='col-xs-12']");
	public By modificaNomeFurnitura = By.xpath("//div[@class='col-xxs-12 col-xs-6']//a[contains(text(),'Modifica')]");
	public By tipo = By.xpath("//tr//th[1]");
	public By stato = By.xpath("//tr//th[2]");
	public By numerocliente = By.xpath("//tr//th[3]");
	public By indirizzoFornitura = By.xpath("//tr//th[4]");
	public By pageTitle_260 = By.xpath("//h1[contains(text(),'Tutto quello che ')]");
	public By titleSubText_260 = By.xpath("//p[@id='subtitleFornitura']");
	public By pagePath_260 = By.xpath("//section[@id='fornDetailPannel']//ol[@class='breadcrumb']");
	public By VAIALDETTAGLIO = By.xpath("//tr[1]//td[5]//a[1][text()='VAI AL DETTAGLIO']");
	
	public FornitureBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);			
	}
	 
	public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	 
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public static final String PageTitle = "Tutto quello che c'è da sapere sulla tua fornitura";
	public static final String TitleSubText = "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.";
	public static final String Path = "Area riservata fornitura";
	public static final String Path1 = "Area riservata Forniture";
	public static final String InfoEnelEnergiaRowText = "Info Enel Energia Veloce, comodo e gratuito, per comunicare con te via sms o mail. Scopri di più";
	public static final String PagePath = "Area riservata Forniture";
	public static final String ModificaNomeFurnitura = "Modifica nome fornitura";
	public static final String Tipo = "Tipo";
	public static final String Stato = "Stato";
	public static final String Numero_cliente = "Numero cliente / Alias";
	public static final String Indirizzo_fornitura = "Indirizzo fornitura";
	public static final String PageTitle_260 = "Tutto quello che c'è da sapere sulla tua fornitura";
	public static final String TitleSubText_260 = "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.";
	

}
