package com.nttdata.qa.enel.components.colla;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GetAccountDetailsComponent {

	WebDriver driver;
	SeleniumUtilities util;
	Properties prop = null;
	
	public By queryResultFields = By.xpath("//*[@id='query_results']/tbody/tr/th");
	public By queryInputField = By.xpath("//textarea[@id='soql_query_textarea']");
	
    public GetAccountDetailsComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
    
    	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
		
	public  void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
	}
	
	public void clearText(By by) throws Exception{
		driver.findElement(by).clear();
	}
	
	public void verifyAccountDetails(By checkObject, String[] Feild, String[] Value) throws Exception{
					
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmActual = new HashMap<String, String>();
		String key, val ;
		List <WebElement> list  = driver.findElements(queryResultFields);
		for (int i=4; i<=list.size();i++)
		{
			key = driver.findElement(By.xpath("//*[@id='query_results']/tbody/tr/th["+ i +"]")).getText();
			val = driver.findElement(By.xpath("//*[@id='query_results']/tbody/tr/td["+ i +"]")).getText();
			hm.put(key, val);
			}
		for (int i=0; i<Feild.length;i++){
		hmActual.put(Feild[i], Value[i]);}
		if(!hm.equals(hmActual))
    		throw new Exception ("The account details are not correctly displayed");
		}
	
	public void verifyAccountDetailsQueryb(By checkObject, String[] Feild, String[] Value) throws Exception{
		
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmActual = new HashMap<String, String>();
		String key, val ;
		List <WebElement> list  = driver.findElements(queryResultFields);
		for (int i=4; i<=list.size();i++)
		{
			key = driver.findElement(By.xpath("//*[@id='query_results']/tbody/tr/th["+ i +"]")).getText();
			val = driver.findElement(By.xpath("//*[@id='query_results']/tbody/tr/td["+ i +"]")).getText();
			hm.put(key, val);
			}
		for (int i=0; i<Feild.length;i++){
		hmActual.put(Feild[i], Value[i]);}
		if(!hm.equals(hmActual))
    		throw new Exception ("The account details are not correctly displayed");
		}

	DateTimeFormatter dtf_test = DateTimeFormatter.ofPattern("EEEE dd MMMM", Locale.ITALY);  
	LocalDateTime now_test = LocalDateTime.now();  
	String dayAndMonth_test = dtf_test.format(now_test);
		
	public static final String[] QUERY_FIELDS_165a = {"Priority","Status","Origin","Subject","ITA_IFM_SubStatus__c"};
	public  final String[] QUERY_VALUES_165a = {"Medium","Chiuso","Web","GESTIONE PRIVACY","INVIATO"};

	public static final String[] QUERY_FIELDS_165b = {"ITA_IFM_Phone__c","ITA_IFM_MarketingConsentPhone__c","ITA_IFM_ThirdPartyConsentPhone__c","ITA_IFM_Email__c","ITA_IFM_MarketingConsentEmail__c","ITA_IFM_ThirdPartyConsentEmail__c"};
	public  final String[] QUERY_VALUES_165b = {"0245708411",ModConsensiACRComponent.list.get(0),ModConsensiACRComponent.list.get(1),"r.centrullo@gmail.com",ModConsensiACRComponent.list.get(2),ModConsensiACRComponent.list.get(3)};
	
	public static final String[] QUERY_FIELDS_164a = {"Priority","Status","Origin","Subject","ITA_IFM_SubStatus__c"};
	public  final String[] QUERY_VALUES_164a = {"Medium","Chiuso","Web","GESTIONE PRIVACY","INVIATO"};
	
	public static final String[] QUERY_FIELDS_164b = {"ITA_IFM_Phone__c","ITA_IFM_MarketingConsentPhone__c","ITA_IFM_ThirdPartyConsentPhone__c","ITA_IFM_Email__c","ITA_IFM_MarketingConsentEmail__c","ITA_IFM_ThirdPartyConsentEmail__c"};
	public  final String[] QUERY_VALUES_164b = {"0818142364",ModConsensiACRComponent.list.get(2),ModConsensiACRComponent.list.get(3),"fabiana.manzo1@yopmail.com","NON ESPRESSO","NON ESPRESSO"};
	
	public static final String[] QUERY_FIELDS_163a = {"Priority","Status","Origin","Subject","ITA_IFM_SubStatus__c"};
	public  final String[] QUERY_VALUES_163a = {"Medium","Chiuso","Web","GESTIONE PRIVACY","INVIATO"};
	
	public static final String[] QUERY_FIELDS_163b = {"ITA_IFM_Phone__c","ITA_IFM_MarketingConsentPhone__c","ITA_IFM_ThirdPartyConsentPhone__c","ITA_IFM_Email__c","ITA_IFM_MarketingConsentEmail__c","ITA_IFM_ThirdPartyConsentEmail__c"};
	public  final String[] QUERY_VALUES_163b = {"0818142364",ModConsensiACRComponent.list.get(0),ModConsensiACRComponent.list.get(1),"fabiana.manzo@nttdata.com",ModConsensiACRComponent.list.get(2),ModConsensiACRComponent.list.get(3)};
}

