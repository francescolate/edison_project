package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSN_InfoEnelEnergiaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//section[@id='nonInscritto']//h1[text()='Info Enel Energia']");
	public By pageSubText = By.xpath("//section[@id='nonInscritto']//p[contains(text(),'Per')]");
	public By proseguiButton = By.xpath("//section[@id='nonInscritto']//descendant::a[.='Prosegui']");
	public By indirzzoText = By.xpath("//h5[contains(text(),'Indirizzo mail e cellulare sui cui ricevi le notifiche')]");
	public By email = By.xpath("//tr//th[1]");
	public By cellulare = By.xpath("//tr//th[2]");
	public By modifica = By.xpath("//tr/td/b[text()='Modifica']");
	public By modificaText = By.xpath("//tr[1]/td[@class='white-space-normal']");
	public By modificaScopriDiPiu = By.xpath("//tr[1]/td[@class='white-space-normal']//following::td[1]");
	public By revoca = By.xpath("//tr/td/b[text()='Revoca']");
	public By revocaText = By.xpath("//tr[2]/td[@class='white-space-normal']");
	public By revocaScopriDiPiu = By.xpath("//tr[2]/td[@class='white-space-normal']//following::td[1]");
	public By infoTitle = By.xpath("//h1[text()='Info Enel Energia']");
	public By pagePath = By.xpath("//ol[@class='breadcrumb']");
	public By titleSubText = By.xpath("//p[contains(text(),'Operazione')]");
	public By estito = By.xpath("//span[text()='Esito']");
	public By messageOK = By.xpath("//p[@id='messageOK']");
	public By sectionTitle = By.xpath("//p[contains(text(),'Da oggi puoi')]");
	public By sectionText = By.xpath("//p[contains(text(),'Il servizio Bolletta')]");
	public By attivaBollettaWeb = By.xpath("//a[text()='Attiva Bolletta Web']");
	public By fineButton = By.xpath("//a[text()='Fine']");
	public By supply = By.xpath("//section[@class='content']//h4[@data-context='header-info-nonActive']//h4");
	public By text = By.xpath("//section[@class='content']//p[contains(text(),'Info Enel Energia ')]");
	public By cheCose = By.xpath("//section[@id='nonInscritto']//h2[contains(text(),'Che')]");
	
	public By page_title261 = By.xpath("(//h1[text()='Info Enel Energia'])[1]");
//	public By page_subtitle261 = By.xpath("(//p[contains(text(),'Per')])[1]");
	public By page_subtitle261 = By.xpath("(//h1[text()='Info Enel Energia'])[1]/following-sibling::p");
	public By section_title276 = By.xpath("//p[contains(text(),'Risparmia tempo')]");
	public By sectionText276 = By.xpath("//p[contains(text(),'Addebito Diretto ')]");
	public By attivaButton276 = By.xpath("//a[text()='Attiva addebito diretto']");
	public By pageTitle276 = By.xpath("//section[@id='active']//h1[text()='Info Enel Energia']");
	public By pageSubtitle276 = By.xpath("//section[@id='active']//h1[text()='Info Enel Energia']//following-sibling::p");
	
	public BSN_InfoEnelEnergiaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponent(By clickObject) throws Exception {
			util.objectManager(clickObject, util.scrollToVisibility, false);
			util.objectManager(clickObject, util.scrollAndClick);
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public static final String PageTitle = "Info Enel Energia";
		public static final String PageSubText = "Per essere sempre informato sulla tua fornitura.Su questa fornitura, Info Enel Energia non è ancora attivo.";
		public static final String ModificaText = "Se cambi email o numero di cellulare, aggiorna i tuoi dati.";
		public static final String RevocaText = "Sei sicuro di voler rinunciare alla comodità delle notifiche di Info Enel Energia?";
		public static final String TitleSubText = "Operazione eseguita correttamente";
		public static final String MessageOK = "La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve. Segui l'avanzamento della tua richiesta nella sezione 'Stato richieste/Servizi'";
		public static final String SectionTitle = "Da oggi puoi risparmiare tempo attivando Bolletta Web.";
		public static final String SectionText = "Il servizio Bolletta Web ti permettere di ricevere le bollette direttamente sulla tua mail. In questo modo risparmierai già dalla prossima bolletta i costi di postalizzazione.";
		public static final String PagePath = "Area riservataFornitureInfo Enel Energia";
		public static final String Text = "Info Enel Energia è un servizio di notifica che ti segue ovunque tu sia, tenendoti sempre informato sulla tua fornitura tramite sms o mail. Un altro modo per seguire le tue esigenze e facilitare la nostra comunicazione in modo semplice, veloce e a impatto zero sull'ambiente.";
		public static final String PageSubText261 = "Per essere sempre informato sulla tua fornitura.Su questa fornitura, Info Enel Energia non è ancora attivo.";
		public static final String SectionTitle276 = "Risparmia tempo attivando l'Addebito Diretto.";
		public static final String SectionText276 = "L'Addebito Diretto è il metodo più facile e veloce per pagare le tue bollette. Attivando il servizio, l'importo della bolletta verrà addebitato direttamente sul tuo Conto Corrente il giorno stesso della scadenza, così non dovrai più preoccuparti di ricordartelo.";
		public static final String PageSubText276 = "Per essere sempre informato sulla tua fornitura.";

}
