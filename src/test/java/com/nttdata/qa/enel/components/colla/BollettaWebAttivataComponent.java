package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BollettaWebAttivataComponent extends BaseComponent {
	

	//WebDriver driver;
	//SeleniumUtilities util;
	
	
	public static final String PageTextConfirmBW = "La tua richiesta è stata presa in carico. Clicca sul link presente nella email e nell'SMS che ti abbiamo inviato per completare la richiesta, qualora i contatti inseriti non risultino già confermati precedentemente. La mancata conferma del link comporta l'annullamento della richiesta. Segui l'avanzamento della tua richiesta nella sezione STATO RICHIESTE..";
	public static final String pathText = "Area riservata Bolletta Web";
	public static final String titleText = "Bolletta Web";
	public static final String statusText = "Operazione eseguita correttamente.";
	public static final String addebitoDirettoText = "Addebito Diretto Risparmia tempo con l'addebito diretto, il servizio che paga le tue bollette alla scadenza.";

	
	public By path = By.xpath("//ol[@class='breadcrumb']");	
	public By title = By.xpath("//ol[@class='breadcrumb']//following-sibling::h1");
	public By status = By.xpath("//ol[@class='breadcrumb']//following-sibling::h1//following-sibling::p");
	public By addebitoDiretto = By.xpath("//div[@class='ty-bollettaWeb parbase']//div[3]");
	
	
	public BollettaWebAttivataComponent(WebDriver driver) {
		//this.driver = driver;
		//util = new SeleniumUtilities(this.driver);
		super(driver);
	}
	
	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
        if (!util.verifyExistence(oggettoEsistente, extTimeoutInterval)) {
            throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
        }
    }
	/*
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		System.out.println("without Normalized:\n"+weText);
		System.out.println("without Normalized we.getText():\n"+we.getText());
		if(nestedTags)
		{
			weText = normalizeInnerHTML(weText);
		System.out.println("Normalized:\n"+weText);
		}
		else
			weText = we.getText();
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
 
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	*/
	
	
	
}