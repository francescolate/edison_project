package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CanaleInvioDocumentaleComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	public By labelCanaleInvio = By.xpath("//label[contains(text(),'Canale invio')]/..//select");
	public By inputEmail = By.xpath("//input[@id='channelValue']");


	public CanaleInvioDocumentaleComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}
	
	public void selezionaCanale(By canaleInvio, String valorecanale) throws Exception{
		util.objectManager(canaleInvio, util.select,valorecanale);
	}
	public void selezionaCanale(String frame,By canaleInvio, String valorecanale) throws Exception{
		util.frameManager(frame, canaleInvio, util.select,valorecanale);
	}
	
	public void inserisciEmail(String frame,By Email,String email) throws Exception{
		util.frameManager(frame, Email,util.clear);
		util.frameManager(frame, Email,util.sendKeys,email);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//util.frameManager(frame, Email,util.sendKeys,Keys.chord(Keys.ENTER));
		
	}

	public void inserisciEmail(By Email,String email) throws Exception{
		util.objectManager(Email,util.clear);
		util.objectManager(Email,util.sendKeys,email);
		util.objectManager(By.xpath("//body"),util.scrollAndClick);
		//util.frameManager(frame, Email,util.sendKeys,Keys.chord(Keys.ENTER));
		
	}
}
