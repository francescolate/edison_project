package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FibraNoCover_ResComponent extends BaseComponent {
	public By fiberCardHeader = By.id("cardHeading1");
	public By fiberCardAddress = By.xpath("//h3[@id='cardHeading1']/../p");
	public By fiberCardStatus = By.id("fornitura");
	public By showOfferBtn = By.xpath("//a[@type='button' and @href='/it/area-clienti/residenziale/informativa-fibra']");
	public By[] infoPageStringLocators = {
			By.xpath("//div[contains(@class,'landing-fibra parbase')]/div[1]"),
			By.xpath("//div[contains(@class,'landing-fibra parbase')]/div[2]"),
			By.xpath("//div[contains(@class,'landing-fibra parbase')]/div[3]"),
			By.xpath("//div[contains(@class,'landing-fibra parbase')]/div[4]"),
			By.xpath("//div[contains(@class,'landing-fibra parbase')]/div[5]")
	};
	public By continueBtn = By.xpath("//a[@href='/it/area-clienti/residenziale/verifica-copertura-fibra']");
	public By popup1CloseBtn = By.xpath("//span[@role='button']/div[@class='close']");
	public By[] popupLocators = {
			By.xpath("//div[@class='modal__container__title']"),
			By.xpath("//div[@class='modal__container__description']")
	};
	public By infoBtn = By.xpath("//button[@class='pos_rad_ico epoint buttonNoBorder']");
	
	public By popupHeader = By.xpath("//*[@id='dialog_desc']");
	public By popupBody = By.xpath("//*[@id='dialog_label']");
	public By popupCloseButton = By.xpath("//button[@class='close buttonClose']");
	public By popupErrorContainer = By.xpath("//div[@class='modal__container']");
	public By melitaFrame = By.xpath("//iframe[@id='myIFrame']");
	
	public FibraNoCover_ResComponent(WebDriver driver) {
		super(driver);
	}
	
	public void switchFrame(By by) throws Exception{
		util.FrameSwitcher(by);
	}
	
	public void checkFiberCardPresence() throws Exception {
		By[] locators = {fiberCardHeader, fiberCardAddress, fiberCardStatus};
		verifyElementsArrayText(locators, fiberCardStrings);
	}
	
	public void checkInfoPageStrings() throws Exception {
		verifyElementsArrayText(infoPageStringLocators, infoPageStrings);
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkPopup1Strings() throws Exception {
		Thread.sleep(2000);
		WebElement theFrame = util.frameForElement(popupLocators[1]);
		driver.switchTo().frame(theFrame);
		verifyElementsArrayText(popupLocators, popup1Strings);
		verifyVisibilityThenClick(popup1CloseBtn);
		driver.switchTo().defaultContent();
		theFrame = util.frameForElement(infoBtn);
		driver.switchTo().frame(theFrame);
	}
	
	public void checkPopup2Strings() throws Exception {
		verifyElementsArrayText(popupLocators, popup2Strings);
	}
	
	//----- Constants -----
	private String[] fiberCardStrings = {
			"Fibra",
			"VIA ALBERTO ASCARI 196 - 00142 ROMA RM",
			"In Lavorazione"
	};
	private String[] infoPageStrings = {
			"MELITA OFFRE LA FIBRA AI CLIENTI ENEL ENERGIA\n" + 
			"Richiedi l’adesione all’offerta FIBRA DI MELITA e connetti la tua casa con la fibra alla massima velocità.",
			
			"Caratteristiche dell’offerta FIBRA DI MELITA\n" + 
			"Con un canone mensile di 27,00 € (IVA inclusa) hai compreso:\n" + 
			"- Internet fino a 1 Gigabit/s in download e fino a 300 Mbit/s in upload\n" + 
			"- Attivazione gratuita\n" + 
			"- Modem Plume SuperPod di ultima generazione in comodato d’uso gratuito\n" + 
			"\n" + 
			"Il servizio sarà attivato a condizione che la tua abitazione sia raggiunta dalla connessione fibra FTTH commercializzata da Melita.",
			
			"Semplicità e comodità nel pagamento\n" + 
			"Se deciderai di aderire all’offerta FIBRA DI MELITA il canone mensile del servizio fibra sarà addebitato direttamente nella tua bolletta di Enel Energia relativa alla fornitura di energia elettrica e/o gas naturale. La periodicità di emissione delle bollette e le modalità di pagamento non cambieranno, con il vantaggio di pagare i due servizi nell’unica bolletta Enel Energia.",
			
			"Durata del contratto FIBRA DI MELITA\n" + 
			"Sottoscriverai direttamente con Melita il contratto fibra con durata di 24 mesi. Potrai recedere dal contratto in ogni momento con un preavviso di almeno 30 giorni. In caso di recesso esercitato prima che sia decorso il termine dei 24 mesi, sarai tenuto a corrispondere a Melita il costo di disattivazione del servizio pari a un canone mensile.",
			
			"Comunicazioni e servizio clienti\n" + 
			"Per ogni richiesta di assistenza chiama il numero verde gratuito 800.900.860, disponibile dalle 7.00 alle 22.00 tutti i giorni dal Lunedì alla Domenica (escluse le festività nazionali)."
	};
	private String[] popup1Strings = {
			"Verifica copertura non disponibile",
			"Non è possibile verificare la copertura su questa fornitura."
	};
	private String[] popup2Strings = {
			"Ci dispiace",
			
			"Attualmente non è possibile procedere con la richiesta di attivazione.\n" + 
			"I motivi potrebbero essere i seguenti:\n" + 
			"\n" + 
			"Il servizio fibra di Melita non è al momento disponibile nella tua zona.\n" + 
			"Non ti preoccupare: non ci vorrà ancora molto.\n" + 
			"\n" + 
			"Il tuo indirizzo di fornitura potrebbe non essere registrato correttamente.\n" + 
			"Nessun problema: chiama il numero verde 800.900.860.\n" + 
			"Un operatore verificherà con te i tuoi dati."
	};
	
	
	public static final String popupHeaderText = "Verifica copertura non disponibile";
	public static final String popupBodyText = "Non è possibile verificare la copertura su questa fornitura.";
	public static final String popupErrorText = "è stata aperta la modale informativa. Chiudi la modaleCi dispiaceAttualmente non è possibile procedere con la richiesta di attivazione. I motivi potrebbero essere i seguenti: Il servizio fibra di Melita non è al momento disponibile nella tua zona. Non ti preoccupare: non ci vorrà ancora molto. Il tuo indirizzo di fornitura potrebbe non essere registrato correttamente. Nessun problema: chiama il numero verde 800.900.860. Un operatore verificherà con te i tuoi dati.Chiudi la modale";
}
