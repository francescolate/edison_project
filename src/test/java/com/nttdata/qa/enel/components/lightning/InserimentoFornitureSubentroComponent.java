package com.nttdata.qa.enel.components.lightning;

//import java.util.Properties;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;


public class InserimentoFornitureSubentroComponent extends BaseComponent implements SFDCBox {


    WebDriver driver;
    SeleniumUtilities util;
    By elemTesnsione;
    By elem_Tipo;

    public By sezioneInserimentoDati = By.xpath("//h2/span[text()='Inserimento forniture']");
    public By tabIndirizzo = By.xpath("//a[text()='Indirizzo' and @role='tab']");
    public By buttonVerifica = By.xpath("//button[text()='Verifica']");
    public By buttonForzaIndirizzo = By.xpath("//button[text()='Forza Indirizzo']");
    public By labelIndirizzzoVerificato = By.xpath(sdfc_box_by_title_text.replaceFirst("##", "Inserimento forniture") + "//*[normalize-space(text())='Indirizzo verificato']");
    public By labelIndirizzzoNonVerificatoGeneric = By.xpath("//section[contains(@class , 'tabContent active ')]//div[contains(@title,'Indirizzo non verificato:')]/span[contains(text(),'Indirizzo non verificato:')]");
    public By labelIndirizzoNonVerificatoChiaviFonetiche = By.xpath("//span[@id='spanFocusX' and text()=\"CANDIDATO ESTRATTO CON LE CHIAVI FONETICHE\" and text()='Indirizzo non verificato.']");
    //public By campoTipoMisuratore = By.xpath("//label[contains(text(),'Tipo misuratore')]/..//input" + not_disabled_value);
    public By campoTipoMisuratore = By.xpath("//label[contains(text(),'Tipo misuratore')]/..//input");
    public By campoTensioneConsegna = By.xpath("//label[contains(text(),'Tensione di consegna (V)')]/..//input");
    public By campoPotenzaContattuale = By.xpath("//label[contains(text(),'Potenza contrattuale')]/..//input");
    public By campoPotenzaFranchigia = By.xpath("//label[contains(text(),'Potenza in franchigia')]/..//input");
    public By inputPotenzaFranchigia = By.xpath("//input[contains(@id,'Potenza in franchigia')]");
    public By inputProvincia = By.xpath("//label[text()='Provincia']/following-sibling::input");
    public By inputComune = By.xpath("//label[text()='Comune']/../input");
    public By inputIndirizzo = By.xpath("//label[text()='Indirizzo']/following-sibling::input");
    public By inputCivico = By.xpath("//label[text()='Numero Civico']/..//input");
    public By buttonConfermaFooter = By.xpath("//footer//button[text()='Conferma' and @name='Conferma']");
    public By aggiungiFornitura = By.xpath("//button[text()='Aggiungi fornitura']");
    public By rimuoviFornitura = By.xpath("//button[text()='Rimuovi fornitura']");
    public By buttonCreaOfferta = By.xpath("//div[@class='slds-p-top_small']//button[text()='Crea offerta' and @name='CreateQuote']");
    public By buttonForzaIndirizzoIndFatturazione = By.xpath("//h2//*[text()='Indirizzo di Fatturazione']/ancestor::div[@class='slds-card ']//button[text()='Forza Indirizzo']");
    public By insf_tab_indirizzo = By.xpath("//h2//*[text()='Inserimento forniture']/ancestor::div[@role='region']//a[text()='Indirizzo' and @role='tab']");
    public By radioButton = By.xpath("//span[@class='slds-checkbox_faux_container']");
    /* ---- */
    public By erroreConferma = By.id("modalTitle-1734");
    public By campoMatricolaContatore = By.xpath("//label[contains(text(), 'Matricola contatore distributore')]/..//input");
    public By alertValidazione = By.xpath("//p[contains(text(), 'Errore! I seguenti pod/pdr non hanno superato il check offertabilità:')]/..");

    /**
     * @param driver
     */
    public InserimentoFornitureSubentroComponent(WebDriver driver /*, Properties prop*/) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
    }

    /**
     * @param existingObject
     * @throws Exception
     */
    public void verifyComponentExistence(By existingObject) throws Exception {
        if (!util.exists(existingObject, 180))
            throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
    }

    /**
     * @param existingObject
     * @return
     * @throws Exception
     */
    public boolean Existence(By existingObject) throws Exception {
        if (!util.exists(existingObject, 180))
            return false;
        else
            return true;
    }


    /**
     * @param clickableObject
     * @throws Exception
     */
    public void clickComponent(By clickableObject) throws Exception {
        util.objectManager(clickableObject, util.scrollToVisibility, false);
        Thread.sleep(5000);
        util.objectManager(clickableObject, util.scrollAndClick);
    }

    /**
     * @param tipo
     * @throws Exception
     */
    public void selezionaTipoMisuratore(String tipo) throws Exception {
        clickComponentWithJse(campoTipoMisuratore);
        elem_Tipo = By.xpath("//label[contains(text(),'Tipo misuratore')]/..//input/ancestor::*[@id]//*[text()='" + tipo + "']");
    
        clickComponentIfExist(elem_Tipo);
        TimeUnit.SECONDS.sleep(3);
    }

    /**
     * @param tensione
     * @throws Exception
     */
    public void selezionaTensione(String tensione) throws Exception {
        clickComponentWithJse(campoTensioneConsegna);
        elemTesnsione = By.xpath("//label[contains(text(),'Tensione di consegna (V)')]/..//input/ancestor::*[@id]//*[text()='" + tensione + "']");
        clickComponentWithJse(elemTesnsione);
    }

    /**
     * @param potenza
     * @throws Exception
     */
    public void selezionaPotenza(String potenza) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        WebElement element = util.waitAndGetElement(campoPotenzaContattuale, 60, 2);
        util.objectManager(campoPotenzaContattuale, util.sendKeys, potenza);
        TimeUnit.SECONDS.sleep(3);
        element.sendKeys(Keys.ENTER);
    }

    /**
     * @param potenza
     * @throws Exception
     */
    public void verificaValorePotenzaFranchigia(String potenza) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        String p = driver.findElement(campoPotenzaFranchigia).getAttribute("value");
        for (int i = 0; i < 8; i++) {
            TimeUnit.SECONDS.sleep(10);
            if (p.contentEquals(potenza))
                break;
        }
        if (!p.contentEquals(potenza))
            throw new Exception("il valore nel campo Potenza in Franchigia è diverso da quello atteso:il valore visualizzato e' " + p + "; quello atteso e' " + potenza);
    }
}
