package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaAccountComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;

	public String orderedList = "//*[text()='$tileHeader$']/ancestor::section//li[contains(@class,'tile') and not(@style)]//h3/ancestor::a";
	public String tile = "//li[contains(@class,'tile')]//h3[text()='$tileName$']/ancestor::a";
	public By accountMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Account']/parent::a");
	public By datiAndContattiHeader = By.xpath("//h1[text()='Dati e Contatti']");
	public By datiAndContattiHeaderText = By.xpath("//h1[text()='Dati e Contatti']/ancestor::div[@class='heading faq-appearance-group section-heading']/h1");
	public By infoButton = By.xpath("//span[@class='icon-info-circle']/..");
	public By closeInfoButton = By.xpath("//a[@data-remodal-action='close']");
	public By profileInfo = By.xpath("//div[@id='profile']");
	public By modificaContattiButton = By.xpath("//span[text()='MODIFICA CONTATTI']/..");
	public By modificaContattiIndietroButton = By.xpath("//span[text()='Indietro']/..");
	public By modificaContattiSalvaModificheButton = By.xpath("//span[text()='Salva Modifiche']/..");
	public By emailInput = By.xpath("//label[@id='cntEmailField']/..//input");
	public By datiSalvatiHeaderText = By.xpath("//h3[@id='id_title']/..");
	public By fineButton = By.xpath("//span[text()='Fine']/..");
	public By loginSuccessful= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
	public By billingAddressQuery1 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	public By billingAddressQuery2 = By.xpath("//*[@id='query_results']/tbody/tr[3]/td[4]");
	public By accountText1 =  By.xpath("//p[contains(text(),'Consulta e aggiorna i tuoi dati di contatto.')]");
	public By accountTitolare = By.xpath("//*[text()='Titolare']"); 
	public By accountCodiceFiscale = By.xpath("//*[text()='Codice Fiscale']");
	public By accountTelefonoFisso = By.xpath("//*[text()='Telefono Fisso']");
	public By accountNumerodiCellulare = By.xpath("//*[text()='Numero di Cellulare']");
	public By accountEmail = By.xpath("//*[text()='Email']");
	public By accountPEC = By.xpath("//*[text()='PEC']");
	public By accountCanale = By.xpath("//*[text()='Canale di contatto preferito']");
	
    public PrivateAreaAccountComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyMenuItemsPresenceAndOrder(String menuItemsString, String tileHeader) throws Exception {
		util.verifyVisibility(By.xpath("//*[@id='spinner'])"), 10, true);
		populateMenuItemsVector(menuItemsString);
		By orderedListXPath = By.xpath(this.orderedList.replace("$tileHeader$", tileHeader));
		List<WebElement> orderedWebElements = util.waitAndGetElements(orderedListXPath);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
		if(orderedWebElements.size()!=menuItemsList.size())
			throw new Exception("actual size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
		for(int i=0;i<orderedWebElements.size();i++){
			if(!orderedWebElements.get(i).equals(menuItemsList.get(i)))
					throw new Exception("the item you're looking for is different from the one in the list at position "+i+".");
		}
	}
	
	private void populateMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(tile.replace("$tileName$", item))));
	}
	
	public  void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
	}
	
	public static final String ACCOUNT_HEADER_TEXT = "Dati e Contatti";  
	public static final String ACCOUNT_HEADER_TEXT1 = "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito";
	public static final String AccountTitolare = "Titolare";
	public static final String AccountCodiceFiscale = "Codice Fiscale";
	public static final String AccountTelefonoFisso = "Telefono Fisso";
	public static final String AccountNumerodiCellulare = "Numero di Cellulare";
	public static final String AccountEmail = "Email";
	public static final String AccountPEC = "PEC";
	public static final String AccountCanale = "Canale di contatto preferito";
	public static final String SAVED_DATA = "Dati salvati correttamenteLa tua richiesta è stata presa in carico, la modifica sarà disponibile a breve.";
	public static final String BILLING_UPDATE_ADDRESS1 = "VIA GRAMSCI 12, 13040, ALICE CASTELLO, ALICE CASTELLO, VC";
	public static final String SAVED_ADDRESS = "Via Gramsci 29 7 13040 Palazzolo Vercellese Palazzolo Vercellese Vc";
	public static final String BILLING_UPDATE_ADDRESS2 = "VIA GRAMSCI 13040, PALAZZOLO VERCELLESE,";
}

