package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BolleteWebBSNComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By path = By.xpath("//section[@class='content']//div[@class='panel-body']//ol");
	public By pageTitle = By.xpath("//section[@class='content']//h1[text()='Bolletta Web']");
	public By titleSubtext = By.xpath("//p[contains(text(),'E la bolletta')]");
	public By pageText = By.xpath("//div[@class='panel panel-default panel-msg']");
	public By attivazioneRow = By.xpath("//tbody//tr[1]");
	public By modificaRow = By.xpath("//tbody//tr[2]");
	public By revocaRow = By.xpath("//tbody//tr[3]");
	public By attivazioneScopriDiPiu = By.xpath("//tbody//tr[1]//td[3]");
	
	public BolleteWebBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public static final String TitleSubText = "E la bolletta diventa digitale";
		public static final String PageText = "Che cos'è ?Bolletta Web è il servizio che ti permette di ricevere le tue bollette direttamente via mail: tu risparmi i costi di postalizzazione e l'ambiente ci guadagna. Durante l'attivazione ti chiederemo indirizzo mail e numero di cellulare, nel caso non fossero già presenti nei nostri sistemi.";
		public static final String AttivazioneRow = "AttivazioneVuoi attivare Bolletta Web sulle tue forniture?Scopri di più";
		public static final String ModificaRow = "ModificaVuoi modificare Bolletta Web?Scopri di più";
		public static final String RevocaRow = "RevocaVuoi disattivare Bolletta Web su alcune delle tue forniture?Scopri di più";
}
