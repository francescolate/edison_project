package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

//import com.nttdata.qa.enel.testqantt.colla.is;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ContactComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	 public By supporto = By.xpath("//li//a[text()='Supporto']");
	 public By contrattiEModulistica=By.xpath("//li//a[text()='Contratti e modulistica']");
	 public By contattaci = By.xpath("//a[text()='Contattaci']");  
	 public String contattaciUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto/faq/contattaci";
	 public By contattaciPath = By.xpath("//ul[@class='breadcrumbs component']");  
	 public By contattaciTitle = By.xpath("//h1[contains(text(),'Tanti modi per')]");  
	 public By contattaciSubtitle = By.xpath("//p[contains(text(),'Enel Energia è sempre' )]");  
	 public By perINostriClienti = By.xpath("//h3[text()='Per i nostri clienti']");  
	 public By text = By.xpath("(//span[@class='text--title-pink'])[3]");
	 public By qui = By.xpath("//a[@href='https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON']");
	 public String serviziOnlineUrl = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON";
	 public By serviziOnlineText = By.xpath("(//div[@class='text-center'])[1]");
	 public By cliccaQui = By.xpath("//a[@id='assConsumatori']");
	 
	 public By contattiTitle = By.xpath("//h3[@id='titolo_invia_messaggio_contatti_no_ass_cons']");
	 public By section1 = By.xpath("//h5[text()='INSERISCI I TUOI DATI']");
	 public By section2 = By.xpath("//h5[@id='dati_interessato']");
	 public By section3 = By.xpath("(//label[text()='Informativa Privacy*'])[2]");
	 public String contattiUrl = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP&azione=CON#";
	 public By siGiaClientiNo = By.xpath("//label[@for='inputCliente_no_Ass']");
	 public By argumentoDropdown = By.xpath("//select[@id='inputTipoAss']");
	// public By nomeAssociazione = By.xpath("//select[@id='inputAssC']");
	 public By cfAssociazione = By.xpath("//input[@id='inputCFAssCons']");
	 public By nominativoDelegato = By.xpath("//input[@id='inputNominativoDel']");
	 public By cfDelegato = By.xpath("//input[@id='inputCFDel']");
	 public By email = By.xpath("//input[@id='inputEmailAss']");
	 public By cellulare = By.xpath("//input[@id='inputCellulareAss']");
	 public By nome = By.xpath("//input[@id='inputNom']");
	 public By cognome = By.xpath("//input[@id='inputCognom']");
	 public By cf = By.xpath("//input[@id='inputCfCliente']");
	 public By messagio = By.xpath("//textArea[@id='inputNoteAss']");
	 public By si = By.xpath("//label[@id='inputAllegatiAss_siLabel']");
	 public By no = By.xpath("//label[@id='inputCliente_no_labelAss']");
	 public By acceptto = By.xpath("//label[@id='inputprivacy_si_labelAss']");
	 public By prosegui = By.xpath("//input[@id='accediuplAss']");
	 public By nomeassociazione = By.xpath("//span[@id='inputAssCSelectBoxItText']");
	 public By codici = By.xpath("//div[@id='pickListAssCons']//li[@id='id_4']");
	 
	 public By popupYes = By.xpath("//button[@id='delegaSI']");
	 public By successMsg = By.xpath("//div[@class='upload-info']//label");
	 public By indetro = By.xpath("//button[@id='accediupl_Indietro']");
	 public By prefix = By.xpath("(//span[@id='inputPrefissoSelectBoxItText'])[2]");
	 
	 public By wbStatus = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	 public By wbProcess = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	 public By wbProductName = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	 public By wbCommunicationEmail = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	 public By wbFirstName = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[9]");
	 public By wbLastName = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[10]");
	 public By wbOrigin = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[11]");
	 public By wbChannel = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[12]");
	 public By wbChannelCode = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[13]");
	 public By wbSubChannel = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[14]");
	 public By wbSubChannelCode = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[15]");
	 public By wbAccountType = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	 public By wbCommodity = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	 public By wbNewOrderItemStatus = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	 public By wbActivationReason = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	 public By itaifmemailc = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	 public By itaifmphonec = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	 public By ita_ifm_pod_c = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	 
	 public By itaifm = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	 public By itaifmagencyc = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	 public By itaifmfiscalcode = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[3]");
	 public By nepaymentc = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	 public By nebillingprofilec = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");

	 //
	 public By itarifmmobilephonec = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	 public By wbNEStatus = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[4]");
	 public By wbOffertype = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[5]");
	 public By wbName = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[6]");
	 public By wbQuoteNumber = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[7]");
	 public By wbOperationType = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[8]");
	 public By wbAccountType1 = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[9]");
	 public By wbBillingProfilePayment = By.xpath("//*[@id='query_results']/tbody/tr[2]/td[10]");
	 
	 
	 public ContactComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.click);
			
		}
		
		public void jsClickComponent(By clickableObject) throws Exception {
			util.jsClickElement(clickableObject);
			
		}
		
		public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkFieldNotEditable(By locator) throws Exception{
			if(driver.findElement(locator).isEnabled())
				throw new Exception("Given field id enable");
		}
		
		public void verifyArgumentoDropdownValues(String[] values,By locator){
			
			WebElement dropdown = driver.findElement(locator);
		    Select select = new Select(dropdown);

		    List<WebElement> options = select.getOptions();
		   // System.out.println(options+"@@@@@@@@@@@@@@@@@@@");
		    for (WebElement we : options) {
		        for (int i = 0; i < values.length; i++) {
		            if (we.getText().equals(values[i])) {
		                i++;
		                }
		        }
		    }
		}
		
		public void selectValueFromDropdown(By locator,String element){
			Select option = new Select(driver.findElement(locator));
			option.selectByVisibleText(element);
			
		}

		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void verifyEmailContent(String email,String emailContent) throws Exception{
				System.out.println(emailContent);
				if(!email.contains(emailContent)) 
					throw new Exception("Given String is not present in Email body");
			}
		
		public void verifyFieldNotNull(By locator) throws Exception{
			
			String value = driver.findElement(locator).getText();
			if(value.equals(""))
				throw new Exception("Given Field value is null");
		}
		
		public static String ContattaciPath = "Home/supporto/Come entrare in contatto con Enel Energia";
		public static String ContattaciTitle = "Tanti modi per entrare in contatto con noi";
		public static String ContattaciSubtitle = "Enel Energia è sempre in contatto con te, se sei cliente e se vuoi diventarlo.";
		public static String Text = "Vuoi scriverci via web? Invia un messaggio cliccando qui.";
		public static String ServiziOnlineText = "Sei già cliente Enel Energia? Registrati subito o accedi alla nostra Area Clienti, potrai gestire le tue forniture in autonomia. Bastano pochi click! Se non sei cliente Enel Energia, scegli l'offerta migliore per te nella sezione LUCE E GAS. Per ogni altra esigenza, puoi mandarci un messaggio. Are you already an Enel Energia customer? Register now or log in to our Customer Area, you can manage your supplies independently. Just a few clicks! If you are not an Enel Energia customer, choose the best offer for you in the LIGHT and GAS section. For any other need, you can send us a message. Puoi scriverci in diverse lingue, clicca qui per visualizzare l’elenco You can write to us in several languages, click here to view the list Invia un messaggio Se rappresenti un'Associazione dei Consumatori, clicca qui. If you represent a Consumer Association, click here.";
		public String[] dropdownValues = {"Attiva i servizi o acquista un bene Enel Energia","Attiva o revoca InfoEnelEnergia","Attiva un nuovo contratto","Attiva Una Nuova Fornitura Di Energia Elettrica","Attiva, modifica o revoca Bolletta Web","Comunica la lettura","Effettua un pagamento o richiedi un rimborso","Modifica i dati di contratto o i consensi","Modifica il tuo attuale metodo di pagamento","Modifica la potenza o la tensione della fornitura","Modifica L'intestazione Di Una Fornitura Domestica","Passa Ad Enel Energia","Rateizza la tua bolletta","Riattiva Una Fornitura Cessata","RIchiedi una rettifica della fattura","Visualizza la bolletta","Altro"};
		public static String SuccessMsg = "Grazie per averci contattato.Ti risponderemo nel più breve tempo possibile.Ti abbiamo inviato una mail con il riepilogo di quanto hai richiesto all'indirizzo di posta elettronica che hai indicato: se non la ricevi verifica nella cartella SPAM della tua casella di posta elettronica.";
		public static String EmailContentLine1 = "Gentile Cliente,";
		public static String EmailContentLine2 = "grazie per averci contattato.";
		public static String EmailContentLine3 = "Ti risponderemo nel più breve tempo possibile tramite email.";
		public static String EmailContentLine4 = "Cordiali saluti.";
		public static String EmailContentLine5 = "Servizio Clienti - Enel Energia";
		public static String EmailContentLine6 = "Questo messaggio è stato generato automaticamente, ti preghiamo di non rispondere";
}
