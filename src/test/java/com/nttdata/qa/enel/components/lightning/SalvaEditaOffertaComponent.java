package com.nttdata.qa.enel.components.lightning;



import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Step;



public class SalvaEditaOffertaComponent extends BaseComponent{

	private WebDriver driver;
	private SeleniumUtilities util;
	public SalvaEditaOffertaComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		this.util = new SeleniumUtilities(this.driver);
	}

	
	public final By salvaInbozza = By.xpath("//button[text()='Salva in Bozza']");
	public final By modificaQuote = By.xpath("//button[contains(text(),'Modifica') and contains(@onclick,'editNEQuote')]");
	
	
}
