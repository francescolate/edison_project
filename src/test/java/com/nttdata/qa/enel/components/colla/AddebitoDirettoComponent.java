package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class AddebitoDirettoComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h2[text()='Addebito Diretto']");
	public By titleSubText = By.xpath("//p[contains(text(),'Il servizio Addebito Diretto ti')]");
	public By subText = By.xpath("//p[contains(text(),'Il servizio Addebito Diretto su')]");
	public By iIcon = By.xpath("//span[@class='icon-info-circle']");
	public By alertTitle = By.xpath("//h3[@id='titleModal']");
	public By alertInnerText = By.xpath("//p[@class='inner-text']");
	public By alertClose = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//span[@class='dsc-icon-close-rounded']");
	public By addressLable = By.xpath("//span[text()='Indirizzo della fornitura']");
	public By IBANLable = By.xpath("//span[text()='IBAN']");
	public By address = By.xpath("//span[@class='indirizzo']//span[2]");
	public By IBANValue = By.xpath("//span[text()='IBAN']/following-sibling::span");
	public By text = By.xpath("//p[contains(text(),'Se vuoi disattivare')]");
	public By modificaContoCorrente = By.xpath("//span[text()='MODIFICA SU CONTO CORRENTE']");
	public By attivaSuCartaDiCredito = By.xpath("//span[text()='ATTIVA SU CARTA DI PAGAMENTO']");
	public By ESCI = By.xpath("//button[text()='ESCI']");
	public By visualizzaTutte = By.xpath("//button[contains(text(),'Visualizza tutte le modalita')]");
	
	public AddebitoDirettoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
	}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	 }
	 
	 public void verifyAddress(By locator,String input) throws Exception{
		 /*String value = driver.findElement(locator).getText();
		 value = value.replace("-", "");
		 value = value.replaceAll("\\s", "");
		// value = value.replace("-", "");
		 input = input.replace(",", "");
		 input = input.replaceAll("\\s", "");
		 System.out.println(value);
		 System.out.println(input);
		 if(!input.contains(value)) throw new Exception(value +" Text is not matching with the expected value "+input);*/
		 String webString = driver.findElement(locator).getText();
		 String web = webString.toUpperCase();
		 System.out.println(webString);
		 System.out.println(input);
		 String value = input.replace("-", "");
		 String wbString = value.replace(",", "");
		 System.out.println(wbString);
		 String[] arrayStr = wbString.split(" ");
		 for(int i=0;i<arrayStr.length;i++){
			 System.out.println(arrayStr[i]);
			 if(!web.contains(arrayStr[i]))
				 throw new Exception(arrayStr[i] +" Text is not matching with the expected value "+webString);
		 }
	 
	 
	 }
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
	public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
	 
	public static final String TitleSubText = "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sulla tua Carta di pagamento, sul tuo Conto Corrente o sul tuo conto Paypal.";
	public static final String SubText = "Il servizio Addebito Diretto su Conto Corrente è attivo sulla fornitura:";
	public static final String AlertInnerText = "Con l’Addebito Diretto, chiamato anche SDD (SEPA Direct-Debit), potrai scegliere di addebitare i pagamenti delle bollette scegliendo una delle seguenti opzioni:Sul tuo conto correnteSu qualsiasi carta di pagamento dotata di PAN (codice di 16 cifre) gestita dai circuiti Visa, Mastercard, American Express, Diners ed emessa in Italia o nell'intera area SEPASul tuo conto PaypalIn questo modo risparmierai tempo e non avrai bisogno di ricordarti il giorno di scadenza delle tue bollette, perchè la tua banca provvederà al pagamento il giorno esatto della scadenza!Ti informiamo che se scegli l’addebito diretto su carta di pagamento la navigazione proseguirà sul sito di SIA, società convezionata operante sul circuito di intermediazione bancaria e garante della sicurezza nel trattamento dei dati riservati. La verifica carta viene effettuata con una transizione autorizzata di un (1) euro comprensiva di autenticazione (password) per la verifica dei protocolli. L’operazione di verifica carta contempla solo la richiesta di autorizzazione sulla carta a cui NON viene fatta seguire alcuna contabilizzazione dell’importo di verifica. Ricorda inoltre che, con l’introduzione del sistema di sicurezza 3DS, per confermare l’inserimento della tua carta di pagamento ti verrà richiesto di inserire il codice usa e getta che riceverai sul tuo cellulare o sull’APP di online banking.";
	public static final String Address = "SALITA MOTTO 30 - 13836 COSSATO BI";
	public static final String IBAN = "IT30P0301503200000000246802";
	public static final String Text = "Se vuoi disattivare il servizio, avvia la richiesta";
	
}
