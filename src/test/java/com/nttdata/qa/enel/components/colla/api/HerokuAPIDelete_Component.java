//HerokuAPIDelete_Component

package com.nttdata.qa.enel.components.colla.api;

import java.util.Properties;
import org.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import com.nttdata.qa.enel.util.Costanti;
import java.net.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;

//Il seguente componente si occupa di effettuare la request post al servizio HerokuAPIDelete_Component.
public class HerokuAPIDelete_Component {


	public HerokuAPIDelete_Component() 
	{

	}
	
	/**
	 * It sends a REST POST request toward a URL, passing some parameters. 
	 * These parameters are sent in JSON format.
	 * @param apiURL
	 * @param json
	 * @return
	 * @throws Exception
	 */
	public JSONObject sendPostRequest(Properties prop) throws Exception{
		/*OkHttpClient client = new OkHttpClient();
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody body = RequestBody.create(prop.getProperty("JSON_INPUT"), JSON);
		
	    Request request = new Request.Builder().url(prop.getProperty("API_URL"))
	    		.addHeader("TOUCHPOINT",prop.getProperty("TOUCHPOINT"))
	    		.addHeader("SOURCE_CHANNEL", prop.getProperty("SOURCE_CHANNEL"))
	    		.addHeader("Authorization",prop.getProperty("Authorization"))
				.addHeader("TID", prop.getProperty("TID"))
	    		.addHeader("SID", prop.getProperty("SID"))
	    		.addHeader("Content-Type", prop.getProperty("Content-Type"))
	    		.delete()
	    		.build();
	    Response response = client.newCall(request).execute();*/
		Response response = sendPost(prop);
//	    int code = 0;
	    JSONObject jsonObject = null;
	    JSONObject jsonObjectStatus = null;
	    try
	    {
		   			    
//		    if (code != 200 )
		    if (!response.toString().contains("code=200") )
		        throw new Exception("Unexpected Http code " + response);
		
		    jsonObject = new JSONObject(response.body().string());
		    jsonObjectStatus = new JSONObject(jsonObject.get("status").toString());
//		    code = Integer.parseInt(jsonObjectStatus.get("code").toString());
	    }
	    catch (Exception e)
	    {
	    	throw new Exception("Unexpected Http response :" + response.toString());
	    }
	    
    	return jsonObjectStatus;
	}
	
	/**
	 * It sends a REST POST request toward a URL, passing some parameters. 
	 * These parameters are sent in JSON format.
	 * @param apiURL
	 * @param json
	 * @return Data Response (String)
	 * @throws Exception
	 */
	public String sendPostRequestReturnData(Properties prop) throws Exception{
		/*OkHttpClient client = new OkHttpClient();
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		//RequestBody body = RequestBody.create(prop.getProperty("JSON_INPUT"), JSON);
		
	    Request request = new Request.Builder().url(prop.getProperty("API_URL"))
	    		.addHeader("Authorization",prop.getProperty("Authorization"))
	       		.addHeader("TOUCHPOINT",prop.getProperty("TOUCHPOINT"))
	    		.addHeader("SOURCE_CHANNEL", prop.getProperty("SOURCE_CHANNEL"))
				.addHeader("TID", prop.getProperty("TID")) 
	    		.addHeader("SID", prop.getProperty("SID")) 
	    		.addHeader("Content-Type", prop.getProperty("Content-Type"))
				.addHeader("CLIENT_IP", "10.10.11.11")
	    	    .delete()
	    		.build();
	    Response response = client.newCall(request).execute();*/
		Response response = sendPost(prop);
	   // int code = 0;
	    JSONObject jsonObject = null;
	   // JSONObject jsonObjectStatus = null;
	    try
	    {
		   			    
//		    if (code != 200 )
		    if (!response.toString().contains("code=200"))
		        throw new Exception("Unexpected Http code " + response);
		
		    jsonObject = new JSONObject(response.body().string());
		    // jsonObjectStatus = new JSONObject(jsonObject.get("data").toString());
		  //  code = Integer.parseInt(jsonObjectStatus.get("code").toString());	
		    
	    }
	    catch (Exception e)
	    {
	    	throw new Exception("Unexpected Http response :" + response.toString());
	    }
	    
		//if (code != 200)
		 //   throw new Exception("Unexpected code response :" + jsonObjectStatus.toString());		

	    //jsonObjectStatus = new JSONObject(jsonObject.get("data").toString());

    	// return jsonObjectStatus.toString();
	    
	    return jsonObject.toString();
	}

/**
 * It sends a REST POST request toward a URL, passing some parameters. 
 * These parameters are sent in JSON format.
 * @param apiURL
 * @param json
 * @return JsonBody
 * @throws Exception
 */
		public JSONObject sendPostRequestReturnJsonBody(Properties prop) throws Exception{
		/*	OkHttpClient client = new OkHttpClient();
			MediaType JSON = MediaType.parse("application/json; charset=utf-8");
			RequestBody body = RequestBody.create(prop.getProperty("JSON_INPUT"), JSON);
			
			Request request = new Request.Builder().url(prop.getProperty("API_URL"))
		    		.addHeader("Authorization",prop.getProperty("Authorization"))
		       		.addHeader("TOUCHPOINT",prop.getProperty("TOUCHPOINT"))
		    		.addHeader("SOURCE_CHANNEL", prop.getProperty("SOURCE_CHANNEL"))
					.addHeader("TID", prop.getProperty("TID")) 
		    		.addHeader("SID", prop.getProperty("SID")) 
		    		.addHeader("Content-Type", prop.getProperty("Content-Type"))
					.addHeader("CLIENT_IP", "10.10.11.11")
		    	    .delete()
		    		.build();
		    Response response = client.newCall(request).execute();*/
			Response response = sendPost(prop);
		    JSONObject jsonObject = null;
		    JSONObject jsonObjectStatus = null;
		    
		    try
		    {
			   			    
		//	    if (code != 200 )
			    if (!response.toString().contains("code=200") )
			        throw new Exception("Unexpected Http code " + response);

			    jsonObject = new JSONObject(response.body().string());
			    jsonObjectStatus = new JSONObject(jsonObject.get("status").toString());
		//	    code = Integer.parseInt(jsonObjectStatus.get("code").toString());
		
			    
		    }
		    catch (Exception e)
		    {
		    	throw new Exception("Unexpected Http response :" + response.toString());
		    }
		    
			return jsonObject;
		}
		/**
		 * It sends a REST POST request toward a URL, passing some parameters. 
		 * These parameters are sent in JSON format.
		 * @param apiURL
		 * @param json
		 * @return response Object
		 * @throws Exception
		 */
				private Response sendPost(Properties prop) throws Exception{
					
					OkHttpClient client = new OkHttpClient();
					//MediaType JSON = MediaType.parse("application/json; charset=utf-8");
					//RequestBody body = RequestBody.create(prop.getProperty("JSON_INPUT"), JSON);
					Response response = null;
					
					Request request = new Request.Builder().url(prop.getProperty("API_URL"))
				    		.addHeader("Authorization",prop.getProperty("Authorization"))
				       		.addHeader("TOUCHPOINT",prop.getProperty("TOUCHPOINT"))
				    		.addHeader("SOURCE_CHANNEL", prop.getProperty("SOURCE_CHANNEL"))
							.addHeader("TID", prop.getProperty("TID")) 
				    		.addHeader("SID", prop.getProperty("SID")) 
				    		.addHeader("Content-Type", prop.getProperty("Content-Type"))
							.addHeader("CLIENT_IP", "10.10.11.11")
				    	    .delete()
				    		.build();
				    
					try (Socket socket = new Socket(Costanti.proxyAddress, Costanti.proxyPort))
					{//Invio request con Proxy
					    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Costanti.proxyAddress, Costanti.proxyPort));


					    client = new OkHttpClient.Builder().proxy(proxy).build();
					    response = client.newCall(request).execute();

					}
					catch  (Exception e)
				    {//Invio request senza Proxy	
						client = new OkHttpClient();
					    response = client.newCall(request).execute();
				    }

					return response;
				}
}
