package com.nttdata.qa.enel.components.lightning;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ReferenteComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public ReferenteComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
	  
   
    public final By referenteTableCheckbox = By.xpath("//div[@id='Referente']//td/lightning-input");
    public final By confermaReferente = By.xpath("//div[@id='Referente']//button[text()='Conferma']");
	  
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void press(String frame,By oggetto) throws Exception{
		util.frameManager(frame,oggetto, util.scrollToVisibility, false);
		util.frameManager(frame,oggetto, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}
	

	
}
