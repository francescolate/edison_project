package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class PrivateAreaAccountBSNComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public List<WebElement> menuItemsList = null;

	public By accountLogo = By.xpath("//img[@id='avatarUtente']");
	public By modificaProfiloLink = By.xpath("//a[@id='modificaProfiloLink']");
	public By modificaDatiContattoHeader = By.xpath("//h1[text()='Modifica dati di contatto']");
	public By modificaDatiContattoHeaderText = By.xpath("//p[@class='lead']");
	public By modificaDatiContattoCompilazione = By.xpath("//p[contains(text(), 'Completa')]");
	public By modificaDatiContattoCompilazioneVerifica = By.xpath("//p[contains(text(), 'Verifica')]");
	public By labelTitolare = By.xpath("//label[text()='Titolare']");
	public By inputTitolare = By.xpath("//label[text()='Titolare']/following-sibling::input");
	public String inputTitolareString = "//label[text()='Titolare']/following-sibling::input";
	public By labelCf = By.xpath("//label[text()='Codice fiscale']");
	public By inputCf = By.xpath("//label[text()='Codice fiscale']/following-sibling::input");
	public String inputCfString = "//label[text()='Codice fiscale']/following-sibling::input";
	public By labelTelefono = By.xpath("//label[text()='Telefono']");
	public By inputTelefono = By.xpath("//label[text()='Telefono']/following-sibling::input");
	public String inputTelefonoString = "//label[text()='Telefono']/following-sibling::input";
	public By inputTelefonoNew = By.xpath("//span[text()='Telefono']//ancestor::div[1]//ancestor::div[1]/following-sibling::div/div/input");
	public String inputTelefonoStringNew = "//span[text()='Telefono']//ancestor::div[1]//ancestor::div[1]/following-sibling::div/div/input";
	public By labelEmail = By.xpath("//label[text()='Email']");
	public By inputEmail = By.xpath("//label[text()='Email']/following-sibling::input");
	public String inputEmailString = "//label[text()='Email']/following-sibling::input";
	public By inputEmailStar = By.xpath("//label[text()='Email*']/following-sibling::input");
	public String inputEmailStarString = "//label[text()='Email*']/following-sibling::input";
	public By labelPec = By.xpath("//label[text()='Email PEC']");
	public By inputPec = By.xpath("//label[text()='Email PEC']/following-sibling::input");
	public String inputPecString = "//label[text()='Email PEC']/following-sibling::input";
	public By telMailPec = By.xpath("//p[contains(text(), 'clicca su')]");
	public By modificaButton = By.xpath("//a[@class='btn btn-primary href-sf-prevent-default']");
	public By mandatoryEmailErrorText = By.xpath("//label[@class='has-error']");
	public By esciButton = By.xpath("//a[@name='Esci']");
	public By prosegui = By.xpath("//a[text()='Prosegui']");
	public By proseguiButton = By.xpath("//button[text()='Prosegui']");
	public By confermaButton = By.xpath("//button[text()='Conferma']");
	public By indietroButton = By.xpath("//button[text()='INDIETRO']");
	
    public PrivateAreaAccountBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner1();
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public void compareTextDisabledInput(String xpath, String textToCheck) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		if(!s.equals(textToCheck))
			throw new Exception("Il testo cercato:\n\t"+textToCheck+"\nè diverso da quello in pagina:\n\t"+s);
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}

	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
	}
	
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public static final String modificaDatiDiContattoHeaderText = "Modifica dati di contatto";
	public static final String modificaDatiDiContattoHeaderSubText = 	"In questa sezione sono riportati i dati di contatto che ci hai comunicato. "
																		+ "Se desideri modificarli, integrarli o sostituirli, inserisci le nuove informazioni nei campi corrispondenti.";
	public static final String telMailPecText = "Di seguito sono riportati i tuoi dati di contatto. Per aggiornarli clicca su \"modifica\".";
	public static final String modificaDatiDiContattoCompilazioneText = "Completa i campi sottostanti e clicca su prosegui";
	public static final String modificaDatiDiContattoCompilatzioneVerificaText = "Verifica i campi sottostanti e clicca su conferma";
}
