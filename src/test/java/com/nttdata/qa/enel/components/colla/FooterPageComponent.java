package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class FooterPageComponent {
	WebDriver driver;
	SeleniumUtilities util;

	public By before_text=By.xpath("//ul[@class='footer-copyright']//li[1]");
	public By after_text=By.xpath("//ul[@class='footer-copyright']//li[2]");
	public By informazioniLegaliLink=By.xpath("//a[@href='/it/supporto/faq/info-legali' and text()='Informazioni Legali']");
    public By creditsLink=By.xpath("//a[@href='/it/supporto/faq/credits' and text()='Credits']");		
	public By privacyLink= By.xpath("//a[@href='/it/supporto/faq/privacy' and text()='Privacy']");
	public By cookieLink=By.xpath("//a[@href='/it/supporto/faq/cookie-policy' and text()='Cookie Policy']");
	public By remitLink=By.xpath("//a[text()='Remit']"); ////a[@href='https://corporate.enel.it/it/remit.html' and text()='Remit']
	public By twitterIcon=By.xpath("//a[@class='icon-twitter']");
	public By facebookIcon=By.xpath("//a[@class='icon-fb']");
	public By youtubeIcon=By.xpath("//a[@class='icon-youtube']");
	public By linkedinIcon=By.xpath("//a[@href='https://www.linkedin.com/company/enelenergia']");
	public By instagramIcon = By.xpath("//a[@href='https://www.instagram.com/enelenergia']");
	public By telegramIcon = By.xpath("//a[@href='https://t.me/enelenergiabot']");
	public By legalInfoPage=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[text()='Informazioni legali']");
	public By creditsPage=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[text()='Credits']");
	public By privacyPage=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[text()='Privacy']");
	
	public By legal_links=By.xpath("//ul[@class='footer-legal-links']//li");
	public By social_icons=By.xpath("//li[@class='footer-social-item']//a");
	public String legal_link_by_text="//ul[@class='footer-legal-links']//li//a[text()='#']";
	//public String legal_link_by_text="//a[text()='#']";
	
	public By contattaciLink = By.xpath("//a[@href='/it/supporto/faq/contattaci']");
	
	public By footerText = By.xpath("//ul[@class='footer-copyright']");
	public By footerLink = By.xpath("//ul[@class='footer-legal-links']");
	


	public FooterPageComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		
		util = new SeleniumUtilities(this.driver);
	}

	public void checkTextFooter(By Object1, String beforetext,By Object2, String aftertext) throws Exception {
		String obj1Text = driver.findElement(Object1).getText();
		String obj2Text = driver.findElement(Object2).getText();
		
		if (!obj1Text.contentEquals(beforetext) && !obj2Text.contentEquals(aftertext) )
			 throw new Exception("on the footer of page is not present le descriptions expected: before description is "+obj1Text+";expected before description is "+beforetext+";after description is "+obj2Text+";expected after description is "+aftertext);
		
	}

	public void checkFooterLegalLinksByTexts(By links_object, String[] texts) throws Exception {
		List<WebElement> objs = util.waitAndGetElements(links_object);
		int i = -1;
		if (objs.size() != texts.length) 
			throw new Exception("the total numer of legal links are not correct excepted '" + texts[i].toString() + "; total links present '" + objs.size());
		for(WebElement obj  : objs) {
			i++;
			if(!obj.getText().equalsIgnoreCase(texts[i])) {
				throw new Exception("the link '" + texts[i].toString() + " is not present in the legal link footer");
			}
		}
	}
	
	public void clickFooterLegalLinkByText(String textLink) throws Exception{
		util.objectManager(By.xpath(legal_link_by_text.replace("#", textLink)), util.scrollAndClick);
	}

	public void checkFooterSocialIconsByAttibuteText(By links_object, String[] texts , String attribute) throws Exception {
		List<WebElement> objs = util.waitAndGetElements(links_object);
		int i = -1;
		if (objs.size() != texts.length) 
			throw new Exception("the total numer of social icons are not correct : excepted '" + texts[i].toString() + "; total social icons present '" + objs.size());
		for(WebElement obj  : objs) {
			i++;
			if(!obj.getAttribute(attribute).equalsIgnoreCase(texts[i])) {
				throw new Exception("the social icon '" + texts[i].toString() + " is not present in the footer");
			}
		}
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}	
	
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
    
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	
	public void verifyFooter() throws Exception{
		verifyComponentExistence(footerText);
		verifyComponentExistence(footerLink);
		compareText(footerText, "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a. Gruppo IVA Enel P.IVA 15844561009", true);
		compareText(footerLink, "Informazioni Legali Credits Privacy Cookie Policy Preferenze Cookie Remit", true);
	}

	
}
