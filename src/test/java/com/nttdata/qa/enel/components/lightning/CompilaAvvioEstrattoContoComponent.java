package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CompilaAvvioEstrattoContoComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public By inputFornituraDaCercare = By.xpath("//label[contains(text(),'Cerca Fornitur')]/..//input");
//	public By divPrimaFornitura = By.xpath("//tbody/tr//div[contains(@id, 'DivCheckboxA')]");
	public By divPrimaFornitura = By.xpath("//tbody/tr//div[contains(@id, 'theCheckboxDiv_')]");
	public By buttonConfermaSelezionaFornitura = By.xpath("//button[contains(text(),'Conferma Selezione Fornitura')]");
	public By selectStatoPagamentoConsumi = By.xpath("//select[@id='j_id0:FormId:ModalitaPagamento']");
	public By inputFatturaDataInizio=By.xpath("//input[@id='j_id0:FormId:datainizio_filtro']");
	public By inputFatturaDataFine=By.xpath("//input[@id='j_id0:FormId:datafine_filtro']");
	public By buttonApplicaFiltri = By.xpath("//button[contains(text(),'Applica filtri')]");
	public By inputFatturaDaCercare = By.xpath("//div[contains(text(),'Cerca Fatture')]/..//input");
	public By buttonStampa = By.xpath("//button[contains(text(),'Stampa')]");
	public By buttonConferma = By.xpath("//button[@id='ButtonConferma']");
	public By spanMessPopupPresaInCarico = By.xpath("//span[@id='ErrorMessage']");
	public By buttonOkPresaInCarico = By.xpath("//button[contains(text(),'OK') and @value='ChiudiCase']");
	public By tabellaFornitura= By.xpath("//table[@id='selectcommodityEC_Table']");
	private String intestazioneTabellaFornitura="./thead/tr/th";
	private String rigaTabellaFornitura="./tbody/tr[%d]";
	private String elemTabellaFornitura="/td[%d]";
	private By msgEstrattoContoError= By.xpath("//span[@id='ErrorMessage']");
	private By msgEstrattoNessunaFatturaError= By.xpath("//span[@id='ErrorHeader']");
	private By righeFatture= By.xpath("//table[@id='fatture']/tbody/tr//input[@type='checkbox']");
	

	public CompilaAvvioEstrattoContoComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(this.driver);
		PageFactory.initElements(driver, this);
	}
	
	public void cercaFornitura(String frame,By inputFornituraDaCercare,By divPrimaFornitura,String pod_pdr) throws Exception{
//		TimeUnit.SECONDS.sleep(10);
//		util.frameManager(frame, inputFornituraDaCercare,util.sendKeys,pod_pdr);
		//util.frameManager(frame, divPrimaFornitura,util.scrollAndClick);
        util.getFrameActive();
		util.objectManager(inputFornituraDaCercare, util.scrollAndSendKeys,pod_pdr);
		driver.switchTo().defaultContent();
	}
	
	public void selezionaFornitura(String frame, By byTabella, String nomeColonna, String valoreColonna) throws Exception{
        util.getFrameActive();
		TimeUnit.SECONDS.sleep(1);
		if (!util.verifyExistence(byTabella, 20))
			throw new Exception("Tabella non trovata");
		else {
			WebElement elTabella=util.waitAndGetElement(byTabella);
			int num=0;
//			//table[@id='selectcommodityEC_Table']//tr//td
//			num = util.getTableRowFromColumnData(elTabella, nomeColonna, valoreColonna, intestazioneTabellaFornitura, rigaTabellaFornitura, elemTabellaFornitura,1);
//			//System.out.println("numero riga:"+num);
			//click su radiobutton relativo all'elemento cercato
//			String elemToClick="//table[@id='selectcommodityEC_Table']/tbody/tr["+num+"]/td[1]/span";
			String elemToClick="//table[@id='selectcommodityEC_Table']/tbody/tr/td[1]";
			
			driver.findElement(By.xpath(elemToClick)).click();
			driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(5);
		}

		
//		WebElement frameToSw = driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
//				);
//		driver.switchTo().frame(frameToSw);
//		WebElement elTabella=util.waitAndGetElement(byTabella);
//		int num=0;
//		num = util.getTableRowFromColumnData(elTabella, nomeColonna, valoreColonna, intestazioneTabellaFornitura, rigaTabellaFornitura, elemTabellaFornitura,1);
////		//System.out.println("numero riga:"+num);
//		//click su radiobutton relativo all'elemento cercato
//		String elemToClick="//table[@id='selectcommodityEC_Table']/tbody/tr["+num+"]/td[1]/span";
//		driver.findElement(By.xpath(elemToClick)).click();
//		driver.switchTo().defaultContent();
//		TimeUnit.SECONDS.sleep(5);
	}
	

//	public void ricercaFatture(String frame,String seCanoneTV,By statoPagamentoConsumi,By fatturaDataInizio, By fatturaDataFine,By applicaFiltri, By fatturaDaCercare, By stampa,String fattura,String modalita,String flaginseriscidata,String datainizio,String datafine) throws Exception{
	public void ricercaFatture(String frame,String seCanoneTV,By statoPagamentoConsumi,By fatturaDataInizio, By fatturaDataFine,By applicaFiltri, By fatturaDaCercare, By stampa,String fattura,String modalita,String flaginseriscidata) throws Exception{
		TimeUnit.SECONDS.sleep(3);
        util.getFrameActive();
		if(seCanoneTV.equals("N")){
//			util.frameManager(frame, statoPagamentoConsumi, util.select,modalita);
			util.objectManager(statoPagamentoConsumi, util.select,modalita);
		TimeUnit.SECONDS.sleep(3);
		}
		//Se flag inseriscidata è Y inserisco inizio e fine data come filtri
		//data fine Sysdate
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String datafine = simpleDateFormat.format(calendar.getTime()).toString();
		//data inizio Sysdate - 5 anni
		calendar.add(Calendar.YEAR, -5);
		String datainizio = simpleDateFormat.format(calendar.getTime());

		if(flaginseriscidata.compareToIgnoreCase("Y")==0){
//			util.frameManager(frame,fatturaDataInizio,util.clear);
//			util.frameManager(frame,fatturaDataInizio,util.sendKeys,datainizio);
//			util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
//			util.frameManager(frame,fatturaDataFine,util.clear);
//			util.frameManager(frame,fatturaDataFine,util.sendKeys,datafine);
//			util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
			util.objectManager(fatturaDataInizio, util.clear);
			util.objectManager(fatturaDataInizio, util.sendKeys,datainizio);
			util.objectManager(By.xpath("//body"), util.scrollAndClick);
			util.objectManager(fatturaDataFine, util.clear);
			util.objectManager(fatturaDataFine, util.sendKeys,datainizio);
			util.objectManager(By.xpath("//body"), util.scrollAndClick);
			TimeUnit.SECONDS.sleep(10);
		}
//		util.frameManager(frame, applicaFiltri,util.scrollAndClick);
		driver.findElement(applicaFiltri).click();
		driver.switchTo().defaultContent();
        util.getFrameActive();
//		WebElement frameToSw = driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
//				);
//		driver.switchTo().frame(frameToSw);
		TimeUnit.SECONDS.sleep(60);
		//Verifica presenza pop-up errore estratto conto
		if(util.exists(msgEstrattoContoError, 30)){
			if(driver.findElement(msgEstrattoContoError).isDisplayed())
				throw new Exception("Errore di Comunicazione o Estratto conto già presente");
		}
		
//		//Ricerca fattura
//		util.frameManager(frame, fatturaDaCercare,util.sendKeys,fattura);
		
		if(util.exists(righeFatture, 60)){
			//unflegga tutte le fatture
			List<WebElement> l = util.waitAndGetElements(By.xpath("//table[@id='fatture']/tbody/tr//input[@type='checkbox']"),10,1);
			for(WebElement el : l) {
				if(el.isSelected()) {
					util.objectManager(el, util.scrollToVisibility,true);
					Thread.currentThread().sleep(2000);
					util.waitAndGetElement(el,By.xpath("..")).click();
				}
			}
			//fine unflegga tutte le fatture
		} else {
			throw new Exception("Fatture non trovate! Impossibile effettuare Estratto Conto");
		}
			
		
		
		//flegga la fattura interessata
//		util.objectManager(By.xpath("//table[@id='fatture']/tbody//td/span[text()='"+fattura+"']/ancestor::tr//input[@type='checkbox']/.."), util.scrollAndClick);
		util.objectManager(By.xpath("//table[@id='fatture']/tbody//tr//input[@type='checkbox']/.."), util.scrollAndClick);

		driver.switchTo().defaultContent();
        util.getFrameActive();
//		util.frameManager(frame, stampa, util.scrollAndClick);
		util.objectManager(stampa, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(20);
//		WebElement frameToSw2 = driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
//				);
//		driver.switchTo().frame(frameToSw2);
		//Verifica presenza pop-up errore mancanza fatture
		if(util.exists(msgEstrattoNessunaFatturaError, 30)){
			if(driver.findElement(msgEstrattoNessunaFatturaError).isDisplayed())
				throw new Exception("Non sono state estratte fatture");
		}
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}
		
	public void confermaInserimentoDatiEstrattoConto(By button,String frame) throws Exception {
		util.frameManager(frame, button, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
	}

	public void confermaInserimentoDatiEstrattoConto(By button) throws Exception {
		util.objectManager(button, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
	}

	public void popUpPresaInCarico(String frame,By messPopupPresaInCarico,By buttonOkPresaInCarico, String mess) throws Exception{
		String messshowed=driver.findElement(messPopupPresaInCarico).getText();
		System.out.println("Messaggio visualizzato:"+messshowed);
		if(messshowed.compareTo(mess)!=0){
			throw new Exception("Il messaggio visualizzato: "+messshowed+"Non è quello atteso:"+mess);
		}
//		util.frameManager(frame,messPopupPresaInCarico, util.scrollAndClick);
		util.objectManager(messPopupPresaInCarico, util.scrollAndClick);
//		WebElement frameToSw = driver.findElement(
//				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
//				);
//		driver.switchTo().frame(frameToSw);
		driver.switchTo().defaultContent();

//		util.frameManager(frame, buttonOkPresaInCarico, util.scrollAndClick);
		util.objectManager(buttonOkPresaInCarico, util.scrollAndClick);
	}

	public void popUpPresaInCarico(By messPopupPresaInCarico,By buttonOkPresaInCarico, String mess) throws Exception{
		util.objectManager(messPopupPresaInCarico, util.scrollAndClick);
        util.getFrameActive();
		String messshowed=driver.findElement(messPopupPresaInCarico).getText();
		driver.switchTo().defaultContent();
		util.objectManager(buttonOkPresaInCarico, util.scrollAndClick);
	}

	public void confermaSelezione(String frame,By button) throws Exception{
//		util.frameManager(frame, button,util.scrollAndClick);
//		TimeUnit.SECONDS.sleep(10);
		util.getFrameActive();
		driver.findElement(button).click();
		driver.switchTo().defaultContent();
		
	}


}
