package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSN_ConactDetailsComponent {
	
	SeleniumUtilities util;
	WebDriver driver;
	
	public By pageTitle =By.xpath("//h1[text()='Richiesta contatto']");
	public By subText = By.xpath("//div[@class='panel-body']/p[@class='lead']");
	public By seiInteressatoA = By.xpath("//div[@id='prefrichiesta_key']/label[1]");
	public By tipologiaOfferta =By.xpath("//div[@id='prefofferta_key']/label[1]");
	public By provincia = By.xpath("//div[@id='provincia']/label[1]");
	public By canaleContattoPreferito = By.xpath("//div[@id='prefcontact_key']/label[1]");
	public By email =By.xpath("//div[@id='email-form-group']/label[1]");
	public By telefonoFisso = By.xpath("//div[@class='col-xxs-12 col-xs-6'][6]/div[@class='form-group']/label");
	public By cellulare = By.xpath("//label[text()='Cellulare']");
	public By bottemText = By.xpath("//p[@class='margin-bottom-25']");
	public By acceto =By.xpath("//label[@class='radio-block inline-block'][1]/span");
	public By nonAcceto = By.xpath("//label[@class='radio-block inline-block'][2]/span");
	public By invia = By.xpath("//button[@id='submitContatti']");
	public By seiInteressatoAError = By.xpath("//label[@id='interessato-error']");
	public By tipologiaOffertaError =By.xpath("//label[@id='tipologiaofferta-error']");
	public By provinciaError = By.xpath("//label[@id='Provincia-error']");
	public By canaleContattoPreferitoError = By.xpath("//label[@id='canale-error']");
	public By emailError =By.xpath("//label[@id='email-error']");
	public By telefonoFissoError = By.xpath("//div[@class='col-xxs-12 col-xs-6'][6]/div[@class='form-group']/label");
	public By cellulareError = By.xpath("//div[@class='col-xxs-12 col-xs-6'][7]/div[@class='form-group']/label");
	public By emailInput = By.xpath("//input[@id='email']");
	public By cellularInput = By.xpath("//input[@id='Cellulare']");
	public By informativaSullaPrivacy =By.xpath("//h5");
	public By path = By.xpath("//ul[@class='breadcrumbs component']");
	public static final String Path = "Home / Imprese";
	public static final String SubText ="Gentile cliente, compilando il modulo sottostante la richiesta verrà inoltrata ad un nostro consulente. Verrai ricontattato al più presto.";
	
	public static final String fields ="Sei interessato a *;Tipologia offerta *;Provincia * ;Canale contatto preferito *;Email *;Telefono fisso;Cellulare";
	
	public static final String bottomText ="Ti ricordiamo che Enel Energia tratterà i tuoi dati personali nel rispetto delle disposizioni previste dal Regolamento UE 2016/679 (GDPR) per le finalità e con le modalità indicate nell'Informativa Privacy. Titolare del trattamento è Enel Energia S.p.A. Informativa privacy completa disponibile sul sito https://www.enel.it";
	
	public BSN_ConactDetailsComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(5000);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void waitForElementToDisplay (By checkObject) throws Exception{
		
		WebDriverWait wait = new WebDriverWait (driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyFields(By checkElement,String element) throws Exception{
		
		Thread.sleep(15000);
		util.scrollToElement(driver.findElement(checkElement));
		String actualText = driver.findElement(checkElement).getText();
         if(!element.toString().contains(actualText))
             throw new Exception ("This page does not has the following Field :'Sei interessato a','Tipologia offerta','Provincia','Canale contatto preferito','Email','Telefono fisso','Cellulare'" );
                
	}

	public void enterInput(By checkObject, String property) throws Exception {			
		
		util.objectManager(checkObject, util.sendKeys, property);	
	}
	
	public void checkFieldDisplay(By checkObject) throws Exception {
		
		Boolean isPresent = driver.findElements(checkObject).size() > 0;
		if(isPresent.equals("true"))
			throw new Exception(checkObject+"Field is displaying");
	}

}
