package com.nttdata.qa.enel.components.colla;

import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GDPRComponent {
	WebDriver driver;
	SeleniumUtilities util;
	Properties prop;
	
	public By homePagePath1 = By.xpath("//main[@id='main']//a[.='Area riservata']");
	public By homePagePath2 = By.xpath("//main[@id='main']//li[contains(text(),'Home')]");
	public By homePageHeading = By.xpath("//section[@id='content-home']//h1[contains(text(),'Ben')]");
	public By iTuoiDirtti = By.xpath("//li[@id='gdprId']/a");
	public By iTuoiDirttiPath1 = By.xpath("//main[@id='main']//ol[@class ='breadcrumb']/li[1]");
	public By iTuoiDirttiPath2 = By.xpath("//main[@id='main']//ol[@class ='breadcrumb']/li[2]");
	public By datiAnagraficiHeading = By.xpath("//div[@class='row'][1]/div[@class='col-xs-12']/h5[contains(text(),'Dati')]");
	public By titolare = By.xpath("//b[@id='nome']");
	public By codiFiscale = By.xpath("//b[@id='cf']");
	public By email = By.xpath("//b[@id='email']");
	public By dirittoDiInformazione = By.xpath("//div[@id='headingOne']//descendant::b[contains(text(),'Informazione')]");
	public By collapseIconDirittoDiInformazione = By.xpath("//div[@id='headingOne']//span");
	public By dirittiDiInformazioneText = By.xpath("//div[@id='collapseOne']/child::div");
	public By cliccaQuiDirittiDiInformazioneText = By.xpath("//div[@id='collapseOne']//a");
	public By dirittoDiAccesso = By.xpath("//div[@id='headingTwo']//descendant::b[contains(text(),'Accesso')]");
	public By collapseIconDirittoDiAccesso = By.xpath("//div[@id='headingTwo']//span");
	public By dirittoDiAccessoText1 = By.xpath("//div[@id='collapseTwo']//div[@class='col-xs-12']/p[1]");
	public By dirittoDiAccessoText2 = By.xpath("//div[@id='collapseTwo']//div[@class='col-xs-12']/p[2]");
	public By dirittoDiAccessoText3 = By.xpath("//div[@id='collapseTwo']//div[@class='col-xs-12']/p[3]");
	public By modificaITuoiDati = By.xpath("//div[@id='collapseTwo']//p[2]/a");
	public By dirittoDiCancellazione = By.xpath("//div[@id='headingThree']//b[.='Diritto di Cancellazione']");
	public By collapseIconDirittoDiCancellazione = By.xpath("//div[@id='headingThree']//span");
	public By dirittoDiCancellazioneText1 = By.xpath("//div[@id='collapseThree']//p[1]");
	public By dirittoDiCancellazioneText2 = By.xpath("//div[@id='collapseThree']//li[1]");
	public By dirittoDiCancellazioneText3 = By.xpath("//div[@id='collapseThree']//li[2]");
	public By dirittoDiCancellazioneText4 = By.xpath("//div[@id='collapseThree']//p[2]");
	public By dirittoLimitazione = By.xpath("//div[@id='headingFour']//b[.='Diritto alla Limitazione del Trattamento']");
	public By collapseIconDirittoLimitazione = By.xpath("//div[@id='headingFour']//span");
	public By dirittoLimitazioneText1 = By.xpath("//div[@id='collapseFour']//p[1]");
	public By dirittoLimitazioneText2 = By.xpath("//div[@id='collapseFour']//li[1]");
	public By dirittoLimitazioneText3 = By.xpath("//div[@id='collapseFour']//li[2]");
	public By dirittoLimitazioneText4 = By.xpath("//div[@id='collapseFour']//li[3]");
	public By dirittoLimitazioneText5 = By.xpath("//div[@id='collapseFour']//p[2]");
	public By dirittoDiOpposizione = By.xpath("//div[@id='headingFive']//b");
	public By collapseIcondirittoDiOpposizione = By.xpath("//div[@id='headingFive']//span");
	public By dirittoDiOpposizionetext1 = By.xpath("//div[@id='collapseFive']//p[1]");
	public By dirittoDiOpposizionetext2 = By.xpath("//div[@id='collapseFive']//p[2]");
	public By dirittoDiOpposizionetext3 = By.xpath("//div[@id='collapseFive']//p[3]");
	public By dirittoDiOpposizionetext4 = By.xpath("//div[@id='collapseFive']//p[4]");
	public By dirittoDiRettifica = By.xpath("//div[@id='headingSix']//b");
	public By collapseIconDirittoDiRettifica = By.xpath("//div[@id='headingSix']//span");
	public By dirittoDiRettificaText1 = By.xpath("//div[@id='collapseSix']//p[1]");
	public By dirittoDiRettificaText2 = By.xpath("//div[@id='collapseSix']//p[2]");
	public By dirittoDiRettificaText3 = By.xpath("//div[@id='collapseSix']//p[3]");
	public By cliccaQuiDirittoDiRettifica = By.xpath("//div[@id='collapseSix']//a[.='Clicca qui']");
	public By dirittoDiPortabilita = By.xpath("//div[@id='headingSeven']//b");
	public By collapseIconDirittoDiPortabilita = By.xpath("//div[@id='headingSeven']//span");
	public By dirittoDiPortabilitaHeading = By.xpath("//div[@id='collapseSeven']//h5");
	public By dirittoDiPortabilitaText1 = By.xpath("//div[@id='collapseSeven']//p[1]");
	public By dirittoDiPortabilitaText2 = By.xpath("//div[@id='collapseSeven']//p[2]");
	public By dirittoDiPortabilitaText3 = By.xpath("//div[@id='collapseSeven']//p[3]");
	public By cliccaQuiDirittoDiPortabilita = By.xpath("//div[@id='collapseSeven']//a");
	public By aacediAlServizoButton = By.xpath("//div[@id='collapseSeven']//a[@id='submit-gdpr']");
	public By sectionPath1 = By.xpath("//div[@id='sc']//ol[@class='breadcrumb']/li[1]");
	public By sectionPath2 = By.xpath("//div[@id='sc']//ol[@class='breadcrumb']/li[2]");
	public By sectionHeading = By.xpath("//div[@id='sc']//h1[.='Diritto di Portabilità']");
	public By checkBox = By.xpath("//table[@class='table']/tbody/tr/td[1]/div[@class='check-group']");
	public By checkBox1 = By.xpath("//table[@class='table']/tbody/tr[1]/td[1]/div[@class='check-group']");
	public By checkBox2 = By.xpath("//table[@class='table']/tbody/tr[2]/td[1]/div[@class='check-group']");
	public By buttonRichediITuoiDati = By.xpath("//div[@id='sc']//button[2]");
	public By buttonContinuaRES = By.xpath("//div[@id='sc']//button[2]");
	public By tipologia = By.xpath("//table[@class='table']/thead/tr/th[2]");
	public By numeroCliente = By.xpath("//table[@class='table']/thead/tr/th[3]");
	public By indirizzoDiFornitura = By.xpath("//table[@class='table']/thead/tr/th[4]");
	public By indirizzoDiFornituraRES = By.xpath("//div[@id='sc']//span[.='Indirizzo della fornitura']");
	public By numeroClienteRES = By.xpath("//div[@id='sc']//span[.='Numero Cliente']");
	public By numeroClienteRESValue = By.xpath("//div[@id='sc']//span[.='961728208']");
	public By section2Text1 = By.xpath("//p[@id='titleSuccessGdpr']/strong");
	public By section2Text2 = By.xpath("//p[@class='bold feedMsg'][2]/strong");
	public By infoIcon = By.xpath("//tbody/tr/td[1]//span[@id='IT001E30294115_info']");
	public By infoPopupHeading = By.xpath("//div[@id='gdpr-modal']//h4");
	public By infoPopupText = By.xpath("//div[@id='gdpr-modal']//p");
	public By esciButton = By.xpath("//div[@class='row-btns row-btns-closing']/button[@class='btn btn-primary btn-bordered pull-left']");
	public By attizoneHeading = By.xpath("//div[@id='modal-boxed-attenzione']//h4");
	public By attizoneText = By.xpath("//div[@id='modal-boxed-attenzione']//p");
	public By tornaAlProcessoButton = By.xpath("//button[@id='closeAbort']");
	public By popupEsciButton =  By.xpath("//div[@id='sendAbort']");
	public By scaricaFile = By.xpath("//table[@class='table']/tbody/tr/td[5]/a[@class='small']");
	public By informativaPrivacy = By.xpath("//ul[@id='internal-menu']//a[.='Informativa Privacy']");
	public By iconUser = By.xpath("//header[@id='globalHedaer']//span[@class='icon-user']");
	public By customerData = By.xpath("//section[@id='profileSectionBusiness']");
	public By condizionigenerali = By.xpath("//h3[@id='condizioni-generali']");
	public By hoLettoText = By.xpath("//div[@id='generatedConsents']/div[@class='informative-new'][1]//span");
	public By informativaPrivacyText = By.xpath("//div[@id='generatedConsents']/div[@class='informative-new'][1]/div[@class='uIdcheckbox-container']//label[2]/button[@id='onlytext']");
	public By buttonFine = By.xpath("//div[@id='sc']//descendant::button[.='FINE']");
	public By sectionText = By.xpath("//div[@id='sc']//descendant::div[@class='resultBox-group']");
	public By esciButtonPopup = By.xpath("//div[@id='sendAbort']");
	public By checkBox3 = By.xpath("//table[@class='table']/tbody/tr[3]/td[1]/div[@class='check-group']");
	
	public GDPRComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
  
  public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
  
  public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
  
  public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
  
  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");

  }
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void  isElementNotPresent(By existentObject ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		
		try{
			WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
		} 
        catch (Exception e) {
        	e.printStackTrace();
        		            
            }
         }
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	public void isRichiediITuoiDatiDisabled(By existentObject) throws Exception{
		if ((!driver.findElement(checkBox).isSelected())&&(driver.findElement(buttonRichediITuoiDati).isEnabled()))
			throw new Exception("The button is Enabled");
		}
	
public void isContinuaDisabled(By existentObject) throws Exception{
		if ((!driver.findElement(checkBox).isSelected())&&(driver.findElement(buttonRichediITuoiDati).isEnabled()))
			throw new Exception("The button is Enabled");
		}

public void isRichiediITuoiDatiEnabled(By existentObject) throws Exception{
		if (!driver.findElement(buttonRichediITuoiDati).isEnabled())
			throw new Exception("The button is not Enabled");
		}



public void SwitchTabToSupporto(String Url) throws Exception
{
    String currentHandle = driver.getWindowHandle();
    Set <String> windows = driver.getWindowHandles();
   
   
    for (String winHandle : windows) {
       
        String pagetitle = driver.getTitle();
        driver.switchTo().window(winHandle);
     }
    	checkURLAfterRedirection(Url);
    	driver.close();
    	driver.switchTo().window(currentHandle);
}
	
	public static final String HOME_PAGE_PATH1 = "Area riservata";
	public static final String HOME_PAGE_PATH2 = "Homepage";
	public static final String HOME_PAGE_HEADING = "Benvenuto nella tua area dedicata Business";
	public static final String HOME_PAGE_PARAGRAPH2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette";
	public static final String PATH1_ITUOIDIRTTI = "Area riservata";
	public static final String PATH2_ITUOIDIRTTI = "I tuoi diritti";
	public static final String DATA_ANAGRAFICI_HEADING = "Dati anagrafici";
	public static final String DIRITTO_DI_INFORMAZIONE = "Diritto di Informazione";
	public static final String DIRITTO_DI_INFORMAZIONE_TEXT = "Al fine di prendere visione dell'informativa privacy, clicca qui.";
	public static final String DIRITTO_DI_ACCESSO = "Diritto di Accesso";
	public static final String DIRITTO_DI_ACCESSO_TEXT1 = "L'interessato ha il diritto di ottenere dal titolare del trattamento la conferma che sia o meno in corso un trattamento di dati personali che lo riguardano e in tal caso, di ottenere l'accesso ai dati personali.";
	public static final String DIRITTO_DI_ACCESSO_TEXT2 = "Se vuoi prendere visione o modificare i tuoi dati di contatto ed i relativi consensi, puoi visitare la sezione Modifica i tuoi dati.";
	public static final String DIRITTO_DI_ACCESSO_TEXT3 = "Per esercitare il diritto puoi anche scrivere alla casella di posta elettronica di seguito indicata: privacy.enelenergia@enel.com.";
	public static final String DIRITTO_DI_CANCELLAZIONE = "Diritto di Cancellazione";
	public static final String DIRITTO_DI_CANCELLAZIONE_TEXT1 = "L'interessato ha il diritto di ottenere dal titolare del trattamento la cancellazione dei dati personali che lo riguardano senza ingiustificato ritardo e il titolare del trattamento ha l'obbligo di cancellare senza ingiustificato ritardo i dati personali, se sussiste uno dei motivi seguenti:";
	public static final String DIRITTO_DI_CANCELLAZIONE_TEXT2 = "i dati personali non sono più necessari rispetto alle finalità per le quali sono stati raccolti o altrimenti trattati;";
	public static final String DIRITTO_DI_CANCELLAZIONE_TEXT3 ="l'interessato revoca il consenso su cui si basa il trattamento.";
	public static final String DIRITTO_DI_CANCELLAZIONE_TEXT4 = "Per esercitare il diritto puoi scrivere alla casella di posta elettronica di seguito indicata: privacy.enelenergia@enel.com.";
	public static final String DIRITTO_LIMITAZIONE = "Diritto alla Limitazione del Trattamento";
	public static final String DIRITTO_LIMITAZIONE_TEXT1 = "L'interessato ha il diritto di ottenere dal titolare la limitazione del trattamento quando ricorre una delle seguenti ipotesi:";
	public static final String DIRITTO_LIMITAZIONE_TEXT2 = "l'interessato contesta l'esattezza dei dati personali;";
	public static final String DIRITTO_LIMITAZIONE_TEXT3 = "il trattamento è illecito e l'interessato si oppone alla cancellazione dei dati personali e chiede invece che ne sia limitato l'utilizzo;";
	public static final String DIRITTO_LIMITAZIONE_TEXT4 = "i dati personali sono necessari all'interessato per l'accertamento, l'esercizio o la difesa di un diritto in sede giudiziaria.";
	public static final String DIRITTO_LIMITAZIONE_TEXT5 = "Per esercitare il diritto puoi scrivere alla casella di posta elettronica di seguito indicata: privacy.enelenergia@enel.com.";
	public static final String DIRITTO_DI_OPPOSIIONE = "Diritto di Opposizione";
	public static final String DIRITTO_DI_OPPOSIIONE_TEXT1 = "L'interessato ha il diritto di opporsi in qualsiasi momento, per motivi connessi alla sua situazione particolare, al trattamento dei propri dati personali che lo riguardano, compresa la profilazione.";
	public static final String DIRITTO_DI_OPPOSIIONE_TEXT2 = "Il titolare del trattamento deve astenersi dal trattamento, salvo che egli dimostri l'esistenza di motivi legittimi cogenti per procedere al trattamento che prevalgono sugli interessi, sui diritti e sulle libertà dell'interessato oppure per l'accertamento, l'esercizio o la difesa di un diritto in sede giudiziaria.";
	public static final String DIRITTO_DI_OPPOSIIONE_TEXT3 = "L'interessato ha il diritto di opporsi in qualsiasi momento al trattamento dei dati personali che lo riguardano effettuato per finalità di marketing, compresa la profilazione.";
	public static final String DIRITTO_DI_OPPOSIIONE_TEXT4 = "Per esercitare il diritto puoi scrivere alla casella di posta elettronica di seguito indicata: privacy.enelenergia@enel.com.";
	public static final String DIRITTO_DI_RETTIFICA = "Diritto di Rettifica";
	public static final String DIRITTO_DI_RETTIFICA_TEXT1 = "L'interessato ha il diritto di ottenere dal titolare del trattamento la rettifica dei dati personali inesatti che lo riguardano senza ingiustificato ritardo. Tenuto conto delle finalità del trattamento, l'interessato ha il diritto di ottenere l'integrazione dei dati personali incompleti, anche fornendo una dichiarazione integrativa.";
	public static final String DIRITTO_DI_RETTIFICA_TEXT2 = "Se vuoi modificare i tuoi dati di contatto (e-mail, telefoni, canale di contatto preferito) direttamente in Area Clienti, Clicca qui.";
	public static final String DIRITTO_DI_RETTIFICA_TEXT3 = "Per esercitare il diritto puoi anche scrivere alla casella di posta elettronica di seguito indicata: privacy.enelenergia@enel.com";
	public static final String DIRITTO_DI_PORTABILITA= "Diritto di Portabilità";
	public static final String DIRITTO_DI_PORTABILITA_HEADING= "Come posso richiedere una copia dei miei dati forniti ad Enel Energia?";
	public static final String DIRITTO_DI_PORTABILITA_TEXT1= "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.";
	public static final String DIRITTO_DI_PORTABILITA_TEXT2= "Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla email collaudol.oyaltycolazzo20.20@gmail.com";
	public static final String DIRITTO_DI_PORTABILITA_TEXT2_190= "Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla email collaudolo.yaltycolazzo.2020@gmail.com";
	public static final String DIRITTO_DI_PORTABILITA_TEXT3= "Se preferisci ricevere le informazioni ad un altro indirizzo di posta elettronica o l’email risulta “non presente”, clicca qui o accedi alla sezione ‘Modifica i tuoi dati’ prima di procedere.";
	public static final String SECTION_PATH1 = "AREA RISERVATA";
	public static final String SECTION_PATH2 = "I TUOI DIRITTI";
	public static final String SECTION_HEADING = "Diritto di Portabilità";
	public static final String TIPOLOGIA = "Tipologia";
	public static final String NUMERO_CLIENTE = "Numero Cliente / Alias";
	public static final String NUMERO_CLIENTE_RES = "Numero Cliente";
	public static final String INDIRIZZO_DI_FORNITURA = "Indirizzo di fornitura";
	public static final String INDIRIZZO_DELLA_FORNITURA = "Indirizzo della fornitura";
	public static final String SECTION2_TEXT1 = "Riceverai entro 30 giorni una email contenente una copia dei tuoi dati.";
	public static final String SECTION2_TEXT2 = "Quando disponibili i tuoi dati saranno anche scaricabili accedendo nuovamente al servizio.";
	public static final String INFO_POPUP_HEADING = "Diritto di portabilità";
	public static final String INFO_POPUP_TEXT = "Hai già effettuato la richiesta. Riceverai una email contenente una copia dei tuoi dati.Quando disponibili i tuoi dati saranno anche scaricabili in questa sezione, e per effettuare una nuova richiesta dovrai attendere qualche giorno.";
	public static final String ATTIZONE_HEADING = "Attenzione";
	public static final String ATTIZONE_TEXT = "Se uscirai dal processo perderai le modifiche effettuate.";
	public static final String URL_INFORMATIVA_PRIVACY = "https://www-coll1.enel.it/it/modifica-profilo";
	public static final String CONDIZIONI_GENERALI = "Condizioni generali";
	public static final String HO_LETTO_TEXT = "Ho letto e accettato:";
	public static final String INFORMATIVA_PRIVACY_TEXT = "Informativa privacy *";
	public static final String SECTION_TEXT = "Operazione eseguita correttamenteRiceverai entro 30 giorni una email contenente una copia dei tuoi dati.Quando disponibili i tuoi dati saranno anche scaricabili accedendo nuovamente al servizio.";
}
