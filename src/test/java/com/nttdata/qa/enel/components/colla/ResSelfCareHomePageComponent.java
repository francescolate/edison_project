package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ResSelfCareHomePageComponent extends BaseComponent {
	public By updatedDateLabel = By.id("datiAggiornati");
	public By welcomeTitleLabel = By.cssSelector("#mainContentWrapper > div.heading > h1");
	public By welcomeSubtitleLabel = By.xpath("//div[@class='heading']//p[text()='In questa sezione potrai gestire le tue forniture']");
	public By supplyTitleLabel = By.cssSelector("#lista-forniture > div.section-heading > h2");
	public By supplySubtitleLabel = By.cssSelector("#lista-forniture > div.section-heading > p:nth-child(3)");
	
	public ResSelfCareHomePageComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyComponentTextStartsWithstring(By objectWithText, String textToCheck) throws Exception {
		verifyComponentVisibility(objectWithText);
//		WebElement element = util.waitAndGetElement(objectWithText,20,1,true);
		WebElement element = driver.findElement(objectWithText);
		String elementText = element.getText();
		if (!elementText.startsWith(textToCheck)) {
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt does not start with:\n" + textToCheck;
			System.out.println(message);
			throw new Exception(message);
		}
	}
	
	public void checkElementsText() throws Exception {
		verifyComponentTextStartsWithstring(updatedDateLabel, updateString);
		By[] locators = {welcomeTitleLabel, welcomeSubtitleLabel, supplyTitleLabel, supplySubtitleLabel};
		verifyElementsArrayText(locators, elementsText);
	}
	
	//----- Constants -----
	
	private final String updateString = "Dati aggiornati al:";
	private final  String[] elementsText = {
			"Benvenuto nella tua area privata",
			"In questa sezione potrai gestire le tue forniture",
			"Le tue forniture",
			"Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti."
	};
	
}
