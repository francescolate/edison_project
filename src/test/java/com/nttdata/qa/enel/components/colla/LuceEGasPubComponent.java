package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LuceEGasPubComponent extends BaseComponent{
	
	public By logoEnel = By.xpath("//img[contains(@class,'logoimg')]");
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By loginSuccessful= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
    public By loginBSNSuccessful= By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
    public By enelPremiaLink = By.xpath("//nav[@id='mainNav']/descendant::a[@id='enelPremia']");
    public By hamburgerLink = By.xpath("//div[@class='dotcom-header__btns']/descendant::span[@class='icon-menu']");
    public By hamburgerEnelPremia = By.xpath("//ul[@class='dotcom-megamenu__links']/parent::div[@class='dotcom-megamenu__accordian__body']//a[contains(text(),'enelpremia WOW!')]");
    
	//footer
	public By copyRight1 = By.xpath("//ul[@class='footer-copyright']/child::li[contains(text(),'Enel Italia')]");
	public By copyRight2 = By.xpath("//ul[@class='footer-copyright']/child::li[contains(text(),'Enel Energia')]");
	public By legaliFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Informazioni Legali']");
	public By creditsFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Credits']");
	public By privacyFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Privacy']");
	public By cookiePolicyFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Cookie Policy']");
	public By remitFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Remit']");
	
	//Header
	public By luceGasHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Luce e gas']");
	public By impresseHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Imprese']");
	public By InsiemeHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Insieme a te']");
	public By storieHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Storie']");
	public By futureHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Futur-e']");
	public By mediaHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Media']");
	public By supportoHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Supporto']");
	
	public By luceHomePath = By.xpath("//a[text()='Home']/ancestor::ul[@class='breadcrumbs component']");
	public By luceTitle = By.xpath("//div[@class='image-hero_content-wrapper']/descendant::h1[text()='Trova la soluzione giusta per te']");
	public By bannerEnelPremia = By.xpath("//div[@class='module_content parbase small-card']/descendant::h3[@class='tile_header text--tile-heading'][contains(text(),'ENELPREMIA WOW!')]");
	
	public By enelPremiaPath = By.xpath("//div[@class='image-hero_content-wrapper']/descendant::ul[@class='breadcrumbs component']");
	public By enelPremiaTitle = By.xpath("//div[@class='image-hero_content-wrapper']/child::h1[contains(text(),'Partecipa a enelpremia')]");
	public By enelPremiaTitle1 = By.xpath("//div[@class='image-hero_content-wrapper']/child::p[contains(text(),'Iscriviti entro')]");
	public By enelPremiaHeader1_2 = By.xpath("//div[@class='image-hero_content-wrapper']/child::h1[contains(text(),'Con ENELPREMIA WOW! è Natale ogni giorno')]");
	public By enelPremiaHeader1 = By.xpath("//div[@class='image-hero_content-wrapper']/child::h1");
	public By enelPremiaHeader2 = By.xpath("//div[@class='image-hero_content-wrapper']/child::p");
	public By enelPremiaButton1 = By.xpath("//div[@class='container-btn-double-cta']/descendant::a[contains(text(),'Accedi')]");
	public By enelPremiaButton = By.xpath("//div[@class='container-btn-double-cta']/descendant::a[contains(text(),'Scopri di più')]");
	public By ogniTitle1 = By.xpath("//div[@class='header-left parbase']/descendant::span[contains(text(),'Ogni settimana una nuova sorpresa')]");
	public By ogniTitel2 = By.xpath("//div[@class='header-left parbase']/descendant::span[contains(text(),'Scopri enelpremia WOW!')]");
	public By diventaCliente = By.xpath("//div[@class='plan-promo_content']/parent::div[@class='hub_inner']//a[text()='Diventa cliente Enel Energia']");
	public By Scarica = By.xpath("//div[@class='plan-promo_content']/parent::div[@class='hub_inner']//a[text()='Scarica il regolamento']");
	
	public By comeFunziona = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//h2[text()='Come funziona ENELPREMIA WOW!']");
	public By scaricaApp = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//h3[contains(text(),'Scarica')]");
	public By scaricaAppContent = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//p[contains(text(),'Sei già nostro cliente')]");
	public By iscriviti = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//h3[contains(text(),'Iscriviti')]");
	public By iScrivitContent = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//p[contains(text(),'L’iscrizione è ')]");
	public By partecipa = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//h3[contains(text(),'Partecipa')]");
	public By partecipaContent = By.xpath("//div[@class='container']/ancestor::div[@class='three-column-band parbase']//p[contains(text(),'Ogni settimana ')]");
	public By allAppStore = By.xpath("(//div[@id='promo_offert_results']/descendant::li//a)[1]");
	public By googlePlayStore = By.xpath("(//div[@id='promo_offert_results']/descendant::li//a)[2]");
	public By guardaIlRegolamento = By.xpath("//div[@class='swiper-container swiper-container-horizontal']/following::span[@class='ico']/preceding-sibling::span[contains(text(),'Guarda il')]");
	public By scopriComeFunziona = By.xpath("//div[@class='swiper-container swiper-container-horizontal']/following::span[@class='ico']/preceding-sibling::span[contains(text(),'Scopri come funziona')]");
	public By enelPremiaContents = By.xpath("//section[@class='content-container content-container-custom']/descendant::section[@data-module='article-text-container']");
	public By regolamento = By.xpath("//div[@class='article-content_inner']/descendant::a[text()='Regolamento']");
	public By download = By.xpath("//div[@class='article-content_inner']/descendant::a[text()='DOWNLOAD']");
	
	public By solaAppHeader = By.xpath("//div[@id='promo_offert_results']/child::h2[contains(text(),'Una sola App')]");
	public By solaAppContents = By.xpath("//div[@id='promo_offert_results']/child::p[contains(text(),'Con l’app Enel Energia')]");
	
	public By scegliHeader = By.xpath("//div[@class='ecps-item-text ecps_item']/descendant::h2[contains(text(),'Scegli')]");
	public By scegliContents = By.xpath("//div[@class='ecps-item-text ecps_item']/descendant::div[@class='rich-text_text text--standard']");
	
	public By alleggerisciHeader = By.xpath("//div[@class='ecps-item-text-item']/descendant::h2[text()='Alleggerisci la bolletta']");
	public By alleggerisciContents = By.xpath("//div[@class='ecps-item-text-item']/descendant::div[@class='rich-text_text text--standard']/p[contains(text(),'Potrai partecipare')]");
	public By alleggerisciContents1 = By.xpath("//div[@class='ecps-item-text-item']/descendant::div[@class='rich-text_text text--standard']/p[contains(text(),'Scarica o accedi')]");
	public By inserisciLink = By.xpath("//div[@class='ecps-item-text-item']/descendant::a[contains(text(),'Inserisci una CTA')]");
	public By partecipaHeader = By.xpath("//div[@class='ecps-item-text-item_wrapper ']/descendant::h2[text()='Partecipa ai concorsi']");
	public By partecipaHeader1 = By.xpath("//div[@class='ecps-item-text-item_wrapper ']/descendant::h2[contains(text(),'Partecipa ai concorsi')]/following-sibling::div[@class='ecps-item-description']");
	public By aumentoHeader = By.xpath("//div[@class='ecps-item-text-item']/following::h2[text()='Aumento di potenza in promozione']");
	public By aumentoContents = By.xpath("//div[@class='ecps-item-text-item']/following::p[contains(text(),'Fino al 30 aprile')]");
	
	public By guardaframeHeader = By.xpath("//div[@class='hub_header_inner']/child::h2[contains(text(),'Guarda dove puoi usare i tuoi')]");
	public By mostraFiltributton = By.xpath("//div[@id='locatorContainer']/descendant::div[text()='MOSTRA FILTRI']");
	public By checkboxHeader = By.xpath("//div[@id='enelMia_productCategories']/child::div[text()='Categorie Merceologiche']");
	public By checkboxLabel = By.xpath("//*[@id='enelMia_productCategories']/descendant::div[@class='customCheckboxLabel']");
	public By bisongnoHeader = By.xpath("//section[@class='anchor module footer-hero-cta ']/descendant::h2[contains(text(),'Hai bisogno di')]");
	public By bisognoTitle = By.xpath("//section[@class='anchor module footer-hero-cta ']/descendant::p[contains(text(),'Leggi le risposte')]");
	public By guardaTutte = By.xpath("//section[@class='anchor module footer-hero-cta ']/descendant::a[contains(text(),'Guarda tutte le risposte')]");
	
	//public String ENELPREMIA_HEADER1 = "I vantaggi continuano con ENELPREMIA WOW!";
	//public String ENELPREMIA_HEADER2 = "Il programma fedeltà continua per un altro anno, fino al 31/10/2021. Se non sei ancora iscritto scopri le sorprese che abbiamo pensato per te. Cosa stai aspettando?";
	public String ENELPREMIA_HEADER1 = "Con ENELPREMIA WOW! è Natale ogni giorno";
	public String ENELPREMIA_HEADER2 = "Partecipa al concorso, vinci uno dei 20 premi Instant Win messi in palio ogni giorno e partecipa all'estrazione finale di cinque TV SAMSUNG QLED 8K 65\" e cinque SAMSUNG Galaxy Z-Flip 5g!";
	public String BISONGNO_HEADER ="Hai bisogno di aiuto?";
	public String BISOGNO_TITLE = "Leggi le risposte alle domande più frequenti";
	public String GUARDA_HEADER = "Guarda dove puoi usare i tuoi coupon";
	public String AUMENTO_HEADER = "Aumento di potenza in promozione";
	public String AUMENTO_CONTENTS = "Fino al 30 aprile se hai bisogno di aumentare la potenza del contatore della tua casa, puoi farlo senza pagare gli oneri commerciali normalmente previsti.";
	public String PARTECIPA_HEADER = "Partecipa ai concorsi";
	public String PARTECIPA_HEADER1 = "Con enelpremia WOW! il tuo anno si riempie di entusiasmo e ricchi premi ti aspettano."
												+"Partecipa al concorso FAI dal 14 marzo al 21 marzo 2019 e prova a vincere!"
												+"Ci sono in palio: 150 carnet da 4 biglietti per l’ingresso nei principali beni FAI e 5 week-end per 2 persone a Matera.";
	
	public String SCEGLI_HEADER = "Scegli i tuoi coupon";
	public String ALLEGGERISCI_HEADER = "Alleggerisci la bolletta";
	
	public String SOLAAPP_HEADER = "Una sola App, tanti vantaggi";
	public String SOLAAPP_CONTENTS = "Con l’app Enel Energia puoi tenere sotto controllo i tuoi consumi e premiarti con il nuovo programma fedeltà ENELPREMIA WOW!";
	public String LUCE_HOMEPATH = "Home / Luce e Gas";
	public String LUCE_TITLE = "Trova la soluzione giusta per te";
	public String ENELPREMIA_PATH = "Home / Luce e Gas / Enelpremia WOW!";
	public String ENELPREMIA_TITLE = "Partecipa a enelpremia WOW!";
	public String ENELPREMIA_TITLE1 = "Iscriviti entro il 30 Aprile al nostro programma fedeltà completamente gratuito, ottieni subito fino a 3 mesi gratis di Infinity.";
	public String OGNI_TITLE1 = "Ogni settimana una nuova sorpresa";
	public String OGNI_TITLE2 = "Scopri enelpremia WOW!, il nuovo programma fedeltà dedicato ai clienti Enel Energia che ti sorprende ogni settimana con un regalo. Collegati subito all’APP di Enel Energia!";
	public String COME_FUNZIONA = "Come funziona ENELPREMIA WOW!";
	public String SCARICA_APP = "Scarica l'app";
	public String SCARICAAPP_CONTENT = "Sei già nostro cliente? Accedi alla sezione ENELPREMIA WOW! dall’Area Clienti (MyEnel) o dall'App di Enel Energia";
	public String ISCRIVITI = "Iscriviti";
	public String ISCRIVITI_CONTENT = "L’iscrizione è completamente gratuita e subito per te un regalo di benvenuto!";
	public String PARTECIPA = "Partecipa";
	public String PARTECIPA_CONTENT = "Ogni settimana per te vantaggiosi coupon sconto e tante iniziative speciali";
	public String ENEL_PREMIA_CONTENTS = "Tutti i clienti di Enel Energia del mercato libero titolari di una fornitura di energia elettrica e/o gas usi domestici possono iscriversi al programma fedeltàcompletamente gratuitoENELPREMIA WOW!accedendo alla sezione dedicata dell'Area Clienti(My Enel)otramite l’App di Enel Energia.Al momento dell'iscrizione tutti i clienti riceveranno uncoupon e punti WOW!di benvenuto. Si avrà inoltre la possibilità di ottenere ogni settimana altri vantaggiosi coupon sconto spendibili presso partner terzi, di partecipare a divertenti concorsi a premio e di accumulare punti WOW!tramite azioni virtuose, che premiano la tua fedeltà e ti permettono di accedere al nostro catalogo per richiedere fantastici premi!Per conoscere tutti i dettagli dell’operazione, leggi il regolamento completo.";
	
	
	public String SCEGLI_CONTENTS = "Con ENELPREMIA WOW! hai a disposizione tantissimeopportunità di risparmio, sta a te scegliere quella che preferisci.Con il nostro programma fedeltàogni settimana, accedendo all’App di Enel Energia o all'Area Clienti, potrai richiedere fino a3 nuovi coupon, da utilizzarequando vuoi o regalarli ad un amico!";
	
	
	public String ALLEGGERISCI_CONTENTS = "Potrai partecipare alle nostre iniziative speciali pensate per premiare la tua fedeltà e ottenere bonus energia sulle tue forniture!";
	public String ALLEGGERISCI_CONTENTS1 = "Scarica o accedi all’APP di Enel Energia per rimanere aggiornato sulle prossime attività.";
	
	
	WebDriver driver;
	SeleniumUtilities util;
	 public LuceEGasPubComponent(WebDriver driver) throws Exception {
//			this.driver = driver;
//			util = new SeleniumUtilities(this.driver);
		 super(driver);
			
		}
	 

//	 public void launchLink(String url) throws Exception {
//			if(!Costanti.WP_BasicAuth_Username.equals(""))
//				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
//			try {
//				driver.manage().window().maximize();
//				driver.navigate().to(url);
//			} catch (Exception e) {
//				throw new Exception("it's impossible the link " + url);
//
//			}
//		}

//		public void clickComponent(By clickableObject) throws Exception {
//			util.objectManager(clickableObject, util.scrollToVisibility, false);
//			Thread.sleep(1000);
//			util.objectManager(clickableObject, util.scrollAndClick);
//		}
		
//		public void switchToFrame(String frameName) throws Exception{
//	        WebDriverWait wait = new WebDriverWait(this.driver, 120);
//	        //LOGGER.log(Level.INFO ,"try to switch to " + frameName);//("try to switch to " + frameName);
//	        WebElement frameToSw = this.driver.findElement(
//	                By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
//	                );
//	        this.driver.switchTo().frame(frameToSw);
//	    }
		
		public void clearText(By object){
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WebElement input = driver.findElement(object);
			input.clear();
		}
		
		public void verifyFooterVisibility() throws Exception {
			By[] locators = {
					legaliFooter,creditsFooter,privacyFooter,cookiePolicyFooter,remitFooter,
										
						};
			String[] footerValue = {"Informazioni Legali","Credits","Privacy","Cookie Policy","Remit"};
			
			for (int i = 0; i < locators.length; i++) {
				String s = driver.findElement(locators[i]).getText();
				//System.out.println(s);
				verifyComponentVisibility(locators[i]);
				String footer= footerValue[i];
				//System.out.println(footer);
				if(!s.equalsIgnoreCase(footer)){
				throw new Exception("Footer tabs are not matching")	;		
				} 

			}
		}

//		public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//					if (!util.verifyExistence(oggettoEsistente, 30)) {
//				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
//			}
//			
//		}
		public void verifyHeaderVisibility() throws Exception {
			By[] locators = {
					luceGasHeader,
					impresseHeader,
					InsiemeHeader,
					storieHeader,
					futureHeader,
					mediaHeader,
					supportoHeader,					
			};
			
			String[] headerValue = {"LUCE E GAS","IMPRESE","INSIEME A TE","STORIE","FUTUR-E","MEDIA","SUPPORTO"};
			
			for (int i = 0; i < locators.length; i++) {
				String s = driver.findElement(locators[i]).getText();
				//System.out.println(s);
				verifyComponentVisibility(locators[i]);
				String Header= headerValue[i];
				//System.out.println(Header);
				if(!s.equalsIgnoreCase(Header)){
				throw new Exception("Header tabs are not matching")	;		
				} 

			}
		}
//		public void SwitchToWindow() throws Exception
//		{
//			Set<String> windows = driver.getWindowHandles();
//			String currentHandle = driver.getWindowHandle();
//			
//			for (String winHandle : windows) {
//			/*	System.out.println(winHandle);
//				System.out.println("switched to "+driver.getTitle()+"  Window");
//				System.out.println(driver.getCurrentUrl());
//			*/	
//		       // String pagetitle = driver.getTitle();
//		        		driver.switchTo().window(winHandle);
//		   /*     System.out.println(winHandle);
//		        System.out.println("After switch"+ driver.getCurrentUrl());
//		        System.out.println(driver.getTitle());
//			*/    
//			 }
//		}
		
		public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}
		public void verifyPDFUrl(String url) throws Exception
		{
			String currentWindow  = driver.getWindowHandle();
			for(String newWindow : driver.getWindowHandles())
			{
			    driver.switchTo().window(newWindow);
			}
			checkUrl(url);
			System.out.println(url);
			driver.close();
			driver.switchTo().window(currentWindow);
			
		}
		
		
		public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
			textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (!util.checkElementText(objectWithText, textToCheck)) {
				WebElement problemElement = driver.findElement(objectWithText);
				String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
				if (elementText.length() < 1) {
					elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
				}
				String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
				System.out.println(message);
				throw new Exception(message);
			}
		}
		public void verifyContent(By object,String text) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
			String value = driver.findElement(object).getText();
			value = value.replaceAll("(\r\n|\n,)","");
			//System.out.println(value);
			if(!text.contentEquals(value))
				throw new Exception("Value is not matching with expected");
			
		}
		public void checkboxLabel(By checkObject) throws Exception {
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				
			String textfield_found = "No";	
			List<WebElement> field = driver.findElements(checkObject);
			
			for(WebElement element : field){
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			//System.out.println(description);
			if (description.contentEquals("Tutti"))
					textfield_found="YES";
			else if(description.contentEquals("Abbigliamento, calzature e accessori"))
				textfield_found="YES";
			else if (description.contentEquals("Alimenti e bevandeo"))
				textfield_found="YES";
			else if (description.contentEquals("Altri Beni e Servizi"))
				textfield_found="YES";
			else if (description.contentEquals("Assicurazioni e Salute"))
				textfield_found="YES";
			else if (description.contentEquals("Bellezza e benessere"))
				textfield_found="YES";
			else if (description.contentEquals("Elettronica"))
				textfield_found="YES";
			else if (description.contentEquals("Mobili, articoli e servizi per la casa"))
				textfield_found="YES";
			else if (description.contentEquals("Tempo libero, cultura e giochi"))
				textfield_found="YES";
			else if (description.contentEquals("Trasporti"))
				textfield_found="YES";
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Checkbox label  not found:");
					   
				}
			
		}
//			public void clickComponentIfExist(By existObject) throws Exception {
//			if (util.exists(existObject, 15)) {
//				util.objectManager(existObject, util.scrollToVisibility, false);
//			    util.objectManager(existObject, util.scrollAndClick);
//			}
//		}
//			public void waitForElementToDisplay (By checkObject) throws Exception{
//				WebDriverWait wait = new WebDriverWait (driver, 30);
//				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
//			}
			
			public void jsClickObject(By by) throws Exception {
				util.jsClickElement(by);
			}
				
		public void enterInput(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void verifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			String textfield_found="NO";
			WebElement element = driver.findElement(checkObject);
			String actualtext = element.getText();
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
		}
		
		public void hanldeFullscreenMessage(By by) throws Exception{
			if(util.verifyExistence(by, 60)){
				util.objectManager(by, util.scrollToVisibility, false);
				util.objectManager(by, util.scrollAndClick);
			}
		}
		
//		public void backBrowser(By checkObject) throws Exception {
//		    try {
//		    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
//		    	driver.navigate().back();
//		    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));    
//		    } catch (Exception e) {
//		        throw new Exception("Previous Page not displayed");
//		    }
//		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
			
			
		}
//	
//		public void checkUrl(String url) throws Exception{
//			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
//			String link=driver.getCurrentUrl();
//			
//			if (url.contains("https://"))
//			{
//				url = url.substring(8);
//			}
//			if (!link.contains(url))
//				 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
//			}
		
		public void pressJavascript(By clickObject) throws Exception {
			WebElement element = util.waitAndGetElement(clickObject);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
		
		

		public String GAURDALINK = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/content/dam/enel-it/enelpremia/documenti-enelpremia/regolamento-enelpremia-wow-proroga.pdf";
		public String PDFLINK = "https://www-coll1.enel.it/content/dam/enel-it/enelpremia/documenti-enelpremia/regolamento-enelpremia-wow-proroga.pdf";
		public static final String PDFLINK1 = "https://www-coll1.enel.it/content/dam/enel-it/enelpremia/documenti-enelpremia/enelpremia_WOW!_regolamento_WEB_161018.pdf";
		public By ques1 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Chi può iscriversi a enelpremia')]");
		public By ans1 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Possono iscriversi a enelpremia')]");
		public static final String  QUES1 = "Chi può iscriversi a enelpremia WOW!?";
		public static final String ANS1 = "Possono iscriversi a enelpremia WOW! tutti i clienti Enel Energia titolari di una fornitura gas e/o elettrica per la casa. Per eventuali esclusioni consulta il regolamento.";
		
		public By ques2 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Come posso iscrivermi')]");
		public By ans2 = By.xpath("//button[contains(text(),'Come posso iscrivermi')]/following-sibling::div");
		public static final String QUES2 = "Come posso iscrivermi?";
		public static final String ANS2 = "Puoi iscriverti attraverso l’apposita sezione dell’app di Enel Energia; Scaricala Subito!È disponibile gratuitamente su Apple Store e Play Store.Ricorda: l’iscrizione deve essere effettuata dal titolare del contratto di fornitura di gas e/o di energia elettrica.";
		
		
		public By ques3 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Ero iscritto ad enelpremia')]");
		public By ans3 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'No, chi ha aderito a enelpremia')]");
		public static final String QUES3 = "Ero iscritto ad enelpremia 3.0. Sono automaticamente iscritto alla nuova edizione enelpremia WOW!";
		public static final String ANS3 ="No, chi ha aderito a enelpremia 3.0 non sarà automaticamente iscritto alla nuova edizione enelpremia WOW!. Iscriviti subito!";
		
		public By ques4 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),' e avevo accorpato ')]");
		public By ans4 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'No, l’“Accorpamento” sarà annullato')]");
		public static final String QUES4 = "Ero iscritto a enelpremia 3.0 e avevo accorpato un codice fedeltà. Tale “Accorpamento” resta valido nella nuova edizione enelpremia WOW!?";
		public static final String ANS4 = "No, l’“Accorpamento” sarà annullato. Il nuovo programma enelpremia WOW! non prevede l’opzione dell’accorpamento.";
		
		public By ques5 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Ho sottoscritto una fornitura')]");
		public By ans5 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Puoi iscriverti appen')]");
		public static final String QUES5 = "Ho sottoscritto una fornitura di gas e/o energia elettrica. Quando posso iscrivermi a enelpremia WOW!?";
		public static final String ANS5 = "Puoi iscriverti appena la tua fornitura risulterà attiva.Per maggiori informazioni consulta il regolamento.";
		
		public By ques6 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Sono titolare di più di una')]");
		public By ans6 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Non è necessario!Iscrivendoti ad')]");
		public static final String QUES6 = "Sono titolare di più di una fornitura di gas e/o energia elettrica. Devo iscrivere al programma ognuna delle mie forniture?";
		public static final String ANS6 = "Non è necessario!Iscrivendoti ad enelpremia WOW!, tutte le tue forniture partecipano automaticamente al programma.";
		
		public By ques7	= By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Sono già iscritto a enelpremia')]");
		public By ans7 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'No, la fornitura risulterà ')]");
		public static final String QUES7 = "Sono già iscritto a enelpremia WOW! con la mia fornitura di gas e/o energia elettrica. Se sottoscrivo una nuova fornitura dovrò iscrivere quest’ultima al programma fedeltà?";
		public static final String ANS7 = "No, la fornitura risulterà automaticamente iscritta a enelpremia WOW!";
		
		public By ques8 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Cos’è il codice fedeltà')]");
		public By ans8 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Il codice fedeltà è un codice')]");
		public static final String QUES8 = "Cos’è il codice fedeltà?";
		public static final String ANS8= "Il codice fedeltà è un codice che ricevi al momento dell’iscrizione. Serve a identificarti in modo univoco e viene utilizzato in ogni comunicazione da parte di enelpremia WOW!, di Enel Energia o delle aziende partner. Puoi trovarlo nella pagina dedicata a enelpremia WOW! della tua area riservata sul enel.it.";
		
		public By ques9	= By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Posso annullare')]");
		public By ans9 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Certo, per annullare la tua iscrizione')]");
		public static final String QUES9 = "Posso annullare l’iscrizione a enelpremia WOW!?";
		public static final String ANS9 = "Certo, per annullare la tua iscrizione al programma scrivi a: Enel Energia Servizio Clienti – Casella Postale 8080 – 85100 Potenza.";
		
		public By ques10 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Quando scade')]");
		public By ans10 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'La scadenza di')]");
		public static final String QUES10 = "Quando scade enelpremia WOW!?";
		public static final String ANS10 = "La scadenza di enelpremia WOW! è fissata al 31 Ottobre 2019.";
		
		public By ques11 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Quando riceverai')]");
		public By ans11 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Il tuo coupon di benvenuto sarà scaricabile')]");
		public static final String QUES11 = "Quando riceverai il tuo coupon di benvenuto?";
		public static final String ANS11 = "Il tuo coupon di benvenuto sarà scaricabile non appena completata la procedura di iscrizione sull’APP di Enel Energia.";
		
		
		public By ques12 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Come posso utilizzare')]");
		public By ans12 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Ogni settimana avrai la possibilità')]");
		public static final String QUES12 = "Come posso utilizzare i coupon settimanali?";
		public static final String ANS12 = "Ogni settimana avrai la possibilità di scegliere uno o più coupon da utilizzare presso uno dei partner commerciali di Enel Energia. I coupon saranno scaricabili dall’APP di Enel Energia per sette giorni, poi verranno sostituiti. Una volta scaricati, saranno disponibili all’interno dell’APP nella sezione “I miei coupon” e potrai utilizzarli fino alla loro scadenza, di volta in volta indicata sui coupon stessi.";
		
		public By ques13 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Posso cedere')]");
		public By ans13 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Sì, una volta scaricati, potrai regalare')]");
		public static final String QUES13 = "Posso cedere i miei coupon?";
		public static final String ANS13 = "Sì, una volta scaricati, potrai regalare i tuoi coupon attraverso la funzione “regala coupon” disponibile all’interno dell’APP Enel Energia.";
		
		public By ques14 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Da quanti')]");
		public By ans14 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Enel Energia e inserendo')]");
		public static final String QUES14 = "Da quanti dispositivi posso accedere ai miei coupon?";
		public static final String ANS14 = "Scaricando l’APP di Enel Energia e inserendo la tua login e la tua password puoi accedere ai tuoi coupon da qualsiasi dispositivo.";
		
	//	public By ques15 = By.xpath("//div[@class='article-content_inner']/descendant::button[contains(text(),'Ho partecipato ad')]");
	//	public By ans15 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Scaricando l’APP di')]");
	//	public static final String QUES15 = "Ho partecipato ad un’attività speciale del programma enelpremia WOW!. A quanto ammonta il bonus energia che ho ottenuto?";
	//	public static final String ANS15 = "Scaricando l’APP di Enel Energia e inserendo la tua login e la tua password puoi accedere ai tuoi coupon da qualsiasi dispositivo.";
		
		public By ques16 = By.xpath("//button[contains(text(),'attività speciale del program')]");
		//public By ans16a = By.xpath("//div[@class='accessibility-drowdown__data']/parent::section[@class='open']/child::div[@class='accessibility-drowdown__data']");
		public By ans16a = By.xpath("//section[@class='open']/descendant::p[contains(text(),'che avrai completato')]");
		public By ans16b = By.xpath("//section[@class='open']/descendant::ul");
		public By ans16c = By.xpath("//section[@class='open']/descendant::p[contains(text(),'Nel caso')]");
		public static final String QUES16 = "Ho partecipato ad un’attività speciale del programma enelpremia WOW!. A quanto ammonta il bonus energia che ho ottenuto?";
		public static final String ANS16a = "Per ogni “Iniziativa Speciale” che avrai completato, riceverai un Bonus Fedeltà di energia elettrica o gas di importo variabile in relazione alla data di attivazione del proprio punto di prelievo (POD/PDR), relativo al tuo contratto di fornitura di energia elettrica o gas stipulato con Enel Energia, come di seguito specificato:";
		public static final String ANS16b = "Fornitura attiva da meno di 1 anno = 1 Bonus Fedeltà del valore di Euro 5,00 oppure"
											+"Fornitura attiva da 1 a 5 anni = 1 Bonus Fedeltà del valore di Euro 10,00 oppure"
											+"Fornitura attiva da più di 5 anni = 1 Bonus Fedeltà del valore di Euro 20,00.";								
		public static final String ANS16c = "Nel caso in cui tu sia titolare di più forniture, riceverai tanti bonus quante sono le forniture a te intestate e attive alla data di completamento dell’attività speciale.";
										
		
		public By ques17 = By.xpath("//button[contains(text(),'Quando riceverò il bonus energia che ho ottenuto g')]");
		public By ans17 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Il o i Bonus Fedeltà')]");
		public static final String QUES17 = "Quando riceverò il bonus energia che ho ottenuto grazie alle attività speciali?";
		public static final String ANS17 = "Il o i Bonus Fedeltà ti saranno attribuiti sulla prima fattura utile emessa dopo il completamento dell’iniziativa speciale o comunque entro e non oltre 180 giorni.";
		
		public By ques18 = By.xpath("//button[contains(text(),'Cosa succede se recedo dal mio contratto di fornit')]");
		public By ans18 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'In caso di cessazione')]");
		public static final String QUES18 = "Cosa succede se recedo dal mio contratto di fornitura gas e/o energia elettrica con Enel Energia prima della data di chiusura del programma?";
		public static final String ANS18 = "In caso di cessazione del/dei contratto/i di fornitura di cui l’iscritto è titolare, il cliente non perderà i vantaggi maturati sino a quel momento qualora aderisca, nel periodo di durata della presente iniziativa ed entro e non oltre il 31.10.19, ad un nuovo contratto di fornitura con Enel Energia che rientri comunque in una delle fattispecie indicate al paragrafo 6 ed effettui una nuova iscrizione al programma in oggetto.";
		
		public By ques19 = By.xpath("//button[contains(text(),'Ho aderito al programma enelpremia WOW!, nel perio')]");
		public By ans19 = By.xpath("//p[contains(text(),'Se non risulterai')]/parent::div[@class='rich-text_text text--standard']");
		public static final String QUES19 = "Ho aderito al programma enelpremia WOW!, nel periodo tra il 7 novembre al 31 dicembre 2018, rientro tra i vincitori dei biglietti per il cinema?";
		public static final String ANS19 = "Se risulteraitra i primi 35.000 iscritti riceverai il tuo premio all’indirizzo e-mail rilasciato in fase di partecipazione il premio.Se non risulterai tra i primi 35.000 iscritti, parteciperai comunque all’estrazione finale di N° 3.000 Voucher Cinema Digitale STARDUST®. L’estrazione avverrà entro il 31/01/19.Entro 10 giorni dall’estrazione finale, se avrai vinto sarai informato all’indirizzo e-mail rilasciato durante la partecipazione e ti verrà spedito il premio al medesimo indirizzo.";
		
		public By ques20 = By.xpath("//button[contains(text(),'Mi avete informato')]");
		public By ans20 = By.xpath("//p[contains(text(),'propri dati personal')]/parent::div[@class='rich-text_text text--standard']");
		public static final String QUES20 = "Mi avete informato di essere tra i vincitori, quando otterrò la vincita?";
		public static final String ANS20 = "Entro 10 giorni dall’estrazione finale verrai informato all’indirizzo e-mail rilasciato in fase di partecipazione e ti verrà richiesto di spedire via e-mail, entro 14 giorni dall’invio della comunicazione di vincita (farà fede la mail di comunicazione vincita) i propri dati personali (nome, cognome, indirizzo, telefono, cellulare, e-mail) e la fotocopia fronte e retro della tua carta di identità.Si precisa che la convalida della vincita potrà essere confermata solo una volta avvenuta la ricezione dei documenti richiesti, che dovranno pervenire comunque entro i termini stabiliti e si precisa inoltre che in caso di non corrispondenza tra i dati il premio non potrà essere assegnato.";
		
		public By ques21 = By.xpath("//button[contains(text(),'Voglio esercitare')]");
		public By ans21 = By.xpath("//div[@class='article-content_inner']/descendant::p[contains(text(),'Per tutte le questioni')]");
		public static final String QUES21 = "Voglio esercitare i miei diritti sulla Protezione dei Dati personali, come posso fare?";
		public static final String ANS21 = "Per tutte le questioni relative al trattamento dei dati personali e all’esercizio dei diritti puoi inviare una email all’indirizzodpo.enelenergia@enel.com.";
		
}


	


