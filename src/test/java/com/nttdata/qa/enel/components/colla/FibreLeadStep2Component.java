package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FibreLeadStep2Component extends BaseComponent {
	public By pagePath1 = By.cssSelector("#main > section > div > nav > ul > li:nth-child(1) > a");
	public By pagePath2 = By.cssSelector("#main > section > div > nav > ul > li:nth-child(2) > a");
	public By pageTitle = By.cssSelector("#main > section > div > div > h1");
	public By pageSubTitle = By.cssSelector("#main > section > div > p");
	public By descrHeader = By.xpath("//h1[starts-with(@id,'titleLead') and not(contains(@class,'hide'))]");
	public By description = By.xpath("//div[starts-with(@id,'messageTitleLead') and not(contains(@class,'hide'))]");
	
	private By[] locators = {pagePath1, pagePath2, pageTitle, pageSubTitle, descrHeader, description};
	
	public FibreLeadStep2Component(WebDriver driver) {
		super(driver);
	}
	
	public void checkPageElements() throws Exception {
		ArrayList<String> theStrings = new ArrayList<String>(Arrays.asList(commonStrings));
		theStrings.add(descriptionString);
		String[] theStringsArray = theStrings.toArray(new String[theStrings.size()]);
		verifyElementsArrayText(locators, theStringsArray);
	}
	
	public void checkPageElementsForInvalidAddress(String address) throws Exception {
		String completeDescription = invalidAddressDescr.replace("#", address);
		ArrayList<String> theStrings = new ArrayList<String>(Arrays.asList(commonStrings));
		theStrings.add(completeDescription);
		verifyElementsArrayText(locators, theStrings.toArray(new String[theStrings.size()]));
	}
	
	// ----- Constants -----
	
	private final String[] commonStrings = {
			"HOME",
			"FIBRA STEP2",
			"Melita offre la fibra ai clienti Enel Energia",
			"Restiamo in contatto",
			"Restiamo in contatto!"
	};
	private final String descriptionString = "Se non trovi il tuo indirizzo di fornitura significa che non è ancora raggiunto dal servizio fibra offerto da Melita. Se vuoi sapere quando il servizio fibra di Melita sarà disponibile presso il tuo indirizzo di fornitura, compila il form e sarai ricontattato.\n" + 
		"Se invece vuoi provare con un altro indirizzo, torna alla pagina precedente per la verifica della copertura.";
	private final String invalidAddressDescr = "L'indirizzo # non è ancora raggiunto dal servizio fibra di Melita.\n" + 
			"Se vuoi sapere quando il servizio fibra di Melita sarà disponibile presso il tuo indirizzo di fornitura, compila il form e ti ricontatteremo. Se vuoi provare con un altro indirizzo, torna alla pagina precedente per la verifica della copertura.";
}
