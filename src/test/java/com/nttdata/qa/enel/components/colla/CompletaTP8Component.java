package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CompletaTP8Component  {
	WebDriver driver;
	SeleniumUtilities util;
//		public By checkboxTrattamenti=By.xpath("//input[@id='ITA_IFM_Type_Legal_Form__c']");
//		public By checkboxTrattamenti=By.xpath("//*[@id='ITA_IFM_Type_Legal_Form__c']");
//		public By checkboxTrattamenti=By.xpath("//input[@id='ITA_IFM_Type_Legal_Form__c']");
//		public By annulla=By.xpath("//button[@id='tp-cancel-data']");
//		public By conferma=By.xpath("//button[@id='tp-confirm-data']");
//		public By menuTendina=By.xpath("//span[@id='selectAnnullamentoOffertaSelectBoxItText']");
//		public By valoreMenu=By.xpath("//ul[@id='selectAnnullamentoOffertaSelectBoxItOptions']");
//		public By ConfAnnulla=By.xpath("//button[@id='annullaOfferta']");
//		public By sezioneModalitaSottoscrizione=By.xpath("//h3[contains(text(),'Modalit')and contains(text(),'di Sottoscrizione')]");
//		public By modalitaSottoscrizioneSi=By.xpath("//input[@id='Yes-Question-1']");
//		public By pressoSpazioEnelPartner=By.xpath("//input[@id='SpaziEnelPartner']");
// Campi seconda pagina per eventuali verifiche sul contenuto (non richiesto dalla TL)
		//div[contains(text(),'Titolarità')]/../..//div[2]     //Proprietario
		//div[contains(text(),'Comune Amministrativo')]/../..//div[2]  //Arcinazzo Romano
		//div[contains(text(),'Comune Catastale')]/../..//div[2]  //Arcinazzo Romano
		//div[contains(text(),'Codice Comune Catastale')]/../..//div[2]  //A370
		//div[contains(text(),'Tipo Unità')]/../..//div[2]  //Fabbricati
		//div[contains(text(),'Sezione')]/../..//div[2]  //20
		//div[contains(text(),'Foglio')]/../..//div[2]  //1
		//div[contains(text(),'Subalterno')]/../..//div[2]  //2
		//div[contains(text(),'Particella')]/../..//div[2]  //3
		//div[contains(text(),'Estensione Particella')]/../..//div[2]  //10
		//div[contains(text(),'Tipo Particella')]/../..//div[2]  //Edificabile
// Nuovi xpath per TP8		
		public By checkboxProprietario=By.xpath("//input[@id='dati_catastali_choice1']");
		public By comuneAmministrativo=By.xpath("//input[@id='comuneAmministrativo']");
		public By comuneCatastale=By.xpath("//input[@id='comuneCatastale']");
		public By codice_comune_catastale=By.xpath("//input[@id='codice_comune_catastale']");
		public By menuTipoUnita=By.xpath("//span[@id='tipo_unitaSelectBoxItText']");
		public By sezione=By.xpath("//input[@id='sezione']");
		public By foglio=By.xpath("//input[@id='foglio']");
		public By subalterno=By.xpath("//input[@id='subalterno']");
		public By particella=By.xpath("//input[@id='particella']");
		public By estensione_particella=By.xpath("//input[@id='estensione_particella']");
		public By menuTipoParticella=By.xpath("//span[@id='tipo_particellaSelectBoxItText']");
		public By prosegui=By.xpath("//button[@id='tp8-next']");
		public By si=By.xpath("//button[@id='agEntrate-yes']");
		public By conferma=By.xpath("//*[@class=' ext-strict']//*[@id='tp8-confirm']");
		public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
		// Gestione messaggio "Ops"
		public By msgOps = By.xpath("//*[contains(text(),'Ops')]");
		public By buttonChiudiOps = By.xpath("//*[contains(text(),'Ops')]/..//button");
		public By buttonClose = By.xpath("//button[@id='tp-close-cross']");
		public By buttonNoOps = By.xpath("(//button[@id='btn-tp-no'])[4]");
		
		
	public CompletaTP8Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);

	}
	
	public void ControllaOps() throws Exception {
        if (this.getElementExistance(msgOps)) {
            clickComponent(buttonChiudiOps);
         }
        if (this.getElementExistance(buttonClose)) {
        	clickComponent(buttonClose);
        	clickComponent(buttonNoOps);
         }
	}
	
    public boolean getElementExistance(By element) throws Exception {
        return util.verifyExistence(element, 30);
    }

	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}

	public void inserisciValore(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	
}
