package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaDisattivazioneFornituraComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By rucuperaPassword = By.xpath("//div[@class='login_recovers']/descendant::a[text()='Recupera Password']");
    public By recuperaUsername = By.xpath("//div[@class='login_recovers']/descendant::a[text()='Recupera Username']");
    public By recuperaHeader = By.xpath("//div[@id='recovery-password']/descendant::h1[contains(text(),'Recupera')]");
    public By recuperaHeader1 = By.xpath("//div[@class='cmPageTitleCont']/following-sibling::div[@class='cmPageSubtitleCont media']//p[contains(text(),'Inserisci')]");
    public By emaiLabel = By.xpath("//input[@id='rec-email']/preceding-sibling::label[contains(text(),'Email')]");
    public By emailInput = By.xpath("//div[@id='formRecoverEmail']/descendant::input[@id='rec-email']");
    public By inviaBtn = By.xpath("//div[@id='formRecoverEmail']/descendant::a[text()='Invia']");
    public By emailInputError = By.xpath("//div[@id='formRecoverEmail']/descendant::span[text()='Campo obbligatorio']");
    public By rucuperaUserNameTitle = By.xpath("//div[@id='recovery-username-section']/descendant::h1[text()='Recupera Username']");
    public By rucuperaUserNameTitle1 = By.xpath("//div[@id='recovery-username-section']/descendant::p[contains(text(),'Inserisci il')]");
    public By rucuperaUsernameLabel = By.xpath("//div[@id='formRecoverUsernameCF']/descendant::label[text()='Codice Fiscale']");
    public By rucuperaUsernameInput = By.xpath("//input[@id='rec-username-fiscal-code'];");
    public By rucuperUserNameErrorMsg = By.xpath("//input[@id='rec-username-fiscal-code']/following-sibling::div[@id='errore_fiscal-code']//span[text()='Campo obbligatorio']");
    public By rucuperUserNameButton = By.xpath("//input[@id='rec-username-fiscal-code']/following::a[@id='rec-username-button']");
    public By registerButton = By.xpath("//a[@href='/it/registrazione']");
    public By registerHeader = By.xpath("//div[@class='cmPageTitleCont']/child::div[contains(text(),'SCOPRI IL')]");
    public By registerHeader1 = By.xpath("//div[@class='cmPageTitleCont']/child::h1[contains(text(),'Registrati')]");
    public By problemidiLink = By.xpath("//div[@class='login-block']/ancestor::div[@class='login-details']/descendant::a[contains(text(),'Problemi di')]");
    public By problemHeader = By.xpath("//main[@id='main']/descendant::ul[@class='breadcrumbs component']");
    public By problemHeader1 = By.xpath("//main[@id='main']/descendant::h1[contains(text(),'Non riesco')]");
    public By offerList = By.xpath("//div[@id='enelool']/child::div[@id='tlm_grid']");
    public By loginErrorMsg = By.xpath("//input[@id='txtLoginUsername']/following-sibling::p[text()='Username obbligatoria']");
    public By passwordErrorMsg = By.xpath("//button[@class='view-password show']/following-sibling::p[text()='Password obbligatoria']");
    public By invalidInputErrorMsg = By.xpath("//input[@id='txtLoginUsername']/following-sibling::p[text()='Username non valida']");
    public By invalidPasswordErrorMsg = By.xpath("//main[@id='main']/descendant::p[contains(text(),'Username o password errata')]");
    public By PrivatoResCustomerButton = By.xpath("//h4[contains(text(),'Area Clienti Casa')]/following-sibling::p[@class='margin-top-60']//a[text()='Entra']");
	public By areaClientiImpressa = By.xpath("//h4[contains(text(),'Area Clienti Impresa')]/following-sibling::p/child::a[text()='Entra']");
	public By areaClientiCasa = By.xpath("//h4[contains(text(),'Area Clienti Casa')]/following-sibling::p/child::a[text()='Entra']");
	public By pageTitle = By.xpath("//h1[text()='Disattivazione Fornitura']");
	public By pageText = By.xpath("//div[@class='heading']");
	public By proseguiconladisattivazioneButton = By.xpath("//button[contains(text(),'PROSEGUI CON LA DISATTIVAZIONE')]");
	public By titleSubText = By.xpath("//h2[contains(text(),'Seleziona una o ')]");
	public By podIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='cliente']//span[text()='POD']");
	public By indrizzodellafornituraIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[text()='Indirizzo della fornitura']");
	public By modalitadirichiestaIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[text()='Modalità di richiesta']");
	public By informationIconIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By modalitadirichiestaValueIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[text()='Numero Verde o Negozio Enel']");
	public By popupTitle = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//h3[text()='Attenzione!']");
	public By popupContent = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@id='modalAlert']");
	public By cliccaquiLink = By.xpath("//a[text()='CLICCA QUI']");
	public By pageTitle1 = By.xpath("//div[@class='scrollable outerScroller uiScrollerWrapper cITA_IFM_LCP451_SelfCareStyle']//h1[text()='Disattivazione Fornitura']");
	public By path = By.xpath("//ul[@class='breadcrumbs component']");
	public By trovaLoTitle = By.xpath("//span[text()='Trova lo Spazio Enel più vicino a te']");
	public By popupClose = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']//span[@class='dsc-icon-close-rounded']");
	public By domandeFrequenti = By.xpath("//h3[text()='Domande frequenti']");
	public By confermaPopupHeader = By.xpath("//h3[@id='titleModal']");
	public By confermaPopupContent = By.xpath("//p[contains(text(),'Procedendo si avvierà')]");
	
	public static final String CONFERMAPOPUP_CONTENT = "Procedendo si avvierà l'effettiva cessazione della fornitura.";
	public By pdr01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='cliente']//span[text()='PDR']");
	public By indrizzodellafornitura01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[text()='Indirizzo della fornitura']");
	public By modalitadirichiesta01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[text()='Modalità di richiesta']");
	public By informationIcon01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By modalitadirichiestaValue01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[text()='Numero Verde o Negozio Enel']");
	
	public By podIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='cliente']//span[text()='POD']");
	public By indrizzodellafornituraIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[text()='Indirizzo della fornitura']");
	public By modalitadirichiestaIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[text()='Modalità di richiesta']");
	public By informationIconIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By modalitadirichiestaValueIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[text()='Numero verde o Negozio Enel']");
	
		
	public By enelEnergiaFooter = By.xpath("//footer[@id='footer']/descendant::li[contains(text(),'Enel Energia')]");
	public By tuttiDirittiFooter = By.xpath("//footer[@id='footer']/descendant::li[contains(text(),'Tutti i Diritti')]");
	public By ivaFooter = By.xpath("//footer[@id='footer']/descendant::li[contains(text(),'IVA ')]");
	public By legalFooter = By.xpath("//footer[@id='footer']/descendant::a[contains(text(),'Informazioni')]");
	public By privacyFooter = By.xpath("//footer[@id='footer']/descendant::a[contains(text(),'Privacy')]");
	public By creditsFooter = By.xpath("//footer[@id='footer']/descendant::a[contains(text(),'Credits')]");
	public By contactFooter = By.xpath("//footer[@id='footer']/descendant::a[contains(text(),'Contattaci')]");
	
	//header	
	public By luceGasHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Luce e gas']");
	public By impresseHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Imprese']");
	public By InsiemeHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Insieme a te']");
	public By storieHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Storie']");
	public By futureHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Futur-e']");
	public By mediaHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Media']");
	public By supportoHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Supporto']");
	
	public By BenvenutoTitle = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Benvenuto nella')]");
	public By BenvenutoContent = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'In questa')]");
	public By fornitureHeader = By.xpath("//section[@id='lista-forniture']/descendant::h2[contains(text(),'Le tue forniture')]");
	public By fornitureContent = By.xpath("//section[@id='lista-forniture']/descendant::p[contains(text(),'Visualizza le informazioni')]");
	
	public By serviziLink = By.xpath("//nav[@id='mainNav']/descendant::span[contains(text(),'Servizi')]");
	public By serviziFornitureTitle = By.xpath("//div[@class='section-heading']/parent::section[@id='forniture-id0']/descendant::h2[contains(text(),'Servizi per le')]");
	public By serviziFornitureContent = By.xpath("//div[@class='section-heading']/parent::section[@id='forniture-id0']/descendant::p[contains(text(),'Di seguito potrai')]");
	public By serviziBolletteTitle = By.xpath("//div[@class='section-heading']/parent::section[@id='forniture-id1']/descendant::h2[contains(text(),'Servizi per le bollette')]");
	public By serviziBolletteContent = By.xpath("//div[@class='section-heading']/parent::section[@id='forniture-id1']/descendant::p[contains(text(),'Di seguito potrai')]");
	public By serviziContratoTitle = By.xpath("//div[@class='section-heading']/parent::section[@id='forniture-id2']/descendant::h2[contains(text(),'Servizi per il contratto')]");
	public By serviziContratoContent = By.xpath("//div[@class='section-heading']/parent::section[@id='forniture-id2']/descendant::p[contains(text(),'Di seguito potrai visualizzare tutti')]");
	
	public By disattivazioneFornitura = By.xpath("//ul[@class='tileWrapper']/descendant::h3[text()='Disattivazione Fornitura']");
	
	public By disattivazioneHeader = By.xpath("//section[@id='landing-text']/descendant::h1[contains(text(),'Disattivazione Fornitura')]");
	public By disattivazioneContent1 = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'La disattivazione')]");
	public By disattivazioneContent2 = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'La richiesta di')]");
	public By disattivazioneContent3 = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'Disattiva comodamente')]");
	
	public By domandeHeader = By.xpath("//section[@id='landing-accordion']/child::h3[contains(text(),'Domande frequenti')]");
	
	public By proseguiButton = By.xpath("//div[@id='landing-dispositiva']/child::button[text()='PROSEGUI CON LA DISATTIVAZIONE']");
		
	public By disattivazioneTitle = By.xpath("//div[@class='list-form-container']/descendant::p[contains(text(),'Ti ricordiamo che:')]");
	public By disattivazioneTitle1 = By.xpath("//div[@class='list-form-container']/descendant::ul[contains(text(),'')]");
	public By disattivazioneTitle2 = By.xpath("//div[@class='list-form-container']/descendant::p[contains(text(),'Se il tuo distributore')]");
	
	public By indrizzoLabelValue = By.xpath("//div[@class='label-container gas max-size']/descendant::span[@class='indirizzo']");
	public By indrizzoLabelValueNew = By.xpath("//div[@class='label-container luce max-size']/descendant::span[@class='indirizzo']");
	public By pdrContentLabelValue = By.xpath("//div[@class='label-container gas max-size']/descendant::span[contains(text(),'PDR')]/parent::span[@class='cliente']");
	public By pdrContentLabelValueNew = By.xpath("//div[@class='label-container luce max-size']/descendant::span[contains(text(),'POD')]/parent::span[@class='cliente']");
	public By indrizzoLabelValue2 = By.xpath("//div[@class='label-container luce']/descendant::span[@class='indirizzo']");
	public By podContentLabelValue = By.xpath("//div[@class='label-container luce']/descendant::span[contains(text(),'POD')]/parent::span[@class='cliente']");
	public By modalistaLabelValue1 = By.xpath("//div[@class='label-container gas max-size']/descendant::span[contains(text(),'Modalità di richiesta')]/parent::span[@class='cliente']");
	public By modalistaLabelValue1New = By.xpath("//div[@class='label-container luce max-size']/descendant::span[contains(text(),'Modalità di richiesta')]/parent::span[@class='cliente']");
	public By modalistaLabelValue2 = By.xpath("//div[@class='label-container luce']/descendant::span[contains(text(),'Modalità di richiesta')]/parent::span[@class='cliente']");
	public By modalistaLabelValue1Updated = By.xpath("//div[@class='label-container gas max-size']/descendant::span[contains(text(),'Modalità di richiesta')]/parent::span[@class='cliente']/span");
	public By modalistaLabelValue2Updated = By.xpath("//div[@class='label-container luce']/descendant::span[contains(text(),'Modalità di richiesta')]/parent::span[@class='cliente']/span");
	
	
	public By iconGas = By.xpath("//span[@class='visually-hidden' and contains(text(),'gas')]");
	public By iconLuce = By.xpath("//span[@class='visually-hidden' and contains(text(),'luce')]");
	public By iconElectricity = By.xpath("//span[@class='icon-line-electricity']");
	public By iconFlame = By.xpath("//span[@class='icon-line-flame']");
	public By iconEye = By.xpath("//span[@class='icon-info-circle']");
	public By IconCheckBox = By.xpath("//span[@class='icon-ckbox']");
	
	public By checkbox = By.xpath("//span[@class='icon-ckbox']");
	public By checkbox1 = By.xpath("//span[@id='checkbox-item-0']");
	public By checkbox2 = By.xpath("//span[@id='checkbox-item-1']");
	public By checkbox6	= By.xpath("//span[@id='checkbox-item-6']");
	public By checkBox35 = By.xpath("//span[@id='checkbox-item-35']");
	public By checkBox36 = By.xpath("//span[@id='checkbox-item-36']");
	public By nonAttivoSupplyCheckBox = By.xpath("(//span[contains(@id,'checkbox-item-')])[1]");
	public By linkEsci = By.xpath("//span[text()='Esci']/parent::a[@id='disconnetti']");
	public By buttonEsci = By.xpath("//div[@class='button-container']/child::button[@class='button_second']");
	public By disattivazioneButton = By.xpath("//div[@class='button-container']/child::button[@id='nextButton']");
	public By disattivazioneButtonDisabled = By.xpath("//span[text()='Disattivazione']/parent::button");
	public By supplyCheckbox = By.xpath("//span[@class='icon-ckbox']");
	public By inserimentoDati = By.xpath("//div[@id='sc']//span[.='Inserimento dati']");
	public By riepilogoeConfermaDati =  By.xpath("//div[@id='content']//span[.='Riepilogo e conferma dati']");
	public By email = By.xpath("//label[@id='emailField']");
	public By cellulareLabel = By.xpath("//label[@id='mobileField']");
	public By cellularevalue = By.xpath("//input[@id='mobileFieldInput']");
	public By confirmCellulareLabel = By.xpath("//label[@id='mobileFieldConfirm']");
	public By confirmCellulareValue = By.xpath("//input[@id='mobileFieldConfirmInput']");
	
	public By stepMenu = By.xpath("//div[@class='wizard']/descendant::ul[@class='stepper']");
	public By invioCommuni = By.xpath("//section[@id='formWrapper']/descendant::h4[contains(text(),'Invio comunicazioni')]");
	public By invioCommnicContent = By.xpath("//section[@id='formWrapper']/descendant::p[contains(text(),'Di seguito puoi')]");
	public By inputEmail = By.xpath("//label[text()='Email*']/following-sibling::input[@id='form-email']");
	public By confermaEmail = By.xpath("//label[text()='Conferma email*']/following-sibling::input[@id='form-email2']");
	public By desideriQues = By.xpath("//div[@class='form-group']/child::p[contains(text(),'Desideri ricevere anche')]");
	public By radioBtnSI = By.xpath("//label[text()='Si']/preceding-sibling::input[@id='tip-email']");
	public By radioBtnNO = By.xpath("//label[contains(text(),'indirizzo postale')]/preceding-sibling::input[@id='tip-indirizzo']");
	public By radionBtNoLabel = By.xpath("//input[@id='tip-indirizzo']/following::label[contains(text(),'su indirizzo postale')]");
	public By dataDis = By.xpath("//div[@id='form-data-div-sfa']/child::h4[contains(text(),'Data Disattivazione')]");
	public By dataDisContent = By.xpath("//div[@id='form-data-div-sfa']/child::p[contains(text(),'Se il tuo distributore')]");
	public By aiutaciTitle = By.xpath("//h4[contains(text(),'Aiutaci a migliorare')]");
	public By aiutaciContent= By.xpath("//h4[contains(text(),'Aiutaci a migliorare')]/following::p[contains(text(),'Per offrire un servizio')]");
	public By indietroButton = By.xpath("//span[text()='INDIETRO']/parent::button[@id='backButton_step1']");
	public By continuaButton = By.xpath("//span[text()='Continua']/parent::button[@id='nextButton_step1']");
	public By indrizzoDella = By.xpath("//span[text()='Indirizzo della fornitura']/ancestor::div[@class='label-container luce max-size']/descendant::span[@class='indirizzo']");
	public By numeroCliente = By.xpath("//span[contains(text(),'Numero cliente')]/ancestor::div[@class='label-container luce max-size']/descendant::span[@class='cliente']");
	public By confermaButton  = By.xpath("//span[text()='Conferma']/parent::button[@id='nextButton_step2']");
	public By indetroBtn = By.xpath("//span[text()='INDIETRO']/parent::button[@id='backButton_step2']");
	public By frequentQuestions = By.xpath("//section[@id='landing-accordion']/descendant::ul[1]/child::li");
	
	public By ques1PlusIcon = By.xpath("//button[@id='accordion-button-item-accordion-0']/child::span[@class='dsc-icon-line-plus']");
	public By ques2PlusIcon = By.xpath("//button[@id='accordion-button-item-accordion-1']/child::span[@class='dsc-icon-line-plus']");
	public By ques3PlusIcon = By.xpath("//button[@id='accordion-button-item-accordion-2']/child::span[@class='dsc-icon-line-plus']");
	public By ques4PlusIcon = By.xpath("//button[@id='accordion-button-item-accordion-3']/child::span[@class='dsc-icon-line-plus']");
	public By ques5PlusIcon = By.xpath("//button[@id='accordion-button-item-accordion-4']/child::span[@class='dsc-icon-line-plus']");
	public By ques6PlusIcon = By.xpath("//button[@id='accordion-button-item-accordion-5']/child::span[@class='dsc-icon-line-plus']");
	
	public By ans1 = By.xpath("//p[contains(text(),'I tempi massimi per')]/parent::div[@id='accordion-panel-item-accordion-0']");
	public By ans2 = By.xpath("//p[contains(text(),'Il servizio di prenotazione')]/parent::div[@id='accordion-panel-item-accordion-1']");
	public By ans3 = By.xpath("//p[contains(text(),'Il costo per la disattivazione')]/parent::div[@id='accordion-panel-item-accordion-2']");
	public By ans4 = By.xpath("//p[contains(text(),'Ad avvenuta cessazione riceverai')]/parent::div[@id='accordion-panel-item-accordion-3']");
	public By ans5 = By.xpath("//p[contains(text(),'La funzionalità è disponibile per')]/parent::div[@id='accordion-panel-item-accordion-4']");
	public By ans6 = By.xpath("//p[contains(text(),'La richiesta di disattivazione può')]/parent::div[@id='accordion-panel-item-accordion-5']");
	
	public static final String ANS1 = "I tempi massimi per l’evasione della tua richiesta sono:LUCE: fino a 7 giorni lavorativi.GAS: fino a 9 giorni lavorativi.Il recesso dal contratto con enel energia avrà efficacia entro 30gg dalla data di richiesta.";
	public static final String ANS2 = "Il servizio di prenotazione è messo a disposizione dal distributore e-distribuzione per le forniture elettriche, erogate da qualsiasi venditore.Puoi verificare il tuo distributore consultando la tua ultima bolletta ricevuta.";
	public static final String ANS3 = "Il costo per la disattivazione della fornitura, che sarà addebitato nell'ultima bolletta che riceverai, è pari a €23 + Iva oltre a eventuali costi fissi applicati dal distributore.";
	public static final String ANS4 = "Ad avvenuta cessazione riceverai una mail contenente il codice che ti permetterà di utilizzare il bonus per l'attivazione di una nuova fornitura sul sito enel.it.Il bonus è valido per 1 anno dalla data di disattivazione della fornitura per la sottoscrizione di un contratto di luce o gas relativo a prima attivazione, voltura, subentro o cambio fornitore su tutte le offerte di Enel Energia, ad esclusione di Sempre con te, Sempre con te Gas, E-Light ed E-Light gas. Il bonus non è cumulabile con altre promozioni in corso.Al tuo rientro in Enel Energia ti verranno accreditati 25 euro ripartiti nelle prime bollette relative alla nuova fornitura.";
	public static final String ANS5 = "La funzionalità è disponibile per tutte le forniture ad uso domestico, tranne per quelle non disalimentabili (es. alimentazione di apparecchi elettromedicali per mantenimento in vita) e per quelle in cui è necessario la rimozione del contatore.";
	public static final String ANS6 = "La richiesta di disattivazione può essere effettuata soltanto dal cliente titolare del contratto di fornitura nei seguenti modi:Accedendo al servizio attraverso il form di autenticazione presente in questa pagina.Recandosi presso uno dei nostri negozi Enel.Chiamando il nostro call center al numero 800.900.860.";
	
	public By selectDate = By.xpath("//div[@class='input-container']/child::input[@id='form-datepicker1']");
	public By datePicker = By.xpath("//div[@class='input-container']/child::a[@class='icon-calendar-alt']");
	
	public By date = By.xpath("//table[@class='ui-datepicker-calendar']/descendant::a[@href='#']");
	public By nextMonth = By.xpath("//div[@id='form-datepicker']/descendant::a[@title='Next']");
	
	public By motivoDella = By.xpath("//label[contains(text(),'Motivo della disattivazione')]");
	public By descrizione = By.xpath("//label[@class='accessibleButton']/following-sibling::textarea[@id='descrivi-motivo']");
	public By haiRichiesto = By.xpath("//h4[contains(text(),'Hai richiesto la disattivazione del servizio per l')]");
	
	public By ConfermaPopupAttenzione = By.xpath("//div[@id='modalAlert']/descendant::h3[@id='titleModal']");
	public By ConfermaPopupContent = By.xpath("//div[@id='modalAlert']/descendant::p[contains(text(),'Procedendo si')]");
	public By popupIndietroButton = By.xpath("//div[@class='button-container']/child::button[@id='overlayNoButton']");
	public By popupProseguiButton = By.xpath("//div[@class='button-container']/child::button[@id='overlayYesButton']");
	public By popupCheckboxes = By.xpath("//div[@id='modalAlert']/descendant::p[contains(text(),'Non è possibile procedere')]");
		
	public By operazioneHeader = By.xpath("//h3[@id='id_title' and contains(text(),'Operazione eseguita correttamente')]");
	public By operazioneContents = By.xpath("//p[contains(text(),'La tua richiesta è stata presa in carico Il proces')]");
	public By operazioneContents1 = By.xpath("//p[contains(text(),'La tua richiesta')]");
	public By fineButton = By.xpath("//span[text()='Fine']/parent::button[@id='finishButton_step3']");
	
	public By mandatoryErrorMsg = By.xpath("//section[@id='formWrapper']/descendant::span[contains(text(),'obbligatorio')]");
	public By popup1 = By.xpath("//div[@id='modalAlert']//span[@class='dsc-icon-close-rounded']");
	
	
	public By luceIndrizzoSupplies = By.xpath("//span[@class='icon-line-electricity']/parent::span[@class='flex']//span[@class='indirizzo']");
	public By luceNumeroSupplies = By.xpath("//span[@class='icon-line-electricity']/parent::span[@class='flex']//span[@class='cliente']");
	public String getEmail = "//div[@class='content-group recap']/child::p[text()='#']";
	public By procedendo = By.xpath("//div[@class='form-wrapper']/child::p[contains(text(),'Procedendo con')]");
	public By riceverai = By.xpath("//div[@class='form-wrapper']/descendant::p[contains(text(),'Riceverai la bolletta')]");
	public By bollettaHeader = By.xpath("//div[@class='content-group recap']/child::h4[contains(text(),'Bolletta di chiusura')]");
	public By suppliesHeader = By.xpath("//fieldset[@class='list-container']/parent::div[@class='content-group grey-bg']/child::h4[contains(text(),'Hai richiesto la disattivazione')]");
	
	public By bolletteLink = By.xpath("//nav[@id='mainNav']/descendant::span[contains(text(),'Bollette')]");
	public By bollettePopupTitle = By.xpath("//h3[@id='titleModal' and text()='Attenzione']");
	public By bollettePopupContent = By.xpath("//h3[@id='titleModal']/following::p[contains(text(),'Sei sicuro di')]");
	public By bollettePopupSIButton = By.xpath("//button[@id='overlayYesButton']//span[text()='SI']");
	public By bollettePopupNoButton = By.xpath("//button[@id='overlayNoButton']//span[text()='NO']");
	public By bollettePopupNoBtn = By.xpath("//span[text()='NO']/parent::button[@id='overlayNoButton']");
	public By bollettePopupSIBtn = By.xpath("//span[text()='SI']/parent::button[@id='overlayYesButton']");
	public By bollettePoupupClose = By.xpath("//button[@id='overlayNoButton']/ancestor::div[@id='modalAlert']/descendant::h3/following-sibling::button[@class='modal-close inline-icon-link']");
	
	public By letueBollette = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Le tue bollette')]");
	public By letueContent = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'Qui')]");
	
	
	//244
	public By homePageBSNPath = By.xpath("//li[text()='Homepage']/parent::ol[@class='breadcrumb']");
	public By headerBSN = By.xpath("//div[@class='panel-body']/descendant::h1[contains(text(),'Benvenuto nella tua area')]");
	public By fornitureHeaderBSN = By.xpath("//*[contains(text(),'Forniture e bollette')]");
	public By fornitureNumber = By.xpath("//*[contains(text(),'888895230')]");
	public By fornitureLuce = By.xpath("//div[@id='heading-Index']/child::h4[@class='panel-title']");
	

	public By fornituraDettaglio = By.xpath("//h5[contains(text(),'La tua fornitura nel dettaglio.')]");
	public By luogo = By.xpath("//b[contains(text(),'Luogo')]");
	public By luogoValue = By.xpath("//*[contains(text(),'Via Gramsci 29')]");
	public By tipologia = By.xpath("(//b[contains(text(),'Tipologia:')])[1]");
	public By offerta = By.xpath("//b[contains(text(),'Offerta:')]");
	public By offeraValue = By.xpath("//*[contains(text(),'Vantaggio Impresa')]");
	public By stato = By.xpath("//b[contains(text(),'Stato:')]");
	public By statoValue = By.xpath("//span[contains(text(),'Attivata')]");
	
	//245
	public By fornituraAggiungi = By.xpath("(//*[contains(text(),'Aggiungi nome fornitura')])[1]");
	public By LuogoValue_92019 = By.xpath("//*[contains(text(),'Via De Gasperi A 7 9201')]");
	public By offertaLuce = By.xpath("//*[contains(text(),' Senza Orari Luce')]");
	public By piuInformationLink = By.xpath("//a[contains(text(),'Più informazioni')]");
	public By tuttoHeader = By.xpath("//ol[@class='breadcrumb']/child::li[@class='forniture active']/following::h1[contains(text(),'Tutto quello che')]");
	public By tuttoContent = By.xpath("//ol[@class='breadcrumb']/child::li[@class='forniture active']/following::p[@id='subtitleFornitura']");
	public By cliente = By.xpath("//*[contains(text(),'Cliente:')]/parent::span");
	public By codiceFiscale = By.xpath("//*[contains(text(),'Codice fiscale:')]/parent::span");
	public By indrizzoFornitura = By.xpath("//*[contains(text(),'Indirizzo fornitura:')]/parent::span");
	public By tipologiaElectrico = By.xpath("//*[contains(text(),'Tipologia:')]/parent::span");
	public By modalitaEmail = By.xpath("//*[contains(text(),'Modalità di ricezione:')]/parent::span");
	public By modalitaPostale = By.xpath("//*[contains(text(),'Modalità di pagamento:')]/parent::span");
	
	public By homePageLink = By.xpath("//a[contains(text(),'HomePage')]/parent::li[@id='homeMenu']");
	public By bolletteBSNLink = By.xpath("//a[contains(text(),'bollette')]/parent::li[@id='menuBoll']");
	public By vaiAllBollette = By.xpath("(//div[@class='row row-btns text-right']/child::div[@class='col-xs-12 ']//a[contains(@name,'elenco bollette')])[1]");
	public By billColumns = By.xpath("//div[@id='table-wrapper']/descendant::th");
	
	//246 
	public By fornitura_300232254 = By.xpath("//input[@id='nome-fornitura']");
	public By fornituraLuce_300232254 = By.xpath("(//*[contains(text(),'300232254')])[1]");
	public By LugoValue_3430 = By.xpath("//*[contains(text(),' VIA NUOVA ESTENSE 3430 - 41053 MARANELLO MO')]");
	public By offerta_Sempre = By.xpath("//*[contains(text(),'Sempre Con')]");
	public By bolletteBillColumns = By.xpath("//div[@class='table-responsive hidden-xxs']/descendant::th");
	public By bolleHeader = By.xpath("//div[@id='tue_bollette']/descendant::h5[contains(text(),'Le tue bollette')]");
	public By letueBolletteHeader = By.xpath("//h1[contains(text(),'Le tue Bollette')]");
	public By letueBolletteContent = By.xpath("//p[contains(text(),'Se cerchi una bolletta')]");
	public By mostraFiltriButton = By.xpath("//span[text()='Mostra']/parent::a[@name='Filtri']");
	public By esportaExcelButton = By.xpath("//div[@class='title']/child::a[@name='Esporta in Excel']");
	
	//236
	public By indrizzodella = By.xpath("//span[@class='indirizzo']/child::span[contains(text(),'Indirizzo della fornitura')]");
	public By indrizooDellaValue = By.xpath("//span[@class='indirizzo']/child::span[contains(text(),'Via Sbarre Superiori Diramazione')]");
	public By POD_IT001E78718844 = By.xpath("//span[@class='valore']/descendant::span[contains(text(),'IT001E78718844')]");
	
	
	//310 
	public By attivaBollettaWebButton = By.xpath("//span[text()='ATTIVA BOLLETTA WEB']/parent::button[@class='button_first']");
	public By modificaIndrizzoEmailButton = By.xpath("//span[text()='MODIFICA INDIRIZZO EMAIL']/parent::button[@class='button_first']");
	public By bollettaWebLink = By.xpath("//h3[text()='Bolletta Web']/ancestor::li[@class='tile']/child::a");
	public By supplyContinuaButton = By.xpath("//span[text()='CONTINUA']/parent::button[@class='button_first']");
	public By attivazioneHeader = By.xpath("//h1[contains(text(),'Attivazione') and @id='h1_bollettaweb']");
	public By attivazioneContent = By.xpath("//h2[contains(text(),'Seleziona una o più')]");
	public By statusContinuaButton = By.xpath("//span[text()='Continua']/parent::button[@class='button_first']");
	public By statusIndietroButton = By.xpath("//span[text()='INDIETRO']/parent::button[@class='button_second']");
	public By fornitureLink = By.xpath("//span[text()='Forniture']/ancestor::a[@id='idFornitura']");
	public By popupHeader= By.xpath("//h3[@id='titleModal' and contains(text(),'Attenzione')]");
	public By popupContent1  = By.xpath("//p[contains(text(),'Sei sicuro di voler')]");
	public By popupSIButton = By.xpath("//span[text()='SI']/parent::button[@id='overlayYesButton']");
	public By popupNoButton = By.xpath("//span[text()='NO']/parent::button[@id='overlayNoButton']");
	public By bollettaWebHeader = By.xpath("//h2[contains(text(),'Bolletta Web')]");
	public By bollettaWebContent = By.xpath("//p[contains(text(),'Il servizio Bolletta Web consente di ricevere')]");
	public By modificaBollettaEmailInput = By.xpath("//label[@id='emailField']/following-sibling::div[@class='input-container']/child::input[@id='emailFieldInput']");
	public By modificaBollettaConfermaEmail = By.xpath("//label[@id='emailFieldConfirm']/following-sibling::div[@class='input-container']/child::input[@id='emailFieldConfirmInput']");
	public By modificaBollettaNumeroInput = By.xpath("//label[@id='mobileField']/following-sibling::div[@class='input-container']/child::input[@id='mobileFieldInput']");
	public By modificaBollettaNumeroConferma = By.xpath("//label[@id='mobileFieldConfirm']/following-sibling::div[@class='input-container']/child::input[@id='mobileFieldConfirmInput']");
	public By modificaBollettaCheckboxInput = By.xpath("//input[@id='checkBoxAggiorna i dati anagrafici']");
	public By modificaBollettaCheckboxLabel = By.xpath("//input[@id='checkBoxAggiorna i dati anagrafici']/following-sibling::label[contains(text(),'Aggiorna i miei')]");
	public By BollettanumerodicellulareLabel = By.xpath("//dt[contains(text(),'Numero di Cellulare')]");
	public By bollettaNumeroCellulareValue = By.xpath("//dd[contains(text(),'3687152807')]");
	public By bollettaStatusContent = By.xpath("//h4[contains(text(),'Bolletta Web verrà')]");
	public By ModificaBollettaConfermaBtn = By.xpath("//span[contains(text(),'Conferma')]/parent::button[@class='button_first']");
	public By modificaBollettaFineBtn = By.xpath("//span[contains(text(),'Fine')]/parent::button[@class='button_first']");
	
	public static final String BOLLETTA_CELLULARELABEL = "Numero di Cellulare";
	public static final String BOLLETTA_CONFIRMCELLULARELABEL = "Conferma numero di cellulare*";
	public static final String BOLLETTA_CELLULAREVALUE = "3687152807";
		
	public static final String BOLLETTASTATUS_CONTENT = "Il servizio di Bolletta Web verrà attivato su:";
	public static final String POPUPHEADER = "Attenzione";
	public static final String BOLLETTAWEB_CONTENT = "Il servizio Bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica di emissione tramite SMS.";
	public static final String BOLLETTAWEB_HEADER = "Bolletta Web";
	public static final String POPUPCONTENT1 = "Sei sicuro di voler uscire da questa sezione?";
	public static final String ATTIVAZIONE_HEADER = "Attivazione Bolletta Web";
	public static final String ATTIVAZIONE_CONTENT = "Seleziona una o più forniture su cui intendi";
	public static final String ATTIVAZIONE_CONTENT1 = "Seleziona una o più forniture su cui intendi attivare il servizio di Bolletta Web.";
	public static final String INDRIZZODELLA_LABEL = "Indirizzo della fornitura";
	public static final String INDRIZZODELLA_VALUE = "Via Sbarre Superiori Diramazione Lomba 81 A 89132 Reggio Calabria Reggio Calabria Rc";
	public static final String PODVALUE_IT001E78718844 = "IT001E78718844";
	public static final String LETUEBOLLETTEHEADER = "Le tue Bollette";
	public static final String LETUEBOLLETTECONTENT = "Se cerchi una bolletta in particolare usa i filtri per trovarla più facilmente.";
	public static final String OFFERTA_SEMPRE = "Offerta: Sempre Con Te Impresa";
	public static final String LUGOVALUE_3430 = "Luogo: VIA NUOVA ESTENSE 3430 - 41053 MARANELLO MO";
	public static final String FORNITURA_300232254  = "300232254";
	public static final String FORNITURALUCE_300232254 = "Fornitura Luce Cliente N°: 300232254";
	public static final String MODALITA_POSTALE = "Modalità di pagamento:Bollettino Postale";
	public static final String MODALITA_EMAIL = "Modalità di ricezione: Mail";
	public static final String TIPOLOGIA_ELECTRICO = "Tipologia:Elettrico";
	public static final String INDRIZZOFORNITURA = "Indirizzo fornitura: Via De Gasperi A 7 92019 Sciacca Ag";
	public static final String CODICEFISCALE = "Codice fiscale: CMMGNN81B03C351L";
	public static final String CLIENTE = "Cliente: Giovanni Cammarata";
	public static final String TUTTO_HEADER = "Tutto quello che c'è da sapere sulla tua fornitura";
	public static final String TUTTO_CONTENT = "In questa sezione visualizzi i dettagli della tua fornitura e le relative bollette.";
	public static final String OFFERTA_LUCE = "Offerta: Senza Orari Luce";
	public static final String LUOGOVALUE_92019 = "Luogo: Via De Gasperi A 7 92019 Sciacca Ag";
	public static final String HOMEPAGE_BSNPATH = "Area riservataHomepage";
	public static final String HEADER_BSN = "Benvenuto nella tua area dedicata Business";
	public static final String FORNITUREHEADER_BSN = "Forniture e bollette";
	public static final String FORNITURE_NUMBER = "888895230";
	public static final String FORNITURE_LUCE= "888895230Fornitura Luce Cliente N °: 888895230";
	public static final String FORNITURA_DETTAGLIO = "La tua fornitura nel dettaglio.";
	public static final String LUOGO = "Luogo:";
	public static final String LUOGO_VALUE = "Luogo: Via Gramsci 29 7 13040 Palazzolo Vercellese Vc";
	public static final String TIPOLOGIA = "Tipologia:";
	public static final String OFFERTA = "Offerta:";
	public static final String OFFERTA_VALUE = "Offerta: Vantaggio Impresa";
	public static final String STATO = "Stato:";
	public static final String STATO_VALUE = "Stato: Attivata";
	
	public static final String MODALISTALABEL_VALUENew = "ModalitÃ  di richiesta";
    public static final String MODALISTALABEL_VALUE2New = "ModalitÃ  di richiesta";
	public static final String FORNITURA_AGGIUNGI = "Aggiungi nome fornitura";
	public static final String FORNITURALUCE_AGGIUNGI = "Aggiungi nome fornituraFornitura Luce Cliente N °: 713338992";
	public static final String INSERIMENTo_DATI ="Inserimento dati";
	public static final String RIEPILOGO_CONFERMA_DATI="Riepilogo e conferma dati";
	public static final String CELLULARE ="3459089078";
	
	public PrivateAreaDisattivazioneFornituraComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void checkboxesSelect(int start, int end) throws Exception {
				
		for (int i=start; i<=end; i++){
			//String s = By.xpath("//span[@id='checkbox-item-"+i+"']").toString();
			//System.out.println(s);
			By checkbox = By.xpath("//span[@id='checkbox-item-"+i+"']");
			WebElement we = driver.findElement(checkbox);
			Thread.sleep(2000);
			we.click();	
			
			}
	}
	
	public void iconClick() throws Exception {
		List<WebElement> icon = driver.findElements(By.xpath("//span[@class='icon-container']/preceding-sibling::a"));
		System.out.println(icon.size());
		for(int i = 0 ; i <= icon.size() ; i++){
		WebElement ie = icon.get(i);
		System.out.println(i);
		
		
		//By eyeIcon = By.xpath("//span[@id='"+i+"']");
		//String s = icon.toString();
		//System.out.println(s);
	/*	
		WebElement we = driver.findElement(icon);
		WebElement popup = driver.findElement(popup1);
			if (we.isDisplayed())
			we.click();
			Thread.sleep(3000);
			popup.click();	
		*/	
			}	
			
		}
	
		public void luceClickCheckbox() throws Exception {
		
		List<WebElement> Electricity = driver.findElements(iconElectricity);
		List<WebElement> checkBoxes = driver.findElements(IconCheckBox);
		int checkboxesCnt = 0;
		int sizeElex = Electricity.size();
		int sizeCheckbox = checkBoxes.size();
		System.out.println(sizeElex);
		System.out.println(sizeCheckbox);
						
			for(int i=0 ; i<sizeElex ; i++){
			Thread.sleep(2000);
			boolean checkStatus = checkBoxes.get(i).isDisplayed();
			boolean  ElexIconStatus = Electricity.get(i).isDisplayed();
			//String s = checkBoxes.get(i).toString();
			//By b = By.xpath(s);
			//System.out.println(checkStatus);
			//System.out.println(ElexIconStatus);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			if(checkStatus==true && ElexIconStatus == true)
			//System.out.println(s);
			executor.executeScript("arguments[0].click();", checkBoxes.get(i));
			     checkboxesCnt++;
				 System.out.println(checkboxesCnt);
				 if(checkboxesCnt>=5)
					 break;	 
					}
				
		}
		public void selectMultiCheckbox(int num) throws Exception {
			
			List<WebElement> Electricity = driver.findElements(iconElectricity);
			List<WebElement> checkBoxes = driver.findElements(IconCheckBox);
			int checkboxesCnt = 0;
			int sizeElex = Electricity.size();
			int sizeCheckbox = checkBoxes.size();
			System.out.println(sizeElex);
			System.out.println(sizeCheckbox);
							
				for(int i=0 ; i<=num ; i++){
				Thread.sleep(2000);
				boolean checkStatus = checkBoxes.get(i).isDisplayed();
				boolean  ElexIconStatus = Electricity.get(i).isDisplayed();
				//String s = checkBoxes.get(i).toString();
				//By b = By.xpath(s);
				//System.out.println(checkStatus);
				//System.out.println(ElexIconStatus);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				if(checkStatus==true && ElexIconStatus == true)
				
				//System.out.println(s);
				executor.executeScript("arguments[0].click();", checkBoxes.get(i));
				     checkboxesCnt++;
					 System.out.println(checkboxesCnt);
					 if(checkboxesCnt>=num)
						 break;	 
						}			
			}
		
	
		public void flameClickEyeIcon() throws Exception {
		
		List<WebElement> flame = driver.findElements(iconFlame);
		List<WebElement> eye = driver.findElements(iconEye);
		
		int eyeIcon = 0;
		int sizeFlame = flame.size();
		int sizeEye = eye.size();
		System.out.println(sizeFlame);
		System.out.println(sizeEye);
		for(int i=0 ; i<sizeFlame ; i++){
			Thread.sleep(2000);
			boolean iconEye = flame.get(i).isDisplayed();
			boolean flameIconStatus = eye.get(i).isDisplayed();
			System.out.println(iconEye);
			System.out.println(flameIconStatus);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			if(iconEye==true && flameIconStatus == true)
				//eye.get(i).click();
				executor.executeScript("arguments[0].click();", eye.get(i));
				Thread.sleep(3000);
				//WebElement popupClose = driver.findElement(By.xpath("//span[@class='dsc-icon-close-rounded']/ancestor::div[@id='modalAlert']/descendant::button[@class='modal-close inline-icon-link']"));
				WebElement pc = driver.findElement(By.xpath("//div[@id='modalAlert']//span[@class='dsc-icon-close-rounded']"));	
			   pc.click();
			   eyeIcon++;
			   System.out.println(eyeIcon);
					}
			}
	
	public void selectDatePlus(int days) throws Exception {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/d");  
		LocalDate now = LocalDate.now();
	    String today1 = dtf.format(now);
	    String[] split = today1.split("/");
	    int todayDate = Integer.parseInt(split[2]);
	     	     
	     String date1 = dtf.format(now.plusDays(days));
	     String[] dateSplit = date1.split("/");
	     String year 	 = dateSplit[0];
	     String month 	 = dateSplit[1];
	     String dateplus = dateSplit[2];
	       	     
			 if(todayDate>20){
				 
			   driver.findElement(By.xpath("//div[@id='form-datepicker']/descendant::a[@title='Next']")).click();
			   Thread.sleep(5000);
			   List<WebElement> Ndates = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']/descendant::a[@href='#']"));
			  
			   for(int i = 0; i<Ndates.size(); i++) {
			   WebElement el = Ndates.get(i);
	     
			   String getDates = Ndates.get(i).getText();
	      	  if(getDates.equals(dateplus)) {
	    		  el.click();
	    		  break;
			   		}		
			   	 }
			 }  
			 
			 else {
		   
		   List<WebElement> dates = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']/descendant::a[@href='#']"));
		   System.out.println(dates.size());
			 for(int j=0; j<dates.size() ; j++) {
				 String getDate = dates.get(j).getText();
				 Thread.sleep(1000);
	      		  if(getDate.equals(dateplus)) {
	      			  Thread.sleep(1000);
	      			  WebElement el1 = dates.get(j); 
	  	    		  el1.click();
	  	    		  break;
	    		  			 }
			 
			 	    }	
	   		} 
	}	
	public void launchLink(String url) throws Exception {
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("Unable to open the link " + url);

		}
	}
	
	public void  todayNextDate() throws Exception {
	  
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/d");  
		 LocalDate now = LocalDate.now();
		 String todayDate = dtf.format(now.plusDays(1));
		 System.out.println(todayDate);  
	     
	     String[] dateSplit = todayDate.split("/");
	     String year 	 = dateSplit[0];
	     String month 	 = dateSplit[1];
	     String dateNext = dateSplit[2];
	     
	     List<WebElement> dates = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']/descendant::a[@href='#']"));
		   System.out.println(dates.size());
			 for(int i=0; i<dates.size() ; i++) {
	    	String getDate = dates.get(i).getText();
	    		Thread.sleep(2000);
	      		if(getDate.equals(dateNext)) {
	  	    	Thread.sleep(3000);
	  	    		WebElement el1 = dates.get(i); 
	  	    		  el1.click();
	  	    		  break;
	    		  			 }
			 		}	
		}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void selectOptions(String option) throws Exception {
		
		String[] optionValue = {"Scegli il motivo","CAMBIO CASA","INUTILIZZO", "USO STAGIONALE", "ALTRO", "PREFERISCO NON RISPONDERE"};
		
		   WebElement dropdown = driver.findElement(By.xpath("//div[@class='slds-form-element dummySelect accessibleButton']/descendant::select[@class='slds-select']"));
		   String value = "No";
				 
		   Select select = new Select(dropdown); 
		   List<WebElement> options = select.getOptions(); 
		                   
		    for (int i=0; i<options.size(); i++){
		    	Thread.sleep(4000);
		    String opt = options.get(i).getText();
		    System.out.println("option value " +opt);
		    
		      if (opt.equals(optionValue[i])){
		      value = "Yes";		    			 
		      System.out.println("dropdown values matching");
		        		}
		                          
		      else {
		      throw new Exception("dropdown values are not matching");
                   }
		     		     
		    } 
		    select.selectByVisibleText(option);
		}
	
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
	
	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
				if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("The Object with xpath " + oggettoEsistente + " doesnot exists.");
		}
		
	}
	public void verifyHeaderVisibility() throws Exception {
		By[] locators = {
				luceGasHeader,
				impresseHeader,
				InsiemeHeader,
				storieHeader,
				futureHeader,
				mediaHeader,
				supportoHeader,					
		};
		
		String[] headerValue = {"LUCE E GAS","IMPRESE","INSIEME A TE","STORIE","FUTUR-E","MEDIA","SUPPORTO"};
		
		for (int i = 0; i < locators.length; i++) {
			String s = driver.findElement(locators[i]).getText();
			//System.out.println(s);
			verifyComponentVisibility(locators[i]);
			String Header= headerValue[i];
			//System.out.println(Header);
			if(!s.equalsIgnoreCase(Header)){
			throw new Exception("Header values are not matching")	;		
			} 
					}
	}
	public void verifyFooterVisibility() throws Exception {
		By[] locators = {
				enelEnergiaFooter,tuttiDirittiFooter,ivaFooter,legalFooter,privacyFooter,creditsFooter,contactFooter
									
		};
		String[] footerValue = {"© Enel Energia S.p.a.","Tutti i Diritti Riservati","Gruppo IVA Enel P.IVA 15844561009","Informazioni Legali","Privacy","Credits","Contattaci"};
		
		for (int i = 0; i < locators.length; i++) {
			String s = driver.findElement(locators[i]).getText();
			//System.out.println(s);
			verifyComponentVisibility(locators[i]);
			String footer= footerValue[i];
			//System.out.println(footer);
			if(!s.equalsIgnoreCase(footer)){
			throw new Exception("Footer values are not matching")	;		
			} 

		}
	}

	public void SwitchToWindow() throws Exception
	{
		Set<String> newWindows = driver.getWindowHandles();
		String currentWindow = driver.getWindowHandle();
		
		for (String winHandle : newWindows) {
		/*	System.out.println(winHandle);
			System.out.println("switched to "+driver.getTitle()+"  Window");
			System.out.println(driver.getCurrentUrl());
		*/	
	       // String pagetitle = driver.getTitle();
	        driver.switchTo().window(winHandle);
	   /*     System.out.println(winHandle);
	        System.out.println("After switch"+ driver.getCurrentUrl());
	        System.out.println(driver.getTitle());
		*/    
		 }
	}
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			//System.out.println(message);
			throw new Exception(message);
		}
	}
	public void verifyContent(By object,String text) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
		String value = driver.findElement(object).getText();
		value = value.replaceAll("(\r\n|\n,)","");
		if(!text.contentEquals(value))
			throw new Exception("Value is not matching with expected");
		
	}
	public By verifyGetEmail(String email) throws Exception{
		By get = By.xpath(getEmail.replace("#", email));
		System.out.println(get);
		return get;
	}

	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	public void waitForElementToDisplay (By checkObject) throws Exception{
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
		public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}
			
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void verifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		String textfield_found="NO";
		WebElement element = driver.findElement(checkObject);
		String actualtext = element.getText();
		System.out.println(actualtext);
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void backBrowser(By checkObject) throws Exception {
	    try {
	    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    	driver.navigate().back();
	    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                
	    } catch (Exception e) {
	        throw new Exception("Previous Page not displayed");
	    }
	}
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch.The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
		
		
	}

	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void confirmUrl(String theUrl) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		if (!wait.until(ExpectedConditions.urlToBe(theUrl))) {
			String current = driver.getCurrentUrl();
			System.out.println(current);
			throw new Exception("Browser did not load required url: " + theUrl + "\nCurrent url: " + current);
		}
	}
	public void pressJavascript(By clickObject) throws Exception {
		WebElement element = util.waitAndGetElement(clickObject);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}
	
	public void getContent(By by,boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("WebPage Content:\n"+weText);
				
	}
	public void Questions(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
		List<WebElement> field = driver.findElements(checkObject);
		
		for(WebElement questions : field){
		String description=questions.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
			
		if(description.contentEquals("Quali sono i tempi per disattivare la fornitura?"))
			textfield_found="YES";
		else if (description.contentEquals("E' possibile prenotare una data di disattivazione?"))
			textfield_found="YES";
		else if (description.contentEquals("Quali sono i costi per disattivare la fornitura?"))
			textfield_found="YES";
		else if (description.contentEquals("Come posso utilizzare il bonus?"))
			textfield_found="YES";
		else if (description.contentEquals("Chi può disattivare una fornitura online?"))
			textfield_found="YES";
		else if (description.contentEquals("Attraverso quali canali posso disattivare la fornitura?"))
			textfield_found="YES";
			
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Questions are not displayed");
		}
	}
	
	public void verifyBillColumn(By columnObject) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(columnObject));
						
		String textfield_found="NO";
		List<WebElement> field = driver.findElements(columnObject);
		
		for(WebElement questions : field){
		String description=questions.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		System.out.println(description);
			
		if(description.contentEquals("Tipo"))
			textfield_found="YES";
		else if (description.contentEquals("N. Bolletta"))
			textfield_found="YES";
		else if (description.contentEquals("Scadenza"))
			textfield_found="YES";
		else if (description.contentEquals("Stato"))
			textfield_found="YES";
		else if (description.contentEquals("Importo"))
			textfield_found="YES";
		else if (description.contentEquals("Indirizzo fornitura"))
			textfield_found="YES";
			
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Bill columns are not displayed");
		}
	}
	public void verifyBolletteBillColumn(By columnObject) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(columnObject));
						
		String textfield_found="NO";
		List<WebElement> field = driver.findElements(columnObject);
		
		for(WebElement questions : field){
		String description=questions.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		System.out.println(description);
			
		if(description.contentEquals("Tipo"))
			textfield_found="YES";
		else if (description.contentEquals("Numero"))
			textfield_found="YES";
		else if (description.contentEquals("Importo"))
			textfield_found="YES";
		else if (description.contentEquals("Stato"))
			textfield_found="YES";
		else if (description.contentEquals("Scadenza"))
			textfield_found="YES";
	
			
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Bill columns are not displayed");
		}
	}
	
	public void verifyGasSupply(By GasList) throws Exception
	{
		
		String res="NO";
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(GasList));
		
		List<WebElement> eleIcons = driver.findElements(By.xpath("//*[contains(@class,'label-container')]//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']"));
		List<WebElement> chkbx = driver.findElements(By.xpath("//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox']"));
		
		if (eleIcons.size()>1 && chkbx.size()>1) {
			res="YES";
			
			for (int i = 1; i <= eleIcons.size(); i++) {
				
				verifyComponentExistence((By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity'])["+i+"]")));  // ElectricityIcon
				verifyComponentExistence(By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox'])["+i+"]")); //Checkbox
				
				//verifyCheckboxnotSelect(By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox'])["+i+"]")); 
			    
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Indir')])["+i+"]"));  //Indir
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Via')])["+i+"]"));  //Via
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'POD')])["+i+"]"));  //POD
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span//span[contains(text(),'IT00')])["+i+"]"));  //IT00
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Modalit')])["+i+"]"));  //Modalit
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Web')])["+i+"]"));  //Web
				
				clickComponent(By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox'])["+i+"]")); // Click on Checkbox
				
			}
		} 
		
		if (res.contentEquals("NO"))
			throw new Exception("The expected count Luce or ELE supply is not greater > 1");
		
	}
	public void LuceIndrizzoNumero(By indrizzo,By numero) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(indrizzo));
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(numero));
		String textfield_found="NO";
		List<WebElement> ind = driver.findElements(indrizzo);
		List<WebElement> num = driver.findElements(numero);
		
		for(int i=0;i<ind.size();i++){
			
		String indriz= ind.get(i).getText();
		String numer = num.get(i).getText();
		
		indriz= indriz.replaceAll("(\r\n|\n)", " ");
		numer = numer.replaceAll("(\r\n|\n)", " ");
		
			 if(indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310501884"))
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310503319"))
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310499886")) 
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310512606"))
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310499471"))
			textfield_found="YES";
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Indrizzo label and values are not displayed");
			
		}
	}
	
	public void LuceIndrizzoNumeroNew(By indrizzo,By numero) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(indrizzo));
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(numero));
		String textfield_found="NO";
		List<WebElement> ind = driver.findElements(indrizzo);
		List<WebElement> num = driver.findElements(numero);
		
		for(int i=0;i<ind.size();i++){
			
		String indriz= ind.get(i).getText();
		String numer = num.get(i).getText();
		
		indriz= indriz.replaceAll("(\r\n|\n)", " ");
		numer = numer.replaceAll("(\r\n|\n)", " ");
		
			 if(indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310524139"))
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Via Nizza 12 60126 Ancona Ancona An") && numer.contentEquals("Numero cliente 310524152"))
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310499886")) 
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310512606"))
			textfield_found="YES";
		else if (indriz.contentEquals("Indirizzo della fornitura Via Nizza 152 00198 Roma Roma Rm") && numer.contentEquals("Numero cliente 310499471"))
			textfield_found="YES";
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Indrizzo label and values are not displayed");
			
		}
	}
	public void checkTextHighlight(By object, String color) throws Exception{
        ColorUtils c = new ColorUtils();
        String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
//        System.out.println(colore);
        if(!colore.contentEquals(color)) throw new Exception("l'oggetto web  e' di colore "+color+". Colore trovato : "+colore);
    }
	
	
	public void verifyStatus() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 Inserimento dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
		// Validating the color of highlighted step
		checkTextHighlight(By.xpath("//li[contains(@class,'step')]/descendant::span[@class='step-label active']"), servicesColor);
	}
	public static final String servicesColor = "Crimson";
	
	
	
	public void verifyDomandeQuesAns() throws Exception{
		
		String result = "NO";
		
		for(int i=0;i<6;i++){
			
			verifyComponentExistence((By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']")));
			verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));
			
			String question = driver.findElement(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']")).getText();
			
			if (domandeQUES.contains(question)) {
				
				WebElement plusIcon = driver.findElement(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));
				plusIcon.isDisplayed();		
				clickComponent(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));//plus
				Thread.sleep(3000);
				String ans = driver.findElement(By.xpath("//div[@id='accordion-panel-item-accordion-"+i+"']/*")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(domandeANS.contains(ans)){		
					result="YES";	
					WebElement minusIcon = driver.findElement(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-minus']"));
					verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-minus']"));
					minusIcon.isDisplayed();
				}
				
					if(result.equals("No"))throw new Exception ("Domande answer is not as expected");
				
			}else {
				
				throw new Exception ("Domande Frequenti Section Faq is not as expected");
			}
			
			
		}
		
		
		
	}
	public By disattivaContent = By.xpath("//h1[@id='h1_disattivazione']/following-sibling::h2[contains(text(),'Seleziona una')]");
	public static final String LETUE_BOLLETTE = "Le tue bollette";
	public static final String LETUE_CONTENT = "Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.";
 	public static final String PageText = "Disattivazione Fornitura La disattivazione della fornitura comporta la chiusura del contatore (è definita tecnicamente “disdetta con suggello”). Viene effettuata sigillando il contatore in modo che non possa essere utilizzato fino a che non venga attivato un nuovo contratto. La richiesta di disattivazione della fornitura rappresenta la manifestazione della volontà di recedere dal contratto di fornitura. Disattiva comodamente la tua fornitura ad uso abitativo dall’area riservata e avrai diritto a un bonus di 25€ per la sottoscrizione di un nuovo contratto con Enel Energia dal sito enel.it entro un anno dalla disattivazione della fornitura."; 
 	public static final String DISATTIVA_CONTENT = "Seleziona una o più forniture che intendi disattivare"; 
 	public static final String ModalitadirichiestaValue = "Numero Verde o Negozio Enel";
 	public static final String PopupContent = "è stata aperta la modale informativa Attenzione! Chiudi La fornitura selezionata non può essere disattivata online perchè risulta 'non disalimentabile' (es. presenza di macchina salvavita connesse a ragioni di sopravvivenza) Per ricevere maggiori informazioni: Contattaci al numero verde 800.900.860, attivo dalle 7.00 alle 22.00 tutti i giorni, dal Lunedì alla Domenica, escluse le festività nazionali Recati al Negozio Enel più vicino a te. CLICCA QUI per scoprire le nostre sedi.";
 	public static final String TrovaText = "Trova lo Spazio Enel più vicino a te";
 	public static final String PopupContent01613101028000 = "è stata aperta la modale informativa Attenzione! Chiudi La fornitura selezionata non può essere disattivata online perché è presente un indirizzo di fornitura errato. Per ricevere maggiori informazioni: Contattaci al numero verde 800.900.860, attivo dalle 7.00 alle 22.00 tutti i giorni, dal Lunedì alla Domenica, escluse le festività nazionali Recati al Negozio Enel più vicino a te. CLICCA QUI per scoprire le nostre sedi.";
 	public static final String ModalitadirichiestaValue1 = "Numero verde o Negozio Enel";
 	public static final String PopupContentIT002E8058051A = "è stata aperta la modale informativa Attenzione! Chiudi La fornitura selezionata non può essere disattivata perché esistono altri processi avviati che risultano incompatibili con la disattivazione. Per ricevere maggiori informazioni: Contattaci al numero verde 800.900.860, attivo dalle 7.00 alle 22.00 tutti i giorni, dal Lunedì alla Domenica, escluse le festività nazionali Recati al Negozio Enel più vicino a te. CLICCA QUI per scoprire le nostre sedi.";
 	
 	public static final String BENVENUTO_TITLE = "Benvenuto nella tua area privata";
	public static final String BENVENUTO_CONTENT = "In questa sezione potrai gestire le tue forniture";
	public static final String FORNITURE_HEADER = "Le tue forniture";
	public static final String FORNITURE_CONTENT = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String OPERAZIONE_HEADER = "Operazione eseguita correttamente";
	public static final String OPERAZIONE_CONTENTS = "La tua richiesta è stata presa in carico Il processo di disattivazione della fornitura è stato avviato. Ti ricordiamo che per essere effettivo il processo potrà impiegare da 1 a 9 giorni. Ti arriverà una mail di riepilogo per tutte le forniture su cui è stata presa in carico la disattivazione.";
	public static final String OPERAZIONE_CONTENTS1 = "La tua richiesta è stata presa in carico. Per completare l'attivazione del servizio Bolletta Web, clicca sul link presente nella email che ti abbiamo inviato. La mancata conferma del link comporta l'annullamento della richiesta. Puoi verificare lo stato di attivazione del servizio navigando nel dettaglio della tua fornitura.Ricordati che puoi attivare o modificare la Bolletta Web anche dall'APP di Enel Energia. Se ancora non ce l'hai, scaricala gratuitamente e scopri tutti i servizi e i vantaggi dedicati a te.";
	public static final String OPERAZIONE_CONTENTS_ELE = "La tua richiesta è stata presa in carico Il processo di disattivazione della fornitura è stato avviato. Ti ricordiamo che per essere effettivo il processo potrà impiegare da 1 a 9 giorni. Ti arriverà una mail di riepilogo per tutte le forniture su cui è stata presa in carico la disattivazione.";
	public static final String OPERAZIONE_CONTENTS_GAS = "La tua richiesta è stata presa in carico Il processo di disattivazione della fornitura è stato avviato. Ti ricordiamo che per essere effettivo il processo potrà impiegare da 1 a 9 giorni. Sarai contattato da un consulente per fissare l'appuntamento con un tecnico del distributore di zona, che effettuerà la disattivazione sulla tua fornitura Gas Ti arriverà una mail di riepilogo per tutte le forniture su cui è stata presa in carico la disattivazione.";
	public static final String DISATTIVAZIONE_TITLE = "Ti ricordiamo che:";
	public static final String DISATTIVAZIONE_TITLE1 = "Puoi selezionare al massimo una fornitura gas;Se scegli una fornitura gas, puoi anche selezionare una ulteriore fornitura luce;Puoi selezionare al massimo cinque forniture luce contemporaneamente.";
	public static final String DISATTIVAZIONE_TITLE2 = "Se il tuo distributore è e-Distribuzione, la fornitura verrà indicata come verificata in questo caso sarà possibile scegliere la data di disattivazione";
	public static final String INDRIZZOLABEL_VALUE = "Indirizzo della fornituraViale Kennedy 405 80125 Napoli Napoli Na";
	public static final String INDRIXXOLABEL_VALUE1 = "Indirizzo della fornituraVia Mille Dei Snc P 66100 Chieti Scalo Chieti Ch";
	public static final String INDRIZZOLABEL_VALUENEW = "Indirizzo della fornituraPiazza Umberto I 2 50060 Londa Londa Fi";
	public static final String INDRIZZOLABEL_VALUE2 = "Indirizzo della fornituraViale Kennedy J F 405 80125 Napoli Napoli Na";
	public static final String INDRIZZOLABEL_VALUE3 = "Indirizzo della fornituraVia Mille Dei S 66100 Chieti Chieti Ch";
	public static final String INDRIZZOLABEL_VALUE2NEW = "Indirizzo della fornituraVia Giovanni Pascoli 12 35125 Padova Padova Pd";
	public static final String PDRLABEL_VALUE = "PDR00352800076609";
	public static final String PDRLABEL_VALUE1 = "PDR01613703035351";
	public static final String PDRLABEL_VALUE2 = "PODIT001E61289783";
	public static final String PDRLABEL_VALUENEW = "PODIT001E42051806";
	public static final String PODLABEL_VALUE = "PODIT001E80030068";
	public static final String PODLABEL_VALUENEW = "PODIT001E31365772";
	public static final String MODALISTALABEL_VALUE = "Modalità di richiestaWeb";
	public static final String MODALISTALABEL_VALUE1 = "Modalità di richiestaNumero Verde o Negozio Enel";
	public static final String MODALISTALABEL_VALUE2 = "Modalità di richiestaNumero verde o Negozio Enel";
	public static final String MODALISTALABEL_VALUE3 = "Modalità di richiest";
	public static final String MODALISTALABEL_VALUE4 = "Modalità di richiestaWeb";
		
	public static final String STEP_MENU = "1Inserimento datiStep attivo 1 di 32Riepilogo e conferma dati3Esito";
	public static final String INVIO_COMM = "Invio comunicazioni";
	public static final String INVIOCOMM_CONTENT = "Di seguito puoi modificare o confermare la mail dove invieremo le future comunicazioni:";
	public static final String DESIDERI_QUES = "Desideri ricevere anche l'ultima bolletta sull'indirizzo email indicato?";
	public static final String DATADIS = "Data Disattivazione";
	public static final String DATADISCONTENT = "Se il tuo distributore è e-distribuzione, puoi selezionare una data futura (fino a un massimo di 30 giorni) come data utile a far partire il processo di disattivazione. Il processo potrà impiegare da 1 a 9 giorni per essere effettivo:";
	public static final String AIUTACITITLE = "Aiutaci a migliorare";
	public static final String AIUTACICONTENT = "Per offrire un servizio migliore ti chiediamo di indicare il motivo per cui stai richiedendo la disattivazione.";
	public static final String INDRIZZODELLA = "Indirizzo della fornituraPiazza Umberto I 2 50060 Londa Londa Fi";
	public static final String NUMEROCLIENTE = "Numero cliente737082921";
	//new line 
		
	public static final String SERVIZIFORNITURE_TITLE = "Servizi per le forniture";
	public static final String SERVIZIFORNITURE_CONTENT = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
	public static final String SERVIZIBOLLETTE_TITLE = "Servizi per le bollette";
	public static final String SERVIZIBOLLETTE_CONTENT = "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette";
	public static final String SERVIZICONTRATO_TITLE = "Servizi per il contratto";
	public static final String SERVIZICONTRATO_CONTENT = "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture";
	public static final String DISATTIVAZIONE_HEADER =  "Disattivazione Fornitura";
														 	
	public static final String DISATTIVAZIONE_CONTENT1 = "La disattivazione della fornitura comporta la chiusura del contatore (è definita tecnicamente “disdetta con suggello”). Viene effettuata sigillando il contatore in modo che non possa essere utilizzato fino a che non venga attivato un nuovo contratto.";
	public static final String DISATTIVAZIONE_CONTENT2 = "La richiesta di disattivazione della fornitura rappresenta la manifestazione della volontà di recedere dal contratto di fornitura.";
	public static final String DISATTIVAZIONE_CONTENT3 = "Disattiva comodamente la tua fornitura ad uso abitativo dall’area riservata e avrai diritto a un bonus di 25€ per la sottoscrizione di un nuovo contratto con Enel Energia dal sito enel.it entro un anno dalla disattivazione della fornitura.";
	public static final String DOMANDE_HEADER= "Domande frequenti";
	public static final String DATA_DIS = "Data Disattivazione";	
	public static final String DATADIS_CONTENT = "Se il tuo distributore è e-distribuzione, puoi selezionare una data futura (fino a un massimo di 30 giorni) come data utile a far partire il processo di disattivazione. Il processo potrà impiegare da 1 a 9 giorni per essere effettivo:";
	public static final String AIUTACI_TITLE = "Aiutaci a migliorare";
	public static final String AIUTACI_CONTENT= "Per offrire un servizio migliore ti chiediamo di indicare il motivo per cui stai richiedendo la disattivazione.";
	public static final String HAIRICHIESTOCONTENT = "Hai richiesto la disattivazione del servizio per le seguenti forniture:";
	public String domandeQUES="Quali sono i tempi per disattivare la fornitura?"
			+"E' possibile prenotare una data di disattivazione?"
			+"Quali sono i costi per disattivare la fornitura?"
			+"Come posso utilizzare il bonus?"
			+"Chi può disattivare una fornitura online?"
			+"Attraverso quali canali posso disattivare la fornitura?";

	public String domandeANS ="I tempi massimi per l’evasione della tua richiesta sono:"
			+"LUCE: fino a 7 giorni lavorativi."
			+"GAS: fino a 9 giorni lavorativi."	
			+"Il recesso dal contratto con enel energia avrà efficacia entro 30gg dalla data di richiesta."
			+"Il servizio di prenotazione è messo a disposizione dal distributore e-distribuzione per le forniture elettriche, erogate da qualsiasi venditore." 
			+" Puoi verificare il tuo distributore consultando la tua ultima bolletta ricevuta."
			+"Il costo per la disattivazione della fornitura, che sarà addebitato nell'ultima bolletta che riceverai, è pari a €23 + Iva oltre a eventuali costi fissi applicati dal distributore."
			+"Ad avvenuta cessazione riceverai una mail contenente il codice che ti permetterà di utilizzare il bonus per l'attivazione di una nuova fornitura sul sito enel.it."
			+" Il bonus è valido per 1 anno dalla data di disattivazione della fornitura per la sottoscrizione di un contratto di luce o gas relativo a prima attivazione, voltura, subentro o cambio fornitore su tutte le offerte di Enel Energia, ad esclusione di Sempre con te, Sempre con te Gas, E-Light ed E-Light gas. Il bonus non è cumulabile con altre promozioni in corso."
			+" Al tuo rientro in Enel Energia ti verranno accreditati 25 euro ripartiti nelle prime bollette relative alla nuova fornitura."
			+"La funzionalità è disponibile per tutte le forniture ad uso domestico, tranne per quelle non disalimentabili (es. alimentazione di apparecchi elettromedicali per mantenimento in vita) e per quelle in cui è necessario la rimozione del contatore."
			+"La richiesta di disattivazione può essere effettuata soltanto dal cliente titolare del contratto di fornitura nei seguenti modi:"
			+"Accedendo al servizio attraverso il form di autenticazione presente in questa pagina."
			+"Recandosi presso uno dei nostri "
			+"negozi Enel."
			+"Chiamando il nostro call center al numero 800.900.860.";
	public static final String BOLLETTEPOUP_CONTENT = "Sei sicuro di voler uscire da questa sezione?";
	public static final String MANDATORY_MESSAGE = "L'indirizzo è obbligatorio";	
	public static final String CONFERMAPOPUP_HEADER = "Attenzione!";
	public static final String POPUPCHECKBOXES_CONTENT = "Non è possibile procedere con l’operazione. Verifica le selezioni seguendo le indicazioni riportate nell' informativa.";
	public static final String ELECTRICITY_SUPPLIES = "Indirizzo della fornitura Via Nizza 152 00198 Rom";
	public static final String PROCEDENDO = "Procedendo con l'operazione avrai diritto ad un bonus di 25€ per la sottoscrizione di un nuovo contratto con Enel Energia entro un anno.";
	public static final String RICEVERAI = "Riceverai la bolletta di chiusura al seguente indirizzo:";
	public static final String BOLLETTA_HEADER = "Bolletta di chiusura";
	public static final String SUPPLIES_HEADER = "Hai richiesto la disattivazione del servizio per le seguenti forniture:";
}
