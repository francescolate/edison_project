 
package com.nttdata.qa.enel.components.lightning;


import org.openqa.selenium.WebDriver;


import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Arrays;



public class CliccaTP2Component {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	static File token = new File("C:\\Appo\\token.json\\Google.Apis.Auth.OAuth2.Responses.TokenResponse-user");
	static File tokenCrm = new File("C:\\Appo\\token.json\\Google.Apis.Auth.OAuth2.Responses.TokenResponse-user.crm");
	static File tokenWeb = new File("C:\\Appo\\token.json\\Google.Apis.Auth.OAuth2.Responses.TokenResponse-user.web");
	static File tokenMobile = new File("C:\\Appo\\token.json\\Google.Apis.Auth.OAuth2.Responses.TokenResponse-user.mobile");

	
	public CliccaTP2Component(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	public void svuotaFileOut() throws Exception {
    	// Il File out.txt deve essere presente e vuoto
    	File fil = new File("C:\\Appo\\out.txt");
        
        if (!fil.exists()) {
     	   throw new Exception("File di output non presente");
        }else {
        	fil.delete();
        	fil.createNewFile();
        }
	}
	
	public void impostaFileToken(String tokenEmail) throws Exception {
    	// Il file token va impostato in base al valore TOKEN nel Properties
       	File tok = new File("token");
        if (!tok.exists()) {
      	   throw new Exception("File token non presente");
         }else {
        	 tok.delete();
         }
                
		switch(tokenEmail) {
		case "CRM":
			Files.copy(tokenCrm.toPath(), token.toPath());
			break;
		case "WEB":
			Files.copy(tokenWeb.toPath(), token.toPath());
			break;
		case "MOBILE":
			Files.copy(tokenMobile.toPath(), token.toPath());
		}
	}
	
	public String lanciaIkimailBat(String l) throws Exception {
    	File fil = new File("C:\\Appo\\out.txt");
    	
    	// eseguo il file bat che legge email e scrive il file out.txt
     	File currentDirFile = new File(".");
    	String helper = currentDirFile.getAbsolutePath();
    	String currentDir = helper.substring(0, helper.length() - 1);
    	String[] command = {currentDir+"resources\\avvioIKImail.bat"};
//    	String[] command = {"CMD", "/C", "dir"};
    	
    	System.out.println(helper);
    	System.out.println(currentDir);
       ProcessBuilder probuilder = new ProcessBuilder( command );
//       probuilder.directory(new File("c:"));
      probuilder.directory(new File("c:\\xWorkSpaceENEL\\Enel_New\\resources"));
        
        Process process = probuilder.start();
        
        //Read out dir output
        java.io.InputStream is;
        is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        System.out.printf("Output of running %s is:\n",
                Arrays.toString(command));
        boolean flag = false;
        while ((line = br.readLine()) != null) {
           if(line.contains("Execution terminated successfully")) {
        	   flag = true;
           }
        }
        
        if(!flag) {
        	throw new Exception("Errore durante l'estrazione del link.");
        }
        
       java.io.InputStream err = process.getErrorStream();
       InputStreamReader er = new InputStreamReader(err);
       br = new BufferedReader(er);
       
       System.out.printf("Error of running %s is:\n",
               Arrays.toString(command));
       while ((line = br.readLine()) != null) {
           System.out.println(line);
       }
 
           // apre il file in lettura
           FileReader filein = new FileReader(fil.getAbsolutePath());
           String toRet = "";
           int next;
           do {
               next = filein.read(); // legge il prossimo carattere
               
               if (next != -1) { // se non e' finito il file
                   char nextc = (char) next;
                   toRet+=nextc; // stampa il carattere
               }

           } while (next != -1);
           
           filein.close(); // chiude il file
           
//           toRet = toRet.replace("oldChar", "");
           toRet = toRet.replace("CONFIRMATIONLINK=", "");
           
           System.out.println(toRet);
           //setproperty
       
        //Wait to get exit value
        try {
            int exitValue = process.waitFor();
            System.out.println("\n\nExit Value is " + exitValue);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		return toRet;
		
	}
	
}

