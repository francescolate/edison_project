package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupplyLightDetailComponent {
	
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By DettaligoFornitura = By.xpath("//span[@class='icon-line-electricity']/../../../../..//div[@class='button-container']/a[2]");
	public By DettaligoFornitura1 = By.xpath("//div[@class='card forniture regolare']/div[2]/a[2]");
	public By LightTest = By.xpath("//h1[@id='fornitura01']");
	public By Mostradi = By.xpath("//div[@class='open-supply']//a");
	public By StatoForitura = By.xpath("//dt[contains(text(),'Stato Fornitura')]");
	public By StatoForituraValue = By.xpath("//span[contains(text(),'Attiva')]");
	public By StatoPagamento = By.xpath("//dt[contains(text(),'Stato pagamento')]");
	public By StatoPagamentoValue = By.xpath(" //dd[@id='statoPag']");
	public By Modalita = By.xpath("//dt[contains(text(),'Modalità di pagamento')]");
	public By ModalitaValue = By.xpath("//span[contains(text(),'Bollettino Postale')]");
	public By Ricezione = By.xpath("//dt[contains(text(),'Ricezione bollette')]");
	public By RicezioneValue = By.xpath("//span[contains(text(),'Cartacea')]");
	public By DataDi = By.xpath("//dt[contains(text(),'Data di attivazione')]");
	public By DataDiValue = By.xpath("//span[5]//dd[1]//span[1]");
	public By Offerata = By.xpath("//dt[contains(text(),'Offerta attiva')]");
	public By OfferataValue = By.xpath("//span[contains(text(),'Sempre Con Te')]");
	public By Numero = By.xpath("//dt[contains(text(),'Numero Offerta')]");
	public By NumeroValue = By.xpath("//span[contains(text(),'2-QOB7AHK')]");
	public By POD = By.xpath("//dt[contains(text(),'POD')]");
	public By PODValue=By.xpath("//span[contains(text(),'IT001E65000330')]");
	public By Potenza =By.xpath("//dt[contains(text(),'Potenza')]");
	public By Tenzione = By.xpath("//dt[contains(text(),'Tensione')]");
	public By TenzioneValue = By.xpath("//span[contains(text(),'220 V')]");
	public By ModalitaSotto = By.xpath("//dt[contains(text(),'Modalità di sottoscrizione')]");
	public By ModalitSottoValue = By.xpath("//span[contains(text(),'Registrazione vocale')]");
	public By Contratto = By.xpath("//dt[contains(text(),'Contratto')]");
	public By ContrattoValue = By.xpath("//span[contains(text(),'PDF non disponibile')]");
	
	public By RinominaButton = By.xpath("//a[contains(text(),'RINOMINA')]");
	public By VisualizzaButton = By.xpath("//a[contains(text(),'VISUALIZZA BOLLETTE')]");
	
	public By InserisciText = By.xpath("//h4[contains(text(),'Inserisci il nuovo nome della')]");
	public By NomeFornituraInput = By.xpath("//input[@id='input_nomeFornitura']");
	public By ContinuaButton = By.xpath("//button[@class='button_first']");
	public By IndietroButton = By.xpath("//*[@id='rinominaFornStep1']/div/div[2]/div[2]/button[1]");
	
	public By VerificaText = By.xpath("//h4[contains(text(),'Verifica la correttezza delle informazioni:')]");
	public By NomeFornituraText = By.xpath("//dt[contains(text(),'Nome Fornitura')]");
	public By NomeFornituraTextValue = By.xpath("//dd[@id='aliasRiepilogo']");
	public By TipologiaText = By.xpath("//dt[contains(text(),'Tipologia')]");
	public By TipologiaTextValue = By.xpath("//dd[contains(text(),'Elettrico')]");
	public By NumeroClienteText = By.xpath("//dt[contains(text(),'Numero Cliente')]");
	public By NumeroClienteTextValue = By.xpath("//dd[contains(text(),'635824982')]");
	public By ConfermaButton = By.xpath("//*[@id='rinominaFornStep2']/div/div[2]/div[2]/button[2]");
	public By Indietrobuttn = By.xpath("//button[@class='button_second']");
	
	
	public By OperaZioneText = By.xpath("//h4[contains(text(),'Operazione eseguita correttamente')]");
	public By OperaZioneTextDetail=By.xpath("//p[contains(text(),'La richiesta di modifica del nome della tua fornit')]");
	public By FineButton = By.xpath("//button[@class='button_first']");
	
	public By InserimentoDatiStatus = By.xpath("//li[@class='step1 step approved']");
	public By RiepilogoStatus = By.xpath("//li[@id='accessibility']");
	public By EsitoStatus = By.xpath("//li[@class='step3 step']");
	
	//Confirm Status
	public By InserimentoConfirmStatus = By.xpath("//span[contains(text(),'Inserimento dati')]");
	public By RiepilogoConfirmStatus = By.xpath("//span[contains(text(),'Riepilogo e conferma dati')]");
	public By EsitoConfirmStatus = By.xpath("//span[contains(text(),'Esito')]");
	
	public By RinominaPageTitle = By.xpath("//h2[contains(text(),'Stai effettuando la modifica del nome della tua fo')]");
	
	//Left Menu List
	public By HomePageMenuList = By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]");
	
	public By SupplyDetailsHeading = By.xpath("//dl[@class='supply-details-heading']");
	
	 public SupplyLightDetailComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	
	public void launchLink(String url) throws Exception {
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void homepageRESIDENTIALMenu (By checkObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Forniture"))
	        item_found="YES";
	   
	    else if(description.contentEquals("Bollette"))
	        item_found="YES";
	    else if (description.contentEquals("Servizi"))
	        item_found="YES";
	    else if (description.contentEquals("enelpremia WOW!"))
	        item_found="YES";
	    else if (description.contentEquals("Novità"))
	        item_found="YES";
	    else if (description.contentEquals("Spazio Video"))
	        item_found="YES";
	    else if (description.contentEquals("Account"))
	        item_found="YES";
	    else if (description.contentEquals("I tuoi diritti"))
	        item_found="YES";
	    else if (description.contentEquals("Supporto"))
	        item_found="YES";
	    else if (description.contentEquals("Trova Spazio Enel"))
	        item_found="YES";
	    else if (description.contentEquals("Esci"))
	    	item_found="YES";
	   
	        if (item_found.contentEquals("NO"))
	        throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'Forniture', 'Bollette','Servizi','enelpremia WOW!','Novità','Spazio Video','Account','I tuoi diritti','Supporto','Trova Spazio Enel','Esci'");
	        
	        
	            }
	    
	        }
	

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
			
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	
	
	public void backBrowser() throws Exception {
		try {
			driver.navigate().back();
			Thread.sleep(5000);
			
		} catch (Exception e) {
			throw new Exception("Previous Page not displayed");

		}
		
	 	
		
	}


}
