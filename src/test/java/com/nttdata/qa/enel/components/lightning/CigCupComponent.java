package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CigCupComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	
	public CigCupComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
//	public By confermaCigCupSWAEVO = By.xpath("//h2[text()='CIG e CUP']/ancestor::div[@role='main']//button[text()='Conferma']");
	public By confermaCigCupSWAEVO = By.xpath("//h2[text()='CIG e CUP']/ancestor::div[@role='main'and @aria-labelledby]//button[text()='Conferma']");
	public By salvaFornituraCigCupSWAEVO = By.xpath("//h2[text()='CIG e CUP']/ancestor::div[@role='main'and @aria-labelledby]//button[text()='Salva fornitura']");
	
	public By selezionaFlag136 = By.xpath("//label[contains(text(),'Flag 136')]/parent::div//input[@role='listbox']");
	public By cig = By.xpath("//label[text()='CIG']/parent::div//input");
	public By cup = By.xpath("//label[text()='CUP']/parent::div//input");
	public By sezioneCigCUP = By.xpath("//h2[text()='CIG e CUP']");
	public String colonne []={"Indirizzo fornitura","POD","CIG","CUP"};
	public By radioButtonCig = By.xpath("//h2[text()='CIG e CUP']/ancestor::div[@role='region']//table/tbody/tr/td//span");
	public By cigMessage = By.xpath("//p[text()='Il primo inserimento di CIG e CUP verrà applicato a tutte le linee di fornitura presenti. Successivamente se necessario sarà possibile modificare singolarmente tutte le linee di fornitura.']");
	
	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void selezionaFlag136(String valore) throws Exception{
		util.objectManager(selezionaFlag136, util.scrollAndClick);
		By scelta=By.xpath("//label[text()='Flag 136']/parent::div//span[text()='"+valore+"']");
		util.objectManager(scelta, util.click);
//		util.selectLighningValue("Flag 136", valore);
	}
	
	public void insertFiled(String valore,By obj) throws Exception{
		util.objectManager(obj, util.scrollAndSendKeys,valore);
	}
	 public void verificaColonneTabella(String [] col) throws Exception{
		 List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//table[@id='supplyComponent_Table']/thead/tr/th/div/span"));
		 if (nomeColonne.size()==col.length) {
			 for(int i = 1; i <= col.length; i++){
				    WebElement c=driver.findElement(By.xpath("//table[@id='supplyComponent_Table']/thead/tr/th["+(i+1)+"]/div/span"));
					String columnName=c.getText();	
					//System.out.println(columnName);
					//System.out.println(col[i-1].trim());
					 if(!columnName.contentEquals(col[i-1].trim()))
					       	throw new Exception("la tabella forniture non contiene le colonne con descrizione: "+col[i]+"; il nome delle colonne visualizzato e' "+columnName);  
				}
		 }
	 }
	 public void verifyComponentExistence(By existingObject) throws Exception {
		    if (!util.exists(existingObject, 15))
					throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
			}
		
}
