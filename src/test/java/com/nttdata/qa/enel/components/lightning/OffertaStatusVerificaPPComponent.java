package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class OffertaStatusVerificaPPComponent
 {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
    public By itemName = By.xpath("//span[text()='Item Name']/ancestor::table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit']//span//a");
    public By buttonAltreAzioni = By.xpath("//button [@name='ITA_IFM_Case_Items__c.Other_actions']");
    public By btnGestionePrev = By.xpath("//button[@name='prev_Button']");
    public By btnAnnulla = By.xpath("//*[@class=' label bBody']/..");
    public By btnAnnulla3 = By.xpath("//*[@id='wrapper-body']//*[@class=' label bBody']");
    
    public By popupAttenzione = By.xpath("//button [@id='commButtonIdA']");  //-- 06/11/2020
//    public By popupAttenzione = By.xpath("//div[@id='brandBand_6']//button [@id='commButtonIdA']");
    
    public By statoPreventivo = By.xpath("//select [@name='StatoPrev']");  //-- 06/11/2020
//    public By statoPreventivo = By.xpath("//div[@id='brandBand_6']//select [@name='StatoPrev']");
  
    public By btnGestionePrevOK = By.xpath("//div[@class='slds-no-flex slds-align-bottom']//button[contains(text(),'Conferma')]"); //- 06/11/2020
//    public By btnGestionePrevOK = By.xpath("//div[@id='brandBand_6']//div[@class='slds-no-flex slds-align-bottom']//button[contains(text(),'Conferma')]");
  
    public By elementoRichesta = By.xpath("//span[contains(text(),'Elemento Richiesta')]//parent::a");
	final By finalStatus = By.xpath("//span[@title='Stato']/following-sibling::div/div/span");
	final By numeroOfferta=By.xpath("//span[@title='Quote']/following-sibling::div/div/span");
	final By numeroOffertaDaRichiesta= By.xpath("//div[text()='Offerta']/ancestor::span[@class='slds-text-heading_small']/a");
//	public final By tabDettagli=By.xpath("//a[@data-tab-value='detailTab' and @aria-selected='true']");
	public final By tabDettagli=By.xpath("//a[@data-tab-value='detailTab' and @aria-selected='false']");
	public final By buttonRiepilogo=By.xpath("//button[@alt='Riepilogo']");
	public final By buttonNuovaVoce=By.xpath("//div[@class='slds-button-group']//button[text()='Nuova Voce']");
	public final By costoUnitario=By.xpath("//div[@id='modalAddAttrId']//*[text()='COSTO UNITARIO']/..//input");
	public final By quantita=By.xpath("//div[@id='modalAddAttrId']//*[contains(text(),'QUANTITA')] /..//input");
	public final By buttonAdd=By.xpath("//div[@id='modalAddAttrId']//button[text()='Add']");
    public By descrizione = By.xpath("//select [@name='StatoPrev']");
    public By btnInvioPrev = By.xpath("//button[@name='send_Button']");
	public By radioCommodity = By.xpath("(//*[@class='slds-radio--faux'])[2]");

	
	public OffertaStatusVerificaPPComponent(WebDriver driver ) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	public String RecuperaNumeroOfferta() throws Exception {
		WebElement ele=util.waitAndGetElement(numeroOfferta);
		String nofferta=ele.getText();
		Logger.getLogger("").log(Level.INFO, "N offerta:"+nofferta);
		return nofferta;
	}
	
	public String RecuperaNumeroOffertaDaRichiesta() throws Exception {
		WebElement ele=util.waitAndGetElement(numeroOffertaDaRichiesta);
		String nofferta=ele.getText();
		Logger.getLogger("").log(Level.INFO, "N offerta:"+nofferta);
		return nofferta;
	}
	
	public void gestionePreventivo() throws Exception{
		
		TimeUnit.SECONDS.sleep(5);
		//*** Verificare esistenza bottone "Altre Azioni"
		if(util.verifyExistence(buttonAltreAzioni, 100)){
   		    System.out.println("Bottone Altre Azioni già visualizzato");
			WebElement altreAzioni = util.waitAndGetElement(buttonAltreAzioni);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
//			altreAzioni = util.waitAndGetElement(buttonAltreAzioni);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", altreAzioni);
   		    System.out.println("Click Bottone Altre Azioni");
			TimeUnit.SECONDS.sleep(25);
		} else {
			// Click sul ItemName
			WebElement element = util.waitAndGetElement(itemName);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			if(util.verifyExistence(itemName, 10)){
       		    System.out.println("Esiste Item Name");
				util.objectManager(itemName, util.scrollToVisibility,false);
				// Click sul Case Item
				executor.executeScript("arguments[0].click();", element);
       		    System.out.println("Click su item Name");
				TimeUnit.SECONDS.sleep(5);
				//*** Verificare esistenza bottone "Altre Azioni"
//				// Click sul bottone Altre Azioni
				element = util.waitAndGetElement(buttonAltreAzioni);
				executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
       		    System.out.println("Click sul bottone Altre Azioni");
				TimeUnit.SECONDS.sleep(30);
			}
		}
		WebElement element = util.waitAndGetElement(btnGestionePrev,100,1,true);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		    System.out.println("Click sul bottone Gestione preventivo");
		TimeUnit.SECONDS.sleep(5);
		// Click sul bottone OK del popup *** Mettere wait sul popup perchè appare dopo molto tempo
		WebElement element1 = util.waitAndGetElement(popupAttenzione,100,1,true);
		executor.executeScript("arguments[0].click();", element1);
		TimeUnit.SECONDS.sleep(5);
	    System.out.println("Click su pop-up Attenzione!");
		
		// Select Stato Preventivo
		String valorePrev = "PREVENTIVO PAGATO";
		util.objectManager(statoPreventivo, util.select, valorePrev);
		TimeUnit.SECONDS.sleep(5);
		// Click sul bottone CONFERMA di Gestione preventivo
		element = util.waitAndGetElement(btnGestionePrevOK);
		executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		    System.out.println("Click sul bottone CONFERMA Gestione preventivo");
		TimeUnit.SECONDS.sleep(5);
		// se esiste il bottone "Gestione Preventivo" clicco su Annulla per chiudere la finestra
		if(util.verifyVisibility(btnGestionePrev, 10)){
			element = util.waitAndGetElement(btnAnnulla);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		}
	}

	public void gestionePreventivoGAS(String importo, String stepPP) throws Exception{
		
		WebElement element;
		JavascriptExecutor executor;
		TimeUnit.SECONDS.sleep(5);
		//*** Verificare esistenza bottone "Altre Azioni"
		if(!util.verifyExistence(buttonAltreAzioni, 10)){
			//TimeUnit.SECONDS.sleep(5);
			if(util.verifyExistence(itemName, 10)){
				util.objectManager(itemName, util.scrollToVisibility,false);
			}
			// Click sul Case Item
			System.out.println("Click sul Case Item");
			element = util.waitAndGetElement(itemName);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(5);
			// Click sul bottone Altre Azioni
			System.out.println("Click sul bottone Altre Azioni");
			element = util.waitAndGetElement(buttonAltreAzioni);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(25);
			// Click sul bottone Gestione preventivo
			System.out.println("Click sul bottone Gestione preventivo");
			element = util.waitAndGetElement(btnGestionePrev,100,1,true);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(15);
			// bottone "Nuova Voce" mettendo Importo del preventivo e confermare
			System.out.println("Click sul bottone Nuova Voce");
			util.verifyClickability(buttonNuovaVoce, 50); //Verifico che il bottone sia cliccabile
			util.objectManager(buttonNuovaVoce, util.scrollAndClick);
			System.out.println("Click eseguito sul bottone Nuova Voce ** OK ** ");
			// Descrizione
			util.objectManager(descrizione, util.select, "Contributo Allacciamento");
			util.objectManager(costoUnitario, util.scrollAndSendKeys, importo);
			util.objectManager(quantita, util.scrollAndSendKeys, "1");
			util.objectManager(buttonAdd, util.scrollAndClick);
			// Gestione nuovo Radio Button - importo preventivo
			element = util.waitAndGetElement(radioCommodity);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(5);
			// Click sul bottone Conferma di Gestione preventivo
			System.out.println("Click sul bottone Conferma");
			element = util.waitAndGetElement(btnGestionePrevOK);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(15);
			// click su bottone Annulla	
			if(util.verifyExistence(btnGestionePrev, 10)){
				element = util.waitAndGetElement(btnAnnulla3);
				executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
			}

			// Click sul bottone Altre Azioni per Preventivo PAGATO
			System.out.println("Passaggio dello Stato Preventivo da: Preventivo Inviato a Preventivo Pagato");
			element = util.waitAndGetElement(buttonAltreAzioni);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(10);
			// Click sul bottone Gestione preventivo
			element = util.waitAndGetElement(btnGestionePrev,100,1,true);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(10);
			// Click sul bottone OK del popup
			if(util.verifyVisibility(popupAttenzione, 5)) {
				util.objectManager(popupAttenzione, util.scrollAndClick);
				System.out.println("Popup Visibile! Confermo con OK");
			}else {
				System.out.println("Popup non visibile! Continuo...");
			}
			// Select Stato Preventivo
			String valorePrev = "PREVENTIVO PAGATO";
			if(util.verifyClickability(statoPreventivo, 5)) {
				util.objectManager(statoPreventivo, util.select, valorePrev);
				System.out.println("Cambio Stato Preventivo in PAGATO eseguito");
				TimeUnit.SECONDS.sleep(5);
			} else {
				System.out.println("Campo Stato Preventivo disabilitato! Errore!");
				throw new Exception("Campo Stato Preventivo disabilitato! Errore!");
			}
			// Click sul bottone OK di Gestione preventivo
			element = util.waitAndGetElement(btnGestionePrevOK);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(5);
			System.out.println("Click sul bottone OK di Gestione preventivo");
			// click su bottone Annulla	
			if(util.verifyExistence(btnGestionePrev, 10)){
				element = util.waitAndGetElement(btnAnnulla);
				executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				System.out.println("click su bottone Annulla");
			}
		} else {
   		    System.out.println("*** Bottone Altre Azioni 2° giro - sono già sul Case Item ***");
			// Click sul bottone Altre Azioni
			element = util.waitAndGetElement(buttonAltreAzioni);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			TimeUnit.SECONDS.sleep(25);
			// Click sul bottone Invio Preventivo
			element = util.waitAndGetElement(btnInvioPrev,100,1,true);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
   		    System.out.println("*** Click sul bottone INVIO Preventivo ***");
			TimeUnit.SECONDS.sleep(15);
			// Click sul bottone Altre Azioni
			element = util.waitAndGetElement(buttonAltreAzioni);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
   		    System.out.println("*** Click sul bottone Altre Azioni ***");
			TimeUnit.SECONDS.sleep(25);
			// Click sul bottone Gestione preventivo
			element = util.waitAndGetElement(btnGestionePrev,100,1,true);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
   		    System.out.println("*** Click sul bottone Gestione preventivo ***");
			TimeUnit.SECONDS.sleep(15);
			// Click sul bottone CONFERMA di Gestione preventivo
			element = util.waitAndGetElement(btnGestionePrevOK);
			executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
   		    System.out.println("*** Click sul bottone CONFERMA di Gestione preventivo ***");
			TimeUnit.SECONDS.sleep(5);
			// click su bottone Annulla	
			if(util.verifyExistence(btnGestionePrev, 10)){
				element = util.waitAndGetElement(btnAnnulla);
				executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
	   		    System.out.println("*** click su bottone Annulla ***");
			}
			
		}
		
	}

	
	public void validateStatus(String status ) throws Exception {
		int tentativi = 7;
		int secondi = 30;
		int time = tentativi * secondi;
		boolean statoOk = false;
		String realStatus = "";
		while(tentativi-- > 0 && !statoOk) {
			realStatus = util.waitAndGetElement(finalStatus).getText();
			//System.out.println("Lo stato visualizzato è: "+realStatus);
			if(!realStatus.toLowerCase().contentEquals(status.toLowerCase())){
				TimeUnit.SECONDS.sleep(secondi);
				driver.navigate().refresh();
			}
			
			else statoOk=true;
			
			
		}
		
		if(!statoOk){
			throw new Exception("Dopo aver atteso "+time+" secondi lo stato della richiesta risulta:"+realStatus+". Lo stato atteso e':"+status);
		}

	
	
	}
	public void clickTabDettagli(By tab) throws Exception {
		util.objectManager(tab, util.click);
	}
	
	public void clickRiepilogo(By button) throws Exception {
		String frame=util.getFrameByIndex(0);
		util.frameManager(frame, button, util.click);
		spinner.checkSpinners();
		
	}
}
