package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSN_RevocoInfoEnelEnergiaComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By scopriDiPiuLink_Revoco_InfoEnelEnergia = By.xpath("//tr/td/b[text()='Revoca']/parent::td/following-sibling::td/a[text()='Scopri di più']");
	public By PROSEGUIbutton = By.xpath("//a[text()='Prosegui']");
	public By disattivazione_ieePageTitle = By.xpath("//ol[@class='breadcrumb']/following-sibling::h1");
	public By disattivazione_Prosegui_button = By.xpath("//button[text()='Prosegui']");
	public By ieePageTitle = By.xpath("//ol[@class='breadcrumb']/following-sibling::h1");
	public By Attiva_Bolletta_Web_button = By.xpath("//div//a[text()='Attiva Bolletta Web']");
	public By fineButton = By.xpath("//div//a[text()='Fine']");
	
	public BSN_RevocoInfoEnelEnergiaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponent(By clickObject) throws Exception {
			util.objectManager(clickObject, util.scrollToVisibility, false);
			util.objectManager(clickObject, util.scrollAndClick);
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		
		public static final String DISATTIVAZIONE_IEE_PAGE_TITLE ="Disattivazione Info Enel Energia";
		public static final String IEE_PAGE_TITLE ="Info Enel Energia";
}



