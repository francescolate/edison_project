package com.nttdata.qa.enel.components.colla;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LuceComponent {
	
	public By logoEnel = By.xpath("//img[@class='logoimg']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	
	public By labelCheContratto = By.xpath("//div[@class='search-section']/descendant::label[contains(text(),'Che contratto vuoi attivare')]");
	public By labelDove = By.xpath("//div[@class='search-section']/descendant::label[contains(text(),'Dove')]");
	public By labelPerQuale = By.xpath("//div[@class='search-section']/descendant::label[contains(text(),'Per quale necessità')]");
	//public By dropdownSelect = By.xpath("//select[@id='product']/child::option");
	public By dropdownSelect = By.xpath("//select[@id='product']");
	public By iniziaButton = By.xpath("//div[@class='btn-section']/child::a[@id='ofertButton']");
	
	public By productDropDown = By.xpath("//span[@id='productSelectBoxItText']");
	public By luce = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Luce']");
	public By luceGas = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Luce e Gas']");
	
	
	public By luce30OfferContent = By.xpath("//h3[text()='Luce 30 Spring']/ancestor::li[@class='offert-item']/descendant::p[contains(text(),'prezzo per kWh')]");
	public By attivaOra_link = By.xpath("//h3[text()='Luce 30 Spring']/ancestor::li[@class='offert-item']/descendant::a[contains(text(),'Attiva ora')]");
	public By adesioneContents = By.xpath("//div[@id='adesione_standard']/child::p[@class='image-hero_detail text--detail']");
	public By inputHeader = By.xpath("//h3[contains(text(),'Inserisci i tuoi dati')]");
	
	public By inputNome = By.xpath("//div[@class='form-group']/child::label[text()='Nome*']/following-sibling::input[@id='ITA_IFM_First_Name__c']");
	public By inputCognome = By.xpath("//div[@class='form-group']/child::label[text()='Cognome*']/following-sibling::input[@id='ITA_IFM_Last_Name__c']");
	public By inputCodiceFiscale = By.xpath("//div[@class='form-group']/child::label[text()='Codice Fiscale*']/following-sibling::input[@id='NE__Fiscal_Code__c']");
	public By inputTelefonoCellulare = By.xpath("//div[@class='form-group']/child::label[text()='Telefono Cellulare*']/following::input[@id='ITA_IFM_Mobile_Phone__c']");
	public By inputEmail = By.xpath("//label[text()='Email*']/parent::div[@class='form-group']//input[@id='NE__E_mail__c']");
	public By inputConfirmEmail = By.xpath("//label[text()='Conferma email*']/parent::div[@class='form-group']//input[@id='NE__E_mail__c_confirm']");
	
	public By labelConfirmEmail = By.xpath("//label[text()='Conferma email*']");
	public By lablelEmail = By.xpath("//span[@id='NE__E_mail__c_errmsg']/preceding-sibling::label[text()='Email*']");
	public By labelCodiceFiscal = By.xpath("//span[@id='NE__Fiscal_Code__c_errmsg']/preceding-sibling::label[text()='Codice Fiscale*']");
	public By lableTellefonoCellulare = By.xpath("//span[@id='ITA_IFM_Mobile_Phone__c_errmsg']/preceding-sibling::label[text()='Telefono Cellulare*']");
	public By labelNome = By.xpath("//span[@id='ITA_IFM_First_Name__c_errmsg']/preceding-sibling::label[text()='Nome*']");
	public By labelCognome = By.xpath("//span[@id='ITA_IFM_Last_Name__c_errmsg']/preceding-sibling::label[text()='Cognome*']");
	
	public By proseguiButton = By.xpath("//span[@id='j_id0:j_id13:j_id14:Customer-save-button']/preceding-sibling::button[text()='Prosegui']");
	
	public By lablePrivacyContents = By.xpath("//label[contains(text(),'In particolare')]");
	public By labelPrivacyCheckbox = By.xpath("//label[@id='label-privacy-consent']");
	
	
	public By titleDiCosa = By.xpath("//div[@class='col-md-12']/child::p[contains(text(),'Di cosa hai')]");
	public By radioCambio = By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'CAMBIO FORNITORE')]");
	public By radioPrima =  By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'PRIMA ATTIVAZIONE')]");
	public By radioSubentro = By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'SUBENTRO')]");
	public By radioVoltura = By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'VOLTURA')]");
	public By inputPod = By.xpath("//div[@class='form-group']//input[@id='ITA_IFM_POD__c']");
	public By inputCap = By.xpath("//div[@class='form-group']//input[@id='ITA_IFM_ZIPCode__c']");
	
	
	
	public static final String LABEL_CHECONTRATTO = "Che contratto vuoi attivare?";
	public static final String LABEL_DOVE = "Dove?";
	public static final String LABEL_QUALE = "Per quale necessità?";
	public static final String LUCE30OFFER_CONTENT = "*prezzo per kWh, tutti i giorni e a tutte le ore."
													+"Il prezzo si riferisce alla sola componente energia ed è bloccato per un anno (IVA ed imposte escluse)";
	public static final String PRIVACY_CONTENT = "Ho preso visione dell'informativa sulla privacy";
	public static final String ADISIONE_CONTENT = "Ricorda che per completare l'adesione devi avere a portata di mano:"
												+ " Codice fiscale / POD / Iban"
												+ " se devi fare un Subentro, una Prima attivazione o una Voltura servono anche:"
												+ " dati dell'immobile / Carta d'identità"
												+ " L'offerta che hai scelto è valida solo per forniture ad uso abitativo.";
												
	public static final String INPUT_HEADER = "Inserisci i tuoi dati";
	
	
	WebDriver driver;
	SeleniumUtilities util;
	
	 public LuceComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	 public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}

		
		public void selectDropDownValue(By checkObject, String property ) throws Exception
		{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			Select select = new Select(driver.findElement(checkObject));
			select.selectByValue(property);
					
		}
		
		public void clickDropDownValue(By dropdown,By input) throws Exception {
	         util.scrollToElement(driver.findElement(dropdown));
	         clickComponent(dropdown);
	         clickComponent(input);
	           
	        }
		

		public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
		public void switchToFrame(String frameName) throws Exception{
			WebDriverWait wait = new WebDriverWait(this.driver, 120);
			
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
					);
			this.driver.switchTo().frame(frameToSw);
		}
		public void clearText(By object){
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WebElement input = driver.findElement(object);
			input.clear();
		}
		
		
		public void verifyComponentExistence(By oggettoEsistente) throws Exception {
					if (!util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
			
		}
		
		public void SwitchToWindow() throws Exception
		{
			Set<String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
			/*	System.out.println(winHandle);
				System.out.println("switched to "+driver.getTitle()+"  Window");
				System.out.println(driver.getCurrentUrl());
			*/	
		       // String pagetitle = driver.getTitle();
		        		driver.switchTo().window(winHandle);
		   /*     System.out.println(winHandle);
		        System.out.println("After switch"+ driver.getCurrentUrl());
		        System.out.println(driver.getTitle());
			*/    
			 }
		}
		public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}
		
		public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
			textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (!util.checkElementText(objectWithText, textToCheck)) {
				WebElement problemElement = driver.findElement(objectWithText);
				String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
				if (elementText.length() < 1) {
					elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
				}
				String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
				System.out.println(message);
				throw new Exception(message);
			}
		}
		public void verifyContent(By object,String text) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
			String value = driver.findElement(object).getText();
			value = value.replaceAll("(\r\n|\n,)","");
			//System.out.println(value);
			if(!text.contentEquals(value))
				throw new Exception("Value is not matching with expected");
			
		}

			public void clickComponentIfExist(By existObject) throws Exception {
			if (util.exists(existObject, 15)) {
				util.objectManager(existObject, util.scrollToVisibility, false);
			    util.objectManager(existObject, util.scrollAndClick);
			}
		}
			public void waitForElementToDisplay (By checkObject) throws Exception{
				WebDriverWait wait = new WebDriverWait (driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				
			}
			public void jsClickObject(By by) throws Exception {
				util.jsClickElement(by);
			}
				
		public void enterInput(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void verifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			String textfield_found="NO";
			WebElement element = driver.findElement(checkObject);
			String actualtext = element.getText();
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
		}
		
		public void hanldeFullscreenMessage(By by) throws Exception{
			if(util.verifyExistence(by, 60)){
				util.objectManager(by, util.scrollToVisibility, false);
				util.objectManager(by, util.scrollAndClick);
			}
		}
		
		public void backBrowser(By checkObject) throws Exception {
		    try {
		    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    	driver.navigate().back();
		    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		                
		    } catch (Exception e) {
		        throw new Exception("Previous Page not displayed");
		    }
		}
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
			
			
		}
		
		public void checkUrl(String url) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			Thread.sleep(5000);
			String link=driver.getCurrentUrl();
			
			if (url.contains("https://"))
			{
				url = url.substring(8);
			}
			if (!link.contains(url))
				 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
			}
		public void pressJavascript(By clickObject) throws Exception {
			WebElement element = util.waitAndGetElement(clickObject);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}

}
