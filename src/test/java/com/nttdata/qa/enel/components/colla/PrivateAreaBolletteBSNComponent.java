package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaBolletteBSNComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;

	//public By  = By.xpath("");
	public By accountLogo = By.xpath("//img[@id='avatarUtente']");
	public By bolletteMenuItem = By.xpath("//a[text()='bollette']");
	public By bolletteHeader = By.xpath("//div[@class='panel panel-default panel-clear no-margin-bottom']//h1[text()='Bollette']");
	public By bolletteHeaderSub = By.xpath("//div[@class='panel panel-default panel-clear no-margin-bottom']//p[@class='lead']");
	public By bolletteDaPagareMenuItem = By.xpath("//a[text()='Bollette da pagare']");
	public By bolletteDaPagareHeader = By.xpath("//section[@class='content bollette-da-pagare']//h1");
	public By bolletteDaPagareHeaderSub = By.xpath("//p[@class='lead seleziona']");
	public By bolletteDaPagareTable = By.xpath("//table");
	public By bolletteDaPagareTableRows = By.xpath("//tbody//tr");
	public By bolletteDate = By.xpath("//table//tr[contains(@class,'table-collapse')]//time");
	
    public PrivateAreaBolletteBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public void compareTextDisabledInput(String xpath, String textToCheck) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		if(!s.equals(textToCheck))
			throw new Exception("Il testo cercato:\n\t"+textToCheck+"\nè diverso da quello in pagina:\n\t"+s);
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}

	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
	}
	
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public void checkBolletteTableRows(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		List<WebElement> l = driver.findElements(by);
		if(l.size()<1)
			throw new Exception("Non ci sono bollette da pagare!");
	}
	
	public static final String bolletteHeaderText = "Bollette";
	public static final String bolletteHeaderSubText = 	"Cerca una fornitura per numero cliente o indirizzo, per visualizzarne le bollette";
	public static final String bolletteDaPagareHeaderText = "Bollette da pagare";
	public static final String bolletteDaPagareHeaderSubText = 	"Seleziona le bollette che vuoi pagare e clicca su PROSEGUI."+
																"Gli importi effettivamente dovuti sono calcolati al netto di eventuali compensazioni.";
}
