package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CheckListComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	public final By builderChecklistOkButton = By.xpath("//div[@id='checklistFooter']" + "//button[text()='Conferma']");
	
	
	final By swaChekclistOkButton = By.xpath("//button[text()='OK' and contains(@onclick,'close')]");
	public final By checkListBodyText = By.xpath("//div[@id='bodyChecklistScript']//p");

	
	public CheckListComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(driver);
		PageFactory.initElements(driver, this);
	}

	// @FindBy(xpath="//iframe[@title = 'accessibility title'")
	List<WebElement> possibleFrames;

	public String clickFrameConferma2() throws Exception{
		driver.switchTo().defaultContent();
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, builderChecklistOkButton, util.scrollAndClick);
		spinner.checkSpinners(frame);
		return frame;
	}
	
	public String getChecklistText(By checklistBodyText) throws Exception{
		String frame = util.getFrameByIndex(1);
		String checklistText =util.frameManager(frame, checklistBodyText, util.getText);
		return checklistText;
	}
	
	public void clickConferma() throws Exception {
		boolean clicked = false;
		int tentativi = 5;
		util.getFrameActive();
		while(!clicked && tentativi >0) {
		try {
			util.waitAndGetElement(By.xpath("//div[@id='checklistFooter']//button[text()='Conferma']")).click();
			clicked = true;
		}catch(Exception e) {
			TimeUnit.SECONDS.sleep(10);
			tentativi--;
		}
		}
		if(!clicked) throw new Exception("Impossibile cliccare sul conferma checklist. verificare rallentamento ambiente");
	}
	
	
	public String clickOk() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, swaChekclistOkButton, util.scrollAndClick);
		return frame;
		
	}
	
	public void clickConferma(By xpathConferma, int idxFrame) throws Exception 
	{
		String frame = util.getFrameByIndex(idxFrame);
		
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		clickConferma(xpathConferma);
		driver.switchTo().defaultContent();	
	}
	
	public void clickConferma(By xpathConferma) throws Exception {
		util.waitUntilIsDisplayed(xpathConferma);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(xpathConferma).click();
		TimeUnit.SECONDS.sleep(5);
	}
	
	public void clickConfermaStattAccortOCosBianc(By xpathConferma) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,100);
		wait.until(ExpectedConditions.elementToBeClickable(xpathConferma));
		driver.findElement(xpathConferma).click();
		TimeUnit.SECONDS.sleep(5);
	}


}
