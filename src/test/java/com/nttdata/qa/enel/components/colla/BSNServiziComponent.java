package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSNServiziComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By pagePath = By.xpath("//div[@class='panel-body']//ol");
	public By pageTitle = By.xpath("//h1[text()='Servizi']");
	public By pageText = By.xpath("//h4[text()='In questa pagina trovi tutti i servizi.']");
	public By infoEnelEnergia = By.xpath("//p[contains(text(),'InfoEnelEnergia')]");
    public By prosegui = By.xpath("//a[@name='Prosegui']");
    public By systemMessage = By.xpath("//p[@class='leadbig']");
    public By errorMessage = By.xpath("//div[@class='panel-body']");
    public By bollettaWeb = By.xpath("//p[contains(text(),'Bolletta Web')]");
	
	public BSNServiziComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			/*System.out.println("Normalized:\n"+weText);
			System.out.println(text);
		*/	if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public static final String Path = "Area riservata Servizi";
		public static final String PageText = "In questa pagina trovi tutti i servizi.";
		public static final String SystemMessage = "Siamo spiacenti, ma non è possibile procedere con la richiesta. Riprova più tardi. Per maggiori informazioni contatta il Numero Verde 800 900 860";
		public static final String ErrorMessage = "Errore nella richiesta Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";

}
