package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ELightBiorariaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By breadcrumbs = By.xpath("//ul[@class='breadcrumbs component']");
	public By title = By.xpath("//h1[text()='E-Light Bioraria']");
	public By subTitle = By.xpath("//h2[contains(text(),'Comoda da gestire online, più conveniente di sera, nei weekend e festivi')]");
	public By buttonAttivaOfferta = By.xpath("//a[@href='/it/adesione-contratto?productType=res&zoneid=giustaperte_bioraria-hero']");
	public By caratteristiche = By.xpath("(//div[@data-module='services-price'])[1]");
	public By dettagli = By.xpath("(//div[@data-module='services-price'])[2]");
	public By documenti = By.xpath("(//div[@data-module='services-price'])[3]");
	public By questionList = By.xpath("(//div[@data-module='services-price'])[2]//child::section/section");
	public By documentList = By.xpath("(//div[@data-module='services-price'])[3]//child::section/section");
	public By documentiContrattoList = By.xpath("(//div[@data-module='services-price'])[3]//child::section/section[1]/div/div");
	public By documentiGeneraliList = By.xpath("(//div[@data-module='services-price'])[3]//child::section/section[2]/div/div");
	public By promo = By.xpath("//div[@class='plan-promo_content' and @id='promo-offert_results']");
	public By offer1 = By.xpath("//div[@class='offert parbase'][1]");
	public By offer2 = By.xpath("//div[@class='offert parbase'][2]");
	public By vediTutteLuce = By.xpath("//span[contains(text(),' Vedi Tutte Luce')]");
	
	public By headerLuceGas = By.xpath("//div[@aria-label='Link principali header']//li[1]/a");
	
	
	
	public ELightBiorariaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	public void checkQuestionsDetails() throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(questionList));
		 String option_found="NO";
		 
		 List<WebElement> questionsList = driver.findElements(questionList);
		 
			for (int i = 1; i <= questionsList.size(); i++) { 
				System.out.println(i);
				String domanda=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")).getText();
				util.jsClickElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"));
				Thread.currentThread().sleep(1000);
				WaitVar = new WebDriverWait(driver, 20);
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']")));
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']")));
			    String text=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']")).getText();
			    text= text.replaceAll("(\r\n|\n)", " ");
			    Thread.currentThread().sleep(500);
			    if (domanda.contentEquals(question1) && text.contains(question1Text))
			         option_found="YES";
			     else if(domanda.contentEquals(question2) && text.contains(question2Text))
			         option_found="YES";
			     else if (domanda.contentEquals(question3) && text.contains(question3Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question4) && text.contains(question4Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question5) && text.contains(question5Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question6) && text.contains(question6Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question7) && text.contains(question7Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question8) && text.contains(question8Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question9) && text.contains(question9Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question10) && text.contains(question10Text))
			     	 option_found="YES";
			     else if (domanda.contentEquals(question11) && text.contains(question11Text))
			     	 option_found="YES";
			     else  
			    	 option_found="NO";
			     util.jsClickElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']"));		
			     Thread.currentThread().sleep(1000);
			     WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")));
			
				 if (option_found.contentEquals("NO"))
						throw new Exception("Nella domanda " + domanda + " non è presente il testo " + text);
			}
	 }
	
	public void checkDocumentsDetails() throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(documentList));
		String option_found="NO";
		List<WebElement> questionsList = driver.findElements(documentList);		 
		for (int i = 1; i == questionsList.size(); i++) { 
			System.out.println(i);
			String text=driver.findElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")).getText();
		    if (text.contains(document1))
		         option_found="YES";
		    if(text.contains(document2))
		    	option_found="YES";
		    else  
		    	option_found="NO";
		    Thread.currentThread().sleep(1000);
			if (option_found.contentEquals("NO"))
				throw new Exception("Nella sezione Documenti non è presente il menù " + text);
			}
	 }
	
	public void checkDocumentiContratto() throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 String option_found="NO";
		 util.jsClickElement(By.xpath("//button[text()='Documenti contratto']"));
		 Thread.sleep(2000);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(documentiContrattoList));
		 List<WebElement> questionsList = driver.findElements(documentiContrattoList);
		 
			for (int i = 1; i <= questionsList.size(); i++) { 
				System.out.println(i);
				Thread.currentThread().sleep(1000);
				WaitVar = new WebDriverWait(driver, 20);
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@data-module='services-price'])[3]//child::section/section[1]/button[@aria-expanded='true']")));
			    String text=driver.findElement(By.xpath("(((//div[@data-module='services-price'])[3]//child::section/section[1]//child::div)[1]//child::div[@class='item-data'])["+i+"]")).getText();
			    text= text.replaceAll("(\r\n|\n)", " ");
			    Thread.currentThread().sleep(500);
			    if (text.contains("Scheda di confrontabilità Pdf size 0.29 mb DOWNLOAD"))
			         option_found="YES";
			    else if(text.contains("Condizioni tecnico economiche Pdf size 0.12 mb DOWNLOAD"))
			    	option_found="YES";
			    else  
			    	option_found="NO";				
				if (option_found.contentEquals("NO"))
					throw new Exception("In documenti contratto non è presente il testo " + text);
			}
	 }
	
	public void checkDocumentiGenerali() throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 String option_found="NO";
		 util.jsClickElement(By.xpath("//button[text()='Documenti generali']"));
		 Thread.sleep(2000);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(documentiGeneraliList));
		 List<WebElement> questionsList = driver.findElements(documentiGeneraliList);
		 
			for (int i = 1; i <= questionsList.size(); i++) { 
				System.out.println(i);
				Thread.currentThread().sleep(1000);
				WaitVar = new WebDriverWait(driver, 20);
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@data-module='services-price'])[3]//child::section/section[1]/button[@aria-expanded='true']")));
			    String text=driver.findElement(By.xpath("(((//div[@data-module='services-price'])[3]//child::section/section[2]//child::div)[1]//child::div[@class='item-data'])["+i+"]")).getText();
			    text= text.replaceAll("(\r\n|\n)", " ");
			    Thread.currentThread().sleep(500);
			    if (text.contains("Nota informativa per il cliente finale size 0.12 mb DOWNLOAD"))
			         option_found="YES";
			    else if(text.contains("Guida alla compilazione della dichiarazione di regolare possesso/detenzione dell'immobile Pdf size 0.68 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Certificato di residenza Pdf size 0.1 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Condizioni generali di fornitura Pdf size 0.15 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Modulo di adesione Pdf size 0.08 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Informazioni precontrattuali Pdf size 0.03 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Dichiarazione dei dati catastali Pdf size 0.43 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Informativa privacy Pdf size 0.63 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Modulo di ripensamento Pdf size 0.08 mb DOWNLOAD"))
			    	option_found="YES";
			    else if(text.contains("Modulo di addebito diretto Pdf size 0.11 mb DOWNLOAD"))
			    	option_found="YES";
			    else  
			    	option_found="NO";				
				if (option_found.contentEquals("NO"))
					throw new Exception("In documenti contratto non è presente il testo " + text);
			}
	 }
	
	public void containsAllText(By by, String text) throws Exception{
		String textWe = driver.findElement(by).getText();
		textWe= textWe.replaceAll("(\r\n|\n)", " ");
		if(!textWe.contains(text))
			throw new Exception("textWe " + textWe + "non corrisponde a " + text);
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = we.getText();		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		if (url.contains("https://"))
			url = url.substring(8);
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
	

		
	public String path = "HOME/LUCE E GAS/GIUSTAPERTE BIORARIA";
	public String caratteristicheTitle = "Caratteristiche";
	public String subCaratteristiche1 = "Sconto del 100% per il primo mese";
	public String subCaratteristiche2 = "L’energia giusta per te";
	public String subCaratteristiche3 = "Prezzo fisso per 1 anno";
	public String subCaratteristiche4 = "Rispetti l’ambiente";
	public String subCaratteristiche5 = "Sconti, bonus e promozioni";
	public String subCaratteristicheText1 = "Se aderisci a GiustaPerTe Bioraria, per il primo mese di fornitura avrai uno sconto in bolletta del 100% sul prezzo della componente energia. Il prezzo fisso si riferisce alla sola componente energia e rappresenta il 41% circa della spesa complessiva per l'elettricità (IVA e imposte escluse)";
	public String subCaratteristicheText2 = "Con GiustaPerTe Bioraria puoi risparmiare se consumi energia soprattutto di sera e nei weekend: in Fascia Blu (sabato, domenica, festivi e tutti i giorni dalle 19 alle 8) il prezzo è di 0,0824 €/kWh; in Fascia Arancione (da lunedì a venerdì, dalle 8 alle 19) il prezzo è di 0,1024 €/kWh. Il prezzo è valido fino al 8 Maggio 2019";
	public String subCaratteristicheText3 = "Il prezzo della componente energia resta uguale per 1 anno. Dopo i primi 12 mesi Enel Energia aggiorna i prezzi in base all'andamento del mercato e ti comunica i nuovi prima della scadenza. Se non desideri accettare i nuovi prezzi puoi recedere dal Contratto, in qualunque momento e senza oneri.";
	public String subCaratteristicheText4 = "Scegli un'offerta che rispetta l'ambiente, utilizzando solo l'energia certificata come prodotta da fonti rinnovabili come acqua, sole, vento e calore della terra, certificata dal nuovo sistema “Garanzie d’Origine” del Gestore Servizi Energetici (direttiva CE 2009/28/CE)";
	public String subCaratteristicheText5 = "Potrai iscriverti a enelpremia WOW!, il programma fedeltà dedicato ai clienti Enel Energia che ti sorprende ogni settimana con una nuova sorpresa, regalandoti tantissime opportunità di risparmio. Dall’app di Enel Energia, potrai scegliere coupon sconto da utilizzare presso partner aderenti all’iniziativa, partecipare a tanti concorsi con fantastici premi in palio e ottenere bonus sulle tue forniture grazie a iniziative speciali!";
	public String dettagliTitle = "Dettagli";
	public String question1 = "In quali casi è possibile attivare l’offerta?";
	public String question2 = "Come aderire all’offerta facendo il passaggio da un altro fornitore";
	public String question3 = "Quanto costa cambiare fornitore?";
	public String question4 = "Quando attivo l’offerta avrò disservizi di corrente?";
	public String question5 = "Bolletta Web o cartacea?";
	public String question6 = "Come si calcola il deposito cauzionale?";
	public String question7 = "Come vuoi pagare la bolletta?";
	public String question8 = "Da cosa è composto il prezzo dell'energia?";
	public String question9 = "Servizi SMS";
	public String question10 = "Come funziona il recesso da Enel?";
	public String question11 = "Come funziona il diritto di Ripensamento?";
	public String question1Text = "Puoi aderire GiustaPerTe Bioraria se provieni da un altro fornitore, se devi attivare un nuovo contatore (prima attivazione), riattivare un contatore disattivato (subentro) o se vuoi intestare a tuo nome un contratto intestato ad altra persona (voltura). ";
	public String question2Text = "Se provieni da un altro fornitore il passaggio per aderire a GiustaPerTe Bioraria è molto semplice. Ti basta sapere che:   l'attivazione è gratuita; non sono necessari interventi sugli impianti o sul contatore; il passaggio sarà effettivo in circa 2/3 mesi, tempo necessario per i controlli previsti dalla normativa vigente.   Per richiedere GiustaPerTe Bioraria ti servono soltanto:   una bolletta; il tuo codice fiscale; l'IBAN, se vuoi addebitare la bolletta sul tuo conto corrente.";
	public String question3Text = "Passare dal vecchio fornitore ad Enel Energia è semplice e non hai nessun costo.";
	public String question4Text = "Non ci sarà nessuna interruzione della corrente durante il passaggio dal vecchio fornitore ad Enel Energia.";
	public String question5Text = "Decidi tu come ricevere la bolletta. Hai a disposizione due opzioni: bolletta online o cartacea.   Con Bolletta Web ricevi le bollette in formato digitale direttamente nella tua casella e-mail. Dai così il tuo contributo all'ambiente riducendo l'uso di carta.   Riceverai la tua prima bolletta un mese dopo la data di attivazione del contratto. Le successive bollette ti verranno recapitate ogni due mesi. Solo per potenze impegnate superiori a 15kW le bollette sono emesse con riferimento a un mese di consumo.";
	public String question6Text = "Il deposito cauzionale, che ti verrà rimborsato nell'ultima bolletta di conguaglio, è calcolato sulla base della potenza del tuo contatore. Nei dettagli:   7,74 € per contatori da 1,5 kW 15,48 € per 3 kW 23,22 € per 4,5 kW 30,96 € per 6 kW Se dovessi recedere dal contratto, il rimborso del deposito è addizionato del tasso di interesse legale    ";
	public String question7Text = "Scegli tu come pagare la bolletta e cambia metodo di pagamento quando vuoi. Puoi pagare tramite:   Bollettino postale e carta di credito Se scegli il pagamento con bollettino postale in fase di adesione, puoi pagare le singole bollette anche con carta di credito attraverso la nostra Area Clienti oppure presso tutte le ricevitorie autorizzate Sisal e Lottomatica. Addebito su conto corrente Un modo veloce per avere sempre sotto controllo i tuoi pagamenti, rispettando l’ambiente senza l’invio di fatture cartacee. La data di addebito corrisponde al giorno della scadenza della bolletta. La bolletta arriva circa 15 giorni prima della scadenza, così hai tutto il tempo per verificarla. PayPal Con PayPal paghi la bolletta in modo semplice e sicuro direttamente dall’Area Clienti Enel Energia. Puoi eseguire il pagamento in qualsiasi momento dal tuo computer, da smartphone o tablet: i tuoi dati sono al sicuro, non vengono comunicati a terzi e sono protetti dai server PayPal.";
	public String question8Text = "ll prezzo indicato si riferisce alla componente energia e rappresenta il 41% circa della spesa complessiva per l’energia elettrica (IVA e imposte escluse) di un cliente domestico tipo residente con:   3 kW di potenza impegnata consumi annui pari a 2700 kWh Sono esclusi da questo prezzo i costi per il dispacciamento, i costi per il servizio di trasporto e gestione del contatore, gli oneri di sistema e i costi di commercializzazione, che sono applicati nella misura definita dall’ARERA e di volta in volta modificati e aggiornati.";
	public String question9Text = "Hai a disposizione molti servizi, attivabili via SMS, con cui ti comunicheremo:   quando è stata emessa la tua bolletta; quando sta per scadere; se l’ultima bolletta risulta pagata correttamente; quando puoi mandarci la lettura del tuo contatore. Gli SMS ricevuti sono gratuiti. Inoltre puoi inviare la lettura del tuo contatore e richiedere:   aggiornamenti sullo stato di lavorazione del tuo contratto; di ricevere le tue bollette via email; modifiche ai tuoi dati personali (indirizzo e-mail, telefono, indirizzo…); dove si trova il Negozio Enel più vicino. Il costo degli SMS inviati cambia a seconda del tuo piano tariffario.";
	public String question10Text = "Puoi recedere dal contratto con Enel in qualsiasi momento tramite il tuo fornitore, in occasione della stipula del nuovo contratto. Inoltre, puoi annullare la richiesta di attivazione della fornitura tramite raccomandata, senza oneri, entro 14 giorni dalla conclusione del contratto. Puoi trovare maggiori informazioni nel documento Attuali condizioni generali di fornitura.";
	public String question11Text = "Puoi esercitare il diritto di Ripensamento come da legge entro 14 giorni dalla conclusione del contratto, senza oneri aggiuntivi. Per inviare ad Enel la comunicazione di recessione dal contratto puoi utilizzare uno dei canali previsti:   online su questo sito via e-mail all’indirizzo: allegati.enelenergia@enel.com chiamando il numero verde 800.900.860 tramite fax al numero 800.046.311 via posta (semplice o raccomandata) a Enel Energia S.p.A.  C.P. 8080 – 85100 Potenza ";
	public String document1 = "Documenti contratto";
	public String document2 = "Documenti generali";
	public String promoText = "LUCE Illumina la tua casa con Enel Energia Scopri le nostre offerte Luce e trova la più conveniente per te VEDI TUTTE LUCE";



}
