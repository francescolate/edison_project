package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SurveyComponent {
	
	public By surveyTitle1 = By.xpath("//div[@class='survey-nps-container module--initialized']//h3[contains(text(),'La tua opinione è importante!')]");
	public By surveyTitle2 = By.xpath("//div[@class='survey-nps-fieldset-cont']/ancestor::section[@id='survey-nps-form']//child::p[@class='survey-nps-question']");
	public By inviaBtn = By.xpath("//section[@id='survey-nps-form']/descendant::div[@class='row button-container']/button[text()='Invia']");
	public By survey1 = By.xpath("//div[@class='survey-nps-legend']/ancestor::section[@id='survey-nps-form']//p[text()='1 - sicuramente NON raccomanderei']");
	public By survey10 = By.xpath("//div[@class='survey-nps-legend']/ancestor::section[@id='survey-nps-form']//p[text()='10 - sicuramente raccomanderei']");
	public By startLabel = By.xpath("//div[@class='set-survey-nps']/parent::fieldset/descendant::label/span[contains(@class,'survey-mobile-label')]");
	public By startInput = By.xpath("//div[@class='set-survey-nps']/parent::fieldset/descendant::input[@value='10']");
	public String starNum = "//div[@class='set-survey-nps']/parent::fieldset/descendant::input[@value='#']";
	public By errorMsgReq = By.xpath("//div[@class='survey-nps-error-container']//span[contains(text(),'Rispondi alla')]");
	public By surveryStars = By.xpath("//div[@class='survey-nps-fieldset-cont']/descendant::div[@class='set-survey-nps']//label");
	String surveryStarsNum = "//div[@class='set-survey-nps']/parent::fieldset/descendant::label[contains(@title,'#')]";
	public By surveySelectAll = By.xpath("//div[@class='survey-nps-container module--initialized']/descendant::span[@class='survey-mobile-label']/parent::label[@for='nps-star10']");
	public By tornaBtn = By.xpath("//section[@id='survey-nps-typ']/descendant::a[@class='full-btn']");
	public By headerprivato1 = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By headerprivato2 = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	public By titleprivato1 = By.xpath("//div[@class='cardWrapper']/parent::section[@id='lista-forniture']/descendant::h2[text()='Le tue forniture']");
	public By titleprivato2 = By.xpath("//div[@class='cardWrapper']/parent::section[@id='lista-forniture']/descendant::p[contains(text(),'Visualizza le informazioni')]");
	
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public SurveyComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	 public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
				if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		
	}
	
		public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
		public void verifyContent(By object,String text) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
		String value = driver.findElement(object).getText();
		value = value.replaceAll("(\r\n|\n,)","");
		if(!text.contentEquals(value))
			throw new Exception("Value is not matching with expected");
		
	}

		public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
			}
		}
		//verifying the star icon
		public void verifyIconDisplay(By starObject) throws Exception {
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(starObject));
			
			List<WebElement> stars = driver.findElements(starObject);
			for(int i=9 ; i>=0 ; i--){
				WebElement we = stars.get(i);
				if(!we.isDisplayed()){
					
					throw new Exception("Star icon is not displayed");
				}
				
			}
		}
		//clicking on star icon - Method 1 
		public void clickIcon()throws Exception {
				for(int i=10 ; i>=1 ; i--){
				String value = Integer.toString(i);
				//System.out.println(i);
				By starItem = By.xpath(surveryStarsNum.replace("#", value ));
				String s = String.valueOf(starItem);
				//System.out.println(s);
				WebElement we = driver.findElement(starItem);
				//System.out.println(we);
				we.click();
					
			}
			
		}
		//Clicking on star icon - Method 2
		public void clickIcon1() throws Exception {
			List<WebElement> stars = driver.findElements(surveryStars);
			for(int i=9 ; i>=0 ; i--){
			String att =stars.get(i).getAttribute("title");
			System.out.println(att);
				WebElement we1 = stars.get(i);
				//System.out.println(we1);
				//String ss = String.valueOf(we1);
				stars.get(i).click();
			}
			
		}
		public void waitForElementToDisplay (By checkObject) throws Exception{
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
	public final static String SURVEY_TITLE1 = "La tua opinione è importante!";
	public final static String SURVEY_TITLE2 = "Considerando la tua esperienza con Enel Energia, da uno a dieci quanto consiglieresti i nostri servizi ad un amico, parente o collega?";
	public final static String ERRORMSG_REQ = "Rispondi alla domanda per procedere all'invio";
	
	public final static String feedback_survey = "Abbiamo ricevuto il tuo feedback";
	public final static String feedback_title = "Grazie per il tempo che ci hai dedicato!";
	public final static String Header1_Privato = "Benvenuto nella tua area privata";
	public final static String Header2_Privato = "In questa sezione potrai gestire le tue forniture";
	
	public final static String Title1_Privato = "Le tue forniture";
	public final static String Title2_Privato = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	
	
			
	
}
