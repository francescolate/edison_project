package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class GiustaPerTeImpresaGasComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	//public By attivaOra=By.xpath("//div[@data-prodpath='/content/enel-it/it/luce-e-gas/gas/offerte/giustaperte-impresa-gas']//a[@href='/it/adesione' and normalize-space(text())='Attiva ora']");
	public By attivaOra=By.id("priceHeaderID");
	public By offertaLink = By.xpath("//a[@href='/it/luce-e-gas/gas/offerte/giustaperte-impresa-gas']");
	public By titoloPagina=By.xpath("//h1[text()='GiustaXTe Gas Impresa']");
	public By sottotitolo=By.xpath("//div[@id='adesione_standard']//p");
	public By pdrI=By.xpath("//a[@class='openPDR icon3-line-info2']");
	public By titoloPdrPopup=By.xpath("//div[@data-remodal-id='modalInfoPdr']//div[@class='remodal-content']//h4[@class='modal-title']");
	public By testoPdr=By.xpath("//div[@data-remodal-id='modalInfoPdr']//div[@class='remodal-content']//div[@class='modal-body']//div[@id='pdr-default']");
	public By chiudiPdr=By.xpath("//button[@aria-label='chiudi modale pdr' and @class='remodal-close close-AEM_SF internal-focus']");
	public By pagina=By.xpath("//div[@class='remodal-wrapper remodal-is-opened']");
	public By dettaglioOffertaButton=By.xpath("//a[@href='/it/luce-e-gas/gas/offerte/giustaperte-impresa-gas']//button[normalize-space(text())='Dettaglio Offerta']");
	public By attivaOraLink=By.xpath("//div[@id='priceHeaderID' and @data-prodpath='/content/enel-it/it/luce-e-gas/gas/offerte/giustaperte-impresa-gas']//a[normalize-space(text())='Attiva ora']");
	public By titolopaginaDettaglio=By.xpath("//h1[text()='GiustaPerTe Impresa Gas']");
	public By attivaoffertaButton=By.xpath("//a[@role='button' and normalize-space(text())='attiva l’offerta']");
	public By sezioneCaratteristiche=By.xpath("//div[@class='group parbase']//div[@data-module='services-price']//h3[@class='plan-main-head' and normalize-space(text())='Caratteristiche']");
	public By listacaratteristiche=By.xpath("//div[@class='item parbase']");
	public By tabDettagli=By.xpath("//li[@class='hub-secondary-nav_list-item']//a[@title='Dettagli' and text()='Dettagli']");
	public By tabDocumenti=By.xpath("//li[@class='hub-secondary-nav_list-item']//a[@title='Documenti' and text()='Documenti']");
	public By sezioneDettagliLabel=By.xpath("//h3[@class='plan-main-head' and normalize-space(text())='Dettagli']");
	public By listaDomandeSezioneDettagli=By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]");
	public By sezioneDocumentiLabel=By.xpath("//h3[@class='plan-main-head' and normalize-space(text())='Documenti']");
	public By documentiContratto=By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti contratto']");
	public By documentiGenerali=By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti generali']");
	public By linkDowload1=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-offerta/giustaxte/gas/GiustaPerTeGasImpresa_cte.pdf']");
	public By linkDowload2=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/CGF%20Business_mercato%20libero.pdf']");
	public By linkDowload3=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/nota_informativa.pdf']");
	public By linkDowload4=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/modulo%20di%20adesione%20Business_mercato%20libero.pdf']");
	public By linkDowload5=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/informativa-privacy.pdf']");
	public By linkDowload6=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/modulo_addebito_diretto.pdf']");
	
	public String sottotitle= "Ricorda che per completare l'adesione devi avere a portata di mano: Codice fiscale / PDR o matricola / Iban / Dati dell'azienda  L'offerta che hai scelto è valida solo per forniture ad uso non abitativo.";
	public String titoloPdr= "COS'E' IL PDR";
    public String descrizionepdr= "Il PDR (Punto di Riconsegna) è il codice che identifica la fornitura di gas. Il PDR: si compone di 14 caratteri numerici non cambia anche se cambi fornitore di gas Se la fornitura è già stata attiva lo trovi nella bolletta. La Matricola contatore è il codice univoco che identifica il contatore. Genericamente composta da 6-8 cifre, è sempre segnata sul contatore nei dati di targa.";
    
    public String linkGIUSTA_PER_TE="it/luce-e-gas/gas/offerte/giustaperte-impresa-gas";
//    public String linkPLUS_IMAGE="etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/plus.png";
//    public String linkMINUS_IMAGE="etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/minus.png";
    public String linkPLUS_IMAGE="plus.png";
    public String linkMINUS_IMAGE="minus.png";
    public String linkDOWNLOAD1="content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-offerta/giustaxte/gas/GiustaPerTeGasImpresa_cte.pdf";
    public String linkDOWNLOAD2="content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/CGF%20Business_mercato%20libero.pdf";
    public String linkDOWNLOAD3="content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/nota_informativa.pdf";
    public String linkDOWNLOAD4="content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/modulo%20di%20adesione%20Business_mercato%20libero.pdf";   
    public String linkDOWNLOAD5="content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/informativa-privacy.pdf";   
    public String linkDOWNLOAD6="content/dam/enel-it/documenti-offerte/documenti-impresa/documenti-generali/modulo_addebito_diretto.pdf";   
    
	public GiustaPerTeImpresaGasComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
		
	}
	public void clickComponent(By clickObject) throws Exception {
		util.waitAndGetElement(clickObject);
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickcomponentjavascript(By object) throws Exception {
		WebElement button=util.waitAndGetElement(object);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		Thread.currentThread().sleep(3000);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner();
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto con xpath " + existentObject + " non esiste.");
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void verificaTesto (By object,String testo) throws Exception {
		util.waitAndGetElement(object);
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
	//	System.out.println(text);
		if (!text.contentEquals(testo)) 
       	throw new Exception("la label di testo con descrizione "+testo+" non e' presente");
	
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void verificaSottotitolieTesti (By object) throws Exception {
	     String option_found="NO";
		 List<WebElement> sottotitoliList = driver.findElements(object);
		 
			for (int i = 1; i <= sottotitoliList.size(); i++) { 
				String titoli=driver.findElement(By.xpath("//div[@class='item parbase']["+i+"]//h3")).getText();
			//	System.out.println(titoli);
				String testo=driver.findElement(By.xpath("//div[@class='item parbase']["+i+"]//p")).getText();
				testo= testo.replaceAll("(\r\n|\n)", " ");
			//	System.out.println(testo);
				 if (titoli.contentEquals("Sconto del 100% per il primo mese") && testo.contentEquals("Con GiustaPerTe Impresa Gas hai uno sconto del 100% sul prezzo della componente materia prima gas per i consumi relativi al primo mese di fornitura")) 
				        option_found="YES";
				 else if(titoli.contentEquals("Prezzo fisso per 1 anno") && testo.contentEquals("Per la tua azienda un prezzo della materia prima gas bloccato per un anno: 0,3660 €/Smc. Il prezzo si riferisce alla sola componente materia prima gas (IVA e imposte escluse). Dopo i primi 12 mesi Enel Energia aggiorna il prezzo in base all'andamento del mercato e ti comunica il nuovo prezzo prima della scadenza"))
				        option_found="YES";
				 else  
				    	 option_found="NO";
				 if (option_found.contentEquals("NO"))
						throw new Exception("sulla pagina di dettaglio dell'offerta Giusta per te non sono presenti le caratteristiche desiderate");
			   }
			}
	
	 public void checkQuestionsDetails(By checkObject,String imageMinus, String imagePlus) throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		 String option_found="NO";
		 
		 List<WebElement> questionsList = driver.findElements(checkObject);
		 
			for (int i = 1; i == 5; i++) { 
				String domanda=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")).getText();
			//System.out.println("domanda"+domanda);
				
				 if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]"),imagePlus)!=true)
						throw new Exception("sulla pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Dettagli' ogni domanda NON e' affiancata dall'immagine +");
				 Thread.currentThread().sleep(1000);
				util.objectManager(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"), util.scrollAndClick);
				Thread.currentThread().sleep(3000);
				 WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']")));
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']")));
			     String text=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']")).getText();
			     text= text.replaceAll("(\r\n|\n)", " ");
			//    //System.out.println("dettaglio"+text);
			     Thread.currentThread().sleep(1000);
					     if (domanda.contentEquals("Come si attiva GiustaPerTe Impresa Gas?") && text.contentEquals("Possono aderire a GiustaPerTe Impresa Gas: tutti gli utenti con partita IVA (professionisti, imprese, rivenditori e commercianti) che provengono da un altro fornitore; i clienti con fornitura Usi Diversi da Abitazione. Per richiedere GiustaPerTe Impresa Gas basta tenere a portata di mano: il numero di partita IVA o codice fiscale; il codice PDR del tuo contatore; il codice IBAN, in caso di addebito su conto corrente. Attivare GiustaPerTe Impresa Gas è molto semplice: non è previsto alcun intervento né sugli impianti, né sul contatore; il passaggio avverrà nell’arco di 2/3 mesi, necessari per effettuare i controlli previsti dalla legge; l’attivazione è gratuita."))
					         option_found="YES";
					     else if (domanda.contentEquals("Come posso pagare la bolletta?") && text.contains("Decidi come pagare la bolletta e cambia metodo di pagamento quando vuoi. Puoi scegliere tra:   Bollettino postale e carta di credito In fase di adesione puoi pagare le singole bollette con carta di credito attraverso l’Area Clienti oppure presso tutte le ricevitorie autorizzate Sisal e Lottomatica. PayPal  Con PayPal paghi la bolletta in modo semplice e sicuro direttamente dall’Area Clienti Enel Energia. Puoi eseguire il pagamento in qualsiasi momento dal tuo pc, da smartphone o tablet e i tuoi dati sono al sicuro, non vengono comunicati a terzi e sono protetti dai server PayPal. Puoi cambiare metodo di pagamento quando vuoi. Addebito su conto corrente La bolletta arriva circa 15 giorni prima della scadenza così puoi verificarla. La data di addebito corrisponde al giorno della scadenza della bolletta. Con l’addebito su conto inoltre eviti code, perdite di tempo ed eventuali ritardi nella ricezione della bolletta."))
				             option_found="YES";
					     else if (domanda.contentEquals("Bolletta Web o cartacea?") && text.contentEquals("Decidi tu come ricevere la bolletta. Hai a disposizione due opzioni: bolletta online o cartacea.   Con Bolletta Web ricevi le bollette in formato digitale direttamente nella tua casella e-mail. Dai così il tuo contributo all'ambiente riducendo l'uso di carta.   Riceverai la tua prima bolletta un mese dopo la data di attivazione del contratto. La bolletta emessa fa riferimento a un solo mese di consumo."))
				             option_found="YES";
					     else if (domanda.contentEquals("Quali sono le condizioni economiche previste?") && text.contains("Il prezzo si riferisce alla sola componente materia prima gas che, per un cliente tipo non domestico (con consumi di gas fino a 200.000 Smc annui) rappresenta il 61% circa della spesa complessiva per il gas (Iva e imposte escluse). Il prezzo non comprende gli oneri per il servizio di trasporto e gestione del contatore, come stabilito dall’Autorità di Regolazione per Energia, Reti e Ambiente."))
				             option_found="YES";
					     else if (domanda.contentEquals("Come è possibile monitorare la propria fornitura?") && text.contains("Come è possibile monitorare la propria fornitura? Enel mette a disposizione dei propri clienti una serie di servizi SMS, che ti permettono di ricevere costanti aggiornamenti sullo stato dei pagamenti, eventuali avvisi e scadenze. In particolare ti comunicheremo:   quando è stata emessa la tua bolletta; quando sta per scadere; se l’ultima bolletta risulta pagata correttamente; quando puoi mandarci la lettura del tuo contatore. Gli SMS ricevuti sono gratuiti. Inoltre potrai inviare la lettura del tuo contatore e richiedere:   aggiornamenti sullo stato di lavorazione del tuo contratto; modifiche ai tuoi dati personali (indirizzo e-mail, telefono, indirizzo…); dove si trova il Negozio Enel più vicino. Il costo degli SMS inviati dipende dal tuo piano tariffario."))
				             option_found="YES";
					     else  
					    	 option_found="NO";
			     if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]"),imageMinus)!=true)
						throw new Exception("sulla pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Dettagli' ogni domanda che si apre dopo un click NON e' affiancata dall'immagine -");
			     Thread.currentThread().sleep(3000);
			     util.objectManager(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']"), util.scrollAndClick);
			     Thread.currentThread().sleep(1000);
			     WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")));
			
				 if (option_found.contentEquals("NO"))
						throw new Exception("nella pagina di dettaglio dell'offerta 'Giusta Per te Impresa Gas' nella sezione 'Dettagli' non sono presenti le 5 domande con il relativo testo o qualcosa e' cambiato nel testo della domanda o della sua risposta");
			}
			
			
	 }
	 
	 public void checkDocumenti(By checkObject, String imagePlus, String imageMinus) throws Exception {
		 String option_found="NO"; 
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		 
				if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]"),imagePlus)!=true)
					throw new Exception("sulla pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Documenti' la sezione 'Documenti Contratto' NON e' affiancata dall'immagine +");
				
				if (!util.exists(checkObject, 15))
					throw new Exception("l'oggetto 'Documenti contratto' con xpath '//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti contratto']' non esiste.");
				
				 util.objectManager(checkObject, util.scrollAndClick);
				 Thread.currentThread().sleep(3000);
				
			     if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]"),imageMinus)!=true)
						throw new Exception("sulla pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Documenti' la sezione 'Documenti Contratto' che si apre dopo un click NON e' affiancata dall'immagine -");	    	 
			     util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='true' and text()='Documenti contratto']"), util.scrollAndClick);
			    
		    	}
	 
	 
	 
	 public void dettagliDocumentiContratto (By checkObject) throws Exception {
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		 String option_found="NO";
		
		 util.objectManager(checkObject, util.scrollAndClick);
		 Thread.currentThread().sleep(1000);
					 List<WebElement> dettagliDocumentiList = driver.findElements(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']"));
					 
						for (int j = 1; j <= dettagliDocumentiList.size(); j++) { 
							 Thread.currentThread().sleep(1000);
							 WaitVar = new WebDriverWait(driver, 10);
							 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]")));
							
							String dettaglio=driver.findElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]")).getText();
							
							checkColor(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]//div[@class='item-data-download']//div"),"DarkGray","size document");
							checkColor(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]//div[@class='item-data-download']//a"),"DeepPink","link download document");
							
							
							dettaglio= dettaglio.replaceAll("(\r\n|\n)", " ");
				        //	System.out.println(dettaglio);
				         	
				         	 if (dettaglio.contentEquals("Condizioni tecnico economiche Pdf size 0.13 mb DOWNLOAD")) 
						         option_found="YES"; 
						     else if(dettaglio.contentEquals("Modulo di adesione per la fornitura Pdf size 0.57 mb DOWNLOAD"))
						         option_found="YES";
						     else if(dettaglio.contentEquals("Condizioni Tecnico Economiche Pdf size 0.26 mb DOWNLOAD"))
						         option_found="YES";
						     else if(dettaglio.contentEquals("Condizioni generali di fornitura size 0.66 mb DOWNLOAD"))
						         option_found="YES";
						     else  
						    	 option_found="NO";
				         	 if (option_found.contentEquals("NO"))
									throw new Exception("nella pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Documenti Contratto' non e' presente la lista di documenti desiderata");
				}
					
	 		}
	 
			 public void checkDocumentiGenerali(By checkObject, String imagePlus, String imageMinus) throws Exception {
				 String option_found="NO"; 
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 
						if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion][2]"),imagePlus)!=true)
							throw new Exception("sulla pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Documenti' la sezione 'Documenti Generali' NON e' affiancata dall'immagine +");
						
						if (!util.exists(checkObject, 15))
							throw new Exception("l'oggetto 'Documenti contratto' con xpath '//section[@data-module='document-accordion']//section[@data-accordion][2]//button[@aria-expanded='false' and text()='Documenti generali']' non esiste.");
						
						 util.objectManager(checkObject, util.scrollAndClick);
						 Thread.currentThread().sleep(2000);
						
					     if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion][2]"),imageMinus)!=true)
								throw new Exception("sulla pagina di dettaglio dell'offerta 'Giusta Per Te Impresa Gas' nella sezione 'Documenti' la sezione 'Documenti Contratto' che si apre dopo un click NON e' affiancata dall'immagine -");	    	 
					   //  util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='true' and text()='Documenti generali']"), util.scrollAndClick);
					    
				}

		   
}
