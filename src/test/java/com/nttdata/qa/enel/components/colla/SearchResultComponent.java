package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SearchResultComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By potenza = By.xpath("//a[contains(text(),'Potenza')]");
	public By date = By.xpath("//a[contains(text(),'Tensione e Potenza')]/preceding::span[1]");
	public By modificareText = By.xpath("//a[contains(text(),'Vuoi modificare')]");
	public By subText = By.xpath("//p[contains(text(),'Tutto quello')]");
	
	public SearchResultComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void handleAlert() {
		Alert alert = driver.switchTo().alert();
		String text = alert.getText();
	}
	
	public static final String Potenza = "Tensione e Potenza";
	public static final String Date = "maggio 04, 2020";
	public static final String Text = "Vuoi modificare la potenza del tuo contatore?";
	public static final String SubText = "Tutto quello che devi sapere per richiedere una modifica della potenza e/o tensione relative alla tua fornitura";

}
