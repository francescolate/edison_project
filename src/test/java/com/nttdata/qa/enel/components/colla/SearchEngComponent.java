package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SearchEngComponent {
    WebDriver driver;
    SeleniumUtilities util;
    
    private List<By> items;
    
    public By quickLinks = By.xpath("//*[contains(text(),'Quick links')]");
    public By bestPrice = By.xpath("//*[contains(text(),'The best prices power and gas')]");
    public By solution = By.xpath("//*[contains(text(),'The solutions for your business')]");
    public By manageSupply = By.xpath("//*[contains(text(),'Manage your supply')]");
    public By missSotries = By.xpath("//*[contains(text(),'miss our stories')]");
    public By workWithUs = By.xpath("//*[contains(text(),'Work with us')]");
    
    public SearchEngComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		this.items = new ArrayList<By>(); 
		this.items.add(this.quickLinks);
		this.items.add(this.bestPrice);
		this.items.add(this.solution);
		this.items.add(this.manageSupply);
		this.items.add(this.missSotries);
		this.items.add(this.workWithUs);
		util = new SeleniumUtilities(this.driver);
	}
    
    public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyMenuItems() throws Exception{
		for(By by: items){
			this.verifyComponentExistence(by);
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
}
