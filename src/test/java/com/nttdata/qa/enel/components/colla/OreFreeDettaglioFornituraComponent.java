package com.nttdata.qa.enel.components.colla;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OreFreeDettaglioFornituraComponent {
	WebDriver driver;
	SeleniumUtilities util;

	public By oreFreeButton = By.xpath("//a[text()='Gestisci le ore free']");
	public By fasciaBaseHeader = By.xpath("//h2[contains(text(), 'Fascia Base')]");
	public By fasciaBaseTextContainer = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div");
	public By fasciaOreFreeHeader = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div/parent::div//div[contains(text(),'Fascia Ore Free')]");
	public By fasciaOreFreeDayTimeHeader = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div/parent::div//div[contains(text(),'11')]");
	public By fasciaOreFreeModificaButton = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div/parent::div//a[text()='MODIFICA']");
	public By fasciaOreFreeModificaButtonNew = By.xpath("//a[text()='MODIFICA']");
	public By fasciaOreFreeModificaDialogHeader = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3");
	public By fasciaOreFreeModificaDialogParagraph = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3/following-sibling::p[contains(text(), 'Scegli')]");
	public By fasciaOreFreeModificaDialogFasciaImpostata = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3/following-sibling::div//div[@class='cm-col cm-col-medium print-new-settings']");	
	public By alleOreSelect = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//*[text()='alle ore:']/following-sibling::*//span[@class='selectboxit-text']");
	//public By alleOreSelect = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//span[@class='selectboxit  selectboxit-disabled selectboxit-btn']");
	public By dalleOreSelect = By.xpath("//span[@id='startingBaseSelectBoxIt']");
	//public By dalleOreSelect = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//span[@class='selectboxit  selectboxit-enabled selectboxit-btn']");
	public By dalleOreDailySelect = By.xpath("//span[@id='startingDailySelectBoxItContainer']");
	public By dalleOreSelectValue = By.xpath("//span[@id='startingBaseSelectBoxItText']");
	public By dalleOreDailySelectValue = By.xpath("//span[@id='startingDailySelectBoxItText']");
	public By alleOreValue = By.xpath("//span[text()='23:00']");
	public By annullaButton = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//div[@class='button-container ore-free-switch-button']//span[text()='Annulla']/parent::button");
	public By confermaButton = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//div[@class='button-container ore-free-switch-button']//span[text()='Conferma']/parent::button");
	public By chiudiButtonCambioFascia = By.xpath("//div[@data-remodal-id='modal-result-ok-today']//h3[contains(text(), 'La Fascia Base')]/parent::div/following-sibling::div[@class='button-container']");
	public By ripristinaFasciaBaseButton = By.xpath("//a[contains(text(), 'Ripristina Fascia Base')]");
	public By ripristinaFasciaBasePopupHeader = By.xpath("//div[@data-remodal-id='modal-reset-base-range']//div[@class='modal-header']");
	public By ripristinaFasciaBasePopupConfermaButton = By.xpath("//div[@data-remodal-id='modal-reset-base-range']//div[@class='modal-header']/..//span[text()='Conferma']/..");
	public By ripristinaFasciaBasePopupAnnullaButton = By.xpath("//div[@data-remodal-id='modal-reset-base-range']//div[@class='modal-header']/..//span[text()='Annulla']/..");
	public By fasciaBaseRipristinataPopupCloseButton = By.xpath("//div[@data-remodal-id='modal-result-ok']//h3[contains(text(), 'La tua richiesta')]/parent::div/following-sibling::div[@class='button-container']");
	public By fasceGiornaliereHeader = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/ancestor::div[@class='forniture-info-heading ']");
	public By ripristinaFasciaBaseFasceGiornaliereButton = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/following-sibling::a[@class='forniture-link forniture-side-link reset-fascia ore-free-switch-desktop modal-reset-base-range']");
	public By modificaFasciaGiornalieraButton = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box ']//a");
	public By modificaFasciaGiornalieraButtonNew = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/parent::div//following-sibling::div/div[3]/div[2]/a");
	public By modificaFasciaGiornalieraButtonNew4 = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/parent::div//following-sibling::div/div[4]/div[2]/a");
	public By modificaFasciaGiornalieraButtonNew5 = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/parent::div//following-sibling::div/div[5]/div[2]/a");
	public By modificaFasciaGiornalieraButtonDisabilitato = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box single-info-box-disabled']//a");
	public By fasciaGiornalieraModificata = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box ']//*[contains(text(),'20:00 - 23:00')]");
	public By chiudiButton = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//button");
	public By popup=By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3[text()='Imposta la tua Fascia Giornaliera']");
    public OreFreeDettaglioFornituraComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyComponentNotExistence(By existingObject) throws Exception {
		if (util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is exist.");
	}
	
	public void verifyModifiedComponentExistence(By by) throws Exception {
		String itemXpath = by.toString().replace("By.xpath: ", "");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM", Locale.ITALY);  
		LocalDateTime now = LocalDateTime.now();  
		String dayAndMonth = dtf.format(now);
		int day = Integer.parseInt(dayAndMonth.substring(0, dayAndMonth.indexOf(" ")))+1;
		String month = StringUtils.capitalize(dayAndMonth.substring(dayAndMonth.indexOf(" ")+1));
		by = By.xpath(itemXpath.replace("$DATE$", day+" "+month));
		if (!util.exists(by, 20))
			throw new Exception("object with xpath " + by + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("1)Normalized web text   2)Input argument:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within the item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	public boolean checkNodeValue(By by, String value) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		if(we.getText().equals(value))
			return true;
		else
			return false;
	}
	
	public void clickModificaComponentByDate(By by, int dayToBeAdded) throws Exception{
		String itemXpath = by.toString().replace("By.xpath: ", "");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM", Locale.ITALY);  
		LocalDateTime now = LocalDateTime.now();  
		String dayAndMonth = dtf.format(now);
		int day = Integer.parseInt(dayAndMonth.substring(0, dayAndMonth.indexOf(" ")))+dayToBeAdded;
		String month = StringUtils.capitalize(dayAndMonth.substring(dayAndMonth.indexOf(" ")+1));
		by = By.xpath(itemXpath.replace("$DATE$", day+" "+month));
		//System.out.println(by);
		if (util.exists(by, 15)) {
			util.objectManager(by, util.scrollToVisibility, false);
		    util.objectManager(by, util.scrollAndClick);
		}else
			throw new Exception("Fascia giornaliera non trovata.");
	}
	public void clickModificaComponentByDatePerFasciaDisabilitata(By by, int dayToBeAdded) throws Exception{
		String itemXpath = by.toString().replace("By.xpath: ", "");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM", Locale.ITALY);  
		LocalDateTime now = LocalDateTime.now();  
		String dayAndMonth = dtf.format(now);
		int day = Integer.parseInt(dayAndMonth.substring(0, dayAndMonth.indexOf(" ")))+dayToBeAdded;
		String month = StringUtils.capitalize(dayAndMonth.substring(dayAndMonth.indexOf(" ")+1));
		by = By.xpath(itemXpath.replace("$DATE$", day+" "+month));
		//System.out.println(by);
		if (util.exists(by, 15)) {
			util.objectManager(by, util.scrollToVisibility, false);
		    util.objectManager(by, util.scrollAndClick);
		    util.objectManager(by, util.scrollToVisibility, false);
		}else
			throw new Exception("Fascia giornaliera non trovata.");
	}
	
	public final static String fasciaBaseText = "Fascia Base La fascia di Ore Free di base impostata pertutti i giorni.Ricordati che il cambiofasciabasesarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura.";
	public final static String fasciaOreFreeHeaderText = "Fascia Ore Free";
	public final static String fasciaOreFreeDayTimeText = "11:00 - 14:00";
	public final static String fasciaOreFreeModificaDialogHeaderText = "Imposta la tua nuova Fascia Base";
	public final static String fasciaOreFreeModificaDialogParagraphText = "Scegli le Ore Free più adatte alle tue esigenze di consumo.";
	public final static String fasciaOreFreeModificaDialogFasciaImpostataText = "Hai impostato la fascia 11:00 - 14:00";
	public final static String ripristinaFasciaBasePopupHeaderText = "è stata aperta la modale Vuoi ripristinare la fascia base? Chiudi Vuoi ripristinare la fascia base? "+
																		"Confermando questa scelta su tutti i giorni, verrà ripristinata la Fascia Base rispetto alla programmazione giornaliera impostata precedentemente. Potrai comunque reimpostare le Ore Free giornaliere in un secondo momento.";
	public final static String fasceGiornaliereHeaderText = "Fasce Giornaliere Ripristina Fascia Base La fascia Ore Free che puoi impostare a piacimento per ciascuno deiprossimi 7 giorni.In alternativa verrà applicata la Fascia Base che hai scelto.Ricordati che il cambiofasciagiornalierasarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura. Ripristina Fascia Base";
}
