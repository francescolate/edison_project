package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ModifyConsents_BsnComponent extends BaseComponent {
	public By userLogo = By.id("avatarUtente");
	public By popupAvatar = By.id("modalAvatarImg");
	public By contactDataBtn = By.id("modificaProfiloLink");
	public By yourProfileBtn = By.name("Il tuo profilo");
	public By supplyGroupSelectionBtn = By.xpath("//a[@name='Prosegui' and contains(@href,'goToMoge')][1]");
	public By pathLbl1 = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > ol > li:nth-child(1) > a");
	public By pathLbl2 = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > ol > li.active");
	public By modPageTitle = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > h1");
	public By modPageSubtitle = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > p");
	public By modPageSection2Title = By.cssSelector("#main > section > div > div > section > div:nth-child(3) > div > div.row > div > p.leadbig.margin-top-15.margin-bottom-30");
	public By modPageSection2Subtitle = By.cssSelector("#main > section > div > div > section > div:nth-child(3) > div > div.row > div > p:nth-child(2)");
	public By giveConsentBtn = By.xpath("//a[@href=\"javascript:checkIOSInclution('/it/area-clienti/imprese/modifica_dati/dispositiva');\"]");
	public By giveConsentByNameBtn = By.xpath("//a[@name='Dai il tuo consenso']");
	public By consPagePathLbl1 = By.cssSelector("#sc > section > div.panel.panel-default.panel-clear > div > ol > li:nth-child(1)");
	public By consPagePathLbl2 = By.cssSelector("#sc > section > div.panel.panel-default.panel-clear > div > ol > li:nth-child(2)");
	public By consPageTitle = By.cssSelector("#sc > section > div.panel.panel-default.panel-clear > div > h1");
	public By consPageSubtitle = By.cssSelector("#sc > section > div.panel.panel-default.panel-clear > div > p");
	public By consPageQuestionLbl = By.cssSelector("#sc > section > div.panel.panel-default.panel-primary > div > div > div > div > form > div:nth-child(4) > div > p");
	public By consPageMailLbl = By.cssSelector("#sc > section > div.panel.panel-default.panel-primary > div > div > div > div > form > div.hidden-xxs > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1) > span");
	public By consPageTelLbl = By.cssSelector("#sc > section > div.panel.panel-default.panel-primary > div > div > div > div > form > div.hidden-xxs > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > span");
	public By profConsLbl = By.cssSelector("#sc > section > div.panel.panel-default.panel-primary > div > div > div > div > form > div.hidden-xxs > div:nth-child(2) > div > div > div > table > tbody > tr > td:nth-child(1) > span");
	public By privacyInfoBtn = By.xpath("//a[@href='#collapsePrivacy']");
	public By backBtn = By.name("Indietro");
	public By confirmBtn = By.xpath("//a[contains(@class,'btn btn-primary') and text()='Conferma']");
	public By mailConsentYesEE = By.xpath("//input[@id='consenso-enel-si--1']/following-sibling::span");
	public By mailConsentNoEE = By.xpath("//input[@id='consenso-enel-no--3']/following-sibling::span");
	public By mailConsentYesThirdParty = By.xpath("//input[@id='consenso-terzi-si--1']/following-sibling::span");
	public By mailConsentNoThirdParty = By.xpath("//input[@id='consenso-terzi-no--3']/following-sibling::span");
	public By telConsentYesEE = By.xpath("//input[@id='consenso-enel-si--3']/following-sibling::span");
	public By telConsentNoEE = By.xpath("//input[@id='consenso-enel-no--3']/following-sibling::span");
	public By telConsentYesThirdParty = By.xpath("//input[@id='consenso-terzi-si--3']/following-sibling::span");
	public By telConsentNoThirdParty = By.xpath("//input[@id='consenso-terzi-no--3']/following-sibling::span");
	public By profConsentYes = By.xpath("//input[@id='consenso-profilazione-si']/following-sibling::span");
	public By profConsentNo = By.xpath("//input[@id='consenso-profilazione-no']/following-sibling::span");
	public By infoPrivacyParentElement = By.cssSelector("#collapsePrivacy");
	public By privacyInfoLbl = By.cssSelector("#collapsePrivacy > p");
	public By[] mailEELocators = {mailConsentYesEE, mailConsentNoEE};
	public By[] telEELocators = {telConsentYesEE, telConsentNoEE};
	public By[] mailThirdPartyLocators = {mailConsentYesThirdParty, mailConsentNoThirdParty};
	public By[] telThirdPartyLocators = {telConsentYesThirdParty, telConsentNoThirdParty};
	public By[] profilingLocators = {profConsentYes, profConsentNo};
	public By[] allYesLocators = {mailConsentYesEE, telConsentYesEE, mailConsentYesThirdParty, telConsentYesThirdParty, profConsentYes};
	//User in sc. 36 has not a registered mail address
	public By[] allYesLocators36 = {telConsentYesEE, telConsentYesThirdParty, profConsentYes};
//	public By[] allNoLocators = {mailConsentNoEE, telConsentNoEE, mailConsentNoThirdParty, telConsentNoThirdParty, profConsentNo}; 
	public By[] allNoLocators = {mailConsentNoEE, telConsentNoEE, mailConsentNoThirdParty, telConsentNoThirdParty, profConsentNo}; 
	public By tyPathLbl1 = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > ol > li:nth-child(1)");
	public By tyPathLbl2 = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > ol > li.active");
	public By tyTitleLbl = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > h1");
	public By outcomeLbl = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear.panel-steps > div > p");
	public By tyDescriptionLbl = By.id("messageOK");
	public By endBtn = By.xpath("//a[@href='/it/area-clienti/imprese' and @title='Fine']");
	
	public ModifyConsents_BsnComponent(WebDriver driver) {
		super(driver);
	}
	
	public boolean checkGroupSelectionExistance() throws Exception {
		try {
			verifyComponentVisibility(supplyGroupSelectionBtn);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public void checkModPageStrings() throws Exception {
		By[] locators = {
				pathLbl1,
				pathLbl2,
				modPageTitle,
				modPageSubtitle,
				modPageSection2Title,
				modPageSection2Subtitle
		};
		verifyElementsArrayText(locators, modPageStrings);
	}
	
	public void checkConsPageStrings() throws Exception {
		By[] locators = {consPagePathLbl1, consPagePathLbl2, consPageTitle, consPageSubtitle};
		verifyElementsArrayText(locators, consPageStrings);
	}
	
	public void checkConsentsTable() throws Exception {
		By[] locators = {
				consPageMailLbl,
				consPageTelLbl,
				profConsLbl,
				mailConsentYesEE,
				mailConsentNoEE,
				mailConsentYesThirdParty,
				mailConsentNoThirdParty,
				telConsentYesEE,
				telConsentNoEE,
				telConsentYesThirdParty,
				telConsentNoThirdParty,
				profConsentYes,
				profConsentNo
		};
		for (int i = 0; i < locators.length; i++) {
			verifyComponentVisibility(locators[i]);
		}
	}
	
	public void checkInfoPrivacyInvisibility() throws Exception {
		WebElement collapsePrivacy = driver.findElement(infoPrivacyParentElement);
		String expandedString = collapsePrivacy.getAttribute("aria-expanded");
		if (expandedString.equals("true")) {
			throw new Exception("The privacy info text is still expanded after clicking on close button.");
		}
	}
	
	public void checkConsPageBottomElements() throws Exception {
		verifyVisibilityAndText(consPageQuestionLbl, consPageQuestionString);
		verifyComponentVisibility(backBtn);
		verifyComponentVisibility(confirmBtn);
	}
	
	public void modifyConsent(By[] locators) throws Exception {
		for (int i = 0; i < locators.length; i++) {
			verifyComponentVisibility(locators[i]);
		}
		Thread.sleep(2000);
		By precedingInputElement = new ByChained(locators[0], By.xpath("./preceding-sibling::input[not(@data-mobile-id)]"));
		WebElement checkBox = new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(precedingInputElement));
		By selectedLocator = null;
		boolean checked = checkBox.isSelected();
		//System.out.println("Checked: " + checked);
		if (checked) {
			//System.out.println("Checked. Premo: " + locators[1]);
			selectedLocator = locators[1];
		} else {
			//System.out.println("Unchecked. Premo: " + locators[0]);
			selectedLocator = locators[0];
		}
		clickComponent(selectedLocator);
	}
	
	public void selectAllYesOrNot(By[] locators) throws Exception {
		baseTimeoutInterval = 120;
		for (By locator: locators) {
			verifyVisibilityThenClick(locator);
		}
		baseTimeoutInterval = 20;
	}
	
	public void checkThankYouPageStrings() throws Exception {
		By[] locators = {
				tyPathLbl1,
				tyPathLbl2,
				tyTitleLbl,
				outcomeLbl,
				tyDescriptionLbl
		};
		verifyElementsArrayText(locators, tyPageStrings);
	}
	
	//----- Constants -----
	
	private String[] modPageStrings = {
			"AREA RISERVATA",
			"MODIFICA DATI",
			"Modifica dati di contatto",
			"In questa sezione sono riportati i dati di contatto che ci hai comunicato. Se desideri modificarli, integrarli o sostituirli, inserisci le nuove informazioni nei campi corrispondenti.",
			"Comunica il tuo consenso",
			"Vuoi essere sempre informato sulle nuove offerte di Enel Energia e dei suoi partner? Lascia il tuo consenso per scoprire tutti i vantaggi a te riservati!"
	};
	public String consentsPageUrl = "it/area-clienti/imprese/modifica_dati/privacy";
	private String[] consPageStrings = {
			"AREA RISERVATA",
			"GESTIONE PRIVACY",
			"Comunica il tuo consenso",
			"In questa sezione puoi gestire i tuoi consensi. Modifica i tuoi consensi e clicca su conferma."
	};
	private String consPageQuestionString = "Vuoi procedere con la modifica?";
	public String privacyInfoString = "Lei potrà liberamente decidere se acconsentire all'utilizzo dei suoi Dati Personali per finalità di marketing e profilazione. Il consenso per le predette finalità è facoltativo e non impedisce la possibilità di fruire dei prodotti o servizi offerti da Enel Energia. In particolare, lei potrà liberamente decidere se acconsentire all'utilizzo dei suoi dati di contatto: 1. Consenso a Enel Energia - per il compimento da parte di Enel Energia S.p.a. di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing e per l'invio di materiale pubblicitario effettuato sia mediante modalità automatizzate di contatto ( es. SMS, e-mail, telefonate senza operatore) che mediante modalità tradizionali di contatto (es. posta cartacea, telefonate con operatore). 2. Consenso Terzi - per le finalità descritte al punto precedente perseguite da società del Gruppo Enel, società controllanti, controllate, collegate o a partner commerciali di Enel Energia sia mediante modalità automatizzate di contatto (es. SMS, e-mail, telefonate senza operatore)che mediante modalità tradizionali di contatto (es. posta cartacea, telefonate con operatore) 3. Consenso Profilazione - da parte di Enel Energia o altre Società del Gruppo Enel, società controllanti, controllate, collegate o partner commerciali di Enel Energia, per attività di profilazione basata sulle sue abitudini di consumo, sui suoi dati personali e le informazioni acquisite osservando l'utilizzo dei prodotti o servizi offerti, al fine di proporle offerte personalizzate. Il consenso da lei eventualmente prestato al trattamento dei suoi dati personali per la predetta finalità di marketing effettuato mediante l'utilizzo di sistemi automatizzati di contatto (es. e-mail), si intenderà riferito anche all'utilizzo delle modalità tradizionali di contatto (es. telefonata con operatore). Parimenti, l'eventuale diritto di opposizione al trattamento dei suoi dati personali per la medesima finalità di marketing, effettuato mediante l'utilizzo di sistemi automatizzati di contatto, si estenderà anche all'utilizzo di modalità tradizionali di contatto, fatta salva la possibilità, a lei riconosciuta, di esercitare tale diritto solo in parte.";
	private String[] tyPageStrings = {
			"AREA RISERVATA",
			"COMUNICA IL TUO CONSENSO",
			"Comunica il tuo consenso",
			"Operazione eseguita correttamente",
			"Grazie per aver modificato lo stato del tuo consenso. Sarai aggiornato sulle ultime novità e promozioni di Enel Energia"
	};
}
