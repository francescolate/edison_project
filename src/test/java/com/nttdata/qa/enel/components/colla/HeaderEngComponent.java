package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class HeaderEngComponent {
    WebDriver driver;
    SeleniumUtilities util;
    
    private List<By> items;
    
    public By powerAndGas = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']//li/a[contains(text(), 'Power and gas')]");
    public By business = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']//li/a[contains(text(), 'Business')]");
    public By enelPremiaWow = By.xpath("//a[contains(text(), 'ENELPREMIA WOW!')]");
    public By stories = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']//li/a[contains(text(), 'Stories')]");
    public By future = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']//li/a[contains(text(), 'Futur-e')]");
    public By media = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']//li/a[contains(text(), 'Media')]");
    public By support = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']//li/a[contains(text(), 'Support')]");
    
    public HeaderEngComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		this.items = new ArrayList<By>(); 
		this.items.add(this.powerAndGas);
		this.items.add(this.business);
		this.items.add(this.enelPremiaWow);
		this.items.add(this.stories);
		this.items.add(this.future);
		this.items.add(this.media);
		this.items.add(this.support);
		util = new SeleniumUtilities(this.driver);
	}
    
    public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyHeaderItems() throws Exception{
		for(By by: items){
			this.verifyComponentExistence(by);
		}
	}
}
