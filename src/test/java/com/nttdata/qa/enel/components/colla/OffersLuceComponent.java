package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OffersLuceComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By labelElight=By.xpath("//*[text()='Valore Luce Plus']");
	public By discovertheofferButton=By.xpath("//div[@id='cbBannerId']//div[@class='cmEvidenceBanner_info']//a");
	public By nobannerPromotionalmessage =By.xpath("//div[@class='anchor plan-promo parbase']//section[@class='plan-promo plan-promo-bg-white']//div[@id='promo-offert_results']");
	public By labelLuceperlacasa=By.xpath("//div[@class='anchor plan-promo parbase']//section[@class='plan-promo plan-promo-bg-white']//div[@id='promo-offert_results']//span[@class='eyebrow' and text()='Luce per la casa']");
	public By labelLuceperlacasa_2=By.xpath("(//span[@class='eyebrow' and text()='Luce per la casa'])[1]");
	public By labelTitle=By.xpath("//h2[@class='title' and text()='Dai più luce alla tua casa con Enel Energia']");
	public By labelDescription=By.xpath("//div[@id='promo-offert_results']//p[@class='description']");
	public By link=By.xpath("//div[@class='anchor plan-promo parbase']//section[@class='plan-promo plan-promo-bg-white']//div[@id='promo-offert_results']//div//a//span[@class='text' and text()=' Vedi tutte luce']");
	public By vediTutteLuce=By.xpath("//span[contains(text(),'Vedi tutte luce')]");
	public By offersList=By.xpath("//div[@class='plan-promo_inner container plan-promo_custom plan-promo-wrapper manage-block']//div[@class='plan-promo_plan-holder']");
	public By titoliListaOfferte=By.xpath("//div[@class='plan-promo_inner container plan-promo_custom plan-promo-wrapper manage-block']//div[@class='plan-promo_plan-holder']//li//h3");
	public By labelInEvidenza=By.xpath("//div[@id='cbBannerId']//div[@class='cmEvidenceBanner_title']//h3[text()='In evidenza']");
	public By bannerGiustaPerTeBioraria = By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/giustaperte-bioraria']//ancestor::li");
	public By bannerELightBioraria = By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/e-light-bioraria']");
	public By attivaOraGiustaPerTeBioraria = By.xpath("//a[@href='/content/enel-it/it/adesione-contratto?productType=res' and contains(@aria-label,'Attiva ora GiustaPerTe Bioraria')]");
	public By scegliTu100x100 = By.xpath("//a[@href='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-100x100']");
	
	public String daiPiuLuce="Dai più luce alla tua casa con Enel Energia";
	public String descrizione="Illumina la tua casa, è semplice: scopri le offerte più adatte ai tuoi consumi. Puoi scegliere un prezzo più conveniente se consumi più di sera o nei weekend o bloccare un prezzo vantaggioso fisso per tutto il giorno. Dai nuova luce alla tua casa!";
	
//	public String SEQUENZA_OFFERTE_IN_PROMOZIONE= "Speciale Luce 25;Valore Luce Plus;Luce 20 Amazon;Luce 30 Spring;Ore Free;GiustaPerTe Bioraria;Pagina TEST Luce 60;Enel Energia PLACET Fissa Luce Consumer;Speciale Luce 60;Remote control;Per Noi E-Light Luce;Open Energy Smart;Open Energy Green;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light Bioraria;Speciale Luce 25;Enel Energia PLACET Variabile Luce Consumer;E-Light;Sempre Con Te;EnergiaX65;GiustaPerTe";
	public String SEQUENZA_OFFERTE_IN_PROMOZIONE= "Luce 30 Spring;Valore Luce Plus;Enel One;Scegli Tu 100x100;Speciale Luce 60;Ore Free;Enel Energia PLACET Variabile Luce Consumer;Enel Energia PLACET Fissa Luce Consumer;Pagina TEST Luce 60;Remote control;Open Energy Green;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light;E-Light Bioraria;GiustaPerTe;GiustaPerTe Bioraria;EnergiaX65";
	
	//public String SEQUENZA_OFFERTE_MONO= "Open Energy Smart;Luce 30 Spring;Pagina TEST Luce 60;Open Energy Mono;Speciale Luce 25;Per Noi E-Light;Open Energy Green;E-Light;Valore Luce Plus;Luce 20 Volagratis;Enel Energia PLACET Fissa Luce Consumer;Ore Free;Remote control;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light Bioraria;Speciale Luce 25;Enel Energia PLACET Variabile Luce Consumer;Luce 20 Amazon;Sempre Con Te;EnergiaX65;GiustaPerTe Bioraria";
	public String SEQUENZA_OFFERTE_MONO= "Pagina TEST Luce 60;Enel Energia PLACET Fissa Luce Consumer;Enel Energia PLACET Variabile Luce Consumer;E-Light;Valore Luce Plus;Open Energy Green;Luce 30 Spring;GiustaPerTe;Enel One;Scegli Tu 100x100;Ore Free;Remote control;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light Bioraria;GiustaPerTe Bioraria;EnergiaX65";
	//public String SEQUENZA_OFFERTE_BIO="Sempre Con Te;E-Light Bioraria;GiustaPerTe Bioraria;Pagina TEST Luce 60;Luce 20 Volagratis;Speciale Luce 25;Enel Energia PLACET Fissa Luce Consumer;Ore Free;Remote control;Per Noi E-Light;Open Energy Smart;Open Energy Mono;Open Energy Green;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light;Valore Luce Plus;Luce 30 Spring;Speciale Luce 25;Enel Energia PLACET Variabile Luce Consumer;Luce 20 Amazon;EnergiaX65";
	public String SEQUENZA_OFFERTE_BIO="Ore Free;GiustaPerTe Bioraria;Sempre Con Te;E-Light Bioraria;Luce 20 Amazon;Pagina TEST Luce 60;Speciale Luce 25;Enel Energia PLACET Fissa Luce Consumer;Speciale Luce 60;Remote control;Per Noi E-Light Luce;Open Energy Smart;Open Energy Green;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;Valore Luce Plus;Luce 30 Spring;Speciale Luce 25;Enel Energia PLACET Variabile Luce Consumer;E-Light;EnergiaX65;GiustaPerTe";
	//public String[] offerteInPromozione= {"Speciale Luce 25","Valore Luce Plus","Luce 30 Spring","Luce 20 Amazon","Pagina TEST Luce 60","Luce 20 Volagratis","Enel Energia PLACET Fissa Luce Consumer","Ore Free","Remote control","Per Noi E-Light","Open Energy Smart","Open Energy Mono","Open Energy Green","Energy management system","Energy Fingerprint","E-Light New","E-Light Bioraria New","E-Light","E-Light Bioraria","Speciale Luce 25","Enel Energia PLACET Variabile Luce Consumer","Sempre Con Te","EnergiaX65","GiustaPerTe Bioraria"};
	//public String[] offerteMono= {"Open Energy Smart","Luce 30 Spring","Pagina TEST Luce 60","Open Energy Mono","Speciale Luce 25","Per Noi E-Light","Open Energy Green","E-Light","Valore Luce Plus","Luce 20 Volagratis","Enel Energia PLACET Fissa Luce Consumer","Ore Free","Remote control","Energy management system","Energy Fingerprint","E-Light New;E-Light Bioraria New","E-Light Bioraria","Speciale Luce 25","Enel Energia PLACET Variabile Luce Consumer","Luce 20 Amazon","Sempre Con Te","EnergiaX65;GiustaPerTe Bioraria"};
	//public String[] offerteBio= {"Sempre Con Te","E-Light Bioraria","GiustaPerTe Bioraria","Pagina TEST Luce 60","Luce 20 Volagratis","Speciale Luce 25","Enel Energia PLACET Fissa Luce Consumer","Ore Free","Remote control","Per Noi E-Light","Open Energy Smart","Open Energy Mono","Open Energy Green","Energy management system","Energy Fingerprint","E-Light New","E-Light Bioraria New","E-Light","Valore Luce Plus","Luce 30 Spring","Speciale Luce 25","Enel Energia PLACET Variabile Luce Consumer","Luce 20 Amazon","EnergiaX65"};
	public String[] offerteInPromozione= {"Speciale Luce 25","Valore Luce Plus","Luce 30 Spring","Luce 20 Amazon","Pagina TEST Luce 60","Luce 20 Volagratis","Enel Energia PLACET Fissa Luce Consumer","Ore Free","Remote control","Per Noi E-Light","Open Energy Smart","Open Energy Mono","Open Energy Green","Energy management system","Energy Fingerprint","E-Light New","E-Light Bioraria New","E-Light","E-Light Bioraria","Speciale Luce 25","Enel Energia PLACET Variabile Luce Consumer","Sempre Con Te","EnergiaX65","GiustaPerTe Bioraria"};
	public String[] offerteMono= {"Luce60","OpenEnergyMono","ValoreLuceplus","PerNoiE-Light","OpenEnergyGreen","OpenEnergyDigital","EnergyFingerprint","OpenEnergyGreen","E-light","GiustaPerTe","EnergiaPuraProtezioneLuce360","ScegliTuOggi","EnelOne","ScegliTU100x100","OreFree","RemoteControl","Energymanagementsystem","E-LightNew","E-LightBiorariaNew","OpenEnergyGreenMobility","E-lightBioraria","GiustaPerTeBioraria","SempreconTe","EnelEnergiaPLACETFissaLuceConsumer","EnelEnergiaPLACETVariabileLuceConsumer","EnergiaX65"};
	public String[] offerteBio= {"OreFree","GiustaPerTeBioraria","SempreconTe","E-lightBioraria","EnergiaPuraProtezioneLuce360","ScegliTuOggi","EnelOne","ValoreLuceplus","ScegliTU100x100","PaginaTestLuce60","E-Light","RemoteControl","PerNoiE-Light","OpenEnergySmart","OpenEnergyMono","OpenEnergyGreen","OpenEnergyGreen","OpenEnergyDigital","Energymanagementsystem","EnergyFingerprint","E-LightNew","E-LightBiorariaNew","E-Light","OpenEnergyGreenMobility","GiustaPerTe","EnelEnergiaPLACETFissaLuceConsumer","EnelEnergiaPLACETVariabileLuceConsumer","EnergiaX65"};
	
	public OffersLuceComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("object with xpath " + existentObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void verifyTextButton (By Button) throws Exception {
		String text=driver.findElement(Button).getText();
		
		if (!text.contentEquals("SCOPRI L'OFFERTA"))
			throw new Exception("il button 'SCOPRI L'OFFERTA' per l'offerta e-light non e' presente");
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
		//System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void verifyTextLabel (By label, String textlabel)throws Exception {
		String text=driver.findElement(label).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
	
		if (!text.contentEquals(textlabel)) 
	        	throw new Exception("la label di testo con descrizione "+textlabel+" non e' presente sotto la sezione dell'offerta in evidenza");
		}
	
		
	public void verifyOfferList (By listOffer, String filtro, String ordineOfferte)throws Exception {
		String description="";
		String elenco ="";
		String offersListfound="";
		
		  List <WebElement> list = driver.findElements(listOffer);
		   
			for (int i = 1; i <= list.size(); i++) {
			 description = driver.findElement(By.xpath("//div[@class='plan-promo_inner container plan-promo_custom plan-promo-wrapper manage-block']//div[@class='plan-promo_plan-holder']//li["+i+"]//h3")).getText();
		//	System.out.println(description);
			 if (filtro.contentEquals("In Promozione")) {
				// //System.out.println("prima lista");
					if (!description.contentEquals("Valore Luce Plus") && !description.contentEquals("Luce 30 Spring") && !description.contentEquals("Enel One")&& !description.contentEquals("Scegli Tu 100x100")&& !description.contentEquals("Ore Free")&& !description.contentEquals("Enel Energia PLACET Variabile Luce Consumer")&& !description.contentEquals("Enel Energia PLACET Fissa Luce Consumer")&& !description.contentEquals("Pagina TEST Luce 60")&& !description.contentEquals("Remote control")&& !description.contentEquals("Open Energy Green") && !description.contentEquals("Energy management system")&& !description.contentEquals("Energy Fingerprint")&& !description.contentEquals("E-Light New")&& !description.contentEquals("E-Light Bioraria New")&& !description.contentEquals("E-Light")&& !description.contentEquals("E-Light Bioraria")&& !description.contentEquals("GiustaPerTe")&& !description.contentEquals("GiustaPerTe Bioraria")&& !description.contentEquals("EnergiaX65") && !description.contentEquals("Enel Energia PLACET Variabile Luce Consumer")&& !description.contentEquals("E-Light")&& !description.contentEquals("Sempre Con Te")&& !description.contentEquals("EnergiaX65")&& !description.contentEquals("GiustaPerTe")) 	
						offersListfound="NO";
					  
					
				}
			else if (filtro.contentEquals("Monoraria")) {
				//	 //System.out.println("seconda lista");
					if (!description.contentEquals("Pagina TEST Luce 60") && !description.contentEquals("Enel Energia PLACET Fissa Luce Consumer") && !description.contentEquals("Enel Energia PLACET Variabile Luce Consumer") && !description.contentEquals("E-Light") && !description.contentEquals("Valore Luce Plus") &&!description.contentEquals("Open Energy Green")&& !description.contentEquals("Luce 30 Spring")&& !description.contentEquals("GiustaPerTe") && !description.contentEquals("Enel One")&& !description.contentEquals("Scegli Tu 100x100")&& !description.contentEquals("Ore Free")&& !description.contentEquals("Remote control")&& !description.contentEquals("Energy management system")&& !description.contentEquals("Energy Fingerprint")&& !description.contentEquals("E-Light New")&& !description.contentEquals("E-Light Bioraria New")&& !description.contentEquals("E-Light Bioraria")&& !description.contentEquals("GiustaPerTe Bioraria")&& !description.contentEquals("EnergiaX65")&& !description.contentEquals("Speciale Luce 25")&& !description.contentEquals("Enel Energia PLACET Variabile Luce Consumer") && !description.contentEquals("E-Light")&& !description.contentEquals("Sempre Con Te") && !description.contentEquals("EnergiaX65")) 
						offersListfound="NO";
					  
					
				}
			 
			else if (filtro.contentEquals("Bioraria")) {
					// System.out.println("description: " + description);
				    //Ore Free;GiustaPerTe Bioraria;E-Light Bioraria;Enel One;Scegli Tu 100x100;Enel Energia PLACET Variabile Luce Consumer;Enel Energia PLACET Fissa Luce Consumer;Pagina TEST Luce 60;Remote control;Open Energy Green;Energy management system;Energy Fingerprint;E-Light New;E-Light Bioraria New;E-Light;Valore Luce Plus;GiustaPerTe;EnergiaX65
					
				if (!description.contentEquals("Ore Free") && !description.contentEquals("GiustaPerTe Bioraria") && !description.contentEquals("Enel One") && !description.contentEquals("Scegli Tu 100x100") && !description.contentEquals("Enel Energia PLACET Variabile Luce Consumer") && !description.contentEquals("Enel Energia PLACET Fissa Luce Consumer") &&!description.contentEquals("Pagina TEST Luce 60")&& !description.contentEquals("Remote control")&& !description.contentEquals("Open Energy Green") && !description.contentEquals("Energy management system")&& !description.contentEquals("Energy Fingerprint")&& !description.contentEquals("E-Light New")&& !description.contentEquals("E-Light Bioraria New")&& !description.contentEquals("E-Light")&& !description.contentEquals("Valore Luce Plus") && !description.contentEquals("GiustaPerTe")&& !description.contentEquals("EnergiaX65")&& !description.contentEquals("Valore Luce Plus")&& !description.contentEquals("Luce 30 Spring") && !description.contentEquals("Speciale Luce 25")&& !description.contentEquals("Enel Energia PLACET Variabile Luce Consumer")&& !description.contentEquals("E-Light") && !description.contentEquals("E-Light Bioraria")&& !description.contentEquals("Luce 30 Spring")) 
						offersListfound="NO";
					  
					
				}
			 
			 if (offersListfound.contentEquals("NO"))
			        throw new Exception("le offerte ricercate per il filtro impostato "+filtro+" non sono presenti nella sezione offerte nell'ordine seguente "+ordineOfferte);    		
				
			}
			
	}
	
	
	public void verifyOfferList2(By listOffer, String[] ordineOfferte)throws Exception {
		List <WebElement> list = driver.findElements(listOffer);
		List <String> listOfferString = new ArrayList();
		for (int i=0; i<list.size();i++){
			System.out.println("listOffer " + list.get(i).getText());
			listOfferString.add(list.get(i).getText());
		}
		//String description = driver.findElement(By.xpath("//div[@class='plan-promo_inner container plan-promo_custom plan-promo-wrapper manage-block']//div[@class='plan-promo_plan-holder']//li["+i+"]//h3")).getText();
		if (listOfferString.contains(ordineOfferte))
			System.out.println("OK");
		else
			System.out.println("KO");
		System.out.println("listOfferString " + listOfferString);
		System.out.println("ordineOfferte " + ordineOfferte);

	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = we.getText();		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	
	
}
