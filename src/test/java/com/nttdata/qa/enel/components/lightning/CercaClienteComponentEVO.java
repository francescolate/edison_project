package com.nttdata.qa.enel.components.lightning;


import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
//import javafx.scene.control.Spinner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;


public class CercaClienteComponentEVO extends BaseComponent {

    private WebDriver driver;
    private SeleniumUtilities util;
    private SpinnerManager spinner;


    /**
     *
     * @param driver
     */
    public CercaClienteComponentEVO(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.util = new SeleniumUtilities(driver);
        this.spinner = new SpinnerManager(driver);
    }

    private By cf = By.xpath("//input[@id='NE__VAT__c' and @type='text']");
    private By cfReferente = By.xpath("//input[@id='ITA_IFM_CF__c' and @type='text']");
    private By ragSociale = By.xpath("//input[@id='Name' and @type='search']");
    private By searchBtn = By.xpath("//button[@id='searchClients']");
    private By frameBy = By.xpath("//iframe[@title = 'accessibility title']");
    private By byPass = By.xpath("//button[@id='newBypassBtn']");
    public By tuttiIProcessi = new By.ByXPath("//button[text() = 'Tutti i processi']");


    private void swichToDefault() throws Exception {
        this.driver.switchTo().defaultContent();
    }


    /**
     *
     * @param param
     * @throws Exception
     */
    @Deprecated
    public void fillCfAndSearch(String param) throws Exception {
        TimeUnit.SECONDS.sleep(20);
        String x = util.frameSearcher(
                frameBy,
                cf,
                r -> {
                    r.sendKeys(param);
                }
        );
        this.driver.switchTo()
                .frame(x)
                .findElement(searchBtn)
                .click();
        this.swichToDefault();

    }

    /**
     *
     * la Ragione Sociale passata come parametro
     *
     * @param param
     * @throws Exception
     */
    public void fillCfAndSearch2(String param) throws Exception {
        util.getFrameActive();
        util.objectManager(cf, util.sendKeys, param);
        util.getFrameActive();
        util.objectManager(searchBtn, util.click);
        checkSpinnersSFDC();
    }


    /**
     * Ricerca la Ragione Sociale passata come parametro
     *
     * @param param
     * @throws Exception
     */
    public void fillCfAndSearchRagSociale(String param) throws Exception {
        TimeUnit.SECONDS.sleep(15);
        String frame = util.getFrameByIndex(0);
        util.frameManager(frame, ragSociale, util.sendKeys, param);
        util.frameManager(frame, searchBtn, util.scrollAndClick);
    }

    /**
     *
     * @throws Exception
     */
    public void byPassIdenticazione() throws Exception {
        TimeUnit.SECONDS.sleep(10);
        if (!util.exists(tuttiIProcessi, 10)) {
            String frame = util.getFrameByIndex(0);
            util.frameManager(frame, byPass, util.scrollAndClick);
        }
    }
}
