package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FibreOkComponent extends BaseComponent {
	public By pagePath1 = By.cssSelector("#main > section > div > nav > ul > li:nth-child(1) > a");
	public By pagePath2 = By.cssSelector("#main > section > div > nav > ul > li:nth-child(2) > a");
	public By pageTitle = By.cssSelector("#main > section > div > div > h1");
	public By pageSubTitle = By.cssSelector("#main > section > div > p");
	//public By descrUpperHeader = By.cssSelector("#main > div > div:nth-child(1) > section > h2");
	public By descrUpperHeader = By.cssSelector("#main > div > div:nth-child(1) > section > h3:nth-child(2)");		// La fibra di Melita è arrivata a casa tua!
	public By descrUpperSubheader = By.cssSelector("#main > div > div:nth-child(1) > section > p");					// L'indirizzo è coperto.
	//public By descrLowerHeader = By.cssSelector("#main > div > div:nth-child(2) > section > h3 > strong");
	public By descrLowerHeader = By.cssSelector("#main > div > div:nth-child(1) > section > p:nth-child(5)");		//description
	public By loginAccess = By.cssSelector("#main > div > div:nth-child(4) > section > div > h2");
	//public By descriptionUpper = By.cssSelector("#main > div > div:nth-child(2) > section > p:nth-child(2)");
	//public By descriptionLower = By.cssSelector("#main > div > div:nth-child(2) > section > div");
	//public By loginAccessBtn = By.cssSelector("#main > div > div:nth-child(2) > section > div > a");
	public By loginAccessBtn = By.cssSelector("#main > div > div:nth-child(4)> section > div > div > div > div > a");
	//public By loginAccessBtn = By.xpath("//a[@href='/it/login?zoneid=fibra_copertura_confermata-link_contextual_accedi']");
	
	private By[] locators = {
			pagePath1, pagePath2, pageTitle, pageSubTitle, descrUpperHeader, descrUpperSubheader, descrLowerHeader
	};

	public FibreOkComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkPageElements(String address) throws Exception {
		//String completeAddress = subheaderString.replace("#", address);
		//pageStrings[5] = completeAddress;
		verifyElementsArrayText(locators, pageStrings);
	}
	
	// ----- Constants -----
	
	private String subheaderString = "L'indirizzo # è coperto.";
	/*private final String[] pageStrings = {
			"HOME",
			"FIBRA COPERTURA CONFERMATA",
			"Melita offre la fibra ai clienti Enel Energia",
			"In promozione, tre mesi di canone in regalo!",
			"La fibra di Melita è arrivata a casa tua!",
			subheaderString,
			"Ora scegli l'offerta luce/gas di Enel Energia a cui abbinare la fibra di Melita",
			"Seleziona un'offerta luce o gas e richiedi contestualmente l'adesione a FIBRA DI MELITA.\n" + 
				"L'attivazione del servizio fibra di Melita avverrà dopo le verifiche tecniche per l'attivazione della tua nuova fornitura Enel Energia.",
			"Se hai già una fornitura Enel Energia a questo indirizzo puoi accedere all'Area Clienti MyEnel e abbinare alla fornitura luce o gas la fibra di Melita."
	};*/
	private final String[] pageStrings = {
			"HOME",
			"FIBRA COPERTURA CONFERMATA",
			"In casa è arrivata una nuova energia",
			"FIRA DI MELITA a 27€ al mese (IVA inclusa). Senza costi di attivazione e con modem WiFi incluso!",
			"La fibra di Melita è arrivata a casa tua!",
			"L'indirizzo è coperto.",
			"Scegli una delle offerte luce e gas abbinate alla FIBRA DI MELITA. Se hai bisogno di supporto, puoi entrare in contatto con un nostro operatore che ti saprà fornire l'aiuto di cui hai bisogno."
	};
	
}
