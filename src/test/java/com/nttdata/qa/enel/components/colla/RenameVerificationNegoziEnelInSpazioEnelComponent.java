package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class RenameVerificationNegoziEnelInSpazioEnelComponent {
	WebDriver driver;
	SeleniumUtilities util;

	public By logoEnel = By.xpath("//img[@class='logoimg']");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By menu = By.xpath("//span[@class='icon-menu']");
	public By linkSpazioEnel = By.xpath("//a[@href='https://www-coll1.enel.it/spazio-enel']");
	public By spazioEnelTitle = By.xpath(
			"//span[text()='Trova lo Spazio Enel più vicino a te' and @class='image-hero_title text--page-heading']");
	public By spazioEnel = By.xpath("//div[@id='legend_enelPoint' and text()='Spazio Enel']");
	public By spazioEnelPartner = By.xpath("//div[@id='legend_partnerPoint' and text()='Spazio Enel Partner']");
	public By urlSpazioEnel = By.xpath("//link[@href='https://www-coll1.enel.it/spazio-enel/']");
	public By frame = By.xpath("//iframe[@id='es_map_iframe']");
	public By nellaTuaCittaLabel = By.xpath("//h1[text()='Nella tua città']");
	public By framesetUbiest = By.xpath("//iframe[@id='es_map_iframe']");
	public By checkVaiAlloSpazioEnel=By.xpath("//div[@class='item-provincia']//div//div[@class='col-sm-3 btn-list-col']//a[@class='btn-list  btn-lg']");

	public RenameVerificationNegoziEnelInSpazioEnelComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);

	}

	public void launchLink(String url) throws Exception {
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("e' impossibile raggiungere il link " + url);

		}
	}

	public void clickComponent(By oggettocliccabile) throws Exception {
		util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
		util.objectManager(oggettocliccabile, util.scrollAndClick);

	}

	public void scrollComponent(By oggetto) throws Exception {
		util.objectManager(oggetto, util.scrollToVisibility, false);
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
		if (!util.exists(oggettoEsistente, 30))
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
	}

	public void setframe(By frame) {
		WebElement frameToSw = driver.findElement(frame);
		driver.switchTo().frame(frameToSw);
	}

	public void checkListUbiest(String description1, String description2) throws Exception {
		driver.switchTo().defaultContent();
		WebElement frameset = driver.findElement(By.xpath("//iframe[@id='es_map_iframe']"));
		driver.switchTo().frame(frameset);

		WebDriverWait WaitVar = new WebDriverWait(driver, 20); // aspetta 10 secondi
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div/div[@class='resultsNumber']")));

		List<WebElement> risultati = driver
				.findElements(By.xpath("//div[@class='resultsContainer']//div[@class='dataResult']"));
		//System.out.println("la lista ubiest contiene un numero di risultati pari a: " + risultati.size());

		String descrizione = "";

		for (int i = 1; i <= risultati.size(); i++) {
			descrizione = driver.findElement(By.xpath(
					"//div[@class='resultsContainer']//div[@class='dataResult'][" + i + "]//div[@class='nameResult']"))
					.getText();
             if (!descrizione.contentEquals(description1) && !descrizione.contentEquals(description2))
            	 throw new Exception("sulla pagina 'https://www-coll1.enel.it/spazio-enel' nella sezione ubitiest non sono presenti i riferimenti enel con descrizione "+description1+" e "+description2);
		}
		
		driver.switchTo().defaultContent();
	}
	
	public void selezionaProvincia(String prov) throws Exception {
		prov=prov.toLowerCase();
		driver.findElement(By.xpath("//div[@class='item-regione']//div[@class='r-prov']//a[contains(@href,'https://www-coll1.enel.it/spazio-enel/"+prov+"')]")).click();
	    WebDriverWait WaitVar= new WebDriverWait (driver,10);  //aspetta 10 secondi
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='es_index_prov']//h1")));
	    prov=prov.toUpperCase();
			if (!driver.findElement(By.xpath("//div[@id='es_index_prov']//h1")).getText().contentEquals("Spazio Enel a "+prov))
				throw new Exception("la pagina 'https://www-coll1.enel.it/spazio-enel/"+prov+"/' non e' visualizzabile");
			}
	
	public void checkLabel (By checkOggetto, String descr, String prov)throws Exception {
		WebDriverWait WaitVar= new WebDriverWait (driver,10);  //aspetta 10 secondi
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkOggetto));
		String description = "";
		prov=prov.toUpperCase();
		int count=0;
		
		String page=driver.findElement(By.xpath("//div[@class='cpagination']/div[@class='details']")).getText();
		
		String[]p=page.split("Pagina 1 di ");
		int pagine=Integer.parseInt(p[1]);
		//System.out.println("le pagine sono "+pagine);
		for (int j=1;j<=pagine;j++) {
				List<WebElement> checkLabel=driver.findElements(checkOggetto);
				int dim=checkLabel.size();
				
				 for(WebElement li:checkLabel) {			
					description =li.getText();
					count=count+1;
						if (!description.contentEquals(descr) )
							throw new Exception("sulla pagina 'https://www-coll1.enel.it/spazio-enel/"+prov+"/' non sono visualizzabili le label con descrizione: "+descr);
					}
				////System.out.println("la dimensione della lista e' "+count);
				 if((dim==count)&& (j==pagine)) break;
				 else {
					 	if(j!=pagine)
					 		driver.findElement(By.xpath("//div/a[text()='Successiva']")).click();	
				 	  }
		}
	}
}
