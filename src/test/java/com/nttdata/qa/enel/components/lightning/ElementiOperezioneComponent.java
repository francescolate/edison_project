package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ElementiOperezioneComponent {

	WebDriver driver;
	public SeleniumUtilities util;
	Actions actions;
	JavascriptExecutor jse;
	SpinnerManager spinner;
	
	public By elementiHeader = By.xpath("//span[text()='(3+)']/preceding-sibling::span");
	public String userDetails = "//span[text()='MODIFICA TENSIONE']/ancestor::td/preceding-sibling::td//span[text()='LAVORI']/ancestor::td//preceding-sibling::td//span[text()='$DATE$']";
	
	public ElementiOperezioneComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		actions = new Actions(driver);
		jse = (JavascriptExecutor) driver;
		spinner = new SpinnerManager(driver);
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
}
