package com.nttdata.qa.enel.components.colla;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PubblicoID61ProcessoAResSwaEleComponent {

	public static String email_id = "";
	
	public By logoEnel = By.xpath("//img[@class='logoimg']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By buttonAcceptCookie= By.xpath("//button[@id='truste-consent-button']");
	
	public By labelCheContratto = By.xpath("//div[@class='search-section']/descendant::label[contains(text(),'Che contratto vuoi attivare')]");
	public By labelDove = By.xpath("//div[@class='search-section']/descendant::label[contains(text(),'Dove')]");
	public By labelPerQuale = By.xpath("//div[@class='search-section']/descendant::label[contains(text(),'Per quale necessità')]");
	//public By dropdownSelect = By.xpath("//select[@id='product']/child::option");
	public By dropdownSelect = By.xpath("//select[@id='product']");
	public By iniziaButton = By.xpath("//div[@class='btn-section']/child::a[@id='ofertButton']");
	
	public By productDropDown = By.xpath("//span[@id='productSelectBoxItText']");
	public By luce = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Luce']");
	public By luceGas = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Luce e //div[@id='adesione_standard']/child::p[@class='image-hero_detail text--detail']Gas']");
	
	
	public By luce30OfferContent = By.xpath("//h3[text()='Luce 30 Spring']/ancestor::li[@class='offert-item']/descendant::p[contains(text(),'prezzo per kWh')]");
	public By attivaOra_link = By.xpath("//div[@id='priceHeaderID']//a[contains(text(),'Attiva ora')]");
	public By attivaLoffertaButton = By.xpath("//a[contains(text(),'attiva l’offerta')]");
	public By adesioneContents = By.xpath("");
	public By compilaManualmente = By.xpath("//button[text()='COMPILA MANUALMENTE']");
	
	public final String switchFrame = "//div[@id='iframe']//iframe";
	
	//Inserisci i tuoi dati
	public By inserisci_I_Tuoi_Dati_Heading = By.xpath("//h3[contains(text(),'Inserisci i tuoi dati')]");
	
	public By labelNome = By.xpath("//span[@id='ITA_IFM_First_Name__c_errmsg']/preceding-sibling::label[text()='Nome*']");
	public By labelCognome = By.xpath("//span[@id='ITA_IFM_Last_Name__c_errmsg']/preceding-sibling::label[text()='Cognome*']");
	public By labelCodiceFiscal = By.xpath("//span[@id='NE__Fiscal_Code__c_errmsg']/preceding-sibling::label[text()='Codice Fiscale*']");
	public By lableTellefonoCellulare = By.xpath("//span[@id='ITA_IFM_Mobile_Phone__c_errmsg']/preceding-sibling::label[text()='Telefono Cellulare*']");
	public By lablelEmail = By.xpath("//span[@id='NE__E_mail__c_errmsg']/preceding-sibling::label[text()='Email*']");
	public By labelConfirmEmail = By.xpath("//label[text()='Conferma email*']");
	
//	public By inputNome = By.xpath("//div[@class='form-group']/child::label[text()='Nome*']/following-sibling::input[@id='ITA_IFM_First_Name__c']");
//	public By inputCognome = By.xpath("//div[@class='form-group']/child::label[text()='Cognome*']/following-sibling::input[@id='ITA_IFM_Last_Name__c']");
//	public By inputCodiceFiscale = By.xpath("//div[@class='form-group']/child::label[text()='Codice Fiscale*']/following-sibling::input[@id='NE__Fiscal_Code__c']");
//	public By inputTelefonoCellulare = By.xpath("//div[@class='form-group']/child::label[text()='Cellulare*']/following::input[@id='ITA_IFM_Mobile_Phone__c']");
//	public By inputEmail = By.xpath("//label[text()='Email*']/parent::div[@class='form-group']//input[@id='NE__E_mail__c']");
//	public By inputConfirmEmail = By.xpath("//label[text()='Conferma email*']/parent::div[@class='form-group']//input[@id='NE__E_mail__c_confirm']");
	
	public By inputNome = By.xpath("//input[@id='nameId']");
	public By inputCognome = By.xpath("//input[@id='surnameId']");
	public By inputCodiceFiscale = By.xpath("//input[@id='cfId']");
	public By inputTelefonoCellulare = By.xpath("//input[@id='phoneId']");
	public By inputEmail = By.xpath("//input[@id='emailId']");
	public By inputConfirmEmail = By.xpath("//input[@id='emailConfirmId']");
	
	public By lablePrivacyContents = By.xpath("//span[contains(text(),'Ho preso visione dell’informativa sulla privacy')]");
	public By labelPrivacyCheckbox = By.xpath("//div[1]/div[@class='user_input_area']/div[@class='row']/label[@class='MuiFormControlLabel-root']/span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1']");
	
	public By titleDiCosa = By.xpath("//div[@class='col-md-12']/child::p[contains(text(),'Di cosa hai')]");
	public By radioCambio = By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'CAMBIO FORNITORE')]");
	public By radioPrima =  By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'PRIMA ATTIVAZIONE')]");
	public By radioSubentro = By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'SUBENTRO')]");
	public By radioVoltura = By.xpath("//div[@class='radio-container']/descendant::span[contains(text(),'VOLTURA')]");
	public By inputPod = By.xpath("//div[@class='form-group']//input[@id='ITA_IFM_POD__c']");
	public By inputCap = By.xpath("//div[@class='form-group']//input[@id='ITA_IFM_ZIPCode__c']");
	
	
	
	//I tuoi dati
	public By iTuoi_Dati_Heading = By.xpath("//h3[text()='I tuoi dati']"); 
	public By fieldInfoText = By.xpath("//p[contains(text(),'Ti informiamo che la fornitura verrà attivata')]");
	public By Citta_Input = By.xpath("(//label[text()='Città'])[1]/following-sibling::input");
	public By Indirizzo_Input = By.xpath("//label[text()='Indirizzo']/following-sibling::input");
	public By Numero_Civico_Input = By.xpath("//label[text()='Numero Civico*']/following-sibling::input");
	public By Attuale_Fornitore_Input = By.xpath("//label[text()='Attuale fornitore*']/following-sibling::div/input");
	public By Attuale_Fornitore_InputSelect = By.xpath("//ul/li/div[text()='ACEA ENERGIA SPA - Libero']");
	
	public By insertManually = By.xpath("//button[text()=' Inserisci manualmente']");
	public By confirmIndrizzo = By.xpath("//button[text()='Conferma indirizzo']");
	
	
	//Informazioni fornitura
	
	public By informazioni_Fornitura_Heading = By.xpath("//h3[text()='Informazioni fornitura']"); 
	public By informazioni_Fornitura_Heading_Subtext = By.xpath("//legend[@id='what-you-need-legend']");
	public By IF_Radio1 = By.xpath("//span[text()='CAMBIO FORNITORE']");
	public By IF_Radio1_description = By.xpath("//span[contains(text(),'Ho gia un contratto con un altro fornitore ma vorrei passare ad Enel')]");
	public By IF_Radio2 = By.xpath("//span[text()='PRIMA ATTIVAZIONE']");
	public By IF_Radio2_description = By.xpath("//span[contains(text(),'Voglio attivare un nuovo contatore')]");
	public By IF_Radio3 = By.xpath("//span[text()='SUBENTRO']");
	public By IF_Radio3_description = By.xpath("//span[contains(text(),'Voglio attivare un contatore disattivato')]");
	public By IF_Radio4 = By.xpath("//span[text()='VOLTURA']");
	public By IF_Radio4_description = By.xpath("//span[contains(text(),'Voglio cambiare intestatario a un contratto di fornitura')]");
	public By POD = By.xpath("//p[text()='POD']");
	public By POD_Input = By.xpath("//input[@id='pod-pdr']");
	public By CAP = By.xpath("//p[text()='CAP']");
	public By CAP_Input = By.xpath("//input[@id='capSupply']");
	public By CAP_Select = By.xpath("//li/div[text()='60027 - OSIMO']");
	public By CAP_Select1 = By.xpath("//span/span[text()='OSIMO']");
	public By step2_Disclaimer = By.xpath("//p[contains(text(),'Se non hai a portata di mano il POD puoi continuare')]");
	
	//Recapiti
	public By recapiti_Heading = By.xpath("//h3[text()='Recapiti']"); 
	public By recapiti_question1 = By.xpath("//label[text()='Sei residente presso la fornitura?']");
	public By recapiti_question1_Radio_SI = By.xpath("//label[@for='yesResidence']");
	public By recapiti_question1_Radio_NO = By.xpath("//label[@for='noResidence']");
	public By recapiti_question1_Info_Text = By.xpath("//p[contains(text(),'Ti ricordiamo che la tariffa')]");
	
	public By recapiti_question2 = By.xpath("//label[text()='Dove vuoi ricevere le comunicazioni relative alla tua fornitura?']");
	public By recapiti_question2_Radio_yesBillingAddress = By.xpath("//span[text()='Allo stesso indirizzo della fornitura']/parent::label[@for='yesBillingAddress']");
	public By recapiti_question2_Radio_noBillingAddress = By.xpath("//span[text()='Ad un indirizzo diverso da quello della fornitura']/parent::label[@for='noBillingAddress']");
	
	//Pagamenti e Bollette
	public By pagamenti_E_Bollette_Heading = By.xpath("//h3[text()='Pagamenti e Bollette']"); 
	public By Metodo_di_Pagamento_Field = By.xpath("//label[text()='Metodo di Pagamento*']");
	public By Metodo_di_Pagamento_FieldDefaultValue = By.xpath("//span[@id='payment-modeSelectBoxItText']");
	public By Metodo_di_Pagamento_FieldSelectValue = By.xpath("//ul[@id='payment-modeSelectBoxItOptions']/li[@data-id='2']/a");
	
	public By Codice_IBAN_Field = By.xpath("//label[text()='Codice IBAN*']");
	public By Codice_IBAN_InputField = By.xpath("//label[text()='Codice IBAN*']/following-sibling::input");
	public By Codice_IBAN_Radio_yesAccountHolder = By.xpath("//span[contains(text(),'Sono ')]/parent::label[@for='yesAccountHolder']");
	public By Codice_IBAN_Radio_noAccountHolder = By.xpath("//span[contains(text(),'L')]/parent::label[@for='noAccountHolder']");
	
	public By Modalità_di_ricezione_Heading = By.xpath("//label[text()='Modalità di ricezione della bolletta*']");
	public By Modalità_di_ricezione_Radio1 = By.xpath("//span[contains(text(),'Voglio ricevere la bolletta')]/parent::label");
	
	public By Modalità_di_ricezione_Radio2 = By.xpath("//span[contains(text(),'Voglio ricevere la fattura')]/parent::label");
	
	public By Codici_Promozionali_Heading = By.xpath("//label[text()='Codici promozionali']");
	public By Codici_Promozionali_Subtext = By.xpath("//label[contains(text(),'Se sei in possesso di un')]");
	public By Codici_Promozionali_DiscountCode = By.xpath("//input[@id='discount-code-input']");
	
	//Mandati e Consensi
	public By Mandati_e_Consensi_Heading = By.xpath("//h3[text()='Mandati e Consensi']");
	public By Mandati_e_Consensi_TextInBox = By.xpath("//div[text()='Dichiaro di voler conferire ad Enel Energia: ']"); //  //div[text()='Dichiaro di voler conferire ad Enel Energia: ']| //div[text()='Dichiaro di voler conferire ad Enel Energia: ']//li/span
	public By Mandati_e_Consensi_chkbx1 = By.xpath("(//div[@class='form-group checkbox privacy-checkbox'])[2]");
	public By Mandati_e_Consensi_chkbx1_Text1 = By.xpath("//label[contains(text(),'Ho preso visione dell')]");
	public By Mandati_e_Consensi_chkbx2 = By.xpath("//span[text()='Ho preso visione delle note legali ai sensi del']/parent::label");
	public By Mandati_e_Consensi_chkbx2_Text1 = By.xpath("(//span[text()='Ho preso visione delle note legali ai sensi del']/parent::label/parent::div/label)[1]");
	public By Mandati_e_Consensi_chkbx2_Text2 = By.xpath("(//span[text()='Ho preso visione delle note legali ai sensi del']/parent::label/parent::div/label)[2]");
	public By Mandati_e_Consensi_chkbx3_Text = By.xpath("//label[text()='Voglio recedere dal contratto attualmente in essere con ACEA ENERGIA SPA (Libero)']");
	
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Heading = By.xpath("//h3[text()='Richiesta di esecuzione anticipata del contratto']");
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_TextInBox = By.xpath("//label[contains(text(),'Desideri che la procedura per')]");
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_LinkText = By.xpath("//span/a[contains(text(),'Maggiori informazioni')]");
	
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_SI = By.xpath("//label[@for='yesSave_RAA']");
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO = By.xpath("//label[@for='noSave_RAA']");
	
	//public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_content = By.xpath("//div[@class='modal-body']/p[contains(text(),'Il Codice del Consumo prevede 14 giorni')]");
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_content = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//p[contains(text(),'Il Codice del Consumo prevede 14 giorni ')]");	
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_POPUP_close = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-body']/p[contains(text(),'Il Codice del Consumo prevede 14 giorni')]/parent::div/preceding-sibling::div/button[@class='remodal-close close-AEM_SF']");
	
	
	public By Consenso_Marketing_Enel_Energia_Heading = By.xpath("//label[text()='Consenso Marketing Enel Energia']");
	public By Consenso_Marketing_Enel_Energia_TextInBox = By.xpath("//div[@class='textarea form-control' and contains(text(),'dati personali da parte di Enel Energia')]");
	public By Consenso_Marketing_Enel_Energia_Accetto = By.xpath("//span[text()='Accetto']/parent::label[@for='accept-marketing-consent']");
	public By Consenso_Marketing_Enel_Energia_NonAccetto = By.xpath("//span[text()='Non accetto']/parent::label[@for='not-accept-marketing-consent']");
	
	public By Consenso_Marketing_Terzi_Heading = By.xpath("//label[text()='Consenso marketing terzi']");
	public By Consenso_Marketing_Terzi_TextInBox = By.xpath("//div[@class='textarea form-control' and contains(text(),'tra cui Enel X')]");
	public By Consenso_Marketing_Terzi_Accetto = By.xpath("//span[text()='Accetto']/parent::label[@for='accept-terzi-consent']");
	public By Consenso_Marketing_Terzi_NonAccetto = By.xpath("//span[text()='Non accetto']/parent::label[@for='not-accept-terzi-consent']");
	
	public By Consenso_Profilazione_Enel_Energia_Heading = By.xpath("//label[text()='Consenso profilazione Enel Energia']");
	public By Consenso_Profilazione_Enel_Energia_TextInBox = By.xpath("//div[@class='textarea form-control' and contains(text(),'profilazione ')]");
	public By Consenso_Profilazione_Enel_Energia_Accetto = By.xpath("//span[text()='Accetto']/parent::label[@for='accept-profile-consent']");
	public By Consenso_Profilazione_Enel_Energia_NonAccetto = By.xpath("//span[text()='Non accetto']/parent::label[@for='not-accept-profile-consent']");
	public By chudi= By.xpath("//div[@class='modaliAdesione remodal remodal-is-initialized remodal-is-opened']//button[@class='remodal-close close-AEM_SF']");
	
	public By ProseguiButton = By.xpath("//div[1]/div[@class='user_input_area']/div[@class='cta_container']/button[@class='proceed_button']");
	public By COMPLETA_ADESIONE_Button = By.xpath("//button[text()='COMPLETA ADESIONE']");
	
	//TORNA ALLA HOME
	public By TornaAllaHomePageTitle = By.xpath("//h1[@class='image-hero_title']");
	public By TornaAllaHomeMSG1 = By.xpath("//h1[@class='image-hero_title']/following-sibling::p[@class='hero_msg1']");
	public By TornaAllaHomeMSG2 = By.xpath("//h1[@class='image-hero_title']/following-sibling::p[@class='hero_msg2']");
	
	//Workbench
	public By Status = By.xpath("//th[text()='Status']");
	public By Status_Value = By.xpath("(//th[text()='Status']/parent::tr[1]/following-sibling::tr)[1]/td[6]");
	public By ITA_IFM_Process__c = By.xpath("//th[text()='ITA_IFM_Process__c']");
	public By ITA_IFM_Process__c_Value = By.xpath("(//th[text()='ITA_IFM_Process__c']/parent::tr[1]/following-sibling::tr)[1]/td[7]");
	public By ITA_IFM_ProdName__c = By.xpath("//th[text()='ITA_IFM_ProdName__c']");
	public By ITA_IFM_ProdName__c_Value = By.xpath("(//th[text()='ITA_IFM_ProdName__c']/parent::tr[1]/following-sibling::tr)[1]/td[8]");
	public By ITA_IFM_AccountFirstName__c = By.xpath("//th[text()='ITA_IFM_AccountFirstName__c']");
	public By ITA_IFM_AccountFirstName__c_Value = By.xpath("(//th[text()='ITA_IFM_AccountFirstName__c']/parent::tr[1]/following-sibling::tr)[1]/td[9]");
	public By ITA_IFM_AccountLastName__c = By.xpath("//th[text()='ITA_IFM_AccountLastName__c']");
	public By ITA_IFM_AccountLastName__c_Value = By.xpath("(//th[text()='ITA_IFM_AccountLastName__c']/parent::tr[1]/following-sibling::tr)[1]/td[10]");
	public By ITA_IFM_CommunicationEmailAddress__c = By.xpath("//th[text()='ITA_IFM_CommunicationEmailAddress__c']");
	public By ITA_IFM_CommunicationEmailAddress__c_Value = By.xpath("(//th[text()='ITA_IFM_CommunicationEmailAddress__c']/parent::tr[1]/following-sibling::tr)[1]/td[11]");
	public By ITA_IFM_AccountType__c = By.xpath("//th[text()='ITA_IFM_AccountType__c']");
	public By ITA_IFM_AccountType__c_Value = By.xpath("(//th[text()='ITA_IFM_AccountType__c']/parent::tr[1]/following-sibling::tr)[1]/td[4]");
	public By ITA_IFM_Commodity__c = By.xpath("//th[text()='ITA_IFM_Commodity__c']");
	public By ITA_IFM_Commodity__c_Value = By.xpath("(//th[text()='ITA_IFM_Commodity__c']/parent::tr[1]/following-sibling::tr)[1]/td[5]");
	public By ITA_IFM_NewOrderItem_Status__c = By.xpath("//th[text()='ITA_IFM_NewOrderItem_Status__c']");
	public By ITA_IFM_NewOrderItem_Status__c_Value = By.xpath("(//th[text()='ITA_IFM_NewOrderItem_Status__c']/parent::tr[1]/following-sibling::tr)[1]/td[7]");
	public By ITA_IFM_FiscalCode__c = By.xpath("//th[text()='ITA_IFM_FiscalCode__c']");
	public By ITA_IFM_FiscalCode__c_Value = By.xpath("(//th[text()='ITA_IFM_FiscalCode__c']/parent::tr[1]/following-sibling::tr)[1]/td[6]");
	
	public By TornaAllaHomeButton = By.xpath("//a[text()='torna alla home']");
	
	public static final String SPECIALELUCE60 = "Speciale Luce 60";
	public static final String ADISIONE_CONTENT = "Ricorda che per completare l'adesione devi avere a portata di mano:"
			+ " Codice fiscale / POD / Iban"
			+ " se devi fare un Subentro, una Prima attivazione o una Voltura servono anche:"
			+ " dati dell'immobile / Carta d'identità"
			+ " L'offerta che hai scelto è valida solo per forniture ad uso abitativo.";
	public static final String INPUT_HEADER = "Inserisci i tuoi datielemento disabilitato 1 di 4";
	public static final String PRIVACY_CONTENT = "Ho preso visione dell’informativa sulla privacy";
	
	public static final String step2DiscliamerText = "Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l'adesione.";
	public static final String fieldInfoMessage = "Ti informiamo che la fornitura verrà attivata con i medesimi requisiti tecnici (potenza e tensione) attualmente in esercizio. Se lo desideri potrai effettuare una variazione di potenza dopo l'attivazione del contratto direttamente dalla tua area clienti.";
	
	public static final String RECAPITI_INFO_CONTENT = "Ti ricordiamo che la tariffa per uso domestico residenziale può essere applicata solo su una delle forniture a te intestate e presso la quale hai la residenza anagrafica.";
	public static final String METADO_DROPDOWN_VALUE= "Addebito sul conto corrente";
	public static final String METADO_DROPDOWN_SELECTVALUE= "Bollettino Postale";
	
	
	public static final String Mandati_e_Consensi_TextBoxContent = "Dichiaro di voler conferire ad Enel Energia:"
			+"mandato irrevocabile senza rappresentanza per lo svolgimento presso il distributore competente delle attività di gestione della connessione dei punti di prelievo (es. aumenti di potenza, spostamenti di gruppi di misura, etc), mantenendo la titolarità di ogni rapporto giuridico con il distributore competente inerente la connessione alla rete dei siti ed impianti. Tale mandato è a titolo oneroso ed obbliga la corrispondere al Fornitore gli importi necessari per l’esecuzione del mandato e per l’adempimento delle obbligazioni che a tal fine il Fornitore ha contratte in proprio nome;"
			+"mandato irrevocabile con rappresentanza per la sottoscrizione del Contratto per il servizio di connessione alla rete elettrica allegate al contratto per il servizio di trasporto dell'energia elettrica, del cui contenuto il Cliente ha preso atto anche in quanto disponibile sul sito "
			+"www.enel.it"
			+", consapevole che l’accettazione ed il rispetto delle stesse è condizione necessaria per l’attivazione ed il mantenimento del servizio di trasporto.";
	
	public static final String Mandati_e_Consensi_chkbx1Text = "Ho preso visione dell'"
			+"informativa sulla privacy"
			+" riguardo a come tratterete i miei dati personali";
	
	public static final String Mandati_e_Consensi_chkbx2Text1 = "Ho preso visione delle note legali ai sensi del"
			+" D.Lgs 70/2003";
	public static final String Mandati_e_Consensi_chkbx2Text2 =	"e le "+"informazioni precontrattuali"
			+" ai sensi dell'art 49 del D.Lgs 21/2014 - Codice del Consumo";
	public static final String Mandati_e_Consensi_chkbx3Text = "Voglio recedere dal contratto attualmente in essere con ACEA ENERGIA SPA (Libero)";
			
	public static final String Consenso_Marketing_Enel_Energia_TextBoxContent ="L’interessato acconsente al trattamento dei propri dati personali da parte di Enel Energia, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail) "
			+"Codice Privacy";
	
	public static final String Consenso_Marketing_Terzi_TextBoxContent ="L’interessato acconsente al trattamento dei propri dati personali da parte delle Società del Gruppo Enel (tra cui Enel X), da società controllanti, controllate o collegate, o da partner commerciali di Enel Energia, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail) "
			+"Codice Privacy";
	
	public static final String RichestaHeading ="Richiesta di esecuzione anticipata del contratto";
	public static final String RichestaText ="Desideri che la procedura per l’attivazione della fornitura sia avviata subito, prima che siano trascorsi i 14 giorni per l’esercizio del diritto di ripensamento?";
	public static final String RichestaLink ="Maggiori informazioni";
	
	public static final String RichestaPopUpContent ="Il Codice del Consumo prevede 14 giorni di tempo per consentire al cliente di esercitare il diritto di ripensamento dopo la conclusione di un contratto. Scegliendo la \"Richiesta di esecuzione anticipata della fornitura\", il Cliente richiede espressamente che le procedure per dar corso all'attivazione vengano avviate subito, ossia prima che sia decorso il termine per il ripensamento. Anche nel caso di \"Richiesta di esecuzione anticipata della fornitura\", per il Cliente sarà sempre possibile esercitare il diritto di ripensamento. Qualora sia possibile annullare la richiesta di attivazione, il Cliente continuerà ad essere fornito dal precedente Fornitore oppure saranno attivati i Servizi di ultima istanza gas o il Servizio di maggior tutela per il settore elettrico. Qualora, invece, non sia possibile interrompere la richiesta di attivazione verso il distributore, il Cliente verrá fornito da Enel Energia e potrà individuare successivamente un altro Fornitore o procedere alla richiesta di chiusura del Punto di fornitura, facendone espressa richiesta. In questi casi il Cliente sará tenuto al pagamento dei corrispettivi previsti dal contratto fino all'avvenuta cessazione della fornitura. Invece, qualora fosse possibile interrompere la richiesta di attivazione verso il distributore, il cliente sarà tenuto al pagamento del corrispettivo di 23,00 euro iva esclusa.";
	
	public static final String Consenso_Profilazione_Enel_Energia_TextBoxContent ="L’interessato acconsente al trattamento dei propri dati personali per attività di profilazione basate sulle abitudini di consumo e sui dati e informazioni acquisite attraverso l’utilizzo dei prodotti o servizi utilizzati, da parte di Enel Energia o da parte di Società del Gruppo Enel, da società controllanti, controllate o collegate o da partner commerciali di Enel Energia.";
	
	public static final String Modalità_di_ricezione_EmailInput = "//div[@id='billOtherAddress']//label[text()='Email*']/following-sibling::input";
	public static final String Modalità_di_ricezione_TelefonoInput = "//div[@id='billOtherAddress']//label[text()='Telefono Cellulare*']/following-sibling::div/input[@id='ITA_IFM_Billing_Phone']";
	
	
	WebDriver driver;
	SeleniumUtilities util;
	
	 public PubblicoID61ProcessoAResSwaEleComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	 public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}
	 
	 public void VerifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String textfield_found="NO";
			
			WebElement element = driver.findElement(checkObject);
			
			String actualtext = element.getText();
			
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
			}

	 public void verifyElementSelected(By checkObject) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String selected="NO";
			
			WebElement element = driver.findElement(checkObject);
			
			if (element.isSelected()){
				selected="YES";
			}			
			if (!element.isSelected()){
				selected = "NO";
				throw new Exception("The element is not selected");
			}
		}
		
	 public void verifyElementNotSelected(By NocheckboxObject) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
			
			String NotSelected="YES";
			
			WebElement element = driver.findElement(NocheckboxObject);
			
			if (!element.isSelected()){
				NotSelected="NO";
			}			
			if (element.isSelected()){
				NotSelected = "YES";
				throw new Exception("The checkbox is selected");
			}
		}
	 
	 public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
	        
	        WebDriverWait wait = new WebDriverWait(this.driver, 50);
	            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
	        if(value.equals(""))throw new Exception("Field is not pre filled");
	        
	        String match = "FAIL";
	        if(value.contentEquals(expectedValue))
	        	match = "PASS";
	        if(match.contentEquals("FAIL"))
	        	throw new Exception("PrePopulatedValue and Expected values are not matching");
	    }
	 
		public void selectDropDownValue(By checkObject, String property) throws Exception
		{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			Select select = new Select(driver.findElement(checkObject));
			select.selectByValue(property);
					
		}
		
		public void selectMetodoPagamentoValue() throws Exception
		{
			verifyComponentExistence(Metodo_di_Pagamento_FieldDefaultValue);
			clickComponent(Metodo_di_Pagamento_FieldDefaultValue);
			Thread.sleep(2000);
			verifyComponentExistence(Metodo_di_Pagamento_FieldSelectValue);
			clickComponent(Metodo_di_Pagamento_FieldSelectValue);
			
		}
		
		public void verifyDefaultDropDownValue(By checkObject, String Value ) throws Exception
		{
			String textFound = "NO";
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			Select select = new Select(driver.findElement(checkObject));
			String actualDropDown = select.getFirstSelectedOption().getText();
			
			if (actualDropDown.contentEquals(Value))
				textFound="YES";
								
			if (textFound.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					
		}
		
		public void verifyDefaultValue(By checkObject, String Value ) throws Exception
		{
			String textFound = "NO";
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String actualValue = driver.findElement(checkObject).getText();
			
			if (actualValue.contentEquals(Value))
				textFound="YES";
								
			if (textFound.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					
		}
		
		public void clickDropDownValue(By dropdown,By input) throws Exception {
	         util.scrollToElement(driver.findElement(dropdown));
	         clickComponent(dropdown);
	         clickComponent(input);
	           
	        }
		

		public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
		
		public void switchToFrame(String frameName) throws Exception{
			WebDriverWait wait = new WebDriverWait(this.driver, 120);
			
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
					);
			this.driver.switchTo().frame(frameToSw);
		}
		public void clearText(By object){
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WebElement input = driver.findElement(object);
			input.clear();
		}
		
		
		public void verifyComponentExistence(By oggettoEsistente) throws Exception {
					if (!util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
			
		}
		
	    public boolean verifyExistence(By by, int timeout) throws Exception{
	    	return util.verifyExistence(by, timeout);
	    }
		
		public void verifyComponentNotExistence(By oggettoEsistente) throws Exception {
			if (util.verifyExistence(oggettoEsistente, 30)) {
		throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
	}
	
		}
		
		public void SwitchToWindow() throws Exception
		{
			Set<String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
			/*	System.out.println(winHandle);
				System.out.println("switched to "+driver.getTitle()+"  Window");
				System.out.println(driver.getCurrentUrl());
			*/	
		       // String pagetitle = driver.getTitle();
		        		driver.switchTo().window(winHandle);
		   /*     System.out.println(winHandle);
		        System.out.println("After switch"+ driver.getCurrentUrl());
		        System.out.println(driver.getTitle());
			*/    
			 }
		}
		public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}
		
		public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
			textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (!util.checkElementText(objectWithText, textToCheck)) {
				WebElement problemElement = driver.findElement(objectWithText);
				String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
				if (elementText.length() < 1) {
					elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
				}
				String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
				System.out.println(message);
				throw new Exception(message);
			}
		}
		public void verifyContent(By object,String text) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
			String value = driver.findElement(object).getText();
			value = value.replaceAll("(\r\n|\n,)","");
			//System.out.println(value);
			if(!text.contentEquals(value))
				throw new Exception("Value is not matching with expected");
			
		}

			public void clickComponentIfExist(By existObject) throws Exception {
			if (util.exists(existObject, 15)) {
				util.objectManager(existObject, util.scrollToVisibility, false);
			    util.objectManager(existObject, util.scrollAndClick);
			}
		}
			public void waitForElementToDisplay (By checkObject) throws Exception{
				WebDriverWait wait = new WebDriverWait (driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				
			}
			public void jsClickObject(By by) throws Exception {
				util.jsClickElement(by);
			}
				
		public void enterInput(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void clickTab(By Object) throws Exception {
			driver.findElement(Object).sendKeys(Keys.TAB);;
		}
		
		public static String generateRandomPOD(int n) {
		    int m = (int) Math.pow(10, n - 1);
		    m = m + new Random().nextInt(9 * m);
		    return String.valueOf(m);
		}
		
		public void scrollDown(By by) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			util.scrollToElement(we);
			util.scrollNaNdecchiaNdecchia();
		}
		
		public void verifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			String textfield_found="NO";
			WebElement element = driver.findElement(checkObject);
			String actualtext = element.getText().replaceAll("(\r\n|\n)", "");;
			System.out.println(actualtext);
			System.out.println(Value);
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
		}
		
		public void hanldeFullscreenMessage(By by) throws Exception{
			if(util.verifyExistence(by, 60)){
				util.objectManager(by, util.scrollToVisibility, false);
				util.objectManager(by, util.scrollAndClick);
			}
		}
		
		public void backBrowser(By checkObject) throws Exception {
		    try {
		    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    	driver.navigate().back();
		    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		                
		    } catch (Exception e) {
		        throw new Exception("Previous Page not displayed");
		    }
		}
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
			
			
		}
		
		public void checkUrl(String url) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			Thread.sleep(5000);
			String link=driver.getCurrentUrl();
			
			if (url.contains("https://"))
			{
				url = url.substring(8);
			}
			if (!link.contains(url))
				 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
			}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!driver.getCurrentUrl().contains(url))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void pressJavascript(By clickObject) throws Exception {
			WebElement element = util.waitAndGetElement(clickObject);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
		
		public void provideEmail(String email)
		{
			email_id =  email;
		}

	public static final String CapXpath = "//label[text()='CAP*']/following-sibling::input";	
	public static final String CapExpectedValue = "60027";	
	
	public static final String Status_ColumnHeading = "Status";
	public static final String Status_ColumnValue = "IN LAVORAZIONE";
	public static final String ITA_IFM_Process__c_ColumnHeading = "ITA_IFM_Process__c";
	public static final String ITA_IFM_Process__c_ColumnValue = "Switch Attivo";
	public static final String ITA_IFM_ProdName__c_ColumnHeading = "ITA_IFM_ProdName__c";
	public static final String ITA_IFM_ProdName__c_ColumnValue = "Speciale Luce 60";
	public static final String ITA_IFM_AccountFirstName__c_ColumnHeading = "ITA_IFM_AccountFirstName__c";
	public static final String ITA_IFM_AccountFirstName__c_ColumnValue = "FARFALLA";
	public static final String ITA_IFM_AccountLastName__c_ColumnHeading = "ITA_IFM_AccountLastName__c";
	public static final String ITA_IFM_AccountLastName__c_ColumnValue = "FARFALLIN";
	public static final String ITA_IFM_CommunicationEmailAddress__c_ColumnHeading = "ITA_IFM_CommunicationEmailAddress__c";
	public static final String ITA_IFM_CommunicationEmailAddress__c_ColumnValue = "sampletestingcrmautomation123@gmail.com";
	
	public static final String ITA_IFM_AccountType__c_ColumnHeading = "ITA_IFM_AccountType__c";
	public static final String ITA_IFM_AccountType__c_ColumnValue = "RESIDENZIALE";
	public static final String ITA_IFM_Commodity__c_ColumnHeading = "ITA_IFM_Commodity__c";
	public static final String ITA_IFM_Commodity__c_ColumnValue = "ELETTRICO";
	public static final String ITA_IFM_FiscalCode__c_ColumnHeading = "ITA_IFM_FiscalCode__c";
	public static final String ITA_IFM_FiscalCode__c_ColumnValue = "FRFFFL73A41H501N";
	public static final String ITA_IFM_NewOrderItem_Status__c_ColumnHeading = "ITA_IFM_NewOrderItem_Status__c";
	public static final String ITA_IFM_NewOrderItem_Status__c_ColumnValue = "Attesa Certificazione Web";
	
	//public static final String torna_alla_homepage_message1 = "Ti abbiamo inviato un messaggio all'indirizzo "+email_id+"Controlla l'email e conferma il contratto della tua fornitura energia.";
	public static final String torna_alla_homepage_message1 = "Ti abbiamo inviato un messaggio all'indirizzoControlla l'email e conferma il contratto della tua fornitura energia.";
}




