package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CarrelloComponent extends BaseComponent{
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;
	public By voceElettricoResidenziale=By.xpath("//h2/span[text()='Carrello']/ancestor::div[@class='slds-card ']//div[text()='Elettrico - Residenziale']/span[text()='Cliccabile' or text()='Aperta']");
	public By inputNomeProdotto=By.xpath("//span[text()='Nome Prodotto']/ancestor::div[1]//input");
	public By voceGasResidenziale=By.xpath("//h2/span[text()='Carrello']/ancestor::div[@role='region']//div[text()='Gas - Residenziale']/span[text()='Cliccabile' or text()='Aperta']");
	public By buttonCercaChiuso=By.xpath("//button/span[text()='Cerca chiuso']");
	public By cercaButton=By.xpath("//div[@id='searchBody']//button[text()='Cerca']");
	public By sezionaFibraButton= By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='Fibra']/div[text()='Fibra']");
	public String aggiungiAlCarrello="//div[@id='multiaddInfo']//h2[text()='#']/ancestor::div[@id='multiaddInfo']//button[@title='Aggiungi']/img[@class='addToCartClass']";
	public By pageConfigurazioni=By.xpath("//h2[text()='Configura']");
	public By buttonSalva=By.xpath("//button[text()='Salva']");
	public By buttonCheckout=By.xpath("//button[text()='Checkout']");
	public By labelCheckoutVerificato=By.xpath("//div[normalize-space(text())='Check Out effettuato']");
	public String prodotti []={"Fibra","VAS","Bonus"};
	public String prodottiCarrelloFibra []={"Fibra","Sconti","VAS"};

	
	//cliente business
	public By voceElettricoBusiness=By.xpath("//h2/span[text()='Carrello']/ancestor::div[@role='region']//div[text()='Elettrico - Business']/span[text()='Cliccabile' or text()='Aperta']");
	public By popupKOCVP = By.xpath("//h2[text()='KO CVP LIGHT']/..//p");
	public String testo= "Gentile * le segnaliamo che, a seguito della sua richiesta e delle verifiche circa il rischio credito effettate sulla Sua posizione (Credit Check), non è intenzione di Enel Energia procedere all'attivazione della nuova fornitura sul Mercato Libero (ELE O GAS).da leggere solo se il cliente effettua una domanda del tipo: ma lei cosa ne sa della mia situazione creditizia? Come in premessa, è previsto un controllo creditizio In base all'esito Enel Energia si riserva la facoltà di non perfezionare il contratto. Sono spiacente ma Enel Energia, non è obbligata a dar corso alle richieste di contratto di fornitura presentate dal cliente sul Mercato Libero.";
	
    public CarrelloComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}

	 
    public void insertValore(By oggetto,String valore) throws Exception{
	   TimeUnit.SECONDS.sleep(1);
	   WebElement element = util.waitAndGetElement(oggetto,60,2);
	   util.objectManager(oggetto, util.sendKeys, valore);	
	   TimeUnit.SECONDS.sleep(1);
	element.sendKeys(Keys.ENTER);
		 
    }
    
    public void verificaTipologieProdotti(String [] product) throws Exception{
    	 if (!util.exists(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='Componenti Prezzo Energia']/div[contains(@class,'showAllFamilyItem')]"), 15))
		    	throw new Exception("nella sezione 'Carrello' il prodotto 'Componenti Prezzo Energia' non esiste.");
    	 List<WebElement> prodList = util.waitAndGetElements(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@class='principal-color-configurator']/div[contains(@class,'showAllFamilyItem')]"));
    	 TimeUnit.SECONDS.sleep(2);
    	
		 if (prodList.size()==product.length) {
			for (int i = 0; i < product.length; i++) { 
				WebElement d= util.waitAndGetElement(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@class='principal-color-configurator']["+(i+1)+"]/div[contains(@class,'showAllFamilyItem')]"));
				String description=d.getText();
				//System.out.println(description);
				 if(!description.contentEquals(product[i].trim()))
				       	throw new Exception("nella sezione 'carrello' e' cambiata la lista prodotti: quello visualizzato e' "+description+"; quello atteso e' "+product[i]);  
			  
                   }
		 }
    }
    
    public void verificaTipologieProdottiGas(String [] product) throws Exception{
   	 if (!util.exists(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='Componenti Prezzo Gas']/div[contains(@class,'showAllFamilyItem')]"), 15))
		    	throw new Exception("nella sezione 'Carrello' il prodotto 'Componenti Prezzo G' non esiste.");
   	 List<WebElement> prodList = util.waitAndGetElements(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@class='principal-color-configurator']/div[contains(@class,'showAllFamilyItem')]"));
   	 TimeUnit.SECONDS.sleep(2);
   	
		 if (prodList.size()==product.length) {
			for (int i = 0; i < product.length; i++) { 
				WebElement d= util.waitAndGetElement(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@class='principal-color-configurator']["+(i+1)+"]/div[contains(@class,'showAllFamilyItem')]"));
				String description=d.getText();
				//System.out.println(description);
				 if(!description.contentEquals(product[i].trim()))
				       	throw new Exception("nella sezione 'carrello' e' cambiata la lista prodotti: quello visualizzato e' "+description+"; quello atteso e' "+product[i]);  
			  
                  }
		 }
   }
    
    public void verificaTipologieProdottiBusiness() throws Exception{
   	// if (!util.exists(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='Opzione KAM_AGCOR']/div[text()='Opzione KAM_AGCOR']"), 15))
		    //	throw new Exception("nella sezione 'Carrello' la voce 'Opzione KAM_AGCOR' non esiste.");
	   	if (!util.exists(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='Componenti Prezzo Energia']/div[text()='Componenti Prezzo Energia']"), 15))
	    	throw new Exception("nella sezione 'Carrello' la voce 'Componenti Prezzo Energia' non esiste.");
	   	if (!util.exists(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='VAS']/div[text()='VAS']"), 15))
	    	throw new Exception("nella sezione 'Carrello' la voce 'VAS' non esiste.");
	   	if (!util.exists(By.xpath("//div[text()='Configurazione']//ancestor::div[@id='familiesList']//div[@id='Sconti']/div[text()='Sconti']"), 15))
	    	throw new Exception("nella sezione 'Carrello' la voce 'Sconti' non esiste.");
    }
    
	public void aggiungiAlCarrello(String itemName) throws Exception {
		String xpathString = aggiungiAlCarrello.replace("#", itemName);
		By item = By.xpath(xpathString);
		clickComponent(item);
	}
	
	 public void popolareCampoOpzioneOpzioneKAM_AGCOR(String OpzioneKAM_AGCOR) throws Exception{
		 util.objectManager(By.xpath("//legend[text()='Opzione KAM_AGCOR']/ancestor::div[@id='attr']//select"),util.select,OpzioneKAM_AGCOR);
		 }
	 
	
	
	public void checkPopupCheckoutAndClickConferma() throws Exception {
		 if (!util.exists(By.xpath("//h2[text()='Checkout']/ancestor::div[@role='dialog']//button[text()='Annulla']"), 15))
		    	throw new Exception("nella popup 'Checkout' non e' presente il pulsante 'Annulla'");
		 if (!util.exists(By.xpath("//h2[text()='Checkout']/ancestor::div[@role='dialog']//button[text()='Conferma']"), 15))
		    	throw new Exception("nella popup 'Checkout' non e' presente il pulsante 'Conferma'");
		 clickComponent(By.xpath("//h2[text()='Checkout']/ancestor::div[@role='dialog']//button[text()='Conferma']"));
	}
	
	public void checkoutEffettuato() throws Exception{
		
		
			for (int i=0;i<8;i++) {
				TimeUnit.SECONDS.sleep(10);
				if (util.exists(labelCheckoutVerificato, 15))
					break;
			}
			
			if (!util.exists(labelCheckoutVerificato, 15)) throw new Exception("non e' presente la label 'CHECK OUT EFFETTUATO'");
		
	 }
	
	public void checkAfterCheckoutEffettuato(String itemName, String pod) throws Exception{
		String offerta="//div[contains(@class,'dimCol ')]//span[text()='#']";
		String xpathStringOfferta = offerta.replace("#", itemName);
		By itemOfferta = By.xpath(xpathStringOfferta);
		
		for (int i=0;i<8;i++) {
			TimeUnit.SECONDS.sleep(10);
			if (util.exists(itemOfferta, 15))
				break;
		}
		if (!util.exists(itemOfferta, 15)) throw new Exception("non e' presente in tabella il prodotto aggiunto "+itemName);
		
		String fornitura="//div[contains(@class,'dimCol ')]//span[text()='#']";
		String xpathStringPod = fornitura.replace("#", pod);
		By itemPod = By.xpath(xpathStringPod);
		if (!util.exists(itemPod, 15)) throw new Exception("non e' presente in tabella il POD "+pod);
		
		if (!util.exists(By.xpath("//span[contains(text(),'Ordine')]"), 15)) throw new Exception("non e' presente il numero dell'ordine ");
	}


	public void verificaTestoPopup(By popupKOCVP2, String nomeCognome) throws Exception {
		
		if (!util.exists(popupKOCVP2, 15)) throw new Exception("non e' presente la popup di esito KO CVP");
		
		String text =driver.findElement(popupKOCVP2).getText().replaceAll("\n", "");
		String temp = this.testo.replace("*", nomeCognome);
		if(!text.equalsIgnoreCase(temp)) {
			throw new Exception("Il testo presente nella popup dell'esito CVP differisce da quanto atteso: "+temp+" Presente: "+testo );
		}
	}
}
