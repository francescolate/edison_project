package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class IntestazioneNuovaRichiestaComponentEVO {

	//La classe viene utilizzata per verificare l'intestazione di una richiesta appena creata
	//e viene recuperato il numero richiesta generato e salvato nel property
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By h1IntestazionePaginaNuovaRichiesta = By.xpath("//div[@id='headerContainer']/h1");
	public By pNRichiestaPaginaNuovaRichiesta = By.xpath("//p[contains(text(),'N. RICHIESTA')]");
	public By pNRichiestaPaginaNuovaRichiesta2 = By.xpath("//*[contains(text(),'N. Richiesta')] | //*[contains(text(),'N. RICHIESTA')]");
	public By pNRichiestaPaginaNuovaRichiesta3 = By.xpath("//p[contains(text(),'REQUEST N.')]");
	public By pNRichiestaPaginaNuovaRichiesta4 = By.xpath("//p[contains(text(),'Richiesta Id')]");
	public By pNRichiestaPaginaNuovaRichiesta5 = By.xpath("//*[contains(text(),'N.RICHIESTA')]");
	//Claudio SWA-EVO
	public By pNRichiestaPaginaNuovaRichiesta6 = By.xpath("//h2[contains(text(),'Richiesta')]"); 
	public By pNRichiestaPaginaNuovaRichiesta7 = By.xpath("//*[contains(text(),'N. RICHIESTA')]");
	public By alert = By.xpath("//h2[text()='Selezione Mercato']/ancestor::div[@role='region']//div[@data-id='errorMexId']//p");
	public String intestazionePaginaAvvioEstrattoConto = "Avvio Estratto Conto";
	public String intestazionePaginaAvvioAttivazioneVAS = "Avvio Attivazione VAS";
	public String intestazionePaginaAvvioGestioneProvacy = "Avvio Gestione Privacy";
	public String intestazionePaginaAvvioEstrattoContoCanoneTV ="Avvio Estratto Conto Canone TV";
	public String intestazionePaginaAvvioRettificaMercato = "Avvio Rettifica Mercato";
	public String intestazionePaginaAvvioAttivazioneSDD = "Avvio Attivazione SDD";
	public String intestazionePaginaAvvioVariazioneSDD = "Avvio Variazione SDD";
	public String intestazionePaginaAvvioRevocaSDD = "Avvio Revoca SDD";
	public String intestazionePaginaAvvioVerificaContatoreEle = "Avvio Verifica Contatore ELE";
	public String intestazionePaginaAvvioVerificaContatoreGas = "Avvio Verifica Contatore GAS";
	public String intestazionePaginaAvvioModificaAnagrafica = "Avvio Modifica Anagrafica";
	public String intestazionePaginaAvvioVolturaConAccollo = "Avvio Voltura con accollo";
	public String intestazionePaginaAvvioCopiaDocumenti = "Avvio Copia Documenti";
	public String intestazionePaginaAvvioModificaStatoResidenza = "Avvio Modifica Stato Residenza";
	public String intestazionePaginaAvvioModificaImpiantoGas = "Avvio Modifica Impianto Gas";
	// public String intestazionePaginaAvvioSpostamentoContatoreELE = "Avvio Spostamento Contatore ELE";
	public String intestazionePaginaAvvioSpostamentoContatoreELE = "Spostamento Contatore ELE";
	public String messageAlert = "Non sei abilitato ad eseguire operazioni sul Mercato Salvaguardia. Invita il cliente a recarsi presso un Punto Enel.";
	
	
	public IntestazioneNuovaRichiestaComponentEVO(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}
	
	public void verificaIntestazioneNuovaRichiesta(By intestazionepagina,String intestazioneattesa) throws Exception{
		FluentWait<WebDriver> wait = new WebDriverWait(this.driver,100).ignoring(WebDriverException.class).pollingEvery(2,TimeUnit.SECONDS);
		//wait.until(ExpectedConditions.elementToBeClickable(intestazionepagina));
		wait.until(ExpectedConditions.presenceOfElementLocated(intestazionepagina));
		//Verifico corretta intestazione
		String intestazione=null;
		intestazione=driver.findElement(intestazionepagina).getText();
//		//System.out.println("Intestazione mostrata:"+intestazione);
//		//System.out.println("Intestazione attesa:"+intestazioneattesa);
		if(intestazione.compareToIgnoreCase(intestazioneattesa)!=0){
			throw new Exception("L'intestazione mostrata:"+intestazione+" è diversa da quella attesa:"+intestazioneattesa);
		//Salvo numero richiesta
		}
	}

	public String salvaNumeroRichiestaVas(By nRichiesta) throws Exception{
		util.getFrameActive();
		String numerorichiesta=null;
		String labelnumerorichiesta="RICHIESTA";
		util.waitUntilIsDisplayed(nRichiesta);
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1);
		Logger.getLogger("").log(Level.INFO, "NUMERO_RICHIESTA="+numerorichiesta);
		return numerorichiesta;
	}

	public String salvaNumeroRichiesta(By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="RICHIESTA";
		util.waitUntilIsDisplayed(nRichiesta);
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1);
		Logger.getLogger("").log(Level.INFO, "NUMERO_RICHIESTA="+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiesta2(By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="N. RICHIESTA";
		util.waitUntilIsDisplayed(nRichiesta);
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
//		System.out.println(numerorichiesta);
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiesta3(String frame,By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="Richiesta Id";
//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		numerorichiesta=driver.findElement(nRichiesta).getText();
		driver.switchTo().defaultContent();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "Richiesta Id:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiesta(String frame,By nRichiesta) throws Exception{
		String numerorichiesta;
		String labelnumerorichiesta="N. RICHIESTA";
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		numerorichiesta=driver.findElement(nRichiesta).getText();
		driver.switchTo().defaultContent();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1);
		Logger.getLogger("").log(Level.INFO, "Numero richiesta:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiestaAllaccio(By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="REQUEST N.";
		numerorichiesta=driver.findElement(nRichiesta).getText();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}

	public void VerificaAlert(String message, By alert) throws Exception {
		
		String temp=driver.findElement(alert).getText();
		if(!temp.equals(message)) {
			throw new Exception("Il messaggio di alert non corrisponde a quello previsto - atteso: "+ message+" - visualizzato: "+temp);
		}
		
	}

}
