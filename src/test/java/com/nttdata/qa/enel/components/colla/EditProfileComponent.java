package com.nttdata.qa.enel.components.colla;

import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WebDriverManager;

public class EditProfileComponent {

	WebDriver driver;
	SeleniumUtilities util;
	Properties prop = null;
	
	public By titleCondizioniGenerali = By.xpath("//h3[@id='condizioni-generali']");
	public By emailLabel = By.xpath("//div[@class='row'][1]/div[@class='form-group col-xs-12 col-sm-12']/label");
	public By emailField = By.xpath("//input[@id='email']");
	public By personaleLabel = By.xpath("//div[@class='cmText2 cmMarginYlg cmPaddingTopXs']");
	public String name = "//dl[@aria-describedby='anagrafica-personale']/span[1]/dd";
	public String cogNome = "//dl[@aria-describedby='anagrafica-personale']/span[2]/dd";
	public String phoneNum = "//dl[@aria-describedby='credenziali-accesso']/span[2]/dd";
	public String codiceFiscale = "//dd[@id='personalId']";
	public By namelabel = By.xpath("//dl[@aria-describedby='anagrafica-personale']/span[1]/dt");
	public By cogNomeLabel = By.xpath("//dl[@aria-describedby='anagrafica-personale']/span[2]/dt");
	public By phoneNumLabel= By.xpath("//dl[@aria-describedby='credenziali-accesso']/span[2]/dt");
	public By codiceFiscaleLabel = By.xpath("//dl[@aria-describedby='anagrafica-personale']/span[3]/dt");
	public By iIcon = By.xpath("//div[@class='form-group col-xs-12 col-sm-12'][3]/span[@class='dsc-icon-info-circle']");
	public By modalTitle = By.xpath("//div[@class='modal-dialog']/div[@class='modal-header']/h3/p");
	public By modalText = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/p");
	public By hereLink = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/p/a");
	public By modalClose = By.xpath("//span[@class='dsc-icon-close']");
	public By checkbox1 = By.xpath("//label//button[@data-title='Informativa privacy']");
	public By checkbox2 = By.xpath("//label//button[@data-title='Condizioni generali dei servizi offerti']");
	public By privacyDisclaimer = By.xpath("//div[@id='generatedConsents']/div[@class='informative-new'][1]/div[@class='uIdcheckbox-container']/div[@class='form-group checkbox col-xs-12 col-sm-12']/label[@id='onlytext']");
	public By privacyDisclaimerClose = By.xpath("//div[@class='container']/button[@class='btn-close pull-right']");
	public By generalCondition = By.xpath("//div[@class='informative-new'][2]/div[@class='uIdcheckbox-container']/div[@class='form-group checkbox col-xs-12 col-sm-12']/label[@id='onlytext']");
	public By generalConditionClose = By.xpath("//div[@class='container']/button[@class='btn-close pull-right']");
	public By modificaEmailMenu = By.xpath("//a[@id='modify-email-button']");
	public By modificaEmailTitle = By.xpath("//form[@id='cambia-email-form']/div[@class='form-group form-group-full']/label");
	public By modificaEmailText = By.xpath("//h1[@id='modifica-e-mail']/../p");
	public By newEmailLabel = By.xpath("//label[@id='lebel_nuovaEmail']");
	public By newEmailInput = By.xpath("//input[@id='input_nuovaEmail']");
	public By confirmNewEmail = By.xpath("//label[@id='lebel_confermaEmail']");
	public By confirmNewEmailInput = By.xpath("//input[@id='input_confermaEmail']"); 
	public By requiredFieldEmail = By.xpath("//div[@class='form-group requiredFields']/p");
	public By emailConfirmation = By.xpath("//button[@id='confirmChangeEmail']");
	public By modificaPasswordMenu = By.xpath("//a[text()='Modifica Password']");
	public By modificaPasswordTitle = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group form-group-full']/label");
	public By oldPassword = By.xpath("//div[@class='form-group']/label[contains(text(),'Conferma nuova password*')]");
	public By oldPasswordInput = By.xpath("//input[@id='confirmpassword']");
	public By newPassword = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group'][1]/label");
	public By newPasswordInput = By.xpath("//input[@id='newpassword']");
	public By confirmPassword = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group'][2]/label");
	public By confirmPasswordInput = By.xpath("//input[@id='confirmpassword']");
	public By passwordHint = By.xpath("//span[@class='confirm-password-hint']");
	public By requiredFieldPsw = By.xpath("//div[@class='form-group requiredFiltersAreaClienti']/p");
	public By pswConfirmation = By.xpath("//button[@id='confirmChangePassord']");
	public By deleteRegistrationMenu = By.xpath("//a[text()='Elimina Registrazione']");
	public By deleteRegistrationText1 = By.xpath("//div[@class='elimina-profilo parbase']/div[@id='elimina-profilo']/h3");
	public By deleteRegistrationText2 = By.xpath("//div[@id='elimina-profilo']/p");
	public By continueDeleteReg = By.xpath("//div[@class='rich-text_inner color-scheme--white']/div[@class='elimina-profilo parbase']/div[@id='elimina-profilo']/div[@class='btn-wrapper']/button");
	public By faqMenu = By.xpath("//a[text()='Faq']");
	public By privacyMenu = By.xpath("//a[text()='Informativa Privacy']");
	public By legalClausesMenu = By.xpath("//a[text()='Clausole Legali']");
	public By customerAreaMenu =By.xpath("//div[@class='modifica-profilo-title parbase'][8]/li/a");
	public By logOutMenu = By.xpath("//a[@class='logout-btn']");
	public By generalConditions = By.xpath("//div[@class='modal-body']/div[@class='row']//div[@class='container']");
	public By informativaPrivacy = By.xpath("//div[@class='cmModalTitle cmTitle1']/ancestor::div[@class='container']");
	
	public EditProfileComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}

	public void checkPopUpText(By title, By text, String property) throws Exception {

		String text1 = driver.findElement(title).getText();
		String text2 = driver.findElement(text).getText();
		
		if(! property.contains(text1) && property.contains(text2))
			throw new Exception("Pop up text is not matching with the expected value "+property);
	}
	
	public void checkFieldValue(String xpath, String property) throws Exception{

		WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        Thread.sleep(2000);
       // util.objectManager(By.xpath(xpath), util.scrollToVisibility, false);
        String s = driver.findElement(By.xpath(xpath)).getText();
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).text");
        Thread.sleep(1000);
      //  System.out.println("value "+s);
        if(!s.equalsIgnoreCase(property))
              throw new Exception ("Field value is not matching with the test data");
        
 }
	
	public void SwitchTabandClose(String qstn1, String qstn2, String qstn3, String qstn4, String qstn5) throws Exception
	{
	   
		Set <String> windows = driver.getWindowHandles();
		 String currentHandle = driver.getWindowHandle();
		
		// String qstn1 = "Chi può accedere all’area riservata di Enel?";
		for (String winHandle : windows) {
		   // Switch to child window
		//	System.out.println("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
		 
		 SupportFaqComponent sqc = new SupportFaqComponent(driver);
			
			sqc.verifyComponentExistence(sqc.pageTitle);
			
			util.scrollToElement(driver.findElement(sqc.qst1));
			verifyComponentExistence(sqc.qst1);
			verifyComponentExistence(sqc.qst2);
			verifyComponentExistence(sqc.qst3);
			verifyComponentExistence(sqc.qst4);
			verifyComponentExistence(sqc.qst5);

		//	System.out.println(driver.findElement(sqc.qst1).getText()+ " ");
			
			checkForText(sqc.qst1, qstn1);

			checkForText(sqc.qst2, qstn2);

			checkForText(sqc.qst3, qstn3);

			checkForText(sqc.qst4, qstn4);
			
			checkForText(sqc.qst5, qstn5);
	 
			driver.close();
			driver.switchTo().window(currentHandle);

	 }
	
	public void checkForText(By checkObject, String property) throws Exception {
		
	
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);
		
	}
	
	public void checkForPlaceholderText(By checkObject, String property) throws Exception{
		
		String actualText = driver.findElement(checkObject).getAttribute("placeholder");
		
		if(!property.equalsIgnoreCase(actualText))				
				throw new Exception("Placeholder text is not matching with the value "+property);			
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{		
        if(!url.equals(driver.getCurrentUrl()))
            throw new Exception("Redirection failed! The current URL is different from the one saved before.");
    }
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}	
}
