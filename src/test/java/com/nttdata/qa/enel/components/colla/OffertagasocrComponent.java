package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OffertagasocrComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By attivaSubito = By.xpath("//a[contains(text(),'Attiva subito')][1]");
	public By attivaOraNew=By.xpath("//a[@id='priceHeaderID']");
	public String offertaUrlgas = "https://www-coll2.enel.it/it/adesione-contratto";
	public String offerUrlgas115 = "https://www-coll1.enel.it/it/adesione-contratto?productType=res&zoneid=valore_luce_plus-hero";
	public By aderisci = By.xpath("//p[text()='ADERISCI']");
	public By pageTitle = By.xpath("//h1[text()='Valore Luce Plus']");
	public By pageText = By.xpath("//p[@class='description_container']");
	public By caricaBolletta = By.xpath("//button[text()='CARICA BOLLETTA']");
	//public By compilaManualmente = By.xpath("//button[contains text()='COMPILA MANUALMENTE']");
	public By compilaManualmente = By.xpath("//button[@class='exit_button'][1]");
	//$x("//button[@class='exit_button'][1]")
		
	//public By compilaManualmente = By.xpath("//button[normalize-space()='COMPILA MANUALMENTE']");
	public By inserisciITuoiDati = By.xpath("//h3[text()='Inserisci i tuoi dati']");
	public By informazioniFornitura = By.xpath("//h3[text()='Informazioni fornitura']");
	public By pagamentiEBollettesection = By.xpath("//h3[text()='Pagamenti e bollette']");
	public By consensi = By.xpath("//h3[text()='Consensi']");
	public By nome = By.xpath("//input[@id='nameId']");
	public By cognome = By.xpath("//input[@id='surnameId']");
	public By cf = By.xpath("//input[@id='cfId']");
	public By cellulare = By.xpath("//input[@id='phoneId']");
	public By email = By.xpath("//input[@id='emailId']");
	public By emailConferma = By.xpath("//input[@id='emailConfirmId']");
	public By checkBox = By.xpath("(//span[contains(text(),'Ho preso visione')])[1]");
	public By prosegui1 = By.xpath("(//button[text()='PROSEGUI'])[1]");
	public By CONTINUA = By.xpath("(//button[text()='CONTINUA'])[1]");
	public By subentro = By.xpath("//input[@id='SUBENTRO']");
	
	public By Utilizzogas = By.xpath("//p[@class='infoCap']");
	public By Cottura = By.xpath("//p[@class='infoCap']");
	public By AcquaCaldaeRiscaldamento = By.xpath("//p[@class='infoCap']");
	public By Tipologiaabitazion = By.xpath("//p[@class='infoCap']");
	public By Appartamento = By.xpath("//p[@class='infoCap']");
	public By Numeropersone = By.xpath("//p[@class='infoCap']");
	
	
	public By subentrotext1 = By.xpath("//p[@class='infoCap']");
	public By subentrotext2 = By.xpath("//p[@class='infoCap']");
	public By informazioneText1 = By.xpath("//p[@class='infoCap']");
	public By informazioneText2 = By.xpath("//p[@class='infoCap']");
	
	public By pdr = By.xpath("//input[@id='pod-pdr']");
	
	public By iban = By.xpath("//input[@id='pod-pdr']");
	
	public By Datidellimmobile = By.xpath("//input[@id='Datidellimmobile']");
	
	public By Cartadidentità = By.xpath("//input[@id='Cartadidentità']");
	
	
	
	
	public By cap = By.xpath("//input[@id='capSupply']");
	public By privacyText = By.xpath("//p[@class='privacy_text']");
//	public By prosegui2 = By.xpath("(//button[text()='PROSEGUI'])[2]");
	public By inseriscilodopo = By.xpath("(//button[text()='INSERISCILO DOPO'])");
	public By labelPrivacyCheckbox = By.xpath("//label[@id='label-privacy-consent']");
	public final String switchFrame = "//div[@id='iframe']//iframe";
	public By indrizzoDiFornituraText = By.xpath("//p[@class='infoCap']");
	public By citta = By.xpath("//input[@id='citySupply']");
	public By indrizzo = By.xpath("//input[@id='addressSupply']");
	public By numeroCivico = By.xpath("//input[@id='houseNumberSupply']");
	public By indrizzoDiFornituraCAP = By.xpath("//input[@id='postalCodeSupply']");
	public By indrizzoDiFornituraPrivacyText = By.xpath("//p[@class='privacy_text']");
	public By Fornitore = By.xpath("//input[@id='currentSupplier']");
	public By recapiti = By.xpath("//p[text()='Recapiti']");
	public By Recaptival = By.xpath("//h3[contains(text(),'Sei residente')]");
	public By recapitians1 = By.xpath("(//span[contains(text(),'SI')])[1]");
	public By recapitians2 = By.xpath("(//span[contains(text(),'NO')])[1]");
	public By recapitiqst2 = By.xpath("//p[contains(text(),'Dove')]");
	public By recapitiansallo = By.xpath("//span[contains(text(),'Allo stesso')]");
	public By recapitiansTwo = By.xpath("//span[contains(text(),'Ad un ')]");
	public By salvaEContinuaDopo = By.xpath("(//button[contains(text(),'SALVA E CONTINUA DOPO')])[1]");
	public By indrizzoDiFornituraPrivacyText2 = By.xpath("//p[@class='text_priv']");
	public By capdropdown = By.xpath("//div[text()='60027-OSIMO']");
	public By popup = By.xpath("//span[contains(text(),'Sono ')]");
	public By popupcosetext = By.xpath("//span[contains(text(),'Sono ')]");
	public By popupclose = By.xpath("//span[contains(text(),'Sono ')]");
	public By pdrtext = By.xpath("//span[contains(text(),' PDR (Punto di Riconsegna) è il codice che identifica la fornitura di gas. ')]");
	public By popuptext1 = By.xpath("//span[contains(text(),'si compone di 14 caratteri numerici ')]");
	public By popuptext2 = By.xpath("//span[contains(text(),'non cambia anche se cambi il fornitre di gas ')]");
	public By popuptext3 = By.xpath("//span[contains(text(),'Se la fornitura è gia attiva lo trovi nella bolletta. ')]");
	public By popuptext4 = By.xpath("//span[contains(text(),'La matricola contatore è il codice univoco che identifica il contatore. ')]");
	
	
	public By popupclosetext1 = By.xpath("//span[contains(text(),'Per procedere con la tua attivazione, tieni a portata di mano le seguenti informazioni')]");
	public By popupclosetext2 = By.xpath("//span[contains(text(),' Dati sul regolare possesso e detenzione dell' immobile. Uno tra queste informazioni')]");
	public By popupclosetext3 = By.xpath("//span[contains(text(),'Proprietà/Usufrutto')]");
	public By popupclosetext4 = By.xpath("//span[contains(text(),' Locazione/Comodato')]");
	public By popupclosetext5 = By.xpath("//span[contains(text(),'Altro diritto sull'immobile')]");
	
	
	
	
	public By Numero_Civico_Input = By.xpath("//label[text()='Numero Civico*']/following-sibling::input");
	public By insertManually = By.xpath("//span[text()='Compila manualmente']");
	public By AttualeFornitoreselect = By.xpath("//div[@title='ACEA ENERGIA SPA']");

	public By Metodo_di_Pagamento_Field = By.xpath("//p[contains(text(),'Metodo di Pagamento')]");
	public By Metodo_di_Pagamento_FieldDefaultValue = By.xpath("(//span[@class='ant-select-selection-item'])[1]");
	public By Metodo_di_Pagamento_Bolletino = By.xpath("(//span[@class='ant-select-selection-item'])[1]");
	public By Metodo_di_Pagamento_FieldSelectValue = By.xpath("(//div[text()='Bollettino Postale'])[2]");
	public static final String METADO_DROPDOWN_VALUE= "Bollettino Postale";
	public static final String Bolletino_selection= "Bollettino Postale";
	public static final String Bolletino_selection115= "Addebito su conto corrente";
	public By Codice_IBAN = By.xpath("//input[@id='iban-text-field']");
	public By Codice_IBAN_Radio_si = By.xpath("//span[contains(text(),'Sono ')]");
	public By Codice_IBAN_Radio_no = By.xpath("//span[contains(text(),'del conto corrente ')]");
//	public By indrizzoDiFornituraCap = By.xpath("//input[@id='postalCodeSupply']");
	public By indrizzoDiFornituraCapDropdown = By.xpath("//div[@label='60027-OSIMO']");
	
	public By Modalità_di_ricezione_Heading = By.xpath("//p[contains(text(),'Modalità di ricezione della bolletta')]");
	public By Modalità_di_ricezione_Radio1 = By.xpath("//span[contains(text(),'Voglio ricevere la bolletta')]/parent::label");
	
	public By Modalità_di_ricezione_Radio2 = By.xpath("//span[contains(text(),'Voglio ricevere la fattura')]/parent::label");
	
	public By Codici_Promozionali_Heading = By.xpath("//label[text()='Codice Promozionale']");
	public By Codici_Promozionali_Subtext = By.xpath("//p[contains(text(),'Se sei in possesso di un')]");
	public By Codici_Promozionali_DiscountCode = By.xpath("//input[@id='idPromo']");
	public By Codici_Promozionalitest = By.xpath("//input[@id='idPromo']");
	public By codiceibanfield = By.xpath("//span[text()='idPromo']");
	
	//Codici_Promozionalitest
	public static final String ModalitaricezioneEmailbollete = "//input[@name='email']";
	public static final String ModalitaricezioneTelefonobollete = "//input[@name='phone']";
	public By bollettinoPostaleText = By.xpath("//p[@class='upper_text_iban']");

	public By Consensi_Heading = By.xpath("//h3[text()='Mandati e Consensi']");
	//public By Consensi_TextInBox = By.xpath("(//div[@class='textarea_appearance consent_box'])[1]");
	public By Consensi_chkbx1_Text = By.xpath("(//span[contains(text(),'Ho preso visione dell')])[1]");
	public By Consensi_chkbx2_Text = By.xpath("(//span[@class='MuiTypography-root MuiFormControlLabel-label MuiTypography-body1'])[2]");
	public By Mandati_e_Consensi_chkbx3_Text = By.xpath("//span[contains(text(),'Voglio recedere')]");
	public By Richiesta_Heading = By.xpath("//h3[text()='Richiesta di esecuzione anticipata del contratto']");
	public By Richiesta_TextInBox = By.xpath("//p[@class='promo_execution']");
	public By Richiesta_LinkText = By.xpath("//div[contains(text(),'Maggiori Informazioni')]");
	public By Richiesta_SI = By.xpath("//span[contains(text(),'Si, voglio')]");
	public By Richiesta_Di_NO = By.xpath("//span[contains(text(),'No, voglio')]");
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupContent = By.xpath("//div[@class='modal_body']//p[contains(text(),'Scegliendo di non')]");
	public By Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_NO_PopupClose = By.xpath("//button[@class='modal_close_icon']");
	
	public By Richiesta_Di_Esecuzione_POPUP_content = By.xpath("//div[@class='modal_body']//p[contains(text(),'Il Codice del Consumo prevede 14 giorni ')]");	
	public By Richiesta_Di_Esecuzione_POPUP_close = By.xpath("//button[@class='modal_close_icon']");
	
	public By Consenso_Marketing_Enel_Energia_Heading = By.xpath("//h4[text()='Consenso marketing Enel Energia']");
	public By Consenso_Marketing_Enel_Energia_TextInBox = By.xpath("//div[@class='textarea_appearance consent_box' and contains(text(),'dati personali da parte di Enel Energia')]");
	public By Enel_Energia_Accetto = By.xpath("(//span[text()='Accetto']/parent::label[@class='MuiFormControlLabel-root'])[1]");
	public By Enel_Energia_NonAccetto = By.xpath("(//span[text()='Non Accetto']/parent::label[@class='MuiFormControlLabel-root'])[1]");
	
	public By Consenso_Marketing_Terzi_Heading = By.xpath("//h4[text()='Consenso marketing terzi']");
	public By Consenso_Marketing_Terzi_TextInBox = By.xpath("//div[@class='textarea_appearance consent_box' and contains(text(),'tra cui Enel X')]");
	public By Terzi_Accetto = By.xpath("(//span[text()='Accetto']/parent::label[@class='MuiFormControlLabel-root'])[2]");
	public By Terzi_NonAccetto = By.xpath("(//span[text()='Non Accetto']/parent::label[@class='MuiFormControlLabel-root'])[2]");
	
	public By Consenso_Profilazione_Enel_Energia_Heading = By.xpath("//h4[text()='Consenso profilazione Enel Energia']");
	public By Consenso_Profilazione_Enel_Energia_TextInBox = By.xpath("//div[@class='textarea_appearance consent_box' and contains(text(),'profilazione ')]");
	public By Energia_Accetto = By.xpath("(//span[text()='Accetto']/parent::label[@class='MuiFormControlLabel-root'])[3]");
	public By CEnel_Energia_NonAccetto = By.xpath("(//span[text()='Non Accetto']/parent::label[@class='MuiFormControlLabel-root'])[3]");
	public By recapitiansOne = By.xpath("//span[contains(text(),'Allo stesso')]");
	public By COMPLETA_ADESIONE_Button = By.xpath("//button[text()='Completa Adesione']");
	public By tornaAllaHomePageTitle = By.xpath("//h1[@class='image-hero_title']");
	public By tornaAllaHomeMSG1 = By.xpath("//h1[@class='image-hero_title']/following-sibling::p[@class='hero_msg1']");
	public By tornaAllaHomeMSG2 = By.xpath("//h1[@class='image-hero_title']/following-sibling::p[@class='hero_msg2']");
	public static String email_id = "";
	public By TornaAllaHomeButton = By.xpath("//a[text()='torna alla home']");
	public By emailConfermaTitle = By.xpath("//div[@class='big-title']");
	public By emailConfermaContent = By.xpath("(//article[@id='containerOtp']//child::div[@class='detail-title'])[1]");
	public By RICHIEDI_CODICE_DI_VERIFICA =By.xpath("//button[text()='RICHIEDI CODICE DI VERIFICA']");
	public By otpText = By.xpath("(//div[@class='detail-title'])[2]");
	public By codiceDiVerifica = By.xpath("//input[@id='tp-codiceverificaIframe']");
	public By verificaCode = By.xpath("//button[text()='VERIFICA CODICE']");
	public By Attuale_Fornitore_InputSelect = By.xpath("//div[@title='ACEA ENERGIA SPA']");
	
	//115
	public By offerHeading = By.xpath("//h1[@class='header']");
	public By offerParagraph = By.xpath("//p[@class='description_container']");
	public By bolletteWebText = By.xpath("//p[@class='price']/span/p[1]");
	public By parapetText = By.xpath("//div[@id='idBox']/div/div[@class='boxContainer']/p[@class='step']");
	public By offerta = By.xpath("//div[@id='idBox']/div/div[@class='boxContainer']//div[@class='boxInfo']/p[@class='offer']");
	public By ValoreLucePlus = By.xpath("//div[@id='idBox']//div[@class='boxInfo']/p[@class='box_header']");
	public By contratto = By.xpath("//div[@id='idBox']/div//div[@class='contractContainer']/p[@class='contract luce30']");
	public By prezzoLuce = By.xpath("//div[@id='idBox']//div[@class='contractContainer']/div[@class='costContainer offP1']/p[@class='cost']");
	public By informazioniFornitura116 = By.xpath("//div[@id='panel2a-header']//div[@class='accordionRow']/h3[@class='sub_header']");
	public By dicosahai115 = By.xpath("//legend[@id='what-you-need-legend']");
	public By cambioFornitura = By.xpath("//span[.='CAMBIO FORNITORE']");
	public By cambioFornituraText = By.xpath("//div[@id='what-you-need']//label[1]//div[2]/span");
	public By subentro115 = By.xpath("//span[.='SUBENTRO']");
	public By subentroText = By.xpath("//div[@id='what-you-need']//label[2]//div[2]/span");
	public By primaAttivazione = By.xpath("//span[.='PRIMA ATTIVAZIONE']");
	public By primaAttivazionetext = By.xpath("//div[@id='what-you-need']//label[3]//div[2]/span");
	public By volutra = By.xpath("//span[.='VOLTURA']");
	public By volutraText = By.xpath("//div[@id='what-you-need']//label[4]//div[2]/span");
	public By confermailCap = By.xpath("//p[@class='infoCap']");
	public By pod = By.xpath("//input[@id='pod-input']");
	public By cap115 = By.xpath("//input[@id='pod-cap']");
	public By pdr115 = By.xpath("//ul/li[1]//span[@class='WhatYouNeedSelect_textInfo__3ozLC']");
	public By iban115 = By.xpath("//ul/li[2]//span[@class='WhatYouNeedSelect_textInfo__3ozLC']");
	public By Datidellimmobile115 = By.xpath("//ul/li[3]//span[@class='WhatYouNeedSelect_textInfo__3ozLC']");
	public By Cartadidentità115 = By.xpath("//ul/li[4]//span[@class='WhatYouNeedSelect_textInfo__3ozLC']");
	public By indirizzoDiFornitura = By.xpath("//h5[@class='InsertSupply_addressTitle__2zyTY']");
	public By citta115 = By.xpath("//input[@id='addressLuce-city']");
	public By indrizzo115 = By.xpath("//input[@id='addressLuce-address']");
	public By numeroCivico115 = By.xpath("//input[@id='addressLuce-civic-number']");
	public By indrizzoDiFornituraCAP115 = By.xpath("//input[@id='addressLuce-cap']");
	public By recapti115 = By.xpath("//h4[@class='InsertSupply_sectionTitle__2BS_i']");
	public By recapti115Text =  By.xpath("//p[contains(text(),'ricordiamo')]");
	public By doveVuoi = By.xpath("//legend[@id='postal-address-same-dual-descr']");
	public By pagamentiEBollette = By.xpath("//div[@id='panel3a-header']/div[@class='MuiAccordionSummary-content Mui-expanded']/div[@class='accordionRow']/h3[@class='sub_header']");
	public By codiceIban = By.xpath("//input[@id='iban-text-field']");
	public By selectedCheckbox = By.xpath("//span[contains(text(),'Voglio ricevere la bolletta ')]");
	public By unselectedCheckbox = By.xpath("//span[contains(text(),'Voglio ricevere la fattura ')]");
	public By codicePromoText = By.xpath("//p[contains(text(),'Se sei in possesso')]");
	public By choosePayment = By.xpath("//input[@id='choosePayment']");
	public By sonoRadioButton1 = By.xpath("//span[contains(text(),'Sono')]");
	public By sonoRadioButton2 = By.xpath("//span[contains(text(),'intestatario del conto corrente ')]");
	public By ricordaText = By.xpath("//form[@id='payment-form']/div/p[@class='upper_text_iban']");
	
	
	public OffertagasocrComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
	 
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void jsClickComponent(By by) throws Exception{
			util.jsClickElement(by);
		}
		
		public void clickTab(By Object) throws Exception {
			driver.findElement(Object).sendKeys(Keys.TAB);;
		}
		
		public void verifyDefaultValue(By checkObject, String Value ) throws Exception
		{
			String textFound = "NO";
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String actualValue = driver.findElement(checkObject).getText();
			
			if (actualValue.contentEquals(Value))
				textFound="YES";
								
			if (textFound.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					
		}
		
		public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
	        
	        WebDriverWait wait = new WebDriverWait(this.driver, 50);
	            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
	        if(value.equals(""))throw new Exception("Field is not pre filled");
	        
	        String match = "FAIL";
	        if(value.contentEquals(expectedValue))
	        	match = "PASS";
	        if(match.contentEquals("FAIL"))
	        	throw new Exception("PrePopulatedValue and Expected values are not matching");
	    }
		
		public void selectMetodoPagamentoValue() throws Exception
		{
			verifyComponentExistence(Metodo_di_Pagamento_FieldDefaultValue);
			clickComponent(Metodo_di_Pagamento_FieldDefaultValue);
			Thread.sleep(2000);
			verifyComponentExistence(Metodo_di_Pagamento_FieldSelectValue);
			clickComponent(Metodo_di_Pagamento_FieldSelectValue);
			
		}
		
		public void verifyElementNotDisplaying(By locator) throws Exception
		{
			List element = (List) driver.findElement(locator);
			if(element.size()>0)
				throw new Exception("Element is displaying");				
		}
		
		public void provideEmail(String email)
		{
			email_id =  email;
		}
		
		
		public void verifyEmailContent(String email,String emailContent) throws Exception{
			System.out.println(emailContent);
			if(!email.contains(emailContent)) 
				throw new Exception("Given String is not present in Email body");
		}
		
		
		public void  isElementPresent(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(existentObject));
			String elementFound = "NO";
			
			try{
			WebElement we =driver.findElement(existentObject);
			we.isDisplayed();
			elementFound = "YES";
			} 
	        catch (Exception e) {

	            System.out.println("Element Not found trying again - " );
	            e.printStackTrace();}
	         }
	  
		
		public static final String PageText = "Vuoi procedere velocemente con l'attivazione?Puoi caricare la tua attuale bolletta in formato digitale e penseremo noi a compilare i tuoi dati.Dovrai solamente verificarli e completarli con alcune informazioni aggiuntive.In alternativa puoi compilare manualmente le informazioni richieste.";
		
		public static final String InformazioneText1 = "Conferma il CAP cliccando sul campo e selezionando dall’elenco la voce corretta.";
		public static final String SUBENTROTEXT1 = "Ti bastano pochi minuti per riattivare un contatore disattivato";
		public static final String SUBENTROTEXT2 = "Ricorda che per completare l’attivazione con l’offerta selezionata devi avere a portata di mano:";
		public static final String PrivacyText = "Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l’adesione.";
		public static final String IndrizzoDiFornituraText = "Verifica e conferma le seguenti informazioni cliccando su ogni campo e selezionando dall’elenco la voce corretta.";
		public static final String POPUPTEXT1 = "si compone di 14 caratteri numerici";
		public static final String POPUPTEXT2 = "non cambia anche se cambi il fornitre di gas";
		public static final String POPUPTEXT3 = "Se la fornitura è gia attiva lo trovi nella bolletta.";
		public static final String POPUPTEXT4 = "La matricola contatore è il codice univoco che identifica il contatore. Genericamente composta da 6-8 cifre, è sempre segnata sul contatore nei dati di targa.";
		public static final String PDRTEXT = " PDR (Punto di Riconsegna) è il codice che identifica la fornitura di gas.";
		public static final String POPUPCLOSETEXT1 = "Per procedere con la tua attivazione, tieni a portata di mano le seguenti informazioni";
		public static final String POPUPCLOSETEXT2 = "Dati sul regolare possesso e detenzione dell' immobile. Uno tra queste informazioni";
		public static final String POPUPCLOSETEXT3 = "Proprietà/Usufrutto";
		public static final String POPUPCLOSETEXT4 = "Locazione/Comodato";
		public static final String POPUPCLOSETEXT5 = "Altro diritto sull'immobile";
		
		public static final String IndrizzoDiFornituraPrivacyText = "Ti informiamo che la fornitura verrà attivata con i medesimi requisiti tecnici (potenza e tensione) attualmente in esercizio. Se lo desideri potrai effettuare una variazione di potenza dopo l'attivazione del contratto direttamente dalla tua area clienti.";
		public static final String IndrizzoPrivacyText = "Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l’adesione.";
		public static final String Recapitiqst1 = "Sei residente presso la fornitura?";
		public static final String Recapitiqst2 = "Dove vuoi ricevere le comunicazioni relative alla tua fornitura?";
		public static final String RecapitiansOne = "Allo stesso indirizzo della fornitura";
		public static final String RecapitiansTwo = "Ad un indirizzo diverso da quello della fornitura";
		public static final String IndrizzoDiFornituraPrivacyText2 = "Ti ricordiamo che la tariffa per uso domestico può essere applicata solo su una delle forniture a te intestate e presso la quale hai la residenza anagrafica.";
		public static final String BollettinoPostaleText = "Ricorda che se scegli l’addebito diretto potrai risparmiare le commissioni previste per il pagamento tramite bollettino postale o Lottomatica.";
		public static final String CodiciPromozionaliSubtext = "Se sei in possesso di un codice promozionale inseriscilo nel campo seguente";
		public static final String Consensi_TextInBox = "Dichiaro di voler conferire ad Enel Energia:mandato irrevocabile senza rappresentanza per lo svolgimento presso il distributore competente delle attività di gestione della connessione dei punti di prelievo (es. aumenti di potenza, spostamenti di gruppi di misura, etc), mantenendo la titolarità di ogni rapporto giuridico con il distributore competente inerente la connessione alla rete dei siti ed impianti. Tale mandato è a titolo oneroso ed obbliga la corrispondere al Fornitore gli importi necessari per l’esecuzione del mandato e per l’adempimento delle obbligazioni che a tal fine il Fornitore ha contratte in proprio nome;mandato irrevocabile con rappresentanza per la sottoscrizione del Contratto per il servizio di connessione alla rete elettrica allegate al contratto per il servizio di trasporto dell'energia elettrica, del cui contenuto il Cliente ha preso atto anche in quanto disponibile sul sito www.enel.it, consapevole che l’accettazione ed il rispetto delle stesse è condizione necessaria per l’attivazione ed il mantenimento del servizio di trasporto.";
		public static final String Consensi_chkbx1Text = "Ho preso visione dell' informativa privacy riguardo come tratterete i miei dati personali.";
		public static final String chkbx2Text =	"Ho preso visione delle note legali ai sensi del D.Lgs 70/2003 e le informazioni precontrattuali ai sensi dell'art 49 del D.Lgs 21/2014 - Codice del Consumo.";
		public static final String Consensi_chkbx3Text = "Voglio recedere dal contratto attualmente in essere con ACEA ENERGIA SPA";
		public static final String Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text = "Ogni consumatore ha diritto ad un periodo di ripensamento di 14 giorni dal completamento del processo di adesione durante i quali non si procede all'avanzamento della pratica di attivazione; se il consumatore lo desidera, può chiedere l'esecuzione anticipata della fornitura. Vuoi avviare subito il processo di attivazione?";
		public static final String Richiesta_Di_Esecuzione_POPUP_Content = "Scegliendo di non effettuare la \"Richiesta di esecuzione anticipata della fornitura\", il Cliente richiede espressamente che le procedure per dar corso all'attivazione vengano avviate solo dopo che siano trascorsi i 14 giorni di ripensamento che partono dalla conclusione del processo di acquisizione e dall’ eventuale restituzione della documentazione obbligatoria. Anche in caso di richiesta di attivazione di un contatore disattivato, chiuso o che non ha mai erogato energia, il cliente sarà tenuto ad attendere il periodo di ripensamento dei 14 giorni previsto dal Codice del Consumo.";
		public static final String Consenso_Marketing_Enel_Energia_Text = "L’interessato acconsente al trattamento dei propri dati personali da parte di Enel Energia, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail) Codice PrivacyCodice Privacy";
		public static final String Consenso_Marketing_Terzi_Text = "L’interessato acconsente al trattamento dei propri dati personali da parte delle Società del Gruppo Enel (tra cui Enel X), da società controllanti, controllate o collegate, o da partner commerciali di Enel Energia, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail) Codice PrivacyCodice Privacy";
		public static final String Consenso_Profilazione_Enel_Energia_Text = "L’interessato acconsente al trattamento dei propri dati personali per attività di profilazione basate sulle abitudini di consumo e sui dati e informazioni acquisite attraverso l’utilizzo dei prodotti o servizi utilizzati, da parte di Enel Energia o da parte di Società del Gruppo Enel, da società controllanti, controllate o collegate o da partner commerciali di Enel Energia.";
		public static final String Richiesta_Di_Esecuzione_PopupContent = "Scegliendo di non effettuare la \"Richiesta di esecuzione anticipata della fornitura\", il Cliente richiede espressamente che le procedure per dar corso all'attivazione vengano avviate solo dopo che siano trascorsi i 14 giorni di ripensamento che partono dalla conclusione del processo di acquisizione e dall’ eventuale restituzione della documentazione obbligatoria. Anche in caso di richiesta di attivazione di un contatore disattivato, chiuso o che non ha mai erogato energia, il cliente sarà tenuto ad attendere il periodo di ripensamento dei 14 giorni previsto dal Codice del Consumo.";
		public static final String TornaAllaHomePageTitle = "Sei ad un passo dal completare l'adesione!";
		public static final String TornaAllaHomeMSG1 = "Ti abbiamo inviato un messaggio all'indirizzoControlla l'email e conferma il contratto della tua fornitura energia.";
		public static final String TornaAllaHomeMSG2 = "Se non dovessi riceverla, verifica la casella della posta indesiderata";
		public static final String EmailContentLine1 = "Gentile PETER PARKER,";
		public static final String EmailContentLine2 = "per confermare l'attivazione dell'offerta Valore Luce Plus fai click sul pulsante entro il";
		public static final String EmailContentLine3 = "Oppure copia e incolla questo link nel tuo browser e premi";
		public static final String EmailContentLine4 = "Ti ricordiamo che la tua fornitura sarà attiva solo dopo la verifica dei dati che ci hai comunicato.";
		public static final String EmailContentLine5 = "Questa email è stata inviata in modo automatico, ti invitiamo a non rispondere a questo indirizzo di posta elettronica.";
		public static final String EmailContentLine6 = "La informiamo che Enel Energia tratterà i suoi dati personali in conformità alla normativa in materia di protezione dei dati personali (Regolamento UE 2016/679, c.d. \"GDPR\").";
		public static final String EmailContentLine7 = "Titolare del trattamento dei dati personali è Enel Energia S.p.A., con sede legale in Viale Regina Margherita, 125 - 00198 - Roma. Informativa completa disponibile sul sito";
		public static final String EmailContentLine8 = "Enel Energia SpA - Società con unico socio - Sede Legale 00198 Roma, viale Regina Margherita 125 - Registro Imprese di Roma - R.E.A. 1150724";
		public static final String EmailContentLine9 = "Gruppo IVA Enel P.IVA  15844561009, C.F 06655971007 Capitale Sociale Euro 302.039,00 i.v. - Direzione e Coordinamento di Enel SpA";

		
		
		public static final String EmailConfermaContent = "Per accedere al link e procedere con l'adesione all'offerta devi inserire il codice di sicurezza che ti verrá inviato al numero di telefono cellulare";
		public static final String OTPText = "Ti abbiamo inviato un codice di verifica al numero di cellulare sopra indicatoInserisci il codice e clicca su Verifica per accedere.";


		
		

		public static final String EmailContent116Line1 = "completa la tua adesione all'offerta E-Light Gas, richiesta su enel.it.";
		public static final String EmailContent116Line2 = "Per inserire gli ultimi dati, clicca sul pulsante entro il";
		public static final String EmailContent116Line3 = "Oppure copia e incolla questo link nel tuo browser e premi";
		public static final String EmailContent116Line4 = "Questa e-mail è stata inviata in modo automatico. Ti invitiamo a non rispondere a questo indirizzo di posta elettronica.";
		public static final String EmailContent116Line5 = "Titolare del trattamento dei dati personali è Enel Energia S.p.A., con sede legale in Viale Regina Margherita 125 00198 Roma - Registro Imprese di Roma Codice Fiscale 06655971007. L'informativa completa è disponibile sul sito enel.it";
		
		public static final String OFFER_HEADING = "Valore Luce Plus";
		public static final String OFFER_PARAGRAPH = "Vuoi procedere velocemente con l'attivazione?Puoi caricare la tua attuale bolletta in formato digitale e penseremo noi a compilare i tuoi dati.Dovrai solamente verificarli e completarli con alcune informazioni aggiuntive.In alternativa puoi compilare manualmente le informazioni richieste.";
		public static final String BOLLETTEWEB_TEXT = "Il prezzo si riferisce alla sola componente energia ed è bloccato per un anno (IVA ed imposte escluse).";
		public static final String PARAPET_TEXT = "Caricamento Bolletta";
		public static final String OFFERTA = "OFFERTA";
		public static final String VALORE_LUCE_PLUS = "Valore Luce Plus";
		public static final String CONTRATTO = "CONTRATTO";
		public static final String PREZZO_LUCE = "Prezzo Luce";
		public static final String INFORMAZIONI_FORNITURA = "Informazioni fornituraelemento 2 di 4";
		public static final String DICOSAHAI = "Di cosa hai bisogno ?";
		public static final String CAMBIO_FORNITORE = "CAMBIO FORNITORE";
		public static final String CAMBIO_FORNITORE_TEXT = "Ho gia un contratto con un altro fornitore ma vorrei passare ad Enel";
		public static final String SUBENTRO = "SUBENTRO";
		public static final String SUBENTRO_TEXT = "Voglio attivare un contatore disattivato";
		public static final String PRIMA_ATTIVAZIONE = "PRIMA ATTIVAZIONE";
		public static final String PRIMA_ATTIVAZIONE_TEXT = "Voglio attivare un nuovo contatore";
		public static final String VOLTURA = "VOLTURA";
		public static final String VOLTURA_TeXT = "Voglio cambiare intestatario a un contratto di fornitura";
		public static final String CONFERMAILCAP = "Conferma il CAP cliccando sul campo e selezionando dall’elenco la voce corretta.";
		public static final String INDIRIZZO_DI_FORNITURA = "Indirizzo di fornitura";
		public static final String RECAPTI_TEXT = "Ti ricordiamo che la tariffa per uso domestico residenziale può essere applicata solo su una delle forniture a te intestate e presso la quale hai la residenza anagrafica.";
		public static final String CODICE_PROMO_TEXT ="Se sei in possesso di un codice promozionale inseriscilo nel campo seguente";
		public static final String RICORDA_TEXT = "Ricorda che se scegli l’addebito diretto potrai risparmiare le commissioni previste per il pagamento tramite bollettino postale o Lottomatica.";
}
