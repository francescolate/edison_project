package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SelezionaQualityCheckComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public SelezionaQualityCheckComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  

	public final By CampoRicerca = By.xpath("//input[@title='Cerca in Salesforce']");
	public final String Risultato = "//span[@title='***']/..";
	public final By PassaLighting = By.xpath("//a[@class='switch-to-lightning']");
	public final By clickOfferta = By.xpath("//div[text()='Offerta']/../a");
	public final By clickQualityCheck = By.xpath("//button[@id='qcConfirm']");
	public final By NomeBarra = By.xpath("//div[@class='appName slds-context-bar__label-action slds-context-bar__app-name']/span");
	
		
	public void selezionaQualityCheck() throws Exception {

//		driver.switchTo().defaultContent();
//
//		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//		boolean flag = false;
//		for (WebElement webElement : els) {
//			
//			
//			if(webElement.findElement(By.xpath("..")).isDisplayed()) {
//				driver.switchTo().frame(webElement);
//				break;
//			}	
//			
//		}
		
		util.getFrameActive();
		
		press(clickQualityCheck);

	}
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	
}
