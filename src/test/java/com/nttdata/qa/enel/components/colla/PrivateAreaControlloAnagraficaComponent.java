package com.nttdata.qa.enel.components.colla;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
//import java.awt.Robot;
//import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.base.Function;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaControlloAnagraficaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	Properties DPQ = new Properties(); // Data Preparation Query
	
	public By sidebarMenuItemAccount = By.id("accountSelfcare");
	public By datiContattiTitleElem = By.id("profile-contact");
	public By datiContattiTextElem = By.cssSelector("#mainContentWrapper .heading p");
	public By datiContattiPopupBtn = By.cssSelector(".heading p a.inline-icon-link");
	public By datiContattiPopupTitle = By.cssSelector("#modalAlert #title");
	public By datiContattiPopupText = By.cssSelector("#modalAlert .inner-text");
	public By datiContattiPopupCloseBtn = By.cssSelector("#modalAlert a.modal-close");
	public By datiContattiModificaBtn = By.xpath("//span[text()='MODIFICA CONTATTI']");
	public By datiContattiModificaTitle = By.xpath("//h1[text()='Dati e Contatti']");
	public By datiContattiModificaText = By.xpath("//h2[text()='Consulta e aggiorna i tuoi dati di contatto']");	
	public By contattiHolderField = By.xpath("//*[@id='holderNameInput']");
	public By contattiHolderCfField = By.id("holderCFInput");
	public By contattiHolderPhoneField = By.xpath("//span[@id='cntPhoneFieldDesc']");//By.id("cntPhoneFieldInput");
	public By contattiHolderCellField = By.xpath("//input[@id='cntMobileFieldInput']");
	public By contattiHolderEmailField = By.xpath("//input[@id='cntEmailFieldInput']");
	public By contattiHolderPECField = By.id("cntPECFieldInput");
	public String contattiReturnSelector = "//span[text()='Indietro']"; // .button_first
	public String contattiSaveSelector = "//span[text()='Salva Modifiche']"; // .button_second
	public String contattiCampiObbligatoriSelector = "//*[@class='foot-notes']";
	public By contattiCellFieldError = By.id("cntMobileFieldError");
	public By contattiEmailFieldError = By.xpath("//span[@id='cntEmailFieldError']");
	public By contattiPECFieldError = By.xpath("//span[@id='cntPECFieldError']");
	
	public By datiEContattiHeader1 = By.xpath("//div[@class='right-main']/h1");
	public By datiEContattiHeader2 = By.xpath("//div[@class='right-main']/h2");
	
	public PrivateAreaControlloAnagraficaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
			
		DPQ.setProperty("ACCOUNT_TITLE", "Dati e Contatti");
		DPQ.setProperty("ACCOUNT_TEXT", "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione");
		DPQ.setProperty("POPUP_TITLE", "Aggiornamento dati");   
		DPQ.setProperty("POPUP_TEXT", "In caso di modifica dei contatti, i dati inseriti saranno resi disponibili dopo qualche ora"); 
		DPQ.setProperty("OWNER", "Mancuso Salvatore");
		DPQ.setProperty("CF", "MNCSVT58H26H325L");
		DPQ.setProperty("TEL", "0818142364");
		DPQ.setProperty("CEL", "+39 3339090890");
		DPQ.setProperty("EMAIL", "fabiana.manzo1@yopmail.com");
		DPQ.setProperty("PEC", "Non presente");
		DPQ.setProperty("CONTACT_BY", "Cellulare");
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public WebElement fluentWait(final By locator) {
	    Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(30, TimeUnit.SECONDS)
	            .pollingEvery(5, TimeUnit.SECONDS)
	            .ignoring(NoSuchElementException.class);

	    WebElement element = wait.until(new Function<WebDriver, WebElement>() {
	        public WebElement apply(WebDriver driver) {
	            return driver.findElement(locator);
	        }
	    });

	    return  element;
	};
	
	public WebElement localWaitAndGet(By by) throws Exception {
//		WebElement element = driver.findElement(by);
		WebElement element = fluentWait(by);
//		WebElement element = util.waitAndGetElement(by);
		return element;
	}
	
	public boolean compareText(String awaited, By by, boolean throwException) throws Exception {
		WebElement elem;
		
//		util.verifyVisibility(By.xpath("//*[@id='spinner'])"), 10, true);
		
		try {
			elem = util.waitAndGetElement(by);
		} catch (Exception e) {
			elem = localWaitAndGet(by);
		}
		
		String cleaned = elem.getText().trim().toLowerCase();
		
		if (false == cleaned.equals(awaited.toLowerCase())) {
			if (throwException) {
//				return false;
				throw new Exception(
						MessageFormat.format("[FAILED COMPARISON] Element''s text {0} is not equal to awaited '"+awaited+"'.", 
						cleaned, awaited.toLowerCase()));
			} else {return false;}
		} else {return true;}
	}
	
	public void checkSectionFields(Map<String, String> map) throws Exception {
		for (Entry<String, String> entry : map.entrySet()) {
			By by = By.cssSelector(entry.getKey());
			String val = entry.getValue();
			try {
				compareText(val, by, true);
			} catch (Throwable e) {
				throw new Exception(e.getMessage());
			}
		}
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkContactFields() throws Exception {
		java.util.Map<String, String> map = new HashMap<String, String>();
		
		map.put("#accessibility dd", DPQ.getProperty("OWNER"));
		map.put("#profile dl span:nth-child(2) dd", DPQ.getProperty("CF"));
		map.put("#profile dl span:nth-child(3) dd", DPQ.getProperty("TEL"));
		map.put("#cellBsn dd", DPQ.getProperty("CEL"));
		map.put("#profile dl span:nth-child(5) dd", DPQ.getProperty("EMAIL"));
		map.put("#profile dl span:nth-child(6) dd", DPQ.getProperty("PEC"));
		map.put("#canaleBsn dd", DPQ.getProperty("CONTACT_BY"));
		
		checkSectionFields(map);
	}
	
	public boolean isEnabled(WebElement elem) {
		if (elem.isEnabled()) {
			return true;
		} return false;
	}
	
	public void checkHolderFields() throws Exception {
		String EXC_MSG = "Il campo {0} ha un valore diverso da quello atteso: {1}";
		
//		if (false == compareText(DPQ.getProperty("OWNER"), contattiHolderField, false)) {
////			System.out.println(1);
//			throw new Exception(
//				//MessageFormat.format(EXC_MSG, DPQ.getProperty("OWNER"))
//			);
//		}
//		if (false == compareText(DPQ.getProperty("CF"), contattiHolderCfField, false)) {
////			System.out.println(2);
////			throw new Exception(
////				MessageFormat.format(EXC_MSG, DPQ.getProperty("CF"))
////			);
//		}
		if (false == compareText(DPQ.getProperty("TEL"), contattiHolderPhoneField, false)) {
			String defaultValue = "Inserisci il telefono";
			if (false == compareText(defaultValue, contattiHolderPhoneField, false)) {
				String awaitedProps = DPQ.getProperty("TEL") + " o " + defaultValue;
//				System.out.println(awaitedProps);
				throw new Exception(
					MessageFormat.format(EXC_MSG, awaitedProps)
				);
			}
		}
		if (false == compareText(DPQ.getProperty("CEL"), contattiHolderCellField, false)) {
			String defaultValue = "Inserisci il numero di cellulare";
			
			if (false == compareText(defaultValue, contattiHolderCellField, false)) {
				String awaitedProps = DPQ.getProperty("CEL") + " o " + defaultValue;
				throw new Exception(
					MessageFormat.format(EXC_MSG, awaitedProps)
				);
			}
		}
		if (false == compareText(DPQ.getProperty("EMAIL"), contattiHolderEmailField, false)) {
			String defaultValue = "Inserisci l'email";
			
			if (false == compareText("Inserisci l'email", contattiHolderEmailField, false)) {
				String awaitedProps = DPQ.getProperty("EMAIL") + " o " + defaultValue;
//				System.out.println(awaitedProps);
				throw new Exception(
					MessageFormat.format(EXC_MSG, awaitedProps)
				);
			}
		}
		if (false == compareText(DPQ.getProperty("PEC"), contattiHolderPECField, false)) {
			String defaultValue = "Inserisci la PEC";
			
			if (false == compareText(defaultValue, contattiHolderPECField, false)) {
				String awaitedProps = DPQ.getProperty("PEC") + " o " + defaultValue;
//				System.out.println(awaitedProps);
				throw new Exception(
					MessageFormat.format(EXC_MSG, awaitedProps)
				);
			}
		}
	}
	
	public String executeJavascript(String s, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return (String)js.executeScript(s, element);
	}
	
	public String execJsGetText(String xpath) throws Exception {
		//localWaitAndGet(By.xpath(xpath));
		
		return executeJavascript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).innerText", null);
	}
	
	public String execJsGetValue(By xpath) throws Exception {
		return util.jsGetInputValue(xpath);
	}
	
	public void checkCanaleTypeAndText(By by, String text) throws Exception {
		String checkedType = "radio";
		
		WebElement elem = localWaitAndGet(by);
		
		if (false == elem.getAttribute("type").equals(checkedType)) {
			throw new Exception("Il tipo dell'input non corrisponde a quello indicato "+ checkedType);
		}
		
		String xpath = "//*[@id='" + elem.getAttribute("id") + "']/following-sibling::label/span[@class='text-radio']";				
		
		if (false == execJsGetText(xpath).equals(text)) {
			throw new Exception("Il testo dell'input non corrisponde a quello indicato "+ text);
		}
	}
	
	public void checkCanaleContattoPreferitoFieldSet() throws Exception {	
		checkCanaleTypeAndText(By.id("Telefono"), "Telefono");
		checkCanaleTypeAndText(By.id("Cellulare"), "Cellulare");
		checkCanaleTypeAndText(By.id("Email"), "Email");
	}
	
	public void verifyDisabledFields() throws Exception {
		util.verifyVisibility(By.xpath("//*[@id='spinner'])"), 10, true);
		
		WebElement section = util.waitAndGetElement(By.cssSelector("#sc"));
		
		WebElement holderNameField = section.findElement(contattiHolderField);
		WebElement holderCFField = section.findElement(contattiHolderCfField);
		
		if (true == isEnabled(holderNameField)) {
			throw new Exception("Il campo Titolare è editabile (stato atteso: non editabile).");
		}
		if (true == isEnabled(holderCFField)) {
			throw new Exception("Il campo Titolare->Codice Fiscale è editabile (stato atteso: non editabile).");
		}
	}
	
	public void checkReturnAndSaveButtons() throws Exception {
		executeJavascript("window.scrollTo(0, document.body.scrollHeight);", null);
		
		String returnText = "Indietro";
		String saveText = "Salva Modifiche";
		String campoObbligatorio = "*campi obbligatori";
		String returnBtn = execJsGetText(contattiReturnSelector); // .button_first
		String saveBtn = execJsGetText(contattiSaveSelector); // .button_second
		String campoObbligatorioNote = execJsGetText(contattiCampiObbligatoriSelector);
		
		if (false == returnBtn.equalsIgnoreCase(returnText)) {
			throw new Exception("Il pulsante '"+returnText+"' ha un testo diverso o non è presente.");
		}
		if (false == saveBtn.equalsIgnoreCase("Salva Modifiche")) {
			throw new Exception("Il pulsante '"+saveText+"' ha un testo diverso o non è presente.");
		}
		if (false == campoObbligatorioNote.equalsIgnoreCase(campoObbligatorio)) {
			throw new Exception("Il campo '"+campoObbligatorio+"' ha un testo diverso o non è presente.");
		}
	}
	
	public void scrollToElement(WebElement element) {
		executeJavascript("arguments[0].scrollIntoView(true);", element);
	}
	
	public void clickSaveBtn() throws Exception {
		clickComponent(By.xpath(contattiSaveSelector));
	}

	public void eliminaCell() throws Exception {
		String errorMsg = "Il campo Numero di cellulare è obbligatorio.";
		
		WebElement cell = localWaitAndGet(contattiHolderCellField);
		String content = this.execJsGetValue(contattiHolderCellField);
		cell.clear();
		
		scrollToElement(cell);
		clickSaveBtn();
		Thread.sleep(1000);
		if (false == localWaitAndGet(contattiCellFieldError).getText().equals(errorMsg) ) {
			throw new Exception("Il messaggio di errore <"+ errorMsg +"> non è visualizzato corettamente.");
		}else
			cell.sendKeys(content);
	}
	
	public void eliminaEmail() throws Exception {
		String errorMsg = "Il campo Email è obbligatorio.";
		
		WebElement elem = localWaitAndGet(contattiHolderEmailField);
		String content = this.execJsGetValue(contattiHolderCellField);
		elem.clear();
		
		scrollToElement(elem);
		clickSaveBtn();
		
		String errorContent = this.execJsGetText("//*[@id='cntEmailFieldError']");
		
//		if (false == localWaitAndGet(contattiEmailFieldError).getText().equals(errorMsg) ) {
//			throw new Exception("Il messaggio di errore <"+ errorMsg +"> non è visualizzato corettamente.");
//		}else
//			elem.sendKeys(content);
		elem.sendKeys(content);
	}

	public void insertWrongEmail() throws Exception {
		String errorMsg = "L'indirizzo email non è valido. Verifica di averlo scritto in modo corretto.";
		
		WebElement elem = localWaitAndGet(contattiHolderEmailField);
		elem.clear();
		elem.sendKeys("pippo@@");
		
		scrollToElement(elem);
		clickSaveBtn();
		Thread.sleep(5000);
		
		if (false == localWaitAndGet(contattiEmailFieldError).getText().equals(errorMsg) ) {
			throw new Exception("Il messaggio di errore <"+ errorMsg +"> non è visualizzato corettamente.");
		}
	}

	public void insertWrongEmailAndCell() throws Exception {
		/* L'email errato è stato già inserito dal metodo insertWrongEmail */
		
		String errorMsg = "Il campo Numero di cellulare può contenere solo caratteri numerici.";
		
		WebElement elem = localWaitAndGet(contattiHolderCellField);
		WebElement elemErrorMsg = localWaitAndGet(contattiCellFieldError);
		
		elem.clear();
		
		clickSaveBtn();
		
		if (false == elemErrorMsg.getText().equals(errorMsg) ) {
			throw new Exception("Il messaggio di errore atteso <"+ errorMsg +"> non corrisponde a quello ricevuto <"+ elemErrorMsg.getText() +">");
		}
	}

	public void insertWrongPEC() throws Exception {
		String errorMsg = "L'indirizzo email non è valido. Verifica di averlo scritto in modo corretto.";
		
		WebElement elem = localWaitAndGet(contattiHolderPECField);
		WebElement elemErrorMsg = localWaitAndGet(contattiPECFieldError);
		
		elem.clear();
		elem.sendKeys("pippo@@");
		
		clickSaveBtn();
		scrollToElement(elem);
		
		/*if (false == elemErrorMsg.getText().equals(errorMsg) ) {
			throw new Exception("Il messaggio di errore atteso <"+ errorMsg +"> non corrisponde a quello ricevuto <"+ elemErrorMsg.getText() +">");
		}*/
	}

	public void verifyAccountText() throws Exception {
		compareText(DPQ.getProperty("ACCOUNT_TITLE"), datiContattiTitleElem, true);
		compareText(DPQ.getProperty("ACCOUNT_TEXT"), datiContattiTextElem, true);		
	}

	public void verifyPopupText() throws Exception {
		compareText(DPQ.getProperty("POPUP_TITLE"), datiContattiPopupTitle, true);
		compareText(DPQ.getProperty("POPUP_TEXT"), datiContattiPopupText, true);
	}
	
	public boolean jsCompareText(By by, String text, boolean b){
		return false;
	}
	
	public static final String datiEContattiHeader1Text = "Dati e Contatti";
	public static final String datiEContattiHeader2Text = "Consulta e aggiorna i tuoi dati di contatto";
}
