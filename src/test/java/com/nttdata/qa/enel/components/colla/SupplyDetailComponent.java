package com.nttdata.qa.enel.components.colla;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupplyDetailComponent {
	public By RinominaFornitura = By.xpath("//nav[@class='supply-heading-links']/a[1]");
	public By VisualizzaBollette = By.xpath("//nav[@class='supply-heading-links']/a[2]");
	public By Addressfields = By.xpath("//div[@class='heading supply']/dl");
	public By SupplyDetailFields =  By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt");
	public By SupplyDetailFieldsNew =  By.xpath("//div[@class='supply details-container']");
//OLD	public By IconInfoCircle = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[3]/dd/a");
	public By IconInfoCircle = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[3]/dd/a/span");
	public By BolletinoPostal = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[3]/dd/span");
	public By Mostradipiu = By.xpath("//div[@id='mainContentWrapper']/div[@class='dettaglioFornitura-component parbase']/section/div[@class='open-supply']/a");
	public By PopupTitle = By.xpath("//div[@id='modalAlert']/div/h3");
	public By PopupInnerText = By.xpath("//div[@id='modalAlert']/div/p");
	public By ScopriButton = By.xpath("//button[@id='overlayYesButton']/span");
	public By LoggedoutText = By.xpath("//h1[contains(text(),'You have successfully logged out.')]");
	public By CloseButton = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/a[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By AddebitoDirettoTitle = By.xpath("//h2[@id='profile-contact']");
	public By AddebitoDirettoDescription = By.xpath("//div[@class='section-heading']/p");
	//public By EsciButton = By.xpath("//div[@id='landing-dispositiva']/button");
	public By EsciButton = By.xpath("//button[text()='ESCI']");
	public By billTitle = By.xpath("//div[@id='mainContentWrapper']/div/div/h1");
	public By billTitleDescription = By.xpath("//div[@id='mainContentWrapper']/div/div/p[2]");
	public By statoPagamento = By.xpath("//fieldset[@id='stato_pagamento']/legend");
	public By tipoDiDocumento = By.xpath("//fieldset[@id='tipo_documento']/legend");
	public By anno = By.xpath("//fieldset[@id='anno']/legend/span");
	public By fornitura = By.xpath("//fieldset[@id='fornitura']/legend");
	public By servizi = By.xpath("//a[@id='servizi']/span[2]");
	public By gestisciLeOreFree = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div/a[2]");
	public By gestisciLeOreFree8c = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][2]/div[@class='button-container']/a[2]");
	SeleniumUtilities util;
	WebDriver driver;
	
	public SupplyDetailComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 30);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(clickableObject));
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void supplyAddressDetailsDisplay(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
			
		List<WebElement> field = driver.findElements(By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='heading supply']/dl/span[dd]"));
		for(WebElement element : field){
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
				
			if (description.contentEquals("L’indirizzo della tua fornitura gas è: VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT"))
				textfield_found="YES";
			if (description.contentEquals("Numero Cliente: 800739251"))
				textfield_found="YES";
								
			if (textfield_found.contentEquals("NO"))
				throw new Exception("on the 'https://www-colla.enel.it/ Address fields are not present with description 'L’indirizzo della tua fornitura gas', 'VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT','Numero Cliente:','800739251'");
					}
	}
	
	public void supplyDetailsDisplay(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt"));
		
		for(WebElement element : field){
		String description=element.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		
		List<WebElement> value = driver.findElements(By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dd"));
		
		for(WebElement element1 : value){
		String description1=element1.getText();
		description1= description1.replaceAll("(\r\n|\n)", " ");
			
		if (description.contentEquals("Stato Fornitura")&&description1.contentEquals("Attiva"))
			textfield_found="YES";
		
		else if(description.contentEquals("Stato pagamento")&& description1.contentEquals("Da pagare"))
			textfield_found="YES";
		else if (description.contentEquals("Modalità di pagamento")&& description1.contentEquals("Bollettino Postale"))
			textfield_found="YES";
		else if (description.contentEquals("Ricezione bollette")&& description1.contentEquals("Cartacea"))
			textfield_found="YES";
		else if (description.contentEquals("Data di attivazione")&& description1.contentEquals("05/10/2014"))
			textfield_found="YES";
		else if (description.contentEquals("Offerta attiva")&& description1.contentEquals("Semplice Gas"))
			textfield_found="YES";
		else if (description.contentEquals("Numero Offerta")&& description1.contentEquals("2-B820YXA"))
			textfield_found="YES";
		else if (description.contentEquals("PDR")&& description1.contentEquals("00594202699601"))
			textfield_found="YES";
		else if (description.contentEquals("Categoria di consumo")&& description1.contentEquals("Non disponibile"))
			textfield_found="YES";
		else if (description.contentEquals("Contratto")&& description1.contentEquals("PDF non disponibile"))
			textfield_found="YES";
			
			if (textfield_found.contentEquals("NO"))
			throw new Exception("on the 'https://www-colla.enel.it/ Supply fields are not present with description 'Stato Fornitura', 'Stato pagamento','Modalità di pagamento','Ricezione bollette','Data di attivazione','Offerta attiva','Numero Offerta','PDR','Categoria di consumo','Contratto'");
		}
		}
}		

public void VerifyText(By checkObject, String Value) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	
	String textfield_found="NO";
	
	WebElement element = driver.findElement(checkObject);
	
	String actualtext = element.getText();
	
	if (actualtext.equals(Value))
	textfield_found="YES";
					
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected text" + Value + " is not found:");
			}
	
public void serviziPerLeFornitureDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
	String expectedSectionHeading = "Servizi per le forniture";
	String actualSectionHeading = driver.findElement(By.xpath("//section[@id='forniture-id0']/div/h2")).getText();
	
	if (expectedSectionHeading.equals(actualSectionHeading))
	{
				
	List<WebElement> field = driver.findElements(By.xpath("//section[1]/ul/li/a/div[2]/h3"));
	
	for(WebElement element : field){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
	
	if (description.contentEquals("Autolettura"))
			textfield_found="YES";
	
	else if(description.contentEquals("Autolettura"))
		textfield_found="YES";
	else if (description.contentEquals("Modifica Indirizzo Fatturazione"))
		textfield_found="YES";
	else if (description.contentEquals("Modifica Potenza e/o Tensione"))
		textfield_found="YES";
	else if (description.contentEquals("Modifica Contatti e Consensi"))
		textfield_found="YES";
	
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected fields'Autolettura','InfoEnelEnergia','Modifica Indirizzo Fatturazione','Modifica Potenza e/o Tensione','Modifica Contatti e Consensi' are not found:");
		
	   }
	}
}

public void serviziPerLeBolletteDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
	String expectedSectionHeading = "Servizi per le bollette";
	String expectedSectionHeadingDescription = "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette";
	String actualSectionHeading = driver.findElement(By.xpath("//section[@id='forniture-id1']/div/h2")).getText();
	String actualSectionHeadingDescription = driver.findElement(By.xpath("//section[@id='forniture-id1']/div/p[2]")).getText();
	if ((expectedSectionHeading.equals(actualSectionHeading)) && (expectedSectionHeadingDescription.equals(actualSectionHeadingDescription)))
	{
				
	List<WebElement> field = driver.findElements(By.xpath("//section[2]/ul/li/a/div[2]/h3"));
	
	for(WebElement element : field){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
	
	if (description.contentEquals("Bolletta Web"))
			textfield_found="YES";
	
	else if(description.contentEquals("Pagamento Online"))
		textfield_found="YES";
	else if (description.contentEquals("Addebito Diretto"))
		textfield_found="YES";
	else if (description.contentEquals("Rettifica Bolletta"))
		textfield_found="YES";
	else if (description.contentEquals("Rateizzazione"))
		textfield_found="YES";
	else if (description.contentEquals("Bolletta di Sintesi o di Dettaglio"))
		textfield_found="YES";
	else if (description.contentEquals("Duplicato Bolletta"))
		textfield_found="YES";
	
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected fields 'Bolletta Web','Addebito Diretto','Rettifica Bolletta','Rateizzazione','Bolletta di Sintesi o di Dettaglio','Duplicato Bolletta' are not found:");
		
	   }
	}
}

public void serviziPerIlContrattoDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
	String expectedSectionHeading = "Servizi per il contratto";
	String expectedSectionHeadingDescription = "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture";
	String actualSectionHeading = driver.findElement(By.xpath("//section[@id='forniture-id2']/div/h2")).getText();
	String actualSectionHeadingDescription = driver.findElement(By.xpath("//section[@id='forniture-id2']/div/p[2]")).getText();
	if ((expectedSectionHeading.equals(actualSectionHeading)) && (expectedSectionHeadingDescription.equals(actualSectionHeadingDescription)))
	{
				
	List<WebElement> field = driver.findElements(By.xpath("//section[3]/ul/li/a/div[2]/h3"));
	
	for(WebElement element : field){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
	
	if (description.contentEquals("Invio Documenti"))
			textfield_found="YES";
	
	else if(description.contentEquals("Disattivazione Fornitura"))
		textfield_found="YES";
			
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected fields 'Invio Documenti','Disattivazione Fornitura','FIBRA DI MELITA' are not found:");
		
	   }
	}
}


public void lightSupplyAddressDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
	List<WebElement> field = driver.findElements(By.xpath("//div[@id='mainContentWrapper']/div/section/div/dl[@class='supply-details-heading']/span[dd]"));	
		
		for(WebElement element : field){
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
		if (description.contentEquals("L’indirizzo della tua fornitura gas è: VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT"))
			textfield_found="YES";
		else if(description.contentEquals("Numero Cliente: 889918382"))
			textfield_found="YES";
							
		if (textfield_found.contentEquals("NO"))
			throw new Exception("on the 'https://www-colla.enel.it/ Address fields are not present with description 'L’indirizzo della tua fornitura gas', 'VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT','Numero Cliente:','800739251'");
				}
}
public void lightSupplyDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
				
	List<WebElement> field = driver.findElements(By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt"));
	
	for(WebElement element : field){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
	
	List<WebElement> value = driver.findElements(By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dd"));
	
	for(WebElement element1 : value){
	String description1=element1.getText();
	description1= description1.replaceAll("(\r\n|\n)", " ");
		
	if (description.contentEquals("Stato Fornitura")&&description1.contentEquals("Attiva"))
		textfield_found="YES";
	
	else if(description.contentEquals("Stato pagamento")&& description1.contentEquals("Da pagare (7)"))
		textfield_found="YES";
	else if (description.contentEquals("Modalità di pagamento")&& description1.contentEquals("Bollettino Postale"))
		textfield_found="YES";
	else if (description.contentEquals("Ricezione bollette")&& description1.contentEquals("Cartacea"))
		textfield_found="YES";
	else if (description.contentEquals("Data di attivazione")&& description1.contentEquals("05/10/2014"))
		textfield_found="YES";
	else if (description.contentEquals("Offerta attiva")&& description1.contentEquals("Semplice Gas"))
		textfield_found="YES";
	else if (description.contentEquals("Numero Offerta")&& description1.contentEquals("2-B820YXA"))
		textfield_found="YES";
	else if (description.contentEquals("POD")&& description1.contentEquals("IT001E45684149"))
		textfield_found="YES";
	else if (description.contentEquals("Potenza")&& description1.contentEquals("3 kW"))
		textfield_found="YES";
//	else if (description.contentEquals("Tensione")&& description1.contentEquals("220 V"))
//		textfield_found="YES";
//	else if (description.contentEquals("Contratto")&& description1.contentEquals("PDF non disponibile"))
//		textfield_found="YES";
		
		if (textfield_found.contentEquals("NO"))
		throw new Exception("on the 'https://www-colla.enel.it/ Supply fields are not present with description 'Stato Fornitura', 'Stato pagamento','Modalità di pagamento','Ricezione bollette','Data di attivazione','Offerta attiva','Numero Offerta','PDR','Categoria di consumo','Contratto'");
	}
	}
}		

public void lightSupplyAddressDetailsDisplay8c(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
		
	
	List<WebElement> field = driver.findElements(By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='heading supply']/dl/span[dd]"));
	for(WebElement element : field){
		String description=element.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		
		if (description.contentEquals("L’indirizzo della tua fornitura gas è: VIA CHIESA 3 - 24010 BRACCA BG"))
			textfield_found="YES";
		else if(description.contentEquals("Numero Cliente: 855343208"))
			textfield_found="YES";
							
		if (textfield_found.contentEquals("NO"))
			throw new Exception("on the 'https://www-colla.enel.it/ Address fields are not present with description 'L’indirizzo della tua fornitura gas', 'VIA RICASOLI 24 - 51019 PONTE BUGGIANESE PT','Numero Cliente:','800739251'");
				}
}

	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}

public void lightSupplyDetailsDisplay8c(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
				
	List<WebElement> field = driver.findElements(By.xpath("//div[@id='mainContentWrapper']/div[@class='dettaglioFornitura-component parbase']/section/div[@class='supply details-container supply-max-size']/dl/span/dt"));
	
	for(WebElement element : field){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
	
	List<WebElement> value = driver.findElements(By.xpath("//div[@id='mainContentWrapper']/div[@class='dettaglioFornitura-component parbase']/section/div[@class='supply details-container supply-max-size']/dl/span/dd"));
	
	for(WebElement element1 : value){
	String description1=element1.getText();
	description1= description1.replaceAll("(\r\n|\n)", " ");
		
	if (description.contentEquals("Stato Fornitura")&&description1.contentEquals("Sospesa"))
		textfield_found="YES";
	
	else if(description.contentEquals("Stato pagamento")&& description1.contentEquals("Da pagare (9)"))
		textfield_found="YES";
	else if (description.contentEquals("modalità di pagamento")&& description1.contentEquals("bollettino postale"))
		textfield_found="YES";
	else if (description.contentEquals("Ricezione bollette")&& description1.contentEquals("Mail"))
		textfield_found="YES";
	else if (description.contentEquals("Data di attivazione")&& description1.contentEquals("31/10/2013"))
		textfield_found="YES";
	else if (description.contentEquals("Offerta attiva")&& description1.contentEquals("Semplice Gas"))
		textfield_found="YES";
	else if (description.contentEquals("Numero Offerta")&& description1.contentEquals("-"))
		textfield_found="YES";
	else if (description.contentEquals("POD")&& description1.contentEquals("00102400821291"))
		textfield_found="YES";
	else if (!(description.contentEquals("Potenza")))
		textfield_found="YES";
	else if (!(description.contentEquals("Tensione")))
		textfield_found="YES";
	else if (description.contentEquals("Contratto")&& description1.contentEquals("PDF non disponibile"))
		textfield_found="YES";
		
		if (textfield_found.contentEquals("NO"))
		throw new Exception("on the 'https://www-colla.enel.it/ Supply fields are not present with description 'Stato Fornitura', 'Stato pagamento','Modalità di pagamento','Ricezione bollette','Data di attivazione','Offerta attiva','Numero Offerta','PDR','Categoria di consumo','Contratto'");
	}
	}
}		

}

	



