package com.nttdata.qa.enel.components.colla;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class TeleMarketingComponent extends BaseComponent{
	
	public By telemarketingLabel = By.xpath("//div[@class='login-details']/descendant::h1[contains(text(),'Accedi con')]");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By rucuperaPassword = By.xpath("//div[@class='login_recovers']/descendant::a[text()='Recupera Password']");
    public By recuperaUsername = By.xpath("//div[@class='login_recovers']/descendant::a[text()='Recupera Username']");
    public By recuperaHeader = By.xpath("//div[@id='recovery-password']/descendant::h1[contains(text(),'Recupera')]");
    public By recuperaHeader1 = By.xpath("//div[@class='cmPageTitleCont']/following-sibling::div[@class='cmPageSubtitleCont media']//p[contains(text(),'Inserisci')]");
    public By emaiLabel = By.xpath("//input[@id='rec-email']/preceding-sibling::label[contains(text(),'Email')]");
    public By emailInput = By.xpath("//div[@id='formRecoverEmail']/descendant::input[@id='rec-email']");
    public By inviaBtn = By.xpath("//div[@id='formRecoverEmail']/descendant::a[text()='Invia']");
    public By emailInputError = By.xpath("//div[@id='formRecoverEmail']/descendant::span[text()='Campo obbligatorio']");
    public By rucuperaUserNameTitle = By.xpath("//div[@id='recovery-username-section']/descendant::h1[text()='Recupera Username']");
    public By rucuperaUserNameTitle1 = By.xpath("//div[@id='recovery-username-section']/descendant::p[contains(text(),'Inserisci il')]");
    public By rucuperaUsernameLabel = By.xpath("//div[@id='formRecoverUsernameCF']/descendant::label[contains(text(),'Codice Fiscale')]");
    public By rucuperaUsernameInput = By.xpath("//input[@id='rec-username-fiscal-code'];");
    public By rucuperUserNameErrorMsg = By.xpath("//input[@id='rec-username-fiscal-code']/following-sibling::div[@id='errore_fiscal-code']//span[text()='Campo obbligatorio']");
    public By rucuperUserNameButton = By.xpath("//input[@id='rec-username-fiscal-code']/following::a[@id='rec-username-button']");
    public By registerButton = By.xpath("//a[@href='/it/registrazione']");
    public By registerHeader = By.xpath("//div[@class='cmPageTitleCont']/child::div[contains(text(),'SCOPRI IL')]");
    public By registerHeader1 = By.xpath("//div[@class='cmPageTitleCont']/child::h1[contains(text(),'Registrati')]");
    public By problemidiLink = By.xpath("//div[@class='login-block']/ancestor::div[@class='login-details']/descendant::a[contains(text(),'Problemi di')]");
    public By problemHeader = By.xpath("//main[@id='main']/descendant::ul[@class='breadcrumbs component']");
    public By problemHeader1 = By.xpath("//main[@id='main']/descendant::h1[contains(text(),'Non riesco')]");
    public By offerList = By.xpath("//div[@id='enelool']/child::div[@id='tlm_grid']");
    public By loginErrorMsg = By.xpath("//input[@id='txtLoginUsername']/following-sibling::p[text()='Username obbligatoria']");
    public By passwordErrorMsg = By.xpath("//button[@class='view-password show']/following-sibling::p[text()='Password obbligatoria']");
    public By invalidInputErrorMsg = By.xpath("//input[@id='txtLoginUsername']/following-sibling::p[text()='Username non valida']");
    public By invalidPasswordErrorMsg = By.xpath("//main[@id='main']/descendant::p[contains(text(),'Username o password errata')]");
    
    public By caseLuce = By.xpath("//div[@id='tlm_grid']/descendant::p[text()='OFFERTE CASA LUCE']");
    public By caseGas = By.xpath("//div[@id='tlm_grid']/descendant::p[text()='OFFERTE CASA GAS']");
    public By impresaLuce = By.xpath("//div[@id='tlm_grid']/descendant::p[text()='OFFERTE IMPRESA LUCE']");
    public By impresaGas = By.xpath("//div[@id='tlm_grid']/descendant::p[text()='OFFERTE IMPRESA GAS ']");
    public By funzionaita = By.xpath("//div[@id='tlm_grid']/descendant::p[contains(text(),'FUNZIONALIT')]");
    public By TeleHeader = By.xpath("//div[@id='tlm_operator']");
    public By verificaPODTitle = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::h1[text()='Verifica POD 2G']");
    public By podInput = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::input[@id='tagName2G']");
    public By podOkButton = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::button[@id='tag-form-submit-2g']");
    public By podCancelButton = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::button[text()='Cancel']");
    public By podInputError = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::label[@id='tagNameErr2G']");
    public By podCloseButton = By.xpath("//h1[text()='Verifica POD 2G']/preceding-sibling::button[@class='remodal-close']");
    public By aderisciTitle = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/child::h1[text()='Verifica Copertura Fibra']");
    public By aderisciInputComune = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::input[@id='tagComuneFibra']");
    public By aderisciInputIndrizzo = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::input[@id='tagIndirizzoFibra']");
    public By aderisciInputNumero = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::input[@id='tagCivicoFibra']");
    public By aderisciOkButton = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::button[@id='tag-form-submit-Fibra']");
    public By aderisciCancelButton = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::button[text()='Cancel']");
    public By aderisciLabelComune = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::label[@id='labelComuneFibra']");
    public By aderisciLabelIndrizzo = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::label[@id='labelIndirizzoFibra']");
    public By aderisciLabelNumero = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']/descendant::label[@id='labelCivicoFibra']");
    public By adersiciInputError = By.xpath("//label[@id='tagErrFibra']");
    public By adersciClosebutton = By.xpath("//h1[text()='Verifica Copertura Fibra']/preceding-sibling::button[@class='remodal-close']");
    
    public By podHeader = By.xpath("//div[@id='adesione_standard']/child::h1[text()='Scegli Tu Ore Free']");
    public By podContents = By.xpath("//div[@id='adesione_standard']/child::p[@class='image-hero_detail text--detail']");
            
    public By AderisciINBOUND = By.xpath("//p[contains(text(),'3-C0061749')]/parent::div/following-sibling::div[@class='tlm_link']//a[contains(text(),'Aderisci INBOUND')]");
    public By AderisciINBOUND_SpecialeLuce60 = By.xpath("//p[contains(text(),'3-C0059719')]/parent::div/following-sibling::div[@class='tlm_link']//a[contains(text(),'Aderisci INBOUND')]");
    public By AderisciFIBRA = By.xpath("//p[contains(text(),'3-C0060642')]/parent::div/following-sibling::div[@class='tlm_link']//a[contains(text(),'Aderisci con FIBRA')]");
    public By communeDropDownRoma = By.xpath("//ul[@id='ui-id-1']/descendant::a[contains(text(),'Roma, Roma, Lazio')]");
    public By indrizzoDropDownVia = By.xpath("//ul[@id='ui-id-2']/descendant::a[contains(text(),'VIA IGNAZIO VIAN')]");;
    public By numeroDropDownNum = By.xpath("//ul[@id='ui-id-3']/descendant::a[contains(text(),'3')]");
    
	public By copyRight1 = By.xpath("//ul[@class='footer-copyright']/child::li[contains(text(),'Enel Italia')]");
	public By copyRight2 = By.xpath("//ul[@class='footer-copyright']/child::li[contains(text(),'Enel Energia')]");
	public By legaliFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Informazioni Legali']");
	public By creditsFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Credits']");
	public By privacyFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Privacy']");
	public By cookiePolicyFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Cookie Policy']");
	public By remitFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Remit']");
		
	public By luceGasHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Luce e gas']");
	public By impresseHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Imprese']");
	public By InsiemeHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Insieme a te']");
	public By storieHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Storie']");
	public By futureHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Futur-e']");
	public By mediaHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Media']");
	public By supportoHeader = By.xpath("//div[@class='dotcom-header__main']/descendant::a[text()='Supporto']");
	
	public By adesioneStandard = By.xpath("//*[@id='adesione_standard']//h1");
	
	
	public By aderisciInboundEnelOneUltra2G = By.xpath("//p[contains(text(),'ENEL ONE PLUS 2G')]//ancestor::div[2]//a[text()='Aderisci INBOUND']");
		
	WebDriver driver;
	SeleniumUtilities util;
		
		 public TeleMarketingComponent(WebDriver driver) throws Exception {
//				this.driver = driver;
//				util = new SeleniumUtilities(this.driver);
				super(driver);
				
			}
		
			public void launchLink(String url) throws Exception {
				try {
					driver.manage().window().maximize();
					driver.navigate().to(url);
				} catch (Exception e) {
					throw new Exception("Unable to open the link " + url);

				}
			}

//			public void clickComponent(By clickableObject) throws Exception {
//				util.objectManager(clickableObject, util.scrollToVisibility, false);
//				Thread.sleep(1000);
//				util.objectManager(clickableObject, util.scrollAndClick);
//			}
			
//			public void clearText(By object){
//				WebDriverWait WaitVar = new WebDriverWait(driver, 10);
//				WebElement input = driver.findElement(object);
//				input.clear();
//			}
			
			public void verifyFooterVisibility() throws Exception {
				By[] locators = {
						legaliFooter,creditsFooter,privacyFooter,cookiePolicyFooter,remitFooter,
											
				};
				String[] footerValue = {"Informazioni Legali","Credits","Privacy","Cookie Policy","Remit"};
				
				for (int i = 0; i < locators.length; i++) {
					String s = driver.findElement(locators[i]).getText();
					//System.out.println(s);
					verifyComponentVisibility(locators[i]);
					String footer= footerValue[i];
					//System.out.println(footer);
					if(!s.equalsIgnoreCase(footer)){
					throw new Exception("Header values are not matching")	;		
					} 

				}
			}

//			public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//						if (!util.verifyExistence(oggettoEsistente, 30)) {
//					throw new Exception("The Object with xpath " + oggettoEsistente + " doesnot exists.");
//				}
//			}
			
			public void verifyHeaderVisibility() throws Exception {
				By[] locators = {
						luceGasHeader,
						impresseHeader,
						InsiemeHeader,
						storieHeader,
						futureHeader,
						mediaHeader,
						supportoHeader,					
				};
				
				String[] headerValue = {"LUCE E GAS","IMPRESE","INSIEME A TE","STORIE","FUTUR-E","MEDIA","SUPPORTO"};
				
				for (int i = 0; i < locators.length; i++) {
					String s = driver.findElement(locators[i]).getText();
					//System.out.println(s);
					verifyComponentVisibility(locators[i]);
					String Header= headerValue[i];
					//System.out.println(Header);
					if(!s.equalsIgnoreCase(Header)){
					throw new Exception("Header values are not matching")	;		
					} 

				}
			}

//			public void SwitchToWindow() throws Exception
//			{
//				Set<String> newWindows = driver.getWindowHandles();
//				String currentWindow = driver.getWindowHandle();
//				
//				for (String winHandle : newWindows) {
//				/*	System.out.println(winHandle);
//					System.out.println("switched to "+driver.getTitle()+"  Window");
//					System.out.println(driver.getCurrentUrl());
//				*/	
//			       // String pagetitle = driver.getTitle();
//			        driver.switchTo().window(winHandle);
//			   /*     System.out.println(winHandle);
//			        System.out.println("After switch"+ driver.getCurrentUrl());
//			        System.out.println(driver.getTitle());
//				*/    
//				 }
//			}
			public void verifyComponentVisibility(By visibleObject) throws Exception {
				if (!util.verifyVisibility(visibleObject, 15)) {
					throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
				}
			}
			
//			public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
//				textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
//				if (!util.checkElementText(objectWithText, textToCheck)) {
//					WebElement problemElement = driver.findElement(objectWithText);
//					String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
//					if (elementText.length() < 1) {
//						elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
//					}
//					String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
//					//System.out.println(message);
//					throw new Exception(message);
//				}
//			}
			
			public void verifyContent(By object,String text) throws Exception{
				WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
				String value = driver.findElement(object).getText();
				value = value.replaceAll("(\r\n|\n,)","");
				if(!text.contentEquals(value))
					throw new Exception("Value is not matching with expected");
				
			}

//			public void clickComponentIfExist(By existObject) throws Exception {
//				if (util.exists(existObject, 15)) {
//					util.objectManager(existObject, util.scrollToVisibility, false);
//				    util.objectManager(existObject, util.scrollAndClick);
//				}
//			}
			public void waitForElementToDisplay (By checkObject) throws Exception{
					WebDriverWait wait = new WebDriverWait (driver, 30);
					wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
				}
				public void jsClickObject(By by) throws Exception {
					util.jsClickElement(by);
				}
					
			public void enterInput(By insertObject, String valore) throws Exception {
				util.objectManager(insertObject, util.sendKeys, valore);
			}
			
			public void verifyText(By checkObject, String Value) throws Exception {
				
				WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				String textfield_found="NO";
				WebElement element = driver.findElement(checkObject);
				String actualtext = element.getText();
				System.out.println(actualtext);
				if (actualtext.contentEquals(Value))
				textfield_found="YES";
								
				if (textfield_found.contentEquals("NO"))
					throw new Exception("The expected text" + Value + " is not found:");
			}
			
			public void hanldeFullscreenMessage(By by) throws Exception{
				if(util.verifyExistence(by, 60)){
					util.objectManager(by, util.scrollToVisibility, false);
					util.objectManager(by, util.scrollAndClick);
				}
			}
			
//			public void backBrowser(By checkObject) throws Exception {
//			    try {
//			    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
//			    	driver.navigate().back();
//			    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
//			                
//			    } catch (Exception e) {
//			        throw new Exception("Previous Page not displayed");
//			    }
//			}
			public void comprareText(By by, String text, boolean nestedTags) throws Exception{
				WebElement we = util.waitAndGetElement(by);
				String weText = we.getAttribute("innerHTML");
				if(nestedTags)
					weText = normalizeInnerHTML(weText);
				else
					weText = we.getText();
				System.out.println("Normalized:\n"+weText);
				System.out.println(text);
				if(!text.equals(weText))
					throw new Exception("Text mismatch.The passed text is not equal to the one within bolletteHeaderText item.");
			}
			
			public String normalizeInnerHTML(String s){
				int less = s.indexOf("<"); int greater = s.indexOf(">");
				if(less==-1)
					return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
				else{
					String tag = s.substring(less, greater+1);
					String endTag = null;
					if(!tag.contains("<br>"))
						endTag = tag.replace("<", "</");
					if(endTag!=null)
						return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
					else
						return normalizeInnerHTML(s.replace(tag, ""));
				}
				
				
			}
			
		    public void compareText(By by, String text, boolean nestedTags, WebDriver driver) throws Exception{
				WebElement we = util.waitAndGetElement(by);
				String weText = we.getAttribute("innerHTML");
				if(nestedTags)
					weText = normalizeInnerHTML(weText);
				else
					weText = we.getText();
				System.out.println("Normalized:\n"+weText);
				System.out.println(text);
				if(!text.equals(weText))
					throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
			}
		
//			public void checkUrl(String url) throws Exception{
//				WebDriverWait WaitVar = new WebDriverWait(driver, 10);
//				String link=driver.getCurrentUrl();
//				if (url.contains("https://"))
//					url = url.substring(8);
//				if (!link.contains(url))
//					 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
//			}
			
			public void pressJavascript(By clickObject) throws Exception {
				WebElement element = util.waitAndGetElement(clickObject);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}
			
			public void getContent(By by,boolean nestedTags) throws Exception{
				WebElement we = util.waitAndGetElement(by);
				String weText = we.getAttribute("innerHTML");
				if(nestedTags)
					weText = normalizeInnerHTML(weText);
				else
					weText = we.getText();
				System.out.println("Normalized:\n"+weText);
						
			}

			public static final String RUCUPERAUSERNAME_TITLE="Recupera Username";
			public static final String RUCUPERAUSERNAME_TITLE1 = "Inserisci il tuo codice fiscale per poter recuperare il tuo username";
			public static final String LOGIN_ERRORMSG = "Username obbligatoria";
			public static final String PASSWORD_ERRORMSG = "Password obbligatoria";
			public static final String INVALID_ERROR_MSG = "Username non valida";
			public static final String POD_CONTENTS = "Ricorda che per completare l'adesione devi avere a portata di mano:"
													+" Codice fiscale / POD / Iban"
													+" se devi fare un Subentro, una Prima attivazione o una Voltura servono anche:"
													+" dati dell'immobile / Carta d'identità"
													+" L'offerta che hai scelto è valida solo per forniture ad uso abitativo.";
				
			public static final String INVALID_PASSWORD_MSG = "Username o password errata. Ricordati di inserire il prefisso se accedi con il numero di cellulare oppure, se non l'hai già fatto, di accedere con l'e-mail e completare il profilo inserendo il cellulare";
			public static final String TELE_HEADER = "Benvenuto,stefano lombardi";
		    public static final String POD_HEADER = "Scegli Tu Ore Free";
		    public static final String POD_TITLE = "Verifica POD 2G";
		    public static final String ADERISCI_TITLE ="Verifica Copertura Fibra";
		    public static final String COMUNE_LABEL = "Comune";
		    public static final String INDRIZZO_LABEL = "Indirizzo";	
		    public static final String NUMERCO_LABEL = "Numero Civico";
		    public static final String POD_INPUT_ERROR = "Il pod non possiede i requisiti per poter aderire all'offerta";
		    public static final String ADERISCI_ERROR_MSG = "L'indirizzo non è coperto dalla fibra";
		    public static final String PROBLEM_HEADER = "Home/supporto/Non riesco ad accedere all'Area Clienti Enel Energia";
		    public static final String PROBLEM_HEADER1 = "Non riesco ad accedere all'Area Clienti Enel Energia";
		    public static final String RUCUPERAUSERNAME_LABEL= "Codice Fiscale";
		    public static final String REGISTER_HEADER = "SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE";
		    public static final String REGISTER_HEADER1 = "Registrati nell'area riservata";
		    
		    public String problemHeaderText = "Home/supporto/Non riesco ad accedere all'Area Clienti Enel Energia";
		    public String problemHeaderText1 = "Non riesco ad accedere all'Area Clienti Enel Energia";


}			
