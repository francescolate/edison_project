package com.nttdata.qa.enel.components.colla;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ImpressComponent {
	public By logoEnel = By.xpath("//img[contains(@class,'logoimg')]");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By buttonAccept = By.xpath("//button[@id='accept-btn']");
	public By homeFullscreenAlertCloseButton = By.xpath("//div[@id='fsa-close-button']");
	public By buttonAcceptCookie= By.xpath("//button[@id='truste-consent-button']");
	
	public By headerImpress = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Imprese']");
	public By impressHome = By.xpath("//div[@class='image-hero_content-wrapper']/child::*/ul[@class='breadcrumbs component']");
	public By impressHeader = By.xpath("//div[@class='image-hero_content-wrapper']/child::h1[contains(text(),'Vicini alle piccole')]");
	public By impressHeader1 = By.xpath("//div[@class='image-hero_content-wrapper']/child::p[@class='image-hero_detail text--detail']");
	public By grandiAzendi = By.xpath("//div[@class='tile_inner ']/ancestor::div[@class='link-content parbase']/following::h3[text()='Grandi aziende']");
	public By grandiHome = By.xpath("//nav[@class='image-hero_breadcrumbs module--initialized']/child::ul[@class='breadcrumbs component']");
	public By grandiHeader1 = By.xpath("//div[@class='image-hero_content-wrapper text-center']/child::h1[text()='Grandi aziende']");
	public By grandiHeader2 = By.xpath("//div[@class='image-hero_content-wrapper text-center']/following-sibling::p[@class='image-hero_detail text--detail']");
	
	public By grandiContent1 = By.xpath("//div[@class='text parbase']/descendant::section[@class='anchor home-plan_container article-container module--initialized']/p[2]");
	//public By grandiContent2= By.xpath("//div[@class='text parbase']/descendant::section[@class='anchor home-plan_container article-container module--initialized']/p[3];");
	public By grandiContent2 = By.xpath("//*[@id='main']/div/div[1]/section/p[3]");
	public By grandiContent3 = By.xpath("//div[@class='text parbase']/descendant::section[@class='anchor home-plan_container article-container module--initialized']/ul");
	
	public By grandiContent4 = By.xpath("//div[@class='container']/child::h2[contains(text(),'Trova il')]");
	public By cerca_btn= By.xpath("//div[@class='btn-section']/ancestor::div[@class='container']/descendant::a[text()='cerca']");
	public By errorMsgNoInput = By.xpath("//div[@class='btn-section']/parent::div[@class='input-holder']/following-sibling::div[text()='Campo Obbligatorio']");
	public By inputValue = By.xpath("//div[@class='btn-section']/parent::div[@class='input-holder']/child::input[@id='search-advisor']");
	public By errorMsgInvalid = By.xpath("//div[@class='btn-section']/parent::div[@class='input-holder']/following-sibling::div[contains(text(),'Si prega di inserire')]");
	
	public By msgFederica = By.xpath("//div[@class='input-results details-block-open']/descendant::span[text()='FEDERICA FUSCHI']");
	public By msgEmail = By.xpath("//div[@class='input-results details-block-open']/descendant::a[contains(text(),'enel.com')]");
		
	//51
	public By disservizioContent1 = By.xpath("//div[@class='nav-login-maintenance-wrapper']/ancestor::div[@class='disservizio-programmato parbase']//span/../p");
	public By disservizioContent2= By.xpath("//div[@class='nav-login-maintenance-wrapper']/ancestor::div[@class='disservizio-programmato parbase']/descendant::div[@class='maintenance-desc-footer']");
	public By disservizioHome = By.xpath("//div[@class='image-hero_content-wrapper text-center']/descendant::ul[@class='breadcrumbs component']");
	public static final String DISSERVIZIO_HOME = "Home / Disservizio Programmato";
	public static final String DISSERVIZIO_HOME1 = "HOME / DISSERVIZIO PROGRAMMATO";
	public static final String DISSERVIZIO_VALUE = "Per migliorare la qualita' del servizio i nostri sistemi MyEnel sono attualmente in manuntenzione.";
	public static final String DISSERVIZIO_VALUE1 = "Ci scusiamo per il disagio.";
	
	
	
	public By copyRight1 = By.xpath("//ul[@class='footer-copyright']/child::li[contains(text(),'Enel Italia')]");
	public By copyRight2 = By.xpath("//ul[@class='footer-copyright']/child::li[contains(text(),'Enel Energia')]");
	public By legaliFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Informazioni Legali']");
	public By legalDisclaimer= By.xpath("//li[@class='footer-legal-item']/child::a[text()='Legal disclaimers']");
	public By creditsFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Credits']");
	public By privacyFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Privacy']");
	public By cookiePolicyFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Cookie Policy']");
	public By remitFooter = By.xpath("//li[@class='footer-legal-item']/child::a[text()='Remit']");
	
	public By luceGasHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Luce e gas']");
	public By impresseHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Imprese']");
	public By InsiemeHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Insieme a te']");
	public By storieHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Storie']");
	public By futureHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Futur-e']");
	public By mediaHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Media']");
	public By supportoHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[text()='Supporto']");
	
	public By powerAndGasHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'Power and gas')]");
	public By businessHeader= By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'Business')]");	
	public By enelPremiaHeader = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'ENELPREMIA WOW!')]");	
	public By storiesHeader= By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'Stories')]");
	public By future1Header = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'Futur-e')]");
	public By media1Header = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'Media')]");
	public By support1Header = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/descendant::a[contains(text(),'Support')]");
	
	public By HomeLink = By.xpath("//ul[@class='breadcrumbs component']/descendant::a[contains(text(),'Home')]");
	public By HomePageQues1 = By.xpath("//div[@class='select-menu-section']/child::label[contains(text(),'What kind of contract?')]");
	public By homePageQues2 = By.xpath("//div[@class='select-menu-section select-secondary']/child::label[contains(text(),'Where?')]");
	public By homePageQues3 = By.xpath("//div[@class='select-menu-section']/child::label[contains(text(),'What for?')]");
	
	//publicco69
	public By reclaimiPath = By.xpath("//body/main[@id='main']/descendant::ul[@class='breadcrumbs component']");
	public final static String RECLAIMI_PATH = "Home / form-reclami";
	public By ModuloHeader = By.xpath("//h1[contains(text(),'Modulo Reclami')]");
	public final static String MODULO_HEADER = "Modulo Reclami";
	public By tipologiaHeader = By.xpath("//h3[contains(text(),' Tipologia del Reclamo ')]");
	
	public By tipolgiaLabel = By.xpath("//label[contains(text(),'Tipologia reclamo')]");
	public By tipolgiaInput = By.xpath("//div[@id='form_select_tipo_reclamo']/child::div[@class='select-styled']");
	
	public By InserisciHeader = By.xpath("//h3[contains(text(),'Inserisci i tuoi dati ')]");
	
	public By NomeLabel = By.xpath("//label[contains(text(),'Nome')]");
	public By CognomeLabel = By.xpath("//label[contains(text(),'Cognome')]");
	
	public By NomeInput = By.xpath("//input[@id='form_nome']");
	public By CognomeInput = By.xpath("//input[@id='form_cognome']");
	
	public By TelefonoLabel = By.xpath("//label[contains(text(),'Telefono cellulare')]");
	public By TelefonoInput = By.xpath("//input[@id='form_numero_cellulare']");
	
	public By fasceLabel = By.xpath("//label[contains(text(),'Fasce orarie di reperibili')]");
	public By fasceInput = By.xpath("//div[@class='select-styled custom-select' and contains(text(),'Seleziona orario')]");
	
	public By codiceFiscaleLabel = By.xpath("//label[contains(text(),'Codice Fiscale/Partita Iva')]");
	public By codiceFiscaleInput= By.xpath("//input[@id='form_codice_fiscale']");
	
	public By indrizzoInput = By.xpath("//input[@id='form_indirizzo']");
	public By indrizzoLabel = By.xpath("//label[contains(text(),'Indirizzo della fornitura')]");
	
	public By emailLabel = By.xpath("//label[contains(text(),'Email')]");
	public By emailInput = By.xpath("//input[@id='form_mail']");
	
	public By confermaEmailLabel = By.xpath("//label[contains(text(),'Conferma indirizzo Email')]");
	public By confermaEmailInput = By.xpath("//input[@id='form_conferma_mail']");
	
	public By numeroClienteLabel = By.xpath("//label[contains(text(),'Numero Cliente')]");
	public By numeroClienteInput = By.xpath("//input[@id='form_eneltel']");
	
	public By codiceLabel = By.xpath("//label[contains(text(),'Codice POD/PDR')]");
	public By codiceInput = By.xpath("//input[@id='form_pod']");
	
	public By argomentoHeader = By.xpath("//h3[contains(text(),' Argomento e descrizione del reclamo ')]");
	
	public By argomentoLabel = By.xpath("//label[contains(text(),'Argomento')]");
	public By argomentoInput = By.xpath("//div[@class='select-styled']");
	
	public By discrizioneLabel = By.xpath("//label[contains(text(),'Descrizione richiesta')]");
	public By discrizioneInput = By.xpath("//textarea[@id='descrizione_reclamo']");
	
	public By autoletturaLabel = By.xpath("//label[contains(text(),'Autolettura del contatore')]");
	public By autoletturaInput = By.xpath("//input[@id='form_autolettura']");
	
	public By datadelLabel = By.xpath("//label[contains(text(),'Data del')]");
	public By datadelInput = By.xpath("//input[@id='form_data']");
	
	public By informativaContent = By.xpath("//div[@class='privacy_box']/child::p[contains(text(),'Il trattamento dei dati')]");
	
	public By informativaCheckboxContent = By.xpath("//span[@class='privacy_text']");
	
	public By informativaConent2 = By.xpath("//p[contains(text(),'I nostri consulenti sono')]");
	
	public By mandatoryfield = By.xpath("//span[contains(text(),'campi obbligatori')]");
	
	
	
	
	WebDriver driver;
	SeleniumUtilities util;
	 public ImpressComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	 

		public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}
	 public void verifyHeaderValue() throws Exception {
			By[] locators = {
					powerAndGasHeader,
					businessHeader,
					enelPremiaHeader,
					storiesHeader,
					future1Header,
					media1Header,
					support1Header
			};
			
			String[] headerValue = {"POWER AND GAS","BUSINESS","ENELPREMIA WOW!","STORIES","FUTURE-E","MEDIA","SUPPORT"};
			
			for (int i = 0; i < locators.length; i++) {
				String s = driver.findElement(locators[i]).getText();
				System.out.println(s);
				verifyComponentVisibility(locators[i]);
				String Header= headerValue[i];
				//System.out.println(Header);
				if(!s.equalsIgnoreCase(Header)){
				throw new Exception("Header values are not matching");		
				} 
			  }
		   }
		public void verifyFooterValue() throws Exception {
			By[] locators = {
					legalDisclaimer,creditsFooter,privacyFooter,cookiePolicyFooter,remitFooter,
										
			};
			String[] footerValue = {"Legal diclaimers","Credits","Privacy","Cookie Policy","Remit"};
			
			for (int i = 0; i < locators.length; i++) {
				String s = driver.findElement(locators[i]).getText();
				System.out.println(s);
				verifyComponentVisibility(locators[i]);
				String footer= footerValue[i];
				//System.out.println(footer);
				if(!s.equalsIgnoreCase(footer)){
				throw new Exception("Header values are not matching")	;		
				} 

			}
		}
		public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
		
		public void clearText(By object){
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WebElement input = driver.findElement(object);
			input.clear();
		}
		
		public void verifyFooterVisibility() throws Exception {
			By[] locators = {
					legaliFooter,creditsFooter,privacyFooter,cookiePolicyFooter,remitFooter,
										
			};
			String[] footerValue = {"Informazioni Legali","Credits","Privacy","Cookie Policy","Remit"};
			
			for (int i = 0; i < locators.length; i++) {
				String s = driver.findElement(locators[i]).getText();
				//System.out.println(s);
				verifyComponentVisibility(locators[i]);
				String footer= footerValue[i];
				//System.out.println(footer);
				if(!s.equalsIgnoreCase(footer)){
				throw new Exception("Header values are not matching")	;		
				} 

			}
		}

		public void verifyComponentExistence(By oggettoEsistente) throws Exception {
					if (!util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
			
		}
		public void verifyHeaderVisibility() throws Exception {
			By[] locators = {
					luceGasHeader,
					impresseHeader,
					//InsiemeHeader,
					storieHeader,
					futureHeader,
					mediaHeader,
					supportoHeader,					
			};
			
			String[] headerValue = {"LUCE E GAS","IMPRESE","STORIE","FUTUR-E","MEDIA","SUPPORTO"};
			
			for (int i = 0; i < locators.length; i++) {
				String s = driver.findElement(locators[i]).getText();
				//System.out.println(s);
				verifyComponentVisibility(locators[i]);
				String Header= headerValue[i];
				//System.out.println(Header);
				if(!s.equalsIgnoreCase(Header)){
				throw new Exception("Header values are not matching");		
				} 

			}
		}

		public void SwitchToWindow() throws Exception
		{
			Set<String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
			/*	System.out.println(winHandle);
				System.out.println("switched to "+driver.getTitle()+"  Window");
				System.out.println(driver.getCurrentUrl());
			*/	
		       // String pagetitle = driver.getTitle();
		        		driver.switchTo().window(winHandle);
		   /*     System.out.println(winHandle);
		        System.out.println("After switch"+ driver.getCurrentUrl());
		        System.out.println(driver.getTitle());
			*/    
			 }
		}
		public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}
		
		public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
			textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (!util.checkElementText(objectWithText, textToCheck)) {
				WebElement problemElement = driver.findElement(objectWithText);
				String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
				if (elementText.length() < 1) {
					elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
				}
				String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
				//System.out.println(message);
				throw new Exception(message);
			}
		}
		public void verifyContent(By object,String text) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
			String value = driver.findElement(object).getText();
			value = value.replaceAll("(\r\n|\n,)","");
			if(!text.contentEquals(value))
				throw new Exception("Value is not matching with expected");
			
		}

			public void clickComponentIfExist(By existObject) throws Exception {
			if (util.exists(existObject, 15)) {
				util.objectManager(existObject, util.scrollToVisibility, false);
			    util.objectManager(existObject, util.scrollAndClick);
			}
		}
			public void waitForElementToDisplay (By checkObject) throws Exception{
				WebDriverWait wait = new WebDriverWait (driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				
			}
			public void jsClickObject(By by) throws Exception {
				util.jsClickElement(by);
			}
				
		public void enterInput(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void verifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			String textfield_found="NO";
			WebElement element = driver.findElement(checkObject);
			String actualtext = element.getText();
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
		}
		
		public void hanldeFullscreenMessage(By by) throws Exception{
			if(util.verifyExistence(by, 60)){
				util.objectManager(by, util.scrollToVisibility, false);
				util.objectManager(by, util.scrollAndClick);
			}
		}
		
		public void backBrowser(By checkObject) throws Exception {
		    try {
		    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    	driver.navigate().back();
		    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		                
		    } catch (Exception e) {
		        throw new Exception("Previous Page not displayed");
		    }
		}
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
			
			
		}
			public void checkUrl(String url) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			String link=driver.getCurrentUrl();
			
			if (url.contains("https://"))
			{
				url = url.substring(8);
			}
			if (!link.contains(url))
				 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
			}
		public void pressJavascript(By clickObject) throws Exception {
			WebElement element = util.waitAndGetElement(clickObject);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
		
		public static final String GRANDI_CONTENT1 = "Enel adatta i propri servizi alle specifiche esigenze di una grande azienda, con una" +
													" spesa per la fornitura di energia elettrica superiore a 100.000 euro e superiore" +
													" a 50.000 euro per il gas, a partire Gentile cliente, dalle ore 22:00 di mercoledì 24 Febbraio alle ore 07:00 di giovedì 25 Febbraio i nostri servizi saranno in manutenzione per migliorarne ulteriormente la qualità. Potrai accedere nuovamente a manutenzione terminata.<br/><br/>Per urgenze o ulteriori informazioni restiamo a tua disposizione ai nostri canali di contatto ufficiali.<br/>Ci scusiamo per il disagio.dal profilo di consumo, dal settore di" +
													" attività e dal tipo di fornitura richiesta."+
													"Se sei responsabile della gestione energetica della tua azienda, contatta un nostro consulente.";
		public static final String GRANDI_CONTENT2 = "Vuoi un aiuto per scegliere la soluzione migliore per il tuo business?Enel mette a disposizione per i suoi clienti una rete di 130 consulenti esperti su tutto il territorio nazionale.Il tuo consulente sarà il punto di riferimento per ogni esigenza:";
		public static final String GRANDI_CONTENT3 = "analizzare insieme a te i bisogni della tua azienda;individuare soluzioni personalizzate trovando l’offerta più vantaggiosa per la tua azienda;supportarti nella gestione delle tue forniture.";
		
		public static final String DISERVIZIO_CONTENT1 = "Gentile cliente, dalle ore 22:00 di mercoledì 24 Febbraio alle ore 07:00 di giovedì 25 Febbraio i nostri servizi saranno in manutenzione per migliorarne ulteriormente la qualità. Potrai accedere nuovamente a manutenzione terminata.&lt;br/&gt;&lt;br/&gt;Per urgenze o ulteriori informazioni restiamo a tua disposizione ai nostri canali di contatto ufficiali.&lt;br/&gt;Ci scusiamo per il disagio.";
		public static final String DISERVIZIO_CONTENT2 = "Ci scusiamo per il disagio.";
		public static final String IMPRESS_HOME = "Home / Imprese";
		public static final String IMPRESS_HEADER_old = "L'energia non è mai stata così semplice";
		public static final String IMPRESS_HEADER = "Vicini alle piccole imprese con un mese gratis di Open Energy";
		public static final String IMPRESS_HEADER1 = "Scopri Open Energy, l'abbonamento dedicato ai Professionisti e le PIVA per pagare la componente energia come la paghiamo noi di Enel.";
		public static final String GRANDI_HOME = "Home/Imprese/Grandi aziende";
		public static final String GRANDI_HEADER1 = "Grandi aziende";
		public static final String GRANDI_HEADER2 = "Le offerte su misura per il tuo business";
		public static final String GRANDI_HEADER4 = "Trova il consulente Enel più vicino a te";
		public static final String ERRORMSG_INVALIDINPUT = "Si prega di inserire correttamente il CAP specifico del Comune di riferimento.";
		public static final String MSG_FEDRICA = "FEDERICA FUSCHI";
		public static final String MSG_EMAIL = "federica.fuschi@enel.com";	
		public static final String HOME_QUES1 = "What kind of contract?";
		public static final String HOME_QUES2 = "Where?";
		public static final String HOME_QUES3 = "What for?";
		
		
}
