package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class NovitaComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	public By pageTitle = By.xpath("//h2[text()='Novità']");
	public By titleSubText = By.xpath("//div[@class='section-heading']//p");
	public By comeFunzionaBanner = By.xpath("//li[@id='enelpremiaPromo']//span[2]");
	public By scopriDiPiuComeFunziona = By.xpath("//li[@id='enelpremiaPromo']//span[2]//a");
	public By scopriDiPiuSuBanner = By.xpath("//li[@class='flex tab promoModel appPromo_preposition_generic_ ']//span[2]");
	public String comeFunzionaBannerUrl = "https://www-coll1.enel.it/it/luce-e-gas/enelpremia/come-funziona";
	public By comeFunzionaBannerPath = By.xpath("//nav[@data-module='navigation-breadcrumbs']//ul");
	public By comeFunzionaBannerTitle = By.xpath("//h1[contains(text(),'Come funziona')]");
	public By primaGasBanner = By.xpath("//li[@class='flex tab promoModel appPromo_preposition_generic__292416302 ']//span[2]");
	public By scaricaIappDiBanner = By.xpath("//li[@class='flex tab promoModel appPromo_preposition_generic__339373176 ']//span[2]");
	public By conIappufirstBanner = By.xpath("//li[@class='flex tab promoModel appPromo_preposition_generic__1896287501 ']//span[2]");
	public String scopriDiPiuSuBannerUrl = "https://www.enel.it/it/contatti/ac-blackfriday"; 
	public By scaricaIappDiBannerPath = By.xpath("//nav[@data-module='navigation-breadcrumbs']");
	public String scaricaIappDiBannerUrl = "https://www-coll1.enel.it/it/supporto/faq/app-enel";
	public By scaricaIappDiBannerTitle = By.xpath("//h1[contains(text(),'A cosa serve')]");
	public By scaricaIappDiBannerTitleSubText = By.xpath("//p[contains(text(),'Gestisci la')]");
	public String conIappufirstBannerUrl = "https://www.ufirst.com/country-selection";
	public By visualizzaItuoiCouponBanner = By.xpath("//span[@class='couponPromo image-coupon image-coupon_preposition_enelprem ']/following-sibling::span");
	public By visualizzaItuoiCouponBannerLink = By.xpath("//span[@class='couponPromo image-coupon image-coupon_preposition_enelprem ']/following-sibling::span//a");
	public By visualizzaItuoiCouponBannerPath = By.xpath("//nav[@role='navigation']");
	public By visualizzaItuoiCouponBannerTitle = By.xpath("//h1[contains(text(),'I miei Coupon')]");
	public String visualizzaItuoiCouponBannerUrl = "https://enel-stage.serijakala.it/web/wallet/openClientWallet?skip";
	
	public NovitaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{		
        if(!url.equals(driver.getCurrentUrl()))
            throw new Exception("Redirection failed! The current URL is different from the one saved before.");
    }
	
	public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	    
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}

	
	public static final String TitleSubText = "In questa sezione potrai visualizzare tutte le offerte e i vantaggi offerti da Enel Energia";
	public static final String ComeFunzionaBanner = "Come funziona Scopri come funziona enelpremia WOW! Scopri di più";
	public static final String ComeFunzionaBannerPath = "Home/Luce e Gas/Come funziona enelpremia WOW!";
	public static final String ScopriDiPiuSuBanner = "È arrivato il Black Friday Con Luce 30 e Gas 30 ottieni lo sconto del 30% sul prezzo della componente energia e della materia prima gas per un anno! Scopri di più";
	public static final String PrimaGasBanner = "30% di sconto sulla materia prima gas Aggiungi alle tue vacanze i vantaggi dell’offerta gas per la tua casa, hai tempo fino al 25 luglio 2019. Scopri di più";
	public static final String ScaricaIappDiBanner = "Scarica l’app di Enel Energia Verifica lo stato della tua fornitura direttamente dal tuo smartphone. Scopri di più";
	public static final String ConIappufirstBanner = "Con l’app ufirst eviti la coda! Con l’app ufirst eviti la coda! Scopri di più";
	public static final String ScaricaIappDiBannerTitle = "A cosa serve la App Enel Energia";
	public static final String ScaricaIappDiBannerTitleSubText = "Gestisci la tua fornitura dallo smartphone";
	public static final String ScaricaIappDiBannerPath = "Home/supporto/A cosa serve la App Enel Energia";
	public static final String VisualizzaItuoiCouponBanner = "Visualizza i tuoi coupon Accedi alla sezione enelpremia WOW! Ogni settimana tanti sconti e regali speciali. Scopri di più";
	public static final String VisualizzaItuoiCouponBannerPath = "HOME ENELPREMIA WOW! I MIEI COUPON";
}
