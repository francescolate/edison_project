package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ReportBillingComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By path = By.xpath("//section[@class='content']//ol");
	public By pageTitle = By.xpath("//section[@class='content']//h1[text()='Report Billing Management']");
	public By titleSubText = By.xpath("//section[@class='content']//h1[text()='Report Billing Management']/following-sibling::p");
	public By checose = By.xpath("//h2[contains(text(),'Che cos')]");
	public By pageContent = By.xpath("//div[@class='row d-flex']");
	public By fineBtn = By.xpath("//section[@class='content']//a[@name='Fine']");
	public By iscrivitiBtn = By.xpath("//a[@id='iscriviti']");
	
	public ReportBillingComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		/*System.out.println("Normalized:\n"+weText);
		System.out.println(text);*/
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	public static final String Path = "Area riservataReport Billing Management";
	public static final String TitleSubText = "L'operazione è andata a buon fine.";
	public static final String PageContent = "È un servizio online per le Imprese che consente di controllare in ogni momento le proprie fornituredi energia elettrica e gas, conoscerne i consumi e avere una visione integrata degli andamentiproduttivi.Consultando il reporting billing management puoi: • accedere alle principali informazioni relative alle tue forniture • consultare on-line l'archivio delle tue fatture • analizzare i tuoi consumi • esportare i dati dei tuoi consumi in formato excel per successive rielaborazioni • esportare i dati del tuo fatturato (elenco fatture o fatturato per competenza) in formatoexcel per successive rielaborazioni";
	public static final String Checose = "Che cos'è?";
}
