package com.nttdata.qa.enel.components.colla;

import java.io.File;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BillsComponent {
	WebDriver driver;
	SeleniumUtilities util;
	static WebDriver driver1;

	public By bolletteHeader = By.xpath("//h1[text()='Le tue bollette']");
	public By bolletteHeaderText = By.xpath("//div[@class='heading']//p[contains(text(), 'Qui potrai')]");
	public By yourFilter = By.xpath("//h3/button[@id='filter-button-mostra']");
	public By scaricaPDF = By.xpath("//li[1]/dl/ul[@class='azioni-pagamento show-azioni']/li[3]/a");
	public By payOnline = By.xpath("//div[@class='button-container']/a/button[@class='button_second']");
	public By redBoxText = By.xpath("//div[@class='bollette-component parbase']/div[@id='box_pagaOnLine']/div[@class='dsc-blue-alert']/p");
	public By paymentStatus = By.xpath("//fieldset[@id='stato_pagamento']//input[@checked='checked']");
	public By typeOfDoc = By.xpath("//fieldset[@id='tipo_documento']//input[@checked='checked']");
	public By year = By.xpath("//fieldset[@id='anno']//input[@checked='checked']");
	public By supply = By.xpath("//fieldset[@id='fornitura']//input[@checked='checked']");
	public By rataFilter = By.xpath("//fieldset[@id='tipo_documento']/div[@class='check-container'][2]/label");
	public By bolletaFilter = By.xpath("//fieldset[@id='tipo_documento']/div[@class='check-container'][1]/label");
	public By applicaFiltri = By.xpath("//button[@class='button button-first button-center']");
	public By more = By.xpath("//div[@id='bill-list-panel-0']//ul[@id='fornitura_0']//li[1]//dl//button[@class='btnAppearanceRemover open-menu-azioni bolletteModel']");
	public By pagaOnline = By.xpath("//li[1]/dl/ul[@class='azioni-pagamento show-azioni']/li[1]/a");
	public By dettaglioRata = By.xpath("//li[1]/dl/ul[@class='azioni-pagamento show-azioni']/li[2]/a");
	public By dettaglioPiano = By.xpath("//li[1]/dl/ul[@class='azioni-pagamento show-azioni']/li[3]/a");
	public By scaricaPDFPiano = By.xpath("//li[1]/dl/ul[@class='azioni-pagamento show-azioni']/li[4]/a");

	
	public BillsComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(5000);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	 public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
	
	public void verifyFilterValues(By checkObject,String property) throws Exception{
				
		List<WebElement> web =   driver.findElements(checkObject);
		
		ArrayList<String> list = new ArrayList<String>();
		for(int i=0;i<web.size();i++){
			
			String checkId = web.get(i).getAttribute("id");
			Thread.sleep(100);
			String filterValue = driver.findElement(By.xpath("//label[@for='"+checkId+"']")).getText();
			list.add(filterValue);
		}	
		boolean found = false;
			for(int j=0;j<list.size();j++){
			
				if(property.contains(list.get(j)))
				{ found = true;}
			}
		
			if (!found)
			throw new Exception(" Filter value is not matching with the expected value "+property);

		
	}

	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	public void checkForText(By checkObject, String property) throws Exception {
		
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equals(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);
		
	}
	
	public static void HoverAndClick(WebElement elementToHover,WebElement elementToClick) {
		Actions action = new Actions(driver1);
		action.moveToElement(elementToHover).click(elementToClick).build().perform();
	}
	
	/*public void verifyPDFDownload(String downloadPath,String property) throws Exception{
		Thread.sleep(10000);
		 File dir = new File(downloadPath);
		 System.out.println(downloadPath);
		  File[] dirContents = dir.listFiles();
		  System.out.println("List of files "+dirContents.length);
		  System.out.println(dir.getName().contains(".pdf"));
		  for (int i = 1; i < dirContents.length; i++) {
			  System.out.println("name "+dirContents[i].getName());
		      if (dirContents[i].getName().equals(property)) {
		          // File has been found, it can now be deleted:
		          dirContents[i].delete();		      
		      }
		   		
		else {
			
			throw new Exception("File is not found or downloaded in the folder");
		}
		  }
	}*/
	
	public boolean isFileDownloaded_Ext(String dirPath, String ext) throws Exception{
        WebDriverWait wait = new WebDriverWait(driver, 10);
        boolean flag=false;
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            flag = false;
        }
       
        for (int i = 1; i < files.length; i++) {
            if(files[i].getName().contains(ext)) {
                flag=true;
            }
        }
        if(!flag)
        	throw new Exception("Impossibile scaricare il target file.");
        
        return flag;
	}
	
	public void deleteDownloadedFile(String dirPath, String filename) throws Exception{
		Thread.sleep(5000);
		File f = new File(dirPath+File.separator+filename);
		if(!f.delete())
			throw new Exception("Impossibile cancellare il target file.");
	}
}
