package com.nttdata.qa.enel.components.colla;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SupplyExcelDownload_BsnComponent extends BaseComponent {
	public By fornitureMenuItem = By.cssSelector("#menuForn > a");
	public By xlExportBtn = By.id("excel-export");
	
	public SupplyExcelDownload_BsnComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkFileDidDownload() throws Exception {
		String downloadPath = util.getChromeDownloadFolderPath();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M_d_yyyy");  
		LocalDate now = LocalDate.now();
		String fileName = "forniture-" + dtf.format(now) + ".csv";
		File dir = new File(downloadPath);
		File[] dirContents = dir.listFiles();
		boolean fileFound = false;
		for (int i = 0; i < dirContents.length; i++) {
			if (dirContents[i].getName().equals(fileName)) {
				fileFound = true;
				break;
			}
		}
		if (!fileFound) {
			throw new Exception("File " + fileName + " was not downloaded to directory " + downloadPath);
		}
	}

}
