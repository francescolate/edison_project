package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaFornituraNOPodComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;
	public String card = "//*[text()='$customer_num$']/ancestor::div[@class='card forniture regolare']";
	public String cardButton = "//*[text()='$customer_num$']/ancestor::div[@class='card forniture regolare']//a";
    
    public PrivateAreaFornituraNOPodComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(String existingObject) throws Exception {
		By by = By.xpath(existingObject);
		if (!util.exists(by, 20))
			throw new Exception("object with xpath " + by + " is not exist.");
	}
	
	public void verifyComponentAbsence(String existingObject) throws Exception {
		By by = By.xpath(existingObject);
		if (util.exists(by, 20))
			throw new Exception("object with xpath " + by + " exists within the page.");
	}
	
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public static final String PRIVATE_AREA_MENU_RES = "Forniture;Bollette;Servizi;enelpremia WOW!;Novità;Spazio Video;Account;I tuoi diritti;Supporto;Trova Spazio Enel;Esci";
	public static final String PRIVATE_AREA_MENU_RES_LIKE = "Forniture;Bollette;Servizi;Novità;Spazio Video;Account;I tuoi diritti;Supporto;Trova Spazio Enel;Esci";
	public static final String PRIVATE_AREA_MENU_BSN = "HomePage;bollette;forniture;servizi;messaggi;stato richieste;report billing management;carica documenti;contatti;logout";
}
