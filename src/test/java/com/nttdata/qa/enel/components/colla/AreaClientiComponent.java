package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class AreaClientiComponent extends RootComponent{
	WebDriver driver;
	SeleniumUtilities util;
	JavascriptExecutor js;
	
	public By areaClientiImpresa = By.xpath("//a[@href='/it/area-clienti/imprese']");
	public By areaClientiCasa = By.xpath("//a[@href='/it/area-clienti/residenziale']");
    		
    		
	public AreaClientiComponent(WebDriver driver) throws Exception {
		super(driver);
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	
}
