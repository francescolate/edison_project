package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SezioneSelezioneClienteUscenteComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By sezioneClienteUscente=By.xpath("//h2[text()='Selezione Cliente Uscente']");
	public By checkBoxCF=By.xpath("//legend[text()='Criterio di Interrogazione']/ancestor::slot[1]//span[text()='Codice Fiscale']/ancestor::label[@class='slds-radio__label']//span[@class='slds-radio_faux']");
	public By checkBoxPIVA=By.xpath("//legend[text()='Criterio di Interrogazione']/ancestor::slot[1]//span[text()='Partita IVA']/ancestor::label[@class='slds-radio__label']//span[@class='slds-radio_faux']");
	public By checkBoxPodPdr=By.xpath("//legend[text()='Criterio di Interrogazione']/ancestor::slot[1]//span[text()='Pod/PDR']/ancestor::label[@class='slds-radio__label']//span[@class='slds-radio_faux']");
	public By checkBoxNumeroCliente=By.xpath("//legend[text()='Criterio di Interrogazione']/ancestor::slot[1]//span[text()='Numero Cliente']/ancestor::label[@class='slds-radio__label']//span[@class='slds-radio_faux']");
	//public By inputBoxCF=By.xpath("//label[text()='Codice Fiscale']/ancestor::slot[1]//input[contains(@id,'Codice Fiscale')]");//FR 31.08.2021
	public By inputBoxCF=By.xpath("//label[text()='Codice Fiscale']/ancestor::slot[1]//input[contains(@id,'Fiscale')]");//FR 31.08.2021
	//public By inputBoxPIVA=By.xpath("//label[text()='Partita Iva']/ancestor::slot[1]//input[contains(@id,'Partita Iva')]");//FR 31.08.2021
	public By inputBoxPIVA=By.xpath("//label[text()='Partita Iva']/ancestor::slot[1]//input[contains(@id,'Partita')]");
	public By inputBoxPodPdr=By.xpath("//label[text()='POD/PDR']/ancestor::slot[1]//input[contains(@id,'POD/PDR')]");
	//public By inputBoxNumeroCliente=By.xpath("//label[text()='Numero Cliente']/ancestor::slot[1]//input[contains(@id,'Numero Cliente')]");//FR 31.08.2021
	public By inputBoxNumeroCliente=By.xpath("//label[text()='Numero Cliente']/ancestor::slot[1]//input[contains(@id,'Cliente')]");
	public By buttonRicerca=By.xpath("//button[text()='Ricerca' and @name='Ricerca']");
	public By tableHeaderBy = By.xpath("//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody");
	public By checkBox= By.xpath("//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']//tbody//label//span[@class='slds-radio_faux']");
	public By buttonConferma=By.xpath("//button[@name='Conferma' and text()='Conferma']");
	public By buttonConfermaAbilitato=By.xpath("//button[@name='Conferma' and text()='Conferma']");
	public String colonne []={"","Nome","Codice Fiscale","Ragione Sociale","Indirizzo di residenza"};
	public By popUpMessaggioErroreSelezioneMercato = By.xpath("//h2[text()='Tale ricerca può essere effettuata nel regime di mercato Libero e Fui']");
	public String testopopUpErroreAtteso="Tale ricerca può essere effettuata nel regime di mercato Libero e Fui";
	public By popUpMessaggioErroreSelezioneMercatoFUI = By.xpath("//h2[text()='Tale ricerca può essere effettuata nel regime di mercato Libero e Salvaguardia']");
	public String testopopUpErroreAttesoMercatoFUI="Tale ricerca può essere effettuata nel regime di mercato Libero e Salvaguardia";	
	public By clienteUscentePIva = By.xpath("//div[text()='COMUNE DI AUGUSTA I.P.']/ancestor::tr//label[@class='slds-radio__label']/span[1]");
	public By closeMessageFUI = By.xpath("//h2[text()='Sono presenti prodotti nel Mercato Libero che non possono essere gestiti nel Mercato FUI. Se il cliente vuole procedere con la Voltura anche per queste forniture ricordati di inserire una nuova richiesta di VSA al termine del processo in corso.']/..//button");
	public SezioneSelezioneClienteUscenteComponent(WebDriver driver){
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		util.getFrameActive();
	    if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	
	public void ifExistence(By existingObject) throws Exception {
	    if (util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	
	public void ifExistClick(By existingObject) throws Exception {
	    if (util.exists(existingObject, 15))
	    	clickComponent(existingObject);
		}
	
	
	
	public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.currentThread().sleep(5000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
	}
	 
	 public void enterPodValue(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	 
	 public void enterCodFiscale(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	 
	 public void verificaColonneTabella(String [] col) throws Exception{
		 List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th/div"));
		 if (nomeColonne.size()==col.length) {
			 for(int i = 1; i < col.length; i++){
				    WebElement c=driver.findElement(By.xpath("//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/thead/tr/th["+(i+1)+"]/div"));
					String columnName=c.getText();	
					//System.out.println(columnName);
					 if(!columnName.contentEquals(col[i].trim()))
					       	throw new Exception("la tabella con le informazioni del cliente non contiene le colonne con descrizione: "+col[i]+"; il nome delle colonne visualizzato e' "+columnName);  
				}
		 }
	 }
	
	 
	 public void selectRecordDenomazioneInTabella (String value) throws Exception {
		//td/c-ita_ifm_lwc045_input[@data-id='BRIGATA BERSAGLIERI GARIBALDI']/ancestor::tr//label[@class='slds-radio__label']/span[1]
		//By obj =  By.xpath("//div[text()='"+value+"']/ancestor::tr//label[@class='slds-radio__label']/span[1]");
		 By obj =  By.xpath("//td/c-ita_ifm_lwc045_input[@data-id='"+value+"']/ancestor::tr//label[@class='slds-radio__label']/span[1]"); //R3 FR 20.09.2021
		
		 util.objectManager(obj, util.scrollToVisibility, false);
			Thread.sleep(5000);
			util.objectManager(obj, util.scrollAndClick);
			
		 
	 }
	 
	 
	 
	 
	 public void selezionaRecordInTabella(String cf) throws Exception
		{
			TimeUnit.SECONDS.sleep(3);
			int r=0;
			List<WebElement> listaTR = driver.findElements(By.xpath("//table[@class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr"));
			
			if (listaTR.size()<1) throw new Exception("la tabella con le informazioni del cliente non contiene righe");  
				for(WebElement li:listaTR){
				  //List<WebElement> listaTD = li.findElements(By.xpath("td")); FR 17.09.2021
				  List<WebElement> listaTD = li.findElements(By.xpath( "//td/c-ita_ifm_lwc045_input[@data-id]"));
				 
					for (WebElement i:listaTD)
					{
						//String codf = i.getText();//FR 17.09.2021
						String codf = i.getAttribute("data-id");
						r=listaTR.indexOf(li);
						if(codf.compareToIgnoreCase(cf)==0 && listaTR.indexOf(li)==r){
						   // listaTD.get(0).click();
							driver.findElement(By.xpath("//table//label/span[@class='slds-radio_faux']")).click();
						    break;  
			               }
                      
			}
				}
			
			
		}
	 

		public void verifyErrorMessageSelezioneMercato(By objText,String popupTestoAtteso) throws Exception{
			if(!util.verifyExistence(objText, 60)) throw new Exception("Non è stato visualizzato il messaggio di errore con testo: "+testopopUpErroreAtteso);
			String textModal = util.waitAndGetElement(objText,false).getText();
//			System.out.println(textModal);
//			if(textModal.compareTo(testopopUpErroreAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+testopopUpErroreAtteso);
		}
		
}
