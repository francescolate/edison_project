package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ModalitaFirmaComponent extends BaseComponent{
	 private WebDriver driver;
	    private SeleniumUtilities util;
	    String frameName;
	    SpinnerManager spinner;

	
	public ModalitaFirmaComponent(WebDriver driver) {
		super(driver);
		  this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
	}

	public static By modalitaFirma = By.xpath("//span[contains(text(),'Modalita firma')]/following::select[1]");
	public static By modalitaFirmaCanaleInvio = By.xpath("//span[contains(text(),'Modalità firma e Canale Invio')]/following::select");
	public static By deliveryChannel = By.xpath("//span[contains(text(),'Delivery channel')]//ancestor::div[@class='slds-form-element is-required']//select");
    public static By registrazioneVocaleButton = By.xpath("//div[@class='slds-p-top_small']//button[text()='REGISTRAZIONE VOCALE' or text()='Registrazione Vocale']");
    public static By avantiButton = By.xpath("//button[text()='Avanti']");
    public static By emailField = By.xpath("//span[contains(text(),'Email')]/following::input");
    public static By linkVerificaIndirizziEsistenti = By.xpath("//a[text()='Verifica Indirizzi Esistenti']");
    public static By primoIndirizzoEsistente = By.xpath("//a[text()='Verifica Indirizzi Esistenti']/following::li[@class='slds-item']/following::label[@class='slds-checkbox']");
    public static By importaButton = By.xpath("//button[text()='Importa']");
    public static By verificaButton = By.xpath("//button[text()='Verifica']");
    public static By indirizzoVerificato = By.xpath("//span[text()='Indirizzo verificato']");
    public By confermaButton = By.xpath("//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");
    public static By forzaVocalOrder = By.xpath("//button[text()='Forza vocal order']");
    public static By confirmQuoteButton = By.xpath("//button[@name='ConfirmQuote']");
    public static By tornaAllOffertaButton = By.xpath("//button[contains(text(),'Torna all') and contains(text(),'offerta')]");
    public By cellulareField = By.xpath("//label[@data-label='phoneValue']//..//input");
    public By sezioniConfermate=By.xpath("//div[text()='Tutte le sezioni confermate']");
    public By buttonConferma=By.xpath("//div[@class='slds-card']//button[@name='ConfirmQuote']");
    public By PopupErroreSep=By.xpath("//h2[text()='La modalità di firma NO VOCAL è compatibile con il prodotto Fibra solo per canali SE e SEP e canale invio contratto STAMPA LOCALE']");
    
	public void switchFrame() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.switchToFrame(frame);
	}
	
	public void selezionaModalitaFirma(By modalitaFirma, String modalita) throws Exception{
		util.objectManager(modalitaFirma, util.scrollToVisibility, false);
		util.objectManager(modalitaFirma,util.select, modalita);
		spinner.checkSpinners();
	}
	
	public void selezionaDeliveryChannel(By deliveryChannel, String channel) throws Exception{
		util.objectManager(deliveryChannel,util.select, channel);
		spinner.checkSpinners();
	}
	
	public void press(By object) throws Exception{
		util.objectManager(object, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void enterField(By field, String text) throws Exception{
		util.objectManager(field, util.scrollAndSendKeysAndTab, text);
	}
	
	public void pressButton(By button) throws Exception {
        util.objectManager(button, util.scrollAndClick);
    }
	
	public void selezionaIndirizzoEsistente() throws Exception{
		util.getFrameActive();
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(linkVerificaIndirizziEsistenti, util.scrollAndClick);
		util.objectManager(primoIndirizzoEsistente, util.scrollAndClick);
		util.objectManager(importaButton, util.scrollAndClick);
		util.objectManager(verificaButton, util.scrollAndClick);
		spinner.checkSpinners();
		if(!util.verifyExistence(indirizzoVerificato, 10)) throw new Exception("Per il canale Posta, l'indirizzo esistente importato non risulta verificato");
	}

	public void selezionaCanaleInvio(String value) throws Exception{
		By list = By.xpath("//span[text()='Modalità firma e Canale Invio']//ancestor::div[@role='region']//label[text()='Canale Invio']/..//input");
		util.objectManager(list, util.scrollAndClick);
		By val = By.xpath("//span[text()='Modalità firma e Canale Invio']//ancestor::div[@role='region']//label[text()='Canale Invio']/..//span[text()='"+value+"']");
		util.objectManager(val, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void selezionaModalitaFirma(String value) throws Exception{   
		By list = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']//input");
		util.objectManager(list, util.scrollAndClick);
		By val = By.xpath("//h2//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Modalità Firma']/ancestor::div[@role='none']/..//span[text()='"+value+"']");
		util.objectManager(val, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void selezionaCanaleInvioAllaccio(String value) throws Exception{
		By list = By.xpath("//span[text()='Modalità firma e Canale Invio']//ancestor::div[@role='region']//label[text()='Canale Invio']/..//input");
		util.objectManager(list, util.scrollAndClick);
		By val = By.xpath("//span[text()='Modalità firma e Canale Invio']//ancestor::div[@role='region']//label[text()='Canale Invio']/..//span[text()='"+value+"']");
		util.objectManager(val, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	
	public void verificaModalitàFirmaPrecompilato(String value) throws Exception{
		String modalitaFirmaText = util.waitAndGetElement(By.xpath("//label[text()='Modalità Firma']/following::input"),true).getAttribute("value");
		if(modalitaFirmaText.compareTo(value)!=0) throw new Exception("Modalità Firma non è precompilato con il valore :"+value);
	
	}
	
	public void VerificaErroreSeSep(By PopupSep)throws Exception{
		String MessaggioAtteso = util.waitAndGetElement(PopupSep,false).getText();
		if(!util.verifyExistence(PopupSep ,30)){
			throw new Exception("Non è presente il pop up con testo:"+ MessaggioAtteso);
		}	
		
	}
	
	public void impostaModalitàFirma(String value) throws Exception{
		  util.selectLighningValue("Modalità Firma", value);
	}
	
	
	public void verificaCanaleInvioPrecompilato(String value) throws Exception{
		String modalitaFirmaText = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[text()='Canale Invio']/ancestor::div[@class='slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click']//input[@class='slds-input readonly']"),true).getAttribute("value");
		if(modalitaFirmaText.compareTo(value)!=0) throw new Exception("Canale Invio non è precompilato con il valore :"+value);
	
	}
	
	public void verificaEmailPrecompilato() throws Exception{
		String modalitaFirmaText = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='main']//label[text()='Indirizzo Email']/following::input"),true).getAttribute("value");
		if(modalitaFirmaText.length()<2) throw new Exception("il campo email è vuoto");
	
	}
	
	public boolean isElementEnabled(By existentObject) throws Exception{
		
		if ((driver.findElement(existentObject).isEnabled()))
				return true;
		else {
			return false;
		}
	}
	
}
