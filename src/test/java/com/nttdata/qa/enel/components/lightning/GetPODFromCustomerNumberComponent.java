package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GetPODFromCustomerNumberComponent {
	WebDriver driver;
	public SeleniumUtilities util;

	public By searchBar = By.xpath("//div[contains(@class, 'forceSearchInputDesktopPillWrapper')]/following-sibling::div//input");
	public By accountLink = By.xpath("//a[text()='Clienti']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']//tbody//th[1]//a");
	public String accountEmail = "//p[text()='Email referente']/..//*[text()='$email$']";
	public By podTD = By.xpath("//span[@title='POD/PDR']/ancestor::table//td[4]");
	public String pod = "//*[text()='$CUSTOMER_NUMBER$']/ancestor::dl//a";
	public By serviceRequestHeader = By.xpath("//span[contains(text(), 'Service Request')]/parent::a");
	//public String attivazioneVasTD = "//span[contains(text(), '$DATE$')]/../../preceding-sibling::td//span[contains(text(),'VAS - Modifica')]";
	public String attivazioneVasTD = "//span[contains(text(), '$DATE$')]/../../preceding-sibling::td//span//a[contains(text(),'VARIAZIONE VAS')]";
	public By serviziEBeniNew = By.xpath("//button[text()='Altro']/lightning-primitive-icon");	
	public By serviziEBeni = By.xpath("//a[text()='Servizi e Beni']");	
	public By podSearchBar = By.xpath("//*[@name='inline-search-input']");
	
	public GetPODFromCustomerNumberComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}	

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
		Thread.sleep(500);
	}
	
	public void scrollAndVerify(By by) throws Exception{
		int counter = 10;
		boolean found = false;
		while(counter>0){
			util.jsScroll();
			if(util.exists(by, 5)){
				counter = 0;
				found = true;
			}
			else
				counter++;
			
			Thread.sleep(100);
		}
		
		if(!found)
			throw new Exception("object with xpath " + by + " is not exist.");
		
		Thread.sleep(200);
	}
	
	public void searchAccount(By by, String filter) throws Exception{
		WebElement we = driver.findElement(by);
		we.sendKeys(filter);
	}
	
	public void searchAccount(By by, String filter, boolean newLine) throws Exception{
		Thread.sleep(5000);
		WebElement we = driver.findElement(by);
		if(newLine){
			we.sendKeys(filter+"\n");
		}
		else{
			we.sendKeys(filter);
		}
	}
	
	public void fillInputField(By by, String str) throws Exception{
		util.objectManager(by, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(by, util.sendKeys, str);
	}
	
	public void scrollToVisibility(By by) throws Exception{
		util.objectManager(by, util.scrollToVisibility, false);
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public String getPOD(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		return we.getText();
	}
	
	public void clearSearchBar(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		we.clear();
		Thread.sleep(250);
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
}
