package com.nttdata.qa.enel.components.lightning;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CustomerDetailsPageComponent {
	
	WebDriver driver;
	public SeleniumUtilities util;
	Actions actions;
	JavascriptExecutor jse;
	SpinnerManager spinner;
	
	public By richiesteAperte = By.xpath("//span[contains(text(),'Richieste Aperte (200)')]");
	public By numeroRichiesta = By.xpath("//div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][2]/span[@class='slds-text-heading_small']/a");
	public By tipologiaRichiesta = By.xpath("//div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][1]/p/strong");
	public By stato = By.xpath("//div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][2]/dl[@class='slds-list_horizontal slds-wrap']/dd[@class='slds-item_detail slds-truncate'][1]");
	public By sottoStato = By.xpath("//div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][2]/dl[@class='slds-list_horizontal slds-wrap']/dd[@class='slds-item_detail slds-truncate'][2]");
	public By creazione = By.xpath("//div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][2]/dl[@class='slds-list_horizontal slds-wrap']/dd[@class='slds-item_detail slds-truncate'][3]");
	public By offerta = By.xpath("//div[@class='slds-p-top_small cITA_IFM_LCP113_PendingCaseCard'][2]/dl[@class='slds-list_horizontal slds-wrap']/dd[@class='slds-item_detail slds-truncate'][4]");
	public By inLavorazione = By.xpath("//force-highlights-details-item[@class='slds-page-header__detail-block'][1]/div/p[@class='fieldComponent slds-text-body--regular slds-show_inline-block slds-truncate']/slot/lightning-formatted-text");
//	public By caseNumber = By.xpath("//force-highlights-details-item[@class='slds-page-header__detail-block'][2]/div/p[@class='fieldComponent slds-text-body--regular slds-show_inline-block slds-truncate']/slot/lightning-formatted-text");
	public By richestaHeader = By.xpath("//div[@class='slds-media__body slds-align-middle']//span[contains(text(), 'Richieste')]");
	public String caseNumber = "//span[contains(text(),'$DATE$')]/ancestor::td/preceding-sibling::td//span[contains(text(),'In Lavorazione')]/ancestor::td/preceding-sibling::td//span//a[contains(text(),'Modifica Potenza')]/ancestor::td/preceding-sibling::th//span//a";
	public String caseNumberVoltageChange = "//span[contains(text(),'$DATE$')]/ancestor::td/preceding-sibling::td//span[contains(text(),'In Lavorazione')]/ancestor::td/preceding-sibling::td//a[text()='Modifica Tensione']/ancestor::td/preceding-sibling::th//a";
	public By elementoRichesta = By.xpath("//span[contains(text(),'Elemento Richiesta')]//parent::a");
	public By preventivoInviato=By.xpath("//span[contains(text(),'PREVENTIVO INVIATO')]");
	public By tableField = By.xpath("//div[@class='listViewContent']//child::table//thead//tr//th");
	public By tableValue = By.xpath("//span[contains(text(),'IT001E73277736')]");
	public By numeroUtenza = By.xpath("//span[contains(text(),'843517154')]");
	public By elementoRichestaTableValuesList = By.xpath("//div[@id='brandBand_9']//tbody//tr//td");
	public By elementoRichestaTableField = By.xpath("//div[@id='brandBand_10']//table//th//div//a");
	public String elementoRichValue ="//*[text()='Elemento Richiesta']/ancestor::div[@class='test-listViewManager slds-card slds-card_boundary slds-grid slds-grid--vertical forceListViewManager']//span[contains(text(),'$Value$')]";
	public By podER = By.xpath("//span[contains(text(),'PREVENTIVO ')]/ancestor::td/following-sibling::td//span[contains(text(),'IT001E73277736')]");
	public By numeroUtenzaER = By.xpath("//span[contains(text(),'IT001E73277736')]/ancestor::td/following-sibling::td//span[@title='843517154']"); 
	public By BOZZAER = By.xpath("//*[text()='Elemento Richiesta']/ancestor::div[@class='test-listViewManager slds-card slds-card_boundary slds-grid slds-grid--vertical forceListViewManager']//span[contains(text(),'PREVENTIVO INVIATO')]/ancestor::td/following-sibling::td//span[contains(text(),'IT001E73277736')]/ancestor::td/following-sibling::td//span[@title='843517154']/ancestor::td/following-sibling::td//span[text()='BOZZA']/ancestor::td/following-sibling::td//span[text()='BOZZA']");
	public By OKER = By.xpath("//*[text()='Elemento Richiesta']/ancestor::div[@class='test-listViewManager slds-card slds-card_boundary slds-grid slds-grid--vertical forceListViewManager']//span[contains(text(),'PREVENTIVO INVIATO')]/ancestor::td/following-sibling::td//span[contains(text(),'IT001E73277736')]/ancestor::td/following-sibling::td//span[@title='843517154']/ancestor::td/following-sibling::td//span[text()='BOZZA']/ancestor::td/following-sibling::td//span[text()='BOZZA']/ancestor::td/following-sibling::td//span[text()='OK']");
	public By caseNumER = By.xpath("//div[@class='slds-truncate']//lightning-formatted-text[contains(text(),'PREVENTIVO')]/ancestor::td/preceding-sibling::th//a");
	public By caseOI = By.xpath("//span[text()='IT001E73277736']/ancestor::td/preceding-sibling::th//span//a");
	public By caseOISafe = By.xpath("//*[text()='Elemento Richiesta']/ancestor::div[@class='test-listViewManager slds-card slds-card_boundary slds-grid slds-grid--vertical forceListViewManager']//span[contains(text(),'PREVENTIVO ')]/ancestor::td/preceding-sibling::th//a");
	public By caseNum = By.xpath("//span[contains(text(),'PREVENTIVO RICHIESTO')]/ancestor::td/preceding-sibling::th//a");
	public By caseNum147 = By.xpath("(//table[@aria-rowcount='1']//a[@tabindex='0']//span)[1]");
	public By orderId = By.xpath("//span[text()='Elemento Richiesta']/ancestor::lst-related-list-view-manager//a/span[contains(text(), 'OI-')]");
	public By richestaHeader1 = By.xpath("//div[@class='leftContent slds-col slds-wrap slds-text-body--small slds-text-color--weak']//h2//a[text()='Richieste']");
	public String request301 = "//span[contains(text(),'$DATE$')]/ancestor::td/preceding-sibling::td//span[contains(text(),'Chiuso')]/ancestor::td/preceding-sibling::td//a[text()='VARIAZIONE ANAGRAFICA']/ancestor::td/preceding-sibling::th//a";
	public By attivitaHeader = By.xpath("//forcegenerated-adgrollup_component___forcegenerated__flexipage_recordpage___ita_ifm_caserecordpage___case___view//h2[contains(@id,'header_')]/a/span[@title='Attività']");
	public By touchPointHeader = By.xpath("//forcegenerated-adgrollup_component___forcegenerated__flexipage_recordpage___ita_ifm_caserecordpage___case___view//h2[contains(@id,'header_')]/a/span[@title='Touch Point']");
	
	public CustomerDetailsPageComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		actions = new Actions(driver);
		jse = (JavascriptExecutor) driver;
		spinner = new SpinnerManager(driver);
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			/*util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(10000);
			util.objectManager(clickableObject, util.scrollAndClick);*/
			WebElement button=driver.findElement(clickableObject);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", button);
			
	}
	 public void clickRichesta(By clickableObject) throws Exception {
			
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(0,2000)");
			
			WebElement button=driver.findElement(clickableObject);
			js.executeScript("arguments[0].click();", button);		
		}
	 
	 public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkFieldValue(By checkObject,String propert) throws Exception{
			String value = driver.findElement(checkObject).getText();
			if(!value.equalsIgnoreCase(propert))
				throw new Exception(value+" Field value is not matching with expected value"+propert);
		}
		
		public void checkFieldLength(By checkObject,String length) throws Exception{
			String value = driver.findElement(checkObject).getText();
			char[] sliptString = value.toCharArray();
			String i = sliptString.length+"";
			System.out.println(sliptString.length);
			System.out.println(length);
			if(!length.contentEquals(i))
				throw new Exception(" Field length is not matching with expected value "+length);
			
		}
		
		public String getDate() {
	    	SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMMM-YYYY",Locale.ITALIAN);
	    	Date today = new Date();
	        String date = DATE_FORMAT.format(today);
	        date = date.replace("-", " ");
	      //  DateTimeFormatter fIn = DateTimeFormatter.ofPattern( date , Locale.ITALIAN ); 
	        System.out.println("Date "+date);
	        return date;
		}
		
		public void checkDate(By checkObject) throws Exception {
			String value = getDate();
			System.out.println(value);
			System.out.println(driver.findElement(checkObject).getText());
			checkFieldValue(checkObject, value);
		}
		
		public void checkelementoRichestaValues(By checkObject,String property) throws Exception
		{			
			String value = driver.findElement(checkObject).getText();
			/*System.out.println(property);
			System.out.println(value);*/
			if(!property.contentEquals(value))
				throw new Exception(property+"Expected Value is not mathcing with ElementoRichesta content value"+value);			
		}
				
		public void pressJavascript(By oggetto) throws Exception {
			WebElement element = util.waitAndGetElement(oggetto);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
		
		public void clickCaseOI(By checkObject) throws Exception{
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			WebElement element = util.waitAndGetElement(checkObject);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[1].click();", element);
		}
		
		public void checkElementoRichiestaStatus() throws Exception {
			for (String value:elementoRichiestaIdStrings) {
				checkelementoRichestaValues(By.xpath(elementoRichValue.replace("$Value$", value)), value);
			}
		}
		
		public void checkElementoRichiestaStatus2() throws Exception {
			for (String value:elementoRichiestaIdStrings2) {
				checkelementoRichestaValues(By.xpath(elementoRichValue.replace("$Value$", value)), value);
			}
		}
	
	public static final String statoText ="Stato: ";
	public static final String SottostatoText ="IN LAVORAZIONE";
	public static final String numero = "9";
	public static final String offertaLength = "8";
	
	//----- TC135 Constants -----
	public final String[] elementoRichiestaIdStrings  = {
			"PREVENTIVO RICHIESTO", //Stato Elemento richiesta
			"PRESA IN CARICO", //Stato R2D
			"BOZZA", //Stato SAP
			"BOZZA", //Stato SEMPRE
			"BOZZA", //Stato SDSAP
			"NON PREVISTO" //Stato UDB
	};
	public final String[] elementoRichiestaIdStrings2  = {
			"PREVENTIVO INVIATO", //Stato Elemento richiesta
			"DA COMPLETARE", //Stato R2D
			"BOZZA", //Stato SAP
			"BOZZA", //Stato SEMPRE
			"OK", //Stato SDSAP
			"NON PREVISTO" //Stato UDB
	};

}
