package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class DocumentValidatorComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	final By conferma = By.xpath("//div[@id='divStartId']//button[text()='Conferma']");
	final By titolo_edilizio_checkbox = By.xpath("//input[@sfdclabel='Titolo Edilizio']/ancestor::label[@class='slds-checkbox']//span");
	final By doc_num_pag= By.xpath("//input[contains(text(),'Numero pagina documento')]");
	final By istanza_num_pag= By.xpath("//input[contains(text(),'Numero pagina Istanza')]");
		
	final By fieldsToValidate = By.xpath("//article[@id='elementi-checklist-card']"
			+ "//select [contains(@class,'sfdcField is-required')] "
			+ "| //input [contains(@class,'sfdcField is-required')]" ); 
	final By allfieldsToValidate = By.xpath("//article[@id='elementi-checklist-card']//select [contains(@class,'sfdcField')]| //input [contains(@class,'sfdcField')]");
	public By statoChiusaOfferta=By.xpath("//p[text()='Stato']/ancestor::div[1]//slot/lightning-formatted-text[text()='Chiusa']");
//	final By documentsToValidate = By.xpath("//tbody[@id='documents']/tr/td/label/input");
	final By documentsToValidate = By.xpath("//tbody[@id='documents']/tr/td[@data-label='Obbligatorio']/div[text()='Y']/ancestor::tr/td/label/input");
	final By allDocuments = By.xpath("//tbody[@id='documents']/tr/td/div[text()='Y' or text()='N']/ancestor::tr/td/label/input");

	final By startCheckList = By.id("Effectua-Checklist-button");
	
	final By chiudi = By.id("cancelButton"); 
	
	final By elementoTabella=By.xpath("//div[@id='divId']/h2/span");
	
	final By spinnerValidazioneDocumenti=By.xpath("//div[@class='slds-modal__container']"
			+ "//div[@class='slds-spinner--large']/img[@alt='Caricamento in corso']");
	
	final By validaDocButton = By.xpath("//button[text()='Valida Documento']");
	final By effettuaChecklistButton = By.xpath("//button[contains(text(),'Effettua Checklist')]");
	
	public final By validaDocumentazione = By.xpath("//button[@data-name='Valida Documentazione']");
	
	
	final By requiredCheckbox = By.xpath("//span[@title='required']/ancestor::label[@class='slds-checkbox']/span[contains(@class,'slds-checkbox')]");
	public By documentiRichiestiBusiness=By.xpath("//article[@id='elementi-checklist-card']//select [contains(@class,'sfdcField')]| //input [contains(@class,'sfdcField')]/ancestor::div[2]//label[contains(@class,'slds-required') and (text()='Cognome e Nome Intestatario' or text()='Data Requisiti Salvaguardia' or text()='Firma') ]");

	public By verificaAppuntamento=By.xpath("//button[contains(text(),'Verifica Appuntamento')]");
	public By buttonEsci=By.xpath("//button[contains(text(),'Esci')]");
	public By buttonSi=By.xpath("//button[contains(text(),'Sì')]");
	public By luogoedataSottoscrizione=By.xpath("//input[@sfdclabel='Luogo e Data Sottoscrizione']//ancestor::label[@class='slds-checkbox']//span");
	public By firma=By.xpath("//input[@sfdclabel='Firma']//ancestor::label[@class='slds-checkbox']//span");
	public DocumentValidatorComponent(WebDriver driver)
	{
		this.driver=driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public void validaDoc(String frame) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
//		driver.switchTo().defaultContent();
		try {
////			WebElement frameToSw = driver.findElement(
////					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
////					);
//			WebElement frameToSw = null;
//			driver.switchTo().defaultContent();
//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			int i=1;
//			for (WebElement webElement : els) {
//				i++;
//				if(webElement.isDisplayed()) {
//					 frameToSw =  webElement;
//					break;
//				}
//				
//			}
//			driver.switchTo().frame(frameToSw);
        util.getFrameActive();
        
		validaDoc();
		}catch(Exception e){}
	finally
	{
		driver.switchTo().defaultContent();
	}
	
	}
	
	public void validaDocSubentro(String frame) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
		try {
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			validaDocProcessoSubentro();
		}catch(Exception e){
		
		}
	finally
	{
		driver.switchTo().defaultContent();
	}
	
	}
	
	public void validaDocSubentroBusiness(String frame) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
		try {
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			validaDocProcessoSubentroBusiness();
		}catch(Exception e){}
	finally
	{
		driver.switchTo().defaultContent();
	}
	
	}
	public void validaTuttiIDoc(String frame) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
		try {
			util.getFrameActive();
		validaAllDocs();
		}catch(Exception e){}
	finally
	{
		driver.switchTo().defaultContent();
	}
	
	}
	
	public void validaTuttiIDoc() throws Exception 
	{

        TimeUnit.SECONDS.sleep(10);
//		driver.switchTo().defaultContent();
//		TimeUnit.SECONDS.sleep(2);
//		util.getFrameActive();
		try {
		util.getFrameActive();
		validaAllDocs();
		}catch(Exception e){}
	finally
	{
		driver.switchTo().defaultContent();
	}
	
	}	
	
	public void validaTuttiIDocFast() throws Exception 
	{

        TimeUnit.SECONDS.sleep(10);
		try {
			util.getFrameActive();
			validaAllDocsFast();
		}catch(Exception e){}
	finally
		{
		driver.switchTo().defaultContent();
		}
	
	}

	
	
	public void verificaValidaDoc(String frame,String docs) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
	
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			
		verificaDocumenti(docs.split(";"));
		validaAllDocs();
		driver.switchTo().defaultContent();
	
	}
	
	public void verificaValidaDoc(String docs) throws Exception 
	{
		
		util.getFrameActive();
        TimeUnit.SECONDS.sleep(2);
		verificaDocumenti(docs.split(";"));
		validaAllDocs();
		driver.switchTo().defaultContent();
	
	}
	
	public void verificaValidaDocObbligatori(String frame,String docs) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
	
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			
		verificaDocumenti(docs.split(";"));
		validaDoc();
		driver.switchTo().defaultContent();
	
	}

	public void verificaDocumenti(String [] documenti) throws Exception{
		List<WebElement> list = util.waitAndGetElements(By.xpath("//span[text()='Elementi da Validare']/following::table//td[2]"));
		for(String doc : documenti) {
			boolean found = false;
			for(int x=0;x<list.size();x++) {
				if(list.get(x).getText().contentEquals(doc))found = true;
			}
			if(!found) throw new Exception("Nella lista dei documenti da validare non è stato trovato il documento "+doc);
	    	
	    }
	}
	
	public void validaDocSWAFast(String frame) throws Exception 
	{

        TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
		try {
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		validaDoc();
		}catch(Exception e){}
	finally
	{
		driver.switchTo().defaultContent();
	}
	
	}
	
	
	
	
	public void validaAllDocs() throws Exception {
		
        TimeUnit.SECONDS.sleep(10);

        util.exists(allDocuments, 100);
		List<WebElement> numelem = driver.findElements(allDocuments);
		if(numelem.size()==0) throw new Exception("Non sono stati trovati documenti da validare");

		for ( WebElement i : numelem)
		{
			String attr = i.getAttribute("disabled");
			if ( attr!=null && attr.contentEquals("true")) {
				continue;
			}
			i.findElement(By.xpath("..")).click();
			util.objectManager(startCheckList, util.scrollAndClick);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			for (WebElement ii : driver.findElements(fieldsToValidate))
			{
				String type = ii.getAttribute("sfdctype");
				switch (type) {
				case "PICKLIST":
					Select select = new Select(ii);
					select.selectByIndex(2);
					break;
				case "DATE":
					ii.sendKeys("27/02/3019");
					break;
				case "STRING":
					ii.sendKeys("12345678");
					break;
				case "BOOLEAN":
					ii.findElement(By.xpath("..")).click();
					break;
				}	
			}
			util.objectManager(validaDocButton, util.scrollAndClick);
			spinner.checkSpinners();
			TimeUnit.SECONDS.sleep(5);

			//Aspetta che l'elemento spinner scompaia
			util.waitUntilIsNotDisplayed(spinnerValidazioneDocumenti);
     		TimeUnit.SECONDS.sleep(5);
		}
		
		//Click su eventuali check box obbligatori da cliccare presenti in pagina, non presenti nelle dettaglio del documento da validare
		List<WebElement> numeCheckboxRequired= driver.findElements(requiredCheckbox);
		for (int j=0;j<numeCheckboxRequired.size();j++){
			numeCheckboxRequired.get(j).click();
		}
		
		util.waitAndGetElement(conferma).click();

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}

	public void validaAllDocsFast() throws Exception {
		
        TimeUnit.SECONDS.sleep(10);

        util.exists(allDocuments, 100);
		List<WebElement> numelem = driver.findElements(allDocuments);
		if(numelem.size()==0) throw new Exception("Non sono stati trovati documenti da validare");

		for ( WebElement i : numelem)
		{
			String attr = i.getAttribute("disabled");
			if ( attr!=null && attr.contentEquals("true")) {
				continue;
			}
			i.findElement(By.xpath("..")).click();
			util.objectManager(startCheckList, util.scrollAndClick);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			for (WebElement ii : driver.findElements(fieldsToValidate))
			{
				String type = ii.getAttribute("sfdctype");
				switch (type) {
				case "PICKLIST":
					Select select = new Select(ii);
					select.selectByIndex(2);
					break;
				case "DATE":
					ii.sendKeys("27/02/3019");
					break;
				case "STRING":
					ii.sendKeys("12345678");
					break;
				case "BOOLEAN":
					ii.findElement(By.xpath("..")).click();
					break;
				}	
			}
			util.objectManager(validaDocButton, util.scrollAndClick);
//			spinner.checkSpinners();
			TimeUnit.SECONDS.sleep(8);
			//Aspetta che l'elemento spinner scompaia
			util.waitUntilIsNotDisplayed(spinnerValidazioneDocumenti);
     		TimeUnit.SECONDS.sleep(5);
		}
		//Click su eventuali check box obbligatori da cliccare presenti in pagina, non presenti nelle dettaglio del documento da validare
		List<WebElement> numeCheckboxRequired= driver.findElements(requiredCheckbox);
		for (int j=0;j<numeCheckboxRequired.size();j++){
			numeCheckboxRequired.get(j).click();
		}
		
		util.waitAndGetElement(conferma).click();

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}

	
	
	public void validaDoc() throws Exception {
		
        TimeUnit.SECONDS.sleep(5);
//		//System.out.println("inizio for");
//        util.waitUntilIsDisplayed(documentsToValidate);
        util.exists(documentsToValidate, 100);
		List<WebElement> numelem = driver.findElements(documentsToValidate);
//		System.out.println("Numero elementi da validare : "+numelem.size());
		
		
		for ( WebElement i : numelem)
		{

			String attr = i.getAttribute("disabled");
			if ( attr!=null && attr.contentEquals("true")) {
  
				continue;
			}

			i.findElement(By.xpath("..")).click();
			util.objectManager(startCheckList, util.scrollAndClick);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			for (WebElement ii : driver.findElements(fieldsToValidate))
			{
				String type = ii.getAttribute("sfdctype");
//				System.out.println("Input type : " + type);
				switch (type) {

				case "PICKLIST":
					Select select = new Select(ii);
					select.selectByIndex(2);
					break;
				case "DATE":
					ii.sendKeys("27/02/3019");
					break;
				case "STRING":
					ii.sendKeys("12345678");
					break;
				case "BOOLEAN":
					ii.findElement(By.xpath("..")).click();
					break;
				}	
			}
			util.objectManager(validaDocButton, util.scrollAndClick);
			spinner.checkSpinners();
			TimeUnit.SECONDS.sleep(3);
			//Aspetta che l'elemento spinner scompaia
			util.waitUntilIsNotDisplayed(spinnerValidazioneDocumenti);
     		TimeUnit.SECONDS.sleep(5);
		}
		
		//Click su eventuali check box obbligatori da cliccare presenti in pagina, non presenti nelle dettaglio del documento da validare
		List<WebElement> numeCheckboxRequired= driver.findElements(requiredCheckbox);
		for (int j=0;j<numeCheckboxRequired.size();j++){
			numeCheckboxRequired.get(j).click();
		}
		
		util.waitAndGetElement(conferma).click();
		
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}
	
   public void validaDocProcessoSubentro() throws Exception {
		
        TimeUnit.SECONDS.sleep(5);
        util.objectManager(By.xpath("//div[@class='slds-col slds-no-flex  slds-container--right']//button[text()='Indietro']"), util.scrollToVisibility, true);
        TimeUnit.SECONDS.sleep(3);
        util.exists(documentsToValidate, 100);
		List<WebElement> numelem = driver.findElements(documentsToValidate);

		
		
		for ( WebElement i : numelem)
		{

			String attr = i.getAttribute("disabled");
		//	System.out.println(attr);
			if ( attr!=null && attr.contentEquals("true")) {
  
				continue;
			}

			i.findElement(By.xpath("..")).click();
			util.objectManager(startCheckList, util.scrollAndClick);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
             
				
			for (WebElement ii : driver.findElements(allfieldsToValidate))
			{
				String type = ii.getAttribute("sfdctype");
//				System.out.println("Input type : " + type);
				switch (type) {

				case "PICKLIST":
					Select select = new Select(ii);
					select.selectByIndex(2);
					break;
				case "DATE":
					ii.sendKeys("27/02/3019");
					break;
				case "STRING":
					ii.sendKeys("12345678");
					break;
				case "BOOLEAN":
					ii.findElement(By.xpath("..")).click();
					break;
				}	
			}
			util.objectManager(validaDocButton, util.scrollAndClick);
			spinner.checkSpinners();
			TimeUnit.SECONDS.sleep(3);
			//Aspetta che l'elemento spinner scompaia
			util.waitUntilIsNotDisplayed(spinnerValidazioneDocumenti);
     		TimeUnit.SECONDS.sleep(5);
		}
		
		//Click su eventuali check box obbligatori da cliccare presenti in pagina, non presenti nelle dettaglio del documento da validare
		List<WebElement> numeCheckboxRequired= driver.findElements(requiredCheckbox);
		for (int j=0;j<numeCheckboxRequired.size();j++){
			numeCheckboxRequired.get(j).click();
		}
		
		util.waitAndGetElement(conferma).click();
		
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}
	
   public void validaDocProcessoSubentroBusiness() throws Exception {
		
       TimeUnit.SECONDS.sleep(5);
       util.objectManager(By.xpath("//div[@class='slds-col slds-no-flex  slds-container--right']//button[text()='Indietro']"), util.scrollToVisibility, true);
       TimeUnit.SECONDS.sleep(3);
       util.exists(documentsToValidate, 100);
		List<WebElement> numelem = driver.findElements(documentsToValidate);

		
		
		for ( WebElement i : numelem)
		{

			String attr = i.getAttribute("disabled");
		//	System.out.println(attr);
			if ( attr!=null && attr.contentEquals("true")) {
 
				continue;
			}

			i.findElement(By.xpath("..")).click();
			util.objectManager(startCheckList, util.scrollAndClick);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    	
			for (WebElement ii : driver.findElements(allfieldsToValidate))
			{
				List<WebElement> e = driver.findElements(By.xpath("//article[@id='elementi-checklist-card']//select [contains(@class,'sfdcField')]| //input [contains(@class,'sfdcField')]/ancestor::div[@id='checklistElementsBody']//label[contains(@class,'slds-required') and (text()='Cognome e Nome Intestatario' or text()='Data Requisiti Salvaguardia' or text()='Firma' or text()='Data Istanza Salvaguardia') ]"));
			    if (e.size()!=4) throw new Exception("non e' presente uno dei seguenti campi: Cognome e Nome intestatario;Data Requisiti salvaguardia;Firma; Data Istanza Salvaguardia");
			
				String type = ii.getAttribute("sfdctype");
//				System.out.println("Input type : " + type);
				switch (type) {

				case "PICKLIST":
					Select select = new Select(ii);
					select.selectByIndex(2);
					break;
				case "DATE":
					ii.sendKeys("27/02/3019");
					break;
				case "STRING":
					ii.sendKeys("12345678");
					break;
				case "BOOLEAN":
					ii.findElement(By.xpath("..")).click();
					break;
				}	
			}
			util.objectManager(validaDocButton, util.scrollAndClick);
			spinner.checkSpinners();
			TimeUnit.SECONDS.sleep(3);
			//Aspetta che l'elemento spinner scompaia
			util.waitUntilIsNotDisplayed(spinnerValidazioneDocumenti);
    		TimeUnit.SECONDS.sleep(5);
		}
		
		//Click su eventuali check box obbligatori da cliccare presenti in pagina, non presenti nelle dettaglio del documento da validare
		List<WebElement> numeCheckboxRequired= driver.findElements(requiredCheckbox);
		for (int j=0;j<numeCheckboxRequired.size();j++){
			numeCheckboxRequired.get(j).click();
		}
		
		util.waitAndGetElement(conferma).click();
		
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}
	public void chiudi() throws Exception{
		util.objectManager(chiudi, util.scrollToVisibility,true);
		util.objectManager(chiudi, util.scrollAndClick);
	}
	
	public void VerificaAppuntamento() throws Exception{
		util.objectManager(verificaAppuntamento, util.scrollToVisibility,true);
		util.objectManager(verificaAppuntamento, util.scrollAndClick);
	}

	public void Esci() throws Exception{
		util.objectManager(buttonEsci, util.scrollToVisibility,true);
		util.objectManager(buttonEsci, util.scrollAndClick);
		util.objectManager(buttonSi, util.scrollToVisibility,true);
		util.objectManager(buttonSi, util.scrollAndClick);
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}

	public void verificaValidaDocMultiELE(String docs) throws Exception 
	{
		
		util.getFrameActive();
        TimeUnit.SECONDS.sleep(2);
		verificaDocumenti(docs.split(";"));
		validaAllDocsMultiELE();
		driver.switchTo().defaultContent();
	
	}
	

	
	
	public void validaAllDocsMultiELE() throws Exception {
		
        TimeUnit.SECONDS.sleep(5);
        util.exists(allDocuments, 100);
		List<WebElement> numelem = driver.findElements(allDocuments);
		if(numelem.size()==0) throw new Exception("Non sono stati trovati documenti da validare");
	
		
		for ( WebElement i : numelem)
		{

			String attr = i.getAttribute("disabled");
			if ( attr!=null && attr.contentEquals("true")) {
  
				continue;
			}

			i.findElement(By.xpath("..")).click();
			util.objectManager(startCheckList, util.scrollAndClick);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			TimeUnit.SECONDS.sleep(1);
			Boolean isPresent = driver.findElements(titolo_edilizio_checkbox).size() > 0;
			if (isPresent.equals(true)) {
				driver.findElement(titolo_edilizio_checkbox).click();	
			}
			TimeUnit.SECONDS.sleep(1);
			isPresent = driver.findElements(doc_num_pag).size() > 0;
			if (isPresent.equals(true)) {
				util.sendText("Numero pagina documento", "3");	
			}
		
			TimeUnit.SECONDS.sleep(1);
			isPresent = driver.findElements(istanza_num_pag).size() > 0;
			if (isPresent.equals(true)) {
				util.sendText("Numero pagina Istanza", "3");	
			}
					
			
			for (WebElement ii : driver.findElements(fieldsToValidate))
			{
				
				
				
				String type = ii.getAttribute("sfdctype");
			
//				System.out.println("Input type : " + type);
				switch (type) {

				case "PICKLIST":
					Select select = new Select(ii);
					select.selectByValue("Istanza 326");;
					break;
				//case "DATE":
				//	ii.sendKeys("27/02/3019");
				//	break;
				//case "STRING":
					//ii.sendKeys("3");
					//break;
				case "BOOLEAN":
					ii.findElement(By.xpath("..")).click();
					break;
				}	
				
				
				
			}
			
						
			util.objectManager(validaDocButton, util.scrollAndClick);
			spinner.checkSpinners();
			TimeUnit.SECONDS.sleep(3);
			//Aspetta che l'elemento spinner scompaia
			util.waitUntilIsNotDisplayed(spinnerValidazioneDocumenti);
     		TimeUnit.SECONDS.sleep(5);
		}
		
		//Click su eventuali check box obbligatori da cliccare presenti in pagina, non presenti nelle dettaglio del documento da validare
		List<WebElement> numeCheckboxRequired= driver.findElements(requiredCheckbox);
		for (int j=0;j<numeCheckboxRequired.size();j++){
			numeCheckboxRequired.get(j).click();
		}
		
		util.waitAndGetElement(conferma).click();
		
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}
	
	

}



