package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class DettagliComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public By web = By.xpath("//span[text()='Origine caso']/following::div[1]//lightning-formatted-text[text()='Web']");
	public String date = "//span[text()='Data di creazione']/following::div[1]//lightning-formatted-text[contains(text(),'$DATE$')]";
	public By stato = By.xpath("//span[text()='Stato']/following::div[1]//lightning-formatted-text[contains(text(),'Chiuso')]");
	public By statoSecondario = By.xpath("//span[text()='Stato Secondario']/following::div[1]//lightning-formatted-text[contains(text(),'Ricevuto')]");
	
	public DettagliComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}


	public void verificaObbligatorietaBollettaWebPec() throws Exception{
        if(util.verifyExistence(By.xpath("//a[text()='Dettagli' and @id='detailTab__item']"), 60)) {
        	util.pressJavascript(By.xpath("//a[text()='Dettagli' and @id='detailTab__item']"));
        	
    		WebElement email=util.waitAndGetElement(By.xpath("//span[contains(text(),'Certificazione Obbligatoria Email') and @lightning-input_input]/ancestor::force-record-layout-item//span[@class='slds-checkbox_faux']/../../input"),true);
    		WebElement cellulare=util.waitAndGetElement(By.xpath("//span[contains(text(),'Certificazione Obbligatoria Cellulare') and @lightning-input_input]/ancestor::force-record-layout-item//span[@class='slds-checkbox_faux']/../../input"),true);

    		if(!email.isSelected()) throw new Exception("Nella sezione Dettagli - Dati di processo, non risulta fleggata la Certificazione Obbligatoria Email/PEC");
    		if(cellulare.isSelected()) throw new Exception("Nella sezione Dettagli - Dati di processo, contrariamente a quanto atteso risulta fleggata la Certificazione Obbligatoria Cellulare");

        }
        else throw new Exception("Impossibile accedere alla sezione DETTAGLI della richiesta");
        

	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}

}
