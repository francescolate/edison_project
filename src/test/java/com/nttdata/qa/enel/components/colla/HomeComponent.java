package com.nttdata.qa.enel.components.colla;

import java.util.NoSuchElementException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class HomeComponent {

	WebDriver driver;
	SeleniumUtilities util;
	public By pageTitle = By.xpath("//div[@id='mainContentWrapper']//div[@class='heading']/h1");
	public By services = By.xpath("//a[@id='servizi']//span[text()='Servizi']");
	public By supplies = By.xpath("//a[@id='idFornitura']");
	public By bills = By.xpath("//a[@id='bollette']");
	public By news = By.xpath("//a[@id='novitaSelfcare']");
	public By videoSpace = By.xpath("//a[@id='tutorialSelfcare']");
	public By yourRights = By.xpath("//a[@id='iTuoiDiritti']");
	public By account = By.xpath("//a[@id='accountSelfcare']");
	public By support = By.xpath("//a[@id='supporto']");
	public By findEnelSpace = By.xpath("//a[@id='negozioEnel']");
	public By exit = By.xpath("//a[@id='disconnetti']");
	public By esciButton = By.xpath("//div[@class='list-form-container']//button[@class='button_second']");
	public By customerNumberAttiva = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='details-container']/div[@class='dl']/span[4]/div[@class='dd']");
	public By viewYourBillsLuce = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='button-container']/a[1]");
//	public By supplyDetailsLuce = By.xpath("//div[@class='card forniture regolare'][2]//dd[@id='fornitura300001594']");
	public By supplyDetailsLuce = By.xpath("//span//dt[text()='Fornitura']//following::dd[text()='Attiva']");
	public By viewYourBillsSospesa = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='button-container']/a[1]");
	public By supplyDetailsSospesa = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='button-container']/a[2]");

	public By attiva = By.xpath("//div[@class='card forniture regolare']/div[@class='details-container']/dl[2]/span[3]/dd[@id='fornitura300001594']");
	public By Cessata = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='details-container']/dl[2]/span[3]/dd[@id='fornitura680661678']");
	public By ridotta = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/dl[2]/span[3]/dd[@id='fornitura824731773']");
	public By sospesa = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='details-container']/dl[2]/span[3]/dd[@id='fornitura855343208']");
	
	//public By supplyLabelLuce = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='details-container']/div[@class='dl']/span[3]/div[@class='dt']");
	public By supplyLabelLuce = By.xpath("//div[@class='card-heading']/h3[text()='Luce']");
	public By daPagare = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='details-container']/dl[@class='dl card-details']/span[2]/dl/dd/span[@id='855343208']");
	public By daPagareRidotta = By.xpath("//span[@id='824731773']");
	//public By regolare = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/div[@class='dl card-details']/span[2]/div[@class='dd']/span[@id='830838890']");
	public By regolare = By.xpath("//span[@id='781537765']");
	public By nomeFornituraGas = By.xpath("//div[@class='card-heading']/h3[@id='cardHeading0']");
	public By addressGasSupply = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/div[@class='card-heading']/p");
	public By gasSupplyStatus = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/div[@class='dl']/span[3]/div[@id='fornitura847868628']");
	public By viewYourBillsGas = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='button-container']/a[1]");
	public By supplyDetailsGas = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='button-container']/a[2]");
	public By gasSupplyLabel = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/div[@class='card-heading']/h3[@id='cardHeading0']");
	public By customerNumberGasSupply = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/div[@class='dl']/span[4]/div[@class='dd']");
	public By yourBillsTitle = By.xpath("//div[@class='text parbase']/div[@class='heading']/h1");
	public By yourBillsSubText = By.xpath("//div[@class='text parbase']/div[@class='heading']/p[2]");
	public By fornitureTitle = By.xpath("//div[@id='mainContentWrapper']/div[@class='heading']/h1");
	public By gas = By.xpath("//div[@class='heading supply']/h1[@id='fornitura01']");
	public By indirizzoGas = By.xpath("//section/div[@class='heading supply']/dl[@class='supply-details-heading']/span[1]/dd");
	public By customerNumberGas = By.xpath("//div[@class='card forniture regolare']/div[@class='details-container']/dl[@class='dl']/span[4]/dd[text()='847868628']");
	public By activateSupplyText1 = By.xpath("//div[@class='card']/div[@class='card-nuovaFornitura']/h3");
	public By activeSupplyText2 = By.xpath("//div[@class='card']/div[@class='card-nuovaFornitura']/p");
	public By seeOffers = By.xpath("//div[@class='card']//div[@class='button-container']/a[@class='button_second']/span[text()='VISUALIZZA LE OFFERTE']");
	public By viewYourBillsRidotta = By.xpath("//section[@id='lista-forniture']/div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div[@class='button-container']/a[1]");
	public By supplyDetailsRidotta = By.xpath("//section[@id='lista-forniture']/div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div[@class='button-container']/a[2]");
	public By yourActivity = By.xpath("//section[@id='activity']/div[@class='section-heading']/h2");
	public By yourActivitySubText = By.xpath("//section[@id='activity']/div[@class='section-heading']/p[2]");
	public By reducedOrSuspendedSupply = By.xpath("//div[@class='attivita_cessate_ridotte parbase']//h4[text()='Fornitura Ridotta/Sospesa']");
	public By reducedOrSuspendedSupplyContent = By.xpath("//div[@class='flex tab activityModel red-border']/span[2]/p");
	public By goDetails = By.xpath("//div[@class='flex tab activityModel red-border']/a[@class='activity goToDetail']");
	public By viewYourBillsLuceAttiva = By.xpath("//section[@id='lista-forniture']/div[@id='wrapper_modalforn']/div[@class='card forniture regolare']/div[@class='button-container']/a[1]");
	public By supplyDetailsLuceAttiva = By.xpath("//section[@id='lista-forniture']/div[@id='wrapper_modalforn']/div[@class='card forniture regolare']/div[@class='button-container']/a[2]");
	public By supplyLabelLuceAttiva = By.xpath("//div[@class='card-heading']/h3[@id='cardHeading0']");
	public String xpathAttivaStatus = "//dd[@id='fornitura@numeroCliente@']";
	public By attivaStatus = By.xpath("//dd[@id='fornitura@numeroCliente@']");
	public By supplyLabelRegoalre = By.xpath("//dd[text()='310504623']/ancestor::div[@class='details-container']//h3");
	public By viewYourBillsRegolare = By.xpath("//dd[text()='310504623']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[1]");
	public By supplyDetailsRegolare = By.xpath("//dd[@id='fornitura310504623']/../../../../div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By customerNumRegalore = By.xpath("//div[@class='card forniture regolare']/div[@class='details-container']/dl[@class='dl']/span[4]/dd[text()='310504623']");
	public By nomeFornituraRegalore = By.xpath("//dd[@id='fornitura310504623']");
	public By customerNum = By.xpath("//section/div[@class='heading supply']/dl[@class='supply-details-heading']/span[3]/dd");
	public By paymentStatus = By.xpath("//span[@id='310504623']");
	public By address = By.xpath("//dd[text()='310504623']/ancestor::div[@class='details-container']//p[1]");
	public By userIcon = By.xpath("//button[@id='user']/span[@class='dsc-icon-user']");
	public By userName = By.xpath("//span[@id='welcomeBox']/p[@id='welcomeUser']");
	public By downArrow = By.xpath("//span[@class='dsc-icon-chevron-down']");
	public By editProfile = By.xpath("//span[@id='welcomeBox']/nav[@id='msg-box']/ul/li[1]/a[@id='modificaprofilo']");
	public By goOut = By.xpath("//span[@id='welcomeBox']/nav[@id='msg-box']/ul/li[2]/a[@id='tr_disconnetti']");
	public By enelLogo = By.xpath("//a[@class='brand href-sf-prevent-default']/img");
	public By gestisciLeOreFree = By.xpath("//div[@class='button-container']/a[2]");
	public By detaglioFurnitura = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='button-container']/a[2]");
	public By detaglioFurnituraES = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][3]/div[@class='button-container']/a[2]");
	public By detaglioFurnitureluce = By.xpath("//div[@class='card forniture regolare'][2]/div[@class='button-container']/a[2]");
	public By detaglioFurnitura_310519430 = By.xpath("//dd[text()='310519430']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By detaglioFurnitura_310519430New = By.xpath("//dd[text()='310519430']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Gestisci fornitura')]");
	public By detaglioFurnitura_310507299 = By.xpath("//dd[text()='310507299']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By pageTitleResOld = By.xpath("//div[@id='mainContentWrapper']//h1[.='Benvenuto nella tua area privata']");
	public By pageTitleRes = By.xpath("//div[@id='mainContentWrapper']//h1[.='Benvenuto nella tua area privata']");
	public By pageTitleResNew = By.xpath("//div[@id='mainContentWrapper']//h1[contains(text(),'Benvenuto nella tua area privata')]");
	public By pageTitleResNew2 = By.xpath("//div[@id='mainProspect']//h1[contains(text(),'Benvenuto nella tua area privata')]");
	public By letueFurniture = By.xpath("//section[@id='lista-forniture']/div[@class='section-heading']/h2");
	public By letueFurnitureSubtext = By.xpath("//p[contains(text(),'Visualizza le informazioni')]");
	public By overLayClose = By.xpath("//button[@class='remodal-close popupLogin-close']");
	public By pageTitleSubText = By.xpath("//p[contains(text(),'In questa sezione')]");
	public By supplyStatus = By.xpath("//div[@class='card forniture regolare'][1]/div[@class='details-container']/div[@class='dl']/span/div[@class='dt']");
	public By areaClientiCasa = By.xpath("//*[contains(text(), 'Area Clienti Casa')]/following-sibling::*//a[text()='Entra']");
	public By areaClientiImpresa = By.xpath("//*[contains(text(), 'Area Clienti Impresa')]/following-sibling::*//a[text()='Entra']");

//	public By dettaglioFurniture_271 = By.xpath("//div[@class='card forniture regolare'][1]//a[1]");
	public By dettaglioFurniture_271 = By.xpath("//a[contains(text(),'Gestisci fornitura')]");
	public By dettaglioFurniture_272 = By.xpath("//div[@class='card forniture regolare'][1]//a[2]");
	
	public By supplyDetail_1Ridotta = By.xpath("//div[@class='card forniture regolare']");
	public By supplyDetail_1Cessata = By.xpath("//dd[@id='fornitura310488279']");
	public HomeComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyVisibility(by, 30)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	 
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	    
	    public void scrollComponent(By object) throws Exception {
			util.objectManager(object, util.scrollToVisibility, false);
		}
	    
	    public void checkForResidentialMenu(String property) throws Exception{
	    	
	    	util.waitAndGetElement(supplies);
	    	if(! property.contains(driver.findElement(supplies).getText()) && property.contains(driver.findElement(services).getText())
	    		&& property.contains(driver.findElement(bills).getText()) && property.contains(driver.findElement(news).getText())
	    		&& property.contains(driver.findElement(videoSpace).getText()) && property.contains(driver.findElement(yourRights).getText())
	    		&& property.contains(driver.findElement(support).getText()) && property.contains(driver.findElement(findEnelSpace).getText())
	    		&& property.contains(driver.findElement(exit).getText()) && property.contains(driver.findElement(account).getText()))
	    	
	    		throw new Exception ("In the page 'https://www-colla.enel.it/' Residential menu item is not displaying");
	    }
	    
	    public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
		
		public void checkSupplyStatus(By checkElement,String property) throws Exception{
			
			String supply = driver.findElement(checkElement).getText();
			
			if(!property.equals(supply)) 
				throw new Exception (" Supply staus"+ supply+" is not matching with the actual supply status "+property);
		}
		
		public void verifyCustomerNumber(By checkElement,String property) throws Exception{
			
			String customerNum = driver.findElement(checkElement).getText();
			
			if(! property.equals(customerNum))
				throw new Exception (" Supply staus"+ customerNum+" is not matching with the actual supply status "+property);

		}
		
		public void checkForSupplyColour(By checkElement) throws Exception{
			
			String color =  driver.findElement(checkElement).getCssValue("color");
			
			System.out.println("Colur "+color);
			
		}
		
		public void checkFieldValue(By checkElement,String property) throws Exception{
			
			String supply = driver.findElement(checkElement).getText();
			
			if(!property.equals(supply)) 
				throw new Exception (" Supply staus"+ supply+" is not matching with the actual supply status "+property);
			
		}
		
		public void billToBePaid(By checkElement , String property) throws Exception
		{
			String bill = driver.findElement(checkElement).getText();
			
			String [] array = bill.split("\\s");
			System.out.println(array[array.length-1]);
				if(array[array.length-1].equals(""))
					throw new Exception ("Bills to be paid is zero ");
			
		}

		public void navigateBack(By checkObject) throws Exception {
			
			driver.navigate().back();
			util.waitAndGetElement(checkObject);
			
		}

		public void checkForContent(By header,By text,String property) throws Exception {
			
			String title = driver.findElement(header).getText();
			String Subtext = driver.findElement(text).getText();
			
			if(! property.contains(title) && property.contains(Subtext))
				throw new Exception (" Text is not matching with actual data"+property);
			
		}

		public void checkFieldDisplay(By checkObject) throws Exception {
			
			Boolean isPresent = driver.findElements(checkObject).size() > 0;
			if(isPresent.equals("true"))
				throw new Exception(checkObject+"Field is displaying");
		}
		
		public boolean isElementExist(By locator)
	    {
	        try {
	            driver.findElement(locator);
	        } catch (NoSuchElementException e) {
	            return false;
	        }

	        return true;
	    }
		
		public void checkURLAfterRedirection(String url) throws Exception{		
	        if(!url.equals(driver.getCurrentUrl()))
	            throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	    }
		
		public void SwitchTabToCustomerArea() throws Exception
		{
		   
			Set <String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
			   // Switch to child window
				//System.out.println("switched to "+driver.getTitle()+"  Window");
		        String pagetitle = driver.getTitle();
			    driver.switchTo().window(winHandle);
			 }
			verifyComponentExistence(pageTitle);
			
			driver.close();
			driver.switchTo().window(currentHandle);
		}
		
		 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
				WebElement we = util.waitAndGetElement(by);
				String weText = we.getAttribute("innerHTML");
				if(nestedTags)
					weText = normalizeInnerHTML(weText);
				else
					weText = we.getText();
				System.out.println("Normalized:\n"+weText);
				System.out.println(text);
				if(!text.equals(weText))
					throw new Exception("Text mismatch. Text is not matching with the expected value");
			}
			
			public String normalizeInnerHTML(String s){
				int less = s.indexOf("<"); int greater = s.indexOf(">");
				if(less==-1)
					return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
				else{
					String tag = s.substring(less, greater+1);
					String endTag = null;
					if(!tag.contains("<br>"))
						endTag = tag.replace("<", "</");
					if(endTag!=null)
						return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
					else
						return normalizeInnerHTML(s.replace(tag, ""));
				}
			}
			
			public void checkForErrorPopup() {
			try {
				if(driver.findElement(By.xpath("//button[@class='remodal-close']")).isDisplayed())
					clickComponent(By.xpath("//button[@class='remodal-close']"));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		public static final String RESIDENTIALMENU = " Forniture | Bollette | Servizi | Novità | Spazio Video | Account | I tuoi diritti | Area Clienti Impresa | Supporto | Trova Spazio Enel | Esci ";
		public static final String PageTitle = "Benvenuto nella tua area privata";
		public static final String LetueFurnitureSubtext = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
		public static final String TitleSubText = "In questa sezione potrai gestire le tue forniture";
}
