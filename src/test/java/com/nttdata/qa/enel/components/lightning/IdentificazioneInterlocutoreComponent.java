package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class IdentificazioneInterlocutoreComponent {

	WebDriver driver;
	SeleniumUtilities util;
	Actions actions;
	SpinnerManager spinner;

	// public By podInput = new
	// By.ByXPath("//label[text()='POD/PDR']/parent::*/div/input");
	
	//MODIFICA 13-12-2019
	//public By podInput = new By.ByXPath("//label[text()='POD/PDR']/parent::*/div/input");
	
	public By podInput = new By.ByXPath("//span[text()='POD/PDR']/parent::*/following-sibling::div/input");
	
	
	public By indirizzoDiFornituraInput = new By.ByXPath("//div[@id='callerIdentBody']"
			+ "/..//span[text()='Indirizzo di fornitura']"
			+ "/../following-sibling::div/input");
//	
	//public By indirizzoDiFornituraInput = new By.ByXPath("//label[@id='ServiceAddressChangeIndirizzo']/.//input");
	
	public By documentoIdentita = new By.ByXPath("//span[contains(text(),'Nr. Documento identit')]/parent::*/following-sibling::div/input");
	
	public By newDocumento = By.xpath("//label[text()='Documento']/..//input");
	
	public By indirizzo = By.xpath("//label[text()='Indirizzo di fornitura']/..//input");
	
	public By NumeroCliente = By.xpath("//span[text()='Numero Cliente']/../..//input");
	
	public By confirmButton = new By.ById("cancelBotton");
	
	public By confermaDocumento = By.xpath("//h2[text()='Identificazione interlocutore']/ancestor::div[@role='region']//button[text()='Conferma']");
	
	public By conferma = By.xpath("//button[text()='Conferma']");
	
	public By buttonChecklis = By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma']");

	public By ImportoUltimaFattura = By.xpath("//span[text()='Importo ultima fattura']/../..//input");
	

	public IdentificazioneInterlocutoreComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		actions = new Actions(driver);
		spinner = new SpinnerManager(driver);
	
	}

	public void insertPod(String pod) throws Exception {
		//TimeUnit.SECONDS.sleep(20);
		spinner.checkSpinners();
		WebElement el = util.waitAndGetElement(this.podInput);
		util.scrollDown();
		actions.moveToElement(el).click().perform();
		TimeUnit.SECONDS.sleep(5);
		el.sendKeys(pod);
		
		
	}
	

	public void insertDocumento(String documento) throws Exception {
		//TimeUnit.SECONDS.sleep(20);
		spinner.checkSpinners();
		WebElement el = util.waitAndGetElement(this.documentoIdentita);
		util.scrollDown();
		actions.moveToElement(el).click().perform();
		TimeUnit.SECONDS.sleep(2);
		el.sendKeys(documento);
		spinner.checkSpinners();
		
	}
	
	public void insertDocumentoNuovoInterlocutore(String documento) throws Exception {
		//TimeUnit.SECONDS.sleep(20);
		spinner.checkSpinners();
		WebElement el = util.waitAndGetElement(this.newDocumento);
		util.scrollDown();
		actions.moveToElement(el).click().perform();
		TimeUnit.SECONDS.sleep(2);
		el.sendKeys(documento);
		spinner.checkSpinners();
		
	}
	
	public void insertIndirizzoInterlocutore(String documento) throws Exception {
		//TimeUnit.SECONDS.sleep(20);
		spinner.checkSpinners();
		WebElement el = util.waitAndGetElement(this.indirizzo);
		util.scrollDown();
		actions.moveToElement(el).click().perform();
		TimeUnit.SECONDS.sleep(2);
		el.sendKeys(documento);
		spinner.checkSpinners();
		
	}
	
	public void inserisciCodiceCliente(String codiceCliente) throws Exception {
		//TimeUnit.SECONDS.sleep(20);
		spinner.checkSpinners();
		WebElement el = util.waitAndGetElement(this.NumeroCliente);
		util.scrollDown();
		actions.moveToElement(el).click().perform();
		TimeUnit.SECONDS.sleep(5);
		el.sendKeys(codiceCliente);
		
	}

	public void pressButton(By button) throws Exception {
		TimeUnit.SECONDS.sleep(2);
//		WebElement el = util.waitAndGetElement(button);
////		util.scrollToElement(el);
//		el.click();
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void insertIndirizzoFornitura(By input,String address) throws Exception {
		//TimeUnit.SECONDS.sleep(10);
		spinner.checkSpinners();
		util.scrollToElement(driver.findElement(input));
		util.waitAndGetElement(input,20,1).click();
		util.waitAndGetElement(input).sendKeys(address);
	}

	public void insertImportoUltimaFattura(String documento) throws Exception {
		//TimeUnit.SECONDS.sleep(20);
		spinner.checkSpinners();
		WebElement el = util.waitAndGetElement(this.ImportoUltimaFattura);
		util.scrollDown();
		actions.moveToElement(el).click().perform();
		TimeUnit.SECONDS.sleep(5);
		el.sendKeys(documento);
		spinner.checkSpinners();
		
	}

	
}
