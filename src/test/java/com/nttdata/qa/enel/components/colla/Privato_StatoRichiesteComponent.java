package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Privato_StatoRichiesteComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By homepageDetail1 = By.xpath("//section[@id='content-home']//descendant::li[1]/a[.='Area riservata']");
	public By homepageDetail2 = By.xpath("//section[@id='content-home']//descendant::li[.='Homepage']");
	public By homepageLogo = By.xpath("//img[@id='avatarUtente']");
	public By homepageHeading = By.xpath("//section[@id='content-home']//h1[contains(text(),'Benvenuto')]");
	public By PageText = By.xpath("//div[@class='panel-body']//ol");
	public By pageText = By.xpath("//h1[text()='Stato Richieste']");
	public By statoText = By.xpath("//h1[text()='Stato Richieste']//following::p[1]");
	
	//public By  = By.xpath("//[@'class='btn btn-primary'][1]");
	
	public By Applica  = By.xpath("//button[@class='btn btn-primary'][1]");
	public By NascondiFiltri   = By.xpath("//button[@class='btn btn-primary'][1]");
	//NASCONDI FILTRI 
	public By Annulla = By.xpath("//[@class='step important past'][1]");
	
	public By tipologiaDropDown = By.xpath("//a[@class='customselect-results'][1]");
	
	public By tipologia = By.xpath("//a[@class='customselect-choice'][1]");
	
	
	//<a href="#" class="customselect-choice"><span class="customselect-choiceelement-empty">Seleziona</span></a>
	public By avanzamento = By.xpath("//a[@class='customselect-choice'][2]");
	public By anno = By.xpath("//a[@class='customselect-choice'][3]");
	public By avanzamentoDropDown = By.xpath("//a[@class='customselect-results'][2]");
	public By casa = By.xpath("//ul[@id='placeSelectBoxItOptions']//li//a[text()='Casa']");
	
	public By annoDropDown = By.xpath("//a[@class='customselect-results'][3]");
	
	//<div id="search" class="btn btn-primary">Applica</div>
	public By Stato_Richesthe = By.xpath("//a[text()='stato richieste']");
	public By Contratti = By.xpath("//a[text()='Contratti']");
	public By Servizi = By.xpath("//a[text()='Servizi']");
	public By Modifiche_Contatore = By.xpath("//a[text()='Modifiche contatore']"); 
	public By Mostra = By.xpath("//a[@name='Filtri']"); 
	
	public By openEnergyDigital = By.xpath("//h4[text()='Open_Energy Digital']"); 
	public By Tipovalue = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Tipo:']"); 
	public By TipoLuce = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span[text()='  Luce ']"); 
	public By numeroOfferta = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Numero Offerta:']"); 
	public By numeroOffertaValue = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span[text()='  SG3235321 ']");
	public By POD = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='  POD  ']"); 
	public By PODValue = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/span[text()='IT004E32838292']");
	public By DATARichiesta = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Data richiesta:']"); 
	public By Open_EnergyDigital = By.xpath("//h4[text()='Open_Energy Digital']/parent::div/parent::div/following-sibling::div/div/span/b[text()='Data richiesta:']");
	public By inviata = By.xpath("//li[@class='step important past'][1]");
	public By ESITO = By.xpath("//li[@class='Esito'][1]");
	public By PlusIcon = By.xpath("//span[@class='collapse-icon'][1]");
	
	public By numeroUtente = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Numero utente:']");
	public By indirizzo = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Indirizzo di fornitura:']");
	public By tipodiattivazione = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Tipo di attivazione:']");
	public By descrizionetipodiattivazione = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Descrizione tipo di attivazione:']");
	public By stato = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Stato:']");
	
	public By descrizionestato = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Descrizione stato:']");
	public By visualizzacontrattofirmato = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Visualizza contratto firmato:']");
	public By documentazionecontrattuale = By.xpath("//h5[text()='Dettaglio avanzamento attivazione fornitura']/parent::div/parent::div/following-sibling::div/div/p/span/b[text()='Documentazione contrattuale:']");
	
	public Privato_StatoRichiesteComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	

	 public void changeDropDownValue(By dropdown,By input) throws Exception {
		 util.scrollToElement(driver.findElement(dropdown));
			clickComponent(dropdown);
			clickComponent(input);
			
		}
	 
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public static final String PAGETEXT= "Area riservataStato Richieste";
	public static final String STATOTEXT = "In questa pagina puoi visionare lo stato di avanzamento delle tue richieste, in ogni momento.";
	public static final String HOMEPAGE_DETAIL1= "Area riservata";
	public static final String HOMEPAGE_PATH1New= "AREA RISERVATA";
	public static final String HOMEPAGE_DETAIL2= "Homepage";
	public static final String HOMEPAGE_SERVIZI_PATH2= "Servizi";
	public static final String HOMEPAGE_FORNITURE_PATH2= "Forniture";
	public static final String HOMEPAGE_FORNITURE_PATH2New= "FORNITURE";
	public static final String MODIFICA_TITTLE = "Modifica Addebito Diretto";
	public static final String HOMEPAGE_HEADING= "Benvenuto nella tua area dedicata Business";
	
}

