package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivatoBollettaWebID308Component extends BaseComponent{
	
	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By areaRiservata = By.xpath("//ol[@class='breadcrumb']");
	public By pageHeading = By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
	
	public By sectionSubheading = By.xpath("//h2[contains(text(),'Vantaggi per il tuo business')]");
	
	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
	public By serviziSelectMenu = By.xpath("//li/a[text()='servizi']");
	public By serviziPageTitle = By.xpath("//h1[text()='Servizi']");
	public By serviziPageTitleSubtext = By.xpath("//h4[text()='In questa pagina trovi tutti i servizi.']");
	public By serviziPageBolletteSection = By.xpath("//h2[text()='Servizi per le bollette']");
	public By Bolletta_Web_box = By.xpath("//p[contains(text(),'Bolletta Web')]/ancestor::div[@class='panel-body']");
//	public By Bolletta_Web_PageTitle = By.xpath("//h1[not(contains(@class,'title'))]");
	public By Bolletta_Web_PageTitle = By.xpath("//h1[contains(@class,'title')]");
//	public By Bolletta_Web_PageTitleSubtext = By.xpath("//h1[not(contains(@class,'title'))]/following-sibling::p");
	public By Bolletta_Web_PageTitleSubtext = By.xpath("//h1[contains(@class,'title')]/following-sibling::p");
	public By Attivazione = By.xpath("//b[text()='Attivazione']/parent::td/parent::tr");
	public By Modifica = By.xpath("//b[text()='Modifica']/parent::td/parent::tr");
	public By Revoca = By.xpath("//b[text()='Revoca']/parent::td/parent::tr");
	public By Attivazione_scopriDiPiu = By.xpath("//b[text()='Attivazione']/parent::td/parent::tr/td[position()=last()]/a");
	public By Modifica_scopriDiPiu = By.xpath("//b[text()='Modifica']/parent::td/parent::tr/td[position()=last()]/a");
	public By Revoca_scopriDiPiu = By.xpath("//b[text()='Revoca']/parent::td/parent::tr/td[position()=last()]/a");
	public By modificaBollettaWeb_button = By.xpath("//div[@id='bollWebAttivo']//a[@name='Modifica']");
	public By modificaBollettaWeb_Title = By.xpath("//h1[contains(text(),'Modifica')]");
	public By modificaBollettaWeb_TitleSubtext = By.xpath("//h1[contains(text(),'Modifica')]/following-sibling::p");
	
	public By indrizzoHeading = By.xpath("//div[@id='bollWebAttivo']//div[@class='panel-block-bordered']//div/b");
	public By indrizzoHeadingValue =  By.xpath("(//div[@id='bollWebAttivo']//div[@class='panel-block-bordered']//div/following-sibling::p)[1]");
	public By emailHeading	= By.xpath("//div[@id='bollWebAttivo']//div[@class='panel-block-bordered']//div/b[text()='Email']");		
	public By emailValue =  By.xpath("(//div[@id='bollWebAttivo']//div[@class='panel-block-bordered']//div/following-sibling::p)[2]");
			
	public By modificaIndrizzoEmailButton =  By.xpath("//a[text()='MODIFICA INDIRIZZO EMAIL']");
	public By Per_disattivareText =  By.xpath("//a[text()='clicca qui']/parent::p");
	public By CLICCA_QUI_Link =  By.xpath("//a[text()='clicca qui']");
	
	public By supplyActiveStatus = By.xpath("//b[text()='è attivo']/parent::p");
	public By supplyNonActiveStatus = By.xpath("//b[text()='non è attivo']/parent::p");
	
	public By fornitureSelect = By.xpath("//li/a[text()='fornitura']");
	public By dailogTitle = By.xpath("//h4[text()='Attenzione']");
	public By dailogText = By.xpath("//h4[text()='Attenzione']/following-sibling::p");
	public By Torna_al_processo_Button = By.xpath("//button[text()='Torna al processo']");
	public By Esci_Button = By.xpath("//div[text()='Esci']");
	public By Esci_ButtonNew = By.xpath("//a[text()='Esci']");
	public By EsciSicuro_ButtonNew = By.xpath("//*[@id='sendAbort']");
	public By fornitureSubText = By.xpath("//h1[contains(text(),'Tutto quello che c')]");
	public By subtitleFornitura = By.xpath("//p[@id='subtitleFornitura']");
	
	public By dettaglioFornitura = By.xpath("//dd[text()='Attiva']/ancestor::*/preceding-sibling::*//span[@class='icon-line-electricity']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
	public By dettaglioFornituraGas = By.xpath("//dd[text()='Attiva']/ancestor::*/preceding-sibling::*//span[@class='icon-line-flame']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
	public By Visualizza_le_bollette_button = By.xpath("(//dd[text()='Attiva']/ancestor::div[@class='details-container']/following-sibling::div/a[contains(text(),'Visualizza le bollette')])[1]");
	
	public By Bolletta_Web_box_Tile = By.xpath("//li[@class='tile']//h3[text()='Bolletta Web']/parent::div");
	public By Bolletta_Web_Title = By.xpath("//div[@class='section-heading']//h2"); 
	public By Bolletta_Web_Title_Subtext = By.xpath("//div[@class='section-heading']//p[contains(text(),'Il servizio Bolletta Web consente')]"); 
	public By EnelLogo = By.xpath("//img[@alt='logo enel']");
	public By WelcomeUsername = By.xpath("//p[@id='welcomeUser']");
	public By userDownArrow = By.xpath("//p[@id='welcomeUser']/following-sibling::span");
	public By fotterSection = By.xpath("//footer//div[@class='footer-info-container']/ul/li");
	public By ATTIVA_BOLLETTA_WEB_BUTTON = By.xpath("//span[text()='ATTIVA BOLLETTA WEB']/parent::button");
	
//	public By attivazioneBollettaWeb_Title = By.xpath("//h2[text()='Bolletta Web']");
	public By attivazioneBollettaWeb_Title = By.xpath("//h1[@id='h1_bollettaweb']");
//	public By attivazioneBollettaWeb_TitleSubtext = By.xpath("//h2[text()='Bolletta Web']/following-sibling::p[contains(text(),'Il servizio Bolletta Web')]");
	public By attivazioneBollettaWeb_TitleSubtext = By.xpath("//h1[@id='h1_bollettaweb']/following-sibling::h2[contains(text(),'Seleziona una o più forniture')]");
	public By attivazioneBollettaWeb_Subtext_MagentaCircle = By.xpath("//h1[@id='h1_bollettaweb']/following-sibling::a/span[@class='icon-info-circle']");
	public By attivazioneBollettaWeb_PopUpTitle = By.xpath("//h3[@id='titleModal']");
	public By attivazioneBollettaWeb_PopUpContent = By.xpath("//p[@class='inner-text']");
	
	public By disattivazioneBollettaWeb_Title = By.xpath("//h1[text()='Disattivazione']");
	public By disattivazioneBollettaWeb_TitleSubtext = By.xpath("//h1[text()='Disattivazione']/following-sibling::p");
	public By numeroClientie_851661167 = By.xpath("//td[text()='851661167']");
	public By Tipo_851661167 = By.xpath("//td[text()='851661167']/preceding-sibling::td[1]");
	public By indirizzo_fornitura_851661167 = By.xpath("//td[text()='851661167']/following-sibling::td");
	public By checkbox_851661167 = By.xpath("//td[text()='851661167']/preceding-sibling::td[3]");
	public By checkbox_Status_851661167 = By.xpath("//td[text()='851661167']/preceding-sibling::td[2]");
	public By SelezionaButton = By.xpath("//a[text()='Seleziona']");
	
	public By CurrentStep = By.xpath("//ul[@class='steps']/li[@class='current']/span[@class='text']");
	
	public By supplyDeactivateMessage = By.xpath("//h5[text()='Stai per disattivare il servizio su questa fornitura.']");
	public By supplyDeactivateConfirmingMessage = By.xpath("//p[text()='Sei sicuro di voler rinunciare ai vantaggi di Bolletta Web?']");
	public By Esci = By.xpath("//a[text()='Esci']");
	public By Indietro = By.xpath("//a[text()='Indietro']");
	public By Conferma = By.xpath("//button[text()='Conferma']");
	
	//h5[text()='Stai per disattivare il servizio su questa fornitura.']
	
	public static String Tipo ;
	public static String Numerocliente ;
	public static String IndirizzoForniture ;
	public static String pathHome ="Area riservataHomepage";
	
	// UDB
		public By idBpm = By.xpath("//input[@name='idBpm']");
		public By esito = By.xpath("//select[@name='esito']");
		public By submit = By.xpath("//input[@type='submit']");
		
	ColorUtils c = new ColorUtils();	
	
	public PrivatoBollettaWebID308Component(WebDriver driver)
	{
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText().replaceAll("(\r\n|\n)", " ");
		System.out.println(actualtext);
		System.out.println(Value);
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void verifyFooterVisibility() throws Exception {
			
			String[] footerValue = {"© Enel Energia S.p.a.","Tutti i Diritti Riservati","Gruppo IVA Enel P.IVA 15844561009","Informazioni Legali","Privacy","Credits","Contattaci"};
			List<WebElement> footer = driver.findElements(fotterSection);
			String footerText [] = new String[footer.size()];
			 
			for (int i = 0; i < footer.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//footer//div[@class='footer-info-container']/ul/li)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(footerValue[i])){
					throw new Exception("Footer values for "+i+" is not matching")	;		
					} 
			}
			
	}

	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		System.out.println("without Normalized:\n"+weText);
		System.out.println("without Normalized we.getText():\n"+we.getText());
		if(nestedTags)
		{
			weText = normalizeInnerHTML(weText);
		System.out.println("Normalized:\n"+weText);
		}
		else
			weText = we.getText();
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
 
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void validateSupply(By checkObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		verifyComponentExistence(checkbox_851661167);
		
		verifyComponentExistence(Tipo_851661167);
		Tipo = driver.findElement(Tipo_851661167).getText();
		VerifyText(Tipo_851661167, "Gas");
		
		verifyComponentExistence(numeroClientie_851661167);
		Numerocliente = driver.findElement(numeroClientie_851661167).getText();
		
		verifyComponentExistence(indirizzo_fornitura_851661167);
		IndirizzoForniture = driver.findElement(indirizzo_fornitura_851661167).getText();
						
		clickComponent(checkbox_851661167);
		Thread.sleep(5000);
		
//		checkColor(checkbox_Status_851661167, successColor, "CheckBoxStatus");
		 
	}
	
	public void confirmSupply(By checkObject, String tipoValue, String numeroclienteValue, String indirizzoFornitureValue) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String Supply_found = "NO";
		
		verifyComponentExistence(Tipo_851661167);
		verifyComponentExistence(numeroClientie_851661167);
		verifyComponentExistence(indirizzo_fornitura_851661167);
		
		if (driver.findElement(Tipo_851661167).getText().contentEquals(tipoValue)) {
			
			if (driver.findElement(numeroClientie_851661167).getText().contentEquals(numeroclienteValue)) {
				
				if (driver.findElement(indirizzo_fornitura_851661167).getText().contentEquals(indirizzoFornitureValue)) {
					
					Supply_found = "YES";
				}
				
			}
		}
		
		if (Supply_found.contentEquals("NO"))
			throw new Exception("The expected Supply is not found:");
	 
	}
	
	/*public void checkColor(By object, String color, String objectName) throws Exception{

		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));

		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
    }*/
	
	public static final String successColor = "Teal";
	
	public static final String popUpTitle = "Che cos'è la Bolletta Web?";
	public static final String popUpContent = "Il servizio di Bolletta Web ti permette di ricevere la bolletta direttamente sulla tua email. Durante l'attivazione ti verranno richiesti email e cellulare e se non presenti nei nostri sistemi, dovranno essere certificati.";
	
	public static final String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String DisattivazioneBollettaWeb_TitleSubtext2 = "Disattivando il servizio Bolletta Web, tornerai a ricevere le bollette in formato cartaceo.";
	
	public String activeSupplyStatus = "Il servizio Bolletta Web è attivo sulla fornitura:";

	public static final String supplyActiveText = "Il servizio Bolletta Web è attivo sulla fornitura:";
	public static final String IndrizzoTextValue = "VIA BACCIO DA MONTELUPO 204 - 50142 FIRENZE FI";
	public static final String EmailValue = "info@hotelalexfirenze.com";
	public static final String per_disattivareText = "Per disattivare il servizio, clicca qui.";

}
