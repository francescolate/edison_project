package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class OfferStatusVerifierComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	final By finalStatus = By.xpath("//span[@title='Stato']/following-sibling::div/div/span");
	final By numeroOfferta=By.xpath("//span[@title='Quote']/following-sibling::div/div/span");
	final By numeroOffertaDaRichiesta= By.xpath("//div[text()='Offerta']/ancestor::span[@class='slds-text-heading_small']/a");
	public final By tabDettagli=By.xpath("//a[@data-tab-value='detailTab' and @aria-selected='true']");
	public final By buttonRiepilogo=By.xpath("//button[@alt='Riepilogo']");
	public final By statoCVP=By.xpath("//span[text()='Stato CVP']/ancestor::div[contains(@class,'slds-hint-parent')]//slot/lightning-formatted-text");
	
	public OfferStatusVerifierComponent(WebDriver driver ) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	public String RecuperaNumeroOfferta() throws Exception {
		WebElement ele=util.waitAndGetElement(numeroOfferta);
		String nofferta=ele.getText();
		Logger.getLogger("").log(Level.INFO, "N offerta:"+nofferta);
		return nofferta;
	}
	
	public String RecuperaNumeroOffertaDaRichiesta() throws Exception {
		WebElement ele=util.waitAndGetElement(numeroOffertaDaRichiesta);
		String nofferta=ele.getText();
		Logger.getLogger("").log(Level.INFO, "N offerta:"+nofferta);
		return nofferta;
	}
	
	public String recuperaStatoCVP() throws Exception {
		WebElement ele=util.waitAndGetElement(statoCVP);
		util.scrollToElement(ele);
		String stato=ele.getText();
		Logger.getLogger("").log(Level.INFO, "Stato CVP:"+stato);
		return stato;
	}
	
	public void validateStatus(String status ) throws Exception {
		int tentativi = 7;
		int secondi = 30;
		int time = tentativi * secondi;
		boolean statoOk = false;
		String realStatus = "";
		while(tentativi-- > 0 && !statoOk) {
			realStatus = util.waitAndGetElement(finalStatus).getText();
			//System.out.println("Lo stato visualizzato è: "+realStatus);
			if(!realStatus.toLowerCase().contentEquals(status.toLowerCase())){
				TimeUnit.SECONDS.sleep(secondi);
				driver.navigate().refresh();
			}
			
			else statoOk=true;
			
			
		}
		
		if(!statoOk){
			throw new Exception("Dopo aver atteso "+time+" secondi lo stato della richiesta risulta:"+realStatus+". Lo stato atteso e':"+status);
		}
	
	
	}
	public void clickTabDettagli(By tab) throws Exception {
		util.objectManager(tab, util.click);
	}
	
	public void clickRiepilogo(By button) throws Exception {
//		String frame=util.getFrameByIndex(0);
//		util.frameManager(frame, button, util.click);
		util.getFrameActive();
		util.objectManager(button, util.click);
		driver.switchTo().defaultContent();
		spinner.checkSpinners();
		
	}
	
	
}
