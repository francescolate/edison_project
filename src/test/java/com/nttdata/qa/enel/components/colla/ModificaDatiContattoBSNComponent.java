package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ModificaDatiContattoBSNComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public List<WebElement> menuItemsList = null;

	public By logoButton = By.xpath("//div[@id='openModalUserBtn']");
	public By datiDiContattoButton = By.xpath("//a[text()='Dati di contatto']");
	public By panelBodyHeader = By.xpath("//div[@class='panel-body']");
	public By prosegui = By.xpath("//a[text()='Prosegui']");
	public By modificaButton = By.xpath("//a[text()='Modifica']");
	public By fineButton = By.xpath("//a[text()='Fine']");
	public By proseguiButton = By.xpath("//button[text()='Prosegui']");
	public By confermaButton = By.xpath("//button[text()='Conferma']");
	public By esciButton = By.xpath("//a[text()='Esci']");
	public By telefonoInput = By.xpath("//label[text()='Telefono']/following-sibling::input");
	public By emailInput = By.xpath("//label[text()='Email*']/following-sibling::input");
	public By requestSentHeader = By.xpath("//div[@class='panel panel-default panel-clear panel-steps']");
	public By requestSent = By.xpath("//div[@class='panel panel-default panel-clear']");
	public By loginBSNSuccessful= By.xpath("//h1[text()='Benvenuto nella tua area dedicata Business']");
	public By phoneErrorMessage = By.xpath("//label[@class='has-error']");
	
	public By pageTitle = By.xpath("//h1[text()='Modifica dati di contatto']");
	public By titleSubText = By.xpath("//div[@class='panel-body']//p[@class='lead']");
	public By inQuestasezione = By.xpath("//p[contains(text(),'In questa sezione sono riportati')]");
	public By path = By.xpath("//div[@class='panel-body']/ol");
	public String titolareInput = "//label[text()='Titolare']/following-sibling::input";
	public String cfInput = "//label[text()='Codice fiscale']/following-sibling::input";
	public String emailInput1 = "//label[text()='Email']/following-sibling::input";
	public String emailPECInput = "//label[text()='Email PEC']/following-sibling::input";
	public String telefonoInputString = "//label[text()='Telefono']/following-sibling::input";
	public String emailInputString = "//label[text()='Email*']/following-sibling::input";

	public By titleSubText1 = By.xpath("//p[contains(text(),'Completa i campi')]");
	public By formText = By.xpath("//p[contains(text(),'Di seguito sono')]");
	public By popupTitle = By.xpath("//h4[text()='Attenzione']");
	public By popupContent = By.xpath("//h4[text()='Attenzione']/following::p[1]");
	public By tornaAlProcesso = By.xpath("//button[@id='closeAbort']");
	public By popupEsci = By.xpath("//div[@id='sendAbort']");
	public By verificaICampi = By.xpath("//p[contains(text(),'Verifica i campi')]");
	public By operazioneText = By.xpath("//p[text()='Operazione eseguita correttamente']");
	public By messageOK = By.xpath("//p[@id='messageOK']");
	public By telefonoInput1 = By.xpath("(//input[@name='field-mono'])[1]");
	public String telefonoInputString1 = "(//input[@name='field-mono'])[1]";

	
	public static String modifiedExistingEmail = "";
	
	public By tipo = By.xpath("//tr//th[text()='Tipo']");
	public By stato = By.xpath("//tr//th[text()='Stato']");
	public By numeroCliente = By.xpath("//tr//th[text()='Numero cliente / Alias']");
	public By address = By.xpath("//tr//th[text()='Indirizzo fornitura']");
	public By tipoValue = By.xpath("//tr//td[text()=' Luce ']");
	public By statoValue = By.xpath("//tr//td[text()='Attivo']");
	public By numeroClienteValue = By.xpath("//tr//td[contains(text(),'Prova2BSN')]");
	public By addressValue = By.xpath("//tr//td//a");

    public ModificaDatiContattoBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 60))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner1();
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the expected text." + text);
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
	}
	
	public String jsGetInputValue(By by) throws Exception{
		return util.jsGetInputValue(by);
	}
	
	public String modifyExistingEmail(By by) throws Exception{
		Thread.sleep(5000);
		String s = util.jsGetInputValue(by);
		if(s.contains("1@") || s.contains("2@"))
			s = s.contains("1@") ? s.replace("1@", "2@") : s.replace("2@", "1@");
		else
			s = s.replace("@", "1@");
		changeInputText(by, "");
		WebElement we = util.waitAndGetElement(by);
		Thread.sleep(200);
		modifiedExistingEmail = s;
		we.sendKeys(s);
		return s;
	}
	
	public String modifyExistingPhone(By by) throws Exception{
		Thread.sleep(6000);
		String s = util.jsGetInputValue(by);
		int n = Integer.parseInt(s.substring(s.length()-1, s.length()));
		n = (n+1)%10;
		s = s.substring(0, (s.length()-1))+n;
		changeInputText(by, "");
		WebElement we = util.waitAndGetElement(by);
		Thread.sleep(200);
		we.sendKeys(s);
		return s;
	}
	
	public String modifyExistingPhoneWithAFakeOne(By by, String fake) throws Exception{
		Thread.sleep(6000);
		String s = util.jsGetInputValue(by);
		changeInputText(by, "");
		WebElement we = util.waitAndGetElement(by);
		Thread.sleep(200);
		we.sendKeys(fake);
		return s;
	}
	
	public void checkInputFieldValue(String xpath, String property) throws Exception{
        
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        System.out.println(s);
        if(!s.equals(property))
              throw new Exception (s+"Expected value is not matching with the test data"+property);
       }

	public static final String panelBodyHeaderText = "Area riservataModifica datiModifica dati di contattoIn questa sezione sono riportati i dati di contatto che ci hai comunicato. Se desideri modificarli, integrarli o sostituirli, inserisci le nuove informazioni nei campi corrispondenti.";
	public static final String requestSentHeaderText = "Area riservatafornituraModifica dati di contattoModifica dati di contattoOperazione eseguita correttamente 1I tuoi dati2Riepilogo e conferma dati3Esito";
	public static final String requestSentText = "La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve.";
	public static final String phoneErrorMessageText = "Numero di telefono non valido. Per proseguire, scrivi un numero di telefono fisso valido oppure lascia il campo vuoto.";
	public static final String TitleSubText = "Scegli il gruppo di forniture per il quale vuoi modificare i dati di contatto e clicca su PROSEGUI.In questa pagina, inoltre, puoi aggiungere o modificare il nome dei gruppi di forniture.";
	public static final String pagePath = "Area riservataModifica dati";
	public static final String confirmPagePath = "Area riservatafornituraModifica dati di contatto";
	public static final String TitleSubText1 ="Completa i campi sottostanti e clicca su prosegui";
	public static final String FormText = "Di seguito sono riportati i tuoi dati di contatto. Per aggiornarli clicca su \"modifica\".";
	public static final String PopupContent = "Se uscirai dal processo perderai le modifiche effettuate."; 
	public static final String VerificaICampi = "Verifica i campi sottostanti e clicca su conferma";
	public static final String MessageOK="La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve.";
	public static final String InQuestasezione_Text = "In questa sezione sono riportati i dati di contatto che ci hai comunicato. Se desideri modificarli, integrarli o sostituirli, inserisci le nuove informazioni nei campi corrispondenti.";

}
