package com.nttdata.qa.enel.components.colla;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateAreaBolletteMyComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;
	
	public By paymentStatusInVerificaOnCard = By.xpath("//dd[@class='dd']/span[@id='303103573']");
	public By supplyHeader = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='heading supply']/h1");
	public By gassupplyAddress = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='heading supply']/dl/span[dd][1]/dd");
	public By gasCustomerNumber = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='heading supply']/dl/span[dd][2]/dd");
	public By statoFornituraFeild = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='supply details-container']/dl/span[1]");
	public By statoFornituraValue = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='supply details-container']/dl/span[1]/dd");
	public By statoPagamentoFeild = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='supply details-container']/dl/span[2]");
	public By statoPagamentoValue = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='supply details-container']/dl/span[2]/dd");
	public By inVerfica = By.xpath("//div[@id='mainContentWrapper']/div/section/div[@class='supply details-container']/dl/span[2]/dd");
	//9b
	public By paymentStatusDaPagareOnCard = By.xpath("//dd[@class='dd']/span[@id='855343208']");
	public By gestisciLeOreFree8c = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][2]/div[@class='button-container']/a[2]");
	//9c
	public By paymentStatusRegolareOnCard = By.xpath("//dd[@class='dd']/span[@id='300001594']");
	public By buttonDettaglioFornituraLuce = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][2]/div[@class='button-container']/a[2]");
	//13
	public By bolletteLink = By.xpath("//nav/ul/div/li[@id='menuBoll']/a"); 
	public By bolletteMenu = By.xpath("//nav/ul[1]/li[@id='menuBoll']/a");
	public By bollettePath = By.xpath("//section[@id='filterFornBoll']/div/div/ol[@class='breadcrumb']");
	public By bolletteHeader1 = By.xpath("//section[@id='filterFornBoll']/div/div[@class='panel-body']/h1");
	public By bolletteTitle = By.xpath("//section[@id='filterFornBoll']/div/div/p[@class='lead']");
	public By bolletteClienteNFeild = By.xpath("//div[@class='customselect-container']/div[@class='customselect-drop']/div[@class='customselect-search']/input"); //input[@placeholder='Inserisci N° cliente/indirizzo di fornitura']
	//public By bolletteClientNFieldNew = By.xpath("//div[@class='customselect-container']/div[@class='customselect-drop']/div[@class='customselect-search']/input/../../preceding-sibling::a");
	public By bolletteClientNFieldNew = By.xpath("//section[@id='filterFornBoll']/div[2]/div[contains(@class,'panel-body')]//div[@class='customselect-drop']/div[@class='customselect-search']/input");
	public By waitForList = By.xpath("//section[@id='filterFornBoll']/div[2]/div[contains(@class,'panel-body')]//ul");
	public By buttonApplica = By.xpath("//div[@class='row-btns']/a[@class='select btn btn-primary btn-fuchsia']");
	public By applica = By.xpath("//a[.='Applica']");
	public By addressList = By.xpath("//div[@class='row']/div[@class='col-xs-12']/div[@class='form-group form-btn-right margin-bottom-15']/div/div[@class='customselect']/div[@class='customselect-container']/div[@class='customselect-drop']/ul[@class='customselect-results']/li/label/span");
	public By errorText = By.xpath("//div[@id='empty-choose-err']/div[@class='panel panel-default panel-dialog no-padding']/div[@class='panel-body gray']/div[@class='row']/div[@class='col-xs-12']/div[@class='custom-ico warning']/span/b");
	public By vediBollette = By.xpath("//table[@class='table']/tbody/tr[@class='fornListBoll']/td[5]/a[@class='fornDetail']");
	public By myTable = By.xpath("//table[@class='table']");
	public By billPath = By.xpath("//ol[@class='breadcrumb']/li/a");
	
	public By billHeader = By.xpath("//*[@id='fornDescription']/div[1]/div/h1");
	public By billTitle = By.xpath("//section[@id='fornDescription']/div[@class='panel panel-default panel-clear']/div[@class='panel-body']/p[@class='lead'][1]");
	public By billSubTitle = By.xpath("//p[@id='inviaDocumentiLab']");
	public By addressOption1 = By.xpath("//div[@class='row']/div[@class='col-xs-12']/div[@class='form-group form-btn-right margin-bottom-15']/div/div[@class='customselect']/div[@class='customselect-container']/div[@class='customselect-drop']/ul[@class='customselect-results']/li[1]/label/span");
	public By commonArea = By.xpath("//section[@id='filterFornBoll']");
	
	//38
	public By homePageLogo = By.xpath("//div[@id='openModalUserBtn']");
	public By modificaLink = By.xpath("//div[@id='modalUser']/div[@class='modal-dialog']/div[@class='modal-content']/div[@class='modal-body']/p");
	public By datiDiContatto = By.xpath("//a[@id='modificaProfiloLink']");
	public By ilTuoProfilo = By.xpath("//div[@id='modalUser']/div[@class='modal-dialog']/div[@class='modal-content']/div[@class='modal-body']/div[@class='row row-btns']/a[@class='btn btn-primary btn-bordered btn-fuchsia #cta_profilo']");
	public By popupText = By.xpath("//form[@id='dragprofile']/div[@class='dz-default dz-message']/span");
	public By seleziona = By.xpath("//div[@class='upload-btn-wrapper']/input[@id='myFile']");
	//39
	public By contactValues = By.xpath("//div[@class='row']/div/div/input");
	public By contactFeilds = By.xpath("//div[@class='row']/div/div/label");
	public By editContactPath = By.xpath("//div[@class='panel-body']/ol");
	public By editContactTitle = By.xpath("//div[@class='panel-body']/h1");
	public By editContactSubTitle = By.xpath("//div[@class='panel-body']/p");
	public By sectionTitle = By.xpath("//form[@class='formacb']/div/div[1]");
	public By contactDetailValues = By.xpath("//div[@class='row']/div/div[@class='form-group']/input");
	public By contactDetailsFeilds = By.xpath("//div[@class='row']/div/div[@class='form-group']/label");
	public By sectionDescription = By.xpath("//div[@class='row']/div[@class='col-xs-12'][2]/p[@class='lead']");
	public By buttonModifica = By.xpath("//div[@class='row-btns']/a[@class='btn btn-primary href-sf-prevent-default']");
	public By sectionTitle1 = By.xpath("//div[@class='row']/div/p[@class='leadbig margin-top-15 margin-bottom-30']");
	public By sectionTitle2 = By.xpath("//div[@class='row']/div[@class='col-xs-12']/p[2]");
	//40
	public By credenzialiDiAccessoHeading = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='cmText2 cmMarginYlg']");
	public By email = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][1]/div/label");
	public By emailValue = By.xpath("//input[@id='emailUsername']");
	public By customerDataFields = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div/label");
	public By customerDataValues = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div/input");
	public By infoIconCircleCellulare = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div[3]/span[@class='dsc-icon-info-circle']");
	public By popUpHeader = By.xpath("//div[@class='modal-dialog']/div[@class='modal-header']/h3/p");
	public By popUpTitle = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/p[a]");
	public By popUpclose = By.xpath("//div[@class='modal-header']/span[@class='dsc-icon-close']");
	public By anagraficaHeading = By.xpath("//form[@id='modificaAnagrafica-form']/div[@id='anagraficaAziendaTitle']");
	public By companyDataFields = By.xpath("//form[@id='modificaAnagrafica-form']/div[@id='anagraficaAziendaSection']/div/label");
	public By acceptTerm = By.xpath("//div[@id='generatedConsents']/div[1]/div[@class='uIdcheckbox-container']/div");
	public By acceptTerm2 = By.xpath("//div[@id='generatedConsents']/div[2]/div[@class='uIdcheckbox-container']/div");
	public By XAPTH_CHKBOX1 =By.xpath("//div[@id='generatedConsents']/div[1]/div[@class='uIdcheckbox-container']/div/input[@type='checkbox']");
	public By XPATH_CHKBOX2=By.xpath("//div[@id='generatedConsents']/div[2]/div[@class='uIdcheckbox-container']/div/input[@type='checkbox']");
	public By informativaPrivacyLink = By.xpath("//div[@class='informative-new'][1]//div/label[@id='onlytext']");
	public By condizioniLink = By.xpath("//div[@class='informative-new'][2]//div/label[@id='onlytext']");
	public By infoPopupHeading = By.xpath("//div[@class='row']/div/div[1]/div");
	public By infoPopupHeading1 = By.xpath("//div[@class='cmModalText cmText5']/h2[1]/b");
	public By infoPopupHeading2 = By.xpath("//div[@class='cmModalText cmText5']/h2[2]/b");
	public By infoPopupHeading3 = By.xpath("//div[@class='cmModalText cmText5']/h2[3]/b");
	public By largerText = By.xpath("//div[@class='cmModalText cmText5']/h2[3]/b");
	public By serviziMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Servizi']/parent::a");
	public By bolletteMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Bollette']/parent::a");
	public By bolletteHeader = By.xpath("//h1[text()='Le tue bollette']");
	public By bolletteHeaderText = By.xpath("//div[@class='heading']//p[contains(text(), 'Qui potrai')]");
	public By tilesHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public String orderedList = "//*[text()='$tileHeader$']/ancestor::section//li[contains(@class,'tile') and not(@style)]//h3/ancestor::a";
	public String tile = "//li[contains(@class,'tile')]//h3[text()='$tileName$']/ancestor::a";
	public By bollettaSintesi = By.xpath("//li[contains(@class,'tile')]//h3[text()='Bolletta di Sintesi o di Dettaglio']/ancestor::a");
	public By bollettaSintesiHeaderText = By.xpath("//div[@class='section-heading']");
	public By accountAddressLuce = By.xpath("//span[@class='icon-line-electricity']/../following-sibling::span[@class='indirizzo']/span[@class='valore']");
	public By customerNumberLuce = By.xpath("//span[@class='icon-line-electricity']/../following-sibling::span/span/span[@class='valore']");
	public String tipologiaBollettaButton = "//button/span[text()='$tipologiaBollettaButton$']";
	public By buttonEsci = By.xpath("//button[text()='ESCI']");
	public By buttonDettaglioFornitura = By.xpath("//div[@class='details-container']//dd[text()='Attiva']/ancestor::div[@class='card forniture regolare']//a[contains(text(),'Dettaglio fornitura')]");
	public By serviziPerLeBolletteHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public By serviziPerLeBolletteHeaderText = By.xpath("//h2[text()='Servizi per le bollette']/ancestor::div[@class='section-heading']");
	public By accountAddressGas = By.xpath("//span[@class='icon-line-flame']/../following-sibling::span[@class='indirizzo']/span[@class='valore']");
	public By customerNumberGas = By.xpath("//span[@class='icon-line-flame']/../following-sibling::span/span/span[@class='valore']");
	
    public PrivateAreaBolletteMyComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void clickBolletteClientField( By clickObject) throws Exception 
	  {  
	  	   WebDriverWait wait = new WebDriverWait(driver, 15);
	   wait.until(ExpectedConditions.visibilityOfElementLocated(clickObject));
	   wait.until(ExpectedConditions.elementToBeClickable(clickObject));
	  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(clickObject));
	   	Thread.sleep(500);
	  	driver.findElement(clickObject).click();
	 		  
	  }
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text '"+text+"' is not equal to the one within bolletteHeaderText item '"+weText+"'.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyMenuItemsPresenceAndOrder(String menuItemsString, String tileHeader) throws Exception {
		util.verifyVisibility(By.xpath("//*[@id='spinner'])"), 10, true);
		populateMenuItemsVector(menuItemsString);
		By orderedListXPath = By.xpath(this.orderedList.replace("$tileHeader$", tileHeader));
		List<WebElement> orderedWebElements = util.waitAndGetElements(orderedListXPath);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
		if(orderedWebElements.size()!=menuItemsList.size())
			throw new Exception("actual size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
		for(int i=0;i<orderedWebElements.size();i++){
			if(!orderedWebElements.get(i).equals(menuItemsList.get(i)))
					throw new Exception("the item you're looking for is different from the one in the list at position "+i+".");
		}
	}
	
	private void populateMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(tile.replace("$tileName$", item))));
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		System.out.println(util.waitAndGetElement(object,true).getCssValue("color"));
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
		System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}	
	
	public void checkMenuPath(String path, By existingObject) throws Exception {
		WebElement obj = util.waitAndGetElement(existingObject);
		String pathDescription=obj.getText(); 
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	

		if (!pathDescription.equalsIgnoreCase(path))
			throw new Exception("object path with xpath " + path + " is not exist.");
	}
	
	public void verifyFeildAttribute(By existingObject, String text) throws Exception {
		String actualText = driver.findElement(By.xpath("//div[@class='customselect-search']/input")).getAttribute("placeholder");
		if (!actualText.equalsIgnoreCase(text))
			throw new Exception("object with text " + text + " is not exist.");
	}
	
	public void enterInputtoField(By checkObject, String property) throws Exception {			
				util.objectManager(checkObject, util.sendKeys, property);
	}
	
	public void clearInputFromField(By checkObject) throws Exception {
		driver.findElement(checkObject).clear();
}
			
	  	
	public void checkForLiftTypeDropDownValue(By checkObject) throws Exception{
		String[] exp = {"Via Lamarmora 31 20122 Milano Mi","Via Lamarmora Alfonso 31 20122 Milano Mi","fornitura gas mia","778186188","780414414"};
		driver.findElement(By.xpath("//div[@class='customselect']")).click();
		List <WebElement> list  = driver.findElements(addressList);
			for (WebElement we : list) {
	    	//System.out.println(we.getText());
	    	if(!Arrays.asList(exp).contains(we.getText() ))
	    		throw new Exception ("The list of addresses are not available to the customer");
	       	    } 
	}
	
	public void verifyContactDetails(By checkObject, String[] Feild, String[] Value) throws Exception{
		
			List <WebElement> list  = driver.findElements(contactDetailsFeilds);
					
			HashMap<String, String> hm = new HashMap<String, String>();
			HashMap<String, String> hmActual = new HashMap<String, String>();
			String key, val ;
			
			for (int i=1; i<=list.size();i++)
			{
				key = driver.findElement(By.xpath("//div[@class='row']/div[@class='col-xxs-12 col-xs-6']["+ i +"]/div[@class='form-group']/label")).getText();
				val = driver.findElement(By.xpath("//div[@class='row']/div[@class='col-xxs-12 col-xs-6']["+ i +"]/div[@class='form-group']/input")).getAttribute("value");
				hm.put(key, val);
				}
			for (int i=0; i<Feild.length;i++){
			hmActual.put(Feild[i], Value[i]);}
			if(!hm.equals(hmActual))
	    		throw new Exception ("The contact details are not correctly displayed");
			}
	
	public void verifyEmailData(By checkObjectFeild,String xpath, String Feild, String Value) throws Exception{
		 WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        
      	HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmExpctd = new HashMap<String, String>();
		hmExpctd.put(Feild, Value);
		
			String key, expValue ;
			key = driver.findElement(checkObjectFeild).getText();
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        expValue = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			hm.put(key, expValue);
		
		if(!hm.equals(hmExpctd))
   		throw new Exception ("The contact details are not correctly displayed");
		}	
	
	public void verifyCustomerData(By checkObjectFeild,String xpath, String Feild[], String Value[]) throws Exception{
		 WebDriverWait wait = new WebDriverWait(this.driver, 50);
         wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
         
        List <WebElement> list  = driver.findElements(checkObjectFeild);
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmExpctd = new HashMap<String, String>();
		
		for (int i=0; i<Feild.length;i++){			
		hmExpctd.put(Feild[i], Value[i]);}
		
		for (int i=1; i<=list.size();i++)
		{
		String key, expValue ;
			key = driver.findElement(By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div["+ i +"]/label")).getText();
			xpath = "//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div["+ i +"]/input";
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        expValue = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			hm.put(key, expValue);
		}	
		if(!hm.equals(hmExpctd))
    		throw new Exception ("The contact details are not correctly displayed");
		}	
	
	public void verifyCompanyData(By checkObjectFeild,String xpath, String Feild[], String Value[]) throws Exception{
		 WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        
       List <WebElement> list  = driver.findElements(checkObjectFeild);
		HashMap<String, String> hm = new HashMap<String, String>();
		HashMap<String, String> hmExpctd = new HashMap<String, String>();
		
		for (int i=0; i<Feild.length;i++){			
		hmExpctd.put(Feild[i], Value[i]);}
		
		for (int i=1; i<=list.size();i++)
		{
		String key, expValue ;
			key = driver.findElement(By.xpath("//form[@id='modificaAnagrafica-form']/div[@id='anagraficaAziendaSection']/div["+ i +"]/label")).getText();
			xpath = "//form[@id='modificaAnagrafica-form']/div[@id='anagraficaAziendaSection']/div["+ i +"]/input";
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        expValue = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			hm.put(key, expValue);
		}	
		if(!hm.equals(hmExpctd))
   		throw new Exception ("The contact details are not correctly displayed");
		}
	public void verifyCheckBoxIsChecked(By checkObject, String text, By Xpath ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		WebElement chkbxtxt = driver.findElement(checkObject);
		WebElement chkbox = driver.findElement(Xpath);
		String description = chkbxtxt.getText();
		if(!(chkbox.isSelected())&& description.contentEquals(text))
		throw new Exception ("The check box is not checked and the correct text is not displayed");
		
	}
	
	public void clickOnTheInputFeild(By checkObject) throws Exception{
		 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		driver.findElement(By.xpath("//div[@class='customselect-search']/input")).click();
	}
	
  public void verifySupplyDisplay(By checkObject) throws Exception {
  WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		WebElement mytable = driver.findElement(By.xpath("//table[@class='table']"));
		List < WebElement > rows_table = mytable.findElements(By.xpath("//table[@class='table']/tbody"));
		int rows_count = rows_table.size();
									
		for (int row = 0; row < rows_count; row++)
			{
				String description = rows_table.get(row).getText();
				if(!description.contentEquals("Gas ATTIVO fornitura gas mia\nVia Lamarmora 31 20122 Milano Mi\nVEDI BOLLETTE"))
				throw new Exception("The supply details displayed are incorrect. actual value ="+description+" expected value=" +"Gas ATTIVO fornitura gas mia\nVia Lamarmora 31 20122 Milano Mi\nVEDI BOLLETTE");
			}
		 }
  
  public void UploadDocumentsComponent(By checkObject) throws Exception {
	  WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		WebElement upload = driver.findElement(By.xpath("//div[@class='upload-btn-wrapper']/input[@id='myFile']"));
		upload.sendKeys("C:Users/Administrator/Image/Image.JPG");
		
	}
    
  public void jsClickElement(By xpath){
		String itemXpath = xpath.toString().replace("By.xpath: ", "");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("(document.evaluate(\""+itemXpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
	}
  
  public void waitForElementToDisplay (By checkObject) throws Exception{
		
		WebDriverWait wait = new WebDriverWait (driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
	}
  
  public void buttonClickRobot(By checkObject) throws Exception{
	  Robot robot = new Robot();
	  robot.mousePress(InputEvent.BUTTON1_DOWN_MASK); 
	  robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
  }
  
  public void refreshPage(){
	  driver.navigate().refresh();
  }
  
	public static final String SUPPLY_HEADER = "Gas";
	public static final String SUPPLY_HEADER_LUCE = "Luce";
	public static final String BOLLETTEPATH = "AREA RISERVATA BOLLETTE";
	public static final String ERROR_TEXT_MESSAGE = "Per effettuare una ricerca ricordati di inserire il tuo numero cliente o un indirizzo di fornitura.";
	public static final String POPUP_TEXT = "Seleziona o trascina qui i file dal tuo computer tramite le cartelle del tuo pc";
	public static final String text = "Inserisci N° cliente/indirizzo di fornitura";
	public static final String BILL_TITLE = "Se cerchi una bolletta in particolare usa i filtri per trovarla più facilmente.";
	public static final String BILL_SUBTITLE = "Ci sono bollette scadute. Pagale comodamente on line. Se richiesto, in seguito puoi inviare il dimostrato pagamento cliccando su \"invia documenti\".";
	                                           
	public static final String[] expCustValue = {"IDEAL COLOR SNC","03430350177","+390818890127","fabiana.manzo32@nttdata.com","idealcolorsnc@comunicapec.it"};
	public static final String[] expCustFeild = {"Titolare","Codice fiscale","Telefono","Email","Email PEC"};
	public static final String EDIT_CONTACT_PATH = "AREA RISERVATA MODIFICA DATI";
	public static final String EDIT_CONTACT_TITLE ="Modifica dati di contatto";
	public static final String EDIT_CONTACT_SUBTITLE ="In questa sezione sono riportati i dati di contatto che ci hai comunicato. Se desideri modificarli, integrarli o sostituirli, inserisci le nuove informazioni nei campi corrispondenti.";
	public static final String SECTION_TITLE = "Aggiorna i tuoi dati";
	public static final String SECTION_DESCRIPTION = "Di seguito sono riportati i tuoi dati di contatto. Per aggiornarli clicca su \"modifica\".";
	public static final String SECTION_TITLE1 = "Comunica il tuo consenso";
	public static final String SECtiON_TITLE2 = "Vuoi essere sempre informato sulle nuove offerte di Enel Energia e dei suoi partner? Lascia il tuo consenso per scoprire tutti i vantaggi a te riservati!";
	public static final String ACCESSO_HEADING ="Credenziali di accesso";
	public static final String EMAIL = "Indirizzo e-mail";
	public static final String EMAIL_VALUE = "immilezell-1212@yopmail.com";
	public static final String[] expCustDataFeild = {"Nome*","Cognome*","Cellulare*","Codice Fiscale*"};
	public static final String[] expCustDataValue ={"test","collaudo","3394956806","03430350177"};
	public static final String POPUP_HEADING = "Modifica numero di cellulare";
	public static final String POPUP_TITLE = "Potrai modificare in ogni momento il numero di cellulare inserito seguendo le indicazioni riportate qui";
	public String XPATH_EMAIL_VALUE = "//div[@class='row'][1]/div[@class='form-group col-xs-12 col-sm-12']/input[@id='email']";
	public String XPATH_CUSTOMER_DATA_VALUES = "//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div/input";
	public static final String ANAGRAFICA_HEADING = "Anagrafica aziendaAnagrafica azienda";
	public static final String[] anagraficaExpCustDataField = {"Denominazione Azienda","Partita IVA","Codice fiscale"};
	public static final String[] anagraficaExpCustDataValue ={"IDEAL COLOR SNC","03430350177","03430350177"};
	public String XPATH_COMPANY_DATA_VALUES = "//form[@id='modificaAnagrafica-form']/div[@id='anagraficaAziendaSection']/div/input";
	public static final String ACCEPT_TERMS_TEXT= "Ho letto e accettato: Informativa privacy *";
	public static final String ACCEPT_TERM_TEXT2 ="Ho letto e accettato: Condizioni generali dei servizi offerti *";
	public static final String INFOPRIV_HEADING = "Informativa privacy";
	public static final String INFOPRIV_HEADING1 = "Portale ENEL ENERGIA";
	public static final String INFOPRIV_HEADING2 = "INFORMATIVA PRIVACY";
	public static final String INFOPRIV_HEADING3 = "AI SENSI DELL’ART. 13 DEL REGOLAMENTO UE 2016/679 (\"GDPR\")";
	public static final String LARGER_TEXT = "";
	
	
	
	
	
	
	public static final String BOLLETTE_HEADER = "Le tue bollette";
	public static final String BOLLETTE_HEADER_TEXT = "Qui potrai visualizzare bollette e/o rate delle tue forniture.";
	public static final String SERVIZI_BOLLETTE = "Bolletta Web;Pagamento Online;Rettifica Bolletta;Rateizzazione;Bolletta di Sintesi o di Dettaglio;Invio Attestazione di Pagamento";
	public static final String TILE_HEADER = "Servizi per le bollette";
	public static final String BOLLETTA_SINTESI_HEADER_TEXT = "Modifica Tipologia Bolletta Scegli se ricevere la Bolletta di Sintesi dove troverai le principali voci di spesa ed i consumi oppure la Bolletta di Dettaglio dove sarà possibile approfondire le singole voci elementari di spesa. La modifica riguarderà solo la ricezione della bolletta via mail oppure cartacea se non hai attiva la bolletta web, mentre scaricando il PDF visualizzerai comunque la versione completa della tua bolletta, comprensiva di sintesi e dettaglio.";
	
	
	     
}
