package com.nttdata.qa.enel.components.lightning;

import java.awt.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

//Il seguente componente si occupa di effettuare l'inserimento del POD per lo SWA-EVO
//specificando 

public class GestionePaginoneSwaEvo {
    private WebDriver driver;
    private SeleniumUtilities util;
    private SpinnerManager spinnerManager;
    
    public GestionePaginoneSwaEvo(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
    util = new SeleniumUtilities(driver);
    spinnerManager = new SpinnerManager(driver);
  }
    
    
    By insertPOD = By.xpath("//div[@class='active hasFixedFooter oneWorkspace']//section[@class='tabContent active oneConsoleTab']//input");
    //   By selectTipoDocumento = By.xpath("//label[contains(text(),'Tipo documento')]/..//select");
    //By insertRegione = By.xpath("//label[contains(text(),'Numero documento')]/..//input");
    By insertProvincia = By.xpath("//label[contains(text(),'*Provincia')]/..//input");
    By insertComune = By.xpath("//label[contains(text(),'*Comune')]/..//input");
    By insertIndirizzo = By.xpath("//label[contains(text(),'*Indirizzo')]/..//input");
    By insertCivico = By.xpath("//label[contains(text(),'*Numero Civico')]/..//input");

    By insertUso = By.xpath("//label[contains(text(),'Uso')]/..//input");
    By insertResidente = By.xpath("//label[contains(text(),'Residente')]/..//input");
    By insertSocietaVendita = By.xpath("//label[contains(text(),'Attuale società di vendita')]/..//input");
    By insertTitolarita = By.xpath("//label[contains(text(),'Titolarità')]/..//input");

    //    public By confermaPodButton = By.xpath("//button[text()='Conferma Pod/Pdr'][1]");
    //    public By confermaPodButton = By.xpath("//button[text()='Conferma Pod/Pdr']");
    public By confermaPodButton = By.xpath("//button[contains(text(),'Conferma Pod/Pdr')]");

    public By confermaPodButton1 = By.xpath("/html/body/div[4]/div[1]/div[2]/div[2]/div/div[2]/div/section[2]/div/div[2]/div/div/article/div[1]/div[4]/article/div[1]/div/div[1]/div/div/div[1]/article/div[2]/div/button");
    public By verificaIndirizzoButton = By.xpath("//button[text()='Verifica'][1]");
    public By confermaDatiSitoButton = By.xpath("//button[text()='Conferma Dati Sito'][1]");
    public By confermaFornitureButton = By.xpath("//section[@class='tabContent active oneConsoleTab']//div//button[text()='Conferma Forniture']");
    //    public By creaOffertaButton = By.xpath("//section[@class='tabContent active oneConsoleTab']//div//button[text()='Crea offerta']");
    public By creaOffertaButton = By.xpath("//section[@class='tabContent active oneConsoleTab']//div[@class='oneWorkspaceTabWrapper'] //article[@class='slds-card']//div[@class='slds-col_bump-left slds-size_10-of-12 slds-align-middle']//div//button[text()='Crea offerta']");
    //    public By okPopupButton = By.xpath("//*[@id='modalButtons-839']/lightning-button/button");
    public By okPopupButton = By.xpath("//button[contains(text(),'OK')]");
	public By campoDataRicezioneEsito= By.xpath("//td[contains(text(),'Data ricezione esito')]/ancestor::tr[1]/td[2]/input");
  

	public void riepilogoOfferta(String dataAdesione, String numContratto) throws Exception{
		TimeUnit.SECONDS.sleep(7);
		//Inserimento Sysdate
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,dataAdesione);
	}
	
	public void inserisciPOD(String POD) throws Exception{
		TimeUnit.SECONDS.sleep(3);
		util.waitAndGetElement(insertPOD).sendKeys(POD);
	}

	public void inserisciIndirizzo(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception{
		
		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
			//logger.info("Ho trovato il campo *Provincia e inserisco il valore " + Provincia);
			util.waitAndGetElement(insertProvincia).sendKeys(Provincia);
			TimeUnit.SECONDS.sleep(5);
		
		util.waitAndGetElement(insertProvincia).sendKeys(Provincia);
		WebElement el1 = util.waitAndGetElement(insertProvincia);
		util.scrollToElement(el1);
		el1.isSelected();
		TimeUnit.SECONDS.sleep(5);

		util.waitAndGetElement(insertComune).sendKeys(Comune);
//		WebElement el2 = util.waitAndGetElement(insertComune);
//		util.scrollToElement(el2);
//		el2.isSelected();
		TimeUnit.SECONDS.sleep(5);

		util.waitAndGetElement(insertIndirizzo).sendKeys(Indirizzo);
//		WebElement el3 = util.waitAndGetElement(insertIndirizzo);
//		util.scrollToElement(el3);
//		el3.isSelected();
		TimeUnit.SECONDS.sleep(5);

		util.waitAndGetElement(insertCivico).sendKeys(Civico);
//		WebElement el4 = util.waitAndGetElement(insertCivico);
//		util.scrollToElement(el4);
//		el4.isSelected();
		TimeUnit.SECONDS.sleep(5);
//		spinnerManager.checkSpinners();
		}

	}

	public void inserisciDettaglioSito(String Uso, String SocietaVendita, String Residente, String Titolarita) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		WebElement dropdown = driver.findElement(insertUso);
		dropdown.click();
//		dropdown.sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(3);
		dropdown.sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
		
//		dropdown.sendKeys(Keys.ARROW_DOWN);
//		//dropdown.sendKeys(Keys.DOWN);
//		dropdown.sendKeys(Keys.ENTER);
//		TimeUnit.SECONDS.sleep(5);
//
//		TimeUnit.SECONDS.sleep(10);

		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
//		WebElement el1 = util.waitAndGetElement(insertSocietaVendita);
//		util.scrollToElement(el1);
//		el1.isSelected();
		TimeUnit.SECONDS.sleep(10);

		WebElement dropdown1 = driver.findElement(insertResidente);
		dropdown1.click();
		TimeUnit.SECONDS.sleep(3);
		dropdown1.sendKeys(Keys.ARROW_DOWN);
		TimeUnit.SECONDS.sleep(5);
		dropdown1.sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(10);

		WebElement dropdown2 = driver.findElement(insertTitolarita);
		dropdown2.click();
		dropdown2.sendKeys(Keys.ARROW_DOWN);
		TimeUnit.SECONDS.sleep(5);
		dropdown2.sendKeys(Keys.ARROW_DOWN);
		TimeUnit.SECONDS.sleep(5);
		dropdown2.sendKeys(Keys.ARROW_DOWN);
		TimeUnit.SECONDS.sleep(5);
		dropdown2.sendKeys(Keys.ENTER);
//		TimeUnit.SECONDS.sleep(2);
//		dropdown2.sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(10);

//		util.waitAndGetElement(insertTitolarita).sendKeys(Titolarita);
//		WebElement el3 = util.waitAndGetElement(insertTitolarita);
//		util.scrollToElement(el3);		
//		el3.isSelected();
	}
	
	
	public void confermaDatiSito(By confermaDatiSito) throws Exception{
		util.waitAndGetElement(confermaDatiSito).click();
		spinnerManager.checkSpinners();
	}
	
	
	public void confermaPod(By confermaPod) throws Exception{
		
		WebElement button = driver.findElement(confermaPod); 
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript ("document.getElementByText(\"Conferma Pod/Pdr\")).click();", button); 

		WebElement Elemento = driver.findElement (confermaPod);
		JavascriptExecutor ecutor = (JavascriptExecutor) driver;
		ecutor.executeScript ("argomenti [0].click ();", Elemento);		
		//util.waitAndGetElement(confermaPod).click();
//		WebElement el = util.waitAndGetElement(conferma);
//		util.scrollToElement(el);
//		el.click();
//		//spinnerManager.waitForSpinnerBySldsHide(spinnerManager.dotSpinner);
		spinnerManager.checkSpinners();
	}
	
	public void verificaIndirizzo(By verifica) throws Exception{
		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
			util.waitAndGetElement(verifica).click();
			spinnerManager.checkSpinners();
			}
		}

	public void confermaForniture(By conferma) throws Exception{
		util.waitAndGetElement(conferma).click();
		spinnerManager.checkSpinners();
	}
	
	public void creaOfferta(By crea) throws Exception{
		util.waitAndGetElement(crea).click();
		spinnerManager.checkSpinners();
	}

	public void okPopup(By ok) throws Exception{
		util.waitAndGetElement(ok).click();
		spinnerManager.checkSpinners();
	}

}
