package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class FormRicontrattoCommBSNComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By imprese = By.xpath("//header[@id='globalHedaer']//descendant::li[2]/a");
	public By impreseHomePagePath = By.xpath("//main[@id='main']//div[@class='image-hero_content-wrapper']/nav");
	public By impreseHomePageHeading_2 = By.xpath("//h1[contains(text(),\"L'energia non è mai stata così semplice\")]");
	public By impreseHomePageHeading = By.xpath("//h1[contains(text(),'Vicini')]");
	public By impreseHomePageParagraph = By.xpath("//main[@id='main']//descendant::p[contains(text(),'Scopri Open')]");
	public By comePossiamoAiutarti = By.xpath("//main[@id='main']//child::div/h2[contains(text(),'Come')]");
	public By contattaci = By.xpath("//main[@id='main']//child::div[1]/li/a");
	public By contattaciSubText =  By.xpath("//div[@id='e_glossary_title']//descendant::div[@class='rich-text_text text--standard']");
	public By consulentiEsperti = By.xpath("//div[@id='e_glossary_title']//child::div/ul/li[4]");
	public By cliccaQui = By.xpath("//div[@class='rich-text_text text--standard']/ul/li[4]/span[@class='text--title-lightpink']/b/a");
	public By contattaciHomepagePath =  By.xpath("//section[@id='general_hero']/div/div/nav");
	public By contattciHeading = By.xpath("//main[@id='main']/section[@id='general_hero']//descendant::h1");
	public By contattciParagraph = By.xpath("//main[@id='main']/section[@id='general_hero']//descendant::p");
	public By nome = By.xpath("//input[@id='form_nome']");
	public By cognome = By.xpath("//input[@id='form_cognome']");
	public By regioneSociale = By.xpath("//input[@id='form_rag_soc']");
	public By importaMedia = By.xpath("//span[@id='importoSelectBoxIt']");
	public By provincia = By.xpath("//span[@id='provinciaSelectBoxItContainer']/span[@id='provinciaSelectBoxIt']/span[@id='provinciaSelectBoxItText']");
	public By cap = By.xpath("//span[@id='idCapSelectBoxItText']");
	public By email = By.xpath("//input[@id='form_email_backmail']");
	public By confermaEmail = By.xpath("//input[@id='form_conferma_email_backmail']");
	public By telefonoCellulare = By.xpath("//input[@id='form_cellulare']");
	public By confirmTelefonoCellulare = By.xpath("//input[@id='confirmtelephone']");
	public By contattciHeading2 = By.xpath("//h3[@class='plan-main-head']");
	public By seigiaclientieHeading = By.xpath("//form[@id='formContactus']/div[@class='form-wrapper'][2]/div/label");
	public By indicaText = By.xpath("//form[@id='formContactus']//descendant::p[contains(text(),'Indica')]");
	public By siRadio = By.xpath("//form[@id='formContactus']/div[@class='form-wrapper'][2]//div[@class='radio-container'][1]/label");
	public By noRadio = By.xpath("//form[@id='formContactus']/div[@class='form-wrapper'][2]//div[@class='radio-container'][2]/label");
	public By informativaHeading = By.xpath("//form[@id='formContactus']/div[@class='form-privacy']/label");
	public By informativaText = By.xpath("//textarea[@id='testo_privacy']");
	public By luceEGasHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[1]/a");
	public By impreseHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[2]/a");
	public By insiemeATeHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[3]/a");
	public By storieHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[3]/a");
	public By futurEHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[4]/a");
	public By mediaHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[5]/a");
	public By supportoHeader = By.xpath("//header[@id='globalHedaer']//descendant::ul/li[6]/a");
	public By accettoRadio = By.xpath("//form[@id='formContactus']/div[@class='form-privacy']//div[1]/label");
	public By NonAccettoRadio = By.xpath("//form[@id='formContactus']/div[@class='form-privacy']//div[2]/label");
	public By tuttiCampiText = By.xpath("//form[@id='formContactus']/div[@class='form-wrapper'][3]/div[@class='form-group']");
	public By inviaButton = By.xpath("//button[@id='SubmitProceedForm']");
	public By campoObbligatorioNome = By.xpath("(//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/span)[1]");
	public By campoObbligatorioCogome = By.xpath("(//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/span)[2]");
	public By campoObbligatorioRegioneSociale = By.xpath("(//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/span)[3]");
	public By campoObbligatorioImportaMedio = By.xpath("(//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/span)[5]");
	public By campoObbligatorioProvincia = By.xpath("(//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/span)[7]");
	public By campoObbligatorioCap = By.xpath("(//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/span)[9]");
	public By campoObbligatorioEmail = By.xpath("//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/div[1]/span[@class='errorMsg']");		
	public By campoObbligatorioTelefonaCellulare = By.xpath("//form[@id='formContactus']/div[@class='form-wrapper'][1]/div/div[3]/span[@class='errorMsg']");
	public By importoMedioList = By.xpath("//span[@id='importoSelectBoxItContainer']//ul//li");
	public By oltre10000 = By.xpath("//span[@id='importoSelectBoxItContainer']//li[.='oltre 10.000']");
	public By roma = By.xpath("//div[@id='divProvincie']//li[.='ROMA']");
	public By cap00198 = By.xpath("//ul[@id='idCapSelectBoxItOptions']/li[.='00198']");
	public By provinicaList = By.xpath("//div[@id='divProvincie']/span//li");
	public By capList = By.xpath("//div[@id='divCap']//li");
	public By step2ContattaciHeading = By.xpath("//section[@id='step2']/h3");
	public By step2ContattaciParagraph = By.xpath("//section[@id='step2']/p");
	public By riepilogoHeading = By.xpath("//section[@id='step2']/h5");
	public By enteredDataNome = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][1]");
	public By enteredDataCognome = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][2]");
	public By enteredDataRegioneSociale = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][3]");
	public By enteredDataImportoMedio = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][4]");
	public By enteredDataProvincia = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][5]");
	public By enteredDatacap = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][6]");
	public By enteredDataEmail = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][7]");
	public By enteredDataCellulare = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][8]");
	public By enteredDataSeiGiaCliente = By.xpath("//section[@id='step2']//div[@class='show-entered-data']/div[@class='form-group'][9]");
	public By confermaButton =  By.xpath("//button[@id='btn-confirm']");
	public By indietroButton = By.xpath("//a[@id='btn-back']");
	public By step3contattoRichiestaHeading  = By.xpath("//section[@id='step3']//h3[.= 'RICHIESTA DI CONTATTO']");
	public By step3contattoRichiestaParagraph1  = By.xpath("//section[@id='step3']/div[@class='text-center']/p[1]");
	public By step3contattoRichiestaParagraph2  = By.xpath("//section[@id='step3']/div[@class='text-center']/p[2]");
	
	public FormRicontrattoCommBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	  public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	  
	  public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	  
	  public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
	  public void VerifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String textfield_found="NO";
			
			WebElement element = driver.findElement(checkObject);
			
			String actualtext = element.getText().replaceAll("(\r\n|\n)", "");
			
			
			if (actualtext.contains(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
			}
	  
	  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void selctValueFromDropdown(By Object, String Valore) throws Exception
		{
			WebDriverWait wait = new WebDriverWait (driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(Object));
			
			List<WebElement> myElements = driver.findElements(Object);
		       for(WebElement e : myElements)
		       {
		    	  String act = e.getText();
		         if(act.equalsIgnoreCase(Valore))
		         {
		             e.click();
		         }
		         else throw new Exception("Value is not Matching");
		       }
		       
		}
		
		public void isRadioSelected(By Object)
		{
			 driver.findElement(Object).isSelected();
		}
		
		public static final String IMPRESE_HOMEPAGE_PATH = "Home / Imprese";
		public static final String IMPRESE_HOMEPAGE_HEADING = "Vicini alle piccole imprese con un mese gratis di Open Energy";
		public static final String IMPRESE_HOMEPAGE_PARAGRAPH = "Scopri Open Energy: l'offerta in abbonamento per pagare l'energia quanto la paghiamo noi. In più i primi due mesi di abbonamento sono gratis!";
		public static final String COME_POSSIAMO_AIUTARTI ="Come possiamo aiutarti?";
		public static final String CONTATTACI = "Contattaci";
		public static final String CONTATTACI_SUBTEXT = "Enel Energia è sempre con te, se sei cliente e se vuoi diventarloEnel è vicina al mondodei professionisti e delle impreseattraverso una rete di canali al servizio delle esigenze dei propri clienti, in particolare:Un nuovo servizio clienti dedicato, con consulenti specializzati a tua disposizione. Per ricevere la consulenza di cui hai bisogno chiama il numero verde800.900.860 e digita il tasto 3. Clicca qui per scoprire il nuovo servizio clienti dedicato; Unachat con consulenti specializzati disponibile all’interno dell’area clienti Business.Clicca qui per scoprire i servizi dedicati dell’area clienti business;Oltre 900 negozi diretti ed indirettidistribuiti su tutto il territorio nazionale.Clicca qui per scoprire i nostri negozi;una rete diconsulenti esperti disponibili direttamente presso la tua impresa per aiutarti ad individuare l'offerta più adatta alle tue esigenze;Clicca qui per richiedere la visita di un consulente.Prova";
		public static final String CONSULTENTI_ESPERTI = "una rete diconsulenti esperti disponibili direttamente presso la tua impresa per aiutarti ad individuare l'offerta più adatta alle tue esigenze;Clicca qui per richiedere la visita di un consulente.";
		public static final String CONTATTACI_HOMEPAGE_PATH = "Home / Form ricontatto commerciale /";
		public static final String CONTATTACI_HEADING = "Contattaci";
		public static final String CONTATTACI_HOMEPAGE_PARAGRAPH = "Inserisci i tuoi dati, sarai ricontattato da nostri consulenti SENZA alcun impegno";
		public static final String SEI_GIA_HEADING = "Sei già cliente Enel Energia?";
		public static final String INDICA_TEXT = "Indica, se sei già nostro cliente.";
		public static final String INFORMATIVA_HEADING = "INFORMATIVA AI SENSI DELL' ART. 13 del Regolamento UE 2016/679 (“GDPR”)";
		public static final String INFORMATIVA_TEXT = "La informiamo che i suoi dati personali sono raccolti e trattati per dare seguito alla sua segnalazione. L'informativa completa è disponibile sul sito: www-coll1.enel.it. Titolare del trattamento è Enel Energia S.p.A., con sede legale in Viale Regina Margherita 125 – 00198 Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007. Enel Energia utilizzerà i dati come sopra rilasciati, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail); i dati rilasciati potranno essere utilizzati con le medesime finalità sopra descritte anche da parte delle Società del Gruppo Enel (tra cui Enel X), da società controllanti, controllate o collegate o da partner commerciali di Enel Energia.";
		public static final String TUTTI_CAMPI_TEXT = "Tutti i campi sono obbligatori.";
		public static final String STEP2_CONTATTACI_HEADING ="Contattaci";
		public static final String STEP2_CONTATTACI_PARAGRAPH = "Se i dati che hai inserito sono corretti clicca sul tasto conferma.In caso negativo torna indietro per procedere alla modifica.";
		public static final String ENTERED_DATA_NOME = "NomeMario";
		public static final String ENTERED_DATA_COGNOME = "CognomeRossi";
		public static final String ENTERED_DATA_REGIONESOCIALE = "Ragione SocialeProva S.r.l.";
		public static final String ENTERED_DATA_IMPORTOMEDIO = "Importo medio bolletta €/annooltre 10.000";
		public static final String ENTERED_DATA_PROVINCIA = "Provincia ROMA";
		public static final String ENTERED_DATA_CAP = "CAP 00198";
		public static final String ENTERED_DATA_EMAIL = "Emailfabiana.manzo@nttdata.com";
		public static final String ENTERED_DATA_CELLULARE = "393669047153";
		public static final String ENTERED_DATA_SEIGUI_CLIENTI = "Sei già cliente Enel Energia?NO";
		public static final String RIEPILOGO_HEADING= "Riepilogo dati cliente";
		public static final String STEP3_CONTATTO_RICHIESTA_HEADING = "RICHIESTA DI CONTATTO";
		public static final String STEP3_CONTATTO_RICHIESTA_PARAGRAPH1 = "Grazie per aver utilizzato i nostri servizi online.";
		public static final String STEP3_CONTATTO_RICHIESTA_PARAGRAPH2 = "La tua richiesta di Informazioni è stata correttamente registrata.";
}
