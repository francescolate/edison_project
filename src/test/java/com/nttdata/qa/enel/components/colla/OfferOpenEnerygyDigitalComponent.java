package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OfferOpenEnerygyDigitalComponent {

	WebDriver driver;
	SeleniumUtilities util;
	  
	public By calcolaOra = By.xpath("//span[.='Calcolalo ora']");
	public By days = By.xpath("//select[@id='days']");
	public By months = By.xpath("//select[@id='months']");
	public By years = By.xpath("//select[@id='years']");
	public By dunno = By.xpath("//input[@id='check13']");
	public By si= By.xpath("//input[@id='yesitalian']");
	public By birthPlace = By.xpath("//input[@id='comuneCF']");
	public By calcola = By.xpath("//button[@id='calcolaBtn']");
	public By busFiscalCode = By.xpath("//input[@id='busFiscalCode']");
	public By codiceFiscaleReferente = By.xpath("//input[@id='referenceTaxCode']");
	public By emailSocieta = By.xpath("//input[@id='companyEmail']");
	public By pecEmail = By.xpath("//input[@id='pecEmail']");
	public By errMsgemailSoc = By.xpath("//span[@id='IIcompanyEmail_errmsg']");
	public By errMsgemailPEC = By.xpath("//span[@id='IIPEC_errmsg']");
	public By infoIconEmailSoc = By.xpath("//a[@id='infoEmailCompany']");
	public By popupText = By.xpath("//p[contains(text(),'Contratto e Bolletta')]");
	public By popupCloseButton = By.xpath("//button[@class='remodal-close close-AEM_SF close-AEM_SF']");
	public By referenceTelephone = By.xpath("//input[@id='referencePhone']");
	public By errMsgRefTelephone = By.xpath("//span[@id='MobilePhone_Ref_errmsg']");
    public By formaGiuridica = By.xpath("//ul[@id='legalFormSelectBoxItOptions']/li");
    public By condominio = By.xpath("//li[.='Condominio']");
    public By tipoReferente = By.xpath("//span[@id='referenceTypeSelectBoxIt']");
    public By tipoReferenteList = By.xpath("//ul[@id='referenceTypeSelectBoxItOptions']/li");
    public By consorziDiBonifica = By.xpath("//li[.='Consorzi di Bonifica']");
    public By ConsorzidiImpresa = By.xpath("//li[.='Consorzi di Impresa']");
    public By enteMorale = By.xpath("//li[.='Ente Morale']");
    public By impInd = By.xpath("//li[.='Imp. Ind.']");
	public By nome = By.xpath("//input[@id='iiName']");
	public By cognome = By.xpath("//input[@id='iiLastName']");
	public By prosegui = By.xpath("//button[@id='customer-data-next']");
	public By nomeErrMsg = By.xpath("//span[@id='IIName_errmsg']");
	public By cognomeErrMsg = By.xpath("//span[@id='IILastName_errmsg']");
	public By onlus = By.xpath("//li[.='ONLUS']");
	public By socCoopAgr = By.xpath("//li[.='Soc.Coop.Agr.']");
	public By sapa = By.xpath("//li[.='s.a.p.a']");
	public By sas = By.xpath("//li[.='s.a.s.']");
	public By sc = By.xpath("//li[.='s.c.']");
	public By scrl = By.xpath("//li[.='s.c.r.l.']");
	public By snc = By.xpath("//li[.='s.n.c.']");
	public By spa = By.xpath("//li[.='s.p.a.']");
	public By srl = By.xpath("//li[.='s.r.l.']");
	public By ss = By.xpath("//li[.='s.s.']");
	public By infoPEC = By.xpath("//a[@id='infoPEC']");
	public By infoPECText = By.xpath("//p[contains(text(),' PEC indicato')]");
	public By regioneSociale = By.xpath("//input[@id='ITA_IFM_Company__c']");
	public By piva = By.xpath("//input[@id='pIVA']");
	public By telefonoSoc = By.xpath("//input[@id='ITA_IFM_Company_Phone__c']");
	public By tipo_Rappresentante =  By.xpath("//li[.='Rappresentante Legale']");
	public By telefonoReferente = By.xpath("//input[@id='referencePhone']");
	public By no = By.xpath("//input[@id='noTracking']");
	public By noText = By.xpath("//p[contains(text(),'agli obblighi d')]");
	public By privacyConsent = By.xpath("//input[@id='privacy-consent']");
	
	public By headinginfoFor = By.xpath("//h3[@id='intestazione_2']");
	public By textinfoFor = By.xpath("//p[.='Di cosa hai bisogno?']");
	public By hcambioFornituro = By.xpath("//span[.='CAMBIO FORNITORE']");
	public By textCambioFornituro = By.xpath("//p[contains(text(),' un contratto con un altro fornitore')]");
	public By hprimaAttivazone = By.xpath("//span[.='PRIMA ATTIVAZIONE']");
	public By textprimaAttivazone = By.xpath("//p[contains(text(),'Voglio attivare un nuovo contatore')]");
	public By hSubentro = By.xpath("//span[.='SUBENTRO']");
	public By textsubentro = By.xpath("//p[contains(text(),'Voglio riattivare un contatore disattivato')]");
	public By cambioFornitura  = By.xpath("//input[@id='changeSupplier']");
	public By pod = By.xpath("//input[@id='ITA_IFM_POD__c']");
	public By cap = By.xpath("//input[@id='ITA_IFM_ZIPCode__c']");
	public By textSte2 = By.xpath("//span[contains(text(),'Se non hai a portata di man')]");
	public By inserisciloDopo = By.xpath("//button[@id='insert-data-after']");
	
	public By indrizzoDiFornituraText = By.xpath("//h1[@id='supplyAddressTitle']");
	public By citta = By.xpath("//input[@id='91:2;a']");
	public By indrizzo = By.xpath("//input[@id='188:2;a']");
	public By numeroCivico = By.xpath("//input[@id='249:2;a']");
	public By indrizzoDiFornituraCAP = By.xpath("//input[@id='332:2;a']");
	public By indrizzoDiFornituraPrivacyText = By.xpath("//div[@class='input-group powerInfoMargin']/p");
	public By potenza = By.xpath("//span[@id='powerSelectBoxIt']");
	
	public By headingRecapti = By.xpath("//h3[.='Recapiti']");
	public By textRecapti = By.xpath("//label[@id='pa_address_label']");
	public By siRecapti = By.xpath("//input[@id='yesResidence']");
	public By noRecapti = By.xpath("//input[@id='noResidence']");
	public By addressRadio = By.xpath("//label[@id='ba_address_radio']");
	public By yesBillingAddress = By.xpath("//input[@id='yesBillingAddress']");
	public By noBillingAddress = By.xpath("//input[@id='noBillingAddress']");
	public By salvaContinuaDopo = By.xpath("//button[@id='supply-data-save']");
	public By insertManually = By.xpath("//span[text()='Compila manualmente']");
	
	public By headingPagamentiBollette = By.xpath("//h3[@id='intestazione_3']");
	public By labelPaymentMode = By.xpath("//label[@id='paymentModeLabel']");
	public By paymentModeSelectBox = By.xpath("//span[@id='payment-modeSelectBoxItText']");
	public By iBANCode = By.xpath("//input[@id='ITA_IFM_IbanCode']");
	public By yesAccountHolder = By.xpath("//input[@id='yesAccountHolder']");
	public By noAccountHolder = By.xpath("//input[@id='noAccountHolder']");
	
	public By Modalità_di_ricezione_Heading = By.xpath("//p[contains(text(),'Modalità di ricezione della bolletta')]");
	public By Modalità_di_ricezione_Radio1 = By.xpath("//span[contains(text(),'Voglio ricevere la bolletta')]/parent::label");
	public By Modalità_di_ricezione_Radio2 = By.xpath("//span[contains(text(),'Voglio ricevere la fattura')]/parent::label");
	public By email = By.xpath("//input[@id='ITA_IFM_Billing_Mail']");
	public By prefisso = By.xpath("//select[@id='billingPhonePrefix']");
	public By cellulare = By.xpath("//input[@id='ITA_IFM_Billing_Phone']");
    
    public OfferOpenEnerygyDigitalComponent(WebDriver driver) throws Exception {
  		this.driver = driver;
  		util = new SeleniumUtilities(this.driver);
  		
  	}
    
	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	
	public void launchCorporateLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals("")){
//			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+java.net.URLEncoder.encode(Costanti.WP_Corporate_BasicAuth_Password)+"@");
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");

			System.out.println(url);
		}
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.click);
		
	}
	
	public void jsClickComponent(By clickableObject) throws Exception {
		util.jsClickElement(clickableObject);
		
	}
	
	public boolean verifyComponentExistence(By by, int seconds) throws Exception{
		return util.verifyExistence(by, seconds);
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//		if (!util.exists(oggettoEsistente, 15))
//			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		*/
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			//System.out.println(message);
			throw new Exception(message);
		}
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
			
	public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found." + " Text found= " +  actualtext);
	}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		if (url.contains("https://"))
			url = url.substring(8);
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
	
	public void checkUrl2(String url) throws Exception{
		String link=driver.getCurrentUrl();
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
	
	public void selectDropdown(By object, String Prop) throws Exception{
		WebElement solutions_dropdown = driver.findElement(object);
		Select solutions = new Select(solutions_dropdown);
		solutions.selectByVisibleText(Prop);
		}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkForDropdownValue(By checkObject, String Value[]) throws Exception{
		 
	 	List<WebElement> List = driver.findElements(By.xpath("//ul[@id='legalFormSelectBoxItOptions']/li"));
		int listSize = List.size();
	 	
	 	List<String> expectedList = Arrays.asList(Value);
		List<String> newList = new ArrayList<String>();
		
		for(WebElement ArgomentoList : List) {
		newList.add(ArgomentoList.getText());
		System.out.println(ArgomentoList.getAttribute("innerText"));
					
		if(!newList.equals(expectedList))
    		throw new Exception ("The  details are not correctly displayed");
		}	
}
	
	 public void verifyFeildAttribute(By existingObject, String text) throws Exception {
			String actualText = driver.findElement(existingObject).getAttribute("placeholder");
			if (!actualText.equalsIgnoreCase(text))
				throw new Exception("object with text " + text + " is not exist.");
		}
	 
	
	public static final String FORMATO_NON_CORETTO ="Formato non corretto";
	public static final String POPUP_TEXT = "Contratto e Bolletta Web saranno inviati all'e-mail da te inserita. Verificane la correttezza prima di procedere.";
	public static final String FORMA_GIURIDICA[] ={};
	public static final String TIPO_REFERENTE_CONDOMINION[] ={};
	public static final String TIPO_REFERENTE_CONSORZI_DI_BONIFICA[] ={};
	public static final String TIPO_REFERENTE_CONSORZI_DI_IMPERSA[] ={};
	public static final String TIPO_REFERENTE[] ={};
	public static final String TIPO_REFERENTE_IMPIND[] ={};
	public static final String CAMPO_OBBLIGATORIO ="Campo obbligatorio";
	public static final String INFO_PEC_TEXT ="L'indirizzo PEC indicato sarà utilizzato per assolvere ad eventuali comunicazioni riferite al tuo contatto secondo la normativa vigente. Verificane la correttezza prima di procedere.";
	public static final String NO_TEXT = "L'azienda è soggetta agli obblighi di tracciabilità dei pagamenti previsti dalla Legge 136 del 13 agosto 2010 (Piano straordinario contro le mafie)?";
	public static final String HEADING_INFO_FORNITURA = "Informazioni fornitura";
	public static final String TEXT_INFO_FORNITURA = "Di cosa hai bisogno?";
	public static final String H_CAMBIO_FORNITORE ="CAMBIO FORNITORE";
	public static final String T_CAMBIO_FORNITORE ="Ho già un contratto con un altro fornitore, ma vorrei passare ad Enel";
	public static final String H_PRIMA_ATTIVAZIONE = "PRIMA ATTIVAZIONE";
	public static final String TEXT_PRIMA_ATTIVAZONE ="Voglio attivare un nuovo contatore";
	public static final String H_SUBENTRO = "SUBENTRO";
	public static final String TEXT_SUBENTRO = "Voglio riattivare un contatore disattivato";
	public static final String TEXT_STEP2 ="Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l'adesione.";
	public static final String IndrizzoDiFornituraText ="Indirizzo di Fornitura";
	public static final String IndrizzoDiFornituraPrivacyText = "Ti informiamo che la fornitura verrà attivata con i medesimi requisiti tecnici (potenza e tensione) attualmente in esercizio. Se lo desideri potrai effettuare una variazionedi potenza dopo l'attivazione del contratto direttamente dalla tua area clienti.";
	public static final String HEADING_RECAPTI = "Recapiti";
	public static final String TEXT_RECAPTI = "L'indirizzo di fornitura è lo stesso dell'indirizzo della sede legale?";
	public static final String ADDRESS_RADIO = "Dove vuoi ricevere le comunicazioni relative alla tua fornitura?";
	public static final String YES_BILLING = "Allo stesso indirizzo della fornitura";
	public static final String NO_BILLING = "Ad un indirizzo diverso da quello della fornitura";
	public static final String HEADING_PAGAMENTI_BOLLETTE = "Pagamenti e Bollette";
	public static final String PAYMENTMODE_SELECTBOX ="Addebito sul conto corrente";
	public static final String Modalità_Di_ricezione_Radio1 = "Voglio ricevere la bolletta via email e le notifiche relative all’immissione della bolletta ai seguenti canali di contatto";
	public static final String Modalità_Di_ricezione_Radio2 ="Voglio ricevere la fattura in forma cartacea tramite posta ordinaria";
}

