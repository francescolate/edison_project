package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//Il seguente componente si occupa di effettuare l'accesso alla sezione clienti. 
//Dopo aver effettuato la Login a Salesforce clicca sul menu a tendina del primo tab aperto per accedere all sezione Clienti
public class AccediTabClientiComponent {
    private WebDriver driver;
    Logger LOGGER = Logger.getLogger(this.getClass().toString());
    
    public AccediTabClientiComponent(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
    
    
    //@FindBy(xpath = "//a[@title='Clienti' and @role = 'option' and @data-itemid='Account']")
    @FindBy(xpath = "//a[@data-label='Clienti' and @role = 'option' and @data-itemid='Account']") //FR 01.09.2021
    private WebElement clientiSpan;
    
    @FindBy(xpath = "//span[(text()='Referenti' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]")
    private WebElement referentiSpan;

    @FindBy(xpath = "//span[(text()='Contatti' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]")
    private WebElement contatti;
    
//    public static final String X_DROPDOWN = "//button[@class='slds-button slds-p-horizontal__"
//    		+ "xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon[1]" ;
    
    public static final String X_DROPDOWN = "//button[@aria-label='Mostra menu di navigazione']/lightning-primitive-icon[1]";
    

    @FindBy(xpath = X_DROPDOWN)
    private WebElement dropdwonButton;

	
	public void accediTabClienti() throws Exception{
		TimeUnit.SECONDS.sleep(7);
		dropdwonButton.click();
        //LOGGER.log(Level.INFO,"DropDown Client clicked");
        TimeUnit.SECONDS.sleep(3);
		clientiSpan.click();
	}
	
	public void accediTabReferenti() throws Exception{
		TimeUnit.SECONDS.sleep(7);
		dropdwonButton.click();
        //LOGGER.log(Level.INFO,"DropDown Client clicked");
		referentiSpan.click();
	}
	
	public void accediTabContatti() throws Exception{
		TimeUnit.SECONDS.sleep(7);
		dropdwonButton.click();
        LOGGER.log(Level.INFO,"DropDown Client clicked");
		contatti.click();
	}
	
}
