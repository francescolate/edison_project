package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ValidaModuloAdesioneComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public ValidaModuloAdesioneComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  


	public final By clickValidaDocumentazione = By.xpath("//button[text()='Valida Documentazione']");
	public final By tableDoc = By.xpath("//tbody[@id='documents']/ancestor::table");
//	public final By tableDoc = By.xpath("//tbody[@id='documents']");
	public final By clickEffettaChecklist = By.xpath("//button[text()='Effettua Checklist']");
	public final By checkboxVerificaRipensamento = By.xpath("//label[text()='Verifica Ripensamento']/ancestor::label/span");
	public final By checkboxFirma = By.xpath("//label[text()='Firma']/../div/label/span");
	public final By checkboxData = By.xpath("//label[text()='Data Sottoscrizione']/../div/label/span");
	public final By clickLuogoData = By.xpath("//label[text()='Luogo e Data Sottoscrizione']/../div/label/span");
	public final By clickConferma = By.xpath("//button[text()='Conferma']");
	public final By clickValida = By.xpath("//button[text()='Valida Documento']");
	public final By clickCheckList = By.xpath("//button[text()='Effettua Checklist']");
	public void clickValidaDocumento() throws Exception{
			driver.switchTo().defaultContent();
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			List<WebElement> els = driver.findElements(clickValidaDocumentazione);
			boolean flag = false;
			for (WebElement webElement : els) {
				//util.scrollToElement(webElement);
				try {
					executor.executeScript("arguments[0].click();", webElement);
				
				}catch(Exception e) {
					
				}
				
			}

			TimeUnit.SECONDS.sleep(5);
	}
	
	public void clickcheckbox() throws Exception{
		
		press(checkboxVerificaRipensamento);
		TimeUnit.SECONDS.sleep(1);
		
		driver.switchTo().defaultContent();
		spinner.checkSpinners();
		

			boolean flag = false;
			
			util.getFrameActive();
				try {
				 util.waitAndGetElement(checkboxFirma);
				 flag=true;
				 
				}catch(Exception e) {
				}
			
				util.getFrameActive();
		if(!flag) {
			
				try {
				
				 press(checkboxData);
				
				}catch(Exception e) {
					driver.switchTo().defaultContent();
				}
			
			
		}else {
			press(checkboxFirma);
			TimeUnit.SECONDS.sleep(1);
			press(clickLuogoData);
			TimeUnit.SECONDS.sleep(1);
		}
	}
	
	public void clickConferma() throws Exception{
		TimeUnit.SECONDS.sleep(5);
		press(clickConferma);
		TimeUnit.SECONDS.sleep(1);
	}
	
	public void clickValida() throws Exception{
//		press(clickValida);
//		TimeUnit.SECONDS.sleep(1);
		driver.switchTo().defaultContent();
		spinner.checkSpinners();
		
		TimeUnit.SECONDS.sleep(10);

//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			WebElement element = null;
//			for (WebElement webElement : els) {
//				driver.switchTo().frame(webElement);
//				try {
//				 element = util.waitAndGetElement(clickValida);
//				 break;
//				}catch(Exception e) {
//					driver.switchTo().defaultContent();
//				}
//			}
			util.getFrameActive();
			press(clickValida);
	}
	
	public void clickCheckList() throws Exception{
//		press(clickValida);
//		TimeUnit.SECONDS.sleep(1);
		driver.switchTo().defaultContent();
		spinner.checkSpinners();
		
		TimeUnit.SECONDS.sleep(10);
//			List<WebElement> els = driver.findElements(clickCheckList);
//			WebElement element = null;
//			for (WebElement webElement : els) {
//				driver.switchTo().frame(webElement);
//				try {
//					press(clickCheckList);
//					System.out.println("trovato");
//				 break;
//				}catch(Exception e) {
//					System.out.println("Non trovato");
//				}
//			}
//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			WebElement element = null;
//			for (WebElement webElement : els) {
//				driver.switchTo().frame(webElement);
//				try {
//					driver.findElement(clickCheckList);
//				 break;
//				}catch(Exception e) {
//					driver.switchTo().defaultContent();
//				}
//			}
			
			util.getFrameActive();
			if(util.exists(clickCheckList, 20)) {
				if(util.verifyVisibility(clickCheckList, 20)) {
					press(clickCheckList);
				}
			}
	}
	
	public void selezionaModuloAdesione() throws Exception {
		driver.switchTo().defaultContent();
//		spinner.checkSpinners();
		
		TimeUnit.SECONDS.sleep(10);
//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			WebElement element = null;
//			for (WebElement webElement : els) {
//				driver.switchTo().frame(webElement);
//				try {
//				 element = util.waitAndGetElement(tableDoc);
//				 break;
//				}catch(Exception e) {
//					driver.switchTo().defaultContent();
//				}
//			}
			
			util.getFrameActive();
			//press(PrecheckButton);
					
			TimeUnit.SECONDS.sleep(5);
			
			List<WebElement> tables = driver.findElements(tableDoc);
			WebElement table = null;
			for (WebElement tab : tables) {
				if(tab.isDisplayed()) {
					table=tab;
				}
			}
			String colName = "OBBLIGATORIO";
			
			int count = util.getTableRowCount(table);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			for (int i = 0; i<count;i++) {
				if(util.getTableCellDataWithoutDiv(table, i+1, colName, "div").equalsIgnoreCase("Y")) {
					WebElement temp = driver.findElement(By.xpath("(//tbody[@id='documents']/ancestor::table//input[@type='radio'])"
							+(i>0?"["+(i+1)+"]":"")
							));
					
					
					
					executor.executeScript("arguments[0].click();", temp);
//					spinner.checkSpinners();
					TimeUnit.SECONDS.sleep(5);
					 WebElement element = util.waitAndGetElement(clickEffettaChecklist);
					executor.executeScript("arguments[0].click();", element);
					TimeUnit.SECONDS.sleep(5);
//					spinner.checkSpinners();
//					TimeUnit.SECONDS.sleep(5);
					// Validazione Documento selezionato
				    this.clickcheckbox();
//				    this.clickCheckList();
				    this.clickValida();
//				    i=i-1;
//				    this.VerificaStatoModulo();

//					break;
				}
				
			}
			
	}
	
	public void VerificaStatoModulo() throws Exception {
		driver.switchTo().defaultContent();
		spinner.checkSpinners();
		
		TimeUnit.SECONDS.sleep(30);
//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			WebElement element = null;
//			for (WebElement webElement : els) {
//				driver.switchTo().frame(webElement);
//				try {
//				 element = util.waitAndGetElement(tableDoc);
//				 break;
//				}catch(Exception e) {
//					driver.switchTo().defaultContent();
//				}
//			}
			
			util.getFrameActive();
			//press(PrecheckButton);
					
			
			
			List<WebElement> tables = driver.findElements(tableDoc);
			WebElement table = null;
			for (WebElement tab : tables) {
				if(tab.isDisplayed()) {
					table=tab;
				}
			}
			
			int count = util.getTableRowCount(table);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			for (int i = 1; i<=count;i++) {
				if(util.getTableCellDataWithoutDiv(table, i, "OBBLIGATORIO", "div").equalsIgnoreCase("Y")) {
					String out = util.getTableCellDataWithoutDiv(table, i, "STATO", "div");
					if(!out.equals("VALIDATO")) {
						throw new Exception("Esito MODULO DI ADESIONE:VALIDATO riscontrato: "+out+" alla riga: "+i);
					}
				}
			}
				
			
			
	}
	//vecchio
	

	
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		try {
//		spinner.checkSpinners();
		}catch(Exception e) {}
	}
	
	
	
}
