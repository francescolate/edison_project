package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;

import javassist.bytecode.analysis.Util;



public class LoginSalesforceComponent {
       
       private WebDriver driver;
       private SeleniumUtilities util;
       
         public LoginSalesforceComponent(WebDriver driver) {
         this.driver = driver;
         util = new SeleniumUtilities(this.driver);
         PageFactory.initElements(driver, this);
       }
         
        @FindBy(id ="username")
       private WebElement username;
       
        @FindBy(id = "password")
       private WebElement password;
       
        @FindBy(id = "Login")
       private WebElement submit;
        
        @FindBy(id="error")
        private WebElement error;
       
        @FindBy(how = How.ID, using="logo")
       private WebElement pageLogo;
       
        public void enterUsername(String text) throws Exception{
             
              username.sendKeys(text);
       }
        

        public void enterField(By xpath,String text) throws Exception{
              driver.findElement(xpath).sendKeys(text);

       }
        
        public void pressButton(By xpath) throws Exception{
        	driver.findElement(xpath).click();
        }
       
        
        
        public void enterPassword(String text) throws Exception{
             password.sendKeys(text);
       }
        
   
       
        public void submitLogin() throws Exception{
             submit.click();
       }
       
        public void checkLogoExistance() throws Exception{
             pageLogo.isDisplayed();
       }
       
        public void navigate(String link) throws Exception{
             driver.get(link);
       }
       
        private void fill_LoginData(String username, String password) throws Exception{
             enterUsername(username);
             enterPassword(password);
       }
        
        
        public void killPreviousSessions() throws Exception {
        	if(util.exists(By.xpath("//form//input[@value='Continua']"), 15)){
        		if(util.exists(By.xpath("//form//input[@type='checkbox' and not(@checked)]"), 5)){
        		util.objectManager(By.xpath("//form//input[@type='checkbox' and not(@checked)]"), util.scrollAndClick);
        		}
        		
        		util.objectManager(By.xpath("//form//input[@value='Continua']"), util.scrollAndClick);
        	}
        	
            if(util.exists(By.xpath("//button[@name='checklistConferma']"), 5)) {
            	util.objectManager(By.xpath("//button[@name='checklistConferma']"), util.scrollAndClick);
            }
        	
        }
       

}
