package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupportoCambioPotenzaContatore extends BaseComponent{
	WebDriver driver;
	SeleniumUtilities util;
	
	public By areaRiservataHomepagePath =  By.xpath("//section[@id='content-home']/div[@class='panel panel-default panel-clear']/div[@class='panel-body']/ol[@class='breadcrumb']");
	public By areRiservataHomepageHeading = By.xpath("//section[@id='content-home']//div/h1");
	public By supportoHeader = By.xpath("//header[@id='globalHedaer']//descendant::li[6]/a");
	//public By supportoHeader = By.xpath("//header[@id='globalHedaer']/div[@class='dotcom-header__main']/div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li[7]/a");
	public By supportoHomePagePath = By.xpath("//main[@id='main']//div[@class='image-hero_content-wrapper']/nav");
	public By supportoHomePagePath1 = By.xpath("//a[.='Home']");
	public By supportoHome1PagePath = By.xpath("//span[.='Home']");
	public By supportoHomePagePath2 = By.xpath("//main[@id='main']//li/a[.='supporto']");
	public By supportoHomePagePath3 = By.xpath("//main[@id='main']//li/a[@title='Potenza e Tensione']");
	public By supportoHomePagePath4 = By.xpath("//main[@id='main']//li/a[.='Come entrare in contatto con Enel Energia']");
	public By supportoHomePagePath5 = By.xpath("//span[.='Spazio Enel']");
	public By supportoHeading = By.xpath("//main[@id='main']//descendant::h1[contains(text(),'Hai')]");
	public By supportoTitleText = By.xpath("//main[@id='main']//descendant::p[contains(text(),'Consulta le')]");
	//public By contrattiemodulistica = By.xpath("//main[@id='main']//child::div[2]/li/a");
	public By contrattiemodulistica = By.xpath("//main[@id='main']//descendant::li/a[.='Contratti e modulistica']");
	public By sosLuceeGas = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='SOS luce e gas']");
	public By autolettura = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Autolettura']");
	public By appEnel = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='App Enel']");
	public By gestisciITuoiDati = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Gestisci i tuoi dati']");
	public By modificaGagliaAConsumi = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Modifica taglia a consumi']");
	public By guidaAiServizieTutorial = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Guida ai servizi e tutorial']");
	public By serviziSMS = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Servizi sms']");
	public By modulistica = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Modulistica']");
	public By subentro = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Subentro']");
	public By volutra = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Voltura']");
	public By primaAttivazione = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Prima attivazione']");
	public By modificaPotenzaeTensione = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Modifica potenza e tensione']");
	public By disAttivaZione = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Disattivazione']");
	public By caricaDocumenti = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Carica documenti']");
	public By pianoSalvaBlackOut = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Piano salva black out']");
	public By faqAreaclienti = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Faq Area clienti']");
	public By assicurazioneClientiFinale = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Assicurazione clienti finale']");
	public By contattaci = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Contattaci']");
	public By conciliazionieRisoluzioneDelleControversie = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Conciliazioni e risoluzione delle controversie']");
	public By guidaAllaBollettaLuceGas = By.xpath("//div[@id='e_glossary_title']//child::div/a[.='Guida alla bolletta luce e gas']");
	public By supportoPotenzoTensionePath = By.xpath("//main[@id='main']//div/nav[@class='image-hero_breadcrumbs module--initialized']");
	public By PotenzaeTenzioneTitle = By.xpath("//main[@id='main']//descendant::h1[contains(text(),'Vuoi')]");
	public By potenzaETenzioneText = By.xpath("//main[@id='main']//descendant::p[contains(text(),'Tutto')]");
	public By potenzaContent1 = By.xpath("//main[@id='main']//child::section/p[1]");
	public By potenzaContent2 = By.xpath("//main[@id='main']//child::section/p[2]");
	public By potenzaContent3 = By.xpath("//main[@id='main']//child::section/p[3]");
	public By potenzaContent4 = By.xpath("//main[@id='main']//child::section/p[4]");
	//public By accediAllaTuaText = By.xpath("//div[@id='notLoggedBoxCes']/h3[@class='open']");
	public By accediAllaTuaText = By.xpath("//div[@id='notLoggedBoxCes']/h3[@class='open']");
	public By email = By.xpath("//input[@id='txtUsernameCes']");
	public By password = By.xpath("//input[@id='txtPasswordCes']");
	public By accediButton = By.xpath("//input[@id='accessBtnCes']");
	public By nonRegistratoRegisrtiLink = By.xpath("//form[@id='formNavloginCes']/p[3]");
	public By serviziodisponsibleText =  By.xpath("//div[@id='loggedBoxCes']/div[@class='user-info-wrapper']/div[not(contains(@style,'display:none'))]/b");
	public By VaiallareaclientiButton = By.xpath("//div[@id='loggedBoxCes']/div[@class='user-info-wrapper']/div[not(contains(@style,'display:none'))]/a");
	public By homePageTitle1 = By.xpath("//ol[@class='breadcrumb']/li[1]/a");
	public By homePageTitle2 = By.xpath("//ol[@class='breadcrumb']/li[@class='active']");
	public By homePageTitleDescriptiom = By.xpath("//div[@class='panel-body']/h1");
	public By servizii = By.xpath("//li[@id='menuService']/a");
	public By serviziPath1 = By.xpath("//ol[@class='breadcrumb']/li[.='Area riservata']");
	public By serviziPath2 = By.xpath("//ol[@class='breadcrumb']/li[.='Servizi']");
	public By serviziHeading = By.xpath("//div[@class='panel-body']/h1");
	public By serviziText = By.xpath("//div[@class='heading']/h4");
	//public By modificaPotenzaTensione = By.xpath("//div[@class='single-service'][4]/a/div");
	public By modificaPotenzaTensione = By.xpath("//h1[contains(text(),'Modifica potenza e/o tensione')]");
	public By modficaPotenzaTensionePath1 = By.xpath("//ol[@class='breadcrumb']/li[.='Area riservata']");
	public By modficaPotenzaTensionePath2 = By.xpath("//ol[@class='breadcrumb']/li[.='Fornitura']");
	public By modficaPotenzaTensionePath3 = By.xpath("//ol[@class='breadcrumb']/li[.='Potenza e tensione']");
	public By modificaPotenzaTensioneHeading = By.xpath("//div[@class='panel-body']/h1");
	public By modificaPotenzaTensioneText = By.xpath("//main[@id='main']//div[@class='d-flex']/section//div[@class='panel-body']/p[@class='lead']");
	public By modificaPotenzaTensioneButton = By.xpath("//*[@id='landing-dispositiva']/button");

	
	
	
	
	public SupportoCambioPotenzaContatore(WebDriver driver) throws Exception {
		super(driver);
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	  
	  public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	  
	  public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
	  
	  public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	  
	  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			//System.out.println("Normalized:\n"+weText);
			//System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void containsText(By by, String text) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			weText = we.getText();		
			System.out.println("weText -->" + weText);
			if (!weText.contains(text))
				throw new Exception("There isn't " + text);
		}
		
//		public void checkURLAfterRedirection(String url) throws Exception{
//			if(!url.equals(driver.getCurrentUrl()))
//				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
//		}
		
		public void verifyComponentnotExist(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
//		public void  isElementNotPresent(By existentObject ) throws Exception{
//			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
//			
//			try{
//				WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
//			} 
//	        catch (Exception e) {
//	        	e.printStackTrace();
//	            }
//	         }
	 
	 public String AREA_RISERVATA_HOMEPAGE_PATH = "Area riservataHomepage";
	 public String AREA_RISERVATA_HOMEPAGE_HEADING = "Benvenuto nella tua area dedicata Business";
	 public String SUPPORTO_HOMEPAGE_PATH = "Home / supporto";
	 public String SUPPORTO_HOMEPAGE_PATH1 = "Home"; 
	 public String SUPPORTO_HOMEPAGE_PATH2 = "supporto";
	 public String SUPPORTO_HOMEPAGE_PATH3 = "Potenza e Tensione";
	 public String SUPPORTO_HOMEPAGE_PATH4 = "Come entrare in contatto con Enel Energia";
	 public String SUPPORTO_HOMEPAGE_PATH5 = "Spazio Enel";
	 public String SUPPORTO_HEADING = "Hai bisogno di aiuto?";
	 public String SUPPORTO_TITLE_TEXT = "Consulta le domande più frequenti e scopri tutti i vantaggi dei servizi di Enel Energia.";
	 public String SOS_LUCE_E_GAS = "SOS luce e gas";
	 public String AUTOLETTURA = "Autolettura";
	 public String APP_ENEL = "App Enel";
	 public String GESTISCI_I_TUOI_DATI = "Gestisci i tuoi dati";
	 public String MODIFICA_TAGLIA_A_CONSUMI = "Modifica taglia a consumi";
	 public String GUIDA_AI_SERVIZI_E_TUTORIAL = "Guida ai servizi e tutorial";
	 public String SERVIZI_SMS = "Servizi sms";
	 public String MODULISTICA = "Modulistica";
	 public String SUBENTRO = "Subentro";
	 public String VOLUTRA = "Voltura";
	 public String PRIMA_ATTIVAZIONE = "Prima attivazione";
	 public String MODIFICA_POTENZA_TENSIONE = "Modifica potenza e tensione";
	 public String DISATTIVAZIONE = "Disattivazione";
	 public String CARICA_DOCUMENTI = "Carica documenti";
	 public String PIANO_SALVA_BLACK_OUT = "Piano salva black out";
	 public String FAQ_AREA_CLIENTI = "Faq Area clienti";
	 public String ASSICURAZIONE_CLIENTI_FINALE = "Assicurazione clienti finale";
	 public String CONTATTACI = "Contattaci";
	 public String CONCILIAZIONI_E_RESOLUZIONE = "Conciliazioni e risoluzione delle controversie";
	 public String GUIDA_ALLA_BOLLETTA_LUCE_E_GAS = "Guida alla bolletta luce e gas";
	 public String SUPPORTO_POTENZAETENZIONE_PATH = "Home/supporto/Potenza e Tensione";
	 public String POTENZA_E_TENZIONE_TITLE = "Vuoi modificare la potenza del tuo contatore?";
	 public String POTENZA_E_TENZIONE_TEXT = "Tutto quello che devi sapere per richiedere una modifica della potenza e/o tensione relative alla tua fornitura";
	 public String POTENZA_CONTENT1 = "Se la potenza disponibile del tuo contatore risulta insufficiente alle tue esigenze di consumo, puoi prendere in considerazione di effettuare una richiesta di aumento di potenza. Viceversa, qualora ritieni che la potenza disponibile del tuo contatore sia sovradimensionata rispetto alle tue esigenze, potrai richiedere una diminuzione della potenza.";
	 public String POTENZA_CONTENT2 = "La modifica della potenza (ed, eventualmente, della tensione) del tuo contatore comporta variazioni alle condizioni economiche e, qualora necessario, anche variazioni alle condizioni tecniche della fornitura (ad esempio: una variazione della tensione, un cambio del contatore etc.).";
	 public String POTENZA_CONTENT3 = "Effettua in maniera semplice e veloce l’operazione dalla tua area riservata. Otterrai subito un preventivo di spesa che potrai accettare o annullare comodamente.";
	 public String POTENZA_CONTENT4 = "Questa funzionalità è disponibile solamente per forniture ad uso abitativo.";
	 public String ACCEDI_ALLA_TUA_TEXT = "Accedi alla tua Area Riservata";
	 public String SERVIZIO_DISPONSIBLE_TEXT = "Servizio disponibile solamente per utenti residenziali";
	 
}
