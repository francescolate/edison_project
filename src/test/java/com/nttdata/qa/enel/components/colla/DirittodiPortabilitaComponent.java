package com.nttdata.qa.enel.components.colla;

import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DirittodiPortabilitaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	Properties prop;
	
	public By homepageHeading1 = By.xpath("//div[@id='mainContentWrapper']//descendant::h1");
	public By homepageParagraph1 = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='In questa sezione potrai gestire le tue forniture']");
	public By homepageHeading2 = By.xpath("//div[@id='mainContentWrapper']//descendant::h2[.='Le tue forniture']");
	public By homepageParagraph2 = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='Visualizza le informazioni principali delle tue forniture e consulta le tue bollette']");
	public By homepageParagraph2New = By.xpath("//div[@id='mainContentWrapper']//descendant::p[.='Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.']");
	public By iTuoiDiritti = By.xpath("//a[@id='iTuoiDiritti']/span");
	public By section1PageHeading1 = By.xpath("//section[@id='landing-text']//descendant::h1");
	public By section1PageHeading2 = By.xpath("//section[@id='landing-text']//descendant::h2");
	public By section1PageHeading3 = By.xpath("//section[@id='landing-text']//descendant::h3[.='Diritto di Informazione']");
	public By section1PageHeading4 = By.xpath("//section[@id='landing-text']//descendant::h4[contains(text(),'Come faccio a visualizzare')]");
	public By section1PageParagraph1 = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][1]//p");
	public By privacyPolicy = By.xpath("//section[@id='landing-text']//descendant::a[.='Privacy Policy']");
	public By privacyPolicyHomePagePath1 = By.xpath("//main[@id='main']//li[1]/a[.='Home']");
	public By privacyPolicyHomePagePath2 = By.xpath("//main[@id='main']//li[2]/a[.='supporto']");
	public By privacyPolicyHomePagePath3 = By.xpath("//main[@id='main']//li[3]/a[.='Privacy']");
	public By privacyPolicyHomePageTitle = By.xpath("//main[@id='main']//h1");
	public By privacyPolicyHomePageText = By.xpath("//main[@id='main']//p[@class='image-hero_detail text--detail']");
	public By cookiePolicy = By.xpath("//section[@id='landing-text']//descendant::a[.='Cookie Policy']");
	public By cookiePolicyHomePagePath1 = By.xpath("//main[@id='main']//li[1]/a[.='Home']");
	public By cookiePolicyHomePagePath2 = By.xpath("//main[@id='main']//li[2]/a[.='supporto']");
	public By cookiePolicyHomePagePath3 = By.xpath("//main[@id='main']//li[3]/a[.=' cookie Policy']");
	public By cookiePolicyHomePageTitle = By.xpath("//main[@id='main']//h1");
	public By cookiePolicyHomePageText = By.xpath("//main[@id='main']//p[@class='image-hero_detail text--detail']");
	public By section1H3DirittoDiAccesso = By.xpath("//section[@id='landing-text']//h3[.='Diritto di Accesso']");
	public By section1H4DirittoDiAccesso = By.xpath("//section[@id='landing-text']//descendant::h4[contains(text(),'Come faccio ad avere')]");
	public By section1PageParagraph2 = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][2]//p");
	public By sezioneAccount = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][2]//a[1]");
	public By sezioneAccountOpposizione = By.xpath("//div[@class='faq-element parbase'][5]//descendant::a[.='Sezione Account']");
	public By sezioneAccountRettifica = By.xpath("//div[@class='faq-element parbase'][6]//descendant::a[.='Sezione Account']");
	public By sezioneAccountPortabilita = By.xpath("//div[@class='faq-element parbase'][7]//descendant::a[.='Sezione Account']");
	public By sezioneAccountTitle = By.xpath("//h1[@id='profile-contact']");
	public By sezioneAccountText = By.xpath("//div[@id='mainContentWrapper']//p");
	public By sezioneAccountOpposizioneText = By.xpath("//div[@id='mainContentWrapper']//p[1]");
	public By sezioneAccountRettificaText = By.xpath("//div[@id='mainContentWrapper']//p[1]");
	public By sezioneAccountPortabilitaText = By.xpath("//div[@id='mainContentWrapper']//p[1]");
	public By popupHeading = By.xpath("//h3[@id='titleModal']");
	public By popupParagraph = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
	public By popupNoButton = By.xpath("//button[@id='overlayNoButton']");
	public By popupSIButton = By.xpath("//button[@id='overlayYesButton']");
	public By addebitoDiretto = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][2]//a[2]");
	public By addebitoDirettoTitle = By.xpath("//h2[@id='profile-contact']");
	public By addebitoDirettoText = By.xpath("//section[@id='landing-text']//p[contains(text(),'Il servizio Addebito Diretto ti')]");
	public By selezionandoUnaTuaFornitura = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][2]//a[3]");
	public By dirittoDiPortabilitaTitle1 = By.xpath("//section[@id='landing-text']//h3[.='Diritto di Portabilità']");
	public By dirittoDiPortabilitaTitle2 = By.xpath("//section[@id='landing-text']//h4[contains(text(),'Come posso richiedere')]");
	public By dirittoDiPortabilitaText = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][7]//p");
	public By accediAlServizoButton = By.xpath("//div[@id='landing-dispositiva']/button");
	public By indirizzoDellaFornitura = By.xpath("//div[@id='sc']//span[@class='indirizzo']/span[@class='valore']");
	public By numeroCliente  = By.xpath("//section[@id='landing-text']//span[4]/span[@class='valore']");
	public By dirittoDiPortabilityPageTitle = By.xpath("//h1[@id='titolo-dispositiva']");
	public By dirittoDiPortabilityPageText = By.xpath("//div[@id='sc']/div//h2");
	public By dirittoDiCancellationTitle1 = By.xpath("//section[@id='landing-text']//h3[.='Diritto di Cancellazione']");
	public By dirittoDiCancellationTitle2 = By.xpath("//section[@id='landing-text']//h4[contains(text(),'È possibile effettuare la cancellazione')]");
	public By dirittoDiCancellationText1 = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][3]/div[@class='faq-appearance-group']/p[1]");
	public By dirittoDiCancellationText2 = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][3]//p[2]");
	public By dirittoDiCancellationText3 = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][3]//li");
	public By dirittoDiLimitazioneText= By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][4]/div[@class='faq-appearance-group']");
	public By dirittoDiOpposizioneText= By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][5]/div[@class='faq-appearance-group']");
	public By sezioneAccountDirittoDiOpposizione = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][5]//a");
	public By checkbox1 = By.xpath("(//div[@id='sc']//div/label//span[@class='icon-ckbox'])[1]");
	public By checkbox2 = By.xpath("(//div[@id='sc']//div/label//span[@class='icon-ckbox'])[2]");
	public By scaricaFile = By.xpath("//div[@id='sc']//div[@class='label-container luce'][1]//a");
	public By buttonRichediITuoiDati = By.xpath("//button[.='RICHIEDI I TUOI DATI']");
	public By buttonEsci = By.xpath("//button[.='ESCI']");
	public By buttonFine = By.xpath("//div[@id='sc']//descendant::button[.='FINE']");
	public By sectionText = By.xpath("//div[@id='sc']//descendant::div[@class='resultBox-group']");
	public By servizi = By.xpath("//a[@id='servizi']//span");
	public By popupCloseButton = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By dirittoDiRettificaText = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][6]/div[@class='faq-appearance-group']");
	public By dirittoDipotabilitaText = By.xpath("//section[@id='landing-text']//div[@class='faq-element parbase'][7]/div[@class='faq-appearance-group']");
	public By accediAlServizioButton = By.xpath("//div[@id='landing-dispositiva']/button[.='ACCEDI AL SERVIZIO']");
	public By footerDisplay = By.xpath("//footer[@id='footer']/div");
	public By headerDisplay = By.xpath("//header[@id='globalHedaer']");
	public By cambioPianoPageDescription = By.xpath("//h1[text()='Cambio piano']/following-sibling::h2"); 
	public By cambioPianoButton = By.xpath("//span[text()='CAMBIO PIANO']");
	public By cambioPianoContinueButton = By.xpath("//span[text()='CONTINUA']");
	public By cambioPianoConfirmButton = By.xpath("//span[text()='CONFERMA']");
	public By cambioPianoFineButton = By.xpath("//span[text()='FINE']");
	public By serviziHeading = By.xpath("//section[@id='forniture-id0']//h2");
	public By serviziText = By.xpath("//section[@id='forniture-id0']//p[2]");
	
	public By cambioPianoPlanText1 = By.xpath("//h1[text()='Cambio piano']"); 
	public By cambioPianoPlanText2 = By.xpath("//h2[text()='Stai cambiando il piano della tua fornitura']");
	public By cambioPianoPlanText3 = By.xpath("//span[text()='Attualmente hai attivo questo piano:']");
	public By cambioPianoPlanText4 = By.xpath("//legend[text()='Puoi cambiare il piano con:']");
	public By cambioPianoPlanText5 = By.xpath("//span[text()='Il piano che hai scelto è il seguente:']");
	public By cambioPianoPlanText6 = By.xpath("//h3[text()='Operazione eseguita correttamente']");
	public By cambioPianoPlanText7 = By.xpath("//p[text()='La tua richiesta è stata presa in carico. A partire dal prossimo mese verrà applicato il nuovo piano che hai scelto.']");
	
	public By dettaglioFornituraLink = By.xpath("//a[contains(text(),'Dettaglio fornitura')]");
	
	public DirittodiPortabilitaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
  
  public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
  
  public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
  
  public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
  
  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void  isElementNotPresent(By existentObject ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		
		try{
			WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
		} 
        catch (Exception e) {
        	e.printStackTrace();
        		            
            }
         }
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
		

public void SwitchTabToSupporto(String Url) throws Exception
{
    String currentHandle = driver.getWindowHandle();
    Set <String> windows = driver.getWindowHandles();
   
   
    for (String winHandle : windows) {
       
        String pagetitle = driver.getTitle();
        driver.switchTo().window(winHandle);
     }
    	checkURLAfterRedirection(Url);
  }

public void SwitchTabToOriginalWindow() throws Exception
{
	String currentHandle = driver.getWindowHandle();
	driver.switchTo().window(currentHandle);
}
	
public void isElementDisabled(By existentObject) throws Exception{
	
	if ((driver.findElement(existentObject).isEnabled()))
			
		throw new Exception("The button is Enabled");
		}

public void isElementEnabled(By existentObject) throws Exception{
	
	if (!(driver.findElement(existentObject).isEnabled()))
			
		throw new Exception("The button is Enabled");
		}

public void switchToNewTab() throws Exception {
	Set <String> windows = driver.getWindowHandles();
	for (String winHandle : windows) {
		driver.switchTo().window(winHandle);
	}
}

	public static final String HOMEPAGE_HEADING1 = "Benvenuto nella tua area privata";
	public static final String HOMEPAGE_PARAGRAPH1 = "In questa sezione potrai gestire le tue forniture";
	public static final String HOMEPAGE_HEADING2 = "Le tue forniture";
	public static final String HOMEPAGE_PARAGRAPH2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette";
	public static final String HOMEPAGE_PARAGRAPH2New = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String SECTION1_HEADING1 = "Come posso esercitare i diritti sui miei dati personali in base a quanto stabilito dal General Data Protection Regulation (GDPR)?";
	public static final String SECTION1_HEADING2 = "Se vuoi esercitare uno o più diritti riconosciuti dal GDPR puoi farlo seguendo le indicazioni riportate di seguito.";
	public static final String SECTION1_HEADING3 = "Diritto di Informazione";
	public static final String SECTION1_HEADING4 = "Come faccio a visualizzare le informative sulla privacy e consultare la politica sui cookie?";
	public static final String SECTION1_PARAGRAPH1 = "È possibile consultare le policy tramite i link Privacy Policye Cookie Policy";
	public static final String URL_PRIVACY_POLICY= "https://www-coll1.enel.it/it/supporto/faq/privacy";
	public static final String URL_COOKIE_POLICY= "https://www-coll1.enel.it/it/supporto/faq/cookie-policy";
	public static final String PRIVACY_POLICY_HOMEPAGE_PATH1 = "Home";
	public static final String PRIVACY_POLICY_HOMEPAGE_PATH2 = "supporto";
	public static final String PRIVACY_POLICY_HOMEPAGE_PATH3 = "Privacy";
	public static final String PRIVACY_POLICY_HOMEPAGE_TITLE = "Privacy";
	public static final String PRIVACY_POLICY_HOMEPAGE_TEXT = "Le società Enel Energia S.p.A. ed Enel Italia S.p.A. (di seguito, complessivamente, le “Società”), in qualità di titolari autonomi del trattamento, informano che i dati personali degli interessati verranno trattati nel rispetto delle disposizioni previste dal REGOLAMENTO UE 2016/679 (“GDPR”) per le finalità e con le modalità indicate nelle rispettive Informative privacy.";
	public static final String COOKIE_POLICY_HOMEPAGE_PATH1 = "Home";
	public static final String COOKIE_POLICY_HOMEPAGE_PATH2 = "supporto";
	public static final String COOKIE_POLICY_HOMEPAGE_PATH3 = "cookie Policy";
	public static final String COOKIE_POLICY_HOMEPAGE_TITLE = "Cookie policy";
	public static final String COOKIE_POLICY_HOMEPAGE_TEXT = "Informativa estesa sull’uso dei Cookie";
	public static final String DIRITTO_DI_ACCESSO_H3 = "Diritto di Accesso";
	public static final String DIRITTO_DI_ACCESSO_H4 = "Come faccio ad avere accesso a tutte le informazioni che ho fornito ad Enel Energia?";
	public static final String SECTION1_PARAGRAPH2 = "È possibile visualizzare i tuoi dati personali e i dati di contatto nellaSezione Account.Per poter accedere ai dati di pagamento puoi consultare il servizio diAddebito Direttoqualora fosse attivo su almeno una delle tue forniture.Per consultare i dati della tua fornitura è possibile trovare queste informazioniselezionando una tua fornituraed accedendo al relativo dettaglio. Qualora avessi forniture in attivazione potrai vedere le informazioni sullo stato di avanzamento della fornitura in lavorazione.";
	public static final String SEZIONE_ACCOUNT_TITLE = "Dati e Contatti";
	public static final String SEZIONE_ACCOUNT_TEXT = "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione";
	public static final String POPUP_HEADING = "Attenzione";
	public static final String POPUP_PARAGRAPH = "Sei sicuro di voler uscire da questa sezione?";
	public static final String ADDEBITO_DIRETTO_TITLE = "Addebito Diretto";
	public static final String ADDEBITO_DIRETTO_TEXT = "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sulla tua Carta di pagamento, sul tuo Conto Corrente o sul tuo conto Paypal.";
	public static final String DIRITTO_DI_PORTABILITA_TITLE1 = "Diritto di Portabilità";
	public static final String DIRITTO_DI_PORTABILITA_TITLE2 = "Come posso richiedere una copia dei miei dati forniti ad Enel Energia?";
	public static final String DIRITTO_DI_PORTABILITA_TEXT = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailyxavepydd-2370@yopmail.com.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String DIRITTO_DI_PORTABILITA_TEXT_196 = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailprova@prova.it.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String DIRITTO_DI_PORTABILITA_TEXT_198 = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailyxavepydd-2370@yopmail.com.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String DIRITTO_DI_PORTABILITA_TEXT_199 = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla emailandrea2.giaffreda@enel.com.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String INDIRIZZO_DELLA_FORNITURA = "Via Foro Romano 2 00186 Roma Roma Rm";
	public static final String INDIRIZZO_DELLA_FORNITURA_196 = "Via Abate Di Tivoli 2 00131 Roma Roma Rm";
	public static final String NUMERO_CLIENTE = "310494829";
	public static final String NUMERO_CLIENTE_196 = "310505200";
	public static final String INDIRIZZO_DELLA_FORNITURA_198 = "Via Abate Di Tivoli 2 00131 Roma Roma Rm";
	public static final String INDIRIZZO_DELLA_FORNITURA_199 = "Via Italia 30 89122 Reggio Calabria Reggio Calabria Rc";
	public static final String NUMERO_CLIENTE_198 = "310505200";
	public static final String DIRITTO_DI_PORTABILITA_PAGE_TITLE ="Diritto di Portabilità";
	public static final String DIRITTO_DI_PORTABILITA_PAGE_TEXT ="Seleziona una o più forniture sulla quale intendi richiedere una copia dei tuoi dati.";
	public static final String DIRITTO_DI_CANCELLATION_TITLE1 = "Diritto di Cancellazione";
	public static final String DIRITTO_DI_CANCELLATION_TITLE2 = "È possibile effettuare la cancellazione dei dati personali forniti da me a Enel Energia?";
	public static final String DIRITTO_DICANCELLATION_TEXT1 = "Hai il diritto di ottenere dal titolare del trattamento la cancellazione dei dati personali che ti riguardano senza ingiustificato ritardo e il titolare del trattamento ha l’obbligo di cancellare senza ingiustificato ritardo i dati personali, se sussiste uno dei motivi seguenti:";
	public static final String DIRITTO_DICANCELLATION_TEXT2 = "Per esercitare il diritto puoi scrivere tramite email al seguente indirizzo:privacy.enelenergia@enel.com";
	public static final String DIRITTO_DICANCELLATION_TEXT3 ="I dati personali non sono più necessari rispetto alle finalità per le quali sono stati raccolti o altrimenti trattati;";
	public static final String DIRITTO_DI_LIMITAZIONE_TEXT = "Diritto di Limitazione del Trattamento È possibile limitare l’utilizzo dei miei dati conservati da Enel Energia? Hai il diritto di ottenere dal titolare la limitazione del trattamento quando ricorre una delle seguenti casistiche:L’interessato contesta l’esattezza dei dati personali;Il trattamento è illecito e l’interessato si oppone alla cancellazione dei dati personali e chiede invece che ne sia limitato l’utilizzo;I dati personali sono necessari all’interessato per l’accertamento, l’esercizio o la difesa di un diritto in sede giudiziaria.Per esercitare il diritto puoi scrivere tramite email al seguente indirizzo:privacy.enelenergia@enel.com";
	public static final String DIRITTO_DI_OPPOSIZIONE_TEXT = "Diritto di Opposizione Come posso gestire i miei consensi marketing e profilazione? Puoi aggiornare i tuoi consensi marketing e profilazione accedendo allaSezione Account";
	public static final String SECTION_TEXT = "Operazione eseguita correttamenteRiceverai entro 30 giorni una email contenente una copia dei tuoi dati.Quando disponibili i tuoi dati saranno anche scaricabili accedendo nuovamente al servizio.";
	public static final String DIRITTO_RETTIFICA = "Diritto di Rettifica Come posso modificare le informazioni che ho fornito ad Enel Energia? NellaSezione Accountè possibile consultare i tuoi dati personali e modificare i tuoi consensi, i tuoi dati di contatto e dati di registrazione.Se hai attivo l’Addebito Direttopuoi consultare e modificare i dati.";
	public static final String DIRITTO_PORTABILITA = "È possibile richiedere una copia dei tuoi dati tramite il nostro servizio dedicato.Se invece hai già effettuato una richiesta, puoi entrare nella sezione per scaricare i tuoi dati. I dati ti verranno inviati alla email#email#.Se desideri aggiornarla o l'email risulta \"non presente\" vai alla Sezione Accountprima di procedere.";
	public static final String FOOTER_DISPLAY ="footer © Enel Energia S.p.a. Tutti i Diritti Riservati Gruppo IVA Enel P.IVA 15844561009 Informazioni Legali Privacy Credits Contattaci Twitter Facebook Youtube Instagram Telegram Linkedin";
	public static final String CambioPianoPageDescription = "Con l'offerta flessibile Scegli tu hai la possibilità di modificare il tuo piano tariffario fino ad un massimo di 3 volte nell’arco dei primi 10 mesi di ogni anno contrattuale. Se hai modificato le abitudini di consumo oppure ti sei accorto che le tue abitudini non sono in linea con il piano scelto in fase di sottoscrizione, grazie a questo servizio puoi cambiare il tuo piano tariffario in modo gratuito e veloce.  Per conoscere il prezzo attualmente applicato sul tuo piano e i prezzi degli altri piani avvia la richiesta di Cambia Piano.";
	public static final String CambioPianoPageDescriptionNew = "Con l'offerta flessibile Scegli tu hai la possibilità di modificare il tuo piano tariffario fino ad un massimo di 3 volte nell’arco dei primi 10 mesi di ogni anno contrattuale. Se hai modificato le abitudini di consumo oppure ti sei accorto che le tue abitudini non sono in linea con il piano scelto in fase di sottoscrizione, grazie a questo servizio puoi cambiare il tuo piano tariffario in modo gratuito e veloce. Per conoscere il prezzo attualmente applicato sul tuo piano e i prezzi degli altri piani avvia la richiesta di Cambia Piano.";
	public static final String URL_chiamaci= "https://www-coll1.enel.it/it/area-clienti/residenziale/chiamaci";
	public static final String SERVIZI_HEADING ="Servizi per le forniture";
	public static final String SERVIZI_TEXT="Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
	public static final String CambioPianoPlanText1 = "Cambio piano";
	public static final String CambioPianoPlanText2 = "Stai cambiando il piano della tua fornitura";
	public static final String CambioPianoPlanText3 = "Attualmente hai attivo questo piano:";
	public static final String CambioPianoPlanText4 = "Puoi cambiare il piano con:";
	public static final String CambioPianoPlanText5 = "Il piano che hai scelto Ã¨ il seguente:";
	public static final String CambioPianoPlanText5New = "Il piano che hai scelto è il seguente:";
	public static final String CambioPianoPlanText6 = "Operazione eseguita correttamente";
	public static final String CambioPianoPlanText7 = "La tua richiesta Ã¨ stata presa in carico. A partire dal prossimo mese verrÃ  applicato il nuovo piano che hai scelto.";
	public static final String CambioPianoPlanText7New = "La tua richiesta è stata presa in carico. A partire dal prossimo mese verrà applicato il nuovo piano che hai scelto.";
	
	
}


