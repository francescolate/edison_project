package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ScegliTu100x100HelpComponent extends BaseComponent {
	public By offerDetailBtn = By.xpath("//a[starts-with(@aria-label,'Dettaglio Offerta Scegli Tu 100x100')]");
	public By activateNowBtn = By.xpath("//a[contains(@href,'scegli-tu-100x100')]//button[contains(text(),'Attiva ora')]");
	//public By startNowBtn = By.cssSelector("#container-footer-hero-button-id > div > div > a");
	public By startNowBtn = By.xpath("//*[@id='container-footer-hero-button-id']/div/div/a");
	public By findYourSolutionLbl = By.xpath("//div[@class='cmSupportTool_claim']");
	public By counterLabel = By.xpath("//div[contains(@class,'visible')]/div[@class='cmSupportTool_question-number']");
	public By questionTitle = By.xpath("//div[contains(@class,'visible')]//div[@class='cmSupportTool_question']");
	public By questionSubtitle = By.xpath("//div[contains(@class,'visible')]//div[@class='cmSupportTool_text']");
	public By answerA_Lbl = By.xpath("//div[contains(@class,'visible')]//li[1]//p");
	public By answerB_Lbl = By.xpath("//div[contains(@class,'visible')]//li[2]//p");
	public By answerC_Lbl = By.xpath("//div[contains(@class,'visible')]//li[3]//p");
	public By answerB_Btn = By.xpath("//div[contains(@class,'visible') and contains(@class,'cmSupportTool_block')]//li[2]");
	public By answerC_Btn = By.xpath("//div[contains(@class,'visible') and contains(@class,'cmSupportTool_block')]//li[3]");
	public By resultTitle = By.xpath("//span[@class='cmSupportTool_result-title']");
	public By resultSubtitle = By.xpath("//p[@class='cmSupportTool_result-text']");
	public By summaryTitle = By.xpath("//div[@class='title']");
	public By summarySubtitle = By.xpath("//p[@class='text-intro']");
	public By summaryDescription = By.xpath("//p[@class='text-description']");
	public By firstPrice = By.xpath("//p[@class='text-description']//span[@class='price'][1]");
	public By secondPrice = By.xpath("//p[@class='text-description']//span[@class='price'][2]");
	public By resultActivationBtn = By.xpath("//a[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-bioraria' and text()='Attiva ora']");
	
	public ScegliTu100x100HelpComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyQuestionsOverlay(String header, String counter, String title, String subtitle, String firstOption, String secondOption, String thirdOption) throws Exception {
		verifyComponentExistence(findYourSolutionLbl);
		verifyComponentText(findYourSolutionLbl, header);
		verifyComponentExistence(counterLabel);
		verifyComponentText(counterLabel, counter);
		verifyComponentExistence(questionTitle);
		verifyComponentText(questionTitle, title);
		if (subtitle != null) {
			verifyComponentExistence(questionSubtitle);
			verifyComponentText(questionSubtitle, subtitle);
		}
		verifyComponentExistence(answerA_Lbl);
		verifyComponentText(answerA_Lbl, firstOption);
		verifyComponentExistence(answerB_Lbl);
		verifyComponentText(answerB_Lbl, secondOption);
		verifyComponentExistence(answerC_Lbl);
		verifyComponentText(answerC_Lbl, thirdOption);
	}
}
