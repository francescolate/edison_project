package com.nttdata.qa.enel.components.colla;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupportPrivacyComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By privacyTitle = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By privacyText = By.xpath("//p[@class='image-hero_detail text--detail']");
	public By privacyEnegrgiaHeader = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown']/h3[@class='anchor plan-main-head']");
	public By privacyEnergiaText1 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[1]/button");
	public By privacyEnergiaText2 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[2]/button");
	public By privacyEnergiaText3 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[3]/button");
	public By privacyEnergiaText4 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[4]/button");
	public By privacyEnergiaText5 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[5]/button");
	public By privacyEnergiaText6 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[6]/button");
	public By privacyEnergiaText7 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[7]/button");
	public By privacyEnergiaText8 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[8]/button");
	public By privacyEnergiaText9 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[9]/button");
	public By privacyEnergiaText10 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[10]/button");
	public By privacyEnergiaText11 = By.xpath("//div[@class='dropdown parbase'][1]/section[@class='9 content-container-dropdown dropdown detail-accordian all-details module--initialized']/section[11]/button");
	public By privacyItaliaHeader = By.xpath("//div[@class='dropdown parbase'][2]/section[@class='9 content-container-dropdown']/h3[@class='anchor plan-main-head']");
	public By privacyItaliaText1 = By.xpath("//div[@class='dropdown parbase'][2]//section[1]/button");
	public By privacyItaliaText2 = By.xpath("//div[@class='dropdown parbase'][2]//section[2]/button");
	public By privacyItaliaText3 = By.xpath("//div[@class='dropdown parbase'][2]//section[3]/button");
	public By privacyItaliaText4 = By.xpath("//div[@class='dropdown parbase'][2]//section[4]/button");
	public By privacyItaliaText5 = By.xpath("//div[@class='dropdown parbase'][2]//section[5]/button");
	public By privacyItaliaText6 = By.xpath("//div[@class='dropdown parbase'][2]//section[6]/button");
	public By privacyItaliaText7 = By.xpath("//div[@class='dropdown parbase'][2]//section[7]/button");
	public By privacyItaliaText8 = By.xpath("//div[@class='dropdown parbase'][2]//section[8]/button");
	public By privacyItaliaText9 = By.xpath("//div[@class='dropdown parbase'][2]//section[9]/button");
	public String currentHandle ="";
	public String Result ="NO";
	public String url = "https://www.enel.it/it/supporto/faq/privacy";
	public String urlCredential = "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@"+"www-coll1.enel.it/it/supporto/faq/privacy";
	public By path = By.xpath("//ul[@class='breadcrumbs component']");
	
	public static final String PRIVACY_ENERGIA_FAQ = "Titolare e responsabile del trattamento dei dati personali Responsabile della protezione dei dati personali (RPD)"
			+ "	Oggetto e modalità del trattamento Finalità e base giuridica del trattamento Registrazione all'area riservata e social login"
			+ "	Finalità di marketing e/o profilazione Destinatari dei dati personali Trasferimento dei dati personali"
			+ "	Periodo di conservazione dei dati personali Diritti degli interessatiUtilizzo dei Cookie";
	
	public static final String PRIVACY_ITALIA_FAQ = "Titolare del trattamento dei Dati Personali;Responsabile della Protezione dei dati (RPD);Oggetto e Modalità del trattamento"
			+ "Finalità e base giuridica del trattamento;Destinatari dei Dati Personali;Trasferimento dei Dati Personali;"
			+ "Periodo di conservazione dei Dati Personali;Diritti degli interessati;Utilizzo dei cookie";
	public SupportPrivacyComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.equalsIgnoreCase(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
				}
		
	public void checkForText(By checkObject, String property) throws Exception {
		
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);
		
	}
	
	public void SwitchTabToPrivacy() throws Exception
	{
	   
		Set <String> windows = driver.getWindowHandles();
		 currentHandle = driver.getWindowHandle();
		
		for (String winHandle : windows) {
		   // Switch to child window
		//System.out.println("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
		 			
			verifyComponentExistence(privacyTitle);
			
			util.scrollToElement(driver.findElement(privacyEnegrgiaHeader));
			verifyComponentExistence(privacyEnergiaText1);
			verifyComponentExistence(privacyEnergiaText2);
			verifyComponentExistence(privacyEnergiaText3);
			verifyComponentExistence(privacyEnergiaText4);
			verifyComponentExistence(privacyEnergiaText5);
			verifyComponentExistence(privacyEnergiaText6);
			verifyComponentExistence(privacyEnergiaText7);
			verifyComponentExistence(privacyEnergiaText8);
			verifyComponentExistence(privacyEnergiaText9);
			verifyComponentExistence(privacyEnergiaText10);
			verifyComponentExistence(privacyEnergiaText11);


			
			String text1 = driver.findElement(privacyEnergiaText2).getText();
		/*//	System.out.println("text "+text1 +driver.findElement(privacyEnergiaText3).getText()+driver.findElement(privacyEnergiaText4).getText()+
					driver.findElement(privacyEnergiaText5).getText()+driver.findElement(privacyEnergiaText6).getText()+
					driver.findElement(privacyEnergiaText7).getText()+driver.findElement(privacyEnergiaText8).getText()+
					driver.findElement(privacyEnergiaText9).getText()+driver.findElement(privacyEnergiaText10).getText()+driver.findElement(privacyEnergiaText11).getText());*/
			
			if(PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText1).getText()) && PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText2).getText())
					&& PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText3).getText()) && PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText4).getText())
					&& PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText5).getText()) && PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText6).getText())
					&& PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText7).getText()) && PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText8).getText())
					&& PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText9).getText()) && PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText10).getText())
					&& PRIVACY_ENERGIA_FAQ.contains(driver.findElement(privacyEnergiaText11).getText())){
				
				Result = "YES";
				
			}
			
			if(Result.equals("NO"))throw new Exception ("Privacy Faq is not matching");

			util.scrollToElement(driver.findElement(privacyItaliaHeader));

			if(PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText1).getText()) && PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText2).getText())
					&& PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText3).getText()) && PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText4).getText())
					&& PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText5).getText()) && PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText6).getText())
					&& PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText7).getText()) && PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText8).getText())
					&& PRIVACY_ITALIA_FAQ.contains(driver.findElement(privacyItaliaText9).getText())){
				
				Result = "YES";
			}
			
			if(Result.equals("NO"))throw new Exception ("Privacy Faq is not matching");
	 
			driver.close();
			driver.switchTo().window(currentHandle);

	 }
	
	public static final String Privacy_Text = "Le società Enel Energia S.p.A. ed Enel Italia S.p.A. (di seguito, complessivamente, le “Società”), in qualità di titolari autonomi del trattamento, informano che i dati personali degli interessati verranno trattati nel rispetto delle disposizioni previste dal REGOLAMENTO UE 2016/679 (“GDPR”) per le finalità e con le modalità indicate nelle rispettive Informative privacy.";
	public static final String Path = "Home/supporto/Privacy";
}
