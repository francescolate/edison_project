package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivatoBollettaWebID321Component {

	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By visualizzaLeBollette_button = By.xpath("(//dd[text()='Attiva']/ancestor::*/following-sibling::div/a[contains(text(),'Visualizza le bollette')])[1]");
	public By dettaglioFornitura_button = By.xpath("(//dd[text()='Attiva']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')])[1]");
	
	//servizi
	
	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
	public By serviziPage = By.xpath("//h2[text()='Servizi per le forniture']");
	public By serviziSectionTitle = By.xpath("//h2[text()='Servizi per le bollette']");
	public By BollettaWebTile = By.xpath("//li[@class='tile']//h3[text()='Bolletta Web']/parent::div");
	public By BollettaWebPageTitle = By.xpath("//div[@class='section-heading']/h2");
	
	public By modificaBollettaWeb_button = By.xpath("//span[contains(text(),'MODIFICA ')]/parent::button");
	public By modificaBollettaWeb_Title = By.xpath("//h1[@id='h1_bollettaweb']");
	public By modificaBollettaWeb_TitleSubtext = By.xpath("//h1[@id='h1_bollettaweb']/following-sibling::h2");
	
	public By supplySelect = By.xpath("//span[@class='icon-ckbox']");
	
	// Disattivazione Process
	
	public By DisattivazioneSteps = By.xpath("//li[contains(@class,'step')]");
	
	public By Inserimento_dati = By.xpath("//li[contains(@class,'step')]/descendant::span[text()='Inserimento dati']");
	public By Riepilogo_e_conferma_dati = By.xpath("//li[contains(@class,'step')]/descendant::span[text()='Riepilogo e conferma dati']");
	public By Esito = By.xpath("//li[contains(@class,'step')]/descendant::span[text()='Esito']");
	
	public By emailInputField = By.xpath("//input[@id='emailFieldInput']");
	public By confermaEmailInputField = By.xpath("//input[@id='emailFieldConfirmInput']");
	
	public By cellulareInputField = By.xpath("//input[@id='mobileFieldInput']");
	public By confermaCellulareInputField = By.xpath("//input[@id='mobileFieldConfirmInput']");
	
	public By CONTINUA_Button = By.xpath("//span[text()='CONTINUA']/parent::button");
	public By continuaButton = By.xpath("//span[text()='Continua']/parent::button");
	public By backButton = By.xpath("//span[text()='INDIETRO']");
	public By confermaButton = By.xpath("//div[@id='sc']//button[.='Conferma']");
	public By finebutton = By.xpath("//div[@id='sc']//button[.='Fine']");
	
	public By fornitureSelect = By.xpath("//a/descendant::span[text()='Forniture']");
	
	public By dailogTitle = By.xpath("//h3[text()='Attenzione']");
	public By dailog_No = By.xpath("//button[@id='overlayNoButton']");
	public By dailog_Yes = By.xpath("//button[@id='overlayYesButton']");
	public By dailogClose = By.xpath("(//button[@onClick='focusCloseModale()'])[1]");
	
	ColorUtils c = new ColorUtils();
	
	
	
	public PrivatoBollettaWebID321Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}
	
	public void validateDisattivazioneSteps() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 Inserimento dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{

		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));

		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
    }
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public static final String servicesColor = "Crimson";
	public static final String errorColor = "Red";
	public static final String successColor = "Teal";
	
	public static final String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
}

