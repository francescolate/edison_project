package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CheckPrivateAreaBSNComponent {

		WebDriver driver;
		SeleniumUtilities util;
		public List<WebElement> menuItemsList = null;
		public String itemXpath = "//ul[@id='internal-menu']//span[text()='$itemName$']/parent::a";
		public String itemBSNXpath = "//ul//a[text()='$itemName$']";
		public By orderedList = By.xpath("//a[contains(@class,'href-sf-prevent-default inline-icon') and not(@style)]");
		public By orderedBSNList = By.xpath("//div[@class='user-details']/following-sibling::div//ul//li[not(contains(@class, 'hide'))]/a");
	    
	    public CheckPrivateAreaBSNComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
		}

		public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void verifyMenuItemsPresenceAndOrder(String menuItemsString) throws Exception {
			populateMenuItemsVector(menuItemsString);
			List<WebElement> orderedWebElements = util.waitAndGetElements(this.orderedList);
			if(orderedWebElements.size()==0)
				throw new Exception("no menu items found using the xpath selector.");
			if(orderedWebElements.size()!=menuItemsList.size())
				throw new Exception("actual menu size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
			for(int i=0;i<orderedWebElements.size();i++){
				if(!orderedWebElements.get(i).equals(menuItemsList.get(i)))
					throw new Exception("You were looking for:\n\t"+menuItemsList.get(i)+"\nwhile the actual item value is:\n\t"+orderedWebElements.get(i));
			}
		}
		
		private void populateMenuItemsVector(String menuItemsString) throws Exception {
			menuItemsList = new ArrayList<WebElement>();
			String[] items = menuItemsString.split(";");
			for(String item : items)
				menuItemsList.add(util.waitAndGetElement(By.xpath(itemXpath.replace("$itemName$", item))));
		}
		
		public void verifyBSNMenuItemsPresenceAndOrder(String menuItemsString) throws Exception {
			populateBSNMenuItemsVector(menuItemsString);
			List<WebElement> orderedWebElements = driver.findElements(orderedBSNList) ;
			if(orderedWebElements.size()==0)
				throw new Exception("no menu items found using the xpath selector.");
			if(orderedWebElements.size()!=menuItemsList.size())
				throw new Exception("actual menu size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
			for(int i=0;i<orderedWebElements.size();i++){
				if(!orderedWebElements.get(i).getText().equals(menuItemsList.get(i).getText()))
						throw new Exception("You were looking for:\n\t"+menuItemsList.get(i)+"\nwhile the actual item value is:\n\t"+orderedWebElements.get(i));
			}
		}
		
		private void populateBSNMenuItemsVector(String menuItemsString) throws Exception {
			menuItemsList = new ArrayList<WebElement>();
			String[] items = menuItemsString.split(";");
			for(String item : items)
				menuItemsList.add(util.waitAndGetElement(By.xpath(itemBSNXpath.replace("$itemName$", item))));
		}
		
		public void checkDocumentReadyState() throws Exception{
			util.checkPageLoaded();
			Thread.sleep(5000);
		}
		
		public static final String PRIVATE_AREA_MENU_RES = "Forniture;Bollette;Servizi;ENELPREMIA WOW!;Novità;Spazio Video;Chiamaci;Porta i tuoi amici;Account;English handbook;I tuoi diritti;Supporto;Trova Spazio Enel.;Esci";
		public static final String PRIVATE_AREA_MENU_BSN = "HomePage;bollette;forniture;servizi;messaggi;stato richieste;report billing management;carica documenti;English handbook;logout";
		public static final String PRIVATE_AREA_MENU = "HomePage;bollette;forniture;servizi;messaggi;stato richieste;report billing management;carica documenti;contatti;i tuoi diritti;logout";
		public static final String PRIVATE_AREA_MENU_LIKE_RES = "HomePage;bollette;forniture;servizi;messaggi;stato richieste;report billing management;area clienti casa;carica documenti;contatti;i tuoi diritti;logout";
	}

