package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class HamburgerMenuComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By rightMenu=By.xpath("//div[@class='dotcom-megamenu__nav__body megamenu-nav-body']");
	public By leftMenu=By.xpath("//nav[@class='megamenu__navSecondary' ]");
	public By centralMenu=By.xpath("//div[@class='dotcom-megamenu__news' ]");
	public By altriSitiLabel=By.xpath("//nav[@class='megamenu__navSecondary']//div[@class='dotcom-megamenu__accordian secondaryMenu publishmode open']//div[normalize-space(text())='Altri Siti']");
	public By openFiberLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Open Fiber']");
	public By enelcuoreLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Enel Cuore']");
	public By sponsorizzazioniLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Sponsorizzazioni']");
	public By negoziazionepariteticaLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Negoziazione paritetica']");
	public By sitiGlobaliLabel=By.xpath("//nav[@class='megamenu__navSecondary']//div[@class='dotcom-megamenu__accordian secondaryMenu publishmode open']//div[normalize-space(text())='Siti Globali']");
	public By enelGroupLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[normalize-space(text())='Enel Group']");
	public By enelGreenPowerLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[normalize-space(text())='Enel Green Power']");
	public By enelXLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Enel X']");
	public By globalTradingLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[normalize-space(text())='Global Trading']");
	public By enelStartupLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Enel Startup']");
	public By globalProcurementLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Global Procurement']");
	public By openInnovabilityLink=By.xpath("//nav[@class='megamenu__navSecondary']//ul[@class='dotcom-megamenu__links']//a[text()='Open Innovability']");
	public By list_of_large_news=By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--large']//div[@class='dotcom-megamenu__article__content']");
	public By list_tweet_news=By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--tweet']//a//div[@class='tweet__username']");
	public By list_of_small_news=By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article ']");
	public By sosLuceAndGasLink=By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article '][2]//div//h4");
	public By iconMenu=By.xpath("//span[@class='icon-menu']");
	public By accettaOpenFiber=By.xpath("//a[contains(text(),'Accetta')]"); ////a[@role='button' and text()='Accetta']");
	public By sosluceegasTitle=By.xpath("//h1[@id='headingtitle' and text()='Manca la corrente o il gas?']");
	//RightMenuObjects
	public By rightMenulinks = By.xpath("//div[contains(text(),'Esplora enel.it')]/..//a[@name]");
	public By frequentQuestionsLink = By.xpath("//div[contains(text(),'Esplora enel.it')]/..//a[contains(text(),'Domande frequenti')]");
	public By fibraDiMelitaLink = By.xpath("//ul[@class='dotcom-megamenu__links']//a[contains(text(),'FIBRA DI MELITA')]");
	//End RightMenuObjects
	
	public String linkSOS_LUCE_E_GAS="it/interruzione-energia-elettrica-gas?zoneid=megamenu-box_small";
	public By areaClienti = By.xpath("//div[@class='dotcom-megamenu__accordian__body']//div[@class='dotcom-megamenu__accordian__body']/a[@class='btn-cta afterLoginBusiness']");
	
	public By condomini = By.xpath("//a[text()='Condomini']");
	public By closeBtn = By.xpath("//div[@id='close-btn']");

	public HamburgerMenuComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	
	public void checkRightMenuLinks() throws Exception{
		String [] labelLinks = {"LUCE E GAS" , "Offerte Luce", "Offerte Gas" , "Condomini" , "enelpremia wow!","Fibra Di Melita","Imprese","Professionisti e partite IVA","Start up e PMI","Grandi aziende","Pubblica Amministrazione","Contatti e consulenza","Storie","GIRO D’ITALIA","Futur-E","Media","Notizie" ,"Comunicati stampa","Supporto","Domande Frequenti","Azienda","Economia Circolare","Contatti","piano strategico 2019-2021","Trova lo Spazio Enel","Remit","Lavora con noi","Fibra di Melita"};

		List<WebElement> links = util.waitAndGetElements(rightMenulinks);
	
//		for(int x=0;x<=labelLinks.length-1;x++) {
		for(int x=0;x<labelLinks.length;x++) {
//			if(!links.get(x).getAttribute("innerText").trim().toLowerCase().contentEquals(labelLinks[x].trim().toLowerCase())) {
			boolean found = false;
			for(int y=0;y<links.size();y++) {
				if (links.get(y).getAttribute("innerText").trim().toLowerCase().contentEquals(labelLinks[x].trim().toLowerCase()))
				{
//					if(x>0)
//				throw new Exception("nel menu di destra non e' visibile il link : "+labelLinks[x].trim()+" dopo il link "+labelLinks[x-1].trim());
//				else throw new Exception("nel menu di destra non e' visibile il link : "+labelLinks[x].trim());
					found =true;
					break;
				}
			}
			if(!found)
				throw new Exception("nel menu di destra non e' visibile il link : "+labelLinks[x].trim());
		}
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
		//System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" non e' di colore "+color+". il colore visualizzato e' : "+colore);
	}
	
	public void jsClickComponent(By xpath) throws Exception{
		util.jsClickElement(xpath);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkSectionLargeNews(By checkOggetto) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkOggetto));
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--large']//div[@class='dotcom-megamenu__article__content']//p//span")));
		String sectionAndTitle1="NO";
		String sectionAndTitle2="NO";
		String sectionAndTitle3="NO";
		String subtitle_section_3="NO";
		List<WebElement> newsList = driver.findElements(checkOggetto);
		
		for (int i = 1; i <= newsList.size(); i++) {
			util.objectManager(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--large']["+i+"]//div[@class='dotcom-megamenu__article__content']//p//span"), util.scrollToVisibility, false);

			
			String section = driver.findElement(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--large']["+i+"]//div[@class='dotcom-megamenu__article__content']//p//span")).getText();
			String sectionTitle=driver.findElement(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--large']["+i+"]//div[@class='dotcom-megamenu__article__content']//h4")).getText();
			String subTitle= driver.findElement(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--large']["+i+"]//div[@class='dotcom-megamenu__article__content']//div//p")).getText();
//			if (section.contentEquals("APP") && sectionTitle.contentEquals("Nuova app Enel Energia: entra in un mondo di vantaggi")) {
//				sectionAndTitle1="YES";
//				checkSectionTweetNews(list_tweet_news);
//			}
//			else 
				if (section.contentEquals("FINANZIARIO") && sectionTitle.contentEquals("Piano Strategico 2019-2021: il nostro futuro rinnovabile")) {
				sectionAndTitle2="YES";	
				checkSectionSmallNews(list_of_small_news);
			}
			
			else if (section.contentEquals("COMMUNITY") && sectionTitle.contentEquals("ENELPREMIA WOW!")) {
				sectionAndTitle3="YES";	
				if (subTitle.contentEquals("Scopri come funziona ENELPREMIA WOW!, il nuovo programma fedeltà gratuito per i clienti Enel Energia")) {
				subtitle_section_3="YES";
				}
			}
			
		}
//		if (sectionAndTitle1.contentEquals("NO") || sectionAndTitle2.contentEquals("NO") || sectionAndTitle3.contentEquals("NO") || subtitle_section_3.contentEquals("NO"))
//			throw new Exception("nel menu' centrale dell' Hamburger menu non e' presente una delle sezioni: 'APP - Nuova app Enel Energia: entra in un mondo di vantaggi'; 'FINANZIARIO - Piano Strategico 2019-2021: il nostro futuro rinnovabile'; 'COMMUNITY - enelpremia WOW!' with subtitle 'Scopri come funziona enelpremia WOW!, il nuovo programma fedeltà gratuito per i clienti Enel Energia'");
		if (sectionAndTitle2.contentEquals("NO"))
			throw new Exception("nel menu' centrale non e' presente la sezione 'FINANZIARIO - Piano Strategico 2019-2021: il nostro futuro rinnovabile'");
		if (sectionAndTitle3.contentEquals("NO"))
			throw new Exception("nel menu' centrale non e' presente la sezione: 'COMMUNITY - enelpremia WOW!'");
		if (subtitle_section_3.contentEquals("NO"))
			throw new Exception("nel menu' centrale non e' presente nella 'COMMUNITY - enelpremia WOW!' la descrizione:'Scopri come funziona enelpremia WOW!, il nuovo programma fedeltà gratuito per i clienti Enel Energia'");

	}
	
	public void checkSectionTweetNews(By checkOggetto) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkOggetto));
        List<WebElement> newsList = driver.findElements(checkOggetto);
		
		for (int i = 1; i <= newsList.size(); i++) {
			util.objectManager(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--tweet']["+i+"]//a//div[@class='tweet__username']"), util.scrollToVisibility, false);
			String sectionTweet = driver.findElement(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article megamenu__article--tweet']["+i+"]//a//div[@class='tweet__username']")).getText();
		    
		    if (!sectionTweet.contentEquals("EnelGroupIT") && !sectionTweet.contentEquals("enelenergia"))
           	 throw new Exception("nel menu' centrale dell' Hamburger menu non e' presente una delle sezioni o entrambe: EnelGroupIT e enelenergia");
		}
	}
	
	public void checkSectionSmallNews(By checkOggetto) throws Exception{
		String sectionAndTitle1="NO";
		String sectionAndTitle2="NO";
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkOggetto));
        List<WebElement> newsList = driver.findElements(checkOggetto);
		
		for (int i = 1; i <= newsList.size(); i++) {
			util.objectManager(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article ']["+i+"]//div//h4"), util.scrollToVisibility, false);
			String section = driver.findElement(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article ']["+i+"]//div//span")).getText();
			String sectionTitle=driver.findElement(By.xpath("//div[@class='dotcom-megamenu__news__inner']//article[@class='dotcom-megamenu__article megamenu__article ']["+i+"]//div//h4")).getText();
						
			if (section.contentEquals("ECONOMIA CIRCOLARE") && sectionTitle.contentEquals("Alla scoperta dell'economia circolare")) {
				sectionAndTitle1="YES";
			}
			else if (section.contentEquals("SUPPORTO") && sectionTitle.contentEquals("SOS luce e gas")) {
				sectionAndTitle2="YES";	
			}
			
		}
		if (sectionAndTitle1.contentEquals("NO") || sectionAndTitle2.contentEquals("NO"))
			throw new Exception("nel menu' centrale dell' Hamburger menu non e' presente una delle sezioni o entrambe: 'ECONOMIA CIRCOLARE:Alla scoperta dell'economia circolare; SUPPORTO:SOS luce e gas ");	
	}
}
