package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PowerAndVoltageComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By areaPrivataHeading = By.xpath("//div[@id='mainContentWrapper']/div/h1");
	public By leFornitureHeading = By.xpath("//section[@id='lista-forniture']/div/h2");
	public By dettaglioFornituraButton = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div/a[2]");
	public By serviziPerLeFornitureHeading = By.xpath("//section[@id='forniture-id0']/div/h2");
	public By modificaPotenzaTensioneTile = By.xpath("//section[@id='forniture-id0']/ul/li[4]/a/div[2]/h3");
	public By modificaPotenzaPage = By.xpath("//section[@id='landing-text']/div/div/h1");
	public By modificaPotenzaButton = By.xpath("//div[@id='landing-dispositiva']/button");
	public By modificaPotenzaTensioneTileHeading = By.xpath("//h1[@id='titleMPT0']");
	public By modificaPotenzaTenzioneTitle = By.xpath("//div[@id='sc']//div[@class='list-form-container']/h2");
	public By radioButton = By.xpath("//label[@id='row-0-label']//span[@id='checkbox-item-0']");
	public By modificaPotenzaTenzioneButton2 = By.xpath("//div[@id='sc']//div[@class='list-form-container']//button[@id='nextButton']/span");
	public By titleMPT = By.xpath("//h1[@id='titleMPT']");
	public By subTitleMPT = By.xpath("//div[@id='sc']/div/div[@class='list-form-container']/h2");
	public By inserimentoDati = By.xpath("//ul/li/span[@class='step-label active']");
	public By preventivo = By.xpath("//ul/li[2]/span[@class='step-label']");
	public By esito = By.xpath("//ul/li[3]/span[@class='step-label']");
	public By powerandVoltageDataEntryHeading = By.xpath("//div[@class='right-main']/div[1]/h3[1]");
	public By potenzalabel = By.xpath("//label[@id='dummyPowerSelect']");
	public By potenzaValue = By.xpath("//select[@id='dummyPowerSelectSelect']");
	public By tensionelabel = By.xpath("//label[@id='dummyVoltageSelect']");
	public By tensioneValue = By.xpath("//select[@id='dummyVoltageSelectSelect']");
	public By textNextToTenzione = By.xpath("//div[1]/div[@class='form-group-container'][1]/div/p");
	public By noRadioButton = By.xpath("//div[@class='radio-container'][1]/label");
	public By modificaPotenzaTensioneTileHeading3 = By.xpath("//div[@class='right-main']/div[1]/h3[2]");
	public By  emailLabel = By.xpath("//label[@id='form-email']");
	public By emailValue = By.xpath("//input[@id='form-emailInput']");
	public By cellulareLabel = By.xpath("//label[@id='form-phone']");
	public By cellulareValue = By.xpath("//input[@id='form-phoneInput']");
	public By potenzaDesiderataLabel = By.xpath("//div[1]/div[@class='form-group mod-invio-email']/label");
	public By potenzaDesiderataValue = By.xpath("//input[@id='descrivi-motivo']");
	public By warningfor50KW = By.xpath("//div[@id='sc']//div[1]/div[@class='blue-alert']/p[2]");
	public By errorMsgfor11KW = By.xpath("//div[@id='sc']//div[1]/div[@class='form-group mod-invio-email']/span[@class='errorMsg']");
	
	public By aggiornamentoTensioneHeading = By.xpath("//p[text()='Aggiornamento Tensione']");
	public By aggiornamentoTensioneSubHeading = By.xpath("//p[text()='Aggiornamento Tensione']/following-sibling::p");
	public By tensioneValueAfterNewSet = By.xpath("//div[@id='input-form-manual-power']");
	
	
	public PowerAndVoltageComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void jsClickElement(By xpath){
			String itemXpath = xpath.toString().replace("By.xpath: ", "");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("(document.evaluate(\""+itemXpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
		}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
				}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL is different from the one saved before.");
	}
	
	public void checkForoptionsAvailable(String property) throws Exception {
		
		if(! property.contains(driver.findElement(inserimentoDati).getText()) && 
				property.contains(driver.findElement(preventivo).getText()) && 
				property.contains(driver.findElement(esito).getText()))
			
			throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
		
		
	}
	
	public void verifyFeildAttribute(By existingObject, String text, String xpath) throws Exception {
	JavascriptExecutor js = (JavascriptExecutor) driver;
	String actualText = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		if (!actualText.equalsIgnoreCase(text))
			throw new Exception("object with text " + text + " is not exist.");
	}
	
	public void isRadioButtonEnabled(By existingObject) throws Exception{
		String str = driver.findElement(By.id("soll-no")).getAttribute("checked");
	if (!str.equalsIgnoreCase("true"))
		throw new Exception("The radio button is not selected");
	}
	
	public void changeDropDownValue(By checkObject,String property) throws Exception {
		
		Select dropdown = new Select(driver.findElement(checkObject));  
		dropdown.selectByVisibleText(property);  
		
	}
	
	public void  validateDropDownValue(By checkObject, String property )throws Exception {
		Select select = new Select(driver.findElement(checkObject));
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();
		if (!defaultItem.equalsIgnoreCase(property))
			throw new Exception("The expected option is not selected in the web page");
	}
	
	public void enterInputtoField(By checkObject, String property) throws Exception {			
				util.objectManager(checkObject, util.sendKeys, property);
	}
	

	
	public void pressTABKey(By checkObject)throws Exception {	
	WebElement tab = driver.findElement(potenzaDesiderataValue);
	tab.sendKeys(Keys.TAB);
	}
	
	public static final String AREA_PRAIVATA_HEADING = "Benvenuto nella tua area privata";
	public static final String LE_FORNITURE_HEADING = "Le tue forniture";
	public static final String MODIFICA_POTENZA_TENZIONE_TILE_HEADING = "Modifica potenza e/o tensione";
	public static final String MODIFICA_POTENZA_TENZIONE_TITLE = "Seleziona la fornitura sulla quale intendi modificare la potenza e/o la tensione";
	public static final String TITLE_MPT ="Modifica potenza e/o tensione";
	public static final String SUBTITLE_MPT = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA OSTIENSE 45, 00154, ROMA, ROMA, RM";
	public static final String POVER_VOLTAGE_DATAENTRY_HEADING ="Inserisci di seguito la potenza e/o tensione che vuoi impostare sulla fornitura selezionata:";
	public static final String POTENZA_LABEL = "Potenza*";
	public String xpath = "//select[@id='dummyPowerSelectSelect']";
	public static final String POTENZA_VALUE = "3 kW(potenza attuale)";
	public static final String setPotenzaValue = "4.5 kW";
	public static final String setPotenzaValueNew = "15 kW";
	public static final String setPotenzaValueNew2 = "6 kW";
	public static final String TENZIONE_LABEL = "Tensione*";
	public String xpathTenzione =  "//select[@id='dummyVoltageSelectSelect']";
	public static final String TENZIONE_VALUE = "220 V(tensione attuale)";
	public static final String TEXT_NEXT_TENZIONE = "Sulla fornitura sono collegati apparati di sollevamento persone (es. ascensore)?";
	public static final String MODIFICA_TENZIONE_HEADING3 = "Conferma o modifica i tuoi dati di contatto sui quali ricevere la documentazione";
	public static final String EMAIL_LABEL = "Email*";
	//public static final String EMAIL_VALUE = "r32019p109s.tatocontiweb@gmail.com";
	public static final String EMAIL_VALUE = "r.aff.aellaquomo@gmail.com";
	public String xpathEmail = "//input[@id='form-emailInput']";
	public static final String CELLULARE_LABEL = "Cellulare*";
	public static final String CELLULARE_VALUE ="3468505368";
	public String xpathCellulare = "//input[@id='form-phoneInput']";
	public static final String POTENZA_DESIDERATA = "Potenza Desiderata* (kW)";
	public static final String WARNING_TEXT_FOR50KW = "Le ricordiamo che tale modifica potrebbe far variare il ciclo di fatturazione. Selezionando una potenza superiore a 10 kW è necessario modificare la tensione da 220V a 380V. Modificando la tensione da 220V a 380V verrà sostituito il contatore e la fornitura passerà da monofase (fase più neutro) a trifase (tre fasi più neutro), pertanto dovrai contattare un tecnico per riallacciare correttamente e in sicurezza il tuo impianto al contatore e alla rete del distributore.";
	public static final String POTENZA_DESIDERATA_MAX = "99";
	public String xpathPotenzaDesiredata ="//input[@id='descrivi-motivo']";
	public static final String ERROR_TEXT_FOR11KW = "Per potenze minori o uguali a 15kW selezionare uno tra i valori nella lista.";
	
}
