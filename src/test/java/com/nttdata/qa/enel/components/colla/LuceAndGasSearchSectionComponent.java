package com.nttdata.qa.enel.components.colla;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LuceAndGasSearchSectionComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By rightSolutionLabel= By.xpath("//div[@class='image-hero_content-wrapper']//h1[@class='hero_title text--page-heading' and text()='Trova la soluzione giusta per te']");
	public By searchSection=By.xpath("//div[@class='search-section']");
	public By iniziaOraButton=By.id("ofertButton");
	public By contractList=By.xpath("//span[@id='productSelectBoxItContainer']//ul[@id='productSelectBoxItOptions']//li[@role='option']");
	public By placeList=By.xpath("//span[@id='placeSelectBoxItContainer']//ul[@id='placeSelectBoxItOptions']//li[@role='option']");
	public By thirdOptionLinkOnPlaceList=By.xpath("//li[@class='lastOption-myProduct']//div//a[@href='/it/imprese']");
	public By scoprileofferte=By.xpath("//li[@class='lastOption-myProduct']//div//a[@href='/it/imprese']//h5[text()='scopri le offerte per le imprese']");
	public By impresePage=By.xpath("//a[@href='/it/imprese' and @title='Imprese']");
	public By needList=By.xpath("//div[@class='selectMenuContent']//ul[@class='selectMenuOptions']//li");
	public By labelLuceandGas=By.xpath("//div[@id='promo_offert_results']//span[@class='eyebrow' and text()='Luce e Gas']");
    public By labelPiùEnergiaperLaTuaCasa=By.xpath("//h2[@class='title' and text()='Più Energia per la tua casa']");
	public By labelperlacasa=By.xpath("//div[@class='cmEvidenceBanner_pretitle']//h1[text()='Per la casa']");
	public By defaultcontractListvalue=By.xpath("//span[@id='productSelectBoxItContainer']//span[@id='productSelectBoxItText' and text()='Luce']");
	public By defaultplaceListvalue=By.xpath("//span[@id='placeSelectBoxItContainer']//span[@id='placeSelectBoxItText' and text()='Casa']");
	public By defaultneedListvalue=By.xpath("//span[@id='myselectSelectBoxItContainer']//span[@id='myselectSelectBoxItText' and text()='Visualizza tutte']");
	public By contractListvalue=By.xpath("//span[@id='productSelectBoxItContainer']//span[@id='productSelectBoxItText']");
	public By placeListvalue=By.xpath("//span[@id='placeSelectBoxItContainer']//span[@id='placeSelectBoxItText']");
	public By Listvalue=By.xpath("//span[@id='myselectSelectBoxItContainer']//span[@id='myselectSelectBoxItText']");
	public String linkIMPRESE="it/imprese";
	public String linkLUCE_E_GAS="it/luce-e-gas";
	public String linkLUCE_E_GAS_CON_FILTRO="it/luce-e-gas/luce/casa?nec=swa";
	public String linkLUCE_E_GAS_SENZA_FILTRO="it/luce-e-gas/luce/casa";
	public String linkLUCE_E_GAS_LUCE_CASA_PROMOZIONE="it/luce-e-gas/luce/casa?nec=non_specificato&sortType=in-promozione";
	public By contractContainer = By.xpath("//span[@id='productSelectBoxItContainer']");
	public By placeContainer = By.xpath("//span[@id='placeSelectBoxItContainer']");
	public By optionContainer = By.xpath("//span[@id='myselectSelectBoxItContainer']");
	public By attivaOraPlacetFissaLuce = By.xpath("//a[contains(@aria-label,'Enel Energia PLACET Fissa Luce')]");
	public By placetFissaLuce = By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/placet-consumer-luce-fissa']");
	
	public LuceAndGasSearchSectionComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void verifyQuestionsExistence(By existentObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(existentObject));
		String description="";
		String question1="Che contratto vuoi attivare?";
		String question2="Dove?";
		String question3="Per quale necessità?";
		
		WebElement questions = driver.findElement(existentObject);
			description = questions.getText();
			
			if (!(description.contains(question1) && description.contains(question2) && description.contains(question3))) 
			  throw new Exception("nella sezione 'Luce and Gas' non sono visibili la labels: "+question1+";"+question2+";"+question3+".");
	}
	
	public void verifyFirstListBox (By typeContractObject)throws Exception {
		//driver.findElement(By.xpath("//span[@id='productSelectBoxIt']")).click();
		util.objectManager(By.xpath("//span[@id='productSelectBoxIt']"), util.scrollToVisibility, false);
		util.objectManager(By.xpath("//span[@id='productSelectBoxIt']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		String[] options = {"", "Luce", "Gas", "Luce e Gas"};
		String description = "";
	    List <WebElement> list = driver.findElements(typeContractObject);
   
			for (int i = 1; i <= list.size(); i++) {
				description = driver.findElement(By.xpath("//span[@id='productSelectBoxItContainer']//ul[@id='productSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
				boolean b = description.equals(options[i]);
		         if(!b)
		        	 throw new Exception("sulla pagina web 'Luce e Gas' per la listbox 'Che contratto vuoi attivare' non è presente l'option: "+options[i]);
			   }
	}
	
	public void verifySecondListBox (By typePlaceObject)throws Exception {
		TimeUnit.SECONDS.sleep(2);
	//	driver.findElement(By.xpath("//span[@id='placeSelectBoxIt']")).click();
		util.objectManager(By.xpath("//span[@id='placeSelectBoxIt']"), util.scrollToVisibility, false);
		util.objectManager(By.xpath("//span[@id='placeSelectBoxIt']"), util.scrollAndClick);
		String description = "";
	    List <WebElement> list = driver.findElements(typePlaceObject);
   
			for (int i = 1; i <= list.size(); i++) {
					description = driver.findElement(By.xpath("//span[@id='placeSelectBoxItContainer']//ul[@id='placeSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
					System.out.println(description);
			        if (!description.contentEquals("Casa") && !description.contentEquals("Negozio - Ufficio") && !description.contentEquals("Altro (Garage, Cantina..)"))
			        	 throw new Exception("sulla Page 'Luce e Gas' nella listbox 'Dove' non sono presenti le opzioni: Casa; Negozio - Ufficio");
			        
				   }
			
	
	   String thirdOptionLink=driver.findElement(By.xpath("//li[@class='lastOption-myProduct']//div//a")).getText();
	   thirdOptionLink= thirdOptionLink.replaceAll("(\r\n|\n)", " ");
	  
			if (!thirdOptionLink.contentEquals("Sei una start up, una PMI o una grande azienda? SCOPRI LE OFFERTE PER LE IMPRESE"))
				throw new Exception("sulla pagina 'Luce e Gas' nella listbox 'Dove' non e' presente la terza opzione lincabile: 'Sei una start up, una PMI o una grande azienda? - SCOPRI LE OFFERTE PER LE IMPRESE'");
			  
	}
	
	public void verifyThirdListBox (By needObject)throws Exception {
		TimeUnit.SECONDS.sleep(2);
		//driver.findElement(By.xpath("//span[@id='myselectSelectBoxIt']")).click();
		util.objectManager(By.xpath("//span[@id='myselectSelectBoxIt']"), util.scrollToVisibility, false);
		util.objectManager(By.xpath("//span[@id='myselectSelectBoxIt']"), util.scrollAndClick);
		String description = "";
		String subDescription = "";
	    List <WebElement> list = driver.findElements(needObject);
   
			for (int i = 1; i <= list.size(); i++) {
					description = driver.findElement(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//h5")).getText();
					subDescription=driver.findElement(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//p")).getText();
					
					if (!description.contentEquals("CAMBIO FORNITORE") && !description.contentEquals("PRIMA ATTIVAZIONE") && !description.contentEquals("SUBENTRO") && !description.contentEquals("VOLTURA") && !description.contentEquals("VISUALIZZA TUTTE"))
			        	 throw new Exception("sulla pagina web 'Luce e Gas' nella listbox 'Per quale necessita'' non sono presenti le seguenti Processi: CAMBIO FORNITORE; PRIMA ATTIVAZIONE; SUBENTRO; VOLTURA; VISUALIZZA TUTTE.");
			    
			        if (!subDescription.contentEquals("Ho un contratto con un altro fornitore ma vorrei passare a Enel") && !subDescription.contentEquals("Voglio attivare un nuovo contatore") && !subDescription.contentEquals("Voglio riattivare un contatore disattivato") && !subDescription.contentEquals("Voglio cambiare intestazione a un contratto di fornitura") && !subDescription.contentEquals("Voglio scoprire tutte le offerte Enel"))
			        	 throw new Exception("sulla pagina web 'Luce e Gas' nella listbox 'Per quale necessita' non e' presente per ogni processo la relativa frase di dettaglio.");
			   			        
			        checkColor(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//h5"),"DeepPink","OPZIONE");
			        checkColor(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//p"),"Black","DETTAGLIO OPZIONE");
				   }
	}
	
	public void selectionOptionsFromListbox (By typeContractObject,String contract,By typePlaceObject, String place,By needObject, String whatNeed)throws Exception {
		driver.findElement(By.xpath("//span[@id='productSelectBoxIt']")).click();
		TimeUnit.SECONDS.sleep(1);
		String contractFound="NO";
		String placeFound="NO";
		String needFound="NO";
	    List <WebElement> contractList = driver.findElements(typeContractObject);
   
			for (int i = 1; i <= contractList.size(); i++) {
				String contractDescription = driver.findElement(By.xpath("//span[@id='productSelectBoxItContainer']//ul[@id='productSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
				 if (contractDescription.contentEquals(contract)) {
						 driver.findElement(By.xpath("//span[@id='productSelectBoxItContainer']//ul[@id='productSelectBoxItOptions']//li[@role='option']["+i+"]//a")).click();
						 contractFound="YES";
				         break;
				         }
			   }
			
			if (contractFound.contentEquals("NO"))
					 throw new Exception("sulla pagina web 'Luce e Gas' non e' possibile selezionare l'opzione "+contract+" dalla listbox 'Che contratto vuoi attivare'");
			
	    driver.findElement(By.xpath("//span[@id='placeSelectBoxIt']")).click();
	    TimeUnit.SECONDS.sleep(1);
	    
	    List <WebElement> placeList = driver.findElements(typePlaceObject);
	    
	        for (int i = 1; i <= placeList.size(); i++) {
	    	   String  placeDescription = driver.findElement(By.xpath("//span[@id='placeSelectBoxItContainer']//ul[@id='placeSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
	    	   if (placeDescription.contentEquals(place)) {
	    		    driver.findElement(By.xpath("//span[@id='placeSelectBoxItContainer']//ul[@id='placeSelectBoxItOptions']//li[@role='option']["+i+"]//a")).click();
	    		    placeFound="YES";
	    	        break;}
	    	        }
	        
	        if (placeFound.contentEquals("NO"))
	        	throw new Exception("sulla pagina web 'Luce e Gas' non e' possibile selezionare l'opzione "+place+" dalla listbox 'Dove'");
	        
	      driver.findElement(By.xpath("//span[@id='myselectSelectBoxIt']")).click();
	      TimeUnit.SECONDS.sleep(4);
	   
		    List <WebElement> needList = driver.findElements(needObject);
		    for (int i = 1; i <= needList.size(); i++) {
			   String needDescription = driver.findElement(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//h5")).getText();
			     if (needDescription.contentEquals(whatNeed)) {
			    	 driver.findElement(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//h5")).click();
			    	 needFound="YES";
			         break;}
		    	  }
		    if (needFound.contentEquals("NO"))
		    	throw new Exception("sulla pagina web 'Luce e Gas' non e' possibile selezionare l'opzione "+whatNeed+" dalla listbox 'Per quale necessita'");
	         
		    }	
	
	
	public void verifyTextSearchMenu (By label, String textlabel)throws Exception {
		String text=driver.findElement(label).getText();
		
		if (!text.contentEquals(textlabel)) 
	        	throw new Exception("il motorino di ricerca non e' settato con il valore : "+textlabel);
		}
	
	public void containsAllText(By by, String text) throws Exception{
		String textWe = driver.findElement(by).getText();
		textWe= textWe.replaceAll("(\r\n|\n)", " ");
		if(!textWe.contains(text))
			throw new Exception("textWe " + textWe + "non corrisponde a " + text);
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = we.getText();		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	public void selectionOptionsFromListbox(String contract,String place,String whatNeed)throws Exception {
		driver.findElement(By.xpath("//span[@id='productSelectBoxIt']")).click();
		TimeUnit.SECONDS.sleep(1);
		String contractFound="NO";
		String placeFound="NO";
		String needFound="NO";
	    List <WebElement> contractList = driver.findElements(this.contractList);
   
			for (int i = 1; i <= contractList.size(); i++) {
				String contractDescription = driver.findElement(By.xpath("//span[@id='productSelectBoxItContainer']//ul[@id='productSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
				 if (contractDescription.contentEquals(contract)) {
						 driver.findElement(By.xpath("//span[@id='productSelectBoxItContainer']//ul[@id='productSelectBoxItOptions']//li[@role='option']["+i+"]//a")).click();
						 contractFound="YES";
				         break;
				         }
			   }
			
			if (contractFound.contentEquals("NO"))
					 throw new Exception("sulla pagina web 'Luce e Gas' non e' possibile selezionare l'opzione "+contract+" dalla listbox 'Che contratto vuoi attivare'");
			
	    driver.findElement(By.xpath("//span[@id='placeSelectBoxIt']")).click();
	    TimeUnit.SECONDS.sleep(1);
	    
	    List <WebElement> placeList = driver.findElements(this.placeList);
	    
	        for (int i = 1; i <= placeList.size(); i++) {
	    	   String  placeDescription = driver.findElement(By.xpath("//span[@id='placeSelectBoxItContainer']//ul[@id='placeSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
	    	   if (placeDescription.contentEquals(place)) {
	    		    driver.findElement(By.xpath("//span[@id='placeSelectBoxItContainer']//ul[@id='placeSelectBoxItOptions']//li[@role='option']["+i+"]//a")).click();
	    		    placeFound="YES";
	    	        break;}
	    	        }
	        
	        if (placeFound.contentEquals("NO"))
	        	throw new Exception("sulla pagina web 'Luce e Gas' non e' possibile selezionare l'opzione "+place+" dalla listbox 'Dove'");
	        
	      driver.findElement(By.xpath("//span[@id='myselectSelectBoxIt']")).click();
	      TimeUnit.SECONDS.sleep(4);
	   
		    List <WebElement> needList = driver.findElements(this.needList);
		    for (int i = 1; i <= needList.size(); i++) {
			   String needDescription = driver.findElement(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//h5")).getText();
			   System.out.println("needDescription --> " + needDescription);
			     if (needDescription.equalsIgnoreCase(whatNeed)) {
			    	 driver.findElement(By.xpath("//span[@id='myselectSelectBoxItContainer']//ul[@id='myselectSelectBoxItOptions']//li["+i+"]//div//h5")).click();
			    	 needFound="YES";
			         break;}
		    	  }
		    if (needFound.contentEquals("NO"))
		    	throw new Exception("sulla pagina web 'Luce e Gas' non e' possibile selezionare l'opzione "+whatNeed+" dalla listbox 'Per quale necessita'");
	         
		    }	
	
}
