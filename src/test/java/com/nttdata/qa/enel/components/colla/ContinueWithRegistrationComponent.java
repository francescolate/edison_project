package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ContinueWithRegistrationComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By primoAccessoHeader = By.xpath("//div[@class='list-form-container']//*[contains(text(),'Stai effettuando il primo accesso in Area Clienti')]");
	public By numeroClienteInputField = By.xpath("//input[@id='input_numeroCliente']");
	public By numeroClienteLabel = By.xpath("//label[@id='label_numeroCliente']");
	public By numeroContrattoLabel = By.xpath("//label[@id='label_numeroContratto']");
	public By numeroContrattoInputField = By.xpath("//input[@id='input_numeroContratto']");
	public By solouno = By.xpath("//span[text()='* Solo uno dei due campi è obbligatorio']");
	public By numeroClienteContinuaButton = By.xpath("//span[text()='CONTINUA']/parent::button");
	public By numeroClienteConfermaButton = By.xpath("//span[text()='CONFERMA']/parent::button");
	public By numeroClienteFineButton = By.xpath("//span[text()='ACCEDI']/parent::button");
	public By autenticazione = By.xpath("//h1[text()='Autenticazione']");
	public By primoAccessoMenu1 = By.xpath("//span[text()='Inserimento dati']");
	public By primoAccessoMenu2 = By.xpath("//span[text()='Riepilogo e conferma dati']");
	public By primoAccessoMenu3 = By.xpath("//span[text()='Esito']");
	public By enelpremiaWoW = By.xpath("//span[text()='enelpremia WOW!']");
	public By Supporto = By.xpath("//span[text()='Supporto']");
	public By trovaSpazioEnel = By.xpath("//span[text()='Trova Spazio Enel']");
	public By esci = By.xpath("//a[@id='disconnetti']//span[text()='Esci']");
	public By numeroClientiTitle = By.xpath("//h4[contains(text(),'Inserisci il numero cliente')]");
	public By campiobbligatori = By.xpath("//span[contains(text(),'* campi obbligatori')]");
	public By iIcon = By.xpath("//h4[contains(text(),'Inserisci il numero cliente')]//a");
	public By numeroClienteiIcon = By.xpath("//a[@data-modal-id='modal-info-client-number']");
	public By NumeroContrattoiIcon = By.xpath("//a[@data-modal-id='modal-info-contract-number']");
	public By dialogTitle = By.xpath("//h3[contains(text(),'Dove trovo il')]");
	public By dialogText = By.xpath("//p[contains(text(),'Il Numero Cliente')]");
	public By dialogText2 = By.xpath("//p[contains(text(),'Il Numero Contratto')]");
	public By dialogeClose = By.xpath("//a[@onClick='closeModale()']");
	public By dialogeCloseNew = By.xpath("//h3[contains(text(),'Dove trovo il numero cliente?')]/following-sibling::a");
	public By dialogeCloseNew2 = By.xpath("//h3[contains(text(),'Dove trovo il numero contratto?')]/following-sibling::a");
	public By operazioneTitle = By.xpath("//h4[contains(text(),'L’operazione non è andata a buon fine')]");
	public By operazioneTitleNew = By.xpath("//h4[contains(text(),'Operazione eseguita correttamente')]");
	public By successText = By.xpath("//p[contains(text(),'ha avuto successo')]");
	public By indietroButton = By.xpath("//button[@onclick='backToInsertStep()']");
	public By confermaTitle = By.xpath("//h4[text()='Verifica la correttezza delle informazioni:']");
	public By numeroClientiError = By.xpath("//div[contains(text(),'Questo campo è obbligatorio')]");
	
    public ContinueWithRegistrationComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void launchLink(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//		if (!util.exists(oggettoEsistente, 15))
//			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		*/
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			//System.out.println(message);
			throw new Exception(message);
		}
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
			
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
	}
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void fillInputField(By by, String s) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		we.clear();
		Thread.sleep(200);
		we.sendKeys(s);
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void firstAccess(String numeroCLiente){
			try{
			verifyComponentExistence(primoAccessoHeader);
			verifyComponentExistence(numeroClienteInputField);
			fillInputField(numeroClienteInputField, numeroCLiente);
			verifyComponentExistence(numeroClienteContinuaButton);
			clickComponent(numeroClienteContinuaButton);
			verifyComponentExistence(numeroClienteConfermaButton);
			clickComponent(numeroClienteConfermaButton);
			verifyComponentExistence(numeroClienteFineButton);
			clickComponent(numeroClienteFineButton);
			}catch(Exception e){
				System.out.print("check sull'esistenza della pagina Conferma Codice Cliente - PAGINA INESISTENTE. PROSEGUO.");
			}
		}
	
	public static final String DialogText = "Il Numero Cliente è un numero identificativo composto da 9 cifre che puoi trovare nella prima pagina della tua bolletta.Se hai più forniture, basterà inserire uno dei numeri cliente e in automatico verranno riconosciuti anche le altre forniture a te intestate.";
	public static final String DialogTextcontratto = "Il Numero Contratto è un numero identificativo composto da almeno 9 cifre che puoi trovare nella prima pagina del tuo contratto e nelle condizioni tecnico economiche.Se hai più forniture, basterà inserire uno dei numeri contratto e in automatico verranno riconosciute anche le altre forniture a te intestate.";
	public static final String DialogTextNext = "Il Numero Cliente è un numero identificativo composto da 9 cifre che puoi trovare nella prima pagina della tua bolletta. Se hai più forniture, basterà inserire uno dei numeri cliente e in automatico verranno riconosciute anche le altre forniture a te intestate.";
	public static final String SuccessText = "L’autenticazione ha avuto successo, ora puoi gestire le tue forniture e navigare nella tua area privata.";

}
