package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CheckPrivateAreaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;
	public String itemXpath = "//ul[@id='internal-menu']//span[text()='$itemName$']/parent::a";
	public String itemBSNXpath = "//ul//a[text()='$itemName$']";
	public By orderedList = By.xpath("//a[contains(@class,'href-sf-prevent-default inline-icon') and (not(@style) or @style='')]");
	public By orderedBSNList = By.xpath("//div[@class='user-details']/following-sibling::div//ul//li[not(contains(@class, 'hide'))]/a");
	//div[@class='user-details']/following-sibling::nav//div//li[not(contains(@class, 'hide'))]/a
	public By button_close_popup_login = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-closed']//button[@aria-label='chiudi']");
    
    public CheckPrivateAreaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyMenuItemsPresenceAndOrder(String menuItemsString) throws Exception {
		populateMenuItemsVector(menuItemsString);
		List<WebElement> orderedWebElements = util.waitAndGetElements(this.orderedList);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
		/*
		 * Modificato il 18/06/2020 a seguito dell'aggiunta del menu item 'Porta i tuoi amici', al fine di far procedere con test manuali.
		 * 
		if(orderedWebElements.size()!=menuItemsList.size())
			throw new Exception("actual menu size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
		for(int i=0;i<orderedWebElements.size();i++){
			if(!orderedWebElements.get(i).equals(menuItemsList.get(i)))
				throw new Exception("You were looking for:\n\t"+menuItemsList.get(i)+"\nwhile the actual item value is:\n\t"+orderedWebElements.get(i));
		}
		*/
		for(WebElement we : menuItemsList){
			if(!orderedWebElements.contains(we))
				throw new Exception("La voce di menù "+we.getText()+" non è contenuta nel menù.");
		}
	}
	
	private void populateMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(itemXpath.replace("$itemName$", item))));
	}
	
	public void verifyBSNMenuItemsPresenceAndOrder(String menuItemsString) throws Exception {
		populateBSNMenuItemsVector(menuItemsString);
		List<WebElement> orderedWebElements = util.waitAndGetElements(this.orderedBSNList, 30, 3);
		if(orderedWebElements.size()==0)
			throw new Exception("no menu items found using the xpath selector.");
		/*
		 * Modificato il 18/06/2020 a seguito dell'aggiunta del menu item 'Porta i tuoi amici', al fine di far procedere con test manuali.
		 * 
		if(orderedWebElements.size()!=menuItemsList.size())
			throw new Exception("actual menu size is "+orderedWebElements.size()+", while you're looking for "+menuItemsList.size()+" elements.");
		for(int i=0;i<orderedWebElements.size();i++){
			if(!orderedWebElements.get(i).getText().equals(menuItemsList.get(i).getText()))
					throw new Exception("You were looking for:\n\t"+menuItemsList.get(i)+"\nwhile the actual item value is:\n\t"+orderedWebElements.get(i));
		}
		*/
		for(WebElement we : menuItemsList){
			if(!orderedWebElements.contains(we))
				throw new Exception("La voce di menù "+we.getText()+" non è contenuta nel menù.");
		}
	}
	
	private void populateBSNMenuItemsVector(String menuItemsString) throws Exception {
		menuItemsList = new ArrayList<WebElement>();
		String[] items = menuItemsString.split(";");
		for(String item : items)
			menuItemsList.add(util.waitAndGetElement(By.xpath(itemBSNXpath.replace("$itemName$", item))));
	}
	
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public static final String PRIVATE_AREA_MENU_RES = "Forniture;Bollette;Servizi;ENELPREMIA WOW!;Novità;Spazio Video;Account;English handbook;I tuoi diritti;Supporto;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_RES_41 = "Forniture;Bollette;Servizi;enelpremia WOW!;Novità;Spazio Video;Porta i tuoi amici;Account;English handbook;I tuoi diritti;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_RES_42 = "Forniture;Bollette;Servizi;Novità;Spazio Video;Porta i tuoi amici;Account;English handbook;I tuoi diritti;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_RES_NEW = "Forniture;Bollette;Servizi;ENELPREMIA WOW!;Novità;Spazio Video;Chiamaci;Account;English handbook;I tuoi diritti;Supporto;Trova Spazio Enel;Documenti;Esci";
	public static final String PRIVATE_AREA_MENU_NO_RES = "Forniture;Bollette;Servizi;Novità;Spazio Video;Account;I tuoi diritti;Area Clienti Impresa;Supporto;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_RES_LIKE = "Forniture;Bollette;Servizi;Novità;Spazio Video;Porta i tuoi amici;Account;English handbook;I tuoi diritti;Supporto;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_BSN_LIKE = "Forniture;Bollette;Servizi;enelpremia WOW!;Novità;Spazio Video;Account;I tuoi diritti;Area Clienti Impresa;Supporto;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_BSN = "HomePage;bollette;forniture;servizi;messaggi;stato richieste;carica documenti;contatti;logout";
	public static final String PRIVATE_AREA_MENU_BSN_NEW = "HomePage;bollette;forniture;servizi;messaggi;stato richieste;report billing management;carica documenti;English handbook;logout";
	public static final String PRIVATE_AREA_MENU_RES_FOR_ACTIVE = "Forniture;Bollette;Servizi;Novità;Spazio Video;Account;I tuoi diritti;Supporto;Trova Spazio Enel.;Esci";
	public static final String PRIVATE_AREA_MENU_RES_LIKE_364 = "Forniture;Bollette;Servizi;Novità;Spazio Video;Porta i tuoi amici;Account;English handbook;I tuoi diritti;Area Clienti Impresa;Supporto;Trova Spazio Enel.;Esci";
}
