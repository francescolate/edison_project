package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModificaBollettaWebComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By leTueAttivitaHeader = By.xpath("//*[text()='Le tue attività']");
	public By attivaLaTuaBollettaWebButton = By.xpath("//*[text()='Attiva ora la Bolletta Web']/../following-sibling::a");
	public By attivaBollettaWebButton = By.xpath("//button//*[text()='ATTIVA BOLLETTA WEB']");
	public By numeroCliente = By.xpath("//span[@class='icon-ckbox']/following-sibling::span[@class='cliente']//span[@class='valore']/span/span[not(text()='')]");
	public By continuaButton = By.xpath("//*[text()='CONTINUA']/parent::button");
	public By continuaSecondButton = By.xpath("//*[text()='Continua']/parent::button");
	public By confermaButton = By.xpath("//*[text()='Conferma']/parent::button");
	public By indietroButton = By.xpath("//*[text()='INDIETRO']/parent::button");
	public By esciButton = By.xpath("//*[text()='Esci']/parent::button");
	public By inputEmail = By.xpath("//input[@id='emailFieldInput']");
	public By inputConfirmEmail = By.xpath("//input[@id='emailFieldConfirmInput']");
	public By inputMobile = By.xpath("//input[@id='mobileFieldInput']");
	public By inputConfirmMobile = By.xpath("//input[@id='mobileFieldConfirmInput']");
	public By infoButton = By.xpath("//span[@class='icon-info-circle']");
	public By inputEmailError = By.xpath("//span[@id='emailFieldError']");
	public By inputConfirmEmailError = By.xpath("//span[@id='emailFieldConfirmError' and not(text()='')]");
	public By inputMobileError = By.xpath("//span[@id='mobileFieldError']");
	public By inputConfirmMobileError = By.xpath("//label[text()='Conferma numero di cellulare*']/..//span[not(text()='')]");
	public By datiContattoModalHeader = By.xpath("//h3[@id='titleModal']");
	public By datiContattoInnerText= By.xpath("//h3[@id='titleModal']/parent::div[@class='modal-header']//p");
	public By aggiornaIMieiDatiAnagrafici = By.xpath("//label[text()='Aggiorna i miei dati anagrafici']");
	public String verificaCorrettezzaEmail = "//dd[text()='$email$']";
	public String verificaCorrettezzaMobile = "//dd[text()='$mobile$']";
	public String verificaCorrettezzaNumeroCliente = "//fieldset[@class='list-container']//span[@class='cliente']//span[text()='$numeroCliente$']";
	public By spinner = By.xpath("//span[@class='dsc-icon-loader']");
	public By fineButton = By.xpath("//span[text()='Fine']/..");
	public By mainPageSubHeader= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
	public By homeFullscreenAlert = By.xpath("//div[@id='home-fullscreen-alert']");
	public By homeFullscreenAlertCloseButton = By.xpath("//div[@id='fsa-close-button']");
	public By serviziMenuItem = By.xpath("//span[text()='Servizi']/..");
	public By serviziPerLeFornitureHeader = By.xpath("//h2[text()='Servizi per le bollette']");
	public By bollettaWebTile = By.xpath("//ul[@class='tileWrapper']//h3[text()='Bolletta Web']/ancestor::a");
	public By modificaIndirizzoEmailButton = By.xpath("//span[text()='MODIFICA INDIRIZZO EMAIL']/..");
	public By fornituraCheckBox = By.xpath("//span[@class='icon-ckbox']");
	public By closeInfoButton = By.xpath("//div[@id='modalAlert']//button");
	
	public By modificaBolletaWeb = By.xpath("//h1[@id='h1_bollettaweb']");
	public By modificaBolletaWebSubText = By.xpath("//h1[@id='h1_bollettaweb']//following::h2");
	public By Forniture = By.xpath("//span[text()='Forniture']");
	public By popUpTitle = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-dialog']//h3");
	public By popUpText = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-dialog']//p");
	public By popUpNoButton = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-dialog']//button[@id='overlayNoButton']");
	public By popUpYesButton = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@class='modal-dialog']//button[@id='overlayYesButton']");
	public By checkBox = By.xpath("//span[@class='icon-ckbox']");
	public By bolleteWebService = By.xpath("//span[text()='Bolletta Web']");
	
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
    public ModificaBollettaWebComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
		
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public void compareText(By by, String[] text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		for(String s : text){
			System.out.println(s);
			if(!weText.contains(s))
				throw new Exception("The webpage container does not contain the text '"+s+"'");
		}
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void changeInputText(By by, String newText) throws Exception{
		String[] array = (by.toString()).split(": ");
		util.setElementValue(array[1], newText);
		Thread.sleep(250);
	}
	
	public void copy(By by) throws Exception{
		util.copy(by);
	}
	
	public void paste(By by) throws Exception{
		util.paste(by);
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	public String jsGetCustomerNumber(By by) throws Exception{
		return util.jsGetInnerText(by);
	}
	
	public void fillInputField(By by, String s) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		we.clear();
		Thread.sleep(200);
		we.sendKeys(s);
	}
	
	public void fillInputFieldIfExist(By by, String s) throws Exception{
		if (util.exists(by, 20)){
			WebElement we = util.waitAndGetElement(by);
			we.clear();
			Thread.sleep(200);
			we.sendKeys(s);
		}
	}
	
	public final String campoEmailObbligatorio = "Il campo Email è obbligatorio.";
	public final String campoMobileObbligatorio = "Il campo Numero di cellulare è obbligatorio.";
	public final String emailNonValida = "Email non valida";
	public final String cellulareNonValido = "Cellulare non valido";
	public final String cellulareSoloNumeri = "Il campo Numero di cellulare può contenere solo caratteri numerici.";
	public final String campiCellulareNonCoincidono = "I campi \"Numero di cellulare\" e \"Conferma numero di cellulare\" non coincidono.";
	public final String copiaIncollaErrore = "Non è possibile copiare e incollare il contenuto; digita di nuovo i dati.";
	public final String datiContattoModalHeaderText = "Aggiorna i miei dati di contatto";
	public final String datiContattoPopupText = "Aggiornando i dati di contatto ci consentirai di modificare i dati del tuo profilo.";
	public final String ModificaBolletaWebSubText="Seleziona una o più forniture su cui intendi modificare il servizio di Bolletta Web.";
	public final String ModificaBolletaTitle = "Modifica Bolletta Web";
	public final String PopUpTitle = "Attenzione";
	public final String PopUpText = "Sei sicuro di voler uscire da questa sezione?";
}
