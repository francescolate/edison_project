package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ForniturePerCambioIndirizzoComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public By barraRicerca = By.xpath("//div[@id='labelDatagridB']//input");
	public By checkboxPrimaFornituraInTabella = By.xpath("//table[@id='ServiceAddressChange_Table']/tbody/tr//div[2]//label");
	public By checkboxPrimaFornituraInTabellaFatturazione = By.xpath("//table[@id='ServiceAddressChange_Table']/tbody/tr//div[1]//label");
	public By btnConfermaIndirizzoFornitura	= By.xpath("//button[text()='Conferma Selezione Fornitura']");
	public By checkboxPrimaFornituraInTabellaModificaPotenzaTensione = 
			By.xpath("//table[@id='selectcommodityMPT_Table']/tbody/tr[1]//div[2]/label") ;
	
	public ForniturePerCambioIndirizzoComponent(WebDriver driver)
	{
		this.driver =driver ;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	public void cercaInTabella(String valoreRicerca) throws Exception{
		util.waitAndGetElement(barraRicerca)
		.sendKeys(valoreRicerca);
	}
	

	public void cliccaPrimoElementoInTabella(By by) throws Exception{
		util.waitAndGetElement(by)
		.click();
	}
	
	public void cercaECliccaFornitura(String valoreRicerca,By by) throws Exception{
		cercaInTabella(valoreRicerca);
		spinner.checkSpinners();
		cliccaPrimoElementoInTabella(by);
		spinner.checkSpinners();
	}
	
	public void clickConfermaFornitura () throws Exception{
//		util.waitAndGetElement(btnConfermaIndirizzoFornitura).click();
		util.objectManager(btnConfermaIndirizzoFornitura, util.scrollAndClick);
	}
	
}
