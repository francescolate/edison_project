package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InfoEnelEnergia_BsnComponent extends BaseComponent {
	public By serviziItem = By.cssSelector("#menuService > a");
	public By serviziItemNew = By.xpath("//span[text()='Servizi']");
	public By serviziItemUpdated = By.xpath("//a[text()='Servizi']");
	public By serviziItemUpdatedNew = By.xpath("//a[text()='servizi']");
	public By fornitureItem = By.cssSelector("#menuForn > a");
	public By bolletteItem = By.cssSelector("#menuBoll > a");
	public By areaRiservataPath = By.xpath("//ol[@class='breadcrumb']/li[text()='Area riservata']");
	public By serviziPath = By.xpath("//ol[@class='breadcrumb']/li[text()='Servizi']");
	public By serviziTitleLbl = By.cssSelector("#main > section > div > div > section > div.panel.panel-default.panel-clear > div > h1");
	public By infoEnelEnergiaBtn = By.xpath("//a[contains(@href,'/it/area-clienti/imprese/forniture/infoEE')]");
	public By infoEETitleLbl = By.xpath("//section[not(contains(@class,'hide'))]/div/div/h1");
	public By supplyNumberLbl = By.xpath("//section[not(contains(@class,'hide'))]/div/div/h4/div/h4[@class='panel-title' and not(@data-context='header-info-nonActive')]");
	public By emailField = By.id("email");
	public By telField = By.id("tel");
	public By modificaBtn = By.xpath("//td/a[contains(@href,'MODIFICA')]");
	public By revocaBtn = By.xpath("//td/a[contains(@href,'Revoca')]");
	public By continueActivationBtn = By.name("Prosegui");
	public By attivaBtn = By.xpath("//button[text()='ATTIVA']");
	
	public InfoEnelEnergia_BsnComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkServiziPageStrings() throws Exception {
		By[] locators = {areaRiservataPath, serviziPath, serviziTitleLbl};
		verifyElementsArrayText(locators, serviziPageStrings);
	}
	
	public void checkInfoEEPageElements() throws Exception {
		verifyVisibilityAndText(infoEETitleLbl, infoEETitle);
		By[] locators = {supplyNumberLbl, emailField, telField, modificaBtn, revocaBtn};
		for (int i = 0; i < locators.length; i++) {
			verifyComponentExistence(locators[i]);
		}
	}
	
	public String extractClientNumber() throws Exception {
		WebElement clientInfo = driver.findElement(supplyNumberLbl);
		String infoString = clientInfo.getText();
		String clientNumber = infoString.substring(infoString.indexOf(':')+2);
		//System.out.println("Client number: " + clientNumber);
		
		return clientNumber;
	}
	
	//----- Constants ------
	
	private final String[] serviziPageStrings = {
			"AREA RISERVATA",
			"SERVIZI",
			"Servizi"
	};
	public final String infoEETitle = "Info Enel Energia";
	
}
