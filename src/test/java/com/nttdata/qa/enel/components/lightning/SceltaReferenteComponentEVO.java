package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SceltaReferenteComponentEVO {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	 public SceltaReferenteComponentEVO(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
	 public By radioButtonReferente=By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//table/tbody/tr[1]//label//span[@class='slds-radio_faux']"); 
//     public final String esecuzioneAnticipataRadioButton = "//h2[text()='Contatti e consensi']/ancestor::lightning-card//span[text()='@SCELTA@']/parent::label";
	
	 	//public By campiSelezioneInSezioneReferente=By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='main']//label/span[@class='slds-radio_faux']");
	    //public By buttonConfermaReferente=By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='main']//button[text()='Conferma']");
	       
//	 public final By confermaButton = By.xpath("//div[@aria-label='Referente']//button[text()='Conferma']");
	 public final By confermaButton = By.xpath("//span[contains(text(),'Referente')]/ancestor::div[@role='region']//button[text()='Conferma']");
//	 public final By radioReferente = By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//table/tbody/tr/td/lightning-input");
//	 public final By radioReferente = By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//table/tbody/tr[1]/td[1]//input");
	 public final By radioReferente = By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//table/tbody/tr[1]//td//span[@class='slds-radio_faux']");
	 public final By inputRefente = By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//input[@type='text']");
     public final By NumeroReferente = By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//table/tbody/tr");
     public final By Referente = By.xpath("//span[text()='Referente']/ancestor::div[@role='region']//table/tbody/tr//td[4]//div[@class='slds-truncate']//div[@class='slds-truncate']");
     
	 public final By confermOfferta = By.xpath("//lightning-button[@data-id='ConfirmQuote']");
	 public void press(By oggetto) throws Exception{
		util.waitAndGetElement(oggetto, 30, 1); // attende l'elemento
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	 public void ricercaReferente(String referente) throws Exception{
			util.getFrameActive();
		 	driver.findElement(inputRefente).sendKeys(referente);
		 	
		 	//numero di record = numero di risultati:
		 	
		 	List<WebElement> temp = driver.findElements(NumeroReferente);
		 	
		 	if(temp.size()!=1) {
		 		throw new Exception ("Il numero di record mostrati per la ricerca del referente è diverso da 1 : "+temp);
		 	}
		 
		 	String cf = driver.findElement(Referente).getText();
		 	
		 	if(!cf.equals(referente)) {
		 		throw new Exception ("Il referente ricercato non è stato trovato.");
		 	}
		 	
			spinner.checkSpinners();
		}
	
	
//	 public void selezionaEsecuzioneAnticipata(String valore) throws Exception{
//		String esecuzioneAnticipataXpath = this.esecuzioneAnticipataRadioButton;
//		if(valore.equalsIgnoreCase("SI")) esecuzioneAnticipataXpath = esecuzioneAnticipataXpath.replaceAll("@SCELTA@", "SI");
//		else if(valore.equalsIgnoreCase("NO")) esecuzioneAnticipataXpath = esecuzioneAnticipataXpath.replaceAll("@SCELTA@", "NO");
//		else throw new Exception("Il valore da selezionare scelto per esecuzione anticipata e' diverso da SI/NO. Verificare dati di input");
//		press(By.xpath(esecuzioneAnticipataXpath));
//	}
	
}
