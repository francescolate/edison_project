package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EnterprisePageComponent extends BaseComponent {
	public By pageTitle = By.cssSelector("#main > section > div.image-hero_inner.image-hero-container.container > div > h1");

	public EnterprisePageComponent(WebDriver driver) {
		super(driver);
	}

}
