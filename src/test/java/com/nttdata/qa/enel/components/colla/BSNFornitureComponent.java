package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSNFornitureComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By pagePath = By.xpath("//div[@class='panel-body']//ol");
	public By pageTitle = By.xpath("//h1[text()='Info Enel Energia']");
	public By pageText = By.xpath("//p[contains(text(),'Per essere')]");
	public By pageText1 = By.xpath("//section[@id='nonInscritto']//div[@class='panel panel-default panel-msg']");
	public String headerVoice = "//div[@class='dotcom-header__links dotcom-header__links-custom']//ul//li//a[text()='$VALUE$']";
	public String footerLink = "//main[@id='main']//a[text()='$Value$']";
	public By footerText = By.xpath("//main[@id='main']//li[contains(text(),'Enel Energia Spa')]");
	public By prosegui = By.xpath("//a[text()='Prosegui']");
	
	public BSNFornitureComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void verifyHeaderVoice(String checkObject,String checkElement) throws Exception{
			 By value = By.xpath(checkObject.replace("$VALUE$", checkElement));	
			 String text = driver.findElement(value).getText();
			/* System.out.println(value);
				 System.out.println(text);*/
				 if(! HeaderVoice.contains(text))
					 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
		 }
		
		public void verifyFooterLink(String checkObject,String checkElement) throws Exception{
			 By value = By.xpath(checkObject.replace("$Value$", checkElement));	
			// System.out.println(value);
			 String text = driver.findElement(value).getText();
			//	 System.out.println(text);
				 if(! FooterLink.contains(text))
					 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
		 }
		
		public static final String HeaderVoice = "LUCE E GAS,IMPRESE,INSIEME A TE,STORIE,FUTUR-E,MEDIA,SUPPORTO";
		public static final String Path = "Area riservataFornitureInfo Enel Energia";
		public static final String PageText = "Per essere sempre informato sulla tua fornitura.Su questa fornitura, Info Enel Energia non è ancora attivo.";
		public static final String PageTextNew = "Per essere sempre informato sulla tua fornitura.";
		public static final String PageText1 = "Che cos'è?Info Enel Energia è un servizio di notifica semplice e veloce, che ti tiene sempre aggiornato sulla tua fornitura tramite sms o mail.";
		public static final String FooterLink = "Informazioni Legali,Credits,Privacy,Cookie Policy,Remit,Contattaci";
		public static final String FooterText = "© Enel Energia Spa - Tutti i Diritti Riservati - Gruppo IVA Enel P.IVA 15844561009";
		public static final String FooterTextNew = "© Enel Energia Spa - Tutti i Diritti Riservati - Gruppo IVA Enel P.IVA 15844561009";
}
