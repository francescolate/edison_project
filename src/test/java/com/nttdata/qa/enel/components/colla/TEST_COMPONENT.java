package com.nttdata.qa.enel.components.colla;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class TEST_COMPONENT {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By cognomeInput = By.xpath("//input[@id='form_cognome']");
	public By nomeInput = By.xpath("//input[@id='form_nome']");
	
	public TEST_COMPONENT (WebDriver driver) throws Exception{
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}


	public void launchLink(String url) throws Exception {
	try {
		driver.manage().window().maximize();
		driver.navigate().to(url);
	} catch (Exception e) {
		throw new Exception("it's impossible the link " + url);
		}
	
	}
	
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
		textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
		if (!util.checkElementText(objectWithText, textToCheck)) {
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
			}
			String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
			System.out.println(message);
			throw new Exception(message);
		}
	}
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 60))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyFineButton(By existingButton) throws Exception{
		if (!util.exists(existingButton, 60))
			throw new Exception("object with xpath " + existingButton + " is not exist.");
		
	}
	public void waitForElementToDisplay(By checkObject) throws Exception{
		if (!util.exists(checkObject, 60))
			throw new Exception("object with xpath " + checkObject + " is not exist.");
		
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.contentEquals(Value)){
		textfield_found="YES";
		}
		if (textfield_found.contentEquals("NO")){
			throw new Exception("The expected text" + Value + " is not found:");
				}
	}
		public void compareTextDisabledInput(String xpath, String textToCheck) throws Exception{
			JavascriptExecutor js = (JavascriptExecutor) driver;
	
			String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
	
			if(!s.equals(textToCheck))
				
		throw new Exception("Il testo cercato:\n\t"+textToCheck+"\nè diverso da quello in pagina:\n\t"+s);
}
		
		public void radiobuttonStatus(By checkObject1, By checkObject2,By checkObject3,By checkObject4){
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 30);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject1));
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject2));
			
			WebElement RB1 = driver.findElement(checkObject1);
			WebElement RBLabel1 = driver.findElement(checkObject2);
			WebElement RB2 = driver.findElement(checkObject3);
			WebElement RBLabel2	= driver.findElement(checkObject4);
			
			//element.getText();
			
			if (!RB1.isSelected()){
			RBLabel1.click();
			System.out.println("Radiobutton1 is clicked");
			}	
			else {
			RBLabel2.click();
			System.out.println("Radiobutton2 is clicked");
			}
				
		}
		public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}

		public void verifyRadioButton(By CheckboxObject) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(CheckboxObject));
			
			String Radio="NO";
			WebElement element = driver.findElement(CheckboxObject);
			WebElement label = driver.findElement(By.xpath("//*[@id='sc']/div/div/div/div/div/section/div[2]/fieldset[1]/div[2]/label"));
				
			if (element.isSelected()){
			Radio="YES";
			System.out.println("Radiobutton NO is selected");
			}	
			if (!element.isSelected()){
			Radio = "NO";
				throw new Exception("The radiobutton is not selected");
			}
			
		}
		
		public void VerifyDatieContatti(By checkObject, String K) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String Content_Avail="NO";
					
			List<WebElement> leftMenu = driver.findElements(checkObject);
			Iterator<WebElement> iterator = leftMenu.iterator();   
			Iterator<Entry<String, String>> mapIterator = map.entrySet().iterator();
			
			while(iterator.hasNext()){
				WebElement element = iterator.next();
				String[] accountDetailsArray = ((String) map.get(K)).split(";");
			    String description=element.getText();
			    description= description.replaceAll("(\r\n|\n)", " ");
			    
			    if (description.contentEquals(accountDetailsArray[0]))
			        Content_Avail="YES";
			    else if (description.contentEquals(accountDetailsArray[1]))
			        Content_Avail="YES";
			    else if(description.contentEquals(accountDetailsArray[2]))
			    	Content_Avail="YES";
			    else if (description.contentEquals(accountDetailsArray[3]))
			    	Content_Avail="YES";
			    else if (description.contentEquals(accountDetailsArray[4]))
			    	Content_Avail="YES";
			    else if (description.contentEquals(accountDetailsArray[5]))
			    	Content_Avail="YES";
			    else if (description.contentEquals(accountDetailsArray[6]))
			    	Content_Avail="YES";
			      
		        if (Content_Avail.contentEquals("NO"))
		        	throw new Exception("Contents are not matching");
		    }
			
		}
		public void homepageRESIDENTIALMenu(By checkObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//div[@id='sticky-wrapper']/nav/ul/div/li"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Forniture"))
	        item_found="YES";
	   
	    else if(description.contentEquals("Bollette"))
	        item_found="YES";
	    else if (description.contentEquals("Servizi"))
	        item_found="YES";
	    else if (description.contentEquals("enelpremia WOW!"))
	        item_found="YES";
	    else if (description.contentEquals("Novità"))
	        item_found="YES";
	    else if (description.contentEquals("Spazio Video"))
	        item_found="YES";
	    else if (description.contentEquals("Account"))
	        item_found="YES";
	    else if (description.contentEquals("I tuoi diritti"))
	        item_found="YES";
	    else if (description.contentEquals("Supporto"))
	        item_found="YES";
	    else if (description.contentEquals("Trova Spazio Enel"))
	        item_found="YES";
	    else if (description.contentEquals("Esci"))
	    	item_found="YES";
	   
        if (item_found.contentEquals("NO"))
        	throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'Forniture', 'Bollette','Servizi','enelpremia WOW!','Novità','Spazio Video','Account','I tuoi diritti','Supporto','Trova Spazio Enel','Esci'");
	      
	    }
	 }

	
	public static final Map<String, String> map = Stream.of(new String[][] {
		  { "25", "Titolare Mancuso Salvatore;Codice Fiscale MNCSVT58H26H325L;Telefono Fisso 0818142364;Numero di Cellulare Non presente;Email fabiana.manzo2@yopmail.com;PEC Non presente;Canale di contatto preferito Telefono Cellulare" }, 
		  { "26", "Doe" }, 
		  { "27", "Doe" }, 
		  { "28", "Titolare Di Matteo Tiziana;Codice Fiscale DMTTZN69H67C765I;Telefono Fisso 0818142364;Numero di Cellulare +39 3334126607;Email tdimatteo.tizi@gmail.com;PEC fama@gmail.com;Canale di contatto preferito Telefono Cellulare"}, 
		  { "29", "Doe" }, 
		  { "30", "Doe" }, 
		}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
	

	public void clearInputField(By checkElement) throws Exception{
		util.objectManager(checkElement, util.clear);
	}
	
	public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			/*System.out.println("Normalized:\n"+weText);
			System.out.println(text);*/
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
	
		public String modifyExistingEmail(By by) throws Exception{
			Thread.sleep(5000);
			String s = util.jsGetInputValue(by);
			if(s.contains("1@") || s.contains("2@"))
				s = s.contains("1@") ? s.replace("1@", "2@") : s.replace("2@", "1@");
			else
				s = s.replace("@", "1@");
			changeInputText(by, "");
			WebElement we = util.waitAndGetElement(by);
			Thread.sleep(200);
			we.sendKeys(s);
			return s;
		}
		
		public void changeInputText(By by, String newText) throws Exception{
			String[] array = (by.toString()).split(": ");
			util.setElementValue(array[1], newText);
		}
		
		public String modifyExistingPhone(By by) throws Exception{
			Thread.sleep(6000);
			String s = util.jsGetInputValue(by);
			int n = Integer.parseInt(s.substring(s.length()-1, s.length()));
			n = (n+1)%10;
			s = s.substring(0, (s.length()-1))+n;
			changeInputText(by, "");
			WebElement we = util.waitAndGetElement(by);
			Thread.sleep(200);
			we.sendKeys(s);
			return s;
		}
		
	public static final String PAGE_TITLE = "Dati e Contatti";
	public static final String TITLE_SUBTEXT = "Consulta e aggiorna i tuoi dati di contatto. Se invece vuoi consultare o aggiornare le credenziali di accesso al sito consulta la sezione Modifica Dati Di Registrazione";
	public static final String POPUP_TITLE = "Aggiornamento dati";
	public static final String POPUP_INNERTEXT = "In caso di modifica dei contatti, i dati inseriti saranno resi disponibili dopo qualche ora";
	public static final String DATI_E_CONTATTI_SUBTEXT = "Consulta e aggiorna i tuoi dati di contatto";
	public static final String OPERAZIONE_HEADING = "Dati salvati correttamente";
	public static final String OPERAZIONE_VALUE = "La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve.";
	public static final String TelefonoError = "Numero di telefono non valido. Per proseguire, scrivi un numero di telefono fisso valido oppure lascia il campo vuoto.";
	public static final String EmailError = "Il campo Email è obbligatorio.";
	public static final String EmailPECError = "L'indirizzo email non è valido. Verifica di averlo scritto in modo corretto.";
	public static final String NumeroDiCellulareError = "Il campo Numero di cellulare è obbligatorio.";
	public static final String Origin = "Web";
	public static final String OperationType= "MODIFICA";
	public static final String Reason = "VARIAZIONE ANAGRAFICA";
	public static final String Status= "Chiuso";
}
