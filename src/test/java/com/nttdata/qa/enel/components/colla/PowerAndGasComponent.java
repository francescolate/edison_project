package com.nttdata.qa.enel.components.colla;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PowerAndGasComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	WebDriverWait wait;
	
	public By seeAllPower = By.xpath("//span[@class='text' and contains(text(), 'See all power')]");
	public By seeAllGas = By.xpath("//span[@class='text' and contains(text(), 'See all gas')]");
	public By contactConsultant = By.xpath("//span[@class='text' and contains(text(), 'CONTACT A CONSULTANT')]");
	public By search = By.xpath("//span[@class='icon-search-small']");
	
	@FindBy(xpath = "//span[@class='text' and contains(text(), 'See all power')]")
	public WebElement seeAllPowerWE;
	
	@FindBy(xpath = "//span[@class='text' and contains(text(), 'See all gas')]")
	public WebElement seeAllGasWE;
	
	@FindBy(xpath = "//span[@class='text' and contains(text(), 'CONTACT A CONSULTANT')]")
	public WebElement contactConsultantWE;
	
	@FindBy(xpath = "//span[@class='icon-search-small']")
	public WebElement searchWE;	
	
	
	
	public PowerAndGasComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
 
 public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
 public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
    
    public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
    


    
    public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
    
    public void clickWebElement(WebElement we){
		wait.until(ExpectedConditions.elementToBeClickable(we)).click();
    }
    
			
    
    

}
