package com.nttdata.qa.enel.components.colla;

	import java.util.List;

import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
	import com.nttdata.qa.enel.util.SpinnerManager;

	public class LoginPageComponent {
		WebDriver driver;
		SeleniumUtilities util;
		
		public By logoEnel = By.xpath("//img[contains(@class,'logoimg')]");
		public By iconUser = By.xpath("//span[@class='icon-user']");
		public By loginPage = By.xpath("//form[@id='formlogin']");
		public By username = By.id("txtLoginUsername");
		public By password = By.id("txtLoginPassword");
		public By buttonLoginAccedi = By.id("login-btn");
		public By buttonAccedi = By.xpath("//*[@class='openlogin']");
		public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	    public By loginSuccessful= By.xpath("//div[@class='heading']/p[text()='In questa sezione potrai gestire le tue forniture']");
	   // public By buttonPopupLogin=By.xpath("//button[@class='remodal-close popupLogin-close']");
	    public By logout=By.id("disconnetti");
	    public By accountButton=By.xpath("//button[@id='user']");
	    public By esciLink=By.xpath("//a[@id='tr_disconnetti']");
	    
	    //Others 
	    public By RecUserName=By.xpath("//*[@id='formlogin']/div[3]/div/span[2]/a");
	    public By RecUserPasswd=By.xpath("//*[@id='formlogin']/div[3]/div/span[1]/a");
	    public By GoogleSignIn=By.xpath("//div[@class= 'login-btn'][1]");
	    public By FacebookSignIn=By.xpath("//button[@class='btn-cta--facebook']");
	    public By AccountTxt=By.xpath("//font[contains(text(),'Dont have an account yet?')]");
	    public By SignIn=By.xpath("//a[@class='btn-cta footer-hero-btn']");
	    //public By LoginProbLink=By.xpath("//a[.='Login problems?']");
	    public By LoginProbLink=By.xpath("//a[.='clicca qui']");
	    public By LoginProb=By.xpath("//*[@id='formlogin']/div[4]/p[1]/text()");
	    public By homeFullscreenAlertCloseButton = By.xpath("//div[@id='fsa-close-button']");
	    public By RegisterPrivate=By.xpath("//*[contains(text(),'Area Clienti Casa')]");
	    public By RegisterBusiness=By.xpath("//*[contains(text(),'Area Clienti Impresa')]");
	    public By RegisterPrivateBtn=By.xpath("//h4[contains(text(),'Casa')]/following-sibling::p/child::a[contains(text(),'Entra')]");
	    public By RegisterBusinessBtn=By.xpath("//li[2]//div[1]//div[1]//div[1]//p[1]//a[1]");
	    public By UsernameValidation=By.xpath("//*[@id='formlogin']/p[1]");
	    public By PasswdValidation=By.xpath("//*[@id='formlogin']/p[3]");
	    
	    //Private Area Page
	    public By LeftForniture=By.xpath("//*[@id='idFornitura']/span[2]");
	    public By LeftBollette=By.xpath("//*[@id='bollette']/span[2]");
	    public By LeftServizi=By.xpath("//*[@id='servizi']/span[2]");
	    public By Welcome=By.xpath("//*[@id='mainContentWrapper']/div[1]/h1/font");
	    public By WelSub=By.xpath("//*[@id=mainContentWrapper]/div[1]/p/font/font");
	    public By Supplies=By.xpath("//*[@id='lista-forniture']/div[1]/h2/font/font");
	    public By SuppSub=By.xpath("//*[@id='lista-forniture']/div[1]/p/font/font");
	    public By accediAMyEnelLabel=By.xpath("//div[@class='login-details']//h1[@aria-label='accedi a myenel']");		
		
	    //LeftMenu
	    public By LeftMenuList = By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]");
	    public By BusinLeftMenuList = By.xpath("//div[@id='sticky-wrapper']/nav/ul/div/li");
	    public By BusinessHomePage = By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a");
	    public By haiProblemiText  = By.xpath("//*[@id='formlogin']/div[contains(@class,'help-message')]");
	    public By nonHaiAccount = By.xpath("//form[@id='formlogin']//descendant::p[contains(text(),'Non hai')]");
	    public By registratiButton = By.xpath("//form[@id='formlogin']//descendant::a[@class='btn-cta footer-hero-btn']");
	    public By googleUserId = By.xpath("//input[@id = 'identifierId']");
	    public By nextButton = By.xpath("//div[@id = 'identifierNext']");
	    public By googlePassword = By.xpath("//div[@id='password']");
	    public By nextButtonPassword = By.xpath("//div[@id='passwordNext']");
	    public By recupraUsernameText = By.xpath("//div[@id='recovery-username-step1']//descendant::p[contains(text(),'Inserisci')]");
	    public By codiceFiscaleLable = By.xpath("//form[@id='formRecoveryUsernameStep1']//descendant::label");
	    public By codiceFiscaleField = By.xpath("//input[@id='rec-username-fiscal-code']");
	    public By continuaButton = By.xpath("//a[@id='rec-username-button']");
	    public By campoObbligatoriaErrorText = By.xpath("//div[@id='errore_fiscal-code']/span[@class='errorMsg']");
	   // public By recuperaUsernameHeading = By.xpath("//div[@id='recovery-username-section']//descendant::h1");
	    public By recuperaUsernameHeading = By.xpath("//div[@id='recovery-username-step-review']//descendant::h1[.='Recupera Username']");
	    public By recuperaUsernameParagraph1 = By.xpath("//div[@id='recovery-username-section']//descendant::p[contains(text(),'Ciao')]");
	   // public By recuperaEmail = By.xpath("//div[@id='recapMail']");
	    public By recuperaEmail = By.xpath("//div[@id='recovery-username-step-review']//div[@class='info-recovery-username-left']/div[1]");
	    //public By recuperaCellulare = By.xpath("//div[@id='recovery-username-step2']/div[@id='recapPhone']");
	    public By recuperaCellulare = By.xpath("//div[@id='div-phones']");
	    public By recuperaUsernameParagraph2 = By.xpath("//div[@id='recovery-username-section']//descendant::p[contains(text(),'Per accedere')]");
	    public By recuperaUsernameParagraph3 = By.xpath("//div[@id='recovery-username-section']//descendant::p[contains(text(),'Se non')]");
	    //public By accediButton = By.xpath("//div[@id='recovery-username-step2']//descendant::button[@class='btn btn-pink']");
	    public By accediButton = By.xpath("//div[@id='recovery-username-step-review']//descendant::button[@class='btn btn-pink']");
	    public By modificaUserNameButton = By.xpath("//button[@id='change-username-button']");
	    public String accountItem = "//*[text()='$ACCOUNT_ITEM$']/ancestor::li";
	    
	    public By inputCfVerifica=By.xpath("//input[@id='tp-cfverifica']");//FR 25.08.2021
	    public By buttonCfVerifica=By.xpath("//button[@id='verify-cf']");//FR 25.08.2021
	    
	    public LoginPageComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

//		public void launchLink(String url) throws Exception {
//			try {
//				driver.manage().window().maximize();
//				driver.navigate().to(url);
//			} catch (Exception e) {
//				throw new Exception("it's impossible the link " + url);
//
//			}
//		}
		
		public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}
		
		public void homepageRESIDENTIALMenu(By checkObject) throws Exception {
			
		    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		                   
		    String item_found="NO";
		    List<WebElement> leftMenu = driver.findElements(By.xpath("//div[@id='sticky-wrapper']/nav/ul/div/li"));
		   
		    for(WebElement element : leftMenu){
		    String description=element.getText();
		    description= description.replaceAll("(\r\n|\n)", " ");
		       
		    if (description.contentEquals("Forniture"))
		        item_found="YES";
		   
		    else if(description.contentEquals("Bollette"))
		        item_found="YES";
		    else if (description.contentEquals("Servizi"))
		        item_found="YES";
		    else if (description.contentEquals("enelpremia WOW!"))
		        item_found="YES";
		    else if (description.contentEquals("Novità"))
		        item_found="YES";
		    else if (description.contentEquals("Spazio Video"))
		        item_found="YES";
		    else if (description.contentEquals("Account"))
		        item_found="YES";
		    else if (description.contentEquals("I tuoi diritti"))
		        item_found="YES";
		    else if (description.contentEquals("Supporto"))
		        item_found="YES";
		    else if (description.contentEquals("Trova Spazio Enel"))
		        item_found="YES";
		    else if (description.contentEquals("Esci"))
		    	item_found="YES";
		   
		        if (item_found.contentEquals("NO"))
		        throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'Forniture', 'Bollette','Servizi','enelpremia WOW!','Novità','Spazio Video','Account','I tuoi diritti','Supporto','Trova Spazio Enel','Esci'");
		        
		        
		            }
		    
		        }


		public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
		
		public void BusinessHomePageMenu(By BusinessObject) throws Exception {
			
		    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(BusinessObject));
		                   
		    String item_found="NO";
		    List<WebElement> BusiLeftMenu = driver.findElements(By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a"));
		   
		    for(WebElement element : BusiLeftMenu){
		    String description=element.getText();
		    description= description.replaceAll("(\r\n|\n)", " ");
		       
		    if (description.contentEquals("HOMEPAGE"))
		        item_found="YES";
		   
		    else if(description.contentEquals("BOLLETTE"))
		        item_found="YES";
		    else if (description.contentEquals("FORNITURE"))
		        item_found="YES";
		    else if (description.contentEquals("SERVIZI"))
		        item_found="YES";
		    else if (description.contentEquals("MESSAGGI"))
		        item_found="YES";
		    else if (description.contentEquals("STATO RICHIESTE"))
		        item_found="YES";
		    else if (description.contentEquals("REPORT BILLING MANAGEMENT"))
		        item_found="YES";
		    else if (description.contentEquals("AREA CLIENTI CASA"))
		        item_found="YES";
		    else if (description.contentEquals("CARICA DOCUMENTI"))
		        item_found="YES";
		    else if (description.contentEquals("CONTATTI"))
		        item_found="YES";
		    else if (description.contentEquals("I TUOI DIRITTI"))
		    	item_found="YES";
		    else if (description.contentEquals("LOGOUT"))
		    	item_found="YES";
		   		   
		        if (item_found.contentEquals("NO"))
		        throw new Exception("on the 'https://www-colla.enel.it/ Business left menu section are not present with description 'HomePage', 'FORNITURE','SERVIZI','MESSAGGI','STATO RICHIESTE','REPORT BILLING MANAGEMENT','CARICA DOCUMENTI','CONTATTI','LOGOUT'");
		        
		        
		            }
		    
		}

		public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public boolean checkComponentExistence(By existingObject) throws Exception {
			return util.exists(existingObject, 10);
		}
		
		public void clickComponentIfExist(By existObject) throws Exception {
			if (util.exists(existObject, 15)) {
				util.objectManager(existObject, util.scrollToVisibility, false);
			    util.objectManager(existObject, util.scrollAndClick);
			}
		}
				
		public void enterLoginParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void backBrowser() throws Exception {
			try {
				driver.navigate().back();
				Thread.sleep(5000);
				
			} catch (Exception e) {
				throw new Exception("Previous Page not displayed");

			}
		
			/*public void checkUrl(String url) throws Exception{
				String link=driver.getCurrentUrl();
				if (!link.contentEquals(url))
					 throw new Exception("the url of page is different than expected: actual url of page is "+link+";expected url of page is "+url);
				}
			
			*/
			
		
			
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void hanldeFullscreenMessage(By by) throws Exception{
			if(util.verifyExistence(by, 60)){
				util.objectManager(by, util.scrollToVisibility, false);
				util.objectManager(by, util.scrollAndClick);
			}
		}
		
	    public void clickWithJS(By oggettocliccabile) throws Exception {
	        util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
	        util.jsClickElement(oggettocliccabile);
	    }
		
		public static final String HAI_PROBLEMI_TEXT = "Se non riesci adaccedereoppure vuoi ricevere informazioni sul Profilo Unicoclicca qui";
		public static final String NON_HAI_ACCOUNT = "Non hai ancora creato il tuo Profilo Unico?";
		public static final String RECUPRA_USERNAME_TEXT = "Inserisci il tuo codice fiscale per poter recuperare il tuo username";
		public static final String CODICE_FISCALE_LABEL = "Codice Fiscale";
		public static final String CAMPO_OBBLIGATORIA = "Campo obbligatorio";
		public static final String RECUPERA_USERNAME_HEADING = "Recupera Username";
		public static final String RECUPERA_USERNAME_PARAGRAPH1 = "Ciao , ecco il tuo username";
		public static final String RECUPERA_EMAIL = "E-mail: r3*****@gmail.com";
		public static final String RECUPERA_CELLULARE = "Numero di cellulare: 3669******53";
		public static final String RECUPERA_USERNAME_PARAGRAPH2 = "Per accedere ai servizi digitali dell'Area Clienti e dell'APP di Enel Energia puoi utilizzare come usernamel'e-mail oppure il numero di cellulare inseriti in fase di registrazione.";
		public static final String RECUPERA_USERNAME_PARAGRAPH3 = "Se non hai più accesso alla tua e-mail o al cellulare, potrai modificare il tuo username in pochi passaggi";
	}


