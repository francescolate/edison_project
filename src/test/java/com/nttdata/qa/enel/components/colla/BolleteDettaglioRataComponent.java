package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BolleteDettaglioRataComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By datiAggiornati = By.xpath("//main/div[@id='datiAggiornati']");
	public By VAIATUTTELEBOLLETTEButton = By.xpath("//a[@class='button_second']");
	public By SCARICAPDFPIANOButton = By.xpath("//a[@class='btnFirstAppearanceMaker']");
	public By gasIcon = By.xpath("//span[@class='icon-line-flame']");
	public By supplyHeading = By.xpath("//h1[@id='fornitura01']");
	public By addressLabel = By.xpath("//dl[@class='supply-details-heading']/span[1]/dt");
	public By address = By.xpath("//dl[@class='supply-details-heading']/span[1]/dd/b");
	public By numeroClienteLabel = By.xpath("//dl[@class='supply-details-heading']/span[3]/dt");
	public By numeroCliente = By.xpath("//dl[@class='supply-details-heading']/span[3]/dd");
	public By dataEmissioneLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[1]/dt");
	public By dataEmissione= By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[1]/dd/span");
	public By dataScadenzaLabel =By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[2]/dt");
	public By dataScadenza = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[2]/dd/span");
	public By dataPagamentoLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[3]/dt");
	public By dataPagamento = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[3]/dd/span");
	public By statoPagamentoLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[4]/dt");
	public By statoPagamento = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[4]/dd[@class='red-color']/span");
	public By modalitàdiPagamentoLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[5]/dt");
	public By modalitàdiPagamento = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[5]/dd/span");
	public By attestazionePagamentoLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[6]/dt");
	public By attestazionePagamento = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[6]/dd/span");
	public By importoRataLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[6]/dt");
	public By importoRata = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[7]/dd/span");
	public By totaledaPagareLabel = By.xpath("//div[@class='bill totalCost']/h4");
	public By totaledaPagare = By.xpath("//div[@class='bill totalCost']/span");
	public By pagaOnline = By.xpath("//button[@class='button_first']");
	public By dettaglioPiano = By.xpath("//button[@class='button_second']");
	public By billDetails = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt");
	
	public static final String[] exp = {"Data Emissione","Data Scadenza","Stato Pagamento","Data Pagamento","Modalità di Pagamento","Attestazione di Pagamento","Importo Rata"};
	public BolleteDettaglioRataComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void verifyBilldetails(By checkObject, String[] exp) throws Exception{
	       
        List <WebElement> list  = driver.findElements(billDetails);
            for (WebElement we : list) {
           // System.out.println(we.getText());
            if(!Arrays.asList(exp).contains(we.getText()))
                throw new Exception ("This page does not has the following Bill Details:'Data Emissione','Data Scadenza','Stato Pagamento','Data Pagamento','Modalità di Pagamento','Attestazione di Pagamento'");
                   }
    }
}
