package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BolleteDettaglioPianoComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By VAIATUTTELEBOLLETTEButton = By.xpath("//a[@class='button_second']");
	public By SCARICAPDFPIANOButton = By.xpath("//a[@class='btnFirstAppearanceMaker']");
	public By addressLabel = By.xpath("//dl[@class='supply-details-heading']/span[1]/dt");
	public By numeroClienteLabel = By.xpath("//dl[@class='supply-details-heading']/span[2]/dt");
	public By modalitàdiPagamentoLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[6]/dt");
	public By attestazionePagamentoLabel = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[6]/dt");
	public By totaledaPagareLabel = By.xpath("//div[@class='bill totalCost']/h4");
	public By numeroPiano = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[1]/dt");
	public By importoRateizzato = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[2]/dt");
	public By interessiDiMora = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[3]/dt");
	public By interessiDiDilazione = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[4]/dt");
	public By numeroRate = By.xpath("//dl/span[5]/dt");
	public By addebitoConBollettino = By.xpath("//dl/span[6]/dd");
	public By commodityIcon = By.xpath("//ul[@class='instalmentPlan-list'][1]//span[@class='dsc-icon-container']/span[@class='icon-line-flame']");
	public By commodity = By.xpath("//h2[@id='instalmentPlan-list-heading']");
	public By rataN = By.xpath("//span[@id='bill_0']/dt[@class='label']");
	public By NDocumento = By.xpath("//ul[@class='instalmentPlan-list'][1]//span[3]/dt[@class='label']");
	public By scade = By.xpath("//ul[@class='instalmentPlan-list'][1]//span[4]/dt[@class='label']");
	public By importo = By.xpath("//ul[@class='instalmentPlan-list'][1]//span[5]/dt[@class='label']");
	public By statoPagamento = By.xpath("//ul[@class='instalmentPlan-list'][1]//span[6]/dt[@class='label']");
	public By pagataDaPagare = By.xpath("//ul[@class='instalmentPlan-list'][1]/li//span[6]/dd[@class='valore']");
	public By paymentAlert = By.xpath("//p[@id='alert-legend01']");
	public By billDetails = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt");

	
	public static final String[] exp = {"Totale da Pagare","Addebito con bollettino","Modalità di Pagamento","Numero Rate","Interessi di Dilazione","Interessi di Mora","Importo Rateizzato","Numero Piano","Data Emissione","Data Scadenza","Stato Pagamento","Data Pagamento","Rata n°","N° Documento","Modalità di Pagamento","Scade il","Attestazione di Pagamento","Importo"};
	
	public BolleteDettaglioPianoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyBilldetails(By checkObject, String[] exp) throws Exception{
	       
        List <WebElement> list  = driver.findElements(billDetails);
            for (WebElement we : list) {
           // System.out.println(we.getText());
            if(!Arrays.asList(exp).contains(we.getText()))
                throw new Exception ("This page does not has the following Bill Details:'Data Emissione','Data Scadenza','Stato Pagamento','Data Pagamento','Modalità di Pagamento','Attestazione di Pagamento'");
                   }
    }

}
