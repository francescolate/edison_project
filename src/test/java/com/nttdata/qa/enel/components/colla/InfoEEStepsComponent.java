package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;

public class InfoEEStepsComponent extends BaseComponent {
	enum OperationType {
		ACTIVATION,
		MODIFICATION
	}
	
	public boolean activationMailIsCertified = false;
	
	public By step1Title = By.cssSelector("#infoenelenergia > h4:nth-child(1)");
	public By comunicaLetturaTitle = By.cssSelector("#checkboxGroup0 > fieldset:nth-child(1) > legend:nth-child(1) > span:nth-child(1)");
	public By comunicaLetturaSmsChk = By.cssSelector("#shallReadSMS");
	public By comunicaLetturaMailChk = By.cssSelector("#shallReadEmail");
	public By emissioneBollettaTitle = By.cssSelector("#checkboxGroup1 > fieldset:nth-child(1) > legend:nth-child(1) > span:nth-child(1)");
	public By emissioneBollettaSmsChk = By.cssSelector("#issueBillSMS");
	public By emissioneBollettaMailChk = By.cssSelector("#issueBillEmail");
	public By avvenutoPagamentoTitle = By.cssSelector("#checkboxGroup2 > fieldset:nth-child(1) > legend:nth-child(1) > span:nth-child(1)");
	public By avvenutoPagamentoSmsChk = By.cssSelector("#paymentNoticeSMS");
	public By avvenutoPagamentoMailChk = By.cssSelector("#paymentNoticeEmail");
	public By scadenzaTitle = By.cssSelector("#checkboxGroup3 > fieldset:nth-child(1) > legend:nth-child(1) > span:nth-child(1)");
	public By scadenzaSmsChk = By.cssSelector("#nextDeadlineSMS");
	public By scadenzaMailChk = By.cssSelector("#nextDeadlineEmail");
	public By sollecitoTitle = By.cssSelector("#checkboxGroup4 > fieldset:nth-child(1) > legend:nth-child(1) > span:nth-child(1)");
	public By sollecitoSmsChk = By.cssSelector("#requestPaymentSMS");
	public By sollecitoMailChk = By.cssSelector("#requestPaymentEmail");
	public By avvisoTitle = By.cssSelector("#checkboxGroup5 > fieldset:nth-child(1) > legend:nth-child(1) > span:nth-child(1)");
	public By avvisoSmsChk = By.cssSelector("#consumptionsSMS");
	public By avvisoMailChk = By.cssSelector("#consumptionsEmail");
	public By emailField = By.cssSelector("#emailFieldInput");
	public By phoneField = By.cssSelector("#mobileFieldInput");
	public By updateChk = By.cssSelector("#checkBoxAggiorna\\ i\\ dati\\ anagrafici");
	public By continueBtn = By.cssSelector("div.button-container:nth-child(3) > button:nth-child(2)");
	public By step3Title = By.cssSelector("#id_title");
	public By step3Subtitle = By.cssSelector(".typ-container > p:nth-child(2)");
	public By step3Esito = By.cssSelector(".step3 > span:nth-child(4)");
//	public By endBtn = By.cssSelector(".button_first");
	public By endBtn = By.xpath("//button/span[text()='Continua']");
	public By endBtnNew = By.xpath("//button/span[text()='Fine']");
	public By emailConfirmField = By.id("emailFieldConfirmInput");
	public By[] typActivationLocators = {
			By.cssSelector("#Header > div > h3"),
			By.xpath("//p[@class='slds-text-heading--xx-small' and contains(text(),'grazie per aver confermato il tuo indirizzo EMAIL')]"),
			By.cssSelector("#tb > tr > td:nth-child(2)"),
			By.cssSelector("#tb > tr > td:nth-child(3)"),
			By.cssSelector("#tb > tr > td:nth-child(4)"),
			By.cssSelector("#tb > tr > td:nth-child(5)"),
			By.cssSelector("#tb > tr > td:nth-child(6)")
	};
	
	public InfoEEStepsComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkStep1ElementsExistence() throws Exception {
		By[] locators = {
				step1Title,
				comunicaLetturaTitle,
				comunicaLetturaSmsChk,
				comunicaLetturaMailChk,
				emissioneBollettaTitle,
				emissioneBollettaSmsChk,
				emissioneBollettaMailChk,
				avvenutoPagamentoTitle,
				avvenutoPagamentoSmsChk,
				avvenutoPagamentoMailChk,
				scadenzaTitle,
				scadenzaSmsChk,
				scadenzaMailChk,
				sollecitoTitle,
				sollecitoSmsChk,
				sollecitoMailChk,
				avvisoTitle,
				avvisoSmsChk,
				avvisoMailChk,
				emailField,
				phoneField,
				updateChk,
				continueBtn
		};
		for (int i = 0; i < locators.length; i++) {
//			verifyComponentExistence(locators[i]);
			verifyComponentVisibility(locators[i]);
		}
	}
	
	public void modifyItem(By locator) throws Exception {
		WebElement element = driver.findElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}
	
	public void changeCertificationMail(String address) throws Exception {
		insertText(emailField, address);
		verifyComponentVisibility(emailConfirmField);
		insertText(emailConfirmField, address);
	}
	
	public void modifyItemAndContinue(By locator) throws Exception {
		WebElement element = driver.findElement(locator);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
		WebElement updateChkBox = driver.findElement(updateChk);
		js.executeScript("arguments[0].click();", updateChkBox);
		clickComponent(continueBtn);
	}
	
	public void checkModifyResults() throws Exception {
		checkResults(OperationType.MODIFICATION);
	}
	
	public void checkActivationResults() throws Exception {
		checkResults(OperationType.ACTIVATION);
	}
	
	private void checkResults(OperationType type) throws Exception {
		By[] locators = {
				step3Title,
				step3Subtitle
		};
		switch (type) {
		case ACTIVATION:
			try {
				String[] strings = {step3ActivationTitle, step3ActivationDescriptionCertified};
				verifyElementsArrayText(locators, strings);
				activationMailIsCertified = true;
				break;
			} catch (Exception e) {
				String[] strings = {step3ActivationTitle, step3ActivationDescriptioNotCertified};
				verifyElementsArrayText(locators, strings);
				activationMailIsCertified = false;
				break;
			}
		case MODIFICATION:
			verifyElementsArrayText(locators, step3ModificationStrings);
			break;
		}
		verifyElementColor(step3Esito, GREEN_COLOUR);
	}
	
	public void step2Continue() throws Exception {
		/*
		 * We'll have to use JavascriptExecutor to attempt to scroll to bottom then click the continue button.
		 * The reason is at this step the page behaviour is very inconsistent:
		 * - sometimes the page refuses to scroll altogether
		 * - the chat activation button sometimes positions itself over the coninue button intercepting the click
		 */
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Long height = (Long)js.executeScript("return Math.max(\n" + 
				"  document.body.scrollHeight, document.documentElement.scrollHeight,\n" + 
				"  document.body.offsetHeight, document.documentElement.offsetHeight,\n" + 
				"  document.body.clientHeight, document.documentElement.clientHeight\n" + 
				")");
		js.executeScript("window.scrollTo(0, " + height + ")");
		verifyComponentVisibility(continueBtn);
		util.objectManager(continueBtn, util.clickwithjse);
	}
	
	public void checkActivationTyp() throws Exception {
		verifyElementsArrayVisibility(typActivationLocators);
//		for (int i = 0; i < typActivationLocators.length; i++) {
//			verifyComponentExistence(typActivationLocators[i]);
//		}
	}
	
	//----- Constants -----
	
	private final String[] step3ModificationStrings = {
			"Operazione eseguita correttamente",
			"La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve."
	};
	private final String step3ActivationTitle = "Operazione eseguita correttamente";
	private final String step3ActivationDescriptionCertified = "La tua richiesta è stata presa in carico, il servizio sarà attivo a breve su tutte le forniture selezionate.";
	private final String step3ActivationDescriptioNotCertified = "La tua richiesta è stata accolta. Per completare l'attivazione del servizio clicca sul link dell'SMS e/o della mail che ti abbiamo inviato.";
	private final Color GREEN_COLOUR = Color.fromString("#008C5A");
}
