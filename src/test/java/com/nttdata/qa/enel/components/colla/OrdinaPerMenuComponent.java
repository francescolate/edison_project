package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OrdinaPerMenuComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By defaultLabel=By.xpath("//span[@id='offerOrderSelectBoxItText' and (text()='In Promozione') ]");
	public By OrdinaperMenu=By.xpath("//span[@id='offerOrderSelectBoxIt']");
	public By labelOrdinaPer=By.xpath("//div[@class='select-menu-section filter-dropdown' ]//label[text()='Ordina Per: ']");
	
	public OrdinaPerMenuComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggeto web con xpath " + existentObject + " non esiste.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}
	
	public void scrollComponent(By oggetto) throws Exception {
		util.objectManager(oggetto, util.scrollToVisibility, false);
	}
		
	public void selectionOptionsFromListbox (By OrdinaperObject,String option)throws Exception {
		driver.findElement(OrdinaperObject).click();
		Thread.currentThread().sleep(2000);
		String found="NO";
		
	    List <WebElement> list = driver.findElements(By.xpath("//ul[@id='offerOrderSelectBoxItOptions']//li//a"));
   
			for (int i = 1; i <= list.size(); i++) {
				String description = driver.findElement(By.xpath("//ul[@id='offerOrderSelectBoxItOptions']//li["+i+"]//a")).getText();
				 if (description.contentEquals(option)) {
						 driver.findElement(By.xpath("//ul[@id='offerOrderSelectBoxItOptions']//li["+i+"]//a")).click();
						 found="YES";
				         break;
				         }
			   }
			
			if (found.contentEquals("NO"))
					 throw new Exception("on Page 'Luce e Gas' is not possible to select the label "+option+" from menu 'ORDINA PER:'");
	}	
	
	public void checkOptionsFromListbox (By OrdinaperObject)throws Exception {
		driver.findElement(OrdinaperObject).click();
		Thread.currentThread().sleep(2000);
		
	    List <WebElement> list = driver.findElements(By.xpath("//ul[@id='offerOrderSelectBoxItOptions']//li//a"));
   
			for (int i = 1; i <= list.size(); i++) {
				String description = driver.findElement(By.xpath("//ul[@id='offerOrderSelectBoxItOptions']//li["+i+"]//a")).getText();
				 if (!description.contentEquals("In Promozione") && !description.contentEquals("Bioraria") && !description.contentEquals("Monoraria")) 
					 throw new Exception("sulla pagina web 'Luce e Gas' nel menu' 'ORDINA PER: ' non sono visibili le opzioni: In Promozione,Bioraria,Monoraria");
				         	}
		driver.findElement(OrdinaperObject).click();
	}
}
