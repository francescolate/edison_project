package com.nttdata.qa.enel.components.lightning;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ContattiConsensiComponent extends BaseComponent{

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public ContattiConsensiComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util=new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}


//	public final String esecuzioneAnticipataRadioButton = "//h2[text()='Contatti e consensi']/ancestor::lightning-card//span[text()='@SCELTA@']/parent::label";
	public final String esecuzioneAnticipataRadioButton = "//h2[text()='Contatti e consensi']/ancestor::div[@class='slds-card']//span[text()='@SCELTA@']/parent::label";
	public final By confermaButton = By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[@role='region']//button[text()='Conferma']");
	public final By SalvaInBozzaButton = By.xpath("//lightning-button[@data-id='SaveAsDraft']");
	public final By iButton = By.xpath("//lightning-button-icon[@data-id='dirittoR']");
//	public final By popupDirittoRipensamento = By.xpath("//div[@data-id='dirittoRipensamento']");
	public final By popupDirittoRipensamentoIntestazione = By.xpath("//div[@data-id='popover']//header/h2");
	public final By popupDirittoRipensamentoTesto = By.xpath("//div[@data-id='popover']//div/slot/p");
	public final By checkboxCondizioniContrattuali = By.xpath("//span[contains(text(),'Accettazione Condizioni Contrattuali')]/ancestor::div[1]/input[@type='checkbox']");
	public final By accettaComprensioneCondContrat = By.xpath("//span[text()='SI']/ancestor::span[@class='slds-radio']/label/span[1]");

	public final By confermOfferta = By.xpath("//lightning-button[@data-id='ConfirmQuote']");
	public void press(By oggetto) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void pressConferma(By oggetto) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		if(!util.exists(By.xpath("//lightning-button[@data-id='ConfirmQuote']/button[@disabled]"), 5))
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}


	public void verificaInformativaDirittoRipensamento() throws Exception{
		util.objectManager(iButton, util.scrollAndClick);
		if(!util.verifyExistence(popupDirittoRipensamentoIntestazione, 5)) throw new Exception("Dopo aver cliccato sulla i non viene mostrata la popup contenente l'informativa sul diritto di ripensamento");
		String testo = util.getElementText(popupDirittoRipensamentoIntestazione)+util.getElementText(popupDirittoRipensamentoTesto);
//		System.out.println(testo);
		if(!testo.contentEquals(dirittoRipensamento)) throw new Exception("Il testo del diritto ripensamento risulta essere diverso da "+dirittoRipensamento+". Verificare la documentazione");
		util.objectManager(iButton, util.scrollAndClick);
		
	}
	
	public void selezionaEsecuzioneAnticipata(String valore) throws Exception{
		String esecuzioneAnticipataXpath = this.esecuzioneAnticipataRadioButton;
		if(valore.equalsIgnoreCase("SI")) esecuzioneAnticipataXpath = esecuzioneAnticipataXpath.replaceAll("@SCELTA@", "SI");
		else if(valore.equalsIgnoreCase("NO")) esecuzioneAnticipataXpath = esecuzioneAnticipataXpath.replaceAll("@SCELTA@", "NO");
		else throw new Exception("Il valore da selezionare scelto per esecuzione anticipata e' diverso da SI/NO. Verificare dati di input");
		press(By.xpath(esecuzioneAnticipataXpath));
	}
	
	
	public void verificaPopolamentoCampiContatti(String telefono,String cellulare, String email) throws Exception{
		String phone = util.waitAndGetElement(By.xpath("//label[text()='Telefono']/following::input"),true).getAttribute("value");
		String cell = util.waitAndGetElement(By.xpath("//label[text()='Cellulare']/following::input"),true).getAttribute("value");
		String mail = util.waitAndGetElement(By.xpath("//label[text()='Email']/following::input"),true).getAttribute("value");
		if(!phone.contentEquals(telefono)) throw new Exception("Nella sezione Contatti e Consensi il numero di telefono risulta diverso da "+telefono+". Valore riscontrato "+phone);
		if(!cell.contentEquals(cellulare)) throw new Exception("Nella sezione Contatti e Consensi il numero di cellulare risulta diverso da "+cellulare+". Valore riscontrato "+cell);
		if(!mail.contentEquals(email)) throw new Exception("Nella sezione Contatti e Consensi la mail risulta diversa da "+email+". Valore riscontrato "+mail);
	}
	
	public void verificaPopolamentoCampiContatti() throws Exception{
		String phone = util.waitAndGetElement(By.xpath("//lightning-layout[@class='slds-grid slds-wrap']//label[text()='Telefono']/ancestor::div[1]/div/input"),true).getAttribute("value");
		String cell = util.waitAndGetElement(By.xpath("//lightning-layout[@class='slds-grid slds-wrap']//label[text()='Cellulare']/ancestor::div[1]/div/input"),true).getAttribute("value");
		String mail = util.waitAndGetElement(By.xpath("//lightning-layout[@class='slds-grid slds-wrap']//label[text()='Email']/ancestor::div[1]/div/input"),true).getAttribute("value");
		if(phone.length() < 1 ) throw new Exception("Nella sezione Contatti e Consensi il numero di telefono risulta essere non popolato");
		if(cell.length() < 1) throw new Exception("Nella sezione Contatti e Consensi il numero di cellulare risulta essere non popolato");
		if(mail.length() < 1) throw new Exception("Nella sezione Contatti e Consensi la mail risulta essere non popolato");
	}
	
	
	public void selezionaFasciaContabilita(String text) throws Exception{
		util.selectLighningValue("Fascia Contattabilità", text);
	}
	
	public void selezionaConsensoProfilazione(String text) throws Exception{
		util.selectLighningValue("Consenso Profilazione", text);
	}
	
	public void selezionaConsensoMarketingTelefonico(String text) throws Exception{
		util.selectLighningValue("Consenso Marketing Telefonico", text);
	}
	
	public void selezionaConsensoTerziTelefonico(String text) throws Exception{
		util.selectLighningValue("Consenso Terzi Telefonico", text);
	}
	
	public void selezionaConsensoMarketingCellulare(String text) throws Exception{
		util.selectLighningValue("Consenso Marketing Cellulare", text);
	}
	
	public void selezionaConsensoTerziCellulare(String text) throws Exception{
		util.selectLighningValue("Consenso Terzi Cellulare", text);
	}
	
	public void selezionaConsensoMarketingEmail(String text) throws Exception{
		util.selectLighningValue("Consenso Marketing Email", text);
	}
	
	public void selezionaConsensoTerziEmail(String text) throws Exception{
		util.selectLighningValue("Consenso Terzi Email", text);
	}
	
	public void verificaPreselezioneAccCondizPrecontattuali() throws Exception{
		String value = driver.findElement(checkboxCondizioniContrattuali).getAttribute("value");
		if(value.compareTo("true")!=0)
		throw new Exception("Il campo 'Accettazione Condizioni Contrattuali' non risulta essere flaggato");
	}
	
	public void accettaComprensioneCondizioniContrattuali() throws Exception{
		util.objectManager(accettaComprensioneCondContrat, util.scrollAndClick);
	}
	
	public void verificaSeNonAbilitato(String nomeCampo) throws Exception{
		if(!util.verifyExistence(By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[@class='slds-card']//label[contains(text(),'"+nomeCampo+"')]/following::input[1]"), baseTimeoutInterval)) 
			throw new Exception("Impossibile trovare il campo di input "+nomeCampo);
		if(!util.verifyExistence(By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[@class='slds-card']//label[contains(text(),'"+nomeCampo+"')]/following::input[@disabled]"), baseTimeoutInterval)) 
			throw new Exception("Il campo  "+nomeCampo+" non risulta disabilitato come atteso");
	}
	
	
	public static String dirittoRipensamento = "Diritto di ripensamento: cosa avviene in caso di esecuzione anticipata.Il Codice del Consumo prevede 14 giorni di tempo per consentire al cliente di esercitare il diritto ripensamento dopo la conclusione di un contratto. Scegliendo la “Richiesta di esecuzione anticipata del contratto”, il Cliente richiede espressamente che le procedure per dar corso all’attivazione vengano avviate subito, ossia prima che sia decorso il termine per il ripensamento. Anche nel caso di “Richiesta di esecuzione anticipata del contratto”, per il Cliente sarà sempre possibile esercitare il diritto di ripensamento.Qualora sia possibile annullare la richiesta di attivazione, il Cliente continuerà ad essere fornito dal precedente Fornitore oppure saranno attivati i Servizi di ultima istanza gas o il Servizio di maggior tutela per il settore elettrico. Qualora, invece, non sia possibile interrompere la richiesta di attivazione verso il distributore, il Cliente continuerà ad essere fornito da Enel Energia e potrà individuare successivamente un altro Fornitore o procedere alla richiesta di chiusura del Punto di fornitura, facendone espressa richiesta.In questo caso il Cliente sarà tenuto al pagamento dei corrispettivi previsti dal contratto fino all’avvenuta cessazione della fornitura.";

}
