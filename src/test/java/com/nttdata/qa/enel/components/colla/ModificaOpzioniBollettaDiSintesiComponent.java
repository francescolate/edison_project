package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

import com.nttdata.qa.enel.components.colla.BaseComponent;

public class ModificaOpzioniBollettaDiSintesiComponent extends BaseComponent{
	public ModificaOpzioniBollettaDiSintesiComponent(WebDriver driver) {
		super(driver);
	}

	public By button_servizi = new ByChained(By.id("menuService") , By.tagName("a"));
	
	public By class_card = By.className("single-service");
	public By card_text = By.className("text-container");	
	public By opzioni_bolletta_card = new ByChained(class_card , card_text, By.xpath("//p[normalize-space(text())='Opzione Bolletta']"));
	
	public By button_modifica = By.id("bollettaWebLinkDisp");

	public By button_homepage = new ByChained(By.id("homeMenu") , By.tagName("a"));

	public By button_popup_esci = By.id("sendAbort");
	
	//public By text_path = By.className("breadcrumb");
	public By text_path = By.xpath("//ol[@class='breadcrumb']");
	public By text_titolo = By.tagName("h1");
	public By text_lead = By.className("lead");
	
	public By tipo_boll_title = new ByChained(By.className("table-responsive") , By.xpath("//th"));
	public By tipo_boll = By.xpath("//*[@id='tipoBoll']");
	public By con = By.className("col-xs-12");
	public By desc = new ByChained(con , By.xpath("//p[contains(text(),'Se vuoi cambiare la tipologia della bolletta, inizia cliccando su \"modifica\".')]"));
	
	public By step_text = By.className("text");
	public By step_num = By.className("num");
	
	public By step_text_0 = new ByChained(By.id("stepper_0") , step_text);
	public By step_num_0 = new ByChained(By.id("stepper_0") , step_num);
	public By step_text_1 = new ByChained(By.id("stepper_1") , step_text);
	public By step_num_1 = new ByChained(By.id("stepper_1") , step_num);
	public By step_text_2 = new ByChained(By.id("stepper_2") , step_text);
	public By step_num_2 = new ByChained(By.id("stepper_2") , step_num);
	public By step_text_3 = new ByChained(By.id("stepper_3") , step_text);
	public By step_num_3 = new ByChained(By.id("stepper_3") , step_num);
	
	public By step_num_1_parent = new ByChained(step_num_1 , By.xpath("/parent::li"));
	
	public By search_box = By.className("customselect");
	public By table_responsive = By.className("table-responsive");
	public By tabele_first_row = new ByChained(table_responsive, By.xpath("//tr"));
	public By table_first_row_3th_cell = new ByChained(tabele_first_row, By.xpath("./th[3]"));
	public By table_first_row_4th_cell = new ByChained(tabele_first_row, By.xpath("./th[4]"));
	public By table_first_row_5th_cell = new ByChained(tabele_first_row, By.xpath("./th[5]"));
	
	public By button = By.tagName("button");
	public By button_indietro = new ByChained(button , By.xpath("//span[text()='INDIETRO']"));
	public By button_prosegui = By.xpath("//span[text()='PROSEGUI']/..");
	
	public By popup_attection = By.id("modal-boxed-attenzione");
	public By popup_title = new ByChained(popup_attection , By.tagName("h4"));
	public By popup_desc = new ByChained(popup_attection , By.className("lead"));
	
	public By button_popup_torna = By.id("closeAbort");
	
	public By succes_icon = By.xpath("//span[@class='custom-ico custom-ico-inline success']");
	public By loading_icon = By.xpath("//span[@class='custom-ico custom-ico-inline loading']");
	public By table_checkbox = By.xpath("//input[not(@disabled) and contains(@id,'checkbox-') and not (contains(@id,'checkbox-m'))]/following-sibling::span");
	public By button_next = new ByChained(By.className("next") , By.xpath("//a[@title='Avanti']"));
	
	public By current_step = By.xpath("//li[contains(@class,'current')]");
	public By is_current_step_2 = new ByChained(current_step , By.xpath("/span[text()='##']".replaceFirst("##", "2")));
	
	
	public void clickAllForniture() throws Exception {
		baseTimeoutInterval = 15;	
		if(!getElementExistance(loading_icon)){
			baseTimeoutInterval = 5;
			if(!getElementExistance(succes_icon)){
				int x = countElement(table_checkbox);
				for (int i = 0; i < x; i++) {
					verifyComponentExistence(table_checkbox);
					clickComponent(table_checkbox);
					if(getElementExistance(succes_icon)){
						break;
					}
				}
			}
			return;
		}
		checkIfNextExist();
	}
			
	private void checkIfNextExist() throws Exception {
		if(getElementExistance(button_next)){
			clickComponent(button_next);	
			clickAllForniture();
		}
	}

	public By ByGetText(By obj) {
		By obj_text = new ByChained(obj , By.xpath("/text()"));
		return obj_text;
	}
	
	public By ByVerifyText(By obj , String text) {
		By obj_text = new ByChained(obj , By.xpath("/text()='##'".replace("##", text)));
		return obj_text;
	}
}
