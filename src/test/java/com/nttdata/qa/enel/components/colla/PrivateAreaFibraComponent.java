package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ByChained;

public class PrivateAreaFibraComponent extends BaseComponent{

	public By serviziMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Servizi']/parent::a");
	public By icon_consensi = By.xpath("//span[@class='dsc-icon-modifica-contratto']");
	public By id_frame = By.id("myIFrame");
	public By icon_informativa = By.xpath("//*[contains(text(), 'Via GIUSEPPE VERDI 5 - 00043 CIAMPINO RM')]/ancestor::div[@class='disp_card']//button[@class='pos_rad_ico epoint buttonNoBorder']"); //By.id("error");
	public By icon_informativa2 = By.xpath("//span[@class='info_user' and text()=\"Via PRIMO SANT'ALFONSO 22 - 70056 MOLFETTA BA\"]/ancestor::div[@class='disp_card']//button[@class='pos_rad_ico epoint buttonNoBorder']");
	public By icon_close_chat = By.xpath("//i[@class='icon icon-chat_closed']");
	public By div_chat_box = By.xpath("//div[@class='chat-box']");
	public By text_popup_title = By.xpath(element_by_text_and_parent_class.replace("#2", "modal__container").replace("#1", "Fibra di Melita già attiva"));
	public By text_popup_text = By.xpath(element_by_text_and_parent_class.replace("#2", "modal__container").replace("#1", "L’offerta FIBRA DI MELITA risulta già attiva su questa fornitura."));
	
	public By text_popup_titledescription = By.xpath("//div[@id='modalid' and @aria-modal='true']//h4[@id='dialog_desc' and text()='Offerta non attivabile']/ancestor::div[1]/div[text()='Non è possibile selezionare questa fornitura perché sono presenti altre operazioni in corso.']/ancestor::div[1]/div/button[@class='close buttonClose' and @aria-label='Chiudi la modale']");
	public By text_popup_titledescription2=By.xpath("//div[@id='modalid' and @aria-modal='true']//h4[@id='dialog_desc' and text()='FIBRA DI MELITA già attiva']/ancestor::div[1]/div[text()='L’offerta FIBRA DI MELITA risulta già attiva su questa fornitura. ']/ancestor::div[1]/div/button[@class='close buttonClose' and @aria-label='Chiudi la modale']");

	public By button_save_change = By.xpath(element_by_text_and_parent_class.replace("#2", "button_first").replace("#1", "Salva Modifiche"));
	public By button_back = By.xpath(element_by_text_and_parent_class.replace("#2", "button_second").replace("#1", "Indietro"));
	public By button_end = By.xpath(element_by_text_and_parent_class.replace("#2", "button_first").replace("#1", "Fine"));
	public By button_change_consensus = By.xpath(link_by_inner_and_href.replace("#2", "it/area-clienti/residenziale/consensi").replace("#1", "MODIFICA CONSENSI"));
	public By input_mobile_marketing = By.xpath("//input[@class='input-radio' and @id='consenso-enel-si--2']");
	public By input_email_marketing = By.xpath("//input[@class='input-radio' and @id='consenso-enel-si--1']");
	public By input_mobile_marketing_consensus_checked = By.xpath("//input[@class='input-radio' and @id='consenso-enel-si--2' and @checked='checked']");
	public By input_email_marketing_consensus_checked = By.xpath("//input[@class='input-radio' and @id='consenso-enel-si--1' and @checked='checked']");

	//public By radiobutton_mobile_marketing_yes = By.xpath("//label[@class='label-radio' and @for='consenso-enel-si--2']");
	public By radiobutton_mobile_marketing_yes = By.xpath("//input[@name='consenso-enel-phone' or @name='consenso-enel-mobile']//ancestor::div[1]/label[text()='SI' and @class='label-radio']");
	public By radiobutton_email_marketing_yes = By.xpath("//label[@class='label-radio' and @for='consenso-enel-si--1']");
	public By button_show_offer_fibra = By.xpath("//*[@id='fibra-card']//*[contains(text(), 'VISUALIZZA')]");
	public By button_continue_fibra = By.xpath("//*[contains(text(), 'Verifica copertura')]");

	public By button_exit = By.xpath(element_by_text_and_parent_class.replace("#2", "flex_button pos_button_page_one").replace("#1", "ESCI"));
	public String url_residential_client = "it/area-clienti/residenziale";

	public By button_continue_disabled = By.xpath("//button[@class='btnDisable' and normalize-space(text()='CONTINUA')]");

	public By card_fibra_hided = By.xpath(element_by_inner_and_parent_class.replace("#2", "nuova-fornitura-component parbase hide").replace("#1", "FIBRA DI MELITA"));
	
	//
	public By button_close = By.xpath("//button[@class='close buttonClose']");
	public By text_popup_title_2 = By.xpath(element_by_text_and_parent_class.replace("#2", "modal__container").replace("#1", "Offerta non attivabile"));
	public By text_popup_text_2 = By.xpath(element_by_text_and_parent_class.replace("#2", "modal__container").replace("#1", "Non è possibile selezionare questa fornitura perché sono presenti altre operazioni in corso."));
	
	public By card_fibra_active = By.xpath("//div[@class='card forniture regolare' and contains(.,'Fibra') and contains(., 'Attiva')]");
	public By button_show_bill = By.xpath(element_by_text_and_parent_class.replace("#1", "Visualizza le bollette").replace("#2", "button-container"));
	public By button_show_details = By.xpath(element_by_text_and_parent_class.replace("#1", "Dettaglio Offerta").replace("#2", "button-container"));
	public By text_address_card_fibra = new ByChained(card_fibra_active, By.xpath("//*[contains(text(), 'Via')]"));
	public By text_status_card_fibra = new ByChained(card_fibra_active, By.xpath("//*[text()='Attiva']"));
	public By text_number_card_fibra = new ByChained(card_fibra_active, By.xpath("//*[text()='Numero Cliente']/following-sibling::*"));
	public By button_show_details_card_fibra = By.xpath("//div[@class='card forniture regolare' and contains(.,'Fibra') and contains(., 'Attiva')]//a[text()='Dettaglio Offerta']");
	public By main = By.id("mainContentWrapper");
	public By main_type_type = new ByChained(main , By.id("fornitura01"));
	public By main_supply_desc = new ByChained(main , By.xpath(".//dl[@aria-label='descrizione rapida']"));
	public By supply_desc_container = new ByChained(main , By.xpath(".//div[@class='supply details-container']"));
	public By supply_desc_status = new ByChained(supply_desc_container , By.xpath(".//dl/span[1]/dd/span"));
	public By supply_desc_date = new ByChained(supply_desc_container , By.xpath(".//dl/span[2]/dd/span"));
	public By supply_desc_payment_type = new ByChained(supply_desc_container , By.xpath(".//dl/span[3]/dd/span"));
	public By supply_desc_reception_mode = By.xpath("//span[@id='accessibility']/dd/span");
	public By supply_desc_info_icon = new ByChained(supply_desc_container , By.xpath("./dl/span[3]/dd/a"));
	public By button_more_information = By.id("overlayYesButton");
	public By button_esci = By.xpath((element_by_text_and_parent_id.replace("#1", "ESCI").replace("#2", "landing-dispositiva")));
	public By supply_desc_info_icon_2 = By.xpath("//span[@id='accessibility']/dd/a");
	public By button_show_bill_card_fibra = new ByChained(card_fibra_active, By.xpath(".//*[contains(@class,'button-container')]//*[contains(text(),\"Visualizza le bollette\")]"));
	
	public By popup_fibra_card = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']");
	public By popup_title = By.xpath(".//h3");
	public By popup_x = By.xpath(".//span[@class='dsc-icon-close-rounded']");
	public By popup_desc = By.xpath(".//div[@class='description-fibra']");
	public By button_decline = By.xpath(element_by_text_and_parent_class.replace("#1", "Annulla").replace("#2", "button_second"));
	public By title_popup_fibra_card = new ByChained(popup_fibra_card, popup_title);	
	public By desc_popup_fibra_card = new ByChained(popup_fibra_card, popup_desc);
	public By button_decline_popup_fibra_card = new ByChained(popup_fibra_card, By.xpath(".//*[contains(@class,'button_second')]//*[contains(text(),\"ANNULLA\")]"));
	public By button_next_popup_fibra_card = new ByChained(popup_fibra_card, By.xpath(".//*[contains(@class,'button_first')]//*[contains(text(),\"PROSEGUI\")]"));
	
	public By closePopup=By.xpath("//div[@id='modalid' and @aria-modal='true']//h4[@id='dialog_desc' and text()='Verifica copertura non disponibile']/ancestor::div[1]/div/button[@class='close buttonClose' and @aria-label='Chiudi la modale']");
	public By closePopup2=By.xpath("//div[@id='modalid' and @aria-modal='true']//h4[@id='dialog_desc' and text()='Offerta non attivabile']/ancestor::div[1]/div/button[@class='close buttonClose' and @aria-label='Chiudi la modale']");
	public PrivateAreaFibraComponent(WebDriver driver) {
		super(driver);
	}

	public void clickElementByIndex(By visibleObject, int i) throws Exception {
		List<WebElement> listElement = util.waitAndGetElements(visibleObject);
		
		if(listElement.size() >= i)
			util.scrollAndClick.accept(listElement.get(i - 1));
		else
			throw new Exception("The element located by " + visibleObject + " with index : " + i +  " does not exist.");
	}

	public WebElement getElementInParentElement(By parentElement, By element) throws Exception {
		return util.waitAndGetElement(util.waitAndGetElement(parentElement) , element);
	}

	public void clickComponent(WebElement visibleObject) {
		util.scrollAndClick.accept(visibleObject);
	}
	
	public void jsClickComponent(By by) throws Exception{
		util.jsClickElement(by);
	}
	
	public void clickIfExist(By oggettocliccabile) throws Exception {
		//if(util.verifyExistence(oggettocliccabile, 150)){
			clickcomponentjavascript(oggettocliccabile);
			//return true;
		//};
		//return false;
		
	}
	
	public void clickcomponentjavascript(By object) throws Exception {
		WebElement button=util.waitAndGetElement(object);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
	}
	
	public void setframe(By frame) {
		WebElement frameToSw = driver.findElement(frame);
		driver.switchTo().frame(frameToSw);
	}

//	public void clickComponentWithJse(By oggettocliccabile) throws Exception {
//			util.objectManager(oggettocliccabile, util.scrollElement);
//			util.objectManager(oggettocliccabile, util.clickwithjse);		
//	}
}
