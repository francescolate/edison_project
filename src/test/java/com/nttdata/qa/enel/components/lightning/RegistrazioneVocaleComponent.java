package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class RegistrazioneVocaleComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public RegistrazioneVocaleComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
	public final By RegistrazioneVocaleButton = By.xpath("//lightning-button[@data-id='StartRegistration']/button[@name='StartRegistration'][1]");
	public final By RegistrazioneVocaleFibraButton = By.xpath("//lightning-button[@data-id='StartFiberRegistration']/button[@name='StartFiberRegistration'][1]");
	public final By AvviaRegistrazioneVocaleButton = By.xpath("//div[@id='modalRegistrazione Vocale']//button[text()='Avvia registrazione']");
	public final By TerminaRegistrazioneVocaleButton = By.xpath("//div[@id='modalRegistrazione Vocale']//button[text()='Termina registrazione']");
	public final By ForzaVocalOrderButton = By.xpath("//div[@id='modalRegistrazione Vocale']//button[text()='Forza vocal order']");
//	public final By PrecheckButton = By.xpath("//div[contains(@id,'principalForm:PrecheckId')]//button[@alt='Precheck']");
	public final By PrecheckButton = By.xpath("//button[@name='Precheck']");
	
	public final By ConfermaButton = By.xpath("//button[@id='confirmButtonHeader']");
	
	public void selezionaRegistrazioneVocale() throws Exception{
		press(RegistrazioneVocaleButton);
	}

	public void selezionaRegistrazioneVocaleFibra() throws Exception{
		press(RegistrazioneVocaleFibraButton);
	}

	public void selezionaAvviaRegistrazione() throws Exception{
		spinner.checkSpinners();
		press(AvviaRegistrazioneVocaleButton);
	}
	
	public void selezionaTerminaRegistrazione() throws Exception{
		press(TerminaRegistrazioneVocaleButton);
	}
	 
	public void Forzavocalorder() throws Exception{
		press(ForzaVocalOrderButton);
	}
	
	public void Precheck() throws Exception{
		driver.switchTo().defaultContent();
//		spinner.checkSpinners();
		

//			List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//			WebElement element = null;
//			for (WebElement webElement : els) {
//				driver.switchTo().frame(webElement);
//				try {
//				 element = util.waitAndGetElement(PrecheckButton);
//				 break;
//				}catch(Exception e) {
//					driver.switchTo().defaultContent();
//				}
//			}
//			
			util.getFrameActive();
		
			press(PrecheckButton);
					
			
			// Verificare se le istruzioni sotto servono ancora ????
			WebElement table = driver.findElement(By.xpath("//table[contains(@id, 'principalForm:insertedSuppliesTable')]"));
			String colName = "Esito Precheck";
			
			int count = util.getTableRowCount(table);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			for (int i = 0; i<count;i++) {
				WebElement temp = driver.findElement(By.xpath("(//table[contains(@id, 'principalForm:insertedSuppliesTable')]//input[@type='radio'])"
						+(i>0?"["+(i+1)+"]":"")
						));
				
				executor.executeScript("arguments[0].click();", temp);
				spinner.checkSpinners();
				TimeUnit.SECONDS.sleep(5);
				 WebElement element = util.waitAndGetElement(PrecheckButton);
				executor.executeScript("arguments[0].click();", element);
				TimeUnit.SECONDS.sleep(5);
				spinner.checkSpinners();
				
				TimeUnit.SECONDS.sleep(5);
			}
			
			
			for (int i = 1; i<=count;i++) {
				
				table = driver.findElement(By.xpath("//table[contains(@id, 'principalForm:insertedSuppliesTable')]"));
				String out = util.getTableCellDataWithoutDiv(table, i, colName, "span");
				if(!(out.equals("OK")||out.equals("NA"))) {
					throw new Exception("Esito Prececk Atteso:OK/NA riscontrato: "+out+" alla riga: "+i);
				}
			}
			
		
	}
	
	public void Conferma() throws Exception{
		TimeUnit.SECONDS.sleep(5);

		press(ConfermaButton);
		TimeUnit.SECONDS.sleep(5);
		

	}
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	
}
