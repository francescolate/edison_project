package com.nttdata.qa.enel.components.colla;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LuceEGasComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[@class='hero_title text--page-heading']");
	public By qst1 = By.xpath("//div[@class='search-block filter-container luce-step-3 module--initialized']/div[@class='search-section']/div[@class='select-menu-section'][1]/label");
	public By ans1 = By.xpath("//span[@id='productSelectBoxIt']/span[@id='productSelectBoxItText']");
	public By qst2 = By.xpath("//div[@class='search-section']/div[@class='select-menu-section select-secondary']/label");
	public By ans2 = By.xpath("//span[@id='placeSelectBoxIt']/span[@id='placeSelectBoxItText']");
	public By qst3 = By.xpath("//div[@class='search-section']/div[@class='select-menu-section'][2]/label");
	public By ans3 = By.xpath("//span[@id='myselectSelectBoxIt']/span[@id='myselectSelectBoxItText']");
	public By loginIcon = By.xpath("//span[@class='icon-user']");
	public String elementpath = "HOME / LUCE E GAS";
	public By luceegas = By.xpath("//nav[@class='image-hero_breadcrumbs module--initialized']");
	public String pageUrl = "https://www-coll1.enel.it/it/luce-e-gas?zoneid=search-quick_link";
	public By searchIcon = By.xpath("//span[@class='icon-search-small']");
	public By lesoluzioni = By.xpath("//a[text()='Le soluzioni per la tua azienda ']");
	public By luceegasInsiemeText = By.xpath("//div[@id='promo-offert_results']//span[text()='Luce e gas insieme per la tua casa']");
	public By trovaLaSoluzioneText = By.xpath("//span[contains(text(),'Trova la soluzione giusta')]");
	public By trovaLaSoluzioneSubText= By.xpath("//span[contains(text(),'Trova la soluzione giusta')] //following::p[1]");
	public By scopriilNegozioLink = By.xpath("//span[contains(text(),'Scopri il Negozio Enel più vicino a te!')]/parent::a");
	public By voltura = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Voltura']");
	public By subentro = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Subentro']");
	public By cambioFornitore = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Cambio Fornitore']");
	public By luceEGasInsiemeTitle = By.xpath("//div[@id='promo-offert_results']//span[contains(text(),'Luce e gas insieme per la tua casa')]");
	public By luceEGasInsiemeText = By.xpath("//h2[text()='Luce e gas insieme, per la tua casa']");
	public By luceEGasInsiemeSubText = By.xpath("//div[@id='promo-offert_results']//p");
	public By vediTutteLuce = By.xpath("//span[text()=' Vedi tutte luce']");
	public By vediTutteGas = By.xpath("//span[text()=' Vedi Tutte gas']");
	public By luce = By.xpath("//span[@id='productSelectBoxItText' and text()='Luce']");
	public By casa = By.xpath("//span[@id='placeSelectBoxItText' and text()='Casa']");
	public By visualizzatutte = By.xpath("//span[@id='myselectSelectBoxItText' and text()='Visualizza tutte']");
	public By attivaOraELightBioraria = By.xpath("//a[contains(@aria-label,'Attiva ora E-Light Bioraria') and contains(text(),'Attiva ora')]");
	public By attivaOraELightBiorariaELightaGas = By.xpath("//a[contains(@aria-label,'Attiva ora E-Light Bioraria') and contains(@aria-label,'E-light Gas') and contains(text(),'Attiva ora')]");
	public By biorariaWeb = By.xpath("//h1[contains(text(),'E-Light Bioraria WEB')]");
	public By gasWeb = By.xpath("//h1[contains(text(),' Gas WEB')]");
	public By gas = By.xpath("//span[@id='productSelectBoxItText' and text()='Gas']");
	public By title = By.xpath("//h2[@class='title']");
	public By pageText = By.xpath("//p[contains(text(),'La forza della')]");
	
	//Offerte
	public By attivaOra = By.xpath("//*[@id='priceHeaderID']");
	public By eLightBioraria_eLightGas = By.xpath("//a[@href='/it/luce-e-gas/luce-e-gas/offerte/elight-gas-elight-bioraria?formOCR=detail']");
	
	public LuceEGasComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
 
 public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
 public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
    
    public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
    
    public void checkQuestionAndFilter(String property1,String property2,String property3) throws Exception{
    	
        WebElement obj = util.waitAndGetElement(qst1);

    	String result = "NO";
    	
    	Map map = new HashMap<>();
    	/*map.put("Che contratto vuoi attivare?", "Luce");
    	map.put("Dove?", "Casa");
    	map.put("Per quale necessità?", "Visualizza tutte");*/
    	map.put(driver.findElement(qst1).getText(), driver.findElement(ans1).getText());
    	map.put(driver.findElement(qst2).getText(), driver.findElement(ans2).getText());
    	map.put(driver.findElement(qst3).getText(), driver.findElement(ans3).getText());
    	
    	String ans1=(String) map.get(driver.findElement(qst1).getText());
    	String ans2 = (String) map.get(driver.findElement(qst2).getText());
    	String ans3 = (String) map.get(driver.findElement(qst3).getText());
    	
    	if(property1.equals(ans1) && property2.equals(ans2) && property3.equals(ans3)){
    		
    		result = "YES";
    	}
    	if(result.equals("NO")){
    		throw new Exception("Question and Filter is not matching");
    	}
    	
    	
    }

    public void checkMenuPath(By checkObject,String path) throws Exception {
        WebElement obj = util.waitAndGetElement(checkObject);
        String pathDescription=obj.getText(); 
        pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");    

     //   System.out.println("URl "+driver.getCurrentUrl()+"Path"+obj.getAttribute("Path"));
        if (!pathDescription.equalsIgnoreCase(path))
            throw new Exception("object path with xpath " + luceegas + " is not exist.");
    }
    
    public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}

    public static final String PageTitle = "Trova la soluzione giusta per te"; 
    public static final String LUCEEGASINSIEMEText="Luce e gas insieme per la tua casa";    
    public static final String TrovaLaSoluzioneText="Trova la soluzione giusta giusta per la tua casa con Enel";
    public static final String TrovaLaSoluzioneSubText="Se non hai trovato quello che cercavi puoi controllare le offerte in promozione per la fornitura luce, o trovare il Negozio Enel più vicino a te e trovare la soluzione più conveniente per te.";
    public static final String ScopriilNegozioLinkText="Scopri il Negozio Enel più vicino a te!";
    public static final String LUCEEGASINSIEMESubText="La forza della luce e la sicurezza del gas di Enel Energia viaggiano insieme. Per te e la tua casa abbiamo pensato a comodi pacchetti completi con forniture luce e gas abbinate. Scegli tra le numerose soluzioni quella più giusta per te, scegli Enel Energia!";    
    public static final String RegistrationPageTitle = "E-Light Bioraria WEB E-Light Gas WEB";
    public static final String Title = "Luce e gas insieme, per la tua casa";
}
