package com.nttdata.qa.enel.components.colla;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class OreFreeComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	//public By offertaOreFree=By.xpath("//h3[text()='ORE FREE NOW']");
	public By offertaOreFree=By.xpath("//h3[text()='ORE FREE']");
//	public By offertaOreFree=By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/scegli-tu-orefree']//h3[@class='title' and text()='Ore Free']");
	//public By verificadisponibilitaButton=By.xpath("//li[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-orefree']//a[@href='/it/adesione' and normalize-space(text())='Verifica e aderisci']");
	public By verificadisponibilitaButton=By.xpath("//*[contains(text(),'Verifica') and contains(@aria-label,'ORE FREE')]");
	public By inserisciNumeroPod=By.xpath("//div[@id='code-insert-modal']//h6[@class='titlePopupDueg' and text()='Inserisci il numero del tuo POD*']");
	public By testopopup=By.xpath("//div[@id='code-insert-modal']//p[text()=\"Verifica che la tua fornitura abbia i requisiti per poter proseguire con l'adesione.\"]");
	public By campoCodicePod=By.xpath("//div[@id='code-insert-modal']//div//input[@placeholder='Codice POD']");
	public By confermaButton=By.xpath("//div[@id='code-insert-modal']//div//button[@id='scegli-tu-orefree' and text()='Conferma']");
	public By linkpod=By.xpath("//div[@id='code-insert-modal']//div//p[text()='*']//a[@href='https://www-coll1.enel.it/it/supporto/faq/subentro' and text()=\"Che cos'è il POD e dove lo trovo?\"]");
	public String link_pod="//div[@id='code-insert-modal']//div//p[text()='*']//a[@href='#it/supporto/faq/subentro' and text()=\"Che cos'è il POD e dove lo trovo?\"]";
	
	public By labelcampoObbligatorio=By.xpath("//div[@id='code-insert-modal']//div//span[@id='error-label' and @data-label-first='Questo campo è obbligatorio' and text()='Questo campo è obbligatorio']");
	public By labelformatononvalido=By.xpath("//div[@id='code-insert-modal']//div//span[@id='error-label' and text()='Formato inserito non valido']");
	public By titolopopup2=By.xpath("//div[@id='modale_errore_2g']//span[@class='titlePopupDueg' and text()='Ci dispiace, l’offerta non è attivabile']");
	public By testopopup2=By.xpath("//div[@id='modale_errore_2g']//p[normalize-space(text())='La tua fornitura non presenta i requisiti per poter aderire a questa offerta. Niente paura scopri le altre offerte disponibili per te.']");
	//public By titolopaginaOreFree=By.xpath("//h1[@class='image-hero_title text--page-heading' and text()='ORE FREE NOW']");
	public By titolopaginaOreFree=By.xpath("//h1[@class='image-hero_title text--page-heading ' and text()='ORE FREE']");
	//public By verificaaderisciVerde=By.xpath("//a[@href='/it/adesione?zoneid=ore_free_now-hero' and normalize-space(text())='Verifica e aderisci']");
	public By verificaaderisciVerde=By.xpath("//a[@role='button' and contains(text(),'Verifica e aderisci')]");
	public By dettaglioffertaButton=By.xpath("//li[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-orefree']//button[normalize-space(text())='Dettaglio Offerta']");
//	public By verificaeaderisciButton=By.xpath("//a[contains(text(),'Verifica e aderisci')]");
//	public By testopopuperror=By.xpath("//div[@id='modale_errore_2g']//p[text()=\"La tua fornitura non presenta i requisiti per poter aderire a questa offerta. Niente paura scopri le altre offerte disponibili per te.\"]");
	public By titoloPage=By.xpath("//h1[@class='image-hero_title text--page-heading' and text()='Scegli Tu Ore Free']");
	public String linkLUCE_E_GAS_FILTRO="it/luce-e-gas/luce/casa?nec=non_specificato";
	
	
	public OreFreeComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
	}
		
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void enterPodValue(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}

	public String generateRandomPodNumberEnergia() throws Exception {
		return util.generateRandomPodNumberEnergia();
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner();
	}
	
	public void replaceLinkCollaInXpathAndCheckExistObject(String link) throws Exception {
		util.objectManager(By.xpath(link_pod.replace("#", link)), util.scrollToVisibility, true);
	}
	
	public void replaceLinkCollaInXpathEClickObject(String link) throws Exception {
		util.objectManager(By.xpath(link_pod.replace("#", link)), util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(By.xpath(link_pod.replace("#", link)), util.click);
	}
}
