package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CookieBannerComponent extends BaseComponent {
	public By banner = By.id("cookie-policy");
	public By title = By.id("cookiePolicyTitle");
	public By description = By.cssSelector("#cookie-policy > div.cookie-para > p");
	public By policyLink = By.cssSelector("#cookie-policy > div.cookie-para > p > a");
	public By policyLinkNew = By.xpath("//a[text()='Cookie Policy qui.']");
	public By acceptBtn = By.id("accept-btn");
	public By closeBtn = By.cssSelector("#close-btn > img");

	public CookieBannerComponent(WebDriver driver) {
		super(driver);
	}
	public void hanldeFullscreenMessage(By by) throws Exception{
		if(util.verifyExistence(by, 60)){
			util.objectManager(by, util.scrollToVisibility, false);
			util.objectManager(by, util.scrollAndClick);
		}
	}
	
	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
//		if (!util.exists(oggettoEsistente, 15))
//			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		if (!util.verifyExistence(oggettoEsistente, 30)) {
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
		}
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		*/
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.click);
		
	}
	
}
