package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SbloccoTabComponent {
    WebDriver driver;
    public SeleniumUtilities util;


    final By numeroTab = By.xpath("//div[contains(@class,'slds-context-bar__secondary navCenter')]"
            + "/div[contains(@class,'tabContainer')]"
            + "/div/ul[@class='tabBarItems slds-grid']"
            + "/li[contains(@class,'tabItem')]"
            + "| //div[@class='slds-context-bar__secondary navCenter']//ul[@class='pinnedItems slds-grid']/li[contains(@class,'tabItem')]");


    public By sbloccoTab = By.xpath("//span[text()='Sblocco Tab']");
    public By sbloccoSubTab = By.xpath("//button[text()='Sblocca tutti i subtabs']");
    public By sbloccofocus = By.xpath("//button[text()='Sblocca il tab in focus']");

    public By okButton = By.xpath("//span[@class=' label bBody' and text() = 'OK']");
    public By body = By.xpath("//body");
    public By riduciIcona = By.xpath("//h2[@title='Sblocco Tab']");
    public By selectedTab = By.xpath("//div[contains(@class,'selectedListItem')]/a");
    
    
    public By confirmClosingTabs = By.xpath("//button[text()='Chiudi tutto']");
    public By tabBar = By.xpath("//ul[@class='tabBarItems slds-grid']/li");
    public By tabFocus = By.xpath("//ul[@class='tabBarItems slds-grid']/li[@role='presentation'][1]");
    
    public By toastMessage = By.xpath("//*[contains(@class, 'toastMessage slds-text-heading')]");
    public By interazioniTab = By.xpath("//a[@title='Interazioni']");
    public By popupAttenzione = By.xpath("//button [@id='commButtonIdA']");  //-- 06/11/2020

    public SbloccoTabComponent(WebDriver driver) {
        this.driver = driver;
        this.util = new SeleniumUtilities(driver);
    }

    public void chiudiVecchieInterazioni() throws Exception {
        WebDriverWait wait = new WebDriverWait(this.driver, 5);
        try {
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccoTab).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccoSubTab).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(body).sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccoSubTab).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(body).sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccofocus).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(body).sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            TimeUnit.SECONDS.sleep(4);
            driver.findElement(okButton).click();
            TimeUnit.SECONDS.sleep(4);
        } catch (TimeoutException | NoSuchElementException e) {
            //System.out.println("pop-up not found, not blocking");
        } catch (InterruptedException e) {

        } finally {
            driver.findElement(riduciIcona).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(body).click();
        }
    }

    public void chiudiVecchieInterazioni2() throws Exception {
        TimeUnit.SECONDS.sleep(20);
        //List <WebElement> tabs = driver.findElements(numeroTab);
        List<WebElement> tabs = util.waitAndGetElements(numeroTab, 30, 2);
        util.waitAndGetElement(sbloccoTab, 30, 2).click();
        //System.out.println("number of tabs = " + tabs.size());
        try {
            for (WebElement tab : tabs) {
                tab.click();
                TimeUnit.SECONDS.sleep(10);
                //System.out.println("Trying to close " + tab );
                util.waitAndGetElement(sbloccoSubTab, 30, 2).click();
                util.waitAndGetElement(sbloccofocus, 30, 2).click();
                TimeUnit.SECONDS.sleep(10);
                util.waitAndGetElement(body, 30, 2)
                        .sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
                //System.out.println(tab + " closed");
            }
        } catch (WebDriverException e) {
            driver.findElement(okButton).click();
            //System.out.println("pop-up not found, not blocking");
        } finally {
            //wait.until(ExpectedConditions.invisibilityOfElementLocated(okButton));
            util.waitAndGetElement(sbloccoTab, 30, 2).click();
        }
    }


    private List<WebElement> getTabs() {
        try {
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            List<WebElement> l = util.waitAndGetElements(numeroTab, 2, 1);
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            return l;
        } catch (Exception e) {
            return null;
        }
    }


    public void chiudiVecchieInterazioni3() throws Exception {

        String modalChecklist = "//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma'] | //section[@data-id='checklistModal']//button[text()='Conferma'] | //div[@data-id='checklistModal']//button[text()='Conferma']";
        TimeUnit.SECONDS.sleep(7); //era 60
        List<WebElement> tabs = getTabs();

        try {
            util.waitAndGetElement(sbloccoTab, 10, 1).click();//timout 50
        } catch (Exception e) {
            String el = modalChecklist;
            if (util.verifyExistence(By.xpath(el), 2)) {
                util.waitAndGetElement(By.xpath(el)).click();
                util.waitAndGetElement(sbloccoTab, 60, 2).click();
            }
        }
        while (tabs != null && tabs.size() > 0) {
            try {
                tabs.get(0).click();
            } catch (Exception e) {
                String el = modalChecklist;
                if (util.verifyExistence(By.xpath(el), 2)) {
                    TimeUnit.SECONDS.sleep(10);
                    util.waitAndGetElement(By.xpath(el)).click();
                    tabs.get(0).click();
                }
            }

            TimeUnit.SECONDS.sleep(7);

            util.waitAndGetElement(sbloccofocus, 3, 1).click();//timout 300
            util.waitAndGetElement(body, 5, 1)//timout a 180
                    .sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));

            try {
                if (util.exists(By.xpath(modalChecklist), 5))
                    util.objectManager(By.xpath(modalChecklist), util.scrollAndClick);
            } catch (Exception e) {
            }
            util.waitAndGetElement(sbloccoSubTab, 60, 1).click();
            util.waitAndGetElement(body, 30, 1)
                    .sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            if (util.exists(By.xpath(modalChecklist), 5))
                util.objectManager(By.xpath(modalChecklist), util.scrollAndClick);

            util.waitAndGetElement(body, 30, 1)
                    .sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            if (util.exists(By.xpath(modalChecklist), 2))
                util.objectManager(By.xpath(modalChecklist), util.scrollAndClick);

            util.waitAndGetElement(body, 30, 1)
                    .sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            TimeUnit.SECONDS.sleep(3);
            tabs = getTabs();
        }
        util.waitAndGetElement(sbloccoTab, 30, 1).click();
    }

    public void chiudiVecchieInterazioni4l() throws Exception {
        By allTabInHeader = By.xpath("//header//a[@role='tab']");
        By xButtonInFocusTab = By.xpath("//header//a[@role='tab']/parent::*//*[text()='Chiudi scheda']/ancestor::*[@tabindex != '-1' or @hidden != 'true' or @disabled != 'true']");
        //TODO using util.click not waitAndGetElement.ecc...
        if (util.verifyExistence(allTabInHeader, 3)) {
            if (util.verifyExistence(sbloccoTab, 3)) {
                util.waitAndGetElement(sbloccoTab, 30, 2).click();
                if (util.verifyExistence(sbloccofocus, 3)) {
                    util.waitAndGetElement(sbloccofocus, 30, 2).click();
                    if (util.verifyExistence(sbloccoSubTab, 3)) {
                        util.waitAndGetElement(sbloccoSubTab, 30, 1).click();
                    } else {
                        throw new Exception("The element located by " + sbloccoSubTab + " does not exist or is visible.");
                    }
                } else {
                    throw new Exception("The element located by " + sbloccofocus + " does not exist or is visible.");
                }
            } else {
                throw new Exception("The element located by " + sbloccoTab + " does not exist or is visible.");
            }
            while (util.verifyExistence(allTabInHeader, 3)) {
                util.waitAndGetElement(allTabInHeader, 30, 2).click();
                if (util.verifyExistence(xButtonInFocusTab, 3)) {
                    util.waitAndGetElement(xButtonInFocusTab, 30, 1).click();
                } else {
                    break;
                }
            }
        }
    }

    public void closeAllTabByShortcut() throws Exception{
    	
    	String selectAll = Keys.chord(Keys.SHIFT, "W");
    	
    	
    	 if(util.verifyVisibility(this.popupAttenzione, 5))
             util.objectManager(this.popupAttenzione, util.scrollAndClick);
    	 //Chiude eventuali pop-up visualizzate all'ingresso su SFDC
    	 String modalChecklist = "//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma'] | //section[@data-id='checklistModal']//button[text()='Conferma'] | //div[@data-id='checklistModal']//button[text()='Conferma']";
         if (util.verifyExistence(By.xpath(modalChecklist), 5)) {
             util.waitAndGetElement(By.xpath(modalChecklist)).click();
             TimeUnit.SECONDS.sleep(2);
         }
    	 
    	//Vengono sbloccati tutti i tab esclusi dallo shortcut
    	if(util.exists(this.tabBar, 30)){
            driver.findElement(sbloccoTab).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccoSubTab).click();
            TimeUnit.SECONDS.sleep(2);
    		driver.findElement(riduciIcona).click();
    		TimeUnit.SECONDS.sleep(3);
    		driver.findElement(By.tagName("html")).sendKeys(selectAll);
    		TimeUnit.SECONDS.sleep(5);
    		if(!this.clickOnInterazioni()){
	    		util.waitAndGetElement(this.confirmClosingTabs, false);
	    		util.jsClickElement(this.confirmClosingTabs);
	    		clickOnInterazioni();
    		}
    	}
    	
    	//Viene sbloccato eventuali tab in focus, se non è stato chiuso precedentemente
    	 List<WebElement> tabs = getTabs();
    	 while (tabs != null && tabs.size() > 0) {
            driver.findElement(tabFocus).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccoTab).click();
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(sbloccofocus).click();
            TimeUnit.SECONDS.sleep(2);
    		driver.findElement(riduciIcona).click();
    		TimeUnit.SECONDS.sleep(3);
            util.waitAndGetElement(body, 30, 1).sendKeys(Keys.chord(Keys.LEFT_SHIFT, "x"));
            TimeUnit.SECONDS.sleep(2);
            clickOnInterazioni();
            TimeUnit.SECONDS.sleep(2);
            tabs = getTabs();
    		}
    	
    }
    
    private boolean clickOnInterazioni() throws Exception{
    	if(util.verifyExistence(this.toastMessage, 5)){
//    		util.jsClickElement(this.interazioniTab);
    		util.jsClickElement(this.selectedTab);
    		return true;
    	}else
    		return false;
    }
    
}
