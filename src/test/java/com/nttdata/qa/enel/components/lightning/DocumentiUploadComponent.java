package com.nttdata.qa.enel.components.lightning;


import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import com.nttdata.qa.enel.util.Utility;

public class DocumentiUploadComponent {

	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinner;
//	final private By tabCorrelato = By.xpath("//a[@data-label='Correlato']");
	final public By tabCorrelato = By.xpath("//section[@aria-expanded='true']//a[@data-label='Correlato']");
	final private By tabCorrelato2 = By.xpath("(//section[@aria-expanded='true']//a[@data-label='Correlato'])[2]");
	final public By tabDettagli = By.xpath("//a[@data-label='Dettagli' and @id='detailTab__item']");
	final private By chiudi = By.xpath("//button/span[text()='Chiudi']");
	final private By fileUpload = By.xpath("//input[@name='fileUploader']");
	final public By fileUpload2 = By.xpath("//div[@class='upload-block']//input[@id='upload-doc']");
	final public By fileUpload3 = By.xpath("//span[text()='Inserisci qui il tuo documento di riconoscimento']/ancestor::div[@class='upload-block']//label[@for='upload-doc2']//input[@id='upload-doc2']");
	final public By fileUpload4 = By.xpath("//span//div[text()='Istanza 326/DL Casa/eventuale documentazione aggiuntiva']/ancestor::div[@class='module-tp4']//label[@for='upload-dl']//input[@id='upload-dl']");
	final private By validaDocButton = By.xpath("//div[@id='pageHeader4Acc']"
			+ "/div[1]//button[text()='Valida Documentazione']");
	final private By fileUpload5 = By.xpath("(//input[@name='fileUploader'])[2]");
	//-- //span[text()='DMS-LocalUpload']/../../th/div/a -- new value
	//-- //tbody//a[contains(text(),'ACT-')] --OLD value
	//Ros 01.04 sostituito xpath in quanto per alcuni processi l'activity da cliccare non Ã¨ il primo della tabella
//	final private By activity = By.xpath("//span[text()='DMS-LocalUpload']/../../th/div/a");
//	final private By activity = By.xpath("//lightning-formatted-text[text()='DMS-LocalUpload']/ancestor::table//tr/th//div/a");
//	final private By activity = By.xpath("//span[text()='DMS-LocalUpload']/../../th/div/a");
	final public By activity = By.xpath("//*[text()='DMS-LocalUpload']/ancestor::tr//a");
	final private By offerte = By.xpath("//a[not(contains(@href,'javascript'))]/span[text()='Offerte' or text()='Offerta' or text()='AttivitÃ  (Offerta)']");
	
	final By generaDocumentoButton = By.xpath("//button[text()='Genera Documento']");
	
	final By caricaDocumento = By.xpath("//input[@id='inputFile']");
	
	final By inputFileParentId = By.xpath("//input[@id='inputFileParentId']");
	
	final By caricaDocumentoButton = By.xpath("//button[@aria-label='Carica documento'] | //button[@label='Carica']");
	
	final By confermaDocumentoButton = By.xpath("//button[contains(@onclick,'confirm') and contains(@id,'ooter')]");
	//final By localUpload = By.xpath("//*[text()='Local Upload']/ancestor::tr//a");
	final By localUpload = By.xpath("//*[text()='DMS-LocalUpload']/ancestor::tr//a");
	
	
    final public By caricaDocumenti=By.xpath("//button[@id='load' and @title='carica documento']");
	public DocumentiUploadComponent(WebDriver driver ){
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
		
	}
	
	public String getFrameByIndex(int index) throws Exception{
		return util.getFrameByIndex(index);
	}

	public void generaDocumentoInDocumentale(String frame) throws Exception{
		util.frameManager(frame, generaDocumentoButton, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void generaDocumentoInDocumentale() throws Exception{
		util.objectManager( generaDocumentoButton, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void confermaDocumentoInDocumentale(String frame) throws Exception{
		util.frameManager(frame, confermaDocumentoButton, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
	}
	public void caricaDocumentoInDocumentale(String frame,String filePath) throws Exception{
//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		chargeDocument(frame, filePath);
		TimeUnit.SECONDS.sleep(10);
		Alert alert = driver.switchTo().alert();
		alert.accept();
		TimeUnit.SECONDS.sleep(2);
		driver.switchTo().defaultContent();
	}
	
	public void caricaDocumentoInDocumentaleSenzaAlert(String frame,String filePath) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		chargeDocument(frame, filePath);
		driver.switchTo().defaultContent();
	}
	
	public void caricaDocumentoInDocumentaleSenzaAlert(String filePath) throws Exception{
		chargeDocument( filePath);

	}
	
	private void chargeDocument(String frame,String filePath) throws Exception{
		File file = new File(filePath);
		TimeUnit.SECONDS.sleep(2);
//		//System.out.println("inizio a caricare");
		String uploadId = getUploadId();
//		//System.out.println("UPLOAD ID TROVATO :"+uploadId);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("j$(\"input[id*='inputFileParentId']\").val('"+uploadId+"');");
		//js.executeScript("j$(\"input[id*='inputFile']\").click();");
		js.executeScript("document.getElementById('inputFileParentId').val='"+uploadId+"';");
		js.executeScript("document.getElementById('inputFileParentId').value='"+uploadId+"';");
		driver.findElement(caricaDocumento).sendKeys(file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(20);
		
	}
	
	private void chargeDocument(String filePath) throws Exception{
		File file = new File(filePath);
		TimeUnit.SECONDS.sleep(2);
//		//System.out.println("inizio a caricare");
		String uploadId = getUploadId();
//		//System.out.println("UPLOAD ID TROVATO :"+uploadId);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("j$(\"input[id*='inputFileParentId']\").val('"+uploadId+"');");
		//js.executeScript("j$(\"input[id*='inputFile']\").click();");
		js.executeScript("document.getElementById('inputFileParentId').val='"+uploadId+"';");
		js.executeScript("document.getElementById('inputFileParentId').value='"+uploadId+"';");
		driver.findElement(caricaDocumento).sendKeys(file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(20);
		
	}
	
	public void uploadFile (String filePath) throws Exception {
		File file = new File(filePath);
		try {
			util.waitAndGetElement(fileUpload).sendKeys(file.getAbsolutePath());
		}catch (Exception e) {
			util.waitAndGetElement(fileUpload5).sendKeys(file.getAbsolutePath());
		}
		WebElement chiudiButton = util.waitAndGetElement(chiudi,150,5);
		util.waitAndGetElement(chiudiButton,By.xpath(".."),150,1).click();
	}
	
public void uploadFile2 (String filePath) throws Exception {
		
		File file = new File(filePath);
		driver.findElement(fileUpload2).sendKeys(file.getAbsolutePath());
		//util.waitAndGetElement(fileUpload2).sendKeys(file.getAbsolutePath());
}

public void uploadGenericFile (String filePath, By inputFile) throws Exception {
	
	File file = new File(filePath);
	driver.findElement(inputFile).sendKeys(file.getAbsolutePath());
	//util.waitAndGetElement(fileUpload2).sendKeys(file.getAbsolutePath());
}
	public void visualizzaDocumenti() throws Exception{
		TimeUnit.SECONDS.sleep(5);
		try {
			util.objectManager(tabCorrelato, util.scrollAndClick);
		}catch (Exception e) {
			util.objectManager(tabCorrelato2, util.scrollAndClick);
		}
		util.scrollDown();
		util.scrollDownDocument();
		TimeUnit.SECONDS.sleep(10);
		if(util.verifyExistence(activity, 10)){
			util.objectManager(activity, util.scrollToVisibility,false);
		}
		else{
			util.objectManager(offerte, util.scrollToVisibility,true);
		}
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(localUpload, util.scrollToVisibility,true);
//		util.scrollDown();
//		util.scrollDownDocument();
		WebElement element = util.waitAndGetElement(activity);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
//		WebElement el = util.waitAndGetElement(activity,20,2);
//		util.scrollToElement(el);
//		TimeUnit.SECONDS.sleep(3);
//		el.click();
		WebElement validaDoc = util.waitAndGetElement(validaDocButton);
		TimeUnit.SECONDS.sleep(10);
		util.scrollUp();
		validaDoc.click();
		TimeUnit.SECONDS.sleep(3);
		driver.switchTo().defaultContent();
		int timeout = 100;
		util.getFrameActive();
		if(!util.verifyExistence(By.xpath("//*[text()='Validazione Documentale']"), timeout))
			throw new Exception("Dopo aver cliccato sull'attivitÃ  documentale, in "+timeout+" secondi non vengono visualizzati i documenti da validare");
	}
	
	public void visualizzaDocumenti2() throws Exception{
		TimeUnit.SECONDS.sleep(5);
		try {
			util.objectManager(tabCorrelato2, util.scrollAndClick);
		}catch (Exception e) {
			util.objectManager(tabCorrelato, util.scrollAndClick);
		}
		util.scrollDown();
		util.scrollDownDocument();
		TimeUnit.SECONDS.sleep(10);
		if(util.verifyExistence(activity, 10)){
			util.objectManager(activity, util.scrollToVisibility,false);
		}
		else{
			util.objectManager(offerte, util.scrollToVisibility,true);
		}
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(localUpload, util.scrollToVisibility,true);
//		util.scrollDown();
//		util.scrollDownDocument();
		WebElement element = util.waitAndGetElement(activity);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
//		WebElement el = util.waitAndGetElement(activity,20,2);
//		util.scrollToElement(el);
//		TimeUnit.SECONDS.sleep(3);
//		el.click();
		WebElement validaDoc = util.waitAndGetElement(validaDocButton);
		TimeUnit.SECONDS.sleep(10);
		util.scrollUp();
		validaDoc.click();
		TimeUnit.SECONDS.sleep(3);
		driver.switchTo().defaultContent();
		int timeout = 100;
		util.getFrameActive();
		if(!util.verifyExistence(By.xpath("//*[text()='Validazione Documentale']"), timeout))
			throw new Exception("Dopo aver cliccato sull'attivitÃ  documentale, in "+timeout+" secondi non vengono visualizzati i documenti da validare");
	}

	
	public String getUploadId() throws Exception{
		WebElement el = util.waitAndGetElement(caricaDocumentoButton);
		String mydata = el.getAttribute("onclick");
        Pattern pattern = Pattern.compile("'(.*?)'");
        Matcher matcher = pattern.matcher(mydata);
        if (matcher.find()) mydata=matcher.group(1);
        else throw new Exception("Impossibile recuperare l'id del documento da caricare. Riprovare");
		return mydata;
	}
	
}
