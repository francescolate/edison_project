package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CVPComponent extends BaseComponent{

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	
	public CVPComponent(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	public By confermaCVP = By.xpath("//div[@aria-label='CVP']/ancestor::div[@role='region']//button[text()='Conferma']");

	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void verificaAssenzaCVP(By button) throws Exception{
		if(util.verifyExistence(button, 30)){
			throw new Exception("La sezione CVP viene erroneamente visualizzata. Per questa tipologia di scenario non deve essere visualizzata");
		}
	}
	
	
	
}
