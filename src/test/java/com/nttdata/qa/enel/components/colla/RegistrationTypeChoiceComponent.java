package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationTypeChoiceComponent extends BaseComponent {
	public By welcomeMessageLabel = By.cssSelector("#initialModal > section > div > div.cmPageTitleCont > div");
	public By pageHeader = By.cssSelector("#initialModal > section > div > div.cmPageTitleCont > h1");
	public By privateSelector = By.id("private");
	public By businessSelector = By.id("business");
	public By continueBtn = By.xpath("//button[@id='continua-button-initial-modal']");
	public By privato = By.xpath("//button[@id='private']");
	public By impresse = By.xpath("//button[@id='business']");
	
	public RegistrationTypeChoiceComponent(WebDriver driver) {
		super(driver);
	}
	
	public void checkContinueButtonIsDisabled() throws Exception {
		boolean privateSelectorActive = util.waitAndGetElement(privateSelector).getAttribute("class").equals("active");
		boolean businessSelectorActive = util.waitAndGetElement(businessSelector).getAttribute("class").equals("active");
		util.objectManager(continueBtn, util.scrollElement);
		boolean continueButtonEnabled = util.waitAndGetElement(continueBtn).getAttribute("class").contains("active");
		if (continueButtonEnabled && (privateSelectorActive == false && !businessSelectorActive == false)) {
			throw new Exception("The continue button should be disabled unless an account type has been selected");
		}
	}
	
	public static final String WelcomeMsg = "SCOPRI IL MONDO DEI SERVIZI PENSATI PER TE";
	public static final String PageTitle = "Registrati nell'area riservata";

}
