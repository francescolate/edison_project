package com.nttdata.qa.enel.components.colla;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ConfermaOffertaComponent extends BaseComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	String text1 = "Benvenuto in Enel Energia!"
			+ "La tua richiesta di cambio fornitore sulla fornitura in "
			+ "VIA MONTECERNO 3, 60027, ANCONA, OSIMO, AN "
			+ "con l'offerta E-Light Bioraria WEB "
			+ "sul pod n IT004E86658896 è confermata!";
	String text2 = "Riceverai una e-mail con il contratto e i documenti da conservare."
			+ "ACCEDI ALL'AREA CLIENTI TORNA ALLA HOMEOppure accedi alla tua area clienti tramite l'app Enel Energia";
	
	public ConfermaOffertaComponent (WebDriver driver) throws Exception{
		super(driver);
		
	}
	
	public void checkHome(String address, String offer, String pod) throws Exception{
		clickComponent(By.xpath("//*[@id='truste-consent-button']"));
		containsText(By.xpath("//div[@id='AS-TYP-cliente']//*[@id='adesioneSmartCliente']"), text1);
		containsText(By.xpath("//div[@id='AS-TYP-cliente']//*[@id='adesioneSmartCliente']"), text2);


	}

}
