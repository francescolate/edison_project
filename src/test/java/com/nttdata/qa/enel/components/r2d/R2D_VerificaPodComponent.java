package com.nttdata.qa.enel.components.r2d;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_VerificaPodComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public R2D_VerificaPodComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	public By inputPOD = By.xpath("//input[@name='pod']");
	public By inputPDR = By.xpath("//input[@name='pdr']");
	public By selectCodiceServizioDt = By.xpath("//select[@name='tipologiaRichiestaDt']");
	public By inputValidazionePreventivo = By.xpath("//input[@name='accettazionePreventivo']");
	public By inputIdRichiestaCRM = By.xpath("//input[@name='idRichiestaCrm']");
	public By buttonCerca = By.xpath("//img[@alt='Cerca'] | //*[contains(text(),'Cerca')]");
	public By buttonDettaglioPratiche = By.xpath("//img[@alt='Visualizza Dettaglio']");
//	public By rigaPratica = By.xpath("//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[2]");
	public By rigaPratica = By.xpath("//td[contains(text(),'Risultati della ricerca:')]/ancestor::tbody[1]//tbody/tr[2]/td[1]");
	public By nessunaPratica = By.xpath("//*[contains(text(),'Nessun risultato')]/ancestor::body[1]//tbody/tr[1]/td[2]/div[2]");
	public By praticaTrovata = By.xpath("//*[contains(text(),'Pratiche trovate')]/ancestor::body[1]//tbody/tr[1]/td[2]/center/table/tbody/tr/td");

	
	public void esistePratica() throws Exception{
		Boolean noPratica = driver.findElement(nessunaPratica).isDisplayed();
		TimeUnit.SECONDS.sleep(3);
	}

	public void inserisciPod(By inputPOD, String valore) throws Exception{
		util.objectManager(inputPOD,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void inserisciPdr(By inputPDR, String valore) throws Exception{
		util.objectManager(inputPDR,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void inserisciIdRichiestaCRM(By inputIdRichiestaCRM, String valore) throws Exception{
		util.objectManager(inputIdRichiestaCRM,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	public void selezionaCodiceServizioDt(By selectCodiceServizio,String valore) throws Exception{
		util.objectManager(selectCodiceServizio,util.scrollAndSelect,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	public void inserisciAccettazionePreventivo(By inputAccettazionePreventivo,String valore) throws Exception{
		util.objectManager(inputAccettazionePreventivo,util.sendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}

	public void cercaPod(By buttonCerca) throws Exception{
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
	public void dettaglioPratiche(By buttonDettaglio) throws Exception{
//		Verifico se è stata trovata almeno una pratica
		Boolean siPratica = driver.findElement(praticaTrovata).isDisplayed();
		TimeUnit.SECONDS.sleep(3);
		if(siPratica) {
			util.objectManager(buttonDettaglio,util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);
		} else  { 
			String messaggio = "Nessuna Pratica Trovata";
			System.out.println(messaggio);
			System.exit(1);
		}

	}
	public void selezionaPratica(By elemento) throws Exception{
		util.objectManager(elemento,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
	}
	
	// lettura e gestione Dettaglio Pratica per GAS
	public void verificaDettaglioPratica(Properties prop,ArrayList<String> campiDaVerificare,ArrayList<String> campiDaSalvare) throws Exception{
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(5);
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		Logger.getLogger("").log(Level.INFO, "campi da verificare:");
		//Elementi da Verificare
		String campo,valoreCampo,valoreCampoAtteso="";
		for(int i=0;i<campiDaVerificare.size();i++){
			String[] elem =campiDaVerificare.get(i).split(";");
			campo=elem[0];
			valoreCampoAtteso=elem[1];
			Logger.getLogger("").log(Level.INFO, "campo:"+campo);
			try{
//				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
				valoreCampo=driver.findElement(By.xpath("//table//td[text()='"+campo+"']/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo);
			valoreCampoAtteso=elem[1];
			//cambio in contains il compare
			if(!valoreCampo.contains(valoreCampoAtteso)){
				throw new Exception("Il valore del campo:"+campo+"non corrisponde a quello atteso.Valore visualizzato:"+valoreCampo+" valore atteso:"+valoreCampoAtteso);
			}
		}
		Logger.getLogger("").log(Level.INFO, "campi da salvare:");
		//Elementi da salvare
		for(int i=0;i<campiDaSalvare.size();i++){
			campo=campiDaSalvare.get(i);
			try{
				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			campo=campo.replaceAll(" ", "_");
			Logger.getLogger("").log(Level.INFO, "campo:"+campo.toUpperCase());
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo.toUpperCase());
			prop.setProperty(campo.toUpperCase(), valoreCampo.toUpperCase());
//			System.out.println("Verifica Nome Campo salvato: "+ campo.toUpperCase()+ " - Valore campo:"+valoreCampo.toUpperCase());
		}
		// aggiunti da claudio
//		prop.setProperty("ID_RICHIESTA", prop.getProperty("CODICE_ROW-ID_CRM"));
		if (!prop.getProperty("DATA_INVIO_RECESSO").isEmpty()){
			prop.setProperty("DATA_RECESSO", prop.getProperty("DATA_INVIO_RECESSO").substring(0, 10));
		}
//		prop.setProperty("DATA_RECESSO", prop.getProperty("DATA_INVIO_RECESSO").substring(0, 10));
		prop.setProperty("STATO_R2D", prop.getProperty("STATO_PRATICA_R2D").substring(0, 2));
		
//		System.out.println("Valore campo Codice ROW-ID CRM: "+ prop.getProperty("ID_RICHIESTA"));
//		System.out.println("Valore campo DATA_INVIO_RECESSO: "+ prop.getProperty("DATA_RECESSO"));
//		System.out.println("Valore campo STATO_R2D: "+ prop.getProperty("STATO_R2D"));
		//		By 	elementoTabella=new By.ByXPath("//table//td[contains(text(),'"+elemento+"')]");
		//		By	valoreElementoTabella=new By.ByXPath("//table//td[contains(text(),'"+elemento+"')]/parent::tr/td[2]");
		//Chiusura pagina attuale
		driver.close();
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		TimeUnit.SECONDS.sleep(3);
	}

	// lettura e gestione Dettaglio Pratica per GAS
	public void verificaDettaglioPratica_GAS(Properties prop,ArrayList<String> campiDaVerificare,ArrayList<String> campiDaSalvare) throws Exception{
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(5);
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		Logger.getLogger("").log(Level.INFO, "campi da verificare:");
		//Elementi da Verificare
		String campo,valoreCampo,valoreCampoAtteso="";
		for(int i=0;i<campiDaVerificare.size();i++){
			String[] elem =campiDaVerificare.get(i).split(";");
			campo=elem[0];
			valoreCampoAtteso=elem[1];
			Logger.getLogger("").log(Level.INFO, "campo:"+campo);
			try{
//				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
				valoreCampo=driver.findElement(By.xpath("//table//td[text()='"+campo+"']/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo);
			valoreCampoAtteso=elem[1];
			//cambio in contains il compare
			if(!valoreCampo.contains(valoreCampoAtteso)){
				throw new Exception("Il valore del campo:"+campo+"non corrisponde a quello atteso.Valore visualizzato:"+valoreCampo+" valore atteso:"+valoreCampoAtteso);
			}
		}
		Logger.getLogger("").log(Level.INFO, "campi da salvare:");
		//Elementi da salvare
		for(int i=0;i<campiDaSalvare.size();i++){
			campo=campiDaSalvare.get(i);
			try{
				valoreCampo=driver.findElement(By.xpath("//table//td[contains(text(),'"+campo+"')]/parent::tr/td[2]")).getText();
			}
			catch (Exception e){
				throw new Exception("Il campo:"+campo+" non è presente nel dettaglio");
			}
			campo=campo.replaceAll(" ", "_");
			Logger.getLogger("").log(Level.INFO, "campo:"+campo.toUpperCase());
			Logger.getLogger("").log(Level.INFO, "valore campo:"+valoreCampo.toUpperCase());
			prop.setProperty(campo.toUpperCase(), valoreCampo.toUpperCase());
//			System.out.println("Verifica Nome Campo salvato: "+ campo.toUpperCase()+ " - Valore campo:"+valoreCampo.toUpperCase());
		}
		// aggiunti da claudio
//		prop.setProperty("ID_RICHIESTA", prop.getProperty("NUMERO_ORDINE_CRM"));
		prop.setProperty("DATA_RECESSO", prop.getProperty("DATA_INVIO_RECESSO"));
		prop.setProperty("STATO_R2D", prop.getProperty("STATO_PRATICA").substring(0, 2));
		
//		System.out.println("Valore campo Codice ROW-ID CRM: "+ prop.getProperty("ID_RICHIESTA"));
//		System.out.println("Valore campo DATA_INVIO_RECESSO: "+ prop.getProperty("DATA_RECESSO"));
//		System.out.println("Valore campo STATO_R2D: "+ prop.getProperty("STATO_R2D"));
		//		By 	elementoTabella=new By.ByXPath("//table//td[contains(text(),'"+elemento+"')]");
		//		By	valoreElementoTabella=new By.ByXPath("//table//td[contains(text(),'"+elemento+"')]/parent::tr/td[2]");
		//Chiusura pagina attuale
		driver.close();
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		TimeUnit.SECONDS.sleep(3);
	}

}
