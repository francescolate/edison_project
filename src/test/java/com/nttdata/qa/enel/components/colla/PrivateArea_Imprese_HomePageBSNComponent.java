package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrivateArea_Imprese_HomePageBSNComponent {

	SeleniumUtilities util;
	WebDriver driver;
	
	public By leftMenuItems = By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a");
	public By contatti =By.xpath("//li[@id='contattiAct']/a");
	public By fornitureebollette = By.xpath("//h2[text()='Forniture e bollette']");
	public By homeMenu =By.xpath("//li[@id='homeMenu']//a");
	public By 	Homepagetitletext = By.xpath("//h1[text()='Le tue Bollette']");
	
	public By 	Homepagetitle = By.xpath("//h1[text()='Le tue Bollette']");
	public By panel1 = By.xpath("//div[@class='panel panel-default panel-primary']//h4[@class='panel-title']");
	public By latuaFornituranelDettaglio = By.xpath("//h5[text()='La tua fornitura nel dettaglio.']");
	public By latuaFornituranelDettaglioValue1 = By.xpath("//div[@class='panel-body gray']//div[1][@class='col-xxs-12 col-xs-6']//span[1]");
	public By latuaFornituranelDettaglioValue2 = By.xpath("//div[@class='panel-body gray']//div[1][@class='col-xxs-12 col-xs-6']//span[2]");
	public By latuaFornituranelDettaglioValue3 = By.xpath("//div[@class='panel-body gray']//div[2][@class='col-xxs-12 col-xs-6']//span[1]");
	public By latuaFornituranelDettaglioValue4 = By.xpath("//div[@class='panel-body gray']//div[2][@class='col-xxs-12 col-xs-6']//span[2]");
	public By chatClose = By.xpath("(//*[@class='icon icon-chat_closed'])[1]");
	public By piuInformazioniLink = By.xpath("//a[text()='Più informazioni']");
	public By letuebollete = By.xpath("//h5[text()='Le tue bollette']");
	public By numero = By.xpath("//table//tr//th[text()='Numero']");
	public By importo = By.xpath("//table//tr//th[text()='Importo']");
	public By stato = By.xpath("//table//tr//th[text()='Stato']");
	public By scadenza = By.xpath("//table//tr//th[text()='Scadenza']");
	public By bill1 = By.xpath("//table//tbody//tr[1]");
	public By bill2 = By.xpath("//table//tbody//tr[3]");
	public By bill3 = By.xpath("//table//tbody//tr[5]");
	public By vaiAllElencoBollette = By.xpath("//div[@class='panel-body gray']//a[contains(text(),'Vai all')]");
	public By pageTitle = By.xpath("//h1[contains(text(),'Benvenuto nella')]");
	public By pagePath = By.xpath("//div[@class='panel panel-default panel-clear']//ol");
	public By vantaggiperil = By.xpath("//h2[text()='Vantaggi per il tuo business']");
	public By banner1 = By.xpath("//div[@id='boxLeft']//div[@class='caption']");
	public By banner2 = By.xpath("//div[@id='boxRight']//div[@class='caption']");
	public By servizi = By.xpath("//li[@id='menuService']/a");
	public By bolleteMenu = By.xpath("//li[@id='menuBoll']//a");
	public By bolleteDaPagare = By.xpath("//a[text()='Bollette da pagare']");
	public By furnitureMenu = By.xpath("//li[@id='menuForn']//a[text()='fornitura']");
	public By messaggiMenu = By.xpath("//li[@id='menuMessaggi']//a");
	public By statoRichiesta = By.xpath("//li[@id='statoRichiesta']//a");
	public By reportBillingManagement = By.xpath("//li[@id='rbmAct']//a");
	public By caricaDoccumenti = By.xpath("//li[@id='caricaDoc']//a");
	public By logout = By.xpath("//div[@id='sticky-wrapper']//a[text()='Logout']");
	
	public By jmeterLogo = By.xpath("//img[@id='avatarUtente']");
	public By datiDiContatto = By.xpath("//a[@name='Dati di contatto']");
	public By defaultSupply = By.xpath("//div[@id='collapseOne']//div[@class='row'][2]//div[@class='col-xxs-12 col-xs-6']//span");
	public By supplies = By.xpath("//div[contains(@id,'heading-')]");
	public By numreoClienti = By.xpath("(//div[@id='accordion']//child::h4[@class='panel-title'])[1]");
	
	public By vaiAllElencoBollette1 = By.xpath("//div[@id='collapseOne']//a[contains(text(),'Vai all')]");
	public By bills = By.xpath("//div[@id='tue_bollette']//tr//td[1]");
	public By VaiAllelencoForniture = By.xpath("//div[@id='moreForn']//a[contains(text(),'Vai all')]");
	
	public PrivateArea_Imprese_HomePageBSNComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	public void leftMenuItemsDisplay (By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String item_found="NO";
	List<WebElement> leftMenu = driver.findElements(By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a"));
	int total = leftMenu.size();
	
	
	for(WebElement element : leftMenu){
	String description=element.getText();
	
	description= description.replaceAll("(\r\n|\n)", " ");
		
	if (description.contentEquals("HOMEPAGE")) 
		item_found="YES";
	
	else if(description.contentEquals("BOLLETTE"))
		item_found="YES";
	else if (description.contentEquals("FORNITURE"))
		item_found="YES";
	else if (description.contentEquals("SERVIZI"))
		item_found="YES";
	else if (description.contentEquals("MESSAGGI"))
		item_found="YES";
	else if (description.contentEquals("STATO RICHIESTE"))
		item_found="YES";
	else if (description.contentEquals("REPORT BILLING MANAGEMENT"))
		item_found="YES";
	/*else if (description.toLowerCase().contentEquals("gestione piano abbonamento"))
		item_found="YES";
	else if (description.toLowerCase().contentEquals("area clienti casa"))
		item_found="YES";*/
	else if (description.contentEquals("CARICA DOCUMENTI"))
		item_found="YES";
	else if (description.contentEquals("ENGLISHANDBOOK"))
	item_found="YES";
	/*else if (description.toLowerCase().contentEquals("i tuoi diritti"))
		item_found="YES";*/
	else if (description.contentEquals("LOGOUT"))
		item_found="YES";
		if (item_found.contentEquals("NO"))
		throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'bollette', 'forniture','Servizi','messaggi','stato richieste','report billing management','gestione piano abbonamento','area clienti casa','caricaDoc','contatti','i tuoi diritti','logout'");
			}
		}
	
	public void isElementSelected(By checkObject) throws Exception{
		util.waitAndGetElement(checkObject);
		Boolean flag = driver.findElement(checkObject).isSelected();
		if(flag.equals("false")){
			throw new Exception("We element is not selected ");
		}
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void isDisplayedChatWindow(By checkObject)
		{
			
			if(driver.findElement(checkObject).isDisplayed())
			{
				driver.findElement(checkObject).click();
			}
			
		}
		
		public void checkDefaultSupply(By checkObject) throws Exception{
		//	clickComponent(By.xpath("//a[@href='#collapse-0']//span"));
			String panelTitle = driver.findElement(numreoClienti).getText();
			String [] title = panelTitle.split("\n");
			String [] FornitureClienti = title[1].split(":");
			System.out.println(FornitureClienti[0]);
			System.out.println(FornitureClienti[1]);
			if((!FornitureClienti[0].equals(FornitureClienti)) && FornitureClienti[1].equals("")) throw new Exception("Fornitura Luce Cliente N ° value is empty");
			
			List<WebElement> ele = driver.findElements(checkObject);
			System.out.println(ele.size());
			for(int i=0;i<ele.size();i++){
			String labelAndValue = ele.get(i).getText();
			System.out.println(labelAndValue);
			ArrayList value = new ArrayList<>();
			String[] array = labelAndValue.split(":");
			System.out.println(array[0]);
			System.out.println(array[1]);
			value.add(array[1]);
			//System.out.println(value.get(i));
			if(!value.contains(array[1]))
			throw new Exception("Field value is not displaying");
			}
		}
		
		public void checkSupplyDetails(By checkObject) throws Exception{
			List<WebElement> ele = driver.findElements(checkObject);
			//System.out.println(ele.size());
			for(int i=0;i<ele.size()-1;i++){
				
				String panelTitle = driver.findElement(By.xpath("//div[@id='heading-"+(i+1)+"']//h4")).getText();
				String [] title = panelTitle.split("\n");
				String [] FornitureClienti = title[1].split(":");
				System.out.println(FornitureClienti[0]);
				System.out.println(FornitureClienti[1]);
				if((!FornitureClienti[0].equals(FornitureClienti)) && FornitureClienti[1].equals("")) throw new Exception("Fornitura Luce Cliente N ° value is empty");
			
			//	verifyComponentExistence(By.xpath("//div[@id='heading-"+(i+1)+"')]"));
				clickComponent(By.xpath("//div[contains(@id,'heading-"+(i+1)+"')]//a[@href='#collapse-"+(i+1)+"']"));
				Thread.sleep(5000);
				List<WebElement> ele1 = driver.findElements(By.xpath("//div[@id='collapse-"+(i+1)+"']//div[@class='row'][2]//div[@class='col-xxs-12 col-xs-6']//span"));
				//System.out.println(ele1.size());
				for(int j=0;j<ele1.size();j++){
				String labelAndValue = ele1.get(j).getText();
				//System.out.println(labelAndValue);
				ArrayList value = new ArrayList<>();
				String[] array = labelAndValue.split(":");
				Thread.sleep(5000);
				System.out.println(array[0]);
				System.out.println(array[1]);
				value.add(array[1]);
				if(!value.contains(array[1]))
				throw new Exception("Field value is not displaying");
				}

			}
		}
		
		public void verifyChronologicalOrder(By checkObject) throws Exception{
			clickComponent(By.xpath("//div[@id='heading-Index']//a"));
			List<WebElement> ele = driver.findElements(checkObject);
			//System.out.println(ele.size());
			ArrayList array = new ArrayList<>();
			for(int i=ele.size()-1;i>=0;i--){
				String value = ele.get(i).getText();
				//System.out.println(value);
				array.add(value);							
			}
			Collections.reverse(array);
			for(int i=0;i<array.size();i++){
			//	System.out.println("Web"+ele.get(i).getText());
			//	System.out.println("Array"+array.get(i));
				if(!ele.get(i).getText().equals(array.get(i)))
					throw new Exception("Displayed data is not in chronological order");
			}
			
		}
		
		public void checkForErrorPopup() {
			try {
				if(driver.findElement(By.xpath("//button[@class='remodal-close']")).isDisplayed())
					clickComponent(By.xpath("//button[@class='remodal-close']"));
					} catch (Exception e) {
						e.printStackTrace();
					}
				
		}
		
		public static final String Path = "Area riservataHomepage";
		public static final String PageTitle = "Benvenuto nella tua area dedicata Business";
		public static final String Panel1Text = "639391728Fornitura Luce Cliente N °: 639391728";
		public static final String latuaFornituranelDettaglioText = "La tua fornitura nel dettaglio.";
		public static final String LatuaFornituranelDettaglioValue1 = "Luogo: VIALE S.LUCIO 59A - 24023 CLUSONE BG";
		public static final String LatuaFornituranelDettaglioValue2 = "Tipologia:Luce";
		public static final String LatuaFornituranelDettaglioValue3 = "Offerta: Anno Sicuro";
		public static final String LatuaFornituranelDettaglioValue4 = "Stato: Disattivato";
		public static final String BillOneDetails = "3043117518€ 57,77PAGATA23/07/2019";
		public static final String BillTwoDetails = "3038076881€ 60,93PAGATA24/06/2019";
		public static final String BillThreeDetails = "3029426809€ 64,53PAGATA24/05/2019";
		public static final String BannerOneTxt = "SERVIZI Addebito Diretto Il servizio di Addebito Diretto ti consente di addebitare l'importo delle tue bollette sulla tua Carta di Credito o sul tuo Conto Corrente.";
		public static final String BannerTwoText = "SERVIZI Bolletta Web Il servizio Bolletta Web consente di ricevere direttamente la bolletta tramite email e la notifica tramite SMS.";
		public static String[] Luogo ={"Via Ada Negri 33 20141 Milano Mi"};
		public static final String FornituraLabel = "Fornitura Luce Cliente N °";
		public static final String HOMEPAGETITLETEXT = "Benvenuto nella tua area dedicata Business";
}
