package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OffertalaunchComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By attivaSubito = By.xpath("//a[contains(text(),'Attiva subito')]");
	public String offertaUrl = "https://www-coll1.enel.it/it/adesione-contratto?productType=res";
	public By aderisci = By.xpath("//p[text()='ADERISCI']");
	public By pageTitle = By.xpath("//h1[text()='Valore Luce Plus']");
	public By pageText = By.xpath("//p[@class='description_container']");
	public By caricaBolletta = By.xpath("//button[text()='CARICA BOLLETTA']");
	public By compilaManualmente = By.xpath("//button[text()='COMPILA MANUALMENTE']");
	public By inserisciITuoiDati = By.xpath("//h3[text()='Inserisci i tuoi dati']");
	public By informazioniFornitura = By.xpath("//h3[text()='Informazioni fornitura']");
	public By pagamentiEBollette = By.xpath("//h3[text()='Pagamenti e bollette']");
	public By consensi = By.xpath("//h3[text()='Consensi']");
	public By nome = By.xpath("//input[@id='nameId']");
	public By cognome = By.xpath("//input[@id='surnameId']");
	public By cf = By.xpath("//input[@id='cfId']");
	public By cellulare = By.xpath("//input[@id='phoneId']");
	public By email = By.xpath("//input[@id='emailId']");
	public By emailConferma = By.xpath("//input[@id='emailConfirmId']");
	public By checkBox = By.xpath("(//span[contains(text(),'Ho preso visione')])[1]");
	public By prosegui1 = By.xpath("(//button[text()='PROSEGUI'])[1]");
	//public By inserci("//div[@id='panel1a-header']//h3[@class='sub_header'][text()='INSERISCILO DOPO']")
	//public By inserisci = By.xpath("(//div[@id='panel1a-header']//h3[@class='sub_header']");
	public By Modi  = By.xpath("//button[@class='step_summary_modify_button']");
	public By ANNULLA  = By.xpath("(//button[text()= 'ANNULLA'])[1]");
	//$x("//button[text()='ANNULLA'][1]")
	//button[text()=ANNULLA'])[3]
	//button[@class='proceed_button'][normalize-space()='CONTINUA']
	//public By CONTINUA  = By.xpath("(//button[@class='CONTINUA']");
	
	public By CONTINUA  =By.xpath("(//button[text()='CONTINUA'])[5]");

	//public By ANNULLA  = By.xpath("//button[@class='exit_button'][normalize-space()='ANNULLA']");
	//public By CONTINUA  = By.xpath("//button[@class='proceed_button'][normalize-space()='CONTINUA']");
	public By cellularechange = By.xpath("//input[@id='phoneId']");
	
	
	//public By Modifica = By.xpath("//input[@id='pod-pdr']");
	//public By prosegui1 = By.xpath("(//button[text()='PROSEGUI'])[1]");
	
	public By informazioneText1 = By.xpath("//p[@class='infoCap']");
	public By pod = By.xpath("//input[@id='pod-pdr']");
	public By cap = By.xpath("//input[@id='capSupply']");
	public By privacyText = By.xpath("//p[@class='privacy_text']");
//	public By prosegui2 = By.xpath("(//button[text()='PROSEGUI'])[2]");
	public By inseriscilodopo = By.xpath("(//button[text()='INSERISCILO DOPO'])");
	public By labelPrivacyCheckbox = By.xpath("//label[@id='label-privacy-consent']");
	public final String switchFrame = "//div[@id='iframe']//iframe";
	

	public OffertalaunchComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
	 
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void jsClickComponent(By by) throws Exception{
			util.jsClickElement(by);
		}
		
		public void clickTab(By Object) throws Exception {
			driver.findElement(Object).sendKeys(Keys.TAB);;
		}
		
		public void verifyDefaultValue(By checkObject, String Value ) throws Exception
		{			
			String actualValue = driver.findElement(checkObject).getText();
			System.out.println(actualValue);
			System.out.println(Value);
			if (!actualValue.equals(Value))
				throw new Exception("Default Field value is not matching with the expected value"+Value);
					
		}
		
		public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
	        
	        WebDriverWait wait = new WebDriverWait(this.driver, 50);
	            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
	        if(value.equals(""))throw new Exception("Field is not pre filled");
	        
	        String match = "FAIL";
	        if(value.contentEquals(expectedValue))
	        	match = "PASS";
	        if(match.contentEquals("FAIL"))
	        	throw new Exception("PrePopulatedValue and Expected values are not matching");
	    }
		
		
		public void verifyElementNotDisplaying(By locator) throws Exception
		{
			List element = (List) driver.findElement(locator);
			if(element.size()>0)
				throw new Exception("Element is displaying");				
		}
	
		
		public void verifyEmailContent(String email,String emailContent) throws Exception{
			System.out.println(emailContent);
			if(!email.contains(emailContent)) 
				throw new Exception("Given String is not present in Email body");
		}
		
		public static final String PageText = "Vuoi procedere velocemente con l'attivazione?Puoi caricare la tua attuale bolletta in formato digitale e penseremo noi a compilare i tuoi dati.Dovrai solamente verificarli e completarli con alcune informazioni aggiuntive.In alternativa puoi compilare manualmente le informazioni richieste.";
		public static final String InformazioneText1 = "Conferma il CAP cliccando sul campo e selezionando dall’elenco la voce corretta.";
		public static final String PrivacyText = "Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l’adesione.";
		public static final String IndrizzoDiFornituraText = "Verifica e conferma le seguenti informazioni cliccando su ogni campo e selezionando dall’elenco la voce corretta.";
		public static final String IndrizzoDiFornituraPrivacyText = "Ti informiamo che la fornitura verrà attivata con i medesimi requisiti tecnici (potenza e tensione) attualmente in esercizio. Se lo desideri potrai effettuare una variazionedi potenza dopo l'attivazione del contratto direttamente dalla tua area clienti.";
		public static final String IndrizzoPrivacyText = "Se non hai a portata di mano il POD puoi continuare a compilare i tuoi dati e inserirlo in seguito. È però necessario inserirlo per completare l’adesione.";
		public static final String Recapitiqst1 = "Sei residente presso la fornitura?";
		public static final String Recapitiqst2 = "Dove vuoi ricevere le comunicazioni relative alla tua fornitura?";
		public static final String RecapitiansOne = "Allo stesso indirizzo della fornitura";
		public static final String RecapitiansTwo = "Ad un indirizzo diverso da quello della fornitura";
		public static final String IndrizzoDiFornituraPrivacyText2 = "Ti ricordiamo che la tariffa per uso domestico può essere applicata solo su una delle forniture a te intestate e presso la quale hai la residenza anagrafica.";
		public static final String BollettinoPostaleText = "Ricorda che se scegli l’addebito diretto potrai risparmiare le commissioni previste per il pagamento tramite bollettino postale o Lottomatica.";
		public static final String CodiciPromozionaliSubtext = "Se sei in possesso di un codice promozionale inseriscilo nel campo seguente";
		public static final String Mandati_E_Consensi_TextInBox = "Dichiaro di voler conferire ad Enel Energia:mandato irrevocabile senza rappresentanza per lo svolgimento presso il distributore competente delle attività di gestione della connessione dei punti di prelievo (es. aumenti di potenza, spostamenti di gruppi di misura, etc), mantenendo la titolarità di ogni rapporto giuridico con il distributore competente inerente la connessione alla rete dei siti ed impianti. Tale mandato è a titolo oneroso ed obbliga la corrispondere al Fornitore gli importi necessari per l’esecuzione del mandato e per l’adempimento delle obbligazioni che a tal fine il Fornitore ha contratte in proprio nome;mandato irrevocabile con rappresentanza per la sottoscrizione del Contratto per il servizio di connessione alla rete elettrica allegate al contratto per il servizio di trasporto dell'energia elettrica, del cui contenuto il Cliente ha preso atto anche in quanto disponibile sul sito www.enel.it, consapevole che l’accettazione ed il rispetto delle stesse è condizione necessaria per l’attivazione ed il mantenimento del servizio di trasporto.";
		public static final String Mandati_e_Consensi_chkbx1Text = "Ho preso visione dell' informativa privacy riguardo come tratterete i miei dati personali.";
		public static final String Mandati_e_Consensi_chkbx2Text =	"Ho preso visione delle note legali ai sensi del D.Lgs 70/2003 e le informazioni precontrattuali ai sensi dell'art 49 del D.Lgs 21/2014 - Codice del Consumo.";
		public static final String Mandati_e_Consensi_chkbx3Text = "Voglio recedere dal contratto attualmente in essere con ACEA ENERGIA SPA";
		public static final String Richiesta_Di_Esecuzione_Anticipata_Del_Contratto_Text = "Ogni consumatore ha diritto ad un periodo di ripensamento di 14 giorni dal completamento del processo di adesione durante i quali non si procede all'avanzamento della pratica di attivazione; se il consumatore lo desidera, può chiedere l'esecuzione anticipata della fornitura. Vuoi avviare subito il processo di attivazione?";
		public static final String Richiesta_Di_Esecuzione_POPUP_Content = "Scegliendo di non effettuare la \"Richiesta di esecuzione anticipata della fornitura\", il Cliente richiede espressamente che le procedure per dar corso all'attivazione vengano avviate solo dopo che siano trascorsi i 14 giorni di ripensamento che partono dalla conclusione del processo di acquisizione e dall’ eventuale restituzione della documentazione obbligatoria. Anche in caso di richiesta di attivazione di un contatore disattivato, chiuso o che non ha mai erogato energia, il cliente sarà tenuto ad attendere il periodo di ripensamento dei 14 giorni previsto dal Codice del Consumo.";
		public static final String Consenso_Marketing_Enel_Energia_Text = "L’interessato acconsente al trattamento dei propri dati personali da parte di Enel Energia, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail) Codice PrivacyCodice Privacy";
		public static final String Consenso_Marketing_Terzi_Text = "L’interessato acconsente al trattamento dei propri dati personali da parte delle Società del Gruppo Enel (tra cui Enel X), da società controllanti, controllate o collegate, o da partner commerciali di Enel Energia, per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail) Codice PrivacyCodice Privacy";
		public static final String Consenso_Profilazione_Enel_Energia_Text = "L’interessato acconsente al trattamento dei propri dati personali per attività di profilazione basate sulle abitudini di consumo e sui dati e informazioni acquisite attraverso l’utilizzo dei prodotti o servizi utilizzati, da parte di Enel Energia o da parte di Società del Gruppo Enel, da società controllanti, controllate o collegate o da partner commerciali di Enel Energia.";
		public static final String Richiesta_Di_Esecuzione_PopupContent = "Scegliendo di non effettuare la \"Richiesta di esecuzione anticipata della fornitura\", il Cliente richiede espressamente che le procedure per dar corso all'attivazione vengano avviate solo dopo che siano trascorsi i 14 giorni di ripensamento che partono dalla conclusione del processo di acquisizione e dall’ eventuale restituzione della documentazione obbligatoria. Anche in caso di richiesta di attivazione di un contatore disattivato, chiuso o che non ha mai erogato energia, il cliente sarà tenuto ad attendere il periodo di ripensamento dei 14 giorni previsto dal Codice del Consumo.";
		public static final String TornaAllaHomePageTitle = "Sei ad un passo dal completare l'adesione!";
		public static final String TornaAllaHomeMSG1 = "Ti abbiamo inviato un messaggio all'indirizzoControlla l'email e conferma il contratto della tua fornitura energia.";
		public static final String TornaAllaHomeMSG2 = "Se non dovessi riceverla, verifica la casella della posta indesiderata";
		
		public static final String EmailContentLine1 = "Gentile PETER PARKER,";
		public static final String EmailContentLine2 = "per confermare l'attivazione dell'offerta Valore Luce Plus fai click sul pulsante entro il";
		public static final String EmailContentLine3 = "Oppure copia e incolla questo link nel tuo browser e premi";
		public static final String EmailContentLine4 = "Ti ricordiamo che la tua fornitura sarà attiva solo dopo la verifica dei dati che ci hai comunicato.";
		public static final String EmailContentLine5 = "Questa email è stata inviata in modo automatico, ti invitiamo a non rispondere a questo indirizzo di posta elettronica.";
		public static final String EmailContentLine6 = "La informiamo che Enel Energia tratterà i suoi dati personali in conformità alla normativa in materia di protezione dei dati personali (Regolamento UE 2016/679, c.d. \"GDPR\").";
		public static final String EmailContentLine7 = "Titolare del trattamento dei dati personali è Enel Energia S.p.A., con sede legale in Viale Regina Margherita, 125 - 00198 - Roma. Informativa completa disponibile sul sito";
		public static final String EmailContentLine8 = "Enel Energia SpA - Società con unico socio - Sede Legale 00198 Roma, viale Regina Margherita 125 - Registro Imprese di Roma - R.E.A. 1150724";
		public static final String EmailContentLine9 = "Gruppo IVA Enel P.IVA  15844561009, C.F 06655971007 Capitale Sociale Euro 302.039,00 i.v. - Direzione e Coordinamento di Enel SpA";
}
