package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PaginaAdesioneInserisciDatiComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By frame = By.xpath("//iframe[@id='iframeSF']");
	public By campoFormaGiuridica=By.xpath("//div[@class='form-group']//label[@id='legalFormLabel' and text()='Forma giuridica*']/ancestor::div[1]//span[@id='legalFormSelectBoxItText' and text()='Forma giuridica']");
	public By codiceFiscaleSocieta=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Codice Fiscale Società*']/ancestor::div[1]//input[@id='busFiscalCode']");
	
	//108
	public By ragioneSociale=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Ragione Sociale*']/ancestor::div[1]//input[@id='ITA_IFM_Company__c']");
	public By PartitaIVA=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Partita IVA']/ancestor::div[1]//input[@id='pIVA']");
	public By Prefisso=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Prefisso']/ancestor::div[1]//select[@id='legalFormPrefix']");
	public By TelefonoSocietà=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Telefono Società']/ancestor::div[1]//input[@id='ITA_IFM_Company_Phone__c']");
	public By EmailSocietà=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Email Società*']/ancestor::div[1]//input[@id='companyEmail']");
	public By PostaElettronicaCertificata=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Posta Elettronica Certificata (PEC)']/ancestor::div[1]//input[@id='pecEmail']");
	public By Tiporeferente=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Tipo referente*']/ancestor::div[1]//span[@id='referenceTypeSelectBoxIt']");
	public By Nomereferente=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Nome referente*']/ancestor::div[1]//input[@id='referenceFirstName']");
	public By Cognomereferente=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Cognome referente*']/ancestor::div[1]//input[@id='referenceLastName']");
	public By CodiceFiscaleReferente=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Codice Fiscale Referente*']/ancestor::div[1]//input[@id='referenceTaxCode']");
	public By PrefissoReferente=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Codice Fiscale Referente*']/ancestor::div[1]//input[@id='referenceTaxCode']");
	public By TelefonoCellulareReferente=By.xpath("//div[@class='form-group']//label[@class='control-label' and text()='Telefono Cellulare Referente*']/ancestor::div[1]//input[@id='referencePhone']");
	
	
	
	
	public By inserisciDati=By.xpath("//h3[@id='intestazione_1' and text()='Inserisci i tuoi dati']");
	public By campoNome=By.xpath("//div[@class='form-group']//label[@for='ITA_IFM_First_Name__c' and text()='Nome*']/ancestor::div[1]//input[@placeholder='Nome' and @id='ITA_IFM_First_Name__c']");
	public By campoNomeError=By.xpath("//div[@class='form-group has-error']//label[@for='ITA_IFM_First_Name__c' and text()='Nome*']/ancestor::div[1]//input[@placeholder='Nome' and @id='ITA_IFM_First_Name__c']");
	public By campoCognome=By.xpath("//div[@class='form-group']//label[@for='ITA_IFM_Last_Name__c' and text()='Cognome*']/ancestor::div[1]//input[@placeholder='Cognome' and @id='ITA_IFM_Last_Name__c']");
	public By campoCognomeError=By.xpath("//div[@class='form-group has-error']//label[@for='ITA_IFM_Last_Name__c' and text()='Cognome*']/ancestor::div[1]//input[@placeholder='Cognome' and @id='ITA_IFM_Last_Name__c']");
	public By campoCodicefiscale=By.xpath("//div[@class='form-group']//label[@for='NE__Fiscal_Code__c' and text()='Codice Fiscale*']/ancestor::div[1]//input[@placeholder='Codice Fiscale' and @id='NE__Fiscal_Code__c']");
	public By campoCodicefiscaleError=By.xpath("//div[@class='form-group has-error']//label[@for='NE__Fiscal_Code__c' and text()='Codice Fiscale*']/ancestor::div[1]//input[@placeholder='Codice Fiscale' and @id='NE__Fiscal_Code__c']");
	
	
	public By campoTelefonocellulare=By.xpath("//div[@class='form-group']//label[@for='ITA_IFM_Mobile_Phone__c' and text()='Cellulare*']/ancestor::div[1]//input[@id='ITA_IFM_Mobile_Phone__c' ]");
	public By campoTelefonocellulareError=By.xpath("//div[@class='form-group has-error']//label[@for='ITA_IFM_Mobile_Phone__c' and text()='Cellulare*']/ancestor::div[1]//input[@id='ITA_IFM_Mobile_Phone__c' ]");
	public By campoTelefonocellulareObbligatorio=By.xpath("//span[@id='ITA_IFM_Mobile_Phone__c_errmsg' and text()='Campo obbligatorio']");
	public By campoTelefonocellulareFormatoNonCorretto=By.xpath("//span[@id='ITA_IFM_Mobile_Phone__c_errmsg' and text()='Formato non corretto']");
	
	public By campoEmail=By.xpath("//div[@class='form-group']//label[@for='NE__E_mail__c' and text()='Email*']/ancestor::div[1]//input[@id='NE__E_mail__c' and @placeholder='Example@mail.it']");
	public By campoEmailError=By.xpath("//div[@class='form-group has-error']//label[@for='NE__E_mail__c' and text()='Email*']/ancestor::div[1]//input[@id='NE__E_mail__c' and @placeholder='Example@mail.it']");
	public By campoEmailFormatoNonCorretto=By.xpath("//span[@id='NE__E_mail__c_errmsg' and text()='Formato non corretto']");
	public By campoEmailObbligatoria=By.xpath("//span[@id='NE__E_mail__c_errmsg' and text()='Campo obbligatorio']");
	
	public By campoConfermaEmail=By.xpath("//div[@class='form-group']//label[@for='NE__E_mail__c_confirm' and text()='Conferma email*']/ancestor::div[1]//input[@id='NE__E_mail__c_confirm' and @placeholder='Example@mail.it']");
	public By campoConfermaEmailError=By.xpath("//div[@class='form-group has-error']//label[@for='NE__E_mail__c_confirm' and text()='Conferma email*']/ancestor::div[1]//input[@id='NE__E_mail__c_confirm' and @placeholder='Example@mail.it']");
	public By checkboxPrivacy=By.xpath("//div[@class='form-group checkbox privacy-checkbox']//label[@id='label-privacy-consent' and text()=\"Ho preso visione dell'informativa sulla privacy\"]");
	public By proseguiButton=By.xpath("//button[@type='button' and text()='Prosegui']");
	public By labelErrorCampoObbligatorio=By.xpath("//span[@aria-live='assertive' and @role='alert' and text()='Campo obbligatorio']/ancestor::div[1]");
	public String campoObbl[]= {"Nome*","Cognome*","Codice Fiscale*","Cellulare*","Email*","Conferma email*","Ho preso visione dell'informativa sulla privacy"};
	public By formatoNonCorrettoNome=By.xpath("//span[@id='ITA_IFM_First_Name__c_errmsg' and text()='Formato non corretto']");
	public By formatoNonCorrettoCognome=By.xpath("//span[@id='ITA_IFM_Last_Name__c_errmsg' and text()='Formato non corretto']");
	public By formatoNonCorrettoCF=By.xpath("//span[@id='NE__Fiscal_Code__c_errmsg' and text()='Codice fiscale errato']");
	public By calcolaloOra=By.xpath("//a[@id='calculateTaxCode']//span[text()='Calcolalo ora']");
	
	public By linkInformativa=By.xpath("//a[@id='privacyCtaId']//span[text()='Informativa Privacy']");
	public By titoloInformativaParte1=By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//h4[@class='modal-title' and contains(text(),\"INFORMATIVA PRIVACY AI SENSI DELL'ART. 13 DEL REGOLAMENTO UE 2016/679 (\")]");
	public By close=By.xpath("//div[@class='modal-header']//button[@id='modalPrivacyInfoCloseBtn' and @aria-label='Chiudi modale informativa privacy']");
	public String informativa="INFORMATIVA PRIVACY AI SENSI DELL'ART. 13 DEL REGOLAMENTO UE 2016/679 (\"GDPR\")";
	public By checkBoxInformativaError=By.xpath("//label[@id='label-privacy-consent' and text()=\"Ho preso visione dell'informativa sulla privacy\"]//ancestor::div[2]//span[@id='privacy-consent_errmsg' and text()='Campo obbligatorio']");
	public By presavisone=By.xpath("//button[@id='checkPresaVisioneBtn' and text()='Ho preso visione']");
	public By checkboxInfo=By.xpath("//input[@id='privacy-consent']");
	
	public PaginaAdesioneInserisciDatiComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verificaTesto (By object,String testo) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
		//System.out.println(text);
		if (!text.contentEquals(testo)) 
        	throw new Exception("la label di testo con descrizione "+testo+" non e' presente");
	
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void verifyNotComponentExistence(By existentObject) throws Exception {
		if (util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " esiste, per cui il campo non e' stato popolato correttamente");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void enterValueInInputBox(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void setframe(By frame) {
		WebElement frameToSw = driver.findElement(frame);
		driver.switchTo().frame(frameToSw);
	}
	
	public void click(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		
	}
	
	public void checkErrorLabelCampoObbligatorio(By object,String [] error)throws Exception {
		TimeUnit.SECONDS.sleep(3);
		 List<WebElement> errorLabel = util.waitAndGetElements(object);
		 
		 if (errorLabel.size()==error.length) {
			for (int i = 0; i < error.length; i++) {
				TimeUnit.SECONDS.sleep(1);
				 String label = util.waitAndGetElement(errorLabel.get(i),By.xpath("./label")).getAttribute("innerText");
			//	System.out.println(label);
				  if(!label.contentEquals(error[i].trim()))
					  	throw new Exception("la label 'Campo Obbligatorio' relativa al campo "+error[i]+" NON e' presente ");  
			}
		 }
		 
		 else throw new Exception("la label 'Campo Obbligatorio' NON compare "+errorLabel.size()+" volte nella pagina web di inserisci dati per l'adesione per l'offerta 'Scegli Tu Ore Free'");
	}
}
