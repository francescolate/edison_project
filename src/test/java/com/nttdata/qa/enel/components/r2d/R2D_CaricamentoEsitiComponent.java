package com.nttdata.qa.enel.components.r2d;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_CaricamentoEsitiComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public By RadioTipoPuntualePS = By.xpath("//input[@id='loadingTypePPS']");
	public By RadiotipoMassivo= By.xpath("//input[@id='loadingTypeM']");
	public By RadiotipoPuntualeSWA= By.xpath("//input[@id='loadingTypePSWA']");
	//	public By buttonCarica=By.xpath("//input[@src='/r2d/ele/img/bot_carica.gif']");
	public By buttonCarica=By.xpath("//input[contains(@src,'carica')] | //a[contains(text(),'Carica')]");
	public By buttonCerca=By.xpath("//input[contains(@src,'cerca')] | //a[contains(text(),'Cerca')]");
	//	public By buttonCerca=By.xpath("//input[@src='/r2d/ele/img/bot_cerca.gif']");
	//	public By buttonConferma=By.xpath("//img[@src='/r2d/ele/img/bot_conferma.gif']");
	public By buttonConferma=By.xpath("//img[contains(@src,'conferma')]");
	
	public By inputPOD = By.xpath("//input[@name='pod']");
	public By inputPDR = By.xpath("//input[@name='pdr']");
	public By inputIdRichiestaCRM = By.xpath("//input[@name='codiceRichiestaCRM']");
	public By inputNumeroOrdineCRM = By.xpath("//input[@name='numeroOrdineCrm']");
	public By selectEvento = By.xpath("//select[@name='workTypeId']");
	//	public By RadioEsitoOK = By.xpath("//input[contains(@type,'radio') and contains(@onclick,'OK')]");
	public By RadioEsitoOK = By.xpath("//input[contains(@type,'radio') and @value='Y']");
	public By RadioEsitoKO = By.xpath("//input[contains(@type,'radio') and contains(@onclick,'KO')]");
	public By campoCodiceRichiestaDt= By.xpath("//td[contains(text(),'Codice richiesta dt')]/ancestor::tr[1]/td[2]/input");
	public By campoDataRicezioneEsito= By.xpath("//td[contains(text(),'Data ricezione esito')]/ancestor::tr[1]/td[2]/input");
	public By campoDataRicezioneEsito2= By.xpath("//td[contains(text(),'Data Ricezione Esito')]/ancestor::tr[1]/td[2]/input");
	public By campoCodiceCausale= By.xpath("//td[contains(text(),'Codice causale')]/ancestor::tr[1]/td[2]/input");
	public By campoDettaglioCausaleDT= By.xpath("//td[contains(text(),'Dettaglio Causale DT')]/ancestor::tr[1]/td[2]/input");
	public By campoDataEseuzionePrestazione= By.xpath("//td[contains(text(),'Data esecuzione della prestazione')]/ancestor::tr[1]/td[2]/input");
	public By campoAttualeOpzioneTariffaria= By.xpath("//td[contains(text(),'Attuale opzione tariffaria')]/ancestor::tr[1]/td[2]/select");
	public By campoAttualeTensioneConsegna= By.xpath("//td[contains(text(),'Attuale tensione consegna')]/ancestor::tr[1]/td[2]/input");
	public By campoAttualeTensioneConsegnaPP= By.xpath("//td[contains(text(),'TENSIONE')]/ancestor::tr[1]/td[2]/input");
	public By campoAttualePotenzaDisponibile= By.xpath("//td[contains(text(),'Attuale potenza disponibile')]/ancestor::tr[1]/td[2]/input");
	public By campoAttualePotenzaDisponibilePP= By.xpath("//td[contains(text(),'POTENZA DISPONIBILE')]/ancestor::tr[1]/td[2]/input");
	public By campoPotenzaImpegnataPP=By.xpath("//td[contains(text(),'POTENZA IMPEGNATA')]/ancestor::tr[1]/td[2]/input");//FR 15.09.2021 Nuovo Campo
	public By campoTipoMisuratore= By.xpath("//td[contains(text(),'Tipo misuratore')]/ancestor::tr[1]/td[2]/select");
	public By campoFranchigia= By.xpath("//td[contains(text(),'Franchigia')]/ancestor::tr[1]/td[2]/input");
	public By aggiornamentoEsito=new By.ByXPath("//span[contains(text(),'Aggiornamento effettuato correttamente')]");
	public By statoPraticaAtteso=new By.ByXPath("//td[contains(text(),'Stato Pratica')]/parent::tr/td[2]");
	public By campoDataRicezioneAmmissibilita= By.xpath("//td[contains(text(),'Data ricezione ammissibilit')]/ancestor::tr[1]/td[2]/input");
	public By campoCPGestore= By.xpath("//td[contains(text(),'CP_GESTORE')]/ancestor::tr[1]/td[2]/input");
	public By campoCPUtente= By.xpath("//td[contains(text(),'CP_UTENTE')]/ancestor::tr[1]/td[2]/input");
	public By campoCPGestore_ANN= By.xpath("//td[contains(text(),'CP_GESTORE_ANN')]/ancestor::tr[1]/td[2]/input");
	public By campoVerificaAMM= By.xpath("//td[contains(text(),'VERIFICA_AMM')]/ancestor::tr[1]/td[2]/input");
	public By campoMatricolaContatore= By.xpath("//td[contains(text(),'Matricola Contatore')]/ancestor::tr[1]/td[2]/input");
	public By campoDataEsecuzione= By.xpath("//td[contains(text(),'Data Esecuzione')]/ancestor::tr[1]/td[2]/input");
	public By campoLetturaContatore= By.xpath("//td[contains(text(),'Lettura Contatore')]/ancestor::tr[1]/td[2]/input");
	public By campoAnnoCostruzioneContatore= By.xpath("//td[contains(text(),'Anno costruzione Contatore')]/ancestor::tr[1]/td[2]/input");
	public By selectAccessibilita229 = By.xpath("//td[contains(text(),'Accessibilit')]/ancestor::tr[1]/td[2]/select");
	public By campoCoefficinenteC= By.xpath("//td[contains(text(),'Coefficiente C')]/ancestor::tr[1]/td[2]/input");
	public By selectClasseContatore = By.xpath("//td[contains(text(),'Classe Contatore')]/ancestor::tr[1]/td[2]/select");
	public By selectClasseContatore2 = By.xpath("//td[contains(text(),'Classe contatore')]/ancestor::tr[1]/td[2]/select");
	public By campoStatoContatore = By.xpath("//td[contains(text(),'Stato Contatore')]/ancestor::tr[1]/td[2]/select");
	public By selectEccezioniListino = By.xpath("//td[contains(text(),'Eccezioni Listino')]/ancestor::tr[1]/td[2]/select");
	public By campoCodiceFiscale= By.xpath("//td[contains(text(),'Codice fiscale')]/ancestor::tr[1]/td[2]/input");
	public By campoPartitaIva= By.xpath("//td[contains(text(),'Partita Iva')]/ancestor::tr[1]/td[2]/input");
	public By campoPod= By.xpath("//td[contains(text(),'Pod')]/ancestor::tr[1]/td[2]/input");
	public By campoDataDecorrenza= By.xpath("//td[contains(text(),'Data decorrenza')]/ancestor::tr[1]/td[2]/input");
	public By campoOpzioneTariffaria= By.xpath("//td[contains(text(),'Opzione tariffaria')]/ancestor::tr[1]/td[2]/input");
	public By campoCodiceRichiesta= By.xpath("//td[contains(text(),'Codice richiesta')]/ancestor::tr[1]/td[2]/input");
	public By campoDataRicezioneAmm= By.xpath("//td[contains(text(),'data ricezione amm rich')]/ancestor::tr[1]/td[2]/input");
	public By campoDataEsecuzioneIntervento= By.xpath("//td[contains(text(),'data esec intervento')]/ancestor::tr[1]/td[2]/input");
	public By campoDataEsecuzioneIntervento2= By.xpath("//td[contains(text(),'Data Esecuzione Intervento')]/ancestor::tr[1]/td[2]/input");
	public By campoDataEsecuzioneVerifica= By.xpath("//td[contains(text(),'data esecuzione verifica')]/ancestor::tr[1]/td[2]/input");
	public By selectTipologiaEsito = By.xpath("//td[contains(text(),'Tipologia Esito')]/ancestor::tr[1]/td[2]/select");
	public By selectTipologiaEsito2 = By.xpath("//td[contains(text(),'Tipololgia esito')]/ancestor::tr[1]/td[2]/select");
	public By buttonCaricaFile = By.xpath("//td[contains(text(),'Upload File')]/ancestor::tr[1]/td[2]/ancestor::tr[1]//td/input[contains(@value,'Carica File')]");
	public By buttonCaricaFile2 = By.xpath("//td[contains(text(),'Upload file')]/ancestor::tr[1]/td[2]/ancestor::tr[1]//td/input[contains(@value,'Carica File')]");
	public By buttonScegliFile = By.xpath("//td[contains(text(),'File da caricare')]/ancestor::tr[1]//td/input[contains(@name,'inputFile')]");
	public By buttonScegliFile2 = By.xpath("//td[contains(text(),'File')]/ancestor::tr[1]//td/input[contains(@name,'inputFile')]");
	public By selectTipologiaDocumento= By.xpath("//td[contains(text(),'Tipologia documento')]/ancestor::tr[1]/td[2]/select");
	public By campoUploadFile= By.xpath("//td[contains(text(),'Upload File')]/ancestor::tr[1]/td[2]/input");
	public By campoImportoPreventivo= By.xpath("//td[contains(text(),'Importo preventivo')]/ancestor::tr[1]/td[2]/input");
	public By campoDataScadenzaPreventivo= By.xpath("//td[contains(text(),'Data scadenza preventivo')]/ancestor::tr[1]/td[2]/input");
	public By campoCodicePreventivoDistributore= By.xpath("//td[contains(text(),'Codice Preventivo Distributore')]/ancestor::tr[1]/td[2]/input");
    public By dataCessazionePratica = By.xpath("//td[contains(text(),'Data Cessazione Pratica')]/..//input");
    public By dataCessazioneCRM = By.xpath("//td[contains(text(),'Data Cessazione CRM')]/..//input");
    public By inputForzaStato = By.xpath("//td[contains(text(),'cambia stato')]/..//input");
    public By inputStato = By.xpath("//*[@id='newStato']");
	public By buttonConfermaForzatura=By.xpath("//*[@id='actionForm']/table/tbody/tr[11]/td/table/tbody/tr[2]/td/a");
//	public By buttonConfermaForzaturaGas=By.xpath("//*[@id='actionForm']/table/tbody/tr[12]/td/table/tbody/tr[2]/td/a");
	public By buttonConfermaForzaturaGas=By.xpath("//*[@id='actionForm']//a[@class='linkBtn']");
	public By selectCausaleCRM = By.xpath("//td[contains(text(),'Causale CRM')]/ancestor::tr[1]/td[2]/select");
	
	public By praticaAggiornata=By.xpath("//*[@id='mainBody']/table/tbody/tr/td[2]/div[2]");
	//subentro evo
	public By selectOpzioneTariffaria = By.xpath("//td[contains(text(),'Attuale opzione tariffaria')]/ancestor::tr[1]/td[2]/select");
	public By selectTipoMisuratore= By.xpath("//td[contains(text(),'Tipo misuratore')]/ancestor::tr[1]/td[2]/select");
//  nuovi Claudio 
	public By selectTipologiaContatore = By.xpath("//td[contains(text(),'Cod tipologia pdr')]/ancestor::tr[1]/td[2]/select");
	public By selectProfiloPrelievo = By.xpath("//td[contains(text(),'Profilo di Prelievo ')]/ancestor::tr[1]/td[2]/select");
	public By selectPrelievoAnnuo  = By.xpath("//td[contains(text(),'Prelievo annuo')]/ancestor::tr[1]/td[2]/input");
	public By selectCodiceREMIPOOL  = By.xpath("//td[contains(text(),'Codice REMI-POOL')]/ancestor::tr[1]/td[2]/input");
	public By radioEsitoFA  = By.xpath("//input[contains(@type,'radio') and @value='A']");
	public By radioEsito3OKGas = By.xpath("//*[@id='mainBody']/table/tbody/tr/td[2]/table[2]/tbody/tr[2]/td/form/table/tbody/tr[5]/td[2]/input[3]");
	public By radioEsito5OKGas = By.xpath("//*[@id='mainBody']/table/tbody/tr/td[2]/table[2]/tbody/tr[2]/td/form/table/tbody/tr[5]/td[2]/input[2]");
	public By letturaContatore = By.xpath("//td[contains(text(),'Lettura Contatore')]/ancestor::tr[1]/td[2]/input");
	public By campoTipoLettura = By.xpath("//td[contains(text(),'Tipo lettura SWA')]/ancestor::tr[1]/td[2]/select");
	public By dataSwitch = By.xpath("//td[contains(text(),'Data Switch')]/ancestor::tr[1]/td[2]/input");
	public By cifreContatore = By.xpath("//td[contains(text(),'contatore cifre')]/ancestor::tr[1]/td[2]/input");
	public By dataRicEsito = By.xpath("//td[contains(text(),'Data ricezione esito')]/ancestor::tr[1]/td[2]/input");
	public By progressivoConsumi = By.xpath("//td[contains(text(),'Progressivo Consumi')]/ancestor::tr[1]/td[2]/input");
	public By idCRM = By.xpath("//input[@name='idCrm']");
	public By codCausale = By.xpath("//td[contains(text(),'COD_CAUSALE')]/ancestor::tr[1]/td[2]/input");
	public By motivazione = By.xpath("//td[contains(text(),'MOTIVAZIONE ')]/ancestor::tr[1]/td[2]/input");
	public By dispacciamento = By.xpath("//td[contains(text(),'Contratto di dispacciamento ')]/ancestor::tr[1]/td[2]/input");
	public By RadioPuntualePS = By.xpath("//input[contains(@type,'radio') and @value='GUNB']");
	public By idPraticaFO = By.xpath("//input[@name='idRichiestaCrm']");
	public By buttonCercaPreventivo=By.xpath("//a[@class='linkBtn']");
	public By buttonAzione=By.xpath("//img[@class='pointer']");

	public By referenteTecnico=By.xpath("//input[@id='nomeReferenteTecnicoP']");
	public By referenteTelefono=By.xpath("//input[@id='telefonoTecnicoP']");
	public By livelloTensioneSelect=By.xpath("//select[@id='livelloTensioneP']");
	public By aggiornamentoPreventivo=new By.ByXPath("//div[contains(text(),'Operazione Conclusa con Successo')]");
	
	//nuovi Michele
	public By codiceRichiestaCrmPratica = By.xpath("//table//tr/td/img[@alt='Carica un esito per questa pratica']/ancestor::tr/td[4]");
	public By bisognaCaricarePreventivoBtn = By.xpath("//a[text()='Bisogna caricare preventivo']");
	public By modificaPraticaBtn = By.xpath("//img[@title='Modifica']");
	public By uploadTypeSelect = By.id("tipoCaricamento");
	
	public By dataRicezioneFld = By.id("dataRicezioneP");
	public By numeroPreventivoFld = By.id("numeroP");
	public By dataPreventivoFld = By.id("dataPreventivo");
	public By codiceRintracciabilitaFld = By.id("codiceRintracciabilitaP");
	public By dataScadenzaPreventivoFld = By.id("dataScadenzaP");
	public By tempoMaxEsecuzioneFld = By.id("tempoMassimoEsecLavoriP");
	public By addPriceBtn = By.cssSelector("#trImportoPreventivoSection > td > img");
	public By priceSelect = By.xpath("//select[@name='voceCosto']");
	public By priceInputFld = By.xpath("//input[@name='importoVoce']");
	public By estimateConfirmBtn = By.xpath("//a[text()='Conferma']");
	public By allaccioPdr = By.xpath("//*[text()='PDR ']/..//input");
	public By tariffa = By.xpath("//*[@id='tariffa']");
	
	//PRIMA ATTIVAZIONE FR 15.09.2021 
	public By campoTensioneConsegnaPA= By.xpath("//td[contains(text(),'TENSIONE')]/ancestor::tr[1]/td[2]/input");
	public By campoPotenzaDisponibilePA= By.xpath("//td[contains(text(),'POTENZA DISPONIBILE')]/ancestor::tr[1]/td[2]/input");//FR 15.09.2021
	public By campoTipoMisuratorePA= By.xpath("//td[contains(text(),'TIPOLOGIA_MISURATORE')]/ancestor::tr[1]/td[2]/select");//FR 15.09.2021
	public By campoPotenzaImpegnataPA=By.xpath("//td[contains(text(),'POTENZA IMPEGNATA')]/ancestor::tr[1]/td[2]/input");//FR 15.09.2021 Nuovo Campo
    public By opzioneTariffariaSelect = By.xpath("//td[contains(text(),'CODICE TARIFFA')]/ancestor::tr[1]/td[2]/select");
    //Nuovi path generici per cambio label negli esiti 5OK introditti a settembre 2021
    public By campoTensione= By.xpath("//td[contains(text(),'TENSIONE')]/ancestor::tr[1]/td[2]/input");
    public By campoPotenzaDisponibile= By.xpath("//td[contains(text(),'POTENZA DISPONIBILE')]/ancestor::tr[1]/td[2]/input");//FR 15.09.2021
    public By campoTipologiaMisuratore= By.xpath("//td[contains(text(),'TIPOLOGIA_MISURATORE')]/ancestor::tr[1]/td[2]/select");//FR 15.09.2021
    public By campoPotenzaImpegnata=By.xpath("//td[contains(text(),'POTENZA IMPEGNATA')]/ancestor::tr[1]/td[2]/input");//FR 15.09.2021 Nuovo Campo
	
    
    public By btnCarica = By.xpath("//a[text()='Carica']");
    
    
    
	public R2D_CaricamentoEsitiComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public void selezionaTipoCaricamento(String tipo) throws Exception{
		if(tipo.compareToIgnoreCase("Puntuale PS")==0){
			util.objectManager(RadioTipoPuntualePS,util.scrollAndClick);
		}
		else if(tipo.compareToIgnoreCase("Massivo")==0) {
			util.objectManager(RadiotipoMassivo,util.scrollAndClick);
		}
		else if(tipo.compareToIgnoreCase("Puntuale SWA")==0){
			util.objectManager(RadiotipoPuntualeSWA,util.scrollAndClick);
		}
		else{
			throw new Exception("Tipo Caricamento inserito non ammesso:"+tipo+".Valori ammessi:Puntuale PS o Massivo");
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(buttonCarica,util.click);
		TimeUnit.SECONDS.sleep(3);
	}

	public void selezionaPuntualePS() throws Exception{
			util.objectManager(RadioPuntualePS,util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);
	}

	public void inserisciPod(By inputPOD, String valore) throws Exception{
		util.objectManager(inputPOD,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	public void inserisciPdr(By inputPDR, String valore) throws Exception{
		util.objectManager(inputPDR,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}

	public void inserisciIdRichiestaCRM(By inputIdRichiestaCRM, String valore) throws Exception{
		util.objectManager(inputIdRichiestaCRM,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	public void inserisciDataCessazionePratica(String data) throws Exception{
		util.objectManager(dataCessazionePratica,util.scrollAndSendKeys,data);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void inserisciDataCessazioneCrm(String data) throws Exception{
		util.objectManager(dataCessazioneCRM,util.scrollAndSendKeys,data);
		TimeUnit.SECONDS.sleep(3);
	}

	public void inserisciNumeroOrdineCRM(By inputNumeroOrdineCRM, String valore) throws Exception{
		util.objectManager(inputNumeroOrdineCRM,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}


	public void cercaPod(By buttonCerca) throws Exception{
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
	public void confermaForzatura(By buttonConferma) throws Exception{
//		String frame = util.getFrameByIndex(0);
		util.objectManager(buttonConferma,util.scrollAndClick);
//		driver.switchTo().alert().dismiss();
		driver.switchTo().alert().accept();
//		if(!util.exists(frame,praticaAggiornata,5)) throw new Exception("Non esiste la conferma 'Pratica Aggiornata'");
		if (!driver.findElement(praticaAggiornata).isDisplayed()) throw new Exception("Non esiste la conferma 'Pratica Aggiornata'");
		TimeUnit.SECONDS.sleep(3);
	}

	public void selezionaTastoAzionePratica() throws Exception{
		By	buttonAzione=new By.ByXPath("//table//tr/td/img[@alt='Carica un esito per questa pratica']");
		int risultatoRicerca=driver.findElements(buttonAzione).size();
		Logger.getLogger("").log(Level.INFO, "Risultato ricerca:"+risultatoRicerca);
		if(risultatoRicerca==0){
			throw new Exception("Nessun risultato restituito");
		}
		else if(risultatoRicerca!=1){
			throw new Exception("La ricerca restituisce più di un risultato.Verificare");
		}
		else{
			util.objectManager(buttonAzione,util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);}
	}

	public void selezionaStatoPratica(By inputStato, String stato) throws Exception{
		util.objectManager(inputStato, util.select, stato);

		TimeUnit.SECONDS.sleep(3);
}
	
	public void checkCambiaStato(By inputForzaStato) throws Exception{
			util.objectManager(inputForzaStato,util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);
	}
	
	public void verificaStatoPraticaAtteso(By campoStatoPraticaAtteso,String statoPraticaAtteso) throws Exception{
		String statoPratica=driver.findElement(campoStatoPraticaAtteso).getText();
		Logger.getLogger("").log(Level.INFO, "Esito Pratica:"+statoPratica);
		if(!statoPratica.equals(statoPraticaAtteso)){
			throw new Exception("Lo stato pratica è diverso da quello atteso.Stato pratica:"+statoPratica+". Stato pratica atteso:"+statoPraticaAtteso);
		}

		TimeUnit.SECONDS.sleep(3);
	}

	public void selezioneEvento(By selectEvento,String esito) throws Exception{
		util.objectManager(selectEvento,util.scrollAndSelect,esito);
		TimeUnit.SECONDS.sleep(3);
	}

	public void selezioneEsito(String esito) throws Exception{
		if(esito.compareToIgnoreCase("OK")==0){
			util.objectManager(RadioEsitoOK,util.scrollAndClick);
		}
		else if(esito.compareToIgnoreCase("KO")==0) {
			util.objectManager(RadioEsitoKO,util.scrollAndClick);
		}
		else if(esito.compareToIgnoreCase("FA")==0) {
			util.objectManager(radioEsitoFA,util.scrollAndClick);
		}
		else{
			throw new Exception("Tipo Esito non ammesso:"+esito+".Valori ammessi:Puntuale OK - KO - FA");
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
	}

	public void selezioneEsito3OKGas(String esito) throws Exception{
		if(esito.compareToIgnoreCase("OK")==0){
			util.objectManager(radioEsito3OKGas,util.scrollAndClick);
		}
		else if(esito.compareToIgnoreCase("KO")==0) {
			util.objectManager(RadioEsitoKO,util.scrollAndClick);
		}
		else{
			throw new Exception("Tipo Esito non ammesso:"+esito+".Valori ammessi: Puntuale OK - KO - FA");
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
	}
	

	public void selezioneEsito5OKGas(String esito) throws Exception{
		if(esito.compareToIgnoreCase("OK")==0){
			util.objectManager(radioEsito5OKGas,util.scrollAndClick);
		}
		else if(esito.compareToIgnoreCase("KO")==0) {
			util.objectManager(RadioEsitoKO,util.scrollAndClick);
		}
		else{
			throw new Exception("Tipo Esito non ammesso:"+esito+".Valori ammessi:Puntuale OK - KO - FA");
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
	}
	
	
	public void inserimentoDettaglioEsito3OKPrimaAttivazioneELE(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Sysdate
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito3OKMogeAnagraficaELE(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Sysdate
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito3OKCodiceRichiestaELE(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Sysdate
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito3KOCodiceRichiestaELE(String codiceRichiestaDt,String data,String causale_KO, String codice_causale, String dettaglio_causale_distributore) throws Exception{
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Selezione causale KO
		util.objectManager(selectCausaleCRM,util.scrollAndSelect,causale_KO);
		//Inserimento Sysdate
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento codice_causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,codice_causale);
		//Inserimento dettaglio causale DT
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,dettaglio_causale_distributore);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito3OKPrimaAttivazioneGAS(String data, String codiceCP) throws Exception{
		//Inserimento Data ricezione Ammissibilita
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneAmmissibilita);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		TimeUnit.SECONDS.sleep(3);
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito3OKMogeAnagrafica(String data, String codiceCP) throws Exception{
		//Inserimento Data ricezione Ammissibilita
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneAmmissibilita);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		TimeUnit.SECONDS.sleep(3);
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito3OKconGestoreGAS(String data, String codiceCP) throws Exception{
		//Inserimento Data ricezione Ammissibilita
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneAmmissibilita);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		TimeUnit.SECONDS.sleep(3);
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito3OKDisdettaStandard(String codiceCP) throws Exception{
	
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito3OKDisdettaStandardGas(String cputente,String cpgestore,String gestoreann,String verifica) throws Exception{
		
		util.objectManager(campoCPUtente,util.scrollAndSendKeys,cputente);
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,cpgestore);
		util.objectManager(campoCPGestore_ANN,util.scrollAndSendKeys,gestoreann);
		util.objectManager(campoVerificaAMM,util.scrollAndSendKeys,verifica);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	public void inserimentoDettaglioEsito5OKPrimaAttivazioneELE(String codiceRichiestaDt,String data,String causale, String opzioneTariffaria, String tensione, String potenza, String tipomisuratore, String franchigia) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
//		//Inserimento Codice causale
//		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
//		//Inserimento Dettaglio casuale Dt
//		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(opzioneTariffariaSelect,util.scrollAndSelect,opzioneTariffaria);
		//InserimentoAttuale tensione consegna
		util.objectManager(campoTensioneConsegnaPA,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoPotenzaDisponibilePA,util.scrollAndSendKeys,potenza);
		//Inserimento tipo misuratore
		util.objectManager(campoTipoMisuratorePA,util.scrollAndSelect,tipomisuratore);
		//Inserimento potenza impegnata
		util.objectManager(campoPotenzaImpegnataPA,util.sendKeys,franchigia);
		
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito5OKPrimaAttivazioneELEOld(String codiceRichiestaDt,String data,String causale, String opzioneTariffaria, String tensione, String potenza, String tipomisuratore, String franchigia) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
//		//Inserimento Codice causale
//		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
//		//Inserimento Dettaglio casuale Dt
//		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSelect,opzioneTariffaria);
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento tipo misuratore
		util.objectManager(campoTipoMisuratore,util.scrollAndSelect,tipomisuratore);
		//Inserimento Franchigia
		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito5OKDisdettaconsuggelloGAS(String data,String codiceCP,String lettura,String statoContatore) throws Exception{
		//Inserimento data ricezione esito
				JavascriptExecutor js = (JavascriptExecutor)driver;	
				WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito);
				js.executeScript("arguments[0].value='"+data+"';", elem);
				//Inserimento CP Gestore
				util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);	
				util.objectManager(campoLetturaContatore,util.scrollAndSendKeys,lettura);
				
				WebElement elem2=util.waitAndGetElement(campoDataEsecuzioneIntervento2);
				js.executeScript("arguments[0].value='"+data+"';", elem2);
				
				util.objectManager(campoStatoContatore,util.scrollAndSelect,statoContatore);
				//0 (SIGILLATO)
				//1 (APERTO) 8 (RICOLLOCA) 2 (RIMOSSO) 9 (SPIOMBATURA)
				util.objectManager(buttonCarica,util.scrollAndClick);
				//Gestione pop-up
				accettaPopUp();
				
				util.objectManager(selectEccezioniListino,util.selectFirstElemet,"1");
				util.objectManager(buttonCarica,util.scrollAndClick);
				//Gestione pop-up
				accettaPopUp();
				
				//Verifica avventuta inserimento esito
				if(!util.exists(aggiornamentoEsito, 30)){
					throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
				}
	}
	
	public void inserimentoDettaglioEsito5OKPrimaAttivazioneGAS(String data,String matricolaContatore, String letturaContatore,String annoCostruzioneContatore, String accessibilita229, String coefficienteC, String classeContatore,String statoContatore) throws Exception{
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Matrica Contatore
		util.objectManager(campoMatricolaContatore,util.scrollAndSendKeys,matricolaContatore);
		//Inserimento Data esecuzione Intervento
//		elem=util.waitAndGetElement(campoDataEsecuzione);
		elem=util.waitAndGetElement(campoDataEsecuzioneIntervento2);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Lettura Contatore
		util.objectManager(campoLetturaContatore,util.scrollAndSendKeys,letturaContatore);
		//Inserimento Anno costruzione Contatore
		util.objectManager(campoAnnoCostruzioneContatore,util.scrollAndSendKeys,annoCostruzioneContatore);
		//Inserimento Anno Accessibilità 229
		util.objectManager(selectAccessibilita229,util.scrollAndSelect,accessibilita229);
		//Inserimento Coefficiente C
		util.objectManager(campoCoefficinenteC,util.scrollAndSendKeys,coefficienteC);
		//Classe contatore
		util.objectManager(selectClasseContatore2,util.scrollAndSelect,classeContatore);
//		//Inserimento Stato Contatore
//		util.objectManager(campoStatoContatore,util.scrollAndSelect,statoContatore);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Inserimento Eccezioni Listino-selezione primo valore della lista
		util.objectManager(selectEccezioniListino,util.selectFirstElemet,"1");
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	public void inserimentoDettaglioEsito5OKVolturaSenzaAccolloELE(String codiceRichiestaDt,String data,String causale) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito5OKDisdettaconsuggello(String codiceRichiestaDt,String data,String causale) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito5OKVolturaGAS(String codiceCP,String data) throws Exception{
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);		
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito2);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito5OKMogeGas(String codiceCP,String data) throws Exception{
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);		
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito2);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito5OKconGestoreGAS(String codiceCP,String data) throws Exception{
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);		
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito2);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	
	public void inserimentoDettaglioEsito5OKSubentroGAS(String codiceCP, String data, String matricolaContatore, String letturaContatore, String annoContatore, String coefficenteC, String statoContatore, String classeContatore, String accesibilità, String eccezioniListino) throws Exception {
		//inserimento CP GESTORE
		util.objectManager(campoCPGestore,  util.scrollAndSendKeys, codiceCP);
		//inserimento Data Ricezione
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//inserimento MatricolaContatore
		util.objectManager(campoMatricolaContatore, util.scrollAndSendKeys, matricolaContatore);
		//inserimento lettura contatore
		util.objectManager(campoLetturaContatore, util.scrollAndSendKeys, letturaContatore);
		//inserimento anno contatore
		util.objectManager(campoAnnoCostruzioneContatore, util.scrollAndSendKeys, annoContatore);
		//coefficente C
		util.objectManager(campoCoefficinenteC, util.scrollAndSendKeys, coefficenteC);
		//stato stato contatore
		util.objectManager(campoStatoContatore, util.select, statoContatore);
		//inserimento classe contatore
		util.objectManager(selectClasseContatore2, util.select, classeContatore);
		//inserimento accessibilità
		util.objectManager(selectAccessibilita229, util.select, accesibilità);
		
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		
		util.objectManager(selectEccezioniListino, util.select, eccezioniListino);
		
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		
		
		
		
	}
	
	
	
	
	public void inserimentoDettaglioEsito5OKAllaccioELE(String codiceRichiestaDt,String data, String tipoCliente,String pod, String tensione, String potenza, String tipomisuratore, String franchigia,String opzioneTariffaria) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		//util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		//util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Codice Fiscale o partita Iva
		//util.objectManager(campoCodiceFiscale,util.scrollAndSendKeys,cf_pi);
		//Inserimento POD campoPod
		util.objectManager(campoPod,util.scrollAndSendKeys,pod);
//		//InserimentoAttuale opzione tariffaria
//		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSelect,opzioneTariffaria);
//		//InserimentoAttuale tensione consegna
//		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
//		//InserimentoAttuale potenza disponibile
//		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
//		//Inserimento tipo misuratore
//		util.objectManager(campoTipoMisuratore,util.scrollAndSelect,tipomisuratore);
//		//Inserimento Franchigia
//		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
//		//Inserimento CODICE TARIFFA
		util.objectManager(opzioneTariffariaSelect,util.scrollAndSelect,opzioneTariffaria);
		//Inserimento TENSIONE
		util.objectManager(campoTensione,util.scrollAndSendKeys,tensione);
		//Inserimento POTENZA
		util.objectManager(campoPotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento TIPOLOGIA MISURATORE
		util.objectManager(campoTipologiaMisuratore,util.scrollAndSelect,tipomisuratore);
//		//Inserimento POTENZA IMPEGNATA
		util.objectManager(campoPotenzaImpegnata,util.scrollAndSendKeys,franchigia);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}
	
	
	
	
	public void inserimentoDettaglioEsito5OKAllaccioELE(String codiceRichiestaDt,String data,String pod,String attualeOpzioneTariffaria, String tensione, String potenza, String tipoMisuratore, String opzioneTariffaria) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento POD campoPod
		util.objectManager(campoPod,util.scrollAndSendKeys,pod);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSelect,opzioneTariffaria);
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento Franchigia
//		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSendKeys,opzioneTariffaria);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}
	
	
	
	public void inserimentoDettaglioEsito3OK_SWA_ELE(String data, String cpgestore) throws Exception{
		//campi Cod. Pratica Gestore e Data Ricezione Ammissibilità
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,cpgestore);
		util.objectManager(campoDataRicezioneAmm,util.scrollAndSendKeys,data);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avvenuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	// aggiunta da claudio 03-04-2020 perchè lo script precedente chiamava 3OK_SWA_ELE
	public void inserimentoDettaglioEsito3OK_SWA_GAS(String cpgestore, String ammissibilita) throws Exception{
		//campi Cod. Pratica Gestore, Cod. Pratica Utente e Codice Ammissibilità
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,cpgestore);
		util.objectManager(campoCPUtente,util.scrollAndSendKeys,cpgestore);
		util.objectManager(campoVerificaAMM,util.scrollAndSendKeys,ammissibilita);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avvenuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito5OK_DisdettaStandard_ELE(String data) throws Exception {
		inserisciDataCessazionePratica(data);
		inserisciDataCessazioneCrm(data);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	
	public void inserimentoDettaglioEsito5OK_DisdettaStandard_GAS(String data, String cputente, String cpgestore) throws Exception {
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,cpgestore);
		util.objectManager(campoCPUtente,util.scrollAndSendKeys,cputente);
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataEsecuzioneIntervento2);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		TimeUnit.SECONDS.sleep(3);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito

		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio2 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
		
	}

	public void inserimentoDettaglioEsito5KO_SWA_ELE(String pratica, String codDispacciamento, String dataDecorrenza) throws Exception{
		
		//Inserimento data decorrenza
		util.objectManager(campoDataDecorrenza,util.scrollAndSendKeys,dataDecorrenza);
		//Codice Causale
		util.objectManager(codCausale,util.scrollAndSendKeys,"RESPINTA");
		//Motivazione
		util.objectManager(motivazione,util.scrollAndSendKeys,"RESPINTA");
		//Dispacciamento
		util.objectManager(dispacciamento,util.scrollAndSendKeys,codDispacciamento);
		//Codice Pratica
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,pratica);
		
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	
	public void inserimentoDettaglioEsito5OK_SWA_ELE(String dataDecorrenza,String opzioneTariffaria) throws Exception{
		//Inserimento data decorrenza
		util.objectManager(campoDataDecorrenza,util.scrollAndSendKeys,dataDecorrenza);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoOpzioneTariffaria,util.scrollAndSendKeys,opzioneTariffaria);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	public void inserimentoDettaglioEsitoFA_SWA_GAS(String matricola, String classe, String tipoPdr, String profilo, String prelievo, String codicePool, String cputente) throws Exception{
//		
		//Inserimento Matricola Contatore
		util.objectManager(campoMatricolaContatore,util.scrollAndSendKeys,matricola);
		//Inserimento Classe Contatore
		util.objectManager(selectClasseContatore,util.select,classe.trim());
		//Inserimento Tipo PDR
		util.objectManager(selectTipologiaContatore,util.select,tipoPdr);
		//Inserimento Profilo Contatore
		util.objectManager(selectProfiloPrelievo,util.select,profilo);
		//InserimentoAttuale Prelievo Annuo
		util.objectManager(selectPrelievoAnnuo,util.scrollAndSendKeys,prelievo);
		//InserimentoAttuale Codice Remi Pool
		util.objectManager(selectCodiceREMIPOOL,util.scrollAndSendKeys,codicePool);
		//InserimentoAttuale CP Utente
		util.objectManager(campoCPUtente,util.scrollAndSendKeys,cputente);

		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	public void inserimentoDettaglioEsito5OK_SWA_GAS(String cputente, String lettura, String tipoLettura, String coefficienteC, String dataSWA, String progressivo, String classe, String contatoreCifre, String dataRicezioneEsito,String dataMinimaSW) throws Exception{
		
		//InserimentoAttuale CP Utente
		util.objectManager(campoCPUtente,util.scrollAndSendKeys,cputente);
		//Inserimento Lettura Contatore
		util.objectManager(letturaContatore,util.scrollAndSendKeys,lettura);
		//Inserimento Tipo Lettura Contatore
		util.objectManager(campoTipoLettura,util.select,tipoLettura);
		//Inserimento Coefficinte C
		util.objectManager(campoCoefficinenteC,util.scrollAndSendKeys,coefficienteC);
		// Calcola la data corretta di SWA
		CalcolaDataSWA(dataMinimaSW, dataSWA);
		
		//InserimentoAttuale Prelievo Annuo
		String ggmm = dataSWA.substring(0, 5);
		if(ggmm.compareToIgnoreCase("01/01")==0)
			util.objectManager(progressivoConsumi,util.scrollAndSendKeys,"0");
		 else 
			 util.objectManager(progressivoConsumi,util.scrollAndSendKeys,progressivo);
		//Inserimento Classe Contatore
		util.objectManager(selectClasseContatore,util.select,classe);
		//Inserimento Cifre Contatore
		util.objectManager(cifreContatore,util.scrollAndSendKeys,contatoreCifre);
		//Inserimento Data Ricezione Esito
		util.objectManager(dataRicEsito,util.scrollAndSendKeys,dataRicezioneEsito);
		
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avvenuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void CalcolaDataSWA(String dataMinimaSW, String dataSWA) throws Exception {
		String anno = dataSWA.substring(6, 10);
		if(anno.compareToIgnoreCase("2019")==0)
			util.objectManager(dataSwitch,util.scrollAndSendKeys,dataMinimaSW);
		 else {
				String meseMinimo = dataMinimaSW.substring(3, 5);
				String meseSWA = dataSWA.substring(3, 5);
				int meseMinimoInt = Integer.parseInt(meseMinimo);
				int meseSWAInt = Integer.parseInt(meseSWA);
				if(meseMinimoInt > meseSWAInt)
					util.objectManager(dataSwitch,util.scrollAndSendKeys,dataMinimaSW);
				 else 
					util.objectManager(dataSwitch,util.scrollAndSendKeys,dataSWA);
		 }

	}
	
	public void inserimentoDettaglioEsito5OKSubentroCombinatoELE(String codiceRichiestaDt,String data,String causale,String cf_pi, String tensione, String potenza, String tipomisuratore, String franchigia,String opzioneTariffaria,String classificazionePreventivo,String pod) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento POD campoPod
		util.objectManager(campoPod,util.scrollAndSendKeys,pod);
//		//Inserimento Codice causale
//		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
//		//Inserimento Dettaglio casuale Dt
//		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Codice Fiscale o partita Iva solo per preventivo non predeterminabile
//		if(classificazionePreventivo.compareToIgnoreCase("NON PREDETERMINABILE")==0){
//			util.objectManager(campoCodiceFiscale,util.scrollAndSendKeys,cf_pi);
//		}
//		//Inserimento tipo misuratore
//		util.objectManager(campoTipoMisuratore,util.scrollAndSendKeys,tipomisuratore);
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento Franchigia
		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSendKeys,opzioneTariffaria);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKVolturaConAccolloELE(String codiceRichiestaDt,String data,String causale) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKModificaPotenzaTensionELE(String codiceRichiestaDt,String data,String causale, String tensione, String potenza,String opzioneTariffaria) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Codice Fiscale o partita Iva solo per preventivo non predeterminabile
		//		if(classificazionePreventivo.compareToIgnoreCase("NON PREDETERMINABILE")==0){
		//			util.objectManager(campoCodiceFiscale,util.scrollAndSendKeys,cf_pi);
		//		}

		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//		//InserimentoAttuale opzione tariffaria
		//		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSendKeys,opzioneTariffaria);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKVariazioneIndirizzoFornituraELE(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento data esecuzione intervento
		util.objectManager(campoDataEsecuzioneIntervento,util.scrollAndSendKeys,data);
		//Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}
	
	public void inserimentoDettaglioEsito5OKMogeAnagraficaELE(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento data esecuzione intervento
		util.objectManager(campoDataEsecuzioneIntervento,util.scrollAndSendKeys,data);
		//Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}
	
	public void inserimentoDettaglioEsito5OKCodiceRichiestaEle(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento data esecuzione intervento
		util.objectManager(campoDataEsecuzioneIntervento,util.scrollAndSendKeys,data);
		//Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKVerificaContatoreELE(String codiceRichiestaDt,String data, String tipologiaEsito, String filePath,String tipologiaDocumento) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento data esecuzione verifica
		util.objectManager(campoDataEsecuzioneVerifica,util.scrollAndSendKeys,data);
		//Seleziona Tipologia Esito
		util.objectManager(selectTipologiaEsito,util.select,tipologiaEsito);		
		//Click su Carica File 
		util.objectManager(buttonCaricaFile,util.scrollAndClick);
		//Switch alla nuova pagine
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(7);
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		//Selezione Tipologia Documento
		util.objectManager(selectTipologiaDocumento,util.select,tipologiaDocumento);
		//Caricamento File
		File file = new File(filePath);
		util.objectManager(buttonScegliFile,util.scrollAndSendKeys,file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(5);
		//Click su Carica(documento)
		util.objectManager(buttonCarica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKSpostamentoContatoreEntro10MELE(String codiceRichiestaDt,String data) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}
	
	
	public void inserimentoDettaglioEsito5OKCambioUsoEle(String codiceRichiestaDt,String data,String causale) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKSpostamentoContatoreOltre10MELE(String codiceRichiestaDt,String data,String causale,String cf_pi, String tensione, String potenza, String tipomisuratore, String franchigia,String opzioneTariffaria) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Codice Fiscale o partita Iva
		util.objectManager(campoCodiceFiscale,util.scrollAndSendKeys,cf_pi);
		//Inserimento tipo misuratore
		util.objectManager(campoTipoMisuratore,util.scrollAndSendKeys,tipomisuratore);
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento Franchigia
		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSendKeys,opzioneTariffaria);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}

	public void inserimentoDettaglioEsito5OKVerificaContatoreGAS(String data, String codiceCP,String tipologiaEsito, String filePath,String tipologiaDocumento) throws Exception{
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito2);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Seleziona Tipologia Esito
		util.objectManager(selectTipologiaEsito2,util.select,tipologiaEsito);	
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);
		//Click su Carica File 
		util.objectManager(buttonCaricaFile2,util.scrollAndClick);
		//Switch alla nuova pagine
		String winHandleBefore = driver.getWindowHandle();
		TimeUnit.SECONDS.sleep(7);
		for(String winHandle : driver.getWindowHandles()){
			if(!winHandle.equals(winHandleBefore)) driver.switchTo().window(winHandle);
		}
		//Selezione Tipologia Documento
		util.objectManager(selectTipologiaDocumento,util.select,tipologiaDocumento);
		//Caricamento File
		File file = new File(filePath);
		util.objectManager(buttonScegliFile,util.scrollAndSendKeys,file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(5);
		//Click su Carica(documento)
		util.objectManager(buttonCarica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//Ripristino pagina precedente
		driver.switchTo().window(winHandleBefore);
		//Click su carica(presa visione)
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}

	}
	
	public void inserimentoDettaglioEsito5OKPredisposizionePresaGAS(String codiceCP,String data, String importoPreventivo, String dataPrev, String codicePreventivo, String filePath) throws Exception{
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);		
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento importo preventivo
		util.objectManager(campoImportoPreventivo,util.scrollAndSendKeys,importoPreventivo);
		//Inserimento data scadenza preventivo
		util.objectManager(campoDataScadenzaPreventivo,util.scrollAndSendKeys,dataPrev);
		//Inserimento codice preventivo distrubutore
		util.objectManager(campoCodicePreventivoDistributore,util.scrollAndSendKeys,codicePreventivo);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Caricamento file file
		File file = new File(filePath);
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(buttonScegliFile2,util.scrollAndSendKeys,file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(5);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	public void inserimentoDettaglioEsito5OKPPELE(String codiceCP,String data, String tensione, String potenza, String franchigia) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceCP);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,"EVASA");
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,"EVASA");
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeTensioneConsegnaPP,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibilePP,util.scrollAndSendKeys,potenza);
		//		//Inserimento Franchigia
		//		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//Inserimento Potenza Impegnata
		util.objectManager(campoPotenzaImpegnataPP,util.scrollAndSendKeys,potenza);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsitoPreventivoPPELE(String codiceCP,String data, String importoPreventivo, String dataScadenza, String codicePreventivo, String tipoCaricamento, String livello, String tempoMax, String referente, String telReferente, String voceCosto) throws Exception{
		// Click su Link: Bisogna Caricare Preventivo
		verifyComponentVisibility(bisognaCaricarePreventivoBtn);
		clickComponent(bisognaCaricarePreventivoBtn);
		
		//Inserimento CP Gestore
		util.objectManager(idPraticaFO,util.scrollAndSendKeys,codiceCP);		
		// Bottone Cerca ** Preventivo **
		clickComponent(buttonCercaPreventivo);
		
		// Bottone Azione
		clickComponent(buttonAzione);
		
		// Seleziona Tipo Caricamento
		verifyComponentVisibility(uploadTypeSelect);
		selectItem(uploadTypeSelect, tipoCaricamento);
		
		// Valorizza Campi Preventivo
		verifyComponentVisibility(dataRicezioneFld);
		util.objectManager(dataRicezioneFld, util.scrollAndSendKeys, data);
		verifyComponentVisibility(numeroPreventivoFld);
		util.objectManager(numeroPreventivoFld, util.scrollAndSendKeys, codicePreventivo);
		verifyComponentVisibility(dataPreventivoFld);
		util.objectManager(dataPreventivoFld, util.scrollAndSendKeys, data);
		verifyComponentVisibility(codiceRintracciabilitaFld);
		util.objectManager(codiceRintracciabilitaFld, util.scrollAndSendKeys, codiceCP);
		verifyComponentVisibility(dataScadenzaPreventivoFld);
		util.objectManager(dataScadenzaPreventivoFld, util.scrollAndSendKeys, dataScadenza);
		verifyComponentVisibility(livelloTensioneSelect);
		selectItem(livelloTensioneSelect, livello);
		verifyComponentVisibility(tempoMaxEsecuzioneFld);
		util.objectManager(tempoMaxEsecuzioneFld, util.scrollAndSendKeys, tempoMax);
		verifyComponentVisibility(referenteTecnico);
		util.objectManager(referenteTecnico, util.scrollAndSendKeys, referente);
		verifyComponentVisibility(referenteTelefono);
		util.objectManager(referenteTelefono, util.scrollAndSendKeys, telReferente);
		verifyVisibilityThenClick(addPriceBtn);
		verifyComponentVisibility(priceSelect);
		selectItem(priceSelect, voceCosto);
		verifyComponentVisibility(priceInputFld);
		util.objectManager(priceInputFld, util.scrollAndSendKeys, importoPreventivo);
		verifyVisibilityThenClick(estimateConfirmBtn);
		//Gestione pop-up
		accettaPopUp();
		
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoPreventivo, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito/preventivo. Verificare");
		}
	}

	public void inserimentoDettaglioEsito5OKAllaccioGAS(String codiceCP,String data, String pdr) throws Exception{
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);		
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento PDR
		util.objectManager(allaccioPdr,util.scrollAndSendKeys, pdr);
		TimeUnit.SECONDS.sleep(5);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		
		//Inserimento Tariffa
//		util.objectManager(tariffa,util.scrollAndSendKeys, "25");
//		TimeUnit.SECONDS.sleep(5);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	
		public void inserimentoDettaglioEsito5OKModificaImpiantoGAS(String codiceCP,String data, String importoPreventivo, String dataPrev, String codicePreventivo, String filePath) throws Exception{
		//Inserimento CP Gestore
		util.objectManager(campoCPGestore,util.scrollAndSendKeys,codiceCP);		
		//Inserimento data ricezione esito
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		WebElement elem=util.waitAndGetElement(campoDataRicezioneEsito);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento Data esecuzione
		elem=util.waitAndGetElement(campoDataEsecuzione);
		js.executeScript("arguments[0].value='"+data+"';", elem);
		//Inserimento importo preventivo
		util.objectManager(campoImportoPreventivo,util.scrollAndSendKeys,importoPreventivo);
		//Inserimento data scadenza preventivo
		util.objectManager(campoDataScadenzaPreventivo,util.scrollAndSendKeys,dataPrev);
		//Inserimento codice preventivo distrubutore
		util.objectManager(campoCodicePreventivoDistributore,util.scrollAndSendKeys,codicePreventivo);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Caricamento file file
		File file = new File(filePath);
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(buttonScegliFile2,util.scrollAndSendKeys,file.getAbsolutePath());
		TimeUnit.SECONDS.sleep(5);
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	
	public void inserimentoDettaglioEsito5OKSubentroELE(String codiceRichiestaDt,String data,String causale, String opzioneTariffaria, String tensione, String potenza, String tipomisuratore, String franchigia) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(campoAttualeOpzioneTariffaria,util.scrollAndSendKeys,opzioneTariffaria);
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento tipo misuratore
		util.objectManager(campoTipoMisuratore,util.scrollAndSendKeys,tipomisuratore);
		//Inserimento Franchigia
		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}

	public void inserimentoDettaglioEsito5OKSubentroELE2(String codiceRichiestaDt,String data,String causale, String opzioneTariffaria, String tensione, String potenza, String tipomisuratore, String franchigia) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//InserimentoAttuale opzione tariffaria
		util.objectManager(selectOpzioneTariffaria,util.select,opzioneTariffaria);
		
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento tipo misuratore
		util.objectManager(selectTipoMisuratore,util.select,tipomisuratore);
		
		//Inserimento Franchigia
		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito5OKSubentroELE3(String codiceRichiestaDt,String data,String causale, String opzioneTariffaria, String tensione, String potenza, String tipomisuratore, String franchigia) throws Exception{
		//Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito,util.scrollAndSendKeys,data);
		//Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt,util.scrollAndSendKeys,codiceRichiestaDt);
		//Inserimento Codice causale
		util.objectManager(campoCodiceCausale,util.scrollAndSendKeys,causale);
		//Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT,util.scrollAndSendKeys,causale);
		//Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione,util.scrollAndSendKeys,data);
		//InserimentoAttuale opzione tariffaria
		//util.objectManager(selectOpzioneTariffaria,util.select,opzioneTariffaria);
		
		//InserimentoAttuale tensione consegna
		util.objectManager(campoAttualeTensioneConsegna,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoAttualePotenzaDisponibile,util.scrollAndSendKeys,potenza);
		//Inserimento tipo misuratore
		util.objectManager(selectTipoMisuratore,util.select,tipomisuratore);
		
		//Inserimento Franchigia
		util.objectManager(campoFranchigia,util.scrollAndSendKeys,franchigia);
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
	
	public void inserimentoDettaglioEsito5OKSubentroELE4(String codiceRichiestaDt, String data, String causale,
			String opzioneTariffaria, String tensione, String potenza, String tipomisuratore, String potenzaImpegnata)
			throws Exception {

		// Inserimento data ricezione esito
		util.objectManager(campoDataRicezioneEsito, util.scrollAndSendKeys, data);
		// Inserimento Codice richiesta dt
		util.objectManager(campoCodiceRichiestaDt, util.scrollAndSendKeys, codiceRichiestaDt);
		// Inserimento Codice causale
		util.objectManager(campoCodiceCausale, util.scrollAndSendKeys, causale);
		// Inserimento Dettaglio casuale Dt
		util.objectManager(campoDettaglioCausaleDT, util.scrollAndSendKeys, causale);
		// Inserimento Data esecuzione della prestazione
		util.objectManager(campoDataEseuzionePrestazione, util.scrollAndSendKeys, data);
		// InserimentoAttuale opzione tariffaria
		 util.objectManager(opzioneTariffariaSelect,util.select,opzioneTariffaria);
		
		//InserimentoAttuale tensione consegna
		util.objectManager(campoTensioneConsegnaPA,util.scrollAndSendKeys,tensione);
		//InserimentoAttuale potenza disponibile
		util.objectManager(campoPotenzaDisponibilePA,util.scrollAndSendKeys,potenza);
		//Inserimento tipo misuratore
		util.objectManager(campoTipoMisuratorePA,util.select,tipomisuratore);
		//Inserimento potenza impegnata
		util.objectManager(campoPotenzaImpegnataPA, util.scrollAndSendKeys,potenzaImpegnata);
		
		//Click su carica- Presa visione
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Click su carica- Conferma inserimento
		util.objectManager(buttonCarica,util.scrollAndClick);
		//Gestione pop-up
		accettaPopUp();
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}
		
	
	
	private void accettaPopUp() throws Exception{
		TimeUnit.SECONDS.sleep(5);
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(3);

	}
	
	//----- TC135 ACR methods -----

	public void inserimentoDettaglioEsitoCambioTensione(String codiceRichiestaDt,String data) throws Exception {
		inserimentoDettaglioEsito3OKPrimaAttivazioneELE(codiceRichiestaDt,data);
	}
	
	public void clickComponent(By oggettocliccabile) throws Exception {
		util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
		util.objectManager(oggettocliccabile, util.scrollAndClick);
		//		spinner.checkMainCollaSpinner();
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 20)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void verifyVisibilityThenClick(By locator) throws Exception {
		//A clickable element is necessarily visible
		if (util.verifyClickability(locator, 20)) {
			clickComponent(locator);
		} else {
			throw new Exception("The element located by " + locator + " does not exist or is not clickable.");
		}
	}
	
	public void selectItem(By selectMenu, String item) throws Exception{
		util.objectManager(selectMenu, util.scrollAndSelect, item);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void sendEstimate(String dataRicezione, String numeroPreventivo, String dataPreventivo, String codiceRintracciabilita, String dataScadenza, String tempoMax, String priceOption, String price) throws Exception {
		verifyComponentVisibility(dataRicezioneFld);
		util.objectManager(dataRicezioneFld, util.scrollAndSendKeys, dataRicezione);
		verifyComponentVisibility(numeroPreventivoFld);
		util.objectManager(numeroPreventivoFld, util.scrollAndSendKeys, numeroPreventivo);
		verifyComponentVisibility(dataPreventivoFld);
		util.objectManager(dataPreventivoFld, util.scrollAndSendKeys, dataPreventivo);
		verifyComponentVisibility(codiceRintracciabilitaFld);
		util.objectManager(codiceRintracciabilitaFld, util.scrollAndSendKeys, codiceRintracciabilita);
		verifyComponentVisibility(dataScadenzaPreventivoFld);
		util.objectManager(dataScadenzaPreventivoFld, util.scrollAndSendKeys, dataScadenza);
		verifyComponentVisibility(tempoMaxEsecuzioneFld);
		util.objectManager(tempoMaxEsecuzioneFld, util.scrollAndSendKeys, tempoMax);
		verifyComponentVisibility(tempoMaxEsecuzioneFld);
		util.objectManager(tempoMaxEsecuzioneFld, util.scrollAndSendKeys, tempoMax);
		verifyVisibilityThenClick(addPriceBtn);
		verifyComponentVisibility(priceSelect);
		selectItem(priceSelect, priceOption);
		verifyComponentVisibility(priceInputFld);
		util.objectManager(priceInputFld, util.scrollAndSendKeys, price);
		verifyVisibilityThenClick(estimateConfirmBtn);
		//Gestione pop-up
		accettaPopUp();
	}
}
