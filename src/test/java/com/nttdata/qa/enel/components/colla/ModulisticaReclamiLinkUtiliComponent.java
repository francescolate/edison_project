package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModulisticaReclamiLinkUtiliComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public String pageUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/supporto/faq/come-fare-un-reclamo-enel-energia";
	public By pageTitle = By.xpath("//h1[contains(text(),'Come inviare un')]");
	public By comeFareunreclamoText = By.xpath("//div[@class='text parbase'][1]//section[@class='anchor home-plan_container article-container module--initialized']");
	public By compilailModulo = By.xpath("//a[text()='Compila il modulo']");
	public By pageText = By.xpath("//div[@class='text parbase'][2]");
	public By moduloReclamiTitle = By.xpath("//h1[text()='Modulo Reclami']");
	public String moduloReclamiUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/modulo-reclami-enel-energia-luce-gas.html";
	public By moduloReclamiLuce = By.xpath("//a[contains(text(),'modulo reclami luce')]");
	public String moduloReclamiLuceUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/content/dam/enel-it/documenti-supporto/moduli-reclamo-enel-energia/Modulo%20Reclami_ELE.pdf";
	public By moduloReclamiGas = By.xpath("//a[text()='modulo reclami gas']");
	public String moduloReclamiGasUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/content/dam/enel-it/documenti-supporto/moduli-reclamo-enel-energia/Modulo%20Reclami_GAS.pdf";
	public By moduloReclamiTutelaGas = By.xpath("//a[text()='modulo reclami tutela gas']");
	public String moduloReclamiTutelaGasUrl ="https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/content/dam/enel-it/documenti-supporto/moduli-reclamo-enel-energia/Modulo%20Reclami_TUTELAGAS.pdf"; 
	public By quiLink = By.xpath("//b[text()='qui']");	
	public String quiUrl = "https://www-coll1.enel.it/it/servizi-online/carica-documenti?ta=AP";
	public By negozioEnelLink = By.xpath("//a[text()='Negozio Enel']");
	public String negozioEnelUrl = "https://www-coll1.enel.it/spazio-enel";
	public String headerVoice = "//div[@class='dotcom-header__links dotcom-header__links-custom']//ul//li//a[text()='$VALUE$']";
	public String footerLink = "//a[text()='$Value$']";
	public By enelItalia =By.xpath("//li[contains(text(),'© Enel Italia S.p.a.')]");
	public By enelEnergia = By.xpath("//li[text()='Gruppo IVA Enel P.IVA 15844561009']");
	public By invioDoccumentiLogin = By.xpath("//h3[contains(text(),'Invio documenti: login')]");
	public By email = By.xpath("//input[@id='inputEmail']");
	public By confirmEmail = By.xpath("//input[@id='inputConfermaEmail']");
	public By cfField = By.xpath("//input[@id='inputUsername']");
	public By accediButton = By.xpath("//input[@id='accessoupl']");
	public By campiObbligatori = By.xpath("//input[@id='accessoupl']//following::label[1]");
	public By autenticati = By.xpath("//h5[contains(text(),'Autenticati')]");
	
	public ModulisticaReclamiLinkUtiliComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void pressByJavascript(By oggetto) throws Exception {
		util.pressJavascript(oggetto);
	}
	

	 public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		 public void verifyHeaderVoice(String checkObject,String checkElement) throws Exception{
			 By value = By.xpath(checkObject.replace("$VALUE$", checkElement));	
			 String text = driver.findElement(value).getText();
			/* System.out.println(value);
				 System.out.println(text);*/
				 if(! HeaderVoice.contains(text))
					 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
		 }
		 
		 public void verifyFooterLink(String checkObject,String checkElement) throws Exception{
			 By value = By.xpath(checkObject.replace("$Value$", checkElement));	
			// System.out.println(value);
			 String text = driver.findElement(value).getText();
			//	 System.out.println(text);
				 if(! FooterLink.contains(text))
					 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
		 }
		
		public static final String PageTitle = "Come inviare un reclamo enel energia";
		public static final String ComeFareunreclamoText = "Come fare un reclamoPer inviarci un reclamo compila il modulo online, modalità semplice e veloce che ci consente di ricevere immediatamente la tua richiesta";
		public static final String PageText = "In alternativa puoi scaricare il modulo in pdf, compilarlo e rispedirlo secondo i canali di seguito riportatiScegli e scarica il modulo relativo alla tua fornitura:modulo reclami luce;modulo reclami gas;modulo reclami tutela gas.E poi inviacelo:Via Web: per fare l'upload del modulo che hai scaricato e compilato cliccaqui.Per posta,scrivendo a:Enel Energia S.p.A. Casella Postale 8080 - 85100 - Potenza (Luce);Enel Energia S.p.A. Casella Postale 1000 - 85100 - Potenza (Gas).Se non utilizzi il nostro modulo, la tua comunicazione dovrà contenere i seguenti dati obbligatori:Il tuo nome e cognomeL’indirizzo della fornituraL’indirizzo di recapito (se diverso dall’indirizzo di fornitura) o di posta elettronica dove desideri ricevere la nostra rispostaIl tipo di servizio oggetto del reclamo (elettrico, gas, entrambi)Il codice alfanumerico che identifica il punto di prelievo dell’energia elettrica (POD) o di riconsegna del gas naturale (PDR), oppure il tuo Numero ClienteUna breve descrizione del reclamoSe il reclamo riguarda gli importi e/o una rettifica di fatturazione, è fondamentale indicare anche l’autolettura e la data in cui l’hai effettuata.Prima di inviare un reclamoentra in live chat, un nostro consulente sarà subito a tua disposizione, oppure chiama il Numero Verde gratuito 800 900 860 o recati presso ilNegozio Enela te più vicino.";
		public static final String ModuloReclamiTitle = "Modulo Reclami";
		//public static final String EnelItalia = "© Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.";
		public static final String EnelItalia = "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.";
		public static final String EnelEnergia = "Gruppo IVA Enel P.IVA 15844561009";
		public static final String FooterLink = "Informazioni Legali,Credits,Privacy,Cookie Policy,Remit";
		public static final String HeaderVoice = "LUCE E GAS,IMPRESE,INSIEME A TE,STORIE,FUTUR-E,MEDIA,SUPPORTO";
}
