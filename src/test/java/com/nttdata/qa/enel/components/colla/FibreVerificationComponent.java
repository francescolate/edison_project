package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FibreVerificationComponent extends BaseComponent {
	public By pagePath1 = By.cssSelector("#main > section > div > nav > ul > li:nth-child(1) > a");
	public By pagePath2 = By.cssSelector("#main > section > div > nav > ul > li:nth-child(2) > a");
	public By pageTitle = By.cssSelector("#main > section > div > div > h1");
	public By pageSubtitle = By.cssSelector("#main > section > div > p");
	public By sectionHeader = By.cssSelector("#mainFibra > div > div > h2");
	public By cityField = By.cssSelector("#comune");
	public By addressField = By.cssSelector("#indirizzo");
	public By streetNumberField = By.cssSelector("#numeroCivico");
	public By coverageVerificationBtn = By.xpath("//button[@class='verifyFibra']");
	public By findAddressLink = By.cssSelector("#ctaLead > span");
	public By emptyFieldsError1 = By.xpath("//input[@id='comune']/following-sibling::p[@class='error']");
	public By emptyFieldsError2 = By.xpath("//input[@id='indirizzo']/following-sibling::p[@class='error']");
	public By emptyFieldsError3 = By.xpath("//input[@id='numeroCivico']/following-sibling::p[@class='error']");
	public By cityFirstOption = By.xpath("//ul[contains(@class,'ul-comune')]/li[1]");
	public By streetFirstOption = By.xpath("//ul[contains(@class,'ul-indirizzo')]/li[1]");
	public By streetNumberFirstOption = By.xpath("//ul[contains(@class,'ul-numeroCivico')]/li[1]");
	
	public FibreVerificationComponent(WebDriver driver) {
		super(driver);
	}
	
	public void insertAddress(String city, String street, String streetNumber) throws Exception {
		util.objectManager(cityField, util.sendKeys, city);
		util.objectManager(cityFirstOption, util.click);
		util.objectManager(addressField, util.sendKeys, street);
		util.objectManager(streetFirstOption, util.click);
		util.objectManager(streetNumberField, util.sendKeys, streetNumber);
		util.objectManager(streetNumberFirstOption, util.click);
	}
	
	public void verifyPageStrings() throws Exception {
		By[] locatorsArray = {
				pagePath1, pagePath2, pageTitle, pageSubtitle
		};
		verifyElementsArrayText(locatorsArray, verificationPageStrings);
	}
	
	public void checkEmptyFieldsError() throws Exception {
		verifyVisibilityAndText(emptyFieldsError1, errorMessage);
		verifyVisibilityAndText(emptyFieldsError2, errorMessage);
		verifyVisibilityAndText(emptyFieldsError3, errorMessage);
	}
	
	// ----- Constants -----
	
	private String[] verificationPageStrings = {
			"HOME",
			"FIBRA DI MELITA",
			"Scegli la velocità della fibra di Melita!",
			"Verifica la copertura nel tuo comune e naviga fino a 1 Gigabit/s in download."
	};
	private final String errorMessage = "Si prega di inserire un valore";
}
