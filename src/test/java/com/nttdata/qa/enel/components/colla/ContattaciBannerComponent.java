package com.nttdata.qa.enel.components.colla;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ContattaciBannerComponent extends BaseComponent{

	WebDriver driver;
	SeleniumUtilities util;
	
	public By contattaci = By.xpath("//*[@class='box-pre-chat']");
	public By boxOffer = By.xpath("//*[@class='container-box-chat offer']");
	public By boxSupport = By.xpath("//*[@class='container-box-chat support']");
	public By BoxCallme = By.xpath("//*[@class='container-box-chat callme']");
	public By boxMessage = By.xpath("//*[@class='container-box-chat message']");
	public By telefonoInput = By.id("telefono");
	public By fasciaOraria = By.id("chat-fascia-orariaSelectBoxIt");
	public By richiamami = By.id("richiamami");
	public By successText = By.xpath("//*[@class='success-page-text']");
	
	public String successTextText ="Grazie per aver inoltrato la tua richiesta Abbiamo preso in carico la tua richiesta. Un nostro esperto ti chiamerà al più presto.";

	
	
	public ContattaciBannerComponent(WebDriver driver) throws Exception {
		super(driver);
	}
	
	
	
}
