package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GreenKitComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public String greenKitPageUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/imprese/green-kit";
	public By greenkitPageTitle = By.xpath("//nav[@class='image-hero_breadcrumbs module--initialized']//following::h1[1]");
	public By greenkitSubText = By.xpath("//nav[@class='image-hero_breadcrumbs module--initialized']//following::p[@class='image-hero_detail text--detail']");
	public By richiediiltuoGreenKit = By.xpath("//h2[text()='Richiedi il tuo Green Kit']");
	public By email = By.xpath("//input[@id='form-green-email']");
	public By codiceCliente = By.xpath("//input[@id='form-green-codiceCliente']");
	public By checkBoxText = By.xpath("//label[@class='left']");
	public By textUnderCheckBox = By.xpath("//div[@class='textUnderCheckbox']//sup");
	public By informativaPolicy = By.xpath("//a[text()='Informativa Privacy']");
	public String headerVoice = "//div[@class='dotcom-header__links dotcom-header__links-custom']//ul//li//a[text()='$VALUE$']";
	public By invia = By.xpath("//a[@id='buttonFormGreenKit']");
	public By emailError = By.xpath("//div[@id='errore_form-green-email_ref']//span");
	public By codiceClienteError = By.xpath("//div[@id='errore_form-green-codiceCliente_ref']//span");
	public By checkBoxError = By.xpath("//div[@id='errore_form-green-consent_ref']//span");
	public By checkbox = By.xpath("//input[@type='checkbox']");
	public By popUpTitle = By.xpath("//h1[text()='Hai richiesto Green Kit']");
	public By popUpText = By.xpath("//p[contains(text(),'Richiesta Effettuata')]");
	public By chiudi = By.xpath("//a[text()='Chiudi']");
	public By enelItalia =By.xpath("//li[contains(text(),'© Enel Italia S.p.a.')]");
	public By enelEnergia = By.xpath("//li[text()='Gruppo IVA Enel P.IVA 15844561009']");
	public String footerLink = "//a[text()='$Value$']";
	public By inputPrivacy = By.xpath("//*[@id='consent']");
	
	public GreenKitComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(5000);
			util.objectManager(clickableObject, util.scrollAndClick);			
	}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	 
	 public void verifyHeaderVoice(String checkObject,String checkElement) throws Exception{
		 By value = By.xpath(checkObject.replace("$VALUE$", checkElement));	
		 String text = driver.findElement(value).getText();
		/* System.out.println(value);
			 System.out.println(text);*/
			 if(! HeaderVoice.contains(text))
				 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
	 }
	 
	 public void verifyFooterLink(String checkObject,String checkElement) throws Exception{
		 By value = By.xpath(checkObject.replace("$Value$", checkElement));	
		// System.out.println(value);
		 String text = driver.findElement(value).getText();
		//	 System.out.println(text);
			 if(! FooterLink.contains(text))
				 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
	 }
	 
	 public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	 
	public static final String PageTitle ="Comunica la scelta sostenibile della tua azienda";
	public static final String PageSubText ="Con il nuovo Green Kit dai valore alla sostenibilità in modo innovativo";
	public static final String CheckBoxText = "Ho preso visione dell’Informativa Privacy";
	public static final String TextUnderCheckBox = "Enel Energia, in qualità di Titolare del trattamento, ti informa che i dati personali da te volontariamente forniti sono trattati, anche con l’ausilio di strumenti informatici, esclusivamente al fine di soddisfare la richiesta del “Green Kit”. Il flag per presa visione è obbligatorio per ricontattarti e fornirti il servizio da te richiesto.";
	public static final String HeaderVoice = "LUCE E GAS,IMPRESE,INSIEME A TE,STORIE,FUTUR-E,MEDIA,SUPPORTO";
	public static final String ErrorMsg = "Campo obbligatorio";
	public static final String PopupTitle = "Hai richiesto Green Kit";
	public static final String PopupText = "Richiesta Effettuata con successo. Riceverai a breve una mail con un link per scaricare il Green Kit";
	//public static final String EnelItalia = "© Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.";
	public static final String EnelItalia = "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.";
	public static final String EnelEnergia = "Gruppo IVA Enel P.IVA 15844561009";
	public static final String FooterLink = "Informazioni Legali,Credits,Privacy,Cookie Policy,Remit";

}
