package com.nttdata.qa.enel.components.colla;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DettaglioBollettaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By datiAggiornati = By.xpath("//main/div[@id='datiAggiornati']");
	public By VAIATUTTELEBOLLETTEButton = By.xpath("//a[@class='button_second']");
	public By SCARICAPDFPIANOButton = By.xpath("//a[@class='btnFirstAppearanceMaker']");
	public By gasIcon = By.xpath("//span[@class='icon-line-flame']");
	public By billHeading = By.xpath("//h1[@id='fornitura01']");
	public By addressLabel = By.xpath("//dl[@class='supply-details-heading']/span[1]/dt");
	public By addressvalue = By.xpath("//dl[@class='supply-details-heading']/span[1]/dd");
	public By address = By.xpath("//dl[@class='supply-details-heading']/span[1]/dd/b");
	public By numeroCliente = By.xpath("//dl[@class='supply-details-heading']/span[3]/dd");
	public By numeroClienteLabel = By.xpath("//dl[@class='supply-details-heading']/span[3]/dt");
	public By statoPagamento = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[4]/dt");
	public By statoPagamentoValue = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span[4]/dd");
	public By billDetails = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt");
	public By pagaOnline = By.xpath("");
		
	public DettaglioBollettaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
		
		}
	
	public void verifyBilldetails(By checkObject, String[] exp) throws Exception{
		
		List <WebElement> list  = driver.findElements(billDetails);
			for (WebElement we : list) {
	    	System.out.println(we.getText());
	    	if(!Arrays.asList(exp).contains(we.getText()))
	    		throw new Exception ("This page does not has the following Bill Details:'Periodo','Data Emissione','Data Scadenza','Stato Pagamento','Data Pagamento','Modalità di Pagamento','Attestazione di Pagamento','Offerta Attiva','Importo Rate Canone TV','Importo Da Pagare Canone TV','Importo Bolletta'");
	       	    } 
	}
	
	public void isStatusDaPagare(By checkObject) throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String statusField = driver.findElement(statoPagamento).getText();
		String statusValue = driver.findElement(statoPagamentoValue).getText();
		if(statusValue.contentEquals("Paga Online") && statusField.contentEquals("Stato Pagamento"))
			verifyComponentExistence(pagaOnline);
		else if(!statusValue.contentEquals("Paga Online") && statusField.contentEquals("Stato Pagamento"))
			System.out.println("The Status is not Paga Online");
	
		else
			throw new Exception ("The payment status is invalid");
		
	}
		       
	public boolean isFileDownloaded_Ext(String dirPath, String ext) throws Exception{
        WebDriverWait wait = new WebDriverWait(driver, 10);
        boolean flag=false;
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            flag = false;
        }
       
        for (int i = 1; i < files.length; i++) {
            if(files[i].getName().contains(ext)) {
                flag=true;
            }
        }
        if(!flag)
        	throw new Exception("Impossibile scaricare il target file.");
        
        return flag;
	}
	
	public void deleteDownloadedFile(String dirPath, String filename) throws Exception{
		Thread.sleep(5000);
		File f = new File(dirPath+File.separator+filename);
		if(!f.delete())
			throw new Exception("Impossibile cancellare il target file.");
	}
    	
       
	public static final String BILL_HEADING = "Bolletta N. 3046130615";
	public static final String ADDRESS_LABEL = "L’indirizzo della tua fornitura gas è:";
	public static final String ADDRESS_VALUE = "VIA VENETO 70 - 56040 CASALE M.MO PI";
	public static final String NUMERO_CLIENTE_LABEL = "Numero Cliente:";
	public static final String NUMERO_CLIENTE ="304137911";
	public static final String[] exp = {"Periodo","Data Emissione","Data Scadenza","Stato Pagamento","Data Pagamento","Modalità di Pagamento","Attestazione di Pagamento","Offerta Attiva","Importo Rate Canone TV","Importo Da Pagare Canone TV","Importo Bolletta"};
	public static final String DIR_PATH = System.getProperty("user.home")+File.separator+"Downloads";
	public static final String FILENAME = "Fattura_0000004000007572.pdf";
	public static final String EXT ="PDF";
	public static final String FILENAME161 = "Contratto_a381l0000003D14AAE-in.pdf";
}


