package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DisattivazioneFornituraComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[text()='Disattivazione Fornitura']");
	public By pageText = By.xpath("//div[@class='heading']");
	public By proseguiconladisattivazioneButton = By.xpath("//button[contains(text(),'PROSEGUI CON LA DISATTIVAZIONE')]");
	public By titleSubText = By.xpath("//h2[contains(text(),'Seleziona una o ')]");
	public By podIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='cliente']//span[text()='POD']");
	public By indrizzodellafornituraIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[text()='Indirizzo della fornitura']");
	public By modalitadirichiestaIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[text()='Modalità di richiesta']");
	public By informationIconIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By modalitadirichiestaValueIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='flex dsd']//span[@class='cliente']//span[text()='Numero Verde o Negozio Enel']");
	public By popupTitle = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//h3[text()='Attenzione!']");
	public By popupContent = By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//div[@id='modalAlert']");
	public By cliccaquiLink = By.xpath("//a[text()='CLICCA QUI']");
	public By pageTitle1 = By.xpath("//div[@class='scrollable outerScroller uiScrollerWrapper cITA_IFM_LCP451_SelfCareStyle']//h1[text()='Disattivazione Fornitura']");
	public By path = By.xpath("//ul[@class='breadcrumbs component']");
	public By trovaLoTitle = By.xpath("//span[text()='Trova lo Spazio Enel più vicino a te']");
	public By popupClose = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']//span[@class='dsc-icon-close-rounded']");
	public By domandeFrequenti = By.xpath("//h3[text()='Domande frequenti']");
	
	public By pdr01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='cliente']//span[text()='PDR']");
	public By indrizzodellafornitura01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[text()='Indirizzo della fornitura']");
	public By modalitadirichiesta01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[text()='Modalità di richiesta']");
	public By informationIcon01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By modalitadirichiestaValue01613101028000 = By.xpath("//span[text()='01613101028000']/ancestor::span[@class='flex dsd']//span[text()='Numero Verde o Negozio Enel']");
	
	public By podIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='cliente']//span[text()='POD']");
	public By indrizzodellafornituraIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[text()='Indirizzo della fornitura']");
	public By modalitadirichiestaIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[text()='Modalità di richiesta']");
	public By informationIconIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[@class='icon-info-circle']");
	public By modalitadirichiestaValueIT002E8058051A = By.xpath("//span[text()='IT002E8058051A']/ancestor::span[@class='flex dsd']//span[text()='Numero verde o Negozio Enel']");
	
	public DisattivazioneFornituraComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
 
 	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
 
 	public void waitForElementToDisplay (By checkObject) throws Exception{
		
		WebDriverWait wait = new WebDriverWait (driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
	}
 	
 	public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
 
 	public static final String PageText = "Disattivazione Fornitura La disattivazione della fornitura comporta la chiusura del contatore (è definita tecnicamente “disdetta con suggello”). Viene effettuata sigillando il contatore in modo che non possa essere utilizzato fino a che non venga attivato un nuovo contratto. La richiesta di disattivazione della fornitura rappresenta la manifestazione della volontà di recedere dal contratto di fornitura. Disattiva comodamente la tua fornitura ad uso abitativo dall’area riservata e avrai diritto a un bonus di 25€ per la sottoscrizione di un nuovo contratto con Enel Energia dal sito enel.it entro un anno dalla disattivazione della fornitura."; 
 	public static final String TitleSubText = "Seleziona una o più forniture che intendi disattivare"; 
 	public static final String ModalitadirichiestaValue = "Numero Verde o Negozio Enel";
 	public static final String PopupContent = "Attenzione! Chiudi Siamo spiacenti ma non è possibile procedere con la richiesta. Per maggiori informazioni chiedi al nostro supporto in chat. Per ricevere maggiori informazioni: Contattaci al numero verde 800.900.860, attivo dalle 7.00 alle 22.00 tutti i giorni, dal Lunedì alla Domenica, escluse le festività nazionali Recati al Negozio Enel più vicino a te. CLICCA QUI per scoprire le nostre sedi.";
 	public static final String TrovaText = "Trova lo Spazio Enel più vicino a te";
 	public static final String PopupContent01613101028000 = "Attenzione! Chiudi La fornitura selezionata non può essere disattivata online perché è presente un indirizzo di fornitura errato. Per ricevere maggiori informazioni: Contattaci al numero verde 800.900.860, attivo dalle 7.00 alle 22.00 tutti i giorni, dal Lunedì alla Domenica, escluse le festività nazionali Recati al Negozio Enel più vicino a te. CLICCA QUI per scoprire le nostre sedi.";
 	public static final String ModalitadirichiestaValue1 = "Numero verde o Negozio Enel";
 	public static final String PopupContentIT002E8058051A = "Attenzione! Chiudi La fornitura selezionata non può essere disattivata perché esistono altri processi avviati che risultano incompatibili con la disattivazione. Per ricevere maggiori informazioni: Contattaci al numero verde 800.900.860, attivo dalle 7.00 alle 22.00 tutti i giorni, dal Lunedì alla Domenica, escluse le festività nazionali Recati al Negozio Enel più vicino a te. CLICCA QUI per scoprire le nostre sedi.";

}
