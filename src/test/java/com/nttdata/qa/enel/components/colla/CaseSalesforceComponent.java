package com.nttdata.qa.enel.components.colla;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CaseSalesforceComponent extends BaseComponent{
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By elementoRichiesta = By.xpath("//*[@id='window' and contains(text(),'OI')]//ancestor::a");
	
	
	
	public CaseSalesforceComponent (WebDriver driver) throws Exception{
		super(driver);
	}
	
//    public void compareText(By by, String text, boolean nestedTags) throws Exception{
//		WebElement we = util.waitAndGetElement(by);
//		String weText = we.getAttribute("innerHTML");
//		if(nestedTags)
//			weText = normalizeInnerHTML(weText);
//		else
//			weText = we.getText();
//		System.out.println("Normalized:\n"+weText);
//		System.out.println(text);
//		if(!text.equals(weText))
//			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
//	}
	
	public void compareText(By by, String[] text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		for(String s : text){
			System.out.println(s);
			if(!weText.contains(s))
				throw new Exception("The webpage container does not contain the text '"+s+"'");
		}
	}
	
	public void compareTextDisabledInput(String xpath, String textToCheck) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		if(!s.equals(textToCheck))
			throw new Exception("Il testo cercato:\n\t"+textToCheck+"\nè diverso da quello in pagina:\n\t"+s);
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	
}
