package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class UploadDocumentsComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By text=By.xpath("//section[@id='contenuto_entrypage']//p");
	public By inviaMessaggioButton=By.xpath("//button[@id='modulo_contatti_button' and @aria-label='Invia un messaggio']");
	public String paragrafiditesto []={"Sei già cliente Enel Energia? Registrati subito o accedi alla nostra Area Clienti, potrai gestire le tue forniture in autonomia. Bastano pochi click! Se non sei cliente Enel Energia, scegli l'offerta migliore per te nella sezione LUCE E GAS. Per ogni altra esigenza, puoi mandarci un messaggio.","Se rappresenti un'Associazione dei Consumatori, clicca qui."};
	public String linkCARICA_DOCUMENTI="it/servizi-online/carica-documenti?ta=AP&azione=CON";
	
	public UploadDocumentsComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("oggetto web con xpath " + existingObject + " non esiste.");
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkTest(By checkobject, String [] par) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkobject));
		
		List<WebElement> paragraphList = driver.findElements(checkobject);
		if (paragraphList.size()==par.length) {
			for (int i = 0; i < par.length; i++) { 
				String description=driver.findElement(By.xpath("//section[@id='contenuto_entrypage']//p["+(i+1)+"]")).getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				 if(!description.contentEquals(par[i].trim()))
			       	throw new Exception("sulla pagina web 'it/servizi-online/carica-documenti?ta=AP&azione=CON' non e' presente il paragrafo di testo ricercato: il testo visualizzato e' "+description+"; quello atteso e' "+par[i]);
			     }
		}
		else throw new Exception("il numero dei paragrafi di testo presenti nella pagina 'it/servizi-online/carica-documenti?ta=AP&azione=CON' e' cambiato: i paragrafi di testo attesi sono "+par.length+"; quelle presenti in pagina sono "+paragraphList.size());
	}
	
}
