package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class GestioneRichiestaComponent {

	WebDriver driver;
	public SeleniumUtilities util;
	Actions actions;
	JavascriptExecutor jse;
	SpinnerManager spinner;
	// public By podInput = new
	// By.ByXPath("//label[text()='POD/PDR']/parent::*/div/input");
	public By tableBy = new By.ByXPath("//table[contains(@id,'_Table')]");
	
	public By selezionaAttivita = new By.ByXPath("//button[text()='Seleziona Attività']");

	public By confirmButton = new By.ByXPath("//button[text()='Conferma Selezione Fornitura']");

	public By confermaFornitura = new By.ByXPath("//button[text()='Conferma']");
	
	public By confirmNext = new By.ByXPath("//div[contains(@class,'titleArea') and @role='banner']//button[text()='Conferma']");
	
	public By buttonCreaOfferta=new By.ByXPath("//button[@id='next_outgoingCustomerButton']");
	
	public By inviaCodice = new By.ByXPath("//button[text()='Invia Codice']");
	
	public By continua = new By.ByXPath("//button[text()='Continua']");
	
	public By canaleInvioPick = new By.ByXPath("//label[text()='CANALE INVIO']/../following-sibling::div/select");

	public By tipologiaClientePick = new By.ByXPath("//label[text()='TIPOLOGIA CLIENTE']/../following-sibling::div/select");
	
	public By pecField = new By.ByXPath("//label[text()='PEC']/../following-sibling::div/input");
	
	public By codiceUfficioField = new By.ByXPath("//label[text()='CODICE UFFICIO']/../following-sibling::div/input");
	
	public By inputCercaFornitura=new By.ByXPath("//label[contains(text(),'Cerca Fornitur')]/..//input");
	
	public By inputCercaFornitura2 = new By.ByXPath("//html[contains(@class,'TerminationSupplies')]//input");
	
	public By ChiudiTabAttivita= new By.ByXPath("//button[contains(@title,'Chiudi Attivit')]");
	
	public By serviceCatalog = new By.ByXPath("//button[contains(text(),'Service Catalog')]");  
	
	public By confirmFornitura1 = new By.ByXPath("//div[@class='slds-page-header titleArea'][1]//button[(text()='Conferma')]");
	
	private String intestazioneTabellaCommodity="./thead/tr/th";
	private String rigaTabellaCommodity="./tbody/tr";
	private String rigaTabellaCommodity2="./tbody/tr[%d]";
	private String elemTabellaCommodity="/td[%d]";

	public GestioneRichiestaComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		actions = new Actions(driver);
		jse = (JavascriptExecutor) driver;
		spinner = new SpinnerManager(driver);
	}

	

	public void selezioneCanale_Tipologia(String canaleInvio, String tipologiaCliente) throws Exception {
		WebElement canaleInvioEl = util.waitAndGetElement(canaleInvioPick);
		new Select(canaleInvioEl).selectByVisibleText(canaleInvio);
		WebElement ctipologiaClienteEl = util.waitAndGetElement(tipologiaClientePick);
		new Select(ctipologiaClienteEl).selectByVisibleText(tipologiaCliente);
	}
	
	public void inserisciPec(String pec) throws Exception{
		util.waitAndGetElement(pecField).sendKeys(pec);
	}
	
	public void inserisciCodiceUfficio(String codiceUfficio) throws Exception{
		util.waitAndGetElement(codiceUfficioField).sendKeys(codiceUfficio);
	}
	
    

	public void checkModifySuccess() throws Exception {
		TimeUnit.SECONDS.sleep(7);
		util.waitSpinnerSupplyDataGrid();
		boolean exist = util.checkModalExist();
		if (!exist)
			throw new Exception(
					"Dopo aver selezionato il tipo di commodity e confermato la modifica, non compare a video la presa in carico");
		else {
			String modalMessage = util.getModalDescription();
			if (!modalMessage.contains(
					"presa in carico"))
				throw new Exception(
						"Dopo aver confermato la modifica tipologia bolletta, compare a video la modal dialog con il seguente messaggio non atteso :"
								+ modalMessage);
			else
				util.acceptModalDialog();

		}
	}

	public void checkFilterSupply(String filter) throws Exception {
		util.waitSpinnerSupplyDataGrid();
		boolean exist = util.checkModalExist();
		if (filter.equals("Sintesi") && exist)
			throw new Exception(util.getModalDescription());
		if (filter.equals("Sintesi e Dettaglio")) {
			if (exist)
				util.acceptModalDialog();
			WebElement select = util
					.waitAndGetElement(By.xpath("//label[text()='Filtro Forniture']/following-sibling::div/select"));
			new Select(select).selectByVisibleText("Sintesi e Dettaglio");
			if (util.checkModalExist())
				throw new Exception(util.getModalDescription());

		}
	}

	public void selectCommodity(String commodity,String tipoCheck) throws Exception {
		util.scrollUp();
		WebElement table = util.waitAndGetElement(tableBy);
		util.scrollToElement(table);
		if(util.getTableRowCount(table) < 1) throw new Exception("Impossibile selezionare una commodity, la tabella Forniture risulta vuota");
		int rowNum = util.getTableRowFromColumnData(table, "COMMODITY", commodity);
//		//System.out.println("Riga in tabella numero "+rowNum);
		if(tipoCheck.equals("radiobutton")) util.getTableRowRadioButton(table, rowNum).click();
		else util.getTableRowCheckbox(table, rowNum).click();
		spinner.checkSpinners();
		
	}
	
	
	public void selectCommodity(String commodity,String tipoCheck, int idxFrame) throws Exception {
		String frame = util.getFrameByIndex(idxFrame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame  +"']")
				);
		this.driver.switchTo().frame(frameToSw);

		selectCommodity(commodity,tipoCheck); 
		
		driver.switchTo().defaultContent();
		
	}
	
	
	
	

	
	
	
	
	
	
	public void pressOkButton() throws Exception {
		By button = By.xpath("//span[contains(text(),'Attenzione, la fornitura selezionata')]/../..//button[text()='OK']");
		if(util.exists(button, 15)) {
			util.objectManager(button, util.scrollAndClick);
			spinner.checkSpinners();
		}
		
		
	}
	public void selectCommodity2(By Bytable,String columnName,String coloumnValue,String tipoCheck) throws Exception {
		util.scrollUp();
		WebElement elTabella = util.waitAndGetElement(Bytable);
		util.scrollToElement(elTabella);
		int num=0;
		num = util.getTableRowFromColumnData(elTabella, columnName, coloumnValue, intestazioneTabellaCommodity, rigaTabellaCommodity, elemTabellaCommodity,1);
//		//System.out.println("Riga in tabella numero "+num);
		if(tipoCheck.equals("radiobutton")) util.getTableRowRadioButton(elTabella, num).click();
		else util.getTableRowCheckbox(elTabella, num).click();
		}
	
	public void selectCommodity4(By Bytable,String columnName,String coloumnValue,String tipoCheck) throws Exception {
		util.scrollUp();
		WebElement elTabella = util.waitAndGetElement(Bytable);
		util.scrollToElement(elTabella);
		int num=0;
		num = util.getTableRowFromColumnData(elTabella, columnName, coloumnValue, intestazioneTabellaCommodity, rigaTabellaCommodity, elemTabellaCommodity,1);
		//comm
		System.out.println("Riga in tabella numero "+num);
		if(tipoCheck.equals("radiobutton")) util.getTableRowRadioButton(elTabella, num).click();
		else util.getTableRowCheckbox2(elTabella, num).click();
		}
	
	public void selectCommodity3(By Bytable,String columnName,String coloumnValue,String tipoCheck) throws Exception {
		util.scrollUp();
		WebElement elTabella = util.waitAndGetElement(Bytable);
		util.scrollToElement(elTabella);
		int num=0;
		num = util.getTableRowFromColumnData(elTabella, columnName, coloumnValue, intestazioneTabellaCommodity, rigaTabellaCommodity2, elemTabellaCommodity,1);
//		//System.out.println("Riga in tabella numero "+num);
		
		
		if(tipoCheck.equals("radiobutton")) util.getTableRowRadioButton(elTabella, num).click();
		else util.getTableRowCheckbox(elTabella, num).click();
		}
		
	public void selectCommodity5(By Bytable,String columnName,String coloumnValue,String tipoCheck) throws Exception {
		util.scrollUp();
		WebElement elTabella = util.waitAndGetElement(Bytable);
		util.scrollToElement(elTabella);
		int num=0;
		num = util.getTableRowFromColumnData(elTabella, columnName, coloumnValue, intestazioneTabellaCommodity, rigaTabellaCommodity2, elemTabellaCommodity,1);
//		//System.out.println("Riga in tabella numero "+num);
		
		
		if(tipoCheck.equals("radiobutton")) util.getTableRowRadioButton(elTabella, num).click();
		else util.getTableRowCheckbox3(elTabella, num).click();
		}
	
	
	public void checkAttivabilita(String commodity) throws Exception{
		util.waitSpinnerSupplyDataGrid();
		WebElement table = util.waitAndGetElement(tableBy);
		int rowNum = util.getTableRowFromColumnData(table, "COMMODITY", commodity);
		String att = util.getTableCellData(table, rowNum, "ATTIVABILITÀ");
		if (!att.equals("Attivabile"))
			throw new Exception("Per la commodity " + commodity
					+ " impossibile effettuare l'operazione. Stato attivabilita' :" + att);
		util.scrollDown();
	}
	
	public void checkAttivabilita2(By Bytable,String columnName,String coloumnValue,String commodity) throws Exception{
		util.waitSpinnerSupplyDataGrid();
		WebElement elTabella = util.waitAndGetElement(Bytable);
		int num=0;
		num = util.getTableRowFromColumnData(elTabella, columnName, coloumnValue, intestazioneTabellaCommodity, rigaTabellaCommodity, elemTabellaCommodity,1);
//		//System.out.println("Riga in tabella numero "+num);
		String att = util.getTableCellData(elTabella, num, columnName);
		if (!att.equals(coloumnValue))
			throw new Exception("Per la commodity " + commodity
					+ " impossibile effettuare l'operazione. Stato attivabilita' :" + att);
		util.scrollDown();
	}
	
	
	public void checkAttivabilita4(By Bytable,String columnName,String coloumnValue,String commodity) throws Exception{
		//util.waitSpinnerSupplyDataGrid();
		spinner.checkSpinners();
		WebElement elTabella = util.waitAndGetElement(Bytable);
		int num=0;
		num = util.getTableRowFromColumnData(elTabella, columnName, coloumnValue, intestazioneTabellaCommodity, rigaTabellaCommodity, elemTabellaCommodity,1);
//		//System.out.println("Riga in tabella numero "+num);
		String att = util.getTableCellData2(elTabella, num, columnName);
		if (!att.equals(coloumnValue))
			throw new Exception("Per la commodity " + commodity
					+ " impossibile effettuare l'operazione. Stato attivabilita' :" + att);
		util.scrollDown();
	}
	
	public void cercaFornitura(String frame,By campoFornitura, String valore) throws Exception
	{
		util.frameManager(frame, campoFornitura, util.sendKeys,valore);
		TimeUnit.SECONDS.sleep(10);
	}
	
	public void cercaFornitura(By campoFornitura, String valore) throws Exception
	{
		util.objectManager(campoFornitura, util.sendKeys,valore);
		TimeUnit.SECONDS.sleep(10);
		spinner.checkSpinners();
	}
	
	public void cercaFornitura(int idxFrame,By campoFornitura, String valore) throws Exception
	{
		String frame = util.getFrameByIndex(idxFrame);
		cercaFornitura(frame,campoFornitura, valore);
	}

	public void pressButton(By button) throws Exception {
		WebElement el = util.waitAndGetElement(button);
		util.scrollToElement(el);
		el.click();
		spinner.checkSpinners();
	}
	public void CloseTabAttivita(){
		driver.findElement(ChiudiTabAttivita).click();
	}

}
