package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DiventaNostroPartnerComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[text()='Diventa nostro partner']");
	public By titleSubText = By.xpath("//p[contains(text(),'Entra a far parte')]");
	public By agenzie = By.xpath("//h3[contains(text(),'Agenzie')]");
	public By agenzieText = By.xpath("//h3[contains(text(),'Agenzie')]/following::p[1]");
	public By scopridipiuAgenzie = By.xpath("//h3[contains(text(),'Agenzie')]/following::a[1]");
	public By offerteTitle = By.xpath("//h1[contains(text(),'Offerte di lavoro')]");
	public By offerteSubText = By.xpath("//p[contains(text(),'Per la promozione di')]");
	public By smartAgent = By.xpath("//h3[text()='Smart agent']");
	public By smartAgentSubText = By.xpath("//p[contains(text(),'Per la promozione dei prodotti Enel Energia al mercato consumer e small business')]");
	public By scopridipiuSmartagent = By.xpath("//h3[text()='Smart agent']//following::a[1]");
	public By smartOffereteTitle = By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1");
	public By smartOffereteSubText = By.xpath("//p[contains(text(),'Per la promozione')]");
	public By puntoEnel = By.xpath("//h3[text()='Punto Enel Negozio Partner ']");
	public By puntoEnelSubText = By.xpath("//p[contains(text(),'Per la promozione dei prodotti Enel Energia ai clienti')]");
	public By scopridipiuPuntoenel = By.xpath("//p[contains(text(),'Per la promozione dei prodotti Enel Energia ai clienti')]//following::a[1]");
	public By negozioTitle = By.xpath("//h1[text()='Negozio Enel partner']");
	public By negozioSubText = By.xpath("//p[contains(text(),'Per la promozione')]");
	public String headerVoice = "//div[@class='dotcom-header__links dotcom-header__links-custom']//ul//li//a[text()='$VALUE$']";
	public String footerLink = "//a[text()='$Value$']";
	public By enelItalia =By.xpath("//li[contains(text(),'© Enel Italia S.p.a.')]");
	public By enelEnergia = By.xpath("//li[text()='Gruppo IVA Enel P.IVA 15844561009']");
	public String url = "https://www-coll1.enel.it/it/imprese/diventa-nostro-partner";
	public String scopridipiuAgenzieUrl = "https://www-coll1.enel.it/it/imprese/diventa-nostro-partner/offerte-di-lavoro-per-le-agenzie";
	public String SmartagentUrl = "https://www-coll1.enel.it/it/imprese/diventa-nostro-partner/offerte-di-lavoro-per-agenti-plurimandatari";
	public String PuntoenelUrl = "https://www-coll1.enel.it/it/imprese/diventa-nostro-partner/punto-enel-negozio-partner";
	
	public DiventaNostroPartnerComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
 
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(5000);
			util.objectManager(clickableObject, util.scrollAndClick);		
	}
	
	 public void verifyHeaderVoice(String checkObject,String checkElement) throws Exception{
		 By value = By.xpath(checkObject.replace("$VALUE$", checkElement));	
		 String text = driver.findElement(value).getText();
		/* System.out.println(value);
			 System.out.println(text);*/
			 if(! HeaderVoice.contains(text))
				 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
	 }
	 
	 public void verifyFooterLink(String checkObject,String checkElement) throws Exception{
		 By value = By.xpath(checkObject.replace("$Value$", checkElement));	
		// System.out.println(value);
		 String text = driver.findElement(value).getText();
		//	 System.out.println(text);
			 if(! FooterLink.contains(text))
				 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
	 }
	 
	 public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	public static final String PageTitle = "Diventa nostro partner";
	public static final String SubText = "Entra a far parte della squadra vincente di Enel Energia";
	public static final String AgenzieText = "Per la promozione di offerte luce e gas Enel Energia ai clienti della tua agenzia.";
	public static final String OfferteTitle = "Offerte di lavoro per le agenzie";
	public static final String OfferteSubText = "Per la promozione di offerte luce e gas Enel Energia ai clienti della tua agenzia";
	public static final String SmartAgentSubText = "Per la promozione dei prodotti Enel Energia al mercato consumer e small business.";
	public static final String SmartAgent_OfferteTitle = "Offerte di lavoro per agenti plurimandatari";
	public static final String SmartAgent_OfferteSubText = "Per la promozione dei prodotti Enel Energia al mercato consumer e small business";
	public static final String PuntoEnelSubText = "Per la promozione dei prodotti Enel Energia ai clienti della tua attività commerciale.";
	public static final String NegozioTitle = "Negozio Enel partner";
	public static final String NegozioSubText = "Per la promozione dei prodotti Enel Energia ai clienti della tua attività commerciale";
	public static final String EnelItalia = "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.";
	public static final String EnelEnergia = "Gruppo IVA Enel P.IVA 15844561009";
	public static final String FooterLink = "Informazioni Legali,Credits,Privacy,Cookie Policy,Remit";
	public static final String HeaderVoice = "LUCE E GAS,IMPRESE,INSIEME A TE,STORIE,FUTUR-E,MEDIA,SUPPORTO";

}
