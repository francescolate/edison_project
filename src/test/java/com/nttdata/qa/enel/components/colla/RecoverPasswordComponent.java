package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class RecoverPasswordComponent extends BaseComponent {
	WebDriver driver;
	SeleniumUtilities util;

	public By pageHeader = By.cssSelector("#recovery-password > div.container.content-container.init-content > div.cmPageTitleCont > h1");
	public By pageSubHeader =  By.xpath("//div[@class='cmPageSubtitleCont media'][1]/p[contains(text(),'Inserisci')]");   //By.cssSelector("#recovery-password > div.container.content-container.init-content > div.cmPageSubtitleCont.media > p");
	public By emailField = By.xpath("//input[@id='rec-email']");
	public By sendBtn = By.id("rec-email-button");
	
	public RecoverPasswordComponent(WebDriver driver) {
		super(driver);
	}
	
}
