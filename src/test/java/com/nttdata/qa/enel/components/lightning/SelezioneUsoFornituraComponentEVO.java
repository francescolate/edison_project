package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SelezioneUsoFornituraComponentEVO {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	public By genericConfermaButton = By.xpath("//button[text()='Conferma']");
	
	public SelezioneUsoFornituraComponentEVO(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	public void pressButton(By button) throws Exception {
        util.objectManager(button, util.scrollAndClick);
    }
	
	public void selezionaLigtheningValue(String label, String value) throws Exception{
		util.selectLighningValue(label, value);
		spinner.checkSpinners();
	}

	public void clickComponent(By oggettocliccabile) throws Exception {
		util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
		util.objectManager(oggettocliccabile, util.scrollAndClick);

	}
	
	public void checkSpinnersSFDC() throws Exception{
		spinner.checkSpinners();
	}
}
