package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OffertaEnergiaperCondominioComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[text()='Per i condomini']");
	public By scoprilaNostra = By.xpath("//h2[contains(text(),'Scopri la nostra')]");
	public By servizionline = By.xpath("//h2[contains(text(),'Servizi online')]");
	public String pageUrl = "https://www-coll1.enel.it/it/offerta-energia-per-condominio";
	public By verificaConsumiSubText = By.xpath("//div[@class='module_content parbase large-card']//p[contains(text(),'Richiedi la verifica')]");
	
	public OffertaEnergiaperCondominioComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
	 }
	 public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			/*System.out.println("Normalized:\n"+weText);
			System.out.println(text);*/
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
	public static final String PageTitle = "Per i condomini"; 
	public static final String OnlineService = "Servizi online"; 
	public static final String ScoprilaNostra = "Scopri la nostra proposta per i condomini"; 
	public static final String VerificaConsumi = "Richiedi la verifica dei consumi del tuo condominio";
}
