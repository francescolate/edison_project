package com.nttdata.qa.enel.components.colla;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DuplicatoBolletteComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By homePageHeading1 = By.xpath("//div[@id='mainContentWrapper']//h1[contains(text(),'Benven')]");
	public By homePageParagraph1 = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'In questa')]");
	public By homePageBSNHeading  = By.xpath("//section[@id='tutorialdate']//h1[@class='fix-align']");
	public By homePageHeading2 = By.xpath("//div[@id='mainContentWrapper']//h2[contains(text(),'Le tue forniture')]");
	public By homePageParagraph2 = By.xpath("//div[@id='mainContentWrapper']//p[contains(text(),'Visualizza')]");
	public By servizii = By.xpath("//a[@id='servizi']");
	public By serviziiBSN = By.xpath("//li[@id='menuService']/a");
	public By rateizzazione = By.xpath("//div[@class='single-service'][7]/a");
	public By sectionHeading1 = By.xpath("//section[@id='forniture-id0']//h2[contains(text(),'Servizi per le forniture')]");
	public By sectionParagraph1 = By.xpath("//section[@id='forniture-id0']//p[contains(text(),'Di se')]");
	public By serviziperLeBollette = By.xpath("//section[@id='forniture-id1']/div[@class='section-heading']/h2");
	public By duplicatoBolletta = By.xpath("//section[@id='forniture-id1']/ul[@class='tileWrapper']/li[@class='tile'][6]/a/div[2]");
	public By invioDocumenti = By.xpath("//section[@id='forniture-id2']/ul[@class='tileWrapper']/li[@class='tile'][1]/a/div[2]");
	public By sectionHeading2 = By.xpath("//div[@id='mainContentWrapper']//p[2]");
	public By sectionHeading3 = By.xpath("//div[@id='mainContentWrapper']//p[4]");
	public By invioDocumentititle = By.xpath("//div[@id='mainContentWrapper']/div[@class='text parbase']/div[@class='section-heading']/h2");
	public By invioDocumentiText1 = By.xpath("//div[@id='mainContentWrapper']/div[@class='text parbase']//p[2]");
	public By invioDocumentiText2 = By.xpath("//div[@id='mainContentWrapper']/div[@class='text parbase']//p[4]");
	public By invioDocumentiText3 = By.xpath("//div[@id='mainContentWrapper']/div[@class='text parbase']//p[6]");
	public By vaiAlleBollette = By.xpath("//a/button[@class='button button-first button-center change-tab']");
	public By leTueBolletteHeading = By.xpath("//div[@id='mainContentWrapper']/div[@class='text parbase']/div[@class='heading']/h1");
	public By leTueBolletteText = By.xpath("//div[@id='mainContentWrapper']/div[@class='text parbase']/div[@class='heading']/p[2]");
	public By bollteLefetMenu = By.xpath("//a[@id='bollette']/span[2]");
	public By enelEnergiaSPA = By.xpath("//li[contains(text(),'© Enel Energia S.p.a.')]");
	public By tuttiDiDirittiRiservati = By.xpath("//li[contains(text(),'Tutti i Diritti Riservati')]");
	public By gruppoIVA = By.xpath("//li[contains(text(),'Gruppo IVA Enel P.IVA  15844561009')]");
	public By informazioniLegali = By.xpath("//ul[2]/li[1]/a[@class='href-sf-prevent-default']");
	public By privacy = By.xpath("//ul[2]/li[2]/a[@class='href-sf-prevent-default']");
	public By credits = By.xpath("//ul[2]/li[3]/a[@class='href-sf-prevent-default']");
	public By contattaci = By.xpath("//ul[2]/li[4]/a[@class='href-sf-prevent-default']");
	public By iTuoiDiritti = By.xpath("//a[@id='iTuoiDiritti']");
	public By serviziPerLeContratto = By.xpath("//section[@id='forniture-id2']/div[@class='section-heading']/h2");
	public By email = By.xpath("//input[@id='email-mono']");
	public By emailText = By.xpath("//div[@id='mainContentWrapper']/div[@class='invio-documenti parbase']//p[2]");
	public By proseguiButton = By.xpath("//span[.='PROSEGUI']");
	public By uploadDocumentiTitle = By.xpath("//h3[@class='plan-main-head  active-bills-header']");
	public By uploadDocumentiText = By.xpath("//p[@id='testo_1']");
	public By computer = By.xpath("//div[@id='dropdiv']/h4");
	public By cloudStorage = By.xpath("//div[@class='cloud-storage']/h4");
	public By cloudStorageText = By.xpath("//p[@id='testo_cloud']");
	public By fileinCaricamento = By.xpath("//h5[@id='titolo_h5']");
	public By fileinCaricamentoTExt = By.xpath("//p[@id='messaggio_caution']");
	public By confermaButton = By.xpath("//button[@id='confermaUploadButton']");
	public By annullaButton = By.xpath("//button[@id='annullaUploadButton']");
	public By guidaAllaCompilazione = By.xpath("//a[@id='link_compilazione']");
	public By novita = By.xpath("//a[@id='novitaSelfcare']/span[2]");
	public By novitaTitle = By.xpath("//h2[@id='promo-list-heading']");
	public By novitaText = By.xpath("//div[@id='mainContentWrapper']/div[@class='preposition-group parbase']/div[@class='section-heading']/p");
	public By annullatoTitle = By.xpath("//div[@id='enelupl']//h5");
	public By annullatoTitle2 = By.xpath("//div[@id='enelupl']//h3");
	public By annullatoText = By.xpath("//div[@id='enelupl']//label");
	public By inviaDiNuovoButton = By.xpath("//button[@id='newUploadButton']");
	public By tornaAllaHome = By.xpath("//button[@class='btn-cta--pink ']");
	public By enelLogo = By.xpath("//a/img[@class='logoimg lazyloaded']/@src");
	public By areaRiservata = By.xpath("//li[.='Area riservata']");
	public By homePage = By.xpath("//li[@class='active']");
	public By fornitura = By.xpath("//li[@class='forniture']");
	public By rateizzazioneActive = By.xpath("//li[@class='active']");
	public By rateizzazioneParagraph = By.xpath("//div[@class='panel-body']/p[.='Operazione non riuscita']");
	public By esciButton = By.xpath("//div[@class='panel-body']/div[@class='row-btns']/a[@class='btn btn-primary']");
	public By rateizzazioneTitle = By.xpath("//h1[@id='titolo-dispositiva']");
	public By rateizzazioneText = By.xpath("//div[@id='sc']//div[@class='list-form-container']/h2");
	public By supplyList = By.xpath("//span[@id='checkbox-item-0']");
	public By indirizzoDellaFornitura = By.xpath("//label[@id='row-0-label']/span[@class='flex']/span[@class='indirizzo']/span[@class='label']");
	public By numeroCliente = By.xpath("//label[@id='row-0-label']/span[@class='flex']/span[@class='cliente'][1]/span[@class='label']");
	public By modalitaDiPagamento = By.xpath("//label[@id='row-0-label']/span[@class='flex']/span[@class='cliente'][2]/span[@class='label']");
	public By addebitoDiretto = By.xpath("//label[@id='row-0-label']/span[@class='flex dsd']/span[@class='cliente'][2]/span[@class='valore']");
	public By attenzione = By.xpath("//h3[@id='titleModal']");
	public By popupText = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/p[@class='inner-text']");
	public By popupClose = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By esciButtonRES = By.xpath("//span[.='ESCI']");
	public By avviaRateizzazioneButton  = By.xpath("//span[.='AVVIA RATEIZZAZIONE']");
	public By rettificaBollette = By.xpath("//section[@id='forniture-id1']/ul[@class='tileWrapper']/li[@class='tile zero-right-margin'][1]/a/div[2]/h3");
	public By textInfoIcon = By.xpath("//a[@id='iconLink']/span[@class='icon-info-circle']");
	public By textInfoIconInnerText = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']");
	public By textInfoIconCloseButton = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By rateizzazioneHeading = By.xpath("//h1[@id='titolo-dispositiva']");
	public By rateizzazioneText2 = By.xpath("//div[@id='sc']//div[@class='list-form-container']/h2");
	public By rettificaButton = By.xpath("//span[.='RETTIFICA']");
	
	public DuplicatoBolletteComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
  
  public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
  
  public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
  
  public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
  
  public void rodioIsNotSelected(By existingObject) throws Exception {
	  	if(driver.findElement(existingObject).isSelected())
		  throw new Exception("object with xpath " + existingObject + " is selected.");
  }
  
  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void  isElementNotPresent(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			
			try{
				WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
			} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        		            
	            }
	         }
	 
	 public void  isRadioSelected(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			for(int i=1;i<=3;i++)
			{
			try{
				driver.findElement(By.xpath("//div[@class='form-group-container simply-border']["+ i +"]/fieldset[@class='form-group inline-align'][1]/div[@class='radio-container'][1]/label[@class='label-radio']")).isSelected();
				i++;
				} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        		            
	         }
			}
	      }
	 public void SwitchToWindow() throws Exception
		{
			Set<String> windows = driver.getWindowHandles();
			for (String winHandle : windows) {
			driver.switchTo().window(winHandle);
		  }
		}
	 
	 public void backBrowser(By checkObject) throws Exception {
		    try {
		    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    	driver.navigate().back();
		    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		                
		    } catch (Exception e) {
		        throw new Exception("Previous Page not displayed");
		    }
		}
	 
	 public void isavvivaRateizzazionedisabled(By existentObject) throws Exception{
			if ((!driver.findElement(existentObject).isSelected())&&(driver.findElement(avviaRateizzazioneButton).isEnabled()))
				throw new Exception("The button is Enabled");
			}
	 
public static final String HOME_PAGE_HEADING1 = "Benvenuto nella tua area privata";
public static final String HOME_PAGE_PARAGRAPH1 = "In questa sezione potrai gestire le tue forniture";
public static final String HOME_PAGE_HEADING2 = "Le tue forniture";
public static final String HOME_PAGE_PARAGRAPH2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
public static final String SECTION_PAGE_HEADING1 = "Servizi per le forniture";
public static final String SECTION_PAGE_PARAGPAH1 = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
public static final String SECTION_PAGE_TITLE = "Servizi per le bollette";
public static final String SECTION_PAGE_TITLE3 = "Servizi per il contratto";
public static final String SECTION_PAGE_HEADING2 = "Se ricevi la bolletta cartacea e non ti è stata ancora consegnata, puoi scaricare il PDF direttamente nella sezione delle tue bollette.";
public static final String SECTION_PAGE_HEADING3 = "Ricorda, se ancora non lo hai fatto, che puoi attivare il servizio di Bolletta Web ricevendo la bolletta direttamente al tuo indirizzo email senza dover più attendere i tempi di consegna da parte del vettore postale.";
public static final String LE_TUE_BOLLETTE_HEADING ="Le tue bollette";
public static final String LE_TUE_BOLLETTE_TEXT ="Qui potrai visualizzare bollette, ricevute e/o rate delle tue forniture.";
public static final String ENEL_ENERGIA_SPA = "© Enel Energia S.p.a.";
public static final String TUTTI_I_DIRITTI = "Tutti i Diritti Riservati";
public static final String GRUPO_IVA = "Gruppo IVA Enel P.IVA 15844561009";
public static final String INFORMAZIONI_LEGALI = "Informazioni Legali";
public static final String PRIVACY = "Privacy";
public static final String CREDITS = "Credits";
public static final String CONTATTACI = "Contattaci";
public static final String INVIO_DOCUMENTI_TITLE = "Invio Documenti";
public static final String INVIO_DOCUMENTI_TEXT1 = "Se hai compilato e devi restituire un documento contrattuale, un modulo oppure devi inviarci un documento di identità, potrai farlo comodamente dal tuo PC o dispositivo mobile utilizzando questo servizio.";
public static final String INVIO_DOCUMENTI_TEXT2 = "Dopo aver archiviato il documento nel tuo PC/dispositivo mobile oppure nel tuo cloud storage, sarà sufficiente selezionarlo ed inviarlo.";
public static final String INVIO_DOCUMENTI_TEXT3 = "Di seguito è presente la email su cui verranno inviate le eventuali comunicazioni.";
public static final String EMAIL_TEXT = "Se la tua email non è presente oppure intendi modificarla, puoi accedere alla sezione Account ed aggiornare il tuo contatto.";
public static final String UPLOAD_DOCUMENTI_TITLE ="Upload Documenti";
public static final String UPLOAD_DOCUMENTI_TEXT ="In questa pagina puoi caricare i tuoi documenti. Clicca qui per visualizzare l'elenco dei documenti che puoi caricare ATTENZIONE. Ricorda di allegare ai tuoi documenti una copia, fronte/retro, di un documento di identita' valido. Se desideri trasmettere il pagamento di una delle nostre bollette, accedi all'area privata, sezione \"bollette scadute\".";
public static final String ClOUD_STORAGE_TEXT = "Cloud Storage";
public static final String FILE_IN_CARICAMENTO = "File in caricamento";
public static final String FILE_IN_CARICAMENTO_TEXT = "Puoi caricare i documenti solo in formato pdf o jpg.ATTENZIONE: La dimensione del file non deve superare 10 MB.";
public static final String GUIDA_ALLA_COMPILAZIONE =  "Guidaalla compilazioneIstanze";
public static final String NOVITA_TITLE = "Novità";
public static final String NOVITA_TEXT = "In questa sezione potrai visualizzare tutte le offerte e i vantaggi offerti da Enel Energia";
public static final String ANNULLATO_TITLE = "Invio documenti: esito operazione";
public static final String ANNULLATO_TITLE2 = "Invio documenti: annullato";
public static final String ANNULLATO_TEXT = "Caricamento Annullato.";
public static final String AREA_RISERVATA = "Area riservata";
public static final String HOME_PAGE = "HomePage";
public static final String HOMEPAGE_BSN_HEADING = "//section[@id='tutorialdate']//h1[@class='fix-align']";
public static final String FORNITURE = "fornitura";
public static final String RATEIZZAZIONE = "Rateizzazione";
public static final String RATEIZZAZIONE_PARAGRAPH = "Operazione non riuscita";
public static final String RATEIZZAZIONE_TEXT = "Seleziona la fornitura sulla quali intendi avviare la rateizzazione. Puoi effetturare questa operazione solo su una fornitura alla volta.";
public static final String INDIRIZZO_DELLA_FORNITURA = "Indirizzo della fornitura";
public static final String NUMERO_CLIENTE = "Numero Cliente";
public static final String MODALITO_PAGAMENTO = "Modalità di Pagamento";
public static final String ADDEBITO_DIRETTO = "Addebito Diretto";
public static final String ATTENZIONE = "Attenzione!";
public static final String POPUP_TEXT = "Siamo spiacenti, ma al momento non è possibile procedere con la rateizzazione delle bollette. Sulla tua fornitura è attiva la modalità di pagamento con addebito diretto.Per chiedere la rateizzazione è necessario:- Contattare la tua banca e bloccare il pagamento delle fatture, inserendo nella causale \"Contestazione Cliente\".- Contattare il nostro supporto in chat.";
public static final String TEXTINFOICON_INNERTEXT ="";
public static final String RATEIZZAZIONE_TEXT2 = "Stai avviando un piano di rateizzazione sulla tua fornitura GAS in VIA ARCHIMEDE 57, 95018, CATANIA, RIPOSTO, CT.";


}

