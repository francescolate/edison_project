package com.nttdata.qa.enel.components.r2d;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_SimulatoreFlussoSIIComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public R2D_SimulatoreFlussoSIIComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	public By inputServizio = By.xpath("//input[@name='servizio']");
	public By inputFlusso = By.xpath("//input[@name='flusso']");
	public By inputPOD = By.xpath("//input[@name='pod']");
	public By buttonCerca = By.xpath("//img[@alt='Cerca'] | //*[contains(text(),'Cerca')]");
	public By inputStatoFornitura = By.xpath("//td[text()='stato fornitura ']/ancestor::tr[1]//input");
	public By buttonCarica= By.xpath("//img[@alt='Carica'] | //a[contains(text(),'Carica')]");

	// ELE
	public By inputPIvaVenditore = By.xpath("//input[@name='C9040']");
	public By inputPIvaGestore = By.xpath("//input[@name='C9050']");
	public By inputCPVenditore = By.xpath("//input[@name='C9060']");
	public By inputCPGestore = By.xpath("//input[@name='C9160']");
	public By inputVerificaAmm = By.xpath("//input[@name='C9070']");
	public By inputEsito = By.xpath("//input[@name='C9100']");
	public By inputCodiceEsito = By.xpath("//input[@name='C9110']");
	public By inputDettaglioEsito = By.xpath("//input[@name='C9120']");
	public By inputCF = By.xpath("//input[@name='C9130']");
	public By inputPIVA = By.xpath("//input[@name='C9140']");
	public By inputCFStraniero = By.xpath("//input[@name='C9150']");
	public By inputPIVAContrComm = By.xpath("//input[@name='C1496']");
	public By inputRagSocContrComm = By.xpath("//input[@name='C1497']");
	public By inputEmailContrComm = By.xpath("//input[@name='C1498']");
	public By inputPotMediaAnnua = By.xpath("//input[@name='C9341']");
	
	// GAS - 
	public By inputPIvaUtenteGas = By.xpath("//input[@name='C775']");
	public By inputPIvaGestoreGas = By.xpath("//input[@name='C776']");
	public By inputCPUtenteGas = By.xpath("//input[@name='C777']");
	public By inputCPGestoreGas = By.xpath("//input[@name='C778']");
	public By inputVerificaAmmGas = By.xpath("//input[@name='C779']");
	public By inputEsitoGas = By.xpath("//input[@name='C784']");
	public By inputCodiceEsitoGas = By.xpath("//input[@name='C785']");
	public By inputDettaglioEsitoGas = By.xpath("//input[@name='C786']");
	public By inputTipoPdrGas = By.xpath("//input[@name='C1290']");
	public By inputCodiceRemiGas = By.xpath("//input[@name='C782']");
	public By inputCFGas = By.xpath("//input[@name='C787']");
	public By inputPIVAGas = By.xpath("//input[@name='C788']");
	public By inputCFStranieroGas = By.xpath("//input[@name='C789']");
	public By inputDenominazioneGas = By.xpath("//input[@name='C792']");
	public By inputPIVADistributoreGas = By.xpath("//input[@name='C1291']");
	public By inputRagSocContrCommGas = By.xpath("//input[@name='C1292']");
	public By inputPIVAContrCommGas = By.xpath("//input[@name='C1293']");
	public By inputPecContrCommGas = By.xpath("//input[@name='C1294']");
	
	public By verificaConfigurazione = By.xpath("//*[@id='mainBody']/table/tbody/tr/td[2]/div[2]");
	

	public void cercaFlusso(String servizio,String flusso, String pod) throws Exception{
		//Selezione Servizio
//		util.objectManager(inputServizio,util.scrollAndSelectByValue,servizio);
		util.objectManager(inputServizio,util.scrollAndSendKeys,servizio);
		TimeUnit.SECONDS.sleep(2);
		//Selezione Flusso
		util.objectManager(inputFlusso,util.scrollAndSendKeys,flusso);
		TimeUnit.SECONDS.sleep(2);
		//Inserimento POD
		util.objectManager(inputPOD,util.scrollAndSendKeys,pod);
		TimeUnit.SECONDS.sleep(2);
		//Click su cerca
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void configuraSimulatoreFlussoPK1(String PIvaVenditore,String CPVenditore,String CPGestore,String VerificaAmm,String esito,String CodiceEsito,
			String DettaglioEsito,String CodiceFiscale, String PIVA, String CFStraniero,String PIVAContrComm,String RagSocContrComm,String EmailContrComm, 
			String PotMediaAnnua, String TipologiaCliente) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		//Selezione PIvaVenditore
		util.objectManager(inputPIvaVenditore,util.scrollAndSendKeys,PIvaVenditore);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CPVenditore
		util.objectManager(inputCPVenditore,util.scrollAndSendKeys,CPVenditore);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CPGestore
		util.objectManager(inputCPGestore,util.scrollAndSendKeys,CPGestore);
		TimeUnit.SECONDS.sleep(1);
		//Selezione VerificaAmm
		util.objectManager(inputVerificaAmm,util.scrollAndSendKeys,VerificaAmm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione esito
		util.objectManager(inputEsito,util.scrollAndSendKeys,esito);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CodiceEsito
		util.objectManager(inputCodiceEsito,util.scrollAndSendKeys,CodiceEsito);
		TimeUnit.SECONDS.sleep(1);
		//Selezione DettaglioEsito
		util.objectManager(inputDettaglioEsito,util.scrollAndSendKeys,DettaglioEsito);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CodiceFiscale
		if(TipologiaCliente.contentEquals("Residenziale")){
			util.objectManager(inputCF,util.scrollAndSendKeys,CodiceFiscale);
		} else {
			util.objectManager(inputPIVA,util.scrollAndSendKeys,CodiceFiscale);
		}
		TimeUnit.SECONDS.sleep(1);
		//Selezione CFStraniero
		util.objectManager(inputCFStraniero,util.scrollAndSendKeys,CFStraniero);
		TimeUnit.SECONDS.sleep(1);
		//Selezione PIVAContrComm
		util.objectManager(inputPIVAContrComm,util.scrollAndSendKeys,PIVAContrComm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione RagSocContrComm
		util.objectManager(inputRagSocContrComm,util.scrollAndSendKeys,RagSocContrComm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione EmailContrComm
		util.objectManager(inputEmailContrComm,util.scrollAndSendKeys,EmailContrComm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione PotMediaAnnua
		util.objectManager(inputPotMediaAnnua,util.scrollAndSendKeys,PotMediaAnnua);
		TimeUnit.SECONDS.sleep(1);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		driver.switchTo().alert().accept();
	}

	public void configuraSimulatoreFlussoPKG1(String PIvaUtente, String PIvaGestore, String CPUtente, String CPGestore, String VerificaAmm,String esito,String CodiceEsito,
			String DettaglioEsito, String TipoPdr, String CodiceFiscale, String CodiceRemi, String CFStraniero,String PIVAContrComm,
			String RagSocContrComm,String PecContrComm, String Denominazione, String pIvaDistributore, String TipologiaCliente) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		//Selezione PIvaUtenteGas
		util.objectManager(inputPIvaUtenteGas,util.scrollAndSendKeys,PIvaUtente);
		TimeUnit.SECONDS.sleep(1);
		//Selezione PIvaGestoreGas
		util.objectManager(inputPIvaGestoreGas,util.scrollAndSendKeys,PIvaGestore);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CPUtenteGas
		util.objectManager(inputCPUtenteGas,util.scrollAndSendKeys,CPUtente);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CPGestoreGas
		util.objectManager(inputCPGestoreGas,util.scrollAndSendKeys,CPGestore);
		TimeUnit.SECONDS.sleep(1);
		//Selezione VerificaAmmGas
		util.objectManager(inputVerificaAmmGas,util.scrollAndSendKeys,VerificaAmm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione esitoGas
		util.objectManager(inputEsitoGas,util.scrollAndSendKeys,esito);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CodiceEsitoGas
		util.objectManager(inputCodiceEsitoGas,util.scrollAndSendKeys,CodiceEsito);
		TimeUnit.SECONDS.sleep(1);
		//Selezione DettaglioEsitoGas
		util.objectManager(inputDettaglioEsitoGas,util.scrollAndSendKeys,DettaglioEsito);
		TimeUnit.SECONDS.sleep(1);
		//Selezione TipoPdrGas
		util.objectManager(inputTipoPdrGas,util.scrollAndSendKeys,TipoPdr);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CodiceRemiGas
		util.objectManager(inputCodiceRemiGas,util.scrollAndSendKeys,CodiceRemi);
		TimeUnit.SECONDS.sleep(1);
		//Selezione CodiceFiscale o PIVA
		if(TipologiaCliente.contentEquals("Residenziale")){
			util.objectManager(inputCFGas,util.scrollAndSendKeys,CodiceFiscale);
		} else {
			util.objectManager(inputPIVAGas,util.scrollAndSendKeys,CodiceFiscale);
		}
		TimeUnit.SECONDS.sleep(1);
		//Selezione CFStranieroGas
		util.objectManager(inputCFStranieroGas,util.scrollAndSendKeys,CFStraniero);
		TimeUnit.SECONDS.sleep(1);
		//Selezione DenominazioneGas
		util.objectManager(inputDenominazioneGas,util.scrollAndSendKeys,Denominazione);
		TimeUnit.SECONDS.sleep(1);
		//Selezione PIVADistributoreGas
		util.objectManager(inputPIVADistributoreGas,util.scrollAndSendKeys,pIvaDistributore);
		TimeUnit.SECONDS.sleep(1);
		//Selezione RagSocContrCommGas
		util.objectManager(inputRagSocContrCommGas,util.scrollAndSendKeys,RagSocContrComm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione PIVAContrCommGas
		util.objectManager(inputPIVAContrCommGas,util.scrollAndSendKeys,PIVAContrComm);
		TimeUnit.SECONDS.sleep(1);
		//Selezione PecContrCommGas
		util.objectManager(inputPecContrCommGas,util.scrollAndSendKeys,PecContrComm);
		TimeUnit.SECONDS.sleep(1);
		//Click su carica
		util.objectManager(buttonCarica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		driver.switchTo().alert().accept();
	}

	public void verificaConfigurazioneFlussoSII() throws Exception{
	
		WebElement el = driver.findElement(verificaConfigurazione);
		String testo = el.getText();
		
		if(!testo.contentEquals("Configurazione effettuata correttamente")) {
			throw new Exception("Attenzione! Configurazione FlussoSII PK1 non effettuata correttamente.");
		} else {
			System.out.println("Configurazione Flusso SII  effettuata correttamente.");
		}

	}

}
