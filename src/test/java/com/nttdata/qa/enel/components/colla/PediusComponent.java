package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PediusComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By pediusPath=By.xpath("//nav[@class='hero_breadcrumbs']//ol[@class='breadcrumbs component']");
    public By pediusTitle=By.xpath("//h1[@class='hero_title text--page-heading' and text()='Enel attiva Pedius, l’app per le persone sorde']");
    public By pediusSubtitle=By.xpath("//p[@class='hero_date text--date' and text()='Pubblicato il giovedì, 5 aprile 2018 ']");
	
	public PediusComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("oggetto web con xpath " + existingObject + " non esiste.");
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkHeaderPagePedius(By path,By title, By subtitle) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(path));
		String pathDescription=driver.findElement(path).getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	
			//System.out.println(pathDescription);	
		if (!pathDescription.contentEquals("HOME MEDIA ENEL ATTIVA PEDIUS, L’APP PER LE PERSONE SORDE"))
			throw new Exception("sulla pagina web 'it/megamenu/media/news/2018/04/enel-lancia-app-pedius-per-comunicazione-non-udenti' l'oggetto web con xpath " + path + " non esiste");
		if (!util.exists(title, 15))
			throw new Exception("sulla pagina web 'it/megamenu/media/news/2018/04/enel-lancia-app-pedius-per-comunicazione-non-udenti' il titolo con xpath " + title + " non esiste");
		if (!util.exists(subtitle, 15))
			throw new Exception("sulla pagina web 'it/megamenu/media/news/2018/04/enel-lancia-app-pedius-per-comunicazione-non-udenti' il sottotitolo " + subtitle + " non esiste");	
	}
}
