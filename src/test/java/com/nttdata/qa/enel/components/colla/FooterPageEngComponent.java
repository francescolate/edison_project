package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class FooterPageEngComponent {
	WebDriver driver;
	SeleniumUtilities util;

	public By footerEnelBusinessDetails = By.xpath("//ul[@class='footer-copyright']");
	public String footerEnelBusinessDetailsText = "Enel Italia S.p.a. | © Enel Energia S.p.a. Gruppo IVA Enel VAT 15844561009";
	
	public By legalDesclaimers = By.xpath("//li[@class='footer-legal-item']//a[text()='Legal disclaimers']");
	public By credits = By.xpath("//li[@class='footer-legal-item']//a[text()='Credits']");
	public By privacy = By.xpath("//li[@class='footer-legal-item']//a[text()='Privacy']");
	public By cookiePolicy = By.xpath("//li[@class='footer-legal-item']//a[text()='Cookie Policy']");
	public By cookiePreferences = By.xpath("//li[@class='footer-legal-item']//a[text()='Cookie Preferences']");
	public By remit = By.xpath("//li[@class='footer-legal-item']//a[text()='Remit']");
	
	private List<By> footerItems;
	
	public FooterPageEngComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		
		this.footerItems = new ArrayList<By>();
		this.footerItems.add(legalDesclaimers);
		this.footerItems.add(credits);
		this.footerItems.add(privacy);
		this.footerItems.add(cookiePolicy);
		this.footerItems.add(cookiePreferences);
		this.footerItems.add(remit);
		
		util = new SeleniumUtilities(this.driver);
	}

	public void checkTextFooter(By Object1, String beforetext,By Object2, String aftertext) throws Exception {
		String obj1Text = driver.findElement(Object1).getText();
		String obj2Text = driver.findElement(Object2).getText();
		
		if (!obj1Text.contentEquals(beforetext) && !obj2Text.contentEquals(aftertext) )
			 throw new Exception("on the footer of page is not present le descriptions expected: before description is "+obj1Text+";expected before description is "+beforetext+";after description is "+obj2Text+";expected after description is "+aftertext);
		
	}

	public void checkFooterLegalLinksByTexts(By links_object, String[] texts) throws Exception {
		List<WebElement> objs = util.waitAndGetElements(links_object);
		int i = -1;
		if (objs.size() != texts.length) 
			throw new Exception("the total numer of legal links are not correct excepted '" + texts[i].toString() + "; total links present '" + objs.size());
		for(WebElement obj  : objs) {
			i++;
			if(!obj.getText().equalsIgnoreCase(texts[i])) {
				throw new Exception("the link '" + texts[i].toString() + " is not present in the legal link footer");
			}
		}
	}

	public void checkFooterSocialIconsByAttibuteText(By links_object, String[] texts , String attribute) throws Exception {
		List<WebElement> objs = util.waitAndGetElements(links_object);
		int i = -1;
		if (objs.size() != texts.length) 
			throw new Exception("the total numer of social icons are not correct : excepted '" + texts[i].toString() + "; total social icons present '" + objs.size());
		for(WebElement obj  : objs) {
			i++;
			if(!obj.getAttribute(attribute).equalsIgnoreCase(texts[i])) {
				throw new Exception("the social icon '" + texts[i].toString() + " is not present in the footer");
			}
		}
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}	
	
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyFooterItemsExistence() throws Exception{
		for(By by: this.footerItems)
			this.verifyComponentExistence(by);
	}
}
