package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class GestionePreventivoComponent extends BaseComponent{
	WebDriver driver ;
	SeleniumUtilities util;
	SpinnerManager spinner;
	//	public final By finalButton = By.id("j_id0:formId:btnConfirmFinal");
	//	public final By finalButton2 = By.xpath("//div[@id='j_id0:formId:footerId']//input[@value='CONFERMA']");
	//	public final By finalButton3 = By.id("j_id0:principalForm:finalConfirm");
	//	public final By finalButton4 = By.xpath("//button[text()='Continua'][1]");
	//	public final By buttonContinuaVolturaAccollo = By.xpath("//input[@value='Conferma']");
		public final By inputClassificazionePreventivo =By.xpath("//input[@name='ClassificazionePreventivo']");
	//	public final By inputClassificazionePreventivo2 = By.xpath("//span[contains(@id,'quoteClass:')]");
	//	public final By inputClassificazionePreventivo3 = By.xpath("//label[text()='Classificazione Preventivo']/ancestor::div[1]//input");
	//	public final By selectCanaleInvioRichiesta = By.xpath("//select[@class='slds-input' and contains(@id,'quoteDataContainer')]");
		public final By selectStatoRichiestaPreventivo = By.xpath("//span[text()='Stato Richiesta']/ancestor::div[1]//select");
	//	public final By selectStatoRichiestaPreventivo2 = By.xpath("//select[contains(@id,'quoteRequests:')]");
	//	public final By selectStatoRichiestaPreventivo3 = By.xpath("//label[text()='Stato Ordine']/ancestor::div[1]//select");

	//	final By confermaPreventivoBtn = By.id("j_id0:formId:confirmFooterBtn");
	//	public final By confermaPreventivoBtn2=By.xpath("//button[text()='Conferma Preventivo'] | //input[@value='CONFERMA PREVENTIVO'] | //button[contains(text(),'Conferma')]");
	//	public final By confermaPreventivoBtn3=By.xpath("//input[@value='CONFERMA PREVENTIVO']");
	//	final By tornaAllOfferta = By.xpath("//button[@id='confirmHeaderBtn']");
	//	public final By verificaCvp = By.xpath("//div[@id='smartScriptKoCvpModalWithButtons']//button");
	//	final By inviaPreventivo = By.xpath("//button[@id='buttonSendPreventivo']");
	//	final By modalOkPreventivo = By.xpath("//button[@id='theModalCmpBtn_A']");
	//	final By buttonCalcolaPreventivo =  By.xpath("//button[normalize-space(text())='Calcola']");
	//	final By calcolaCodiceUfficioButton = By.xpath("//button[@name='calculateButton']");
	//	final By okButtonFatturazioneElettronica = By.xpath("//button[@title='OK']");
	//	final By nextButtonFatturazioneElettronica = By.xpath("//button[@name='nextButton']");
	//	public final By paginaGestionePreventivo=By.xpath("//div[contains(@id,'Dettaglio Preventivo')]");
	public final By tabellaRiepilogoOrdini = By.xpath("//table[@title='Tabella Riepilogo ordini']");
	//	public final By colonnaPreventivo=By.xpath("//table[@title='Tabella Riepilogo ordini']/tbody/tr/td[8]");
	public final By confermaPreventivo=By.xpath("//button[contains(text(),'Conferma')]");
	public final By testoDettaglioPreventivo = By.xpath("//div[@title='Dettaglio Preventivo']/ancestor::div[1]//lightning-formatted-rich-text/span");
	public final By linkGestionePreventivo=By.xpath("//a[contains(@id,'preventivoPageLink')]");
	public final By popInformativaPreventivo=By.xpath("//h2[text()='Attenzione']");
	public final By buttonInformativaPreventivoOK=By.xpath("//h2[text()='Attenzione']/ancestor::div[@class='slds-modal__container']//button[@aria-label='OK']");
	public final String dettaglioPreventivoNonPredeterminabile="Trattasi di richiesta con preventivo NON PREDETERMINABILE. La richiesta di preventivo è stata inoltrata al distributore, che invierà  un tecnico per il sopralluogo e conseguentemente stima del preventivo";
	public final String dettaglioPreventivoPredeterminabile="Il preventivo è stato classificato come PREDETERMINABILE";
	public int baseTimeoutInterval = 20;

	public GestionePreventivoComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util=new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public void verificaDettaglioPreventivoNonPredetarminabile(By obj,String testoAtteso) throws Exception{
		util.objectManager(obj, util.scrollElement);
		util.waitAndGetElement(obj);
//		String testo=driver.findElement(obj).getText();
		String testo=util.getElementText(obj);
		Logger.getLogger("").log(Level.INFO,testoAtteso);
		Logger.getLogger("").log(Level.INFO,testo);
		if(testo.compareTo(testoAtteso)!=0){
			throw new Exception("Il testo visualizzato nella sezione dettaglio preventivo non Ã¨ quello atteso.Testo atteso:"+testoAtteso+" mentre quello mostrato a video Ã¨:"+testo);
		}
	}
	
	public void verificaDettaglioPreventivoPredeterminabile(By obj,String testoAtteso) throws Exception{
		util.objectManager(obj, util.scrollElement);
		util.waitAndGetElement(obj);
//		String testo=driver.findElement(obj).getText();
		String testo=util.getElementText(obj);
		Logger.getLogger("").log(Level.INFO,testoAtteso);
		Logger.getLogger("").log(Level.INFO,testo);
			if(testoAtteso.contains(testo)){
			throw new Exception("Il testo visualizzato nella sezione dettaglio non corrisponde alla classificazione del preventivo.Testo atteso:"+testoAtteso+" mentre quello mostrato a video è:"+testo);
		}
	}
	
	
	public void verificaCampiDatiRichiesta(String statoRichiestaAtteso, String classifPreventivoAtteso, String emissioneFatturaAtteso) throws Exception{
		String statoRichiesta = util.waitAndGetElement(By.xpath("//span[text()='Stato Richiesta']/following::select"),true).getAttribute("value");
		//		String addebitoBolletta = util.waitAndGetElement(By.xpath("//span[text()='Addebito In Bolletta']/following::input"),true).getAttribute("value");
		String classificazionePreventivo = util.waitAndGetElement(By.xpath("//label[contains(text(),'Classificazione Preventivo')]/following::input"),true).getAttribute("value");
		String emissioneFatturaAnticipata = util.waitAndGetElement(By.xpath("//label[text()='Emissione Fattura Anticipata']/following::input"),true).getAttribute("value");

//		System.out.println(statoRichiesta);
		if(statoRichiesta.compareTo(statoRichiestaAtteso)!=0)  throw new Exception("Stato richiesta risulta diverso dal valore "+statoRichiestaAtteso);
		//				System.out.println(addebitoBolletta);
		//		if((addebitoBolletta.length()>0))  throw new Exception("Addebito non risulta erroneamente popolato ");
		verifyInputFieldExist("Addebito In Bolletta");
//		System.out.println(classificazionePreventivo);
		if(classificazionePreventivo.compareTo(classifPreventivoAtteso)!=0) throw new Exception("Classificazione preventivo risulta diverso dal valore"+classifPreventivoAtteso);
//		System.out.println(emissioneFatturaAnticipata);
		if(emissioneFatturaAnticipata.compareTo(emissioneFatturaAtteso)!=0) throw new Exception("Emissione Fattura Anticipata risulta diverso dal valore"+emissioneFatturaAtteso);



	}

	public void verificaCampiPreventivoAllaccioAttivazioneNonPredeterminabile(String totaleVociAtteso) throws Exception{
		String totaleVoci = util.waitAndGetElement(By.xpath("//label[text()='Totale Voci']/following::input"),true).getAttribute("value");
		verifyInputFieldExist("Data Scadenza");
//		System.out.println(totaleVoci);
		if(totaleVoci.compareTo(totaleVociAtteso)!=0)  throw new Exception("Totale Voci risulta diverso dal valore "+totaleVociAtteso);
		verifyInputFieldExist("Note Distributore");
		verifyInputFieldExist("Tempi");
		verifyInputFieldExist("Tecnico");
		verifyInputFieldExist("Totale");
		verifyInputFieldExist("Codice Rintracciabilità");
		verifyInputFieldExist("Note");

	}
	
	public void verificaCampiPreventivoAllaccioAttivazionePredeterminabile() throws Exception{
		verifyInputFieldExist("Data Scadenza");
		verifyInputFieldExist("Totale Voci");
		verifyInputFieldExist("Note Distributore");
		verifyInputFieldExist("Tempi");
		verifyInputFieldExist("Tecnico");
		verifyInputFieldExist("Totale");
		verifyInputFieldExist("Codice Rintracciabilità");
		verifyInputFieldExist("Note");

	}

	public void verifyInputFieldExist(String fieldName) throws Exception{
		if(!util.verifyExistence(By.xpath("//label[contains(text(),'"+fieldName+"')]/following::input[1] | //span[contains(text(),'"+fieldName+"')]/following::input[1]"), baseTimeoutInterval)) 
			throw new Exception("Impossibile trovare il campo di input "+fieldName);
	}


	//	public void selezionaCanaleInvioPreventivo(String canaleInvio) throws Exception{
	//		String frame = util.getFrameByIndex(1);
	//		Logger.getLogger("").log(Level.INFO,frame);
	//		util.frameManager(frame, selectCanaleInvioRichiesta, util.select, canaleInvio);
	//		spinner.checkSpinners(frame);
	//	}

	//	public void confermaPreventivo(By finalb) throws Exception {
	//		driver.switchTo().defaultContent();
	//		String frame = util.getFrameByIndex(1);
	//		Logger.getLogger("").log(Level.INFO,frame);
	//		WebDriverWait wait = new WebDriverWait(driver, 120);
	//		spinner.checkSpinners(frame);
	//		util.frameManager(frame, finalb, util.scrollAndClick);
	//		spinner.checkSpinners(frame);
	//		//Verifica CVP
	//		Logger.getLogger("").log(Level.INFO,"Ricerca popup CVP");
	//		verificaPopUpCVP(frame,verificaCvp);
	//		
	//		By disapperingFrame = By.xpath("//iframe[@title ='accessibility title' "
	//				+ "and @name ='" + frame +"']");
	//		
	////		try { 
	////			util.frameManager(frame, verificaCvp, util.scrollAndClick);
	////			util.frameManager(frame, finalButton, util.scrollAndClick);
	////		} catch(Exception e) {}
	//
	//		
	//
	//
	//		//System.out.println("waitin for " + disapperingFrame );
	//		wait.until(
	//				ExpectedConditions.not(
	//						ExpectedConditions.presenceOfAllElementsLocatedBy(disapperingFrame)
	//						)
	//				);
	//		//System.out.println("frame not found " + disapperingFrame );
	//		// spinner.waitForSpinnerByStyleDiplayNone(frame,spinner.tinySpinner);
	//
	//	}

	//	public void confermaPreventivo(String frame, By finalb) throws Exception {
	//		spinner.checkSpinners(frame);
	//		Logger.getLogger("").log(Level.INFO,frame);
	//		util.frameManager(frame, finalb, util.scrollAndClick);
	//		spinner.checkSpinners(frame);
	//		

	//	}


	//	public void confermaPreventivoNoFrame(By finalb) throws Exception {
	//		spinner.checkSpinners();
	//		TimeUnit.SECONDS.sleep(5);
	//		util.objectManager(finalb, util.scrollAndClick);
	//		spinner.checkSpinners();
	//	}
	//	public void confermaPreventivoInFrame(String frame,By finalb) throws Exception {
	//		spinner.checkSpinners();
	//		TimeUnit.SECONDS.sleep(5);
	//		util.frameManager(frame, finalb, util.scrollAndClick);
	//		spinner.checkSpinners();
	//	}
	//	
	//	public void confermaPreventivo2() throws Exception{
	//		String frame = util.getFrameByIndex(1);
	//		Logger.getLogger("").log(Level.INFO,frame);
	//		util.frameManager(frame, tornaAllOfferta, util.scrollAndClick);
	//	}
	//	
	//	public void confermaPreventivo2(String frame) throws Exception{
	//		Logger.getLogger("").log(Level.INFO,frame);
	//		util.frameManager(frame, tornaAllOfferta, util.scrollAndClick);
	//	}
	//
	//	public void inviaPreventivo(String predeterminabile) throws Exception {
	//		By fattEle = By.xpath("//span[contains(@title,'Fatturazione Elettronica')]");
	//	
	////		if (util.exists(fattEle, 30)){
	//		if (util.exists(fattEle, 30)&& driver.findElement(fattEle).isDisplayed()){
	//		util.waitAndGetElement(calcolaCodiceUfficioButton).click();
	//		spinner.checkSpinners();
	//		util.waitAndGetElement(okButtonFatturazioneElettronica).click();
	//		util.waitAndGetElement(nextButtonFatturazioneElettronica).click();
	//		spinner.checkSpinners();
	//		}
	//		try {
	//			
	//		util.waitAndGetElement(modalOkPreventivo).click();
	//		}
	//		catch (Exception e) {
	//			if (util.exists(By.id("errorBannerMex"),5)) {
	//				String error = driver.findElement(By.id("errorBannerMex")).getText();
	//				throw new Exception("Impossibile generare il preventivo, errore generato : " + error);
	//			}
	//		}
	//		TimeUnit.SECONDS.sleep(2);
	//		util.scrollAndClick.accept(
	//		util.waitAndGetElement(inviaPreventivo));
	//		TimeUnit.SECONDS.sleep(2);
	//		spinner.checkSpinners();
	////		if (util.exists(By.id("theModalCmpBtn_A"),5))
	//		if (util.exists(By.id("theModalCmpBtn_A"),15) && driver.findElement(By.id("theModalCmpBtn_A")).isDisplayed())
	//			util.objectManager(modalOkPreventivo,util.scrollAndClick);
	////			throw new Exception("La richiesta di preventivo è terminata in modo anomalo. testo della pop-up :"+
	////					"La richiesta ha ricevuto la seguente rilavorazione: \"NA_Il codice POD non esiste o non corretto\". "
	////					+ "Si prega di rilavorare l’attività di scarto"
	////					);
	//		if (predeterminabile.equalsIgnoreCase("y"))
	//		util.objectManager(
	//				By.xpath("//button[@class='slds-button slds-button_brand' and contains(text(),'Conferma' )]"),
	//				util.scrollAndClick);
	//		spinner.checkSpinners();
	//	}

	public void verificaClassificazionePreventivo(String preventivoAtteso, By inputClassificazionePreventivo) throws Exception{
		if(!util.exists(inputClassificazionePreventivo, 60)){
			throw new Exception("La pagina di dettaglio preventivo non è stata visualizzata, controllare dati input(cliente e/o indirizzo di esecuzione lavori)");
		}

		String dettaglioPreventivo=driver.findElement(inputClassificazionePreventivo)
				.getAttribute("value");
		if(dettaglioPreventivo == null) dettaglioPreventivo = driver.findElement(inputClassificazionePreventivo).getText();
		//		//System.out.println("Valore campo2:"+dettaglioPreventivo);
		if (preventivoAtteso.compareToIgnoreCase(dettaglioPreventivo)!=0){
			throw new Exception("Il preventivo risulta :"+dettaglioPreventivo+". Preventivo atteso:"+preventivoAtteso);
		}
	}
	
	public void accettaInformativa(By inputClassificazionePreventivo, By buttonConferma) throws Exception{
		if(util.exists(inputClassificazionePreventivo, 60) && util.driver.findElement(inputClassificazionePreventivo).isDisplayed()){
			util.objectManager(buttonConferma, util.scrollAndClick);
			spinner.checkSpinners();
		}

	}

		public void selezionaRichiestaPreventivo(By selectStatoRichiesta, String valore) throws Exception{
			util.objectManager(selectStatoRichiesta, util.scrollAndSelect,valore);
		}
		public void selezionaRichiestaPreventivoInFrame(String frame,By selectStatoRichiesta, String valore) throws Exception{
			util.frameManager(frame, selectStatoRichiesta, util.scrollAndSelect, valore);
		}
		
		public boolean verificaCaricamentoGestionePreventivo(By paginaGestionePreventivo) throws Exception{
			boolean flag=false;
			if(util.exists(paginaGestionePreventivo, 40)&& driver.findElement(paginaGestionePreventivo).isDisplayed()){
				flag=true;
				Logger.getLogger("").log(Level.INFO,"Pagina Gestione Preventivo Trovata");
			}
			else{
			Logger.getLogger("").log(Level.INFO,"Pagina Gestione Preventivo NON trovata");
			}
			return flag;
		}

	public void apriGestionePreventivoDaRiepilogoOrdini(By tabellaRiepilogoOrdini, By linkPaginaPreventivo, int index) throws Exception{
		try{
			TimeUnit.SECONDS.sleep(3);
//			String frame = util.getFrameByIndex(index);
//			WebElement frameToSw = driver.findElement(
//					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
//					);
//
//			driver.switchTo().frame(frameToSw);
			util.getFrameActive();
			WebElement elTabella = util.waitAndGetElement(tabellaRiepilogoOrdini);
			util.scrollToElement(elTabella);
			String valoreColonnaPreventivo=util.getTableCellText(elTabella, 1, "Preventivo", "./thead/tr/th", "./tbody/tr", "/td[%d]",0);		
			Logger.getLogger("").log(Level.INFO,"Valore colonna preventivo:"+valoreColonnaPreventivo);
//					System.out.println("Valore colonna preventivo:"+valoreColonnaPreventivo);
			if (valoreColonnaPreventivo.compareToIgnoreCase("Gestione Preventivo")==0){
				util.objectManager(linkPaginaPreventivo, util.scrollAndClick);
				spinner.checkSpinners();
			}
			else{
				throw new Exception("Nel Riepologo ordini, la colonna 'Preventivo' risulta essere vuota. Preventivo non ancora generato.");
			}
		}
		catch(Exception E){
			Logger.getLogger("").log(Level.INFO,"Riepilogo Ordine non visualizzato");
		}
		driver.switchTo().defaultContent();
	}



	//	public String verificaClassificazionePreventivoInFrame(String preventivoAtteso, By inputClassificazionePreventivo, int index) throws Exception{
	//		    String frame = util.getFrameByIndex(index);
	//		    WebElement frameToSw = driver.findElement(
	//					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
	//					);
	//			driver.switchTo().frame(frameToSw);
	//		    verificaClassificazionePreventivo(preventivoAtteso, inputClassificazionePreventivo);
	//		    driver.switchTo().defaultContent();
	//		    return frame;
	//	}
	//	
	//	public void verificaPopUpCVP(String frame, By buttonOK) throws Exception{
	//		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//		try{
	//			WebElement frameToSw = driver.findElement(
	//					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
	//					);
	//			driver.switchTo().frame(frameToSw);
	//		if (util.exists(buttonOK, 20)&& driver.findElement(buttonOK).isDisplayed()){
	//			util.objectManager(buttonOK, util.scrollAndClick);
	//			Logger.getLogger("").log(Level.INFO,"Popup CVP trovata");
	//			TimeUnit.SECONDS.sleep(3);}
	//		else{
	//			Logger.getLogger("").log(Level.INFO,"Popup CVP NON trovata");
	//		}
	//		driver.switchTo().defaultContent();
	//		}
	//		catch (Exception e) {
	//			Logger.getLogger("").log(Level.INFO,"Popup CVP NON trovata");
	//		};
	//		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	//	}




}
