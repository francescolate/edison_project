package com.nttdata.qa.enel.components.colla;

import java.awt.List;
//import java.util.List;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.support.ui.Select;

public class CompletaTP2Component {

	WebDriver driver;
	SeleniumUtilities util;

//		public By checkboxTrattamenti=By.xpath("//input[@id='ITA_IFM_Type_Legal_Form__c']");
//		public By checkboxTrattamenti=By.xpath("//*[@id='ITA_IFM_Type_Legal_Form__c']");
	public By checkboxTrattamenti = By.xpath("//input[@id='ITA_IFM_Type_Legal_Form__c']");
	public By annulla = By.xpath("//button[@id='tp-cancel-data']");
	public By conferma = By.xpath("//button[@id='tp-confirm-data']");
	public By menuTendina = By.xpath("//span[@id='selectAnnullamentoOffertaSelectBoxItText']");
	public By valoreMenu = By.xpath("//ul[@id='selectAnnullamentoOffertaSelectBoxItOptions']");
	public By ConfAnnulla = By.xpath("//button[@id='annullaOfferta']");
	public By sezioneModalitaSottoscrizione = By
			.xpath("//h3[contains(text(),'Modalit')and contains(text(),'di Sottoscrizione')]");
	public By modalitaSottoscrizioneSi = By.xpath("//input[@id='Yes-Question-1']");
	public By pressoSpazioEnelPartner = By.xpath("//input[@id='SpaziEnelPartner']");
	public By confermaButtonTp2old = By.id("buttonNextTP4");
	public By confermaButtonTp2 = By.id("tp-dlcasa-data-next");
	public By proseguiButtonTp2 = By.id("confirmTP4");
	public By radioButtonTp2 = By.id("tp-titoloProp");
	public By radioButtonAbitazioneTp2 = By.xpath("//input[@id='tp-subhome']");
	public By confermaButton2tp2 = By.id("tp-dlcasa-data-next");
	public By uploadDocument = By.id("upload-doc");
	public By inputCaricaDoc = By.xpath("//label[@class='btn full-btn tp-up']");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public final By CCEsitoChiamata = By.xpath("(//div[@class='modal-body']/ancestor::div//select)[2]");
	public final By listaAnnulla = By.xpath("//span[@id='selectAnnullamentoOffertaSelectBoxIt']");
	public final By listaSceltaAnnulla = By.xpath("//span[@id='selectAnnullamentoOffertaSelectBoxItText']");
	public final By selectCheck = By.xpath("//option[text()='Ho cambiato idea']/..");
	public final By inviaRichiestaFibra = By.id("confirmFiber");
	public By radioButtonTp2xpath = By.xpath("input[@id='tp-titoloProp']");
	public By acceptCookie = By.xpath("//button[@id='truste-consent-button']");
	public By buttonCaricaDocumentoRiconoscimento=By.xpath("//span[text()='Inserisci qui il tuo documento di riconoscimento']/ancestor::div[@class='upload-block']//label[@for='upload-doc']");
	
	public By labelAbitazione = By.xpath("//div[text()='abitazione']");
	public By labelProprieta = By.xpath("//div[text()='Proprietà/Usufrutto/Abitazione per decesso del convivente di fatto']");
	
	public By invioDocumenti = By.xpath("//button[text()='Invio documenti']");
	// Gestione Messaggio Ops qualcosa non ha funzionato
	public By messaggioOps = By.xpath("//*[contains(text(),'Ops')]");
	public By chiudiOps = By.xpath("//*[contains(text(),'Ops')]/../..//button");
	
	
	public CompletaTP2Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);

	}

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}

	public void jsClickObject(By by) throws Exception {
		util.objectManager(by, util.scrollToVisibility, false);
		util.jsClickElement(by);
	}

	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
			util.objectManager(existObject, util.scrollAndClick);
		}
	}

	// **** Select Opzione:
	public void selectOption(By oggetto, By val) throws Exception {
		Thread.currentThread().sleep(20000);
		boolean flag = false;
		WebElement element = null;
		element = driver.findElement(oggetto);
		flag = true;
		if (!flag) {
			throw new Exception("Esito Termina Chiamata non trovato.");
		}
//		WebElement temp = driver.findElement(CCEsitoChiamata);
		clickComponentIfExist(oggetto);
		Thread.currentThread().sleep(5000);
//		SELEZIONA DA UNA LISTA UN VALORE:
		WebElement select = driver.findElement(selectCheck);

		((JavascriptExecutor) driver).executeScript(
				"var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }",
				select, "QC OK senza Nuova Registrazione");

//		clickComponentIfExist(val);
	}

	public void switchFrameEClicca(By oggetto) throws Exception {
		try {
			driver.switchTo().defaultContent();
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(oggetto));
			driver.findElement(oggetto).click();;
		} catch (TimeoutException e) {
			driver.switchTo().frame(0);
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(oggetto));
			driver.findElement(oggetto).click();;
		} catch (Exception e) {
			throw e;
		}
	}

	public void jsClickComponentNew(By oggetto) {
		try {
			driver.switchTo().defaultContent();
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(oggetto));
			WebElement testJS = driver.findElement(oggetto);
			((JavascriptExecutor) driver).executeScript("arguments[0].click()", testJS);
		} catch (TimeoutException e) {
			driver.switchTo().frame(0);		
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(oggetto));
			WebElement testJS = driver.findElement(oggetto);
			((JavascriptExecutor) driver).executeScript("arguments[0].click()", testJS);						
		}
	}
	
	
public void uploadFile2 (String filePath) throws Exception {
		
		File file = new File(filePath);
		driver.findElement(inputCaricaDoc).sendKeys(file.getAbsolutePath());
		//util.waitAndGetElement(fileUpload2).sendKeys(file.getAbsolutePath());
}


public void aspettaCookieEAccetta() {
	try {
	new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(acceptCookie));
	driver.findElement(acceptCookie).click();
	} catch(TimeoutException e) {
		System.out.println("Impossibile accettare i cookie nella pagina corrente perché non sono presenti");
	} catch (Exception f) {
		throw f;
	}
}

}
