package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PublicAreaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By productDropDown = By.xpath("//span[@id='productSelectBoxItText']");
	public By luce = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Luce']");
	public By gas = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Gas']");
	public By placeDropDown = By.xpath("//span[@id='placeSelectBoxItText']");
	public By casa = By.xpath("//ul[@id='placeSelectBoxItOptions']//li//a[text()='Casa']");
	public By myselectionDD = By.xpath("//span[@id='myselectSelectBoxItText']");
	public By visualizza = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Visualizza tutte']");
	public By iniziaOra = By.xpath("//span[text()='INIZIA ORA']/parent::a");
	public By dettaglioOfferta = By.xpath("(//button[ @class='btn btn-pink' and contains(text(),'Dettaglio Offerta')])[1]");
	public By dettaglioOffertaNew = By.xpath("//div[@class='plan-promo_plan-holder']");
	//public By bannerPhrase = By.xpath("//p[@class='MsoNormal']");
	public By bannerPhrase = By.xpath("//div[@class='box-pre-chat']");
	public By banner = By.xpath("//button[text()='SCEGLI OFFERTA ']");
	public By leNostreOfferte = By.xpath("//span[text()='Le nostre offerte']");
	public By chiamamiGratis = By.xpath("//span[text()='Chiamami gratis']");

	//public By bannerText1 = By.xpath("//p[contains(text(),'Richiedere aiuto è facile: inserisci il tuo numero e ti richiameremo senza impegno')]");
	public By bannerText1 = By.xpath("//*[@id='call-tab-content']/p");
	public By telefono = By.xpath("//input[@id='telefono']");
	//public By fasciaOraria = By.xpath("//span[@class='selectboxit form-control selectboxit-enabled selectboxit-btn' and @name='timeslot']");
	public By fasciaOraria = By.xpath("//span[@id='chat-fascia-orariaSelectBoxIt']");
	public By fasciaOraria17 = By.xpath("//*[@id='chat-fascia-orariaSelectBoxItOptions']/li[2]/a/span");
	public By fasciaOrariaInput = By.xpath("//span[@name='timeslot']/following::ul//li[@data-id='1']");
	public By bannerText2 = By.xpath("//div[@class='contact-legalterms']//p[contains(text(),'Ai sensi')]");
	public By closeBtn = By.xpath("//div[@id='close-btn']");
	public By chiamamigratis = By.xpath("//a[@id='richiamami']");
	public By responseMsg = By.xpath("//p[text()='Abbiamo preso in carico la tua richiesta. Un nostro esperto ti chiamerà al più presto.']");
	public By telefonoValue = By.xpath("//div[@class='call-tab-response active']//div//b[1]");
	public By fasciaOrariaValue = By.xpath("//div[@class='call-tab-response active']//div//b[2]");
	public By livechat = By.xpath("//span[text()='LIVE CHAT']");
	public By liveChatText = By.xpath("//p[contains(text(),'Al momento non ci sono operatori disponibili. Ti preghiamo di riprovare più tardi.')]");
	public By impresse = By.xpath("//a[text()='Imprese']");
	public String publicAreaUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it";
	public By linkutili = By.xpath("//h3[text()='Link utili']");
	public By utiliLinks = By.xpath("//div[@class='help-tabs__content__inner']");
	public By diventanostropartner = By.xpath("//a[text()='Diventa nostro partner']");
	public By searchIcon = By.xpath("//span[@class='icon-search-small']");
	public By quickLinks = By.xpath("//div[@class='dotcom-search-form__content']");
	public By lemigliori = By.xpath("//a[text()='Le migliori tariffe luce e gas']");
	public By luceEgas = By.xpath("//ul[@id='productSelectBoxItOptions']//li//a[text()='Luce e Gas']");
	public By primaAttivazione = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Prima Attivazione']");
	public By modulisticaReclami = By.xpath("//a[text()='Modulistica reclami']");
	
	//77
	public By nagozio = By.xpath("//ul[@id='placeSelectBoxItOptions']//li//a[text()='Negozio - Ufficio']");
	public By cambio1 = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Cambio Fornitore']");
	public By cambio = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//h5[contains(text(),'Cambio')]");
	//public By visualiza = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Visualizza tutte']");
	public By visualiza = By.xpath("//label[text()='Per quale necessità?']/following-sibling::span//span[text()='Visualizza tutte']");
	public By subentro = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Subentro']");
	public By voltura = By.xpath("//ul[@id='myselectSelectBoxItOptions']//li//div[@class='optionsContent']//h5[text()='Voltura']");
	//public  detaglioOffertgasaUrl2 = ("//a[@href='/content/enel-it/it/adesione-contratto?productType=res'][1]");
	public By detaglioOffertgasaUrl2 = By.xpath("//a[@href='/content/enel-it/it/adesione-contratto?productType=res'][1]");
	public String detaglioOffertaUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/luce-e-gas/luce/casa?nec=swa&sortType=in-promozione";
	public String detaglioOffertgasaUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/luce-e-gas/gas/casa?nec=non_specificato&sortType=in-promozione";
	
	//104
	public By home = By.xpath("//a[.='Home']");
	public By luceEGas = By.xpath("//a[.='Luce e Gas']");
	public By enelOne = By.xpath("//a[.='Enel One']");
	public By enelOneTitle = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By enelOneText = By.xpath("//p[@class='image-hero_detail text--detail']");
	public By enelOneTitle1 = By.xpath("//h2[@class='cmBusinessProfessionals__title']");
	public By enelOneText1 = By.xpath("//p[@class='cmBusinessProfessionals__subtitle']");
	public By basicVerifica = By.xpath("//div[@id='cardItem_card_wrapper_item_484128605']//a");
	public By popupTitle = By.xpath("//div[@id='productpopup-wrapper3-C0063930']/div[@id='code-insert-modal']/div[@class='code-insert-block']/h6[@class='titlePopupDueg']");
	public By popupText = By.xpath("//div[@id='productpopup-wrapper3-C0063930']/div[@id='code-insert-modal']/div[@class='code-insert-block']/p");
	public By confermaButton = By.xpath("//button[@id='enel-one-basic']");
	public By popupLink = By.xpath("//div[@id='productpopup-wrapper3-C0063930']/div[@id='code-insert-modal']/div[@class='code-insert-block']//p/a");
	public By popupClose = By.xpath("//div[@id='productpopup-wrapper3-C0063930']/button[@class='fancybox-close-small']");
	public By podInput = By.xpath("//div[@id='productpopup-wrapper3-C0063930']/div[@id='code-insert-modal']//input[@class='form-control']");
	public By errorMessgae = By.xpath("//span[@id='error-label']");
	public By openEnergyOffertaButton = By.xpath("//div[@class='cmEvidenceBanner_info']//a");
	public By openEnergyDigitalAttivaOra = By.xpath("(//div[@class='cmBusinessProfessionals__column']//a)[2]");
	
	//115
	public By dettaglioOfferta115 = By.xpath("(//ul[@class='plan-promo_pricing-list']/li[@class='offert-item'][1]//div[@class='btn-wrapper']/button[@class='btn btn-pink']");

	HashMap map = new HashMap<>();
	public String value;
	public PublicAreaComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	
	
	//public String detaglioOffertgasaUrl = "https://enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it/it/luce-e-gas/luce/casa?nec=swa&sortType=in-promozione";
	//HashMap map = new HashMap<>();
	//public String value;
	//public PublicAreaComponent(WebDriver driver) throws Exception {
			//this.driver = driver;
			//util = new SeleniumUtilities(this.driver);
			
		//}
	
	
	
	
	
	 
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	 public void changeDropDownValue(By dropdown,By input) throws Exception {
		 util.scrollToElement(driver.findElement(dropdown));
			clickComponent(dropdown);
			clickComponent(input);
			
		}
	 
	 public String changeFasciaOrariaValue(By dropdown,By input) throws Exception {
			util.scrollToElement(driver.findElement(dropdown));
			clickComponent(dropdown);
			Thread.sleep(2000);
			clickComponent(input);
			Thread.sleep(2000);
			value = driver.findElement(dropdown).getText();
			System.out.println(value);
			map.put("FasciaValue", value);
			return value;
		}
	 
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
	}
	 
	 public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}
	 
	 public void waitForLinkUtili(By checkObject){		 
		 util.scrollToElement(driver.findElement(checkObject));
		 WebDriverWait wait = new WebDriverWait (driver, 10);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	 }

	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			/*System.out.println("Normalized:\n"+weText);
			System.out.println(text);*/
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void checkForData(By checkObject, String property) throws Exception {
			
			waitForElementToDisplay(checkObject);			
			String actualValue = driver.findElement(checkObject).getText();
		/*	System.out.println(actualValue);
			System.out.println(property);
		*/	
			if(! property.equalsIgnoreCase(actualValue))
				throw new Exception( actualValue+" Displaying Data is not matching with the Test Data "+property);
			
		}
		
		public void checkTextValue(By checkObject, String property) throws Exception{
			
			String value = driver.findElement(checkObject).getText();
			String text = value.substring(0, 86);
			if(!text.equals(property))
				throw new Exception("Response text is not matching");
		}
		
		public void checkForFasciaValue(By checkObject) throws Exception {
			
			String value = driver.findElement(checkObject).getText();
			String actualText = (String) map.get("FasciaValue");
			System.out.println("actualText " + actualText);
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy hh:mm");
			Date dt = df.parse(actualText);			
			//System.out.println(dt);
			SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Date dt1 = df1.parse(value);
			System.out.println("dt " + dt + "dt1 " + dt1);
			
			if(! dt.toString().contains(dt1.toString())){
				System.out.println("dt.toString() " + dt.toString() + "  dt1.toString() " + dt1.toString());
				throw new Exception("Response text is not matching");	
			}
		}
		
		public void getText() {
			
			String text = driver.findElement(By.xpath("//div[@class='help-tabs__content__inner']")).getText();
			System.out.println(text);
			
		}
		
		public void checkTelefonoValue(By telefonoValue2, String property) throws Exception {
			String value = driver.findElement(telefonoValue2).getText();
			if(!value.contains(property))
				throw new Exception("telefono input is not matching");
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
	 public static final String Banner = "20% di sconto su componente energia e materia prima gas più un buono Amazon da 50€. Scopri di più con un nostro operatore CLICCA QUI&gt;";
	 public static final String BannerTitle = "Per supporto immediato contattaci in chat altrimenti inserisci il tuo numero per essere richiamato: saremo nuovamente disponibili da lunedì mattina alle 8:00";
	 public static final String BannerText = "Ai sensi del Regolamento UE 679/2016 - “GDPR” autorizzo Enel Energia S.p.A. a contattarmi per ricevere informazioni sulle offerte commerciali e ricevere assistenza. Clicca quiper consultare l'informativa Privacy di Enel Energia S.p.A.";
	 public static final String ResponseMessage = "Abbiamo preso in carico la tua richiesta. Un nostro esperto ti chiamerà al più presto.";
	 public static final String LiveChat = "Al momento non ci sono operatori disponibili. Ti preghiamo di riprovare più tardi.";
	 public static final String Sos = "SOS luce e gas";
	 public static final String Text = "SOS luce e gas Mix Combustibili Piano salva Black out (PESSE) Servizio di salvaguardia Servizio default di distribuzione Conciliazioni e risoluzione delle controversie Informazioni utili Nuove regole europee per la protezione dei dati Contattaci Diventa nostro partner Accedi alle sponsorizzazioni Guida alla bolletta luce e gas Prescrizione degli importi fatturati per la fornitura di energia elettrica e gas Modulistica Modulistica reclami Remit Negoziazione paritetica Evoluzione mercati al dettaglio Offerta Servizio Tutela Gas";
	 public static final String QuickLinks= "Quick links Le migliori tariffe luce e gas Le soluzioni per la tua azienda Gestisci la tua fornitura Segui le nostre storie Lavora con noi";
	 public static final String Home = "Home";
	 public static final String LuceEGas = "Luce e Gas";
	 public static final String EnelOne = "Enel One";
	 public static final String EnelOneTitle = "La tua energia tutto incluso e cambi piano con un click.";
	 public static final String EnelOneText = "Per te che possiedi un contatore di seconda generazione, da oggi c’è ENEL ONE: l'abbonamento che ti permette di scegliere il piano più adatto a te con un unico importo mensile, tutto incluso.";
	 public static final String EnelOneTitle1 = "Scegli il piano più adatto a te!";
	 public static final String EnelOneText1 = "Ogni piano offre un quantitativo mensile in kWh per soddisfare le tue abitudini di consumo. Il prezzo è unico ed è tutto incluso! Se le tue abitudini cambiano, puoi cambiare piano gratuitamente, comodamente da web o smartphone. E se passi a Enel Energia da un altro fornitore, ricevi subito un bonus di benvenuto di 20€!";
	 public static final String POPUP_TITLE = "Inserisci il numero del tuo POD*";
	 public static final String POPUP_TEXT = "Verifica che la tua fornitura abbia i requisiti per poter proseguire con l'adesione.";
	 public static final String ERROR_MESSAGE = "Formato inserito non valido";
	 
	
}
