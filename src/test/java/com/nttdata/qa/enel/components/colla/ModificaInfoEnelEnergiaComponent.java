package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModificaInfoEnelEnergiaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[@id='titolo-dispositiva']");
	public By titleSubText = By.xpath("//h2[contains(text(),'Il servizio')]");
	public By radioButton = By.xpath("//span[@class='icon-ckbox']");
	public By esciButton = By.xpath("//div[@class='list-form-container']//button[@class='button_second']");
	public By continuaButton = By.xpath("//div[@class='list-form-container']//span[text()='CONTINUA']");
	public By titleSubText1 = By.xpath("//h2[contains(text(),'Stai effettuando')]");
	public By inserimentoDati = By.xpath("//span[text()='Inserimento dati']");
	public By stepOneDescription = By.xpath("//h4[text()='Seleziona la tipologia di avviso']");
	
	public By comunicaLettura = By.xpath("//span[text()='Comunica Lettura']");
	public By comunicaLetturaSMS = By.xpath("//span[text()='Comunica Lettura']/following::label[1]");
	public By comunicaLetturaEmail = By.xpath("//span[text()='Comunica Lettura']/following::label[2]");
	public By emissioneBolletta = By.xpath("//span[text()='Emissione Bolletta']");
	public By emissioneBollettaSMS = By.xpath("//span[text()='Emissione Bolletta']/following::label[1]");
	public By emissioneBollettaEmail = By.xpath("//span[text()='Emissione Bolletta']/following::label[2]");
	public By avvenutoPagamento = By.xpath("//span[text()='Avvenuto Pagamento']");
	public By avvenutoPagamentoSMS = By.xpath("//span[text()='Avvenuto Pagamento']/following::label[1]");
	public By avvenutoPagamentoEmail = By.xpath("//span[text()='Avvenuto Pagamento']/following::label[2]");
	public By scadenzaProssimaBolletta = By.xpath("//span[text()='Scadenza Prossima Bolletta']");
	public By scadenzaProssimaBollettaSMS = By.xpath("//span[text()='Scadenza Prossima Bolletta']/following::label[1]");
	public By scadenzaProssimaBollettaEmail = By.xpath("//span[text()='Scadenza Prossima Bolletta']/following::label[2]");
	public By sollecitoPagamento = By.xpath("//span[text()='Sollecito Pagamento']");
	public By sollecitoPagamentoSMS = By.xpath("//span[text()='Sollecito Pagamento']/following::label[1]");
	public By sollecitoPagamentoEmail = By.xpath("//span[text()='Sollecito Pagamento']/following::label[2]");
	public By avvisoConsumi = By.xpath("//span[text()='Avviso Consumi']");
	public By avvisoConsumiSMS = By.xpath("//span[text()='Avviso Consumi']/following::label[1]");
	public By avvisoConsumiEmail = By.xpath("//span[text()='Avviso Consumi']/following::label[2]");
	public By inserisciITuoiDati = By.xpath("//h4[text()='Inserisci i tuoi dati']");
	
	public By emailIp = By.xpath("//input[@id='emailFieldInput']");
	public String emailInput = "//input[@id='emailFieldInput']";
	public By cellulare = By.xpath("//input[@id='mobileFieldInput']");
	public By confirmCellulare = By.xpath("//input[@id='mobileFieldConfirmInput']");
	public By checkBox = By.xpath("//label[@for='checkBoxAggiorna i dati anagrafici']");
	public By continueButton = By.xpath("//span[text()='Continua']");
	public By confirmEmail = By.xpath("//input[@id='emailFieldConfirmInput']");
	public By cellularError = By.xpath("//span[@id='mobileFieldError']");
	public By emailError = By.xpath("//span[@id='emailFieldError']");
	public By riepilogoEConfermaDati = By.xpath("//span[text()='Riepilogo e conferma dati']");
	public By tipologiaDiAvviso = By.xpath("//h4[text()='Tipologia di avviso']");
	
	public By comunicaLetturaTitle = By.xpath("//dt[text()='Comunica Lettura']");
	public By comunicaLetturaValue = By.xpath("//dt[text()='Comunica Lettura']/following-sibling::dd");
	public By emissioneBollettaTitle = By.xpath("//dt[text()='Emissione Bolletta']");
	public By emissioneBollettaValue = By.xpath("//dt[text()='Emissione Bolletta']/following-sibling::dd");
	public By avvenutoPagamentoTitle = By.xpath("//dt[text()='Avvenuto Pagamento']");
	public By avvenutoPagamentoValue = By.xpath("//dt[text()='Avvenuto Pagamento']/following-sibling::dd");
	public By prossimaScadenzaTitle = By.xpath("//dt[text()='Prossima Scadenza']");
	public By prossimaScadenzaValue = By.xpath("//dt[text()='Prossima Scadenza']/following-sibling::dd");
	public By sollecitoPagamentoTitle = By.xpath("//dt[text()='Sollecito Pagamento']");
	public By sollecitoPagamentoValue = By.xpath("//dt[text()='Sollecito Pagamento']/following-sibling::dd");
	public By avvisoConsumiTitle = By.xpath("//dt[text()='Avviso Consumi']");
	public By avvisoConsumiValue = By.xpath("//dt[text()='Avviso Consumi']/following-sibling::dd");
	public By iTuoiDati = By.xpath("//h4[text()='I tuoi dati']");
	public By email = By.xpath("//dt[text()='Email']/following-sibling::dd");
	public By cellulare1 = By.xpath("//dt[text()='Cellulare']/following-sibling::dd");
	public By supplyTitle = By.xpath("//h4[contains(text(),'InfoEnelEnergia')]");
	public By address = By.xpath("//div[@class='content-group grey-bg']//span[text()='Indirizzo della fornitura']");
	public By confermaButton = By.xpath("//div[@class='right-main']//span[text()='Conferma']");
	public By supply = By.xpath("//span[@class='cliente']//span[contains(text(),'300001597')]");
	public By supplyNew = By.xpath("//span[text()='Numero Cliente']/following-sibling::span/span/span");
	public By cellulareInvalidError = By.xpath("//span[@id='mobileFieldError']");
	public By fineButton = By.xpath("//span[text()='Fine']");
	public By modificabutton = By.xpath("//span[text()='MODIFICA INFOENELENERGIA']/parent::button[@class='button_first']");	
	public By confermaText1 = By.xpath("//h3[text()='Operazione eseguita correttamente']");
	public By confermaText2 = By.xpath("//h3[text()='Operazione eseguita correttamente']/following-sibling::p");
	
	public ModificaInfoEnelEnergiaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
		
		public void clickComponent(By clickObject) throws Exception {
			util.objectManager(clickObject, util.scrollToVisibility, false);
			util.objectManager(clickObject, util.scrollAndClick);
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkPrePopulatedValue(String xpath) throws Exception{
			
			WebDriverWait wait = new WebDriverWait(this.driver, 50);
		        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		        JavascriptExecutor js = (JavascriptExecutor) driver;
		        String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
			if(value.equals(""))throw new Exception("Field is not pre filled");
		}
		
		public void changeInputText(By by, String newText) throws Exception{
			String[] array = (by.toString()).split(": ");
			util.setElementValue(array[1], newText);
		}
		
		public String modifyExistingPhone(By by) throws Exception{
			Thread.sleep(6000);
			String s = util.jsGetInputValue(by);
			int n = Integer.parseInt(s.substring(s.length()-1, s.length()));
			n = (n+1)%10;
			s = s.substring(0, (s.length()-1))+n;
			changeInputText(by, "");
			WebElement we = util.waitAndGetElement(by);
			Thread.sleep(200);
			we.sendKeys(s);
			return s;
		}
		
		public void ClearInputFieldValue(By locator) throws Exception{
			util.objectManager(locator, util.clear);
		}
		
		public void enterInput(By element,String input){
			util.objectManager(element, util.sendKeys, input);
		}
		
		public static final String PageTitle = "Modifica InfoEnelEnergia";
		public static final String TitleSubText = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
		public static final String TitleSubText1 = "Stai effettuando la modifica del servizio di InfoEnelEnergia";
		public static final String EmptyFieldError = "Il campo Cellulare è obbligatorio.";
		public static final String EmailError = "Email non valida";
		public static final String CellularInvalidError = "Cellulare non valido"; 
		public static final String ConfermaText1 = "Operazione eseguita correttamente"; 
		public static final String ConfermaText2 = "La tua richiesta è stata presa in carico, la modifica sarà disponibile a breve."; 
}
