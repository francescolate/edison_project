package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ScontiEBonusComponent  extends BaseComponent implements SFDCBox {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	
	public ScontiEBonusComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	//public By confermaScontiEBonusSWAEVO = By.xpath("//div[@aria-label='Sconti e Bonus']/ancestor::div[@role='region']//button[text()='Conferma']");
//	public By confermaScontiEBonusSWAEVO = By.xpath("//div//h2//span[text()='Sconti e Bonus']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By confermaScontiEBonusSWAEVO = By.xpath("//span[text()='Sconti e Bonus']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By modificaScontiEBonusSWAEVO = By.xpath("//span[text()='Sconti e Bonus']/ancestor::div[@role='region']//button[text()='Modifica']");
	public By codiceAmico = By.xpath("//*[text()='Sconti e Bonus']/ancestor::div[@role='region']//label[contains(text(), 'Codice Amico')]/../div/input");
	public By ErroreCodiceAmico=By.xpath("//div[@class='slds-notify slds-notify_alert slds-theme_alert-texture slds-notify_container slds-theme_error']/h2[contains(text(), 'Codice non valido')]");
    public By campoCodiceCampagna = By.xpath("//*[@id=\"input-picklist-3279\"]");
    public By msgErrore = By.xpath("//*[@title='Chiudi']");
  
    
    
    
    
	
	public void pressButton(By button) throws Exception{
		driver.switchTo().defaultContent();
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void inserisciCodiceAmico(String cod) throws Exception {
		driver.switchTo().defaultContent();
		driver.findElement(codiceAmico).sendKeys(cod);
		spinner.checkSpinners();
		
	}
	
	public void pulisciCodiceAmico() throws Exception {
		String campoVuoto = null;
		driver.switchTo().defaultContent();
		driver.findElement(codiceAmico).clear();
		driver.findElement(codiceAmico).sendKeys("");
		driver.findElement(codiceAmico).sendKeys(campoVuoto);
		
	}
	
	public void verificaErrore() throws Exception {
		driver.switchTo().defaultContent();
		driver.findElement(ErroreCodiceAmico);
		util.objectManager(msgErrore, util.scrollAndClick);
//		spinner.checkSpinners();
		
	}
}
