package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Luce30SpringComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By offerta30Spring=By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/luce30']");
	public By testoinAlto=By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/luce30']//div[@class='plan-promo-block-text']");
	public By buttonDettaglioOfferta=By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/luce30']//button[normalize-space(text())='Dettaglio Offerta']");
	public By labelSconto=By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/luce30']//div[@class='discount-details  ']//p[text()='Sconto 30% sulla componente energia per 12 mesi']");
	public By testoinBasso=By.xpath("//div[@class='pricing-details' and @data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/luce30']//p");
	public By linkAttivaOra=By.xpath("//div[@class='pricing-details' and @data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/luce30']//a[normalize-space(text())='Attiva ora']");
	public By titoloLuce30Spring=By.xpath("//h1[@class='image-hero_title text--page-heading' and text()='Luce 30 Spring']");
	public By sottotitoloLuce30Spring=By.xpath("//h2[@class='image-hero_detail text--detail' and  normalize-space(text())='Attiva l’offerta online entro il 18 aprile e approfitta dello sconto del 30% sul prezzo della componente energia']");
	public By ButtonAttivaSubito=By.xpath("//a[@href='/it/adesione?zoneid=luce_30_spring-hero' and normalize-space(text())='> Attiva subito <']");
	public By titoloCaratteristiche=By.xpath("//div[1][@class='group parbase']//div[@data-module='services-price']//h3[@class='plan-main-head']");
	public By sottotitolosx=By.xpath("//div[@class='text-clean parbase']//h3[text()='Hai lo sconto del 30%']");
	public By sottotitolodx=By.xpath("//div[@class='text-clean parbase']//h3[text()='Hai un contratto chiaro']");
	public By sottotitolodxbasso=By.xpath("//div[@class='text-clean parbase']//h3[text()='Sconti, bonus e promozioni']");
	public By listaSottotitoli=By.xpath("//div[@class='item parbase']//h3");
	public By listaTesti=By.xpath("//div[@class='item parbase']//p");
	public By sezioneDettagliLabel=By.xpath("//h3[@class='plan-main-head' and normalize-space(text())='Dettagli']");
	public By dettagli=By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]");
	public By sezioneDocumentiLabel=By.xpath("//h3[@class='plan-main-head' and normalize-space(text())='Documenti']");
	public By documenti=By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]");
	public By linkDowload1=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/cte-luce30.pdf']");
	public By linkDowload2=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/schede-confrontabilita-luce-30.pdf']");
	public By linkDowload3=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/cte-luce30.pdf']");
	public By linkDowload4=By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/schede-confrontabilita-luce-30.pdf']");
	public By titoloBannerFinePagina=By.xpath("//h2[@class='title' and text()='Illumina la tua casa con Enel Energia']");
	public By sottotitoloFinePagina=By.xpath("//p[@class='description' and text()='Scopri le nostre offerte Luce e trova la più conveniente per te']");
	public By linkVediTuttoGas=By.xpath("//a[@href='/it/luce-e-gas/gas/casa']//span[@class='text' and normalize-space(text())='Vedi tutte gas']");
	public By tabDettagli=By.xpath("//li[@class='hub-secondary-nav_list-item']//a[@title='Dettagli' and text()='Dettagli']");
	public By tabDocumenti=By.xpath("//li[@class='hub-secondary-nav_list-item']//a[@title='Documenti' and text()='Documenti']");
	
	public By gas=By.xpath("//a[@href='/it/luce-e-gas/gas/offerte/gas30?zoneid=luce_30_spring-box_offerta']//div[@class='plan-promo-block-text']//span[text()='Gas']");
	public By testoinAlto2=By.xpath("//a[@href='/it/luce-e-gas/gas/offerte/gas30?zoneid=luce_30_spring-box_offerta']//div[@class='plan-promo-block-text']");
	public By buttonDettaglioOfferta2=By.xpath("//a[@href='/it/luce-e-gas/gas/offerte/gas30?zoneid=luce_30_spring-box_offerta']//button[normalize-space(text())='Dettaglio Offerta']");
	public By labelSconto2=By.xpath("//a[@href='/it/luce-e-gas/gas/offerte/gas30?zoneid=luce_30_spring-box_offerta']//div[@class='discount-details  ']//p[text()='Sconto del 30% sulla materia prima gas nei primi 12 mesi di fornitura']");
	public By testoinBasso2=By.xpath("//div[@class='pricing-details']//p");
	public By linkAttivaOra2=By.xpath("//div[@class='pricing-details']//a[normalize-space(text())='Attiva Ora']");
	
	public String testoInAlto="30% di sconto Luce 30 Spring Attiva l’offerta online entro il 18 aprile e approfitta dello sconto del 30% sul prezzo della componente energia";
	public String testoInBasso="*prezzo per kWh, tutti i giorni e a tutte le ore. Il prezzo si riferisce alla sola componente energia ed è bloccato per un anno (IVA ed imposte escluse)";
	public String titoloSezioneCaratteristiche="Caratteristiche dell'offerta";
	
	public String testo_in_alto="GAS 30% di sconto Gas 30 Spring Attiva l’offerta online entro il 18 aprile e approfitta dello sconto del 30% sul prezzo della materia prima gas";
	public String testo_in_basso= "*prezzo per smc, tutti i giorni e a tutte le ore Il prezzo di riferisce alla sola componente materia prima gas ed è bloccato per un anno (IVA e imposte escluse)";
	
//	public String linkPLUS_IMAGE="etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/plus.png";
//	public String linkMINUS_IMAGE="etc/designs/enel-comm-common/clientlib-site/css/image/product_vaspage/minus.png";
	public String linkPLUS_IMAGE="plus.png";
	public String linkMINUS_IMAGE="minus.png";
	public String linkPDF_ATTUALI_CONDIZIONI_FORNITURA_GENERALE="content/dam/enel-it/documenti-offerte/documenti-casa/documenti-generali/CGF%20Residenziale_mercato%20libero.pdf";
//	public String linkDOWNLOAD1="content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/Condizioni%20tecnico%20economiche.pdf";
//	public String linkDOWNLOAD2="content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/Scheda%20confrontabilit%C3%A0.pdf";
//	public String linkDOWNLOAD3="content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/cte-luce30.pdf";
//	public String linkDOWNLOAD4="content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/schede-confrontabilita-luce-30.pdf";
	public String linkDOWNLOAD1="content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/cte-luce30.pdf";
	public String linkDOWNLOAD2="content/dam/enel-it/documenti-offerte/documenti-flash-offer/luce30/schede-confrontabilita-luce-30.pdf";
    public String linkGAS_CASA="it/luce-e-gas/gas/casa";
	
	
	
	
	public Luce30SpringComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("object with xpath " + existentObject + " is not exist.");
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void click(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(2);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		
	}
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void verificaTesto (By object,String testo) throws Exception {
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
//		System.out.println(text);
		if (!text.contentEquals(testo)) 
        	throw new Exception("Testo Offerta: '"+text+"' diverso testo atteso: '"+testo+"'");
	
	}
	
	public void verificaSottotitoli (By object) throws Exception {
	     String option_found="NO";
		 List<WebElement> sottotitoliList = driver.findElements(object);
		 
			for (int i = 1; i <= sottotitoliList.size(); i++) { 
				String description=driver.findElement(By.xpath("//div[@class='item parbase']["+i+"]//h3")).getText();
			//	System.out.println(description);
				
				 if (description.contentEquals("Caratteristiche dell'offerta")) 
				        option_found="YES";
				 else if(description.contentEquals("Hai lo sconto del 30%"))
				        option_found="YES";
				 else if (description.contentEquals("Hai un contratto chiaro") )
				    		 option_found="YES";
				 else if (description.contentEquals("Rispetti l'ambiente"))
				    		 option_found="YES";
				 else if (description.contentEquals("Sconti, bonus e promozioni") )
				    		 option_found="YES";
				 else  
				    	 option_found="NO";
				 if (option_found.contentEquals("NO"))
						throw new Exception("sulla pagina di dettaglio dell'offerta Luce 30 Spring non sono presenti i sottotitoli: ");
			   }
			}
			
			public void verificaTesti (By object) throws Exception {
			     String option_found="NO";
				 List<WebElement> sottotitoliList = driver.findElements(object);
				 
					for (int i = 1; i <= sottotitoliList.size(); i++) { 
						String description=driver.findElement(By.xpath("//div[@class='item parbase']["+i+"]//p")).getText();
						description= description.replaceAll("(\r\n|\n)", " ");
					//	System.out.println(description);
						
						 if (description.contentEquals("Hai uno sconto del 30% sul prezzo della componente energia. Il prezzo si riferisce alla sola componente energia ed è bloccato per un anno (IVA ed imposte escluse). L'offerta è valida solo fino al 18 aprile 2019")) 
						        option_found="YES";
						 else if(description.contentEquals("Il prezzo si riferisce alla sola componente energia e rappresenta il 35% circa della spesa complessiva per l'elettricità (IVA e imposte escluse). Dopo i primi 12 mesi Enel Energia aggiorna il prezzo in base all'andamento del mercato e ti comunica il nuovo prima della scadenza. Entro 30 giorni, poi, potrai decidere se accettare o recedere dal contratto"))
						        option_found="YES";
						 else if (description.contentEquals("Con questa offerta usi solo l'energia certificata come prodotta da fonti rinnovabili come acqua, sole, vento e calore della terra. La provenienza dell’energia da fonti rinnovabili è certificata tramite il sistema delle garanzie di origine del GSE") )
						    		 option_found="YES";
						 else if (description.contentEquals("Potrai iscriverti a enelpremia WOW!, il programma fedeltà dedicato ai clienti Enel Energia che ti sorprende ogni settimana con una nuova sorpresa, regalandoti tantissime opportunità di risparmio. Dall’app di Enel Energia, potrai scegliere coupon sconto da utilizzare presso partner aderenti all’iniziativa, partecipare a tanti concorsi con fantastici premi in palio e ottenere bonus sulle tue forniture grazie a iniziative speciali!"))
						    		 option_found="YES";
						 else  
						    	 option_found="NO";
						 if (option_found.contentEquals("NO"))
								throw new Exception("sulla pagina di dettaglio dell'offerta Luce 30 Spring non sono presenti i testi relativi ad ognuna delle quettro sezioni sotto 'Caratteristiche dell'offerta' ");
				
					}
	
	}
			
			
			 public void checkPlusImageDettagli(By checkObject, String imagePlus) throws Exception {
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 List<WebElement> dettagliList = driver.findElements(checkObject);
			
					for (int i = 1; i <= dettagliList.size(); i++) {
						if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]"),imagePlus)!=true)
							throw new Exception("sulla pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Dettagli' ogni domanda NON e' affiancata dall'immagine +");
						
						 }
				    }
			 public void checkDocumenti(By checkObject, String imagePlus, String imageMinus) throws Exception {
				 String option_found="NO"; 
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 List<WebElement> documentiList = driver.findElements(checkObject);
			
					for (int i = 1; i <= documentiList.size(); i++) {
						if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]"),imagePlus)!=true)
							throw new Exception("sulla pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Documenti' ogni sezione documenti NON e' affiancata dall'immagine +");
						String documenti=driver.findElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")).getText();
						//System.out.println(documenti);
						if (documenti.contentEquals("Documenti contratto"))
							option_found="YES";
					     else if(documenti.contentEquals("Documenti generali"))
					     	option_found="YES"; 
					     else  
					    	 option_found="NO";
						 if (option_found.contentEquals("NO"))
								throw new Exception("nella pagina di dettaglio dell'offerta luce 30 spring non sono presenti nella sezione 'Documenti' i documenti contratto e generali");
						 TimeUnit.SECONDS.sleep(1);
						 //util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"), util.scrollAndClick);
						 util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"), util.scrollToVisibility, false);
						 util.jsClickElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"));
						 TimeUnit.SECONDS.sleep(1);
						
					     if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]"),imageMinus)!=true)
								throw new Exception("sulla pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Documenti' ogni sezione di documento che si apre dopo un click NON e' affiancata dall'immagine -");	    	 
					     util.jsClickElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']"));
					    
				    	}
				    }
			 
			 public void checkQuestionsDetails(By checkObject, String imageMinus) throws Exception {
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 String option_found="NO";
				 
				 List<WebElement> questionsList = driver.findElements(checkObject);
				 
					for (int i = 1; i <= questionsList.size(); i++) { 
						String domanda=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")).getText();
						////System.out.println("domanda"+domanda);
						//OLD 10.06 Ros util.objectManager(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"), util.scrollAndClick);
						util.jsClickElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"));
						Thread.currentThread().sleep(1000);
						 WaitVar = new WebDriverWait(driver, 20);
						 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']")));
						 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']")));
					     String text=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//div[@class='item-data']//div[@class='rich-text_text text--standard']")).getText();
					     text= text.replaceAll("(\r\n|\n)", " ");
					   // //System.out.println("dettaglio"+text);
					     Thread.currentThread().sleep(500);
							     if (domanda.contentEquals("In quali casi è possibile attivare l’offerta?") && text.contentEquals("Puoi aderire a Primavera Luce 30 se provieni da un altro fornitore, se devi attivare un nuovo contatore (prima attivazione), riattivare un contatore disattivato (subentro)."))
							         option_found="YES";
							     else if(domanda.contentEquals("Come aderire all’offerta facendo il passaggio da un altro fornitore") && text.contentEquals("Se provieni da un altro fornitore il passaggio per aderire a Primavera Luce 30 è molto semplice. Ti basta sapere che:   l'attivazione è gratuita; non sono necessari interventi sugli impianti o sul contatore; il passaggio sarà effettivo in circa 2/3 mesi, tempo necessario per i controlli previsti dalla normativa vigente.   Per richiedere Primavera Luce 30 ti servono soltanto:   una bolletta; il tuo codice fiscale; l'IBAN, se vuoi addebitare la bolletta sul tuo conto corrente."))
							         option_found="YES";
							     else if (domanda.contentEquals("Bolletta Web o cartacea?") && text.contains("Decidi tu come ricevere la bolletta. Hai a disposizione due opzioni: bolletta online o cartacea.   Con Bolletta Web ricevi le bollette in formato digitale direttamente nella tua casella email. Dai così il tuo contributo all'ambiente riducendo l'uso di carta.   Riceverai la tua prima bolletta 1 mese dopo la data di attivazione del contratto. Le successive bollette ti verranno recapitate ogni 2 mesi."))
							     	 option_found="YES";
							     else if (domanda.contentEquals("Come vuoi pagare la bolletta?") && text.contentEquals("Scegli tu come pagare la bolletta e cambia metodo di pagamento quando vuoi. Puoi pagare tramite:    Bollettino postale e carta di credito Se scegli il pagamento con bollettino postale in fase di adesione, puoi pagare le singole bollette anche con carta di credito attraverso la nostra Area Clienti oppure presso tutte le ricevitorie autorizzate Sisal e Lottomatica. PayPal Con PayPal paghi la bolletta in modo semplice e sicuro direttamente dall’Area Clienti Enel Energia. Puoi eseguire il pagamento in qualsiasi momento dal tuo computer, da smartphone o tablet: i tuoi dati sono al sicuro, non vengono comunicati a terzi e sono protetti dai server PayPal. Addebito su conto corrente Un modo veloce per avere sempre sotto controllo i tuoi pagamenti è l’addebito su conto corrente: rispetti l’ambiente senza l’invio di fatture cartacee. La data di addebito corrisponde al giorno della scadenza della bolletta. La bolletta arriva circa 15 giorni prima della scadenza, così hai tutto il tempo per verificarla."))
							     	 option_found="YES";
							     else if (domanda.contentEquals("Da cosa è composto il prezzo dell'energia?") && text.contentEquals("ll prezzo indicato si riferisce alla componente energia e rappresenta il 35% circa della spesa complessiva per l’energia elettrica (IVA e imposte escluse) di un cliente domestico tipo residente con:   3 kW di potenza impegnata consumi annui pari a 2700 kWh Sono esclusi da questo prezzo i costi per il dispacciamento, i costi per il servizio di trasporto e gestione del contatore, gli oneri di sistema e i costi di commercializzazione, che sono applicati nella misura definita dall’ARERA e di volta in volta modificati e aggiornati."))
							     	 option_found="YES";
							     else if (domanda.contentEquals("Servizi SMS") && text.contentEquals("Hai a disposizione molti servizi, attivabili via SMS, con cui ti comunicheremo:   quando è stata emessa la tua bolletta; quando sta per scadere; se l’ultima bolletta risulta pagata correttamente; quando puoi mandarci la lettura del tuo contatore.  Gli SMS ricevuti sono gratuiti. Inoltre puoi inviare la lettura del tuo contatore e richiedere:  aggiornamenti sullo stato di lavorazione del tuo contratto; di ricevere le tue bollette via email; modifiche ai tuoi dati personali (indirizzo e-mail, telefono, indirizzo…); dove si trova il Punto Enel più vicino.  Il costo degl SMS inviati cambia a seconda del tuo piano tariffario."))
							     	 option_found="YES";
							     else if (domanda.contentEquals("Come si calcola il deposito cauzionale?") && text.contentEquals("Il deposito cauzionale, che ti verrà rimborsato nell'ultima bolletta di conguaglio, è calcolato sulla base della potenza del tuo contatore. Nei dettagli:    7,74 € per contatori da 1,5 kW 15,48 € per 3 kW 23,22 € per 4,5 kW 30,96 € per 6 kW  Se dovessi recedere dal contratto, il rimborso del deposito è addizionato del tasso di interesse legale."))
							     	 option_found="YES";
							     else if (domanda.contentEquals("Come funziona il recesso da Enel Energia?") && text.contentEquals("Puoi recedere dal contratto con Enel in qualsiasi momento tramite il tuo fornitore, in occasione della stipula del nuovo contratto. Inoltre, puoi annullare la richiesta di attivazione della fornitura tramite raccomandata, senza oneri, entro 14 giorni dalla conclusione del contratto. Puoi trovare maggiori informazioni nel documento \"Attuali condizioni generali di fornitura\"."))
							     	 option_found="YES";
							     else if (domanda.contentEquals("Come funziona il diritto di Ripensamento?") && text.contentEquals("Puoi esercitare il diritto di Ripensamento come da legge entro 14 giorni dalla conclusione del contratto, senza oneri aggiuntivi. Per inviare ad Enel la comunicazione puoi utilizzare uno dei canali previsti:   online su questo sito via e-mail all’indirizzo: allegati.enelenergia@enel.com chiamando il numero verde 800.900.860 tramite fax al numero 800.046.311 via posta (semplice o raccomandata) a  Enel Energia S.p.A.  C.P. 8080 – 85100 Potenza "))
							     	 option_found="YES";
							     else  
							    	 option_found="NO";
					     if(util.checkAfterBackgroundUrl(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]"),imageMinus)!=true)
								throw new Exception("sulla pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Dettagli' ogni domanda che si apre dopo un click NON e' affiancata dall'immagine -");	    	 
					     // OLD util.objectManager(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']"), util.scrollAndClick);
					     util.jsClickElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='true']"));
							
					     Thread.currentThread().sleep(1000);
					     WaitVar = new WebDriverWait(driver, 10);
						 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")));
					
						 if (option_found.contentEquals("NO"))
								throw new Exception("nella pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Dettagli' non sono presenti le 9 domande con il relativo testo o qualcosa e' cambiato nel testo della domanda o della sua risposta");
					}
			 }
			 
			 public void clickAttualiCondizioni(By checkObject) throws Exception {
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 String option_found="NO";
				 
				 List<WebElement> questionsList = driver.findElements(checkObject);
				 
					for (int i = 1; i <= questionsList.size(); i++) { 
						String domanda=driver.findElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']")).getText();
						if (domanda.contentEquals("Come funziona il recesso da Enel Energia?")){ 
							//OLDutil.objectManager(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"), util.scrollAndClick);
							util.jsClickElement(By.xpath("//section[@data-module='details-accordion']//section[@data-accordion]["+i+"]//button[@aria-expanded='false']"));
							// OLD util.objectManager(By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-casa/documenti-generali/CGF%20Residenziale_mercato%20libero.pdf' and text()='Attuali condizioni generali di fornitura']"), util.scrollAndClick);
							util.jsClickElement(By.xpath("//a[@href='/content/dam/enel-it/documenti-offerte/documenti-casa/documenti-generali/CGF%20Residenziale_mercato%20libero.pdf' and text()='Attuali condizioni generali di fornitura']"));
						}
							
					}
					
				}
			 
			 
			 public void dettagliDocumentiContratto (By checkObject) throws Exception {
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 String option_found="NO";
				 
				util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti contratto']"), util.scrollToVisibility, false);
				util.jsClickElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti contratto']"));
				
							
							 List<WebElement> dettagliDocumentiList = driver.findElements(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']"));
							 
								for (int j = 1; j <= dettagliDocumentiList.size(); j++) { 
									Thread.currentThread().sleep(1000);
									 WaitVar = new WebDriverWait(driver, 10);
									 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]")));
									
									String dettaglio=driver.findElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]")).getText();
									dettaglio= dettaglio.replaceAll("(\r\n|\n)", " ");
						        // 	System.out.println(dettaglio);
			  				       
						         	 if (dettaglio.contentEquals("Condizioni tecnico economiche Pdf size 0.22 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Scheda di confrontabilità Pdf size 0.43 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Condizioni tecnico economiche Pdf size 0.14 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Scheda di confrontabilità Pdf size 0.43 mb DOWNLOAD") )
								         option_found="YES";
								     else  
								    	 option_found="NO";
						         	 if (option_found.contentEquals("NO"))
											throw new Exception("nella pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Documenti Contratto' non e' presenta la lista di documenti desiderata");
						}
							
		
	}
			 
			 public void dettagliDocumentiGenerali (By checkObject) throws Exception {
				 WebDriverWait WaitVar = new WebDriverWait(driver, 10);
				 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				 String option_found="NO";
				 util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='true' and text()='Documenti contratto']"), util.scrollToVisibility, false); 
				 util.jsClickElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='true' and text()='Documenti contratto']"));
				 Thread.currentThread().sleep(1000);
				 util.objectManager(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti generali']"), util.scrollToVisibility, false);
				 util.jsClickElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//button[@aria-expanded='false' and text()='Documenti generali']"));
		
							 List<WebElement> dettagliDocumentiList = driver.findElements(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']"));
							 
								for (int j = 1; j <= dettagliDocumentiList.size(); j++) { 
									Thread.currentThread().sleep(1000);
									 WaitVar = new WebDriverWait(driver, 10);
									 WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]")));
									
									String dettaglio=driver.findElement(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]")).getText();
									
									checkColor(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]//div[@class='item-data-download']//div"),"DarkGray","size document");
									checkColor(By.xpath("//section[@data-module='document-accordion']//section[@data-accordion]//div[@aria-hidden='false']//div[@class='item-data']["+j+"]//div[@class='item-data-download']//a"),"DeepPink","link download document");
									
									
									dettaglio= dettaglio.replaceAll("(\r\n|\n)", " ");
						        	//System.out.println(dettaglio);
						         	
						         	 if (dettaglio.contentEquals("Nota informativa per il cliente finale size 0.12 mb DOWNLOAD") ) {
						         		 
						       
								         option_found="YES"; }
								     else if(dettaglio.contentEquals("Guida alla compilazione della dichiarazione di regolare possesso/detenzione dell'immobile Pdf size 0.68 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Dichiarazione dei dati catastali Pdf size 0.06 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Certificato di residenza Pdf size 0.1 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Condizioni generali di fornitura Pdf size 0.15 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Modulo di adesione Pdf size 0.08 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Informazioni precontrattuali Pdf size 0.03 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Dichiarazione dei dati catastali Pdf size 0.43 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Informativa privacy Pdf size 0.63 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Modulo di ripensamento Pdf size 0.08 mb DOWNLOAD") )
								         option_found="YES";
								     else if(dettaglio.contentEquals("Modulo di addebito diretto Pdf size 0.11 mb DOWNLOAD") )
								         option_found="YES";
								     else  
								    	 option_found="NO";
						         	 if (option_found.contentEquals("NO"))
											throw new Exception("nella pagina di dettaglio dell'offerta luce 30 spring nella sezione 'Documenti Generali' non e' presente la lista di documenti desiderata");
						}
							
		
	}
}
