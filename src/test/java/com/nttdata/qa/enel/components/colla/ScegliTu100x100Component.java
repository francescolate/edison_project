package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ScegliTu100x100Component {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By offertaSceglitu100x100=By.xpath("//a[@href='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-100x100']//h3[@class='title' and text()='Scegli Tu 100x100']");
	public By attivaOraButton=By.xpath("//a[@href='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-100x100']//button[normalize-space(text())='Attiva ora']");
	public By titoloScegliTu=By.xpath("//h1[@class='image-hero_title text--page-heading' and text()='Scegli Tu 100x100']");
	public By domandaNelFooter=By.xpath("//div[@class='container footer-hero-container']//h2[@class='footer-hero-cta__title' and normalize-space(text())='Hai bisogno di supporto per trovare il piano più adatto a te?']");
	public By labelTiaiutiamoNoi=By.xpath("//div[@class='container footer-hero-container']//p[text()='Ti aiutiamo noi']");
	public By buttonIniziaOra=By.xpath("//div[@class='container-footer-hero-button ']//a[normalize-space(text())='INIZIA ORA' and @class='btn-cta btn-cta--clear ']");
	
	public ScegliTu100x100Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void jsClickComponent(By xpath) throws Exception{
		util.jsClickElement(xpath);
	}
	
}
