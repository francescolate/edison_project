package com.nttdata.qa.enel.components.lightning;

import java.awt.List;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.text.Document;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

//Il seguente componente si occupa di effettuare l'inserimento del POD per lo SWA-EVO
//specificando 

public class GestionePODperSWAEVO {
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;

	public GestionePODperSWAEVO(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}

	By insertPOD = By.xpath(
			"//label[text()='POD/PDR']/../div/input|//div[@class='active hasFixedFooter oneWorkspace']//section[@class='tabContent active oneConsoleTab']//input");
	// By selectTipoDocumento = By.xpath("//label[contains(text(),'Tipo
	// documento')]/..//select");
	// By insertRegione = By.xpath("//label[contains(text(),'Numero
	// documento')]/..//input");
	By insertProvincia = By.xpath("//label[contains(text(),'*Provincia')]/..//input");
	By insertComune = By.xpath("//label[contains(text(),'*Comune')]/..//input");
	By insertIndirizzo = By.xpath("//label[contains(text(),'*Indirizzo')]/..//input");
	By insertCivico = By.xpath("//label[contains(text(),'*Numero Civico')]/..//input");

	By insertUso = By.xpath("//label[contains(text(),'Uso')]/..//input");
	By insertResidente = By.xpath("//label[contains(text(),'Residente')]/..//input");
	By insertSocietaVendita = By.xpath("//label[contains(text(),'Attuale società di vendita')]/..//input");
	By insertSocietaVenditaSpan = By.xpath("//label[contains(text(),'Attuale società di vendita')]/../div/following::li[@data-record='0']");
	By insertTitolarita = By.xpath("//label[contains(text(),'Titolarità')]/..//input");
	By consumoAnnuo = By.xpath("//label[contains(text(),'Consumo annuo')]/..//input");
	public By aggiungiFornitura = By.xpath("//button[text()='Aggiungi fornitura']");
    By rimuoviFornitura = By.xpath("//button[text()='Rimuovi fornitura']");
    // GAS
    By matricolaContatore = By.xpath("//label[contains(text(),'Matricola contatore')]/..//input");
    By tipoInstallazione = By.xpath("//label[contains(text(),'Tipo installazione')]/..//input");
  
    By categoriaConsumo = By.xpath("//label[contains(text(),'Categoria di Consumo')]/..//input");
  
    By categoriaMarketing = By.xpath("//label[contains(text(),'Categoria di Marketing')]/..//input");
  
    By ordineGrandezza = By.xpath("//label[contains(text(),'Ordine di Grandezza')]/..//input");
  
    By profiloConsumo = By.xpath("//label[contains(text(),'Profilo di Consumo')]/..//input");
  
    By potenzialita = By.xpath("//label[contains(text(),'Potenzialità')]/..//input");
  
    By inputUtilizzo = By.xpath("//label[contains(text(),'Utilizzo')]/..//input");
    

	// public By confermaPodButton = By.xpath("//button[text()='Conferma
	// Pod/Pdr'][1]");
	// public By confermaPodButton = By.xpath("//button[text()='Conferma
	// Pod/Pdr']");
	// public By confermaPodButton = By.xpath("//button[contains(text(),'Conferma
	// Pod/Pdr')]");
	public By confermaPodButton = By.xpath("//button[contains(text(),'Conferma Pod')]");

	public By confermaPodButton1 = By.xpath(
			"/html/body/div[4]/div[1]/div[2]/div[2]/div/div[2]/div/section[2]/div/div[2]/div/div/article/div[1]/div[4]/article/div[1]/div/div[1]/div/div/div[1]/article/div[2]/div/button");
	public By verificaIndirizzoButton = By.xpath("//button[text()='Verifica'][1]");
	public By confermaDatiSitoButton = By.xpath("//button[text()='Conferma Dati Sito'][1]");
	public By confermaFornitureButton = By
			.xpath("//section[@class='tabContent active oneConsoleTab']//div//button[text()='Conferma Forniture']");
	// public By creaOffertaButton = By.xpath("//section[@class='tabContent active
	// oneConsoleTab']//div//button[text()='Crea offerta']");
	public By creaOffertaButton = By.xpath(
//			"//section[@class='tabContent active oneConsoleTab']//div[@class='oneWorkspaceTabWrapper'] //article[@class='slds-card']//div[@class='slds-col_bump-left slds-size_10-of-12 slds-align-middle']//div//button[text()='Crea offerta']");
//			"//section[@class='tabContent active oneConsoleTab']//div[@class='oneWorkspaceTabWrapper']//article[@class='slds-card']//button[text()='Crea offerta']//div//button[text()='Crea offerta']");
			"//div//button[text()='Crea offerta']");
	// public By okPopupButton =
	// By.xpath("//*[@id='modalButtons-839']/lightning-button/button");
	public By okPopupButton = By.xpath("//button[contains(text(),'OK')]");
	public By modificaButton = By
			.xpath("//button[text()='Modifica Indirizzo']/parent::div[not(contains(@class,'slds-hide'))]");
	
	public By mercatodiprovenienza = By.xpath("//label[text()='Mercato di provenienza']/..//input");
	// Nuovi per Forzatura Indirizzo
	public By inputProvincia=By.xpath("//label[contains(text(),'Provincia')]/../input");
	public By inputCapLavori = By.xpath("//h2//span[text()='Forniture inserite']/../../../../..//label[contains(text(),'CAP')]/../input");
	public By nomeComuneLavori = By.xpath("//h2//span[text()='Forniture inserite']/ancestor::div[@role='region']//label[contains(text(),'*Comune')]/..//input"); 
	public By forzaIndirizzoButtonInserimentoLavori = By.xpath("//h2//span[text()='Forniture inserite']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
	public By buttonIndirizzoNonForzatoLavori = By.xpath("//h2//span[text()='Forniture inserite']/ancestor::div[@role='region']//div[@class='slds-visible slds-p-around--small']//span[text()='Indirizzo Non forzato']/ancestor::span");
	public By capSpan=By.xpath("//label[contains(text(),'CAP')]/../following::li[@data-record='0']");
//	public By capSpanModFirma=By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']");
	
	//pop-up Modifica Stato Residenza
	public By popUpModificaStatoResidenzaSWA = By.xpath("//div[@class='slds-card__body slds-card__body_inner']//h2[text()='Attenzione']");
	public By popUpModificaStatoResidenzaTestoSWA = By.xpath("//div[@class='slds-card__body slds-card__body_inner']//h2[text()='Attenzione']/ancestor::div[1]//p");
	public By buttonChiudipopUpModificaStatoResidenzaSWA = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='OK']");
	public String testopopUpModificaStatoResidenzaSWA="Il cliente potrebbe ricevere comunicazioni di revoca dello stato residente se precedentemente dichiarato su altre forniture";

	public void inserisciPOD(String frame, String POD) throws Exception {
		TimeUnit.SECONDS.sleep(7);
		util.frameManager(frame, insertPOD, util.sendKeys, POD);
	}
	
	public void press(By element) throws Exception{
		util.objectManager(element, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}
	
	public void verificaPulsantiFornituraDisabilitati() throws Exception {
		util.checkElementDisabled(aggiungiFornitura);
		util.checkElementDisabled(rimuoviFornitura);
	}
	
	public void aggiungiFornitura() throws Exception {
		TimeUnit.SECONDS.sleep(30);
		util.objectManager(aggiungiFornitura, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void inserisciPOD(String POD) throws Exception {
		TimeUnit.SECONDS.sleep(3);
		util.waitAndGetElement(insertPOD).sendKeys(POD);
	}

	public void inserisciIndirizzo(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {

		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
			// logger.info("Ho trovato il campo *Provincia e inserisco il valore " +
			// Provincia);
		
			util.objectManager(insertComune, util.scrollAndSendKeysAndTab,Comune);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(insertCivico, util.scrollAndSendKeys,Civico);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(insertProvincia, util.scrollAndSendKeysAndTab,Provincia);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(insertIndirizzo, util.scrollAndSendKeysAndTab,Indirizzo);
			TimeUnit.SECONDS.sleep(5);
		}

	}

	public void inserisciDettaglioSito(String Uso, String SocietaVendita, String Residente, String Titolarita, String Mercato)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		util.selectLighningValue("Residente", Residente);
		util.selectLighningValue("Titolarità", Titolarita);
		util.checkElementValue(mercatodiprovenienza, Mercato);

	}
	
	public void inserisciDettaglioSitoPod2(String Uso, String Residente, String Potenza, 
			String Tensione, String SocietaVendita, String TipoMisuratore, String Mercato, String Titolarita)
			throws Exception {
		
//		util.selectLighningValue("Uso",Uso);
		util.selectLighningValue("Residente", Residente);
//		util.selectLighningValue("Potenza",Potenza);
//		util.selectLighningValue("Tensione",Tensione);
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
//		util.selectLighningValue("Tipo Misuratore", TipoMisuratore);
		util.selectLighningValue("Titolarità", Titolarita);
//		util.selectLighningValue("Mercato di Provenienza", Mercato);

	}
	
	public void inserisciDettaglioSito(String Uso, String Residente, String Potenza, 
			String Tensione, String SocietaVendita, String TipoMisuratore, String Mercato, String Titolarita)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		util.selectLighningValue("Residente", Residente);
//		util.selectLighningValue("Potenza",Potenza);
//		util.selectLighningValue("Tensione",Tensione);
		// Gestione Società di Vendita
//		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
//		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+Mercato+" • "+ "ELETTRICO");
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(3);
//		util.objectManager(insertSocietaVenditaSpan, util.scrollAndClick);
//		TimeUnit.SECONDS.sleep(3);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(8);
		util.selectLighningValue("Titolarità", Titolarita);
		TimeUnit.SECONDS.sleep(1);

	}

	public void inserisciDettaglioSitoResidenteSi(String Uso, String Residente, String Potenza, 
			String Tensione, String SocietaVendita, String TipoMisuratore, String Mercato, String Titolarita)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		util.selectLighningValue("Residente", Residente);
		// Gestione Società di Vendita
//		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
//		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+Mercato+" • "+ "ELETTRICO");
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(3);
//		util.objectManager(insertSocietaVenditaSpan, util.scrollAndClick);
//		TimeUnit.SECONDS.sleep(3);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
		util.selectLighningValue("Titolarità", Titolarita);
		TimeUnit.SECONDS.sleep(1);
		  

	}

	public void inserisciDettaglioSito(String SocietaVendita, String Residente, String Titolarita, String Mercato)
			throws Exception {
		
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);
		util.selectLighningValue("Residente", Residente);
		util.selectLighningValue("Titolarità", Titolarita);
		util.checkElementValue(mercatodiprovenienza, Mercato);

	}
	
	public void inserisciDettaglioSito2(String Uso, String SocietaVendita, String Titolarita, String Mercato)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);
		util.selectLighningValue("Titolarità", Titolarita);

	}
	
	
	public void inserisciDettaglioSito2(String SocietaVendita, String Titolarita,String Mercato)
			throws Exception {
		
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);
		util.selectLighningValue("Titolarità", Titolarita);
		TimeUnit.SECONDS.sleep(2);

	}
	
	public void inserisciDettaglioSitoNonResidenziale(String Uso, String SocietaVendita, String Mercato, String Categoria, String Potenza, String Tensione, String Misuratore,String Consumo)
			throws Exception {
		
		util.selectLighningValue("Uso",Uso);
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);
		util.selectLighningValue("Categoria Merceologica", Categoria);
		TimeUnit.SECONDS.sleep(2);
		util.selectListOptionValue("Potenza Contrattuale", Potenza);
		util.selectLighningValue("Tensione disponibile", Tensione);
		util.selectLighningValue("Tipo misuratore", Misuratore);
		util.checkElementValue(mercatodiprovenienza, Mercato);
		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);

	}
	

	public void inserisciDettaglioSitoNonResidenzialePod2(String Uso, String SocietaVendita, String Mercato, String Categoria, String Potenza, String Tensione, String Misuratore,String Consumo)
			throws Exception {
		
//		util.selectLighningValue("Uso",Uso);
//		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(2);
		util.selectLighningValue("Categoria Merceologica", Categoria);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectListOptionValue("Potenza Contrattuale", Potenza);
		util.selectLighningValue("Tensione disponibile", Tensione);
		util.selectLighningValue("Tipo misuratore", Misuratore);
		util.checkElementValue(mercatodiprovenienza, Mercato);
		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);

	}
	public void inserisciDettaglioSitoNonResidenzialeGas(String Uso, String SocietaVendita, String Mercato, 
			String Matricola, String TipoInstallazione, String CatConsumo, String CatMarketing,String OrdineGrandezza,
			String Profilo,String Potenzialita,String Utilizzo,String Consumo, String Titolarità)
			throws Exception {

		//label[text()='Uso']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Tipo installazione']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Categoria di Consumo']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Categoria di Marketing']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Ordine di Grandezza']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Profilo di Consumo']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Potenzialità']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		//label[text()='Utilizzo']/parent::lightning-combobox//input[@lightning-basecombobox_basecombobox]
		
		
		//label[text()='Uso']/parent::lightning-combobox//span[text()='Uso Diverso da Abitazione']]
		util.selectLighningValue("Uso",Uso);
//		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectLighningValue("Tipo installazione",TipoInstallazione);
// con Agriturismo funziona mentre Industria non lo setta
		util.selectLighningValue("Categoria di Consumo",CatConsumo);
		util.selectLighningValue("Categoria di Marketing",CatMarketing);
		util.selectLighningValue("Ordine di Grandezza",OrdineGrandezza);
		util.selectLighningValue("Profilo di Consumo",Profilo);
		util.selectLighningValue("Potenzialità",Potenzialita);
		util.selectLighningValue("Utilizzo",Utilizzo);
		String matricola=util.randomNumeric(10);
		TimeUnit.SECONDS.sleep(5);
		util.waitAndGetElement(matricolaContatore).sendKeys(matricola);
		util.selectLighningValue("Titolarità",Titolarità);
		//Aggiunto da Capuano Tommaso in data 12/05/2020
		TimeUnit.SECONDS.sleep(5);
		
//		util.objectManager(consumoAnnuo, util.scrollAndSendKeys,Consumo);
//		util.objectManager(matricolaContatore, util.scrollAndSendKeys,Matricola);
		
//		TimeUnit.SECONDS.sleep(3);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);
//		util.waitAndGetElement(categoriaConsumo).sendKeys(CatConsumo);
//		util.waitAndGetElement(categoriaMarketing).sendKeys(CatMarketing);
//		util.waitAndGetElement(ordineGrandezza).sendKeys(OrdineGrandezza);
//		util.waitAndGetElement(profiloConsumo).sendKeys(Profilo);
//		util.waitAndGetElement(potenzialita).sendKeys(Potenzialita);
//		util.waitAndGetElement(inputUtilizzo).sendKeys(Utilizzo);
	
	}
	
	public void inserisciDettaglioSitoNonResidenzialeGas2(String SocietaVendita, String Mercato, 
			String Matricola, String TipoInstallazione, String CatConsumo, String CatMarketing,String OrdineGrandezza,
			String Profilo,String Potenzialita,String Utilizzo,String Consumo)
			throws Exception {

		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectLighningValue("Tipo installazione",TipoInstallazione);
// con Agriturismo funziona mentre Industria non lo setta
		util.selectLighningValue("Categoria di Consumo",CatConsumo);
		util.selectLighningValue("Categoria di Marketing",CatMarketing);
		util.selectLighningValue("Ordine di Grandezza",OrdineGrandezza);
		util.selectLighningValue("Profilo di Consumo",Profilo);
		util.selectLighningValue("Potenzialità",Potenzialita);
		util.selectLighningValue("Utilizzo",Utilizzo);
		//Aggiunto da Capuano Tommaso in data 12/05/2020
//		util.waitAndGetElement(matricolaContatore).sendKeys(Matricola);
		String matricola=util.randomNumeric(10);
		util.waitAndGetElement(matricolaContatore).sendKeys(matricola);
		
//		util.objectManager(consumoAnnuo, util.scrollAndSendKeys,Consumo);
//		util.objectManager(matricolaContatore, util.scrollAndSendKeys,Matricola);
		
//		TimeUnit.SECONDS.sleep(3);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(tipoInstallazione).sendKeys(TipoInstallazione);
//		util.waitAndGetElement(consumoAnnuo).sendKeys(Consumo);
//		util.waitAndGetElement(categoriaConsumo).sendKeys(CatConsumo);
//		util.waitAndGetElement(categoriaMarketing).sendKeys(CatMarketing);
//		util.waitAndGetElement(ordineGrandezza).sendKeys(OrdineGrandezza);
//		util.waitAndGetElement(profiloConsumo).sendKeys(Profilo);
//		util.waitAndGetElement(potenzialita).sendKeys(Potenzialita);
//		util.waitAndGetElement(inputUtilizzo).sendKeys(Utilizzo);
	
	}
	public void inserisciDettaglioSitoNonResidenzialeGas(String SocietaVendita,
			String Matricola, String TipoInstallazione, String CatConsumo, String CatMarketing,String OrdineGrandezza,
			String Profilo,String Potenzialita,String Utilizzo,String Consumo,String Mercato)
			throws Exception {
		//util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita+" • "+Mercato);
		util.waitAndGetElement(insertSocietaVendita).sendKeys(SocietaVendita);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(insertSocietaVendita).sendKeys(Keys.ENTER);
		util.selectLighningValue("Tipo installazione",TipoInstallazione);
		util.selectLighningValue("Categoria di Consumo",CatConsumo);
		util.selectLighningValue("Categoria di Marketing",CatMarketing);
		util.selectLighningValue("Ordine di Grandezza",OrdineGrandezza);
		util.selectLighningValue("Profilo di Consumo",Profilo);
		util.selectLighningValue("Potenzialità",Potenzialita);
		util.selectLighningValue("Utilizzo",Utilizzo);
		//Aggiunto da Capuano Tommaso in data 12/05/2020
		String matricola=util.randomNumeric(10);
		util.waitAndGetElement(matricolaContatore).sendKeys(matricola);
	
	}

	public void confermaDatiSito(By confermaDatiSito) throws Exception {
		util.objectManager(confermaDatiSito, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}
	
	public void verificaPopUpResidenteSWA(By obj, By objText,By button,String popupTestoAtteso) throws Exception{	
		if(util.exists(obj, 60)){
		String textModal = util.waitAndGetElement(objText,false).getText();
		System.out.println("Messaggio visualizzato	:"+textModal);
		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);
		}
		else throw new Exception("Non è stata visualizzata la pop up con testo");

	}
	
	
	public void verificaTabellaFornitureInserite(String commodity,String pod, String indirizzo, String istat, String off) throws Exception{
		
//		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[2]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//*[@data-row='"+pod+"']/../..//td[2]//div//div"));
		
//		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//td[3]//div//div"));
		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//*[@data-row='"+pod+"']/../..//td[3]//div//div"));		
//		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[4]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//*[@data-row='"+pod+"']/../..//td[4]//div//div"));
//		WebElement podIstat = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[5]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement podIstat = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//*[@data-row='"+pod+"']/../..//td[5]//div//div"));
//		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[6]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement offertabilita = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//*[@data-row='"+pod+"']/../..//td[6]//div//div"));
        
		if(!tipoCommodity.getText().contentEquals(commodity)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il tipo Commodity risulta diverso da "+commodity);
		if(!numeroFornitura.getText().contentEquals(pod)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il pod risulta diverso da "+pod+". Numero fornitura trovato : "+numeroFornitura.getText());
	    if(!indirizzofornitura.getText().contains(indirizzo)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite l'indirizzo risulta diverso da "+indirizzo);
		if(!podIstat.getText().contentEquals(istat)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di POD ISTAT risulta diverso da "+istat);
		if(!offertabilita.getText().contentEquals(off)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+off);
	}
	
	public void verificaTabellaFornitureInseriteGas(String commodity,String pod, String indirizzo, String istat, String off) throws Exception{
		
//		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[2]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//*[@id='supplyConfigDataTable']//table//div[text()='GAS']"));
//		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table//div[text()='GAS']/ancestor::tr/td[3]/div/div"));
//		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[4]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table//div[text()='GAS']/ancestor::tr/td[4]/div/div"));
//		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td[6]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table//div[text()='GAS']/ancestor::tr/td[6]/div/div"));
       
		if(!tipoCommodity.getText().contentEquals(commodity)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il tipo Commodity risulta diverso da "+commodity);
		if(!numeroFornitura.getText().contentEquals(pod)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il pod risulta diverso da "+pod+". Numero fornitura trovato : "+numeroFornitura.getText());
	    if(!indirizzofornitura.getText().contains(indirizzo)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite l'indirizzo risulta diverso da "+indirizzo);
		if(!offertabilita.getText().contentEquals(off)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+off);
	}
	
//	public void verificaTabellaFornitureInseriteMulti(String commodity,String pod, String indirizzo, String istat, String off) throws Exception{
//		
//		try {
//			util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		} catch (Exception e) {
//			throw new Exception("Il pod:"+pod+" non esiste nella tabella delle forniture");
//		}
//		System.out.println("POD attuale:"+pod);
//		
//		WebElement tipoCommodity = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[2]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
////		WebElement numeroFornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[3]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		WebElement indirizzofornitura = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[4]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//		WebElement offertabilita = util.waitAndGetElement(By.xpath("//table[contains(@id,'SupplyConfigDataTable')]//tr[contains(@class,'slds-is-selected')]//td//span[contains(text(),'"+pod+"')]/ancestor::tr//td[6]//span[@id='simpleTextId']/span[@data-aura-class='uiOutputText']"));
//        
//		if(!tipoCommodity.getText().contentEquals(commodity)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il tipo Commodity risulta diverso da "+commodity);
////		if(!numeroFornitura.getText().contentEquals(pod)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il pod risulta diverso da "+pod+". Numero fornitura trovato : "+numeroFornitura.getText());
//	    if(!indirizzofornitura.getText().contains(indirizzo)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite l'indirizzo risulta diverso da "+indirizzo);
//		if(!offertabilita.getText().contentEquals(off)) throw new Exception("Dopo aver inserito il POD nella tabella forniture inserite il valore di OFFERTABILITA risulta diverso da "+off);
//	}

	public void confermaPod(By confermaPod) throws Exception {
		util.objectManager(confermaPod, util.scrollAndClick);
		//util.waitAndGetElement(confermaPod).click();
		TimeUnit.SECONDS.sleep(10);
		spinnerManager.checkSpinners();
	}

	public void verificaIndirizzo(By verifica) throws Exception {
		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
			util.objectManager(verifica,util.scrollAndClick);
			spinnerManager.checkSpinners();
		}
		try {
			util.waitAndGetElement(modificaButton, 20, 1); // verifica se esiste il bottone "Modifica Indirizzo"
		} catch (Exception e) {
			throw new Exception("L'indirizzo di fornitura non è stato validato");
		}
	}

	public void verificaIndirizzoNew(String Cap) throws Exception {
		WebElement prov = driver.findElement(insertProvincia);
		if (prov.isEnabled()) {
//			util.objectManager(verifica,util.scrollAndClick);
			util.objectManager(verificaIndirizzoButton,util.scrollAndClick);
			spinnerManager.checkSpinners();
//			TimeUnit.SECONDS.sleep(6);
		}
		try {
			forzaIndirizzoSWAEVO(Cap);
			util.waitAndGetElement(modificaButton, 20, 1); // verifica se esiste il bottone "Modifica Indirizzo"
		} catch (Exception e) {
			throw new Exception("L'indirizzo di fornitura non è stato validato");
		}
	}
	
	
	public void confermaForniture(By conferma) throws Exception {
		util.objectManager(conferma, util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void creaOfferta(By crea) throws Exception {
		util.objectManager(crea,util.scrollAndClick);
		spinnerManager.checkSpinners();
	}

	public void okPopup(By ok) throws Exception {
		util.waitAndGetElement(ok).click();
		spinnerManager.checkSpinners();
	}
	

	public void forzaIndirizzoSWAEVO(String Cap) throws Exception{
		// Bottone Modifica Indirizzo
		WebElement verificaButton = driver.findElement(verificaIndirizzoButton);
//		if (verificaButton.isEnabled()) {
		if (verificaButton.isDisplayed()) {
			// Salva il CAP
			WebElement element1 = util.waitAndGetElement(inputCapLavori,true);
			String leggiCap = element1.getAttribute("value");
			// Salva il nome del Comune
			element1 = util.waitAndGetElement(nomeComuneLavori,true);
			String nomeCitta = element1.getAttribute("value");
			
			//Click sul Bottone Switch indirizzo non forzato
			util.objectManager(buttonIndirizzoNonForzatoLavori, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);
		
			//Inserimento CAP
			if(leggiCap.isEmpty()) {
				util.objectManager(inputCapLavori, util.scrollAndSendKeys, Cap);
			} else {
				util.objectManager(inputCapLavori, util.scrollAndSendKeys, leggiCap);
			}
			TimeUnit.SECONDS.sleep(5);
			pressJavascript(capSpan);
			//Forza indirizzo
			util.objectManager(forzaIndirizzoButtonInserimentoLavori, util.scrollAndClick);
//			spinnerManager.checkSpinners();
			TimeUnit.SECONDS.sleep(6);
	//			// *** Se appare la schermata Seleziona ISTAT ***
	//			WebElement istat = util.waitAndGetElement(codiceIstat,true);
	//			if (istat.isDisplayed()) {
	//				util.objectManager(radioIstat, util.scrollAndClick);
	//				TimeUnit.SECONDS.sleep(5);
	//				util.objectManager(buttonConfermaIstat, util.scrollAndClick);
	//			}
			
		}
		
	}
	
	public void pressJavascript(By oggetto) throws Exception{
		WebElement element = util.waitAndGetElement(oggetto);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}

	
}
