package com.nttdata.qa.enel.components.lightning;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GetAccountFromComponent {
	WebDriver driver;
	public SeleniumUtilities util;

	public By searchBar = By.xpath("//div[contains(@class, 'forceSearchInputDesktopPillWrapper')]/following-sibling::div//input");
	public By accountLink = By.xpath("//a[text()='Contatti']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']//tbody//th[1]//a");
	public By itemClick = By.xpath("(//table)[2]/tbody/tr/th[@scope='row']//a");
	public String accountEmail = "//p[contains(text(),'Email')]/..//*[text()='$email$']";
	public By attivaLink = By.xpath("//span[text()='Attività']/parent::a");
	
	//
	public By clientiaccountLink = By.xpath("//a[text()='Clienti']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']//tbody//th[1]//a");
	public By dettagliLink = By.xpath("//a[text()='Dettagli']"); 
	public By dettagliEmailClienteLink = By.xpath("//span[text()='Email Cliente']/parent::div//following-sibling::div"); 
	public By dettagliEmailClienteLinkNew = By.xpath("//div[@id='sectionContent-966']/div/slot/force-record-layout-row[2]/slot/force-record-layout-item[2]/div/div/div[2]"); 
	public static final String DettagliEmailClienteLink = "m_angelacaponetto@tiscali.it";
	public String accountEmailNew = "//*[contains(text(),'$email$')]";
	
	public By clientiNome = By.xpath("//*[text()='Clienti']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']/descendant::span[@class='slds-grid slds-grid--align-spread']/child::a");
	
	public GetAccountFromComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}	

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void searchAccount(By by, String filter) throws Exception{
		WebElement we = driver.findElement(by);
		we.sendKeys(filter);
	}
	
	public void searchAccount(By by, String filter, boolean newLine) throws Exception{
		Thread.sleep(5000);
		WebElement we = driver.findElement(by);
		if(newLine){
//			we.sendKeys(filter+"\n");
			we.sendKeys(filter+Keys.ENTER);
		}
		else{
			we.sendKeys(filter);
		}
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	public void jsClickComponent(By by) throws Exception{
		util.jsClickElement(by);
	}
}
