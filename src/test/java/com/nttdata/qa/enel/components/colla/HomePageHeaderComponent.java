package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePageHeaderComponent extends BaseComponent {
	public By luceGasBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(1) > a");
	public By enterpriseBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(2) > a");
	//public By loyaltyBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(3) > a");
	public By storiesBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(3) > a");
	public By futurE_Btn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(4) > a");
	public By mediaBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(5) > a");
	public By supportBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__links.dotcom-header__links-custom > ul > li:nth-child(6) > a");
	public By loginMenuBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__btns > button.dotcom-header__btn.btn-user-open > span");
	public By searchMenuBtn = By.cssSelector("#globalHedaer > div > div.dotcom-header__btns > button.dotcom-header__btn.btn-search-open > span");
	public By hamburgerMenu = By.cssSelector("#globalHedaer > div > div.dotcom-header__btns > button.dotcom-header__btn.btn-menu > span");
	public By fibreSelectionBtn = By.xpath("//a[contains(text(),'FIBRA DI MELITA') and @href='https://www-coll1.enel.it/it/fibra.html']");
	//public By englishSelectionButton = By.cssSelector("body > div:nth-child(7) > div.dotcom-megamenu.module.module--initialized > nav > div.dotcom-megamenu__nav__header > ul > li > a");
	public By englishSelectionButton = By.xpath("//a[@alt='en']");
	public HomePageHeaderComponent(WebDriver driver) {
		super(driver);
	}

}
