package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ConfermaPreventivoComponent {
	WebDriver driver ;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public final By finalButton = By.id("j_id0:formId:btnConfirmFinal");
	public final By finalButton2 = By.xpath("//div[@id='j_id0:formId:footerId']//input[@value='CONFERMA']");
	public final By finalButton3 = By.id("j_id0:principalForm:finalConfirm");
	public final By finalButton4 = By.xpath("//button[text()='Continua'][1]");
	public final By buttonContinuaVolturaAccollo = By.xpath("//input[@value='Conferma']");
	public final By inputClassificazionePreventivo =By.xpath("//input[@name='ClassificazionePreventivo']");
	public final By inputClassificazionePreventivo2 = By.xpath("//span[contains(@id,'quoteClass:')]");
	public final By inputClassificazionePreventivo3 = By.xpath("//label[text()='Classificazione Preventivo']/ancestor::div[1]//input");
	public final By selectCanaleInvioRichiesta = By.xpath("//select[@class='slds-input' and contains(@id,'quoteDataContainer')]");
	public final By selectStatoRichiestaPreventivo = By.xpath("//span[text()='Stato Richiesta']/ancestor::div[1]//select");
	public final By selectStatoRichiestaPreventivo2 = By.xpath("//select[contains(@id,'quoteRequests:')]");
	public final By selectStatoRichiestaPreventivo3 = By.xpath("//label[text()='Stato Ordine']/ancestor::div[1]//select");

	final By confermaPreventivoBtn = By.id("j_id0:formId:confirmFooterBtn");
	public final By confermaPreventivoBtn2=By.xpath("//button[text()='Conferma Preventivo'] | //input[@value='CONFERMA PREVENTIVO'] | //button[contains(text(),'Conferma')]");
	public final By confermaPreventivoBtn3=By.xpath("//input[@value='CONFERMA PREVENTIVO']");
	final By tornaAllOfferta = By.xpath("//button[@id='confirmHeaderBtn']");
	public final By verificaCvp = By.xpath("//div[@id='smartScriptKoCvpModalWithButtons']//button");
	final By inviaPreventivo = By.xpath("//button[@id='buttonSendPreventivo']");
	final By modalOkPreventivo = By.xpath("//button[@id='theModalCmpBtn_A']");
	final By buttonCalcolaPreventivo =  By.xpath("//button[normalize-space(text())='Calcola']");
	final By calcolaCodiceUfficioButton = By.xpath("//button[@name='calculateButton']");
	final By okButtonFatturazioneElettronica = By.xpath("//button[@title='OK']");
	final By nextButtonFatturazioneElettronica = By.xpath("//button[@name='nextButton']");
	public final By paginaGestionePreventivo=By.xpath("//div[contains(@id,'Dettaglio Preventivo')]");
	public final By tabellaRiepilogoOrdini = By.xpath("//table[@title='Tabella Riepilogo ordini']");
	public final By colonnaPreventivo=By.xpath("//table[@title='Tabella Riepilogo ordini']/tbody/tr/td[8]");
	public final By linkGestionePreventivo=By.xpath("//a[contains(@id,'preventivoPageLink')]");
	
	public ConfermaPreventivoComponent(WebDriver driver){
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}

	
	public void selezionaCanaleInvioPreventivo(String canaleInvio) throws Exception{
		String frame = util.getFrameByIndex(1);
		Logger.getLogger("").log(Level.INFO,frame);
		util.frameManager(frame, selectCanaleInvioRichiesta, util.select, canaleInvio);
		spinner.checkSpinners(frame);
	}
	
	public void confermaPreventivo(By finalb) throws Exception {
		driver.switchTo().defaultContent();
		String frame = util.getFrameByIndex(1);
		Logger.getLogger("").log(Level.INFO,frame);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		spinner.checkSpinners(frame);
		util.frameManager(frame, finalb, util.scrollAndClick);
		spinner.checkSpinners(frame);
		//Verifica CVP
		Logger.getLogger("").log(Level.INFO,"Ricerca popup CVP");
		verificaPopUpCVP(frame,verificaCvp);
		
		By disapperingFrame = By.xpath("//iframe[@title ='accessibility title' "
				+ "and @name ='" + frame +"']");
		
//		try { 
//			util.frameManager(frame, verificaCvp, util.scrollAndClick);
//			util.frameManager(frame, finalButton, util.scrollAndClick);
//		} catch(Exception e) {}

		


		//System.out.println("waitin for " + disapperingFrame );
		wait.until(
				ExpectedConditions.not(
						ExpectedConditions.presenceOfAllElementsLocatedBy(disapperingFrame)
						)
				);
		//System.out.println("frame not found " + disapperingFrame );
		// spinner.waitForSpinnerByStyleDiplayNone(frame,spinner.tinySpinner);

	}
	
	public void confermaPreventivo(String frame, By finalb) throws Exception {
		spinner.checkSpinners(frame);
		Logger.getLogger("").log(Level.INFO,frame);
		util.frameManager(frame, finalb, util.scrollAndClick);
		spinner.checkSpinners(frame);
		TimeUnit.SECONDS.sleep(3);
	}


	public void confermaPreventivoNoFrame(By finalb) throws Exception {
		spinner.checkSpinners();
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(finalb, util.scrollAndClick);
		spinner.checkSpinners();
	}
	public void confermaPreventivoInFrame(String frame,By finalb) throws Exception {
		spinner.checkSpinners();
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, finalb, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void confermaPreventivo2() throws Exception{
		String frame = util.getFrameByIndex(1);
		Logger.getLogger("").log(Level.INFO,frame);
		util.frameManager(frame, tornaAllOfferta, util.scrollAndClick);
	}
	
	public void confermaPreventivo2(String frame) throws Exception{
		Logger.getLogger("").log(Level.INFO,frame);
		util.frameManager(frame, tornaAllOfferta, util.scrollAndClick);
	}

	public void inviaPreventivo(String predeterminabile) throws Exception {
		By fattEle = By.xpath("//span[contains(@title,'Fatturazione Elettronica')]");
	
//		if (util.exists(fattEle, 30)){
		if (util.exists(fattEle, 30)&& driver.findElement(fattEle).isDisplayed()){
		util.waitAndGetElement(calcolaCodiceUfficioButton).click();
		spinner.checkSpinners();
		util.waitAndGetElement(okButtonFatturazioneElettronica).click();
		util.waitAndGetElement(nextButtonFatturazioneElettronica).click();
		spinner.checkSpinners();
		}
		try {
			
		util.waitAndGetElement(modalOkPreventivo).click();
		}
		catch (Exception e) {
			if (util.exists(By.id("errorBannerMex"),5)) {
				String error = driver.findElement(By.id("errorBannerMex")).getText();
				throw new Exception("Impossibile generare il preventivo, errore generato : " + error);
			}
		}
		TimeUnit.SECONDS.sleep(2);
		util.scrollAndClick.accept(
		util.waitAndGetElement(inviaPreventivo));
		TimeUnit.SECONDS.sleep(2);
		spinner.checkSpinners();
//		if (util.exists(By.id("theModalCmpBtn_A"),5))
		if (util.exists(By.id("theModalCmpBtn_A"),15) && driver.findElement(By.id("theModalCmpBtn_A")).isDisplayed())
			util.objectManager(modalOkPreventivo,util.scrollAndClick);
//			throw new Exception("La richiesta di preventivo è terminata in modo anomalo. testo della pop-up :"+
//					"La richiesta ha ricevuto la seguente rilavorazione: \"NA_Il codice POD non esiste o non corretto\". "
//					+ "Si prega di rilavorare l’attività di scarto"
//					);
		if (predeterminabile.equalsIgnoreCase("y"))
		util.objectManager(
				By.xpath("//button[@class='slds-button slds-button_brand' and contains(text(),'Conferma' )]"),
				util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void verificaClassificazionePreventivo(String preventivoAtteso, By inputClassificazionePreventivo) throws Exception{
		if(!util.exists(inputClassificazionePreventivo, 60)){
			throw new Exception("La pagina di dettaglio preventivo non è stata visualizzata, controllare dati input(cliente e/o indirizzo di esecuzione lavori)");
		}

	   String dettaglioPreventivo=driver.findElement(inputClassificazionePreventivo)
			   .getAttribute("value");
       if(dettaglioPreventivo == null) dettaglioPreventivo = driver.findElement(inputClassificazionePreventivo).getText();
		//		//System.out.println("Valore campo2:"+dettaglioPreventivo);
		if (preventivoAtteso.compareToIgnoreCase(dettaglioPreventivo)!=0){
			throw new Exception("Il preventivo risulta :"+dettaglioPreventivo+". Preventivo atteso:"+preventivoAtteso);
		}
	}
	
	public void selezionaRichiestaPreventivo(By selectStatoRichiesta, String valore) throws Exception{
		util.objectManager(selectStatoRichiesta, util.scrollAndSelect,valore);
	}
	public void selezionaRichiestaPreventivoInFrame(String frame,By selectStatoRichiesta, String valore) throws Exception{
		util.frameManager(frame, selectStatoRichiesta, util.scrollAndSelect, valore);
	}
	
	public boolean verificaCaricamentoGestionePreventivo(By paginaGestionePreventivo) throws Exception{
		boolean flag=false;
		if(util.exists(paginaGestionePreventivo, 40)&& driver.findElement(paginaGestionePreventivo).isDisplayed()){
			flag=true;
			Logger.getLogger("").log(Level.INFO,"Pagina Gestione Preventivo Trovata");
		}
		else{
		Logger.getLogger("").log(Level.INFO,"Pagina Gestione Preventivo NON trovata");
		}
		return flag;
	}
	
	public void apriGestionePreventivoDaRiepilogoOrdini(By tabellaRiepilogoOrdini, By linkPaginaPreventivo, int index) throws Exception{
		String frame = util.getFrameByIndex(index);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement elTabella = util.waitAndGetElement(tabellaRiepilogoOrdini);
		util.scrollToElement(elTabella);
		String valoreColonnaPreventivo=util.getTableCellText(elTabella, 1, "Preventivo", "./thead/tr/th", "./tbody/tr", "/td[%d]",0);		
		Logger.getLogger("").log(Level.INFO,"Valore colonna preventivo:"+valoreColonnaPreventivo);
//		System.out.println("Valore colonna preventivo:"+valoreColonnaPreventivo);
			if (valoreColonnaPreventivo.compareToIgnoreCase("Gestione Preventivo")==0){
				util.objectManager(linkPaginaPreventivo, util.scrollAndClick);
				spinner.checkSpinners();
				driver.switchTo().defaultContent();
		}
			else{
			throw new Exception("Nel Riepologo ordini, la colonna 'Preventivo' risulta essere vuota. Preventivo non ancora generato.");
		}
	}
	
	
	
	public String verificaClassificazionePreventivoInFrame(String preventivoAtteso, By inputClassificazionePreventivo, int index) throws Exception{
		    String frame = util.getFrameByIndex(index);
		    WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		    verificaClassificazionePreventivo(preventivoAtteso, inputClassificazionePreventivo);
		    driver.switchTo().defaultContent();
		    return frame;
	}
	
	public void verificaPopUpCVP(String frame, By buttonOK) throws Exception{
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try{
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
		if (util.exists(buttonOK, 20)&& driver.findElement(buttonOK).isDisplayed()){
			util.objectManager(buttonOK, util.scrollAndClick);
			Logger.getLogger("").log(Level.INFO,"Popup CVP trovata");
			TimeUnit.SECONDS.sleep(3);}
		else{
			Logger.getLogger("").log(Level.INFO,"Popup CVP NON trovata");
		}
		driver.switchTo().defaultContent();
		}
		catch (Exception e) {
			Logger.getLogger("").log(Level.INFO,"Popup CVP NON trovata");
		};
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	}

	
	

}
