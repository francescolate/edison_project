package com.nttdata.qa.enel.components.colla;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModalErrorComponent extends BaseComponent{
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By close = By.xpath("//button[@class='remodal-close popupLogin-close']");

	
	public ModalErrorComponent (WebDriver driver) throws Exception{
		super();
		
	}

}
