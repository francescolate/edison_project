package com.nttdata.qa.enel.components.lightning;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Step;


public class CercaClienteComponent {

	private WebDriver driver;
	private SeleniumUtilities util;
	public CercaClienteComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}



	private By cf = By.xpath("//input[@id='NE__VAT__c' and @type='text']");

	private By ragSociale = By.xpath("//input[@id='Name' and @type='search']");

	private By searchBtn = By.xpath(".//button[@id='searchClients']");
	
	private By frameBy = By.xpath("//iframe[@title = 'accessibility title']");


	private void swichToDefault () throws Exception{
		this.driver.switchTo().defaultContent();
	}



	@Deprecated
	public void fillCfAndSearch(String param) throws Exception
	{
		TimeUnit.SECONDS.sleep(20);
		String x = util.frameSearcher(
				frameBy,
				cf,
				r->{r.sendKeys(param);
				}
				);
		this.driver.switchTo()
		.frame(x)
		.findElement(searchBtn)
		.click();
		this.swichToDefault();

	}
	
	public void fillCfAndSearch2(String param) throws Exception
	{
		TimeUnit.SECONDS.sleep(5);
		String frame = util.getFrameByIndex(0);
		util.frameManager(frame, cf, util.sendKeys,param);
		util.frameManager(frame, searchBtn, util.scrollAndClick);
	}
	
	/**
	 * Ricerca la Ragione Sociale passata come parametro
	 * @param param
	 * @throws Exception
	 */
	public void fillCfAndSearchRagSociale(String param) throws Exception
	{
		TimeUnit.SECONDS.sleep(15);
		String frame = util.getFrameByIndex(0);
		util.frameManager(frame, ragSociale, util.sendKeys,param);
		util.frameManager(frame, searchBtn, util.scrollAndClick);
	}
}
