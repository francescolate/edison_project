package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TutelaPageComponent extends BaseComponent { 
	public By path1 = By.cssSelector("#general_hero > div > div > nav > ul > li:nth-child(1) > a");
	public By path2 = By.cssSelector("#general_hero > div > div > nav > ul > li:nth-child(2) > a");
	public By path3 = By.cssSelector("#general_hero > div > div > nav > ul > li:nth-child(3) > a");
	public By title = By.cssSelector("#general_hero > div > div > h1");
	public By subtitle = By.cssSelector("#general_hero > div > div > p");
	public By whatItIsBtn = By.xpath("//a[@href='#1']");
	public By economicConditionsBtn = By.xpath("//a[@href='#2']");
	public By priceBtn = By.xpath("//a[@href='#3']");
	public By documentsBtn = By.xpath("//a[@href='#4']");
	public By question1 = By.xpath("//*[@id=\"1\"]/div/section/div/div/h3");
	public By answer1 = By.xpath("//*[@id=\"1\"]/div/section/div/div/p[1]");
	public By question2 = By.xpath("//*[@id=\"1\"]/div/section/div/div/p[2]/b");
	public By answer2 = By.xpath("//*[@id=\"1\"]/div/section/div/div/p[3]");
	public By listPoint1 = By.xpath("//*[@id=\"1\"]/div/section/div/div/ul/li[1]");
	public By listPoint2 = By.xpath("//*[@id=\"1\"]/div/section/div/div/ul/li[2]");
	public By locationLbl1 = By.xpath("//*[@id=\"formWrapper\"]/h3[1]");
	public By locationLbl2 = By.xpath("//*[@id=\"formWrapper\"]/h3[2]");
	public By locationLbl3 = By.xpath("//*[@id=\"formLocation\"]/div[1]/div/label");
	public By inputFld = By.id("tipologia_fornitura");
	public By mandatoryLbl = By.xpath("//*[@id=\"formLocation\"]/div[2]/p[2]");
	public By sendBtn = By.id("btnModify");
	public By sendBtn2 = By.xpath("//*[@id='btnModify']");
	public By errorLbl = By.id("tipologia_fornitura_error");
	public By cityMenu = By.xpath("//ul[contains(@class,'ui-autocomplete') and contains(@id,'ui-id-')]");
	//public By romaMenuItem = By.xpath("//li[@class='ui-menu-item' and text()='ROMA (RM)']");
	public By romaMenuItem = By.xpath("//li[@class='ui-menu-item']//div[text()='ROMA (RM)']");
	public By romaItem = By.xpath("//ul[@id='ui-id-1']//div[text()='ROMA (RM)']");
	public By milanoItem = By.xpath("//ul[@id='ui-id-1']//div[text()='MILANO (MI)']");
	public By napoliItem = By.xpath("//ul[@id='ui-id-1']//div[text()='NAPOLI (NA)']");
	public By cityItem = By.xpath("//ul[@id='ui-id-1']/li[1]");
	
	public By condizEconomGasTitleLbl = By.cssSelector("#step2 > div > h3.plan-main-head.active-bills-header");
	public By locationLbl = By.cssSelector("#step2 > div > h3.plan-main-head.locationTitle");
	public By condizEconomBodyTxt = By.cssSelector("#step2 > div > section:nth-child(3) > div > div");
	public By fasceScaglioniLbl = By.cssSelector("#step2 > div > h3:nth-child(4)");
	public By scaglioniConsumoLbl = By.cssSelector("#step2 > div > h3:nth-child(5)");
	public By componenteTariffariaLbl = By.cssSelector("#step2 > div > h3:nth-child(7)");
	public By altreComponentiLbl = By.cssSelector("#step2 > div > h3:nth-child(9)");
	public By tariffaRisultante = By.cssSelector("#step2 > div > h3:nth-child(10)");
	public By notaSectionLbl = By.cssSelector("#step2 > div > section:nth-child(12)");
	public By areraLink = By.cssSelector("#step2 > div > section:nth-child(12) > div > div > p:nth-child(4) > a");
	public By[] economicConditionsTxtLocators = {
			By.cssSelector("#\\32  > div > section > div > div > h3"),
			By.cssSelector("#\\32  > div > section > div > div > div:nth-child(3)"),
			By.cssSelector("#\\32  > div > section > div > div > p:nth-child(4)"),
			By.cssSelector("#\\32  > div > section > div > div > p:nth-child(6)"),
			By.cssSelector("#\\32  > div > section > div > div > ul:nth-child(8) > li:nth-child(1)"),
			By.cssSelector("#\\32  > div > section > div > div > ul:nth-child(8) > li:nth-child(2)"),
			By.cssSelector("#\\32  > div > section > div > div > ul:nth-child(8) > li:nth-child(3)"),
			By.cssSelector("#\\32  > div > section > div > div > ul:nth-child(8) > li:nth-child(4)"),
			By.cssSelector("#\\32  > div > section > div > div > p:nth-child(10)"),
			By.cssSelector("#\\32  > div > section > div > div > ul:nth-child(11) > li:nth-child(1)"),
			By.cssSelector("#\\32  > div > section > div > div > ul:nth-child(11) > li:nth-child(2)"),
			By.cssSelector("#formWrapper > h3.plan-main-head.active-bills-header"),
			By.cssSelector("#formWrapper > h3:nth-child(3)"),
	};
//	public By milanoMenuItem = By.xpath("//li[@class='ui-menu-item' and text()='MILANO (MI)']");
	public By milanoMenuItem = By.xpath("//li[@class='ui-menu-item']//div[text()='MILANO (MI)']");
	public By[] priceTxtLocators = {
			By.cssSelector("#\\33  > div > section > div > div > h3"),
			By.cssSelector("#\\33  > div > section > div > div > p:nth-child(3)"),
			By.cssSelector("#\\33  > div > section > div > div > ul:nth-child(4) > li:nth-child(1)"),
			By.cssSelector("#\\33  > div > section > div > div > ul:nth-child(4) > li:nth-child(2)"),
			By.cssSelector("#\\33  > div > section > div > div > ul:nth-child(4) > li:nth-child(3)"),
			By.cssSelector("#\\33  > div > section > div > div > p:nth-child(6)"),
			By.cssSelector("#\\33  > div > section > div > div > ul:nth-child(8) > li:nth-child(1)"),
			By.cssSelector("#\\33  > div > section > div > div > ul:nth-child(8) > li:nth-child(2)"),
			By.cssSelector("#\\33  > div > section > div > div > p:nth-child(9)"),
			By.cssSelector("#formWrapper > h3.plan-main-head.active-bills-header"),
			By.cssSelector("#formWrapper > h3:nth-child(3)")
	};
//	public By napoliMenuItem = By.xpath("//li[@class='ui-menu-item' and text()='NAPOLI (NA)']");
	public By napoliMenuItem = By.xpath("//li[@class='ui-menu-item']//div[text()='NAPOLI (NA)']");
	public By[] downloadsTxtLocators = {
			By.cssSelector("#\\34  > div.text.parbase > section > div > div > h3"),
			By.cssSelector("#\\34  > div:nth-child(2) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(2) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(3) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(3) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(4) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(4) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(5) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(5) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(6) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(6) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(7) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(7) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(8) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(8) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#\\34  > div:nth-child(9) > div > div.list-item_text-wrapper > h4 > a"),
			By.cssSelector("#\\34  > div:nth-child(9) > div > div.list-item_file-wrapper > span"),
			By.cssSelector("#formWrapper > h3.plan-main-head.active-bills-header"),
			By.cssSelector("#formWrapper > h3:nth-child(3)")
	};
	public By[] downloadsSubtitlesLocators = {
			By.cssSelector("#\\34  > div:nth-child(2) > div > div.list-item_text-wrapper > p"),
			By.cssSelector("#\\34  > div:nth-child(3) > div > div.list-item_text-wrapper > p")
	};
	public By[] downloadButtonsLocators = {
			By.cssSelector("#\\34  > div:nth-child(2) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(3) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(4) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(5) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(6) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(7) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(8) > div > div.list-item_file-wrapper > a"),
			By.cssSelector("#\\34  > div:nth-child(9) > div > div.list-item_file-wrapper > a")
	};
	
	private By[] pagePresentationElements = {
			path1,
			path2,
			path3,
			title,
			subtitle
	};
	
	private By[] genericContentLocators = {
			question1,
			answer1,
			question2,
			answer2,
			listPoint1,
			listPoint2,
			locationLbl1,
			locationLbl2,
	};
	
	private By[] locationLocators = {
			locationLbl3,
			mandatoryLbl
	};
	
	public TutelaPageComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyPagePresentation() throws Exception {
		verifyElementsArrayText(pagePresentationElements, TutelaPageComponent_Constants.pagePresentationStrings);
	}
	
	public void validateBar() throws Exception {
		verifyVisibilityAndText(whatItIsBtn, TutelaPageComponent_Constants.whatItIs);
		boolean firstButtonIsSelected = Boolean.parseBoolean(driver.findElement(whatItIsBtn).getAttribute("aria-selected"));
		if (!firstButtonIsSelected) {
			throw new Exception("First bar button is not selected");
		}
		verifyVisibilityAndText(economicConditionsBtn, TutelaPageComponent_Constants.economicConditions);
		verifyVisibilityAndText(priceBtn, TutelaPageComponent_Constants.price);
		verifyVisibilityAndText(documentsBtn, TutelaPageComponent_Constants.documents);
	}
	
	private void validateLocationElements() throws Exception {
		verifyElementsArrayText(locationLocators, TutelaPageComponent_Constants.locationStrings);
		verifyComponentVisibility(inputFld);
		verifyAttributeValue(inputFld, "placeholder", "Scrivi la località");
		verifyComponentVisibility(sendBtn);
	}
	
	public void validatePageContent() throws Exception {
		verifyElementsArrayText(genericContentLocators, TutelaPageComponent_Constants.pageContentStrings);
		validateLocationElements();
	}
	
	public void checkEmptyFieldError() throws Exception {
		clickComponent(sendBtn);
		verifyVisibilityAndText(errorLbl, TutelaPageComponent_Constants.errorMessage);
		verifyAttributeValue(errorLbl, "style", "color: red;");
	}
	
	public void selectCity(By locator, String city) throws Exception {
		insertText(inputFld, city);
		Thread.currentThread().sleep(2000);
		switch(city){
			case "Napoli":	jsClickComponent(napoliItem); 	break;
			case "Roma": 	jsClickComponent(romaItem); 	break;
			case "Milano":	jsClickComponent(milanoItem); 	break;
			default:		jsClickComponent(cityItem); 	break;
		}
		Thread.sleep(1000);
//		verifyComponentVisibility(locator);
//		WebElement menu = new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(cityMenu));
//		WebElement cityElement = new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(locator));
//		Actions actionProvider = new Actions(driver);
//		actionProvider.moveToElement(menu).moveToElement(cityElement).build().perform();
//		cityElement.click();
		jsClickComponent(sendBtn2);
	}
	
	public void jsClickComponent(By xpath) throws Exception{
		util.jsClickElement(xpath);
	}
	
	public void checkTextForCity() throws Exception {
		By[] locators = {
				condizEconomGasTitleLbl,
				locationLbl,
				condizEconomBodyTxt,
				fasceScaglioniLbl,
				scaglioniConsumoLbl
		};
		String[] stringsToCheck = {
				TutelaPageComponent_Constants.condizEconomGasTitle,
				TutelaPageComponent_Constants.location,
				TutelaPageComponent_Constants.condizEconomBody,
				TutelaPageComponent_Constants.fasceScaglioni,
				TutelaPageComponent_Constants.scaglioniConsumo
		};
		verifyElementsArrayText(locators, stringsToCheck);
	}
	
	private By[][] getScaglioniConsumoLocators() {
		String temp;
		ArrayList<By[]> table = new ArrayList<By[]>();
		//Adding header row
		ArrayList<By> header = new ArrayList<By>();
		for (int i = 0; i < 3; i++) {
			temp = TutelaPageComponent_Constants.scaglioniTableHeaderElements.replace("#", String.valueOf(i+1));
			header.add(By.xpath(temp));
		}
		table.add(header.toArray(new By[header.size()]));
		//Adding elements
		for (int i = 2; i <= 9; i++) {
			ArrayList<By> row = new ArrayList<By>();
			for (int j = 0; j < 3; j++) {
				temp = TutelaPageComponent_Constants.scaglioniTableBodyElements.replace("#", String.valueOf(i)).replace("§", String.valueOf(j+1));
				row.add(By.xpath(temp));
			}
			table.add(row.toArray(new By[row.size()]));
		}
		
		return table.toArray(new By[table.size()][3]);
	}
	
	public void checkScaglioniConsumo() throws Exception {
		verifyTable(getScaglioniConsumoLocators(), TutelaPageComponent_Constants.scaglioniTableStrings);
	}
	
	private By[][] getComponenteTariffariaLocators() {
		String temp;
		ArrayList<By[]> table = new ArrayList<By[]>();
		//Adding header rows
		ArrayList<By> header1 = new ArrayList<By>();
		ArrayList<By> header2 = new ArrayList<By>();
		for (int i = 0; i < 4; i++) {
			temp = TutelaPageComponent_Constants.compTarifHeader1Elements.replace("#", String.valueOf(i+1));
			header1.add(By.xpath(temp));
			temp = TutelaPageComponent_Constants.compTarifHeader2Elements.replace("#", String.valueOf(i+1));
			header2.add(By.xpath(temp));
		}
		table.add(header1.toArray(new By[header1.size()]));
		table.add(header2.toArray(new By[header2.size()]));
		//Adding elements
		for (int i = 3; i <= 10; i++) {
			ArrayList<By> row = new ArrayList<By>();
			for (int j = 0; j < 4; j++) {
				temp = TutelaPageComponent_Constants.compTarifBodyElements.replace("#", String.valueOf(i)).replace("§", String.valueOf(j+1));
				row.add(By.xpath(temp));
			}
			table.add(row.toArray(new By[row.size()]));
		}

		return table.toArray(new By[table.size()][4]);
	}
	
	public void checkComponenteTariffaria(String city) throws Exception {
		verifyVisibilityAndText(componenteTariffariaLbl, TutelaPageComponent_Constants.componenteTariffaria);
		String[][] stringsArray = new String[10][4];
		switch (city) {
		case "Roma":
			stringsArray = TutelaPageComponent_Constants.compTarifTableStrings_Roma;
			break;
		case "Milano":
			stringsArray = TutelaPageComponent_Constants.compTarifTableStrings_Milano;
			break;
		case "Napoli":
			stringsArray = TutelaPageComponent_Constants.compTarifTableStrings_Napoli;
			break;
		}
		verifyTable(getComponenteTariffariaLocators(), stringsArray);
	}
	
	private By[][] getAltreComponentiLocators() {
		String temp;
		ArrayList<By[]> table = new ArrayList<By[]>();
		//Adding header rows
		ArrayList<By> header1 = new ArrayList<By>();
		ArrayList<By> header2 = new ArrayList<By>();
		ArrayList<By> header3 = new ArrayList<By>();
		for (int i = 0; i < 6; i++) {
			temp = TutelaPageComponent_Constants.altreCompHeader1Elements.replace("#", String.valueOf(i+1));
			header1.add(By.xpath(temp));
			temp = TutelaPageComponent_Constants.altreCompHeader2Elements.replace("#", String.valueOf(i+1));
			header2.add(By.xpath(temp));
			temp = TutelaPageComponent_Constants.altreCompHeader3Elements.replace("#", String.valueOf(i+1));
			header3.add(By.xpath(temp));
		}
		table.add(header1.toArray(new By[header1.size()]));
		table.add(header2.toArray(new By[header2.size()]));
		table.add(header3.toArray(new By[header3.size()]));
		//Adding elements
		for (int i = 4; i <= 11; i++) {
			ArrayList<By> row = new ArrayList<By>();
			for (int j = 0; j < 6; j++) {
				temp = TutelaPageComponent_Constants.altreCompBodyElements.replace("#", String.valueOf(i)).replace("§", String.valueOf(j+1));
				row.add(By.xpath(temp));
			}
			table.add(row.toArray(new By[row.size()]));
		}

		return table.toArray(new By[table.size()][6]);
	}
	
	public void checkAltreComponenti(String city) throws Exception {
		verifyVisibilityAndText(altreComponentiLbl, TutelaPageComponent_Constants.altreComponenti);
		verifyVisibilityAndText(tariffaRisultante, TutelaPageComponent_Constants.tariffaRisultante);
		String[][] stringsArray = new String[11][6];
		switch (city) {
		case "Roma":
			stringsArray = TutelaPageComponent_Constants.altreComponentiTableStrings_Roma;
			break;
		case "Milano":
			stringsArray = TutelaPageComponent_Constants.altreComponentiTableStrings_Milano;
			break;
		case "Napoli":
			stringsArray = TutelaPageComponent_Constants.altreComponentiTableStrings_Napoli;
			break;
		}
		verifyTable(getAltreComponentiLocators(), stringsArray);
	}
	
	public void checkNota() throws Exception {
		verifyVisibilityAndText(notaSectionLbl, TutelaPageComponent_Constants.nota);
	}
	
	public void openAreraSite() throws Exception {
		clickComponent(areraLink);
		confirmUrl(TutelaPageComponent_Constants.areraRedirectUrl);
	}
	
	public void checkEconomicConditions() throws Exception {
		verifyComponentVisibility(economicConditionsBtn);
		clickComponent(economicConditionsBtn);
		verifyElementsArrayText(economicConditionsTxtLocators, TutelaPageComponent_Constants.economicConditionsStrings);
		validateLocationElements();
	}
	
	public void checkPriceElements() throws Exception {
		verifyComponentVisibility(priceBtn);
		clickComponent(priceBtn);
		verifyElementsArrayText(priceTxtLocators, TutelaPageComponent_Constants.priceStrings);
		validateLocationElements();
	}
	
	public void checkDocumentsElements() throws Exception {
		verifyComponentVisibility(documentsBtn);
		clickComponent(documentsBtn);
		verifyElementsArrayText(downloadsSubtitlesLocators, TutelaPageComponent_Constants.downloadSubtitleStrings);
		Color greyColour = Color.fromString("rgb(179, 179, 179)");
		for (By locator:downloadsSubtitlesLocators) {
			verifyElementColor(locator, greyColour);
		}
		for (By locator:downloadButtonsLocators) {
			verifyComponentVisibility(locator);
		}
		verifyElementsArrayText(downloadsTxtLocators, TutelaPageComponent_Constants.downloadStrings);
		validateLocationElements();
	}
	
	public void checkPdfLinks() throws Exception {
		int i=1;
		String xpath = "//div[@class='asset-item parbase'][$]//div[@class='list-item_file-wrapper']//a";
		for (i = 1; i <= downloadButtonsLocators.length; i++) {
			//clickComponent(downloadButtonsLocators[i]);
			clickComponent(By.xpath(xpath.replace("$", String.valueOf(i))));
			ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs2.get(1));
		    System.out.println(i + " --> " + TutelaPageComponent_Constants.downloadLinks[i]);
		    confirmUrl(TutelaPageComponent_Constants.downloadLinks[i]);
		    driver.close();
		    driver.switchTo().window(tabs2.get(0));
		}
	}
 
}
