package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ConfirmationCallComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public ConfirmationCallComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver,this);
		util=new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	public final By QCButton = By.xpath("//button[text()='Esegui Quality Call']");
	public final By CCButton = By.xpath("//button[text()='Esegui Confirmation Call']");
	public final By CCTerminaChiamata = By.xpath("//button[@name='ITA_IFM_Call_History__c.Termina_Chiamata']");
	public final By CCConferma = By.xpath("//h2[text()='Chiamata terminata']/ancestor::div//input[@value='Conferma']");
	public final By CCEsitoChiamata = By.xpath("//h2[text()='Chiamata terminata']/ancestor::div//select");
	
	public void selectOption(By oggetto, String val) throws Exception{
		Thread.currentThread().sleep(30000);
		driver.switchTo().defaultContent();
		boolean flag = false;
		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		WebElement element = null;
		for (WebElement webElement : els) {
			driver.switchTo().frame(webElement);
			try {
			 element = driver.findElement(CCEsitoChiamata);
			 flag = true;
			 break;
			}catch(Exception e) {
				driver.switchTo().defaultContent();
			}
		}
		if(!flag) {
			throw new Exception("Esito Termina Chiamata non trovato.");
		}
		WebElement temp = driver.findElement(CCEsitoChiamata);
		Select select = new Select(temp);
		select.selectByVisibleText(val);
	}
	
	public void selezionaTerminaChiamataVisibile(By oggetto) throws Exception{
		//ce ne sono 2 in pagina 1 non visibile.
		Thread.currentThread().sleep(30000);
		List<WebElement> els = driver.findElements(oggetto);
		boolean flag = false;
		for (WebElement webElement : els) {
			if(webElement.isDisplayed()) {
				try {
				webElement.click();
				flag = true;
				}catch(Exception e) {}
				
			}
		}
		if(!flag) {
			throw new Exception("Pulsante Termina Chiamata non trovato.");
		}
		
	}
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void conferma(By oggetto)throws Exception{
		util.getFrameActive();
		
		driver.findElement(oggetto).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(oggetto).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(oggetto).sendKeys(Keys.ENTER);
//		util.objectManager(oggetto, util.scrollToVisibility, false);
		
//		driver.switchTo().defaultContent();
//		WebElement ret = null;
//		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
//		boolean flag = false;
//		for (WebElement webElement : els) {
//			
//			
//			if(webElement.findElement(By.xpath("..")).isDisplayed()&&webElement.findElement(oggetto).isDisplayed()) {
//				driver.switchTo().frame(webElement);
//				ret = webElement;
//				break;
//			}	
//			
//		}
//		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}

}

