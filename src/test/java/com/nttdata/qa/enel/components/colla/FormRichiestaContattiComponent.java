package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

public class FormRichiestaContattiComponent extends BaseComponent{
	
	public By button_contatti = new ByChained(By.id("contattiAct") , By.tagName("a"));
	public By text_path = By.className("breadcrumb");
	public By text_titolo = By.tagName("h1");
	public By text_lead = By.className("lead");
	public By id_richiesta = By.id("prefrichiesta_key");
	public By select_interessi = By.className("customselect-choice");
	public By full_select_richiesta_interessi = new ByChained(id_richiesta , select_interessi);
	public By id_offerta = By.id("prefofferta_key");
	public By select_offer_type = By.className("customselect-choice");
	public By full_select_activation_interessi = new ByChained(id_offerta , select_offer_type);
	public By offer_luce = new ByChained(select_offer_type , By.xpath("//input[@name='tipologiaofferta']"), By.xpath("//span[text()='Luce']"));
	public By text_provincia = By.id("Provincia");
	public By first_result_city = By.id("ui-id-1");
	public By id_contact = By.id("prefcontact_key");
	public By select_contact = By.className("customselect-choice");
	public By full_select_contact = new ByChained(id_contact , select_contact);
	public By contact_email = new ByChained(select_offer_type , By.xpath("//input[@name='tipologiaofferta']"), By.xpath("//span[text()='Email']"));
	public By text_email = By.id("email");
	public By radio_block = By.xpath("//label[@for='privacy-accetto']");
	public By radio_button_accetto = new ByChained(radio_block , By.xpath("./span"));
	public By button_invia = By.id("submitContatti");
	public By button_fine = By.name("Fine");
	public By avatar = By.id("avatarUtente");

	public FormRichiestaContattiComponent(WebDriver driver) {
		super(driver);
	}

}
