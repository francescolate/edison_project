package com.nttdata.qa.enel.components.lightning;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Time;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.testqantt.VerificaIndirizziDomicilio;
import com.nttdata.qa.enel.util.PartitaIva;
import com.nttdata.qa.enel.util.SeleniumUtilities;

//Il seguente componente si occupa della gestione della creazione di una nuova anagrafica dal tab Clienti
//Gestisce la creazione di nuove anagrafiche Residenziali, Business, Condominio e Impresa Individuale
public class CreaNuovoClienteComponent {
    private WebDriver driver;
    private SeleniumUtilities util;
    String frameName;
    public CreaNuovoClienteComponent(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
    util=new SeleniumUtilities(driver);
    
  }
 
    
    @FindBy(xpath = "//div[@title='Nuovo' and (text() = 'Nuovo')]")
    private WebElement nuovoClienteButton;
    
 
  
    
    public void setFrameName(String frame) throws Exception{
    	this.frameName = frame;
    }
    
    public final By selectTipologiaCliente = By.xpath("//div[@id='tipologiaCliente']//label[text()='Tipologia Cliente']/..//select[contains(@id,'tipologiaCliente')]");
    public final By radioButtonResidenziale = By.xpath("//label[@for='ITA_IFM_Residential']/span[@class='slds-radio--faux']");
    public final By radioButtonImpresaIndividuale = By.xpath("//label[@for='ITA_IFM_ImpresaIndividuale']/span[@class='slds-radio--faux']");
    public final By radioButtonCondominio = By.xpath("//label[@for='ITA_IFM_Condominio']/span[@class='slds-radio--faux']");
    public final By radioButtonBusiness = By.xpath("//label[@for='ITA_IFM_Business']/span[@class='slds-radio--faux']");
    private final By confirmButton = By.xpath("//button[@id='modalNewAccountNext']");
    public final By nuovoClienteDaInterazioniButton = By.xpath("//div[@id='AccountSearchCard']//button[text()='Nuovo Cliente' and contains(@class,'newAccountBtn')]");

   private final By cellulareField = By.xpath("//input[contains(@id,'valorecellulare')]");
   private final By descrizioneCellulareField = By.xpath("//select[contains(@id,'descrizionecellulare')]");
	// nuovi campi email  
   private final By emailField = By.xpath("//label[@for='valoreemail']//../div/input");
   private final By descrizioneEmailField = By.xpath("//label[@for='descrizioneemail']//../div/div/select");
   
   private final By inputNome = By.xpath("//input[contains(@id,'FirstName')]");
   private final By inputCognome = By.xpath("//input[contains(@id,'LastName')]");
   private final By inputSesso = By.xpath("//select[contains(@id,'sesso')]");
   private final By inputCF = By.xpath("//input[contains(@id,'CodiceFiscale')]");
   private final By inputDataNascita = By.xpath("//input[contains(@id,'datePicker')]");
   private final By inputComuneNascita = By.xpath("//input[contains(@id,'lookupComuneDiNascita')]");
   private final By inputPartitaIva = By.xpath("//input[contains(@id,'partitaIVA')]");
   private final By inputRagioneSociale = By.xpath("//input[contains(@id,'nomeClienteRagioneSociale')]");
   private final By inputAccount = By.xpath("//input[contains(@id,'codiceFiscaleAccount')]");
   private final By selectTipoFormaGiuridica = By.xpath("//select[contains(@id,'TipoFormaGiuridica')]");
   private final By selectRuoloReferente = By.xpath("//select[contains(@id,'ruolo')]");
   public final By selectRuoloReferente2 = By.xpath("//select[contains(@id,'ruoloContact')]");
   
   public final By checkDipendenteEnel = By.xpath("//legend[text()='Dipendente Enel']/../..//label[@class='slds-checkbox']");
		   
   private final By calcolaCodiceFiscaleButton = By.xpath("//button[text()='Calcola Codice Fiscale']");

   private final By confermaButton = By.xpath("//input[contains(@id,'btnConferma') | //button[@value='Conferma' and contains(@class,'saveRecord')]]");
   
   private final By conferma = By.xpath("//p[contains(text(),'Nuovo Cliente')]/ancestor::div[@role='banner']//button[@value='Conferma'] | //input[contains(@id,'confermaButtonTop')]");
   private final By annulla = By.xpath("//p[contains(text(),'Nuovo Cliente')]/ancestor::div[@role='banner']//button[text()='Annulla']");
   private final By continua = By.id("buttonConfirmID_A");

   private final By confermaComuneNascita = By.xpath("//div[@id='lookupItemId']");  
   
	private By  tuttiIProcessi = new By.ByXPath("//button[text() = 'Tutti i processi']");;
	
	public By tendina = By.xpath("//div[@id='lookupItemId']//ul//a");
	public By clienteField = By.id("lookupContattoAccount");
	
	private By twitterAccount = By.xpath("//input[contains(@id,'form:twitterAccount')]");
	private By linkedinAccount = By.xpath("//input[contains(@id,'LinkedInAccount')]");
	private By fbAccount = By.xpath("//input[contains(@id,'FBAccount')]");
	private By webSite = By.xpath("//input[contains(@id,'form:Website')]");
	
	private By titoloStudio = By.xpath("//select[contains(@id,'form:TitoloDiStudio')]");
	private By professione = By.xpath("//select[contains(@id,'form:Professione')]");
	
	
	private void popolaCampiAnagrafica(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception {
		util.frameManager(this.frameName, inputComuneNascita,util.sendKeys,comune);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(this.frameName, confermaComuneNascita, util.scrollAndClick);
		util.frameManager(this.frameName, inputDataNascita,util.scrollAndSendKeys,dataNascita + Keys.RETURN);
		util.frameManager(this.frameName, inputNome,util.scrollAndSendKeys,nome);
		util.frameManager(this.frameName, inputCognome,util.scrollAndSendKeys,cognome);
		util.frameManager(this.frameName, inputSesso, util.scrollAndSelect,sesso);
		util.frameManager(this.frameName, calcolaCodiceFiscaleButton, util.scrollAndClick);
		
	}
	

	public void verificaCalcoloCodiceFiscale(Properties prop) throws Exception {
		this.popolaCampiAnagrafica(prop.getProperty("NOME").trim(),prop.getProperty("COGNOME").trim(),prop.getProperty("SESSO").trim(),prop.getProperty("DATA_NASCITA").trim(),prop.getProperty("COMUNE_NASCITA").trim());
		//this.driver.switchTo().frame(this.frameName);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		String cf = util.waitAndGetElement(inputCF).getAttribute("value");
		prop.setProperty("CODICE_FISCALE", cf);
		if(cf.length()<16) throw new Exception("Dopo la pressione del pulsante Calcola Codice Fiscale, il campo codice fiscale non viene popolato con un codice corretto");
		this.driver.switchTo().defaultContent();
	}
	

	public void verificaCalcoloCodiceFiscaleReferente(Properties prop) throws Exception {
		String nomeReferente =prop.getProperty("NOME_REFERENTE", "").trim(); 
		if (nomeReferente.equalsIgnoreCase("RANDOM"))
		{
			nomeReferente = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME_REFERENTE", nomeReferente);
		}
		this.compilaAnagraficaReferente(nomeReferente,prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"));
		//this.driver.switchTo().frame(this.frameName);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		String cf = util.waitAndGetElement(inputCF).getAttribute("value");
		prop.setProperty("CODICE_FISCALE", cf);
		if(cf.length()<16) throw new Exception("Dopo la pressione del pulsante Calcola Codice Fiscale, il campo codice fiscale non viene popolato con un codice corretto");
		this.driver.switchTo().defaultContent();
	}
	
	private String compilaAnagraficaClienteResidenziale(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception{
		this.popolaCampiAnagrafica(nome, cognome, sesso, dataNascita, comune);
		//this.driver.switchTo().frame(this.frameName);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		String cf = util.waitAndGetElement(inputCF).getAttribute("value");
		this.driver.switchTo().defaultContent();
		return cf;
	}
	
	
	private boolean checkAlertPresence(WebElement alertBar) throws Exception{
		boolean exist = false;
		String style = alertBar.getAttribute("style");
		if(style.contentEquals("display: none;")) exist = false;
		else exist = true;
		return exist;
	}
	
	private boolean checkAlertMessage(String message) throws Exception {
		WebElement errors = util.waitAndGetElement(By.xpath("//span[@id='RedAlertsErrors']"));
		if(errors.getText().contains(message)) return true;
		else return false;
	}
	
	public void fleggaDipendenteEnel(String frameName) throws Exception {
		util.frameManager(frameName, checkDipendenteEnel, util.scrollAndClick);
	}
	
	
	public void verificaClienteGiaPresente() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un cliente gia' presenta a sistema non viene mostrato alcun messaggio di errore");
		else {
			if(!checkAlertMessage("Il codice fiscale inserito è già presente nel sistema") && !checkAlertMessage("La P.Iva inserita è già presente nel sistema") && !checkAlertMessage("Partita Iva e Codice Fiscale già presenti nel sistema")) throw new Exception("Dopo aver inserito un cliente già presente a sistema, vien rilevato un messaggio di errore diverso da : Il codice fiscale (o codice partita iva) inserito è già presente nel sistema");

		}
		this.driver.switchTo().defaultContent();
		
	}
	
	public void verificaObbligatorietaCampiAnagraficaImpresa() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		util.objectManager(conferma, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Il campo ragione sociale è obbligatorio")) throw new Exception("Il campo Ragione Sociale risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo partita IVA è obbligatorio")) throw new Exception("Il campo Partita Iva risulta non essere obbligatorio");
	
			util.objectManager(closeBar, util.scrollAndClick);
			driver.switchTo().defaultContent();
		
		}
		
		
	}
	
	public void verificaObbligatorietaCampiAnagraficaCondominio() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		util.objectManager(conferma, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Il campo ragione sociale è obbligatorio")) throw new Exception("Il campo Ragione Sociale risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo codice fiscale è obbligatorio")) throw new Exception("Il campo Codice Fiscale risulta non essere obbligatorio");
			if(checkAlertMessage("iva") || checkAlertMessage("Iva")) throw new Exception("Il campo Partita Iva risulta essere obbligatorio contrariamente a quanto atteso");

			util.objectManager(closeBar, util.scrollAndClick);
			driver.switchTo().defaultContent();
		
		}
	}
		
		public void verificaObbligatorietaCampiAnagraficaBusiness() throws Exception {
			util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
			WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
			By closeBar = By.xpath("//div[@id='RedAlerts']//button");

			util.objectManager(conferma, util.scrollAndClick);
			if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
			else {
				
				if(!checkAlertMessage("Il campo ragione sociale è obbligatorio")) throw new Exception("Il campo Ragione Sociale risulta non essere obbligatorio");
				if(!checkAlertMessage("Il campo codice fiscale è obbligatorio")) throw new Exception("Il campo Codice Fiscale risulta non essere obbligatorio");
				if(!checkAlertMessage("Il campo tipo forma giuridica")) throw new Exception("Il campo Tipo forma giuridica risulta non essere obbligatorio");
				if(checkAlertMessage("iva") || checkAlertMessage("Iva")) throw new Exception("Il campo Partita Iva risulta essere obbligatorio contrariamente a quanto atteso");
				util.objectManager(closeBar, util.scrollAndClick);

				}

				driver.switchTo().defaultContent();
			
			}
		

	
	
	
	public void verificaObbligatorietaCampiAnagraficaResidenziale() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");
		if(!util.exists(checkDipendenteEnel, 5)) throw new Exception("Impossibile trovare la checkbox Dipendente Enel nella sezione Informazioni Anagrafiche Titolare");
		util.objectManager(calcolaCodiceFiscaleButton, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Nome")) throw new Exception("Il campo Nome risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Cognome")) throw new Exception("Il campo Cognome risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Sesso")) throw new Exception("Il campo Sesso risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Data di nascita")) throw new Exception("Il campo Data di nascita risulta non essere obbligatorio");
			if(!checkAlertMessage("Per calcolare il Codice Fiscale valorizzare il campo Comune di nascita")) throw new Exception("Il campo Comune di nascita risulta non essere obbligatorio");
            
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(inputComuneNascita,util.sendKeys,"NAPOLI");
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(confermaComuneNascita, util.scrollAndClick);
			util.objectManager(inputDataNascita,util.scrollAndSendKeys,"04/03/1985" + Keys.RETURN);
			util.objectManager(inputNome,util.scrollAndSendKeys,"TEST");
			util.objectManager(inputCognome,util.scrollAndSendKeys,"TEST");
			util.objectManager(inputSesso, util.scrollAndSelect,"M");
			util.objectManager(conferma, util.scrollAndClick);
			if(!checkAlertMessage("Errore: Il campo codice fiscale è obbligatorio")) throw new Exception("Il campo Codice Fiscale risulta non essere obbligatorio");
			util.objectManager(closeBar, util.scrollAndClick);
			driver.switchTo().defaultContent();
		
		}
		
		
	}
	
	public void verificaObbligatorietaCampiContatti() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");
		util.objectManager(conferma, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo risulta obbligatorio, impossibile trovare la Red Alert");
		else {
			
			if(!checkAlertMessage("Almeno un contatto telefonico è obbligatorio")) throw new Exception("Durante la creazione di un nuovo cliente senza inserire alcun contatto il sistema non restituisce il messaggio di errore : Almeno un contatto telefonico e' obbligatorio");
		 
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(cellulareField, util.sendKeys,"3958854741");
			util.objectManager(descrizioneCellulareField, util.select,"Privato");
			util.objectManager(conferma, util.scrollAndClick);

			TimeUnit.SECONDS.sleep(10);
			
			if(util.exists(By.xpath("//div[@id='RedAlerts']"), 5)) throw new Exception("Durante la creazione di un nuovo cliente, nonostante l'inserimento di almeno un contatto telefonico il sistema presenta un messaggio di errore");
	
			
			driver.switchTo().defaultContent();
		
		}
		
		
	}
	
	
	
	

	
	
	
	public void verificaTipologiaCliente(String tipologia) throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		Select s = new Select(util.waitAndGetElement(this.selectTipologiaCliente, true));
		WebElement option = s.getFirstSelectedOption();
		String defaultItem = option.getText();
		if(!defaultItem.trim().contentEquals(tipologia)) throw new Exception("La tipologia cliente presente all'interno del campo Tipologia Cliente di default non risulta: "+tipologia+". Valore riscontrato: "+defaultItem);
		driver.switchTo().defaultContent();
	}
	
	
	public void compilaAnagraficaReferente(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception{
		this.compilaAnagraficaClienteResidenziale(nome,cognome,sesso,dataNascita,comune);
	}
	
	public void compilaAnagraficaReferenteConRuolo(String nome, String cognome, String sesso, String dataNascita, String comune,String ruolo) throws Exception{
		util.frameManager(this.frameName, selectRuoloReferente,util.select,ruolo);
		this.compilaAnagraficaClienteResidenziale(nome,cognome,sesso,dataNascita,comune);
	}
	
	public void compilaContatti(String frameName,String cellulare,String descrizioneCellulare) throws Exception {
		util.frameManager(frameName, cellulareField, util.scrollAndClick);
		util.frameManager(frameName, cellulareField, util.sendKeys,cellulare);
		util.frameManager(frameName, descrizioneCellulareField, util.select,descrizioneCellulare);
		
	}
	
	public void compilaContattiEmail(String frameName,String cellulare,String descrizioneCellulare,String email,String descrizioneEmail) throws Exception {
		util.frameManager(frameName, cellulareField, util.scrollAndClick);
		util.frameManager(frameName, cellulareField, util.sendKeys,cellulare);
		util.frameManager(frameName, descrizioneCellulareField, util.select,descrizioneCellulare);
		util.frameManager(frameName, emailField, util.scrollAndClick);
		util.frameManager(frameName, emailField, util.sendKeys,email);
		util.frameManager(frameName, descrizioneEmailField, util.select,descrizioneEmail);
		
	}

	public void confermaCreazione() throws Exception {
		util.frameManager(this.frameName, conferma, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		
	}
	
	public void annullaCreazione() throws Exception {
		util.FrameSwitcher(By.xpath("//iframe[@name='" +this.frameName +"'] | //frame[@name='" +this.frameName +"']"));
		util.objectManager(annulla, util.scrollAndClick);
		util.objectManager(continua, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		if(!util.exists(By.xpath("//div[@title='Nuovo' and (text() = 'Nuovo')]"), 5)) throw new Exception("Dopo aver annullato il processo di creazione cliente, il sistema non torna alla schermata Clienti");
//		if(util.exists(nuovoClienteButton,5)) {}
		driver.switchTo().defaultContent();
	}
	
	
	
	public void vaiADettagliCliente() throws Exception{
		util.objectManager(By.xpath("//a[@data-refid='recordId' and contains(@class,'textUnderline')]"), util.scrollAndClick);
	}
	
	public void verificaCreazione() throws Exception {
		
		try {
			this.driver.switchTo().defaultContent();
			TimeUnit.SECONDS.sleep(10);
			util.waitAndGetElement(tuttiIProcessi,30,1);
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Impossibile creare la nuova anagrafica. Verificare i dati inseriti");
		}
	}
	
	
	
	public void selezionaClienteEsistente(String cliente) throws Exception{
		util.frameManager(this.frameName, clienteField, util.sendKeys,cliente);
		util.frameManager(frameName, tendina, util.scrollAndClick);
		
	}
	
	
	public void compilaAnagraficaImpresaIndividuale(String piva, String ragioneSociale) throws Exception{
		   util.frameManager(this.frameName, inputPartitaIva, util.sendKeys,piva);
		   util.frameManager(this.frameName, inputRagioneSociale, util.sendKeys,ragioneSociale);
	}
	
	public void compilaAnagraficaCondominio(String cf,String piva, String ragioneSociale) throws Exception{
		util.frameManager(this.frameName, inputAccount, util.sendKeys,cf);
		this.compilaAnagraficaImpresaIndividuale(piva, ragioneSociale);
	}
	
	public void compilaAnagraficaBusiness(String cf, String piva, String ragioneSociale, String tipoFormaGiuridica) throws Exception {
		this.compilaAnagraficaCondominio(cf, piva, ragioneSociale);
		util.frameManager(this.frameName, selectTipoFormaGiuridica, util.select,tipoFormaGiuridica);
	}
	
	public String selezionaTipologiaCliente(By tipologia) throws Exception{
		String frame = util.getFrameByIndex(0);
		this.frameName = frame;
		util.frameManager(frame, tipologia, util.scrollAndClick);
		util.frameManager(frame, confirmButton, util.scrollAndClick);
		return frame;
		
	}
	
	public String selezionaTipologiaCliente(int frameId,By tipologia) throws Exception{
		String frame = util.getFrameByIndex(frameId);
		this.frameName = frame;
		util.frameManager(frame, tipologia, util.scrollAndClick);
		util.frameManager(frame, confirmButton, util.scrollAndClick);
		return frame;
		
	}

	public void selezionaTipologia(By tipologia) throws Exception{
		util.frameManager(frameName, tipologia, util.scrollAndClick);
		util.frameManager(frameName, confirmButton, util.scrollAndClick);	
	}
	
	public void nuovoCliente() throws Exception{
		TimeUnit.SECONDS.sleep(7);
		nuovoClienteButton.click();
		TimeUnit.SECONDS.sleep(10);
	}
	public void nuovoClienteDaInterazioni() throws Exception{
		TimeUnit.SECONDS.sleep(7);
		String frame = util.getFrameByIndex(0);
		this.frameName = frame;
		util.frameManager(frame, nuovoClienteDaInterazioniButton, util.scrollAndClick);
	
	}
	
	

	public void creaClienteResidenziale(Properties prop) throws Exception {
		String nomeCliente =prop.getProperty("NOME", "").trim(); 
		if (nomeCliente.equalsIgnoreCase("RANDOM"))
		{
			nomeCliente = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME", nomeCliente);
		}
		String cf = this.compilaAnagraficaClienteResidenziale(nomeCliente,prop.getProperty("COGNOME").trim(),prop.getProperty("SESSO").trim(),prop.getProperty("DATA_NASCITA").trim(),prop.getProperty("COMUNE_NASCITA").trim());
		prop.setProperty("CODICE_FISCALE", cf);
	}
	
	public void creaClienteResidenziale2(Properties prop) throws Exception {
		String nomeCliente =prop.getProperty("NOME", "").trim(); 
		if (nomeCliente.equalsIgnoreCase("RANDOM"))
		{
			nomeCliente = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME", nomeCliente);
		}
		String cf = this.compilaAnagraficaClienteResidenziale(nomeCliente,prop.getProperty("COGNOME").trim(),prop.getProperty("SESSO").trim(),prop.getProperty("DATA_NASCITA").trim(),prop.getProperty("COMUNE_NASCITA").trim());
		prop.setProperty("CODICE_FISCALE", cf);
		String filePath = "src/test/resources/nuoviclienti.txt";
		Files.write(Paths.get(filePath), ("RESIDENZIALE;"+prop.getProperty("CODICE_FISCALE")+"\n").getBytes(), StandardOpenOption.APPEND);
	}

	public void creaClienteImpresaIndividuale(Properties prop) throws Exception {
		String partitaIva =prop.getProperty("PARTITA_IVA", "").trim(); 
		if (partitaIva.equalsIgnoreCase("RANDOM"))
		{
			partitaIva = PartitaIva.getPartitaIVA();
			prop.setProperty("PARTITA_IVA", partitaIva);
		}
		String nomeReferente =prop.getProperty("NOME_REFERENTE", "").trim(); 
		if (nomeReferente.equalsIgnoreCase("RANDOM"))
		{
			nomeReferente = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME_REFERENTE", nomeReferente);
		}
		this.compilaAnagraficaImpresaIndividuale(partitaIva, prop.getProperty("RAGIONE_SOCIALE"));
		this.compilaAnagraficaReferente(nomeReferente,prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"));
		
	}

	public void creaClienteCondominio(Properties prop) throws Exception {
		String partitaIva =prop.getProperty("PARTITA_IVA", "").trim(); 
		if (partitaIva.equalsIgnoreCase("RANDOM"))
		{
			partitaIva = PartitaIva.getPartitaIVA("8");
			prop.setProperty("PARTITA_IVA", partitaIva);
		}
		String nomeReferente =prop.getProperty("NOME_REFERENTE", "").trim(); 
		if (nomeReferente.equalsIgnoreCase("RANDOM"))
		{
			nomeReferente = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME_REFERENTE", nomeReferente);
		}
		String codiceFiscale = partitaIva;
		prop.setProperty("CODICE_FISCALE", codiceFiscale);	
		this.compilaAnagraficaCondominio(codiceFiscale,partitaIva, prop.getProperty("RAGIONE_SOCIALE"));
		this.compilaAnagraficaReferente(nomeReferente,prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"));
	}
	
	public void creaClienteBusiness(Properties prop) throws Exception {
		String partitaIva =prop.getProperty("PARTITA_IVA", "").trim(); 
		if (partitaIva.equalsIgnoreCase("RANDOM"))
		{
			partitaIva = PartitaIva.getPartitaIVA();
			prop.setProperty("PARTITA_IVA", partitaIva);
		}
		String nomeReferente =prop.getProperty("NOME_REFERENTE", "").trim(); 
		if (nomeReferente.equalsIgnoreCase("RANDOM"))
		{
			nomeReferente = SeleniumUtilities.randomAlphaNumeric(9);
			prop.setProperty("NOME_REFERENTE", nomeReferente);
		}
		String codiceFiscale = partitaIva;
		prop.setProperty("CODICE_FISCALE", codiceFiscale);	
		this.compilaAnagraficaBusiness(codiceFiscale,partitaIva, prop.getProperty("RAGIONE_SOCIALE").trim(),prop.getProperty("TIPO_FORMA_GIURIDICA").trim());
		this.compilaAnagraficaReferenteConRuolo(nomeReferente,prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"),prop.getProperty("RUOLO_REFERENTE"));

	}
	
	public void selezionaRuolo(By el,String ruolo) throws Exception{
		util.frameManager(frameName, el, util.select, ruolo);
	}
	
	
	public void verificaEsistenzaCampiSocialeStudi(String frame) throws Exception {
		//Verifica esistenza campi sezione Social e Web
		if(!util.exists(frame, twitterAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Twitter");
		if(!util.exists(frame, linkedinAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Linkedin");
		if(!util.exists(frame, fbAccount, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Profilo Facebook");
		if(!util.exists(frame, webSite, 5)) throw new Exception("Nella sezione Social e Web impossibile trovare il campo Sito Web");

		//Verifica esistenza campi sezione Studi e Professione
		if(!util.exists(frame, titoloStudio, 5)) throw new Exception("Nella sezione Studi e Professione impossibile trovare la select Titolo di Studio");
		if(!util.exists(frame, professione, 5)) throw new Exception("Nella sezione Studi e Professione impossibile trovare il campo Professione");

	}
	
}
