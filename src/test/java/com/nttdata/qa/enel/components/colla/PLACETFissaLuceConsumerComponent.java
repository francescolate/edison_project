package com.nttdata.qa.enel.components.colla;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class PLACETFissaLuceConsumerComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public By offertaplacetFissaLuceConsumer=By.xpath("//h3[@class='title' and text()='Enel Energia PLACET Fissa Luce Consumer']");
	public By dettaglioOffertaButton=By.xpath("//a[@href='/it/luce-e-gas/luce/offerte/placet-consumer-luce-fissa']//button[normalize-space(text())='Dettaglio Offerta']");
	public By attivaOraLink=By.xpath("//div[@class='pricing-details' and @data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/placet-consumer-luce-fissa']//a[normalize-space(text())='Attiva ora']");
	public By titoloPagina=By.xpath("//h1[text()='Enel Energia PLACET Fissa Luce Consumer']");
	public By inserisciDati=By.xpath("//h3[@id='intestazione_1']");
	public By attivaoraspalletta=By.xpath("//div[@id='priceHeaderID']//a[@href='/it/adesione-contratto?productType=res']");
	public By dettagliSplattetta=By.xpath("//div[@id='priceBoxDetails']//ul");
	public By attivaOraButton = By.id("priceHeaderID");
	public By scrittaPiccolaSpalletta=By.xpath("//div[@id='priceBoxDetails']//p");
    public By documentigenerali=By.xpath("//button[@aria-expanded='false' and normalize-space(text())='Documenti generali']");
	public By attivaoffertaButton=By.xpath("//a[@role='button' and normalize-space(text())='attiva l’offerta']");
	
	public String dettagli_Spalletta="Tipologia soluzione Luce Canone 84,0 €/anno F1 0,1128 €/kWh* F23 0,1028 €/kWh* Pagamento Addebito diretto su CC / Bollettino postale Invio bolletta Online / Cartacea";
	public String scritta_in_piccolo="i prezzi si riferiscono alla sola componente energia (IVA e imposte escluse) e sono bloccati per un anno";
	public String linkOFFERTA_PLACET= "it/luce-e-gas/luce/offerte/placet-consumer-luce-fissa";
	
	public PLACETFissaLuceConsumerComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
		
	}
	
	public void clickcomponentjavascript(By object) throws Exception {
		WebElement button=util.waitAndGetElement(object);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner();
	}
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}

	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void verificaTesto (By object,String testo) throws Exception {
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
	//	System.out.println(text);
		if (!text.contentEquals(testo)) 
       	throw new Exception("la label di testo con descrizione "+testo+" non e' presente");
	
	}
	
	public void verificaTesto2 (By object) throws Exception {
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
		String textfound="NO";
		//System.out.println(text);
		if (text.contains("Tipologia soluzione Luce Canone") && text.contains("€/anno F1") && text.contains("€/kWh* F23") && text.contains("€/kWh* Pagamento Addebito diretto su CC / Bollettino postale Invio bolletta Online / Cartacea")) 
			 textfound="SI";
		else 	throw new Exception("la label di testo desiderata non e' presente");
	
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void containsAllText(By by, String text) throws Exception{
		String textWe = driver.findElement(by).getText();
		textWe= textWe.replaceAll("(\r\n|\n)", " ");
		if(!textWe.contains(text))
			throw new Exception("textWe " + textWe + "non corrisponde a " + text);
	}
}
