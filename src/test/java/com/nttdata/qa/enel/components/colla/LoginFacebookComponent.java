package com.nttdata.qa.enel.components.colla;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LoginFacebookComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	//public By fbPageTitle = By.xpath("//div[text()='Log Into Facebook']");
	public By fbPageTitle = By.xpath("//div[text()='Accedi a Facebook']");
	public By email = By.xpath("//input[@id='email']");
	public By password = By.xpath("//input[@id='pass']");
	public By loginButton = By.xpath("//button[@id='loginbutton']");
	public By facebookCookies = By.xpath("//button[@title='Accept All']");
	public By accettaTuttiCookie = By.xpath("//button[@title='Consenti tutti i cookie']");
	
	 public LoginFacebookComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
	 
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	 
	 public void verifyComponentExistence(By oggettoEsistente) throws Exception {
			if (!util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
		}
	 
	 public void enterLoginParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}

	 public void verifyCookies() {
		 try {
			if(util.verifyExistence(facebookCookies, 20)) {
				 clickComponent(facebookCookies);
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
	 }
	public void switchToFBLogin() {
		
		
	}
}
