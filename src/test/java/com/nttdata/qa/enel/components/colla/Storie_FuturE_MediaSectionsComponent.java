package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Storie_FuturE_MediaSectionsComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By logoEnel=By.xpath("//img[@class='global-header__logo__bw']");
	public By luceAndGasTab=By.xpath("//li//a[text()='LUCE E GAS']");
    public By impreseTab=By.xpath("//li//a[text()='IMPRESE']");
    public By insiemeateTab=By.xpath("//li//a[text()='INSIEME A TE']");
    public By storieTab=By.xpath("//li//a[text()='Storie']");
    public By futureTab=By.xpath("//li/a[.='Futur-e']");
    public By mediaTab=By.xpath("//li//a[text()='MEDIA ']");
    public By supportoTab=By.xpath("//li//a[text()='SUPPORTO']");
    public By informazioniLegaliLink=By.xpath("//a[@href='https://www.enel.it/it/supporto/faq/info-legali'and text()='Informazioni Legali']");
    public By creditsLink=By.xpath("//a[@href='https://www.enel.it/it/supporto/faq/credits'and text()='Credits']");		
	public By privacyLink= By.xpath("//a[@href='https://www.enel.it/it/supporto/faq/privacy' and text()='Privacy']");
	public By cookieLink=By.xpath("//a[@href='/it/cookie-policy'and text()='Cookie Policy']");
	public By remitLink=By.xpath("//a[@href='/it/remit' and text()='Remit']");
	public By twitterIcon=By.xpath("//a[@class='icon-twitter']");
	public By facebookIcon=By.xpath("//a[@class='icon-fb']");
	public By youtubeIcon=By.xpath("//a[@class='icon-youtube']");
	public By linkedinIcon=By.xpath("//a[@href='https://www.linkedin.com/company/enelgroup/']");
	public By legalInfoPage=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[text()='Informazioni legali']");
	public By creditsPage=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[text()='Credits']");
	public By privacyPage=By.xpath("//div[@class='image-hero_content-wrapper text-center']//h1[text()='Privacy']");
	public By luceAndGasTitle=By.xpath("//a[@title='Luce e Gas']");
	public By impreseTitle=By.xpath("//a[@title='Imprese']");
	public By supportoTitle=By.xpath("//a[@title='supporto']");
	
	public Storie_FuturE_MediaSectionsComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
}
