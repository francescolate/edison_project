package com.nttdata.qa.enel.components.colla;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.SeleniumUtilities;


public class HomePageResidentialComponent {
	
	public By ariallabelpagetitle = By.xpath("//a[@aria-label='rinomina il titolo della tua fornitura']");
	public By Forniture = By.xpath("//a[@id='idFornitura']");
	public By Bollette = By.xpath("//a[@id='bollette']");
	public By Servizi = By.xpath("//a[@id='servizi']");
	public By EnelpremiaWow = By.xpath("//a[@id='enelPremia']");
	public By Novita = By.xpath("//a[@id='novitaSelfcare']");
	public By SpazioVideo = By.xpath("//a[@id='tutorialSelfcare']");
	public By Account = By.xpath("//a[@id='accountSelfcare']");
	public By ItuoiDiritti = By.xpath("//a[@id='supporto']");
	public By Supporto = By.xpath("//a[@id='supporto']");
	public By TrovaSpazioEnel = By.xpath("//a[@id='negozioEnel']");
	public By Esci = By.xpath("//a[@id='disconnetti']");
	public By TrovaSiButton = By.xpath("//button[@id='overlayYesButton']");
	public By DettaglioFornitura = By.xpath("//div[@id='wrapper_modalforn']/div[@class='card forniture regolare'][1]/div[@class='button-container']/a[2]");
	public By leftMenu = By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]");
	public By zoomin = By.xpath("//div[@id='mapContainer']//button[@title='Zoom avanti']");
	public By zoomOut = By.xpath("//div[@id='mapContainer']//button[@title='Zoom indietro']");
	
	//ACR_10
	public By welcomeText = By.xpath("//main/div[@id='mainContentWrapper']/div/h1");
	public By welcomePageHeading = By.xpath("//p[.='In questa sezione potrai gestire le tue forniture']");
	public By viewInfoSupplyBill = By.xpath("//section[@id='lista-forniture']/div/p[2]");
	public By yourSupplies = By.xpath("//section[@id='lista-forniture']/div/h2");
	public By yourActivities = By.xpath("//section[@id='activity']/div/h2");
	public By viewActivitiesManageSupply = By.xpath("//p[contains(text(),'Qui')]");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By iconUserButton = By.xpath("//span[@class='icon-user']/parent::button[@class='dotcom-header__btn btn-user-open']");
	
	//ACB 207
	
	public By chiamaci = By.xpath("//a[@id='enelChatVoip']");
	public By porta = By.xpath("//a[@id='portaAmici']");
	public By itu = By.xpath("//a[@id='iTuoiDiritti']");
	public By enelpremiaWowtext1 = By.xpath("//a[text()='Home']");
	public By enelpremiaWowtext2 = By.xpath("//a[text()='Luce e Gas']");
	public By enelpremiaWowtext3 = By.xpath("//a[contains(text(),'Enelpremia WOW!')]");
	public By enelpremiaWowHeading = By.xpath("//h1[.='Partecipa a ENELPREMIA WOW!']");
	public By enelpremiaWowHeadingNew = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By enelpremiaWowTitle = By.xpath("//p[.='Il programma fedeltà che ti offre ogni settimana tanti sconti e regali speciali']");
	public By enelpremiaWowTitleNew = By.xpath("//p[.='Iscriviti entro il 30 novembre 2020 e prova a vincere una Carta Regalo IKEA® digitale del valore di 100 €!']");
	public By novitaCardLink = By.xpath("//li[@id='appPromo']/span");
	public By chiamaciText1 = By.xpath("//h2[text()='Vuoi contattare un operatore?']");
	public By chiamaciText2 = By.xpath("//p[text()='Un operatore sarà a tua disposizione per offrirti il nostro supporto e darti informazioni sui nostri servizi.']");
	
	public static final String FornitureMenuText = "Forniture";
	public static final String BolletteMenuText = "Bollette";
	public static final String ServiziMenuText = "Servizi";
	public static final String EnelpremiaMenuText = "enelpremia WOW!";
	public static final String NovitaMenuText = "Novità";
	public static final String SpazioVideoMenuText = "Spazio Video";
	public static final String ChiamaciMenuText = "Chiamaci";
	public static final String PortaMenuText = "Porta i tuoi amici"; 
	public static final String AccountMenuText = "Account";
	public static final String ItuMenuText = "I tuoi diritti";
	public static final String SupportoMenuText = "Supporto";
	public static final String TrovaMenuText = "Trova Spazio Enel.";
	public static final String EsciMenuText = "Esci";
	public static final String URL_PRIVACY_POLICY= "https://www-coll1.enel.it/it/luce-e-gas/enelpremia"; 
	public static final String EnelpremiaWowtext1 = "Home";
	public static final String EnelpremiaWowtext2 = "Luce e Gas";
	public static final String EnelpremiaWowtext3 = "Enelpremia WOW! posizione attuale";
	public static final String EnelpremiaWowHeading = "Partecipa a ENELPREMIA WOW!";
	public static final String EnelpremiaWowHeadingNew = "Con ENELPREMIA WOW! prova a vincere la Maglia Rosa";
	public static final String EnelpremiaWowTitle = "Il programma fedeltà che ti offre ogni settimana tanti sconti e regali speciali";
	public static final String EnelpremiaWowTitleNew = "Iscriviti entro il 30 novembre 2020 e prova a vincere una Carta Regalo IKEA® digitale del valore di 100 €!";
	public static final String ChiamaciText1 = "Vuoi contattare un operatore?";
	public static final String ChiamaciText2 = "Un operatore sarà a tua disposizione per offrirti il nostro supporto e darti informazioni sui nostri servizi.";
	
	SeleniumUtilities util;
	WebDriver driver;
	Properties prop;
	
	public HomePageResidentialComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	
	}
	
	public void SwitchToWindow() throws Exception
	{
		Set<String> windows = driver.getWindowHandles();
		
		
		for (String winHandle : windows) {
		/*	System.out.println(winHandle);
			System.out.println("switched to "+driver.getTitle()+"  Window");
			System.out.println(driver.getCurrentUrl());
		*/	
	       // String pagetitle = driver.getTitle();
	        driver.switchTo().window(winHandle);
	   /*     System.out.println(winHandle);
	        System.out.println("After switch"+ driver.getCurrentUrl());
	        System.out.println(driver.getTitle());
		*/    
		 }
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void SwitchTabToSupporto(String Url) throws Exception
		{
		    String currentHandle = driver.getWindowHandle();
		    Set <String> windows = driver.getWindowHandles();
		   
		   
		    for (String winHandle : windows) {
		       
		        String pagetitle = driver.getTitle();
		        driver.switchTo().window(winHandle);
		     }
		    	checkURLAfterRedirection(Url);
		  }

		public void SwitchTabToOriginalWindow() throws Exception
		{
			String currentHandle = driver.getWindowHandle();
			driver.switchTo().window(currentHandle);
		}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void homepageRESIDENTIALMenu (By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String item_found="NO";
	List<WebElement> leftMenu = driver.findElements(By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]"));
	
	for(WebElement element : leftMenu){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
		
	if (description.contentEquals("Forniture")) 
		item_found="YES";
	
	else if(description.contentEquals("Bollette"))
		item_found="YES";
	else if (description.contentEquals("Servizi"))
		item_found="YES";
	else if (description.contentEquals("enelpremia WOW!"))
		item_found="YES";
	else if (description.contentEquals("Novità"))
		item_found="YES";
	else if (description.contentEquals("Spazio Video"))
		item_found="YES";
	else if (description.contentEquals("Account"))
		item_found="YES";
	else if (description.contentEquals("I tuoi diritti"))
		item_found="YES";
	else if (description.contentEquals("Supporto"))
		item_found="YES";
	else if (description.contentEquals("Trova Spazio Enel"))
		item_found="YES";
	else if (description.contentEquals("Esci"))
	item_found="YES";
	
		if (item_found.contentEquals("NO"))
		throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'Forniture', 'Bollette','Servizi','enelpremia WOW!','Novità','Spazio Video','Account','I tuoi diritti','Supporto','Trova Spazio Enel','Esci'");
			}
		}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
				
		if (actualtext.contentEquals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
				}
	
}
		
	   

