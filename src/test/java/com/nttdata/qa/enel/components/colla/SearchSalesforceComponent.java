package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SearchSalesforceComponent extends BaseComponent {
	

	public By search = By.xpath("//input[contains(@class, 'slds-input') and @placeholder='Cerca...']");
	public By result = By.xpath("//div[@class='topResultsWrapper']/div/div/div/div/div/div[1]");
	public By waffle = By.xpath("//*[@class='slds-icon-waffle']");
	public By caseItemTitle = By.xpath("//div[@class='topResultsWrapper']/div/div/div/div/div/div[1]/div/div/div/div/h2/a[contains(text(),'Case Item')]");
	public By searchApp = By.xpath("//input[@type='search' and @placeholder='Cerca nelle app e nelle voci...']");
	public By searchList = By.xpath("//input[@type='search' and @placeholder='Cerca in questo elenco...']");
	public By textSearched;
	public String textSearchedString = "//a[text()='$text$' and @title='$text$']";
	
	public String caseItem = "Case Items";
	
	
	
	public SearchSalesforceComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
        if (!util.verifyExistence(oggettoEsistente, extTimeoutInterval)) {
            throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
        }
	}
	
	public void openObjectInSalesforce(String object, String name) throws Exception{
		System.out.println("Object: " + object + " --  Name: " + name);
		Thread.sleep(10000);
		clickComponent(waffle);
        util.waitUntilIsDisplayed(searchApp);
        clearAndSendKeys(searchApp, object);
        simulateEnterKey(searchApp);
        util.waitUntilIsDisplayed(searchList);
        clickComponent(searchList);
        clearAndSendKeys(searchList, name);
        simulateEnterKey(searchList);
        textSearched=By.xpath(textSearchedString.replace("$text$", name));
        util.waitUntilIsDisplayed(textSearched);
        clickComponent(textSearched);
		
	}
	
	
	
	
}