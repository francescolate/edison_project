package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSN_47_CustomerSupplyComponent {
	
	

	public By leftMenuItems = By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a");
	public By homePageTitle1 = By.xpath("//ol[@class='breadcrumb']/li[1]/a");
	public By homePageTitle2 = By.xpath("//ol[@class='breadcrumb']/li[@class='active']");
	public By supplyHomePageTitle1 = By.xpath("//section[@id='fornDetailPannel']/div[@class='panel panel-default panel-clear']/div[@class='panel-body']/ol[@class='breadcrumb']/li[1]");
	public By supplyHomePageTitle2 = By.xpath("//section[@id='fornDetailPannel']/div[@class='panel panel-default panel-clear']/div[@class='panel-body']/ol[@class='breadcrumb']/li[@class='forniture active']");
	public By homePageTitleDescriptiom = By.xpath("//div[@class='panel-body']/h1");
	public By homePageLogo = By.xpath("//div[@id='openModalUserBtn']");
	public By supplyBillTitle = By.xpath("//section[@id='content-home']/div[@class='panel panel-default panel-primary']/div[@class='panel-body']/div[@class='title']/h2");
	public By customerLightSupply = By.xpath("//div[@id='heading-Index']/h4");
	public By supplyDescription = By.xpath("//div[@id='collapseOne']/div[1]/div[1]/div/h5");
	public By supplyHeading = By.xpath("//div[@id='collapseOne']/div[1]/div/div/h5");
	public By supplyDetails = By.xpath("//div[@id='collapseOne']/div[1]/div/div/span");
	public By SupplyTitle1 = By.xpath("//div[@class='panel-body']/h1[@class='fix-align']");
	public By supplyDescription1 = By.xpath("//p[@id='subtitleFornitura']");
	public By supplyDetails1 = By.xpath("//div[@class='panel panel-default']/div[@class='col-xs-12']");
	public By modificaLink = By.xpath("//a[@class='btn btn-link btn-fuchsia edit-field']");
	public By vaiAllelencolink = By.xpath("//div[@id='tue_bollette']//a[contains(@class,'btn-arrow')]");////div[@id='tue_bollette']//a[contains(@class,'btn-arrow')]
	public By SupplyTitle2 = By.xpath("//section[@id='fornDescription']/div/div/h1");
	public By supplyDescription2 = By.xpath("//section[@id='fornDescription']/div/div/p[@class='lead'][1]");
	public By supplyDescription3 = By.xpath("//p[@id='inviaDocumentiLab']");
	public By billTable = By.xpath("//div[@id='tue_bollette']/div/div");
	public By billDetails = By.xpath("//div[@id='collapseOne']/div[2]/div/div/h5");
	public By piuInformazionlink = By.xpath("//div[@id='collapseOne']/div[1]/div/div/div/a");
	public By inviaDocumenti = By.xpath("//div[@id='inviaDocumenti']/a");
	public By mostraFiltri = By.xpath("//div[@class='col-xs-12']/a[@class='btn btn-primary btn-bordered collapsed']");
	public By esportainExcel = By.xpath("//div[@class='title']/a[@id='excel-export']");
	
	public By AggiungiTitle = By.xpath("//*[@id='headingOne']/h4/form/div/div/div[1]");
	public By AggiungiValue = By.xpath("//div[contains(text(),'Fornitura Luce')]");
	public By SalviLink = By.xpath("//button[@class='btn btn-link btn-fuchsia save-field']");
	public By AggiungiEnterText = By.xpath("//*[@id='nome-fornitura']");
	
	public By FornituraField = By.xpath("//*[@id='nome-fornitura']");
		
	SeleniumUtilities util;
	WebDriver driver;
	
	public BSN_47_CustomerSupplyComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void leftMenuItemsDisplay (By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String item_found="NO";
	List<WebElement> leftMenu = driver.findElements(By.xpath("//nav/ul/div/li[not(contains(@class, 'hide'))]/a"));
	int total = leftMenu.size();
	
	
	for(WebElement element : leftMenu){
	String description=element.getText();
	
	description= description.replaceAll("(\r\n|\n)", " ");
		
	if (description.contentEquals("HOMEPAGE")) 
		item_found="YES";
	
	else if(description.contentEquals("BOLLETTE"))
		item_found="YES";
	else if (description.contentEquals("FORNITURE"))
		item_found="YES";
	else if (description.contentEquals("SERVIZI"))
		item_found="YES";
	else if (description.contentEquals("MESSAGGI"))
		item_found="YES";
	else if (description.contentEquals("STATO RICHIESTE"))
		item_found="YES";
	else if (description.contentEquals("REPORT BILLING MANAGEMENT"))
		item_found="YES";
	/*else if (description.toLowerCase().contentEquals("gestione piano abbonamento"))
		item_found="YES";
	else if (description.toLowerCase().contentEquals("area clienti casa"))
		item_found="YES";*/
	else if (description.contentEquals("CARICA DOCUMENTI"))
		item_found="YES";
	else if (description.contentEquals("CONTATTI"))
	item_found="YES";
	/*else if (description.toLowerCase().contentEquals("i tuoi diritti"))
		item_found="YES";*/
	else if (description.contentEquals("LOGOUT"))
		item_found="YES";
		if (item_found.contentEquals("NO"))
		throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'bollette', 'forniture','Servizi','messaggi','stato richieste','report billing management','gestione piano abbonamento','area clienti casa','caricaDoc','contatti','i tuoi diritti','logout'");
			}
		}

	public void VerifyText(By checkObject, String Value) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	
	String textfield_found="NO";
	
	WebElement element = driver.findElement(checkObject);
	
	String actualtext = element.getText();
	
	
	if (actualtext.contentEquals(Value))
	textfield_found="YES";
					
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected text" + Value + " is not found:");
			}
	
	
	public void compareText(By checkObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
				
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		System.out.print(actualtext);
		
				}
	   public void clearText(By textObject) throws Exception {
		   WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(textObject));
			WebElement element = driver.findElement(textObject);
			element.clear();
	   }

	public void supplyDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String textfield_found="NO";
				
	List<WebElement> field = driver.findElements(By.xpath("//div[@id='collapseOne']/div[1]/div/div/span"));
	
	for(WebElement element : field){
	String description=element.getText();
	description= description.replaceAll("(\r\n|\n)", " ");
		
	 if(description.contentEquals("Luogo: Via Padule 10a 25035 Ospitaletto Bs"))
		textfield_found="YES";
	else if (description.contentEquals("Tipologia: Luce"))
		textfield_found="YES";
	else if (description.contentEquals("Offerta: Senza Orari Luce"))
		textfield_found="YES";
	else if (description.contentEquals("Stato: Attivata"))
		textfield_found="YES";
	else if (description.contentEquals("Più informazioni"))
		textfield_found="YES";
	if (textfield_found.contentEquals("NO"))
		throw new Exception("on the 'https://www-colla.enel.it/ Supply fields are not present with description 'La tua fornitura nel dettaglio.','Luogo:Via Padule 10a 25035 Ospitaletto Bs','Tipologia:Luce','Offerta: Senza Orari Luce','Stato: Attivata','Più informazioni' ");
	}
}

	public void backBrowser(By checkObject) throws Exception {
    try {
    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
    	
        driver.navigate().back();
                
    } catch (Exception e) {
        throw new Exception("Previous Page not displayed");
    }
   }
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}

	public void supplyBillDetailsDisplay(By checkObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	String expectedRowTitle = "Numero Importo Stato Scadenza";
	String actualRowTitle = driver.findElement(By.xpath("//div[@id='tue_bollette']/div[@class = 'row']//table/thead/tr")).getText();
	if (expectedRowTitle.equals(actualRowTitle))
	{
				String textfield_found= "Yes";
				//to locate the table
				WebElement mytable = driver.findElement(By.xpath("//div[@id='tue_bollette']/div[@class = 'row']//table"));
				//to locate rows of table
				List < WebElement > rows_table = mytable.findElements(By.xpath("//div[@id='tue_bollette']/div[@class = 'row']//table/tbody"));
				//to calculate number of rows in table
				int rows_count = rows_table.size();
				 // To retrieve text from that specific cell.
				
				for (int row = 0; row < rows_count; row++)
				{
					String description = rows_table.get(row).getText();
					
				if(description.contentEquals("Numero Importo Stato Scadenza"))
					textfield_found="YES";
				else if (description.contentEquals("3032636823 € 234,80 ADDEBITO SU CONTO CORRENTE BANCARIO 27/05/2019/n3018051229 € 236,02 ADDEBITO SU CONTO CORRENTE BANCARIO 26/03/2019"))
					textfield_found="YES";
				if (textfield_found.contentEquals("NO"))
					throw new Exception("on the 'https://www-colla.enel.it/ Supply fields are not present with description 'Numero Importo Stato Scadenza','3044485811 € 298,57 ADDEBITO SU CONTO CORRENTE BANCARIO 24/07/2019','3032636823 € 234,80 ADDEBITO SU CONTO CORRENTE BANCARIO 27/05/2019','3018051229 € 236,02 ADDEBITO SU CONTO CORRENTE BANCARIO 26/03/2019',");
				}
             }
	
}


}
