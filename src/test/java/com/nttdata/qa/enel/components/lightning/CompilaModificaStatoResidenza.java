package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

/**

	/**
	 * Compila i dati per il form "Dettaglio Modifica Stato Residenza"
	 */
	public class CompilaModificaStatoResidenza extends BaseComponent{
		WebDriver driver ;
		SeleniumUtilities util;
		SpinnerManager spinner;

		public By selectStatoResidenza = new By.ByXPath("//td[@data-label='residenceStatus']//select");
		public By inputData = By.xpath("//td[@data-label='newStartDateLab']//select");
		public By selectAvantiButton = new By.ByXPath("//button[@class='slds-button slds-button--brand confirmBT']");
		public By selectModalitaFirma = new By.ByXPath("//select[@class='single slds-select select uiInput uiInputSelect uiInput--default uiInput--select']");
		public By selectOkButton = new By.ByXPath("//div[@id='promptContainer']//button[@class='slds-button slds-button--neutral']");
		public By selectConfermaButton = new By.ByXPath("//button[@class='slds-button slds-button--brand confirmBT']");
		
		
		
		public CompilaModificaStatoResidenza(WebDriver driver){
			super(driver);
			this.driver = driver;
			this.util = new SeleniumUtilities(driver);
			this.spinner = new SpinnerManager(driver);
		}


		public void selectStatoRes(String statoResidenza) throws Exception {
			
			TimeUnit.SECONDS.sleep(5);			
			util.objectManager(selectStatoResidenza, util.select,statoResidenza);
			
		}


		public void selectAvantiButton() throws Exception {
			
			TimeUnit.SECONDS.sleep(5);	
			util.objectManager(selectAvantiButton, util.scrollAndClick);//,statoResidenza);
		}


		public void modalitaFirma(String modalitaFirma) throws Exception {
			
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(selectModalitaFirma, util.select,modalitaFirma);
			
		}
			
		public void selectOkButton() throws Exception {
			
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(selectOkButton, util.scrollAndClick);
			
		}	
		
		public void selectConfermaButton() throws Exception {
			
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(selectConfermaButton, util.scrollAndClick);
			
		}	
		
		
		public void verificaFornitureRiepilogoStatoResidenza(int numForniture) throws Exception{
			List<WebElement> list =util.waitAndGetElements(By.xpath("//h3[contains(text(),'Riepilogo Stato Residenza')]/following::table[1]//tbody//tr"));
		    if(list.size()!=numForniture) throw new Exception("Il numero di forniture nella tabella di riepilogo stato residenza non e' "+numForniture+". Numero riscontrato :"+list.size());
		}
		
		
		public void checkCampiModalitaFirmaPrepopolati(String modalitaFirmaPrepopolata, String residenteEsteroPrepopolato) throws Exception{
			verifyComponentText(By.xpath("//label[text()='Modalità firma']/following::select[1]/option[@selected]"),modalitaFirmaPrepopolata);
			verifyComponentText(By.xpath("//label[text()='Residente Estero']/following::select[1]/option[@selected]"),residenteEsteroPrepopolato);

		}
		
		
		
	}
		


