package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CheckVariazioneAnagraficaComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By richiesteHeader = By.xpath("//div[@class='container']//h2//span[contains(text(), 'Richieste')]/..");
	public By variazioneAnagraficaTD = By.xpath("//table//tbody//td//a[text()='VARIAZIONE ANAGRAFICA']");//usare JavascriptExecutor
	public By dettagliButton = By.xpath("//a[@data-tab-value='detailTab']");
	public By origineCaso = By.xpath("//div[@role='list']//span[text()='Origine caso']/../following-sibling::div//lightning-formatted-text[text()='Web']");
	public By stato = By.xpath("//div[@role='list']//span[text()='Stato']/../following-sibling::div//lightning-formatted-text[text()='Chiuso']");
	public By statoSecondario = By.xpath("//div[@role='list']//span[text()='Stato Secondario']/../following-sibling::div//lightning-formatted-text[text()='Ricevuto']");
	public By newDataIframe = By.xpath("//div[@class='content iframe-parent']//iframe");	
	public By newDataEmail = By.xpath("//tbody//td[2]//table//td[text()='Email']/following-sibling::td");
	public By newDataTelefono = By.xpath("//tbody//td[2]//table//td[text()='Telefono']/following-sibling::td");
	
    public CheckVariazioneAnagraficaComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void launchLink(String url) throws Exception {
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	
	public void scrollToElement(By by) throws Exception{
		util.objectManager(by, util.scrollToVisibility, false);
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		//System.out.println("Normalized:\n"+weText);
		//System.out.println(text);
		Thread.sleep(250);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 60))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void jsClickComponent(By by) throws Exception{
		util.jsClickElement(by);
	}
	
	public void switchToFrame(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		driver.switchTo().frame(we);
	}
}


