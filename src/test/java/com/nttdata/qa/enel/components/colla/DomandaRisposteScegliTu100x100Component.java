package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class DomandaRisposteScegliTu100x100Component {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public By labelTrovaLaSoluzione=By.xpath("//div[@class='cmSupportTool_claim' and text()='Trova la tua soluzione']");
	public By pageNumber1=By.xpath("//div[@class='cmSupportTool_block cmSupportTool_block-question 0 visible first-question ']//div[@class='cmSupportTool_question-number' and text()='1 / 3']");
	public By domanda=By.xpath("//div[@class='cmSupportTool_question' and text()='Quando tu e la tua famiglia trascorrete più tempo in casa?']");
	public By listarisposte=By.xpath("//div[@class='cmSupportTool_block cmSupportTool_block-question 0 visible first-question ']//ul[@class='cmSupportTool_list-answer']//li");
	public String risposte [] = {"Solitamente durante tutto il giorno","Prevalentemente la sera e il weekend","Prevalentemente la notte e la domenica"};
//	public By pageNumber2=By.xpath("//div[@class='cmSupportTool_block cmSupportTool_block-question 1   visible']//div[@class='cmSupportTool_question-number' and text()='2 / 3']");
	public By pageNumber2=By.xpath("//div[@class='cmSupportTool_question-number' and text()='2 / 3']");
	public By domanda2=By.xpath("//div[@class='cmSupportTool_question' and text()='In quale fascia oraria utilizzi principalmente gli elettrodomestici?']");
	public By testo2=By.xpath("//div[@class='cmSupportTool_text' and text()='Lavatrice, Lavastoviglie, Condizionatore, Scaldabagno...']");
	public String risposte2 [] = {"Durante tutto il giorno indistintamente","Prevalentemente la sera e il weekend","Prevalentemente la notte e la domenica"};
	public By listarisposte2=By.xpath("//div[contains(@class,'cmSupportTool_block cmSupportTool_block-question 1')]//ul[@class='cmSupportTool_list-answer']//li");
//	public By pageNumber3=By.xpath("//div[@class='cmSupportTool_block cmSupportTool_block-question 2  last-question visible']//div[@class='cmSupportTool_question-number' and text()='3 / 3']");
	public By pageNumber3=By.xpath("//div[@class='cmSupportTool_question-number' and text()='3 / 3']");
	public By domanda3=By.xpath("//div[@class='cmSupportTool_question' and text()='Saresti disposto a spostare i tuoi consumi per avere una bolletta più leggera?']");
	public String risposte3 [] = {"No, Voglio consumare indistintamente tutto il giorno","Si, verso la fascia serale e il weekend","Si, verso la fascia notturna e la domenica"};
	public By listarisposte3=By.xpath("//div[contains(@class,'cmSupportTool_block cmSupportTool_block-question 2')]//ul[@class='cmSupportTool_list-answer']//li");
	public By titolo=By.xpath("//span[@class='cmSupportTool_result-title' and text()='Ecco la soluzione migliore per te!']");
	public By sottotitolo=By.xpath("//p[@class='cmSupportTool_result-text' and text()='E se le tue abitudini cambiano? Potrai scegliere di cambiare il tuo piano fino a 3 volte l’anno']");
	public By testo=By.xpath("//div[@class='cmSupportTool_result-col']//div[@class='cmSupportTool_result-content']");
	//public String text="Notti e Festivi Per te il prezzo della componente energia gratis tutte le notti e nei giorni festivi Da lunedì a sabato dalle 23 alle 7, domenica e festività nazionali , 0,0 €/kWh Da lunedì a sabato, dalle 7 alle 23 , 0,16 €/kWh ATTIVA ORA";
	//public String text="Notti e Festivi Per te il prezzo della componente energia gratis tutte le notti e nei giorni festivi ATTIVA ORA";
	//public String text="Notti e FestiviPer te il prezzo della componente energia gratis tutte le notti e nei giorni festivi Da lunedì a sabato dalle 23 alle 7, domenica e festività nazionali, 0,0 €/kWhDa lunedì a sabato, dalle 7 alle 23, 0,144 €/kWhAttiva ora";
	public String text="Notti e FestiviPer te il prezzo della componente energia gratis tutte le notti e nei giorni festivi Da lunedì a sabato dalle 23 alle 7, domenica e festività nazionali, $Da lunedì a sabato, dalle 7 alle 23, £Attiva ora";
	public By buttonAttivaOra=By.xpath("//a[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-notte-festivi' and text()='Attiva ora']");
	public By titoloPagina=By.xpath("//h1[text()='Scegli Tu Notte e Festivi']");
	//public String text2="Senza Orari Un unico prezzo a qualsiasi ora del giorno e della notte Da lunedì a domenica, senza vincoli di orario , 0,088 €/kWh ATTIVA ORA";
	//public String text2="Senza Orari Un unico prezzo a qualsiasi ora del giorno e della notte ATTIVA ORA";
	public String text2="Senza OrariUn unico prezzo a qualsiasi ora del giorno e della notte Da lunedì a domenica, senza vincoli di orario, 0,09808 €/kWhAttiva ora";
	public By buttonAttivaOra2=By.xpath("//a[@data-prodpath='/content/enel-it/it/luce-e-gas/luce/offerte/scegli-tu-senza-orari' and text()='Attiva ora']");
	public By titoloPagina2=By.xpath("//h1[text()='Scegli Tu Senza Orari']");
	public By title = By.xpath("//div[@class='title' and contains(text(),'Senza Orari')]");
	public By textIntro = By.xpath("//p[@class='text-intro' and contains(text(),'Un unico prezzo a qualsiasi ora del giorno e della notte')]");
	public By attivaOraButton = By.xpath("//a[text()='Attiva ora' and @href='/it/adesione-contratto?productType=res&zoneid=smart_tool-cta']");
	
	
	
	
	
	
	public DomandaRisposteScegliTu100x100Component(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("Nella pagina di dettaglio dell'offerta 'scegli tu 100x100' l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void verificaEsistenzaRisposte(String [] risposte, By checkobject) throws Exception {
		List<WebElement> r = util.waitAndGetElements(checkobject);
		
		for(int x = 0;x<risposte.length;x++) {
					
					String p = util.waitAndGetElement(r.get(x),By.xpath("./p")).getAttribute("innerText"); 
				//	System.out.println(p);
			        if(!p.contentEquals(risposte[x].trim()))
			        	throw new Exception("Nella pagina di dettaglio dell'offerta 'scegli tu 100x100' non risulta presente la domanda "+risposte[x]);
		}
	}
	
	public void scegliRisposta(By checkobject, String risposta) throws Exception {
		String rispostaTrovata="NO";
		List<WebElement> q = util.waitAndGetElements(checkobject);
	
		for(int x = 0;x<q.size();x++) {
			
			String p = util.waitAndGetElement(q.get(x),By.xpath("./p")).getAttribute("innerText"); 
					
			        if(p.contentEquals(risposta)) {
			      	util.waitAndGetElement(q.get(x),By.xpath("./p")).click();			
			      	rispostaTrovata="SI";
			        TimeUnit.SECONDS.sleep(1);
			        break;
			        }     
		}
		
		if (rispostaTrovata.contentEquals("NO"))
			throw new Exception("Nella pagina di dettaglio dell'offerta 'scegli tu 100x100' non risulta presente e cliccabile la risposta "+risposta);
	}
	
	
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void verificaTesto (By object,String testo) throws Exception {
		String text=driver.findElement(object).getText();
		text= text.replaceAll("(\r\n|\n)", " ");
		//System.out.println(text);
		if (!text.contentEquals(testo)) 
        	throw new Exception("la label di testo con descrizione "+testo+" non e' presente");
	
	}
	
	public void checkColorBackground(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("background-color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner();
	}
	
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = normalizeInnerHTML(weText);		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
}
