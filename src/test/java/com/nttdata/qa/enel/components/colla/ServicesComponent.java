package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ServicesComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	 public int extTimeoutInterval = 15;
	
	public By servicesLeftMenuItem = By.xpath("//nav[@id='mainNav']//span[text()='Servizi']");
	public By serviceTitle = By.xpath("//div[@class='section-heading']//h2[text()='Servizi per le forniture']");
	public By serviceSubText = By.xpath("//div[@class='section-heading']/p[text()='Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture']");
	public By autolettura = By.xpath("//h3[text()='Autolettura']");
	public By infoEnelEnergia = By.xpath("//li[@class='tile']//h3[text()='InfoEnelEnergia']");
	public By modificaIndirizzoFatturazione = By.xpath("//h3[text()='Modifica Indirizzo Fatturazione']");
	public By modificaPotenzaeoTensione = By.xpath("//h3[text()='Modifica Potenza e/o Tensione']");
	public By modificaContattieConsensi = By.xpath("//li[@class='tile']//h3[text()='Modifica Contatti e Consensi']");
	public By billServices = By.xpath("//div[@class='section-heading']/h2[text()='Servizi per le bollette']");
	public By billServicesSubText = By.xpath("//div[@class='section-heading']//p[text()='Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette']");
	public By webBill = By.xpath("//li[@class='tile']//h3[text()='Bolletta Web']");
	public By onlinePayment = By.xpath("//li[@class='tile']//h3[text()='Pagamento Online']");
	public By directDebit = By.xpath("//h3[text()='Addebito Diretto']");
	public By billCorrection = By.xpath("//h3[text()='Rettifica Bolletta']");
	public By installments = By.xpath("//h3[text()='Rateizzazione']");
	public By summary = By.xpath("//h3[text()='Bolletta di Sintesi o di Dettaglio']");
	public By duplicateUtilityBill = By.xpath("//h3[text()='Duplicato Bolletta']");
	public By contractServices = By.xpath("//div[@class='section-heading']/h2[text()='Servizi per il contratto']");
	public By contractServicesSubText = By.xpath("//section[@id='forniture-id2']/div[@class='section-heading']/p[2]");
	public By sendingDoc = By.xpath("//h3[text()='Invio Documenti']");
	public By supplyDeactivation = By.xpath("//h3[text()='Disattivazione Fornitura']");
	public By melitaFiber = By.xpath("//h3[text()='Modifica Consumo Concordato']");
	public By haiBisogno = By.xpath("//span[text()='Hai bisogno di aiuto?']");
	public By haiBisognoExpansionText = By.xpath("//div[@class='content color_white1 bg_green1']");
	public By cliccaSulRiquadro = By.xpath("//a[@id='chat_reason_cta']/strong");
	public By reasonsTitle = By.xpath("//div[@class='chat-box']/div[@class='content']/div[@class='top']/span[@class='title']");
	public By bolletaLiveChat = By.xpath("//div[@class='items']/div[@class='item'][1]/div[@class='inner']/button");
	public By pagamentiLiveChat = By.xpath("//div[@class='items']/div[@class='item'][2]/div[@class='inner']/button");
	public By contrattoLiveChat = By.xpath("//div[@class='items']/div[@class='item'][3]/div[@class='inner']/button");
	public By enelpremiaLiveChat = By.xpath("//div[@class='items']/div[@class='item'][4]/div[@class='inner']/button");
	public By sitoWebLiveChat = By.xpath("//div[@class='items']/div[@class='item'][5]/div[@class='inner']/button");
	public By infoGenericheLiveChat = By.xpath("//div[@class='items']/div[@class='item'][6]/div[@class='inner']/button");
	public By infoSuvociinBolletta = By.xpath("//div[@class='item'][1]/div[@class='inner open']/div[@class='radios']/div[@class='radio'][1]");
	public By mancatoRecapitoBolletta = By.xpath("//div[@class='item'][1]/div[@class='inner open']/div[@class='radios']/div[@class='radio'][2]");
	public By richiestaRettificaBolletta = By.xpath("//div[@class='item'][1]/div[@class='inner open']/div[@class='radios']/div[@class='radio'][3]");
	public By webZoomBolletta = By.xpath("//div[@class='item'][1]/div[@class='inner open']/div[@class='radios']/div[@class='radio'][4]");
	public By canoneRaiBolletta = By.xpath("//div[@class='item'][1]/div[@class='inner open']/div[@class='radios']/div[@class='radio'][5]");
	public By altroBolletta = By.xpath("//div[@class='item'][1]/div[@class='inner open']/div[@class='radios']/div[@class='radio'][6]");
	public By avviaChat = By.xpath("//a[@id='chat_loading_cta']");
	public By loadingChat = By.xpath("//div[@id='disclaimer']");
	public By message = By.xpath("//table[@id='ChatTranscript']");
	public By chatText = By.xpath("//body[@id='bodyChatTranscripts']//tbody");
	public String chatFrame = "ChatTranscript";
	public String mainFrame = "main";
	public String frameContainer = "iframechatap";
	
	
	//357A
	public By headingautolettura = By.xpath("//h1[text()='Comunica la lettura del contatore']");
	public By subtitleautolettura = By.xpath("//h1[text()='Comunica la lettura del contatore']/following-sibling::h2");
	public By titleautolettura = By.xpath("//h1[text()='Comunica la lettura del contatore']/following-sibling::h4");
	public By Indirizzodellafornitura = By.xpath("//span[text()='Indirizzo della fornitura']");
	public By NumeroCliente = By.xpath("//span[text()='Numero Cliente']");
	public By linkStoricoLetture = By.xpath("//a[text()='Storico Letture']");
	public By esciButton = By.xpath("//span[text()='ESCI']");
	public By comminica = By.xpath("//span[text()='COMUNICA LETTURA']");
	public By no = By.xpath("//span[text()='NO']");
	public By close = By.xpath("//div[@id='modalAlert']//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By popUpHeading = By.xpath("//h3[@id='titleModal']");
	public By popUpInnerText = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
	//public By noButton = By.xpath("//button[@id='overlayNoButton']");
	public By siButton = By.xpath("//button[@id='overlayYesButton']");
	//public By closePopUp = By.xpath("//div[@id='modalAlert']//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By eyeButton = By.xpath("//a[@id='iconLink']");
		
	
	public By esci = By.xpath("//a[text()='Esci']");
	public By message1 = By.xpath("//div[@class='text extrabig']//p[contains(text(),'Al momento i')]"); 
	public String Message1 = "Al momento i nostri operatori non sono disponibili. Si prega di riprovare più tardi.";
	
	public By storicoLetturePopup = By.xpath("//h3[@id='title-storico-lettura']");
	public By storicoLetturePopupClose = By.xpath("//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close']");
	
	public ServicesComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	  public void verifyComponentNonExistence(By oggettoEsistente) throws Exception {
	        if (util.verifyExistence(oggettoEsistente, extTimeoutInterval)) {
	            throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
	        }
	  }
	    
	    public void scrollComponent(By object) throws Exception {
			util.objectManager(object, util.scrollToVisibility, false);
		}
	    
	    public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}

		public void checkForSupplyServices(String property) throws Exception {
			
			if(! property.contains(driver.findElement(autolettura).getText()) && property.contains(driver.findElement(infoEnelEnergia).getText()) && property.contains(driver.findElement(modificaIndirizzoFatturazione).getText())
					&& property.contains(driver.findElement(modificaPotenzaeoTensione).getText()) && property.contains(driver.findElement(modificaContattieConsensi).getText()))
				
				throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
			
			
		}
		
		public void checkForBillServices(String property) throws Exception {
			
			util.scrollToElement(driver.findElement(billServices));

			if(! property.contains(driver.findElement(webBill).getText()) && property.contains(driver.findElement(onlinePayment).getText()) && property.contains(driver.findElement(directDebit).getText())
					&& property.contains(driver.findElement(billCorrection).getText()) && property.contains(driver.findElement(installments).getText())
					&& property.contains(driver.findElement(summary).getText()) && property.contains(driver.findElement(duplicateUtilityBill).getText()))
				
				throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not displaying");
			
			
		}

		public void checkForContractServices(String property) throws Exception {

			util.scrollToElement(driver.findElement(contractServices));
			
			if(!property.contains(driver.findElement(sendingDoc).getText()) && property.contains(driver.findElement(supplyDeactivation).getText()))
				
				throw new Exception("Contrct Services are not matching with portal data");
			
		}
		
		public void checkForText(By checkObject, String property) throws Exception {
			
			
			String actualText = driver.findElement(checkObject).getText().trim();
			String value = actualText.replaceAll("\\s+", " ");
			String result = "NO";
			if(value.equalsIgnoreCase(property)){
				result="YES";
			}
			if(result.equals("NO"))
				throw new Exception(value+" Text is not matching with "+property);
			
		}
		
		public void checkLiveChatReason(String property) throws Exception{
			
			
				if(! property.contains(driver.findElement(bolletaLiveChat).getText()) && property.contains(driver.findElement(pagamentiLiveChat).getText()) && property.contains(driver.findElement(contrattoLiveChat).getText())
						&& property.contains(driver.findElement(enelpremiaLiveChat).getText()) && property.contains(driver.findElement(sitoWebLiveChat).getText()) && property.contains(driver.findElement(infoGenericheLiveChat).getText()))										

					throw new Exception("Live chat reason is not matching");					
		
		}
		
		public void checkBolletaSection(String property) throws Exception{
			
			if(! property.contains(driver.findElement(infoSuvociinBolletta).getText()) && property.contains(driver.findElement(mancatoRecapitoBolletta).getText()) && property.contains(driver.findElement(richiestaRettificaBolletta).getText())
					&& property.contains(driver.findElement(webZoomBolletta).getText()) && property.contains(driver.findElement(canoneRaiBolletta).getText()) && property.contains(driver.findElement(altroBolletta).getText()))										

				throw new Exception("Bolleta section is not matching");					
			
		}
		
		
		public void VerifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String textfield_found="NO";
			
			WebElement element = driver.findElement(checkObject);
			
			String actualtext = element.getText();
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					}
		
		public void checkLiveChatMessage() throws InterruptedException{
			
			Thread.sleep(15000);
			
			driver.switchTo().frame("iframechatap");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("ChatTranscript");
			System.out.println("After switching to frame");
			String actualText = driver.findElement(By.xpath("//body[@id='bodyChatTranscripts']//tbody//tr[3]//td[@class ='msg-operatore']//div[@class='text']")).getText();
			System.out.println("text "+ actualText);
		}
		
		public void switchFrame(String frame) throws Exception{
			util.switchToFrame(frame);
		}
		
		public void switchToDefault(){
			driver.switchTo().defaultContent();
		}
		
		public boolean verifyExistenceInFrame(String frame,By element, int timeout) throws Exception {
			try {
			// driver.switchTo().frame(frame);
			WebElement frameToSw = this.driver.findElement(
			By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
			);
			this.driver.switchTo().frame(frameToSw);
			new WebDriverWait(driver,timeout).until(ExpectedConditions.presenceOfElementLocated(element));

			String text = driver.findElement(element).getText();
			System.out.println(text+" Text");
			}catch ( Exception e) {
			return false;
			}
			return true;
			}
			
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkLoadingMessage(By checkObject, String msg) throws Exception {
			
			try{
			Boolean isPresent = driver.findElements(checkObject).size() > 0;
			if(isPresent.equals("true"))
			{
				comprareText(checkObject, msg, true);
			}
			else{
				util.waitAndGetElement(chatText);
			}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		public void jsClick(By checkObject) throws Exception{
			util.jsClickElement(checkObject);
		}
		
		public static final String chatMessage = "Grazie per averci contattato in chat. Riceverai assistenza il prima possibile.Stai entrando in chat.... ElenaCiao sono Elena, l’Assistente Virtuale di Enel Energia, sono qui per aiutarti a usufruire al meglio dei nostri servizi. ElenaSe hai bisogno di aiuto prova a farmi una domanda.Per esempio: Posso pagare le bollette con carta di credito? Oppure, posso comunicarti l'autolettura? Quanto è l'importo della mia bolletta?";
		public static final String loadingMessage = " Ti preghiamo di attendere. Ti forniremo assistenza il prima possibile.I contenuti di questa chat, nonche' le valutazioni facoltativamente espresse dal Cliente con la compilazione di un apposito questionario presente in area riservata, potranno essere trattati "
				+ "dagli incaricati di Enel Energia e conservati per quanto necessario ai fini del monitoraggio sulla qualita' del nostro servizio e la correttezza delle informazioni offerte, nel rispetto della normativa sulla privacy. Nel caso in cui dovesse richiedere il supporto dell'operatore per il compimento delle attivita' self care fruibili da web, i suoi dati personali saranno trattati e conservati dagli incaricati di Enel Energia a tal fine preposti, nel rispetto della normativa sulla privacy.";
		public static final String bolletaSection = "Info su voci in bolletta| Mancato recapito|Richiesta rettifica bolletta|Bolletta Web/Zoom |Canone Rai |Altro";
		public static final String LIVECHATREASONS =" Bolletta|Pagamenti|Contratto|EnelPremia Wow!|Sito Web| Info Generiche";
		public static final String HAI_BISOGNO_EXPANDING_TEXT ="Clicca sul riquadro e un nostro espertoti risponderà E' ancora in linea?";
		public static final String SupplyServices = "Autolettura InfoEnelEnergia Modifica Indirizzo di Fatturazione Modifica Potenza e/o tensione  Modifica Contatti e Consensi ";
		public static final String ServiziSubText = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
		public static final String BolleteSubText = "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette";
		public static final String ContrattoSubText = "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture";
		public static final String serviziText = "Servizi per le forniture";
		
		//357_A
		public static final String HEADING_autolettura = "Comunica la lettura del contatore";
		public static final String SUBTITLEautolettura = "Seleziona la fornitura attiva su cui effettuare l'autolettura.";
		public static final String TITLE_autolettura = "Prima di effettuare la lettura ti consigliamo di controllare lo storico.";
		public static final String STORICOLETTURE_POPUP_TEXT = "Siamo spiacenti, ma non sono ancora state rilevate letture sulla fornitura selezionata.";
}
