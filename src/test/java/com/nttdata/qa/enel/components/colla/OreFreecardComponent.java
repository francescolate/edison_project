package com.nttdata.qa.enel.components.colla;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OreFreecardComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By datiAggiornati = By.xpath("//div[@id='datiAggiornati']");
	public By homePageHeading1 = By.xpath("//div[@id='mainContentWrapper']/div[@class='heading']/h1");
	public By homePageTitle = By.xpath("//div[@id='mainContentWrapper']/div[@class='heading']/p[.='In questa sezione potrai gestire le tue forniture']");
	public By homePageHeading2 = By.xpath("//section[@id='lista-forniture']/div[@class='section-heading']/h2");
	public By homePageTitle2 = By.xpath("//section[@id='lista-forniture']/div[@class='section-heading']/p[contains(text(), 'Visualizza le informazioni')]");
	public By electricalCard = By.xpath("//div[@class='card forniture regolare']");
	public By gestisciOreFreeButton = By.xpath("//div[@id='wrapper_modalforn']//div/a[2]");
	public By rinominaFornituraButton = By.xpath("//div[@id='mainContentWrapper']/div//nav/a[1]");
	public By visualizzaBollette = By.xpath("//div[@id='mainContentWrapper']/div//nav/a[1]");
	public By lightIcon = By.xpath("//div//span[2]/dt/span");
	public By supplyHeading = By.xpath("//div[@class='heading supply']/dl/span[1]/dt");
	public By numeroClienta = By.xpath("//div[@class='heading supply']/dl/span[3]/dt");
	public By numeroClientaValue = By.xpath("//div[@class='heading supply']/dl/span[3]/dd");
	public By statoFornitura = By.xpath("//div[@class='supply details-container']/dl/span[1]/dt");
	public By statoPagamento = By.xpath("//div[@class='supply details-container']/dl/span[2]/dt");
	public By modalitaDePagamento = By.xpath("//div[@class='supply details-container']/dl/span[3]/dt");
	public By ricezioneBollette = By.xpath("//div[@class='supply details-container']/dl/span[4]/dt");
	public By mostraDiPIU = By.xpath("//div[@id='mainContentWrapper']/div//div[@class='open-supply']");
	public By moreSupplyDetails = By.xpath("//div[@class='supply details-container supply-max-size']/dl/span/dt");
	public By oreFreeDiOggi = By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-block oreFree-oggi_container']//h2");
	public By Sceglilatuafascia = By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-heading']/a[@class='scroll-fasce_daily forniture-link forniture-side-link ore-free-switch-desktop']");
	public By oreFreeDiOggiText = By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-block oreFree-oggi_container']//p");
	public By FasceOreFree = By.xpath("//div[@class='single-info-box']/div[@class='info-title']");
	public By FasceOreFreeTiming = By.xpath("//div[@id='fasciaGiornalieraImpostata']");
	public By consumiGiornalieri = By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-block consumi-giornalieri_container']//h2");
	public By consumiGiornalieriText = By.xpath("//div[@id='box_orefreedetail']//div[@class='forniture-info-block consumi-giornalieri_container']/div/p");
	public By scegliIlGiornoLink = By.xpath("//div[@id='box_orefreedetail']//div/a[@class='forniture-link forniture-side-link ore-free-switch-desktop modal-choose-day']");
	public By domenicaGiugno = By.xpath("//div[1]/b[@id='today']");
	public By oreFreeTimings = By.xpath("//div[@id='freeSlot']");
	public By fasciaBase = By.xpath("//div[@id='anchor_fasce']/div/h2");
	public By fasciaBaseText1 = By.xpath("//div[@id='anchor_fasce']/div/p[2]");
	public By fasciaBaseText2 = By.xpath("//div[@id='anchor_fasce']/div/p[3]");
	public By fasciaOreFree = By.xpath("//div[@id='box_orefreedetail']/section[@class='cm-forniture-infos']/div[@id='anchor_fasce']/div[@class='forniture-info-body info-rows info-rows-bg ']/div[@class='single-info-box ']/div[@class='infos ']/div[@class='infos-inner ']/div[@class='info-title ']");
	public By fasciaOreFreeTimings = By.xpath("//div[@id='fasciaGiornalieraBase']");
	public By modificaLink = By.xpath("//div[@id='anchor_fasce']//div[2]/a[@class='forniture-link modify-current']");
	public By fasceGiornaliere = By.xpath("//div[@id='fasce_daily']/div/h2");
	public By fasceGiornaliereText1 = By.xpath("//div[@id='fasce_daily']/div/p[1]");
	public By fasceGiornaliereText2 = By.xpath("//div[@id='fasce_daily']/div/p[2]");
	public By day = By.xpath("//div[@id='fasce_daily']/div[@class='forniture-info-body info-rows ']/div[1]/div[1]/div[1]");
	public By dayOreFree = By.xpath("//div[@id='fasce_daily']/div[@class='forniture-info-body info-rows ']/div[1]/div[1]/div[2]");
	//public By IconInfoCircle = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[3]/dd/a/span");
	public By iconInfoCircle = By.xpath("//span[text()='Bollettino Postale']/following-sibling::a");
	public By PopupTitle = By.xpath("//div[@id='modalAlert']/div/h3");
	public By PopupInnerText = By.xpath("//div[@id='modalAlert']/div/p");
	public By ScopriButton = By.xpath("//button[@id='overlayYesButton']/span");
	public By CloseButton = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/a[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By iconInfocartacea = By.xpath("//*[text()='Ricezione bollette']/following-sibling::*/a");
	public By cartaceaPopupTitle = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/h3[@class='bold-title']");
	public By cartaceaPopupInnertext = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/p[@class='inner-text']");
	public By autolettura = By.xpath("//section[@id='forniture-id0']/ul/li[1]/a/div[2]/h3");
	public By infoEnelEnergia = By.xpath("//section[@id='forniture-id0']/ul/li[2]/a/div[2]/h3");
	public By modificaIndirizzoFatturazione = By.xpath("//section[@id='forniture-id0']/ul/li[3]/a/div[2]/h3"); 
	public By modificaPotenzaTensione = By.xpath("//section[@id='forniture-id0']/ul/li[4]/a/div[2]/h3");
	public By modificaContattiConsensi = By.xpath("//section[@id='forniture-id0']/ul/li[5]/a/div[2]/h3");
	public By serviziPerLiForniture = By.xpath("//section[@id='forniture-id0']/div[@class='section-heading']/h2");
	public By bollettaWeb = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[1]/a/div[2]/h3");
	public By pagamentoOnline = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[2]/a/div[2]/h3");
	public By addebitoDiretto = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[3]/a/div[2]/h3");
	public By rettificaBolletta = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[4]/a/div[2]/h3");
	public By rateizzazione = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[5]/a/div[2]/h3");
	public By bollettaDiSintesidiDettaglio = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[6]/a/div[2]/h3");
	public By duplicatoBolletta = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[7]/a/div[2]/h3");
	public By serviziPerBollette = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/div[@class='section-heading']/h2");
	public By bollettaWebAttiva = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[1]/a/div[2]//h3");
	public By addebitoDirettoAttiva = By.xpath("//div[@id='mainContentWrapper']/section[@id='forniture-id1']/ul/li[3]/a/div[2]//h3");
	public By modificaAll = By.xpath("//div/div/a[@class='forniture-link modify-daily']");
	public By modificaFasciaGiornalieraButton = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box ']//a");
	public By serviziPerLiControtto = By.xpath("//section[@id='forniture-id2']/div/h2");
	public By invioDocumenti = By.xpath("//section[@id='forniture-id2']/ul/li/a/div//*[text()='Invio Documenti']");
	public By disattivazioneFornitura = By.xpath("//section[@id='forniture-id2']/ul/li/a/div//*[text()='Disattivazione Fornitura']");
	public By serviziPerLiContrattoHeading = By.xpath("//section[@id='forniture-id2']/div/p[2]");
	
	public OreFreecardComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
			}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
		
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void  isElementPresent(By existentObject ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(existentObject));
		String elementFound = "NO";
		
		try{
		WebElement we =driver.findElement(By.xpath("//div[@class='card forniture regolare']//*[contains(text(), '11:00 - 14:00')]"));
		we.isDisplayed();
		elementFound = "YES";
		} 
        catch (Exception e) {

            System.out.println("Element Not found trying again - " );
            e.printStackTrace();}
         }
		
	public void checkForSupplyDetails(String property) throws Exception {
		
		if(! property.contains(driver.findElement(statoFornitura).getText()) && property.contains(driver.findElement(statoPagamento).getText()) && property.contains(driver.findElement(modalitaDePagamento).getText())
				&& property.contains(driver.findElement(ricezioneBollette).getText()))
			
			throw new Exception("In the page 'https://www-colla.enel.it/' Supply Details are not matching");
		
		
	}
	
public void checkForservicesforsupplyDetails(String property) throws Exception {
		
		if(! property.contains(driver.findElement(autolettura).getText()) && property.contains(driver.findElement(infoEnelEnergia).getText()) && property.contains(driver.findElement(modificaIndirizzoFatturazione).getText())
				&& property.contains(driver.findElement(modificaPotenzaTensione).getText()) && property.contains(driver.findElement(modificaContattiConsensi).getText()))
			
			throw new Exception("In the page 'https://www-colla.enel.it/' Supply Details are not matching");
		
		
	}

public void checkForservicesforBillDetails(String property) throws Exception {
	
	if(! property.contains(driver.findElement(bollettaWeb).getText()) && property.contains(driver.findElement(pagamentoOnline).getText()) && property.contains(driver.findElement(addebitoDiretto).getText())
			&& property.contains(driver.findElement(rettificaBolletta).getText()) && property.contains(driver.findElement(rateizzazione).getText())
			&& property.contains(driver.findElement(bollettaDiSintesidiDettaglio).getText()) && property.contains(driver.findElement(duplicatoBolletta).getText()))
		
		throw new Exception("In the page 'https://www-colla.enel.it/' Supply Details are not matching");
	
	
}

public void checkForContrattoservicesforBillDetails(String property) throws Exception {
	
	if(! property.contains(driver.findElement(invioDocumenti).getText()) && property.contains(driver.findElement(disattivazioneFornitura).getText()));
		
		throw new Exception("In the page 'https://www-coll1.enel.it/' Contratto Service Details are not matching");
	
	
}

	public void verifyMoreSupplyDetails(By checkObject, String[] exp) throws Exception{
		
			List <WebElement> list  = driver.findElements(moreSupplyDetails);
			for (WebElement we : list) {
	    	System.out.println(we.getText());
	    	if(!Arrays.asList(exp).contains(we.getText()))
	    		throw new Exception ("The list of addresses are not available to the customer");
	       	    } 
	}
	
	public void modificaLinkDisplay(By checkObject) throws Exception{
		List <WebElement> list  = driver.findElements(By.xpath("//div/div/a[@class='forniture-link modify-daily']"));
		int i = list.size();
		for (int j=1;j<=i;j++){
			WebElement we = driver.findElement(By.xpath("//div["+ j +"]/div/a[@class='forniture-link modify-daily']"));
			we.isEnabled();
			if(j>=7)
				we.isEnabled();
	    		throw new Exception ("The list of addresses are not available to the customer");
		}
				
		}
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
		
	public void pressJavascript(By oggetto) throws Exception {
        WebElement element = util.waitAndGetElement(oggetto);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }
	
		
		public void verifyModificaComponentByDate(By by) throws Exception{
			for (int i=0;i<8;i++)
			{
			String itemXpath = by.toString().replace("By.xpath: ", "");
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM", Locale.ITALY);  
			String now = dtf.format(java.time.LocalDate.now().plusDays(i)); 
			int day = Integer.parseInt(now.substring(0, now.indexOf(" ")));
			String month = StringUtils.capitalize(now.substring(now.indexOf(" ")+1));
								
			by = By.xpath(itemXpath.replace("$DATE$", day+" "+month));
			if(util.exists(by, 15))
			{
				util.objectManager(by, util.scrollToVisibility, false);
							
				if(i < 6 && !driver.findElement(by).isEnabled())
				{
					throw new Exception("Fascia giornaliera non trovata.");
				}
				else if(i>6 && !driver.findElement(by).isEnabled())
			  {
				  driver.findElement(by).isEnabled();
				  throw new Exception("Fascia giornaliera non trovata.");
			  }
			}		  
		}
	}
	
	public static final String HOMEPAGE_HEADING1 = "Benvenuto nella tua area privata";
	public static final String HOMEPAGE_TITLE = "In questa sezione potrai gestire le tue forniture";
	public static final String HOMEPAGE_HEADING2 ="Le tue forniture";
	public static final String HOMEPAGE_TITLE2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String SUPPLY_HEADING = "L’indirizzo della tua fornitura luce è:";
	public static final String NUMERO_CLIENTA = "Numero Cliente:";
	public static final String NUMERO_CLIENTA_VALUE = "310488453";
	public static final String[] MORE_SUPPLY_DETAILS = {"Stato Fornitura","Stato pagamento","Modalità di pagamento","Ricezione bollette","Data di attivazione","Offerta attiva","Numero Offerta","POD","Potenza","Tensione","Contratto"};
	public static final String[] MORE_SUPPLY_DETAILS_VALUE ={};
	public static final String ORE_FREE_DI_OGGI = "Ore Free di oggi";
	public static final String ORE_FREE_DI_OGGI_TEXT ="Di seguito la tua fascia di Ore Free valida per oggi.";
	public static final String FASCE_ORE_FREE ="Fasce Ore Free";
	public static final String FASCE_ORE_FREE_TIMING ="11:00 - 14:00";
	public static final String CONSUMI_GIORNALIERI = "Consumi giornalieri";
	public static final String CONSUMI_GIORNALIERI_TEXT = "Di seguito potrai visualizzare i consumi relativi all'ultimo giorno per il quale è disponibile il dato.Potrai sempre avere una vista storica dei consumi giornalieri cambiando il giorno.";
	public static final String DOMENICA_GUIGNO = "Domenica 14 Giugno";
	public static final String ORE_FREE_TIMINGS = "Ore Free: 11:00 - 14:00";
	public static final String FASCIA_BASE = "Fascia Base";
	public static final String FASCIA_BASE_TEXT1 ="La fascia di Ore Free di base impostata pertutti i giorni.";
	public static final String FASCIA_BASE_TEXT2 ="Ricordati che il cambiofasciabasesarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura.";
	public static final String FASCIA_ORE_FREE = "Fascia Ore Free";
	public static final String FASCIA_ORE_FREE_TIMINGS = "11:00 - 14:00";
	public static final String FASCE_GIORNALIERE = "Fasce Giornaliere";
	public static final String FASCE_GIORNALIERE_TEXT1 = "La fascia Ore Free che puoi impostare a piacimento per ciascuno deiprossimi 7 giorni.In alternativa verrà applicata la Fascia Base che hai scelto.";
	public static final String FASCE_GIORNALIERE_TEXT2 = "Ricordati che il cambiofasciagiornalierasarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura.";
	public static final String WEEKLY_DETAIL_DISPLAY1 = "";
	public static final String POPUP_TITLE = "Lo sapevi che puoi addebitare l’importo della tua bolletta sulla tua carta di credito o sul tuo conto corrente?";
	public static final String POPUP_TITLE_INNERTEXT ="Scegli la strada che non costa fatica, passa all’addebito diretto e scopri tutti i vantaggi di questo metodo di pagamento.";
	public static final String CARTACEA_POPUP_TITLE = "Lo sapevi che puoi ricevere più velocemente la tua bolletta?";
	public static final String CARTACEA_POPUP_INNERTEXT ="Attiva il servizio Bolletta Web e non dovrai più aspettare che la bolletta ti venga recapitata a casa evitando così i rischi di smarrimento e in più contribuisci al rispetto dell’ambiente riducendo l’uso di carta.";
	public static final String SERVIZI_PER_LI_FORNITURE = "Servizi per le forniture";
	public static final String SERVIZI_PER_LI_BOLLETTE = "Servizi per le bollette";
	public static final String BOLLETTA_WEB = "Bolletta Web";
	public static final String ADDEBITO_DIRETTO_ATTIVA = "Addebito Diretto";
	public final static String BILL_SERVICE_DETAILS = "Bolletta Web|Pagamento Online|Addebito Diretto|Rettifica Bolletta|Rateizzazione|Bolletta di Sintesi o di Dettaglio|Duplicato Bolletta";
	public static final String SUPPLY_SERVICE_DETAILS = "Autolettura|InfoEnelEnergia|Modifica Indirizzo Fatturazione|Modifica Potenza e/o Tensione|Modifica Contatti e Consensi";
	public static final String FASCIA_ORE_FREE_TEXT = "Fascia Ore Free 11:00 - 14:00";
	public static final String SERVIZIPERLI_CONTRATTO ="Servizi per il contratto";
	public static final String SERVIZIPERLI_CONTRATTO_HEADING = "Di seguito potrai visualizzare tutti i servizi per gestire il contratto delle tue forniture";
}
