package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Privato253InfoEnelEnergiaAcrComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By dettaglioFornitura = By.xpath("(//div[@class='card-heading']/h3[text()='Luce']/ancestor::*/following-sibling::div/a[text()='Dettaglio fornitura'])[1]");
	public By dettaglioFornituraLuce = By.xpath("(//div[@class='card-heading']/h3[text()='Luce'])[1]/parent::div/following-sibling::dl/span/dd[text()='Attiva']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
	public By dettaglioFornituraGas = By.xpath("(//dd[text()='Gas'][1]/ancestor::dl/following-sibling::dl//dd[text()='Attiva']/ancestor::div[@class='details-container']/following-sibling::div/a[contains(text(),'Dettaglio fornitura')])[1]");
	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
	
	public By serviziPerLeForniture = By.xpath("//h2[text()='Servizi per le forniture']");
	
	public By serviziPerLeFornitureSubText = By.xpath("//h2[text()='Servizi per le forniture']/following-sibling::p[2]");
	
	public By infoEnelEnergia_button = By.xpath("//h3[text()='InfoEnelEnergia']/parent::div");
	public By infoEnelEnergia_button_status = By.xpath("//h3[text()='InfoEnelEnergia']/parent::div/span");
	public By infoEnelEnergia_Heading = By.xpath("//div[@class='section-heading']/h2[text()='InfoEnelEnergia']");
	public By infoEnelEnergia_Subtext = By.xpath("//div[@class='section-heading']/p[contains(text(),'Il servizio InfoEnelEnergia')]");
	
	public By infoEnelEnergia_non_e_attivo_status = By.xpath("//b[text()='non è attivo ']/parent::p");
	
	public By AttivaInfoEnelEnergia_button = By.xpath("//button/span[text()='ATTIVA INFOENELENERGIA']");
	public By ESCI_button = By.xpath("//button[text()='ESCI']");
	
	public By AttivaInfoEnelEnergia_PageTitle = By.xpath("//div/h1[@id='titolo-dispositiva']");
	public By AttivaInfoEnelEnergia_Description = By.xpath("//div/h2[contains(text(),'Il servizio InfoEnelEnergia')]");
	
	public By AttivaInfoEnelEnergia_ESCI_button = By.xpath("//button//span[text()='Esci']");
	public By AttivaInfoEnelEnergia_CONTINUA_button = By.xpath("//button//span[text()='CONTINUA']");
	
	
	public By selectAttivaInfoEnelEnergiaCheckbox = By.xpath("//span[@class='flex']/span[contains(@id,'checkbox-item-')]");
	
	public Privato253InfoEnelEnergiaAcrComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
public void verifyCheckboxnotSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (!element.isSelected()){
		checkbox="YES";
		}			
		if (element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is selected");
				}
	}
	
	
	public void verifyCheckboxSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (element.isSelected()){
		checkbox="YES";
		}			
		if (!element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is not selected");
				}
	}

	public void selectAttivaInfoEnelEnergiaCheckbox(By checkObject) throws Exception
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		
		List<WebElement> chkbx = driver.findElements(selectAttivaInfoEnelEnergiaCheckbox);
		
		for (int i = 0; i < chkbx.size(); i++) {
			
			verifyCheckboxnotSelected(By.xpath("(//span[@class='flex']/span[contains(@id,'checkbox-item-')])["+(i+1)+"]"));
			clickComponent(By.xpath("(//span[@class='flex']/span[contains(@id,'checkbox-item-')])["+(i+1)+"]"));
			
		}
	}
	
	public void VerifyText(By checkObject, String Value) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	
	String textfield_found="NO";
	
	WebElement element = driver.findElement(checkObject);
	
	String actualtext = element.getText();
	
	
	if (actualtext.contentEquals(Value))
	textfield_found="YES";
					
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected text" + Value + " is not found:");
	}

	
	public String infoEnelEnergiaSubtext = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
	public String attivaInfoEnelEnergia_Description = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
	public String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
}
