package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ServiziOnlineCaricaDocumentiComponent {
	WebDriver driver;
	SeleniumUtilities util;
	JavascriptExecutor js;
	
	private List<By> items;
	public List<By> error;
	public List<By> errorNoDisplay;
	
		
	//First page
	public By buttonMsg=By.xpath("//button[contains(text(),'Invia un messaggio')]");
	public By text1By=By.xpath("//p[@id='paragrafo_1IT']");
	public By textSub1By=By.xpath("//p[@id='p_it']");
	public By text2By=By.xpath("//p[@id='paragrafo_2IT']");
	public By cliccaQuiLanguage=By.xpath("//a[@id='open_modal']");
	
	//Second page after click button
	public By main=By.xpath("//main[@id='main']");
	public By titleBy=By.xpath("//*[@id='titolo_invia_messaggio_contatti_no_ass_cons' and contains(text(),'Contatti')]");
	public By inserisciDatiBy=By.xpath("//*[@class='title-inserisci-dati' and contains(text(),'INSERISCI I TUOI DATI')]");
	public By privacyBy=By.xpath("//*[@id='label_testo_privacy' and contains(text(),'Informativa Privacy*')]");
	public By nomeBy=By.xpath("//div[@class='form-group']//child::label[@id='label_nome' and contains(text(),'Nome*')]//following-sibling::input[@type='text' and @id='inputNome']");
	public By cognomeBy=By.xpath("//div[@class='form-group']//child::label[@id='label_cognome' and contains(text(),'Cognome*')]//following-sibling::input[@type='text' and @id='inputCognome']");
	public By prefissoBy=By.xpath("//label[@id='label_prefisso' and contains(text(),'Prefisso')]");
	public By prefissoDefaultBy=By.xpath("//*[@id='inputPrefissoSelectBoxItText' and contains(text(),'+39')]//ancestor::span[@role='combobox']");
	public By cellulareBy=By.xpath("//div[@class='form-group']//child::label[@id='label_cellulare' and contains(text(),'Cellulare*')]//following-sibling::input[@type='text' and @id='inputCellulare']");
	public By indirizzoBy=By.xpath("//div[@class='form-group']//child::label[@id='label_indirizzo' and contains(text(),'Indirizzo')]//following-sibling::input[@type='text' and @id='inputIndirizzo']");
	public By emailBy=By.xpath("//div[@class='form-group']//child::label[@id='label_email' and contains(text(),'Indirizzo e-mail*')]//following-sibling::input[@type='email' and @id='inputEmail']");
	public By confermaEmailBy=By.xpath("//div[@class='form-group']//child::label[@id='label_conferma_mail' and contains(text(),'Conferma indirizzo e-mail*')]//following-sibling::input[@type='email' and @id='inputConfermaEmail']");
	public By codiceFiscalePIvaBy=By.xpath("//div[@class='form-group']//child::label[@for='inputUsername' and contains(text(),'Codice Fiscale / Partita IVA*')]//following-sibling::input[@type='text' and @id='inputUsername']");
	public By seiGiaClienteBy=By.xpath("//label[@id='cliente' and contains(text(),'Sei già cliente?')]");
	public By seiGiaClienteSiBy=By.xpath("//label[@id='cliente' and contains(text(),'Sei già cliente?')]//parent::div//child::div[@aria-checked='true']");
	public By seiGiaClienteNoBy=By.xpath("//label[@id='cliente' and contains(text(),'Sei già cliente?')]//parent::div//child::div[2]");
	public By numeroClienteBy=By.xpath("//div[@class='form-group']//child::label[@for='inputFornitura' and contains(text(),'Numero cliente')]//following-sibling::input[@type='text' and @id='inputFornitura']");
	public By asteriscoNumeroClienteBy=By.xpath("//span[@id='asterisco_id_cliente' and text()='*']");
	public By diCosaHaiBisognoBy=By.xpath("//label[@id='label_bisogno' and contains(text(),'Di cosa hai bisogno?')]");
	public By inviareUnaRichiestaBy=By.xpath("//label[@id='label_inputBisogno_si' and contains(text(),'Inviare una richiesta')]//parent::div//child::input[@checked='checked' and @role='radiogroup']");
	public By informazioniBy=By.xpath("//label[@id='label_inputBisogno_no' and contains(text(),'Informazioni')]");
	public By argomentoBy=By.xpath("//div[@class='form-group']//child::label[@id='inputTipoLabel' and contains(text(),'Argomento*')]//following-sibling::select[@id='inputTipo']");
	public By inputArgomento = By.xpath("//*[@id='inputTipoSelectBoxIt']");
	public By messaggioBy=By.xpath("//div[@class='form-group']//child::label[@id='label_inputNote' and contains(text(),'Messaggio*')]//following-sibling::textarea[@id='inputNote']");
	public By vuoiInserireAllegatiBy=By.xpath("//label[@id='label_inputAllegati' and contains(text(),'Vuoi inserire allegati?')]");
	public By vuoiInserireAllegatiSiBy=By.xpath("//label[@for='inputAllegati_si' and contains(text(),'Sì')]//parent::div//child::input[@type='radio']");
	public By vuoiInserireAllegatiNoBy=By.xpath("//label[@for='inputAllegati_no' and contains(text(),'No')]//parent::div//child::input[@type='radio' and @checked='checked']");
	public By informativaTextBy=By.xpath("//textarea[@id='testo_privacy']");
	public By accettoBy=By.xpath("//label[@id='inputprivacy_si_label' and contains(text(),'Accetto')]//parent::div//child::input[@type='radio']");
	public By accettoBy2=By.xpath("//*[@id='form_privacy_accept']");
	public By nonAccettoBy=By.xpath("//label[@id='inputprivacy_no_label' and contains(text(),'Non accetto')]//parent::div//child::input[@type='radio' and @checked='checked']");
	public By proseguiBy=By.xpath("//button[@id='accediupl' and @type='button']");
	public By campiObbligatoriBy=By.xpath("//label[@id='campiobbligatori' and text()='*campi obbligatori']");
	public By textFinalBy=By.xpath("//*[@id='enelupl']/section/div[1]/div[1]");
	public By buttonIndietro=By.xpath("//*[@id='accediupl_Indietro']");
	public WebElement mainWE;
	public WebElement accettoWE;
	public WebElement nonAccettoWE;
	public WebElement inputPrivacy;
	public WebElement proseguiWE;
	public By inputNome = By.xpath("//*[@id='inputNome']");
	public By inputCognome = By.xpath("//*[@id='inputCognome']");
	public By inputCellulare = By.xpath("//*[@id='inputCellulare']");
	public By inputEmail = By.xpath("//*[@id='inputEmail']");
	public By inputConfermaEmail = By.xpath("//*[@id='inputConfermaEmail']");
	public By inputUsername = By.xpath("//*[@id='inputUsername']");
	public By inputMessaggio=By.xpath("//*[@id='inputNote']");
	public By inputNumeroCliente=By.xpath("//*[@id='inputFornitura']");
	
	public By selectNuovoContratto = By.xpath("//*[@id='inputTipoSelectBoxIt']//child::span[2][contains(text(),'Nuovo Contratto')]");


	
	
	
	public By lingueDisponibili=By.xpath("//*[@class='md-content']/div/h2");
	public By tabellaLingue=By.xpath("//*[@class='md-content']/div/table");
	public By buttonCloseLanguage=By.xpath("//a[@role='button' and @aria-label='chiudi questa finestra di dialogo']");
	
	public By errorPrivacy=By.xpath("//div[not(contains(@style,'display: none')) and @id='errorPrivacy' and contains(text(),\"Per poter procedere è necessario accettare l'informativa sulla privacy\")]");
	public By errorNome=By.xpath("//div[not(contains(@style,'display: none')) and @id='inputNome_error' and contains(text(),'Il campo è obbligatorio')]");
	public By errorCognome=By.xpath("//div[not(contains(@style,'display: none')) and @id='inputCognome_error' and contains(text(),'Il campo è obbligatorio')]");
	public By errorCellulare=By.xpath("//div[not(contains(@style,'display: none')) and @id='inputCellulare_error' and contains(text(),'Il numero non è formalmente valido')]");
	public By errorEmail=By.xpath("//div[not(contains(@style,'display: none')) and @id='inputEmail_error' and contains(text(),'Il campo è obbligatorio')]");
	public By errorEmailInvalid=By.xpath("//div[not(contains(@style,'display:none')) and @id='error_message_invalid_ass_mail' and contains(text(),'Il valore inserito non è valido')]");
	public By errorConfermaEmail=By.xpath("//div[not(contains(@style,'display: none')) and @id='inputConfermaEmail_error' and contains(text(),\"Digitare nuovamente l'indirizzo e-mail\")]");
	public By errorConfermaEmail_2=By.xpath("//div[not(contains(@style,'display: none')) and @id='inputConfermaEmail_error' and contains(text(),\"L'indirizzo e-mail non corrisponde a quello digitato in precedenza\")]");
	public By errorCodiceFiscalePIva=By.xpath("//div[not(contains(@style,'display:none')) and @id='inputUsername_error' and contains(text(),'Il Codice Fiscale/Partita IVA è obbligatorio')]");
	public By errorCodiceFiscalePIvaInvalid=By.xpath("//div[@id='inputUsername_error' and contains(text(),'Il Codice Fiscale/Partita IVA deve essere formalmente valido e non può essere uguale a quello del delegato')]");
	public By errorFornitura=By.xpath("//div[not(contains(@style,'display:none')) and @id='inputFornitura_error' and contains(text(),'Il numero cliente, se indicato, deve essere di 9 cifre')]");
	public By errorArgomento=By.xpath("//div[@id='select_missing_field_2' and contains(text(),'Il campo è obbligatorio')]");
	public By errorMessaggio=By.xpath("//div[not(contains(@style,'display:none')) and @id='error_message_note_id' and contains(text(),'Il campo è obbligatorio')]");
	
	public By errorPrivacyNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='errorPrivacy' and contains(text(),\"Per poter procedere è necessario accettare l'informativa sulla privacy\")]");
	public By errorNomeNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='inputNome_error' and contains(text(),'Il campo è obbligatorio')]");
	public By errorCognomeNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='inputCognome_error' and contains(text(),'Il campo è obbligatorio')]");
	public By errorCellulareNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='inputCellulare_error' and contains(text(),'Il numero non è formalmente valido')]");
	public By errorEmailNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='inputEmail_error' and contains(text(),'Il campo è obbligatorio')]");
	public By errorEmailInvalidNoDisplay=By.xpath("//div[contains(@style,'display:none') and @id='error_message_invalid_ass_mail' and contains(text(),'Il valore inserito non è valido')]");
	public By errorConfermaEmailNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='inputConfermaEmail_error' and contains(text(),\"Digitare nuovamente l'indirizzo e-mail\")]");
	public By errorCodiceFiscalePIvaNoDisplay=By.xpath("//div[contains(@style,'display:none') and @id='inputUsername_error' and contains(text(),'Il Codice Fiscale/Partita IVA è obbligatorio')]");
	//public By errorCodiceFiscalePIvaInvalidNoDisplay=By.xpath("//div[@id='error_message_invalid_cf_piva' and contains(text(),'Il Codice Fiscale/Partita IVA deve essere formalmente valido e non può essere uguale a quello del delegato')]");
	public By errorFornituraNoDisplay=By.xpath("//div[contains(@style,'display:none') and @id='inputFornitura_error' and contains(text(),'Il numero cliente, se indicato, deve essere di 9 cifre')]");
	//public By errorArgomentoNoDisplay=By.xpath("//div[contains(@style,'display: none') and @id='error_argomento_no_value' and contains(text(),'Il campo è obbligatorio')]");
	public By errorMessaggioNoDisplay=By.xpath("//div[contains(@style,'display:none') and @id='error_message_note_id' and contains(text(),'Il campo è obbligatorio')]");
	public By si = By.xpath("//label[@id='inputCliente_si_label']");
	public By no = By.xpath("//label[@id='inputCliente_no_label']");
	public By inviareUnaRichiesta = By.xpath("//label[@id='label_inputBisogno_si']");
	public By informazioni = By.xpath("//label[@id='label_inputBisogno_no']");
	
	
    public String text1= "Sei già cliente Enel Energia? Registrati subito o accedi alla nostra Area Clienti, potrai gestire le tue forniture in autonomia. Bastano pochi click! Se non sei cliente Enel Energia, scegli l'offerta migliore per te nella sezione LUCE E GAS. Per ogni altra esigenza, puoi mandarci un messaggio.";
    public String textSub1= "Puoi scriverci in diverse lingue, clicca qui per visualizzare l’elenco";
    public String text2= "Se rappresenti un'Associazione dei Consumatori, clicca qui.";
    public String buttonText= "Invia un messaggio";
    public String title= "Contatti";
    public String inserisciDati= "INSERISCI I TUOI DATI";
    public String privacy= "Informativa Privacy*";
    public String nome= "Nome*";
    public String cognome= "Cognome*";
    public String prefisso= "Prefisso";
    public String cellulare= "Cellulare*";
    public String indirizzo= "Indirizzo";
    public String email= "Indirizzo e-mail*";
    public String emailConferma= "Conferma indirizzo e-mail*";
    public String codiceFiscalePIva= "Codice Fiscale / Partita IVA*";
    public String seiGiaCliente= "Sei già cliente?";
    public String numeroCliente= "Numero cliente* ";
    public String diCosaHaiBisogno= "Di cosa hai bisogno?";
    public String argomento= "Argomento*";
    public String messaggio= "Messaggio*";
    public String vuoiInserireAllegati= "Vuoi inserire allegati?";
  
    public String informativaText= "INFORMATIVA AI SENSI DELL' ART. 13 del Regolamento UE 2016/679 (\"GDPR\")La informiamo che i dati personali ivi indicati sono raccolti e trattati per dare seguito alla sua segnalazione. L'informativa completa è disponibile sul sito www.enel.it. Titolare del trattamento è Enel Energia S.p.A., con sede legale in Viale Regina Margherita 125 - 00198 Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007. Per esercitare i diritti previsti agli articoli 15-22 del GDPR è possibile inviare una comunicazione alla casella di posta dedicata: privacy.enelenergia@enel.com.";
    public String listaLingue="1 Inglese2Francese3Tedesco4Spagnolo5Cinese6Arabo7Russo8Rumeno9Punjabi10Albanese11Serbocroato12Sloveno13Bengalese14Cingalese15Hindi16Polacco17Portoghese18Tigrino19Ucraino20Urdu";
    public String valuesArgomento="Seleziona Argomento...Attiva I Servizi O Acquista Un Bene Enel EnergiaAttiva O Revoca InfoEnelEnergiaAttiva Un Nuovo ContrattoAttiva Una Nuova Fornitura Di Energia ElettricaAttiva, Modifica O Revoca Bolletta WebComunica La LetturaEffettua Un Pagamento O Richiedi un RimborsoModifica I Dati Di Contratto O I ConsensiModifica Il Tuo Attuale Metodo Di PagamentoModifica La Potenza O La Tensione Della FornituraModifica L'intestazione Di Una Fornitura DomesticaPassa Ad Enel EnergiaRateizza La Tua BollettaRiattiva Una Fornitura CessataRichiedi Una Rettifica Della FatturaVisualizza La BollettaEnelpremia WOW!Altro Scopri L'offerta Fibra";
    public String textFinal="Grazie per averci contattato. Ti risponderemo nel più breve tempo possibile. Ti abbiamo inviato una mail con il riepilogo di quanto hai richiesto all'indirizzo di posta elettronica che hai indicato: se non la ricevi verifica nella cartella SPAM della tua casella di posta elettronica.";
    public String campiObligatori = "*campi obbligatori";		
    		
	public ServiziOnlineCaricaDocumentiComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		this.mainWE = driver.findElement(By.xpath("//main[@id='main']"));
		this.items = new ArrayList<By>(); 
		this.items.add(titleBy);
		this.items.add(inserisciDatiBy);
		this.items.add(privacyBy);
		this.items.add(nomeBy);
		this.items.add(cognomeBy);
		this.items.add(prefissoBy);
		this.items.add(prefissoDefaultBy);
		this.items.add(cellulareBy);
		this.items.add(indirizzoBy);
		this.items.add(emailBy);
		this.items.add(confermaEmailBy);
		this.items.add(codiceFiscalePIvaBy);
		this.items.add(seiGiaClienteBy);
		this.items.add(seiGiaClienteSiBy);
		this.items.add(seiGiaClienteNoBy);
		this.items.add(numeroClienteBy);
		this.items.add(diCosaHaiBisognoBy);
		this.items.add(inviareUnaRichiestaBy);
		this.items.add(informazioniBy);
		this.items.add(argomentoBy);
		this.items.add(messaggioBy);
		this.items.add(vuoiInserireAllegatiBy);
		this.items.add(vuoiInserireAllegatiSiBy);
		this.items.add(vuoiInserireAllegatiNoBy);
		this.items.add(informativaTextBy);
		this.items.add(accettoBy);
		this.items.add(nonAccettoBy);
		this.items.add(proseguiBy);
		this.items.add(campiObbligatoriBy);
		this.error = new ArrayList<By>();												;
		this.error.add(errorNome);
		this.error.add(errorCognome);
		this.error.add(errorCellulare);
		this.error.add(errorEmail);
		this.error.add(errorConfermaEmail);
		this.error.add(errorCodiceFiscalePIva);
		this.error.add(errorFornitura);
		this.error.add(errorArgomento);
		this.error.add(errorMessaggio);
		this.errorNoDisplay = new ArrayList<By>();
		this.errorNoDisplay.add(errorNomeNoDisplay);
		this.errorNoDisplay.add(errorCognomeNoDisplay);
		this.errorNoDisplay.add(errorCellulareNoDisplay);
		this.errorNoDisplay.add(errorEmailNoDisplay);
		this.errorNoDisplay.add(errorConfermaEmailNoDisplay);
		this.errorNoDisplay.add(errorCodiceFiscalePIvaNoDisplay);
		this.errorNoDisplay.add(errorFornituraNoDisplay);
		//this.errorNoDisplay.add(errorArgomentoNoDisplay);
		this.errorNoDisplay.add(errorMessaggioNoDisplay);
		this.accettoWE = driver.findElement(By.xpath("//*[@id='inputprivacy_si_label']"));
		this.nonAccettoWE = driver.findElement(By.xpath("//*[@id='inputprivacy_no_label']"));
		this.inputPrivacy = driver.findElement(By.xpath("//*[@id='inputPrivacy']"));
		this.proseguiWE = driver.findElement(By.xpath("//button[@id='accediupl' and @type='button']"));
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("oggetto web con xpath " + existingObject + " non esiste.");
	}
	
	public void verifyComponentNotExistence(By existingObject) throws Exception {
		if (util.exists(existingObject, 15))
			throw new Exception("oggetto web con xpath " + existingObject + " esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	
	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkHeaderPageContattaci(By path,By title, By subtitle) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(path));
		String pathDescription=driver.findElement(path).getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	
				
		if (!pathDescription.contentEquals("HOME / SUPPORTO / CONTATTACI"))
			throw new Exception("sulla pagina web 'Contattaci' l'oggetto web con xpath " + path + " non esiste");
		if (!util.exists(title, 15))
			throw new Exception("sulla pagina web 'Contattaci' il titolo con xpath " + title + " non esiste");
		if (!util.exists(subtitle, 15))
			throw new Exception("sulla pagina web 'Contattaci' il sottotitolo " + subtitle + " non esiste");	
	}
		
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");

  }
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
    public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
    
	public void verifyItems() throws Exception{
		for(By by: items){
			this.verifyComponentExistence(by);
		}
	}
	
	public void verifyListBy(List<By> errorList) throws Exception{
		for(By by: errorList){
			this.verifyComponentExistence(by);
		}
	}
	
	public void verifyNotListBy(List<By> errorList) throws Exception{
		for(By by: errorList){
			this.verifyComponentNotExistence(by);
		}
	}
	
	
	public void accettaPrivacy(){
		js = (JavascriptExecutor)driver;
		js.executeScript("$('#inputprivacy_si_label').click();",mainWE);
		js.executeScript("$('#inputPrivacy').val('SI')",mainWE);
		js.executeScript("$('#inputprivacy_si_label').toggleClass('active')",mainWE);
		js.executeScript("$('#inputprivacy_no_label').toggleClass('active')",mainWE);
	}
	
	public void sendKey(By by, String s){
		WebElement we = this.driver.findElement(by);
		we.sendKeys(s);
	}
	
	public void clearKey(By by){
		WebElement we = this.driver.findElement(by);
		we.clear();
	}
	
	public String findOption(By by){
		Select select = new Select(driver.findElement(by));
		List<WebElement> allArguments = new ArrayList();
		allArguments = select.getOptions();
		int i;
		String textArguments = "";
		for(i=0;i<allArguments.size();i++){
			WebElement we = allArguments.get(i);	;
			textArguments+=we.getAttribute("innerHTML");			
		}
		System.out.println("textArguments -->" + normalizeInnerHTML(textArguments));
		return normalizeInnerHTML(textArguments);
	}
	
	public void compareString(String a, String b)throws Exception{
		if(a.toUpperCase() != b.toUpperCase())
			throw new Exception("Stringe diverse " + a.toUpperCase() + "\n" + b.toUpperCase());
	}
	
	public void getArgomentoNuovoContratto()throws Exception{ 
		js = (JavascriptExecutor)driver;
		WebElement we = util.waitAndGetElement(inputArgomento);
		js.executeScript("$('#inputTipoSelectBoxIt').click()");
		we.sendKeys(Keys.DOWN,Keys.DOWN,Keys.DOWN, Keys.ENTER);
	}

	public void isDefaultRadioSelected(By existentObject) throws Exception{
		if (!driver.findElement(existentObject).isSelected())
			throw new Exception("The button is not Enabled");
		}
}
