package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PrivateAreaDettaglioFornitureComponent extends BaseComponent {
	
	public static String button_by_pod = "//div[@class='button-container']/a[@data-pod='#1' and text()='Visualizza Stato']";
	public static String button_by_ctl = "//div[@class='button-container']/a[@data-cf='#1' and text()='Visualizza Stato']";
	public static String element_by_text_and_parent_class ="//*[contains(@class,'#2')]//*[contains(text(),\"#1\")]";
	public String forniture_header_class = "heading";
//	public By link_open_supply = By.xpath("//div[@class='open-supply']/a");
	public By fornitureMenuItem = By.xpath("//ul[@id='internal-menu']//span[text()='Forniture']/parent::a");
	public By text_status_active = By.xpath("//li[@class='track-item active']/h3");
	public By pod = By.xpath("//a[@data-section='fornituraInAttivazione' and @data-pod='IT002E3810990A']");
	public PrivateAreaDettaglioFornitureComponent(WebDriver driver) {
		super(driver);
	}

}
