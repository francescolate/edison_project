package com.nttdata.qa.enel.components.lightning;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

import io.qameta.allure.Step;


public class CercaPODComponent {

	private WebDriver driver;
	private SeleniumUtilities util;
	public CercaPODComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(this.driver);
	}


	private By pod = By.xpath("//input[@id='ITA_IFM_POD__c' and @type='text']");

	private By searchBtn = By.xpath(".//button[@id='searchClients']");
	
	private By frameBy = By.xpath("//iframe[@title = 'accessibility title']");



			private void swichToDefault () throws Exception{
				this.driver.switchTo().defaultContent();
			}


			@Deprecated
			public void fillCfAndSearch(String param) throws Exception
			{
				TimeUnit.SECONDS.sleep(60);
				String x = util.frameSearcher(
						frameBy,
						pod,
						r->{r.sendKeys(param);
						}
						);
				this.driver.switchTo()
				.frame(x)
				.findElement(searchBtn)
				.click();
				this.swichToDefault();

			}
			
			public void fillCfAndSearch2(String param) throws Exception
			{
				TimeUnit.SECONDS.sleep(5);
				String frame = util.getFrameByIndex(0);
				util.frameManager(frame, pod, util.scrollAndSendKeys,param);
				util.frameManager(frame, searchBtn, util.scrollAndClick);
			}
}
