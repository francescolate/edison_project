package com.nttdata.qa.enel.components.colla;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModificaConsumoConcordatoComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	Properties prop;
	
	public By modificaConsumoConcordatoLink = By.xpath("//h3[text()='Modifica Consumo Concordato']/ancestor::a");
	public By modificaConsumoConcordatoPagesubText1 = By.xpath("//h2[text()='Modifica Consumo Concordato']/following-sibling::p[2]");
	public By modificaConsumoConcordatoPagesubText2 = By.xpath("//h2[text()='Modifica Consumo Concordato']/following-sibling::p[3]");
	public By cambiaConsumoButton = By.xpath("//span[text()='CAMBIA CONSUMO CONCORDATO']");
	public By confermaButton = By.xpath("//span[text()='Conferma']");
	public By fineButton = By.xpath("//span[text()='Fine']");
	public By step1Header = By.xpath("//span[text()='Inserimento dati']");
	public By step2Header = By.xpath("//span[text()='Riepilogo e conferma dati']");
	public By step3Header = By.xpath("//span[text()='Esito']"); 
	public By inserisciText = By.xpath("//h4[text()='Inserisci la nuova stima dei consumi']"); 
	public By consumoBimestraleConcordatoInput = By.xpath("//input[@class='form-control takeValue']");
	public By continuaButton = By.xpath("//span[text()='Continua']");
	public By checkBox = By.xpath("//span[starts-with(@class,'icon-ckbox')]");
	public By icon = By.xpath("//span[@class='icon-line-electricity']");
	public By indirizzo  = By.xpath("//span[text()='Indirizzo della fornitura']");
	public By consumo = By.xpath("//span[text()='Consumo Stimato']");
	public By letture = By.xpath("//a[text()='Storico letture']");
	public By successText1 = By.xpath("//h3[text()='Operazione eseguita correttamente']");
	public By successText2 = By.xpath("//p[contains(text(),'La tua richiesta')]");
	public By summaryText1 = By.xpath("//div[@class='blue-alert']/p");
	public By summaryText2 = By.xpath("//h4[text()='Il nuovo consumo bimestrale stimato:']");
	
	public ModificaConsumoConcordatoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
  
  public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
  
  public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
  
  public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
  
  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		//System.out.println(weText);
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void  isElementNotPresent(By existentObject ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		
		try{
			WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
		} 
        catch (Exception e) {
        	e.printStackTrace();
        		            
            }
         }
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
		

public void SwitchTabToSupporto(String Url) throws Exception
{
    String currentHandle = driver.getWindowHandle();
    Set <String> windows = driver.getWindowHandles();
   
   
    for (String winHandle : windows) {
       
        String pagetitle = driver.getTitle();
        driver.switchTo().window(winHandle);
     }
    	checkURLAfterRedirection(Url);
  }

public void SwitchTabToOriginalWindow() throws Exception
{
	String currentHandle = driver.getWindowHandle();
	driver.switchTo().window(currentHandle);
}
	
public void isElementDisabled(By existentObject) throws Exception{
	
	if ((driver.findElement(existentObject).isEnabled()))
			
		throw new Exception("The button is Enabled");
		}

public void isElementEnabled(By existentObject) throws Exception{
	
	if (!(driver.findElement(existentObject).isEnabled()))
			
		throw new Exception("The button is Enabled");
		}

public void switchToNewTab() throws Exception {
	Set <String> windows = driver.getWindowHandles();
	for (String winHandle : windows) {
		driver.switchTo().window(winHandle);
	}
}

public static final String ModificaConsumoConcordatoPagesubText1 = "Con l’offerta E-light, grazie al consumo concordato indicato in fase di sottoscrizione, hai scelto di ricevere le bollette contenenti sempre gli stessi consumi bimestrali ad eccezione della prima bolletta dopo ogni anno contrattuale, sulla quale viene calcolato il conguaglio rispetto ai consumi effettivi.";
public static final String ModificaConsumoConcordatoPagesubText2 = "Se hai notato che i tuoi consumi bimestrali non corrispondono a quelli stabiliti nel contratto o in successive modifiche, oppure hai modificato le tue abitudini di consumo, grazie a questo servizio puoi allineare il tuo consumo concordato a quello attuale.";
public static final String Step1HeaderText = "Inserimento dati";
public static final String Step2HeaderText = "Riepilogo e conferma dati";
public static final String Step3HeaderText = "Esito";
public static final String InserisciText = "Inserisci la nuova stima dei consumi";
public static final String SuccessText1 = "Operazione eseguita correttamente";
public static final String SuccessText2 = "La tua richiesta è andata a buon fine. Le prossime bollette saranno aggiornate sulla base del consumo che hai fornito.";
public static final String SummaryText1 = "Attenzione! Il consumo inserito differisce di oltre il 15% rispetto a quello attualmente impostato. Sei sicuro di voler procedere?";
public static final String SummaryText2 = "Il nuovo consumo bimestrale stimato:";
}
