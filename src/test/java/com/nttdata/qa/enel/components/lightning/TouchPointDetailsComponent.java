package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BaseComponent;

public class TouchPointDetailsComponent extends BaseComponent {
	public String rowsXpath = "//span[text()='Tipo e Sezione']/ancestor::thead/following-sibling::tbody/tr";
	public String dataCreazioneXpath = "//span[text()='Tipo e Sezione']/ancestor::thead/following-sibling::tbody//td//span[contains(text(),'$DATE$')]";
	public String tipoSezioneXpath = "//span[text()='Tipo e Sezione']/ancestor::thead/following-sibling::tbody/tr[$ROW_NUMBER$]/td[3]";
	public String dataScadenzaXpath = "//span[text()='Tipo e Sezione']/ancestor::thead/following-sibling::tbody/tr[$ROW_NUMBER$]/td[9]";
	

	public TouchPointDetailsComponent(WebDriver driver) {
		super(driver);
	}
	
	public int findNumberOfTpRecords(String referenceDate, int attempts) throws Exception {
		if (attempts <= 0) {
			throw new Exception("Cannot find records in Touch Point section");
		} else {
			System.out.println("Trying to access records in Touch Point section. Remaining attempts: " + (attempts - 1));
		}
		By dataCreazioneLocator = By.xpath(dataCreazioneXpath.replace("$DATE$", referenceDate));
		WebDriverWait wait = new WebDriverWait(driver, baseTimeoutInterval);
		List<WebElement> rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(dataCreazioneLocator));
		int numberOfRows;
		try {
			numberOfRows = rows.size();
		} catch (Exception e) {
			numberOfRows = 0;
		}
		if (numberOfRows == 0) {
			//Let's try again after one minute
			Thread.sleep(60000);
			System.out.println("Trying again");
			findNumberOfTpRecords(referenceDate, attempts - 1);
		}
		
		return numberOfRows;
	}
	
	public void checkTipoSezioneAndScadenza(String referenceDate, int numberOfRows) throws Exception {
		if (numberOfRows != tipoSezioneStrings.length) {
			throw new Exception("The number of TP records is different from what expected");
		}
		for (int i = 0; i < tipoSezioneStrings.length; i++) {
			By tipoSezioneLocator = By.xpath(tipoSezioneXpath.replace("$ROW_NUMBER$", Integer.toString(i+1)));
			verifyComponentText(tipoSezioneLocator, tipoSezioneStrings[i]);
			if (!tipoSezioneStringsPart2[i].equals("")) {
				verifyComponentText(tipoSezioneLocator, tipoSezioneStringsPart2[i]);
			}
			By dataScadenzaLocator = By.xpath(dataScadenzaXpath.replace("$ROW_NUMBER$", Integer.toString(i+1)));
			String dataScadenza = getElementTextString(dataScadenzaLocator);
			if (dataScadenza != null && !dataScadenza.equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date creazione = sdf.parse(referenceDate);
				Date scadenza = sdf.parse(dataScadenza);
				if (!scadenza.after(creazione)) {
					throw new Exception("Data scadenza not after data creazione: " + dataScadenza);
				}
			}
		}
		
	}
	
	//----- Constants -----
	
	private String[] tipoSezioneStrings = {
			"TP7 - Pagamento",
			"TP7 - Pagamento",
			"TP1 - Completamento dati/informazioni",
			"TP1 - Completamento dati/informazioni"
	};
	private String[] tipoSezioneStringsPart2 = {
			"- Click e Pay",
			"",
			"- Modifica Potenza Tensione",
			""
	};
	
}
