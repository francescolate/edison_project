package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class UsefulLinksEngComponent extends BaseComponent {
	public By sectionTitle = By.cssSelector("#main > div.wrapper-sec > div > section > div > h3");
	public By fuelMixLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(1) > a");
	public By emergencyPlanLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(2) > a");
	public By safeguardLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(3) > a");
	public By defaultServiceLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(4) > a");
	public By adrLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(1) > div > ul > li:nth-child(5) > a");
	public By usefulInfoLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(1) > a");
	public By dataProtectionLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(2) > a");
	public By contactUsLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(3) > a");
	public By partnerLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(4) > a");
	public By sponsorshipLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(2) > div > ul > li:nth-child(5) > a");
	public By formsLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(1) > a");
	public By complaintFormsLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(2) > a");
	public By remitLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(3) > a");
	public By evolutionLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(4) > a");
	public By negotiationLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(5) > a");
	public By tutelaLink = By.cssSelector("#main > div.wrapper-sec > div > section > div > div > div > div > div > div > div:nth-child(3) > div > ul > li:nth-child(6) > a");
	
	public By[] sectionElements = {
			sectionTitle,
			fuelMixLink,
			emergencyPlanLink,
			safeguardLink,
			defaultServiceLink,
			adrLink,
			usefulInfoLink,
			dataProtectionLink,
			contactUsLink,
			partnerLink,
			sponsorshipLink,
			formsLink,
			complaintFormsLink,
			remitLink,
			evolutionLink,
			negotiationLink,
			tutelaLink
	};
	
	public UsefulLinksEngComponent(WebDriver driver) {
		super(driver);
	}
	
	public void verifySectionStrings() throws Exception {
		verifyElementsArrayText(sectionElements, sectionStrings);
	}
	
	// -----------------  Constants -----------------------
	
	public final String[] sectionStrings = {
			"Useful links",
			"Fuel mix",
			"Emergency plan",
			"Safeguard service",
			"Default Service for gas",
			"ADR rights / dispute resolution",
			"Useful information",
			"New European data protection rules",
			"Contact us",
			"Become our partner",
			"Sponsorship",
			"Forms",
			"Complaint form",
			"Remit",
			"Evolution of the markets",
			"Negotiation on an equal footing",
			"Tutela gas service cost and advantages"
	};
}
