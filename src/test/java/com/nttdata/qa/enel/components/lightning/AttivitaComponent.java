package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class AttivitaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public AttivitaComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public final By activityLink = By.xpath(
			"//table//*[contains(text(),'ACT-')]/ancestor::table[1]//*[text()='Processo']/ancestor::tr//a/span[contains(text(),'ACT-')]");
	public final By selectDescrizione = By.xpath("//label[normalize-space()='* Descrizione']/..//select");
	public final By selectTipo_attivita = By.xpath("//label[contains(text(),'Tipo attività')]//ancestor::div[@class='slds-form-element ']//div[@class='slds-form-element__control']//select");
	public final By selectCausale_contatto = By.xpath("//label[contains(text(),' Causale Contatto')]//ancestor::div[@class='slds-form-element ']//div[@class='slds-form-element__control']//select");
	public final By selectSpecifica = By.xpath("//label[contains(text(),' Specifica')]//ancestor::div[@class='slds-form-element ']//div[@class='slds-form-element__control']//select");
	public final By selectStato = By.xpath("//label[contains(text(),' Stat')]//ancestor::div[@class='slds-form-element ']//div[@class='slds-form-element__control']//select");
	public final By chiudiSezioneAttivita = By.xpath("//span[@id='cancelButton']");
	public final By modificaAttivita = By.xpath(
			"//div[contains(@aria-label,'Attività') and contains(@class,'ActivityLayout')]//button[text()='Modifica']");
	public final By salvaAttivita = By.xpath(
			"//div[contains(@aria-label,'Attività') and contains(@class,'ActivityLayout')]//button[text()='Salva']");
	public final By podInput = By.xpath("//label[normalize-space()='* POD']/..//input");

	public void press(By oggetto) throws Exception {
		util.objectManager(oggetto, util.scrollToVisibility, true);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void pressJavascript(By oggetto) throws Exception {
		WebElement element = util.waitAndGetElement(oggetto);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	/*
	 * Il modulo si occupa di verificare nella schermata di riepilogo della
	 * richiesta su SFDC Che sia stata creata una Attività e che le colonne della
	 * tabella Tipo Attività Causale Contatto Descrizione Specification siano
	 * conformi al tipo di richiesta avanzata
	 * 
	 * es. Tipo Attività = Processo, Causale Contatto = Gestione Cliente,
	 * Descrizione = Modifica, Specification = Anagrafica.
	 * 
	 * 
	 */

	public void checkActivity(String tipo, String causale, String descrizione, String specifica) throws Exception {
        //Scroll necessari affinchè avvenga il caricamento 
		//della sezione Attività, su schermi piccoli resta nascosta
		//se non ci si posiziona più volte sopra scrollando la pagina
		util.scrollDownDocument();
        Thread.currentThread().sleep(2000);
        util.scrollUpDocument();
        Thread.currentThread().sleep(2000);
        util.scrollDown();
        Thread.currentThread().sleep(2000);
        util.getFrameActive();
		if (!util.verifyExistence(activityLink, 20))
			throw new Exception("Non e' stata generata alcuna attività di tipo: " + tipo + " con causale; " + causale
					+ " e descrizione: " + descrizione);
		else {
			pressJavascript(activityLink);
			spinner.checkSpinners();
		//	String type = driver.findElement(selectTipo_attivita).getText();
			String type = new Select(util.waitAndGetElement(selectTipo_attivita, true)).getFirstSelectedOption()
					.getText();
		//	String reason = driver.findElement(selectCausale_contatto).getText();
			String reason = new Select(util.waitAndGetElement(selectCausale_contatto, true)).getFirstSelectedOption()
					.getText();
	//		String desc = driver.findElement(selectDescrizione).getText();
			String desc = new Select(util.waitAndGetElement(selectDescrizione, true)).getFirstSelectedOption()
					.getText();
		//	String spec =driver.findElement(selectSpecifica).getText();
			String spec = new Select(util.waitAndGetElement(selectSpecifica, true)).getFirstSelectedOption().getText();
			if (!type.trim().toLowerCase().contentEquals(tipo.toLowerCase().trim()))
				throw new Exception("Per l'attività generata il Tipo Attivita' risulta essere diverso da " + tipo
						+ ". Valore riscontrato: " + type);
			if (!reason.trim().toLowerCase().contentEquals(causale.toLowerCase().trim()))
				throw new Exception("Per l'attività generata la Causale Contatto risulta essere diverso da " + causale
						+ ". Valore riscontrato: " + reason);
			if (!desc.trim().toLowerCase().contentEquals(descrizione.toLowerCase().trim()))
				throw new Exception("Per l'attività generata la Descrizione risulta essere diversa da " + descrizione
						+ ". Valore riscontrato: " + desc);
			if (!spec.trim().toLowerCase().contentEquals(specifica.toLowerCase().trim()))
				throw new Exception("Per l'attività generata ila Specification risulta essere diversa da " + specifica
						+ ". Valore riscontrato: " + spec);
			press(chiudiSezioneAttivita);
			spinner.checkSpinners();
		}
	}

	public void setActivity(String causale, String descrizione, String specifica, String stato, String pod) throws Exception {
		util.objectManager(selectCausale_contatto, util.scrollAndSelect, causale);
		Thread.currentThread().sleep(2000);
		util.objectManager(selectDescrizione, util.scrollAndSelect, descrizione);
		Thread.currentThread().sleep(2000);
		util.objectManager(selectSpecifica, util.scrollAndSelect, specifica);
		Thread.currentThread().sleep(2000);
		util.objectManager(selectStato, util.scrollAndSelect, stato);
		Thread.currentThread().sleep(2000);
		util.objectManager(this.podInput, util.clear);
		Thread.currentThread().sleep(2000);
		util.objectManager(this.podInput, util.scrollAndSendKeys, pod);
		Thread.currentThread().sleep(2000);
		press(salvaAttivita);
		spinner.checkSpinners();
		TimeUnit.SECONDS.sleep(5);

	}

}
