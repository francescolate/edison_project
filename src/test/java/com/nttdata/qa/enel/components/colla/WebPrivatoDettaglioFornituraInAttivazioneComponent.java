package com.nttdata.qa.enel.components.colla;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class WebPrivatoDettaglioFornituraInAttivazioneComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public List<WebElement> menuItemsList = null;
	public String visualizzaStato = "//*[text()='$customerCode$']/ancestor::div[@class='card forniture regolare']//a[text()='Visualizza Stato']";
	public String Visualizzalebollette = "//*[text()='$customerCode$']/ancestor::div[@class='card forniture regolare']//a[text()='Visualizza le bollette']";
	public String Visualizzalestato = "//*[text()='$customerCode$']/ancestor::div[@class='card forniture regolare']//a[text()='Visualizza le bollette']";
	public String Dettagliofornitura = "//*[text()='$customerCode$']/ancestor::div[@class='card forniture regolare']//a[text()='Dettaglio fornitura']";
	public By supplyIcon = By.xpath("//span[@class='icon-line-electricity']");
	public String supplyAddressPrefix = "L’indirizzo della tua fornitura $supplyType$ è: ";
	public String supplyAddressLuce = "L’indirizzo della tua fornitura luce è: ";
	public String customerNumber = "Numero Cliente: ";
	public By supplyDetailsHeading = By.xpath("//dl[@class='supply-details-heading']");
	public By tipoAttivazioneInfoButton = By.xpath("//a[@id='icon-info-circle-0']");
	public By subentroInnerText = By.xpath("//p[@class='inner-text']");
	public By tipoAttivazioneInfoCloseButton = By.xpath("//div[@class='remodal remodal-is-initialized remodal-is-opened']//button");
	public By mostraDiPiu = By.xpath("//a[@href='#']");
	public By supplyDetailContainer = By.xpath("//div[contains(@class, 'supply details-container')]");
	public By lavoriSulContatoreHeading = By.xpath("//div[@class='section-heading']");
	public By preventivoContainer = By.xpath("//li[contains(@class,'track-item active')]");
	public By statoDiAtticazioneContainer = By.xpath("//h2[text()='Stato di attivazione']/parent::div[@class='section-heading']");
	public By fornitura = By.xpath("//*[@id='fornitura01']");
	public By tracking = By.xpath("//ul[@class='track-list tracing']");
	public By no = By.xpath("//span[text()='NO']");
	public By close = By.xpath("//div[@id='modalAlert']//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By popUpHeading = By.xpath("//h3[@id='titleModal']");
	public By popUpInnerText = By.xpath("//div[@id='modalAlert']//p[@class='inner-text']");
	//public By noButton = By.xpath("//button[@id='overlayNoButton']");
	public By siButton = By.xpath("//button[@id='overlayYesButton']");
    
    public WebPrivatoDettaglioFornituraInAttivazioneComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void checkDocumentReadyState() throws Exception{
		util.checkPageLoaded();
		Thread.sleep(5000);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);	
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public void jcClickComponent(By xpath) throws Exception{
		util.jsClickElement(xpath);
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = normalizeInnerHTML(weText);		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	public void containsText(By by, List<String> text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		System.out.println("weText -->" + weText);
		weText = normalizeInnerHTML(weText);
		for (int i=0; i<text.size(); i++){
			if (!weText.contains(text.get(i)))
				throw new Exception("There isn't " + text);
		}
	}
	
	public final String subentroInfoText = "Sottoscrizione di un nuovo contratto e contestuale riattivazione del contatore precedentemente disattivato";
	public final String subentroInfoTextNew = "Sottoscrizione di un nuovo contratto di fornitura e contestuale cessazione automatica del contratto sottoscritto con il precedente Fornitore di energia elettrica o gas";
	public final String moreSupplyDetails = "Stato FornituraIn LavorazioneTipo Attivazione SUBENTROData Richiesta 17/03/2020POD IT012E06547415Offerta Attiva GiustaXTeNumero Offerta SZ3094275Modalità di sottoscrizione CartaceaContratto PDF non disponibile";
	public final String moreSupplyDetailsNew = "Stato FornituraIn LavorazioneTipo Attivazione Cambio FornitoreData Richiesta 21/05/2020POD IT002E6473657AOfferta Attiva Sempre Con TeNumero Offerta 331163720Modalità di sottoscrizione CartaceaContratto PDF non disponibile";
	public final String lavoriSulContatoreTextNew = "Stato di attivazioneVisualizza lo stato di attivazione della nuova offerta";
	public final String lavoriSulContatoreText = "Lavori sul contatoreVisualizza lo stato di avanzamento delle operazioni sul contatore";
	public final String preventivoText = "PreventivoRichiesta di preventivo inviata";
	public final String preventivoTextNew = "Richiesta di attivazione in lavorazioneLa richiesta di attivazione del contatore è stata ricevuta dalla società di distribuzione ed è in lavorazione.";
	public final String statoDiAttivazioneText = "Stato di attivazioneVisualizza lo stato di attivazione della nuova offerta";
	
	public List<String> supplyDetailContainerText = new ArrayList<String>();
	public final String statoFornitura = "Stato Fornitura";
	public final String tipoAttivazione = "Tipo Attivazione ";
	public final String dataRichiesta = "Data Richiesta ";
	public final String pod = "POD ";
	public final String offertaAttiva = "Offerta Attiva ";
	public final String numeroOfferta = "Numero Offerta ";
	public final String sottoscrizione = "Modalità di sottoscrizione ";
	public final String contratto = "Contratto ";
	public final String statoRichiestaAttivazione = "Richiesta di attivazione in lavorazioneLa richiesta di attivazione del contatore è stata ricevuta dalla società di distribuzione ed è in lavorazione.";
	public final String statoRichiestaAttivazione_2 = "Richiesta preventivoStiamo elaborando il preventivo per la realizzazione dei lavori.";


}
