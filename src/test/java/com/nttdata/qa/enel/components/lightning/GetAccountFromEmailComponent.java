package com.nttdata.qa.enel.components.lightning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class GetAccountFromEmailComponent {

	WebDriver driver;
	public SeleniumUtilities util;

	public By searchBar = By.xpath("//div[contains(@class, 'forceSearchInputDesktopPillWrapper')]/following-sibling::div//input");
	public By accountLink = By.xpath("//a[text()='Clienti']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']//tbody//th[1]//a");
	public String pod = "//a[text()='Punti di Fornitura']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']//a[text()='$POD$']";
	public String request = "//span[text()='LAVORI']/ancestor::td//preceding-sibling::td//span[text()='In Lavorazione']/ancestor::td//preceding-sibling::td//span[contains(text(),'$DATE$')]/ancestor::td//preceding-sibling::td//div[@class='outputLookupContainer forceOutputLookupWithPreview']//a[text()='Modifica Potenza e Tensione']";
	public String request147 = "//span[text()='LAVORI']/ancestor::td//preceding-sibling::td//span[text()='In Lavorazione']/ancestor::td//preceding-sibling::td//span[contains(text(),'$DATE$')]/ancestor::td//preceding-sibling::td//div[@class='outputLookupContainer forceOutputLookupWithPreview']//a[text()='Modifica Potenza']";
	
	public GetAccountFromEmailComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}	

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 45))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void searchAccount(By by, String filter) throws Exception{
		WebElement we = driver.findElement(by);
		we.sendKeys(filter);
	}
	
	public void searchAccount(By by, String filter, boolean newLine) throws Exception{
		Thread.sleep(5000);
		WebElement we = driver.findElement(by);
		if(newLine){
			we.sendKeys(filter+"\n");
		}
		else{
			we.sendKeys(filter);
		}
	}
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
}
