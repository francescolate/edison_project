package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ScriviciComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	//92
	public By supporto = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/child::ul/li/a[contains(text(),'Supporto')]");
	public By supportoPath = By.xpath("//a[contains(text(),'Home')]/ancestor::ul[@class='breadcrumbs component']");
	public By contractiModilistica = By.xpath("//a[contains(text(),'Contratti e modulistica')]");
	public By contatacci = By.xpath("//a[contains(text(),'Contattaci')]");
	public By contatacciHeader = By.xpath("//h1[contains(text(),'Tanti modi per entrare in contatto con noi')]");
	public By contatacciSubHeader = By.xpath("//p[contains(text(),'Enel Energia è sempre in contatto con te, se sei cliente e se vuoi diventarlo.')]");
	public By nostriClienti = By.xpath("//h3[contains(text(),'Per i nostri clienti')]");
	public By vuoiViaWebLink = By.xpath("//b[contains(text(),'Vuoi scriverci via web?')]");
	public By vuoiWebQui = By.xpath("(//span[@class='text--title-pink']/child::a[text()='qui'])[1]");
	
	public By quiTitle = By.xpath("//p[@id='paragrafo_1IT' and contains(text(),'Sei già cliente Enel Energia? Registrati subito o accedi')]");
	public By quiSubTitle = By.xpath("//p[@id='p_it']");
	public By quiSubTitle1 = By.xpath("//p[@id='paragrafo_2IT']");
	
	public By inviaButton = By.xpath("//button[@id='modulo_contatti_button']");
	
	public By cliccaQui = By.xpath("//a[@id='assConsumatori']");
	public By conttati = By.xpath("//h3[contains(text(),'Contatti')]");
	public By inserisci = By.xpath("(//h5[contains(text(),'INSERISCI I TUOI DATI')])[2]");
	public By inserisciDati = By.xpath("//h5[@id='dati_interessato']");
	public By informativaPrivacy = By.xpath("//label[@id='label_testo_privacyAss']");
	
	public By nomeAssociazioneLabel = By.xpath("//label[@id='label_inputAssC']");
	public By nomeAssociazioneInput = By.xpath("//span[@id='inputAssCSelectBoxItText']");
	public By cfAssociazioneLabel = By.xpath("//label[@id='label_inputCFAssCons']");
	public By cfAssociazioneInput = By.xpath("//input[@id='inputCFAssCons']");
	public By cfDelagatoLabel = By.xpath("//label[@id='label_inputCFDel']");
	public By cfdelagatoInput = By.xpath("//input[@id='inputCFDel']");
	
	public By nominativoLabel = By.xpath("//label[@id='label_inputNominativoDel']");
	public By nominativoInput = By.xpath("//input[@id='inputNominativoDel']");
	public By indrizzoMailLabel = By.xpath("//label[@id='label_inputEmailAss']");
	public By indrizzoMailInput = By.xpath("//input[@id='inputEmailAss']");
	public By indrizzoMailInputNew = By.xpath("//input[@id='inputIndirizzo']");
	public By numeroTelephonoLabel = By.xpath("//label[@id='label_inputCellulareAss']");
	public By numberoTelephonoInput = By.xpath("//input[@id='inputCellulareAss']");
	public By nomeLabel= By.xpath("//label[@id='label_nomeAss']");
	public By nomeInput = By.xpath("//input[@id='inputNom']");
	public By cognomeLabel = By.xpath("//label[@id='label_cognomeAss']");
	public By cognomeInput = By.xpath("//input[@id='inputCognom']");
	public By codiceFiscaleLabel = By.xpath("//label[@id='label_inputCfCliente']");
	public By codiceFiscaleInput = By.xpath("//input[@id='inputCfCliente']");
	public By clienteLabel = By.xpath("//label[@id='labelCliente']");
	public By dicosahaiLabel = By.xpath("//label[@id='bisognoAss']");
	
	public By argomentoLabel = By.xpath("//label[@id='label_inputTipoAss']");
	public By argomentoDropDown = By.xpath("//span[@id='inputTipoAssSelectBoxItText']");
	public By argomentoDropDownNew = By.xpath("//span[@id='inputTipoSelectBoxIt']");
	public By messagioLabel = By.xpath("//label[@id='label_messaggio']");
	public By messagioTextbox = By.xpath("//textarea[@id='inputNoteAss']");
	
	public By infomativaTextArea = By.xpath("//textarea[@id='testo_privacyAss']");
	
	public By accettoLabel= By.xpath("//label[@id='inputprivacy_si_labelAss']");
	public By nonAccettoLabel = By.xpath("//label[@id='inputprivacy_no_labelAss']");
	
	public By proseguiButton = By.xpath("//input[@id='accediuplAss']");
	
	public By nomeDropDown = By.xpath("//div[@id='pickListAssCons']/child::select[@id='inputAssC']");
	
	public By dropdownClick = By.xpath("//span[@id='inputAssCSelectBoxItContainer']/child::span[@id='inputAssCSelectBoxIt']");
	public By altroConsumoValue = By.xpath("//span[@id='inputAssCSelectBoxItText' and contains(text(),'ALTROCONSUMO')]");
	
	public By siButton = By.xpath("//button[@id='delegaSI']");
	public By noButton = By.xpath("//button[@id='delegaNO']");
	public By okButton = By.xpath("//button[text()='OK']");
	public By okButtonNew = By.xpath("//div[@id='textRequiredDelega']/div/div/div[3]/div/button");
	
	public By cliccaquiLink = By.xpath("//u[text()='clicca qui']");
	
	public By vuoiSIRadio = By.xpath("//label[@id='inputAllegatiAss_siLabel']");
	public By accettoRadio = By.xpath("//label[@id='inputprivacy_si_labelAss']");
	
	public By uploadDocHeader = By.xpath("//h3[@class='plan-main-head  active-bills-header' and contains(text(),'Upload Documenti')]");
	public By uploadDocTitle = By.xpath("//p[@id='testo_2']");
	
	public By cloudStorageHeading = By.xpath("//h4[contains(text(),'Cloud Storage')]");
	public By cloudStorageTitle = By.xpath("//p[contains(text(),'aggiungi files')]");
	
	public By computerHeading = By.xpath("//h4[contains(text(),'Computer')]");
	public By computerTitle = By.xpath("//p[contains(text(),'Seleziona')]");
	public By buttonSELEZIONA = By.xpath("//button[@id='getFileButton']");
	
	public By fileinHeader = By.xpath("//h5[@id='titolo_h5']");
	public By fileinContent = By.xpath("//p[@id='messaggio_caution']");
	public By buttonAnnulla = By.xpath("//button[@id='annullaUploadButton']");
	public By buttonConferma = By.xpath("//button[@id='confermaUploadButton']");
	public By guidaInstanzeLink = By.xpath("//a[@id='link_compilazione']");
	
	public By tornaAllaHomeButton = By.xpath("//button[@class='btn-cta--pink ' and contains(text(),'Torna alla Home')]");
	public By inviaDiNuoVoHomeButton = By.xpath("//button[@id='newUploadButton' and contains(text(),'Invia di nuovo')]");
	
	//94
	
	public By nomeFieldLabel = By.xpath("//label[@id='inputNomeLabel']");
	public By nomeFieldInput = By.xpath("//input[@id='inputNome']");
	public By cognomeFieldLabel = By.xpath("//label[@id='inputCognomeLabel']");
	public By cognomeFieldInput = By.xpath("//input[@id='inputCognome']");
	public By cfFieldLabel = By.xpath("//label[@id='inputeUsernameLabel']");
	public By cfFieldInput = By.xpath("//input[@id='inputUsername']");
	public By cellulareFieldLabel = By.xpath("//label[@id='inputCellulareLabel']");
	public By cellulareFieldInput = By.xpath("//input[@id='inputCellulare']");
	public By indrizzoFieldLabel = By.xpath("//label[@id='inputEmailLabel']");
	public By indrizzoFieldInput = By.xpath("//input[@id='inputEmail']");
	public By confermaIndrizzoLabel = By.xpath("//label[@id='inputConfermaEmailLabel']");
	public By confermaIndrizzoInput = By.xpath("//input[@id='inputConfermaEmail']");
	public By numeroClienteInput = By.xpath("//input[@id='inputFornitura']");
	public By numeroClienteLabel = By.xpath("//label[@id='inputFornituraLabel']");
	public By messagioFieldLabel = By.xpath("//label[@id='inputNoteLabel']");
	public By messagioFieldInput = By.xpath("//textarea[@id='inputNote']");
	public By informativaFieldLabel = By.xpath("//label[@id='testo_privacy_label']");
	public By informativaFieldInput = By.xpath("//textarea[@id='testo_privacy']");
	public By buttonProsegui = By.xpath("//button[@id='accediupl']");
	public By guidaAllaLink = By.xpath("//a[contains(text(),'Guida alla compilazione')]");
	
	public By selectBox = By.xpath("//span[@id='inputAssCSelectBoxItText']");
    public By pickupListIcons = By.xpath("//div[@id='pickListAssCons']//li[@id='id_4']");
	
	
	
	public static final String NOMEFIELD_LABEL = "Nome*";
	public static final String COGNOMEFIELD_LABEL = "Cognome*";
	public static final String CFFIELD_LABEL = "Codice Fiscale / Partita IVA*";
	public static final String CELLULARE_LABEL = "Cellulare*";
	public static final String INDRIZZO_LABEL = "Indirizzo e-mail*";
	public static final String CONFERMAINDRIZZO_LABEL = "Conferma indirizzo e-mail*";
	public static final String NUMERO_LABEL = "Numero cliente";
	public static final String MESSAGIOFIEDL_LABEL = "Messaggio*";
	public static final String INFORMATIVAFIELD_LABEL = "Informativa Privacy*";
	//public static final String INFORMATIVA_CONTENT = "INFORMATIVA AI SENSI DELL' ART. 13 del Regolamento UE 2016/679 ("GDPR")";
	
	
	
	public static final String FILE_HEADER = "File in caricamento";
	public static final String FILE_CONTENT = "Puoi caricare i documenti solo in formato pdf o jpg.ATTENZIONE: La dimensione del file non deve superare 10 MB.";
	public static final String UPLOADDOC_HEADER = "Upload Documenti";
	public static final String UPLOADDOC_TITLE = "In questa pagina puoi caricare i tuoi documenti.";
	public static final String CLOUDSTORAGE_HEADING = "Cloud Storage";
	public static final String CLOUDSTORAGE_TITLE = "aggiungi files direttamente da Google drive... e altri storage";
	public static final String COMPUTER_HEADING = "Computer";
	public static final String COMPUTER_TITLE = "Seleziona o trascina qui i files dal tuo computerTramite le cartelledel tuo pc";
	public static final String NOMINATIVO_LABEL = "Nominativo Delegato";
	public static final String INDRIZZOMAIL_LABEL = "Indirizzo Mail Associazione*";
	public static final String NUMEROTELEPHONE_LABEL = "Numero di Telefono Associazione*";
	public static final String NOME_LABEL = "Nome*";
	public static final String COGNOME_LABEL = "Cognome*";
	public static final String CODICEFISCALE_LABEL = "Codice Fiscale / Partita IVA*";
	public static final String CLIENTE_LABEL = "È già Cliente?";
	public static final String DICOSAHAI_LABEL = "Di cosa hai bisogno?";
	public static final String ARGOMENTO_LABEL = "Argomento*";
	public static final String MESSAGIO_LABEL = "Messaggio*";
	public static final String ACCETTO_LABEL = "Accetto";
	public static final String NONACCETTO_LABEL = "Non accetto";
	public static final String PDFLINK = "https://www-coll1.enel.it/content/dam/enel-it/servizi-online/pdf/Guida_alla_CompilazioneIstanze19_01_2016_v3.pdf";
	public static final String SUPPORTO_PATH = "Home/supporto/Come entrare in contatto con Enel Energia";
	public static final String CONTACCI_HEADER = "Tanti modi per entrare in contatto con noi";
	public static final String CONTACCI_SUBHEADER = "Enel Energia è sempre in contatto con te, se sei cliente e se vuoi diventarlo.";
	public static final String NOSTRI_CLIENTI = "Per i nostri clienti";
	public static final String QUI_TITLE = "Sei già cliente Enel Energia? Registrati subito o accedi alla nostra Area Clienti, potrai gestire le tue forniture in autonomia. Bastano pochi click! Se non sei cliente Enel Energia, scegli l'offerta migliore per te nella sezione LUCE E GAS. Per ogni altra esigenza, puoi mandarci un messaggio.";
	public static final String QUI_SUBTITLE = "Puoi scriverci in diverse lingue, clicca qui per visualizzare l’elenco";
	public static final String QUI_SUBTITLE1 = "Se rappresenti un'Associazione dei Consumatori, clicca qui.";
	public static final String CONTATTI = "Contatti";
	public static final String INSERISCI = "INSERISCI I TUOI DATI";
	public static final String INSERISCI_DATI = "INSERISCI I DATI DELL'INTERESSATO";
	public static final String INFORMATIVA_PRIVACY = "Informativa Privacy*";
	public static final String NOMEASSOCIAZIONE_LABEL = "Nome Associazione*";
	public static final String CFASSOCIAZIONE_LABEL = "Codice Fiscale Associazione*";
	public static final String CFDELAGATO_LABEL = "Codice Fiscale Delegato*";
	
	
	
	public ScriviciComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
	
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
    
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
    public void selectOptions(String option) throws Exception {
		
		String[] optionValue = {"Scegli il motivo","CAMBIO CASA","INUTILIZZO", "USO STAGIONALE", "ALTRO", "PREFERISCO NON RISPONDERE"};
		
		WebElement dropdown = driver.findElement(By.xpath("//select[@id='inputTipoAss']")) ;  
		//WebElement dropdown = driver.findElement(By.xpath("//div[@id='pickListAssCons']/child::select[@id='inputAssC']"));
		   String value = "No";
				 
		   Select select = new Select(dropdown); 
		   List<WebElement> options = select.getOptions(); 
		                   
		    for (int i=0; i<options.size(); i++){
		    Thread.sleep(4000);
		    String opt = options.get(i).getText();
		    System.out.println("option value " +opt);
		    
		      if (opt.equals(optionValue[i])){
		      value = "Yes";		    			 
		      System.out.println("dropdown values matching");
		        		}
		                        
		      else {
		      throw new Exception("dropdown values are not matching");
                   }
		     		     
		    } 
		    select.selectByVisibleText(option);
		  		    
		}
    
    public By ddvaluecontainer = By.xpath("//span[@id='inputTipoAssSelectBoxItContainer']");
    public By ddvalue = By.xpath("//select[@id='inputTipoAss']/child::option");
    
	public void selectDropDownValue(By checkObject, String property) throws Exception
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		Select select = new Select(driver.findElement(checkObject));
		select.selectByValue(property);
				
	}
	
	public void selectValue() throws Exception
	{
		verifyComponentExistence(dropdownClick);
		clickComponent(dropdownClick);
		Thread.sleep(6000);
		verifyComponentExistence(altroConsumoValue);
		//clickComponent(altroConsumoValue);
		jsClickElement(altroConsumoValue);
		
	}
	public void jsClickElement(By xpath) throws Exception {
        String itemXpath = xpath.toString().replace("By.xpath: ", "");
        //waitAndGetElement(xpath);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("(document.evaluate(\"" + itemXpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
    }
	public void verifyPDFUrl(String url) throws Exception
	{
		String currentWindow  = driver.getWindowHandle();
		for(String newWindow : driver.getWindowHandles())
		{
		    driver.switchTo().window(newWindow);
		}
		checkUrl(url);
		System.out.println(url);
		driver.close();
		driver.switchTo().window(currentWindow);
		}
	public void checkUrl(String url) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url of the page is different from the expected one: the url of the page' "+link+";the expected one' "+url);
		}
	
  
}
