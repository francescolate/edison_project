package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.persistence.internal.expressions.TreatAsExpression;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class GestioneFornituraFormComponent extends BaseComponent{

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	final By avantiTipoMercato = new By.ById("j_id0:formId:ReferenceMarketNextId");
	final By selectMarket = new By.ById("j_id0:formId:referenceMarketInputId");
	public By selectMarket2 = new By.ById("j_id0:principalForm:marketSelectionInput");
	public By selectMarket3 =new By.ByXPath("//span[text()='Mercato Riferimento di Prodotto']/ancestor::div[1]//select");
	public By selectMarket4 =new By.ByXPath("//label[text()='Mercato di riferimento prodotto']/..//input");
	final By podInput = new By.ById("j_id0:formId:podPdrText");
	final By eseguiPrecheck = new By.ById("checkSupplyBtn");
	final By istatPopUp = new By.ByXPath("//td[contains(@id,'formId:territoryTable')]//label");
	final By confermaFornituraButton = new By.ByXPath("//input[contains(@id,'ConfermaFornituraButton')]");
	final By confermaFornituraButtonVCA = By.xpath("//button[text()='Conferma Fornitura']");
	final By sezioneDettaglioPrecheck=new By.ById("precheckDetailPanelBnt");
	public final By confermaButton = new By.ByXPath("//button[text()='Conferma']");
	public final By confermaButtonPrimaAttivazione = new By.ByXPath("//span[text()='Inserimento forniture']/ancestor::div[@role='region']//button[text()='Conferma']");
	public final By confermaButtonDisabled=By.xpath("//button[text()='Conferma' and @disabled]");
	public By buttonAnnulla=By.xpath("//button[text()='Annulla']");
    public final By creaOffertaMainButton = By.xpath("//button[text()='Crea offerta']");
    public final By creaOffertaMainButtonDisabled = By.xpath("//button[text()='Crea offerta' and @disabled]");
	final By istatPopUpContinua = new By.ById("continueBtnModalIstat");
//	final By street = new By.ById("precheckDetailAddressComponent:streetNameInput");
//	final By province = new By.ById("precheckDetailAddressComponent:provinceInput");
//	final By civico = new By.ById("precheckDetailAddressComponent:streetNumberInput");
//	final By city = new By.ById("precheckDetailAddressComponent:cityInput");
//	final By addressCheckBtn = new By.ById("precheckDetailAddressComponent:verifyAddrBtn");
	final By cap = new By.ById("j_id0:formId:CapText");
	public final By confermaButtonContattiEConsensi = By.xpath("//h2[text()='Contatti e consensi']/ancestor::div[@role='region']//button[text()='Conferma']");
	
	
	final By tipoMisuratore = new By.ById("j_id0:formId:meterType-input");

	final By tensioneConsegna = new By.ById("j_id0:formId:voltage-input");

	final By potenzaContrattuale = new By.ById("j_id0:formId:contractualPower-input");

	public final By creaOffertaButton = new By.ById("j_id0:formId:createOfferFooterBtn");
	
	public final By creaOffertaButton2 = new By.ByXPath("//button[text()='CREA OFFERTA']");

	public final By avantiCodiceAmico = new By.ById("checkOfferModeButton");
	
	public final By tipoMisuratore2 = By.xpath("//label[contains(text(),'Tipo misuratore')]/following::select");
	public final By tensioneConsegna2 = By.xpath("//label[contains(text(),'Tensione di consegna')]/following::select");
	public final By potenzaContrattuale2 = 	 By.xpath("//label[contains(text(),'Potenza contrattuale')]/following::input");
	
	
	public final By buttonAggiungiFornitura=new By.ByXPath("//input[@value='Aggiungi fornitura']");
	public final By selectUnitaAbitativa=new By.ById("j_id0:formId:housingUnitInputId");
	public final By selectUnitaAbitativa2=new By.ByXPath("//div[contains(@id,'HousingUnitContainer')]//select");
	
	public final By buttonAvantiUnitaAbitativa=new By.ByXPath("//div[@id='j_id0:formId:housingUnitSectionId']/..//section[@class='slds-clearfix slds-p-around--small']/div/input[@value='Avanti']");
	public final By buttonAvantiUnitaAbitativa2=new By.ById("nextHousingUnit_Button");

	public final By nuovaFornituraRadioButton = new By.ByXPath("//span[text()='Nuovo']/ancestor::tr//input[@aria-label='selezione forniture']/parent::label");
	
	public final String radioButtonFornituraInserita = "//td[text()='#']/ancestor::tr//input[@aria-label='selezione forniture']/parent::label";
	
	final By avantiReferente= new By.ByXPath("//div[@id='j_id0:formId:accountRefTable_wrapper']/..//input[@value='Avanti']");
	
	final By avantiDelegato = new By.ById("j_id0:formId:delegateNextBtn");

	final By usoAbitativo = new By.ById("j_id0:formId:sendingChannelInputId");

	final By usoAbitativoNextBtn = new By.ById("supplyUsageNextBtn");

	final By swaPod = new By.ByXPath("//input[contains(@id,'SWAPOD')]");
	
	final By puntoDiFornituraLabel = new By.ByXPath("//label[contains(text(),'Punto di Fornitura')]");
	
	public By selectUsoFornitura=new By.ByXPath("//select[@data-fieldname='Uso fornitura']");
	
	public By selectUsoFornitura2=new By.ByXPath("//select[@name='UsoFornitura']");
	public By campoUsoFornitura=By.xpath("//label[contains(text(),'Uso Fornitura')]/..//input");
	public By selectUsoEnergia=new By.ByXPath("//select[@name='UsoEnergia']");
	
	public By buttonAvantiUsoFornitura=new By.ByXPath("//div[text()='Uso Fornitura']/parent::div//button[@alt='Avanti' and contains(@id,'next')]");
	public By buttonAvantiUsoFornitura2 = By.xpath("//button[@id='buttonNext']");
	
	public By confirmPopUpWarning= By.xpath("//h2[text()='Attenzione!']/ancestor::div[contains(@class,'modal__container')]//button[text()='Conferma']");

	public By buttonPrecheck= By.xpath("//button[@alt='Precheck']");
	
	public By buttonConferma= By.xpath("//button[contains(@id,'confirmButton') and @alt='Conferma']");

	public By genericConfermaButton = By.xpath("//button[text()='Conferma']");
	public By confermaButtonSelezioneMercato = By.xpath("//h2[text()='Selezione Mercato']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By confermaMercatoDisabilitato = By.xpath("//h2[text()='Selezione Mercato']/ancestor::div[@role='region']//button[text()='Conferma' and @disabled]");
	public By confermaInserimentoFornitureDisabilitato= By.xpath("//h2//span[text()='Inserimento forniture']/ancestor::div[@role='region']//button[text()='Conferma' and @disabled ]");
	public By confermaButtonInsCommodity = By.xpath("//h2[text()='Commodity']//ancestor::div[@role='region']//button[text()='Conferma']");
	
	public By table_Commodity = new By.ByXPath("//h2[text()='Commodity']//ancestor::div[@class='slds-card']//table[@aria-label='Tabella delle intestazioni cliccabili e dei valori ordinabili']/tbody/tr");
	
	public By selezionaUsoFornituraConferma = By.xpath("//h2[text()='Selezione Uso Fornitura']/ancestor::div[@class='slds-card']//button[text()='Conferma']");
	public By selezionaUnitaAbitativaConferma = By.xpath("//h2[text()='Unità abitativa']/ancestor::div[@class='slds-card']//button[text()='Conferma']");
	public By selezionaUnitaAbitativaModifica = By.xpath("//h2[text()='Unità abitativa']/ancestor::div[@class='slds-card']//button[text()='Modifica']");
	public By messaggioUnitaAbitativaVCA = By.xpath("//h2[text()='Unità abitativa']/ancestor::div[@class='slds-card']//span[text()='Chiedi al cliente se i due punti di fornitura afferiscono alla stessa unità abitativa.']");
	public By clienteProduttoreFlag = By.xpath("//span[text()='Cliente Produttore']");
	
	public By confermaFatturazioneElettronicaSWAEVO = By.xpath("//div[@aria-label='Fatturazione Elettronica']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By buttonCalcolaAscensore = By.xpath("//button[text()='Calcola Ascensore']");
	public By usoFornituraValue = By.xpath("//table[@role='table']//div[text()='Uso Abitativo']");
	//BozzaRiprendiDettaglioSubentro Module
	public By button_conferma = By.xpath("//button[@name='Conferma' and text()='Conferma']");
	public By button_crea_offerta = By.xpath("//button[@name='CreateQuote' and text()='Crea offerta']");	
	public By button_popup_ko = By.xpath("//slot[@name='footer' and contains(@id, 'modalButtons-')]//button[text()='Chiudi']");
	public By container_referente = By.xpath("//span[text()='Referente']");
	public By radio_first_in_referente_table = By.xpath("(//table[@id='Referente_Table']//input[contains(@id, 'radio-')])[1]/../.");
	public By button_conferma_in_referente_container = By.xpath("//h2//*[text()='Referente']/ancestor::div[@role='main']//button[text()='Conferma']");	
	public By buttonConfermaReferente = By.xpath("//h2//*[text()='Referente']/../../../../..//button[text()='Conferma']");	
	public By buttonConfermaSelezioneUsoFornitura = By.xpath("//label[contains(text(),'Uso Fornitura')]/ancestor::div[@role='region']//button[text()='Conferma']");
	public By sezioneSelezioneUsoFornitura = By.xpath("//h2[text()='Selezione Uso Fornitura']");
	public By pickuplist_in_container_suf = By.xpath("//h2[text()='Selezione Uso Fornitura']/ancestor::div[@role='main']//input[contains(@id,'input-picklist-')]");
//	public By pickuplist_by_text = By.xpath("//*[contains(@id, 'input-picklist-')]//*[text()='Uso Abitativo']");
	public By button_conferma_in_suf_container = By.xpath("//h2[text()='Selezione Uso Fornitura']/ancestor::div[@role='main']//button[text()='Conferma']");
	public By button_salva_in_bozza = By.xpath("//button[text()='Salva in Bozza']/ancestor::article/div//button[text()='Salva in Bozza']");
	public By button_ssdc_by_text = By.xpath("//button[normalize-space(text())='Modifica']");
	public By frame_ssdc = By.xpath("//iframe[@title ='accessibility title']/ancestor::div[@class ='content iframe-parent']/iframe");
	public By popup_vbl_ko = By.xpath("//h2[contains(@id , 'modalTitle-') and text()='VBL KO']");
	public By popup_cvp_light_ko = By.xpath("//h2[contains(@id , 'modalTitle-') and text()='KO CVP LIGHT']");
	public By popup_prodotto_non_valido = By.xpath("//h2[contains(@id , 'modalTitle-') and text()='Prodotto non valido']");
	
	//AnnullamentoSubentro Module
	public By button_annulla = By.xpath("//button[@name='Cancel' and text()='Annulla']");
	public By button_close_annullament = By.xpath("//button[@name='CancelCase' and text()='Chiudi']");
	public By button_close_annullament_quote = By.xpath("//button[@name='CancelAnnullmentQuote' and text()='Chiudi']");
	public By button_confirm_annullament = By.xpath("//button[@name='ConfirmAnnullmentCase' and text()='Conferma']");
	public By button_confirm_annullament_quote = By.xpath("//button[@name='ConfirmAnnullmentQuote' and text()='Conferma']");
	public String check_stato_by_text = "//div/p[@title='Stato']/ancestor::div//slot//*[text()='##']";
	public String check_statodc_by_text = "//span[@title='##']";
	public String check_sstatodc_by_text = "//span[@title='##']";
	
	//AggiungiFornitura Module
	
	public By button_aggiungi_fornitura = By.xpath("//button[@name='Aggiungi fornitura']");
	public String pickuplist_by_text = "//*[contains(@id, 'input-picklist-')]//*[text()='##']";
	public By container_commodity = By.xpath("//div[@aria-label='Commodity']");
	
	//Allaccio
	public By fornitura_ele_allaccio = By.xpath("//div[text()='ELETTRICO']/ancestor::tr//label[@class='slds-radio__label']/span[1]");
	public By fornitura_gas_allaccio = By.xpath("//div[text()='GAS']/ancestor::tr//label[@class='slds-radio__label']/span[1]");
	
	public By fornitura_gas_voltura = By.xpath("//div[text()='GAS']/ancestor::tr//label[@class='slds-radio__label']/span[1]");

	public By consumo_annuo_allaccio = By.xpath("//label[contains(text(),'Consumo Annuo')]//ancestor::div[@class='slds-form-element']//input");
	public By confermaFornitura = By.xpath("//button[contains(text(),'Conferma Fornitura')]");
	public By confermaFornitura2 = By.xpath("//button[contains(text(),'Conferma fornitura')]");
	public By telefono_distributore = By.xpath("//label[contains(text(),'Telefono per Distributore')]/following::input");
	
	public By potenza_richiesta = By.xpath("//label[contains(text(),'Potenza Richiesta')]/following::input");
	public By corrente_regime = By.xpath("//label[contains(text(),'Corrente di regime')]/following::input");
	public By modalitàFirmaConfermaButton = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By checkboxForfait = By.xpath("//span[text()='Forfait']/ancestor::label/span");
	
	//KO Forfait
	public By popUpForfaitKO = By.xpath("//h2[text()='Attenzione - Errore distributore']");
	public By popUpForfaitKOtesto = By.xpath("//h2[text()='Attenzione - Errore distributore']/ancestor::div[1]//p");
	public By buttonOKpopUpForfaitKO = By.xpath("//h2[text()='Attenzione - Errore distributore']/ancestor::div[1]//button[text()='OK']");
	public String testopopUpForfaitKOAtteso="Attenzione. Sul comune il distributore presente non è Edistribuzione. Annullare la richiesta e inserire la tripletta per la gestione extra sistema";
	
	
	// ascensore
	public By popUpAscensoreTitle = By.xpath("//h2[text()='Attenzione']");
	public By popUpAscensoretesto = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//p");
	public By buttonOKpopUpAscensore = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='OK']");
	public String testopopUpAscensoreAtteso="Per forniture straordinarie che alimentano giostre ricorda di selezionare Ascensore = Si";
	
	// Popup errore potenza richiesta
	public By popUpErrorePotenzaTitle = By.xpath("//h2[text()='Errore']");
	public By popUpErrorePotenzaTesto = By.xpath("//h2[text()='Errore']/ancestor::div[1]//p");
	public String popUpErrorePotenzaTestoAtteso ="Per potenze superiori a 10 KW non è possibile richiedere una tensione di 220 V";
	public By buttonPopUpChiudiErrorePotenza = By.xpath("//h2[text()='Errore']/ancestor::div[1]//button[text()='Chiudi']");	
	
	// Popup richiesta di anticipo
		public By popUpAnticipoBSNTitle = By.xpath("//h2[text()='Attenzione']");
		public By popUpAnticipoBSNTesto = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//p");
		public String popUpAnticipoBSNTestoAtteso ="Il cliente dovrà pagare un anticipo di 100 €";
		public By popUpAnticipoBSNButtonOK = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='OK']");	
		
		
	// pop-up residente SI
	public By popUpResidente = By.xpath("//h2[text()='Attenzione']");
	public By popUpResidenteTesto = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//p");
	public By buttonOKpopUpResidente = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='Chiudi']");
	public String testopopUpResidenteAtteso="Il cliente risulta residente presso un altro indirizzo di fornitura. Se prosegui verrà effettuata una \"Modifica Stato Residente\"";
	
	public By usoFornituraValueUsoDivDaAbitaz =  By.xpath("//table[@role='table']//div[text()='Uso Diverso da Abitazione']");
	public By usoFornituraValueIlluminazPubblica =  By.xpath("//table[@role='table']//div[text()='Illuminazione Pubblica']");
	
	//VCA Campi Tipologia Disalimentabilità e Telefono Disalimentabilità
	public By tipologia_disalimentabilita = By.xpath("//label[contains(text(),'Tipologia Disalimentabilit')]/ancestor::div[1]//input");
	public By telefono_disalimentabilita = By.xpath("//label[contains(text(),'Telefono Disalimentabilit')]/ancestor::div[1]//input");
	public By popUpErrore_telefono_disalimentabilita = By.xpath("//p[contains(text(),'I seguenti campi sono obbligatori o non correttamente compilati:Telefono Disalimentabilit')]");
	public By buttonChiudi_popUpErrore_telefono_disalimentabilita = By.xpath("//button[contains(text(),'Chiudi')]");
	
	//pop-up Si Illiminazione pubblica
	public By popUpAscensoreSiPA = By.xpath("//h2[text()='Attenzione']");
	public By popUpAscensoreSiPATesto = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//p");
	public By buttonOKpopUpAscensoreSiPA = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='OK']");
	public String testopopUpAscensoreSiPA="Ricorda che per l’inserimento dell’ascensore è necessario il calcolo della potenza richiesta.";
	
	//pop-up Si potenza suggerita calcola ascensore
	public By popUpPotenzaSuggerita = By.xpath("//h2[text()='Attenzione']");
	public By popUpPotenzaSuggeritaTesto = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//p");
	public By buttonSIPotenzaSuggerita = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='SI']");
	
	//pop-up Modifica Stato Residenza
	public By popUpModificaStatoResidenza = By.xpath("//h2[text()='Attenzione']");
	public By popUpModificaStatoResidenzaTesto = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//p");
	public By buttonChiudipopUpModificaStatoResidenza = By.xpath("//h2[text()='Attenzione']/ancestor::div[1]//button[text()='Chiudi']");
	public String testopopUpModificaStatoResidenza="Il cliente risulta residente presso un altro indirizzo di fornitura. Se prosegui verrà effettuata una \"Modifica Stato Residente\"";
	
	//pop-up Potenza Richiesta nulla
	public By popUpPotenzaNulla = By.xpath("//h2[text()='Errore']");
	public By popUpPotenzaNullaTesto = By.xpath("//h2[text()='Errore']/ancestor::div[1]//p");
	public By buttonChiudipopUpPotenzaNulla = By.xpath("//h2[text()='Errore']/ancestor::div[1]//button[text()='Chiudi']");
	public String testopopUpPotenzaNulla="Attenzione: la potenza richiesta non può essere nulla";
	
	// Seleziona Mercato
		public By testoMessaggioErrore = By.xpath("//p[text()='Non sei abilitato ad eseguire operazioni sul Mercato Salvaguardia. Invita il cliente a recarsi presso uno Spazio Enel.']");
		public String testoMessaggioErroreAtteso= "Non sei abilitato ad eseguire operazioni sul Mercato Salvaguardia. Invita il cliente a recarsi presso uno Spazio Enel.";
	
		public By popUpAttenzione = By.xpath("//h2[text()='Attenzione']");
		public By popUpAttenzioneButtonOK = By.xpath("//button[@name='ConfirmButtonModal']");
		public By popupLimiteForniture=By.xpath("//h2[contains(@id , 'modalTitle-') and text()='Attenzione - Limite forniture']/ancestor::div[1]//p[text()='Hai raggiunto il numero massimo di linee di offerta inseribili per offerta Dual Energy']");
		public By popUpErroreFlag = By.xpath("//p[contains(text(),'Il flag residente deve essere selezionato a SI')]");
		public By ordineFittizio = By.xpath("//label[contains(text(),'Ordine Fittizio')]/ancestor::div[2]//input");
		
		public GestioneFornituraFormComponent(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	public void verificaCategoriaDiConsumo(String [] lista, String consumo) throws Exception {
		//util.waitAndGetElement(By.xpath("//label[text()='Mercato di riferimento prodotto']//ancestor::lightning-combobox[1]//input")).click();
		util.objectManager(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Ordine di Grandezza']/..//input"), util.scrollToVisibility, false);
		util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria di Consumo']/..//input")).click();
		TimeUnit.SECONDS.sleep(1);
		String mercato="";
		
		
		ArrayList<String> arr = new ArrayList<String>();
		ArrayList<String> temp =  new ArrayList<String>();
		for (int i = 0; i < lista.length; i++) {
			arr.add(i, lista[i]);
			temp.add(i, lista[i]);
		}
		ArrayList<String> picklist = new ArrayList<String>();
		//List <WebElement> m = util.waitAndGetElements(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria di Consumo']/..//div//input"));//sostituirto con riga 285
		
		List <WebElement> m = util.waitAndGetElements(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria di Consumo']/../div/../div//span/span"));
		//  System.out.println(m.size());
			for (int i = 0; i < m.size(); i++) {
				
			    mercato = m.get(i).getText();
//			    System.out.println(mercato);
			    picklist.add(i, mercato);
	      }
			
			
			 arr.removeAll(picklist);
		if(arr.size()>0) throw new Exception("dalla combobox 'Commodity -> Categoria di Consumo' non sono presenti i valori selezionabili attesi: "+Arrays.toString(arr.toArray())+" riscontrati:"+Arrays.toString(picklist.toArray()));		 
		
		picklist.removeAll(temp);
		if(picklist.size()>0) throw new Exception("dalla combobox 'Commodity -> Categoria di Consumo' non sono presenti i valori selezionabili attesi: "+Arrays.toString(picklist.toArray()));		 
		
		util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria di Consumo']/..//span[text()='"+consumo+"']")).click();

		spinner.checkSpinners();
		Thread.currentThread().sleep(3000);
		
	}
	
	
	
	public void verificaCategoriaMerceologicaSAP() throws Exception {
		//util.waitAndGetElement(By.xpath("//label[text()='Mercato di riferimento prodotto']//ancestor::lightning-combobox[1]//input")).click();
		util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//input")).click();
		TimeUnit.SECONDS.sleep(1);
		String mercato="";
		String valueList="";
		String [] lista = {
				"SERVIZI GENERALI DEGLI EDIFICI PER ABITA",
				"POMPE DI CALORE NELLE ABITAZIONI PRIVATE",
				"AZIENDE AGRICOLE COMPRESI I  SERVIZI CON",
				"AZIENDA DI ALLEVAMENTO COMPRESI I SERVIZ",
				"BONIFICA E MIGLIORAMENTO FONDIARIO",
				"AZIENDE FORESTALI COMPRESI I SERVIZI CON",
				"RISTORANTI, BAR E TAVOLE CALDE",
				"CONSUMI RELATIVI AL POMPAGGIO",
				"SERVIZI AUSILIARI DELLE CENTRALI IDROELE",
//				"AREA DELLE COSTRUZIONI",
				"SERVIZI AUSILIARI DELLE CENTRALI TERMOEL",
//				"AREA DELLA PRODUZIONE E TRASMISSIONE",
//				"AREA DELLA DISTRIBUZIONE",
//				"MINIERE",
//				"CESSIONI A TITOLO VARIO",
				"VENDITE ALL'ESTERO",
				"VENDITE AI RIVENDITORI",
				"ESTRAZIONE DI COMBUSTIBILI SOLIDI",
				"ESTRAZIONE DI COMBUSTIBILI LIQUIDI E GAS",
				"ESTRAZIONE DI MINERALI DI URANIO E TORIO",
				"ESTRAZIONE DI MINERALI METALLIFERI",
				"ESTRAZIONE DI MATERIALI DI CAVA",
				"ESTRAZIONE DI MINERALE PER LE INDUSTRIE",
				"INDUSTRIA ALIMENTARE IN FORMA ASSOCIATA",
				"INDUSTRIA ALIMENTARE: CARNE, PESCE, LATT",
				"INDUSTRIA ALIMENTARE DEI PRODOTTI AGRICO",
				"BEVANDE",
				"TABACCO",
				"INDUSTRIE TESSILI",
				"INDUSTRIA DEL VESTIARIO ED ABBIGLIAMENTO",
				"INDUSTRIA DELLE PELLI E DEL CUOIO ESCLUS",
				"INDUSTRIA DELLE CALZATURE",
				"INDUSTRIA DEL LEGNO E DEL SUGHERO ESCLUS",
				"INDUSTRIA DELLA PASTA DI CARTA, CARTA E",
				"ARTICOLI IN CARTA E CARTONE",
				"EDITORIA, STAMPA E RIPRODUZIONE DI SUPPO",
				"COKERIA (INCLUSO TRATTAMENTO COMBUSTIBIL",
				"RAFFINERIE DI PETROLIO",
				"GAS TECNICI",
				"PRODOTTI ELETTROTERMICI",
				"CLORO-SODA",
				"ALTRE SOLUZIONI CHIMICHE PRIMARIE INORGA",
				"PRODUZIONE DI ORGANICI DI BASE ED INTERM",
				"FERTILIZZANTI E RELATIVI PRODOTTI DI BAS",
				"MATERIE PLASTICHE, RESINE SINTETICHE ED",
				"FARMACEUTICHE",
				"ALTRE PRODUZIONI DELLA CHIMICA SECONDARI",
				"PRODUZIONE DI FIBRE ARTIFICIALI O SINTET",
				"INDUSTRIA DEGLI ARTICOLI IN GOMMA",
				"INDUSTRIA DEGLI ARTICOLI IN MATERIE PLAS",
				"INDUSTRIE VETRARIE",
				"INDUSTRIE DEI PRODOTTI IN CERAMICA E PIA",
				"INDUSTRIE DEI LATERIZI",
				"INDUSTRIE DEL CEMENTO",
				"INDUSTRIE DEI MANUFATTI IN CEMENTO E SIM",
				"ALTRE INDUSTRIE DEI PRODOTTI MINERALI NO",
				"INDUSTRIE DELLA CALCE, GESSO E SIMILI",
				"SIDERURGICHE AL FORNO ELETTRICO",
				"INDUSTRIE PER LA PRODUZIONE DI FERRO LEG",
				"ALTRE PRODUZIONI SIDERURGICHE(CICLO INTE",
				"PRODUZIONE E PRIMA LAVORAZIONE DELL'ALLU",
				"FONDERIE",
				"PRODUZIONE E PRIMA LAVORAZIONE DELLO ZIN",
				"PRODUZIONE E PRIMA LAVORAZIONE DI ALTRI",
				"ELEMENTI IN METALLO",
				"INDUSTRIE MECCANICHE ED APPARECCHI MECCA",
				"FABBRICAZIONE DI MACCHINE PER UFFICIO,DI",
				"FABBRICAZIONE DI MACCHINE ED APPARECCHI",
				"APPARECCHI RADIOTELEVISIVI E PER LE COMU",
				"APPARECCHI MEDICALI, DI PRECISIONE, OTTI",
				"FABBRICAZIONE DI AUTOVEICOLI, RIMORCHI E",
				"COSTRUZIONI NAVALI E RIPARAZIONE DI MEZZ",
				"COSTRUZIONE DI ALTRI MEZZI DI TRASPORTO",
				"COSTRUZIONE E RIPARAZIONE DI MEZZI DI TR",
				"INDUSTRIA DEL MOBILIO E DELL' ARREDAMENT",
				"INDUSTRIE MANIFATTURIERE N.C.A.",
				"RICICLAGGIO",
				"ENERGIA ELETTRICA, VAPORE E ACQUA CALDA",
				"GAS",
				"ACQUA",
				"ALTRI LAVORI ED ATTIVITA' DELL'INDUSTRIA",
				"COMMERCIO DI AUTOVEICOLI, MOTOVEICOLI E",
				"MANUTENZIONE E RIPARAZIONE DI AUTOVEICOL",
				"VENDITA AL DETTAGLIO DI CARBURANTI E LUB",
				"COMMERCIO ALL'INGROSSO ED ATTIVITA' AUSI",
				"GRANDE DISTRIBUZIONE",
				"COMMERCIO AL MINUTO ALIMENTARE",
				"COMMERCIO AL MINUTO ESCLUSA LA GRANDE DI",
				"RIPARAZIONE DI BENI PERSONALI E PER LA CASA",
				"ESERCIZI ALBERGHIERI ED EXTRA-ALBERGHIER",
				"FORNITURE DI PASTI PREPARATI",
				"F.S. TRAZIONE",
				"ALTRI TRASPORTI TERRESTRI",
				"TRASPORTI MEDIANTE OLEODOTTI E GASDOTTI",
				"F.S. ALTRI USI",
				"TRASPORTI MARITTIMI E PER VIE D'ACQUA",
				"TRASPORTI AEREI",
				"ATTIVITA' AUSILIARIE DEI TRASPORTI",
				"COMUNICAZIONI",
				"CREDITO",
				"ASSICURAZIONI",
				"ATTIVITA' AUSILIARIE DELLA INTERMED. FIN",
				"ATTIVITA' IMMOBILIARI",
				"NOLEGGI SENZA OPERATORE",
				"INFORMATICA ED ATTIVITA' CONNESSE",
				"RICERCA E SVILUPPO",
				"ALTRE ATTIVITA' PROFESSIONALI E IMPRENDI",
				"PUBBLICA AMMINISTRAZIONE",
				"ISTRUZIONE PUBBLICA",
				"ISTRUZIONE PRIVATA",
				"OSPEDALI PUBBLICI",
				"CASE DI CURA PRIVATE",
				"ALTRI SERVIZI SANITARI PUBBLICI",
				"ALTRI SERVIZI SANITARI PRIVATI",
				"SERVIZI ECOLOGICI",
				"ORGANIZZAZIONI ASSOCIATIVE",
				"ATTIVITA' RICREATIVE,CULTURALI E SPORTIV",
				"ALTRI SERVIZI",
				"CANTIERI EDILI (EDIFICI PER USO RESIDEN",
				"CANTIERI EDILI (PER ALTRI USI CIVILI E U",
				"CAROVANE ANNESSE A SPETTACOLI",
				"COLONNINA PER RICARICA PUBBLICA",
				"COLONNINE DI RICARICA PRIVATA",
				"ALTRE AREE" //R3 FR 23.09.2021 aggiunta in R3
				

		};
		
		//PRINT LISTA VALORI ATTESI:
		System.out.println("VALORI ATTESI");
		for(String va: lista) System.out.println(va);
		System.out.println("TOTALE VALORI ATTESI: "+lista.length);
            
		ArrayList<String> arr = new ArrayList<String>();
		ArrayList<String> temp =  new ArrayList<String>();
		for (int i = 0; i < lista.length; i++) {
			arr.add(i, lista[i]);
			temp.add(i, lista[i]);
		}
		ArrayList<String> picklist = new ArrayList<String>();
		List <WebElement> m = util.waitAndGetElements(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//span[@class='slds-media__body']"));
		//  System.out.println(m.size());
		System.out.println("VALORI PRESENTI NELLA PICKLIST");
			for (int i = 0; i < m.size(); i++) {
				
			    mercato = m.get(i).getText();
			    System.out.println(mercato);
//			    System.out.println(mercato);
			    picklist.add(i, mercato.toUpperCase());
	      }
			System.out.println("TOTALE: "+picklist.size());
//			System.out.println(Arrays.toString(picklist.toArray()));
//			System.out.println(Arrays.toString(arr.toArray()));
			 arr.removeAll(picklist);
		if(arr.size()>0) throw new Exception("dalla combobox 'Commodity -> Categoria Merceologica SAP' non sono presenti i valori selezionabili attesi: "+Arrays.toString(arr.toArray())+" riscontrati:"+Arrays.toString(picklist.toArray()));		 
		
		picklist.removeAll(temp);
		if(picklist.size()>0) throw new Exception("dalla combobox 'Commodity -> Categoria Merceologica SAP' non sono presenti i valori selezionabili attesi: "+Arrays.toString(picklist.toArray()));		 
		

		spinner.checkSpinners();
		Thread.currentThread().sleep(3000);

	}
	
	public void cliccaConfermaChecklist() throws Exception {
		 if(util.exists(By.xpath("//button[@name='checklistConferma']"), 5)) {
	        	util.objectManager(By.xpath("//button[@name='checklistConferma']"), util.scrollAndClick);
	        }
	}
	
	public void selezionaFornitura(String frame,String pod) throws Exception{
		String fornituraXpath = this.radioButtonFornituraInserita.replaceAll("#", pod);
		util.frameManager(frame, By.xpath(fornituraXpath), util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
		
	}

	public void clickAvantiSelectionMercato(String frame, String market) throws Exception
	{

		util.frameManager(frame, this.selectMarket, util.select,market);

		util.frameManager(frame, this.avantiTipoMercato, util.scrollAndClick);
		spinner.checkSpinners(frame);


	}
	
	public void selezionaCategoria(String uso) throws Exception{
		if(util.verifyExistence(By.xpath("//h2[text()='']/following::select"), 30)) {
		util.selectLighningValue("Uso Fornitura", uso);
		Thread.currentThread().sleep(5000);
		util.objectManager(By.xpath("//h2[text()='Selezione Uso Fornitura']/ancestor::div[@role='main']//button[text()='Conferma' and not(@disabled)]"), util.scrollAndClick);
	    spinner.checkSpinners();
	    Thread.currentThread().sleep(5000);
		}else throw new Exception("Impossibile selezionare l'Uso Fornitura");
	}
	
	
public void verificaCategoriaDiConsumo(String categoria) throws Exception{
		
		String categoriaDiConsumo = util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria di Consumo']/..//input"),true).getAttribute("value");
		
		if((categoriaDiConsumo.length()<=0) ) {
		this.selezionaLigtheningValue("Categoria di Consumo", categoria);
		}
		
	 }

public void verificaCategoriaMarketing(String categoria) throws Exception{
	
	String categoriaMarketing = util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria di Marketing']/..//input"),true).getAttribute("value");
	
	if((categoriaMarketing.length()<=0) ) {
	this.selezionaLigtheningValue("Categoria di Marketing", categoria);
	}
	
 }
	
public void verificaOrdineGrandezza(String ordinegra) throws Exception{
	
	String ordine = util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Ordine di Grandezza']/..//input"),true).getAttribute("value");
	
	if((ordine.length()<=0) ) {
	this.selezionaLigtheningValue("Ordine di Grandezza", ordinegra);
	}
	
 }

public void verificaProfiloConsumo(String profilocon) throws Exception{
	
	String profilo = util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Profilo di Consumo']/..//input"),true).getAttribute("value");
	
	if((profilo.length()<=0) ) {
	this.selezionaLigtheningValue("Profilo di Consumo", profilocon);
	}
	
 }


public void verificaCampiPopolati() throws Exception{
		
		String ordineDiGrandezza = util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Ordine di Grandezza']/..//input"),true).getAttribute("value");
		String categMarketing = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Codice Ufficio']/..//input"),true).getAttribute("value");
		String profiloDiConsumo = util.waitAndGetElement(By.xpath("//h2[text()='Fatturazione Elettronica']/ancestor::div[@class='slds-card']//label[text()='Tipo Invio']/..//input"),true).getAttribute("value");
		
		if((ordineDiGrandezza.length()==0) )  throw new Exception(" ordineDiGrandezza Non risulta prepopolato");
		if((categMarketing.length()==0) )  throw new Exception(" ordineDiGrandezza Non risulta prepopolato");
		if((profiloDiConsumo.length()==0) )  throw new Exception(" profiloDiConsumo Non risulta prepopolato ");
	 }

	 public void selezionaUsoFornitura(String uso_forn) throws Exception{
			TimeUnit.SECONDS.sleep(3);
			WebElement element = util.waitAndGetElement(campoUsoFornitura,60,2);
			element.sendKeys(uso_forn);		
			TimeUnit.SECONDS.sleep(3);
			element.sendKeys(Keys.ENTER);
			
		}
	 

	
	public void clickAvantiSelectionMercato(String frame,By selectMercato,By avantiMercato, String market) throws Exception
	{

		util.frameManager(frame, selectMercato, util.select,market);
		util.frameManager(frame, avantiMercato, util.scrollAndClick);
		spinner.checkSpinners(frame);


	}
	
	public void selezionaMercato(By selectMercato, String mercato)throws Exception{
		util.objectManager(selectMercato, util.scrollAndSelect, mercato);
		spinner.checkSpinners();
	}
	
	
	public String inserisciSWAPod(String frame,String pod) throws Exception {
		util.frameManager(frame, this.swaPod, util.sendKeys,pod);
		util.frameManager(frame, this.puntoDiFornituraLabel, util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
		return pod;
		};
		
		
    public void confermaFornitura(String frame) throws Exception {
    	util.frameManager(frame, this.confermaFornituraButton, util.scrollAndClick);
    	spinner.waitForSpinnerByStyleDiplayNone(frame);
    }


	public void insertPodandPrecheck(String frame,String CAP, String pod) throws Exception
	{

//       //System.out.println("POD "+pod);
		util.frameManager(frame, this.podInput, util.sendKeys,pod);

		util.frameManager(frame, this.cap, util.sendKeys,CAP);

		util.frameManager(frame, this.eseguiPrecheck, util.scrollAndClick);

		spinner.waitForSpinnerByStyleDiplayNone(frame);
		//Inserito controllo per gestione pop up opzione istat
		WebElement ele = driver.findElement(By.xpath("//iframe[@name='"+ frame +"']"));
		driver.switchTo().frame(ele);
		if (util.exists(this.istatPopUp, 15)&&(driver.findElement(istatPopUp).isDisplayed())){
//			util.frameManager(frame, this.istatPopUp, util.scrollAndClick);
			util.objectManager(this.istatPopUp, util.scrollAndClick);
//			spinner.waitForSpinnerByStyleDiplayNone(frame);
			spinner.checkSpinners();
//			util.frameManager(frame, this.istatPopUpContinua, util.scrollAndClick);
			util.objectManager(this.istatPopUpContinua, util.scrollAndClick);
//			spinner.waitForSpinnerByStyleDiplayNone(frame);
			spinner.checkSpinners();
		}
		driver.switchTo().defaultContent();
	}
	
	public void nuovaFornitura(String frame) throws Exception{
		util.frameManager(frame, nuovaFornituraRadioButton, util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	
	
	
	public void creaOfferta(String frame,By buttonCrea) throws Exception{
		spinner.waitForSpinnerByStyleDiplayNone(frame);
//		//System.out.println("Clicco nuova offerta");
		util.frameManager(frame, buttonCrea, util.scrollAndClick);
//		//System.out.println("Cliccato nuova offerta");
	
		
	}


	public void compileVoltageInfo(String frame, String misuratore, String tension, String potenza) throws Exception
	{
		util.frameManager(frame,this.tipoMisuratore,util.select,misuratore);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
		util.frameManager(frame,this.tensioneConsegna,util.select,tension);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
		util.frameManager(frame,this.potenzaContrattuale,util.sendKeys,potenza);
		driver.findElement(
				By.tagName("body")
				).click();
		spinner.waitForSpinnerByStyleDiplayNone(frame);
//		//System.out.println("Clicco nuova offerta");
		util.frameManager(frame, this.creaOffertaButton, util.scrollAndClick);
//		//System.out.println("Cliccato nuova offerta");
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}
	
	public void compileVoltageInfo(By tipoMisuratore, By tensioneConsegna, By potenzaContrattuale,String misuratore, String tension, String potenza) throws Exception
	{
		
		util.objectManager(tipoMisuratore,util.select,misuratore);
		util.objectManager(tensioneConsegna,util.select,tension);
        Thread.currentThread().sleep(2000);
		util.objectManager(potenzaContrattuale,util.sendKeys,potenza);
		driver.findElement(
				By.tagName("body")
				).click();
		spinner.checkSpinners();
		//util.objectManager(this.creaOffertaButton, util.scrollAndClick);


	}
	
	public void compileVoltageInfoEVO(String misuratore, String tension, String potenza) throws Exception
	{
		util.selectLighningValue("Tipo misuratore", misuratore);
		spinner.checkSpinners();
		//util.objectManager(tipoMisuratore,util.select,misuratore);
		util.selectLighningValue("Tensione di consegna (V)", tension);
        Thread.currentThread().sleep(2000);
        util.sendText("Potenza contrattuale", potenza);
 //       util.objectManager(By.tagName("body"), util.scrollAndClick);
//		driver.findElement(
//				By.tagName("body")
//				).click();
		spinner.checkSpinners();
		//util.objectManager(this.creaOffertaButton, util.scrollAndClick);


	}

	public void avanti(String frame, String uso,String tipofatturazione) throws Exception {

		spinner.waitForSpinnerByStyleDiplayNone(frame);
		util.frameManager(frame, this.avantiCodiceAmico ,util.scrollAndClick);

		// Per uso diverso da abitazione
		//if(uso.compareTo("Uso Diverso da Abitazione")==0){
		if(tipofatturazione.compareTo("BUSINESS")==0){
			spinner.waitForSpinnerByStyleDiplayNone(frame);
			util.frameManager(frame, avantiReferente,util.scrollAndClick);
		}
		else{
			spinner.waitForSpinnerByStyleDiplayNone(frame);
			util.frameManager(frame, avantiDelegato,util.scrollAndClick);
		}
		
		spinner.waitForSpinnerByStyleDiplayNone(frame);
		util.frameManager(frame, usoAbitativo,util.select,uso);

		util.frameManager(frame, usoAbitativoNextBtn,util.scrollAndClick);

		spinner.waitForSpinnerByStyleDiplayNone(frame);
		
	}
	
	public void aggiungiFornitura(String frame,By aggiungiFornitura) throws Exception{
		util.frameManager(frame, aggiungiFornitura ,util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}
	
//	public void inserisciUnitaAbitativa(String frame,By unitaAbitativa,String valueUnitaAbitativa){
//		util.frameManager(frame, unitaAbitativa ,util.select,valueUnitaAbitativa);
//		
//	}
	public void avantiUnitaAbitativa(String frame,By buttonAvanti) throws Exception{
		util.frameManager(frame, buttonAvanti ,util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
		
	}
	public void selezionaUsoFornitura(String frame,By selectUsoFornitura,String valore) throws Exception{
		util.frameManager(frame, selectUsoFornitura ,util.select,valore);
		spinner.waitForSpinnerByStyleDiplayNone(frame, spinner.loadingSpinner);
		
	}
	public void selezionaUsoFornitura(By selectUsoFornitura,String valore) throws Exception{
		util.objectManager(selectUsoFornitura, util.scrollAndSelect, valore);
		spinner.checkSpinners();
		
	}
	
	public void selezionaUsoEnergia(By selectUsoEnergia,String valore) throws Exception{
		util.objectManager(selectUsoEnergia, util.scrollAndSelect, valore);
		spinner.checkSpinners();
		
	}
	
	
	public void avantiUsoFornitura(String frame,By buttonAvanti) throws Exception{
		util.frameManager(frame, buttonAvanti ,util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame, spinner.loadingSpinner);
		
	}
	
	public void avantiUsoFornitura(By buttonAvanti) throws Exception{
		util.objectManager(buttonAvanti, util.scrollAndClick);
		spinner.checkSpinners();
		
	}
	public void ConfermaPopUpWarning(String frame,By buttonConfirmPopUp) throws Exception{
		TimeUnit.SECONDS.sleep(5);
//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		if(util.exists(buttonConfirmPopUp, 30)&& driver.findElement(buttonConfirmPopUp).isDisplayed()){
			util.objectManager(buttonConfirmPopUp, util.scrollAndClick);
			spinner.checkSpinners();
			}
		driver.switchTo().defaultContent();
	}
	public void preCheck(String frame,By button) throws Exception{
		util.frameManager(frame, button ,util.scrollAndClick);
		spinner.checkSpinners(frame);	
	}
	
	public void conferma(String frame,By button) throws Exception{
		util.frameManager(frame, button ,util.scrollAndClick);
		spinner.checkSpinners(frame);	
	}
	
	public void selezionaFornitura(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();	
	}
	
	public void verificaPopUpconTesto(By obj, By objText,By button,String popupTestoAtteso) throws Exception{
		if(!util.verifyExistence(obj, 60)) throw new Exception("Non è stata visualizzata la pop up con testo: "+popupTestoAtteso);
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);

	}
	
	public void verificaPopUp(By obj, By objText,By button) throws Exception{
		if(!util.verifyExistence(obj, 60)) throw new Exception("Non è stata visualizzata la pop up con testo");
		String textModal = util.waitAndGetElement(objText,false).getText();
		if(textModal.length()<2) throw new Exception("La pop-up ha testo vuoto");
		util.objectManager(button,util.scrollAndClick);

	}
	
	public void verificaPopUpconTestoOpzionale(By obj, By objText,By button,String popupTestoAtteso) throws Exception{
		WebElement webElement = driver.findElement(By.xpath("//body"));
		webElement.sendKeys(Keys.TAB);
		spinner.checkSpinners();	
		if(util.exists(obj, 60)){
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);
		}

	}
	
	public void verificaPopUpResidente(By obj, By objText,By button,String popupTestoAtteso) throws Exception{
		WebElement webElement = driver.findElement(By.xpath("//body"));
		webElement.sendKeys(Keys.TAB);
		spinner.checkSpinners();	
		if(util.exists(obj, 60)){
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
		util.objectManager(button,util.scrollAndClick);
		}
		else throw new Exception("Non è stata visualizzata la pop up con testo");

	}
	
	
	public void verifyErrorMessageSelezioneMercato(By objText,String popupTestoAtteso) throws Exception{
		if(!util.verifyExistence(objText, 60)) throw new Exception("Non è stato visualizzato il messaggio di errore con testo: "+popupTestoAtteso);
		String textModal = util.waitAndGetElement(objText,false).getText();
//		System.out.println(textModal);
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
	}
	
	
	public void verifyValue(By obj,String text) throws Exception{

		String value = util.waitAndGetElement(obj,false).getText();
		if(value.compareTo(text)!=0) throw new Exception("il valore mostrato è:"+value+ "il valore atteso è:" +text);

	}
	
	
	public void verifyListCatMerceologicaSAP() throws Exception{
		
		util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//input")).click();
		TimeUnit.SECONDS.sleep(1);
		List <WebElement> m = util.waitAndGetElements(By.xpath("//label[text()='Categoria Merceologica SAP']/ancestor::div[@role='none']//div[@class='slds-form-element__control']"));
		  System.out.println(m.size());
		if (m.size() < (118)) {
			throw new Exception("La lista Categoria Merceologica SAP non contiene tutte le descrizioni richieste!");		
		}
	}
	

	public void checkListCatMerceologicaSAP() throws Exception {
		//util.waitAndGetElement(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//input")).click();
		util.objectManager(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//input"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		String value="";
		//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//span[@class='slds-media__body']"
		List <WebElement> m = util.waitAndGetElements(By.xpath("//h2[text()='Commodity']/ancestor::div[@role='region']//label[text()='Categoria Merceologica SAP']/..//span[@class='slds-media__body']"));
		  System.out.println(m.size());
			for (int i = 0; i < m.size(); i++) {
				
				value = m.get(i).getText();
			    System.out.println(value);
		         if (!value.contentEquals("SERVIZI PER LA RETE STRADALE") && !value.contentEquals("SERVIZI PER LA RETE AUTOSTRADALE"))
		        	throw new Exception("dalla combobox Categoria Merceologica SAP non sono presenti i valori selezionabili 'SERVIZI PER LA RETE STRADALE' e 'SERVIZI PER LA RETE AUTOSTRADALE', valore mostrato in picklist:"+value );		
	      }
	}

	public void selezionaMesiPodOrario() throws Exception {
		// TODO Auto-generated method stub
		String Misuratore = this.getLigtheningValue("Misuratore");
		
		if (Misuratore.equals("Orario") || Misuratore.equals("O"))  {
			String [] mesi = {"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};
			for (String string : mesi) {
				driver.findElement(By.xpath("//label[text()='"+string+"']/../..//input")).clear();
				driver.findElement(By.xpath("//label[text()='"+string+"']/../..//input")).sendKeys("1");
				Thread.currentThread().sleep(1000);
			}
			
			
		}
	}

	public void InseririsciConsumoAnnuoPodNonOrario() throws Exception {
		// TODO Auto-generated method stub
		if(util.exists(By.xpath("//label[text()='Misuratore']/..//input"), 6)) {
			String Misuratore = this.getLigtheningValue("Misuratore");
			
			if (Misuratore.equals("Non Orario") || Misuratore.equals("Elettronico")) {
			
				//selezione Consumo Annuo
				this.insertTextByChar(this.consumo_annuo_allaccio, "1254");
				
			}
		}
	}
	
	public void verificaEsistenzaMessaggioErroreCategoriaMerciologicaSap(String pod) throws Exception {
		
		if(!util.exists(By.xpath("//h2[contains(text(),\" La Categoria merceologica SAP non è congruente con l'Uso Fornitura del POD: \")]"), 15)) {
			throw new Exception("Messaggio 'La Categoria merceologica SAP non è congruente con l'Uso Fornitura del POD', non trovato.");
		}
		
	}

	public void compilaMultiCommodityVolturaConAccolloBSN(String categoriasap,String disalimentabilita, String categoriaconsumo, String categoriamarketing, String ordinegrandezza, String profiloconsumo) throws Exception {
	 List<WebElement> listaTR = driver.findElements(this.table_Commodity);

     if (listaTR.size() < 1)
         throw new Exception("la tabella con le informazioni  delle intestazioni ordinabili e dei valori cliccabili non contiene righe");

    
     for (int i=1; i<listaTR.size()+1; i++) {
    	 TimeUnit.SECONDS.sleep(5);
    	 util.objectManager(By.xpath("//h2[text()='Commodity']//ancestor::div[@class='slds-card']//table[@aria-label='Tabella delle intestazioni cliccabili e dei valori ordinabili']/tbody/tr["+i+"]//span[@class='slds-radio_faux']"), util.scrollAndClick);
    	 //driver.findElement(By.xpath("//h2[text()='Commodity']//ancestor::div[@class='slds-card']//table[@aria-label='Tabella delle intestazioni cliccabili e dei valori ordinabili']/tbody/tr["+i+"]//span[@class='slds-radio_faux']")).click();
     	 
     	 TimeUnit.SECONDS.sleep(5);
          String commodity= driver.findElement(By.xpath("//h2[text()='Commodity']//ancestor::div[@class='slds-card']//table[@aria-label='Tabella delle intestazioni cliccabili e dei valori ordinabili']/tbody/tr["+i+"]/td[2]")).getText();	
         if (commodity.equals("ELETTRICO"))
         { // Categoria Merceologica SAP
			this.selezionaLigtheningValue("Categoria Merceologica SAP", categoriasap);
		//Disalimentabilità
			this.selezionaLigtheningValue("Disalimentabilita’", disalimentabilita);
         }
         else if (commodity.equals("GAS"))
         {
        	// Categoria Merceologica SAP
        	 this.selezionaLigtheningValue("Categoria Merceologica SAP", categoriasap);
 			// Categoria di Consumo
        	 this.selezionaLigtheningValue("Categoria di Consumo",categoriaconsumo);
 			// Categoria di Marketing
        	 this.selezionaLigtheningValue("Categoria di Marketing",categoriamarketing);
 			// Ordine di Grandezza
        	 this.selezionaLigtheningValue("Ordine di Grandezza",ordinegrandezza);
 			//Profilo di Consumo
        	 this.selezionaLigtheningValue("Profilo di Consumo",profiloconsumo);
 
         }
                  driver.findElement(this.confermaFornituraButtonVCA).click();
         }
        
     	
     }
	
	}

