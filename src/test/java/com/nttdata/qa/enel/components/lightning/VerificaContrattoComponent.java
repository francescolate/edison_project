package com.nttdata.qa.enel.components.lightning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaContrattoComponent extends BaseComponent{
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;
	
	public By riepilogoOffertaLabel=By.xpath("//span[contains(text(),'Riepilogo offerta numero:')]");
	public By frame=By.xpath("//iframe[@id='iframeSF']");
	public By sezioneRiepilogo=By.xpath("//div[@class='big-black-title' and text()='RIEPILOGO']");
	public By sezioneDatiFornitura=By.xpath("//h3[@class='big-black-title' and text()='Dati fornitura']");
	public By sezioneDatiPagamento=By.xpath("//h3[@class='big-black-title' and text()='Dati di pagamento']");
	public By sezioneIndirizzoFatturazione=By.xpath("//h3[@class='big-black-title' and text()='Indirizzo di fatturazione']");
	public By sezioneIndirizzoResidenza=By.xpath("//h3[@class='big-black-title' and text()='Indirizzo di residenza']");
	public By sezioneTempiRipensamento=By.xpath("//h3[@class='big-black-title' and text()='Tempi di ripensamento']");
	public By sezioneConsensi=By.xpath("//h3[@class='big-black-title' and text()='Consensi per attività di marketing']");
	public By buttonEsci=By.xpath("//button[text()='Esci']");
	public By buttonConferma=By.xpath("//button[text()='Conferma']");
	public By accettaCookie=By.xpath("//button[@id='truste-consent-button']");
	public By pulsanteProsegui=By.xpath("//h4[text()='Ci siamo quasi!']/ancestor::div[@class='remodal-content']//p[text()='Per terminare la tua richiesta di attivazione compila la documentazione obbligatoria.']/ancestor::div[@class='remodal-content']//button[text()='Prosegui']");
	public By titleSezioneDocumenti=By.xpath("//div[@class='title' and normalize-space(text())='Dichiarazione di regolare possesso/detenzione dell’immobile (DL Casa)']");
	public By buttonCarica=By.xpath("//span[text()='Inserisci qui il tuo documento di riconoscimento']/ancestor::div[@class='upload-block']//label[@for='upload-doc']/input[@id='upload-doc']");
	public By buttonCaricaLabel=By.xpath("//span[text()='Inserisci qui il tuo documento di riconoscimento']/ancestor::div[@class='upload-block']//label[@for='upload-doc']");
	public By labelCaricoRichiesta=By.xpath("//p[normalize-space(text())='Abbiamo preso in carico la tua richiesta']");
	public By buttonInviaDocumento=By.xpath("//button[text()='Invia documento']");
	public By labelCompletaAttivazione=By.xpath("//div[@class='standard-title' and normalize-space(text())=\"Completa l'attivazione\"]");
	public By pagineProceduraAttivazione=By.xpath("//div[3]//p[@class='number-pagination' and normalize-space(text())='1/4']");
	public By metodoPagamentoBollettinoPostale=By.xpath("//input[@type='radio' and @value='Bollettino Postale']/ancestor::div[@class='radio-container']//label//span[text()='Bollettino Postale']");
	public By buttonContinua=By.xpath("//button[text()='CONTINUA']");
	public By buttonConfermaWeb=By.xpath("//button[text()='Conferma']");
	public By labelIndirizzoFatt=By.xpath("//h3[text()=\"Seleziona l'indirizzo di fatturazione\"]");
	public By radioIndirizziFatturazione=By.xpath("//input[@type='radio' and contains(@id,'billingAddress')]//ancestor::div[@class='radio-container']//span[contains(text(),'INDIRIZZO DI FATTURAZIONE')]");
	public By radioPrimoIndirizziFatturazione=By.xpath("//input[@type='radio' and contains(@id,'billingAddress')]//ancestor::div[@class='radio-container']//span[text()='INDIRIZZO DI FATTURAZIONE 1']");
	public By labelVuoiComunicarci=By.xpath("//label[text()='Oppure vuoi comunicarci un nuovo indirizzo di fatturazione?']");
	public By checkSi=By.xpath("//input[@type='radio' and @value='yes']/ancestor::div[@class='radio-container']//label//span[text()='Si']");
	public By checkNO=By.xpath("//input[@type='radio' and @value='no']/ancestor::div[@class='radio-container']//label//span[text()='No']");
	public By labelIndirizzoResidenza=By.xpath("//h3[text()='Indirizzo di residenza']");
	public By labelSeiResidente=By.xpath("//label[text()=\"Sei residente all'indirizzo per il quale stai richiedendo la fornitura?\"]");
	public By checkSiSeiResidente=By.xpath("//input[@type='radio' and @value='yes' and @id='yesResHolder' and @checked='true']/ancestor::div[@class='radio-container']//label//span[text()='Si']");
	public By checkNoSeiResidente=By.xpath("//input[@type='radio' and @value='no' and @id='noResHolder']/ancestor::div[@class='radio-container']//label//span[text()='No']");
	public By pagine3ProceduraAttivazione=By.xpath("//div[3]//p[@class='number-pagination' and normalize-space(text())='3/4']");
	public By labelConsensiAttMarketing=By.xpath("//h3[text()='Consensi per attività di marketing']");
	public By checkSiMarketingEnelEnergiaCellulare=By.xpath("//input[@type='radio' and @value='yes' and @id='cel-yes-marketing-consent' ]/ancestor::div[@class='radio-container']//label//span[text()='Si']");
	public By checkSiMarketingTerziCellulare=By.xpath("//input[@type='radio' and @value='yes' and @id='cel-yes-terzi-consent' ]/ancestor::div[@class='radio-container']//label//span[text()='Si']");
	public By checkSiMarketingProfilazioneEnergiaCellulare=By.xpath("//input[@type='radio' and @value='yes' and @id='yes-profile-consent' ]/ancestor::div[@class='radio-container']//label//span[text()='Si']");
	public By pagine4ProceduraAttivazione=By.xpath("//div[3]//p[@class='number-pagination' and normalize-space(text())='4/4']");
	public String text="Ho letto l’Informativa in materia di trattamento dei dati personali ai sensi del Regolamento UE 2016/679 (\"GDPR\")";
	public By informativa=By.xpath("//div[@class='form-group checkbox privacy-checkbox']//label/a[text()='Informativa']");
	
	public By pageCompilazioneDocumenti=By.xpath("//div[normalize-space(text())='Compilazione documenti']");
	public By dettaglioCompilazioneDocumenti=By.xpath("//div[@class='detail-title']");
	public By sezioneDichiarazioneragolaritaUrbanistica=By.xpath("//div[normalize-space(text())='Dichiarazione sulla regolarità urbanistica (Istanza 326)']//a[text()='consulta la guida']");
	public By sezioneDichiarazionePossessoImmobile=By.xpath("//div[normalize-space(text())='Dichiarazione di regolare possesso/detenzione dell’immobile (DL Casa)']//a[text()='consulta la guida']");
	public By testo1DichiarazioneragolaritaUrbanistica=By.xpath("//form//div[normalize-space(text())='Il sottoscritto, con la presente istanza, consapevole delle responsabilità e delle sanzioni penali previste dall’art. 75, 76 del D.P.R. 445/00 per false attestazioni e dichiarazioni mendaci, sotto la propria responsabilità']");
	public By testo2DichiarazioneragolaritaUrbanistica=By.xpath("//form//div[normalize-space(text())='dichiara']");
	public By testo3DichiarazioneragolaritaUrbanistica=By.xpath("//form//div[normalize-space(text())='che, in conformità all’art 48 del DPR n. 380/01 e successive modificazioni e alle norme, anche regionali, vigenti in materia, l’immobile è stato realizzato in base a:']");
	public By voceNessunProvvedimento=By.xpath("//div[text()='nessun provvedimento o comunicazione perché non richiesti dalla normativa vigente in materia']/ancestor::label[1]");
	public By titleSezioneDocumenti2=By.xpath("//div[@class='title' and normalize-space(text())='Dichiarazione di regolare possesso/detenzione dell’immobile (DL Casa)']//a[text()='consulta la guida']");
	public By testo1DichiarazioneRegolarePossesso=By.xpath("//div[@class='text' and normalize-space(text())='Il sottoscritto in conformità all’art. 5 del D.L.47/14, convertito in L. 80/14, DICHIARA di avere il seguente titolo sull’immobile sopra indicato:']");
	public By selezioneComodato=By.xpath("//div[text()='Locazione/Comodato (Atto già registrato o in corso di registrazione)']/ancestor::label[1]");
    public String testo2="Per poter procedere alla stipula del contratto dovrai compilare la seguente documentazione obbligatoria, così come imposto dalla normativa urbanistica e dalla legge 80/2014. Eventuali mancanze o errori NON permetteranno di attivare la fornitura. Tale documentazione verrà inoltrata al Comune territorialmente competente.";
	public By prosegui=By.xpath("//button[text()='prosegui']");
	public By conferma=By.xpath("//button[text()='Conferma']");
	public By titolo1PageInviaDocumentazione=By.xpath("//div[@class='title-upload' and normalize-space(text())='Scarica la documentazione che hai appena compilato da inviare firmata']");
	public By testo2PageInviaDocumentazione=By.xpath("//div[@class='text-upload' and normalize-space(text())='Istanze']");
	public By titolo2PageInviaDocumentazione=By.xpath("//div[@class='title-upload' and normalize-space(text())='Carica qui la documentazione firmata, accompagnata da un documento di identità valido.']");
	public By testoSmartPhone=By.xpath("//div[@class='title-upload']//span[@class='infoCamera']");
	public By testo3PageInviaDocumentazione=By.xpath("//div[@class='text-upload' and text()='Istanza 326/DL Casa/eventuale documentazione aggiuntiva']");
	public By testo4PageInviaDocumentazione=By.xpath("//div[@class='text-upload' and text()='Documento di riconoscimento']");
	public By buttonInviaDocumenti=By.xpath("//button[text()='invia documenti']");
	public By buttonInvioDocumento=By.xpath("//button[text()='Invia documento']");
	public By buttonCaricaDocumentoRiconoscimento=By.xpath("//span[text()='Inserisci qui il tuo documento di riconoscimento']/ancestor::div[@class='upload-block']//label[@for='upload-doc2']");
	public By buttonCaricaDoc = By.xpath("//span[text()='Inserisci qui il tuo documento di riconoscimento']/ancestor::div[@class='upload-block']//label[@for='upload-doc']");
	public By buttonCaricaIstanza=By.xpath("//span//div[text()='Istanza 326/DL Casa/eventuale documentazione aggiuntiva']/ancestor::div[@class='module-tp4']//label[@for='upload-dl']");
	public By TP2AcceptButton=By.xpath("//button[@id='truste-consent-button']");
	public String testo3="Manca poco per completare la tua richiesta! Ti basterà scaricare la documentazione obbligatoria che hai appena compilato, firmarla e effettuare il caricamento.";
	public String testo4="Se stai utilizzando il tuo Smartphone, potrai acquisire le immagini direttamente dalla tua fotocamera.";
	public VerificaContrattoComponent(WebDriver driver)  {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}
	
	public void scroll(By oggetto)throws Exception {
		util.objectManager(oggetto, util.scrollElement);
	}
	
	public void checkOfferta(String tipoOfferta, String pod, String contratto) throws Exception {
	String tipOfferta="//div[@id='field']//div[@class='bold-title' and text()='tipo offerta']/ancestor::div[1]//div[@class='CElight-title' and text()='#']";
	String tipo = tipOfferta.replace("#", tipoOfferta);
	By item1 = By.xpath(tipo);	
	verifyComponentExistence(item1);
	
	String npod="//div[@id='field']//div[@class='bold-title' and text()='POD/PDR']/ancestor::div[1]//div[@class='CElight-title' and text()='#']";
	String numeroPod = npod.replace("#", pod);
	By item2 = By.xpath(numeroPod);	
	verifyComponentExistence(item2);
	
	String nContratto="//div[@id='field']//div[@class='bold-title' and text()='numero contratto']/ancestor::div[1]//div[@class='CElight-title' and text()='#']";
	String numeroContratto= nContratto.replace("#", contratto);
	By item3 = By.xpath(numeroContratto);	
	verifyComponentExistence(item3);
	
	By prezzo=By.xpath("//div[@id='field']//div[@class='bold-title' and text()='PREZZO']");
	verifyComponentExistence(prezzo);
	
	By data=By.xpath("//div[@id='field']//div[@class='bold-title' and text()='data richiesta']");
	verifyComponentExistence(data);
	
	By indirizzo=By.xpath("//div[@id='field']//div[contains(@class,'bold-title') and text()='indirizzo fornitura']");
	verifyComponentExistence(indirizzo);
	}
	
	public void checkOffertaPageCompilazioneDocumentiAndSelectTerzaOpzione(String tipoOfferta, String pod, String contratto) throws Exception {
		String tipOfferta="//div[@id='field']//div[@class='bold-title' and normalize-space(text())='tipo offerta']/ancestor::div[1]//div[@class='CElight-title' and  normalize-space(text())='#']";
		String tipo = tipOfferta.replace("#", tipoOfferta);
		By item1 = By.xpath(tipo);	
		verifyComponentExistence(item1);
		
		String npod="//div[@id='field']//div[@class='bold-title' and normalize-space(text())='POD/PDR']/ancestor::div[1]//div[@class='CElight-title' and  normalize-space(text())='#']";
		String numeroPod = npod.replace("#", pod);
		By item2 = By.xpath(numeroPod);	
		verifyComponentExistence(item2);
		
		String nContratto="//div[@id='field']//div[@class='bold-title' and normalize-space(text())='numero contratto']/ancestor::div[1]//div[@class='CElight-title' and  normalize-space(text())='#']";
		String numeroContratto= nContratto.replace("#", contratto);
		By item3 = By.xpath(numeroContratto);	
		verifyComponentExistence(item3);
		
		By prezzo=By.xpath("//div[@id='field']//div[@class='bold-title' and normalize-space(text())='PREZZO']");
		verifyComponentExistence(prezzo);
		
		By data=By.xpath("//div[@id='field']//div[@class='bold-title' and normalize-space(text())='data richiesta']");
		verifyComponentExistence(data);
		
		By indirizzo=By.xpath("//div[@id='field']//div[contains(@class,'bold-title') and normalize-space(text())='indirizzo fornitura']");
		verifyComponentExistence(indirizzo);
		
		By checkBox1=By.xpath("//label/div[text()='Proprietà/Usufrutto/Abitazione per decesso del convivente di fatto']/ancestor::div[1]//input[@type='radio']");
		verifyComponentExistence(checkBox1);
		
		By checkBox2=By.xpath("//label/div[text()='Locazione/Comodato (Atto già registrato o in corso di registrazione)']/ancestor::div[1]//input[@type='radio']");
		verifyComponentExistence(checkBox2);
		
		By checkBox3=By.xpath("//label/div[text()='Altro documento che non necessita di registrazione']/ancestor::div[1]//input[@type='radio']");
		verifyComponentExistence(checkBox3);
		
		By checkBox4=By.xpath("//label/div[text()='Uso/Abitazione dell’immobile']/ancestor::div[1]//input[@type='radio']");
		verifyComponentExistence(checkBox4);
		
		clickComponentWithJse(By.xpath("//div[text()='Locazione/Comodato (Atto già registrato o in corso di registrazione)']/ancestor::label[1]"));
		}
	
	public void UploadDocumentsComponent(By checkObject) throws Exception {
		  WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			WebElement upload = driver.findElement(By.xpath("//div[@class='upload-block']//input[@id='upload-doc']"));
			upload.sendKeys("C:Desktop/doc.pdf");
			
		}
	
	public void sezioneNuovoIndirizzo() throws Exception {
		By indirizzo=By.xpath("//label[@id='Recupera Indirizzi di FatturazioneIndirizzo' and text()='Indirizzo']//ancestor::div[1]//input");
		By provincia=By.xpath("//label[@id='Recupera Indirizzi di FatturazioneProvincia' and text()='Provincia']//ancestor::div[1]//input");
		By comune=By.xpath("//label[@id='Recupera Indirizzi di FatturazioneComune' and text()='Comune']//ancestor::div[1]//input");
		By nCivico=By.xpath("//label[text()='Numero Civico']//ancestor::lightning-input[1]//input");
		verifyComponentExistence(indirizzo);
		verifyComponentExistence(provincia);
		verifyComponentExistence(comune);
		verifyComponentExistence(nCivico);
	}
	
	public void checkTestoInformativa()throws Exception {
		String testo=driver.findElement(By.xpath("//div[@class='form-group checkbox privacy-checkbox']//label")).getText();
		testo= testo.replaceAll("(\r\n|\n)", " ");
		//System.out.println(testo);
		if (!testo.contentEquals(text)) 
        	throw new Exception("la label di testo con descrizione "+text+" non e' presente");
	}
	
	public void checkTesto(By object, String testoDaVerificare)throws Exception {
		String testo=driver.findElement(object).getText();
		testo= testo.replaceAll("(\r\n|\n)", " ");
	//	System.out.println(testo);
		if (!testo.contentEquals(testoDaVerificare)) 
        	throw new Exception("la label di testo con descrizione "+testoDaVerificare+" non e' presente");
	}

	public void verificaCampiPresenti() throws Exception {
		// TODO Auto-generated method stub
		
		String [] Temp = {"TIPO OFFERTA","POD/PDR","PREZZO","DATA RICHIESTA","NUMERO CONTRATTO","INDIRIZZO FORNITURA"};
		ArrayList<String> toAttend = new ArrayList<String>(Arrays.asList(Temp));
		List<WebElement> els = driver.findElements(By.xpath("//div[@id='field']//div[contains(@class,'bold-title')]"));
		for (WebElement webElement : els) {
			if(webElement.getText()!=null) {
				toAttend.remove(webElement.getText());
			}
		}
		if(toAttend.size()!=0) {
			throw new Exception("Mancano nella sezione di riepilogo i seguenti campi: "+Arrays.toString(toAttend.toArray()));
		}
		
	}

	public void verificaSezioniPresenti() throws Exception {
		// TODO Auto-generated method stub
		String [] Temp = {"Dati fornitura","Dati di pagamento","Indirizzo di fatturazione","Indirizzo di residenza","Tempi di ripensamento","Consensi per attività di marketing"};
		ArrayList<String> toAttend = new ArrayList<String>(Arrays.asList(Temp));
		List<WebElement> els = driver.findElements(By.xpath("//h3[text()!='']"));
		for (WebElement webElement : els) {
			if(webElement.getText()!=null) {
				toAttend.remove(webElement.getText());
			}
		}
		if(toAttend.size()!=0) {
			throw new Exception("Mancano le seguenti Sezioni: "+Arrays.toString(toAttend.toArray()));
		}
	}
}
