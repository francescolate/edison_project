package com.nttdata.qa.enel.components.r2d;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_CaricamentoEsitiDelibera40Component {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public By buttonCarica=By.xpath("//input[contains(@src,'carica')]");
	public By buttonCerca=By.xpath("//input[contains(@src,'cerca')] | // a[contains(text(),'Cerca')]");
	public By buttonConferma=By.xpath("//input[contains(@src,'conferma')] | // a[contains(text(),'Conferma')]");
	public By inputPRD = By.xpath("//input[@name='pdr']");
	public By inputNumeroOrdineCRM = By.xpath("//input[@name='numeroOrdineCrm']");
	public By selectAvanzamento = By.xpath("//select[@name='avanzamentoDel40']");
	public By RadioAvanzamentoDelibera40 = By.xpath("//input[contains(@type,'radio') and (@value='D')]");
	public By aggiornamentoEsito=new By.ByXPath("//div[contains(text(),'Aggiornamento avvenuto con successo')]");

	public R2D_CaricamentoEsitiDelibera40Component(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	
	public void selezionaTipoCaricamentoAvanzato(String tipo) throws Exception{
		if(tipo.compareToIgnoreCase("Avanzamenti delibera 40")==0){
			util.objectManager(RadioAvanzamentoDelibera40,util.scrollAndClick);
		}
		else{
			throw new Exception("Tipo Caricamento Avanzata inserito non ammesso:"+tipo+".Valori ammessi:Avanzamenti delibera 40");
		}
	}
	
	public void inserisciPdr(By inputPDR, String valore) throws Exception{
		util.objectManager(inputPDR,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void inserisciNumeroOrdineCRM(By inputNumeroOrdineCRM, String valore) throws Exception{
		util.objectManager(inputNumeroOrdineCRM,util.scrollAndSendKeys,valore);
		TimeUnit.SECONDS.sleep(3);
	}
	
	
	public void cercaPod(By buttonCerca) throws Exception{
		util.objectManager(buttonCerca,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
	}
	
	public void selezionaTastoAggiornaPratica() throws Exception{
	By	buttonAzione=new By.ByXPath("//table//tr/td/img[contains(@alt,'Procedi')and contains(@alt,'aggiornamento')]");
	int risultatoRicerca=driver.findElements(buttonAzione).size();
	Logger.getLogger("").log(Level.INFO, "Risultato ricerca:"+risultatoRicerca);
	if(risultatoRicerca==0){
		throw new Exception("Nessun risultato restituito");
	}
	else if(risultatoRicerca!=1){
		throw new Exception("La ricerca restituisce più di un risultato.Verificare");
	}
	else{
		util.objectManager(buttonAzione,util.scrollAndClick);
	TimeUnit.SECONDS.sleep(3);}
}
	
	public void selezioneAvanzamento(By selectAvanzamento,String avanzamento) throws Exception{
		util.objectManager(selectAvanzamento,util.scrollAndSelect,avanzamento);
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(buttonConferma,util.scrollAndClick);
		//Gestione pop-up
		TimeUnit.SECONDS.sleep(10);
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(3);
		//Verifica avventuta inserimento esito
		if(!util.exists(aggiornamentoEsito, 30)){
			throw new Exception("Messaggio 'Aggiornamento effettuato correttamente' non visualizzato. Possibili problemi nell'inserimento dell'esito. Verificare");
		}
	}



}
