package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupportFaqComponent {


	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By qst1 = By.xpath("//section[1]/button");
	public By qst2 = By.xpath("//section[2]/button");
	public By qst3 = By.xpath("//section[3]/button");
	public By qst4 = By.xpath("//section[4]/button");
	public By qst5 = By.xpath("//section[5]/button");
	
	public String InfoLegaliUrl = "https://www-coll1.enel.it/it/supporto/faq/info-legali";
	public By infoLegaliContent = By.xpath("//section[@data-module='article-text-container']");

	public String creditsUrl = "https://www-coll1.enel.it/it/supporto/faq/credits";
	public By creditsContent = By.xpath("//section[@data-module='article-text-container']");
	public By creditsPath = By.xpath("//ul[@class='breadcrumbs component']");
	public By creditsTitle = By.xpath("//h1[text()='Credits']");
	
	public String suppotoFaqUrl = "https://www-coll1.enel.it/it/supporto/faq/faq-area-clienti";
	public By supportoFaqPath = By.xpath("//nav[@role='navigation']");
	public By ans3 = By.xpath("//section[3]//p[2]");
	public By inviaci = By.xpath("(//section[3]//p[2]//a)[1]");
	
	public SupportFaqComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.equalsIgnoreCase(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
				}
		
	public void checkForText(By checkObject, String property) throws Exception {
		
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);
		
	}
	
	public void CheckForFaq(String qstn1, String qstn2, String qstn3, String qstn4, String qstn5) throws Exception
	{
	   
		Set <String> windows = driver.getWindowHandles();
		 String currentHandle = driver.getWindowHandle();
		
		// String qstn1 = "Chi può accedere all’area riservata di Enel?";
		for (String winHandle : windows) {
		   // Switch to child window
		//	System.out.println("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
		 
		 SupportFaqComponent sqc = new SupportFaqComponent(driver);
			
			sqc.verifyComponentExistence(sqc.pageTitle);
			
			util.scrollToElement(driver.findElement(sqc.qst1));
			
			verifyComponentExistence(qst1);
			verifyComponentExistence(qst2);
			verifyComponentExistence(qst3);
			verifyComponentExistence(qst4);
			verifyComponentExistence(qst5);

			//System.out.println(driver.findElement(sqc.qst1).getText()+ " ");
			
			checkForText(qst1, qstn1);

			checkForText(qst2, qstn2);

			checkForText(qst3, qstn3);

			checkForText(qst4, qstn4);
			
			checkForText(qst5, qstn5);
	 
			driver.close();
			driver.switchTo().window(currentHandle);

	 }
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkForQuestions(String arr[],By qstn) throws Exception{
			String question = driver.findElement(qstn).getText();
			System.out.println(question);
			if(!Arrays.asList(arr).contains(question))
				throw new Exception ("Provided question is not present in the Page");
		}
	public static final String InfoLegaliContent = "Informazioni legaliI testi, le informazioni, gli altri dati pubblicati in questo sito nonché i link ad altri siti presenti sul web hanno esclusivamente scopo informativo e non assumono alcun carattere di ufficialità.Enel Energia ed Enel Italia non si assumono alcuna responsabilità per eventuali errori od omissioni di qualsiasi tipo e per qualunque tipo di danno diretto, indiretto o accidentale derivante dalla lettura o dall'impiego delle informazioni pubblicate, o di qualsiasi forma di contenuto presente nel proprio sito o per l'accesso o l'uso del materiale contenuto in altri siti.";
	public static final String CreditsContent = "Enel Energia S.p.A.Sede Legale 00198 Roma, viale Regina Margherita 125Numero R.E.A. RM 1150724Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007Capitale Sociale: Euro 302.039,00 (al 1° aprile 2016) i.v.Enel Italia S.r.l.Sede Legale 00198 Roma, viale Regina Margherita 125​Iscritta al Registro delle Imprese,Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007Capitale Sociale: Euro 50.000.000Enel Produzione S.p.A.​Sede Legale 00198 Roma, viale Regina Margherita 125​Iscritta al Registro delle Imprese,Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007Capitale Sociale Euro 1.800.000.000";
	public static final String CreditsPath = "Home/supporto/Credits";
	public static final String PageTitle = "Non riesco ad accedere all'Area Clienti Enel Energia";
	public static final String SupportoFaqPath = "Home/supporto/Non riesco ad accedere all'Area Clienti Enel Energia";
	public static final String SupportoFaqPathNew = "Home/supporto/Non riesco ad accedere all'Area Clienti Enel Energia posizione attuale";
	public String[] SuppotFaqArray = {"Non riesco ad accedere e a registrarmi al mio profilo unico","Ho dimenticato la password. Posso recuperarla?","Non riesci ancora ad accedere o a registrarti?","Ho problemi con il codice OTP. Cosa devo fare?","Non ricordo lo username con il quale mi sono registrato, cosa devo fare?"};
	public static final String Ans3 = "Non ti preoccupare, inviaci un messaggio e ti aiuteremo. Oppure parla con un operatore cliccando qui.";

}
