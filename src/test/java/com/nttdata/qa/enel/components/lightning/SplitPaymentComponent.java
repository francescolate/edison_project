package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SplitPaymentComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	
	public SplitPaymentComponent(WebDriver driver)
	{
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
//	public By confermaSplitSWAEVO = By.xpath("//h2[text()='Split Payment']/ancestor::div[@role='slds-card']//button[text()='Conferma']");
	public By confermaSplitSWAEVO = By.xpath("//h2[text()='Split Payment']/ancestor::div[@role='main' and @aria-labelledby]//button[text()='Conferma']");
	public By selezionaSplitPayment = By.xpath("//label[contains(text(),'Split Payment')]/parent::div//input[@role='listbox']");
	public By buttonConfermaSplitPayment=By.xpath("//h2//*[text()='Split Payment']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By salvaFornituraSplitPayment = By.xpath("//h2//*[text()='Split Payment']/ancestor::div[@role='region']//button[text()='Salva fornitura']");
	public By sezioneSpliPayment = By.xpath("//h2[text()='Split Payment']");
    public String colonne []={"Indirizzo fornitura","POD","Split Payment","POD/PDR","Inizio validità","Fine validità"};
	public By radiobuttonSplitPayment = By.xpath("//h2[text()='Split Payment']/ancestor::div[@role='region']//table/tbody/tr/td//span");
	public By splitPaymentMessage = By.xpath("//p[text()='Il primo inserimento di Split Payment verrà applicato a tutte le linee di fornitura presenti. Successivamente se necessario sarà possibile modificare singolarmente tutte le linee di fornitura.']");
	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void selezionaSplitPayment(String valore) throws Exception{
//		util.selectLighningValue("Split Payment", valore);
		util.objectManager(selezionaSplitPayment, util.scrollAndClick);
		By scelta=By.xpath("//label[text()='Split Payment']/parent::div//span[text()='"+valore+"']");
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(scelta, util.click);
	}
	
	 public void popolaCampiSplitPayment()throws Exception{
		 SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/YYYY");
	     Calendar calendar = new GregorianCalendar();
	     fmt.setCalendar(calendar );
	     String dateFormatted = fmt.format(calendar.getTime());
	     By inizioValidita=By.xpath("//h2[text()='Split Payment']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Inizio Validità')]/following::input[1]");
	     //By fineValidita=By.xpath("//h2[text()='Split Payment']/ancestor::div[contains(@class,'slds-card')]//label[contains(text(),'Fine Validità')]/following::input[1]");
	     util.objectManager(inizioValidita, util.sendKeys,dateFormatted);		
	     //util.objectManager(fineValidita, util.sendKeys,"31/12/2999");		
	 }
	 
	 public void verificaColonneTabella(String [] col) throws Exception{
		 List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//table[@id='supplyComponent_Table']/thead/tr/th/div/span"));
		 if (nomeColonne.size()==col.length) {
			 for(int i = 1; i <= col.length; i++){
				    WebElement c=driver.findElement(By.xpath("//table[@id='supplyComponent_Table']/thead/tr/th["+(i+1)+"]/div/span"));
					String columnName=c.getText();	
					//System.out.println(columnName);
					//System.out.println(col[i-1].trim());
					 if(!columnName.contentEquals(col[i-1].trim()))
					       	throw new Exception("la tabella forniture non contiene le colonne con descrizione: "+col[i]+"; il nome delle colonne visualizzato e' "+columnName);  
				}
		 }
	 }
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
		    if (!util.exists(existingObject, 15))
					throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
			}
		
}
