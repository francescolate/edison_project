package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class TopMenuComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	
	public final static String voceMenu="//li//a[text()='#']";
	public By path_menu_text=By.xpath("//ul[@class='breadcrumbs component']");
	public By title_menu_text=By.xpath("//h1");
	public final static String detail_menu_text_by_class="//p[contains(@class,'#1')]";
	public final static String detail_menu_text_by_id="//p[@id='#1']";
	
	public TopMenuComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
		
	}

	public void sceltaMenu(String textLink) throws Exception{
		util.objectManager(By.xpath(voceMenu.replace("#", textLink)), util.scrollToVisibility, false);
		util.objectManager(By.xpath(voceMenu.replace("#", textLink)), util.click);
	}

	public void checkUrl(String url) throws Exception{
		String link=driver.getCurrentUrl();
		
		if (url.contains("https://"))
		{
			url = url.substring(8);
		}
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
		}
	
	public void checkMenuPath(String path) throws Exception {
		WebElement obj = util.waitAndGetElement(path_menu_text);
		String pathDescription=obj.getText();
		pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");	

		if (!pathDescription.contains(path))
			throw new Exception("object path with xpath " + path_menu_text + " is not exist. \n" + pathDescription + "\n" + path);
	}

	public void checkTitle(String title) throws Exception {
		WebElement obj = util.waitAndGetElement(title_menu_text);
		String titleDesctiption=obj.getText();

		if (!titleDesctiption.equalsIgnoreCase(title))
			throw new Exception("page title not equals to " + title);
	}
	
	public void checkDetailByClass(String detail, String className ) throws Exception {
		WebElement obj = util.waitAndGetElement(By.xpath(detail_menu_text_by_class.replace("#1" , className)));
		String detailDesctiption=obj.getText();
		detailDesctiption= detailDesctiption.replaceAll("(\r\n|\n)", " ");
		if (!detailDesctiption.equalsIgnoreCase(detail))
			throw new Exception("page detail '"+detailDesctiption+"' not equals to " + detail);
	}
	public void checkDetailById(String detail, String id ) throws Exception {
		WebElement obj = util.waitAndGetElement(By.xpath(detail_menu_text_by_id.replace("#1" , id)));
		String detailDesctiption=obj.getText();
		detailDesctiption= detailDesctiption.replaceAll("(\r\n|\n)", " ");
		if (!detailDesctiption.equalsIgnoreCase(detail))
			throw new Exception("page detail not equals to " + detail);
	}
}
