package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
public class ModuloReclamiComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By moduloReclamiTitle = By.xpath("//h1[text()='Modulo Reclami']");
	public By moduloReclamiPagePath = By.xpath("//ul[@class='breadcrumbs component']");
	public By tipologia_reclamoLabel= By.xpath("//label[@for='tipologia_reclamo' and text()='Tipologia reclamo']");
	public By tipologia_reclamoDropdown = By.xpath("(//div[@class='select-styled' and @tabindex='0'])[1]");
	public By reclamoLuce = By.xpath("//ul//li[text()='Reclamo Gas']");
//	public By nome = By.xpath("//input[@name='form_nome']");
//	public By cognome = By.xpath("//input[@name='form_cognome']");
	public By cellulare = By.xpath("//input[@name='form_numero_cellulare']");
	public By fasceorarie = By.xpath("//div[@class='select-styled custom-select' and @tabindex='0']");
	public By cf = By.xpath("//input[@id='form_codice_fiscale']");
//	public By email = By.xpath("//input[@id='form_mail']");
//	public By confermaEmail = By.xpath("//input[@id='form_conferma_mail']");
	public By numeroClienti = By.xpath("//input[@id='form_eneltel']");
	public By pod = By.xpath("//input[@id='form_pod']");
	public By fasceorarieValue = By.xpath("//ul[@class='select-options scrollbar']//li[text()='14:00 - 17:00']");
	public By fasceorarieValue1 = By.xpath("//ul[@class='select-options scrollbar']//li[text()='8:00 - 12:00']");
	public By indrizzo = By.xpath("//input[@id='form_indirizzo']");
	
	public By argumentoDropdown = By.xpath("(//div[@class='select-styled'])[2]");
	public By morosita = By.xpath("//li[contains(text(),'Morosita')]");
	public By descrizione = By.xpath("//textarea[@id='descrizione_reclamo']");
	public By autolettura = By.xpath("//input[@id='form_autolettura']");
	public By dateAutolettura = By.xpath("//input[@id='form_data']");
	public By checkBox = By.xpath("//span[@class='checkmark']");
	public By conferma = By.xpath("//button[@id='SubmitProceedForm']");
	public By moduloReclaimMsg = By.xpath("//section[@id='message_success_invio']//label[@class='testo_mail_ok']");
	public By indietro = By.xpath("//button[@id='accediupl_Indietro']");
	public By moduloReclaimMsgTitle = By.xpath("//section[@id='message_success_invio']//h3[contains(text(),'Modulo Reclami')]");
	public By supportoPath = By.xpath("//ul[@class='breadcrumbs component']");
	public By supportoTitle = By.xpath("//h1[contains(text(),'Come inviare')]");
	
	public String headerVoice = "//div[@class='dotcom-header__links dotcom-header__links-custom']//ul//li//a[text()='$VALUE$']";
	public String footerLink = "//a[text()='$Value$']";
	public By enelItalia =By.xpath("//li[contains(text(),'Tutti i diritti riservati  © Enel Italia S.p.a. © Enel Energia S.p.a. ')]");
	public By enelEnergia = By.xpath("//li[text()='Gruppo IVA Enel P.IVA 15844561009']");
	public By preferenzeCookieContent = By.xpath("//div[@class='gdpr']");
	public By popupFrame = By.xpath("//iframe[contains(@id,'pop-frame')]");
	public By inviaPreferenze = By.xpath("//a[text()='Invia preferenze']");
	public By informativaPolicy = By.xpath("//a[@id='privacyPolicyAnchor']");
	public By impostazioniTrasmesse = By.xpath("//span[text()='Impostazioni Trasmesse']");
	public By impostazioniTrasmesseContent = By.xpath("//div[@class='contentfield']");
	public By chiudere = By.xpath("//a[text()='chiudere']");

	public By userIcon = By.xpath("//span[@class='icon-user']");
	
	
	public By home = By.xpath("//li/a[.='Home']");
	public By formReclami = By.xpath("//li/a[.='form-reclami']");
	public By reclamiHeading = By.xpath("//h1[.='Modulo Reclami']");
	public By errorMessageTipologiaReclamo = By.xpath("//div[@id='form_select_tipo_reclamo']/div[@class='text-right errore_testo p-0']/span[.='Effettua una selezione']");
	public By tipologiaDelReclamo = By.xpath("//div[@id='form_select_tipo_reclamo']");
	public By optionReclamoLuce = By.xpath("//div[@id='form_select_tipo_reclamo']/ul/li[.='Reclamo Luce']");
	public By optionReclamoGas = By.xpath("//div[@id='form_select_tipo_reclamo']/ul/li[.='Reclamo Gas']");
	public By inserisciITuoiDati = By.xpath("//form[@id='form_active_web']//h3[.='Inserisci i tuoi dati ']");
	public By nome = By.xpath("//input[@id='form_nome']");
	public By cognome = By.xpath("//input[@id='form_cognome']");
	public By telefonoCellulare = By.xpath("//input[@id='form_numero_cellulare']");
	public By fasceOrarieDiReperibilità = By.xpath("//div[@id='form_fasce_orarie']//div[.='Seleziona orario']");
	public By option8to12 = By.xpath("//div[@id='form_fasce_orarie']//ul/li[.='8:00 - 12:00']");
	public By codiceFiscale = By.xpath("//input[@name='form_codice_fiscale']");
	public By indirizzoDellaForniture = By.xpath("//input[@name='form_indirizzo']");
	public By email = By.xpath("//input[@id='form_mail']");
	public By confermaEmail = By.xpath("//input[@id='form_conferma_mail']");
	public By numeroCliente = By.xpath("//input[@id='form_eneltel']");
	public By codicePOD = By.xpath("//input[@id='form_pod']");
	public By codicePDR = By.xpath("//input[@id='form_pod']");
	public By codicePDRLabel = By.xpath("//div[@id='pdr']/label");
	public By codicePODLabel = By.xpath("//div[@id='pod']/label");
	public By confermaButton = By.xpath("//button[@id='SubmitProceedForm']");
	public By argomentoeDescrizioneDelReclamo = By.xpath("//form[@id='form_active_web']/h3[.=' Argomento e descrizione del reclamo ']");
	public By effttuaUnaSelezione = By.xpath("//div[@id='form_argomento_recl']//span[.='Effettua una selezione']");
	public By argomentoValue = By.xpath("//div[@id='form_argomento_recl']/ul[@class='select-options scrollbar']/li");
	public By argomentDropdown = By.xpath("//form[@id='form_active_web']/div[@class='row ml-0'][2]/div[@id='form_argomento_recl']/div[@class='select-styled']");
	public By contatti = By.xpath("//div[@id='form_argomento_recl']/ul[@class='select-options scrollbar']/li[.='Contratti']");
	public By descrizioneRichiesta = By.xpath("//textarea[@id='descrizione_reclamo']");
	public By descrizioneErrorMessage = By.xpath("//form[@id='form_active_web']//span[@class='errorMsg errore_descrizione']");
	public By casellaNonSelezionata = By.xpath("//form[@id='form_active_web']//span[.='Casella non selezionata']");
	public By errorMessageNome = By.xpath("//input[@id='form_nome']/following-sibling::div/span[.='Il campo è obbligatorio']");
	public By errorMessgaeCogNome = By.xpath("//input[@id='form_cognome']/following-sibling::div/span");
	public By errorMessagetelefonaCellulare = By.xpath("//div[@id='gruppo_cell']//span[.='Il campo è obbligatorio']");
	public By errorMessagetelefonaCellulare2 = By.xpath("//div[@id='gruppo_cell']//span[.='Formato non corretto']");
	public By errorMessageCodiceFiscale = By.xpath("//input[@id='form_codice_fiscale']/following-sibling::div/span[.='Il campo è obbligatorio']");
	public By errorMessageCodiceFiscale2 = By.xpath("//input[@id='form_codice_fiscale']/following-sibling::div/span[.='Il codice fiscale o la partita iva non sono validi']");
	public By errorMessageindirizzoDellaForniture = By.xpath("//input[@id='form_indirizzo']/following-sibling::div/span[.='Il campo è obbligatorio']");
	public By errorMessageindirizzoDellaForniture2 = By.xpath("//input[@id='form_indirizzo']/following-sibling::div/span[.='Formato non corretto']");
	public By errorMessageEmail = By.xpath("//input[@id='form_mail']/following-sibling::div/span");
	public By errorMessageConfermaEmail = By.xpath("//input[@id='form_conferma_mail']/following-sibling::div/span");
	public By errorMessagefasceOrarieDiReperibilità = By.xpath("//div[@id='form_fasce_orarie']//span[@class='errore_select_orari']");
	public By errorMessgaeincorrectNome = By.xpath("//input[@id='form_nome']/following-sibling::div/span[.='Sono ammesse solo lettere']");
	public By errorMessageNumeroCliente = By.xpath("//input[@id ='form_eneltel']/following-sibling::div/span");
	public By calcolaOra = By.xpath("//span[@id='myBtn_3']");
	public By calcolaCodiceFiscalePopup = By.xpath("//h3[@id='titolo_calcolo_cf']");
	public By name = By.xpath("//input[@id='name']");
	public By surName = By.xpath("//input[@id='surname']");
	public By gender = By.xpath("//div[@id='div_gender']");
	public By date = By.xpath("//input[@id='birthday']");
	public By si = By.xpath("//input[@id='yesitalian']");
	public By no = By.xpath("//input[@id='noitalian']");
	public By provincia = By.xpath("//div[@id='provincia']/div[@class='select-styled']");
	public By commune = By.xpath("//select[@id='birthplace']");
	public By popUpClose = By.xpath("//div[@id='myModal_3']//span[@class='close_3 pointer']");
	public By provinciaAgrigento = By.xpath("//div[@id='provincia']/ul[@class='select-options scrollbar']/li[.='Agrigento']");
	public By provinciaNapoli = By.xpath("//div[@id='provincia']/ul[@class='select-options scrollbar']/li[.='Napoli']");
	public By errorMessageName = By.xpath("//input[@id='name']/following-sibling::div/span[.='Il campo è obbligatorio']");
	public By errorMessagesurName = By.xpath("//input[@id='surname']/following-sibling::div/span[.='Il campo è obbligatorio']");
	public By errorMessageBirthDay = By.xpath("//input[@id='birthday']/following-sibling::span[.='Il campo è obbligatorio']");
	public By errorMessagesesso = By.xpath("//span[@class='errorMsg mr-3 mr-lg-2 errore_gender']");
	public By sessoFemmina = By.xpath("//div[@id='div_gender']/ul[@class='select-options scrollbar']/li[.='Femmina']");
	public By calcolaOraButton = By.xpath("//button[@id='button_cf']");
	
	 public ModuloReclamiComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}
		public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.click);
			
		}
		
		public void jsClickComponent(By clickableObject) throws Exception {
			util.jsClickElement(clickableObject);
			
		}

		public void verifyComponentExistence(By oggettoEsistente) throws Exception {
			if (!util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
		}
		
		public void verifyComponentNonExistence(By oggettoEsistente) throws Exception {
			if (util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
		}
		
		public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}
		
		public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void checkForArgomentoValue(By checkObject, String Value[]) throws Exception{
			List<WebElement> List = driver.findElements(By.xpath("//span[@id='form_argomento_reclamoSelectBoxItContainer']//ul/li"));
			int listSize = List.size();
			List<String> expectedList = Arrays.asList(Value);
			List<String> newList = new ArrayList<String>();
			
			for(WebElement ArgomentoList : List) {
			newList.add(ArgomentoList.getAttribute("innerText"));
			System.out.println(ArgomentoList.getAttribute("innerText"));
			}
						
			Collections.sort(newList);
			Collections.sort(expectedList);
			
			if(!newList.equals(expectedList))
	    		throw new Exception ("The  details are not correctly displayed");
			
	}
		
		public void jsClickObject(By by) throws Exception {
			util.jsClickElement(by);
		}
		
		 public void scrollComponent(By object) throws Exception {
				util.objectManager(object, util.scrollToVisibility, false);
			}
		 
		 public void verifyFeildAttribute(By existingObject, String text) throws Exception {
				String actualText = driver.findElement(existingObject).getAttribute("placeholder");
				if (!actualText.equalsIgnoreCase(text))
					throw new Exception("object with text " + text + " is not exist.");
			}
		 
		 public void verifyTex(By existingObject, String text) throws Exception {
				String actualText = driver.findElement(existingObject).getText();
				actualText = normalizeInnerHTML(actualText);
				if (!actualText.equalsIgnoreCase(text))
					throw new Exception("object with text " + text + " is not exist.");
			}
		 
		 public void checkForSessoValue(By checkObject, String Value[]) throws Exception{
				List<WebElement> List = driver.findElements(By.xpath("//div[@id='div_gender']/ul[@class='select-options scrollbar']/li"));
				int listSize = List.size();
				List<String> expectedList = Arrays.asList(Value);
				List<String> newList = new ArrayList<String>();
				
				for(WebElement ArgomentoList : List) {
				newList.add(ArgomentoList.getAttribute("innerText"));
				System.out.println(ArgomentoList.getAttribute("innerText"));
				}
							
				Collections.sort(newList);
				Collections.sort(expectedList);
				
				if(!newList.equals(expectedList))
		    		throw new Exception ("The  details are not correctly displayed");
				
		}
		 
		 public void checkForProvinciaValue(By checkObject, String Value[]) throws Exception{
			 
			 	List<WebElement> List = driver.findElements(By.xpath("//select[@id='birthplace_province']/following-sibling::ul/li"));
				int listSize = List.size();
			 	
			 	List<String> expectedList = Arrays.asList(Value);
				List<String> newList = new ArrayList<String>();
				
				for(WebElement ArgomentoList : List) {
				newList.add(ArgomentoList.getText());
				System.out.println(ArgomentoList.getAttribute("innerText"));
							
				if(!newList.equals(expectedList))
		    		throw new Exception ("The  details are not correctly displayed");
				}	
		}
		 
		 public void checkForCommuneValue(By checkObject, String Value[]) throws Exception{
				List<WebElement> List = driver.findElements(By.xpath("//div[@id='comune']/ul[@class='select-options scrollbar']/li"));
				int listSize = List.size();
				List<String> expectedList = Arrays.asList(Value);
				List<String> newList = new ArrayList<String>();
				
				for(WebElement ArgomentoList : List) {
				newList.add(ArgomentoList.getAttribute("innerText"));
				System.out.println(ArgomentoList.getAttribute("innerText"));
				}
							
				Collections.sort(newList);
				Collections.sort(expectedList);
				
				if(!newList.equals(expectedList))
		    		throw new Exception ("The  details are not correctly displayed");
				
		}
		 
		 public void getAttributeInnerText(By checkObject, String Value)throws Exception {
			  
			 String we = driver.findElement(checkObject).getAttribute("innerText");
			 if(!we.equals(Value))
		    	throw new Exception ("The  values are not correctly displayed");
			 
		 }
		 
		 public void verifyHeaderVoice(String checkObject,String checkElement) throws Exception{
			 By value = By.xpath(checkObject.replace("$VALUE$", checkElement));	
			 String text = driver.findElement(value).getText();
			/* System.out.println(value);
				 System.out.println(text);*/
				 if(! HeaderVoice.contains(text))
					 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
		 }
		
		public void verifyFooterLink(String checkObject,String checkElement) throws Exception{
			 By value = By.xpath(checkObject.replace("$Value$", checkElement));	
			// System.out.println(value);
			 String text = driver.findElement(value).getText();
			//	 System.out.println(text);
				 if(! FooterLink.contains(text))
					 throw new Exception("Header voice is not matching with  Luce e Gas, Impresa, Insieme a Te, Storie, Futur-E, Media, Supporto"); 
		 }
		
		public void switchFrame(By frame) throws Exception{
			util.FrameSwitcher(frame);
		}
		
		public void switchToDefault(){
			driver.switchTo().defaultContent();
		}
		
		public void verifyEmailContent(String email,String emailContent) throws Exception{
			System.out.println(emailContent);
			if(!email.contains(emailContent)) 
				throw new Exception("Given String is not present in Email body");
		}
		
		public void  isElementNotPresent(By existentObject ) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			
			try{
				WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
			} 
	        catch (Exception e) {
	        	e.printStackTrace();
	            }
	         }
		 
public static final String MODULORECLAMIPAGEPATH = "Home / form-reclami";
public static final String HOME ="Home";
public static final String FORM_RECLAMI ="form-reclami";
public static final String FORM_RECLAMI_HEADING ="Modulo Reclami";
public static final String INSERICI_I_TUOI_DATI ="Inserisci i tuoi dati";
public static final String ARGOMENTO_DESCRIZIONE_DEL_RECLAMO ="Argomento e descrizione del reclamo";
public static final String VALUE[] ={"Contratti","Morosita' e sospensione","Mercato","Fatturazione","Misura","Connessioni, lavori e qualita' tecnica","Bonus sociale","Qualita' commerciale","Altro","Seleziona un argomento"};
public static final String EFFTTUAUNASELEZIONE ="Effettua una selezione";
public static final String FORMATO_NON_CORETTO ="Formato non corretto";
public static final String ERROR_MESSAGE_DESCRIZIONE="Il campo è obbligatorio";
public static final String ERROR_MESSAGE ="Il campo è obbligatorio";
public static final String CASELLA_NON_SELEZIONATA ="Casella non selezionata";
public static final String CODICE_PDR_LABEL ="Codice PDR";
public static final String CODICE_POD_LABEL ="Codice POD";
public static final String ERROR_MESSAGE_INCORRECTVALUE ="Sono ammesse solo lettere";
public static final String ERROR_MESSAGE_CODICE_FISCALE ="Il codice fiscale o la partita iva non sono validi";
public static final String ERROR_MESSAGE_FORMATO_NON_CORRETTO ="Formato non corretto";
public static final String ERROR_MESSAGE_CORRISPONDONO ="I due campi non corrispondono";
public static final String CALCOLA_CODICE_FISCALE = "Calcola il tuo Codice Fiscale";
public static final String SESSO_VALUE[] ={"Maschio","Femmina","Seleziona il sesso"};
public static final String PROVINCIA_VALUE[]={"Agrigento","Alessandria","Ancona","Aosta","Ascoli Piceno","L'Aquila","Arezzo","Asti","Avellino","Bari","Bergamo","Biella","Belluno","Benevento","Bologna","Brindisi","Brescia","Barletta-Andria-Trani","Bolzano","Cagliari","Campobasso","Caserta","Chieti","Carbonia-Iglesias","Caltanissetta","Cuneo","Como","Cremona","Cosenza","Catania","Catanzaro",
											  "Enna","Forlì-Cesena","Ferrara","Foggia","Firenze","Fermo","Frosinone","Genova","Gorizia","Grosseto","Imperia","Isernia","Crotone","Lecco","Lecce","Livorno","Lodi","Latina","Lucca","Monza e della Brianza","Macerata","Messina","Milano","Mantova","Modena","Massa-Carrara","Matera","Napoli","Novara","Nuoro","Ogliastra","Oristano","Olbia-Tempio","Palermo","Piacenza","Padova","Pescara","Perugia","Pisa","Pordenone","Prato","Parma","Pistoia","Pesaro e Urbino", "Pavia","Potenza","Ravenna","Reggio Calabria",
											  "Reggio Emilia","Ragusa","Rieti","Roma","Rimini","Rovigo","Salerno","Siena","Sondrio","La Spezia","Siracusa","Sassari","Savona","Taranto","Teramo","Trento","Torino","Trapani","Terni","Trieste","Treviso","Udine","Varese","Verbano-Cusio-Ossola","Vercelli","Venezia","Vicenza","Verona","Viterbo","Vibo Valentia",};

public static final String COMMUNE_VALUE[] ={"Acerra","Afragola","Agerola","Anacapri","Arzano","Bacoli","Barano D'Ischia","Boscoreale","Boscotrecase","Brusciano",
											"Caivano","Calvizzano","Camposano","Capri","Carbonara Di Nola","Cardito","Casalnuovo Di Napoli","Casamarciano",
											"Casamicciola Terme","Casandrino","Casavatore","Casola Di Napoli","Casoria","Castellammare Di Stabia","Castello Di Cisterna",
	"Cercola","Cicciano","Cimitile","Comiziano","Crispano","Ercolano","Forio","Frattamaggiore","Frattaminore","Giugliano In Campania","Gragnano",
	"Grumo Nevano","Ischia","Lacco Ameno","Lettere","Liveri","Marano Di Napoli","Mariglianella","Marigliano","Massa Di Somma",
	"Massa Lubrense","Melito Di Napoli","Meta","Monte Di Procida","Mugnano Di Napoli","Napoli","Nola","Ottaviano","Palma Campania",
	"Piano Di Sorrento","Pimonte","Poggiomarino","Pollena Trocchia","Pomigliano D'Arco","Pompei","Portici","Pozzuoli",
	"Procida","Qualiano","Quarto","Roccarainola","San Gennaro Vesuviano","San Giorgio A Cremano","San Giuseppe Vesuviano",
	"San Paolo Bel Sito","San Sebastiano Al Vesuvio","San Vitaliano","Sant'Agnello","Sant'Anastasia","Sant'Antimo","Sant'Antonio Abate",
	"Santa Maria La Carita'","Saviano","Scisciano","Serrara Fontana","Somma Vesuviana","Sorrento","Striano","Terzigno","Torre Annunziata",
	"Torre Del Greco","Trecase","Tufino","Vico Equense","Villaricca","Visciano","Volla"};

public String MODULORECLAIMMSG = "Grazie per averci contattato. Ti risponderemo nel più breve tempo possibile. Ti abbiamo inviato una mail con il riepilogo di quanto hai richiesto all'indirizzo di posta elettronica che hai indicato: se non la ricevi verifica nella cartella SPAM della tua casella di posta elettronica.";
public String SUPPORTOPATH = "Home/supporto/Come inviare un reclamo enel energia";
public String SUPPORTOTITLE = "Come inviare un reclamo enel energia";
public static final String HeaderVoice = "LUCE E GAS,IMPRESE,INSIEME A TE,STORIE,FUTUR-E,MEDIA,SUPPORTO";
public static final String FooterLink = "Informazioni Legali,Credits,Privacy,Cookie Policy,Remit,Preferenze Cookie";
public static final String EnelItalia = "Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.";
public static final String EnelEnergia = "Gruppo IVA Enel P.IVA 15844561009";
public static final String PreferenzeCookieContent ="Cookie e tecnologie correlate su questo sitoSi prega di scegliere se questo sito puÃ² usare cookie o tecnologie collegate come web beacon, pixel tag e Flash object (â€œCookieâ€�) come descritto qui di seguito. Ãˆ possibile ottenere ulteriori informazioni su come questo sito usa i cookie e le tecnologie collegate leggendo la nostra politica sulla privacy di cui troverete il collegamento qui di seguito.ACCETTA TUTTORifiuta Tutto Alcune delle scelte di revoca del consenso non sono state completate a causa di un errore proveniente dal fornitore, oppure perchÃ© il fornitore ha impiegato troppo tempo per rispondere. Invia preferenze Accept All Decline All";
public static final String ImpostazioniTrasmesseContent = "Le impostazioni per i cookie sono state aggiornate con successo.";
public static final String EmailContentLine1 = "Gentile Cliente,";
public static final String EmailContentLine2 = "grazie per averci contattato.";
public static final String EmailContentLine3 = "Ti risponderemo nel piÃ¹ breve tempo possibile tramite email.";
public static final String EmailContentLine4 = "Cordiali saluti.";
public static final String EmailContentLine5 = "Servizio Clienti - Enel Energia";
public static final String EmailContentLine6 = "Questo messaggio Ã¨ stato generato automaticamente, ti preghiamo di non rispondere";
public static final String EmailContentLine7 = "Hai una richiesta da  noreply.enelenergia@enel.com";
}

