package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;


public class CambioUsoComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
    
    public CambioUsoComponent (WebDriver driver) {
    	  this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  
    public static final By selectUsoFornitura = By.xpath("//span[contains(text(),'Uso Fornitura')]/following::select");
    public static final By selectCategoriaConsumo = By.xpath("//span[contains(text(),'Categoria di consumo')]/following::select");
    public static final By selectProfiloConsumo = By.xpath("//span[contains(text(),'Profilo di consumo')]/following::select");
    public static final By selectCategoriaMarketing = By.xpath("//span[contains(text(),'Categoria Marketing')]/following::select");
    public static final By selectOrdineGrandezza = By.xpath("//span[contains(text(),'Ordine di grandezza')]/following::select");
    public static final By selectPotenzialita = By.xpath("//span[contains(text(),'Potenzialit')]/following::select");
    public static final By selectUtilizzo = By.xpath("//span[contains(text(),'Utilizzo:')]/following::select");
    public static final By selectCategoriaUso = By.xpath("//span[contains(text(),'Categoria') and contains(text(),'Uso')]/following::select[1]");
    public static final By buttonAvanti = By.xpath("//button[text()='Avanti']");
    public static final By buttonConferma = By.xpath("//div[contains(@class,'CambioUso')]//button[text()='Conferma']");
	public static final By modalText = By.xpath("//div[@id='theModalCmp_B']//p");
	public static final By modalOk = By.xpath("//div[@id='theModalCmp_B']//button[text()='OK']");
	public static final By closeAllertChiaviSap = By.xpath("//button[@class='slds-button_reset slds-notify__close']//lightning-primitive-icon");
    
    public void inserisciNuoviParametriConsumoGas(String frame,String usoFornitura, String categoriaConsumo, String profiloConsumo, String categoriaMarketing, String ordineGrandezza, String potenzialita, String utilizzo, String categoriaUso) throws Exception{
		util.frameManager(frame, selectUsoFornitura, util.select, usoFornitura);
		util.frameManager(frame, selectCategoriaConsumo, util.select, categoriaConsumo);
		util.frameManager(frame, selectProfiloConsumo, util.select, profiloConsumo);
		util.frameManager(frame, selectCategoriaMarketing, util.select, categoriaMarketing);
		util.frameManager(frame, selectOrdineGrandezza, util.select, ordineGrandezza);
		util.frameManager(frame, selectPotenzialita, util.select, potenzialita);
		util.frameManager(frame, selectUtilizzo, util.select, utilizzo);
		util.frameManager(frame, selectCategoriaUso, util.select, categoriaUso);
		util.frameManager(frame, buttonAvanti, util.scrollAndClick);
		spinner.checkSpinners(frame);
		TimeUnit.SECONDS.sleep(5);
		spinner.checkSpinners(frame);
	}
    
    
    public void inserisciNuoviParametriConsumoGasUsoNonAbitativo(String frame,String usoFornitura, String categoriaConsumo, String profiloConsumo, String categoriaMarketing, String ordineGrandezza, String utilizzo, String categoriaUso) throws Exception{
		util.frameManager(frame, selectUsoFornitura, util.select, usoFornitura);
		util.frameManager(frame, selectCategoriaConsumo, util.select, categoriaConsumo);
		util.frameManager(frame, selectProfiloConsumo, util.select, profiloConsumo);
		util.frameManager(frame, selectCategoriaMarketing, util.select, categoriaMarketing);
		util.frameManager(frame, selectOrdineGrandezza, util.select, ordineGrandezza);		
		util.frameManager(frame, selectUtilizzo, util.select, utilizzo);
		util.frameManager(frame, selectCategoriaUso, util.select, categoriaUso);
		util.frameManager(frame, buttonAvanti, util.scrollAndClick);
		spinner.checkSpinners(frame);
		TimeUnit.SECONDS.sleep(5);
		spinner.checkSpinners(frame);
	}
    
    
    public void inserisciNuoviParametriConsumoEle(String frame,String usoFornitura) throws Exception{
  		util.frameManager(frame, selectUsoFornitura, util.select, usoFornitura);
  		util.frameManager(frame, buttonAvanti, util.scrollAndClick);
  		spinner.checkSpinners(frame);
  		TimeUnit.SECONDS.sleep(5);
  		spinner.checkSpinners(frame);
  	}
    
	public void switchFrame() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.switchToFrame(frame);
	}
    
    public void press(By object) throws Exception{
    	util.objectManager(object, util.scrollAndClick);
    	spinner.checkSpinners();
    }
    
    public void checkModalText(By modal,String text) throws Exception{
    	if(!util.verifyExistence(modal, 10)) throw new Exception("Dopo aver confermato il cambio uso non viene mostrata a video la modale con messaggio: "+text);
        String textModal = util.waitAndGetElement(modal,false).getText();
        if(!text.contentEquals(textModal)) throw new Exception("Dopo aver confermato il cambio uso viene mostrata una modale ma non contiene il testo : "+text);
    }
    
    public void checkModalTextAndPressOk(By modal,String text) throws Exception{
    	if(!util.verifyExistence(modal, 10)) throw new Exception("Dopo aver confermato il cambio uso non viene mostrata a video la modale con messaggio: "+text);
        String textModal = util.waitAndGetElement(modal,false).getText();
        if(!text.contentEquals(textModal)) throw new Exception("Dopo aver confermato il cambio uso viene mostrata una modale ma non contiene il testo : "+text);
        if(!util.verifyExistence(modalOk, 10)) throw new Exception("Dopo aver confermato il cambio uso nella modale non compare il pulsante OK");
        util.objectManager(modalOk,util.scrollAndClick);
    }

    
    public void checkandclickAlertSap() throws Exception{
    	if( util.verifyExistence(closeAllertChiaviSap, 5))
    		  util.objectManager(closeAllertChiaviSap,util.click);
    }
}