package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SezioneSelezioneFornituraComponent extends BaseComponent {
    WebDriver driver;
    SeleniumUtilities util;
    public By sezioneSelezioneFornitura = By.xpath("//h2/span[text()='Selezione Fornitura']");
    public By sezioneSelezioneFornituraSdd = By.xpath("//h2/span[text()='Selezione forniture']"); //FR R3 02.08.2021
    public By messaggioMoroso = By.xpath("//div[@class='slds-notify slds-notify_alert slds-theme_alert-texture slds-notify_container slds-theme_error']/h2[text()='Non è possibile selezionare contemporaneamente forniture morose e non morose']");
    public By campoCercaFornitura = By.xpath("//label[text()='Cerca Fornitura']/ancestor::div[1]//input");
    public By campoCercaFornituraSdd=By.xpath("//span[text()='Selezione forniture']/ancestor::div//div[lightning-icon/lightning-primitive-icon]/input");
    public By campoRicerca = By.xpath("//input[contains(@id,'Ricerca')]");
    public By campoRicerca1 = By.xpath("//input[contains(@data-id,'input')]");
    public By buttonConfermaSelezioneFornitura = By.xpath("//button[text()='Conferma Selezione Fornitura' and @disabled]");
    public By buttonConfermaSelezioneFornituraAbilitato = By.xpath("//button[text()='Conferma Selezione Fornitura']");
    public String colonne[] = {"COMMODITY", "STATO", "NUMERO CLIENTE", "POD/PDR", "INDIRIZZO FORNITURA", "ATTIVABILITÀ", "OFFERTABILITÀ", "POD MOROSO"};
    public String colonneAttivazioneSDD[] = {"COMMODITY", "STATO", "NUMERO CLIENTE", "POD/PDR", "INDIRIZZO DI FORNITURA", "MODALITÀ DI PAGAMENTO", "ESITO COMPATIBILITÀ"};
    public String colonneVariazioneSDD[] = {"COMMODITY", "STATO", "NUMERO CLIENTE", "POD/PDR", "INDIRIZZO DI FORNITURA", "MODALITÀ DI PAGAMENTO", "ESITO COMPATIBILITÀ"};
    public String colonneRic[] = {"COMMODITY", "STATO", "NUMERO UTENZA", "POD", "INDIRIZZO DI FORNITURA", "USO FORNITURA", "MERCATO DI RIFERIMENTO", "PRODOTTO", "ATTIVABILITÀ", "OFFERTABILITÀ"};
    public String colonne_copia_doc[] = {"COMMODITY", "STATO", "NUMERO UTENTE", "POD/PDR", "INDIRIZZO FORNITURA"};
    public String colonne_kit_contruattuale[] = {"Id Documento", "Numero Offerta", "Prodotto", "Modello", "Data Invio Documento", "POD/PDR", "Processo"};
    public By radioButtonCopiaDocumento = By.xpath("//h2[text()='Selezione Fornitura']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-radio_faux']");
    public By radioButtonKitContrattuale = By.xpath("//span[text()='Kit Contrattuale Padre']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-radio_faux']");
    public By buttonConfermaKitContrattuale = By.xpath("//span[text()='Kit Contrattuale Padre']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By buttonConfermaKitContrattualeFiglio = By.xpath("//span[text()='Kit Contrattuale']//ancestor::div[@class='slds-card']//button[@name='Conferma' and text()='Conferma']");
    public By radioButtonKitContrattualeFiglio = By.xpath("//span[text()='Kit Contrattuale']//ancestor::div[@class='slds-card']//table[@role='table']/tbody//span[@class='slds-checkbox_faux']");
    public By pagina = By.xpath("//label[normalize-space(text())='Pagina']");
    public By buttonConferma = By.xpath("//button[@name='Conferma' and text()='Conferma']");
    public By buttonCreaOfferta = By.xpath("//div[@class='slds-p-top_small']//button[text()='Crea offerta']");
    public By popupSi = By.xpath("//h2[text()='Proposta Voltura Dual']/ancestor::div[@data-id='modal']//p[text()=\"È possibile eseguire una voltura Dual selezionando l'altra fornitura avente lo stesso indirizzo. Il cliente ha accettato?\"]/ancestor::div[@data-id='modal']//footer//button[@name='Sì' and text()='Sì']");
    //public By messaggioErroreSelFornitura = By.xpath("//div[@role='alert']//p[1]");
    public By messaggioErroreSelFornitura = By.xpath("//p[text()='Attenzione! Banner di errore']/parent::div/h2");
  
    public By messaggioErroreSelFornituraNonAttivabile = By.xpath("//h2[@data-title='0']");  
    public String messaggioErroreTestoAtteso = "E' impossibile procedere con l'inserimento della VCA per limite massimo siti volturabili superato. Rivolgiti al tuo TL che provvederà ad assegnare la gestione della richiesta al Team dedicato.";
    public String messaggioAlertTestoAttesoFornNonAttivabile = "Attenzione! E' stata trovata una fornitura non attivabile, avendo l'obbigo di volturare tutte le forniture non sarà possible continuare con la voltura.";
    public By sezioneMessaggioFornitura = By.xpath("//p[text()='Si prega di ricordare al cliente che si può usare la sezione self-care del web']");
    public By sezioneTipologiaDoc = By.xpath("//span[text()='Tipologie Documenti']");
    public By buttonKitContrattuale = By.xpath("//div[@data-label='contractKitParent']//span[@class='slds-checkbox_faux']");
    public By buttonFatture = By.xpath("//div[@data-label='factures']//span[@class='slds-checkbox_faux']");
    public By buttonPreventivi = By.xpath("//div[@data-label='quotes']//span[@class='slds-checkbox_faux']");
    public By buttonEstrattoConto = By.xpath("//div[@data-label='statements']//span[@class='slds-checkbox_faux']");
    public By buttonPianoDiRientro = By.xpath("//div[@data-label='repaymentPlans']//span[@class='slds-checkbox_faux']");
    public By buttonInbound = By.xpath("//div[@data-label='inbounds']//span[@class='slds-checkbox_faux']");
    public By campoRicercaKitContruattuale = By.xpath("//span[text()='Kit Contrattuale Padre']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public By campoRicercaKitContr = By.xpath("//span[text()='Kit Contrattuale']//ancestor::div[@role='region']//label[text()='Ricerca']/..//input");
    public By buttonOutbound = By.xpath("//div[@data-label='outbounds']//span[@class='slds-checkbox_faux']");
    public By buttonDimPagamento = By.xpath("//div[@data-label='demoPayments']//span[@class='slds-checkbox_faux']");
    public By buttonAnnulla = By.xpath("//button[@name='Cancel']");
    public By popUpAnnullaTitle = By.xpath("//h2[text()='Annulla']");
    public By popUpAnnullaTesto = By.xpath("//h2[text()='Annulla']/ancestor::div[2]//p");
    public By popUpAnnullaButtonConferma = By.xpath("//h2[text()='Annulla']/ancestor::div[2]//button[text()='Conferma']");
    public String popUpAnnullaTestoAtteso = "Confermi di voler annullare il Caso?";
    public By SelectAllFatture = By.xpath("//span[@class='slds-checkbox_faux']");
    public By popUpErrore = By.xpath("//h2[text()='Numero massimo di documenti raggiunto. Non puoi selezionare altri documenti.']");
    public String popUpErroreTestoAtteso = "Numero massimo di documenti raggiunto. Non puoi selezionare altri documenti.";
    public By popUpErroreTesto = By.xpath("//h2[text()='Numero massimo di documenti raggiunto. Non puoi selezionare altri documenti.']");
    public By tableBy = new By.ByXPath("//table[@role='table']");
    




    public SezioneSelezioneFornituraComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);

    }

    public void verifyComponentExistence(By existingObject) throws Exception {
        if (!util.exists(existingObject, 30))
            throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
    }

    public void scrollComponent(By object) throws Exception {
        util.objectManager(object, util.scrollToVisibility, false);
    }

    public void clickComponent(By clickableObject) throws Exception {
        util.objectManager(clickableObject, util.scrollToVisibility, false);
        Thread.sleep(5000);
        util.objectManager(clickableObject, util.scrollAndClick);

    }

    public void enterPodValue(By insertObject, String valore) throws Exception {
        util.objectManager(insertObject, util.sendKeys, valore);
    }

    
    /**
     * Serve a verificare l'esistenza delle colonne nella sezione selezione fornitura per il processo di attivazione SDD
     * @param col
     * @throws Exception
     */
    public void VerificaColonneTabellaFornitureAttivazioneSDD(String[] col) throws Exception
    {
    	String xpath="//span[text()='Selezione forniture']/ancestor::div//table[@id='ITA_IFM_ATTIVAZIONESDD_Table']/thead//th";
    	List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath(xpath));
    	 if (nomeColonne.size() == col.length+1) {//è stato aggiunto più uno perchè esiste anche la colonna del checkbox
             for (int i = 1; i <= col.length; i++) {
                 WebElement c = driver.findElement(By.xpath("//table[@id='ITA_IFM_ATTIVAZIONESDD_Table']/thead/tr/th[" + (i + 1) + "]/div/span"));
                 String columnName = c.getText();
                 System.out.println(columnName);
                 System.out.println(col[i-1].trim());
                 if (!columnName.contentEquals(col[i - 1].trim()))
                     throw new Exception("la tabella forniture non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
             }
         }
    }
    
    public void VerificaColonneTabellaFornitureSDD(String[] col,String idtabella) throws Exception
    {
    	String xpath="//span[text()='Selezione forniture']/ancestor::div//table[@id='@idtabella@']/thead//th".replace("@idtabella@", idtabella);
    	List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath(xpath));
    	 if (nomeColonne.size() == col.length+1) {//è stato aggiunto più uno perchè esiste anche la colonna del checkbox
             for (int i = 1; i <= col.length; i++) {
                 WebElement c = driver.findElement(By.xpath("//table[@id='ITA_IFM_VARIAZIONESDD_Table']/thead/tr/th[" + (i + 1) + "]/div/span"));
                 String columnName = c.getText();
                 System.out.println(columnName);
                 System.out.println(col[i-1].trim());
                 if (!columnName.contentEquals(col[i - 1].trim()))
                     throw new Exception("la tabella forniture non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
             }
         }
    }
    
    public void verificaColonneTabella(String[] col) throws Exception {
        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//table[@id='supplyComponent_Table']/thead/tr/th/div/span"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//table[@id='supplyComponent_Table']/thead/tr/th[" + (i + 1) + "]/div/span"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella forniture non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }
    }
/**
 * R3 FR
 * @param pod
 * @throws Exception
 */
    public void selezionaFornituraAttivazioneSdd(String pod) throws Exception {
       Thread.sleep(15000);
        int r = 0;
        String found = "NO";
//			List<WebElement> listaTR = driver.findElements(By.xpath("//table[@id='supplyComponent_Table' or @id='VCASupplySelectionComponent_Table']/tbody/tr"));
        List<WebElement> listaTR = driver.findElements(By.xpath("//table[@id='ITA_IFM_ATTIVAZIONESDD_Table']//tbody/tr"));

        if (listaTR.size() < 1)
            throw new Exception("la tabella con le informazioni della fornitura non contiene righe");
        for (WebElement li : listaTR) {
            List<WebElement> listaTD = li.findElements(By.xpath("td"));
            for (WebElement i : listaTD) {
                String codf = i.getText();
                r = listaTR.indexOf(li);
                if (codf.compareToIgnoreCase(pod) == 0 && listaTR.indexOf(li) == r) {
                    try {
                    	
                        listaTD.get(0).click();
                        System.out.println("POD SELEZIONATO");
                    } catch (Exception e) {
                        System.out.println("Error checkbox");
                    }
                    found = "SI";
                    break;
                } else
                    found = "NO";


            }

            if (found.contentEquals("NO"))
                throw new Exception("e' stato impossibile selezionare il record nella tabella forniture");

        }


    }
    
    public void selezionaFornituraSDD(String pod,String idtabella) throws Exception {
        Thread.sleep(15000);
         int r = 0;
         String found = "NO";
     	String xpath="//table[@id='@idtabella@']//tbody/tr".replace("@idtabella@", idtabella);
         List<WebElement> listaTR = driver.findElements(By.xpath(xpath));
         if (listaTR.size() < 1)
             throw new Exception("la tabella con le informazioni della fornitura non contiene righe");
         for (WebElement li : listaTR) {
             List<WebElement> listaTD = li.findElements(By.xpath("td"));
             for (WebElement i : listaTD) {
                 String codf = i.getText();
                 r = listaTR.indexOf(li);
                 if (codf.compareToIgnoreCase(pod) == 0 && listaTR.indexOf(li) == r) {
                     try {
                     	
                         listaTD.get(0).click();
                         System.out.println("POD SELEZIONATO");
                     } catch (Exception e) {
                         System.out.println("Error checkbox");
                     }
                     found = "SI";
                     break;
                 } else
                     found = "NO";


             }

             if (found.contentEquals("NO"))
                 throw new Exception("e' stato impossibile selezionare il record nella tabella forniture");

         }


     }
    

    public void selezionaRecordInTabella(String pod) throws Exception {
        TimeUnit.SECONDS.sleep(10);
        int r = 0;
        String found = "NO";
//			List<WebElement> listaTR = driver.findElements(By.xpath("//table[@id='supplyComponent_Table' or @id='VCASupplySelectionComponent_Table']/tbody/tr"));
        List<WebElement> listaTR = driver.findElements(By.xpath("//*[text()='Selezione Fornitura']/ancestor::div[@role='region']//table/tbody/tr"));

        if (listaTR.size() < 1)
            throw new Exception("la tabella con le informazioni della fornitura non contiene righe");
        for (WebElement li : listaTR) {
            List<WebElement> listaTD = li.findElements(By.xpath("td"));
            for (WebElement i : listaTD) {
                String codf = i.getText();
                r = listaTR.indexOf(li);
                if (codf.compareToIgnoreCase(pod) == 0 && listaTR.indexOf(li) == r) {
                    try {
                        listaTD.get(0).click();
                    } catch (Exception e) {
                        System.out.println("Error checkbox");
                    }
                    found = "SI";
                    break;
                } else
                    found = "NO";


            }

            if (found.contentEquals("NO"))
                throw new Exception("e' stato impossibile selezionare il record nella tabella forniture");

        }


    }
    
    public void ControlloPrimoRecordInTabella(ArrayList<String> campi ) throws Exception {
        TimeUnit.SECONDS.sleep(2);

        WebElement listaTR = driver.findElement(By.xpath("//*[text()='Selezione Fornitura']/ancestor::div[@role='region']//table/tbody/tr"));

        ArrayList<String> arr = new ArrayList<String>();
       
            List<WebElement> listaTD = listaTR.findElements(By.xpath("td"));
            for (WebElement i : listaTD) {
                String codf = i.getText();
                
                System.out.println(codf);
                if(!codf.contentEquals(""))arr.add(codf);


            }
            
            if(!campi.equals(arr)) {
            	 throw new Exception("Trovati valori non attesi in tabella:"+String.join(",", campi) );
            }

    }

    public void selezionaTuttiRecordInTabella() throws Exception {
        TimeUnit.SECONDS.sleep(3);
        int r = 0;
        String found = "NO";
        List<WebElement> listaTR = driver.findElements(By.xpath("//table[@id='supplyComponent_Table']/tbody/tr"));

        if (listaTR.size() < 1)
            throw new Exception("la tabella con le informazioni della fornitura non contiene righe");
        for (WebElement li : listaTR) {
            List<WebElement> listaTD = li.findElements(By.xpath("td//input/.."));
            for (WebElement i : listaTD) {

                try {
                    i.click();
                    TimeUnit.SECONDS.sleep(13);
                } catch (Exception e) {

                    e.printStackTrace();
                }


            }


        }


    }
    
  
  
    public void selezionaTuttiRecordInTabellaNonResidenziale() throws Exception {
        TimeUnit.SECONDS.sleep(3);
        //int r = 1;
        List<WebElement> listaTR = driver.findElements(By.xpath("//table[@title='tabella delle intestazioni ordinabili e dei valori cliccabili']/tbody/tr"));

        if (listaTR.size() < 1)
            throw new Exception("la tabella con le informazioni della intestazioni ordinabili e dei valori cliccabili non contiene righe");

       
        for (int i=1; i<listaTR.size()+1; i++) {
        	 driver.findElement(By.xpath("//table[@title='tabella delle intestazioni ordinabili e dei valori cliccabili']/tbody/tr["+i+"]//span[@class='slds-checkbox']")).click();
             TimeUnit.SECONDS.sleep(13);
        	
        }
      //  driver.findElement(By.xpath("(//table[@id='supplyComponent_Table']/tbody/tr/td//input/..)[2]")).click();
       // TimeUnit.SECONDS.sleep(3);
    }
    
    public void selezionaTuttiRecordInTabellaMoroso() throws Exception {
        TimeUnit.SECONDS.sleep(3);
        int r = 0;
        List<WebElement> listaTR = driver.findElements(By.xpath("//table[@id='supplyComponent_Table']/tbody/tr"));

        if (listaTR.size() < 1)
            throw new Exception("la tabella con le informazioni della fornitura non contiene righe");

        driver.findElement(By.xpath("(//table[@id='supplyComponent_Table']/tbody/tr/td//input/..)[1]")).click();
        TimeUnit.SECONDS.sleep(13);
        driver.findElement(By.xpath("(//table[@id='supplyComponent_Table']/tbody/tr/td//input/..)[2]")).click();
        TimeUnit.SECONDS.sleep(3);
    }

    public void checkValoreInTable(String attivabilita) throws Exception {
        WebElement table = driver.findElement(By.xpath("//*[text()='Selezione Fornitura']/ancestor::div[@role='region']//table"));
        TimeUnit.SECONDS.sleep(2);
        String valore = util.getTableCellDataWithoutDiv(table, 1, "ATTIVABILITÀ", "span/div/div/span/span").trim();
        //System.out.println(valore);
        if (!valore.contentEquals(attivabilita))
            throw new Exception("nella tabella forniture in corrispondenza della colonna ATTIVABILITA' non e' presente il valore: " + attivabilita + "; ma il valore " + valore);

    }
    
    public void checkEsitCompatibilitaSDD(String attivabilita) throws Exception {
        WebElement table = driver.findElement(By.xpath("//table[@id='ITA_IFM_ATTIVAZIONESDD_Table']"));
        TimeUnit.SECONDS.sleep(2);
        String valore = util.getTableCellDataWithoutDiv(table, 1, "ESITO COMPATIBILITÀ", "span/div/div/span/span").trim();
        
       
        
        
        
        if (!valore.contentEquals(attivabilita))
            throw new Exception("nella tabella forniture in corrispondenza della colonna ATTIVABILITA' non e' presente il valore: " + attivabilita + "; ma il valore " + valore);

    }
    
    public void checkEsitCompatibilitaVariazioneSDD(String attivabilita,String idtabella) throws Exception {
    	String xpath="//table[@id='@idtabella@']".replace("@idtabella@", idtabella);
        WebElement table = driver.findElement(By.xpath(xpath));
        TimeUnit.SECONDS.sleep(2);
        String valore = util.getTableCellDataWithoutDiv(table, 1, "ESITO COMPATIBILITÀ", "span/div/div/span/span").trim();

        
        if (!valore.contentEquals(attivabilita))
            throw new Exception("nella tabella forniture in corrispondenza della colonna ATTIVABILITA' non e' presente il valore: " + attivabilita + "; ma il valore " + valore);

    }

    public void checkValoreInTableMoroso(String moroso, int riga) throws Exception {
        WebElement table = driver.findElement(By.xpath("//table[@id='supplyComponent_Table' or @id='VCASupplySelectionComponent_Table']"));
        TimeUnit.SECONDS.sleep(2);
        String valore = util.getTableCellDataWithoutDiv(table, riga, "POD MOROSO", "span/div/div/span/span").trim();
        //System.out.println(valore);
        if (!valore.contentEquals(moroso))
            throw new Exception("nella tabella forniture in corrispondenza della colonna POD MOROSO' non e' presente il valore: " + moroso + "; ma il valore " + valore);

    }

    public boolean clickPopupIfExist(By oggettocliccabile) throws Exception {
        if (this.getElementExistance(oggettocliccabile)) {
            clickComponent(oggettocliccabile);

            verifyComponentExistence(buttonConfermaSelezioneFornituraAbilitato);
            clickComponent(buttonConfermaSelezioneFornituraAbilitato);

            TimeUnit.SECONDS.sleep(1);
            verifyComponentExistence(buttonConferma);
            clickComponent(buttonConferma);

            TimeUnit.SECONDS.sleep(1);
            verifyComponentExistence(buttonCreaOfferta);
            clickComponent(buttonCreaOfferta);
            return true;
        }
        ;
        return false;
    }


    public void verificaMessaggioErroreSelezioneFornitura(By obj, String popupTestoAtteso) throws Exception {
        if (!util.verifyExistence(obj, 60))
            throw new Exception("Non è stata visualizzata la pop up con testo: " + popupTestoAtteso);
        String textModal = util.waitAndGetElement(obj, false).getText();
        if (textModal.compareTo(popupTestoAtteso) != 0)
            throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: " + textModal + " testo atteso:" + popupTestoAtteso);
    }

    public void verificaColonneTabellaSelezioneFornitura(String[] col) throws Exception {
        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Selezione Fornitura']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Selezione Fornitura']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Selezione forniture non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }
    }

    public void verificaColonneTabellaKitContrattualePadre(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Kit Contrattuale Padre']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Kit Contrattuale Padre']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Kit Contrattuale Padre non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaSezioneInbound(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Inbound']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Inbound']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Inbound non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaSezioneOutbound(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Outbound']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Outbound']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Outbound non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaSezioneDimostratoPagamento(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Dimostrato Pagamento']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Dimostrato Pagamento']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Dimostrato Pagamento non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaSezioneFatture(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Fatture']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Fatture']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Fatture non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaSezionePianoDiRientro(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Piano Di Rientro']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Piano Di Rientro']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Piano Di Rientro non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaSezioneEstrattoConto(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Estratto Conto']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Estratto Conto']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Estratto Conto non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void verificaColonneTabellaKitContrattuale(String[] col) throws Exception {

        List<WebElement> nomeColonne = util.waitAndGetElements(By.xpath("//h2[text()='Kit Contrattuale']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th/div/div/div/div[1]"));
        if (nomeColonne.size() == col.length) {
            for (int i = 1; i <= col.length; i++) {
                WebElement c = driver.findElement(By.xpath("//h2[text()='Kit Contrattuale']//ancestor::div[@class='slds-card']//table[@role='table']/thead/tr/th[" + (i + 1) + "]/div/div/div/div[1]"));
                String columnName = c.getText();
                //System.out.println(columnName);
                //System.out.println(col[i-1].trim());
                if (!columnName.contentEquals(col[i - 1].trim()))
                    throw new Exception("la tabella Kit Contrattuale non contiene le colonne con descrizione: " + col[i] + "; il nome delle colonne visualizzato e' " + columnName);
            }
        }


    }

    public void setTipologiaDoc(String valore) throws Exception {

        if (valore.equals("KIT_CONTRUATTUALE")) {
            press(buttonKitContrattuale);
        } else if (valore.equals("INBOUND")) {
            press(buttonInbound);
        } else if (valore.equals("OUTOUND")) {
            press(buttonOutbound);
        } else if (valore.equals("DIMOSTRATO_PAGAMENTO")) {
            press(buttonDimPagamento);
        } else if (valore.equals("FATTURE")) {
            press(buttonFatture);
        } else if (valore.equals("PIANO_DI_RIENTRO")) {
            press(buttonPianoDiRientro);
        } else if (valore.equals("PREVENTIVI")) {
            press(buttonPreventivi);
        } else if (valore.equals("ESTRATTO_CONTO")) {
            press(buttonEstrattoConto);
        } else {
            throw new Exception("Non e' presente nessuna tipologia di documenti da selezionare");
        }
        ;
    }


    public void verificaPopUpError(By obj, By objText, String popupTestoAtteso) throws Exception {
        if (!util.verifyExistence(obj, 60))
            throw new Exception("Non è stata visualizzata la pop up con testo: " + popupTestoAtteso);
        String textModal = util.waitAndGetElement(objText, false).getText();
        if (textModal.compareTo(popupTestoAtteso) != 0)
            throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: " + textModal + " testo atteso:" + popupTestoAtteso);

    }

    public void verificaPopUpconTesto(By obj, By objText, By button, String popupTestoAtteso) throws Exception {
        if (!util.verifyExistence(obj, 60))
            throw new Exception("Non è stata visualizzata la pop up con testo: " + popupTestoAtteso);
        String textModal = util.waitAndGetElement(objText, false).getText();
//		System.out.println("Messaggio visualizzato	:"+textModal);
//		System.out.println("Messaggio atteso		:"+popupTestoAtteso);
        if (textModal.compareTo(popupTestoAtteso) != 0)
            throw new Exception("Il testo della pop-up non è quello atteso. Testo visualizzato: " + textModal + " testo atteso:" + popupTestoAtteso);
        util.objectManager(button, util.scrollAndClick);

    }


}
