package com.nttdata.qa.enel.components.lightning;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class AttivazioneSDDCreaNuovoMetodoDiPagamento extends BaseComponent {

	
	
	
    private WebDriver driver;
    Logger LOGGER = Logger.getLogger(this.getClass().toString());
	
	
	public AttivazioneSDDCreaNuovoMetodoDiPagamento(WebDriver driver) {
		this.driver=driver;
		this.util=new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
		
	}
	
	
	//SEZIONE CREAZIONE NUOVO METODO PAGAMENTO
	public final  By bottoneNuovoMetodoPagamento=By.xpath("//span[text()='Metodo di pagamento']/ancestor::div//button[text()='Nuovo']");
	public final By bottoneCopiaIntestatarioContratto=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//button[@title='Copia da Intestatario Contratto']");
	public final By textBoxNome=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//input[contains(@id,'Nome')]");
	public final By textBoxCognome=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//input[contains(@id,'Cognome')]");
	public final By textBoxRagioneSociale=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//input[contains(@id,'Sociale')]");
	public final By textBoxCF=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//input[contains(@id,'Fiscale')]");
	public final By textBoxIban=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//input[contains(@id,'IBAN') and @type='text']");
	public final By chexBoxIbanEstero=By.xpath("//label[contains(@for,'ITA_IFM_Flag_Foreign_IBAN')]/span[@class='slds-checkbox_faux']");
	public final By bottoneSalva=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//button[@name='Salva']");
	public final By bottoneAnnulla=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']/parent::div/parent::div//button[@name='Annulla']");
	public final By labelMessaggioValidazioneIban=By.xpath("//label[contains(text(),'IBAN')]/parent::div/h6");
	
	
	public final By labelNome=By.xpath("//label[@data-label='NE__First_Name__c']");
	
	//label[contains(text(),'Nome')]/parent::div/h6[text()='Questo campo è obbligatorio']
	//LABEL VALIDAZIONE VERIFICA OBBLIGATORIETA CAMPI
	public final By msgValidazioneNome=By.xpath("//label[contains(text(),'Nome')]/parent::div/h6[text()='Questo campo è obbligatorio']");
	public final By msgValidazioneCognome=By.xpath("//label[contains(text(),'Cognome')]/parent::div/h6[text()='Questo campo è obbligatorio']");
    public final By msgValidazioneCF=By.xpath("//label[contains(text(),'Codice Fiscale')]/parent::div/h6[text()='Questo campo è obbligatorio']");
	public final By msgValidazioneIban=By.xpath("//label[contains(text(),'IBAN')]/parent::div/h6[text()='Questo campo è obbligatorio']");
	
	public void VerificaObbligatorietaCampi(boolean isBusiness) throws Exception
	{
		
		driver.findElement(bottoneNuovoMetodoPagamento).click();
		
		if(isBusiness)
		{
			
		}
		else
		{
			//NOME
			driver.findElement(textBoxNome).sendKeys("");
			driver.findElement(labelNome).click();
			Thread.sleep(2000);
		    if(!util.exists(msgValidazioneNome,10))
		    {
		    	throw new Exception("Errore - non viene mostrato il messaggio di validazione per il campo NOME");
		    }
			Thread.sleep(2000);
			
		    //COGNOME
		    driver.findElement(textBoxCognome).sendKeys("");
		    driver.findElement(labelNome).click();
			Thread.sleep(2000);
		    if(!util.exists(msgValidazioneCognome,10))
		    {
		    	throw new Exception("Errore - non viene mostrato il messaggio di validazione per il campo COGNOME");
		    }
			Thread.sleep(2000);
			
			
		    //CF
		    driver.findElement(textBoxCF).sendKeys("");
		    driver.findElement(labelNome).click();
			Thread.sleep(2000);
		    if(!util.exists(msgValidazioneCF,10))
		    {
		    	throw new Exception("Errore - non viene mostrato il messaggio di validazione per il campo CF");
		    }
			Thread.sleep(2000);
		    
		    
		    //IBAN 
		    driver.findElement(textBoxIban).sendKeys("");
		    driver.findElement(labelNome).click();
			Thread.sleep(2000);
		    if(!util.exists(msgValidazioneIban,10))
		    {
		    	throw new Exception("Errore - non viene mostrato il messaggio di validazione per il campo CF");
		    }
			Thread.sleep(2000);
			
			
		    //CLICCO SU ANNULLA
		    driver.findElement(bottoneAnnulla).click();
		}
		//
		
	}
	
	
	public void CompilaFormNuovoRID_Res(String nome,String cognome,String CF, String IBAN, boolean isIbanEstero) throws Exception {
		driver.findElement(bottoneNuovoMetodoPagamento).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(textBoxNome).sendKeys(nome);
		driver.findElement(textBoxCognome).sendKeys(cognome);
		driver.findElement(textBoxCF).sendKeys(CF);
		TimeUnit.SECONDS.sleep(1);
		if(isIbanEstero)
		{
			driver.findElement(chexBoxIbanEstero).click();
		}
		driver.findElement(textBoxIban).sendKeys(IBAN);
		TimeUnit.SECONDS.sleep(1);
		//driver.findElement(bottoneSalva).click();
	}
	public void CompilaFormNuovoRID_Bsn(String nome,String ragioneSociale,String CF, String IBAN, boolean isIbanEstero) throws Exception {
		driver.findElement(bottoneNuovoMetodoPagamento).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(textBoxNome).sendKeys(nome);
		driver.findElement(textBoxRagioneSociale).sendKeys(ragioneSociale);
		driver.findElement(textBoxCF).sendKeys(CF);
		TimeUnit.SECONDS.sleep(1);
		if(isIbanEstero)
		{
			driver.findElement(chexBoxIbanEstero).click();
		}
		driver.findElement(textBoxIban).sendKeys(IBAN);
		
		TimeUnit.SECONDS.sleep(1);
		//driver.findElement(bottoneSalva).click();
	}
	
	public void SalvaNuovoRid()
	{
		driver.findElement(bottoneSalva).click();
	}
	
	public void AnnullaCreazioneNuovoRid()
	{
		driver.findElement(bottoneAnnulla).click();
	}
	
	public void VerificaMessaggioValidazioneIban(String expectedMessage) throws Exception
	{
		//label[contains(text(),'IBAN')]/parent::div/h6
		WebElement messaggioValidazione=driver.findElement(labelMessaggioValidazioneIban);
		String msg=messaggioValidazione.getText();
		LOGGER.info(String.format("Verifica Messaggio validazione IBAN  OK - Messaggio: %s Atteso: %s ",msg,expectedMessage));
		if(!expectedMessage.contentEquals(msg))
		{
			throw new Exception(String.format("Messaggio  validazione IBAN diverso da valore atteso!!!  - Messaggio: %s Atteso: %s ",msg,expectedMessage));
		}
		else
		{
			
		}
	}
	
	
	public void ClicNuovoECompila(String IBAN, boolean isIbanEstero) throws Exception {
		
		driver.findElement(bottoneNuovoMetodoPagamento).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(bottoneCopiaIntestatarioContratto).click();
		TimeUnit.SECONDS.sleep(1);
		if(isIbanEstero)
		{
			driver.findElement(chexBoxIbanEstero).click();
		}
		driver.findElement(textBoxIban).sendKeys(IBAN);
		TimeUnit.SECONDS.sleep(1);
		//driver.findElement(bottoneSalva).click();
	}
	
	/**
	 * 
	 * @param IbanEx
	 * @param nomeEx
	 * @param cognomeEx
	 * @param CFEx
	 * @param bancaEx
	 * @param AgenziaEx
	 * @param statoEx
	 * @throws Exception
	 */
	public void VerificaCreazioneNuovoMetodoDiPagamento(String IbanEx, String nomeEx, String cognomeEx,String CFEx, String bancaEx, String AgenziaEx, String statoEx, boolean isBusiness) throws Exception
	{
		WebElement table=driver.findElement(By.xpath("//div[div[header[div/h2[span[text()='Metodo di pagamento']]]]]//table"));
		HashMap<String, Integer> columnsIndex=new HashMap<String, Integer>();
		TimeUnit.SECONDS.sleep(5);
		//Verifica intestazione
		String[] expectedColumns=null;
		List<WebElement> colonneIntestazione=table.findElements(By.xpath("thead/tr/th/div/div/div/div[@class='slds-assistive-text']"));
		if(isBusiness)
		{
			expectedColumns= new String[] {"Stato", "Nome", "Rag.Sociale", "Codice Fiscale", "Iban", "Banca", "Agenzia"};
		}
		else
		{
			expectedColumns= new String[] {"Stato", "Nome", "Cognome", "Codice Fiscale", "Iban", "Banca", "Agenzia"};
		}
		
		for(int i=0;i<colonneIntestazione.size();i++)
		{
			String expected=expectedColumns[i];
			String actual=colonneIntestazione.get(i).getText();
			System.out.println(String.format("Verifica Colonne - Actual: %s - Expected: %s",actual, expected));
			if(!actual.contentEquals(expected))
			{
				throw new Exception("per la colonna xx  - intestazione diversa da valore atteso");
			}
			
			columnsIndex.put(actual, i);
		}
		
		
		String xpath="//div[div[header[div/h2[span[text()='Metodo di pagamento']]]]]//input[contains(@id,'Ricerca')]";
		util.objectManager(By.xpath(xpath),util.scrollAndSendKeys, IbanEx);
		
		String iban="";
		String nome="";
		String Cognome="";
		String stato="";
		String codiceFiscale="";
		String Banca="";
		String Agenzia="";
		List<WebElement> rows=table.findElements(By.xpath("tbody/tr"));
		boolean newPaymentMethodCreated=false;
		for(int i=0;i<rows.size();i++)
		{
			List<WebElement> values=rows.get(i).findElements(By.xpath("td/div/div"));
			if(values.get(columnsIndex.get("Iban")).getText().contentEquals(IbanEx))
			{
				 iban=values.get(columnsIndex.get("Iban")).getText();
				 nome=values.get(columnsIndex.get("Nome")).getText();
				 Cognome=values.get(isBusiness?columnsIndex.get("Rag.Sociale"):columnsIndex.get("Cognome")).getText();
				 stato=values.get(columnsIndex.get("Stato")).getText();
				 codiceFiscale=values.get(columnsIndex.get("Codice Fiscale")).getText();;
				 Banca=values.get(columnsIndex.get("Banca")).getText();
				 Agenzia=values.get(columnsIndex.get("Agenzia")).getText();;
				
				
				 if(!nome.equalsIgnoreCase(nomeEx))
				 {
					 throw new Exception(String.format("Nome - Valore Atteso %s - presente valore %s ",nomeEx,nome));
				 }
				 if(!Cognome.equalsIgnoreCase(cognomeEx))
				 {
					 throw new Exception(String.format("Cognome - Valore Atteso %s - presente valore %s ",cognomeEx,Cognome));
				 }
				 if(!stato.equalsIgnoreCase(statoEx))
				 {
					 throw new Exception(String.format("STATO - Valore Atteso %s - presente valore %s ",statoEx,stato));
				 }
				 if(!codiceFiscale.equalsIgnoreCase(CFEx))
				 {
					 throw new Exception(String.format("CF - Valore Atteso %s - presente valore %s ",CFEx,codiceFiscale));
				 }
				 if(!Banca.equalsIgnoreCase(bancaEx))
				 {
					 throw new Exception(String.format("BANCA - Valore Atteso %s - presente valore %s ",bancaEx,Banca));
				 }
				 if(!Agenzia.equalsIgnoreCase(AgenziaEx))
				 {
					 throw new Exception(String.format("Agenzia - Valore Atteso %s - presente valore %s ",AgenziaEx,Agenzia));
				 }
				newPaymentMethodCreated=true;
				WebElement radio=table.findElement(By.xpath(String.format("tbody/tr[%s]//input[@type='radio']",i+1)));
				String selected=radio.getAttribute("aria-checked");
				System.out.println("Checked: "+selected);
				System.out.println("IBAN: "+iban);
				if(selected.contentEquals("false"))
				{
					throw new Exception(String.format("Il nuovo metodo di pagamento con iban %s non risulta pre-selezionato",iban));
					//table.findElement(By.xpath(String.format("//tbody/tr[%s]//span[@class='slds-radio_faux']",i+1))).click();
				}
				break;
			}
		}
		if(!newPaymentMethodCreated)
		{
			throw new Exception(String.format("Il nuovo metodo di pagamento con iban %s non è presente nella tabella dei metodi di pagamento",iban));
		}
		
		
	}
	
	public void CliccaConferma() throws Exception
	{
		By conferm=By.xpath("//div[div/header/div/h2/span/text()='Metodo di pagamento']//button[text()='Conferma']");
		util.objectManager(conferm,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//driver.findElement().click();
	}
	
	
	public void VerificaTabellaDepositoCauzionale() throws Exception
	{
		Thread.sleep(5);
		By xpathNonDisp=By.xpath("//h2[text()='Deposito cauzionale']/ancestor::header/parent::div//table/thead/tr/th/div[text()='NON DISPONIBILE']");
		
		if(util.exists(xpathNonDisp,10))
		{
			LOGGER.info("Tabella deposito cauzionale presente ma non compilata");
		}
		else
		{
			WebElement table=driver.findElement(By.xpath("//h2[text()='Deposito cauzionale']/ancestor::header/parent::div//table"));
			
			HashMap<String, Integer> columnsIndex=new HashMap<String, Integer>();
			TimeUnit.SECONDS.sleep(5);
			//Verifica intestazione
			String[] expectedColumns=null;
			List<WebElement> colonneIntestazione=table.findElements(By.xpath("/thead/tr/th/div/div/div/div[@class='slds-assistive-text']"));
		
				expectedColumns= new String[] {"Fornitura Pod/Pdr", "Indirizzo di fornitura", "Deposito Cauzionale Corrente", "Deposito Cauzionale To Be", "Deposito Cauzionale To Be"
						,"Importo deposito cauzionale"};
			
			
			for(int i=0;i<colonneIntestazione.size();i++)
			{
				String expected=expectedColumns[i];
				String actual=colonneIntestazione.get(i).getText();
				System.out.println(String.format("Verifica Colonne - Actual: %s - Expected: %s",actual, expected));
				if(!actual.contentEquals(expected))
				{
					throw new Exception("per la colonna xx  - intestazione diversa da valore atteso");
				}
				
				columnsIndex.put(actual, i);
			}
		}
		
		
		
	}
	
	public void CompilaModalitaFirma(String modalitaFirma,String canale) throws Exception
	{
		//div[header/div/h2/span[text()='Modalità firma e Canale Invio']]/parent::div
		By confermaButton = By.xpath("//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");
		selezionaLigtheningValue("Modalità firma e Canale Invio", "Modalità Firma",modalitaFirma );
		TimeUnit.SECONDS.sleep(3);
		selezionaLigtheningValue("Modalità firma e Canale Invio", "Canale Invio",canale );
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(confermaButton,util.scrollAndClick);
		//driver.findElement(confermaButton).click();
		
	
	}
	
	
	public void ConfermaOperazioneAttivazioneSdd() throws Exception
	{
		checkSpinnersSFDC();
		//By confermaButton = By.xpath("//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");
		By confermaButton = By.xpath("//button[@name='Confirm' and text()='Conferma']");
		util.objectManager(confermaButton,util.scrollAndClick);
		//driver.findElement(confermaButton).click();
		TimeUnit.SECONDS.sleep(15);
		
		
		
	}
	
	
}
