package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class ArticlePageComponent extends BaseComponent {
	public By article_page_text=By.xpath("//div[@class='text parbase']");
	public By article_cookie_button=By.xpath("//button[contains(.,'Cookie')]");
	public String link_by_inner_and_href="//a[contains(.,\"#1\") and contains(@href, \"#2\")]";
	public String link_by_text_and_href="//a[contains(text(),\"#1\") and contains(@href, \"#2\")]";
	//TO DO PROVARE AD USARE element_by_text
	public String article_tab_by_text="//li/a[contains(text(),'#1')]";
	public String article_tab_active_by_text="//li/a[contains(text(),'#1') and contains(@class,'active')]";
	//TO DO PROVARE AD USARE element_by_text_and_parent_class
	public String article_question_by_text_and_section="//section[contains(@class,'#2')]/section/button[contains(text(),\"#1\")]";
	public String article_open_answer_text_by_section="//section[contains(@class,'#1')]/section[@class='open']/div/div";
	//TO DO TROVARE UNA CLASSE COMUNE PER QUESTI XPATH
	public String element_by_text = "//*[contains(text(),\"#1\")]";
//	public String element_by_class = "//*[contains(@class,\"#1\")]";
//	public String element_by_text_and_parent_class_hybrid = element_by_class.replace("#1", "#2").concat(element_by_text);
//	public String element_by_text_and_parent_id ="//*[contains(@id,'#2')]//*[contains(text(),\"#1\")]";
//	public String element_by_text_and_parent_class ="//*[contains(@class,'#2')]//*[contains(text(),\"#1\")]";
//	public String element_by_inner_and_parent_id ="//*[contains(@id,'#2')]//*[contains(.,\"#1\")]";
//	public String element_by_inner_and_parent_class ="//*[contains(@class,'#2')]//*[contains(.,\"#1\")]";
	public By remodalContainer = By.xpath("//*[@class='remodal remodal-is-initialized remodal-is-opened']");
	//
	
	//TO DO TROVARE UNNA NUOVA CLASSE
	public By gas_button = By.id("gasButton");
	public By luce_button = By.id("luceButton");
	public By gas_indirizzo_link = By.xpath("//section[@id='gasSection']/div/a");
	public By luce_indirizzo_link = By.xpath("//section[@id='luceSection']/div/div/a");
	public By info_icon = By.xpath("//a[@class='openDistributore icon-line-info']");
	public By close_popup_button = By.xpath("//button[@class='remodal-close']");
	public By citta_input = By.id("cittaSel");
	public By indirizzo_input = By.id("Indirizzo__c-auto");
	public By civico_input = By.id("Civico__c");
	public By privicy_textarea = By.id("testo_privacy");
	public By esc_button = By.id("formEditEsc");
	public By confirm_button = By.id("preConfirmForm");
	public By citta_error = By.id("cittaSel-error");
	public By indirizzo_error = By.id("Indirizzo__c-auto-error");
	public By civico_error = By.id("Civico__c-error");
	//
	
	public ArticlePageComponent(RemoteWebDriver driver) {
		super(driver);
	}
	
	//TO DO SEMPLIFICARE TUTTI QUESTI METODI
	private void checkPageLinkByInnerAndHref(String text, String href) throws Exception{
		if(!util.exists(By.xpath(link_by_inner_and_href.replace("#1", text).replace("#2", href)), 2))
			throw new Exception("page article doesn't contain link '" + text + "'" + " : " + href);
	}
	public void clickPageLinkByInnerAndHref(String text, String href) throws Exception{
		util.objectManager(By.xpath(link_by_inner_and_href.replace("#1", text).replace("#2", href)), util.scrollAndClick);
	}
	public void clickPageLinkByTextAndHref(String text, String href) throws Exception{
		util.objectManager(By.xpath(link_by_text_and_href.replace("#1", text).replace("#2", href)), util.scrollAndClick);
	}
	private void clickPageLinkByInnerAndHrefInNewTab(String text, String href) throws Exception{
		//OLD String keys = Keys.CONTROL.toString()+Keys.ENTER.toString();
		//OLD String keys = Keys.chord(Keys.CONTROL.toString(),Keys.ENTER.toString());
		By by = By.xpath(link_by_inner_and_href.replace("#1", text).replace("#2", href));
		
		util.objectManager(by, util.scrollElement);
		//Ros util.objectManager(by, util.scrollAndClick);
		// OLD util.waitAndGetElement(by).sendKeys(keys);
		WebElement myLink = util.waitAndGetElement(by);
		String myHref = myLink.getAttribute("href");
		Thread.currentThread().sleep(200);
		//myLink.sendKeys(keys);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.open('"+myHref+"','_blank');");
	}

	public void checkLinks(String[][] links) throws Exception {
		for(String[] link : links) {
			this.checkPageLinkByInnerAndHref(link[0], link[1]);		
		}
	}

	public void checkPageLinks(String[] txtLinks , String separator) throws Exception {
		for(String txtLink : txtLinks) {
			String[] param = txtLink.split(Pattern.quote(separator));
			String parent_window = driver.getWindowHandle();
			this.checkPageLinkByInnerAndHref(param[0] , param[1]);
			this.clickPageLinkByInnerAndHref(param[0]  , param[1]);
			Thread.sleep(2000);
			Set<String> handles = driver.getWindowHandles();
			if(handles.size() == 1) {
				if(!driver.getCurrentUrl().contains(param[2]))
					throw new Exception("link : '" + param[0] + "' in page doesn't open -> " + param[2]);
				driver.navigate().back();
			}else {
				for (String handle : driver.getWindowHandles()) {
					if(!parent_window.equals(handle))
					{
						driver.switchTo().window(handle);
						if(!driver.getCurrentUrl().contains(param[1]))
							throw new Exception("link : '" + param[0] + "' in page doesn't open -> " + param[2]);
						driver.close();
						driver.switchTo().window(parent_window);
					}
				}
			}		
		}
	}
	public void checkPageLinksMultiTab(String[] txtLinks , String separator) throws Exception {
		String parent_window = driver.getWindowHandle();
		for(String txtLink : txtLinks) {
			String[] param = txtLink.split(Pattern.quote(separator));
			this.checkPageLinkByInnerAndHref(param[0] , param[1]);
			this.clickPageLinkByInnerAndHrefInNewTab(param[0] , param[1]);
			driver.switchTo().window(parent_window);
		}
		List<String> urls = new ArrayList<>();
		for(String txtLink : txtLinks) {
			String[] param = txtLink.split(Pattern.quote(separator));
			urls.add(param[2]);
		}

		String errorUrl = null;
		Set<String> handles = driver.getWindowHandles();
		handles.remove(parent_window);
		for (String handle : handles) {
			driver.switchTo().window(handle);
			String currentUrl = driver.getCurrentUrl();
			
/*OLD			if(urls.contains(currentUrl))
				urls.remove(currentUrl);
			else
			{
				errorUrl += "\n "+currentUrl +"' \n";
			}*/
			
			boolean flagSearch = false;
			for (int idx =0; idx< urls.size(); idx++)
			{
				String urlVerify = urls.get(idx);
				//Da R3 2020 le url Enel contengono user e passwd
				//rimuovo protocollo se url Enel
				if (urlVerify.contains("https://www-coll"))
				{
					urlVerify = urlVerify.replace("https://", "");
				}
				if (currentUrl.contains(urlVerify))
					{
						urls.remove(idx);
						flagSearch = true;
					}
			}
			if(!flagSearch)
			{
				errorUrl += "\n "+currentUrl +"' \n";
			}
			driver.close();
			//			driver.switchTo().window(parent_window);
		}
		driver.switchTo().window(parent_window);
		if(urls.size() > 0 ){
			throw new Exception("link : '" + urls.toString() + "' doesn't match with: " +errorUrl);
		}
	}

	private void checkArticleTab(String text) throws Exception{
		if(!util.exists(By.xpath(article_tab_by_text.replace("#1", text)), 2))
			throw new Exception("page article doesn't contain tab '" + text + "'" );
	}

	private void checkArticleTabActiveByText(String text) throws Exception{
		if(!util.exists(By.xpath(article_tab_active_by_text.replace("#1", text)), 2))
			throw new Exception("page article focus on tab '" + text + "' doesn't work" );
	}

	private void clickArticleTabByText(String text) throws Exception{
		util.objectManager(By.xpath(article_tab_by_text.replace("#1", text)), util.scrollAndClick);
	}

	public void checkArticlePageTabs(String[] tabs) throws Exception {
		for(String tab : tabs) {
			this.checkArticleTab(tab);
			this.clickArticleTabByText(tab);
			this.checkArticleTabActiveByText(tab);
		}		
	}

	private void checkQuestionByTextAndSection(String question , String section) throws Exception {
		if(!util.exists(By.xpath(article_question_by_text_and_section.replace("#1", question)
				.replace("#2",section)) , 2))
			throw new Exception("page article doesn't contain question '" + question + "' in section '" + section + "'");
	}

	private void clickQuestionByTextAndSection(String question , String section) throws Exception{
		util.objectManager(By.xpath(article_question_by_text_and_section
				.replace("#1", question)
				.replace("#2",section)), util.scrollAndClick);
	}

	public void checkQuestionsBySection(String[] questions , String section) throws Exception {
		for(String question  : questions) {
			checkQuestionByTextAndSection(question , section);
		}
	}

	public void checkAnswersByQuestionsTextAndSection(String[] answers, String[] questions, String section) throws Exception {
		int i = 0;
		By by = By.xpath(article_open_answer_text_by_section.replace("#1", section));
		Thread.sleep(5000);
		if (questions.length != answers.length) 
			throw new Exception("the numer of questions '" + questions.length + "' are different from answers '" + answers.length + "'");
		for(String question  : questions) {
			this.clickQuestionByTextAndSection(question , section);
			Thread.sleep(3500);
			if(!util.checkElementText(by, answers[i]))
				throw new Exception("the question '" + question + "' doesn't math the answer '" + (i + 1) + "'");
			i++;
		}
	}

	public void writeAnswers( String[] questions , String section) throws Exception {
		By by = By.xpath(article_open_answer_text_by_section.replace("#1", String.valueOf(section)));
		Thread.sleep(5000);
		for(String question  : questions) {
			this.clickQuestionByTextAndSection(question, section);
			Thread.sleep(3500);
			System.out.println(question);
			System.out.println(util.waitAndGetElement(by).getText().replace("\n" , "/n"));
		}
	}

	public void checkArticlePageText(String[] texts) throws Exception {
		WebElement obj = util.waitAndGetElement(article_page_text);
		String articlePageText = obj.getText();
		for(String text : texts ) {
			if(!articlePageText.contains(text)) {
				throw new Exception("page article doesn't contain text : " + text);
			}
			articlePageText = articlePageText.replaceFirst(Pattern.quote(text) , "");
		}	
	}

	public void checkPageTexts(String[] texts) throws Exception {
		for(String text : texts ) {
			if(!util.exists(By.xpath(element_by_text.replace("#1", text)), 2))
				throw new Exception("page doesn't contain text : " + text);
		}	
	}
	public void checkTextsByInnerAndId(String[] texts , String id) throws Exception {
		for(String text : texts ) {
			if(!util.exists(By.xpath(element_by_inner_and_parent_id.replace("#1", text).replace("#2", id)), 2))
				throw new Exception("page doesn't contain text : " + text);
		}	
	}
	public void checkTextsByTextAndId(String[] texts , String id) throws Exception {
		for(String text : texts ) {
			if(!util.exists(By.xpath(element_by_text_and_parent_id.replace("#1", text).replace("#2", id)), 2))
				throw new Exception("page doesn't contain text : " + text);
		}	
	}

	public void checkTextsByTextAndClass(String[] texts , String className) throws Exception {
		for(String text : texts ) {
			if(!util.exists(By.xpath(element_by_text_and_parent_class.replace("#1", text).replace("#2", className)), 2))
				throw new Exception("page doesn't contain text : " + text);
		}	
	}
	public void checkTextsByInnerAndClass(String[] texts ,String className) throws Exception {
		for(String text : texts ) {
			if(!util.exists(By.xpath(element_by_inner_and_parent_class.replace("#1", text).replace("#2", className)), 2))
				throw new Exception("page doesn't contain text : " + text);
		}	
	}
	public void clickCookieButton(By cookie_button) throws Exception {
		if(!util.exists(cookie_button, 2))
			throw new Exception("page article doesn't contain question 'cookie button'");
		util.objectManager(cookie_button, util.scrollAndClick);
	}
	
	public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
}
