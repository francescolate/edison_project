package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class OreFreeDettaglioComponent {
	WebDriver driver;
	SeleniumUtilities util;

	public By oreFreeButton = By.xpath("//a[text()='Gestisci le ore free']");
	public By fasciaBaseHeader = By.xpath("//h2[contains(text(), 'Fascia Base')]");
	public By fasciaBaseTextContainer = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div");
	public By fasciaOreFreeHeader = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div/parent::div//div[contains(text(),'Fascia Ore Free')]");
	public By fasciaOreFreeDayTimeHeader = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div/parent::div//div[contains(text(),'11')]");
	public By fasciaOreFreeModificaButton = By.xpath("//h2[contains(text(), 'Fascia Base')]/parent::div/parent::div//a[text()='MODIFICA']");
	public By fasciaOreFreeModificaDialogHeader = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3");
	public By fasciaOreFreeModificaDialogParagraph = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3/following-sibling::p[contains(text(), 'Scegli')]");
	public By fasciaOreFreeModificaDialogFasciaImpostata = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//h3/following-sibling::div//div[@class='cm-col cm-col-medium print-new-settings']");
	public By alleOreSelect = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//span[@class='selectboxit selectboxit-disabled selectboxit-btn']");
	public By dalleOreSelect = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//span[@id='startingDailySelectBoxItContainer']"); 
	public By dalleOreDailySelect = By.xpath("//span[@id='startingDailySelectBoxItContainer']");
	public By dalleOreSelectValue = By.xpath("//span[@id='startingBaseSelectBoxItText']");
	public By dalleOreDailySelectValue = By.xpath("//span[@id='startingDailySelectBoxItText']");
	public By alleOreValue = By.xpath("//span[text()='23:00']");
	public By annullaButton = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//div[@class='button-container ore-free-switch-button']//span[text()='Annulla']/parent::button");
	public By confermaButton = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//div[@class='button-container ore-free-switch-button']//span[text()='Conferma']/parent::button");
	public By chiudiButtonCambioFascia = By.xpath("//div[@data-remodal-id='modal-result-ok-today']//h3[contains(text(), 'La Fascia Base')]/parent::div/following-sibling::div[@class='button-container']");
	public By ripristinaFasciaBaseButton = By.xpath("//a[contains(text(), 'Ripristina Fascia Base')]");
	public By ripristinaFasciaBasePopupHeader = By.xpath("//div[@data-remodal-id='modal-reset-base-range']//div[@class='modal-header']");
	public By ripristinaFasciaBasePopupConfermaButton = By.xpath("//div[@data-remodal-id='modal-reset-base-range']//div[@class='modal-header']/..//span[text()='Conferma']/..");
	public By ripristinaFasciaBasePopupAnnullaButton = By.xpath("//div[@data-remodal-id='modal-reset-base-range']//div[@class='modal-header']/..//span[text()='Annulla']/..");
	public By fasciaBaseRipristinataPopupCloseButton = By.xpath("//div[@data-remodal-id='modal-result-ok']//h3[contains(text(), 'La tua richiesta')]/parent::div/following-sibling::div[@class='button-container']");
	public By fasceGiornaliereHeader = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/ancestor::div[@class='forniture-info-heading ']");
	public By ripristinaFasciaBaseFasceGiornaliereButton = By.xpath("//*[contains(text(),'Fasce Giornaliere')]/following-sibling::a[@class='forniture-link forniture-side-link reset-fascia ore-free-switch-desktop modal-reset-base-range']");
	public By modificaFasciaGiornalieraButton = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box ']//a");
	public By fasciaGiornalieraModificata = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box ']//*[contains(text(),'20:00 - 23:00')]");
	public By chiudiButton = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//button");
	public By dropdown = By.xpath("/html/body/div[34]/div/div/div/div[1]/div/div[1]/div/select");
	public By dateConferma = By.xpath("//span[text()='CONFERMA']/parent::button[@class='button_first']");
	public By dateAnnula = By.xpath("/html/body/div[29]/div/div/div/div[2]/button[1]");
	public By headingTitle1 = By.xpath("//*[@id='mainContentWrapper']/div[1]/h1");
	public By headingTitle2 = By.xpath("//*[@id='mainContentWrapper']/div[1]/p[2]");
	public By homepageTitle1 = By.xpath("//h2[contains(text(),'Le tue forniture')]");
	public By homepageTitle2 = By.xpath("//*[@id='lista-forniture']/div[1]/p[2]");
	public By HomePageLogo = By.xpath("//*[@id='header']/div/a[1]");
	public By datiaggiornati = By.xpath("//*[@id='datiAggiornati']");
	
	public By fornitureTitle1 = By.xpath("//h2[contains(text(),'Ore Free di oggi')]");
	public By scegliLink = By.xpath("//*[@id='box_orefreedetail']/section/div[19]/div[1]/a[1]");
	public By scegli  = By.xpath("(//a[contains(text(),'Scegli la tua fascia')])[1]");
//ROpublic By fornitureTitle2 = By.xpath("//*[@id='box_orefreedetail']/section/div[18]/div[1]/p");
	public By fornitureTitle2 = By.xpath("//*[@id='box_orefreedetail']/section/div[19]/div[1]/p");
	public By fasciaHeader = By.xpath("//*[@id='box_orefreedetail']/section/div[19]/div[2]/div/div[1]");
	public By fasciaTime = By.xpath("//div[@id='fasciaGiornalieraImpostata']");
	public By fasceTitle = By.xpath("//*[@id='fasce_daily']/div[1]/h2");
	public By FasceContent1 =By.xpath("//*[@id='fasce_daily']/div[1]/p[1]");
	public By fasceContent2 = By.xpath("//*[@id='fasce_daily']/div[1]/p[2]");
	
	
	public By consumi = By.xpath("//h2[contains(text(),'Consumi giornalieri')]");
	//public By consumiContent = By.xpath("//*[@id='box_orefreedetail']/section/div[19]/div[1]/p");
	public By consumiContent = By.xpath("(//p[contains(text(),'Di seguito potrai visualizzare')])[1]");
	public By martedi = By.xpath("//*[@id='today']");
	public By Orefreetime = By.xpath("//div[@id='freeSlot']");
	public By scegliGiorno1 = By.xpath("//a[contains(text(), 'Scegli la tua fascia')]");
	public By scegliGiorno = By.xpath("(//a[contains(text(),'Scegli il Giorno')])[1]");
	public By dateFirst = By.xpath("(//div[@class='datepicker-panel']//li[text()='1'])[1]");
	public By dateFirstSelect = By.xpath("//div[@class='datepicker-panel']/ancestor::div[@class='modal-header']//li[text()='1']/parent::ul");
	public By todaydate = By.xpath("//*[contains(text(), '$DATE$')]/ancestor::div[@class='single-info-box section-mb']//*[@id='today']");
	public By fasciaModifica = By.xpath("//a[@class='forniture-link modify-current']");
	public By fasciachudiBtn = By.xpath("(//span[contains(text(),'Chiudi')]/parent::button[@class='button_first'])[4]");
	public By day = By.xpath("//*[@id='today']");
	public By dateConfermaBtn = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//div[@class='button-container ore-free-switch-button']//span[text()='CONFERMA']/parent::button");
	public By dateAnnullaBtn = By.xpath("//div[@class='cm-forniture-infos-modal remodal remodal-is-initialized remodal-is-opened']//div[@class='button-container ore-free-switch-button']//span[text()='ANNULLA']/parent::button");
	
	public By titoloPopup=By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//h3[@class='bold-title']");
	public By testoPopup=By.xpath("//div[@class='remodal-wrapper remodal-is-opened']//p[2]");
	
	public By dropdownClick = By.xpath("//span[@id='inputAssCSelectBoxItContainer']");
	public By dropdownClickNew = By.xpath("//span[@id='inputTipoSelectBoxIt']");
	public By altroConsumoValue = By.xpath("//span[@id='inputAssCSelectBoxItText' and contains(text(),'ALTROCONSUMO')]");
	public By ddvaluecontainer = By.xpath("//span[@id='inputTipoAssSelectBoxItContainer']");
	
	public OreFreeDettaglioComponent(WebDriver driver) throws Exception {
    	
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 60))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void verifyModifiedComponentExistence(By by) throws Exception {
		String itemXpath = by.toString().replace("By.xpath: ", "");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM", Locale.ITALY);  
		LocalDateTime now = LocalDateTime.now();  
		String dayAndMonth = dtf.format(now);
		//System.out.println(dayAndMonth);
		int day = Integer.parseInt(dayAndMonth.substring(0, dayAndMonth.indexOf(" ")))+1;
		String month = StringUtils.capitalize(dayAndMonth.substring(dayAndMonth.indexOf(" ")+1));

		
		//System.out.println(month);
		by = By.xpath(itemXpath.replace("$DATE$", day+" "+month));
		if (!util.exists(by, 20))
			throw new Exception("object with xpath " + by + " is not exist.");
	}
	
		public void Todaydate(By dateobject) throws Exception{
			String itemXpath = dateobject.toString().replace("By.xpath: ", "");
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEEE dd MMMM", Locale.ITALY);  
			LocalDateTime now = LocalDateTime.now();  
			String dayAndMonth = dtf.format(now);
		//	System.out.println(dayAndMonth);
			int day = Integer.parseInt(dayAndMonth.substring(0, dayAndMonth.indexOf(" ")));
			String month = StringUtils.capitalize(dayAndMonth.substring(dayAndMonth.indexOf(" ")));
		//	System.out.println(month);
		/*	
			{
			WebElement e = driver.findElement(By.xpath("//*[@id='fasce_daily']/div[2]/div[1]/div[1]/div[1]/div[2]"));
			System.out.println(e.getText());
			}
			{
			WebElement el = driver.findElement(By.xpath("//*[@id='today']"));
			String tod = el.getText();
			System.out.println(tod);
			}
			*/
	}

		public String previousDate() throws Exception{
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEEE d MMMM", Locale.ITALY);  
			LocalDateTime now = LocalDateTime.now();  
			String preDateMonth = dtf.format(now.minusDays(1));
			System.out.println(preDateMonth);
			
			//WebElement el = driver.findElement(By.xpath("//div[@class='single-info-box section-mb']//div[1]/b"));
			return preDateMonth;
		}
		
		public String firstDateofMonth() throws Exception{
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEEE d MMMM", Locale.ITALY);  
			LocalDateTime now = LocalDateTime.now();  
			String firstDayAndMonth = dtf.format(now.withDayOfMonth(1));
			System.out.println(firstDayAndMonth);
			
			//WebElement el = driver.findElement(By.xpath("//div[@class='single-info-box section-mb']//div[1]/b"));
			return firstDayAndMonth;
			
	}
		public void checkForDate(By dateObject,String dat) throws Exception {
			WebElement we = util.waitAndGetElement(dateObject);
			Thread.sleep(2000);
			String textValue = driver.findElement(dateObject).getText();
			
			if(!dat.equalsIgnoreCase(textValue))
			throw new Exception (" date is not matching with actual date"+dat);
			
		}
		
		public boolean display(By disp) throws Exception{
			WebElement we = util.waitAndGetElement(disp);
			we = driver.findElement(disp);
			Thread.sleep(3000);
			//System.out.println(we.getText());
			if(we.isDisplayed()){
				return true;
			}
				else{
					
			throw new Exception("Value is not displayed");
			
				}
			
		}
	public void clickModificaComponentByDate(By by, int dayToBeAdded) throws Exception{
		String itemXpath = by.toString().replace("By.xpath: ", "");
		System.out.println(itemXpath);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM", Locale.ITALY);  
		LocalDateTime now = LocalDateTime.now();  
		String dayAndMonth = dtf.format(now);
		System.out.println(dayAndMonth);
		int day = Integer.parseInt(dayAndMonth.substring(0, dayAndMonth.indexOf(" ")))+dayToBeAdded;
		String month = StringUtils.capitalize(dayAndMonth.substring(dayAndMonth.indexOf(" ")+1));
		by = By.xpath(itemXpath.replace("$DATE$", day+" "+month));
		if (util.exists(by, 15)) {
			util.objectManager(by, util.scrollToVisibility, false);
		    util.objectManager(by, util.scrollAndClick);
		}else
			throw new Exception("Fascia giornaliera non trovata.");
	}
	
	public void checkForText(By text,String property) throws Exception {
		WebElement we = util.waitAndGetElement(text);
		String textValue = driver.findElement(text).getText();
		
		if(!property.contains(textValue))
		throw new Exception (" Text is not matching with actual data"+property);
		
	}
	
	
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, false);
		util.objectManager(clickObject, util.scrollAndClick);
	}
	
	public void clickComponentDatePicker(By clickObject) throws Exception {
		util.objectManager(clickObject, util.click);
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text: '"+text+"' is not equal to the one within web portal page: '"+weText+"'.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
	public By dd = By.xpath("//span[@id='inputTipoAssSelectBoxItText' and contains(text(),'Rateizza la tua bolletta')]");
	public boolean checkNodeValue(By by, String value) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		if(we.getText().equals(value))
			return true;
		else
			return false;
	}
    public void selectDropdown(By dropdownObject) throws Exception{
		
		WebElement dropdown = driver.findElement(dropdownObject);
		
		Select dd = new Select(dropdown);
		Thread.sleep(4000);
		dd.selectByValue("10:00");
		
	/*	List<WebElement> li = dd.getOptions();
		for (int i=0 ; i<li.size() ; i++){
			String options = li.get(i).getText().toString();
			System.out.println(options);
	    		}
	    		*/
	}
	
	public void scrollDown(By by) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		util.scrollToElement(we);
		util.scrollNaNdecchiaNdecchia();
	}
	
	
	public final static String fasciaBaseText = "Fascia Base La fascia di Ore Free di base impostata pertutti i giorni * .Ricordati che il cambiofasciabasesarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura.";
	public final static String fasciaOreFreeHeaderText = "Fascia Ore Free";
	public final static String fasciaOreFreeDayTimeText = "11:00 - 14:00";
	public final static String fasciaOreFreeModificaDialogHeaderText = "Imposta la tua nuova Fascia Base";
	public final static String fasciaOreFreeModificaDialogParagraphText = "Scegli le Ore Free più adatte alle tue esigenze di consumo.";
	public final static String fasciaOreFreeModificaDialogFasciaImpostataText = "Hai impostato la fascia 11:00 - 14:00";
	public final static String ripristinaFasciaBasePopupHeaderText = "è stata aperta la modale Vuoi ripristinare la fascia base? Chiudi Vuoi ripristinare la fascia base? "+
																		"Confermando questa scelta su tutti i giorni, verrà ripristinata la Fascia Base rispetto alla programmazione giornaliera impostata precedentemente. Potrai comunque reimpostare le Ore Free giornaliere in un secondo momento.";
	public final static String fasceGiornaliereHeaderText = "Fasce Giornaliere Ripristina Fascia Base La fascia Ore Free che puoi impostare a piacimento per ciascuno deiprossimi 7 giorni.In alternativa verrà applicata la Fascia Base che hai scelto.Ricordati che il cambiofasciagiornalierasarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura. Ripristina Fascia Base";
    public final static String headingtitleValue1 = "Benvenuto nella tua area privata";
    public final static String headlingtitleValue2 = "In questa sezione potrai gestire le tue forniture";
    public final static String homepagetitleValue1 = "Le tue forniture";
    public final static String homepagetitleValue2 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
    public final static String fornitureTitle1Value = "Ore Free di oggi";
	public final static String fornitureTitle2Value = "Di seguito potrai visualizzare i consumi relativi all'ultimo giorno per il quale è disponibile il dato.Potrai sempre avere una vista storica dei consumi giornalieri cambiando il giorno.";
	public final static String fasciaHeaderValue = "Fasce Ore Free";
	public final static String fasciaTimeValue = "12:00 - 15:00";
	public final static String fasceTitleValue = "Fasce Giornaliere";
	public final static String FasceContent1Value = "La fascia Ore Free che puoi impostare a piacimento per ciascuno deiprossimi 7 giorni.In alternativa verrà applicata la Fascia Base che hai scelto.";
	public final static String fasceContent2Value = "Ricordati che il cambiofasciagiornalierasarà disponibile solo dopo la nostracomunicazioneviaSMS/EMAILe nel frattempo sarà garantita la fascia diOre Freescelta in fase disottoscrizionedel contratto di fornitura.";

	public final static String consumiValue = "Consumi giornalieri";
	public final static String consumiContentValue = "Di seguito potrai visualizzare i tuoi consumi giornalieri.";
	public final static String martediValue = "Martedì 16 Giugno";
	public final static String orefreetimeValue = "Ore Free: 11:00 - 14:00";
	public final static String dayMonth = "Giovedì 18 Giugno";
	


}
