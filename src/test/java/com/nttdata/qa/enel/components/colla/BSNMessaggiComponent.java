package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BSNMessaggiComponent {
	SeleniumUtilities util;
	WebDriver driver;
	
	public By path = By.xpath("//section[@class='content']//ol");
	public By pageTitle = By.xpath("//h1[text()='Teniamoci in contatto']");
	public By pageSubText = By.xpath("//h1[text()='Teniamoci in contatto']/following::p[1]");
	
	public BSNMessaggiComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public static final String Path = "Area riservataMessaggi";
	public static final String TitleSubText ="Qui puoi leggere tutto ciò che ti abbiamo scritto e che ti scriveremo.";
}
