package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ChangePowerAndVoltagePreventintoComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By preventintoText = By.xpath("//div[@class='content-group'][1]/p");
	public By potenzoLabel = By.xpath("//div[@class='details-container mpt_preventivo']/dl/span[1]/dt");				//Marco - Change name class
	public By tensionLabel = By.xpath("//div[@class='details-container mpt_preventivo']/dl/span[2]/dt");				//Marco - Change name class
	public By tensionValue= By.xpath("//div[@class='details-container mpt_preventivo']/dl/span[2]/dd");					//Marco - Change name class
	public By potenzoValue= By.xpath("//div[@class='details-container mpt_preventivo']/dl/span[1]/dd");					//Marco - Change name class
	public By detagliocostiTitle = By.xpath("//h3[@class='plan-main-head bold']");
	public By costOfLabour = By.xpath("//p[@class='totalCostContainer']/span[1]/span[1]");
	public By costOfLabourValue = By.xpath("//p[@class='totalCostContainer']/span[1]/span[2]");
	public By costOfLaourSubTxt = By.xpath("//span[@class='ditTableHeading']");
	public By cumertialBurden= By.xpath("//div[@class='dtl-container']/dl/span[1]");
	public By administrativeBurden= By.xpath("//div[@class='dtl-container']/dl/span[2]");
	public By powerQuote = By.xpath("//dt[text()='Quota Potenza:']");
	public By executionTime = By.xpath("//h4[@class='tiny-head']");
	public By executionTimeSubText = By.xpath("//div[@class='ditTableContainer']/p[2]");
	public By executionTimeText1 = By.xpath("//p[@class='noMargin tableInfo']");
	public By preventintoText2 = By.xpath("//div[@class='content-group tiny-border'][1]");
	public By modalitàdiPagamento = By.xpath("//h4[@class='min-bottom']");
	public By modalitàdiPagamentoText = By.xpath("//div[@class='content-group tiny-border'][2]//p[@class='noMargin']");
	public By addebitoInBolletta = By.xpath("//div[@class='radio-group-container']/div[@class='radio-container'][1]");
	public By voglioPagareOnline= By.xpath("//div[@class='radio-group-container']/div[@class='radio-container'][2]");
	public By checkBox= By.xpath("//label[@id='inputchecklinklabel']");
	public By checkBoxText = By.xpath("//label[@id='inputchecklinklabel']");
	public By informativa = By.xpath("//a[@id='input-check-info-link']");
	public By informativaClose = By.xpath("//span[@class='dsc-icon-close']");
	public By informativaTitle = By.xpath("//h3[@class='modal-title']");
	public By informativaText = By.xpath("//div[@class='modal-body']//p");
	public By preventivoText1 = By.xpath("//div[2]/div[@class='content-group'][2]/h4");
	public By emailInput = By.xpath("//dt[text()='Email']//following-sibling::dd");
	public By email = By.xpath("//span[@class='form-group motivo'][1]/dt");
	public By cellular = By.xpath("//span[@class='form-group motivo'][2]/dt");
	public By cellularInput = By.xpath("//dt[text()='Numero cellulare']//following-sibling::dd");
	public By preventivoText2 = By.xpath("//div[@class='content-group grey-bg']/h4");
	public By preventivoText3= By.xpath("//div[@class='right-main']/div[2]/p[@class='noMargin']");
	public By addressLable= By.xpath("//span[@class='flex']/span[@class='indirizzo']/span[@class='label']");
	public By address = By.xpath("//div/span[@class='flex']/span[@class='indirizzo']/span[@class='valore']");
	public By POD = By.xpath("//div/span[@class='flex']/span[@class='cliente']/span[@class='valore']");
	public By PODLable = By.xpath("//div/span[@class='flex']/span[@class='cliente']/span[@class='label']");
	public By conferma = By.xpath("//div[@class='right-main']/div[@class='button-container']/button[@id='nextButton']");
	public By popUp = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']");
	public By popUpClose = By.xpath("//span[@class='dsc-icon-close']");
	public By popUpYes = By.xpath("//button[@id='overlayYesButton']");
	public By popUpNo = By.xpath("//button[@id='overlayNoButton']");
	public By supplySection = By.xpath("//fieldset[@class='list-container']");
	public By indietro = By.xpath("//div[@class='right-main']/div[@class='button-container']/button[@class='button_second']");
	public By modalAlert = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/p[@class='inner-text']");
	public By si = By.xpath("//button[@id='overlayYesButton']");
	public By richesta = By.xpath("//div[@class='typ-container content-group cITA_IFM_LCP451_SelfCare_Closure_Step']");
	public By fineButton = By.xpath("//span[text()='FINE']");
	
	
	public ChangePowerAndVoltagePreventintoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(5000);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	 
	 public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 100);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}

	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkData(By checkObject,String property) throws Exception{
			util.waitAndGetElement(checkObject);
			util.scrollToElement(driver.findElement(checkObject));
			String actualValue = driver.findElement(checkObject).getText();
			if(!property.equalsIgnoreCase(actualValue))
				throw new Exception(actualValue+" Displayed value is not mathcing with the expected Data "+property);
		}
		
		public void jsClickComponent(By by) throws Exception{
			util.jsClickElement(by);
		}

		public static final String preventivo_realizzato = "Il preventivo è stato realizzato sulla base dei seguenti valori di potenza e/o tensione da te inseriti:";				//Marco
		public static final String provinica_text = "Se decidi di continuare, verrà contattato il distributore competente per il preventivo definitivo. Questo potrebbe comportare l’invio di un tecnico per un sopralluogo sul tuo impianto.";
		public static final String dettaglio_costi = "Dettaglio costi e tempi";
		public static final String costOfLabourText = "Costituito dalla somma delle seguenti voci:";
		public static final String ExcecutionTimeText ="I tempi di completamento dell'operazione sono indicativamente tra i 5 e i 30 giorni, calcolati sulla base delle condizioni della tua richiesta.";
		public static final String ModalitadiPagamento = "Puoi scegliere di pagare tramite addebito in bolletta oppure di ricevere sulla email il link per effettuare il pagamento online.";
		public static final String CheckBoxText = "Dichiaro di aver preso visione dell' informativa*";
		public static final String InformativaText = "Nel caso di selezione della modalità di pagamento \"Voglio pagare online\" riceverai tramite e-mail il preventivo in formato PDF e il link con le indicazioni su come pagare l'importo, e, solo dopo aver effettuato il pagamento, sarà possibile dare esecuzione alla richiesta. Se hai selezionato la modalità \"Addebito in bolletta\" riceverai una e-mail di presa in carico della richiesta e dovrai attendere la successiva comunicazione di modifica avvenuta. Cliccando sul pulsante in basso \"Ho preso visione\" dichiari, inoltre, di aver compreso che riceverai il preventivo completo nella casella e-mail indicata.";
		public static final String PreventivoText1 = "Riceverai la documentazione tramite le seguenti modalità di contatto:";
		public static final String PreventivoText2 = "Il nuovo Deposito Cauzionale sarà pari a 18,08€ a fronte del Deposito Cauzionale Attuale pari a 15,49€La quota potenza trasporto che troverai in bolletta dopo la variazione: 74,52€ annui (attuale quota potenza trasporto: 63,87€ annui)";
		public static final String PreventivoText3 = "Per verificare la fattibilità tecnica e confermare il preventivo clicca su Continua. In alcuni casi potrebbe essere necessario il sopralluogo da parte di un tecnico del distributore.";
		public static final String ExecutionTimeSubText ="I tempi di completamento dell'operazione sono indicativamente tra i 5 e i 30 giorni, calcolati sulla base delle condizioni della tua richiesta.";
		public static final String ExecutionTimeBottomText ="* L'iva attualmente in vigore è pari al 10%.";
		public static final String PreventivoText4 = "La modifica di potenza e/o tensione verrà applicata sulla seguente fornitura:";
		public static final String modalAlertTxet = "Stiamo confermando il preventivo";
		public static final String RichestaText = "La tua richiesta è stata presa in caricoRiceverai a breve una email di conferma di avvio della richiesta di modifica potenza e/o tensione, unitamente al preventivo in formato PDF. Grazie per aver utilizzato il servizio!";
		public static final String modalAlert2 = "Procedendo si avvierà l'effettiva richiesta di modifica.Vuoi continuare?";
		public static final String confirmText = "La tua richiesta è stata presa in caricoRiceverai a breve una email di conferma di avvio della richiesta di modifica potenza e/o tensione, unitamente alle indicazioni relative alle modalità d'intervento da parte del tecnico della rete.Grazie per aver utilizzato il servizio!";
}
