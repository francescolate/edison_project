package com.nttdata.qa.enel.components.colla;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class DettaglioFornitureComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By pageTitle = By.xpath("//h2[text()='Servizi per le forniture']");
	public By serviziPerleforniture = By.xpath("//section[@id='forniture-id0']/div[@class='section-heading']/h2");
	public By modificaPotenzo = By.xpath("//h3[contains(text(),'Modifica Potenza e/o Tensione')]");
	//337
	
	//335 New 
	public By dettaglioForniuturaButton310522248 = By.xpath("//dd[text()='310522248']/ancestor::div[@class='details-container']/following::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
		
	
	public By emailInput_337 = By.xpath("//label[text()='E-Mail']/following-sibling::input[@id='email']");
	public By circuitoDropdown = By.xpath("//label[text()='Circuito']/following::select[@id='circuito']");
	public By numerodiCarta = By.xpath("//label[text()='Numero di carta']/following-sibling::input[@id='numeroCarta']");
	public By scandenzaDropDownMonth = By.xpath("//label[text()='Scadenza']/following::select[@id='scadenzaMese']");
	public By scandenzaDropDrownYear = By.xpath("//select[@id='scadenzaAnno']");
	public By cvvInput = By.xpath("//label[text()='CVV2/CVC2/4DBC']/following-sibling::input[@id='cvv2']");
	public By privacyCheckbox =By.xpath("//label[text()='Privacy']/following-sibling::input[@id='accetto']");
	public By proseguibutton = By.xpath("//div[@id='buttons']/child::button[@id='proseguiOn']");
	public By addebitoDirettoHeader = By.xpath("//div[@class='section-heading']/child::h2[contains(text(),'Addebito Diretto')]");
	public By addebitoDirettoContent = By.xpath("//div[@class='section-heading']/child::p[contains(text(),'Il servizio Addebito Diretto')]");
	public By activePaymentContent = By.xpath("//div[@id='accessibility']/child::p[contains(text(),'Il servizio Addebito Diretto')]");
	public By indrizzo_50053 = By.xpath("//span[contains(text(),'Indirizzo della fornitura')]/following-sibling::span[contains(text(),'DA VERRAZZANO 40')]");
	public By modificaCartaButton = By.xpath("//span[text()='MODIFICA SU CARTA DI PAGAMENTO']/parent::button[@class='button_second']");
	public By operazioneContent = By.xpath("//h4[contains(text(),'Operazione eseguita correttamente')]");
	public By larichiestaContent = By.xpath("//div[@class='step-box-container']/descendant::p[contains(text(),'La richiesta di modifica')]");
	public By addebitoFineButton = By.xpath("//span[text()='FINE']/parent::button[@class='button_first']");
	//Salesforce337 
	public By customerName = By.xpath("//span[@class='slds-grid slds-grid--align-spread']/parent::th[@class='slds-cell-edit cellContainer']/descendant::a[contains(text(),'CONFEZIONE ANGELO DI XIE DESHOU')]");
	public By dettaglioForniuturaButton = By.xpath("//div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By dettaglioForniuturaButton1 = By.xpath("//div[text()='773999385']/ancestor::div[@class='details-container']/following::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By dettaglioForniuturaButton310506437 = By.xpath("(//dd[text()='310506437']/ancestor::div[@class='details-container']/following::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')])[1]");
	public By addebitoDirettoLink = By.xpath("//h3[contains(text(),'Addebito Diretto')]/ancestor::a"); 
	
	// Addebito Diretto
	public By addebitoDirettoLinkIn = By.xpath("//p[contains(text(),'Addebito Diretto')]");
	public By addebitoDirettoTtile = By.xpath("//section[@id='monoFonr']//h1[contains(text(),'Addebito Diretto')]");
	public By addebitoDirettoSubTtile = By.xpath("//section[@id='monoFonr']//p[contains(text(),'Il servizio Addebito Diretto ti')]");
	public By modifica = By.xpath("//table[@class='table']/tbody/tr[2]/td[@class='text-right']");
	public By modificaSubTitle = By.xpath("//p[.='Vuoi modificare il servizio anche per le altre forniture?']");
	public By modificaSubTitle2 = By.xpath("//label[.='Seleziona le forniture su cui vuoi cambiare la modalità di pagamento.']");
	public By signRadio = By.xpath("//td[text()='310512585']//following-sibling::td[2]");
	public By signCheck = By.xpath("//td[text()='310512585']/preceding-sibling::td[3]"); 
	public By pROSEGUI = By.xpath("//span[text()='PROSEGUI']");
	public By signSubtitle = By.xpath("//h5[text()='Modalità di pagamento']"); 
	public By signSubtitle2 = By.xpath("//*[text()='Addebito Diretto su conto corrente:']"); 
	public By iBAN = By.xpath("//label[text()='IBAN*']/following-sibling::input");
	
	public By cognome = By.xpath("//label[text()='Cognome/Ragione sociale Intestatario C/C*']/following-sibling::input");
	public By cONFERMA = By.xpath("//span[text()='CONFERMA']");
	public By fine = By.xpath("//a[text()='FINE']");
	public By finePage_Text1 = By.xpath("//p[text()='Operazione eseguita correttamente.']");
	public By finePage_Text2 = By.xpath("//p[contains(text(),'La tua richiesta è stata presa in carico ed')]");
	
	public By addebitoDirettoSubTtile1 = By.xpath("//p[@id='labelAtt']");
	public By addebitoDirettoSubTtile2 = By.xpath("//p[contains(text(),'Il servizio Addebito Diretto su Conto Corrente')] ");
	public By indirizzodella = By.xpath("//p[@id='indFornAtt']");
	public By iBAN2 = By.xpath("//p[@id='iban']");
	
	public By infoEnelEnergiaTile = By.xpath("//h3[text()='InfoEnelEnergia']");
	public By errorTitle = By.xpath("//h1[text()='Errore nella richiesta']");
	public By errorText1 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	public By errorText2 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	public By errorText3 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	public By errorText4 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	public By errorText5 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	public By errorText6 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	public By errorText7 = By.xpath("//p[contains(text(),'Ci scusiamo')]");
	
	public By autolettura = By.xpath("//h3[text()='Autolettura']");
	public By infoEnelEnergia = By.xpath("//li[@class='tile']//h3[text()='InfoEnelEnergia']");
	public By modificaIndirizzoFatturazione = By.xpath("//h3[text()='Modifica Indirizzo Fatturazione']");
	public By modificaPotenzaeoTensione = By.xpath("//h3[text()='Modifica Potenza e/o Tensione']");
	public By modificaContattieConsensi = By.xpath("//li[@class='tile']//h3[text()='Modifica Contatti e Consensi']");
	public By billServices = By.xpath("//div[@class='section-heading']/h2[text()='Servizi per le bollette']");
	public By billServicesSubText = By.xpath("//div[@class='section-heading']//p[text()='Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette']");
	public By webBill = By.xpath("//li[@class='tile']//h3[text()='Bolletta Web']");
	public By onlinePayment = By.xpath("//li[@class='tile']//h3[text()='Pagamento Online']");
	public By directDebit = By.xpath("//h3[text()='Addebito Diretto']");
	public By billCorrection = By.xpath("//h3[text()='Rettifica Bolletta']");
	public By installments = By.xpath("//h3[text()='Rateizzazione']");
	public By summary = By.xpath("//h3[text()='Bolletta di Sintesi o di Dettaglio']");
	public By duplicateUtilityBill = By.xpath("//h3[text()='Duplicato Bolletta']");
	public By sendingDoc = By.xpath("//h3[text()='Invio Documenti']");
	public By supplyDeactivation = By.xpath("//h3[text()='Disattivazione Fornitura']");
	public By indirizzoEmaildiRecapito = By.xpath("//h3[text()='Indirizzo e-mail di recapito']");
	
	//339
	public By modificaNew = By.xpath("//a[text()='MODIFICA CONTO CORRENTE']");
	public By signRadioNew310492564 = By.xpath("//td[text()='310492564']/following-sibling::td[2]/a");
	public By signCheckNew310492564 = By.xpath("//td[text()='310492564']/preceding-sibling::td[3]"); 
	
	public DettaglioFornitureComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
 
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
 	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
    
    public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
    
    public void jsClickObject(By by) throws Exception {
		util.jsClickElement(by);
	}
	
    public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch.The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
			
	}
	public void checkTextHighlight(By object, String color) throws Exception{
        ColorUtils c = new ColorUtils();
        String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
//        System.out.println(colore);
        if(!colore.contentEquals(color)) throw new Exception("l'oggetto web  e' di colore "+color+". Colore trovato : "+colore);
    }
	
	
	public void verifyStepStatus(String color) throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 Inserimento dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
		checkTextHighlight(By.xpath("//li[contains(@class,'step')]/descendant::span[@class='step-label active']"), color);
	}
	
	public void checkFieldValue(String xpath, String property) throws Exception{

		WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        Thread.sleep(2000);
        System.out.println("value "+s);
        if(!s.equalsIgnoreCase(property))
              throw new Exception ("Field value is not matching with the actual value");
        
	}
	
	public void checkForPlaceholderText(By checkObject, String property) throws Exception{
		String actualText = driver.findElement(checkObject).getAttribute("placeholder");
		System.out.println(actualText);
		
		if(! property.equalsIgnoreCase(actualText))				
				throw new Exception("Placeholder value is not matching with the expected value "+property);			
	}
	public void checkboxTitle(By checkObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
		List<WebElement> field = driver.findElements(checkObject);
		
		for(WebElement questions : field){
		String description=questions.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
			
		if(description.contentEquals("Comunica Lettura"))
			textfield_found="YES";
		else if (description.contentEquals("Emissione Bolletta"))
			textfield_found="YES";
		else if (description.contentEquals("Avvenuto Pagamento"))
			textfield_found="YES";
		else if (description.contentEquals("Scadenza Prossima Bolletta"))
			textfield_found="YES";
		else if (description.contentEquals("Sollecito Pagamento"))
			textfield_found="YES";
		else if (description.contentEquals("Avviso Consumi"))
			textfield_found="YES";
			
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Checkbox title are not displayed");
		}
	}
	public void verifyStatus() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 Inserimento dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
		// Validating the color of highlighted step
		checkTextHighlight(By.xpath("//li[contains(@class,'step')]/descendant::span[@class='step-label active']"), servicesColor);
	}
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	public void verifyAddebitoStatus() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		String stepFound="NO";
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("Inserimento dati")) {
				stepFound = "YES";
			
			}else if (stepDescription.contentEquals("Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
			
	}
	public void selectOptions(String option, By dropdownObject) throws Exception {
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(dropdownObject));
	WebElement dropdown = driver.findElement(dropdownObject);
		   	Select select = new Select(dropdown); 
		    select.selectByVisibleText(option);
		}
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
    public void pressJavascript(By clickObject) throws Exception {
	WebElement element = util.waitAndGetElement(clickObject);
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", element);
}
	
	public void verifyCheckboxLabel(By labelObject) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(labelObject));
						
		String textfield_found="NO";
		List<WebElement> label = driver.findElements(labelObject);
		
		for(WebElement labelcheckbox : label){
		String description=labelcheckbox.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		System.out.println(description);
			
		if(description.contentEquals("SMS"))
			textfield_found="YES";
		else if (description.contentEquals("Email"))
			textfield_found="YES";
	
			
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Checkbox Label are not displayed");
		}
	}
	public void attivazioneverifyStepStatus(String color) throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			System.out.println(stepDescription);
			
			if (stepDescription.contains("1 Inserimento Dati Step attivo 1 di 3")) {
				stepFound = "YES";
			}else if (stepDescription.contains("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contains("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
		//checkTextHighlight(By.xpath("//li[contains(@class,'step')]/descendant::span[@class='step-label active']"), color);
	}
	public void verifyCheckboxHeaders(By headers) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(headers));
						
		String textfield_found="NO";
		List<WebElement> header = driver.findElements(headers);
		
		for(WebElement checkboxHeaders : header){
		String description=checkboxHeaders.getText();
		description= description.replaceAll("(\r\n|\n)", " ");
		System.out.println(description);
		if(description.contentEquals("Comunica Lettura"))
			textfield_found="YES";
		else if (description.contentEquals("Emissione Bolletta"))
			textfield_found="YES";
		else if (description.contentEquals("Avvenuto Pagamento"))
			textfield_found="YES";
		else if (description.contentEquals("Scadenza Prossima Bolletta"))
			textfield_found="YES";
		else if (description.contentEquals("Sollecito Pagamento"))
			textfield_found="YES";
		else if (description.contentEquals("Avviso Consumi"))
			textfield_found="YES";
			
		if (textfield_found.contentEquals("NO"))
			throw new Exception("Checkbox headers are not displayed");
		}
		
	}
	
	public void verifyServiceList(By checkObject,String[] values) throws Exception{
		List <String> list =  Arrays.asList(values);
		String fieldValue = driver.findElement(checkObject).getText();
		//System.out.println(fieldValue);
		//System.out.println(list.size());
		for(int i=0;i<list.size();i++){
			//System.out.println(list.get(i));
		if(!list.contains(fieldValue))
			throw new Exception(fieldValue+" Service is unavailable ");
		}
	}
	
	public static final String servicesColor = "Crimson";
	

    public By DetaglioFornitura_310502654 = By.xpath("//a[contains(@onclick,'310502654')]/parent::div[@class='button-container']/child::a[contains(text(),'Dettaglio fornitura')]");
    public By luceHeading = By.xpath("//div[@class='heading supply']/child::h1[text()='Luce']");
    public By luceContent = By.xpath("//div[@class='heading supply']/descendant::dl[@class='supply-details-heading']");
    public By serviziBolletteHeading = By.xpath("//div[@class='section-heading']/child::h2[contains(text(),'Servizi per le bollette')]");
    public By seriviziBolletteContent = By.xpath("//div[@class='section-heading']/child::h2[contains(text(),'Servizi per le bollette')]/following-sibling::p[contains(text(),'Di seguito potrai')]");
    public By addebitoDirectolink = By.xpath("//h3[text()='Addebito Diretto']/following-sibling::span[text()='Non Attivo']");
    public By addebittoDiretto = By.xpath("//h3[text()='Addebito Diretto']/ancestor::a");
    public By addebittoDirettoLink = By.xpath("//span[@class='dsc-icon-addebito-diretto']");
    public By addebitoDirectoHeader = By.xpath("//div[@class='section-heading']/child::h2[text()='Addebito Diretto']");
    public By addebitoDirectoContent= By.xpath("//div[@class='section-heading']/child::p[contains(text(),'Il servizio Addebito')]");
    public By indrizzo00141  = By.xpath("//span[contains(text(),'VIA ABETONE 3')]/parent::span[@class='indirizzo']");
    public By numeroCliente310502654 = By.xpath("//span[contains(text(),'Numero cliente')]/following-sibling::span[text()='310502654']");
	public By visualizzaTuttemodalita = By.xpath("//button[contains(text(),'Visualizza tutte le modalita')]");
    public By attivaCorrente = By.xpath("(//span[text()='310502654']/ancestor::div[@class='list-container ingaggiModel']/following-sibling::div[@class='button-container']//span[text()='ATTIVA SU CONTO CORRENTE'])[1]");
    public By aTTIVASUCONTOCORRENTE = By.xpath("//span[text()='ATTIVA SU CONTO CORRENTE']");
    //public By checkBox = By.xpath("//span[starts-with(@id,'checkbox-item-')]");
    
    
    public By attivazioneHeader = By.xpath("//h1[contains(text(),'Attivazione Addebito Diretto')]");
    public By attivazioneContent = By.xpath("//h2[contains(text(),'Seleziona una o più forniture su')]");
    public By modificaCorrente00144 = By.xpath("(//span[text()='IT45K0307502200CC8500154420']/ancestor::div[@class='list-container ingaggiModel']/following-sibling::div[@class='button-container']//span[text()='MODIFICA CONTO CORRENTE'])[2]");
    public By indrizzo00144 = By.xpath("//span[contains(text(),'VIALE AFRICA 22')]/parent::span[@class='indirizzo']");
    public By ibanIT45K03 = By.xpath("(//span[contains(text(),'IBAN')]/following-sibling::span[text()='IT45K0307502200CC8500154420'])[2]");
    public By addebitoLink = By.xpath("//h2[contains(text(),'Servizi per le bollette')]/ancestor::section[@class='tile-container']/descendant::h3[text()='Addebito Diretto']");
    public By indirzzoModifica00144 = By.xpath("//span[contains(text(),'Viale Africa 22 00144 Roma Roma Rm')]");
    public By numeroCliente310492951 = By.xpath("//span[contains(text(),'Numero Cliente')]/following-sibling::span[@class='valore']//span[text()='310491502']");
    public By modalita = By.xpath("(//span[text()='Conto Corrente']/parent::span[@class='cliente'])[2]");
    public By attivasuCorrente = By.xpath("//span[text()='ATTIVA SU CONTO CORRENTE']");
    public By attivasucontopaypal = By.xpath("//span[text()='ATTIVA SU CONTO PAYPAL']");
   // public By attivazioneContent = By.xpath("//h2[contains(text(),'Seleziona una o più forniture su')]");
    public By attivasucartadipagamento= By.xpath("//span[text()='ATTIVA SU CARTA DI PAGAMENTO']");
   
    
    public By BenvenutoTitle = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Benvenuto nella')]");
	public By BenvenutoContent = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'In questa')]");
	public By fornitureHeader = By.xpath("//section[@id='lista-forniture']/descendant::h2[contains(text(),'Le tue forniture')]");
	public By fornitureContent = By.xpath("//section[@id='lista-forniture']/descendant::p[contains(text(),'Visualizza le informazioni')]");
    public By attivaIndrizzo = By.xpath("//span[@class='indirizzo']/child::span[contains(text(),'Via Abetone 3 00141 Roma Roma Rm')]");
    public By attivaNumero310502654 = By.xpath("//span[text()='Numero Cliente']/parent::span/descendant::span[text()='310502654']");
    public By checkbox310502654 = By.xpath("//span[text()='310502654']/ancestor::span[@class='cliente']/preceding-sibling::span[@class='icon-ckbox']");
    public By attivaAddebitoBtn = By.xpath("//span[text()='ATTIVA ADDEBITO']/parent::button[@class='Button button_first']");
    public By checkBox = By.xpath("//span[starts-with(@id,'checkbox-item-')]");
    public By cognomeLabel = By.xpath("//label[@id='cognome-intestatario' and contains(text(),'sociale Intestatario')]");
    public By cognomeInput = By.xpath("//input[@id='cognome-intestatarioInput']");
    public By nomeLabel = By.xpath("//label[@id='nome-intestatario']");
    public By nomeInput = By.xpath("//input[@id='nome-intestatarioInput']");
    public By codiceLabel = By.xpath("//label[@id='cf-intestatario']");
    public By codiceInput = By.xpath("//input[@id='cf-intestatarioInput']");
    public By ibanLabel = By.xpath("//label[@id='iban-intestatario']");
    public By ibanInput = By.xpath("//input[@id='iban-intestatarioInput']");
    public By ibanCheckbox = By.xpath("//input[@id='checkbox-unique-id-73']");
    public By agencyInputDisable = By.xpath("//input[@id='bank-agencyInput']");
    public By nomeBancaInputDisable = By.xpath("//input[@id='bank-nameInput']");
    public By continuaButton = By.xpath("//span[text()='CONTINUA']/parent::button[@class='button_first']");
    public By verificaHeader = By.xpath("//h4[contains(text(),'Verifica la correttezza')]");
    public By laModalitaHeader = By.xpath("//h4[contains(text(),'La modalità')]	");
    public By inserisciHeader = By.xpath("//h4[contains(text(),'Inserisci la tua')]");
    public By emailInput = By.xpath("//input[@id='email-intestatarioInput']");
    public By confermaButton = By.xpath("//span[text()='CONFERMA']/parent::button[@class='button_first']");
    public By operazioneHeader = By.xpath("//h3[contains(text(),'Operazione eseguita')]");
    public By confermaOperazioneContents = By.xpath("//h3[contains(text(),'Operazione eseguita')]/following-sibling::p[contains(text(),'La richiesta di attivazione')]");
    public By fineButton = By.xpath("//span[text()='FINE']/parent::button[@class='button_first']");
    public By fine1Button = By.xpath("//span[text()='Fine']/parent::button[@class='button_first']");
    public By popupHeader = By.xpath("//div[@id='modalAlert']/descendant::h3[contains(text(),'Operazione non consentita')]");
    public By popupContent = By.xpath("//div[@id='modalAlert']/descendant::p[contains(text(),'Siamo spiacenti')]");
    public By popupClose = By.xpath("//div[@id='modalAlert']/descendant::span[@class='dsc-icon-close-rounded']");
    public By iconCircle310492951 = By.xpath("//span[text()='310492951']/ancestor::span[@class='cliente']/parent::span//a/span[@class='icon-info-circle']");
    public By serivizLink = By.xpath("//span[text()='Servizi']/ancestor::a[@id='servizi']");
    
    //SalesForce
    public By podIT002E6365412A = By.xpath("//a[text()='Punti di Fornitura']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-m-bottom_small']//a[@title='IT002E6365412A']");
    public By dataCreatiozione = By.xpath("//span[text()='Elementi Operazione']/ancestor::div[@class='container forceRelatedListSingleContainer']/descendant::div[text()='Data creazione:'][1]/parent::div/descendant::span[contains(text(),'02/09/2020, 08:41')]");
    public By tipoOperazione = By.xpath("//span[text()='Elementi Operazione']/ancestor::div[@class='container forceRelatedListSingleContainer']/descendant::div[text()='Tipo Operazione:'][1]/parent::div/descendant::span[contains(text(),'Attivazione RID')]");
    public By tipoRecord = By.xpath("//span[text()='Elementi Operazione']/ancestor::div[@class='container forceRelatedListSingleContainer']/descendant::div[text()='Tipo di record:'][1]/parent::div/descendant::span[contains(text(),'Gestione RID')]");
    public By oiNumber = By.xpath("//span[text()='Elementi Operazione']/ancestor::div[@class='container forceRelatedListSingleContainer']/descendant::div[@class='outputLookupContainer forceOutputLookupWithPreview'][1]");
    
    public By processo = By.xpath("//span[@class='test-id__field-label'][contains(text(),'Processo')]");
    
    
    //379
    public By serviziForniture = By.xpath("//div[@class='section-heading']/child::h2[contains(text(),'Servizi per le forniture')]");
    public By serviziFornitureContent= By.xpath("//div[@class='section-heading']/child::h2[contains(text(),'Servizi per le forniture')]/following-sibling::p[contains(text(),'Di seguito potrai visualizzare')]");
    public By infoEnelEnergiaLink = By.xpath("//h3[contains(text(),'InfoEnelEnergia')]/ancestor::li[@class='tile']/child::a");
    public By infoEnelEnergiaHeader = By.xpath("//section[@id='landing-text']/descendant::h2[contains(text(),'InfoEnelEnergia')]");
    public By infoEnelEnergiaContent = By.xpath("//section[@id='landing-text']/descendant::p[contains(text(),'di ricevere gratuitamente')]");
    public By servizioInfoEnel = By.xpath("//div[@data-id='cardIngaggio_forniture']/descendant::p[1]");
    public By modificaInfoEnelEnergiaBtn = By.xpath("//span[text()='MODIFICA INFOENELENERGIA']/parent::button[@class='button_first']");
    public By modificaTitle = By.xpath("//h1[@id='titolo-dispositiva' and contains(text(),'Modifica')]");
    public By modificaContent = By.xpath("//div[@class='list-form-container']/child::h2[contains(text(),'Il servizio InfoEnelEnergia')]");
    public By esciButton = By.xpath("//span[text()='Esci']/parent::button[@class='button_second']");
    public By linkEsci = By.xpath("//span[text()='Esci']/parent::a[@id='disconnetti']");
    public By continuaBtn = By.xpath("//span[text()='CONTINUA']/parent::button[@type='button']");
    public By emailAttivo = By.xpath("//span[text()='Email/Cellulare']/parent::span[@class='cliente']");
    public By numero300001597 = By.xpath("//span[text()='300001597']/ancestor::span[@class='cliente']");
    
    public By communica = By.xpath("//span[contains(text(),'Comunica Lettura')]");
    public By communicaSMSLabel = By.xpath("//input[@id='paymentNoticeSMS']/following-sibling::label[contains(text(),'shallReadSMS')]");
    public By communicaEmail = By.xpath("//input[@id='shallReadEmail']/following-sibling::label[contains(text(),'Email')]");
    public By emmissione = By.xpath("//span[contains(text(),'Emissione Bolletta')]");
    public By emmissioneSMSLabel = By.xpath("//input[@id='issueBillSMS']/following-sibling::label[contains(text(),'SMS')]");
    public By emissionEmail = By.xpath("//input[@id='issueBillEmail']/following-sibling::label[contains(text(),'Email')]");
    public By avvenuto = By.xpath("//span[contains(text(),'Avvenuto Pagamento')]");
    public By avvenutoSMS = By.xpath("//input[@id='paymentNoticeSMS']/following-sibling::label[contains(text(),'SMS')]");
    public By avvenutoEmail = By.xpath("//input[@id='paymentNoticeEmail']/following-sibling::label[contains(text(),'Email')]");
    public By scandenza = By.xpath("//span[contains(text(),'Scadenza Prossima Bolletta')");
    public By scandenzaSMS = By.xpath("//input[@id='nextDeadlineEmail']/following-sibling::label[contains(text(),'Email')]");
    public By scandenzaEmail = By.xpath("//input[@id='nextDeadlineSMS']/following-sibling::label[contains(text(),'SMS')]");
    public By sollecito = By.xpath("//span[contains(text(),'Sollecito Pagamento')]");
    public By sollecitoSMS = By.xpath("//input[@id='requestPaymentSMS']/following-sibling::label[contains(text(),'SMS')]");
    public By sollecitoEmail = By.xpath("//input[@id='requestPaymentEmail']/following-sibling::label[contains(text(),'Email')]");
    public By avviso = By.xpath("//span[contains(text(),'Avviso Consumi')]");
    public By avvisoSMS = By.xpath("//input[@id='consumptionsSMS']/following-sibling::label[contains(text(),'SMS')]");
    public By avvisoEmail = By.xpath("//input[@id='consumptionsEmail']/following-sibling::label[contains(text(),'Email')]");
    public By inserisci = By.xpath("//h4[contains(text(),'Inserisci i tuoi dati')]");
    
    //279
    public By checkboxHeader = By.xpath("//span[@class='bold']");
    public By checkboxInput = By.xpath("//div[@class='check-container']/child::input");
    public By checkBoxLabel = By.xpath("//div[@class='check-container']/child::label");
    public By checkboxSMSFirst = By.xpath("(//input[@id='shallReadSMS']/following::label[contains(text(),'SMS')])[1]");
    public By continuButton = By.xpath("//span[contains(text(),'Continua')]/parent::button");
    public By indietroButton = By.xpath("//span[contains(text(),'Continua')]/parent::button/preceding-sibling::button[contains(text(),'INDIETRO')]");
    public By billSMSCheckboxLabel = By.xpath("//input[@id='issueBillSMS']/following-sibling::label[contains(text(),'SMS')]");
    public By checkboxHeaders = By.xpath("//span[@class='full-width']/dt");
    public By checkboxValues = By.xpath("//span[@class='full-width']/dd");
    public By inputEmail = By.xpath("//span[@id='emailFieldDesc']/preceding-sibling::input[@id='emailFieldInput' and  contains(@placeholder,'Inserisci')]");
    public By confermaEmail = By.xpath("//input[@id='emailFieldConfirmInput']");
    public By aggiornaCheckboxInput = By.xpath("//label[contains(text(),'Aggiorna dati')]/preceding-sibling::input[@id='checkBoxAggiorna i dati anagrafici']");
    public By aggironaCheckboxLabel = By.xpath("//label[contains(text(),'Aggiorna dati')]");
    public By cellulareField = By.xpath("//span[@id='mobileFieldDesc']/preceding-sibling::input[@id='mobileFieldInput']");
    public By cellulareConfirmField = By.xpath("//span[@id='mobileFieldConfirmDesc']/preceding-sibling::input[@id='mobileFieldConfirmInput']");
    
    
    public By ModificaHeader = By.xpath("//h2[contains(text(),'Stai effettuando')]");
    public By conferButton = By.xpath("//span[text()='Conferma']/parent::button");
    public By indieButton = By.xpath("//span[text()='INDIETRO']/parent::button");
    public By modificaInfoEnelEnergiaHeader = By.xpath("//h1[@id='titolo-dispositiva']");
    public By modificaInfoEnelEnergiaContent = By.xpath("//h2[contains(text(),'Il servizio InfoEnelEnergia')]");
    public By tipologiaHeader = By.xpath("//h4[contains(text(),'Tipologia di avviso')]");
    public By smsEmailHeader = By.xpath("//span[@class='full-width']//dt");
    public By smsEmailValue = By.xpath("//span[@class='full-width']//dd");
    public By supplyData = By.xpath("//h4[contains(text(),'InfoEnelEnergia verrà')]");
    public By energiaIndietroBtn = By.xpath("//div[@class='button-container']/child::button[text()='INDIETRO']");
    public By energiaConfermaBtn = By.xpath("//span[text()='Conferma']/parent::button");
    public By radioBtnGas = By.xpath("//span[@id='checkbox-item-0']");
    
    //326
    public By dettaglioFornitura_300001597 = By.xpath("//a[contains(@onclick,'300001597') and contains(text(),'Dettaglio fornitura')]");
    public By indrizzo_40017 = By.xpath("//span[contains(text(),'Indirizzo della fornitura')]/following-sibling::span[contains(text(),'VIA VIRGINIA 12/A')]");
	public By numeroClient_300001597 = By.xpath("//span[contains(text(),'Indirizzo della fornitura')]/following::span[contains(text(),'300001597')]");
	
	public By nonActiveSupplyContent = By.xpath("//div[@id='accessibility']/descendant::*[contains(text(),'servizio Addebito')]");
	public By attivaSuCartaButton = By.xpath("(//span[contains(text(),'ATTIVA SU CARTA DI PAGAMENTO')]/parent::button[@class='button_first'])[1]");
	//327
	public By attivazioneindietroButton = By.xpath("//span[text()='INDIETRO']/parent::button[@class='button_second']");
	public By cognomeLabel1 = By.xpath("//label[@id='cognome-intestatario' and contains(text(),'sociale Intestatario')]");
	public By verificacongnomeValue = By.xpath("//dd[contains(text(),'ARANDIA')]");
	public By verificaCongnomelabel = By.xpath("//dt[contains(text(),'Cognome')]");
	public By verificaNomeLabel = By.xpath("//dt[contains(text(),'Nome Intestatario')]");
	public By verificaNomeValue = By.xpath("//dd[contains(text(),'LUIS CARLOS')]");
	public By veroficaCcodiceLabel = By.xpath("//dt[contains(text(),'Codice Fiscale')]");
	public By verificaCcodiceValue = By.xpath("//dd[contains(text(),'RNDLCR55L07Z601K')]");
	public By verificaIbanLabel = By.xpath("//dt[contains(text(),'IBAN')]");
	public By verificaIbanValue = By.xpath("//dd[contains(text(),'IT30P0301503200000000246802')]");
	
	//343
	public By seVuoiContent = By.xpath("//div[@class='button-container']/following-sibling::p[contains(text(),'Se vuoi disattivare')]");
	public By attivoContent = By.xpath("//div[@id='accessibility']/child::p[contains(text(),'Il servizio Addebito Diretto')]");
	public By indrizzo_00135 = By.xpath("//span[contains(text(),'Indirizzo della fornitura')]/following-sibling::span[contains(text(),'VIA ABANO TERME 1 - 00135 ROMA RM')]");
	public By iban_it58 =By.xpath("//span[text()='IBAN']/following-sibling::span[contains(text(),'IT58U0200803292000104571038')]");
	public By modificaCorrenteButton = By.xpath("//span[text()='MODIFICA CONTO CORRENTE']/parent::button[@class='button_second']");
	public By avviaRichestaLink = By.xpath("//a[contains(text(),'avvia la richiesta')]");
	public By dissativaDiretto = By.xpath("//h1[contains(text(),'Disattiva Addebito Diretto')]");
	public By dissativoDirettoContent = By.xpath("//h2[contains(text(),'Seleziona la fornitura su cui')]");
	public By Radiobtn_310490795 = By.xpath("//span[text()='310490795']/ancestor::span[@class='cliente']/preceding-sibling::span[@id='checkbox-item-0']");
	public By Radiobtn_310499155 = By.xpath("//span[text()='310499155']/ancestor::span[@class='cliente']/preceding-sibling::span[@id='checkbox-item-1']");
	public By disattivoAddebitoBtn = By.xpath("//span[text()='DISATTIVA ADDEBITO']/parent::button[@class='Button button_first']");
	
	public By disattivandoContent = By.xpath("//span[@class='icon-info-circle']/following-sibling::p[contains(text(),'Disattivando il servizio')]");
	public By disattivandoContent1 = By.xpath("//h4[contains(text(),'Il servizio verrà disattivato sulla seguente fornitura')]");
	public By numero_310490795 = By.xpath("//span[contains(text(),'Numero cliente')]/following-sibling::span[text()='310490795']");
	public By disattivaConfermaButton = By.xpath("//span[contains(text(),'CONFERMA')]/parent::button[@class='button_first']");
	public By direttoFineContent = By.xpath("//h3[@id='id_title']/following-sibling::p[contains(text(),'La richiesta di disattivazione')]");
	public By attivaCorrentebtn_310490795 = By.xpath("//button[contains(@onclick,'310490795')]/preceding-sibling::button[@class='button_first']");
	public By checkbox_310490795  = By.xpath("//span[text()='310490795']/ancestor::span[@class='cliente']/preceding-sibling::span[@id='checkbox-item-3']");
	public By attivaAddebitoButton = By.xpath("//span[text()='ATTIVA ADDEBITO']/parent::button[@class='Button button_first']");
	
	
	
	
	public static final String DIRETTOFINE_CONTENT = "La richiesta di disattivazione dell'addebito diretto è stata inoltrata al tuo istituto di credito. Dalla prossima bolletta riceverai il bollettino per il pagamento.Ricordati che puoi attivare o modificare l'Addebito Diretto anche dall'APP di Enel Energia. Se ancora non ce l'hai, scaricala gratuitamente e scopri tutti i servizi e i vantaggi dedicati a te.";
	public static final String NUMERO_310490795 = "310490795";
	public static final String DISATTVANDO_CONTENT1 ="Il servizio verrà disattivato sulla seguente fornitura:";
	public static final String DISATTVANDO_CONTENT = "Disattivando il servizio tornerai alla modalità di pagamento che prevede il bollettino premarcato. Verrà inoltre addebitato il costo del deposito cauzionale nella prossima bolletta.";
	public static final String DISATTIVADIRETTO_CONTENT = "Seleziona la fornitura su cui intendi revocare il servizio di addebito della bolletta sul tuo conto corrente. Puoi effettuare questa operazione solo su una fornitura alla volta.";
	public static final String DISSATIVA_DIRETTO = "Disattiva Addebito Diretto";
	public static final String IBAN_IT58= "IT58U0200803292000104571038";
	public static final String INDRIZZO_00135 = "VIA ABANO TERME 1 - 00135 ROMA RM";
	public static final String ATTIVO_CONTENT = "Il servizio Addebito Diretto non è attivo sulla fornitura:";
	public static final String SEVUOI_CONTENT ="Se vuoi disattivare il servizio, avvia la richiesta";
	public static final String VERIFICALA_HEADER= "Verifica la correttezza delle informazioni inserite:";
	public static final String VERIFICACONGNOME_VALUE = "ARANDIA";
	public static final String VERIFICANOME_VALUE = "LUIS CARLOS";
	public static final String VERIFICACODICE_VALUE = "RNDLCR55L07Z601K";
	public static final String VERIFICAIBAN_VALUE = "IT30P0301503200000000246802";
    public static final String CHECKBOX_LABEL = "Aggiorna dati anagrafici";
    public static final String TIPOLOGIA_HEADER = "Tipologia di avviso";
    public static final String MODIFICA_INFOENELHEADER = "Modifica InfoEnelEnergia";
    public static final String MODIFICA_INFOENELCONTENT = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
    public static final String SUPPLY_DATA = "InfoEnelEnergia verrà aggiornato su:";
    public static final String MODIFICA_HEADER = "Modifica InfoEnelEnergia";
    public static final String COMMUNICA_TITLE = "Comunica Lettura";
    public static final String 	EMISSIONE_TITLE = "Emissione Bolletta";
    public static final String MODIFICAENERGIA_HEADER = "Modifica InfoEnelEnergia";
    public static final String MODIFICAENERGIA_CONTENT = "Stai effettuando la modifica del servizio di InfoEnelEnergia";
    public static final String  INFOENELENERGIA_HEADER = "InfoEnelEnergia";
    public static final String INFOENELENERGIA_CONTENT = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
    public static final String MODIFICA_TITLE = "Modifica";
    public static final String MODIFICA_CONTENT = "Il servizio InfoEnelEnergia ti permette di ricevere gratuitamente le notifiche utili alla gestione delle tue forniture ricevendo le informazioni direttamente tramite SMS o tramite email.";
    public static final String SERVIZI_FORNITURE = "Servizi per le forniture";
    public static final String SERVIZIFORNITURE_CONTENT = "Di seguito potrai visualizzare tutti i servizi per la gestione delle tue forniture";
    public static final String POPUP_HEADER = "Operazione non consentita";
    public static final String POPUP_CONTENT = "Siamo spiacenti, ma non è possibile avviare la richiesta poiché ne risulta un'altra in corso. Non appena concluso sarà possibile effettuare una nuova richiesta. Per maggiori informazioni chiedi al nostro supporto in chat.";
    public static final String CONFERMAOPERAZIONE_CONTENTS = "La richiesta di attivazione dell'addebito diretto è stata inoltrata al tuo istituto di credito che dovrà autorizzarla. Puoi verificare lo stato di attivazione del servizio navigando nel dettaglio della tua fornitura. Ricordati che puoi attivare o modificare l'Addebito Diretto anche dall'APP di Enel Energia. Se ancora non ce l'hai, scaricala gratuitamente e scopri tutti i servizi e i vantaggi dedicati a te.";
    public static final String CONFERMAOPERAZIONE_CONTENTS1 = "La richiesta di attivazione dell'addebito diretto è stata inoltrata al tuo istituto di credito che dovrà autorizzarla. Puoi verificare lo stato di attivazione del servizio navigando nel dettaglio della tua fornitura.Ricordati che puoi attivare o modificare l'Addebito Diretto anche dall'APP di Enel Energia. Se ancora non ce l'hai, scaricala gratuitamente e scopri tutti i servizi e i vantaggi dedicati a te.";
    public static final String INSERISCI_HEADER = "Inserisci la tua email per ricevere informazioni sullo stato della tua richiesta:";
    public static final String LAMODALITA_HEADER = "La modalità di pagamento verrà aggiornata su:";
    public static final String VERIFICA_HEADER = "Verifica la correttezza delle informazioni inserite:";
    public static final String NOMEBANCALABEL = "Nome banca";
    public static final String AGENCYLABEL = "Agenzia";
    public static final String IBANLABEL = "IBAN*";
    public static final String CODICELABEL = "Codice fiscale (Avente poteri di firma)*";
    public static final String NOMELABEL = "Nome Intestatario C/C*";
    public static final String CONGNOME_LABEL= "Cognome/Ragione sociale Intestatario C/C*";
    public static final String ATTIVA_INDRIZZO = "Via Abetone 3 00141 Roma Roma Rm";
    public static final String ATTIVA_NUMERO = "310502654";
	public static final String ATTIVAZIONE_HEADER = "Attivazione Addebito Diretto";
	public static final String ATTIVAZIONE_CONTENT= "Seleziona una o più forniture su cui intendi attivare il servizio di addebito della bolletta sul tuo conto corrente.";
	public static final String INDRIZZO_VALUE = "Indirizzo della fornitura VIA ABETONE 3 - 00141 ROMA RM";
	public static final String NUMEROCLIENTE_VALUE= "310502654";
	public static final String SERVIZIBOllE_HEADING = "Servizi per le bollette";
	public static final String SERVIZIBOLLE_CONTENT = "Di seguito potrai visualizzare tutti i servizi per gestire le tue bollette";
	public static final String ADDEBITTODIRECTO_HEADER = "Addebito Diretto";
	public static final String ADDEBITTODIRECTO_CONTENT = "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sulla tua Carta di pagamento, sul tuo Conto Corrente o sul tuo conto Paypal.";
	public static final String LUCE_HEADING = "Luce";
	public static final String LUCE_CONTENT = "L’indirizzo della tua fornitura luce è:VIA ABETONE 3 - 00141 ROMA RM Numero Cliente:310502654";
    public static final String BENVENUTO_TITLE = "Benvenuto nella tua area privata";
	public static final String BENVENUTO_CONTENT = "In questa sezione potrai gestire le tue forniture";
	public static final String FORNITURE_HEADER = "Le tue forniture";
	public static final String FORNITURE_CONTENT = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String OPERAZIONE_HEADER = "Operazione eseguita correttamente";
	public static final String OPERAZIONE_CONTENTS = "La tua richiesta è stata presa in carico Il processo di disattivazione della fornitura è stato avviato. Ti ricordiamo che per essere effettivo il processo potrà impiegare da 1 a 9 giorni. Ti arriverà una mail di riepilogo per tutte le forniture su cui è stata presa in carico la disattivazione.";
	public static final String ACTIVEPAYMENT_CONTENT = "Il servizio Addebito Diretto su Carta di Credito è attivo sulla fornitura:";
	public static final String OPERAZIONE_CONTENT = "Operazione eseguita correttamente";
	public static final String LARICHIESTA_CONTENT = "La richiesta di modifica dell’addebito diretto sulla carta di credito è stata inviata.";
	public static final String INDRIZZOMODIFICA_00144 = "Viale Africa 22 00144 Roma Roma Rm";
	public static final String INDRIZZO_00144 = "Indirizzo della fornitura VIALE AFRICA 22 - 00144 ROMA RM";
	public static final String IBAN_IT45K03 = "IT45K0307502200CC8500154420";
    public static final String NUMERO_310492951 = "310491502";
    public static final String MODALITA = "Modalità Addebito DirettoConto Corrente"; 
    public static final String AddebitoDirettoTtile = "Addebito Diretto"; 
    public static final String AddebitoDirettoSubTtile = "Il servizio Addebito Diretto ti consente di addebitare l'importo delle tue bollette sul Tuo Conto Corrente";
    public static final String ModificaSubTitle = "Vuoi modificare il servizio anche per le altre forniture?";
    public static final String ModificaSubTitle2 = "Seleziona le forniture su cui vuoi cambiare la modalità di pagamento.";
    public static final String SignSubtitle = "Modalità di pagamento";
    public static final String SignSubtitle2 = "Addebito Diretto su conto corrente:";
    public static final String NONACTIVESUPPLY_CONTENT  = "Il servizio Addebito Diretto non è attivo sulla fornitura:";
    // new line 
    public static final String ErrorText = "Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";
    public static final String[] services = {"Autolettura","InfoEnelEnergia","Modifica Indirizzo Fatturazione","Modifica Potenza e/o Tensione","Modifica Contatti e Consensi","Cambio Piano ENEL ONE","Bolletta Web","Pagamento Online","Addebito Diretto","Rettifica Bolletta","Rateizzazione","Bolletta di Sintesi o di Dettaglio","Duplicato Bolletta","Indirizzo e-mail di recapito","Invio Documenti","Disattivazione Fornitura"};

    //339
    
    public static final String AddebitoDirettoSubTtile1 = "Il servizio di Addebito Diretto è già attivo.";
    public static final String AddebitoDirettoSubTtile2 = "Il servizio Addebito Diretto su Conto Corrente è attivo sulla fornitura:";
    public static final String FinePage_Text1 = "Operazione eseguita correttamente.";
    public static final String FinePage_Text2 = "La tua richiesta è stata presa in carico ed è stata inoltrata al tuo istituto di credito che dovrà autorizzarla. Segui l'avanzamento della tua richiesta nella sezione STATO RICHIESTE";
    public static final String ErrorText1 = "Codice IBAN non corretto. Non è possibile inserire più di 32 cifre";
    public static final String ErrorText2 = "non è possibile gestire Iban esteri non aderenti al circuito europeo (SEPA)";
    public static final String ErrorText3 = "Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";
    public static final String ErrorText4 = "Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";
    public static final String ErrorText5 = "Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";
    public static final String ErrorText6 = "Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";
    public static final String ErrorText7 = "Ci scusiamo per il malfunzionamento, ma non è possibile evadere la richiesta in questo momento. Si prega di riprovare più tardi.";
    
    
    
    
}


