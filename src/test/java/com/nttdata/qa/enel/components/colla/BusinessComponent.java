package com.nttdata.qa.enel.components.colla;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class BusinessComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
    private List<By> items;
	
	public By home = By.xpath("//*[@title='Home' and text()='Home']");
	public By business = By.xpath("//*[@title='Business' and text()='Business']");
	public By title = By.xpath("//*[text()='Vicini alla tua impresa con Open Energy']");
	public By subTitle = By.xpath("//*[contains(text(),'Professionisti e PIVA pagano la componente energia come la paghiamo noi di Enel e hanno i primi tre mesi di abbonamento gratis')]");
	

	
	public BusinessComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		this.items = new ArrayList<By>(); 
		this.items.add(this.home);
		this.items.add(this.business);
		this.items.add(this.title);
		this.items.add(this.subTitle);
	}
 
 public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
 public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
    
    public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, false);
	}
    
    public void checkQuestionAndFilter(String property1,String property2,String property3) throws Exception{
    	
        WebElement obj = util.waitAndGetElement(qst1);

    	String result = "NO";
    	
    	Map map = new HashMap<>();
    	/*map.put("Che contratto vuoi attivare?", "Luce");
    	map.put("Dove?", "Casa");
    	map.put("Per quale necessità?", "Visualizza tutte");*/
    	map.put(driver.findElement(qst1).getText(), driver.findElement(ans1).getText());
    	map.put(driver.findElement(qst2).getText(), driver.findElement(ans2).getText());
    	map.put(driver.findElement(qst3).getText(), driver.findElement(ans3).getText());
    	
    	String ans1=(String) map.get(driver.findElement(qst1).getText());
    	String ans2 = (String) map.get(driver.findElement(qst2).getText());
    	String ans3 = (String) map.get(driver.findElement(qst3).getText());
    	
    	if(property1.equals(ans1) && property2.equals(ans2) && property3.equals(ans3)){
    		
    		result = "YES";
    	}
    	if(result.equals("NO")){
    		throw new Exception("Question and Filter is not matching");
    	}
    	
    	
    }

    public void checkMenuPath(By checkObject,String path) throws Exception {
        WebElement obj = util.waitAndGetElement(checkObject);
        String pathDescription=obj.getText(); 
        pathDescription= pathDescription.replaceAll("(\r\n|\n)", " ");    

     //   System.out.println("URl "+driver.getCurrentUrl()+"Path"+obj.getAttribute("Path"));
        if (!pathDescription.equalsIgnoreCase(path))
            throw new Exception("object path with xpath " + luceegas + " is not exist.");
    }
    
    public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
    
    public void clickWebElement(WebElement we){
    	we.click();
    }
    
	public void verifyMenuItems() throws Exception{
		for(By by: items){
			this.verifyComponentExistence(by);
		}
	}
    
    

}
