package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class IntestazioneNuovaRichiestaComponent {
	//La classe viene utilizzata per verificare l'intestazione di una richiesta appena creata
	//e viene recuperato il numero richiesta generato e salvato nel property
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By h1IntestazionePaginaNuovaRichiesta = By.xpath("//div[@id='headerContainer']/h1");
	public By pNRichiestaPaginaNuovaRichiesta = By.xpath("//*[contains(text(),'N. RICHIESTA')] | //*[contains(text(),'N. RICHESTA')]");
	public By pNRichiestaPaginaNuovaRichiesta2 = By.xpath("//*[contains(text(),'N. Richiesta')] | //*[contains(text(),'N. RICHIESTA')]");
	public By pNRichiestaPaginaNuovaRichiesta3 = By.xpath("//p[contains(text(),'REQUEST N.')]");
	public By pNRichiestaPaginaNuovaRichiesta4 = By.xpath("//p[contains(text(),'Richiesta Id')]");
	public By pNRichiestaPaginaNuovaRichiesta5 = By.xpath("//*[contains(text(),'N.RICHIESTA')] | //*[contains(text(),'N.Richiesta')]");
	public By pNRichiestaPaginaNuovaRichiesta6 = By.xpath("//p[contains(text(),'N.Richiesta')]");
	public By pNRichiestaPaginaNuovaRichiesta7 = By.xpath("/html/body/form/div[3]/div[2]/div/div[1]/div/div[2]/p");
	
	public By pNRichiestaPaginaNuovaRichiestaGestionePrivacy = By.xpath("//h1[@id='headerCallerIdentification']");
	
	public String intestazionePaginaAvvioEstrattoConto = "Avvio Estratto Conto";
	public String intestazionePaginaAvvioAttivazioneVAS = "Avvio Attivazione VAS";
	public String intestazionePaginaAvvioGestioneProvacy = "Avvio Gestione Privacy";
	public String intestazionePaginaAvvioEstrattoContoCanoneTV ="Avvio Estratto Conto Canone TV";
	public String intestazionePaginaAvvioRettificaMercato = "Avvio Rettifica Mercato";
	public String intestazionePaginaAvvioAttivazioneSDD = "Avvio Attivazione SDD";
	public String intestazionePaginaAvvioVariazioneSDD = "Avvio Variazione SDD";
	public String intestazionePaginaAvvioRevocaSDD = "Avvio Revoca SDD";
	public String intestazionePaginaAvvioVerificaContatoreEle = "Avvio Verifica Contatore ELE";
	public String intestazionePaginaAvvioVerificaContatoreGas = "Avvio Verifica Contatore GAS";
	public String intestazionePaginaAvvioModificaAnagrafica = "Avvio Modifica Anagrafica";
	public String intestazionePaginaAvvioVolturaConAccollo = "Avvio Voltura con accollo";
	public String intestazionePaginaAvvioCopiaDocumenti = "Avvio Copia Documenti";
	public String intestazionePaginaAvvioModificaStatoResidenza = "Avvio Modifica Stato Residenza";
	public String intestazionePaginaAvvioModificaImpiantoGas = "Avvio Modifica Impianto Gas";
	// public String intestazionePaginaAvvioSpostamentoContatoreELE = "Avvio Spostamento Contatore ELE";
	public String intestazionePaginaAvvioSpostamentoContatoreELE = "Spostamento Contatore ELE";
	
	public IntestazioneNuovaRichiestaComponent(WebDriver driver) {
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
	}
	
	public void verificaIntestazioneNuovaRichiesta(By intestazionepagina,String intestazioneattesa) throws Exception 
	{
//		FluentWait<WebDriver> wait = new WebDriverWait(this.driver,100).ignoring(WebDriverException.class).pollingEvery(2,TimeUnit.SECONDS);
//		//wait.until(ExpectedConditions.elementToBeClickable(intestazionepagina));
//		wait.until(ExpectedConditions.presenceOfElementLocated(intestazionepagina));
		util.waitUntilIsDisplayed(intestazionepagina);
		//Verifico corretta intestazione
		String intestazione=null;
		intestazione=driver.findElement(intestazionepagina).getText();
//		//System.out.println("Intestazione mostrata:"+intestazione);
//		//System.out.println("Intestazione attesa:"+intestazioneattesa);
		
		if(intestazione.compareToIgnoreCase(intestazioneattesa)!=0){
			throw new Exception("L'intestazione mostrata:"+intestazione+" è diversa da quella attesa:"+intestazioneattesa);
		//Salvo numero richiesta
		}
			
	}

	public String salvaNumeroRichiestaEC(By nRichiesta) throws Exception{
		
		TimeUnit.SECONDS.sleep(5);
		String numerorichiesta=null;
		String labelnumerorichiesta="N.Richiesta";
		System.out.println("Seleziono il Frame");
		util.getFrameActive();
		System.out.println("Attendo il Numero_Richiesta visibile");
		util.waitUntilIsDisplayed(nRichiesta);
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
//		System.out.println(numerorichiesta);
		TimeUnit.SECONDS.sleep(5);
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
		System.out.println("NUMERO_RICHIESTA="+numerorichiesta);
		numerorichiesta=numerorichiesta.replace(" ", ""); 
		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}

	public String salvaNumeroRichiesta(By nRichiesta) throws Exception{
		
		TimeUnit.SECONDS.sleep(10);
		String numerorichiesta=null;
		String labelnumerorichiesta="N.Richiesta";
		util.waitUntilIsDisplayed(nRichiesta);
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
//		System.out.println(numerorichiesta);
		TimeUnit.SECONDS.sleep(5);
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		numerorichiesta=numerorichiesta.replace("A: ", ""); 
		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiesta2(By nRichiesta) throws Exception{
		String numerorichiesta="";
		String labelnumerorichiesta="N. RICHIESTA";
		util.waitUntilIsDisplayed(nRichiesta);
		Thread.currentThread().sleep(5000);
		int x = 5;
		while(x-- >0 && numerorichiesta.length()<1) {
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
		Thread.currentThread().sleep(5000);
		}
	    if(numerorichiesta.length()<1) throw new Exception("Impossibile prelevare il numero richiesta. Probabile sistema lento.");
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiesta3(String frame,By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="Richiesta Id";
//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		numerorichiesta=driver.findElement(nRichiesta).getText();
		driver.switchTo().defaultContent();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "Richiesta Id:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiesta(String frame,By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="N. RICHIESTA";
//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		numerorichiesta=driver.findElement(nRichiesta).getText();
		driver.switchTo().defaultContent();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());

		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}
	
	public String salvaNumeroRichiestaAllaccio(By nRichiesta) throws Exception{
		String numerorichiesta=null;
		String labelnumerorichiesta="REQUEST N.";
		numerorichiesta=driver.findElement(nRichiesta).getText();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
//		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "N richiesta:"+numerorichiesta);
		return numerorichiesta;
	}

	public String salvaNumeroRichiestaFrame(String frame, By nRichiesta) throws Exception{
		String numerorichiesta=null; 
		String labelnumerorichiesta="N. RICHIESTA";
		
//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		util.waitUntilIsDisplayed(nRichiesta);
		numerorichiesta=util.waitAndGetElement(nRichiesta).getText();
		numerorichiesta=numerorichiesta.substring(numerorichiesta.lastIndexOf(labelnumerorichiesta)+labelnumerorichiesta.length()+1, numerorichiesta.length());
		driver.switchTo().defaultContent();
		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, labelnumerorichiesta+":"+numerorichiesta);
		return numerorichiesta;
	}
}
