package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;


/**
 * Il seguente componente si occupa di gestire l'inserimento del tipo delega
 */
public class DelegationManagementComponentEVO extends BaseComponent {

    private WebDriver driver;
    private SeleniumUtilities util;
    private SpinnerManager spinnerManager;
    String del;

    public DelegationManagementComponentEVO(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
        spinnerManager = new SpinnerManager(driver);
    }

    public By selectTipoDelega = By.xpath("//label[contains(text(),'Tipo delega')]/..//input");
    public By inputTipoDelega = By.xpath("//h2[text()='Delegato']/ancestor::div[@class='slds-card']//label[contains(text(),'Tipo delega')]/..//input[@disabled]");
    public By confermaButton = By.xpath("//h2[text()='Delegato']/ancestor::div[@role='region']//button[text()='Conferma']");
    public By avantiButton = By.xpath("//div[text()='Delegato']/..//input[@value='Avanti' ]");
    public By nome = By.xpath("//*[contains(@data-id,'ITA_IFM_Delegate_FirstName__c')]/..//input | //*[contains(@data-label,'ITA_IFM_Delegate_FirstName__c')]/..//input");
    public By cognome = By.xpath("//*[contains(@data-id,'ITA_IFM_Delegate_LastName__c')]/..//input | //*[contains(@data-label,'ITA_IFM_Delegate_LastName__c')]/..//input");
    public By codiceFiscale = By.xpath("//*[contains(@data-id,'ITA_IFM_Delegate_FiscalCode__c')]/..//input | //*[contains(@data-label,'ITA_IFM_Delegate_FiscalCode__c')]/..//input");
    public By telefono = By.xpath("//label[contains(text(),'Telefono')]/..//input");
    public By tipologiaDelegato = By.xpath("//label[contains(text(),'Tipo delegato')]/../..//input");
    public By selectTipoDocumento = By.xpath("//label[contains(text(),'Tipo Documento')]/../..//input");
    public By numeroDocumento = By.xpath("//*[contains(@data-id,'ITA_IFM_Delegate_IdentityCard__c')]/..//input | //*[contains(@data-label,'ITA_IFM_Delegate_IdentityCard__c')]/..//input");
    public By rilasciatoDA = By.xpath("//*[contains(@data-id,'ITA_IFM_DelegateDocReleasedBy__c')]/..//input | //*[contains(@data-label,'ITA_IFM_DelegateDocReleasedBy__c')]/..//input");
    public By rilasciatoIL = By.xpath("//h2[text()='Delegato']/ancestor::div[@role='region']//label[contains(text(),'Rilasciato il')]/..//input");
    public By delegatoAltro = By.xpath("//label[contains(text(),'Tipo delegato altro')]/../..//input");
    public By confermaPagina = By.xpath("//lightning-button[@data-id='Confirm']");


    public void selezionaDelega(String frame, String tipoDelega) throws Exception {
        TimeUnit.SECONDS.sleep(7);
        util.frameManager(frame, selectTipoDelega, util.select, tipoDelega);
    }

    public void selezionaDelega(String tipoDelega) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        WebElement element = util.waitAndGetElement(selectTipoDelega, 60, 2);
        element.sendKeys(tipoDelega);
        TimeUnit.SECONDS.sleep(3);
        element.sendKeys(Keys.ENTER);
        spinnerManager.waitForSpinnerBySldsHide(spinnerManager.dotSpinner);
    }

    public void valorizzaDelegato() throws Exception {
        String Documento = "Carta D’Identita'";
        String Nome = "Michela";
        String Cognome = "Pigna";
        String CF = "PGNMHL80A41H501J";
        String Telefono = "3458696012";
        String NumeroDocumento = "80380000";
        String RilasciatoDA = "Comune";
        String RilasciatoIL = "20/04/2020";
        String TipologiaDelegato = "Altro";
        String Altro = "Xxxxxx";
        TimeUnit.SECONDS.sleep(3);
        util.verifyText(nome, Nome);
        util.verifyText(cognome, Cognome);
        util.verifyText(codiceFiscale, CF);
        WebElement tipoDelegato = driver.findElement(tipologiaDelegato);
        tipoDelegato.sendKeys(TipologiaDelegato);
        tipoDelegato.sendKeys(Keys.ENTER);
        WebElement tipoDoc = driver.findElement(selectTipoDocumento);
        tipoDoc.sendKeys(Documento);
        TimeUnit.SECONDS.sleep(3);
        tipoDoc.sendKeys(Keys.ENTER);
        util.objectManager(numeroDocumento, util.scrollAndSendKeys, NumeroDocumento);
        util.objectManager(rilasciatoDA, util.scrollAndSendKeys, RilasciatoDA);
        util.objectManager(rilasciatoIL, util.scrollAndSendKeys, RilasciatoIL);
        util.objectManager(delegatoAltro, util.scrollAndSendKeys, Altro);
        TimeUnit.SECONDS.sleep(3);
        spinnerManager.waitForSpinnerBySldsHide(spinnerManager.dotSpinner);
    }

    public void valorizzaDelegatoStrong() throws Exception {
        String Documento = "Carta D’Identita'";
        String Nome = "Michela";
        String Cognome = "Pigna";
        String CF = "PGNMHL80A41H501J";
        String Telefono = "3458696012";
        String NumeroDocumento = "80380000";
        String RilasciatoDA = "Comune";
        String RilasciatoIL = "20/04/2020";
        TimeUnit.SECONDS.sleep(3);
        util.verifyText(nome, Nome);
        util.verifyText(cognome, Cognome);
        util.verifyText(codiceFiscale, CF);
        util.verifyText(telefono, Telefono);
        WebElement tipoDoc = driver.findElement(selectTipoDocumento);
        tipoDoc.sendKeys(Documento);
        TimeUnit.SECONDS.sleep(3);
        tipoDoc.sendKeys(Keys.ENTER);
        util.objectManager(numeroDocumento, util.scrollAndSendKeys, NumeroDocumento);
        util.objectManager(rilasciatoDA, util.scrollAndSendKeys, RilasciatoDA);
        util.objectManager(rilasciatoIL, util.scrollAndSendKeys, RilasciatoIL);
        TimeUnit.SECONDS.sleep(3);
        spinnerManager.waitForSpinnerBySldsHide(spinnerManager.dotSpinner);
    }

    public void conferma(By conferma) throws Exception {
        util.objectManager(conferma, util.scrollAndClick);
        spinnerManager.checkSpinners();
    }

    public void avanti(String frame) throws Exception {
        util.frameManager(frame, avantiButton, util.scrollAndClick);
    }

    public void avanti(String frame, By buttonAvanti) throws Exception {
        util.frameManager(frame, buttonAvanti, util.scrollAndClick);
        spinnerManager.waitForSpinnerByStyleDiplayNone(frame, spinnerManager.loadingSpinner);
    }

    public void verificaValoreTipoDelega(String tipo) throws Exception {

        del = driver.findElement(inputTipoDelega).getAttribute("value");

        if (!del.contentEquals(tipo))
            throw new Exception("il valore nel campo 'Tipo delega' è diverso da quello atteso:il valore visualizzato e' " + del + "; quello atteso e' " + tipo);

    }
}
