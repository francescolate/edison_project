package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class SceltaProcessoComponent extends BaseComponent {
    WebDriver driver;
    SeleniumUtilities util;
    SpinnerManager spinner;

    public SceltaProcessoComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
        spinner = new SpinnerManager(driver);
    }

    //	public By  tuttiIProcessi = new By.ByXPath("//div[@title = 'Tutti i processi'" + " and (text() = 'Tutti i processi')]");;
    //public By  tuttiIProcessi = new By.ByXPath("//div[text() = 'Tutti i processi']");
    //public By  tuttiIProcessi = new By.ByXPath("//a[@title = 'Tutti i processi']");
    //public By  tuttiIProcessi = new By.ByXPath("//a//*[text() = 'Tutti i processi']");
    public By tuttiIProcessi = new By.ByXPath("//button[text() = 'Tutti i processi']");
    //	public By  btnAnnullaOfferta = new By.ByXPath("//*[text() = 'Annulment']");
    public By btnAnnullaOfferta = new By.ByXPath("//button[@name='NE__Quote__c.Annulment']");
    public By btnAnnullaOrdine = By.xpath("//*[text() = 'Annulment' and contains(@name,'NE__OrderItem')]");
    public By annullaButton = By.xpath("//*[text() = 'Annulla' and @name='Cancel']");
    public By confermaAnnullamentoButton = By.xpath("//button[@name='ConfirmAnnullmentQuote']");
    public By confermaButton = By.xpath("//button[@name='ConfirmQuote']");
    public By closeButtonAnnulmmentCase = By.xpath("//button[@name='ConfirmAnnullmentCase']");
    public By referente = new By.ByXPath("//button[text() = 'Nuovo Referente']");
    //	public By  tipologiaReferente  = new By.ByXPath("//*[contains(text(),'Referenti (')]/../../../../../div/div/div/div/dl/dd[1]");
    public By tipologiaReferente = new By.ByXPath("//*[contains(text(),'(Primario)')]");
    public By primaAttivazione = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Prima Attivazione')]");
    public By primaAttivazioneEVO = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Prima Attivazione EVO')]");
    public By gestioneSdd = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Attivazione SDD')]"); //R3 FR 02.08.2021
    public By variazioneSdd = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Variazione SDD')]");
    public By revocaSdd = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Revoca SDD')]");
    public By gestioneScontiBonus = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Bonus')]");
    public By avvioReclamoScritto = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Reclamo Scritto')]");
    public By gestioneSplitAtt = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Attivazione Split Payment')]");
    public By subentro = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Subentro')]");
    public By avvioSubentroEVO = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Subentro EVO')]");
    public String processo = "//div[@class='modal-container slds-modal__container']//div[contains(a, '#')]";
    public String processo1 = "//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, '#')]";
    public By switchAttivo = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Switch attivo']");
    //public By switchAttivoEVO = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Switch attivo EVO']");//[FR]2021.05.17: commentato
    public By switchAttivoEVO = new By.ByXPath("//div[contains(@class,'modal-body scrollable')]//div/a[@title='Avvio Switch attivo EVO']"); //[FR]2021.05.17
//  public By predisposizionePresaEVO = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Predisposizione presa Gas EVO']");
//  public By predisposizionePresaEVOEle = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Predisposizione presa Ele EVO']");
    public By predisposizionePresaEVOEle = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Predisposizione presa Ele EVO')]");
    public By predisposizionePresaEVOGas = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Predisposizione presa Gas EVO')]");
    public By switchAttivoFast = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Switch attivo fast']");
    public By modificaTipologiaBolletta = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Modifica Tipologia Bolletta']");
    public By attivazioneFatturazioneElettronica = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//h2[text()='Avvio Attivazione Fatturazione Elettronica']");
    public By modificaFatturazioneElettronica = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//h2[text()='Avvio Modifica Fatturazione Elettronica']");
    public By revocaFatturazioneElettronica = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//h2[text()='Avvio Revoca Fatturazione Elettronica']");
    public By memberGetMember = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//h2[text()='Avvio Member Get Member']");
    public By fatturazioneElettronicaMenuItem = new By.ByXPath("//span[text()='Fatturazione Elettronica']");
//    public By avvioEstrattoConto = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Estratto Conto']");
    public By avvioEstrattoConto = new By.ByXPath("(//div/a[@title='Avvio Estratto Conto'])[2]");
    public By avvioEstrattoContoCanoneTv = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Estratto Conto Canone TV')]");
    public By avvioVariazioneIndirizzoFornitura = new By.ByXPath("//div[@class='slds-modal__content slds-p-around--medium']//span[contains(text(),'Avvio Variazione Indirizzo Fornitura')]");
    //public By avvioAttivazioneVas = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']"
    //		+ "//h2[text()='Avvio Attivazione VAS']");
    //	public By avvioAttivazioneVas = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//span[text()='Avvio Attivazione VAS']");
    public By avvioAttivazioneVas = new By.ByXPath("//div[@class='slds-is-relative cITA_IFM_LCP192_ServiceCatalog']//span[text()='Avvio Attivazione VAS']");
    public By avvioGestionePrivacy = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//a[@title='Avvio Gestione Privacy']");
    //	public By avvioModificaAnagrafica = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']//a[@title='Avvio Modifica Anagrafica']");
    public By avvioModificaAnagrafica = new By.ByXPath("//h2[text()='Tutti i processi']/../..//*[text()='Avvio Modifica Anagrafica']");
//	public By avvioRettificaMercato = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']"
//			+ "//h2[text()='Avvio Rettifica Mercato']");
    public By avvioRettificaMercato = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"
            + "//div[contains(a, 'Avvio Rettifica Mercato')]");
//	public By avvioVariazioneIndirizzoFatturazione = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']"
//			+ "//h2[text()='Avvio Variazione Indirizzo Fatturazione']");
//    public By avvioVariazioneIndirizzoFatturazione = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"
//            + "//div[contains(a, 'Avvio Variazione Indirizzo Fatturazione')]");
  public By avvioVariazioneIndirizzoFatturazione = new By.ByXPath("//div[contains(@class,'modal-body scrollable')]//div/a[@title='Avvio Variazione Indirizzo Fatturazione']");
    public By avvioVolturaSenzaAccollo = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Voltura senza accollo')]");
    public By avvioVolturaSenzaAccolloEVO = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Voltura Senza Accollo EVO')]");
    public By buttonChecklis = By.xpath("//section[@aria-expanded='true']//div[@id='modalChecklist']//button[text()='Conferma']");
    //public By avvioModificaPotenzaTensione = new  By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//h2[text()='Avvio Modifica Potenza e Tensione']");
    public By avvioModificaPotenzaTensione = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Modifica Potenza e Tensione']");
    public By avvioVerificaContatoreEle = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"
            + "//div[contains(a, 'Avvio Verifica Contatore ELE')]");
//	public By avvioVerificaContatoreEle = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']"
//			+ "//h2[text()='Avvio Verifica Contatore ELE']");
    public By avvioVerificaContatoreGas = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"
            + "//div[contains(a, 'Avvio Verifica Contatore GAS')]");
//	public By avvioVerificaContatoreGas = new By.ByXPath("//div[@class='cITA_IFM_LCP192_ServiceCatalog']"
//			+ "//h2[text()='Avvio Verifica Contatore GAS']");
    public By avvioAllaccioPrimaAttivazione = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Allaccio e Attivazione']");
//    public By avvioAllaccioPrimaAttivazioneEVO = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Allaccio e Attivazione EVO']");
    public By avvioAllaccioPrimaAttivazioneEVO = new By.ByXPath("//article[@class='slds-card']//span[text()='Avvio Allaccio e Attivazione EVO']");
    public By avvioAutolettura = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Autolettura']");
    public By avvioDisdettaStandard = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Disdetta Standard']");
    public By avvioDisdettaSuggello = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Chiusura Contatore']");
    public By avvioVariazioneSDD = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Variazione SDD']");
    public By avvioAttivazioneSDD = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Attivazione SDD']");
    public By avvioRevocaSDD = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Revoca SDD']");
    //public By spostamentoContatoreELE = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//h2[text()='Avvio Spostamento Contatore ELE']");
    public By spostamentoContatoreELE = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"
            + "//div[contains(a, 'Avvio Spostamento Contatore ELE')]");
    public By avvioVolturaConAccollo = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Voltura con accollo']");
    public By avvioCopiaDocumenti = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div/a[@title='Avvio Copia Documenti']");
    //public By avvioModificaStatoResidenza  = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//h2[text()='Avvio Modifica Stato Residenza']");;
    public By avvioModificaStatoResidenza = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Modifica Stato Residenza']");
    public By modificaImpiantoGas = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//div[contains(a, 'Avvio Modifica Impianto Gas')]");
    public By avviaAttFatturazioneEle = By.xpath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Attivazione Fatturazione Elettronica']");
    public By avviaModFatturazioneEle = By.xpath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Modifica Fatturazione Elettronica']");
    public By cambioUso = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content uiModal--recordActionWrapper']//div[contains(a, 'Avvio Cambio Uso')]");
    public By cambioPianoTariffario = new By.ByXPath("//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']//span[text()='Avvio Cambio Piano Tariffario']");
    public By motivoAnnullamento = new By.ByXPath("//*[contains(text(),'OPERAZIONE ERRATA')]/..");
    public By motivoAnnullamento2old = By.xpath("//*[contains(text(),'Annullamento Richiesta')]/following::[@class='slds-select']");
    public By motivoAnnullamento2 = By.xpath("//select[@id='j_id0:j_id4:j_id6:j_id7:cancellationReasonsPicklist']");
  
    public By motivoAnnullamentoofferta = By.xpath("//*[contains(text(),'Annullamento Richiesta')]/following::div[@id='annuPop']//select");
    public By motivoAnnullamentoOption = new By.ByXPath("//option[contains(text(),'Ripensamento')]");
    //	public By nomeiFrame = new By.ByXPath("//iframe[@name='NE__Quote__c.Annulment']");
    //	public By btnConferma2 = new By.ByXPath("//button[@id='btnConferma2']");
//	public By btnConferma2 = new By.ByXPath("//div[@id='annullamentoComponent']//button[@id='btnConferma2']");
    public By btnConferma2 = new By.ByXPath("//div[@id='annullamentoComponent']//span[contains(@id,'quickActionSection')]//button[@id='btnConferma2']");
    //	public By btnAnnulla = new By.ByXPath("//div[@id='annullamentoComponent']//span[contains(@id,'quickActionSection')]//button[@id='btnAnnulla2']");
    public By btnAnnulla = new By.ByXPath("//*[@class='tabContent active oneConsoleTab']/div/div[2]/div[2]/div/div[2]/div/div[3]/button[1]");
    public By chiudiFinestra = new By.ByXPath("//button[@title='Chiudi questa finestra']");
    //	String nomeiFrame = "NE__Quote__c.Annulment";
    public By annullaframe = By.xpath("//iframe[@name='NE__Quote__c.Annulment']");
    public By chiudiOrder = By.xpath("//span[@title='NE  OrderItem  c']/ancestor::li//button[contains(@title,'Chiudi')]");
    public By OKpopupNessunSito = By.xpath("//div[text()='Nessun sito presente']/..//button");
    public By annullaOrdine = By.xpath("//lightning-button[@data-id='Cancel']//button");
    public By confermaAnnullaOrdine = By.xpath("//button[@name='ConfirmAnnullmentQuote']");
    public By annullaframeofferta = By.xpath("//iframe[@id='corePageIframe']");
  
    public By confermaAnnullaCase=By.xpath("//button[@name='ConfirmAnnullmentCase']"); //FR 03.09.2021
    
    /**
     *
     * @param locator
     * @throws Exception
     */
    public void ensureMenuItemIsOpened(By locator) throws Exception {
        WebElement menuItem = util.waitAndGetElement(locator);
        WebElement menuButtonItem = util.waitAndGetElement(menuItem, By.xpath("./ancestor::button"));
        if (menuButtonItem.getAttribute("aria-expanded").equals("false")) menuItem.click();
    }


    /**
     *
     * @throws Exception
     */
    public void clickAllProcess() throws Exception {
        clickComponentIfExist(tuttiIProcessi);
    }

    /**
     *
     * @throws Exception
     */
    public void clickReferente() throws Exception {
        TimeUnit.SECONDS.sleep(5);
        WebElement referenteEl = util.waitAndGetElement(referente, 50, 5);
        referenteEl.click();
        TimeUnit.SECONDS.sleep(10);
    }

    /**
     *
     * @param button
     * @throws Exception
     */
    public void pressButton(By button) throws Exception {
        TimeUnit.SECONDS.sleep(2);
//		WebElement el = util.waitAndGetElement(button);
////		util.scrollToElement(el);
//		el.click();
        util.objectManager(button, util.scrollAndClick);
        spinner.checkSpinners();
    }

    /**
     *
     * @param motivo
     * @throws Exception
     */
    public void sceltaMotivoAnnullamento(String motivo) throws Exception {

        TimeUnit.SECONDS.sleep(2);

//	    driver.switchTo().frame(nomeiFrame);
//	    driver.switchTo().frame("NE__Quote__c.Annulment");

        WebElement frameToSw = driver.findElement(annullaframe);
        driver.switchTo().frame(frameToSw);

//		util.getFrameActive();
        TimeUnit.SECONDS.sleep(1);
        Select select = new Select(util.waitAndGetElement(motivoAnnullamento2));
        select.selectByVisibleText(motivo);
        TimeUnit.SECONDS.sleep(5);
        util.scrollDown();
        util.scrollDownDocument();
        if (util.verifyExistence(btnConferma2, 10)) {
//			util.objectManager(btnConferma2, util.scrollToVisibility,false);
            WebElement element = util.waitAndGetElement(btnConferma2);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", element);
        }

        TimeUnit.SECONDS.sleep(5);
        driver.switchTo().defaultContent();
        if (util.verifyExistence(btnAnnulla, 10)) {
            WebElement element = util.waitAndGetElement(btnAnnulla);
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", element);
        }
//        spinner.checkSpinners();
//        driver.switchTo().defaultContent();
    }

    /**
     *
     * @param frame
     * @param motivo
     * @throws Exception
     */
    public void sceltaMotivoAnnullamento(String frame, String motivo) throws Exception {
        TimeUnit.SECONDS.sleep(2);
        util.switchToFrame(frame);
        Select select = new Select(util.waitAndGetElement(motivoAnnullamento2));
        select.selectByVisibleText(motivo);
        TimeUnit.SECONDS.sleep(5);
        util.objectManager(btnConferma2, util.scrollToVisibility, true);
        util.objectManager(btnConferma2, util.clickwithjse);
        spinner.checkSpinners();
        driver.switchTo().defaultContent();
        if (util.verifyExistence(chiudiFinestra, 10)) util.objectManager(chiudiFinestra, util.scrollAndClick);
        util.objectManager(chiudiOrder, util.scrollAndClick);
    }


    public void sceltaMotivoAnnullamentoRichiesta() throws Exception {

        TimeUnit.SECONDS.sleep(2);

//	    driver.switchTo().frame(nomeiFrame);
//	    driver.switchTo().frame("NE__Quote__c.Annulment");

//        WebElement frameToSw = driver.findElement(annullaframeofferta);
//        driver.switchTo().frame(frameToSw);
        driver.switchTo().defaultContent();
//		util.getFrameActive();
        TimeUnit.SECONDS.sleep(1);
   //     Select select = new Select(util.waitAndGetElement(motivoAnnullamentoofferta));
        driver.findElement(motivoAnnullamentoofferta).click();
        TimeUnit.SECONDS.sleep(1);
       driver.findElement(motivoAnnullamentoOption).click();
              
   
    }
    
    /**
     *
     * @param locator
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public void chooseProcessToStart(By locator) throws Exception {
        FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 60)
                .ignoring(WebDriverException.class)
                .pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        util.objectManager(locator, util.scrollToVisibility, false);
        util.objectManager(locator, util.scrollAndClick);
        spinner.checkSpinners();
    }

    /**
     * @param objectLocator - xpath per identificare link del processo
     * @param searchString  - Stringa da ricercare
     * @throws Exception Start di un processo dopo aver effettuato la ricerca.
     */
    @SuppressWarnings("deprecation")
    public void chooseProcessToStartWithSearch(By objectLocator, String searchString) throws Exception {
        TimeUnit.SECONDS.sleep(5);
        //input[@name = 'enter-search']
        WebElement searchElement = util.waitAndGetElement(By.xpath("//input[@name = 'enter-search']"), 50, 5);
        searchElement.click();
        searchElement.sendKeys(searchString);
        TimeUnit.SECONDS.sleep(5);
        FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 50)
                .ignoring(WebDriverException.class)
                .pollingEvery(2, TimeUnit.SECONDS);

        wait.until(ExpectedConditions.elementToBeClickable(objectLocator)).click();
        spinner.checkSpinners();

    }

    /**
     *
     * @param itemName
     * @throws Exception
     */
    public void scegliProcesso(String itemName) throws Exception {
        String xpathString = processo1.replace("#", itemName);
        By item = By.xpath(xpathString);
        chooseProcessToStart(item);
    }

    /**
     *
     * @throws Exception
     */
    public void verificaPresenzaPopupNessunSitoPresente() throws Exception {

        if (!util.exists(By.xpath("//div[text()='Nessun sito presente']/..//div[text()=\"L’operazione richiesta non può essere eseguita. Il cliente selezionato non ha un sito.\"]"), 10)) {
            throw new Exception("La popup di allert 'Nessun sito presente' non è presente");
        }

    }

    /**
     *
     * @throws Exception
     */
    public void verificaPulsantiAnnullaConferma() throws Exception {
        // TODO Auto-generated method stub
        if ((!util.exists(By.xpath("//h2[text()='Annulla']/..//button[text()='Chiudi']"), 10)) || (!util.exists(By.xpath("//h2[text()='Annulla']/..//button[text()='Conferma']"), 10))) {
            throw new Exception("Pulsante Chiudi e conferma della popup di annullamento non presenti");
        }
    }

}
