package com.nttdata.qa.enel.components.colla;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.QANTTLogger;
import com.nttdata.qa.enel.util.WebDriverManager;

public class SetDatiAnagraficiSDD {
	/*
	COGNOME_REFERENTE
	NOME_REFERENTE
	RAGIONE_SOCIALE
	*/
	
	public static void main(String[] args) throws Exception {
		Properties prop = null;
		prop = WebDriverManager.getPropertiesIstance(args[0]);
		QANTTLogger logger = new QANTTLogger(prop);

		try {
			/*
			COGNOME_REFERENTE
			NOME_REFERENTE
			RAGIONE_SOCIALE
			*/
			prop.setProperty("NOME_NUOVO_SDD", "NOME_REFERENTE");
			prop.setProperty("COGNOME_NUOVO_SDD", "COGNOME_REFERENTE");
			prop.setProperty("CF_NUOVO_SDD", "COGNOME_REFERENTE");
			prop.setProperty("", "RAGIONE_SOCIALE");
			
			
			prop.setProperty("RETURN_VALUE", "OK");
		} 
		catch (Exception e) 
		{
			prop.setProperty("RETURN_VALUE", "KO");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			logger.write("ERROR_DESCRIPTION: "+errors.toString());

			prop.setProperty("ERROR_DESCRIPTION", "ERR:"+errors.toString());
			if(prop.getProperty("RUN_LOCALLY","N").equals("Y")) throw e;

		}finally
		{
			//Store WebDriver Info in properties file
			prop.store(new FileOutputStream(args[0]), "Set TestObject Info");			
		}
	}
	
}
