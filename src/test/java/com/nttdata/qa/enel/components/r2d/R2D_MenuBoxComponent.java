package com.nttdata.qa.enel.components.r2d;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_MenuBoxComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public R2D_MenuBoxComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

//	public void selezionaVoceMenuBox(String voce) throws Exception{
//		By	VoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']/div[contains(text(),'"+voce+"')]");
//		By	DivVoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+voce+"')]/..");
//		String style=driver.findElement(DivVoceMenuBox).getAttribute("style");
//		System.out.println("valore style:"+style);
//		if (style.compareTo("display: none;")==0) {
////			throw new Exception("Impossibile cliccare sulla voce:"+voce+"Il menù risulta essere chiuso");
//			util.objectManager(VoceMenuBox,util.scrollAndClick);
//		}
//		System.out.println("Menù già aperto");
//		TimeUnit.SECONDS.sleep(3);
//	}
//	public void selezionaVoceSottomenuBox(String voce) throws Exception{
////		By	DivVoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+voce+"')]/..");
////		String style=driver.findElement(DivVoceMenuBox).getAttribute("style");
////		System.out.println("valore style:"+style);
////		if (style.compareTo("display: none;")==0) {
////			throw new Exception("Impossibile cliccare sulla voce:"+voce+"Il menù risulta essere chiuso");
////		}
//		By	VoceSottomenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+voce+"')]");
//		util.objectManager(VoceSottomenuBox,util.scrollAndClick);
//		TimeUnit.SECONDS.sleep(3);
//	}
	
	public void selezionaVoceMenuBox(String menu,String sottomenu) throws Exception{
		By	VoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']/div[contains(text(),'"+menu+"')]");
		By	DivVoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+sottomenu+"')]/..");
		By	VoceSottomenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+sottomenu+"')]");
		String style=driver.findElement(DivVoceMenuBox).getAttribute("style");
		Logger.getLogger("").log(Level.INFO, "valore style:"+style);
		if (style.compareTo("display: none;")==0) {
//			throw new Exception("Impossibile cliccare sulla voce:"+voce+"Il menù risulta essere chiuso");
			util.objectManager(VoceMenuBox,util.scrollAndClick);
			util.objectManager(VoceSottomenuBox,util.scrollAndClick);
		}
		else{
			Logger.getLogger("").log(Level.INFO, "Menù già aperto");
			util.objectManager(VoceSottomenuBox,util.scrollAndClick);
		}
	}
	
	public void selezionaVoceMenuBoxF(String menu,String sottomenu) throws Exception{
		By	VoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']/div[contains(text(),'"+menu+"')]");
//		By	DivVoceMenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+sottomenu+"')]/..");
//		By	VoceSottomenuBox=new By.ByXPath("//div[@id='MENU_BOX']//div[contains(text(),'"+sottomenu+"')]");
//		By	DivVoceMenuBox=new By.ByXPath("//*[@id='MENU_SUB_4']/div[3]/..");
		By	DivVoceMenuBox=new By.ByXPath("//*[@id='MENU_ARROW_4']");
//		By	VoceSottomenuBox=new By.ByXPath("//*[@id='MENU_SUB_4']/div[3]");
		By	VoceSottomenuBox=new By.ByXPath("//*[@id='MENU_SUB_4']/div");
		By	VoceSottomenuBoxStyle=new By.ByXPath("//*[@id='MENU_SUB_4']");
		String style=driver.findElement(VoceSottomenuBoxStyle).getAttribute("style");
		Logger.getLogger("").log(Level.INFO, "valore style:"+style);
		if (style.compareTo("display: none;")==0) {
//			throw new Exception("Impossibile cliccare sulla voce:"+voce+"Il menù risulta essere chiuso");
			util.objectManager(VoceMenuBox,util.scrollAndClick);
			util.objectManager(VoceSottomenuBox,util.scrollAndClick);
		}
		else{
			Logger.getLogger("").log(Level.INFO, "Menù già aperto");
			util.objectManager(VoceSottomenuBox,util.scrollAndClick);
		}
	}

}
