package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ScegliTuBiorariaComponent extends BaseComponent {
	public By header = By.cssSelector("#adesione_standard > h1");
	public By detailLabel = By.cssSelector("#adesione_standard > p");
	public By podInfoBtn = By.cssSelector("#adesione_standard > p > span > a.openPOD.icon3-line-info2");
	public By buildingDataBtn = By.cssSelector("#adesione_standard > p > span > a.openDatiC.icon3-line-info2");
	public By[] podInfoPopup = {
			//Header
			By.cssSelector("body > div.remodal-wrapper.remodal-is-opened > div > div > div > div.modal-header > h4"),
			//Description above list
			By.cssSelector("#pod-default > p:nth-child(1)"),
			//List
			By.cssSelector("#pod-default > ul > li:nth-child(1)"),
			By.cssSelector("#pod-default > ul > li:nth-child(2)"),
			By.cssSelector("#pod-default > ul > li:nth-child(3)"),
			By.cssSelector("#pod-default > ul > li:nth-child(4)"),
			//Description below list, paragraph 1
			By.cssSelector("#pod-default > p:nth-child(3)"),
			//Description below list, paragraph 2
			By.cssSelector("#pod-default > p:nth-child(4)")
	};
	public By[] buildingDataPopup = {
			//header
			By.cssSelector("body > div.remodal-wrapper.remodal-is-opened > div > div > div > div.modal-header > h4"),
			//Description, below header
			By.cssSelector("body > div.remodal-wrapper.remodal-is-opened > div > div > div > div.modal-body > p"),
			//Description, just above list
			By.cssSelector("#datiCSUB > li > span"),
			//List
			By.cssSelector("#datiCSUB > li > ul > li:nth-child(1) > span"),
			By.cssSelector("#datiCSUB > li > ul > li:nth-child(2) > span"),
			By.cssSelector("#datiCSUB > li > ul > li:nth-child(3) > span"),
	};
	public By podPopupCloseBtn = By.cssSelector("body > div.remodal-wrapper.remodal-is-opened > div > div > div > div.modal-header > button");
	public By buildingInfoCloseBtn = By.cssSelector("body > div.remodal-wrapper.remodal-is-opened > div > div > div > div.modal-header > button");

	public ScegliTuBiorariaComponent(WebDriver driver) {
		super(driver);
	}

}
