package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
 //import com.sun.xml.internal.bind.v2.TODO;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class ConfiguraProdottoComponent extends BaseComponent {


    WebDriver driver;
    SeleniumUtilities util;
    SpinnerManager spinner;

    public ConfiguraProdottoComponent(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
        util = new SeleniumUtilities(driver);
        spinner = new SpinnerManager(driver);
    }


    public final By linkElettricoResidenziale = By.xpath("//div[text()='Elettrico - Residenziale']");
    public final By linkElettricoResidenzialeFUI = By.xpath("//div[text()='FUI-DEFAULT']");
    public final By linkElettricoNonResidenziale = By.xpath("//div[text()='Elettrico - Business']");
    public final By linkVas = By.xpath("//div[text()='VAS']");
    public final By linkVasEx = By.xpath("//div[@id='VAS EX']/div[text()='VAS EX']");
    public final By linkSconti = By.xpath("//div[text()='Sconti']");
    public final By linkAbbonamenti = By.xpath("//div[text()='Scelta Abbonamenti' and @class='slds-truncate showAllFamilyItem']");
    public final By linkGasResidenziale = By.xpath("//ancestor::div[@role='region']//div[contains(text(),'Gas - Residenziale')]");
    public final By linkGasNonResidenziale = By.xpath("//ancestor::div[@role='region']//div[text()='Gas - Business']");
    public final String aggiungiProdotto = "//*[text()='@PRODOTTO@']/ancestor::div[@id='multiaddItem']//img[@class='addToCartClass']/..";
    public final String aggiungiProdottoVAS = "//*[text()='@PRODOTTO_VAS@']/ancestor::div[@id='multiaddItem']//img[@class='addToCartClass']";
    public final String aggiungiSconto = "//*[text()='@SCONTO@']/ancestor::div[@id='multiaddItem']//img[@class='addToCartClass']";
    public final String aggiungiAbbonamento = "//*[text()='@ABBONAMENTO@']/ancestor::div[@id='multiaddItem']//img[@class='addToCartClass']";
    public final String modificaProdotto = "//div[text()='@PRODOTTO@']/ancestor::div[@class='itemsIntoCart']//button[@title='Modifica']";
    //public final By buttonSalva = By.xpath("//div[contains(@class,'win_Configurator')]//button[text()='Salva']");
    public final By buttonSalva = By.xpath("//div[contains(@class,'slds-button-group slds-float_right')]//button[text()='Salva']");
    public final By checkout = By.xpath("//button[text()='Checkout']");
    public final By confermaCheckout = By.xpath("//h2[text()='Checkout']/../..//button[text()='Conferma']");
    public final By cambiaProdotto = By.xpath("//button[text()='CAMBIA PRODOTTO']");
    public final By avantiButton = By.xpath("//button[text()='Avanti']");
    public final By espandiSezioneCerca = By.xpath("//img[@id='showHideSearch']/../..");
    public final By inputProdotto = By.xpath("//input[@name='setupSearch']");
    public final By buttonCerca = By.xpath("//div[@class='slds-col']//button[contains(text(),'Cerca') and @class='slds-button slds-button_neutral slds-button slds-button_brand']");
    public final By svuotaCerca = By.xpath("//button[@class='slds-button slds-button_neutral  slds-button_destructive' and contains(text(),'Svuota')]");
    public final By selectOpzioneKAM_AGCOR = By.xpath("//legend[text()='Opzione KAM_AGCOR']/ancestor::div[@class='slds-form-element__control']//select");
    public final By selectVax_Ex = By.xpath("//legend[text()='VAS EX']/ancestor::div[@class='slds-form-element__control']//select");
    public final By eliminaVAS = By.xpath("//div[@id='deleteButton']//button");
    public final By linkOpzioneKAM_AGCOR = By.xpath("//legend[text()='Opzione KAM_AGCOR']/ancestor::div[@class='slds-form-element__control']//select");
    public final By selectSceltaAbbonamento = By.xpath("//legend[text()='Scelta Abbonamenti']/ancestor::div[@class='slds-form-element__control']//select");
    public final By messaggioKOPrecheck2G = By.xpath("//*[text()='Precheck 2G Na']");
    public final By messaggioOKPrecheck2G = By.xpath("//*[text()='Precheck 2G']//ancestor::div[@class='slds-card']//img[@title='Esito Precheck 2G OK']");
    public final By buttonMessaggioKOPrecheck2G = By.xpath("//*[text()='Precheck 2G Na']/..//button[text()='Conferma']");
    public final By OrarioOneFree = By.xpath("//*[text()='Ora Inizio']/../..//select");
    public final By ConfermaAssicurazioneServizi = By.xpath("//*[text()='Carrello']/ancestor::div[@role='region']//button[text()='Conferma']");
    public final By ConfiguraVasEmail = By.xpath("//div[@id='cartStandard']//span[text()='Configura']/..");
    public final By ConfiguraVasEmailInd = By.xpath("//*[text()='E-mail']/../..//input");
    public final By ConfiguraVasEmailConfInd = By.xpath("//*[text()='Conferma E-mail']/../..//input");
    //public final By aggiungiProdottoxpath = By.xpath(aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto));
    public final By PianoTariffario = By.xpath("//*[@class='NEBit2win_Attribute']/../..//select");
    public final By linkFibra = By.xpath("//*[@id='Fibra']");
    public final String linkProdottoTab = "//*[@id='@PRODOTTOTAB@']";
    public final String aggiungiProdottoFibra = "//*[text()='Fibra']/ancestor::div[@id='multiaddItem']//img[@class='addToCartClass']";
    public final By ConfiguraProdotto_defaultFUI =By.xpath("//div[text()='PRODOTTO_DEFAULT' and @class='slds-truncate showAllFamilyItem']");
    public final By ConfiguraSegmentoDefault =By.xpath("//legend[text()='SEGMENTO DEFAULT']/ancestor::div[@class='slds-form-element__control']//select");
    public final By ConfiguraCausaleDefault =By.xpath("//legend[text()='CAUSALE ATT. DEFAULT']/ancestor::div[@class='slds-form-element__control']//select");
    public final By ConfiguraAttesaNuovaConfVendita =By.xpath("//legend[text()='ATTESA NUOVA CONF. VENDITA']/ancestor::div[@class='slds-form-element__control']//select");
    public final By ConfiguraAreaDefault = By.xpath("//legend[text()='AREA DEFAULT']/ancestor::div[@class='slds-form-element__control']//select");
    public final By ConfiguraMercatoFornitura = By.xpath("//legend[text()='MERCATO DI RIFERIMENTO']/ancestor::div[@class='slds-form-element__control']//select");
    public final By ScegliTuLink = By.xpath("//div[@id='Scegli Tu']");


    public void press(By oggetto) throws Exception {
        util.getFrameActive();
        if (util.exists(oggetto, 60)) {
            util.objectManager(oggetto, util.scrollAndClick);
            TimeUnit.SECONDS.sleep(10);
            spinner.checkSpinners();
        } else throw new Exception("Impossibile interagire con l'oggetto identificato da" + oggetto.toString());
    }

    public void pressResidenziale(By oggetto) throws Exception {
        util.getFrameActive();
        if (util.exists(oggetto, 60)) {
            util.objectManager(oggetto, util.scrollAndClick);
            TimeUnit.SECONDS.sleep(10);
            spinner.checkSpinners();
        } else throw new Exception("Impossibile interagire con l'oggetto identificato da" + oggetto.toString());
    }

    public String switchToFrame() throws Exception {
        String frame = util.getFrameByIndex(1);
        util.switchToFrame(frame);
        return frame;
    }

    public String switchToFrame2() throws Exception {
        String frame0 = util.getFrameByIndex(0);
        String frame = util.getFrameByIndex(1);
        System.out.println("Frame0:" + frame0);
        System.out.println("Frame1:" + frame);

        util.switchToFrame(frame);
        return frame;
    }


    public void configuraProdotto(String prodotto) throws Exception {
        //Cerca Prodotto
        util.objectManager(espandiSezioneCerca, util.click);
        util.objectManager(inputProdotto, util.sendKeys, prodotto);
        util.objectManager(buttonCerca, util.click);
        spinner.checkSpinners();
        util.objectManager(espandiSezioneCerca, util.scrollAndClick);


        String aggiungiProdottoxpath = this.aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto);
        util.objectManager(By.xpath(aggiungiProdottoxpath), util.scrollAndClick);
        spinner.checkSpinners();

        if (prodotto.compareTo("New_Soluzione Gas Impresa Business") == 0) {
            util.objectManager(selectOpzioneKAM_AGCOR, util.scrollAndSelect, "CORPORATE SUPER");
            spinner.checkSpinners();
        }

        if (util.exists(buttonSalva, 30) && driver.findElement(buttonSalva).isDisplayed()) {
            press(buttonSalva);
            spinner.checkSpinners();
        }
        press(checkout);
        spinner.checkSpinners();
        press(confermaCheckout);
        spinner.checkSpinners();
        if (!util.exists(By.xpath("//div[contains(text(),'Check Out effettuato')]"), 50))
            throw new Exception("Dopo aver confermato il checkout non compare il messaggio 'Checkout effettuato'");
    }

	//TODO: 19/03/2021  Ottimizzare
    public void selezionaProdotto(String prodotto) throws Exception {

        Thread.sleep(5000);

        JavascriptExecutor executor = (JavascriptExecutor) driver;

        WebElement element = driver.findElement(espandiSezioneCerca);

        executor.executeScript("arguments[0].click();", element);

        util.objectManager(inputProdotto, util.sendKeys, prodotto);

        util.objectManager(buttonCerca, util.click);

        spinner.checkSpinners();

        element = driver.findElement(espandiSezioneCerca);

        executor.executeScript("arguments[0].click();", element);

        util.objectManager(By.xpath(this.aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto)), util.scrollAndClick);

        spinner.checkSpinners();

    }

    public void configuraVas(String prodotto, String vas) throws Exception {

        //Aggiunta VAS
        if (util.exists(linkVas, 20) && driver.findElement(linkVas).isDisplayed()) {
            press(linkVas);
        } else {
            String modificaProdottoPath = this.modificaProdotto.replaceAll("@PRODOTTO@", prodotto);
            util.objectManager(By.xpath(modificaProdottoPath), util.scrollAndClick);
            spinner.checkSpinners();
            press(linkVas);

        }
        String aggiungiVasXpath = this.aggiungiProdottoVAS.replaceAll("@PRODOTTO_VAS@", vas);
        util.objectManager(By.xpath(aggiungiVasXpath), util.scrollAndClick);
        spinner.checkSpinners();


    }
    /**
     * Serve a cambiare la mail certificata nel VAS
     * @param prodotto prodotto per il quale è stato selezionato il VAS
     * @param mail mail da inserire
     */
    public void configuraVasMailCertificata(String prodotto,String mail) throws Exception {

    	String btn_modifica="//button[@title='Modifica']";
    	String btn_configura="//button[@class='slds-button slds-button_icon configure-incomplete-icon slds-button_icon-bare']";
    	String input_Mail="//legend[text()='E-mail']/parent::div/parent::div/input[@type='text']";
    	 TimeUnit.SECONDS.sleep(3);
    	String input_Mail_conf="//legend[text()='Conferma E-mail']/parent::div/parent::div/input[@type='text']";
    	String btn_Save="//button[text()='Salva']";
        //Aggiunta VAS
        if (util.exists(linkVas, 20) && driver.findElement(linkVas).isDisplayed()) {
            press(linkVas);
        } else {
            String modificaProdottoPath = this.modificaProdotto.replaceAll("@PRODOTTO@", prodotto);
            util.objectManager(By.xpath(modificaProdottoPath), util.scrollAndClick);
            spinner.checkSpinners();
            press(linkVas);

        }
        
        if (util.exists(By.xpath(btn_modifica),5) && driver.findElement(By.xpath(btn_modifica)).isDisplayed()) {
        	util.objectManager(By.xpath(btn_modifica), util.scrollAndClick);
        } else {
        	//Aggiunto else in caso di presenza del bottone configura nel carrello GAS residenziale
        	util.objectManager(By.xpath(btn_configura), util.scrollAndClick);
        }
        
        spinner.checkSpinners();
        util.objectManager(By.xpath(input_Mail), util.sendKeys, mail);
        TimeUnit.SECONDS.sleep(5);
        util.objectManager(By.xpath(input_Mail_conf), util.sendKeys, mail);
        TimeUnit.SECONDS.sleep(5);
        util.objectManager(By.xpath(btn_Save), util.scrollAndClick); 
        
        
        

    }
    
    public void configuraVasEx(String vas) throws Exception {

        //Aggiunta VAS
        if (util.exists(linkVasEx, 20) && driver.findElement(linkVasEx).isDisplayed()) {
            press(linkVasEx);
        } else {
        	util.objectManager(selectVax_Ex, util.scrollAndSelect, vas);
            spinner.checkSpinners();
            press(linkVasEx);

        }
        util.objectManager(selectVax_Ex, util.scrollAndSelect, vas);
        spinner.checkSpinners();


    }
    
    public void eliminaVas() throws Exception {

        //Elimina VAS
        if (util.exists(linkVas, 20) && driver.findElement(linkVas).isDisplayed()) {
            press(linkVas);
        }
            util.objectManager(eliminaVAS, util.scrollAndClick);
            spinner.checkSpinners();
            press(linkVas);
    }

    public void ConfiguraFibra(String prodotto) throws Exception {

        //Aggiunta FIBRA
        if (util.exists(linkFibra, 20) && driver.findElement(linkFibra).isDisplayed()) {
            press(linkFibra);
            util.objectManager(By.xpath(this.aggiungiProdottoFibra), util.scrollAndClick);
            spinner.checkSpinners();
        } else {
        	System.out.println("Errore - FIBRA non selezionabile! ");
        }
    }

    
    public void ConfiguraProdottoTab(String prodotto, String PianoTariffario) throws Exception {

        //Aggiunta Prodotto nel TAB del Carrello
        String modificaProdottoPath = this.linkProdottoTab.replaceAll("@PRODOTTOTAB@", prodotto);
       
        util.objectManager(By.xpath(modificaProdottoPath), util.scrollAndClick);
//        this.ConfiguraPianoTariffario(PianoTariffario);
    }
   
    public void configuraVasEmail(String email) throws Exception {

        util.objectManager(ConfiguraVasEmail, util.scrollAndClick);
        spinner.checkSpinners();

        if (util.exists(ConfiguraVasEmailInd, 20)) {
            util.objectManager(ConfiguraVasEmailInd, util.scrollAndSendKeys, email);
            util.objectManager(ConfiguraVasEmailConfInd, util.scrollAndSendKeys, email);
        }

    }

    public void configuraOpzioneKAM(String opzioneKAM) throws Exception {

        //Aggiunta KAM
        util.objectManager(selectOpzioneKAM_AGCOR, util.scrollAndSelect, opzioneKAM);
        spinner.checkSpinners();
    }
    
	public void configuraProdottoFUI(String SegmentoDefault, String CausaleDefault, String AreaDefault,
			String AttesaConfVendita, String MercatoFornitura) throws Exception {

		if (util.exists(ConfiguraProdotto_defaultFUI, 10)
				&& driver.findElement(ConfiguraProdotto_defaultFUI).isDisplayed()) {
			press(ConfiguraProdotto_defaultFUI);

			util.objectManager(ConfiguraSegmentoDefault, util.scrollAndSelect, SegmentoDefault);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(ConfiguraCausaleDefault, util.scrollAndSelect, CausaleDefault);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(ConfiguraAreaDefault, util.scrollAndSelect, AreaDefault);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(ConfiguraAttesaNuovaConfVendita, util.scrollAndSelect, AttesaConfVendita);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(ConfiguraMercatoFornitura, util.scrollAndSelect, MercatoFornitura);
			spinner.checkSpinners();
		} else {
			throw new Exception("Configura Prodotto FUI non trovato");
		}

	}

    public void configuraSconto(String prodotto, String sconto) throws Exception {

        //Aggiunta sconto
        if (util.exists(linkSconti, 20) && driver.findElement(linkSconti).isDisplayed()) {
            press(linkSconti);
        } else {
            String modificaProdottoPath = this.modificaProdotto.replaceAll("@PRODOTTO@", prodotto);
            util.objectManager(By.xpath(modificaProdottoPath), util.scrollAndClick);
            spinner.checkSpinners();
            press(linkSconti);

        }
        String aggiungiScontoXpath = this.aggiungiSconto.replaceAll("@SCONTO@", sconto);
        util.objectManager(By.xpath(aggiungiScontoXpath), util.scrollAndClick);
        spinner.checkSpinners();
    }

    public void configuraOrarioFree(String Orario) throws Exception {

        util.objectManager(OrarioOneFree, util.scrollAndSelect, Orario);

    }
    public void ConfiguraPianoTariffario(String Piano) throws Exception {

        util.objectManager(PianoTariffario, util.scrollAndSelect, Piano);

    }
    
    public void configuraSceltaAbbonamenti(String prodotto, String abbonamento) throws Exception {

        //Aggiunta sconto
        if (util.exists(linkAbbonamenti, 20) && driver.findElement(linkAbbonamenti).isDisplayed()) {
            press(linkAbbonamenti);
        } else {
            String modificaProdottoPath = this.modificaProdotto.replaceAll("@PRODOTTO@", prodotto);
            util.objectManager(By.xpath(modificaProdottoPath), util.scrollAndClick);
            spinner.checkSpinners();
            press(linkAbbonamenti);

        }
        //Scelta abbonamenti
        util.objectManager(selectSceltaAbbonamento, util.scrollAndSelect, abbonamento);
        spinner.checkSpinners();
    }

    public void salvaConfigurazione() throws Exception {

        if (util.exists(buttonSalva, 30) && driver.findElement(buttonSalva).isDisplayed()) {
            press(buttonSalva);
//            spinner.checkSpinners();
        }
    }


    public void configuraProdottoConVas(String prodotto, String vas) throws Exception {

        //Cerca Prodotto
        TimeUnit.SECONDS.sleep(10);
        util.objectManager(espandiSezioneCerca, util.click);
        TimeUnit.SECONDS.sleep(2);
        util.objectManager(inputProdotto, util.sendKeys, prodotto);
        TimeUnit.SECONDS.sleep(2);
        util.objectManager(buttonCerca, util.click);
        spinner.checkSpinners();
        TimeUnit.SECONDS.sleep(2);
        util.objectManager(espandiSezioneCerca, util.click);

        String aggiungiProdottoxpath = this.aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto);
        util.objectManager(By.xpath(aggiungiProdottoxpath), util.scrollAndClick);
        spinner.checkSpinners();

        if (prodotto.compareTo("New_Soluzione Gas Impresa Business") == 0) {
            util.objectManager(selectOpzioneKAM_AGCOR, util.scrollAndSelect, "CORPORATE SUPER");
            spinner.checkSpinners();
            press(buttonSalva);
            spinner.checkSpinners();
        }

        //Aggiunta VAS
        if (util.exists(linkVas, 30) && driver.findElement(linkVas).isDisplayed()) {
            press(linkVas);
        } else {
            String modificaProdottoPath = this.modificaProdotto.replaceAll("@PRODOTTO@", prodotto);
            util.objectManager(By.xpath(modificaProdottoPath), util.scrollAndClick);
            spinner.checkSpinners();
            press(linkVas);

        }
        String aggiungiVasXpath = this.aggiungiProdottoVAS.replaceAll("@PRODOTTO_VAS@", vas);
        util.objectManager(By.xpath(aggiungiVasXpath), util.scrollAndClick);
        spinner.checkSpinners();
        if (util.exists(buttonSalva, 30) && driver.findElement(buttonSalva).isDisplayed()) {
            press(buttonSalva);
            spinner.checkSpinners();
        }


        press(checkout);
        spinner.checkSpinners();
        press(confermaCheckout);
        spinner.checkSpinners();
        if (!util.exists(By.xpath("//div[contains(text(),'Check Out effettuato')]"), 50))
            throw new Exception("Dopo aver confermato il checkout non compare il messaggio 'Checkout effettuato'");
    }

    public void modificaProdotto(String prodotto) throws Exception {
        String aggiungiProdottoxpath = this.aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto);
        util.objectManager(By.xpath(aggiungiProdottoxpath), util.scrollAndClick);
        spinner.checkSpinners();

        if (util.exists(buttonSalva, 30) && driver.findElement(buttonSalva).isDisplayed()) {
            press(buttonSalva);
            spinner.checkSpinners();
        }
        press(checkout);
        spinner.checkSpinners();
        util.objectManager(confermaCheckout, util.scrollAndClick);
        TimeUnit.SECONDS.sleep(20);

    }

    public void modificaProdottoOpzioneKAMAGCOR(String prodotto, String Opzione) throws Exception {
        String aggiungiProdottoxpath = this.aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto);
        util.objectManager(By.xpath(aggiungiProdottoxpath), util.scrollAndClick);
        spinner.checkSpinners();
        configuraOpzioneKAM(Opzione);
        if (util.exists(buttonSalva, 30) && driver.findElement(buttonSalva).isDisplayed()) {
            press(buttonSalva);
            spinner.checkSpinners();
        }
        press(checkout);
        spinner.checkSpinners();
        util.objectManager(confermaCheckout, util.scrollAndClick);
        TimeUnit.SECONDS.sleep(20);

    }


    public void avantiProdotto() throws Exception {
        driver.switchTo().defaultContent();
        String frame = util.getFrameByIndex(1);
        //System.out.println(frame);
        if (util.verifyExistenceInFrame(frame, avantiButton, 200)) {
            util.frameManager(frame, avantiButton, util.scrollAndClick);
            spinner.checkSpinners(frame);
        } else
            throw new Exception("Impossibile cliccare sul pulsante Avanti dopo configurazione prodotto. Sistema lento");

    }

    public void svuotaRicerca() throws Exception {
        //Cerca Prodotto
        util.objectManager(espandiSezioneCerca, util.click);
        TimeUnit.SECONDS.sleep(2);
        util.objectManager(svuotaCerca, util.click);
        TimeUnit.SECONDS.sleep(2);
        spinner.checkSpinners();
        TimeUnit.SECONDS.sleep(2);
        util.objectManager(espandiSezioneCerca, util.click);

    }

    public void configuraProdottoSingle(String prodotto) throws Exception {
        //Cerca Prodotto
        try {
            util.objectManager(espandiSezioneCerca, util.click);
            TimeUnit.SECONDS.sleep(5);
            util.objectManager(inputProdotto, util.sendKeys, prodotto);
            TimeUnit.SECONDS.sleep(5);
            util.objectManager(buttonCerca, util.click);
            spinner.checkSpinners();
            TimeUnit.SECONDS.sleep(10);
            util.objectManager(espandiSezioneCerca, util.click);
            TimeUnit.SECONDS.sleep(5);
        } catch (Exception e) {
        }
        String aggiungiProdottoxpath = this.aggiungiProdotto.replaceAll("@PRODOTTO@", prodotto);
        util.objectManager(By.xpath(aggiungiProdottoxpath), util.scrollAndClick);
        spinner.checkSpinners();

        if (prodotto.compareTo("New_Soluzione Gas Impresa Business") == 0) {
            util.objectManager(selectOpzioneKAM_AGCOR, util.scrollAndSelect, "CORPORATE SUPER");
            spinner.checkSpinners();
        }


        if (util.exists(buttonSalva, 30) && driver.findElement(buttonSalva).isDisplayed()) {
            press(buttonSalva);
            spinner.checkSpinners();
        }
    }


    public void checkOut() throws Exception {
        press(checkout);
        press(confermaCheckout);
        spinner.checkSpinners();
        if (!util.exists(By.xpath("//div[contains(text(),'Check Out effettuato')]"), 20))
            throw new Exception("Dopo aver confermato il checkout non compare il messaggio 'Checkout effettuato'");
    }

}
