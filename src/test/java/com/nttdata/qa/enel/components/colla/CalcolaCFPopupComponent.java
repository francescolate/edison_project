package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class CalcolaCFPopupComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	public By nomePopup=By.xpath("//div[@class='form-group']//label[@for='nomeCF' and text()='Nome*']/ancestor::div[1]//input[@placeholder='Nome' and @id='nomeCF']");
	public By nomePopupError=By.xpath("//div[@class='form-group has-error']//label[@for='nomeCF' and text()='Nome*']/ancestor::div[1]//input[@placeholder='Nome' and @id='nomeCF']");
	public By cognomePopup=By.xpath("//div[@class='form-group']//label[@for='cognomeCF' and text()='Cognome*']/ancestor::div[1]//input[@placeholder='Cognome' and @id='cognomeCF']");
	public By cognomePopupError=By.xpath("//div[@class='form-group has-error']//label[@for='cognomeCF' and text()='Cognome*']/ancestor::div[1]//input[@placeholder='Cognome' and @id='cognomeCF']");
	
	public By datadinascitalabelPopup=By.xpath("//label[@class='normal-text' and text()='Data di nascita*']");
	public By giornoPopup=By.xpath("//span[@id='daysSelectBoxItContainer']//span[@id='daysSelectBoxItText' and text()='Giorno']");
	public By mesePopup=By.xpath("//span[@id='monthsSelectBoxItContainer']//span[@id='monthsSelectBoxItText' and text()='Mese']");
	public By annoPopup=By.xpath("//span[@id='yearsSelectBoxItContainer']//span[@id='yearsSelectBoxItText' and text()='Anno']");
	public By donnaCheckBoxPopup=By.xpath("//label[text()='Sesso*']/ancestor::div[2]//div[@role='radiogroup']//input[@value='F' and @name='sessoCF']/ancestor::div[1]//span[text()='Donna']");
	public By uomoCheckBoxPopup=By.xpath("//label[text()='Sesso*']/ancestor::div[2]//div[@role='radiogroup']//input[@value='M' and @name='sessoCF']/ancestor::div[1]//span[text()='Uomo']");
	public By siCheckBoxPopup=By.xpath("//label[text()='Sei nato in Italia?']//ancestor::div[2]//div[@role='radiogroup']//input[@id='yesitalian' and @value='y']/ancestor::div[1]//span[text()='Si']");
	public By noCheckBoxPopup=By.xpath("//label[text()='Sei nato in Italia?']//ancestor::div[2]//div[@role='radiogroup']//input[@id='noitalian' and @value='n']/ancestor::div[1]//span[text()='No']");
	public By buttonCalcola=By.xpath("//button[@id='calcolaBtn' and text()='Calcola*']");
	public By labelCampoObbligatorioGiorno=By.xpath("//span[@id='days_errmsg' and text()='Campo obbligatorio']");
	public By labelCampoObbligatorioMese=By.xpath("//span[@id='months_errmsg' and text()='Campo obbligatorio']");
	public By labelCampoObbligatorioAnno=By.xpath("//span[@id='years_errmsg' and text()='Campo obbligatorio']");
	public By labelCampoObbligatorioSesso=By.xpath("//span[@id='sessoCF_errmsg' and text()='Campo obbligatorio']");
	public By labelCampoObbligatorioItalia=By.xpath("//span[@id='yesitalianCF_errmsg' and text()='Campo obbligatorio']");
	public By nomeNonCorretto=By.xpath("//span[@id='nomeCF_errmsg' and text()='Formato nome non corretto']");
	public By cognomeNonCorretto=By.xpath("//span[@id='cognomeCF_errmsg' and text()='Formato cognome non corretto']");
	public By comuneObbligatorio=By.xpath("//span[@id='comuneCF_errmsg' and text()='Campo obbligatorio']");
	public By inputComune=By.xpath("//div[@class='form-group has-error']//div[@id='comuneCFcontainer' ]//input[@placeholder='Comune di nascita' and @id='comuneCF']");
	
	public CalcolaCFPopupComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		spinner = new SpinnerManager(this.driver);
		
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void click(By clickObject) throws Exception {
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=driver.findElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		
	}
	
	public void clickeaspetta(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		spinner.checkCollaSpinner();
	}
	
	public void clickdatendina(By clickObject) throws Exception {
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=driver.findElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
	
		
		driver.findElement(clickObject).sendKeys(Keys.DOWN);
		driver.findElement(clickObject).sendKeys(Keys.ENTER);
		
		
	
	}
	
	public void inserisciValue(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void inserisciComuneDiNascita(String valore) throws Exception {
		 List <WebElement> comune = util.waitAndGetElements(By.xpath("//ul[@id='ui-id-1']//li//span"));
		 
		 for (int i=1; i<=comune.size(); i++) {
			String com=util.waitAndGetElement(By.xpath("//ul[@id='ui-id-1']//li["+i+"]//span")).getText();
			//System.out.println(com);
			
			if (com.contentEquals(valore)) {
			//	util.waitAndGetElement(By.xpath("//ul[@id='ui-id-1']//li["+i+"]//span")).click();
				WebElement button=driver.findElement(By.xpath("//ul[@id='ui-id-1']//li["+i+"]//span"));
				TimeUnit.SECONDS.sleep(1);
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", button);
				TimeUnit.SECONDS.sleep(1);
				break;
			}
			 
		 }
	}
	
	public void inserisciData(String data, By button) throws Exception {
		String[] d = data.split("-");	
		String giorno=d[0];
		String Found="NO";
		String mese=d[1];
		String anno=d[2];
		System.out.println(giorno);
		By pathMenuGiorno=By.xpath("//span[@id='daysSelectBoxItContainer' and @aria-expanded='false']//span[@id='daysSelectBoxIt']");
		TimeUnit.SECONDS.sleep(1);
		click(pathMenuGiorno);
		TimeUnit.SECONDS.sleep(1);
		 List <WebElement> g = util.waitAndGetElements(By.xpath("//span[@id='daysSelectBoxItContainer']//ul[@id='daysSelectBoxItOptions']//li[@role='option']"));
		  //System.out.println(g.size());
			
				TimeUnit.SECONDS.sleep(1);
			
		    	String text=	util.waitAndGetElement(By.xpath("//span[@id='daysSelectBoxIt']")).getAttribute("innerHTML");
				System.out.println(text);
				
				
				WebElement element=driver.findElement(By.xpath("//span[@id='daysSelectBoxIt']"));
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].setAttribute('data-val', '02')",element);
				js.executeScript("arguments[0].setAttribute('aria-activedescendant', 'id_2')",element);
				js.executeScript("arguments[0].setAttribute('innertext', '02')",element);
				click(button);
				TimeUnit.SECONDS.sleep(1);
				System.out.println(text);
				//Var d = document.getElementById("//span[@id='daysSelectBoxIt']");
			/*	js.executeScript("arguments[0].setAttribute('data-val', '02')",element);
			 * js.executeScript("arguments[0].setAttribute('aria-activedescendant', 'id_2')",element);
				TimeUnit.SECONDS.sleep(1);
				js.executeScript("arguments[0].setAttribute('innertext', '02')",element);
				TimeUnit.SECONDS.sleep(1);*/
				
	}
			/*	if (!day.contentEquals(giorno)) {
					System.out.println(day);
					   //  util.objectManager(By.xpath("//span[@id='daysSelectBoxItContainer']//ul[@id='daysSelectBoxItOptions']//li[@role='option']["+i+"]//a"), util.scrollAndClick);
					    // driver.findElement(By.xpath("//span[@id='daysSelectBoxItContainer']//ul[@id='daysSelectBoxItOptions']//li[@role='option']["+i+"]//a")).click();
					   //  clickdatendina(By.xpath("//span[@id='daysSelectBoxItContainer']//ul[@id='daysSelectBoxItOptions']//li[@role='option']["+i+"]//a"));
					   TimeUnit.SECONDS.sleep(1);
					  
					     Found="YES";
				         break;
				         }
			   }
			
			if (Found.contentEquals("NO"))
					 throw new Exception("sulla popup web 'CALCOLA IL TUO CF' non e' possibile selezionare il giorno "+giorno);*/
		
	/*		Found="NO";
			By pathMenuMese=By.xpath("//span[@id='monthsSelectBoxItContainer' and @aria-expanded='false']//span[@id='monthsSelectBoxIt']");
			TimeUnit.SECONDS.sleep(1);
			click(pathMenuMese);
			
			 List <WebElement> m = util.waitAndGetElements(By.xpath("//span[@id='monthsSelectBoxItContainer']//ul[@id='monthsSelectBoxItOptions']//li[@role='option']"));
			  //System.out.println(m.size());
				for (int i = 1; i <= m.size(); i++) {
					TimeUnit.SECONDS.sleep(1);
					String month = driver.findElement(By.xpath("//span[@id='monthsSelectBoxItContainer']//ul[@id='monthsSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
					
					if (month.contentEquals(mese)) {
						   //  util.objectManager(By.xpath("//span[@id='monthsSelectBoxItContainer']//ul[@id='monthsSelectBoxItOptions']//li[@role='option']["+i+"]//a"), util.scrollAndClick);
						     click(By.xpath("//span[@id='monthsSelectBoxItContainer']//ul[@id='monthsSelectBoxItOptions']//li[@role='option']["+i+"]//a"));
						     Found="YES";
					         break;
					         }
				   }
				
				if (Found.contentEquals("NO"))
						 throw new Exception("sulla popup web 'CALCOLA IL TUO CF' non e' possibile selezionare il mese "+mese);
				
				Found="NO";
				By pathMenuAnno=By.xpath("//span[@id='yearsSelectBoxItContainer' and @aria-expanded='false']//span[@id='yearsSelectBoxIt']");
				TimeUnit.SECONDS.sleep(1);
				click(pathMenuAnno);
				
				 List <WebElement> a = util.waitAndGetElements(By.xpath("//span[@id='yearsSelectBoxItContainer']//ul[@id='yearsSelectBoxItOptions']//li[@role='option']"));
				  //System.out.println(a.size());
					for (int i = 1; i <= a.size(); i++) {
						TimeUnit.SECONDS.sleep(1);
						String years = driver.findElement(By.xpath("//span[@id='yearsSelectBoxItContainer']//ul[@id='yearsSelectBoxItOptions']//li[@role='option']["+i+"]//a")).getText();
						
						if (years.contentEquals(anno)) {
							   //  util.objectManager(By.xpath("//span[@id='yearsSelectBoxItContainer']//ul[@id='yearsSelectBoxItOptions']//li[@role='option']["+i+"]//a"), util.scrollAndClick);
							     click(By.xpath("//span[@id='yearsSelectBoxItContainer']//ul[@id='yearsSelectBoxItOptions']//li[@role='option']["+i+"]//a"));
							     Found="YES";
						         break;
						         }
					   }
					
					if (Found.contentEquals("NO"))
							 throw new Exception("sulla popup web 'CALCOLA IL TUO CF' non e' possibile selezionare l'anno "+anno);*/
					
	
}
