package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

/**
 * @author ChianteseRo
 * Il seguente componente si occupa di effettuare la ricerca di una Richiesta   
 * dal tab Richieste già aperto 
 */
public class RicercaRichiesteComponent 
{
	private WebDriver driver;
	private SeleniumUtilities util;
	SpinnerManager spinner;

	public RicercaRichiesteComponent(WebDriver driver) 
	{
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
		PageFactory.initElements(driver, this);
	} 

	private By searchBy = By.xpath("//input[@name='Case-search-input']"); 
	public final By tabCorrelato=By.xpath("//a[@title='Correlato'] and @aria-selected='true']");
	public final By elementoRichiestaPreventivo=By.xpath("//td/span[text()='PREVENTIVO INVIATO']/ancestor::tr//a[contains(text(),'OI-')]");
	public final By buttonGestionePreventivo=By.xpath("//button[text()='Gestione Preventivo']");

	public static final String X_DROPDOWN = "//button[@class='slds-button slds-p-horizontal__"
			+ "xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon[1]" ;

	@FindBy(xpath = X_DROPDOWN)
	private WebElement dropdwonButton;


	/**
	 * @param richiestaInput
	 * @throws Exception
	 */
	public void searchRichiesta(String richiestaInput) throws Exception
	{
		//String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(2);
		//		dropdwonButton.click();
		//		serachObject = xpathTab;
		//		util.frameManager(frame, searchObject, util.sendKeys, richiestaInput);
		//		WebElement searchObject = driver.findElement(searchBy);
		//		searchObject.sendKeys(richiestaInput);
		util.objectManager(searchBy, util.sendKeysCharByChar, richiestaInput);

		TimeUnit.SECONDS.sleep(10);
		//		util.frameManager(frame, searchObject, util.sendKeys, Keys.ENTER.toString());
		//		searchObject.sendKeys(Keys.ENTER);
	//	util.objectManager(searchBy, util.sendKeysCharByChar, Keys.ENTER.toString());
		util.objectManager(By.xpath("//span[contains(text(),' Sandbox: UAT')]"), util.scrollAndClick);
		//span[contains(text(),' Sandbox: UAT')]
		TimeUnit.SECONDS.sleep(10);
        //Se il numero richiesta è formattato male con caratteri sporchi prima
		String split[]= richiestaInput.trim().split(" ");
		String xPathElement = "//a[@title='"+split[split.length-1]+"']";
		By elemento = By.xpath(xPathElement);

		if (!util.verifyExistence(elemento, 30))
		{
			throw new Exception("Errore! Il codice richiesta " + richiestaInput + " non presente a sistema!");
		}

		//	util.frameManager(frame, element, util.scrollAndClick);
		//Istanzio oggetto link della Richiesta
		//		searchObject = driver.findElement(element);
		//		searchObject.click();
		TimeUnit.SECONDS.sleep(10);
//		util.objectManager(element, util.scrollAndClick);
		WebElement element = util.waitAndGetElement(elemento);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		TimeUnit.SECONDS.sleep(3);


	}

	public void verifySottostatoRichiesta(String sottoStatoInput) throws Exception
	{
		//		String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(2);

		String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Sottostato']/../div/span[@title='"+sottoStatoInput+"']";

		By element = By.xpath(byElement);

		if (!util.verifyExistence(element, 10))
		{
			throw new Exception("Errore! Il sottostato richiesta non è " + sottoStatoInput);
		}				
	}

	public boolean verificaSottostatoRichiesta(String sottoStatoInput) throws Exception
	{
		//		String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(2);

		String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Sottostato']/../div/span[@title='"+sottoStatoInput+"']";

		By element = By.xpath(byElement);

//		if (!util.verifyExistence(element, 10))
		if (!util.exists(element, 20))
		{
			return false;
		}
		else{
			return true;
		}
	}

	public void verifyStatoRichiesta(String statoInput) throws Exception
	{
		//String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(2);

		String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Stato']/../div/span[@title='"+statoInput+"']";

		By element = By.xpath(byElement);

		if (!util.verifyExistence(element, 10))
		{
			throw new Exception("Errore! Stato richiesta non è " + statoInput);
		}				
	}

	
	public boolean verificaStatoRichiesta(String statoInput) throws Exception
	{

		TimeUnit.SECONDS.sleep(2);

		String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Stato']/../div/span[@title='"+statoInput+"']";
		
		By element = By.xpath(byElement);

//		if (!util.verifyExistence(element, 10))
		if (!util.exists(element, 20))
		{
			return false;
		}
		else{
			return true;
		}
	}


	public void verifyStatoOfferta(String statoInput) throws Exception
	{
		//String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(2);

		String byElement = "//div[@class='slds-form-element slds-form-element_edit slds-hint-parent']/span[text()='Stato']/../div/span[@title='"+statoInput+"']";

		By element = By.xpath(byElement);

		if (!util.verifyExistence(element, 10))
		{
			throw new Exception("Errore! Stato richiesta non è " + statoInput);
		}				
	}
	public void clickTabCorrelato(By tab) throws Exception {
		util.objectManager(tab, util.click);
		spinner.checkSpinners();
		
	}
	public void clickElementoRichiestaPreventivo(By linkOI) throws Exception {
		try{
		util.objectManager(linkOI, util.click);
		}
		catch(Exception e){
			throw new Exception("Lo stato della preventivo risulta diverso da 'PREVENTIVO INVIATO'");
		}
		spinner.checkSpinners();
	}
	public void clickButtonGestionePreventivo(By buttonGestionePreventivo) throws Exception {
		util.objectManager(buttonGestionePreventivo, util.click);
		spinner.checkSpinners();
	}
	
	public void espandiElementiRichiesta() throws Exception{
		util.objectManager(By.xpath("//span[contains(text(),'Navigazione processo')]/ancestor::article//div[contains(text(),'Elementi della Richiesta')]"), util.scrollAndClick);
	}
	
	
	public String recuperaInfoDaIdRichiesta(String info, String id) throws Exception {
		System.out.println("//div[contains(@data-component-id,'ProcessExplorer')]//a[contains(text(),'"+id+"')]/ancestor::ul//span[text()='"+info+"']/../strong");
		List<WebElement> list = util.waitAndGetElements(By.xpath("//div[contains(@data-component-id,'ProcessExplorer')]//a[contains(text(),'"+id+"')]/ancestor::ul//span[text()='"+info+"']/../strong"));
		if(list.size()==0) throw new Exception("Impossibile recuperare il valore di "+info+" a partire dall id richiesta "+id);
		
		return list.get(0).getText();
		
	}
}
