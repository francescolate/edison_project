package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CreaNuovaInterazioneComponentEVO extends BaseComponent {

    public WebDriver driver;
    public SeleniumUtilities util;
    public SpinnerManager spinner;

    public static final String X_DROPDOWN = "//*[local-name() = 'button'][contains(@class,'slds-button') and contains(@class,'slds-button_icon') and contains(@class,'slds-p-horizontal__xxx-small') "
            + "and contains(@class,'slds-button_icon-small') and contains(@class,'slds-button_icon-container')]/*[local-name() = 'lightning-primitive-icon']//*[local-name() = 'svg'][@class='slds-button__icon']";
    public static final String ITerationSpan = "//*[local-name() = 'a'][@class='menuItem' and @href='/lightning/o/ITA_IFM_Interaction__c/home']/*[local-name() = 'span'][2]/*[local-name() = 'span']";
    public static final String NEwIterationButton = "//a[@title='Nuovo' and (@role='button')]";

    public CreaNuovaInterazioneComponentEVO(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.util = new SeleniumUtilities(driver);
        this.spinner = new SpinnerManager(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = ITerationSpan)
    public WebElement iterationSpan;

    @FindBy(xpath = X_DROPDOWN)
    public WebElement dropdwonButton;

    @FindBy(xpath = NEwIterationButton)
    public WebElement newIterationButton;


    public void createNewiteration() throws Exception {
        dropdwonButton.click();
        iterationSpan.click();
        checkSpinnersSFDC();
        newIterationButton.click();
        checkSpinnersSFDC();
    }

}
