package com.nttdata.qa.enel.components.colla;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CreateEmailAddressesListComponent {
	WebDriver driver;
	SeleniumUtilities util;
	int dotOffset = 2;
	int lastUsablePosition = 0;

    public CreateEmailAddressesListComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}
	
	public String setDotOnEmailAddress(String email) throws Exception{
		String dottedEmail = "";
		String domain = email.substring(email.indexOf("@")-lastUsablePosition);
		if(!domain.equals(email)){
			email = email.replace(domain, "");
			int dotIndex = email.indexOf("."), lastDotIndex = email.lastIndexOf(".");
			
			if(countDots(email) < (email.length()-1)-countDots(email))
				if(dotIndex > -1)
					if(dotIndex == email.length()-2)
						dottedEmail = setDottedEmail(email, domain, 0, 1);
					else
						if(lastDotIndex != dotIndex+dotOffset){
							email = email.replaceFirst("(?:\\.)+", "");
							dotIndex++;
							if(email.charAt(dotIndex) == '.')
								dottedEmail = setDottedEmail(email, domain, 0, 1);
							else
								dottedEmail = setDottedEmail(email, domain, 0, dotIndex);
						}else{
							dottedEmail = setDottedEmail(email, domain, 0, 1);
							dotOffset+=2;
						}
				else
					dottedEmail = setDottedEmail(email, domain, 0, 1);
			else{
				dotOffset = 2;
				lastUsablePosition++;
				dottedEmail = email.replace(".", "")+domain;
			}
		}else
			dottedEmail = null;
		
		return dottedEmail;
	}
	
	public List<String> getListOfAvailableEmailAddresses() throws Exception{
		String line = null;
		List<String> list = new ArrayList<String>();
		File f = new File("EmailAddressesForRegistration");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		while((line = br.readLine()) != null){
			line.replace("\n", "").replace("\r", "");
			list.add(line);
		}
		return list;
	}
	
	public void setAvailableEmailAddresses(List<String> list) throws Exception{
		File f = new File("EmailAddressesForRegistration");
		FileOutputStream fos = new FileOutputStream(f);
		for(String s : list){
			s.replace("\n", "").replace("\r", "");
			fos.write((s+"\n").getBytes());
		}
	}
	
	public void setAvailableEmailAddresses(HashMap<String, String> map) throws Exception{
		File f = new File("EmailAddressesForRegistration");
		FileOutputStream fos = new FileOutputStream(f);
		for(Entry<String, String> entry : map.entrySet()){
			fos.write((entry.getKey().replace("\n", "").replace("\r", "")+"\n").getBytes());
		}
		fos.close();
	}
	
	public void restoreAvailableEmailAddresses(List<String> list, String mobile) throws Exception{
		HashMap<String, String> map = new HashMap<String, String>();
		for(String s : list){
			s.replace("\n", "").replace("\r", "");
			map.put(s, s);
		}
		map.put(mobile, mobile);
		this.setAvailableEmailAddresses(map);
	}
	
	public String getEmailAddress() throws Exception{
		String line = null;
		List<String> list = new ArrayList<String>();
		File f = new File("EmailAddressesForRegistration");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		while((line = br.readLine()) != null){
			line.replace("\n", "").replace("\r", "");
			list.add(line);
		}
		br.close();
		fr.close();
		
		String emailAddress = null;
		for(int i=0;i<list.size();i++){
			emailAddress = list.get(i);
			if(!APIService.accountExists("email", emailAddress)){
				list.remove(i);
				i = list.size();
			}
		}
		setAvailableEmailAddresses(list);
		
		return emailAddress;
	}
	
	private String setDottedEmail(String email, String domain, int start, int end){
		String first = email.substring(start,end);
		String second  = email.substring(end);
		return  first+"."+second+domain;
	}
	
	private static int countDots(String str) {
		return str.replaceAll("[^.]", "").length();
	}
}