package com.nttdata.qa.enel.components.colla;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import junit.framework.Assert;

public class ChangePowerAndVoltageComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By changePowerAndVoltageTitle = By.xpath("//h1[text()='Modifica potenza e/o tensione']");
	public By changePowerAndVoltageContent = By.xpath("//div[@class='heading']//p");
	public By changePowerAndVoltageButton = By.xpath("//div[@id='landing-dispositiva']//button");
	public By faqTitle = By.xpath("//section[@id='landing-accordion']/h3[@id='faq-disattivazione']");
	public By pageTitle = By.xpath("//h1[@id='titleMPT0']");
	public By changePowerAndVoltageSubText = By.xpath("//h2[text()='Seleziona la fornitura sulla quale intendi modificare la potenza e/o la tensione']");
	public By radioButton = By.xpath("//label[@id='row-0-label']//span[@id='checkbox-item-0']");
	public By radioButton149 = By.xpath("//span[@id='checkbox-item-1']");
	public By flashIcon = By.xpath("//label[@id='row-0-label']//span[@class='icon-line-electricity']");
	public By address = By.xpath("//label[@id='row-0-label']//span[@class='indirizzo']/span[@class='valore']");
	public By pod = By.xpath("//label[@id='row-0-label']//span[@class='cliente'][1]/span[@class='valore']");
	public By powerVoltageValue = By.xpath("//label[@id='row-0-label']//span[@class='cliente'][2]/span[@class='valore']");
	public By exitButton = By.xpath("//button[@id='exitButton']");
	public By changePowerOrVoltageButton = By.xpath("//button[@id='nextButton']");
	public By titleSubText = By.xpath("//div[@class='list-form-container']/h2");
	public By formLine1 = By.xpath("//div[@class='list-form-container']//h3[1]");
	public By dataEntry = By.xpath("//span[text()='Inserimento dati']");
	public By estimate = By.xpath("//span[text()='Preventivo']");
	public By outCome = By.xpath("//span[text()='Esito']");
	public By powerLable = By.xpath("//label[@id='dummyPowerSelect']");
	public By voltageLable = By.xpath("//label[@id='dummyVoltageSelect']");
	public By powerValue = By.xpath("//select[@id='dummyPowerSelectSelect']");
	public By voltageValue = By.xpath("//select[@id='dummyVoltageSelectSelect']");
	public By formLine2 = By.xpath("//div[@class='form-group']//p[text()='Sulla fornitura sono collegati apparati di sollevamento persone (es. ascensore)?']");
	public By radioNo = By.xpath("//div[@class='radio-container']//label[text()='No']");
	public By radioYes = By.xpath("//div[@class='radio-container']//label[text()='Si']");
	public By backButton = By.xpath("//button[@class='button_second']//span[text()='Indietro']");
	public By calculateQuote = By.xpath("//button[@class='button_first']//span[text()='Calcola preventivo']");
	public By liftTypeLabel = By.xpath("//div[@class='form-group motivo'][1]/label");
	public By runningCurrentLable = By.xpath("//label[@id='form-corrente-regime']");
	public By suggestedPowerLable = By.xpath("//label[@id='form-potenza-suggerita']");
	public By runningCurrentInput = By.xpath("//input[@id='form-corrente-regimeInput']");
	public String runningCurrentInputString = "input[@id='form-corrente-regimeInput']";
	public By suggestedPowerField = By.xpath("//input[@id='form-potenza-suggeritaInput']");
	public String suggestedPowerFieldString = "//input[@id='form-potenza-suggeritaInput']";
	public By iIcon = By.xpath("//span[@class='icon-info-circle']");
	public By popupTitle = By.xpath("//div[@class='modal-header']/h3[@class='bold-title']");
	public By popupInnerText = By.xpath("//div[@class='modal-header']/p[@class='inner-text']");
	public By popupClose = By.xpath("//button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close-rounded']");
	public By liftTypeDropdown = By.xpath("//select[@id='dummyAscentoreTipSelectSelect']");
	public By startingCurrentLable = By.xpath("//label[@id='form-corrente-avviamento']");
	public By startingCurrentInput = By.xpath("//input[@id='form-corrente-avviamentoInput']");
	public By warningTitle = By.xpath("//div[@class='blue-alert']/p[1]");
	public By warningText = By.xpath("//div[@class='blue-alert']/p[2]");
	public By formLine3 = By.xpath("//div[@class='step-box-container']//h3[2]");
	public By email = By.xpath("//input[@id='form-emailInput']");
	public By phoneNum = By.xpath("//input[@id='form-phoneInput']");
	public By voltWarningTitle = By.xpath("//div[@class='blue-alert']/p[1]");
	public By voltWarningText = By.xpath("//div[@class='blue-alert']/p[2]");
	public By faq1 = By.xpath("//button[@id='accordion-button-item-accordion-0']");
	public By faq1Plus = By.xpath("//button[@id='accordion-button-item-accordion-0']/span[@class='dsc-icon-line-plus']");
	public By faq1Minus = By.xpath("//button[@id='accordion-button-item-accordion-0']/span[@class='dsc-icon-line-minus']");
	public By attenzionepopUp = By.xpath("//div[@class='modal-header']/h3[@class='bold-title']");
	public By attenzionepopUpText = By.xpath("//div[@class='modal-header']/p[@class='inner-text']");
	public By popupNo = By.xpath("//button[@id='overlayNoButton']");
	public By popupYes = By.xpath("//button[@id='overlayYesButton']");
	public By attenzionepopupClose = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/button[@class='modal-close inline-icon-link']/span[@class='dsc-icon-close']");
	public By hydraulicPopup1 = By.xpath("//div[@class='blue-alert']/p[2]");
	public By powerInput = By.xpath("//div[@class='form-group mod-invio-email']/input[@id='descrivi-motivo']");
	public By powerError = By.xpath("//span[@class='errorMsg']");
	public By confirmEmail = By.xpath("//input[@id='form-emailConfirmInput']");
	public By cellular = By.xpath("//input[@id='form-phoneInput']");
	public By confirmCellular = By.xpath("//input[@id='form-phoneConfirmInput']");
	public By calcovaPreventivoPopUp = By.xpath("//div[@class='modal-header']/p[@class='inner-text']");
	public By enelLogo = By.xpath("//a[@class='brand href-sf-prevent-default']/img");
	public By userIcon = By.xpath("//span[@class='dsc-icon-user']");
	public By user = By.xpath("//p[@id='welcomeUser']");
	public By footerSection = By.xpath("//div[@class='footer-info-container']");
	public By enelEnergia = By.xpath("//div/div[@class='footer-info-container']/ul[1]/li[1]");
	public By tuttiIDirittiRiservati = By.xpath("//div/div[@class='footer-info-container']/ul[1]/li[2]");
	public By pVIA = By.xpath("//div/div[@class='footer-info-container']/ul[1]/li[3]");
	public By legali = By.xpath("//div[@class='footer-info-container']/ul[2]/li[1]");
	public By privacy = By.xpath("//div[@class='footer-info-container']/ul[2]/li[2]");
	public By credits = By.xpath("//div[@class='footer-info-container']/ul[2]/li[3]");
	public By contatti = By.xpath("//div[@class='footer-info-container']/ul[2]/li[4]");
	public By services = By.xpath("//a[@id='servizi']//span[text()='Servizi']");
	public By warning = By.xpath("//div[@class='blue-alert']");
	public By radioButton1 = By.xpath("//label[@id='row-1-label']/span[@class='flex']/span[@id='checkbox-item-1']");
	public String emailInput = "//input[@id='form-emailInput']";
	public By attendiPopUp = By.xpath("//div[@class='modal-dialog']/div[@id='modalAlert']/div[@class='modal-header']/p[@class='inner-text']");
	public By attendiPopUpTitle = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/h3[@class='bold-title']");
	public By emailError = By.xpath("//span[@id='form-emailError']");
	public By confirmEmailError = By.xpath("//span[@id='form-emailConfirmError']");
	public By cellularError = By.xpath("//span[@id='form-phoneError']");
	public By confirmInvalidEmailError = By.xpath("//span[@id='form-emailConfirmError' and contains(text(),'Le email')]");
	public By defaultPVError = By.xpath("//span[@class='errorMsg' and contains(text(),'Impossibile procedere se non si modificano i valori attuali di potenza e/o tensione')]");
	public By liftTypeError = By.xpath("//span[@class='errorMsg' and text()='Selezionare la tipologia di ascensore']");
	public By correnteaRegimeError = By.xpath("//span[@id='form-corrente-regimeError']");
	public By correntediavviamentoError = By.xpath("//span[@id='form-corrente-avviamentoError']");
	public By cellulare = By.xpath("//input[@id='form-phoneInput']");
	public By confirmCellulare = By.xpath("//input[@id='form-phoneConfirmInput']");
	public By radioButtonpod = By.xpath("//span[text()='IT002E5879839A']/ancestor::span[@class='cliente']/preceding-sibling::span[@class='icon-ckbox']");
	public By sopralluogo = By.xpath("//div[@class='step-box-container']/div[@class='right-main']/div[2]/h3");
	public By sopralluogoText1 = By.xpath("//div[@class='right-main']/div[2]/p[1]");
	public By sopralluogoText2 = By.xpath("//div[@class='right-main']/div[2]/p[2]");
	public By potenzaInput = By.xpath("//div[@class='details-container']/dl/span[1]/dd");
	public By tensionInput = By.xpath("//div[@class='details-container']/dl/span[2]/dd");
	public By emailIp = By.xpath("//span[@class='form-group motivo full-length'][1]/dd");
	public By cellularIp = By.xpath("//span[@class='form-group motivo full-length'][2]/dd");
	public By sopralluogoText3 = By.xpath("//div[@class='content-group']/p[1]");
	public By sopralluogoText4 = By.xpath("//div[@class='content-group grey-bg']/h4");
	public By luceIcon = By.xpath("//span[@class='icon-line-electricity']");
	public By luceAdd = By.xpath("//div/span[@class='flex']/span[@class='indirizzo']/span[@class='valore']");
	public By lucepod = By.xpath("//div/span[@class='flex']/span[@class='cliente']/span[@class='valore']");
	public By indietro = By.xpath("//div[@class='right-main']/div[@class='button-container']/button[@class='button_second']");
	public By conferma = By.xpath("//div[@class='right-main']/div[@class='button-container']/button[@id='nextButton']");
	public By radioButtonIT002E5678559A = By.xpath("//span[text()='IT002E5678559A']/ancestor::span/child::span[@class='icon-ckbox']");
	public By firstSupplyCheckBox = By.xpath("//span[contains(@class,'flex') and not(contains(@class,'dsd'))]/span[@class='icon-ckbox']");
	public By firstSupplyPod = By.xpath("//span[contains(@class,'flex') and not(contains(@class,'dsd'))]/span[@class='cliente']/span[2]/span/span");
	public By modifyTensionBtn = By.xpath("//button[@id='nextButton']/span[text()='Modifica potenza e/o tensione']");
	public By voltageUpdateLbl = By.cssSelector(".blue-alert");
	public By messageBodyPart1 = By.cssSelector("#sc > div > div.list-form-container > div > div.right-main > div:nth-child(2) > p:nth-child(2)");
	public By messageBodyPart2 = By.cssSelector("#sc > div > div.list-form-container > div > div.right-main > div:nth-child(2) > p:nth-child(3)");
	public By messageBodyPart3 = By.cssSelector("#sc > div > div.list-form-container > div > div.right-main > div:nth-child(2) > div:nth-child(5) > p");
	public By confermaBtn = By.xpath("//button/span[text()='Conferma']");
	public By inspectionPopupTitle = By.cssSelector("#modalAlert > div > h3");
	public By inspectionPopupBody = By.cssSelector("#modalAlert > div > p");
	public By resultsPageTitle = By.id("id_title");
	public By resultsPageBody = By.xpath("//h3[@id='id_title']/../p");
	public By esitoStepLbl = By.xpath("//span[text()='Esito']");
	public By fineBtn = By.xpath("//button/span[text()='FINE']");
	
	public static final String popUp = "Stiamo elaborando il preventivo sulla base della tua richiesta" ;
	public String result = "NO";

	public String faqAns = " La potenza è la quantità di energia elettrica massima erogata dal contatore e viene misurata in kW."
							+ "La tensione è la differenza di potenziale elettrico che c’è tra due conduttori elettrici (ad esempio due cavi elettrici in rame)."
							+ "monofase (220V) oppure trifase (380V). L'utilizzo di numerosi elettrodomestici può comportare il superamento della potenza disponibile"
							+ " sul tuo contatore e, nel caso di forniture con limitazione della potenza, alla conseguente interruzione nell'erogazione di energia elettrica. "
							+ "Quando un evento del genere capita frequentemente, ti consigliamo di considerare una richiesta di aumento di potenza della tua fornitura."
							+"Viceversa, nel caso in cui ritieni che la potenza attuale del contatore sia sovradimensionata rispetto alle tue esigenze, puoi considerare una richiesta di riduzione di potenza."
							+"Tieni presente che, per richieste di potenza fino a 15 kW avrai a disposizione una potenza massima disponibile pari al valore di potenza richiesto aumentato del 10% (ad esempio," 
							+"un contatore con potenza contrattuale richiesta di 3 kW garantisce una potenza disponibile pari a 3,3 kW)."
							+"La tempistica varia a seconda se sia necessario o meno effettuare un sopralluogo da parte di un tecnico del tuo distributore, il quale ti comunicherà i tempi di esecuzione del lavoro."
							+" Se non è necessario il sopralluogo l'operazione viene effettuata mediamente tra 2 e 5 giorni lavorativi."
							+"Se la modifica di potenza e/o tensione non è eseguibile direttamente dal tuo distributore di energia elettrica in telegestione, è necessario il sopralluogo di un tecnico del distributore"
							+ " per determinare l'entità dell'intervento da effettuare. In questi casi sarà lo stesso distributore a contattare il cliente per accordarsi sulla data di esecuzione del sopralluogo."
							+"L’operazione di modifica della potenza e/o tensione comporta un costo, che ti verrà comunicato nel preventivo. Tale costo dipende della variazioni di potenza e/o tensione richieste, e si "
							+ "compone di una quota relativa alle attività sulla rete di distribuzione e di una quota dipendente dalla condizioni del tuo contratto di fornitura con Enel Energia."
							+"Nel preventivo che ti invieremo troverai, inoltre, una nostra stima degli impatti in bolletta della modifica richiesta: infatti, i servizi di rete (trasporto, distribuzione dell'energia e gestione del contatore), gli"
							+"oneri generali di sistema e l’ammontare delle accise applicate in bolletta variano in base alla potenza del tuo contatore. I costi indicati nel preventivo sono da considerarsi al netto dell'IVA."
							+"Se l'importo del preventivo è inferiore a 1000 euro potrai scegliere se addebitare il costo in bolletta oppure pagare online con carta di credito o, in alternativa, tramite bollettino."
							+"Se l'importo è superiore a 1000 euro non sarà possibile richiedere l'addebito in bolletta, ma è necessario effettuare il pagamento online con carta di credito o tramite bollettino."
							+"In tutti i casi, se hai la domiciliazione attiva, il costo verrà automaticamente addebitato in bolletta e non dovrai fare nulla."
							+"Il sistema ti guiderà nella scelta della modalità di pagamento che preferisci tra quelle disponibili.";
							
	public ChangePowerAndVoltageComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(5000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
	}
	 
	public void clickComponent(String webElementString) throws Exception {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			if(js.executeScript("(document.evaluate(\""+webElementString+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click()") == null)
				throw new Exception("Unable to click on WebElement (input) 'POTENZA SUGGERITA'");
	}	
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			verifyComponentExistence(existingObject, 15);
		}
	 
	 public void verifyComponentExistence(By existingObject, int timeout) throws Exception {
		 if (!util.exists(existingObject, timeout))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	 }
	    
	    public void scrollComponent(By object) throws Exception {
			util.objectManager(object, util.scrollToVisibility, false);
		}

		public void checkForPageContent() throws Exception {
			
			
			String pageContent = "Se la potenza disponibile del tuo contatore risulta insufficiente alle tue esigenze di consumo, puoi prendere in considerazione di "
					+ "effettuare una richiesta di aumento di potenza. Viceversa, qualora ritieni che la potenza disponibile del tuo contatore sia sovradimensionata rispetto"
					+ " alle tue esigenze, potrai richiedere una riduzione della potenza. La modifica della potenza (ed, eventualmente, della tensione) del tuo contatore comporta "
					+ "variazioni alle condizioni economiche e, qualora necessario, anche variazioni alle condizioni tecniche della fornitura (ad esempio: una variazione della tensione, "
					+ "un cambio del contatore etc.).Effettua in maniera semplice e veloce l’operazione dalla tua area riservata. Otterrai subito un preventivo di spesa che potrai accettare o"
					+ " annullare comodamente.Questa funzionalità è disponibile solamente per forniture ad uso abitativo.";
			
			String actualText = driver.findElement(changePowerAndVoltageContent).getText();
			
			if(pageContent.equals(actualText)){
				result = "YES";
			}
			
			if(!result.equals("NO")){
	    		throw new Exception ("Page content text is not displaying as expected");
			}
			
		}

		public void checkForFaqQst(String property) throws Exception  {
			
			util.waitAndGetElement(faqTitle);
			
			for(int i=0;i<6;i++){
				
				if( ! property.contains(driver.findElement(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']")).getText()))
					
		    		throw new Exception ("Faq is not displaying as expected "+property);

			}
			
			
			
		}

		public void checkForPlusIcon() throws Exception  {
		
			for(int i=0;i<6;i++){
				
				if(! driver.findElement(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']")).isDisplayed())
					
		    		throw new Exception ("Plus icong for Faq is not displaying");

			}
			
		}

		public void waitForElementToDisplay (By checkObject) throws Exception{
			
			WebDriverWait wait = new WebDriverWait (driver, 100);
			wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
		}

		public void checkForExitButtonStatus(By checkObject) throws Exception {
			
			waitForElementToDisplay(checkObject);
			
			if(!driver.findElement(checkObject).isEnabled())
				throw new Exception("Exit button status is not enable");
			
		}

		
		public void checkForData(By checkObject, String property) throws Exception {
			
			waitForElementToDisplay(checkObject);
			
			String actualValue = driver.findElement(checkObject).getText();
			
			if(! property.equalsIgnoreCase(actualValue))
				throw new Exception( actualValue+" Displaying Data is not matching with the Test Data "+property);

			
		}

		public void checkForMenuItem(String property) throws Exception{
			
			if(! property.contains(driver.findElement(dataEntry).getText()) && property.contains(driver.findElement(estimate).getText())
					&& property.contains(driver.findElement(outCome).getText()))
				throw new Exception("Menu Item is not matching with the value "+property);
			
		}

		public void checkForDefaultValue(By checkObject, String property) throws Exception{
			
			Select select = new Select(driver.findElement(checkObject));
			WebElement option = select.getFirstSelectedOption();
			String selectedText = option.getText();
			
			if(! property.equals(selectedText))
				throw new Exception("Default value "+selectedText+" is not matching with the value "+property);
			
		}

		public void checkForPowerDropDownValue() throws Exception{
			
			String[] exp = {"0.5 kW", "1 kW", "1.5 kW", "2 kW", "2.5 kW", "3 kW(potenza attuale)", "3.5 kW", "4 kW", "4.5 kW", "5 kW", "5.5 kW", "6 kW", "7 kW","8 kW","9 kW","10 kW","15 kW","Inserimento manuale"};
		    WebElement dropdown = driver.findElement(powerValue);
		    Select select = new Select(dropdown);

		    List<WebElement> options = select.getOptions();
		    for (WebElement we : options) {
		        for (int i = 0; i < exp.length; i++) {
		            if (we.getText().equals(exp[i])) {
		                i++;
		                }
		        }
		    }
			
		}
		
		public void checkForVoltageDropDownValue() throws Exception{
			
			String[] exp = {"220 V(tensione attuale)","380 V"};
		    WebElement dropdown = driver.findElement(voltageValue);
		    Select select = new Select(dropdown);

		    List<WebElement> options = select.getOptions();
		    for (WebElement we : options) {
		        for (int i = 0; i < exp.length; i++) {
		            if (we.getText().equals(exp[i])) {
		                i++;
		                }
		        }
		    }
			
		}
		
		public void checkForLiftTypeDropDownValue() throws Exception{
			
			String[] exp = {"Scegli la tipologia di ascensore"," Argano"," Oleodinamico "};
		    WebElement dropdown = driver.findElement(liftTypeDropdown);
		    Select select = new Select(dropdown);

		    List<WebElement> options = select.getOptions();
		    for (WebElement we : options) {
		        for (int i = 0; i < exp.length; i++) {
		            if (we.getText().equals(exp[i])) {
		                i++;
		                }
		        }
		    }
			
		}

		public void checkForPlaceholderText(By checkObject, String property) throws Exception{
			
			String actualText = driver.findElement(checkObject).getAttribute("placeholder");
			
			if(! property.equalsIgnoreCase(actualText))				
					throw new Exception("Placeholder text is not matching with the value "+property);			
		}
		
		public void checkForChangePowerOrVoltageButtonStatus(By checkObject) throws Exception {
			
			String actualValue ="";
			boolean flag = false;
			
			if(!driver.findElement(checkObject).isEnabled()){
				
				actualValue = "Dissable";
				result ="Yes";
				flag =false;
			}
			else if(driver.findElement(checkObject).isEnabled()){
				actualValue = "Enable";
				flag= true;
			}
			
		}

		public void checkSuggestPowerButtonStatus() throws Exception{
			
			if(driver.findElement(suggestedPowerField).isEnabled())
				throw new Exception("Suggested Power field is enable ");
			
		}

		public void checkForPopup() throws Exception {
			
			driver.switchTo().activeElement();
			waitForElementToDisplay(popupTitle);
			
			verifyComponentExistence(popupTitle);
			verifyComponentExistence(popupClose);
			
			String actualtext = "Le informazioni relative all'apparato di sollevamento persone collegato alla tua fornitura le puoi trovare sul fascicolo tecnico dell'impianto rilasciato dalla società che ha provveduto all'installazione e collaudo.";
		
			String innerText = driver.findElement(popupInnerText).getText();
			
			if(! actualtext.equalsIgnoreCase(innerText))
				throw new Exception("Warning text is not matching with the value "+actualtext);
		
		}

	
		public void enterInputtoField(By checkObject, String property) throws Exception {			
			
				util.objectManager(checkObject, util.sendKeys, property);
			
		}

		public void checkForWarning(String property) throws Exception {
			
			if(! property.contains(driver.findElement(warningTitle).getText()) && property.contains(driver.findElement(warningText).getText()))
				throw new Exception("Warning text is not matching with the value "+property);
			
		}

		public void changePowerDropDownValue(String property) throws Exception {
			
			Select drpCountry = new Select(driver.findElement(voltageValue));
			drpCountry.selectByVisibleText(property);

			
		}

		public void checkVoltWarningText() throws Exception{
			
			String actualText = "Modificando la tensione da 220V a 380V verrà sostituito il contatore e la fornitura passerà da monofase (fase più neutro) a trifase (tre fasi più neutro), pertanto dovrai contattare un tecnico per riallacciare correttamente e in sicurezza il tuo impianto al contatore e alla rete del distributore.";
			
			String text = driver.findElement(voltWarningText).getText();
			
			if(!actualText.equals(text))
				throw new Exception("Warning text is not matching with the value "+actualText);

		}

		
		public void checkSuggestedPowerValue(String xpath, String property) throws Exception{
                
                WebDriverWait wait = new WebDriverWait(this.driver, 50);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
                JavascriptExecutor js = (JavascriptExecutor) driver;
                String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
                if(!s.equals(property))
                      throw new Exception ("Suggested power value is not matching with the test data");
                
         }
		
		public void checkFieldValue(String xpath, String property) throws Exception{
            
            WebDriverWait wait = new WebDriverWait(this.driver, 50);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
            if(!s.equals(property))
                  throw new Exception ("Suggested power value is not matching with the test data");
            
     }
			
		

		public void checkFaqOneDetails() throws Exception{
			
			verifyComponentExistence(faq1);
			verifyComponentExistence(faq1Plus);
			clickComponent(faq1Plus);
			
			String ans1 = driver.findElement(faq1).getText();
			
			if(faqAns.contains(ans1)){		
				result="YES";				
			}
			
			if(result.equals("No"))throw new Exception ("Faq one answer is not as expected");
			
			verifyComponentExistence(faq1Minus);
			
		}

		public void checkFaqDetails() throws Exception{
			
			for(int i=1;i<6;i++){
				
				verifyComponentExistence((By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']")));//faq
				verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));// plus
				
				clickComponent(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));//plus
				
				String ans = driver.findElement(By.xpath("//div[@id='accordion-panel-item-accordion-"+i+"']/p")).getText();
				
				if(!faqAns.contains(ans)){		
					result="YES";				
				}
				
				if(result.equals("No"))throw new Exception ("Faq one answer is not as expected");
				verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-minus']"));//minus
				verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+(i-1)+"']/span[@class='dsc-icon-line-plus']"));//prev plus
			}
			
		}

		public void changeDropDownValue(By checkObject,String property) throws Exception {
			
			Select dropdown = new Select(driver.findElement(checkObject));  
			dropdown.selectByVisibleText(property);  
			
		}

		public void checkForPopUp() throws Exception{
			
			driver.switchTo().activeElement();
			verifyComponentExistence(attenzionepopUp);
			verifyComponentExistence(attenzionepopUpText);
			verifyComponentExistence(attenzionepopupClose);
			verifyComponentExistence(popupNo);
			verifyComponentExistence(popupYes);
		
		}

		public void checkStartingCurrentField(By checkObject) throws Exception {
			
			boolean flag;
			if(!driver.findElement(checkObject).isDisplayed())
				
				 flag= true;
		}

		public void checkForHydraulicWarning(String property) throws Exception {
			
			if(! property.contains(driver.findElement(warningTitle).getText()) && property.contains(driver.findElement(warningText).getText()))
					throw new Exception("Warning text is not matching with the expected value");
			
		}

		public void checkforErrorMsg(By checkObject,String property) throws Exception{
			
			String actualText = driver.findElement(checkObject).getText();
			
			if(!actualText.equalsIgnoreCase(property))
				throw new Exception(actualText+" Error text is not matching with the expected message "+property);

		}
		
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			/*System.out.println("Normalized:\n"+weText);
			System.out.println(text);*/
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within displaying Text item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkTextHighlight(By object, String color) throws Exception{
			ColorUtils c = new ColorUtils();
			String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
			//System.out.println(colore);
			if(!colore.contentEquals(color)) throw new Exception("l'oggetto web  e' di colore "+color+". Colore trovato : "+colore);
		}
		
		public void checkFooterSection(By checkObject,String property[]) throws Exception{
			
				String actualText = driver.findElement(checkObject).getText();
			//	System.out.println(actualText+"Text");
				if(!Arrays.asList(property).contains(actualText))
					throw new Exception ("Footer Section is not matching with © Enel Energia S.p.a. |Tutti i Diritti Riservati| P. IVA 06655971007| Informazioni Legali| Privacy| Credits| Contattaci ");
		}
		
		public void checkButtonDissable(By checkObject) throws Exception {
			if(driver.findElement(checkObject).isSelected())
			throw new Exception("Button is enabled");
		}
		
		public void checkButtonIsEnable(By checkObject) throws Exception {

			if(!driver.findElement(checkObject).isEnabled())
				throw new Exception("Button is not enable");
		}
		
		public void clearField(By cellular2) throws Exception {
			driver.findElement(cellular2).clear();
			enterInputtoField(cellular2, "3333323231");
			if(driver.findElement(confirmCellular).isDisplayed()){
				enterInputtoField(confirmCellular, "3333323231");
			}
			
		}
		
		public void clearFieldValue(By checkObject) throws Exception {			
			util.waitAndGetElement(checkObject);
			driver.findElement(checkObject).clear();			
		}
		
		public void checkFieldDisplay(By checkObject) throws Exception {
			
			Boolean isPresent = driver.findElements(checkObject).size() > 0;
			if(isPresent.equals("true"))
				throw new Exception(checkObject+"Field is displaying");
		}
		
		public void selectSupply() {			
			Boolean isPresent = driver.findElements(By.xpath("//p[contains(text(),'attiva sulla fornitura')]")).size() > 0;
			System.out.println(isPresent);
			if(isPresent.toString().equals("true"))
			{
				try {
					clickComponent(By.xpath("//span[text()='Indietro']"));
					clickComponent(radioButton1);
					clickComponent(changePowerOrVoltageButton);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}	
		}
		
		public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
			textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
			WebElement problemElement = driver.findElement(objectWithText);
			String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
			if (elementText.length() < 1) {
				elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
			}
			if (!elementText.contains(textToCheck)) {
				String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
				System.out.println(message);
				throw new Exception(message);
			}
		}
		
		public void verifyElementColor(By locator, Color colorToCheck) throws Exception {
			Color elementColor = Color.fromString(driver.findElement(locator).getCssValue("color"));
			if (!elementColor.equals(colorToCheck)) {
				throw new Exception("The element located by:\n" + locator + "\nhas color: " + elementColor + "\ninstead of expected color: " + colorToCheck);
			}
		}
		
		public static final String servicesColor = "Crimson";
		public static final String bottomSection[] = {"© Enel Energia S.p.a.","Tutti i Diritti Riservati","Gruppo IVA Enel P.IVA 15844561009", "Informazioni Legali","Privacy","Credits","Contattaci"};
		public static final String ModificaSubText = "Seleziona la fornitura sulla quale intendi modificare la potenza e/o la tensione";
		public static final String TitleSubText = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA OSTIENSE 45, 00154, ROMA, ROMA, RM";
		public static final String menuItem = "1 Inserimento dati, 2 Preventivo, 3 Esito";
		public static final String agroWarning = "Attenzione La potenza consigliata è di 1.5 kW. Ti consigliamo di consultare la scheda tecnica relativa al tuo apparato di sollevamento per verificare la tensione necessaria per il corretto funzionamento.";
		public static final String agroWarning1="AttenzioneLa tua potenza attuale è uguale alla potenza consigliata per il funzionamento dell'apparato di sollevamento.Ti consigliamo di consultare la scheda tecnica relativa al tuo apparato di sollevamento per verificare la tensione necessaria per il corretto funzionamento.";
		public static final String FormLine1 = "Inserisci di seguito la potenza e/o tensione che vuoi impostare sulla fornitura selezionata:";
		public static final String FormLine2 = "Sulla fornitura sono collegati apparati di sollevamento persone (es. ascensore)?";
		public static final String FormLine3 = "Conferma o modifica i tuoi dati di contatto sui quali ricevere la documentazione";
		public static final String Attendiqualchesecondo = "Stiamo elaborando il preventivo sulla base della tua richiesta";
		public static final String emptyEmailError = "Il campo Email è obbligatorio.";
		public static final String invalidEmailError = "Email non valida";
		public static final String confirmEmailErrorMsg = "Le email non corrispondono";
		public static final String cellularErrorMsg = "Il campo Cellulare è obbligatorio.";
		public static final String InvalidCellularErrorMsg = "Il campo Cellulare può contenere solo caratteri numerici.";
		public static final String PVErrorMsg = "Impossibile procedere se non si modificano i valori attuali di potenza e/o tensione";
		public static final String LiftTypeErrorMsg ="Selezionare la tipologia di ascensore";
		public static final String correnteaRegimeErrorMsg = "Il campo Corrente a regime (A) è obbligatorio.";
		public static final String CorrentediavviamentoErrorMsg = "Il campo Corrente di avviamento (A) è obbligatorio.";
		public static final String Faq = "Cosa si intende per potenza e tensione? Quando è necessario modificare la potenza? Quali sono i tempi per l’esecuzione della modifica di potenza o tensione? Cosa si intende per sopralluogo tecnico? Quali sono i costi dell’operazione?"
				+ "Come posso pagare il preventivo?";
		public static final String MenuItem = "Inserimento dati Preventivo Esito";
		public static final String Placeholder = "Inserisci il valore in Ampere";
		public static final String Warning = "Attenzione La potenza inserita non è sufficiente per il sollevamento";
		public static final String Powerdefault = "3 kW(potenza attuale)";
		public static final String PVvalue = "3 kW / 220 V";
		public static final String Voltagefault = "220 V(tensione attuale)";
		public static final String SopralluogoText1 = "Non è stato possibile sviluppare il preventivo online perché sulla base dei valori che hai inserito per la tua fornitura è necessario un sopralluogo da parte di un tecnico del tuo distributore.";
		public static final String SopralluogoText2 = "Di seguito puoi visualizzare i dati che hai inserito relativi ai nuovi parametri contrattuali e i tuoi canali di comunicazione dove verrai contattato:";
		public static final String SopralluogoText3 = "Se decidi di continuare, verrà contattato il distributore competente per il preventivo definitivo. Questo potrebbe comportare l’invio di un tecnico per un sopralluogo sul tuo impianto.";
		public static final String SopralluogoText4 = "La modifica di potenza e/o tensione verrà applicata sulla seguente fornitura:";

		//----- Constants added for TC135 -----
		public final String supplySelectionTitle = "Modifica potenza e/o tensione";
		public final String supplySelectionSubtitle = "Seleziona la fornitura sulla quale intendi modificare la potenza e/o la tensione";
		public final String voltageUpdateString = "Aggiornamento tensione\r\n" + 
				"\r\n" + 
				"Modificando la tensione da 220V a 380V verrà sostituito il contatore e la fornitura passerà da monofase (fase più neutro) a trifase (tre fasi più neutro), pertanto dovrai contattare un tecnico per riallacciare correttamente e in sicurezza il tuo impianto al contatore e alla rete del distributore.";
		public final String messagePart1 = "Non è stato possibile sviluppare il preventivo online perché sulla base dei valori che hai inserito per la tua fornitura è necessario un sopralluogo da parte di un tecnico del tuo distributore.";
		public final String messagePart2 = "Di seguito puoi visualizzare i dati che hai inserito relativi ai nuovi parametri contrattuali e i tuoi canali di comunicazione dove verrai contattato:";
		public final String messagePart3 = "Se decidi di continuare, verrà contattato il distributore competente per il preventivo definitivo. Questo potrebbe comportare l’invio di un tecnico per un sopralluogo sul tuo impianto.";
		public final String inspectionPopupTitleText = "Attendi qualche secondo...";
		public final String inspectionPopupBodyText = "Stiamo verificando la necessità di un sopralluogo tecnico";
		public final String resultsPageTitleText = "La tua richiesta è stata presa in carico";
		public final String resultsPageBodyText = "Riceverai a breve una email di conferma di avvio della richiesta di modifica potenza e/o tensione, unitamente alle indicazioni relative alle modalità d'intervento da parte del tecnico della rete.Grazie per aver utilizzato il servizio!";
		public final Color greenColor = Color.fromString("#008C5A");
		public final String setVoltageValue = "380 V";
		public final String TitleSubText_141 = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA OSTIENSE 45, 00154, ROMA, ROMA, RM"; 
		public final String TitleSubText_134 = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA PASQUALE LEONE 4, 73015, SALICE SALENTINO, SALICE SALENTINO, LE";

}
