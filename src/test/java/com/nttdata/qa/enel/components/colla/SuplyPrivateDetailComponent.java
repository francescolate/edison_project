package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.ColorUtils;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SuplyPrivateDetailComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By HomePageClose = By.xpath("//*[@id='modalPopupLogin']/div[1]/div[2]/button");
    //Supply Details page 
    
//    public By GasDetail = By.xpath("//*[@id='wrapper_modalforn']/div[2]/div[2]/a[2]");
    public By GasDetail = By.xpath("//span[@class='icon-line-flame']/../../../../..//div[@class='button-container']/a[2]");
    public By ShowMoreClick = By.xpath("//*[@id='mainContentWrapper']/div/section/div[3]/a");
    
    public By StatoFornitura = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[1]/dt");
    public By Statopagamento = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[2]/dt");
    public By Modalitàdipagamento = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[3]/dt");
    public By Ricezionebollette = By.xpath("//*[@id='accessibility-ricezione']/dt");
    public By Datadiattivazione = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[5]/dt");
    public By Offertaattiva = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[6]/dt");
    public By NumeroOfferta = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[7]/dt");
    public By PDR = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[8]/dt");
    public By Categoriadiconsumo = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[9]/dt");
    public By Contratto = By.xpath("//*[@id='mainContentWrapper']/div/section/div[2]/dl/span[10]/dt");
    
    public By RenameSupply = By.xpath("//*[@id='mainContentWrapper']/div/section/div[1]/nav/a[1]");
    public By ViewBills = By.xpath("//*[@id='mainContentWrapper']/div/section/div[1]/nav/a[2]");
    
    public By InputNomeFornitura = By.xpath("//*[@id='input_nomeFornitura']");
    
    public By NomeFornitura = By.xpath("//*[@id='lebel_nomeFornitura']");
    public By Campi = By.xpath("//*[@id='rinominaFornStep1']/div/div[2]/span");
    public By Indietro = By.xpath("//*[@id='rinominaFornStep1']/div/div[2]/div[2]/button[1]");
    public By Continua = By.xpath("//*[@id='rinominaFornStep1']/div/div[2]/div[2]/button[2]");
    
    public By Inserimento = By.xpath("//*[@id='rinominaFornStep1']/div/div[1]/ul/li[1]/span[2]");
    public By Riepilogo  = By.xpath("//*[@id='rinominaFornStep1']/div/div[1]/ul/li[2]/span[2]");
    public By Esito = By.xpath("//*[@id='rinominaFornStep1']/div/div[1]/ul/li[3]/span[2]");
  //*[
    public By Questo = By.xpath("//*[@id='errorRinomina']/span");
    
    public By Inserimentodati = By.xpath("//*[@id='accessibility']/span[2]");
    public By RiepilogoConferma = By.xpath("//*[@id='rinominaFornStep2']/div/div[1]/ul/li[2]/span[2]");
    
    //LeftMenu
    public By LeftMenuList = By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]");
    
    
    
    //Servizi
    
    public By ServiziLink = By.xpath("//a[@id='servizi']//span[contains(text(),'Servizi')]") ;
    public By FornituraTitle = By.xpath("//h2[contains(text(),'Servizi per le forniture')]");
    public By FornituraTitleContent = By.xpath("//section[@id='forniture-id0']//p[contains(text(),'Di seguito potrai visualizzare tutti i servizi per')]");
    public By BolletteTitle = By.xpath("//h2[contains(text(),'Servizi per le bollette')]");
    public By BolletteTitleContent= By.xpath("//section[@id='forniture-id1']//p[contains(text(),'Di seguito potrai visualizzare tutti i servizi per')]");
    public By ContratoTitle = By.xpath("//h2[contains(text(),'Servizi per il contratto')]");
    public By ContratoTitleContent = By.xpath("//section[@id='forniture-id2']//p[contains(text(),'Di seguito potrai visualizzare tutti i servizi per')]");
    
    public By FornitureContents = By.xpath("//*[@id='forniture-id0']/ul/li");
    public By BolletteContents = By.xpath("//*[@id='forniture-id1']/ul/li");
    
    public By ModifficaIndrizzolink = By.xpath("//section[@id='forniture-id0']//li[3]//a[1]");
    
    public By ModificaDetails = By.xpath("//fieldset[@id='disdetta']/div/label/span/span[not(contains(@class,'icon'))]");
    public By ModificaEsci = By.xpath("//button[@id='exitButton']");
    public By ModificaTitle = By.xpath("//h1[@id='h1_mif']");
    public By ModificaContents = By.xpath("//*[@id='sc']/div/div/div/div/div/h2[contains(text(),'Seleziona una o più forniture su cui intendi modif')]");
    public By ModificaCheckbox = By.xpath("//span[@id='checkbox-item-0']");
    public By ModificaCheckbox1 = By.xpath("//span[@class='icon-ckbox']");
    
    public By ModificaButton = By.xpath("//button[@id='nextButton']");
    
    public By CittaLabel = By.xpath("//*[@id='data-label-Città']");
    public By CittaInputField = By.xpath("//input[@id='206:0']");
    public By CittaInputField1 = By.xpath("//input[@id='226:0']");
    public By CittaInputField176 = By.xpath("//input[@id='201:0']");
    public By CittaInputField176New = By.xpath("//input[@id='206:0']");
    public By CittaDropDownRoma = By.xpath("//div[@class='slds-lookup__menu slds-dropdown slds-dropdown_fluid ']/ul[@class='slds-lookup__list']/li[1]");
    
    
    public By IndrizzoLabel = By.xpath("//label[@id='data-label-Indirizzo']");
    public By IndrizzoInput = By.xpath("//input[@id='303:0']");
    public By IndrizzoInputNew = By.xpath("//input[@id='346:0']");
    public By IndrizzoInput176 = By.xpath("//input[@id='298:0']");
    public By IndrizzoInput176New = By.xpath("//input[@id='303:0']");
    public By IndrizzoDropDownAntica = By.xpath("//*[@id='sc']/div/div/div/div/div/div/div[2]/div[1]/div/div[4]/div/form/div/div[1]/div/div[7]/div/div/div[2]/ul/li/span/div/span/div/ul/li");
    public By IndrizzoInputField1 = By.xpath("//input[@id='323:0']");
    public By IndrizzoInputAfterWarn = By.xpath("//input[@id='346:0']");
    
    public By NumeroCivicoLabel = By.xpath("//label[@id='data-label-Civico']");
    public By NumercoCivicInput = By.xpath("//*[@id='364:0']");
    public By NumercoCivicInput176 = By.xpath("//input[@id='359:0']");
    public By NumercoCivicInput176New = By.xpath("//input[@id='364:0']");
    public By NumercoCivicInputField1 = By.xpath("//input[@id='384:0']");
    
    public By ScalaLable = By.xpath("//label[contains(text(),'Scala')]");
    public By ScalaInput = By.xpath("//input[@id='382:0']");
    
    public By PianoLabel = By.xpath("//label[contains(text(),'Piano')]");
    public By PianoInput = By.xpath("//input[@id='400:0']");
    
    public By InternoLabel = By.xpath("//label[contains(text(),'Interno')]");
    public By InternoInput = By.xpath("//input[@id='418:0']");
    
    public By Caplabel = By.xpath("//label[@id='labelId']");
    public By CaplabelNew = By.xpath("//label[contains(text(),'CAP*')]");
    public By CapInput = By.xpath("//input[@id='447:0']");
    public By CapDropdownValue = By.xpath("//div[contains(@class,'slds-lookup__menu slds-dropdown')]/ul/li[1]");	
    
    public By CapIRoma = By.xpath("//span[text()='ROMA']");
    
    public By PressoLabel = By.xpath("//label[contains(text(),'Presso')]");
    public By PressoInput = By.xpath("//input[@id='465:0']");
    
    public By InterioBtn = By.xpath("//button[@class='button_second']");
        
    //public By ContinuaBtn = By.xpath("//*[@id='sc']/div/div/div/div/div/div/div[2]/div[2]/button[2]");
    public By ContinuaBtn = By.xpath("//button/span[text()='Continua']");
    public By MandatoryFieldValue = By.xpath("//span[@class='foot-notes cITA_IFM_LCP451_SelfCareAddress']");
    
    public By CittaWarningMessage = By.xpath("//span[@id='errorMsgMIF']");
    
    public By AttenzionePopup = By.xpath("//h3[@id='titleModal']");
    public By AttenzionePopupText = By.xpath("//p[@class='inner-text']");
    public By AttenzionePopupClose = By.xpath("//*[@id='modalAlert']/div/button");
    public By DropdownBivo = By.xpath("//*[@id='330:0']/option[contains(text(),'BIVIO')]");
    public By VerifyInserimento1 = By.xpath("//div[@class='details-container']//dl/span");
    public By VerifyInserimento2 = By.xpath("//h4[@class='plan-main-head']");
    public By VerifyInserimento3 = By.xpath("//*[@id='form_mifRecap']/div[2]/fieldset/div/div/span/span");
//    public By Conferma = By.xpath("//*[@id='sc']/div/div/div/div/div/div/div[2]/div[2]/button[2]");
    public By Conferma = By.xpath("//button/span[text()='Conferma']");
    public By ConfermaTitle = By.xpath("//h3[@id='id_title']");
    public By ConfermaContents = By.xpath("//*[@id='sc']/div/div/div/div/div/div/div[2]/div");
    public By Finebutton = By.xpath("//*[@id='sc']/div/div/div/div/div/div/div[2]/div[2]/button");
    
    
    //Bollette 
    public By BolletteLink = By.xpath("//a[@id='bollette']");
    public By BolletteDetailsHeading = By.xpath("//*[@id='mainContentWrapper']/div[1]/div/h1");
    public By BolletteDetailsHeadingContent = By.xpath("//*[@id='mainContentWrapper']/div[1]/div/p[2]");
    public By BolletteHeading = By.xpath("//div[@id='mainContentWrapper']//h1[contains(text(),'Benvenuto nella tua area privata')]");
    public By BolletteHeadingContents = By.xpath("//*[@id='mainContentWrapper']/div[1]/p[2]");
    public By LucedetailsContents = By.xpath("//div[@class='tab-supply-container open']/div[2]/ul/li");
    public By VisualizzaTutte = By.xpath("//div[@id='bill-list-panel-0']//a[contains(text(),'Visualizza tutte le bollette')]");
    public By BolletTitle = By.xpath("//h1[contains(text(),'Le tue bollette')]");
    public By BolletContent = By.xpath("//div[@class='text parbase']//p[contains(text(),'e')]");
    public By GasName = By.xpath("//h2[@id='fornitura1']");
    public By GasDetails = By.xpath("//div[@class='tab-supply-container']//dl[@class='tab-details-heading']/span");
    public By ServiziBolletteTitle = By.xpath("//h2[contains(text(),'Servizi per le bollette')]");
    public By ServiziContents = By.xpath("//p[contains(text(),'Di seguito potrai visualizzare tutti i servizi per')]");
    public By GasDetailsList = By.xpath("//ul[@class='tileWrapper']/li");
    public By MostraFiltri = By.xpath("//button[@id='filter-button-mostra']");
    public By LuceDocumentMessage = By.xpath("//ul[@id='fornitura_1']//p[contains(text(),'Non sono stati trovati documenti che rispettano i')]");
    public By LuceWarningMessage = By.xpath("//div[@class='tab-supply-component']//div[@id='bill-list-panel-0']/ul/div/h2/span/p");
    public By LuceDetailsSection = By.xpath("//div[@class='tab-supply-component']//ul[@id='fornitura_0']/li");
    public By GasExpanIcon = By.xpath("//button[@id='bill-list-button-1']");
  
    //Checkboxes
    public By DaPagareCheckbox = By.xpath("//*[@id='check-id1']");
    public By DaPagareLabel = By.xpath("//*[@id='stato_pagamento']/div[1]/label");
    public By DaPagareCheckboxStatus = By.xpath("//fieldset[@id='stato_pagamento']//div[1]");
    public By InPagamentocheckbox = By.xpath("//input[@id='check-id2']");
    public By InPagamentolabel = By.xpath("//*[@id='stato_pagamento']/div[2]/label");
    public By PagatoCheckbox = By.xpath("//input[@id='check-id3']");
    public By PagatoLabel = By.xpath("//label[contains(text(),'Pagato')]");
    public By TipodiDocumentoLabel = By.xpath("//legend[contains(text(),'Tipo di documento')]");
    public By AnnoLabel = By.xpath("//span[contains(text(),'Anno')]");
    public By FornituraLable = By.xpath("//legend[contains(text(),'Fornitura')]");
    public By BolletaCheckbox = By.xpath("//input[@id='check-id4']");
    public By BolletaLabel = By.xpath("//*[@id='tipo_documento']/div[1]/label");
    public By CheckBox2020 = By.xpath("//input[@id='check-id6']");
    public By CheckBox2019	= By.xpath("//input[@id='check-id7']");
    public By CheckBox2018	= By.xpath("//input[@id='check-id8']");
    public By Label2020 = By.xpath("//*[@id='anno']/div[1]/label");
    public By Label2019 = By.xpath("//*[@id='anno']/div[2]/label");
    public By Label2018 = By.xpath("//*[@id='anno']/div[3]/label");
    public By LuceCheckbox= By.xpath("//input[@id='check-id00']");
    public By LuceLabel = By.xpath("//body//div[@class='fieldset-container']//div//div[1]//label[1]");
    public By GasCheckbox = By.xpath("//input[@id='check-id01']");
    public By GasLabel = By.xpath("//fieldset[@id='fornitura']//div[2]//label[1]");
    public By ApplicaButton = By.xpath("//button[@class='button button-first button-center']");
    public By ReimpostaButton = By.xpath("//*[@id='filter-panel']/form/div[2]/button[1]");
    
    
    		
 
    public SuplyPrivateDetailComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}

	public void launchLink(String url) throws Exception {
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e) {
			throw new Exception("it's impossible the link " + url);

		}
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{
		ColorUtils c = new ColorUtils();
		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));
	//	System.out.println(colore);
		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
	}
	
	public void homepageRESIDENTIALMenu(By checkObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//nav[@id='mainNav']/ul/li/a[not(contains(@style, 'display:none'))]"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Forniture"))
	        item_found="YES";
	   
	    else if(description.contentEquals("Bollette"))
	        item_found="YES";
	    else if (description.contentEquals("Servizi"))
	        item_found="YES";
	    else if (description.contentEquals("enelpremia WOW!"))
	        item_found="YES";
	    else if (description.contentEquals("Novità"))
	        item_found="YES";
	    else if (description.contentEquals("Spazio Video"))
	        item_found="YES";
	    else if (description.contentEquals("Account"))
	        item_found="YES";
	    else if (description.contentEquals("I tuoi diritti"))
	        item_found="YES";
	    else if (description.contentEquals("Supporto"))
	        item_found="YES";
	    else if (description.contentEquals("Trova Spazio Enel"))
	        item_found="YES";
	    else if (description.contentEquals("Esci"))
	    	item_found="YES";
	   
	        if (item_found.contentEquals("NO"))
	        throw new Exception("Website 'https://www-colla.enel.it/ Resedential left menu section are not present with description 'Forniture', 'Bollette','Servizi','enelpremia WOW!','Novità','Spazio Video','Account','I tuoi diritti','Supporto','Trova Spazio Enel','Esci'");
	        
	        
	            }
	    
	        }

	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void jsClickComponent(By clickableObject) throws Exception {
		util.jsClickElement(clickableObject);
	}
	
	public void VerifyForniture(By checkObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//*[@id='forniture-id0']/ul/li"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Autolettura"))
	        item_found="YES";
	   
	    else if(description.contentEquals("InfoEnelEnergia"))
	        item_found="YES";
	    else if (description.contentEquals("Servizi"))
	        item_found="YES";
	    else if (description.contentEquals("Modifica Indirizzo Fatturazione"))
	        item_found="YES";
	    else if (description.contentEquals("Modifica Potenza e/o Tensione"))
	        item_found="YES";
	    else if (description.contentEquals("Modifica Contatti e Consensi"))
	        item_found="YES";
	    
	   
	        if (item_found.contentEquals("NO"))
	        throw new Exception("In Forniture 'Autolettura','InfoEnelEnergia,Modifica Indirizzo Fatturazione','Modifica Potenza e/o Tensione','Modifica Contatti e Consensi' are missing");
	        
	        
	            }
	    
	        }
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 60))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	public void VerifyBollette(By checkObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//*[@id='forniture-id1']/ul/li"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Forniture"))
	        item_found="YES";
	   
	    else if(description.contentEquals("Bolletta Web"))
	        item_found="YES";
	    else if (description.contentEquals("Pagamento Online"))
	        item_found="YES";
	    else if (description.contentEquals("Addebito Diretto"))
	        item_found="YES";
	    else if (description.contentEquals("Rettifica Bolletta"))
	        item_found="YES";
	    else if (description.contentEquals("Rateizzazione"))
	        item_found="YES";
	    else if (description.contentEquals("Bolletta di Sintesi o di Dettaglio"))
	        item_found="YES";
	    else if (description.contentEquals("Duplicato Bolletta"))
	        item_found="YES";
	   	   
	        if (item_found.contentEquals("NO"))
	        throw new Exception("In Bollette 'Bolletta Web','Pagamento Online','Addebito Diretto','Rettifica Bolletta','Rateizzazione','Bolletta di Sintesi o di Dettaglio','Duplicato Bolletta'");
	        
	        
	            }
	    
	        }
		public void clickComponentIfExist(By existObject) throws Exception {
		if (util.exists(existObject, 15)) {
			util.objectManager(existObject, util.scrollToVisibility, false);
		    util.objectManager(existObject, util.scrollAndClick);
		}
	}
			
	public void enterLoginParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void enterText(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeysCharByChar, valore);
	}
	public void VerifyText(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String actualtext = element.getText();
		
		if (actualtext.equals(Value))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
				}
	
public void GasHeadingDetails(By GasHeadingObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(GasHeadingObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//div[@class='tab-supply-container']//dl[@class='tab-details-heading']/span"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Numero Cliente 300001597"))
	        item_found="YES";
	   
	    else if(description.contentEquals("Fornitura Gas"))
	        item_found="YES";
	    else if (description.contentEquals("Indirizzo: VIA VIRGINIA 12/A - 40017 SAN GIOVANNI IN PERSICETO BO"))
	        item_found="YES";
	 
	        if (item_found.contentEquals("NO"))
	        throw new Exception("Heading details are incorrect");
	        
      }
	    
 }

public void gasDetails(By GasDetailsObject) throws Exception {
	
    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(GasDetailsObject));
                   
    String item_found="NO";
    List<WebElement> leftMenu = driver.findElements(By.xpath("//ul[@class='tileWrapper']/li"));
   
    for(WebElement element : leftMenu){
    String description=element.getText();
    description= description.replaceAll("(\r\n|\n)", " ");
       
    if (description.contentEquals("Bolletta Web"))
        item_found="YES";
   
    else if(description.contentEquals("Bolletta Web"))
        item_found="YES";
    else if (description.contentEquals("Pagamento Online"))
        item_found="YES";
    else if (description.contentEquals("Rettifica Bolletta"))
        item_found="YES";
    else if (description.contentEquals("Rateizzazione"))
        item_found="YES";
    else if (description.contentEquals("Bolletta di Sintesi o di Dettaglio"))
        item_found="YES";
    else if (description.contentEquals("Invio Attestazione di Pagamento"))
        item_found="YES";
  
   	   
        if (item_found.contentEquals("NO"))
        throw new Exception("Gas details are not correct");
        
   }
    
}

public void verifyLuceDetatils(By LuceDetailsObject) throws Exception {
	
    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(LuceDetailsObject));
                   
    String item_found="NO";
    List<WebElement> leftMenu = driver.findElements(By.xpath("//div[@class='tab-supply-container open']/div[2]/ul/li"));
   
    for(WebElement element : leftMenu){
    String description=element.getText();
    description= description.replaceAll("(\r\n|\n)", " ");
       
    if (description.contentEquals("Commodity Luce Bolletta n° 3012704002 Scade il 27/02/2019 Importo 148,59 € Stato Pagamento Addebitata su C/C"))
        item_found="YES";
    else if(description.contentEquals("Commodity Luce Bolletta n° 2976114874 Scade il 27/12/2018 Importo 118,88 € Stato Pagamento Addebitata su C/C"))
        item_found="YES";
    else if (description.contentEquals("Commodity Luce Bolletta n° 2963895516 Scade il 26/10/2018 Importo 121,81 € Stato Pagamento Addebitata su C/C"))
        item_found="YES";
    else if (description.contentEquals("Commodity Luce Bolletta n° 2950534202 Scade il 28/08/2018 Importo 129,33 € Stato Pagamento Addebitata su C/C"))
        item_found="YES";
    else if (description.contentEquals("Commodity Luce Bolletta n° 2938057049 Scade il 28/06/2018 Importo 237,52 € Stato Pagamento Addebitata su C/C"))
        item_found="YES";
       	   
    if (item_found.contentEquals("NO"))
    throw new Exception("Luce details are incorrect");
        
        
            }
    
    }
public void verifyCheckboxSelect(By CheckboxObject) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(CheckboxObject));
	
	String checkbox="NO";
	WebElement element = driver.findElement(CheckboxObject);
	element.getText();
	
	if (element.isSelected()){
	checkbox="YES";
	}	
	if (!element.isSelected()){
	checkbox = "NO";
		throw new Exception("The checkbox is not selected");
	}
}

public void verifyCheckboxnotSelect(By NocheckboxObject) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
	
	String checkbox="NO";
	
	WebElement element = driver.findElement(NocheckboxObject);
	
	if (!element.isSelected()){
	checkbox="YES";
	}			
	if (element.isSelected()){
	checkbox = "NO";
		throw new Exception("The checkbox is selected");
			}
}
	public void backBrowser() throws Exception {
		try {
			driver.navigate().back();
			Thread.sleep(5000);
			
		} catch (Exception e) {
			throw new Exception("Previous Page not displayed");

		}
		
		
	}
	public void verifyInserimento(By checkObject) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		List<WebElement> leftMenu = driver.findElements(checkObject);
		   String item_found = "NO";
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
		
		if (description.contentEquals("Indirizzo Via Appia Antica,1"))
	        item_found="YES";
	    else if(description.contentEquals("Località ROMA"))
	        item_found="YES";
	    else if (description.contentEquals("Provincia ROMA"))
	        item_found="YES";
	    else if (description.contentEquals("CAP 00179"))
	        item_found="YES";
	    else if (description.contentEquals("Presso -"))
	        item_found="YES";
	    else if (description.contentEquals("L'indirizzo di fatturazione verrà aggiornato su:"))
	        item_found="YES";
	    if (item_found.contentEquals("NO"))
	    throw new Exception("Inserimento dati detail are incorrect");
	    
	    }
	}
public void modificaDetails(By ModificaDetailsObject) throws Exception {
		
	    WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    WaitVar.until(ExpectedConditions.visibilityOfElementLocated(ModificaDetailsObject));
	                   
	    String item_found="NO";
	    List<WebElement> leftMenu = driver.findElements(By.xpath("//fieldset[@id='disdetta']/div/label/span/span[not(contains(@class,'icon'))]"));
	   
	    for(WebElement element : leftMenu){
	    String description=element.getText();
	    description= description.replaceAll("(\r\n|\n)", " ");
	       
	    if (description.contentEquals("Indirizzo della fornitura Via Virginia 12 A 40017 San Matteo Della Decima San Giovanni In Persiceto Bo"))
	        item_found="YES";
	   
	    else if(description.contentEquals("Numero Cliente 300001597"))
	        item_found="YES";
	    else if (description.contentEquals("Attuale indirizzo di Fatturazione Via Nizza 152 00198 Roma Roma Roma Lazio Rm"))
	        item_found="YES";
	    else if (description.contentEquals("Indirizzo della fornitura Via Virginia 12 A 40017 San Matteo Della Decima San Giovanni In Persiceto Bo"))
	        item_found="YES";
	    else if (description.contentEquals("Numero Cliente 300001594"))
	        item_found="YES";
	    else if (description.contentEquals("Attuale indirizzo di Fatturazione Via Della Pisana 66 00163 Roma Roma Roma Lazio Rm"))
	        item_found="YES";
	       	   
	        if (item_found.contentEquals("NO"))
	        throw new Exception("Gas details are not correct");
			}
			
	}

}
