package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;

/**
 * @author ChianteseRo
 * Il seguente componente si occupa di effettuare l'accesso ad una sezione del  
 * menu a tendina del primo tab aperto 
 */
public class AccediTabPaginaPrincipaleComponent 
{
    private WebDriver driver;
    private SeleniumUtilities util;
    
    public AccediTabPaginaPrincipaleComponent(WebDriver driver) 
    {
	    this.driver = driver;
	    PageFactory.initElements(driver, this);
    } 
    
    private By byOpenObject = By.xpath("//span[(text()='Richieste' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]");
    private By byOpenObjectOfferta = By.xpath("//span[(text()='Offerte' and @class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity')]");
    

//    public static final String X_DROPDOWN = "//button[@class='slds-button slds-button_icon slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container"
//    		+ "']/lightning-primitive-icon[1]" ;

    public static final String X_DROPDOWN = "//button[@class='slds-button slds-button_icon slds-p-horizontal__"
    		+ "xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon[1]" ;


    @FindBy(xpath = X_DROPDOWN)
    private WebElement dropdwonButton;

	
	/**
	 * @param xpathTab -> Object By per identificare il Tab da aprire
	 * @throws Exception
	 */
	public void openTab(By xpathTab) throws Exception
	{
//		String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(5);
		dropdwonButton.click();
		TimeUnit.SECONDS.sleep(2);
		byOpenObject = xpathTab;
		WebElement element = driver.findElement(byOpenObject);
		element.click();

//		util.frameManager(frame, serachObject, util.scrollAndClick);
		
	}
	
	public void openTabOfferta(By xpathTab) throws Exception
	{
//		String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(5);
		dropdwonButton.click();
		TimeUnit.SECONDS.sleep(2);
		byOpenObject = xpathTab;
		WebElement element = driver.findElement(byOpenObjectOfferta);
		element.click();

//		util.frameManager(frame, serachObject, util.scrollAndClick);
		
	}

	
}
