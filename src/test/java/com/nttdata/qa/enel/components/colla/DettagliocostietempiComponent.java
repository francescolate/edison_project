package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;



public class DettagliocostietempiComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	public By lavoroCost = By.xpath("//div[2]/div/p[@class='totalCostContainer']/span[1]");
	public By sommaSeguenti = By.xpath("//div[2]/div/p[@class='totalCostContainer']/span[2]");
	public By item1 = By.xpath("//div[2]//div[@class='dtl-container']/dl/span[1]");
	public By item2 = By.xpath("//div[2]//div[@class='dtl-container']/dl/span[2]");
	public By item3 = By.xpath("//div[2]//div[@class='dtl-container']/dl/span[3]");
	public By tempiDiEsecuzioneHeading =  By.xpath("//div[2]/div[@class='ditTableContainer']/h4");
	public By tempiDiEsecuzioneParagraph1 = By.xpath("//div[2]/div[@class='ditTableContainer']/p[2]");
	public By tempiDiEsecuzioneParagraph2 = By.xpath("//div[2]/div[@class='ditTableContainer']/p[3]");
	public By tempiDiEsecuzioneParagraph3 = By.xpath("//div[2]/div[@class='content-group tiny-border'][1]");
	public By radioAddebitoinBolletta = By.xpath("//div[@class='radio-group-container']/div[@class='radio-container'][1]/label");
	public By radioVoglioPagareOnline = By.xpath("//div[@class='radio-group-container']/div[@class='radio-container'][2]/label");
	public By checkBoxDichiaro = By.xpath("//label[@id='inputchecklinklabel']");
	public By confermaButton = By.xpath("//div[@class='right-main']/div[@class='button-container']/button[@id='nextButton']/span");
	public By attizoneHeading = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/h3[@class='bold-title']");
	public By attizoneTitle = By.xpath("//div[@id='modalAlert']/div[@class='modal-header']/p[@class='inner-text']");
	public By noButton = By.xpath("//button[@id='overlayNoButton']");
	public By yesButton = By.xpath("//button[@id='overlayYesButton']");
	public By fineButton = By.xpath("//div[@class='right-main']/div[@class='button-container']/button[@class='button_first']");
	public By dettaglioFornituraButton = By.xpath("//div[@id='wrapper_modalforn']/div[6]/div/a[2]");
	public By detaglioFurnitura_310501884 = By.xpath("//dd[text()='310501884']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By detaglioFurnitura_310511532 = By.xpath("//dd[text()='310511532']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By detaglioFurnitura_310511532New = By.xpath("//dd[text()='310511532']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Gestisci fornitura')]");
	public By detaglioFurnitura_930943292 = By.xpath("//dd[text()='930943292']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By detaglioFurnitura_930943292New = By.xpath("//dd[text()='930943292']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Dettaglio fornitura')]");
	public By detaglioFurnitura_930943292NewUpdated = By.xpath("//dd[text()='930943292']/ancestor::div[@class='details-container']//following-sibling::div[@class='button-container']/a[contains(text(),'Gestisci fornitura')]");
	public By radioIT002E9834235A = By.xpath("//span[@id='checkbox-item-1']");
	public By radioIT001E64378944 = By.xpath("//span[text()='IT001E64378944']/ancestor::span[@class='cliente']/preceding-sibling::span[@class='icon-ckbox']");
	public By radioIT002E5415888A = By.xpath("//span[text()='IT002E5415888A']/ancestor::span[@class='cliente']/preceding-sibling::span[@class='icon-ckbox']");
	public By radioIT002E3831521A = By.xpath("//span[text()='IT002E3831521A']/ancestor::span[@class='cliente']/preceding-sibling::span[@class='icon-ckbox']");
	public By subTitleMPT = By.xpath("//div[@id='sc']/div/div[@class='list-form-container']/h2");
	public By attendiPopUp = By.xpath("//div[@class='modal-dialog']/div[@id='modalAlert']/div[@class='modal-header']/p[@class='inner-text']");
	public By inserimentoDati = By.xpath("//ul/li/span[@class='step-label active']");
	public By preventivo = By.xpath("//ul/li[2]/span[@class='step-label']");
	public By esito = By.xpath("//ul/li[3]/span[@class='step-label']");
	
	public DettagliocostietempiComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		Thread.sleep(5000);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
 
 public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
 
 public void waitForElementToDisplay (By checkObject) throws Exception{
		
		WebDriverWait wait = new WebDriverWait (driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
	}

 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	public void changeDropDownValue(By checkObject,String property) throws Exception {
		
		Select dropdown = new Select(driver.findElement(checkObject));  
		dropdown.selectByVisibleText(property);  
		
	}
	public void checkData(By checkObject,String property) throws Exception{
		util.waitAndGetElement(checkObject);
		util.scrollToElement(driver.findElement(checkObject));
		String actualValue = driver.findElement(checkObject).getText();
		if(!property.equalsIgnoreCase(actualValue))
			throw new Exception(actualValue+" Displayed value is not mathcing with the expected Data"+property);
	}
	
public void checkForoptionsAvailable(String property) throws Exception {
		
		if(! property.contains(driver.findElement(inserimentoDati).getText()) && 
				property.contains(driver.findElement(preventivo).getText()) && 
				property.contains(driver.findElement(esito).getText()))
			
			throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
		
		
	}
		
	
public static final String LAVARO_COSTO_TEXT = "Il costo del lavoro è di: 165,73€+ iva*";
public static final String SOMMA_SEGUENTI_TEXT = "Costituito dalla somma delle seguenti voci:";
public static final String ITEM_ONE_TEXT = "Onere Commerciale:23,00€";
public static final String ITEM_TWO_TEXT = "Onere Amministrativo:25,51€";
public static final String ITEM_THREE_TEXT = "Quota Potenza:117,22€";
public static final String TEMPIDIESECUZIONE_HEADING = "Tempi di esecuzione";
public static final String TEMPIDIESECUZIONE_PARAGRAPH1= "I tempi di completamento dell'operazione sono indicativamente tra i 5 e i 30 giorni, calcolati sulla base delle condizioni della tua richiesta.";
public static final String TEMPIDIESECUZIONE_PARAGRAPH2= "* L'iva attualmente in vigore è pari al 10%.";
public static final String TEMPIDIESECUZIONE_PARAGRAPH3= "L’aumento della potenza comporterà un aumento delle accise in fattura quantificabile tra i € 20 e i € 40 anno stimati sui consumi medi di una famiglia tipo (cliente residenziale con 3 kW di potenza impegnata e 2.700 kWh di consumo annuo). La quota potenza trasporto che troverai in bolletta dopo la variazione: 95,81€ annui (attuale quota potenza trasporto: 63,87€ annui)";
public static final String RADIO_ADDEBITOINBOLLETTA = "Addebito in bolletta";
public static final String RADIO_VOGLIOPAGAREONLINE = "Voglio pagare online";
public static final String CHECKBOX_DICHIARO = "Dichiaro di aver preso visione dell' informativa*";
public static final String ATTIZONE_HEADING = "Attendi qualche secondo...";
public static final String ATTIZONE_TITLE = "Stiamo verificando la necessità di un sopralluogo tecnico";
//public static final String SUBTITLE_MPT = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA NIZZA 152, 00198, ROMA, ROMA, RM";
public static final String SUBTITLE_MPT = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA ANNIBALE NINCHI 3, 00128, ROMA, ROMA, RM";
public static final String Attendiqualchesecondo = "Stiamo confermando il preventivo";
public static final String SUBTITLE_MPT_New = "Stai effettuando la modifica della potenza e/o tensione della tua fornitura Luce in VIA MARIO MUSCO 19, 00147, ROMA, ROMA, RM";
}
