package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SceltaMetodoPagamentoComponentEVO extends BaseComponent implements SFDCBox{
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	//	final public By nuovoMetodoPagamento = new By.ByXPath("//input[@id='billingCheckId_' "
	//			+ "and @type='radio']/parent::label"); 
	final public By nuovoMetodoPagamento = new By.ByXPath("//td[text()='Nuovo']/parent::tr//input[@type='radio']/parent::label"); 
	final public By nuovoMetodoPagamento2= new By.ByXPath("//button[text()='Nuovo']"); 

	//	final public By bollettinoPostale = new By.ByXPath("//input[@id='billingCheckId_a0k9E000002tH0kQAE' "
	//			+ "and @type='radio']/parent::label"); 
	final public By bollettinoPostale =new By.ByXPath("//label[@class='slds-radio__label']/span");

	final public By bollettinoPostale2 = new By.ByXPath("//input[contains(@id,'Bollettino Postale') and @type='radio']");

	final public By bollettinoPostale3 = new By.ByXPath("//span[text()='Bollettino Postale']/ancestor::tr//input[@type='radio']");

//	final public By bollettinoPostale4 = new By.ByXPath("//*[text()='Bollettino Postale']/ancestor::*/preceding-sibling::td//label[@class='slds-radio__label']");
	final public By bollettinoPostale4 = new By.ByXPath("//*[text()='Bollettino Postale']/ancestor::*/preceding-sibling::td//label[@class='slds-radio__label']/span[@class='slds-radio_faux']");

	final public By bollettinoPostale5 = new By.ByXPath("//span[text()='Bollettino Postale']/ancestor::tr//label[@id='radio']");

	final public By bollettinoPostale6 = new By.ByXPath("//div[text()='Bollettino Postale']/ancestor::tr//input[@type='radio']");
	final public By bollettinoPostale7 = new By.ByXPath("//*[text()='Metodo di pagamento']/ancestor::div[@role='region']//*[text()='Bollettino Postale']/ancestor::tr//input/..");
	final public By chiudiModalMetodoPagamento=new By.ByXPath("//div[@class='slds-theme--info slds-notify--toast slds-notify slds-notify--toast']/button/lightning-icon/lightning-primitive-icon");
	final public By chiudiModalMetodoPagamento2=new By.ByXPath("//button[@class='slds-button_reset slds-notify__close']//lightning-icon/lightning-primitive-icon");

	//	final public By rid = new By.ByXPath("//input[@id='billingCheckId_a0k9E000003iTghQAE'"  
	//			+ "and @type='radio']/parent::label");  
	//	final public By rid = new By.ByXPath("//td[text()='SDD']/parent::tr//input[@type='radio']/parent::label");
	final public By rid = new By.ByXPath("//div[text()='SDD']/ancestor::tr//input[@type='radio']/parent::span");
	final public By rid2= new By.ByXPath("//span[text()='RID']/ancestor::tr//input[@type='radio']/parent::label[1]");
    final public By rid3 = By.xpath("//div[text()='SDD']/ancestor::tr[1]//input[@type='radio']/parent::div[@data-label]//span[1]");
	final public By depositoCauzionaleBtn = new By.ByXPath("//div[@class='slds-modal__content slds-p-around_medium']/div/following-sibling::div/div/div/button");
	final public By depositoCauzionaleBtn2 = new By.ByXPath("//div[@id='modal-content-id-1']//button[text()='OK'] | //section[@aria-describedby='modal-content-id-1']//button[text()='OK']");
	//	final public By depositoCauzionaleBtn3 = new By.ByXPath("//h2[text()='Deposito Cauzionale']/ancestor::div[1]//button[text()='OK']");
//	final public By depositoCauzionaleBtn3 = new By.ByXPath("//h2[text()='Deposito Cauzionale']/ancestor::div[@class='slds-modal__container']//button[text()='Ok']");
	final public By depositoCauzionaleBtn3 = new By.ByXPath("//*[text()='Deposito Cauzionale']/ancestor::div[@class='slds-modal__container']//button[text()='Ok']");
	final public By depositoCauzionaleBtn4 = new By.ByXPath("//h2[text()='Deposito Cauzionale']/ancestor::div[@class='slds-modal__container']//button[text()='OK']");
	final public By depositoCauzionaleBtn5 = new By.ByXPath("//*[text()='Deposito Cauzionale']/..//button[text()='Ok']");

	
	//	final public By copiaDaIntestatario = new By.ById("j_id0:formId:j_id516");
	final public By copiaDaIntestatario = new By.ByXPath("//a[text()='Copia da Intestatario']");
	final public By copiaDaIntestatario2 = new By.ByXPath("//button[text()='Copia da Intestatario Contratto']");
	final public By ibanField = new By.ById("j_id0:formId:newBillingMethodIbanId");
	//final public By ibanField2 = new By.ByXPath("//label[text()='IBAN']/parent::div//input");

	final public By ibanField3 = new By.ByXPath("//label[contains(text(),'IBAN')]/ancestor::div[1]//input");
	final public By ibanField2 = new By.ByXPath("//label[contains(text(),'IBAN')]/ancestor::lightning-input[1]//input");
	final public By ibanField5 = new By.ByXPath("//label[contains(text(),'IBAN')]/..//input");
	

//	final public By nomeIntestatarioCC = new By.ByXPath("//label[contains(text(),'Nome Intestatario del')]/ancestor::div[1]//input");
	final public By nomeIntestatarioCC = new By.ByXPath("//label[contains(text(),'Nome Intestatario del')]/ancestor::lightning-input[1]//input");
//	final public By ragioneSocialeCC = new By.ByXPath("//label[contains(text(),'Ragione Sociale Intestatario')]/ancestor::div[1]//input");
	final public By ragioneSocialeCC = new By.ByXPath("//label[contains(text(),'Ragione Sociale Intestatario')]/ancestor::lightning-input[1]//input");
//	final public By codiceFiscaleCC = new By.ByXPath("//label[contains(text(),'Codice Fiscale del sottoscrittore')]/ancestor::div[1]//input");
	final public By codiceFiscaleCC = new By.ByXPath("//label[contains(text(),'Codice Fiscale del sottoscrittore')]/ancestor::lightning-input[1]//input");
	final public By cercaIban = new By.ByXPath("//label[text()='Cerca']/ancestor::div[1]//input");
	final public By cercaMdp = new By.ByXPath("//input[@id='billingAccountTable-search-input']");
	final public By ibanEsteroCheckBox=By.xpath("//label[contains(@for,'ITA_IFM_Flag_Foreign_IBAN__c')]/span[@class='slds-checkbox_faux']"); //FR R3 05.08.2021
	//final public By campoRicerca = new By.ByXPath("//input[contains(@id,'Ricerca')]"); ////[FR]2021.05.18 - rimuovere
	final public By campoRicerca = new By.ByXPath("//button[text()='Nuovo']//ancestor::div[@class='slds-grid slds-wrap']//input[contains(@id,'Ricerca')]"); //[FR]2021.05.18
	final public By cercaMdpVolutra = new By.ByXPath("//label[contains(text(), 'Cerca')]/following-sibling::div/input[@name=contains(text(), 'search')]");
	//	final public By checkPayMetod = new By.ById("j_id0:formId:paymentNext");
	final public By checkPayMetod = new By.ByXPath("//input[contains(@id,'j_id0:')and contains(@id,'paymentNext')]");
	final public By saveMetod = new By.ById("buttonSavePaymentMethod");
	final public By saveMetod2 = new By.ByXPath("//button[text()='Salva']");
	final public By saveMetod3 = new By.ByXPath("//lightning-button[@data-id='saveButton']//button[text()='Salva']");
	final public By avantiPaymentMethod = new By.ByXPath("//div[contains(@id,'paymentMethod')]//input[@value='Avanti']");
	final public By buttonAvanti = new By.ById("buttonNext");
	final public By buttonAvantiVolturaAccollo = new By.ByXPath("//input[@id='j_id0:principalForm:paymentNext']");
	final public By buttonConfermaMetodoPagamento = new By.ByXPath("//button[text()='Conferma Metodo di Pagamento']");
	final public By buttonConfermaMetodoPagamento2 = new By.ByXPath("//button[text()='Conferma Metodo Pagamento']");
	final public By buttonConferma2 = By.xpath("//div[@aria-label='Metodo di pagamento']//button[text()='Conferma'] | //span[text()='Metodo di pagamento']/ancestor::div[@role='region']//button[@name='Conferma']");
	final public By linkNuovoMetodo = new By.ById("newMethod");
	final public By msgRidEsistente=new By.ByXPath("//h2[contains(text(),'RID') and contains(text(),'presente a sistema')]/ancestor::div[@role='alert']");
	final public By buttonChiudiMsgRidEsistente=new By.ByXPath("//h2[contains(text(),'RID') and contains(text(),'presente a sistema')]/ancestor::div[@role='alert']/button/img[@alt='Chiudi']");
    final public By buttonTextAvanti = By.xpath("//button[text()='Avanti']");
    final public By bonifico = new By.ByXPath("//*[text()='Bonifico']/ancestor::*/preceding-sibling::td//label[@class='slds-radio__label']");
    final public By bonifico2 = new By.ByXPath("//*[text()='Bonifico']/ancestor::*/preceding-sibling::td//label[@class='slds-radio__label']/span[@class='slds-radio_faux']");
	public By okDeposito = By.xpath("//*[@id=\"modalButtons-4624\"]/lightning-button/button");
    
    
	public SceltaMetodoPagamentoComponentEVO(WebDriver driver){
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}
	
	public void press(String frame,By button) throws Exception{
		util.frameManager(frame, button, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}
	public void press(By button) throws Exception{
		util.objectManager( button, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void selezionaBollettinoPagamento (String frame,By bollettinoBy,By depositoCauzionaleBtnBy, By checkPayMetodBy) throws Exception{
		selezionaBollettinoPagamento(frame, bollettinoBy, depositoCauzionaleBtnBy, checkPayMetodBy,true);
	}


	public void inserisciNuovoMetodo(String frame) throws Exception{
		//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		this.driver.switchTo().frame(frameToSw);

		TimeUnit.SECONDS.sleep(5);
		By ok = By.xpath("//*[text()='Attenzione']/../../..//*[text()='OK']");

		if(util.exists(ok, 25) && driver.findElement(ok).isDisplayed()) {
			util.objectManager(ok, util.scrollAndClick);
		}


		if(util.exists(linkNuovoMetodo, 25)&& driver.findElement(linkNuovoMetodo).isDisplayed()) {
			WebElement el = driver.findElement(linkNuovoMetodo);
			if(el.getAttribute("aria-disabled").equals("false"))
				util.scrollToElement(el);
			el.click();


		}
		TimeUnit.SECONDS.sleep(5);


		if(util.exists(ok, 25) && driver.findElement(ok).isDisplayed()) {
			util.objectManager(ok, util.scrollAndClick);
		}

		driver.switchTo().defaultContent();
	}
	
	public void selezionaBollettino(String frame, By bollettinoBy, By depositoCauzionaleBtnBy, By checkPayMetodBy,
			Boolean depositosi) throws Exception {
		// driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver
				.findElement(By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']"));
		this.driver.switchTo().frame(frameToSw);
	    util.objectManager(bollettinoBy, util.scrollAndClick);
		spinner.checkSpinners();

		if (depositosi) {
			util.objectManager(depositoCauzionaleBtnBy, util.scrollAndClick);
			spinner.checkSpinners();
		}

		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.checkSpinners();
		driver.switchTo().defaultContent();

	}


	public void selezionaBollettinoPagamento (String frame,By bollettinoBy,By depositoCauzionaleBtnBy, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
		//		driver.switchTo().frame(frame);
		if(frame!= null) {
			util.getFrameActive();
		}else {
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			this.driver.switchTo().frame(frameToSw);
		}
		WebElement checkbox = util.waitAndGetElement(bollettinoBy);
		String checkboxattribute = checkbox.getAttribute("value");
		WebElement labelcheckbox=checkbox.findElement(By.xpath(".."));
		driver.switchTo().defaultContent();
		//		System.out.println("attributo "+checkboxattribute);
		if(checkboxattribute.equals("false")) {
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			this.driver.switchTo().frame(frameToSw);
			labelcheckbox.click();
			driver.switchTo().defaultContent();
			spinner.waitForSpinnerByStyleDiplayNone(frame);
			if(depositosi){
				util.frameManager(frame, depositoCauzionaleBtnBy, util.scrollAndClick);
				spinner.waitForSpinnerByStyleDiplayNone(frame);
			}
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bollettino gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.frameManager(frame, checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	} 

	public void selezionaBollettinoPagamento2 (String frame,By bollettinoBy,By depositoCauzionaleBtnBy, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
		//		driver.switchTo().frame(frame);
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		WebElement checkbox = util.waitAndGetElement(bollettinoBy);
		WebElement labelcheckbox=checkbox.findElement(By.xpath("ancestor::tr"));
		String checkboxattribute = labelcheckbox.getAttribute("class");
		driver.switchTo().defaultContent();
		//System.out.println("attributo "+checkboxattribute);
		if(!checkboxattribute.contains("slds-is-selected")) {
			frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			this.driver.switchTo().frame(frameToSw);
			checkbox.click();
			driver.switchTo().defaultContent();
			spinner.checkSpinners(frame);
			if(depositosi){
				util.frameManager(frame, depositoCauzionaleBtnBy, util.scrollAndClick);
				spinner.checkSpinners(frame);
			}
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bollettino gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.frameManager(frame, checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.checkSpinners(frame);

	} 

	//Questo metodo seleziona il metodo di pagamento. Se già selezionato non viene ri-selezionato
	//bollettinoBy-->input del checkbox
	//attributoDaRecuperare-->Attributo da recuperare-usato per discriminare se il check box è preselezionato o meno
	//valoreAttesoAttributo-->Valore dell'attributo atteso
	public void selezionaBollettinoPagamento (String frame,By bollettinoBy,String attributoDaRecuperare,String valoreAttesoAttributo,By depositoCauzionaleBtnBy, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
		driver.switchTo().defaultContent();
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(cercaMdp, util.scrollAndSendKeys,"Bollettino");
		TimeUnit.SECONDS.sleep(3);
		WebElement checkbox = util.waitAndGetElement(bollettinoBy);
		//System.out.println("attributo da recuperare:"+attributoDaRecuperare);
		String checkboxattribute=checkbox.getAttribute(attributoDaRecuperare);     
		WebElement labelcheckbox=checkbox.findElement(By.xpath(".."));
		driver.switchTo().defaultContent();
		//System.out.println("attributo "+checkboxattribute);
		if(checkboxattribute==null) {
			frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			this.driver.switchTo().frame(frameToSw);
			labelcheckbox.click();
			driver.switchTo().defaultContent();
			spinner.waitForSpinnerByStyleDiplayNone(frame);
			if(depositosi){
				util.frameManager(frame, depositoCauzionaleBtnBy, util.scrollAndClick);
				spinner.waitForSpinnerByStyleDiplayNone(frame);
			}
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bollettino gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.frameManager(frame, checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	/*
	 * Override del metodo selezionaBollettinoPagamento
	 * Aggiunta un'istanza By alla firma del metodo poichè, in caso di Voltura Con Accollo, 
	 * il campo INPUT per la ricerca del metodo di pagamento, è individabile attraverso un XPath 
	 * diverso da quello specificato nel (primo)metodo selezionaBollettinoPagamento 
	 */
	public void selezionaBollettinoPagamento (String frame,By cercaInputBy, By bollettinoBy,String attributoDaRecuperare,String valoreAttesoAttributo,By depositoCauzionaleBtnBy, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
		driver.switchTo().defaultContent();
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(cercaInputBy, util.scrollAndSendKeys,"Bollettino");
		TimeUnit.SECONDS.sleep(3);
		WebElement checkbox = util.waitAndGetElement(bollettinoBy);
		//System.out.println("attributo da recuperare:"+attributoDaRecuperare);
		String checkboxattribute=checkbox.getAttribute(attributoDaRecuperare);     
		WebElement labelcheckbox=checkbox.findElement(By.xpath(".."));
		driver.switchTo().defaultContent();
		//System.out.println("attributo "+checkboxattribute);
		if(checkboxattribute==null) {
			frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			this.driver.switchTo().frame(frameToSw);
			labelcheckbox.click();
			driver.switchTo().defaultContent();
			spinner.waitForSpinnerByStyleDiplayNone(frame);
			if(depositosi){
				util.frameManager(frame, depositoCauzionaleBtnBy, util.scrollAndClick);
				spinner.waitForSpinnerByStyleDiplayNone(frame);
			}
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bollettino gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.frameManager(frame, checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void selezionaBollettinoPagamento (By bollettinoBy,String attributoDaRecuperare,String valoreAttesoAttributo,By depositoCauzionaleBtnBy, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
				
		WebElement checkbox = util.waitAndGetElement(bollettinoBy);
		//System.out.println("attributo da recuperare:"+attributoDaRecuperare);
		String checkboxattribute=checkbox.getAttribute(attributoDaRecuperare);     
		WebElement labelcheckbox=checkbox.findElement(By.xpath(".."));
		//System.out.println("attributo "+checkboxattribute);
		if(checkboxattribute==null) {
			util.scrollToElement(labelcheckbox);
			labelcheckbox.click();
			spinner.checkSpinners();
			Thread.currentThread().sleep(7000);
			//spinner.checkSpinners();
			if(depositosi){
				util.objectManager(depositoCauzionaleBtnBy, util.scrollAndClick);
				spinner.checkSpinners();
			}
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bollettino gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.checkSpinners();

	}
	
	public void selezionaBollettinoPagamentoConRicerca (By bollettinoBy,By cerca,String attributoDaRecuperare,String valoreAttesoAttributo,By depositoCauzionaleBtnBy, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
		
		util.objectManager(cerca, util.scrollAndSendKeys,"Bollettino");
		TimeUnit.SECONDS.sleep(3);
		
		WebElement checkbox = util.waitAndGetElement(bollettinoBy);
		//System.out.println("attributo da recuperare:"+attributoDaRecuperare);
		String checkboxattribute=checkbox.getAttribute(attributoDaRecuperare);     
		WebElement labelcheckbox=checkbox.findElement(By.xpath(".."));
		//System.out.println("attributo "+checkboxattribute);
		if(checkboxattribute==null) {
			util.scrollToElement(labelcheckbox);
			labelcheckbox.click();
			spinner.checkSpinners();
			Thread.currentThread().sleep(7000);
			//spinner.checkSpinners();
			if(depositosi){
				util.objectManager(depositoCauzionaleBtnBy, util.scrollAndClick);
				spinner.checkSpinners();
			}
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bollettino gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.checkSpinners();

	} 
	
	
	public void selezionaBollettinoPagamento (By bollettinoBy, By depositoCauzionale) throws Exception
	{
		util.objectManager(bollettinoBy, util.scrollAndClick);
		
			spinner.checkSpinners();
			Thread.currentThread().sleep(7000);
			//spinner.checkSpinners();
			
			util.objectManager(depositoCauzionale, util.scrollAndClick);
			
	
		TimeUnit.SECONDS.sleep(3);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");

	}
	
	public void selezionaBonifico (By bonificoBy,String attributoDaRecuperare,String valoreAttesoAttributo, By checkPayMetodBy, Boolean depositosi) throws Exception
	{
		WebElement checkbox = util.waitAndGetElement(bonificoBy);
		String checkboxattribute=checkbox.getAttribute(attributoDaRecuperare);     
		WebElement labelcheckbox=checkbox.findElement(By.xpath(".."));
		if(checkboxattribute==null) {
			util.scrollToElement(labelcheckbox);
			labelcheckbox.click();
			spinner.checkSpinners();
		}
		else //System.out.println("Bollettino gia presente, non lo riseleziono");
			Logger.getLogger("").log(Level.INFO, "Bonifico gia presente, non lo riseleziono");
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		Logger.getLogger("").log(Level.INFO, "Avanti bollettino postale");
		spinner.checkSpinners();

	} 


	public void pressButton(String frame,By button) throws Exception{
		util.frameManager(frame, button, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}

	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}


	public void selezionaNuovoMetodoPagamento (String frame,String iban,By ridBy,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy) throws Exception
	{
		//cerca IBAN
		util.frameManager(frame, cercaIban, util.scrollAndSendKeys,iban);
		TimeUnit.SECONDS.sleep(3);

		if (util.verifyExistenceInFrame(frame, ridBy, 5))
			util.frameManager(frame, ridBy, util.scrollAndClick);
		else {
			util.frameManager(frame, nuovoMetodoPagamentoBy, util.scrollAndClick);
			spinner.checkSpinners(frame);
			util.frameManager(frame, copiaDaIntestatarioBy, util.scrollAndClick);
			spinner.checkSpinners(frame);
			util.frameManager(frame, ibanFieldBy, util.sendKeys,iban);
			util.frameManager(frame, saveMetodBy, util.scrollAndClick);
		}
		spinner.checkSpinners(frame);
		util.frameManager(frame, checkPayMetodBy, util.scrollAndClick);

	}

	public void selezionaNuovoMetodoPagamentoRevoca(String frame,By metodo) throws Exception{
		util.frameManager(frame, metodo, util.scrollAndClick);
		spinner.checkSpinners(frame);
		WebElement fr = driver.findElement(By.xpath("//iframe[@name='"+frame+"']"));
		driver.switchTo().frame(fr);
		TimeUnit.SECONDS.sleep(5);
		By ok = By.xpath("//*[contains(text(),'Deposito')]/../../..//*[text()='OK']");

		if(util.exists(ok, 25) && driver.findElement(ok).isDisplayed()) {
			util.objectManager(ok, util.scrollAndClick);
		}

		driver.switchTo().defaultContent();


	}



	public void inserisciCodiceIban(String frame,By ibanBy, String iban) throws Exception{
		util.frameManager(frame, ibanBy, util.sendKeysAndEnter, iban);
		util.frameManager(frame, By.tagName("body"), util.scrollAndClick);
		spinner.checkSpinners(frame);
	}


	public boolean controllaEsistenzaRID(String frame,By msgRidEsistente, By buttonChiudiMsgRidEsistente) throws Exception{
		boolean flag=false;
		if (util.exists(frame, msgRidEsistente, 20)) {
			//			driver.switchTo().frame(frame);
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			this.driver.switchTo().frame(frameToSw);
			if(driver.findElement(msgRidEsistente).isDisplayed()){
				util.objectManager(buttonChiudiMsgRidEsistente, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(3);
				flag=true;
			}
			driver.switchTo().defaultContent();
		}
		return flag;
	}


	public void confermaMetodoPagamento(String frame, By buttonConferma) throws Exception{
		util.frameManager(frame, buttonConferma, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}
	
	
	public void selezionaSDD() throws Exception{
		util.objectManager(rid3, util.scrollAndClick);
		util.objectManager(buttonConferma2, util.scrollAndClick);
		spinner.checkSpinners();
	}

	/**
	 * FR 06.08.2021
	 * @param iban
	 * @param ridBy
	 * @param nuovoMetodoPagamentoBy
	 * @param copiaDaIntestatarioBy
	 * @param ibanFieldBy
	 * @param saveMetodBy
	 * @param checkPayMetodBy
	 * @param nomeIntestatarioCC
	 * @param intestatario
	 * @param ragioneSocialeCC
	 * @param ragioneSociale
	 * @param codiceFiscaleCC
	 * @param cf
	 * @throws Exception
	 */
	public void SelezionaNuovoMetodoPagamentoIbanEstero(String iban,By ridBy,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy,
			By nomeIntestatarioCC,
			String intestatario,
			By ragioneSocialeCC,
			String ragioneSociale,
			By codiceFiscaleCC,
			String cf, By checkBoxIbanEstero,boolean copiaDaIntestario ) throws Exception
	{
		//cerca IBAN
		/*
				util.objectManager(cercaIban, util.scrollAndSendKeys,iban);
				TimeUnit.SECONDS.sleep(3);

				if (util.exists(ridBy, 20))
					util.objectManager(ridBy, util.scrollAndClick);
				else {*/
					util.objectManager(nuovoMetodoPagamentoBy, util.scrollAndClick);
					spinner.checkSpinners();
					//Inserimenti dati IBAN
					if (driver.findElement(copiaDaIntestatarioBy).isEnabled() && copiaDaIntestario){
						util.objectManager(copiaDaIntestatarioBy, util.scrollAndClick);
						spinner.checkSpinners();
					}
					else{
						//Inserimento Nome Intestatario
						util.objectManager(nomeIntestatarioCC, util.sendKeys,intestatario);
						//Inserimento Cognome Ragione Sociale
						util.objectManager(ragioneSocialeCC, util.sendKeys,ragioneSociale);
						//Inserimento Codice fiscale sottoscrittore
						util.objectManager(codiceFiscaleCC, util.sendKeys,cf);
					}
					
					//spunto checkbox IBAN estero
					util.objectManager(checkBoxIbanEstero, util.scrollAndClick);
					util.objectManager(ibanFieldBy, util.scrollAndSendKeys,iban);
					util.objectManager(saveMetodBy, util.scrollAndClick);
					spinner.checkSpinners();
				//}
				util.objectManager(checkPayMetodBy, util.scrollAndClick);
				spinner.checkSpinners();
	}
	
	public void selezionaNuovoMetodoPagamento (String iban,By ridBy,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy,
			By nomeIntestatarioCC,
			String intestatario,
			By ragioneSocialeCC,
			String ragioneSociale,
			By codiceFiscaleCC,
			String cf ) throws Exception
	{
		//cerca IBAN
		util.objectManager(cercaIban, util.scrollAndSendKeys,iban);
		TimeUnit.SECONDS.sleep(3);

		if (util.exists(ridBy, 20))
			util.objectManager(ridBy, util.scrollAndClick);
		else {
			util.objectManager(nuovoMetodoPagamentoBy, util.scrollAndClick);
			spinner.checkSpinners();
			//Inserimenti dati IBAN
			if (driver.findElement(copiaDaIntestatarioBy).isEnabled()){
				util.objectManager(copiaDaIntestatarioBy, util.scrollAndClick);
				spinner.checkSpinners();
			}
			else{
				//Inserimento Nome Intestatario
				util.objectManager(nomeIntestatarioCC, util.sendKeys,intestatario);
				//Inserimento Cognome Ragione Sociale
				util.objectManager(ragioneSocialeCC, util.sendKeys,ragioneSociale);
				//Inserimento Codice fiscale sottoscrittore
				util.objectManager(codiceFiscaleCC, util.sendKeys,cf);
			}
			util.objectManager(ibanFieldBy, util.scrollAndSendKeys,iban);
			util.objectManager(saveMetodBy, util.scrollAndClick);
			spinner.checkSpinners();
		}
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		spinner.checkSpinners();

	}

	public void selezionaNuovoMetodoPagamentoNoSearch (String iban,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy,
			By nomeIntestatarioCC,
			String intestatario,
			By ragioneSocialeCC,
			String ragioneSociale,
			By codiceFiscaleCC,
			String cf ) throws Exception
	{
		//ricerca dell'IBAN in tabella
	
		boolean trovato=false;
		boolean haveNextPage=true;
		int pagine=1;
//		int numeroPagine=Integer.parseInt(driver.findElement(By.xpath("//table[@id='Metodo di pagamento_Table']/ancestor::html[1]//span[@class='tokenActionColorText uiOutputNumber']")).getText());
		
		TimeUnit.SECONDS.sleep(5);
		
		String testoPagina=driver.findElement(By.xpath("//lightning-layout-item[@class='slds-p-around_small slds-col slds-size_3-of-12 slds-col_bump-right']")).getText();
		testoPagina=testoPagina.substring(3, testoPagina.length());
//		System.out.println("Numero pagine:"+testoPagina);
		int numeroPagine=Integer.parseInt(testoPagina);

//		By avanti=new By.ByXPath("//span[text()='Avanti']");
		By avanti=new By.ByXPath("//div[@aria-label='Avanti']/p");
		while(trovato==false && pagine<=numeroPagine){
//			int nunerorighe=driver.findElements(By.xpath("//table[@id='Metodo di pagamento_Table']//tbody/tr")).size();
			int nunerorighe=driver.findElements(By.xpath("//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr")).size();
//			System.out.println("Numero righe"+nunerorighe);
//			System.out.println("Pagina n°"+pagine);
			for(int j=1;j<=nunerorighe;j++){
//				String ibanCorrente=driver.findElement(By.xpath("//table[@id='Metodo di pagamento_Table']//tbody/tr["+j+"]//td[6]//span[@id='simpleTextId']")).getText();
				String ibanCorrente=driver.findElement(By.xpath("//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr["+j+"]//td[6]")).getText();
//				System.out.println("Iban"+j+" "+ibanCorrente);
				if (ibanCorrente.compareTo(iban)==0){
//					util.objectManager(new By.ByXPath("//table[@id='Metodo di pagamento_Table']//tbody/tr["+j+"]//input[@type='radio']/parent::span/label"), util.click);
					util.objectManager(new By.ByXPath("//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr["+j+"]//div[@class='slds-radio']/label/span[1]"), util.click);
//					driver.findElement(By.xpath("//table[@id='Metodo di pagamento_Table']//tbody/tr["+j+"]//input[@type='radio']/parent::span/label")).click();
					trovato=true;
					break;
				}
			}
			//Passo alla pagina successiva
			if(trovato==false){
				util.objectManager(avanti, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(5);
				pagine++;

			}
		}

		if (trovato==false){
			util.objectManager(nuovoMetodoPagamentoBy, util.scrollAndClick);
			spinner.checkSpinners();
			//Inserimenti dati IBAN
			if (driver.findElement(copiaDaIntestatarioBy).isEnabled()){
				util.objectManager(copiaDaIntestatarioBy, util.scrollAndClick);
				spinner.checkSpinners();
			}
			else{
				//Inserimento Nome Intestatario
				util.objectManager(nomeIntestatarioCC, util.sendKeys,intestatario);
				//Inserimento Cognome Ragione Sociale
				util.objectManager(ragioneSocialeCC, util.sendKeys,ragioneSociale);
				//Inserimento Codice fiscale sottoscrittore
				util.objectManager(codiceFiscaleCC, util.sendKeys,cf);
			}
			util.objectManager(ibanFieldBy, util.scrollAndSendKeys,iban);
			util.objectManager(saveMetodBy, util.scrollAndClick);
			spinner.checkSpinners();
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		spinner.checkSpinners();

	}

	public void selezionaNuovoMetodoPagamentoSub (String iban,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy,
			By nomeIntestatarioCC,
			String intestatario,
			By ragioneSocialeCC,
			String ragioneSociale,
			By codiceFiscaleCC,
			String cf ) throws Exception
	{
		TimeUnit.SECONDS.sleep(2);
		
		util.objectManager(nuovoMetodoPagamentoBy, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(8);
		//Inserimenti dati IBAN
		if (driver.findElement(copiaDaIntestatarioBy).isEnabled()){
			util.objectManager(copiaDaIntestatarioBy, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(5);
		}
		else{
			//Inserimento Nome Intestatario
			util.objectManager(nomeIntestatarioCC, util.sendKeys,intestatario);
			//Inserimento Cognome Ragione Sociale
			util.objectManager(ragioneSocialeCC, util.sendKeys,ragioneSociale);
			//Inserimento Codice fiscale sottoscrittore
			util.objectManager(codiceFiscaleCC, util.sendKeys,cf);
		}
		util.objectManager(ibanFieldBy, util.scrollAndSendKeys,iban);
		util.objectManager(saveMetodBy, util.scrollAndClick);
		spinner.checkSpinners();
		
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		spinner.checkSpinners();

	}
	
	public void selezionaIBANSearch (String iban,
			By cerca
			 )throws Exception
	{
		//ricerca dell'IBAN in tabella
		
		util.objectManager(cerca, util.scrollAndSendKeys,iban);
		TimeUnit.SECONDS.sleep(3); 
	}
	/**
	 * 
	 * @param iban
	 * @param cerca
	 * @param nuovoMetodoPagamentoBy
	 * @param copiaDaIntestatarioBy
	 * @param ibanFieldBy
	 * @param saveMetodBy
	 * @param checkPayMetodBy
	 * @param nomeIntestatarioCC
	 * @param intestatario
	 * @param ragioneSocialeCC
	 * @param ragioneSociale
	 * @param codiceFiscaleCC
	 * @param cf
	 * @throws Exception
	 */
	public void selezionaNuovoMetodoPagamentoWithSearchAndIbanEstero(String iban,
			By cerca,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy,
			By nomeIntestatarioCC,
			String intestatario,
			By ragioneSocialeCC,
			String ragioneSociale,
			By codiceFiscaleCC,
			String cf , By checkBoxIbanEstero) throws Exception
	{
		//ricerca dell'IBAN in tabella
		
		util.objectManager(cerca, util.scrollAndSendKeys,iban);
		TimeUnit.SECONDS.sleep(3);
	
		boolean trovato=false;
		boolean haveNextPage=true;
		int pagine=1;

		
		TimeUnit.SECONDS.sleep(5);
		
		String testoPagina=driver.findElement(By.xpath("//lightning-layout-item[@class='slds-p-around_small slds-col slds-size_3-of-12 slds-col_bump-right']")).getText();
		testoPagina=testoPagina.substring(3, testoPagina.length());
//		System.out.println("Numero pagine:"+testoPagina);
		int numeroPagine=Integer.parseInt(testoPagina);

//		By avanti=new By.ByXPath("//span[text()='Avanti']");
		By avanti=new By.ByXPath("//div[@aria-label='Avanti']/p");
		while(trovato==false && pagine<=numeroPagine){

			int nunerorighe=driver.findElements(By.xpath("//h2/span[text()='Metodo di pagamento']/ancestor::div[@role='region']//table[@role='table']/tbody/tr")).size();
			
			for(int j=1;j<=nunerorighe;j++){

				String ibanCorrente=driver.findElement(By.xpath("//h2/span[text()='Metodo di pagamento']/ancestor::div[@role='region']//table[@role='table']/tbody/tr["+j+"]//td[6]")).getText();
				if (ibanCorrente.compareTo(iban)==0){					
					util.objectManager(new By.ByXPath("//div[@id='tableMDPMetodo di pagamento']//ancestor::div[@role='region']//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr["+j+"]//div[@class='slds-radio']/label/span[1]"), util.click);
					trovato=true;
					break;
				}
			}
			//Passo alla pagina successiva
			if(trovato==false){
				util.objectManager(avanti, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(5);
				pagine++;

			}
		}

		if (trovato==false){
			util.objectManager(nuovoMetodoPagamentoBy, util.scrollAndClick);
			spinner.checkSpinners();
			//Inserimenti dati IBAN
			if (driver.findElement(copiaDaIntestatarioBy).isEnabled()){
				util.objectManager(copiaDaIntestatarioBy, util.scrollAndClick);
				spinner.checkSpinners();
			}
			else{
				//Inserimento Nome Intestatario
				util.objectManager(nomeIntestatarioCC, util.sendKeys,intestatario);
				//Inserimento Cognome Ragione Sociale
				util.objectManager(ragioneSocialeCC, util.sendKeys,ragioneSociale);
				//Inserimento Codice fiscale sottoscrittore
				util.objectManager(codiceFiscaleCC, util.sendKeys,cf);
			}
			util.objectManager(ibanFieldBy, util.scrollAndSendKeys,iban);
			//spunto checkbox IBAN estero
			util.objectManager(checkBoxIbanEstero, util.scrollAndClick);
			util.objectManager(saveMetodBy, util.scrollAndClick);
			spinner.checkSpinners();
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		spinner.checkSpinners();

	}
	
	public void selezionaNuovoMetodoPagamentoWithSearch (String iban,
			By cerca,
			By nuovoMetodoPagamentoBy,
			By copiaDaIntestatarioBy,
			By ibanFieldBy,
			By saveMetodBy,
			By checkPayMetodBy,
			By nomeIntestatarioCC,
			String intestatario,
			By ragioneSocialeCC,
			String ragioneSociale,
			By codiceFiscaleCC,
			String cf ) throws Exception
	{
		//ricerca dell'IBAN in tabella
		
		util.objectManager(cerca, util.scrollAndSendKeys,iban);
		TimeUnit.SECONDS.sleep(3);
	
		boolean trovato=false;
		boolean haveNextPage=true;
		int pagine=1;
//		int numeroPagine=Integer.parseInt(driver.findElement(By.xpath("//table[@id='Metodo di pagamento_Table']/ancestor::html[1]//span[@class='tokenActionColorText uiOutputNumber']")).getText());
		
		TimeUnit.SECONDS.sleep(5);
		
		String testoPagina=driver.findElement(By.xpath("//lightning-layout-item[@class='slds-p-around_small slds-col slds-size_3-of-12 slds-col_bump-right']")).getText();
		testoPagina=testoPagina.substring(3, testoPagina.length());
//		System.out.println("Numero pagine:"+testoPagina);
		int numeroPagine=Integer.parseInt(testoPagina);

//		By avanti=new By.ByXPath("//span[text()='Avanti']");
		By avanti=new By.ByXPath("//div[@aria-label='Avanti']/p");
		while(trovato==false && pagine<=numeroPagine){
//			int nunerorighe=driver.findElements(By.xpath("//table[@id='Metodo di pagamento_Table']//tbody/tr")).size();
//			int nunerorighe=driver.findElements(By.xpath("//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr")).size();
			int nunerorighe=driver.findElements(By.xpath("//h2/span[text()='Metodo di pagamento']/ancestor::div[@role='region']//table[@role='table']/tbody/tr")).size();
//			System.out.println("Numero righe"+nunerorighe);
//			System.out.println("Pagina n°"+pagine);
			for(int j=1;j<=nunerorighe;j++){
//				String ibanCorrente=driver.findElement(By.xpath("//table[@id='Metodo di pagamento_Table']//tbody/tr["+j+"]//td[6]//span[@id='simpleTextId']")).getText();
//				String ibanCorrente=driver.findElement(By.xpath("//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr["+j+"]//td[6]")).getText();
				String ibanCorrente=driver.findElement(By.xpath("//h2/span[text()='Metodo di pagamento']/ancestor::div[@role='region']//table[@role='table']/tbody/tr["+j+"]//td[6]")).getText();
//				System.out.println("Iban"+j+" "+ibanCorrente);
				if (ibanCorrente.compareTo(iban)==0){
//					util.objectManager(new By.ByXPath("//table[@id='Metodo di pagamento_Table']//tbody/tr["+j+"]//input[@type='radio']/parent::span/label"), util.click);
					util.objectManager(new By.ByXPath("//div[@id='tableMDPMetodo di pagamento']//ancestor::div[@role='region']//table[@role='table' and @class='slds-table slds-table_cell-buffer slds-table_bordered']/tbody/tr["+j+"]//div[@class='slds-radio']/label/span[1]"), util.click);
//					driver.findElement(By.xpath("//table[@id='Metodo di pagamento_Table']//tbody/tr["+j+"]//input[@type='radio']/parent::span/label")).click();
					trovato=true;
					break;
				}
			}
			//Passo alla pagina successiva
			if(trovato==false){
				util.objectManager(avanti, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(5);
				pagine++;

			}
		}

		if (trovato==false){
			util.objectManager(nuovoMetodoPagamentoBy, util.scrollAndClick);
			spinner.checkSpinners();
			//Inserimenti dati IBAN
			if (driver.findElement(copiaDaIntestatarioBy).isEnabled()){
				util.objectManager(copiaDaIntestatarioBy, util.scrollAndClick);
				spinner.checkSpinners();
			}
			else{
				//Inserimento Nome Intestatario
				util.objectManager(nomeIntestatarioCC, util.sendKeys,intestatario);
				//Inserimento Cognome Ragione Sociale
				util.objectManager(ragioneSocialeCC, util.sendKeys,ragioneSociale);
				//Inserimento Codice fiscale sottoscrittore
				util.objectManager(codiceFiscaleCC, util.sendKeys,cf);
			}
			util.objectManager(ibanFieldBy, util.scrollAndSendKeys,iban);
			util.objectManager(saveMetodBy, util.scrollAndClick);
			spinner.checkSpinners();
		}
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(checkPayMetodBy, util.scrollAndClick);
		spinner.checkSpinners();

	}


}
