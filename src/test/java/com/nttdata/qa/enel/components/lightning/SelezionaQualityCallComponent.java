package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class SelezionaQualityCallComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public SelezionaQualityCallComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  

	public final By clickConfermaOfferta = By.xpath("//button[text()='Esegui Quality Call']");
	
		
	public void selezionaQualityCall() throws Exception {

		driver.switchTo().defaultContent();

		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		WebElement element = null;
		for (WebElement webElement : els) {
			driver.switchTo().frame(webElement);
			try {
			 element = util.waitAndGetElement(clickConfermaOfferta);
			 break;
			}catch(Exception e) {
				driver.switchTo().defaultContent();
			}
		}
		
		press(clickConfermaOfferta);
		
	}
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	
}
