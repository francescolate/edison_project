package com.nttdata.qa.enel.components.colla;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.ColorUtils;

public class Privato211ACRDisattivazioneComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	
	//HomePage
	public By homePageCentralText = By.xpath("//div[@id='mainContentWrapper']//h1[text()='Benvenuto nella tua area privata']");
	public By homePageCentralSubText = By.xpath("//div[@id='mainContentWrapper']//p[text()='In questa sezione potrai gestire le tue forniture']");
	
	public By sectionTitle = By.xpath("//h2[text()='Le tue forniture']");
	public By sectionTitleSubText = By.xpath("//p[contains(text(),'Visualizza le informazioni principali delle tue forniture e consulta le tue bollette')]");
	
	public By dettaglioFornituraButton = By.xpath("//dd[text()='Attiva']/ancestor::*/preceding-sibling::*//span[@class='icon-line-electricity']/ancestor::*/following-sibling::div/a[contains(text(),'Dettaglio fornitura')]");
	
	//servizi
	
	public By serviziSelect = By.xpath("//a/descendant::span[text()='Servizi']");
	
	public By serviziPage = By.xpath("//h2[text()='Servizi per le forniture']");
	
	public By serviziSectionTitle = By.xpath("//h2[text()='Servizi per il contratto']");
	
    public By disattivazioneFornituraItem = By.xpath("//h2[text()='Servizi per il contratto']/parent::div/following-sibling::ul//h3[text()='Disattivazione Fornitura']/parent::div");
	
    //disattivazioneFornitura
	public By disattivazioneFornitura = By.xpath("//h1[text()='Disattivazione Fornitura']");
	
	public By enelLogo = By.xpath("//img[@alt='logo enel']");
	
	public By littleManIcon = By.xpath("//span[@class='dsc-icon-user']");
	
	public By loggedInUserName = By.xpath("//span[@class='dsc-icon-user']/parent::button/following-sibling::p");
	
//	"Domande frequenti" section
	
    public By domandeFrequentiSection = By.xpath("//h3[text()='Domande frequenti']");
	
	//Footer
	
	public By fotterSection = By.xpath("//footer//div[@class='footer-info-container']/ul/li");
	
	public By footerEnelEnergia = By.xpath("//footer//div[@class='footer-info-container']/ul/li[contains(text(),'© Enel Energia S.p.a')]");
	
	public By footerTuttiDiritti = By.xpath("//footer//div[@class='footer-info-container']/ul/li[contains(text(),'Tutti i Diritti Riservati')]");
	
	public By footerIVA = By.xpath("//footer//div[@class='footer-info-container']/ul/li[contains(text(),'P. IVA 06655971007')]");
	public By footerInformazioni = By.xpath("//footer//div[@class='footer-info-container']/ul/li/a[contains(text(),'Informazioni Legali')]");
	
	public By footerPrivacy = By.xpath("//footer//div[@class='footer-info-container']/ul/li/a[contains(text(),'Privacy')]");
	
	public By footerCredits = By.xpath("//footer//div[@class='footer-info-container']/ul/li/a[contains(text(),'Credits')]");
	public By footerContattaci = By.xpath("//footer//div[@class='footer-info-container']/ul/li/a[contains(text(),'Contattaci')]");
	
	public By PROSEGUI_CON_LA_DISATTIVAZIONE_button = By.xpath("//button[text()='PROSEGUI CON LA DISATTIVAZIONE']");
	
	//disattivazioneFornituraHomePage
	public By disattivazioneFornituraPageTitle = By.xpath("//div[@class='list-form-container']//h1[text()='Disattivazione Fornitura']");

	public By disattivazioneFornituraPageTitleSubText = By.xpath("//div[@class='list-form-container']//h2[text()='Seleziona una o più forniture che intendi disattivare']");

	public By dailogBox = By.xpath("//div[@class='blue-alert']/p  | //div[@class='blue-alert']/ul");  
	
	
	//PDR - 03081000468030
	public By pdrCheckbox_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::span//span[@class='icon-ckbox']");
	public By pdrIconFlame_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::span//span[@class='icon-line-flame']");
	
	public By indrizzodellafornitura_text_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::span//span[text()='Indirizzo della fornitura']");
	public By indrizzodellafornitura_value_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::span//span[text()='Indirizzo della fornitura']/following-sibling::span");
	
	public By pdrText_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::*//span[text()='PDR']");
	public By pdrValue_03081000468030 = By.xpath("//span[text()='PDR']/parent::*//span[text()='03081000468030']");
	
	public By modalitadirichiesta_text_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::span//span[text()='Modalità di richiesta']");
	public By modalitadirichiesta_value_03081000468030 = By.xpath("//span[text()='03081000468030']/ancestor::span//span[text()='Modalità di richiesta']/following-sibling::span");
	
	//PDR - 00080000211465
	public By pdrCheckbox_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::span//span[@class='icon-ckbox']");
	public By pdrIconFlame_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::span//span[@class='icon-line-flame']");
	
	public By indrizzodellafornitura_text_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::span//span[text()='Indirizzo della fornitura']");
	public By indrizzodellafornitura_value_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::span//span[text()='Indirizzo della fornitura']/following-sibling::span");
	
	public By pdrText_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::*//span[text()='PDR']");
	public By pdrValue_00080000211465 = By.xpath("//span[text()='PDR']/parent::*//span[text()='00080000211465']");
	
	public By modalitadirichiesta_text_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::span//span[text()='Modalità di richiesta']");
	public By modalitadirichiesta_value_00080000211465 = By.xpath("//span[text()='00080000211465']/ancestor::span//span[text()='Modalità di richiesta']/following-sibling::span");
	
	
	
	//PDR - 00800006885570
	public By pdrCheckbox_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::span//span[@class='icon-ckbox']");
	public By pdrIconFlame_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::span//span[@class='icon-line-flame']");
	
	public By indrizzodellafornitura_text_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::span//span[text()='Indirizzo della fornitura']");
	public By indrizzodellafornitura_value_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::span//span[text()='Indirizzo della fornitura']/following-sibling::span");
	
	public By pdrText_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::*//span[text()='PDR']");
	public By pdrValue_00800006885570 = By.xpath("//span[text()='PDR']/parent::*//span[text()='00800006885570']");
	
	public By modalitadirichiesta_text_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::span//span[text()='Modalità di richiesta']");
	public By modalitadirichiesta_value_00800006885570 = By.xpath("//span[text()='00800006885570']/ancestor::span//span[text()='Modalità di richiesta']/following-sibling::span");
	
	
	//POD - IT004E20070603
	public By podCheckbox_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::span//span[@class='icon-ckbox']");
	public By podIconElectricity_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::span//span[@class='icon-line-electricity']");
	
	public By indrizzodellafornitura_text_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::span//span[text()='Indirizzo della fornitura']");
	public By indrizzodellafornitura_value_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::span//span[text()='Indirizzo della fornitura']/following-sibling::span");
	
	public By podText_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::*//span[text()='POD']");
	public By podValue_IT004E20070603 = By.xpath("//span[text()='POD']/parent::*//span[text()='IT004E20070603']");
	
	public By modalitadirichiesta_text_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::span//span[text()='Modalità di richiesta']");
	public By modalitadirichiesta_value_IT004E20070603 = By.xpath("//span[text()='IT004E20070603']/ancestor::span//span[text()='Modalità di richiesta']/following-sibling::span");
	
	
	//POD - IT001E04392213
	public By podCheckbox_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::span//span[@class='icon-ckbox']");
	public By podIconElectricity_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::span//span[@class='icon-line-electricity']");
	
	public By indrizzodellafornitura_text_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::span//span[text()='Indirizzo della fornitura']");
	public By indrizzodellafornitura_value_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::span//span[text()='Indirizzo della fornitura']/following-sibling::span");
	
	public By podText_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::*//span[text()='POD']");
	public By podValue_IT001E04392213 = By.xpath("//span[text()='POD']/parent::*//span[text()='IT001E04392213']");
	
	public By modalitadirichiesta_text_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::span//span[text()='Modalità di richiesta']");
	public By modalitadirichiesta_value_IT001E04392213 = By.xpath("//span[text()='IT001E04392213']/ancestor::span//span[text()='Modalità di richiesta']/following-sibling::span");
	
		
	//POD - IT001E56436337
	public By podCheckbox_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::span//span[@class='icon-ckbox']");
	public By podIconElectricity_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::span//span[@class='icon-line-electricity']");
	
	public By indrizzodellafornitura_text_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::span//span[text()='Indirizzo della fornitura']");
	public By indrizzodellafornitura_value_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::span//span[text()='Indirizzo della fornitura']/following-sibling::span");
	
	public By podText_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::*//span[text()='POD']");
	public By podValue_IT001E56436337 = By.xpath("//span[text()='POD']/parent::*//span[text()='IT001E56436337']");
	
	public By modalitadirichiesta_text_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::span//span[text()='Modalità di richiesta']");
	public By modalitadirichiesta_value_IT001E56436337 = By.xpath("//span[text()='IT001E56436337']/ancestor::span//span[text()='Modalità di richiesta']/following-sibling::span");
	
	public By supplyTableList = By.xpath("//*[contains(@class,'label-container')]");
	
	public By GasDetails = By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-flame'])[3]/parent::span/following-sibling::span");
	public By LightDetails = By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-electricity'])[1]/parent::span/following-sibling::span");
	
	public By Disattivazione_button = By.xpath("//button/span[text()='Disattivazione']");
	
	// Disattivazione Process
	
	public By DisattivazioneSteps = By.xpath("//li[contains(@class,'step')]");
	
	public By Inserimento_dati = By.xpath("//li[contains(@class,'step')]/descendant::span[text()='Inserimento dati']");
	public By Riepilogo_e_conferma_dati = By.xpath("//li[contains(@class,'step')]/descendant::span[text()='Riepilogo e conferma dati']");
	public By Esito = By.xpath("//li[contains(@class,'step')]/descendant::span[text()='Esito']");
	
	//Invio comunicazioni | Data Entry
	public By Inviocomunicazioni = By.xpath("//div[@class='form-group-container simply-border'][1]/h4[text()='Invio comunicazioni']");
	public By InviocomunicazioniSectionText1 = By.xpath("//div[@class='form-group-container simply-border'][1]/div/p[contains(text(),'Di seguito puoi modificare')]");
	public By Inviocomunicazioni_Email = By.xpath("//div[@class='form-group-container simply-border'][1]//div/label[@id='form-email-label-sfa']");
	public By Inviocomunicazioni_EmailInput = By.xpath("//div[@class='form-group-container simply-border'][1]//div/input[@id='form-email']");
	public By InviocomunicazioniSectionText2 = By.xpath("//div[@class='form-group-container simply-border'][2]//p[contains(text(),'Desideri ricevere')]");
	public By Inviocomunicazioni_RadioButton_Si = By.xpath("//div[@class='form-group-container simply-border'][2]//label[text()='Si']/preceding-sibling::input");
	public By Inviocomunicazioni_RadioButton_No = By.xpath("//div[@class='form-group-container simply-border'][2]//div[@class='radio-container']/label[text()='No, su indirizzo postale.']");
	
	//Email
	public By Email_ErrorMsg = By.xpath("//div[@class='form-group-container simply-border'][1]//div/span[contains(text(),'indirizzo mail')]");
	public By Inviocomunicazioni_ConfermaEmail = By.xpath("//div[@class='form-group-container simply-border'][1]//div/label[@id='form-email2-label-sfa']");
	public By Inviocomunicazioni_ConfermaEmailInput = By.xpath("//div[@class='form-group-container simply-border'][1]//div/input[@id='form-email2']");
	public By ConfermaEmail_ErrorMsg = By.xpath("//div[@class='form-group-container simply-border'][1]//div/span[contains(text(),'Le email')]");
	
	//Indica l'indirizzo presso cui ricevere la bolletta:
	public By Indical_indirizzoPresso = By.xpath("//div[@class='form-group-container simply-border'][*]//h4[contains(text(),'bolletta')]");
	
	public By CittaInputbox = By.xpath("(//div[@class='form-group-container simply-border'][*]//label[@id='data-label-Città']/following-sibling::input)[1]");
	public By IndirizzoInputbox = By.xpath("(//div[@class='form-group-container simply-border'][*]//label[@id='data-label-Indirizzo']/following-sibling::input)[1]");
	public By NumeroCivicoInputbox = By.xpath("(//div[@class='form-group-container simply-border'][*]//label[@id='data-label-Civico']/following-sibling::input)[1]");
	public By CAP_Inputbox = By.xpath("(//div[@class='form-group-container simply-border'][*]//label[text()='CAP*']/following-sibling::input)[1]");
	public By CAP_InputSelect = By.xpath("//div/span[@class='uiOutputText']");
	public By ToponomasticaSelect = By.xpath("//label[text()='Toponomastica']/following-sibling::select");
	
	public By  Inserisci_manualmente_Button = By.xpath("//button[text()=' Inserisci manualmente']");
	public By  Conferma_indirizzo_Button = By.xpath("//button[text()='Conferma indirizzo']");
	
	public By indirizzoinserto_non_e_stato_ErrorMsg = By.xpath("//button[text()=' Inserisci manualmente']/parent::div[contains(text(),'indirizzo inserito non è stato riconosciuto')]");
	public By Indical_indirizzoPressoErrorMsg = By.xpath("//div[@class='form-group-container simply-border'][*]//h4[contains(text(),'bolletta')]/../following-sibling::*[@class='errorMsg']");
	
	//Aiutaci a migliorare
	public By Aiutaci_a_migliorare = By.xpath("//div[@class='form-group-container simply-border'][*]/h4[text()='Aiutaci a migliorare']");
	public By Aiutaci_a_migliorare_SectionText = By.xpath("//div[@class='form-group-container simply-border'][*]/div/p[contains(text(),'Per offrire un servizio')]");
	public By Aiutaci_a_migliorare_MotivoDellaDisattivazione = By.xpath("//div[@class='form-group-container simply-border'][*]/div//label[contains(text(),'Motivo della')]");
	public By Aiutaci_a_migliorare_MotivoDellaDisattivazione_dropDown = By.xpath("//div[@class='form-group-container simply-border'][*]/div//label[contains(text(),'Motivo della')]/following-sibling::*//select");
	public By INDIETRO = By.xpath("//button[@id='backButton_step1']");
	public By CONTINUA = By.xpath("//button[@id='nextButton_step1']");
	
	// Riepilogo e conferma dati | Summary and Confirmation
	
	public By Bolletta_di_chiusura_HEADING = By.xpath("//div/h4[text()='Bolletta di chiusura']");
	public By Bolletta_di_chiusura_SUBTEXT = By.xpath("//div/p[contains(text(),'Riceverai la bolletta di chiusura')]");
	public By Bolletta_di_chiusura_SUBTEXTVALUE = By.xpath("//div/p[text()='Via Nizza 4 00198 Roma Roma Lazio']");
	public By Motivo_disattivazione_HEADING = By.xpath("//div/h4[text()='Motivo disattivazione']");
	public By Motivo_disattivazione_SUBTEXT = By.xpath("//div/p[contains(text(),'Preferisco')]");
	public By Motivo_disattivazione_Description = By.xpath("//div/p[contains(text(),'Procedendo con')]");
	public By Hai_richiesto_la_disattivazione_HEADING = By.xpath("//div/h4[contains(text(),'Hai richiesto')]");
	
	//Numero - 300001597
	
	public By IconFlame_300001597 = By.xpath("//span[text()='300001597']/ancestor::*/span[@class='icon-line-flame']");
	
	public By IconFlame_310507745 = By.xpath("//span[text()='310507745']/ancestor::*/span[@class='icon-line-flame']");
	public By ConfirmGasDetails = By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-flame']//parent::span[@class='flex'])[1]");
	
	//Numero - 300001597
	
	public By IconElectricity_300001594 = By.xpath("//span[text()='300001594']/ancestor::*/span[@class='icon-line-electricity']");
	
	public By IconElectricity_310513588 = By.xpath("//span[text()='310513588']/ancestor::*/span[@class='icon-line-electricity']");
	public By ConfirmElectricityDetails = By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-electricity']//parent::span[@class='flex'])[1]");
	
	public By indicaFields = By.xpath("(//*[text()='Città'])[1]/following-sibling::input | /following-sibling::input | (//*[text()='Indirizzo'])[2]/following-sibling::input | (//*[text()='Numero Civico*'])[1]/following-sibling::input");
	
	/*public By dailogTitle = By.xpath("(//h3[text()='Attenzione!'])[1]");
	public By dailogClose = By.xpath("(//button[@onClick='focusCloseModale()'])[1]");*/
	
	ColorUtils c = new ColorUtils();
	
	public Privato211ACRDisattivazioneComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void checkTextHighlight(By object, String color) throws Exception{

        String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));

        if(!colore.contentEquals(color)) throw new Exception("l'oggetto web  e' di colore "+color+". Colore trovato : "+colore);
    }
	
	public void verifyFooterVisibility() throws Exception {
		
		String[] footerValue = {"© Enel Energia S.p.a.","Tutti i Diritti Riservati","Gruppo IVA Enel P.IVA 15844561009","Informazioni Legali","Privacy","Credits","Contattaci"};
		List<WebElement> footer = driver.findElements(fotterSection);
		String footerText [] = new String[footer.size()];
		 
		for (int i = 0; i < footer.size(); i++) {
			
			footerText[i] = driver.findElement(By.xpath("(//footer//div[@class='footer-info-container']/ul/li)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
			
			if(!footerText[i].equalsIgnoreCase(footerValue[i])){
				throw new Exception("Footer values for "+i+" is not matching")	;		
				} 
		}
		
	}
	
	public void leftMenuItemsDisplay (By checkObject) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
					
	String item_found="NO";
	List<WebElement> leftMenu = driver.findElements(By.xpath("//ul[@id='internal-menu']/li/a[not(@style='display:none')]"));
	
	String leftMenuItems [] = PRIVATE_AREA_MENU_RES.split(";");
	
	for (int i=0, j=0; i<leftMenu.size() && j<leftMenuItems.length; i++,j++)
	{

		String description = driver.findElement(By.xpath("(//ul[@id='internal-menu']/li/a[not(@style='display:none')])["+(i+1)+"]")).getText();
		description= description.replaceAll("(\r\n|\n)", " ");

		if (description.contentEquals(leftMenuItems[(j)])) 
			item_found="YES";

		if (item_found.contentEquals("NO"))
			throw new Exception("on the 'https://www-colla.enel.it/ Resedential left menu section are not present with description "+PRIVATE_AREA_MENU_RES);
				}
	}
	
	public void verifyElementIfHiglightedByCompare(By element) throws Exception
	{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
			
			String textHighlight = "NO";
			
			WebElement keyElement = driver.findElement(element);
			String keyElementColor = c.getColorNameFormRgbString(util.waitAndGetElement(element,true).getCssValue("color"));  
			String keyElementColorText = keyElement.getText();
			
			List<WebElement> menuList= driver.findElements(By.xpath("//ul[@id='internal-menu']/li/a[not(@style='display:none')]"));

			for (int i = 0; i < menuList.size(); i++) {

				String menuItemText = driver.findElement(By.xpath("(//ul[@id='internal-menu']/li/a[not(@style='display:none')]/span[text()])["+(i+1)+"]")).getText();
				String menuItemColor = c.getColorNameFormRgbString(util.waitAndGetElement(By.xpath("(//ul[@id='internal-menu']/li/a[not(@style='display:none')]/span[text()])["+(i+1)+"]"),true).getCssValue("color"));
	
				if (!(keyElementColor.equalsIgnoreCase(menuItemColor))) {
					textHighlight = "YES";
					
				} else if(keyElementColor.equalsIgnoreCase(menuItemColor) && keyElementColorText.contentEquals(menuItemText)){
					
					continue;
				}
				
				if (textHighlight.contentEquals("NO")) {
					throw new Exception(keyElementColorText+" is not highlighted when compared with other elements");
				}
				
			}
	}

	public void checkDomandeFrequentiSectionFaqDetails() throws Exception{
		
		for(int i=0;i<6;i++){
			
			verifyComponentExistence((By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']")));//faq
			verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));// plus
			
			String question = driver.findElement(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']")).getText();
			
			if (domandeFrequentiSectionFaq.contains(question)) {
								
				clickComponent(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-plus']"));//plus
				
				Thread.sleep(3000);
				
				String ans = driver.findElement(By.xpath("//div[@id='accordion-panel-item-accordion-"+i+"']/*")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(domandeFrequentiSectionFaqAns.contains(ans)){		
					result="YES";		
					verifyComponentExistence(By.xpath("//button[@id='accordion-button-item-accordion-"+i+"']/span[@class='dsc-icon-line-minus']"));//minus
				}
				
				if(result.equals("No"))throw new Exception ("Domande Frequenti Section Faq answer is not as expected");
				
			}else {
				throw new Exception ("Domande Frequenti Section Faq is not as expected");
			}
		}
		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 20))
			throw new Exception("object with xpath " + existingObject + " is not exist.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		util.objectManager(clickableObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentVisibility(By visibleObject) throws Exception {
		if (!util.verifyVisibility(visibleObject, 15)) {
			throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
		}
	}

	public void validateDailogBoxContent(By checkObject, String Value) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(checkObject);
		
		String text = element.getText();
		
		if (Value.contains(text))
		textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text" + Value + " is not found:");
		}
	
	
	public void verifyLuceSupplyGreaterThan1(By supplyTableList) throws Exception
	{
		
		String res="NO";
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(supplyTableList));
		
		List<WebElement> eleIcons = driver.findElements(By.xpath("//*[contains(@class,'label-container')]//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']"));
		List<WebElement> chkbx = driver.findElements(By.xpath("//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox']"));
		
		if (eleIcons.size()>1 && chkbx.size()>1) {
			res="YES";
			
			for (int i = 1; i <= 4; i++) {
				
				/*Thread.sleep(5000);
				if(i>=6)
				{
					driver.findElement(dailogClose).click();
				}*/
				
				verifyComponentExistence((By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity'])["+i+"]")));  // ElectricityIcon
				verifyComponentExistence(By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox'])["+i+"]")); //Checkbox
				
				verifyCheckboxnotSelected(By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox'])["+i+"]")); 
			    
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Indir')])["+i+"]"));  //Indir
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Via')])["+i+"]"));  //Via
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'POD')])["+i+"]"));  //POD
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span//span[contains(text(),'IT00')])["+i+"]"));  //IT00
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Modalit')])["+i+"]"));  //Modalit
				verifyComponentExistence(By.xpath("(//span[@class='icon-ckbox']/following-sibling::span/span[@class='icon-line-electricity']/parent::*/following-sibling::span/span[contains(text(),'Web')])["+i+"]"));  //Web
				
				clickComponent(By.xpath("(//*[contains(@class,'label-container')]//span[@class='icon-line-electricity']/parent::*/preceding-sibling::span[@class='icon-ckbox'])["+i+"]")); // Click on Checkbox
				
				
			}
		} 
		
		if (res.contentEquals("NO"))
			throw new Exception("The expected count Luce or ELE supply is not greater > 1");
		
	}
		
	
	
	public void verifyCheckboxnotSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (!element.isSelected()){
		checkbox="YES";
		}			
		if (element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is selected");
				}
	}
	
	
	public void verifyCheckboxSelected(By NocheckboxObject) throws Exception {
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
		
		String checkbox="NO";
		
		WebElement element = driver.findElement(NocheckboxObject);
		
		if (element.isSelected()){
		checkbox="YES";
		}			
		if (!element.isSelected()){
		checkbox = "NO";
			throw new Exception("The checkbox is not selected");
				}
	}

	public void VerifyText(By checkObject, String Value) throws Exception {
	
	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
	
	String textfield_found="NO";
	
	WebElement element = driver.findElement(checkObject);
	
	String actualtext = element.getText();
	
	
	if (actualtext.contentEquals(Value))
	textfield_found="YES";
					
	if (textfield_found.contentEquals("NO"))
		throw new Exception("The expected text" + Value + " is not found:");
	}
	
	
	public void validateAndClickDisattivazioneButton(By DisattivazioneButton) throws Exception
	{
		
		verifyComponentExistence(pdrCheckbox_00080000211465);
		verifyComponentExistence(podIconElectricity_IT004E20070603);
		
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(pdrCheckbox_00080000211465));
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(podIconElectricity_IT004E20070603));
		
		
		String checkbox="NO";
		
		WebElement checkbox_Gas = driver.findElement(pdrCheckbox_00080000211465);
		WebElement checkbox_Light = driver.findElement(podIconElectricity_IT004E20070603);
		
		if (!checkbox_Gas.isSelected() && !checkbox_Light.isSelected()){
		checkbox="YES";
		
		clickComponent(pdrCheckbox_00080000211465);
		clickComponent(podIconElectricity_IT004E20070603);
		
		verifyComponentExistence(DisattivazioneButton);
		clickComponent(DisattivazioneButton);
		}	
		
		if (checkbox.contentEquals("NO"))
			throw new Exception("Checkbox is already selected for either ELE or GAS supply");
		
	}
	
	public void validateDisattivazioneSteps() throws Exception
	{
		List<WebElement> steps = driver.findElements(By.xpath("//li[contains(@class,'step')]"));
		
		String stepFound="NO";
		
		for(WebElement step : steps){
			String stepDescription=step.getText();
			stepDescription= stepDescription.replaceAll("(\r\n|\n)", " ");
			
			if (stepDescription.contains("1 Inserimento dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("2 Riepilogo e conferma dati")) {
				stepFound = "YES";
			}else if (stepDescription.contentEquals("3 Esito")) {
				stepFound = "YES";
			}
			
			if (stepFound.contentEquals("NO"))
				throw new Exception("Steps are not displayed");
		}
		
//		// Validating the color of highlighted step
//		checkTextHighlight(By.xpath("//li[contains(@class,'step')]/descendant::span[@class='step-label active']"), servicesColor);
	}
	
	
	public void validateDisattivazioneSupplyDetails(String expectedEmail) throws Exception
	{
		//Invio comunicazioni
		verifyComponentExistence(Inviocomunicazioni);
		VerifyText(Inviocomunicazioni, Invio_comunicazioni);
		
		
		verifyComponentExistence(InviocomunicazioniSectionText1);
		VerifyText(InviocomunicazioniSectionText1, Invio_comunicazioni_SectionText1);
		
		verifyComponentExistence(Inviocomunicazioni_Email);
		VerifyText(Inviocomunicazioni_Email, Invio_comunicazioni_Email);

		verifyComponentExistence(Inviocomunicazioni_EmailInput);

		//Verifying the populated email 
		checkPrePopulatedValueAndCompare(populatedEmailValue,expectedEmail);
		
		verifyComponentExistence(InviocomunicazioniSectionText2);
		VerifyText(InviocomunicazioniSectionText2, Invio_comunicazioni_SectionText2);
		
		//Radio button validation  - Selected
		verifyComponentExistence(Inviocomunicazioni_RadioButton_Si);
		verifyCheckboxSelected(Inviocomunicazioni_RadioButton_Si);
		
		//Radio button validation  - Not Selected
		verifyComponentExistence(Inviocomunicazioni_RadioButton_No);
		verifyCheckboxnotSelected(Inviocomunicazioni_RadioButton_No);
		
		
		//Aiutaci a migliorare
		verifyComponentExistence(Aiutaci_a_migliorare);
		VerifyText(Aiutaci_a_migliorare, Aiutaci_Heading);
		
		verifyComponentExistence(Aiutaci_a_migliorare_SectionText);
		VerifyText(Aiutaci_a_migliorare_SectionText, Aiutaci_SectionText);
		
		verifyComponentExistence(Aiutaci_a_migliorare_MotivoDellaDisattivazione);
		VerifyText(Aiutaci_a_migliorare_MotivoDellaDisattivazione, Aiutaci_MotivoDellaDisattivazione);
		
		verifyComponentExistence(Aiutaci_a_migliorare_MotivoDellaDisattivazione_dropDown);
		validateDropDownValue(Aiutaci_a_migliorare_MotivoDellaDisattivazione_dropDown, Aiutaci_MotivoDellaDisattivazione_defaultDropdownValue);
		
		verifyComponentExistence(INDIETRO);
		verifyComponentExistence(CONTINUA);
		
	}
	
	public void validateDisattivazioneSupplyAndConfirmDetails(By gasObject, By lightObject) throws Exception
	{
		//Bolletta di chiusura
		verifyComponentExistence(Bolletta_di_chiusura_HEADING);
		VerifyText(Bolletta_di_chiusura_HEADING, Bollettadichiusura_HEADING);
		
		verifyComponentExistence(Bolletta_di_chiusura_SUBTEXT);
		VerifyText(Bolletta_di_chiusura_SUBTEXT, Bollettadichiusura_SUBTEXT);
		
		verifyComponentExistence(Bolletta_di_chiusura_SUBTEXTVALUE);
		VerifyText(Bolletta_di_chiusura_SUBTEXTVALUE, Bollettadichiusura_SUBTEXTVALUE);
		
		//Motivo disattivazione
		verifyComponentExistence(Motivo_disattivazione_HEADING);
		VerifyText(Motivo_disattivazione_HEADING, Motivodisattivazione_HEADING);
		
		verifyComponentExistence(Motivo_disattivazione_SUBTEXT);
		VerifyText(Motivo_disattivazione_SUBTEXT, Motivodisattivazione_SUBTEXT);
		
		verifyComponentExistence(Motivo_disattivazione_Description);
		VerifyText(Motivo_disattivazione_Description, Motivodisattivazione_Description);
		
		verifyComponentExistence(Hai_richiesto_la_disattivazione_HEADING);
		VerifyText(Hai_richiesto_la_disattivazione_HEADING, Hai_richiesto_ladisattivazione_HEADING);
		
		//Confirmation of supply details - Gas,Light
		confirmGasSupply(gasObject);
		confirmLightSupply(lightObject);
				
	}	
	
	public void emailValidation(By checkObject, String emailText) throws Exception
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		
		try {
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			WebElement element = driver.findElement(checkObject);
			element.sendKeys(emailText);
			Thread.sleep(3000);
			element.sendKeys(Keys.TAB);
//			element.sendKeys(Keys.chord(Keys.TAB));
			
		} catch (Exception e) {
			throw new Exception("Email element not found");
		}
		

	}
	
	public void enter_Indica_l_indirizzo_FieldDetails(By checkObject)
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
		String[] field = {"(//*[text()='Città'])[1]/following-sibling::input","(//*[text()='Indirizzo'])[2]/following-sibling::input","(//*[text()='Numero Civico*'])[1]/following-sibling::input"};
		String[] fieldValues = {"ROMA (ROMA) LAZIO","VIA NIZZA","4"};
		
		List<WebElement> fields = driver.findElements(checkObject);
		
		for (int i = 0; i < fields.size(); i++) {
			
			driver.findElement(By.xpath(field[i])).sendKeys(fieldValues[i]);
			driver.findElement(By.xpath(field[i])).sendKeys(Keys.TAB);
		}
	}
	
	public void verifyErrorMsgDisplayed(By errorObject, String errorTextValue, String errorColor) throws Exception
	{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(errorObject));
		
		String textfield_found="NO";
		
		WebElement element = driver.findElement(errorObject);
		
		String actualErrortext = element.getText();
		String actualColor = c.getColorNameFormRgbString(element.getCssValue("color"));
		
		if (actualErrortext.contentEquals(errorTextValue) && actualColor.contentEquals(errorColor))				
			textfield_found="YES";
						
		if (textfield_found.contentEquals("NO"))
			throw new Exception("The expected text/color for error message" + errorTextValue + " is not found/matching:");
	}
	
	public void verifyErrorMsgNotDisplayed(String errorMessage) throws Exception
	{
		
		String found = "YES";
		
		if (!(driver.getPageSource().contains(Email_ErrorMessage))) {
			found = "No";
		}

		if (found.contentEquals("YES"))
			throw new Exception("The error message is displayed which was not expected");
		
	}
	
	public void checkColor(By object, String color, String objectName) throws Exception{

		String colore = c.getColorNameFormRgbString(util.waitAndGetElement(object,true).getCssValue("color"));

		if(!colore.contentEquals(color)) throw new Exception("l'oggetto web "+objectName+" e' di colore "+color+". Colore trovato : "+colore);
    } 
	
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}
	
	public void enterInput(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void checkForDropDownValue_Aiutaci_a_migliorare(By checkObject) throws Exception{
		
		String[] optionValue = {"Scegli il motivo","CAMBIO CASA","INUTILIZZO", "USO STAGIONALE", "ALTRO", "PREFERISCO NON RISPONDERE"};
		
	    WebElement dropdown = driver.findElement(checkObject);
	    Select select = new Select(dropdown);

	    List<WebElement> options = select.getOptions();
	    for (WebElement we : options) {
	        for (int i = 0; i < optionValue.length; i++) {
	            if (we.getText().equals(optionValue[i])) {
	                i++; 
	                }
	        }
	    }
		
	}
	
	public void validateDropDownValue(By checkObject, String property )throws Exception {
		Select select = new Select(driver.findElement(checkObject));
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();
		if (!defaultItem.equalsIgnoreCase(property))
			throw new Exception("The expected option is not selected in the web page");
	}
	
	public void selectDropDownByValue(By checkObject, String Value )throws Exception {
		
		WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(checkObject));
        
        String valueFound = "NO";
        
		Select select = new Select(driver.findElement(checkObject));
		
		List<WebElement> options = select.getOptions();
	    for (WebElement we : options) {
	        if (we.getText().equals(Value)) {
	        	valueFound = "Yes";
	        	select.selectByValue(Value);
	                
	                }
	    }
	    if (valueFound.contentEquals("NO"))
			throw new Exception("Option not found to select");
	}
	
	public void selectOptions(String option) throws Exception {
		
		String[] optionValue = {"Scegli il motivo","CAMBIO CASA","INUTILIZZO", "USO STAGIONALE", "ALTRO", "PREFERISCO NON RISPONDERE"};
		
		   WebElement dropdown = driver.findElement(By.xpath("//div[@class='slds-form-element dummySelect accessibleButton']/descendant::select[@class='slds-select']"));
		   String value = "No";
				 
		   Select select = new Select(dropdown); 
		   List<WebElement> options = select.getOptions(); 
		                   
		    for (int i=0; i<options.size(); i++){
		    String opt = options.get(i).getText();
		    
		      if (opt.equals(optionValue[i])){
		      value = "Yes";		    			 
		        		}
		                          
		      else {
		      throw new Exception("dropdown values are not matching");
                   }
		     		     
		    } 
		    select.selectByVisibleText(option);
		}
	
	public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
        
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        if(value.equals(""))throw new Exception("Field is not pre filled");
        
        String match = "FAIL";
        if(value.contentEquals(expectedValue))
        	match = "PASS";
        if(match.contentEquals("FAIL"))
        	throw new Exception("PrePopulatedValue and Expected values are not matching");
    }
	

	
	public void gasSupplyDetailsDisplay(By checkObject) throws Exception {
		
		verifyComponentExistence(pdrCheckbox_00080000211465);
		verifyComponentExistence(pdrIconFlame_00080000211465);
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-flame'])[2]/parent::span/following-sibling::span/span[1]"));
		List<WebElement> value = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-flame'])[2]/parent::span/following-sibling::span/span[2]"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='icon-line-flame'])[2]/parent::span/following-sibling::span/span[1])["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='icon-line-flame'])[2]/parent::span/following-sibling::span/span[2])["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("Via Noventana 192 35027 Noventa Padovana Noventa Padovana Pd")) {
				textfield_found="YES";
			}else if (description.contentEquals("PDR") && descriptionValue.contentEquals("00080000211465")) {
				textfield_found="YES";
			}else if (description.contentEquals("Modalità di richiesta") && descriptionValue.contentEquals("Web")) {
				textfield_found="YES";
			} 
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Gas Supply details are not matching for index :"+i);
			
		}
		
	}	
	public void lightSupplyDetailsDisplay(By checkObject) throws Exception {
		
		verifyComponentExistence(podCheckbox_IT004E20070603);
		verifyComponentExistence(podIconElectricity_IT004E20070603);
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-electricity'])[1]/parent::span/following-sibling::span/span[1]"));
		List<WebElement> value = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-electricity'])[1]/parent::span/following-sibling::span/span[2]"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='icon-line-electricity'])[1]/parent::span/following-sibling::span/span[1])["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='icon-line-electricity'])[1]/parent::span/following-sibling::span/span[2])["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("Via Abbadia 1 60027 Osimo Osimo An")) {
				textfield_found="YES";
			}else if (description.contentEquals("POD") && descriptionValue.contentEquals("IT004E20070603")) {
				textfield_found="YES";
			}else if (description.contentEquals("Modalità di richiesta") && descriptionValue.contentEquals("Web")) {
				textfield_found="YES";
			} 
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Light Supply details are not matching for index :"+i);
			
		}
		
	}
	
	public void confirmGasSupply(By checkObject) throws Exception {
			
			verifyComponentExistence(IconFlame_310507745);
			
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
							
			String textfield_found="NO";
						
			List<WebElement> field = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-flame']//parent::span[@class='flex'])/span[not(contains(@class,'icon'))]/span[1]"));
			List<WebElement> value = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='icon-line-flame']//parent::span[@class='flex'])/span[not(contains(@class,'icon'))]/span[2]"));
			
			for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
				
				WebElement element = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='icon-line-flame']//parent::span[@class='flex'])/span[not(contains(@class,'icon'))]/span[1])["+i+"]"));
				String description=element.getText();
				description= description.replaceAll("(\r\n|\n)", " ");
				
				WebElement element1 = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='icon-line-flame']//parent::span[@class='flex'])/span[not(contains(@class,'icon'))]/span[2])["+j+"]"));
				String descriptionValue=element1.getText();
				descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
				
				if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("Via Noventana 192 35027 Noventa Padovana Noventa Padovana Pd")) {
					textfield_found="YES";
				}else if (description.contentEquals("Numero cliente") && descriptionValue.contentEquals("310507745")) {
					textfield_found="YES";
				}
				
				if (textfield_found.contentEquals("NO"))
					throw new Exception("Confirm Gas Supply details are not matching for index :"+i);
				
			}
			
		}
	
	public void confirmLightSupply(By checkObject) throws Exception {
		
		verifyComponentExistence(IconElectricity_310513588);
		
		
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
						
		String textfield_found="NO";
					
		List<WebElement> field = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='flex'])[2]/span[not(contains(@class,'icon'))]/span[1]"));
		List<WebElement> value = driver.findElements(By.xpath("(//div[contains(@class,'label-container')]//span[@class='flex'])[2]/span[not(contains(@class,'icon'))]/span[2]"));
		
		for (int i = 1,j = 1; i <= field.size() && j<= value.size() ; i++,j++) {
			
			WebElement element = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='flex'])[2]/span[not(contains(@class,'icon'))]/span[1])["+i+"]"));
			String description=element.getText();
			description= description.replaceAll("(\r\n|\n)", " ");
			
			WebElement element1 = driver.findElement(By.xpath("((//div[contains(@class,'label-container')]//span[@class='flex'])[2]/span[not(contains(@class,'icon'))]/span[2])["+j+"]"));
			String descriptionValue=element1.getText();
			descriptionValue= descriptionValue.replaceAll("(\r\n|\n)", " ");
			
			if (description.contentEquals("Indirizzo della fornitura") && descriptionValue.contentEquals("Via Abbadia 1 60027 Osimo Osimo An")) {
				textfield_found="YES";
			}else if (description.contentEquals("Numero cliente") && descriptionValue.contentEquals("310513588")) {
				textfield_found="YES";
			}
			
			if (textfield_found.contentEquals("NO"))
				throw new Exception("Confirm Light Supply details are not matching for index :"+i);
			
		}
		
	}
	
	
	public static final String servicesColor = "Crimson";
	public static final String errorColor = "Red";
	public static final String successColor = "Teal";
	public static final String PRIVATE_AREA_MENU_RES = "Forniture;Bollette;Servizi;enelpremia WOW!;Novità;Spazio Video;Chiamaci;Porta i tuoi amici;Account;I tuoi diritti;Supporto;Trova Spazio Enel.;Esci";
	public static final String bottomSection[] = {"© Enel Energia S.p.a.","Tutti i Diritti Riservati","P. IVA 06655971007", "Informazioni Legali","Privacy","Credits","Contattaci"};
	
	public String result = "NO";
	
	public String domandeFrequentiSectionFaq ="Quali sono i tempi per disattivare la fornitura?"
			+"E' possibile prenotare una data di disattivazione?"
			+"Quali sono i costi per disattivare la fornitura?"
			+"Come posso utilizzare il bonus?"
			+"Chi può disattivare una fornitura online?"
			+"Attraverso quali canali posso disattivare la fornitura?";
	
	public String domandeFrequentiSectionFaqAns ="I tempi massimi per l’evasione della tua richiesta sono:"
			+"LUCE: fino a 7 giorni lavorativi."
			+"GAS: fino a 9 giorni lavorativi."	
			+"Il recesso dal contratto con enel energia avrà efficacia entro 30gg dalla data di richiesta."
			+"Il servizio di prenotazione è messo a disposizione dal distributore e-distribuzione per le forniture elettriche, erogate da qualsiasi venditore." 
			+" Puoi verificare il tuo distributore consultando la tua ultima bolletta ricevuta."
			+"Il costo per la disattivazione della fornitura, che sarà addebitato nell'ultima bolletta che riceverai, è pari a €23 + Iva oltre a eventuali costi fissi applicati dal distributore."
			+"Ad avvenuta cessazione riceverai una mail contenente il codice che ti permetterà di utilizzare il bonus per l'attivazione di una nuova fornitura sul sito enel.it."
			+" Il bonus è valido per 1 anno dalla data di disattivazione della fornitura per la sottoscrizione di un contratto di luce o gas relativo a prima attivazione, voltura, subentro o cambio fornitore su tutte le offerte di Enel Energia, ad esclusione di Sempre con te, Sempre con te Gas, E-Light ed E-Light gas. Il bonus non è cumulabile con altre promozioni in corso."
			+" Al tuo rientro in Enel Energia ti verranno accreditati 25 euro ripartiti nelle prime bollette relative alla nuova fornitura."
			+"La funzionalità è disponibile per tutte le forniture ad uso domestico, tranne per quelle non disalimentabili (es. alimentazione di apparecchi elettromedicali per mantenimento in vita) e per quelle in cui è necessario la rimozione del contatore."
			+"La richiesta di disattivazione può essere effettuata soltanto dal cliente titolare del contratto di fornitura nei seguenti modi:"
			+"Accedendo al servizio attraverso il form di autenticazione presente in questa pagina."
			+"Recandosi presso uno dei nostri "
			+"negozi Enel."
			+"Chiamando il nostro call center al numero 800.900.860.";
	
	public String dailogBoxContent ="Ti ricordiamo che:"
			+"Puoi selezionare al massimo una fornitura gas;"
			+"Se scegli una fornitura gas, puoi anche selezionare una ulteriore fornitura luce;"
			+"Puoi selezionare al massimo cinque forniture luce contemporaneamente."
			+"Se il tuo distributore è "
			+"e-Distribuzione"
			+", la fornitura verrà indicata come verificata "
			+" in questo caso sarà possibile scegliere la data di disattivazione";
	
	public String populatedEmailValue = "//div[@class='form-group-container simply-border'][1]//div/input[@id='form-email']";
	public String populatedCAPboxValue = "(//div[@class='form-group-container simply-border'][*]//label[text()='CAP*']/following-sibling::input)[1]";
	
	public String Invio_comunicazioni = "Invio comunicazioni";
	public String Invio_comunicazioni_SectionText1 = "Di seguito puoi modificare o confermare la mail dove invieremo le future comunicazioni:";
	public String Invio_comunicazioni_Email = "Email*";
	public String Invio_comunicazioni_ConfermaEmail = "Conferma email*";
	public String Invio_comunicazioni_ExpectedEmail = "annamaria.cecili@enelsaa3006.enel.com";
	public String Invio_comunicazioni_ValidEmail = "annamaria.cecili@enelsaa3006.enel.com";
	public String Invio_comunicazioni_InvalidEmail = "annamaria.cecili@enelsaa3006.enel.com@";
	public String Invio_comunicazioni_SectionText2 = "Desideri ricevere anche l'ultima bolletta sull'indirizzo email indicato?";
	
	public String Email_ErrorMessage = "L'indirizzo mail non è valido. Verifica di averlo scritto in modo corretto.";
	public String ConfermaEmail_ErrorMessage = "Le email non corrispondono";
	
	public String Indical_indirizzoPresso_Heading = "Indica l'indirizzo presso cui ricevere la bolletta:";
	
	public String CittaInputText = "ROMA (ROMA) LAZIO";
	public String IndirizzoInputText = "VIA NIZZA";
	public String NumeroCivicoInputText = "4";
	public String empty_CAPbox = "";
	public String CAPbox = "00198";
	public String Toponomastica = "VIA";
	
	public String Indical_indirizzoPresso_ErrorMsg = "L'indirizzo è obbligatorio";
	public String Indirizzoinserto_non_e_stato_ErrorMsg = "L'indirizzo inserito non è stato riconosciuto";
	
	public String Aiutaci_Heading = "Aiutaci a migliorare";
	public String Aiutaci_SectionText = "Per offrire un servizio migliore ti chiediamo di indicare il motivo per cui stai richiedendo la disattivazione.";
	public String Aiutaci_MotivoDellaDisattivazione = "Motivo della disattivazione (facoltativo)";
	public String Aiutaci_MotivoDellaDisattivazione_defaultDropdownValue = "Scegli il motivo";
	public String Aiutaci_MotivoDellaDisattivazione_selectDropdownValue = "PREFERISCO NON RISPONDERE";
	
	public String  Bollettadichiusura_HEADING= "Bolletta di chiusura";
	public String  Bollettadichiusura_SUBTEXT= "Riceverai la bolletta di chiusura al seguente indirizzo:";
	public String  Bollettadichiusura_SUBTEXTVALUE= "Via Nizza 4 00198 Roma Roma Lazio";
	public String  Motivodisattivazione_HEADING= "Motivo disattivazione";
	public String  Motivodisattivazione_SUBTEXT= "Preferisco non rispondere";
	public String  Motivodisattivazione_Description= "Procedendo con l'operazione avrai diritto ad un bonus di 25€ per la sottoscrizione di un nuovo contratto con Enel Energia entro un anno.";
	public String  Hai_richiesto_ladisattivazione_HEADING= "Hai richiesto la disattivazione del servizio per le seguenti forniture:";
	
	public String SectionTitleSubText = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
		
}
