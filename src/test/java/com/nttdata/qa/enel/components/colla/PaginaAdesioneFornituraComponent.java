package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PaginaAdesioneFornituraComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By campoCap=By.xpath("//label[@for='ITA_IFM_ZIPCode__c' and text()='CAP*']/ancestor::div[1]/input[@id='ITA_IFM_ZIPCode__c' and @placeholder='CAP']");
	public By checkBoxCambioFornitore=By.xpath("//label/span[text()='CAMBIO FORNITORE']/ancestor::div[1]/input[@id='changeSupplier']");
	
	
	public PaginaAdesioneFornituraComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	public void inserisciCap(String valore) throws Exception {
		 List <WebElement> cap = util.waitAndGetElements(By.xpath("//ul[@id='ui-id-1']//li//div"));
		 
		 for (int i=1; i<=cap.size(); i++) {
			String c=util.waitAndGetElement(By.xpath("//ul[@id='ui-id-1']//li["+i+"]//div")).getText();
		//	System.out.println(c);
			
			if (c.contentEquals(valore)) {
				WebElement button=driver.findElement(By.xpath("//ul[@id='ui-id-1']//li["+i+"]//div"));
				TimeUnit.SECONDS.sleep(1);
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", button);
				TimeUnit.SECONDS.sleep(1);
				break;
			}
			 
		 }
	}
	public void clickComponent(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		util.objectManager(clickObject, util.scrollAndClick);
		
	}
	
	public void verifyComponentExistence(By existentObject) throws Exception {
		if (!util.exists(existentObject, 15))
			throw new Exception("l'oggetto web con xpath " + existentObject + " non esiste.");
	}
	
	public void scrollComponent(By object) throws Exception {
		util.objectManager(object, util.scrollToVisibility, true);
	}
	
	public void enterValueInInputBox(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void click(By clickObject) throws Exception {
		util.objectManager(clickObject, util.scrollToVisibility, true);
		TimeUnit.SECONDS.sleep(3);
		WebElement button=util.waitAndGetElement(clickObject);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", button);
		
	}
}
