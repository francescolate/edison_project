package com.nttdata.qa.enel.components.colla;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class SupportLegalClausesComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public By clausesPageTitle = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By faqTitle = By.xpath("//h3[@class='anchor plan-main-head']");
	public By legalClausesFaq1 = By.xpath("//section[1]/button");
	public By legalClausesFaq2 = By.xpath("//section[2]/button");
	public By legalClausesFaq3 = By.xpath("//section[3]/button");
	public By legalClausesFaq4 = By.xpath("//section[4]/button");
	public By legalClausesFaq5 = By.xpath("//section[5]/button");
	public By legalClausesFaq6 = By.xpath("//section[6]/button");
	public By legalClausesFaq7 = By.xpath("//section[7]/button");
	public By legalClausesFaq8 = By.xpath("//section[8]/button");
	public By legalClausesFaq9 = By.xpath("//section[9]/button");
	
	public String Result ="NO";
	
	public static final String LegalClausesFAQ ="Oggetto delle Condizioni Generali;Registrazione;Assunzione di responsabilità e Manleva"
			+ "Recesso;Sospensione e/o interruzione dei servizi da parte di Enel Energia;Modifiche delle Condizioni Generali"
			+ "Proprietà Intellettuale;Limitazione di responsabilità di Enel Energia;Legge applicabile";

	
	public SupportLegalClausesComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	public void checkForText(By checkObject, String property) throws Exception {
		
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);		
	}
	
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}
	
	public void SwitchTabToLegalClauses() throws Exception
	{
	   
		Set <String> windows = driver.getWindowHandles();
		 String currentHandle = driver.getWindowHandle();
		
		for (String winHandle : windows) {
		   // Switch to child window
			//System.out.println("switched to ### "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
		 
			verifyComponentExistence(clausesPageTitle);
			verifyComponentExistence(faqTitle);
			
			util.scrollToElement(driver.findElement(legalClausesFaq1));
			
			if(LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText()) && LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText())
					&& LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText()) && LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText())
					&& LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText()) && LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText())
					&& LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText()) && LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText())
					&& LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText()) && LegalClausesFAQ.contains(driver.findElement(legalClausesFaq1).getText())){
				
				Result="YES";
			}
			
			if(Result.equals("NO")) throw new Exception("Legal Clauses Faq is not matching");
			

			driver.close();
			driver.switchTo().window(currentHandle);
		
	}
}
