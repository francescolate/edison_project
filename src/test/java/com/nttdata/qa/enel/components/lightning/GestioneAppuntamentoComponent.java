package com.nttdata.qa.enel.components.lightning;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class GestioneAppuntamentoComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public GestioneAppuntamentoComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util=new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}


	public final String sceltaAppuntamento = "//lightning-button/button[text()='@SCELTA@']";
	public final By nomeAppuntamento = By.xpath("//input[@name='Nome']");
	public final By cognomeAppuntamento = By.xpath("//input[@name='Cognome']");
	public final By telefonoAppuntamento = By.xpath("//label[text()='Telefono Appuntamento']/following-sibling::div/input");
	public final By setInformativa = By.xpath("//span[@class='slds-checkbox']/ancestor::div[1]//span[@class='slds-checkbox_faux']");
	public final By confermaButton = By.xpath("//div[@data-aura-class='cITA_IFM_LCP500_ManageContactData']//button[text()='Conferma']");
	public final By esciButton = By.xpath("//button[text()='Esci']");
	public final By confermaEsciAppuntamento = By.xpath("//button[text()='Sì']");
	
	public void press(By oggetto) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void presaAppuntamento(String sceltaAppuntamento) throws Exception{
		String sceltaAppuntamentoxpath = this.sceltaAppuntamento.replaceAll("@SCELTA@", sceltaAppuntamento);
		util.objectManager(By.xpath(sceltaAppuntamentoxpath), util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void inserimentoDatiAppuntamento(String nome,String cognome,String telefono) throws Exception{
		
		util.objectManager(nomeAppuntamento,util.sendKeys,nome);
		TimeUnit.SECONDS.sleep(2);
		util.objectManager(cognomeAppuntamento,util.sendKeys,cognome);
		TimeUnit.SECONDS.sleep(2);
		util.objectManager(telefonoAppuntamento,util.sendKeys,telefono);
//		TimeUnit.SECONDS.sleep(2);
//		util.objectManager(setInformativa,util.click);
		spinner.checkSpinners();
	}


//	public void selezionaEsecuzioneAnticipata(String valore) throws Exception{
//		String esecuzioneAnticipataXpath = this.esecuzioneAnticipataRadioButton;
//		if(valore.equalsIgnoreCase("SI")) esecuzioneAnticipataXpath = esecuzioneAnticipataXpath.replaceAll("@SCELTA@", "SI");
//		else if(valore.equalsIgnoreCase("NO")) esecuzioneAnticipataXpath = esecuzioneAnticipataXpath.replaceAll("@SCELTA@", "NO");
//		else throw new Exception("Il valore da selezionare scelto per esecuzione anticipata e' diverso da SI/NO. Verificare dati di input");
//		press(By.xpath(esecuzioneAnticipataXpath));
//	}

}
