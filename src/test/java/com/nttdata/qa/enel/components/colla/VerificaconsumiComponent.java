package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class VerificaconsumiComponent {

	WebDriver driver;
	SeleniumUtilities util;
	
	public String pageUrl = "https://www-coll1.enel.it/it/verifica-consumi-fatturati?zoneid=offerta-energia-per-condominio-tile_large";
	public By pageTitle = By.xpath("//h1[text()='Verifica consumi']");
	public By pageTitle1 = By.xpath("//section[@class='form-container active-bills form-data retrieve-data']//h3[text()='Verifica Consumi Fatturati']");
	public By richiestadiVerificaConsumiFatturati = By.xpath("//h3[contains(text(),'Richiesta di')]");
	public By richiestadiVerificaConsumiFatturatiSubText = By.xpath("//h4[contains(text(),'Compila il modulo')]");
	public By amministratorediCondominio = By.xpath("//input[@id='amm_condominio']");
	public By partitaIva = By.xpath("//input[@id='partita_iva']");
	public By codiceFiscaleAlfanumericoNumerico = By.xpath("//input[@id='codice_fiscale']");
	public By telefono = By.xpath("//input[@id='telefono']");
	public By email = By.xpath("//input[@id='email']");
	public By letturaDellaPrimaFornitura = By.xpath("//h3[text()='LETTURA DELLA PRIMA FORNITURA']");
	public By letturaDellaPrimaFornituraSubText = By.xpath("//h4[contains(text(),'Si richiede')]");
	public By denominazioneCondominio = By.xpath("//input[@id='den_condominio']");
	public By numeroCliente = By.xpath("//input[@id='num_cliente']");
	public By Pdr = By.xpath("//input[@id='Pdr_input_id']");
	public By misuratore = By.xpath("//input[@id='Mis_input_id']");
	public By letturaCorrettore = By.xpath("//input[@id='lettcorr_input_id']");
	public By tipoFornitura = By.xpath("//span[@data-val='Gas']");
	public By altraFurnitura = By.xpath("//button[@data-showsection='#section1']");
	public By letturaDellaSecondaFornitura = By.xpath("//h3[text()='LETTURA DELLA SECONDA FORNITURA']");
	public By denominazioneCondominioSecondaFornitura = By.xpath("//input[@id='den_condominio_1']");
	public By numeroClienteSecondaFornitura = By.xpath("//input[@id='num_cliente_1']");
	public By PdrSecondaFornitura = By.xpath("//input[@id='Pdr_input_id_1']");
	public By misuratoreSecondaFornitura = By.xpath("//input[@id='Mis_input_id_1']");
	public By letturaCorrettoreSecondaFornitura = By.xpath("//input[@id='lettcorr_input_id_1']");
	public By tipoFornituraSecondaFornitura = By.xpath("//span[@data-val='Gas_1']");
	public By altraFurnituraSecondaFornitura = By.xpath("//button[@data-showsection='#section2']");
	public By letturaDellaTerzaFornitura = By.xpath("//h3[text()='LETTURA DELLA TERZA FORNITURA']");
	public By denominazioneCondominioTerzaFornitura = By.xpath("//input[@id='den_condominio_2']");
	public By numeroClienteTerzaFornitura = By.xpath("//input[@id='num_cliente_2']");
	public By PdrTerzaFornitura = By.xpath("//input[@id='Pdr_input_id_2']");
	public By misuratoreTerzaFornitura = By.xpath("//input[@id='Mis_input_id_2']");
	public By letturaCorrettoreTerzaFornitura = By.xpath("//input[@id='lettcorr_input_id_2']");
	public By tipoFornituraTerzaFornitura = By.xpath("//span[@data-val='Gas_2']");
	public By eliminaTerzaFornitura = By.xpath("//button[@data-removesection='#section2']");
	public By eliminaSecondaFornitura = By.xpath("//button[@data-removesection='#section1']");
	public By tipoFornituraDropDownSF = By.xpath("//span[@id='tipologia_fornitura_1SelectBoxIt']");
	public By electricaSF = By.xpath("//li[@data-val='Elettrica_1']//a");
	public By podSecondaFornitura = By.xpath("//input[@id='pod_input_id_1']");
	public By letturaA3SecondaFornitura = By.xpath("//input[@id='letta3_input_id_1']");
	public By letturaA2SecondaFornitura = By.xpath("//input[@id='letta2_input_id_1']");
	public By letturaA1SecondaFornitura = By.xpath("//input[@id='letta1_input_id_1']");
	public By informativaPolicyText = By.xpath("//label[text()='Informativa privacy ']//following::textarea");
	public By invia = By.xpath("//button[text()='INVIA']");
	public By amministratorediCondominioError = By.xpath("//input[@id='amm_condominio']//following::span[1][text()='Campo obbligatorio']");
	public By telefonoError = By.xpath("//input[@id='telefono']//following::span[1][text()='Campo obbligatorio']");
	public By emailError = By.xpath("//input[@id='email']//following::span[1][text()='Campo obbligatorio']");
	public By denominazioneCondominioError = By.xpath("//input[@id='den_condominio']//following::span[1][text()='Campo obbligatorio']");
	public By numeroClienteError = By.xpath("//input[@id='num_cliente']//following::span[1][text()='Campo obbligatorio']");
	public By PdrError = By.xpath("//input[@id='Pdr_input_id']//following::span[1][text()='Campo obbligatorio']");
	public By misuratoreError = By.xpath("//input[@id='Mis_input_id']//following::span[1][text()='Campo obbligatorio']");
	public By partitaIvaError = By.xpath("//input[@id='partita_iva']//following::span[1][contains(text(),'Compilare Partita')]");
	public By codiceFiscaleAlfanumericoNumericoError = By.xpath("//input[@id='codice_fiscale']//following::span[1][contains(text(),'Compilare Partita')]");
	
	public By invalidEmailError = By.xpath("//span[text()='Email non valida']");
	public By invalidPartitaIvaError = By.xpath("//span[text()='Numero non valido']");
	public By invalidTelefonoError = By.xpath("//input[@id='telefono']/following::span[text()='Numero non valido']");
	public By invalidNumeroClientiError = By.xpath("//input[@id='num_cliente']/following::span[text()='Numero non valido']");
	public By invalidmisuratoreError = By.xpath("//input[@id='Mis_input_id']/following::span[text()='Numero non valido']");
	public By invalidNumeroClientiSFError = By.xpath("//input[@id='num_cliente_1']/following::span[text()='Numero non valido']");
	public By invalidPODSFError = By.xpath("//input[@id='pod_input_id_1']/following::span[text()='POD non valido']");
	public By invalidlettura3SFError = By.xpath("//input[@id='letta3_input_id_1']/following::span[1][text()='Numero non valido']");
	public By invalidlettura2SFError = By.xpath("//input[@id='letta2_input_id_1']/following::span[1][text()='Numero non valido']");
	public By invalidlettura1SFError = By.xpath("//input[@id='letta1_input_id_1']/following::span[1][text()='Numero non valido']");
	public By pod = By.xpath("//input[@id='pod_input_id']");
	public By tipoFornituraDropDown = By.xpath("//span[@id='tipologia_fornituraSelectBoxItText']");
	public By electrica = By.xpath("//li[@data-val='Elettrica']//a");
	public By letturaA3 = By.xpath("//input[@id='letta3_input_id']");
	public By letturaA2 = By.xpath("//input[@id='letta2_input_id']");
	public By letturaA1 = By.xpath("//input[@id='letta1_input_id']");
	public By accetto = By.xpath("//label[@for='form_privacy_accept']");
	
	public By verificaConsumiFatturati = By.xpath("//section[@id='step2']//h3[text()='Verifica Consumi Fatturati']");
	public By verificaConsumiFatturatiSubText = By.xpath("//section[@id='step2']//p");
	public By riepilogoDatiCliente = By.xpath("//h5[text()='Riepilogo dati cliente']");

	public By amministratorediCondominioIp = By.xpath("//span[text()='GIOCONDA BALZO']");
	public By partitaIvaIp = By.xpath("//span[text()='90097060736']");
	public By cfIp = By.xpath("//span[text()='BLZGND65A46L049K']");
	public By telefonoIp = By.xpath("//span[contains(text(),'333323231')]");
	public By emailIp = By.xpath("//span[text()='raffaellaquomo@gmail.com']");
	public By denominazioneCondominioIp = By.xpath("//span[text()='CONDOMINIO PRESSO CONDOMINIO VIA BRUNELL']");
	public By numeroClienteIp = By.xpath("//span[text()='777825292']");
	public By podIp = By.xpath("//span[text()='IT001E74380467']");
	public By letturaA3Ip = By.xpath("//label[text()='LetturaA3']/following::span[1]");
	public By letturaA2Ip = By.xpath("//label[text()='LetturaA2']/following::span[1]");
	public By letturaA1Ip = By.xpath("//label[text()='LetturaA1']/following::span[1]");
	public By confirma = By.xpath("//button[text()='Conferma']");
	public By confirmaText = By.xpath("//section[@id='step3']//div[@class='text-center']");
	
	public VerificaconsumiComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);		
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void enterInputParameters(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
	 }
	 public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	 
	 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. Text is not matching with the expected value");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void verifyFieldDisplay(By checkElement) throws Exception {
			Boolean isPresent = driver.findElements(checkElement).size() > 0;
			if(isPresent.equals("true"))
				throw new Exception(checkElement+"Field is displaying");			
		}
		
		public void changeDropDownValue(By dropdown,By input) throws Exception {
	         util.scrollToElement(driver.findElement(dropdown));
	            clickComponent(dropdown);
	            clickComponent(input);          
	     }
		
		public void checkForData(By checkObject, String property) throws Exception {				
			String actualValue = driver.findElement(checkObject).getText();			
			if(! property.equalsIgnoreCase(actualValue))
				throw new Exception( actualValue+" Displaying Data is not matching with the Test Data "+property);			
		}
		
		public void checkForTelefonoValue(By telefonoIp2, String property) throws Exception {
			String value = driver.findElement(telefonoIp2).getText();
			if(!value.contains(property))
				throw new Exception("Telefono value is not matching");
		}
		
		public static final String RichiestadiVerificaConsumiFatturatiSubText = "Compila il modulo sottostante per richiedere la verifica dei consumi relativi al condominio o ai condomini da te amministrati.";
		public static final String LetturaDellaPrimaFornituraSubText = "Si richiede la verifica dei consumi fatturati per la fornitura relativa al seguente condominio:";
		public static final String InformativaPolicyText = "La informiamo che i dati personali ivi indicati sono raccolti e trattati per dare seguito alla sua segnalazione. L'informativa completa è disponibile sul sito www-coll1.enel.it. Titolare del trattamento è Enel Energia S.p.A., con sede legale in Viale Regina Margherita 125 – 00198 Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007. Per esercitare i diritti previsti agli articoli 15-22 del GDPR è possibile inviare una comunicazione alla casella di posta dedicata: privacy.enelenergia@enel.com.A condizione che tu vi acconsenta, i tuoi dati saranno trattati:A.Consenso Marketing Enel Energia: per il compimento, da parte di Enel Energia, di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing. Tali attività potranno essere eseguite mediante l’invio di materiale pubblicitario, informativo, promozionale o di inviti, mediante modalità tradizionali (es. posta cartacea) o sistemi automatizzati di contatto (es. SMS, e-mail). B.Consenso Marketing Terzi: per il compimento di ricerche di mercato, vendite dirette, anche telefoniche, per il collocamento di prodotti o servizi, per comunicazioni commerciali o attività di marketing svolte da parte di Società del Gruppo Enel (tra cui Enel X), da società controllanti, controllate o collegate a Enel Energia, o da partner commerciali, di Enel Energia;C.Consenso Profilazione Enel Energia: per attività di profilazione basata sulle abitudini di consumo e sui dati e informazioni acquisite attraverso l’utilizzo dei prodotti o servizi utilizzati, da parte di Enel Energia";
		public static final String VerificaConsumiFatturatiSubText = "Se i dati che hai inserito sono corretti clicca sul tasto conferma.In caso negativo torna indietro per procedere alla modifica.";
		public static final String ConfirmaText = "Verifica Consumi Fatturati Richiesta effettuata con successo Grazie per aver utilizzato i nostri servizi online. La tua richiesta di informazioni è stata registrata con successo.";

		
		
}
