package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaCreazioneOffertaComponent extends BaseComponent{
	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;
	
	public By intestazioneAllaccioAttivazione = By.xpath("//h1[@title='Allaccio e Attivazione']");
	
	    public VerificaCreazioneOffertaComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}

	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
	}
	
	public String verificaCreazioneOfferta(String intestazione) throws Exception{
		By intest = By.xpath("//h1[@title='"+intestazione+"']");
		
		if(!util.verifyExistence(intest, baseTimeoutInterval)){ 
			throw new Exception("Impossibile trovare l'intestazione: "+intestazione);
		}
		int loopCounter=0;
		String numeroOfferta="";
		boolean keep=true;
		while(keep)
		{
			try {
				loopCounter++;
				Thread.currentThread().sleep(5*1000);
				numeroOfferta= driver.findElement(By.xpath("//h1[@title='"+intestazione+"']/ancestor::div[@class='slds-media__body']/h2[contains(text(),'Offerta')]")).getText();
				String labelnumeroOfferta="OFFERTA";
				numeroOfferta=numeroOfferta.substring(numeroOfferta.lastIndexOf(labelnumeroOfferta)+labelnumeroOfferta.length()+1, numeroOfferta.length());
				keep=false;
				break;
			}
			catch(StringIndexOutOfBoundsException e)
			{
				if(loopCounter>12)
				{
					throw e;
				}
			}
		}
		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "N offerta:"+numeroOfferta);
		return numeroOfferta;
		
	}
	
	public String verificaCreazioneQuote(String intestazione) throws Exception{
		By intest = By.xpath("//span[contains(text(),'"+intestazione+"')]");
		
		if(!util.verifyExistence(intest, baseTimeoutInterval)){ 
			throw new Exception("Impossibile trovare l'intestazione: "+intestazione);
	}
		Thread.currentThread().sleep(5000);
		String numeroOfferta= driver.findElement(By.xpath("//span[contains(text(),'"+intestazione+"')]/ancestor::div[@class='highlights slds-clearfix slds-page-header slds-page-header_record-home']//p[contains(text(),'Quote')]/ancestor::force-highlights-details-item//slot/lightning-formatted-text")).getText();
		String labelnumeroOfferta="OFFERTA";
		//numeroOfferta=numeroOfferta.substring(numeroOfferta.lastIndexOf(labelnumeroOfferta)+labelnumeroOfferta.length()+1, numeroOfferta.length());
		//System.out.println("N richiesta:"+numerorichiesta);
		Logger.getLogger("").log(Level.INFO, "N offerta:"+numeroOfferta);
		return numeroOfferta;
		
	}
	
	
	
	
	
	
			 
}
