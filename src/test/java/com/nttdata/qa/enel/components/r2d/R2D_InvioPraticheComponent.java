package com.nttdata.qa.enel.components.r2d;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.Properties;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class R2D_InvioPraticheComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;


	public By buttonCarica=By.xpath("//input[@type='image']");
	public By buttonCerca=By.xpath("//input[contains(@src,'cerca')]");
	public By buttonConferma=By.xpath("//img[contains(@src,'conferma')] | //*[contains(text(),'Conferma')]");
	public By buttonConferma2=By.xpath("//a[contains(text(),'Conferma')]");
	public By buttonInviaTutto=By.xpath("//a[contains(text(),'Invia Tutto')]");
	public By linkElencoPraticheDaInviare = By.xpath("//span[contains(text(),'Elenco pratiche PS')]");
	public By esitoCaricamento=new By.ByXPath("//div[contains(text(),'Pratiche inviate')]");
	public By esitoInvioPraticheSWA=new By.ByXPath("//div[contains(text(),'Operazione conclusa con successo! Le pratiche sono state prese in carico dal sistema')]");


	public R2D_InvioPraticheComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public void selezionaDistributore(String distributore) throws Exception{
		By	rigadistributore=new By.ByXPath("//table//td[contains(text(),'"+distributore+"')]");
		By paginaSuccessiva=new By.ByXPath("//a[contains(text(),'successiva')]");
		boolean trovato=false;
		while(!trovato){
			try{
				if(util.exists(rigadistributore, 7)){
					util.objectManager(rigadistributore,util.scrollAndClick);
					trovato=true;
				}
				else{
					util.objectManager(paginaSuccessiva,util.scrollAndClick);
					TimeUnit.SECONDS.sleep(3);
				}
			}
			catch(Exception e){
				throw new Exception("Distrubutore non trovato.Distributore da cercare:"+distributore);
			}
		}
		TimeUnit.SECONDS.sleep(3);
	}

	public void selezionaDistributoreAcquirenteUnico(String distributore) throws Exception{
		By	rigacheckbuttondistributore=new By.ByXPath("//table//td[contains(text(),'"+distributore+"')]/parent::tr/td[1]/span");
		By paginaSuccessiva=new By.ByXPath("//a[contains(text(),'successiva')]");
		boolean trovato=false;
		while(!trovato){
			try{
				if(util.exists(rigacheckbuttondistributore, 7)){
					util.objectManager(rigacheckbuttondistributore,util.scrollAndClick);
					trovato=true;
				}
				else{
					util.objectManager(paginaSuccessiva,util.scrollAndClick);
					TimeUnit.SECONDS.sleep(3);
				}
			}
			catch(Exception e){
				throw new Exception("Distrubutore non trovato.Distributore da cercare:"+distributore);
			}
		}
		TimeUnit.SECONDS.sleep(3);

		//Click su button conferma
		util.objectManager(buttonConferma2,util.scrollAndClick);

		TimeUnit.SECONDS.sleep(10);
		//Conferma pop-up
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);

	}

	public void selezionaDistributoreSWA(String distributore) throws Exception{
		By	rigacheckbuttondistributore=new By.ByXPath("//table//td[contains(text(),'"+distributore+"')]/parent::tr/td[1]/span");
		By paginaSuccessiva=new By.ByXPath("//a[contains(text(),'successiva')]");
		boolean trovato=false;
		while(!trovato){
			try{
				if(util.exists(rigacheckbuttondistributore, 7)){
					util.objectManager(rigacheckbuttondistributore,util.scrollAndClick);
					trovato=true;
				}
				else{
					util.objectManager(paginaSuccessiva,util.scrollAndClick);
					TimeUnit.SECONDS.sleep(3);
				}
			}
			catch(Exception e){
				throw new Exception("Distrubutore non trovato.Distributore da cercare:"+distributore);
			}
		}
		TimeUnit.SECONDS.sleep(3);

		//Click su button invia tutto
		util.objectManager(buttonInviaTutto,util.scrollAndClick);

		TimeUnit.SECONDS.sleep(10);
		//Conferma pop-up
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);

		//Verifica invio pratica
		if(!util.exists(esitoInvioPraticheSWA, 30)){
			throw new Exception("Messaggio 'Operazione conclusa con successo! Le pratiche sono state prese in carico dal sistema' non visualizzato. Possibili problemi nell'invio delle pratiche. Verificare");
		}

	}

	public void selezionaTipoLavorazioneCoda(String processo) throws Exception{
//		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/..//input[@type='image']");
		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/../td[4]/input");
//		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'Allaccio')]/ancestor::table[1]/tbody//td[contains(text(),'Richiesta PN1')] /../td[4]/input");
		try{
//			driver.findElement(tastoAzioneProcesso).click();
//			System.out.println("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/..//input[@type='image']");
			TimeUnit.SECONDS.sleep(10);
			JavascriptExecutor js = (JavascriptExecutor)driver;	
			WebElement elem=util.waitAndGetElement(tastoAzioneProcesso);
			js.executeScript("arguments[0].click();", elem);
		}
		catch(Exception e){
			throw new Exception("Tipo processo non trovato nella coda ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA.Processo da cercare:"+processo);
		}
		TimeUnit.SECONDS.sleep(10);

		//Conferma pop-up
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaTipoLavorazioneCodaPN1(String processo) throws Exception{
//		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/..//input[@type='image']");
//		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/../td[4]/input");
		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'Allaccio')]/ancestor::table[1]/tbody//td[contains(text(),'Richiesta PN1')] /../td[4]/input");
		try{
//			driver.findElement(tastoAzioneProcesso).click();
//			System.out.println("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/..//input[@type='image']");
			TimeUnit.SECONDS.sleep(10);
			JavascriptExecutor js = (JavascriptExecutor)driver;	
			WebElement elem=util.waitAndGetElement(tastoAzioneProcesso);
			js.executeScript("arguments[0].click();", elem);
		}
		catch(Exception e){
			throw new Exception("Tipo processo non trovato nella coda ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA.Processo da cercare:"+processo);
		}
		TimeUnit.SECONDS.sleep(10);

		//Conferma pop-up
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}
	public void selezionaTipoLavorazioneCodaDoppia(String processo, String sottoProcesso) throws Exception{
//		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/../td[4]");
		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]/ancestor::tr/td[contains(text(),'"+sottoProcesso+"')]/../td[4]/input");
		try{
//			driver.findElement(tastoAzioneProcesso).click();
			TimeUnit.SECONDS.sleep(10);
			JavascriptExecutor js = (JavascriptExecutor)driver;	
			WebElement elem=util.waitAndGetElement(tastoAzioneProcesso);
			js.executeScript("arguments[0].click();", elem);
		}
		catch(Exception e){
			throw new Exception("Tipo processo non trovato nella coda ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA.Processo da cercare:"+processo);
		}
		TimeUnit.SECONDS.sleep(10);

		//Conferma pop-up
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}
	
	
	public void selezionaTipoLavorazioneCodaAcquirenteUnico(String processo) throws Exception{
		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA')]/ancestor::table[1]/tbody//td[2][contains(text(),'"+processo+"')]");
		try{
			util.objectManager(tastoAzioneProcesso,util.scrollAndClick);
		}
		catch(Exception e){
			throw new Exception("Tipo processo non trovato nella coda ELENCO PRATICHE PER TIPI LAVORO DI RICHIESTA.Processo da cercare:"+processo);
		}
		TimeUnit.SECONDS.sleep(10);
	}

	public void selezionaTipoLavorazioneSWA(String processo) throws Exception{
		By	tastoAzioneProcesso=new By.ByXPath("//table/tbody/tr/td[contains(text(),'ELENCO PRATICHE PER TIPO LAVORO')]/ancestor::table[1]/tbody//td[contains(text(),'"+processo+"')]");
		try{
			util.objectManager(tastoAzioneProcesso,util.scrollAndClick);
		}
		catch(Exception e){
			throw new Exception("Tipo processo non trovato nella coda ELENCO PRATICHE PER TIPO LAVORO.Processo da cercare:"+processo);
		}
		TimeUnit.SECONDS.sleep(10);
	}

	public void invioPratiche() throws Exception{
		//Click link pratiche da inviare
		try {
		util.objectManager(linkElencoPraticheDaInviare, util.scrollAndClick);
		}catch (Exception e) {
			//non trovo la voce perchè mi trovo già nella pagina
		}
		TimeUnit.SECONDS.sleep(10);
		//Click su button Conferma
		util.objectManager(buttonConferma, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		//Conferma pop-up
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
		//Verifica avventuta caricamento pratica
		if(!util.exists(esitoCaricamento, 30)){
			throw new Exception("Messaggio 'Pratiche invate' non visualizzato. Possibili problemi nell'invio delle pratiche. Verificare");
		}

	}

	//	public void selezionaDataVuotaSwitchAttivoInAmbito() throws Exception{
	//		By paginaSuccessiva=new By.ByXPath("//a[contains(@title,'successiva')]");
	//		By tabella=new By.ByXPath("//td[contains(text(),'ELENCO PRATICHE A PORTALE PER DATA INVIO SWA')]/ancestor::table[1]//tr/td[1]");
	//		//Scorro tutte le righe per trovare quella con descrizione "In ambito - Switch Attivo"
	//
	//		boolean trovato=false;
	//		while(!trovato){
	//			List<WebElement> righetabella=driver.findElements(tabella);
	//			String testoRiga="";
	//			int i=0;
	//			try{
	//				for(i=0;i<righetabella.size()-1;i++){
	//					testoRiga=righetabella.get(i).getText();
	//					if(testoRiga.compareToIgnoreCase("In Ambito - Switch Attivo (SE1)")==0){
	//						break;
	//					}
	//
	//				}
	//			}	catch(Exception e){
	//				throw new Exception("Non è stata la voce 'In Ambito - Switch Attivo (SE1)'");
	//			}	
	//			//Scorro da indice fino a fine tabella
	//			try{
	//				int j=0;
	//				for(j=i+1;j<righetabella.size()-1;j++){
	//					testoRiga=righetabella.get(j).getText();
	//					if(testoRiga.compareTo("")==0){
	//						trovato=true;
	//						//Click su elemento
	//						righetabella.get(j).click();
	//						break;
	//					}
	//				}
	//				//se non trovato, vado alla pagina successiva
	//				if (!trovato){
	//					util.objectManager(paginaSuccessiva,util.scrollAndClick);
	//					TimeUnit.SECONDS.sleep(3);
	//				}
	//
	//			}
	//			catch(Exception e){
	//				throw new Exception("Non è stata trovata nessuna data vuota");
	//			}	
	//			TimeUnit.SECONDS.sleep(3);
	//		}
	//	}

	public void selezionaDataInvioSwitchAttivoInAmbito(String data) throws Exception{

		By paginaIniziale=new By.ByXPath("//a[contains(@title,'pagina 1')]");
		By paginaSuccessiva=new By.ByXPath("//a[contains(@title,'successiva')]");
		By tabella=new By.ByXPath("//td[contains(text(),'ELENCO PRATICHE A PORTALE PER DATA INVIO SWA')]/ancestor::table[1]//tr/td[1]");

		//Ricerca data invio nella tabella, se non presente si prova a cercare riga con data vuota

		//Ricerca data invio
		//Scorro tutte le righe per trovare quella con descrizione "In ambito - Switch Attivo"
		boolean datatrovata=false;
		boolean finetabella=false;
		while(!datatrovata && !finetabella){
			List<WebElement> righetabella=driver.findElements(tabella);
			String testoRiga="";
			int i=0;
			//scorro per selezionare data invio switch
			try{
				for(i=0;i<righetabella.size()-1;i++){
					testoRiga=righetabella.get(i).getText();
					if(testoRiga.compareToIgnoreCase("In Ambito - Switch Attivo (SE1)")==0){
						break;
					}

				}
			}	catch(Exception e){
				throw new Exception("Non è stata la voce 'In Ambito - Switch Attivo (SE1)'");
			}	
			//Scorro da indice fino a fine tabella
			int j=0;
			for(j=i+1;j<righetabella.size()-1;j++){
				testoRiga=righetabella.get(j).getText();
				if(testoRiga.compareTo(data)==0){
					datatrovata=true;
					//Click su elemento
					righetabella.get(j).click();
					break;
				}
				if(testoRiga.compareTo("Richiesta RN1 (RC1)")==0){ 
					finetabella=true;
					break;
				}
			}
			//se non trovato, vado alla pagina successiva
			if (!datatrovata && !finetabella){
				util.objectManager(paginaSuccessiva,util.scrollAndClick);
				TimeUnit.SECONDS.sleep(3);
			}

			TimeUnit.SECONDS.sleep(3);
		}
		//Se la tabella è finita e la data non è stata trovata,torno all'inizio e provo a cercare data vuota
		if (finetabella){
			util.objectManager(paginaIniziale,util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);

			//ricerca eventuale data vuota
			//Scorro tutte le righe per trovare quella con descrizione "In ambito - Switch Attivo"
			boolean datavuotatrovata=false;
			finetabella=false;
			while(!datavuotatrovata && !finetabella){
				List<WebElement> righetabella=driver.findElements(tabella);
				String testoRiga="";
				int i=0;
				//scorro per selezionare data invio switch
				try{
					for(i=0;i<righetabella.size()-1;i++){
						testoRiga=righetabella.get(i).getText();
						if(testoRiga.compareToIgnoreCase("In Ambito - Switch Attivo (SE1)")==0){
							break;
						}

					}
				}	catch(Exception e){
					throw new Exception("Non è stata la voce 'In Ambito - Switch Attivo (SE1)'");
				}	
				//Scorro da indice fino a fine tabella
				try{
					int j=0;
					for(j=i+1;j<righetabella.size()-1;j++){
						testoRiga=righetabella.get(j).getText();
						if(testoRiga.compareTo("")==0){
							datavuotatrovata=true;
							//Click su elemento
							righetabella.get(j).click();
							break;
						}
					}
					//se non trovato, vado alla pagina successiva
					if (!datavuotatrovata){
						util.objectManager(paginaSuccessiva,util.scrollAndClick);
						TimeUnit.SECONDS.sleep(3);
					}
				}
				catch(Exception e){
					throw new Exception("Non è stata trovata la data invio SWA o una data vuota");
				}	
				TimeUnit.SECONDS.sleep(3);
			}	
		}
	}
	
	
	public void selezionaRichiestaDisdettaStandard(String data) throws Exception{

		By paginaIniziale=new By.ByXPath("//a[contains(@title,'pagina 1')]");
		By paginaSuccessiva=new By.ByXPath("//a[contains(@title,'successiva')]");
		By righeTabellaConData=new By.ByXPath("//*[contains(text(),'Richiesta Disdetta standard')]/parent::tr//following-sibling::tr[contains(@onclick,'RC1') and contains(@onclick,'"+data+"')]");
		By righeTabellaSenzaData=new By.ByXPath("//*[contains(text(),'Richiesta Disdetta standard')]/parent::tr//following-sibling::tr[contains(@onclick,'RC1')]");

		 
		//Ricerca data invio
		//Scorro tutte le righe per trovare quella con descrizione "Richiesta Disdetta standard (RC1)"
		boolean datatrovata=false;
		boolean finetabella=false;
		List<WebElement> righetabellaConData=driver.findElements(righeTabellaConData);
		if(righetabellaConData.size() > 0) {
			righetabellaConData.get(0).click();
		}
		else {
			List<WebElement> righetabellaSenzaData=driver.findElements(righeTabellaSenzaData);
			if(righetabellaSenzaData.size() > 0) {
				righetabellaSenzaData.get(0).click();
				
			}
				
			else throw new Exception("Non sono state trovate richieste di disdetta standard");
			
		} 
			
	}


	//	public void selezionaDataSwitchAttivoInAmbito(String data) throws Exception{
	//		By paginaSuccessiva=new By.ByXPath("//a[contains(@title,'successiva')]");
	//		By tabella=new By.ByXPath("//td[contains(text(),'ELENCO PRATICHE A PORTALE PER DATA SWITCH')]/ancestor::table[1]//tr/td[1]");
	//		//Scorro tutte le righe per trovare quella con descrizione "In ambito - Switch Attivo"
	//		boolean trovato=false;
	//		while(!trovato){
	//			List<WebElement> righetabella=driver.findElements(tabella);
	//			String testoRiga="";
	//			int i=0;
	//			try{
	//				for(i=0;i<righetabella.size()-1;i++){
	//					testoRiga=righetabella.get(i).getText();
	//					if(testoRiga.compareToIgnoreCase("In Ambito - Switch Attivo (SE1)")==0){
	//						System.out.println("Trovato");
	//						break;
	//					}
	//
	//				}
	//			}	catch(Exception e){
	//				throw new Exception("Non è stata la voce 'In Ambito - Switch Attivo (SE1)'");
	//			}	
	//			System.out.println("Indice:"+i);
	//			//Scorro da indice fino a fine tabella
	//			try{
	//				int j=0;
	//				for(j=i+1;j<righetabella.size()-1;j++){
	//					testoRiga=righetabella.get(j).getText();
	//					if(testoRiga.compareTo(data)==0){
	//						trovato=true;
	//						//Click su elemento
	//						righetabella.get(j).click();
	//						break;
	//					}
	//				}
	//				//se non trovato, vado alla pagina successiva
	//				if (!trovato){
	//					util.objectManager(paginaSuccessiva,util.scrollAndClick);
	//					TimeUnit.SECONDS.sleep(3);
	//				}
	//
	//			}
	//			catch(Exception e){
	//				throw new Exception("Non è stata trovata nessuna data vuota");
	//			}	
	//			TimeUnit.SECONDS.sleep(3);
	//		}
	//	}	

	public void selezionaDataSwitchAttivoInAmbito(String data) throws Exception{
		By paginaSuccessiva=new By.ByXPath("//a[contains(@title,'successiva')]");
		By tabella=new By.ByXPath("//td[contains(text(),'ELENCO PRATICHE A PORTALE PER DATA SWITCH')]/ancestor::table[1]//tr/td[1]");
		//Scorro tutte le righe per trovare quella con descrizione "In ambito - Switch Attivo"
		boolean trovato=false;
		while(!trovato){
			List<WebElement> righetabella=driver.findElements(tabella);
			String testoRiga="";
			int i=0;
			try{
				for(i=0;i<righetabella.size()-1;i++){
					testoRiga=righetabella.get(i).getText();
					if(testoRiga.compareToIgnoreCase("In Ambito - Switch Attivo (SE1)")==0){
						System.out.println("Trovato");
						break;
					}

				}
			}	catch(Exception e){
				throw new Exception("Non è stata la voce 'In Ambito - Switch Attivo (SE1)'");
			}	
			System.out.println("Indice:"+i);
			//Scorro da indice fino a fine tabella
			try{
				int j=0;
				for(j=i+1;j<righetabella.size()-1;j++){
					testoRiga=righetabella.get(j).getText();
					if(testoRiga.compareTo(data)==0){
						trovato=true;
						//Click su elemento
						righetabella.get(j).click();
						break;
					}
				}
				//se non trovato, vado alla pagina successiva
				if (!trovato){
					util.objectManager(paginaSuccessiva,util.scrollAndClick);
					TimeUnit.SECONDS.sleep(3);
				}

			}
			catch(Exception e){
				throw new Exception("Non è stata trovata nessuna data vuota");
			}	
			TimeUnit.SECONDS.sleep(3);
		}
	}	

}
