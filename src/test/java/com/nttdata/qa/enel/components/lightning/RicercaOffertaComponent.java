package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class RicercaOffertaComponent {
	
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	  public RicercaOffertaComponent(WebDriver driver) {
		    this.driver = driver;
		    PageFactory.initElements(driver, this);
		    util=new SeleniumUtilities(driver);
		    spinner = new SpinnerManager(driver);
		  }
	  

//	public final By CampoRicerca = By.xpath("//input[contains(@title,'Cerca')]");
	public final By CampoRicerca = By.xpath("//*[@type='search']");
	public final By CampoRicerca2 = By.xpath("(//*[@type='search'])[2]");
	public final String Risultato = "//span[@title='***']/..";
	public final By PassaLighting = By.xpath("//a[@class='switch-to-lightning']");
	public final By clickOfferta = By.xpath("//div[text()='Offerta']/../a");
	public final By clickQualityCheck = By.xpath("//button[@id='qcConfirm']");
	public final By NomeBarra = By.xpath("//div[@class='appName slds-context-bar__label-action slds-context-bar__app-name']/span");
	public final By searchBy = By.xpath("//input[@name='NE__Quote__c-search-input']");
	public final By statoCVP = By.xpath("//span[text()='Stato CVP']/ancestor::div[contains(@class,'slds-form-element')]//span[contains(@class,'field-value')]//lightning-formatted-text");
	public final By buttonCerca = By.xpath("//button[@aria-label='Cerca']");
	public final By inputCerca = By.xpath("//input[@placeholder='Cerca...']");
	public final By inputCerca1 = By.xpath("//input[@placeholder='Cerca Interazioni e altro...']");
	public final By inputCerca2 = By.xpath("//input[@placeholder='Cerca Richieste e altro...']");
	public final String Risultato2 = "//*[@class='data-match']";
	
	
	public void inserisciRichiestaOld(String chiaveRicerca) throws Exception{
		driver.switchTo().defaultContent();
		driver.findElement(CampoRicerca2).clear();
		driver.findElement(CampoRicerca2).sendKeys(chiaveRicerca);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(CampoRicerca2).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(5);
	}
	
	public void inserisciRichiesta(String numeroRichiesta) throws Exception{
		driver.switchTo().defaultContent();
		//driver.findElement(buttonCerca).click(); //R3 FR il click su bottone cerca non è più necessario
		By inputCerca=By.xpath("//input[@placeholder='Cerca Richieste e altro...']");
		driver.findElement(inputCerca).clear();
		driver.findElement(inputCerca).sendKeys(numeroRichiesta);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(inputCerca).clear();
		driver.findElement(inputCerca).sendKeys(numeroRichiesta);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(inputCerca).click();
	}
	
	public void inserisciRichiestaRVC(String numeroRichiesta) throws Exception{

		driver.switchTo().defaultContent();
//		driver.findElement(buttonCerca).click();// B.G.05/10/2021 Commentato in quanto lo script andava in errore non trovando il button Cerca
		
		if(util.exists(inputCerca, 3))
        {driver.findElement(inputCerca).clear();
        //driver.findElement(CampoRicerca).click();
        
        driver.findElement(inputCerca).sendKeys(numeroRichiesta);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(inputCerca).clear();
		driver.findElement(inputCerca).sendKeys(numeroRichiesta);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(inputCerca).click();}
		else
		{   
			
			if(util.exists(inputCerca1, 3))
			{driver.findElement(inputCerca1).clear();
	        driver.findElement(inputCerca1).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			driver.findElement(inputCerca1).clear();
			driver.findElement(inputCerca1).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			driver.findElement(inputCerca1).click();}
			
		}
	}
	public void inserisciRichiesta2(String numeroRichiesta) throws Exception{

		driver.switchTo().defaultContent();
		
		if(util.exists(inputCerca1, 3))
        {
			driver.findElement(inputCerca1).clear();
	        driver.findElement(inputCerca1).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			driver.findElement(inputCerca1).clear();
	        driver.findElement(inputCerca1).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			//driver.findElement(inputCerca1).sendKeys(Keys.ENTER);
        }
		else
		{  
			if(util.exists(inputCerca, 3))
			{
			driver.findElement(inputCerca).clear();
	        driver.findElement(inputCerca).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			driver.findElement(inputCerca).clear();
	        driver.findElement(inputCerca).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			//driver.findElement(inputCerca).sendKeys(Keys.ENTER);
			}
		}
	}
	
	public void inserisciRichiesta3(String numeroRichiesta) throws Exception{

		driver.switchTo().defaultContent();
		
		if(util.exists(inputCerca2, 3))
        {
			driver.findElement(inputCerca2).clear();
	        driver.findElement(inputCerca2).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			driver.findElement(inputCerca2).clear();
	        driver.findElement(inputCerca2).sendKeys(numeroRichiesta);
			TimeUnit.SECONDS.sleep(5);
			//driver.findElement(inputCerca1).sendKeys(Keys.ENTER);
        }
		else
		{  
			if(util.exists(inputCerca, 3))
			{
				driver.findElement(inputCerca).clear();
		        driver.findElement(inputCerca).sendKeys(numeroRichiesta);
				TimeUnit.SECONDS.sleep(5);
				driver.findElement(inputCerca).clear();
		        driver.findElement(inputCerca).sendKeys(numeroRichiesta);
				TimeUnit.SECONDS.sleep(5);
				//driver.findElement(inputCerca).sendKeys(Keys.ENTER);
			}
		}
	}
	
	public void cliccaRisultato(String numeroRichiesta) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		//potrebbe non visualizzare i risultati:
		try{
			press(By.xpath(Risultato.replace("***", numeroRichiesta)));
		}catch(Exception e){
			this.inserisciRichiesta(numeroRichiesta);
			press(By.xpath(Risultato.replace("***", numeroRichiesta)));
		}
		TimeUnit.SECONDS.sleep(5);
		
		
		
	}
	
	public void cliccaSwitch() throws Exception{
		
		try {
			

			if(util.exists(PassaLighting,5)) {
			
		
			press(PassaLighting);
			
			}
			
			if(!driver.findElement(NomeBarra).getText().contentEquals("Lightning Service Console")) {
				driver.findElement(By.xpath("//button[text()='Visualizza tutto']")).click();
				TimeUnit.SECONDS.sleep(3);
				driver.findElement(By.xpath("//input[@type='search' and @placeholder='Cerca nelle app o nelle voci...']")).click();
				TimeUnit.SECONDS.sleep(3);
				driver.findElement(By.xpath("//mark[text()='Lightning Service Console']")).click();
				TimeUnit.SECONDS.sleep(3);
				
			}
		}catch(Exception e) {}
		
		
	}
	
	public void selezionaOfferta() throws Exception {

		 List<WebElement> button = driver.findElements(clickOfferta);
		 JavascriptExecutor js = (JavascriptExecutor)driver;
		 for (WebElement webElement : button) {
			 TimeUnit.SECONDS.sleep(2);
			 js.executeScript("arguments[0].click();", webElement);
		}
		
		
		TimeUnit.SECONDS.sleep(5);
		
		
		//util.objectManager(clickOfferta, util.scrollToVisibility, false);
		
		
	}
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	

	
	public void searchOfferta(String offertaInput) throws Exception
	{
		//String frame = util.getFrameByIndex(0);

		TimeUnit.SECONDS.sleep(2);
		//		dropdwonButton.click();
		//		serachObject = xpathTab;
		//		util.frameManager(frame, searchObject, util.sendKeys, richiestaInput);
		//		WebElement searchObject = driver.findElement(searchBy);
		//		searchObject.sendKeys(richiestaInput);
		util.objectManager(searchBy, util.sendKeysCharByChar, offertaInput);

		TimeUnit.SECONDS.sleep(10);
		//		util.frameManager(frame, searchObject, util.sendKeys, Keys.ENTER.toString());
		//		searchObject.sendKeys(Keys.ENTER);
	//	util.objectManager(searchBy, util.sendKeysCharByChar, Keys.ENTER.toString());
		util.objectManager(By.xpath("//span[contains(text(),' Sandbox: UAT')]"), util.scrollAndClick);
		//span[contains(text(),' Sandbox: UAT')]
		TimeUnit.SECONDS.sleep(10);
        //Se il numero richiesta è formattato male con caratteri sporchi prima
		String split[]= offertaInput.trim().split(" ");
		String xPathElement = "//a[@title='"+split[split.length-1]+"']";
		By element = By.xpath(xPathElement);

		if (!util.verifyExistence(element, 30))
		{
			throw new Exception("Errore! Il codice offerta " + offertaInput + " non presente a sistema!");
		}

		//	util.frameManager(frame, element, util.scrollAndClick);
		//Istanzio oggetto link della Richiesta
		//		searchObject = driver.findElement(element);
		//		searchObject.click();
		util.objectManager(element, util.scrollAndClick);


	}
	//Restituisce false se esito CVP è vuoto, true se è popolato
	public boolean verificaEsitoCVP(By oggetto) throws Exception{
//		WebElement element = util.waitAndGetElement(oggetto);

		util.scrollToElement(driver.findElement(oggetto));
		String value=driver.findElement(oggetto).getText();
		System.out.print("Valore stato CVP:"+value);
		if(value.compareTo("")==0){
			return false;
		}
		return true;
		
	}

	public void cliccaRisultato2(String inputRicerca) throws Exception{
		TimeUnit.SECONDS.sleep(5);
		//potrebbe non visualizzare i risultati:
		try{
			press(By.xpath(Risultato2.replace("***", inputRicerca)));
		}catch(Exception e){
			this.inserisciRichiesta(inputRicerca);
			press(By.xpath(Risultato2.replace("***", inputRicerca)));
		}
		TimeUnit.SECONDS.sleep(2);
		
	}
	

	
}
