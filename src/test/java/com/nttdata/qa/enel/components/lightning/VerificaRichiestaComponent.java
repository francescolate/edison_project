package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class VerificaRichiestaComponent {

	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;

	public VerificaRichiestaComponent(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinner = new SpinnerManager(driver);
	}

	public final By navigazioneProcessoLabel = By.xpath("//span[text()='Navigazione processo']");
	public final By status = By.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//span[text()='Stato']/following::*/span");
	public final By sottostatus = By.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//span[text()='Sottostato']/following::*/span");
	public final By statoPODRichiesta = By.xpath(
	"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='POD']/following::*[1]");
	public final By elementiRichiestaSection = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]");
	public final By elementiOrdineSection = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]");
//	public final By elementiRichiestaSection = By.xpath("//*[@title='SWITCH ATTIVO']/ancestor::article//section");
//	public final By oiRichiesta = By
//			.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//ul[1]//a[contains(text(),'OI-')]");
	public final By oiRichiesta = By
	.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//ul[1]//a[contains(text(),'OI-')]");
	public final String oiRichiestaDaPod = "//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//strong[text()='#POD#']/ancestor::ul[1]//a[contains(text(),'OI-')]";
	public final String oiOrdineDaPod = "//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//strong[text()='#POD#']/ancestor::ul[1]//a[contains(text(),'OI-')]";
	//FR 27.08.2021 introdotto per VCA 
	public final String oiOrdineCessazioneVcaDaPod="//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//strong[text()='#POD#']/ancestor::ul[1]/li[strong[text()='Cessazione']]/parent::ul//a[contains(text(),'OI-')]";
	public final String oiOrdineDynamicDaPod = "//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//strong[text()='Commodity']/ancestor::ul//a[contains(text(),'OI-')]";
	//public final String oiOrdineDynamicDaPod = "//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//strong[text()='%s']/ancestor::ul[1]//a[contains(text(),'%s')]";
	//	public final By statoRichiesta = By
//			.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//ul[1]//*[text()='Stato']/following::*[1]");
	public final By statoElementoRichiesta = By
	.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato']/following::*[1]");

	public final String statoDaPod = "//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//strong[text()='#POD#']/ancestor::ul[1]//*[text()='#STATO#']/following::*[1]";
	
	//	public final By statoR2D = By.xpath(
//			"//span[text()='Navigazione processo']/ancestor::article[1]//ul[1]//*[text()='Stato R2D']/following::*[1]");
	public final By statoR2DRichiesta = By.xpath(
	"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato R2D']/following::*[1]");
//	public final By statoSAP = By.xpath(
//			"//span[text()='Navigazione processo']/ancestor::article[1]//ul[1]//*[text()='Stato SAP-ISU']/following::*[1]");
	public final By statoSAPRichiesta = By.xpath(
	"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato SAP-ISU']/following::*[1]");
//	public final By statoSEMPRE = By.xpath(
//			"//span[text()='Navigazione processo']/ancestor::article[1]//ul[1]//*[text()='Stato SEMPRE']/following::*[1]");
	public final By statoSEMPRERichiesta = By.xpath(
	"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato SEMPRE']/following::*[1]");
//	public By expandActivitySection = new By.ByXPath(
//			"//th[text()='Activity Name']/ancestor::article//header//span[contains(@title,'Attivit')]");
	public By expandActivitySection = new By.ByXPath(
			"//*[text()='Activity Name']/ancestor::article//h2//span[contains(@title,'Attivit')]");
	public By buttonNuovaAttivita = new By.ByXPath("//div[@title='Nuova Attività']");
	public By attivitaTableCompleta = new By.ByXPath("//span[@title='Activity Name']/ancestor::table");
	public By chiudiTabDocumenti = new By.ByXPath("//button[@title='Chiudi Documenti']");
	public By chiudiTabAttivita = new By.ByXPath("//button[contains(@title,'Chiudi Attivit')]");
//	public By documentiTable = new By.ByXPath("//th[text()='Document Ref']/ancestor::table");
//	public By documentiTable = new By.ByXPath("//span[text()='Document Ref']/ancestor::table");
	public By documentiTable = new By.ByXPath("(//span[text()='Document Ref']/ancestor::table)[2]");
//	public By expandDocumentSection = new By.ByXPath(
//			"//th[text()='Document Ref']/ancestor::article//header//span[@title='Documenti']");
	public By expandDocumentSection = new By.ByXPath(
			"//span[text()='Document Ref']/ancestor::article//span[@title='Documenti']");
	public By documentTableCompleta = new By.ByXPath("//span[@title='Document Ref']/ancestor::table");
	
//	public By statoRichiesta  = new By.ByXPath("//article//span[text()='Stato']/../div/span");//span[text()='Navigazione processo']/ancestor::article//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//span[contains(text(),'Stato')]/ancestor::li/strong
	public By statoRichiesta  = new By.ByXPath("//span[text()='Navigazione processo']/ancestor::article//div[contains(text(),'Elementi della Richiesta')]/ancestor::div[@class='slds-form-element']//span[contains(text(),'Stato')]/ancestor::li/strong");

	//Stato Richiesta per Pred. Presa --> rinominato: statoRichiestaPP
	public By statoRichiestaPP  = new By.ByXPath("//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Richiesta')]/ancestor::div[@class='slds-card__body']//span[contains(text(),'Stato')]/following::*[1]");

	public By dettaglioAzioneEseguita = new By.ByXPath("//span[text()='Dettaglio azione eseguita']/following::*[1]");
	public By azioneEseguita = new By.ByXPath("//span[text()='Azione eseguita']/following::*[1]");
	public By statoR2d = new By.ByXPath("//span[text()='Stati integrazione']/ancestor::div[@class='test-id__section slds-m-vertical_none slds-section has-header slds-p-bottom_medium slds-is-open']//span[text()='Stato R2D']/following::*[1]");
	public By statoConfigurationItem = new By.ByXPath("//span[text()='Informazioni']/ancestor::div[@class='slds-tabs_card']//span[text()='Stato']/following::*[1]");
	public By sottoStatoRichiesta  = new By.ByXPath("//article//span[text()='Sottostato']/../div/span");
	public String tipoRichiesta="//span[text()='Navigazione processo']/ancestor::article[1]//span[text()='Tipo']/following::*/span[text()='#']";
	public By labelCreataIl=By.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//span[text()='Creata il']");
	public By numeroRichiesta=By.xpath("//article//div[text()='Richiesta']/..//a");
	public By tipo=By.xpath("//article//span[text()='Tipo']/../div/span");
	public final By ordineStato = By.xpath("//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato']/following::*[1]");
	public final By ordineR2D = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato R2D']/following::*[1]");
	public final By ordineTipo = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Tipo']/following::*[1]");
	public final By ordinePOD = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='POD']/following::*[1]");
	public final By ordineCommodity = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Commodity']/following::*[1]");
	public final By ordinePratica = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Pratica']/following::*[1]");
	public final By ordineSapSD = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato SAP-SD']/following::*[1]");
	public final By ordineSapISU = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato SAP-ISU']/following::*[1]");
	public final By ordineSEMPRE = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Stato SEMPRE']/following::*[1]");
	public final By ordineProdotto = By.xpath(
			"//span[text()='Navigazione processo']/ancestor::article[1]//*[contains(text(),'Elementi dell') and contains(text(),'Ordine')]/ancestor::div[@class='slds-form-element']//ul[1]//*[text()='Prodotto']/following::*[1]");
		
	public By numero_contratto=By.xpath("//p[text()='Numero Contratto']/ancestor::div[1]//lightning-formatted-text | //p[text()='Numero caso']/ancestor::div[1]//lightning-formatted-text");
	
	public By richiesta=By.xpath("//div[text()='Richiesta']//ancestor::h2//a");
	public By offerta=By.xpath("//div[text()='Offerta']//ancestor::h2//a");
	
	public void verificaProcesso(String itemName) throws Exception {
		String xpathString = tipoRichiesta.replace("#", itemName);
		By item = By.xpath(xpathString);
		//System.out.println(item);
		if (!util.verifyExistence(item, 150)) 
			throw new Exception("l'oggetto con xpath " + item + " non esiste.");
		if (!util.verifyExistence(labelCreataIl, 150)) 
			throw new Exception("l'oggetto con xpath " + labelCreataIl + " non esiste.");
	}
	
	public void press(By oggetto) throws Exception {
		driver.switchTo().defaultContent();
		// util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void pressJavascript(By oggetto) throws Exception {
		WebElement element = util.waitAndGetElement(oggetto);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void attesaCaricamentoPaginaRichiesta() {
		FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150).ignoring(WebDriverException.class)
				.pollingEvery(2, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.elementToBeClickable(expandActivitySection));

	}
	
	public void verificaStatoSottostatoRichiesta(String stato,String sottostato) throws Exception{
		String state= util.waitAndGetElement(status).getText();
		String subState= util.waitAndGetElement(sottostatus).getText();
		if(!stato.toLowerCase().trim().contentEquals(state.trim().toLowerCase())) throw new Exception("Lo stato della Richiesta risulta diverso da : "+stato+". Stato riscontrato: "+state);
		if(!sottostato.toLowerCase().trim().contentEquals(subState.trim().toLowerCase())) throw new Exception("Il sottostato della Richiesta risulta diverso da : "+sottostato+". Stato riscontrato: "+subState);

	}
	
	public void verificaStatoSottostatoRichiestaGestioneMultipla(String stato_1, String stato_2, String sottostato)
			throws Exception {
		String state = util.waitAndGetElement(status).getText();
		String subState = util.waitAndGetElement(sottostatus).getText();
		if (stato_1.toLowerCase().trim().contentEquals(state.trim().toLowerCase())
				|| stato_2.toLowerCase().trim().contentEquals(state.trim().toLowerCase())) {

		} else {

			throw new Exception("Lo stato della Richiesta risulta diverso da:" + stato_1 + "/" + stato_2
					+ ". Stato riscontrato: " + state);
		}
		if (!sottostato.toLowerCase().trim().contentEquals(subState.trim().toLowerCase()))
			throw new Exception("Il sottostato della Richiesta risulta diverso da : " + sottostato
					+ ". Stato riscontrato: " + subState);

	}
	
	
	public void attesaCaricamentoSezioneCliente() {
		FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 180).ignoring(WebDriverException.class)
				.pollingEvery(1, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.presenceOfElementLocated(navigazioneProcessoLabel));

	}

	public String recuperaOIRichiesta() throws Exception {
		try {
			TimeUnit.SECONDS.sleep(3);
			String oi = util.waitAndGetElement(oiRichiesta).getText();
			return oi;
		} catch (Exception e) {
			throw new Exception("Impossibile recuperare l'OI della richiesta");
		}

	}
	
	public String recuperaOIRichiestaDaPod(String pod) throws Exception {
		try {
			String oi = util.waitAndGetElement(By.xpath(oiRichiestaDaPod.replace("#POD#", pod))).getText();
			return oi;
		} catch (Exception e) {
			throw new Exception("Impossibile recuperare l'OI della richiesta");
		}

	}
	
	public void annullaConfigurationItem(String pod, String oiOrdine) throws Exception{
		pressJavascript(elementiOrdineSection);
		
		try {
			pressJavascript(By.xpath(String.format(oiOrdineDynamicDaPod,pod,oiOrdine)));
			TimeUnit.SECONDS.sleep(10);
		}catch(Exception e) {
			throw new Exception("Impossibile accedere al configuration item dell'ordine "+oiOrdine);
		}
	}
	
	
	public String recuperaOIOrdineDaPod(String pod) throws Exception {
		try {
			String oi = util.waitAndGetElement(By.xpath(oiOrdineDaPod.replace("#POD#", pod))).getText();
			return oi;
		} catch (Exception e) {
			throw new Exception("Impossibile recuperare l'OI dell'Ordine, pod: " +pod);
		}

	}
	
	

	public void verificaStatoAttivitaComplete(String descrizioneAttivita, String status) throws Exception {
		// Variazione dettaglio fattura
		TimeUnit.SECONDS.sleep(10);
		util.objectManager(buttonNuovaAttivita, util.scrollToVisibility,true);
		TimeUnit.SECONDS.sleep(3);
		if (!util.exists(expandActivitySection, 120))
			throw new Exception("Impossibile trovare la sezione Attivita. Verificare latenza ambiente");
		
		pressJavascript(expandActivitySection);
		WebElement table = null;
		List<WebElement> tables = util.waitAndGetElements(attivitaTableCompleta);
		table = tables.get(tables.size() - 1);
		TimeUnit.SECONDS.sleep(10);
		int rowNum = getTableRowFromColumnTitleData(table, "DESCRIZIONE", descrizioneAttivita);
		String stato = getTableCellDataWithTitle(table, rowNum, "STATO");
		if (!stato.equals(status))
			throw new Exception(
					"L'Activity creata risulta avere uno stato diverso da " + status + ". Stato riscontrato :" + stato);

	}

	public void verificaStatiRichiesta(String stato, String R2D, String SAP, String SEMPRE) throws Exception {
		if (!util.checkElementText(statoRichiesta, stato))
			throw new Exception("Lo stato della richiesta e' diverso da " + stato);
		if (!util.checkElementText(statoR2DRichiesta, R2D))
			throw new Exception("Lo stato R2D della richiesta e' diverso da " + R2D);
		if (!util.checkElementText(statoSAPRichiesta, SAP))
			throw new Exception("Lo stato SAP-ISU della richiesta e' diverso da " + SAP);
		if (!util.checkElementText(statoSEMPRERichiesta, SEMPRE))
			throw new Exception("Lo stato SEMPRE della richiesta e' diverso da" + SEMPRE);
	}

	public void richiestaAnnullata(String azione_eseguita, String dettaglio_azione_eseguita) throws Exception {
		if (!util.checkElementText(azioneEseguita, azione_eseguita))
			throw new Exception("azione eseguita e' diverso da " + azione_eseguita);
		if (!util.checkElementText(dettaglioAzioneEseguita, dettaglio_azione_eseguita))
			throw new Exception("il dettaglio dell'azione eseguita  e' diverso da " + dettaglio_azione_eseguita);
	}
	
	public void statoConfigurazioneItem(String stato, String statoTrasporto) throws Exception {
		if (!util.checkElementText(statoR2d, stato))
			throw new Exception("stato r2D e' diverso da " + stato);
		if (!util.checkElementText(statoConfigurationItem, statoTrasporto))
			throw new Exception("lo stato e' diverso da " + statoTrasporto +"valore mostrato: " +statoConfigurationItem);
	}
	public void verificaStatiRichiestaPP(String nRichiesta, String Tipo, String StatoRichiesta, String SottoStato, String StatoElementoRichiesta) throws Exception {
		
		if (!util.checkElementText(numeroRichiesta, nRichiesta))
			throw new Exception("Lo stato della richiesta e' diverso da " + nRichiesta);
		if (!util.checkElementText(tipo, Tipo))
			throw new Exception("Lo stato della richiesta e' diverso da " + Tipo);
		if (!util.checkElementText(statoRichiestaPP, StatoRichiesta))
			throw new Exception("Lo stato della richiesta e' diverso da " + StatoRichiesta);
		else 			
			System.out.println("Stato Richiesta: " + StatoRichiesta);
		if (!util.checkElementText(sottoStatoRichiesta, SottoStato))
			throw new Exception("Lo stato della richiesta e' diverso da " + SottoStato);
		else 
			System.out.println("SottoStato Richiesta: " + SottoStato);
		if (!util.checkElementText(statoElementoRichiesta, StatoElementoRichiesta))
			throw new Exception("Lo stato della richiesta e' diverso da " + StatoElementoRichiesta);
		
	}

	public void verificaStatiOrdinePP(String StatoOrdine, String Tipo, String StatoPOD, String StatoR2D, String StatoCommodity, String StatoSapSD, String StatoSapISU, String StatoSEMPRE, String Pratica, String Prodotto) throws Exception {
 		
		if (!util.checkElementText(ordineStato, StatoOrdine))
			throw new Exception("Lo stato dell'Ordine e' diverso da " + StatoOrdine);
		if (!util.checkElementText(ordineTipo, Tipo))
			throw new Exception("Lo stato del Tipo e' diverso da " + Tipo);
		if (!util.checkElementText(ordinePOD, StatoPOD))
			throw new Exception("Lo stato del POD e' diverso da " + StatoPOD);
		int a =1;
		if (!util.checkElementText(ordineR2D, StatoR2D))
			throw new Exception("Lo stato di R2D e' diverso da " + StatoR2D);
		if (!util.checkElementText(ordinePratica, Pratica))
			throw new Exception("Lo stato della Pratica e' diverso da " + Pratica);
		if (!util.checkElementText(ordineCommodity, StatoCommodity))
			throw new Exception("Lo stato della Commodity e' diverso da " + StatoCommodity);
		if (!util.checkElementText(ordineSapSD, StatoSapSD))
			throw new Exception("Lo stato di SAP SD e' diverso da " + StatoSapSD);
		if (!util.checkElementText(ordineSapISU, StatoSapISU))
			throw new Exception("Lo stato dI SAP ISU e' diverso da " + StatoSapISU);
		if (!util.checkElementText(ordineSEMPRE, StatoSEMPRE))
			throw new Exception("Lo stato di SEMPRE e' diverso da " + StatoSEMPRE);
		if (!util.checkElementText(ordineProdotto, Prodotto))
			throw new Exception("Il Prodotto e' diverso da " + Prodotto);

	}
	public void verificaStatiRichiestaAnnulla(String stato, String R2D, String SAP, String SEMPRE, String POD, String sottoStato, String StatoRichiesta, String nRichiesta, String Tipo) throws Exception {
		
		if (!util.checkElementText(numeroRichiesta, nRichiesta))
			throw new Exception("Il Numero richiesta e' diverso da " + nRichiesta);
		if (!util.checkElementText(tipo, Tipo))
			throw new Exception("Il Tipo della richiesta e' diverso da " + Tipo);
		if (!util.checkElementText(statoRichiesta, StatoRichiesta))
			throw new Exception("Lo stato della richiesta e' diverso da " + StatoRichiesta);
		if (!util.checkElementText(statoElementoRichiesta, stato))
			throw new Exception("Lo Stato Elemento della richiesta e' diverso da " + stato);
		if (!util.checkElementText(sottoStatoRichiesta, sottoStato))
			throw new Exception("Il Sottostato della richiesta e' diverso da " + sottoStato);
		if (!util.checkElementText(statoPODRichiesta, POD))
			throw new Exception("Lo stato POD della richiesta e' diverso da " + POD);
		if (!util.checkElementText(statoR2DRichiesta, R2D))
			throw new Exception("Lo stato R2D della richiesta e' diverso da " + R2D);
		if (!util.checkElementText(statoSAPRichiesta, SAP))
			throw new Exception("Lo stato SAP-ISU della richiesta e' diverso da " + SAP);
		if (!util.checkElementText(statoSEMPRERichiesta, SEMPRE))
			throw new Exception("Lo stato SEMPRE della richiesta e' diverso da" + SEMPRE);
	}
	
	public void verificaStatiRichiestaAnnullaPP(String stato, String R2D, String SAP, String SEMPRE, String POD, String sottoStato, String StatoRichiesta, String nRichiesta, String Tipo) throws Exception {
		
		if (!util.checkElementText(numeroRichiesta, nRichiesta))
			throw new Exception("Il Numero richiesta e' diverso da " + nRichiesta);
		if (!util.checkElementText(tipo, Tipo))
			throw new Exception("Il Tipo della richiesta e' diverso da " + Tipo);
		if (!util.checkElementText(statoRichiestaPP, StatoRichiesta))
			throw new Exception("Lo stato della richiesta e' diverso da " + StatoRichiesta);
		else 
			System.out.println("Stato Richiesta: " + StatoRichiesta);
		if (!util.checkElementText(statoElementoRichiesta, stato))
			throw new Exception("Lo Stato Elemento della richiesta e' diverso da " + stato);
		if (!util.checkElementText(sottoStatoRichiesta, sottoStato))
			throw new Exception("Il Sottostato della richiesta e' diverso da " + sottoStato);
		else 
			System.out.println("SottoStato Richiesta: " + sottoStato);
		if (!util.checkElementText(statoPODRichiesta, POD))
			throw new Exception("Lo stato POD della richiesta e' diverso da " + POD);
		if (!util.checkElementText(statoR2DRichiesta, R2D))
			throw new Exception("Lo stato R2D della richiesta e' diverso da " + R2D);
		if (!util.checkElementText(statoSAPRichiesta, SAP))
			throw new Exception("Lo stato SAP-ISU della richiesta e' diverso da " + SAP);
		if (!util.checkElementText(statoSEMPRERichiesta, SEMPRE))
			throw new Exception("Lo stato SEMPRE della richiesta e' diverso da" + SEMPRE);
	}
	
	public void verificaStatiRichiestaAnnullaGenerica(String stato, String R2D, String SAP, String SEMPRE, String sottoStato, String StatoRichiesta, String nRichiesta, String Tipo) throws Exception {
		
		if (!util.checkElementText(numeroRichiesta, nRichiesta))
			throw new Exception("Il Numero richiesta e' diverso da " + nRichiesta);
		if (!util.checkElementText(tipo, Tipo))
			throw new Exception("Il Tipo della richiesta e' diverso da " + Tipo);
		if (!util.checkElementText(statoRichiestaPP, StatoRichiesta))
			throw new Exception("Lo stato della richiesta e' diverso da " + StatoRichiesta);
		if (!util.checkElementText(statoElementoRichiesta, stato))
			throw new Exception("Lo Stato Elemento della richiesta e' diverso da " + stato);
		if (!util.checkElementText(sottoStatoRichiesta, sottoStato))
			throw new Exception("Il Sottostato della richiesta e' diverso da " + sottoStato);
		if (!util.checkElementText(statoR2DRichiesta, R2D))
			throw new Exception("Lo stato R2D della richiesta e' diverso da " + R2D);
		if (!util.checkElementText(statoSAPRichiesta, SAP))
			throw new Exception("Lo stato SAP-ISU della richiesta e' diverso da " + SAP);
		if (!util.checkElementText(statoSEMPRERichiesta, SEMPRE))
			throw new Exception("Lo stato SEMPRE della richiesta e' diverso da" + SEMPRE);
	}

	public void verificaStatiRichiestaConPod(String stato, String R2D, String SAP, String SEMPRE, String pod) throws Exception {
		String s = util.getElementText(By.xpath(statoDaPod.replace("#POD#", pod).replace("#STATO#", "Stato R2D")));
		if (!s.contains(R2D))
			throw new Exception("Per il pod "+pod+" Lo stato R2D della richiesta e' diverso da " + R2D+". Stato riscontrato : "+s);
		   s = util.getElementText(By.xpath(statoDaPod.replace("#POD#", pod).replace("#STATO#", "Stato SAP-ISU")));
		   if (!s.contains(SAP))
			throw new Exception("Per il pod "+pod+" Lo stato SAP-ISU della richiesta e' diverso da " + SAP+". Stato riscontrato : "+s);
		   s = util.getElementText(By.xpath(statoDaPod.replace("#POD#", pod).replace("#STATO#", "Stato SEMPRE")));
		   if (!s.contains(SEMPRE))
			throw new Exception("Per il pod "+pod+" Lo stato SEMPRE della richiesta e' diverso da " + SEMPRE+". Stato riscontrato : "+s);
		   s = util.getElementText(By.xpath(statoDaPod.replace("#POD#", pod).replace("#STATO#", "Stato")));
			if (!s.contains(stato))
				throw new Exception("Per il pod "+pod+" Lo stato della richiesta e' diverso da " + stato+". Stato riscontrato : "+s);
	}
	
	public void verificaStatiRichiesta(String stato, String R2D, String pod) throws Exception {
		String s = util.getElementText(By.xpath(statoDaPod.replace("#POD#", pod).replace("#STATO#", "Stato R2D")));
		if (!s.contains(R2D))
			throw new Exception("Per il pod "+pod+" Lo stato R2D della richiesta e' diverso da " + R2D+". Stato riscontrato : "+s);
		   s = util.getElementText(By.xpath(statoDaPod.replace("#POD#", pod).replace("#STATO#", "Stato SAP-ISU")));
			if (!s.contains(stato))
				throw new Exception("Per il pod "+pod+" Lo stato della richiesta e' diverso da " + stato+". Stato riscontrato : "+s);
	}
	
	public void verifyStatoSottostatoRichiesta(String stato,String sottostato) throws Exception{
		String state= util.waitAndGetElement(status).getText();
		String subState= util.waitAndGetElement(sottostatus).getText();
		if(!stato.toLowerCase().trim().contentEquals(state.trim().toLowerCase())) throw new Exception("Lo stato della Richiesta risulta diverso da : "+stato+". Stato riscontrato: "+state);
		if(!sottostato.toLowerCase().trim().contentEquals(subState.trim().toLowerCase())) throw new Exception("Il sottostato della Richiesta risulta diverso da : "+sottostato+". Stato riscontrato: "+subState);

	}
	public int getTableRowFromColumnTitleData(WebElement table, String columnName, String columnValue)
			throws Exception {

		int rowCount = getTableRowCount(table);
		boolean guard = false;
		if (rowCount < 1)
			throw new Exception("Non risultano righe in tabella. Impossibile cercare un elemento");

		int colNumber = getTableColumnTitleIndexByName(table, columnName);
		colNumber++;
		int rowNum = 1;
		while (rowNum <= rowCount) {
			WebElement colContent = util.waitAndGetElement(table, By.xpath("./tbody/tr[" + rowNum
					+ "]/td[not(self::td[@class='slds-cell-shrink'])][" + colNumber + "]/span/span"));

			if (colContent.getText().equals(columnValue)) {
				guard = true;
				break;
			}
			rowNum++;
		}
		if (!guard)
			throw new Exception(
					"Impossibile trovare in tabella il valore " + columnValue + " nella colonna " + columnName);
		return rowNum;

	}

	public void verificaStatoDocumentiComplete(String modelloDocumento, String status) throws Exception {
		// Variazione dettaglio fattura
		Thread.currentThread().sleep(2000);
		util.objectManager(expandDocumentSection, util.scrollToVisibility, false);
		pressJavascript(expandDocumentSection);
		TimeUnit.SECONDS.sleep(5);
		WebElement table = null;
		List<WebElement> tables = util.waitAndGetElements(documentTableCompleta);
		table = tables.get(tables.size() - 1);
		TimeUnit.SECONDS.sleep(10);
		int rowNum = getTableRowFromColumnTitleData(table, "MODELLO", modelloDocumento);
		String stato = getTableDocumentCellDataWithTitle(table, rowNum, "STATO");
		if (!stato.equals(status))
			throw new Exception("Il documento creato risulta avere uno stato diverso da " + status
					+ ". Stato riscontrato :" + stato);

	}

	// Ritorna il numero di righe della tabella
	public int getTableRowCount(WebElement table) throws Exception {
		return util.waitAndGetElements(table, By.xpath("./tbody/tr")).size();
	}

	// Ritorna il numero di colonne della tabella. N.B: ritorna solo il numero di
	// righe significative,
	// non quelle con presenza checkbox
	public int getTableColumnCount(WebElement table) throws Exception {
		return util.waitAndGetElements(table, By.xpath("./thead/tr/th")).size();

	}

	// Ritorna l'indice della colonna della tabella a partire dal suo nome
	public int getTableColumnTitleIndexByName(WebElement table, String colName) throws Exception {
		int colIndex = 0;
		boolean guard = false;
		List<WebElement> colNamesSpan = util.waitAndGetElements(table, By.xpath("./thead/tr/th[@title]//span[@title]"));
		for (WebElement el : colNamesSpan) {
			colIndex = colIndex + 1;
			////System.out.println("Colonna "+el.getText());
			if (el.getText().equalsIgnoreCase(colName)) {
				////System.out.println("Colonna trovata");
				guard = true;
				break;
			}
		}

		if (!guard)
			throw new Exception("Impossibile trovare la colonna " + colName + " nella tabella");

		return colIndex;
	}

	public String getTableCellDataWithTitle(WebElement table, int rowNumber, String columnName) throws Exception {
		if (rowNumber == 0) {
			throw new Exception("Il numero di righe deve partire da 1");
		}
		int rowCount = getTableRowCount(table);
		if (rowCount < rowNumber)
			throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");

		int colNumber = getTableColumnTitleIndexByName(table, columnName);
		////System.out.println("Indice Colonna: "+colNumber);

		colNumber = colNumber + 2;
		String xpath = "./tbody/tr[" + rowNumber + "]/td[not(self::td[@class='slds-cell-shrink'])][" + colNumber
				+ "]/span/span";

		////System.out.println(xpath);

		WebElement colContent = util.waitAndGetElement(table, By.xpath(xpath));

		return colContent.getText();
	}

	public void closeTabAttivita() {
		driver.findElement(chiudiTabAttivita).click();
	}

	public void closeTabDocumenti() {
		driver.findElement(chiudiTabDocumenti).click();
	}

	public void verificaDocumentiCreati(int numDocumenti, String[] descrizioni) throws Exception {
		WebElement table = util.waitAndGetElement(documentiTable);
		int documenti = getTableRowCount(table);
		if (documenti != numDocumenti)
			throw new Exception("Nella Tabella Documenti, numero di documenti creati : " + documenti
					+ ". Documenti attesi: " + numDocumenti);
		if (descrizioni.length != documenti)
			throw new Exception(
					"Il numero di documenti riscontrate risulti diverso dal numero di descrizioni da verificare. Num documenti : "
							+ documenti + ". Num descrizioni " + descrizioni.length);
		int index = 1;
		for (String p : descrizioni) {
			String tableCell = getTableCellData(table, index, "MODELLO");
			////System.out.println("Descrizione n°"+index+":"+tableCell);
			if (!p.equals(tableCell))
				throw new Exception("Per il documento numero " + index
						+ " creata, il campo descrizione in tabella risulta diverso da quello atteso. Valore riscontrato: "
						+ tableCell + ". Valore atteso: " + p);

			index++;
		}

	}

	public String getTableDocumentCellDataWithTitle(WebElement table, int rowNumber, String columnName)
			throws Exception {
		if (rowNumber == 0) {
			throw new Exception("Il numero di righe deve partire da 1");
		}
		int rowCount = getTableRowCount(table);
		if (rowCount < rowNumber)
			throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");

		int colNumber = getTableColumnTitleIndexByName(table, columnName);
		////System.out.println("Indice Colonna: "+colNumber);

		colNumber = colNumber + 1;
		String xpath = "./tbody/tr[" + rowNumber + "]/td[not(self::td[@class='slds-cell-shrink'])][" + colNumber
				+ "]/span/span";

		////System.out.println(xpath);

		WebElement colContent = util.waitAndGetElement(table, By.xpath(xpath));

		return colContent.getText();
	}
	
	
	public void verificaCreazioneLinkDocumenti(String modello) throws Exception{
		By link = By.xpath("//h1[text()='Documenti']/ancestor::div[@data-aura-class='forceListViewManager']//table//span[text()='"+modello+"']/ancestor::tr//a[text()='Link']");
		if(!util.verifyExistence(link, 10)) throw new Exception("Non è stato generato il Link per il documento "+modello);
	}

	public String getTableCellData(WebElement table, int rowNumber, String columnName) throws Exception {
		if (rowNumber == 0) {
			throw new Exception("Il numero di righe deve partire da 1");
		}
		int rowCount = getTableRowCount(table);
		if (rowCount < rowNumber)
			throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");

		int colNumber = getTableColumnIndexByName(table, columnName);
		List<WebElement> colContents = util.waitAndGetElements(table,
				By.xpath("./tbody/tr[" + rowNumber + "]/child::*"));

		return util.waitAndGetElement(colContents.get(colNumber - 1), By.xpath("./span")).getText();
	}

	// Ritorna l'indice della colonna della tabella a partire dal suo nome
	public int getTableColumnIndexByName(WebElement table, String colName) throws Exception {
		int colIndex = 0;
		boolean guard = false;
		List<WebElement> colNamesSpan = util.waitAndGetElements(table, By.xpath("./thead/tr/th"));
		for (WebElement el : colNamesSpan) {
			colIndex = colIndex + 1;
			if (el.getText().equalsIgnoreCase(colName)) {
				guard = true;
				break;
			}
		}

		if (!guard)
			throw new Exception("Impossibile trovare la colonna " + colName + " nella tabella");

		return colIndex;
	}

	public void verificaSottoStatiRichiesta(String sottoStato) throws Exception{
		if(!util.checkElementText(sottoStatoRichiesta, sottoStato)) throw new Exception("Il Sottostato della richiesta e' diverso da "+sottoStato);
		
	}
	
	public String salvaNumeroContratto() throws Exception {
	String numerocontratto= util.waitAndGetElement(numero_contratto,true).getAttribute("value");
	//System.out.println(numerocontratto);
	return numerocontratto;
	}
	
	public void checkOfferta(String processo, String commodity, String stato) throws Exception {
		String tipoOfferta="//h1/div[text()='Offerta']/ancestor::div[1]//span[text()='#']";
		String xpathString = tipoOfferta.replace("#", processo);
		By item = By.xpath(xpathString);
		if (!util.verifyExistence(item, 150)) {
			throw new Exception("l'oggetto con xpath " + item + " non esiste.");
		   }
		By quote=By.xpath("//p[text()='Quote']");
		if (!util.verifyExistence(quote, 150)) {
			throw new Exception("l'oggetto con xpath " + quote + " non esiste.");
		   }
		
		String comm="//p[text()='Tipo Offerta']//ancestor::div[1]//lightning-formatted-text[text()='#']";
		String xpathString2 = comm.replace("#", commodity);
		By item2 = By.xpath(xpathString2);
		if (!util.verifyExistence(item2, 150)) {
			throw new Exception("l'oggetto con xpath " + item2 + " non esiste.");
		   }	
		By data=By.xpath("//p[text()='Data Adesione']");
		if (!util.verifyExistence(data, 150)) {
			throw new Exception("l'oggetto con xpath " + data + " non esiste.");
		   }	
	     
		String state="//p[text()='Stato']//ancestor::div[1]//lightning-formatted-text[text()='#']";
		String xpathString3 = state.replace("#", stato);
		By item3 = By.xpath(xpathString3);
		if (!util.verifyExistence(item3, 150)) {
			throw new Exception("l'oggetto con xpath " + item3 + " non esiste.");
		   }	
	}
	
}
