package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.Locale;

import com.github.javafaker.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.IBAN;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class PopupNuovoMetodoPagamentoComponent extends BaseComponent{

	private WebDriver driver;
	private SeleniumUtilities util;
	private SpinnerManager spinnerManager;
	
	
	public By campoNome=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']//ancestor::div[@class='slds-modal__container']//label[text()='Nome']/../div/input");
	public By campoCognome=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']//ancestor::div[@class='slds-modal__container']//label[text()='Cognome']/../div/input");
	public By campoCF=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']//ancestor::div[@class='slds-modal__container']//label[text()='Codice Fiscale']/../div/input");
	public By campoIban=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']//ancestor::div[@class='slds-modal__container']//label[text()='IBAN']/../div/input");
	public By buttonSalva=By.xpath("//h2[text()='Nuovo Metodo di Pagamento']//ancestor::div[@class='slds-modal__container']//button[text()='Salva']");
	public By ibanPresente=By.xpath("//div[@role='alert' and text()='IBAN già presente a sistema, procedere alla modifica']");
    public By ragioneSociale = By.xpath("//h2[text()='Nuovo Metodo di Pagamento']//ancestor::div[@class='slds-modal__container']//label[text()='Ragione Sociale']/../div/input"); 
	public PopupNuovoMetodoPagamentoComponent(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		util = new SeleniumUtilities(driver);
		spinnerManager = new SpinnerManager(driver);
	}

	 public void popolareCampoIbanRandomico(By oggetto) throws Exception{
	        TimeUnit.SECONDS.sleep(1);
	        
		    WebElement element = util.waitAndGetElement(oggetto,60,2);
		
		    String iban =IBAN.generate();
		    util.objectManager(oggetto,util.sendKeys, iban);	
		  
		    TimeUnit.SECONDS.sleep(1);
			element.sendKeys(Keys.ENTER);
			}
	
		
		 public void popolareCampi(By oggetto,String valore) throws Exception{
		        TimeUnit.SECONDS.sleep(1);
			    WebElement element = util.waitAndGetElement(oggetto,60,2);
			    util.objectManager(oggetto, util.sendKeys, valore);	
				TimeUnit.SECONDS.sleep(1);
				element.sendKeys(Keys.ENTER);
				}
		 
		 public void popolaNomeCognome(String name)throws Exception{
			 String nome="";
			 String cognome="";
			   String[] nomi = name.trim().split(" ");
			   if(nomi.length==2) {
				   nome = nomi[1];
				   cognome = nomi[0];
			   }else if(nomi.length==3) {
				   nome = nomi[2];
				   cognome = nomi[0]+" "+nomi[1];
			   }
			   
			   TimeUnit.SECONDS.sleep(1);
			   WebElement element = util.waitAndGetElement(campoNome,60,2);
			   util.objectManager(campoNome, util.sendKeys, nome);	
			   TimeUnit.SECONDS.sleep(1);
			   element.sendKeys(Keys.ENTER);
			   TimeUnit.SECONDS.sleep(1);
			   element = util.waitAndGetElement(campoCognome,60,2);
			   util.objectManager(campoCognome, util.sendKeys, cognome);	
			   TimeUnit.SECONDS.sleep(1);
			   element.sendKeys(Keys.ENTER);	
		}
		
		 

}
