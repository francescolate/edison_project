package com.nttdata.qa.enel.components.colla;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class ModificapwdComponent {
	
	public By iconUser = By.xpath("//button[@class='dotcom-header__btn btn-user-open']/child::span[@class='icon-user']");
	public By datiDiRegister = By.xpath("//span[text()='Dati di Registrazione']/parent::a[@id='modificaprofilo']");
	public By credenzialiHeader = By.xpath("//section[@id='step-info-username']/descendant::h3[contains(text(),'Credenziali di accesso')]");
	public By anagraficaHeader = By.xpath("//section[@id='profileSection']/descendant::h3[contains(text(),'Anagrafica personale')]");
	public By cellulareEyeIcon = By.xpath("//span[text()='Icona informativa']/preceding-sibling::span[@class='dsc-icon-info-circle']");
	public By denominazione = By.xpath("//input[@id='denominazione' and @placeholder='Denominazione*']");
	public By partita = By.xpath("//input[@id='partitaIva' and @placeholder='Partita IVA']");
	public By codice = By.xpath("//input[@id='cfa' and @placeholder='Fiscal Code Azienda']");
	public By genericErrorMsg = By.xpath("//p[contains(text(),'Si è verificato un errore durante il processo di a')]");
	public By Chevrondown = By.xpath("//span[@class='dsc-icon-chevron-down']");
	public By EditProfile = By.xpath("//span[@id='welcomeBox']/nav[@id='msg-box']/ul/li[1]/a[@id='modificaprofilo']");
	public By goOut = By.xpath("//span[@id='welcomeBox']/nav[@id='msg-box']/ul/li[2]/a[@id='tr_disconnetti']");
	
	public By custHomePageloc = By.xpath("//div[@class='panel-body']/descendant::ol[@class='breadcrumb']");
	public By HomePagelogo = By.xpath("//a[@class='brand href-sf-prevent-default']");
	public By cutHomePageHeader = By.xpath("//div[@class='panel-body']/descendant::h1[contains(text(),'Benvenuto nella')]");
	
	public By impresaButton = By.xpath("//li[@class='col-sm-6 margin-bottom-30']/descendant::a[@href='/it/area-clienti/imprese'][text()='Entra']");
	public By custBSNTitle = By.xpath("//div[@id='mainContentWrapper']/descendant::h1[contains(text(),'Benvenuto nella tua area privata')]");
	public By custBSNContent = By.xpath("//div[@id='mainContentWrapper']/descendant::p[contains(text(),'In questa sezione potrai')]");
	public static final String CUSTBSN_TITLE = "Benvenuto nella tua area privata";
	public static final String CUSTBSN_CONTENT = "In questa sezione potrai gestire le tue forniture";
	public String emailIn = "//input[@id='email']";
	public String nomeIn = "//input[@id='nome']";
	public String cogoNome = "//input[@id='cognome']";
	public String cellulare = "//input[@id='complete-profile-mobile-num']";
	public String codiceFisc = "//input[@id='cf']";
	
	public By emailInput =  By.xpath("//label[text()='Email*']/following-sibling::input[@id='email' and @placeholder='example@test.com']");
	public By NomeInput = By.xpath("//label[text()='Nome*']/following-sibling::input[@id='nome' and @placeholder='Nome']");
	public By cogNomeInput = By.xpath("//label[text()='Cognome*']/following-sibling::input[@id='cognome' and @placeholder='Cognome']");
	public By cellulareInput = By.xpath("//a[@id='link_modal-info-cell']/following-sibling::input[@name='phoneNumber' and @placeholder='Inserisci il tuo Numero di telefono']");
	public By codiceFiscaleInput = By.xpath("//label[text()='Codice Fiscale*']/following-sibling::input[@id='cf' and @placeholder='Codice fiscale']");
	public By NuovoEmailInput = By.xpath("//input[@id='newemail' and @placeholder='Example@mail.com']");
	public By confermaNuovoEmailInput = By.xpath("//input[@id='confirmemail' and @placeholder='Example@mail.com']");
	public By vecchiaPassword = By.xpath("//input[@id='oldpassword' and @placeholder='Vecchia password']");
	public By ModificaPassword = By.xpath("//label[contains(text(),'Modifica la tua password')]");
	public By nuovaPassword = By.xpath("//input[@id='newpassword' and @placeholder='Nuova password']");
	public By conferaNuovaPassword = By.xpath("//input[@id='confirmpassword' and @placeholder='Conferma nuova password']");
	
	public static final String MODIFICA_PASSWORD = "Modifica la tua password";
	public static final String EMAIL = "immilezell-1212@yopmail.com";
	public static final String EMAIL_RAF = "raffael.laquomo@gmail.com";
	public static final String NOME = "TEST";
	public static final String NOME_MARI = "MARIA MADDALENA";
	public static final String COGNOME = "COLLAUDO";
	public static final String COGNOME_RIO = "DI LEO";
	public static final String CELLULARE = "3394956806";
	public static final String CELLULARE_RIO = "+39064745721";
	public static final String CODICEFISCALE = "03430350177";
	public static final String CODICEFISCALE_RIO = "DLIMMD69P57I533U";
	public static final String EMAILINPUT_VALUE = "Example@mail.com";
	public static final String VECCHIA_PASSWORD = "Vecchia password";
	public static final String NUOVA_PASSWORD = "Nuova password";
	public static final String CONFERMANUOAVA_PASSWORD = "Conferma nuova password";
	

	public By popupModificaTitle = By.xpath("//h3[@id='info-cell-title']/p[contains(text(),'Modifica numero di cellulare')]");
	public By popupPotraiContent = By.xpath("//div[@class='modal-content']/child::p[contains(text(),'Potrai modificare')]");
	public By quiLink = By.xpath("//div[@class='modal-content']/child::p[contains(text(),'Potrai modificare')]/descendant::a[text()='qui']");
	public By popupClose = By.xpath("//button[@class='modal-close']/child::span[@class='dsc-icon-close']");
	
	public By condizioni = By.xpath("//div[text()='Condizioni Generali']");
	public By informativaTitle = By.xpath("//span[text()='Ho letto e accettato:']");
	public By condizioniTitle = By.xpath("//span[text()='Ho letto e accettato:']");
	public By informativaCheckbox = By.xpath("//input[@data-checkbox-id='89']/parent::div[@class='form-group checkbox col-xs-12 col-sm-12']");
	public By condizioniCheckbox = By.xpath("//input[@data-checkbox-id='86']/parent::div[@class='form-group checkbox col-xs-12 col-sm-12']");
	public By linkInformativa = By.xpath("//button[@data-title='Informativa privacy']");
	public By linkCondizioni = By.xpath("//button[@data-title='Condizioni generali dei servizi offerti']");
	public By generalConditions = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/div[@class='modal-body']/div[@class='row']");
	public By informativaPrivacy = By.xpath("//div[@class='cmModalTitle cmTitle1']/ancestor::div[@class='container']");
	public By linkINformativa1 = By.xpath("(//span[contains(text(),'Ho letto e accettato:')])[1]");
	public By linkCondition1 = By.xpath("(//span[contains(text(),'Ho letto e accettato:')])[2]");	
	public By modificaEmailLink = By.xpath("//ul[@id='internal-menu']/descendant::a[text()='Modifica Email']");
	public By ModificaPassword1 = By.xpath("//ul[@id='internal-menu']/descendant::a[text()='Modifica Password']");
	public By eliminaRegistrazioneLink = By.xpath("//ul[@id='internal-menu']/descendant::a[text()='Elimina Registrazione']");
	public By esciLink = By.xpath("//ul[@id='internal-menu']/descendant::a[text()='ESCI']");
	public By tornaClientiLink = By.xpath("//ul[@id='internal-menu']/descendant::a[contains(text(),'Torna')]");
	public By clausoleLink = By.xpath("//ul[@id='internal-menu']/descendant::a[contains(text(),'Clausole Legali')]");
	
		
	public By modificaEmailHeader = By.xpath("//form[@id='cambia-email-form']/descendant::label[text()='Modifica email']");
	public By modificaEmailContent = By.xpath("//form[@id='cambia-email-form']/descendant::p[contains(text(),'Inserisci il')]");
	public By confermaEmailPasswdBtn = By.xpath("//button[@id='confirmChangeEmail' and text()='Conferma']");
	public By EmailCampiValue = By.xpath("//form[@id='cambia-email-form']/descendant::p[contains(text(),'*Campi obbligatori')]");
	public static final String CAMPI_TEXT = "*Campi obbligatori";
	
	public By modificaPasswordHeader = By.xpath("//form[@id='cambia-password-form']/descendant::label[text()='Scegli la tua nuova password']");
	public By modificaPasswordSubHeader = By.xpath("//form[@id='cambia-password-form']/descendant::p[text()='Ti ricordiamo che per effettuare correttamente il login hai a disposizione ']");
	public By confermaChangePasswdBtn = By.xpath("//button[@id='confirmChangePassord' and text()='Conferma']");
	public By passwordCampiValue = By.xpath("//form[@id='cambia-password-form']/descendant::p[contains(text(),'*Campi obbligatori')]");
	public By nuovaPasswordValue = By.xpath("//div[@class='form-group']/child::span[contains(text(),'La password deve')]");
	public By nuovaPasswordLabel = By.xpath("//label[text()='Nuova password*']");
	public By confermaPasswordLabel = By.xpath("//label[text()='Conferma nuova password*']");
	public By confermaPasswordInput = By.xpath("//input[@id='newpassword']");
	public By nuovaPasswordInput = By.xpath("//input[@id='confirmpassword']");
	public By errorMsg = By.xpath("//span[text()='Le due password devono coincidere']");
	public By errorMsgNuova = By.xpath("//input[@id='newpassword']/following-sibling::div/span");
	public By errorMsgConferma = By.xpath("//input[@id='confirmpassword']/following-sibling::div/span");
	public By tornaButton = By.xpath("//h3[text()='Non è stato possibile avviare la richiesta']/parent::div/div[2]/a/button");
	
	
	public By eliminaHeader = By.xpath("//div[@id='elimina-profilo']/child::h3[contains(text(),'Sei sicuro di')]");
	public By elininaContent = By.xpath("//div[@id='elimina-profilo']/child::p");
	public By proseguiButton = By.xpath("//button[@type='button' and contains(text(),'Prosegui')]");
	
	
	public By quiSupportoPath = By.xpath("//main[@id='main']/descendant::ul[@class='breadcrumbs component']");
	public By quiSupportoHeader = By.xpath("//main[@id='main']/descendant::h1[contains(text(),'Non riesco ad')]");
	public By quiSupportoQues1 = By.xpath("//button[contains(text(),'Non riesco ad accedere')]");
	public By quiSupportoQues2 = By.xpath("//button[contains(text(),'Ho dimenticato')]");
	public By quiSupportoQues3 = By.xpath("//button[contains(text(),'Non riesci ancora')]");
	public By quiSupportoQues4 = By.xpath("//button[contains(text(),'Ho problemi')]");
	public By quiSupportoQues5 = By.xpath("//button[contains(text(),'Non ricordo lo')]");
	
	
	public By privacyMenu = By.xpath("//a[text()='Informativa Privacy']");
	public By legalClausesMenu = By.xpath("//a[text()='Clausole Legali']");
	
	public By clausoleLegaliHeader = By.xpath("//h1[contains(text(),'Clausole legali')]");
	public By clausoleQues1 = By.xpath("//button[contains(text(),'Oggetto delle Condizioni Generali')]");
	public By clausoleQues2 = By.xpath("//button[contains(text(),'Registrazione')]");
	public By clausoleQues3 = By.xpath("//button[contains(text(),'Assunzione di responsabilità e Manleva')]");
	public By clausoleQues4 = By.xpath("//button[contains(text(),'Recesso')]");
	public By clausoleQues5 = By.xpath("//button[contains(text(),'Sospensione')]");
	public By clausoleQues6 = By.xpath("//button[contains(text(),'Modifiche delle Condizioni Generali')]");
	public By clausoleQues7 = By.xpath("//button[contains(text(),'Proprietà Intellettuale')]");
	public By clausoleQues8 = By.xpath("//button[contains(text(),'Limitazione di responsabilità di Enel Energia')]");
	public By clausoleQues9 = By.xpath("//button[contains(text(),'Legge applicabile')]");

	public By faqPath = By.xpath("//ul[@class='breadcrumbs component']");
	public By faqHeader = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By faqQues1 = By.xpath("//button[contains(text(),'Non riesco ad')]");
	public By faqQues2 = By.xpath("//button[contains(text(),'Ho dimenticato la password')]");
	public By faqQues3 = By.xpath("//button[contains(text(),'Non riesci ancora')]");
	public By faqQues4 = By.xpath("//button[contains(text(),'Ho problemi con')]");
	public By faqQues5 = By.xpath("//button[contains(text(),'Non ricordo lo username')]");
	
		
	public By informativaPrivacyLink = By.xpath("//ul[@id='internal-menu']/descendant::a[text()='Informativa Privacy']");
	public By privacy = By.xpath(" //h1[@class='image-hero_title text--page-heading' and text()='Privacy']");
	public static final String FAQ_QUES2 = "Ho dimenticato la password. Posso recuperarla?";
		
	
	public By privacyContent = By.xpath("//p[@class='image-hero_detail text--detail']");
	public By PrivacyQuesTitle = By.xpath("//h3[contains(text(),'Informativa privacy di Enel Energia ai sensi dell')]");		
	public By privacyQues1 = By.xpath("//button[contains(text(),'Titolare e responsabile del trattamento dei dati p')]");
	public By privacyQues2 = By.xpath("//button[contains(text(),'Responsabile della')]");
	public By privacyQues3 = By.xpath("//button[contains(text(),'Oggetto e')]");
	public By privacyQues4 = By.xpath("//button[contains(text(),'Finalità e base')]");
	public By privacyQues5 = By.xpath("//button[contains(text(),'Registrazione all')]");
	public By privacyQues6 = By.xpath("//button[contains(text(),'Finalità di')]");
	public By privacyQues7 = By.xpath("//button[contains(text(),'Destinatari dei')]");
	public By privacyQues8	= By.xpath("//button[contains(text(),'Trasferimento dei')]");
	public By privacyQues9	= By.xpath("//button[contains(text(),'Periodo di')]");
	public By privacyQues10 = By.xpath("//button[contains(text(),'Diritti degl')]");
	public By privacyQues11 = By.xpath("//button[contains(text(),'Utilizzo de')]");
	public By facebookAssociaBtn = By.xpath("(//span[contains(text(),'ASSOCIA')]/parent::button[@class='button_first joinSocial'])[1]");
	public By socialContent = By.xpath("//h1[@id='social-profile']/following-sibling::p[contains(text(),'Gestisci in questa sezione')]");
	public static final String SOCIAL_CONTENT = "Gestisci in questa sezione il tuo account di Enel Energia e associalo a quello del tuo profilo Social. Potrai così accedere ai servizi digitali dell’Area Clienti e dell’APP di Enel Energia inserendo le credenziali dell’account associato. Se lo desideri, potrai in ogni momento rimuovere l’associazione al tuo profilo social.";
	public static final String PRIVACY = "Informativa Privacy";
	public static final String PRIVACY_CONTENT = "Le società Enel Energia S.p.A. ed Enel Italia S.p.A. (di seguito, complessivamente, le “Società”), in qualità di titolari autonomi del trattamento, informano che i dati personali degli interessati verranno trattati nel rispetto delle disposizioni previste dal REGOLAMENTO UE 2016/679 (“GDPR”) per le finalità e con le modalità indicate nelle rispettive Informative privacy.";
	public static final String PRIVACYQUES_TITLE = "Informativa privacy di Enel Energia ai sensi dell’art. 13 del Regolamento UE 2016/679 (“GDPR”)";
	public static final String PRIVACY_QUES1 = "Titolare e responsabile del trattamento dei dati personali";
	public static final String PRIVACY_QUES2 = "Responsabile della protezione dei dati personali (RPD)";
	public static final String PRIVACY_QUES3 = "Oggetto e modalità del trattamento";
	public static final String PRIVACY_QUES4 = "Finalità e base giuridica del trattamento";
	public static final String PRIVACY_QUES5 = "Registrazione all'area riservata e social login";
	public static final String PRIVACY_QUES6 = "Finalità di marketing e/o profilazione";
	public static final String PRIVACY_QUES7 = "Destinatari dei dati personali";
	public static final String PRIVACY_QUES8 = "Trasferimento dei dati personali";
	public static final String PRIVACY_QUES9 = "Periodo di conservazione dei dati personali";
	public static final String PRIVACY_QUES10 = "Diritti degli interessati";
	public static final String PRIVACY_QUES11 = "Utilizzo dei Cookie";
	public static final String MODIFICAPASSWORD_HEADER = "Scegli la tua nuova password";
	public static final String MODIFICAPASSWORDSUB_HEADER = "Ti ricordiamo che per effettuare correttamente il login hai a disposizione 10 tentativi per inserire la password, superati i quali dovrai attendere qualche minuto prima di riprovare.";

	public static final String QUISUPPORTO_PATH = "Home/supporto/Non riesco ad accedere all'Area Clienti Enel Energia";
	public static final String QUISUPPORTO_HEADER = "Non riesco ad accedere all'Area Clienti Enel Energia";
	public static final String QUISUPPORTO_QUES1 = "Non riesco ad accedere e a registrarmi al mio profilo unico";
	public static final String QUISUPPORTO_QUES2 = "Ho dimenticato la password. Posso recuperarla?";
	public static final String QUISUPPORTO_QUES3 = "Non riesci ancora ad accedere o a registrarti?";
	public static final String QUISUPPORTO_QUES4 = "Ho problemi con il codice OTP. Cosa devo fare?";
	public static final String QUISUPPORTO_QUES5 = "Non ricordo lo username con il quale mi sono registrato, cosa devo fare?";
	
	public static final String FAQ_HEADER = "Non riesco ad accedere all'Area Clienti Enel Energia";
	public static final String FAQ_QUES1 = "Non riesco ad accedere e a registrarmi al mio profilo unico";
	public static final String FAQ_QUES3 = "Non riesci ancora ad accedere o a registrarti?";
	public static final String FAQ_QUES4 = "Ho problemi con il codice OTP. Cosa devo fare?";
	public static final String FAQ_QUES5 = "Non ricordo lo username con il quale mi sono registrato, cosa devo fare?";
	public static final String CUSTHOMEPAGE_LOC = "Area riservataHomepage";
	public static final String CUSTHOMEPAGE_HEADER = "Benvenuto nella tua area dedicata Business";
	public static final String CREDENZIALI_HEADER = "Credenziali di accesso";
	public static final String ANAGRAFICA_HEADER = "Anagrafica personale";
	public static final String ANAGRAFICA_HEADER1 = "Anagrafica azienda";
	public static final String MODIFICA_TITLE = "Modifica numero di cellulare";
	public static final String MODIFICAEMAIL_TITLE = "Modifica email";
	public static final String MODIFICAEMAIL_CONTENT = "Inserisci il tuo nuovo indirizzo email. Riceverai un messagio all´indirizzo di posta elettronica indicato.La modifica sarà attiva solo dopo aver cliccato sul link di convalida presente all´interno della email.";
	public static final String POTRAI_CONTENT = "Potrai modificare in ogni momento il numero di cellulare inserito seguendo le indicazioni riportate qui";
	public static final String FAQ_PATH = "Home/supporto/Non riesco ad accedere all'Area Clienti Enel Energia";
	public static final String NUOVAPASSWD_VALUE = "La password deve contenere almeno 8 caratteri, di cui uno in maiuscolo ed un numero";
	public static final String MODIFICAPASSWD_HEADER = "Scegli la tua nuova password";
	public static final String MODIFICAPASSWDSUB_HEADER = "Ti ricordiamo che per effettuare correttamente il login hai a disposizione 10 tentativi per inserire la password, superati i quali dovrai attendere qualche minuto prima di riprovare.";
	public static final String ELIMINA_HEADER = "Sei sicuro di voler eliminare la tua registrazione?";
	public static final String ELININA_CONTENT = "Procedendo con l’eliminazione i tuoi dati di registrazione verranno eliminati. Per accedere nuovamente alla tua area riservata dovrai avviare una nuova registrazione.";
	public static final String INFORMATIVA_TITLE = "Ho letto e accettato:";
	public static final String CONDIZIONI_TITLE = "Ho letto e accettato:";
	public static final String CLAUSOLE_PATH = "HOME/SUPPORTO/CLAUSOLE LEGALI";
	public static final String CLAUSOLELEGAL_HEADER = "Clausole legali";
	public static final String CLAUSOLE_QUES1 = "Oggetto delle Condizioni Generali";
	public static final String CLAUSOLE_QUES2 = "Registrazione";
	public static final String CLAUSOLE_QUES3 = "Assunzione di responsabilità e Manleva";
	public static final String CLAUSOLE_QUES4 = "Recesso";
	public static final String CLAUSOLE_QUES5 = "Sospensione e/o interruzione dei servizi da parte di Enel Energia";
	public static final String CLAUSOLE_QUES6 = "Modifiche delle Condizioni Generali";
	public static final String CLAUSOLE_QUES7 = "Proprietà Intellettuale";
	public static final String CLAUSOLE_QUES8 = "Limitazione di responsabilità di Enel Energia";
	public static final String CLAUSOLE_QUES9 = "Legge applicabile";
	public static final String ERRORMSG_VALUE = "Le due password devono coincidere";
	public static final String ERRORMSGNUOVA_VALUE = "Password non valida";
	public static final String ERRORMSGCONFERMA_VALUE = "Password non valida";
	//public static final String MODIFICAPASSWD_HEADER = "Scegli la tua nuova password";
	//public static final String MODIFICAPASSWDSUB_HEADER = "Ti ricordiamo che per effettuare correttamente il login hai a disposizione 10 tentativi per inserire la password, superati i quali dovrai attendere qualche minuto prima di riprovare.";
	
	WebDriver driver;
	SeleniumUtilities util;
	Properties prop = null;
	
	public By title = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='cmText2 cmMarginYlg']");
	public By anagraficaHeader1 = By.xpath("//h3[contains(text(),'Anagrafica azienda')]");
	public By emailLabel = By.xpath("//div[@class='row'][1]/div[@class='form-group col-xs-12 col-sm-12']/label");
	public By emailField = By.xpath("//input[@id='email']");
	public By personaleLabel = By.xpath("//div[@class='cmText2 cmMarginYlg cmPaddingTopXs']");
	public String name = "//input[@id='nome']";
	public String cogNome = "//input[@id='cognome']";
	public String phoneNum = "//input[@id='complete-profile-mobile-num']";
	public String codiceFiscale = "//input[@id='cf']";
	public By namelabel = By.xpath("//form[@id='modificaAnagrafica-form']/div[@class='row'][2]/div[@class='form-group col-xs-12 col-sm-12'][1]/label");
	public By cogNomeLabel = By.xpath("//div[@class='row'][2]/div[@class='form-group col-xs-12 col-sm-12'][2]/label");
	public By phoneNumLabel= By.xpath("//div[@class='row'][2]/div[@class='form-group col-xs-12 col-sm-12'][3]/label");
	public By codiceFiscaleLabel = By.xpath("//div[@class='row'][2]/div[@class='form-group col-xs-12 col-sm-12'][4]/label");
	public By iIcon = By.xpath("//div[@class='form-group col-xs-12 col-sm-12'][3]/span[@class='dsc-icon-info-circle']");
	public By modalTitle = By.xpath("//div[@class='modal-dialog']/div[@class='modal-header']/h3/p");
	public By modalText = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/p");
	public By hereLink = By.xpath("//div[@class='modal-dialog']/div[@class='modal-content']/p/a");
	public By modalClose = By.xpath("//span[@class='dsc-icon-close']");
	public By checkbox1 = By.xpath("//div[@class='uIdcheckbox-container']//input[@data-checkbox-id ='11']");
	public By checkbox2 = By.xpath("//div[@class='uIdcheckbox-container']//input[@data-checkbox-id ='10']");
	public By privacyDisclaimer = By.xpath("//div[@id='generatedConsents']/div[@class='informative-new'][1]/div[@class='uIdcheckbox-container']/div[@class='form-group checkbox col-xs-12 col-sm-12']/label[@id='onlytext']");
	public By privacyDisclaimerClose = By.xpath("//div[@class='container']/button[@class='btn-close pull-right']");
	public By generalCondition = By.xpath("//div[@class='informative-new'][2]/div[@class='uIdcheckbox-container']/div[@class='form-group checkbox col-xs-12 col-sm-12']/label[@id='onlytext']");
	public By generalConditionClose = By.xpath("//div[@class='container']/button[@class='btn-close pull-right']");
	public By modificaEmailMenu = By.xpath("//a[text()='Modifica Email']");
	public By modificaEmailTitle = By.xpath("//form[@id='cambia-email-form']/div[@class='form-group form-group-full']/label");
	public By modificaEmailText = By.xpath("//p[@class='cmPageSubtitle cmText3 media-body media-middle']");
	public By newEmailLabel = By.xpath("//form[@id='cambia-email-form']/div[@class='form-group'][1]/label");
	public By newEmailInput = By.xpath("//div[@class='form-group'][1]/input[@id='newemail']");
	public By confirmNewEmail = By.xpath("//form[@id='cambia-email-form']/div[@class='form-group'][2]/label");
	public By confirmNewEmailInput = By.xpath("//div[@class='form-group'][2]/input[@id='confirmemail']"); 
	public By requiredFieldEmail = By.xpath("//div[@class='form-group requiredFields']/p");
	public By emailConfirmation = By.xpath("//button[@id='confirmChangeEmail']");
	public By modificaPasswordMenu = By.xpath("//a[text()='Modifica Password']");
	public By modificaPasswordTitle = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group form-group-full']/label");
	public By oldPassword = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group form-group-full']/div[@class='form-group']/label");
	public By oldPasswordInput = By.xpath("//input[@id='oldpassword']");
	public By newPassword = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group'][1]/label");
	public By newPasswordInput = By.xpath("//input[@id='newpassword']");
	public By confirmPassword = By.xpath("//form[@id='cambia-password-form']/div[@class='form-group'][2]/label");
	public By confirmPasswordInput = By.xpath("//input[@id='confirmpassword']");
	public By passwordHint = By.xpath("//span[@class='confirm-password-hint']");
	public By requiredFieldPsw = By.xpath("//div[@class='form-group requiredFiltersAreaClienti']/p");
	public By pswConfirmation = By.xpath("//button[@id='confirmChangePassord']");
	public By deleteRegistrationMenu = By.xpath("//a[text()='Elimina Registrazione']");
	public By deleteRegistrationText1 = By.xpath("//div[@class='elimina-profilo parbase']/div[@id='elimina-profilo']/h3");
	public By deleteRegistrationText2 = By.xpath("//div[@id='elimina-profilo']/p");
	public By continueDeleteReg = By.xpath("//div[@class='rich-text_inner color-scheme--white']/div[@class='elimina-profilo parbase']/div[@id='elimina-profilo']/div[@class='btn-wrapper']/button");
	public By faqMenu = By.xpath("//a[text()='Faq']");
	
	public By customerAreaMenu =By.xpath("//div[@class='modifica-profilo-title parbase'][8]/li/a");
	public By logOutMenu = By.xpath("//a[@class='logout-btn']");
	
	
	public ModificapwdComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	 public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
		}
	 
	 public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 15))
				throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
		}

	public void checkPopUpText(By title, By text, String property) throws Exception {

		String text1 = driver.findElement(title).getText();
		String text2 = driver.findElement(text).getText();
		
		if(! property.contains(text1) && property.contains(text2))
			throw new Exception("Pop up text is not matching with the expected value "+property);
	}
	
	public void checkFieldValue(String xpath, String property) throws Exception{

		WebDriverWait wait = new WebDriverWait(this.driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
        Thread.sleep(2000);
        System.out.println("value "+s);
        if(!s.equalsIgnoreCase(property))
              throw new Exception ("Field value is not matching with the actual value");
        
 }
	
	public void SwitchTabandClose(String qstn1, String qstn2, String qstn3, String qstn4, String qstn5) throws Exception
	{
	   
		Set <String> windows = driver.getWindowHandles();
		 String currentHandle = driver.getWindowHandle();
		
		// String qstn1 = "Chi può accedere all’area riservata di Enel?";
		for (String winHandle : windows) {
		   // Switch to child window
		//	System.out.println("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
		 
		 SupportFaqComponent sqc = new SupportFaqComponent(driver);
			
			sqc.verifyComponentExistence(sqc.pageTitle);
			util.scrollToElement(driver.findElement(sqc.qst1));
			verifyComponentExistence(sqc.qst1);
			verifyComponentExistence(sqc.qst2);
			verifyComponentExistence(sqc.qst3);
			verifyComponentExistence(sqc.qst4);
			verifyComponentExistence(sqc.qst5);
		//	System.out.println(driver.findElement(sqc.qst1).getText()+ " ");
			checkForText(sqc.qst1, qstn1);
			checkForText(sqc.qst2, qstn2);
			checkForText(sqc.qst3, qstn3);
			checkForText(sqc.qst4, qstn4);
			checkForText(sqc.qst5, qstn5);
	 		driver.close();
			driver.switchTo().window(currentHandle);

	 }
	
	public void checkForText(By checkObject, String property) throws Exception {
		
	
		String actualText = driver.findElement(checkObject).getText().trim();
		String value = actualText.replaceAll("\\s+", " ");
		String result = "NO";
		if(value.equalsIgnoreCase(property)){
			result="YES";
		}
		if(result.equals("NO"))
			throw new Exception(value+" Text is not matching with "+property);
		
	}
	
	public void SwitchTabToSupporto() throws Exception
	{
	   
		Set <String> windows = driver.getWindowHandles();
		String currentHandle = driver.getWindowHandle();
		
		for (String winHandle : windows) {
			//System.out.println("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
		    driver.switchTo().window(winHandle);
		 }
	}
	public void closeChildWindows() throws Exception {
		
		String currentHandle = driver.getWindowHandle();
		System.out.println("Parent window is "  +currentHandle);
		System.out.println("Current URL " +driver.getCurrentUrl());
		Set <String> windows = driver.getWindowHandles();
		for (String winHandle : windows) {
			
	        driver.switchTo().window(winHandle);
	        if(!winHandle.equals(currentHandle)){
	        System.out.println("switched to "+driver.getTitle()+"  ChildWindow");
	        System.out.println("Switched to Child URL " +driver.getCurrentUrl());
	        System.out.println(winHandle);
	        driver.close();
	        }
		  
		 }
	
	}
	public void checkForPlaceholderText(By checkObject, String property) throws Exception{
		
		String actualText = driver.findElement(checkObject).getAttribute("placeholder");
		System.out.println(actualText);
		
		if(! property.equalsIgnoreCase(actualText))				
				throw new Exception("Placeholder text is not matching with the value "+property);			
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{		
        if(!url.equals(driver.getCurrentUrl()))
            throw new Exception("Redirection failed! The current URL is different from the one saved before.");
    }
	
	public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}	
	
	public void enterValueInInputBox(By insertObject, String valore) throws Exception {
		util.objectManager(insertObject, util.sendKeys, valore);
	}
	
	public void containsText(By by, String text, Boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
}
