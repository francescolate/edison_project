package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class LeadACaldoComponent {
	WebDriver driver;
	SeleniumUtilities util;
	
	public By compilaModulaHeading1 = By.xpath("//section[@id='formWrapper']//descendant::h1[@class='plan-main-head']");
	public By compilaModulaHeading2 = By.xpath("//section[@id='formWrapper']//descendant::h2[@class='plan-main-head']");
	public By nome = By.xpath("//input[@id='form_nome']");
	public By cognome = By.xpath("//input[@id='form_cognome']");
	public By cellulare = By.xpath("//input[@id='form_cellulare']");
	public By InformativaHeading = By.xpath("//form[@id='form-lead-a-caldo']//div[@class='form-privacy']/label");
	public By InformativaText = By.xpath("//div[@id='testo_privacy']");
	public By accetto = By.xpath("//input[@id ='form_privacy_accept']");
	public By nonAccetto = By.xpath("//input[@id ='form_privacy_non_accept']");
	public By clickAccetto = By.xpath("//form[@id='form-lead-a-caldo']//descendant::div[@class='radio-container lead radio-inline'][1]/label");
	public By campiObligatori = By.xpath("//form[@id='form-lead-a-caldo']/div//div[@class='nota_obbligatori']/p");
	public By proseguiButton = By.xpath("//button[@id='submit-lead-a-caldo']");
	public By campoObbligatorioNome = By.xpath("//form[@id='form-lead-a-caldo']//div[1]/span");
	public By campoObbligatorioCogNome = By.xpath("//form[@id='form-lead-a-caldo']//div[2]/span");
	public By campoObbligatorioCellulare = By.xpath("//form[@id ='form-lead-a-caldo']//div[@class='form-group wrap-input form-prefix error']/span[.='Campo obbligatorio']");
	public By errorMessage = By.xpath("//div[@class='form-group form-inline']/span[@class='errorMsg']");
	public By confirmationMessageHeading = By.xpath("//div[@id = 'myContainer']//descendant::h3[contains(text(),'')]");
	public By confirmationMessageText = By.xpath("//h2[contains(text(),'Operazione')]");
	//public By tornaAllaHomeButton = By.xpath("//section[@id='step-ok']/div[@id='myContainer']//button");
	public By tornaAllaHomeButton = By.xpath("//div[@id='myContainer']//descendant::button[.='Torna alla Home']");
	
	
	public LeadACaldoComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
		
	}
	
	  public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
	  
	  public void jsClickObject(By by) throws Exception {
	        util.jsClickElement(by);
	    }
	  
	  public void enterInputParameters(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
	  
	  public void verifyComponentExistence(By existingObject) throws Exception {
			if (!util.exists(existingObject, 20))
				throw new Exception("object with xpath " + existingObject + " is not exist.");
		}
	  
	  public void comprareText(By by, String text, boolean nestedTags) throws Exception{
	        util.objectManager(by, util.scrollToVisibility, false);
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			//System.out.println(weText);
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			//System.out.println("Normalized:\n"+weText);
			//System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
		}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			if(!url.equals(driver.getCurrentUrl()))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
public static final String COMPILA_MODULA_HEADING1 = "Compila il modulo con i tuoi dati.";
public static final String COMPILA_MODULA_HEADING2 = "① Lascia i tuoi dati ② Ti ricontattiamo il prima possibile③ Ti assistiamo nell'attivazione dell'offerta che preferisciÈ semplice e gratuito!";
public static final String INFORMATIVA_HEADING = "Informativa Privacy e consensi";
public static final String INFORMATIVA_TEXT = "Enel Energia, in qualità di Titolare del trattamento, ti informa che i dati personali indicati nel presente modulo sono trattati, anche con l'ausilio di strumenti informatici, per consentire ad Enel Energia di ricontattarti come da tua richiesta e concordare un successivo appuntamento volto ad illustrarti la tipologia di offerta da te indicata.Il rilascio dei tuoi dati è presupposto indispensabile ai fini del ricontatto ed un eventuale rifiuto non consentirebbe ad Enel Energia di procedere. Per il perseguimento delle finalità sopra indicate i dati potranno essere comunicati a società del Gruppo Enel, nonchè a terzi fornitori di servizi, che li utilizzeranno quali Titolari autonomi o di Responsabili del trattamento, appositamente nominati, e potranno inoltre essere comunicati a soggetti, preposti alla gestione di alcune attività, che verranno allo scopo nominati Persone Autorizzate al trattamento.Ai fini dell'esercizio dei diritti di cui agli artt. 15-22 del Regolamento UE 679/2016, è possibile inviare una richiesta alla casella postale dedicata: privacy.enelenengia@enel.com. Titolare del trattamento dei dati è Enel Energia S.p.A. con sede legale in Viale Regina Margherita 125 - 00198 Roma, Gruppo IVA Enel P.IVA 15844561009, C.F 06655971007. Il Titolare ha designato un proprio Responsabile della Protezione dei dati personali ('RPD'), che può essere contattato all'indirizzo e-mail: dpo.enelenergia@enel.com, per tutte le questioni relative al trattamento dei dati personali e all'esercizio dei diritti.L'informativa completa sul trattamento dei dati personali è consultabile sul sito www.enel.it.";
public static final String CAMPI_OBBLIGATORI = "* Campi obbligatori";
public static final String CAMPO_OBBLIGATORIO = "Campo obbligatorio";
public static final String ERROR_MESSAGE = "Occorre accettare la informativa sulla privacy";
public static final String CONFIRMATION_MESSAGE_HEADING = "Grazie per aver lasciato i tuoi dati";
public static final String CONFIRMATION_MESSAGE_TEXT = "Operazione conclusa correttamente. Un nostro operatore ti contatterà a breve.";
//public static final String CONFIRMATION_MESSAGE_TEXT = "Si prega di riprovare più tardi.I nostri sistemi sono momentaneamente non disponibili.";


		
}
