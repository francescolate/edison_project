package com.nttdata.qa.enel.components.lightning;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

public class GestioneConsensiComponent {
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	
	
	final public By selectConsensoMarketingTelefono = By.xpath("//span[text()='Consenso Marketing Telefono']/../following-sibling::select");
	final public By selectConsensoTerziTelefono = By.xpath("//span[text()='Consenso Terzi Telefono']/../following-sibling::select");
	final public By selectConsensoMarketingCellulare = By.xpath("//span[text()='Consenso Marketing Cellulare']/../following-sibling::select");
	final public By selectConsensoTerziCellulare = By.xpath("//span[text()='Consenso Terzi Cellulare']/../following-sibling::select");
	final public By selectConsensoProfilazione = By.xpath("//span[text()='Consenso Profilazione']/../following-sibling::select");
    
	final public By buttonRevocaConsensi = By.xpath("//button[text()='Revoca Consensi']");
	
	final public By buttonAvanti = By.xpath("//button[text()='Avanti']");
	
	final public By buttonSalva = By.xpath("//button[text()='Salva']");
	
	final public By buttonForzaVocalOrder = By.xpath("//button[text()='Forza vocal order']");
	
	final public By telefonoFisso = By.xpath("//div[contains(@class,'EditContactsData')]//input[@name='telefono']");

	final public By cellulare = By.xpath("//div[contains(@class,'EditContactsData')]//input[@name='cellphone']");
	
	final public By email = By.xpath("//div[contains(@class,'EditContactsData')]//input[@name='email']");
	
	final public String radioSiXpath = "//ancestor::div[@class='slds-grid']//span[text()='Si']/preceding-sibling::span";
	
	final public String radioNoXpath = "//ancestor::div[@class='slds-grid']//span[text()='No']/preceding-sibling::span";
	
	final public By buttonConfermModificaDatiContatto = By.xpath("//button[text()='Conferma Modifica Dati Di Contatto']");
	
	final public By buttonOkModal = By.xpath("//*[text()='Modifica Consensi']/ancestor::div[@id='divHeaderB']//button[text()='Ok']");
	
	public GestioneConsensiComponent(WebDriver driver)
	{
		this.driver =driver ;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}
	
	
	public void selezionaConsensoMarketingTelefono(String consenso) throws Exception{
		util.objectManager(selectConsensoMarketingTelefono, util.select, consenso);
	}
	
	public void selezionaConsensoTerziTelefono(String consenso) throws Exception{
		util.objectManager(selectConsensoTerziTelefono, util.select, consenso);
	}
	
	public void selezionaConsensoMarketingCellulare(String consenso) throws Exception{
		util.objectManager(selectConsensoMarketingCellulare,  util.select, consenso);
	}
	
	public void selezionaConsensoTerziCellulare(String consenso) throws Exception{
		util.objectManager(selectConsensoTerziCellulare,  util.select, consenso);
	}
	
	public void selezionaConsensoProfilazione(String consenso) throws Exception{
		util.objectManager(selectConsensoProfilazione, util.select, consenso);
	}
	
	
	public void changeSelectedOption(By el) throws Exception{
		Select sel = new Select(driver.findElement(el));
		List<WebElement> list = sel.getOptions();
		for(WebElement l : list) {
			if(!l.getText().equals(sel.getFirstSelectedOption().getText())) {
				sel.selectByVisibleText(l.getText());
				break;
			}
		}
		
	}
	
	public void selezionaRadioDaRadice(WebElement el, String xpath) throws Exception{
		   WebElement capo = util.waitAndGetElement(el, By.xpath(xpath));
		   capo.click();
		   spinner.checkSpinners();
	
	}
	
	public void pressButton(By button) throws Exception{
		util.objectManager(button, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		spinner.checkSpinners();
	}
	
	public void cambiaTelefonoFisso(String telefono) throws Exception{
		WebElement el = util.waitAndGetElement(telefonoFisso);
//		el.sendKeys(telefono);
		util.objectManager(telefonoFisso, util.sendKeysCharByChar,telefono);
		selezionaRadioDaRadice(el, radioSiXpath);
	}
	
}
