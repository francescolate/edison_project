package com.nttdata.qa.enel.components.lightning;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;

import java.util.Properties;

public class SFDCBoxSubentroComponent extends BaseComponent implements SFDCBox {



    public SFDCBoxSubentroComponent(WebDriver driver) {
        super(driver);
    }

    public static By searchBtn = By.xpath("//button[@id='searchClients']");

    public By sezioni_non_confermate = By.xpath("//a/*[contains(text(), 'sezioni non confermate')]");
    public By sezioni_confermate = By.xpath("//a/*[contains(text(), '/14 sezioni confermate')]");
    public By sezioni_fase_corrente = By.xpath("//*[contains(text(), 'Fase corrente')]");

    //BOX Riepilogo offerta
    String container_rieoff_string = sdfc_box_by_title_text.replaceFirst("##", "Riepilogo offerta");

    public By container_rieoff = By.xpath(container_rieoff_string);
    public By rieoff_error_banner = By.xpath(container_rieoff_string + sfdc_banner_error_by_button_x);


    //BOX Modalità firma e Canale Invio
    String container_mfeci_string = sdfc_box_by_title_text.replaceFirst("##", "Modalità firma e Canale Invio");
    String label_ci = label_by_text.replaceFirst("##", "Canale Invio");
    String label_mf = label_by_text.replaceFirst("##", "Modalità Firma");

    public By container_mfeci = By.xpath(container_mfeci_string);
    public By mfeci_input_modfi = By.xpath(container_mfeci_string + label_mf + input_in_any_parent + any_parent);
    public By mfeci_pickuplist_modfi_no_vocal = By.xpath(container_mfeci_string + label_mf + any_parent + any_with_text.replaceFirst("##", "NO VOCAL"));
    public By mfeci_input_caninvio = By.xpath(container_mfeci_string + label_ci + input_in_any_parent + any_parent);
    public By mfeci_pickuplist_caninvio_no_vocal = By.xpath(container_mfeci_string + label_ci + any_parent + any_with_text.replaceFirst("##", "STAMPA LOCALE"));
    public By mfeci_button_conferma = By.xpath(container_mfeci_string + button_conferma);

    //BOX Contatti e consensi
    String container_cec_string = sdfc_box_by_title_text.replaceFirst("##", "Contatti e consensi");
    String label_cp = label_by_text.replaceFirst("##", "Consenso Profilazione");
    String label_cmt = label_by_text.replaceFirst("##", "Consenso Marketing Telefonico");
    String label_cmc = label_by_text.replaceFirst("##", "Consenso Marketing Cellulare");
    String label_cme = label_by_text.replaceFirst("##", "Consenso Marketing Email");
    String label_ctt = label_by_text.replaceFirst("##", "Consenso Terzi Telefonico");
    String label_ctc = label_by_text.replaceFirst("##", "Consenso Terzi Cellulare");
    String label_cte = label_by_text.replaceFirst("##", "Consenso Terzi Email");

    public By container_cec = By.xpath(container_cec_string);
    public By cec_input_consenso_prof = By.xpath(container_cec_string + label_cp + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_acquisto = By.xpath(container_cec_string + label_cp + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_input_consenso_martel = By.xpath(container_cec_string + label_cmt + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_martel_acquisito = By.xpath(container_cec_string + label_cmt + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_input_consenso_marcel = By.xpath(container_cec_string + label_cmc + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_marcel_acquisito = By.xpath(container_cec_string + label_cmc + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_input_consenso_mareml = By.xpath(container_cec_string + label_cme + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_mareml_acquisito = By.xpath(container_cec_string + label_cme + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_input_consenso_tertel = By.xpath(container_cec_string + label_ctt + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_tertel_acquisito = By.xpath(container_cec_string + label_ctt + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_input_consenso_tercel = By.xpath(container_cec_string + label_ctc + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_tercel_acquisito = By.xpath(container_cec_string + label_ctc + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_input_consenso_tereml = By.xpath(container_cec_string + label_cte + input_in_any_parent + not_disabled_value);
    public By cec_pickuplist_tereml_acquisito = By.xpath(container_cec_string + label_cte + any_parent + any_with_text.replaceFirst("##", "ACQUISITO"));
    public By cec_button_conferma = By.xpath(container_cec_string + button_conferma);

    //BOX Carrello
    String container_car_string = sdfc_box_by_title_text.replaceFirst("##", "Carrello");
    String car_button_add_cart_by_name = "//*[text()='##']/ancestor::div[@id='itemIntoCart']//div[@id='addbutton']/button";
    String menu_item_by_text = "//div[text()='##']";
    String pop_up_by_text = "//h2[text()='##']" + any_parent + any_parent;
    String car_pop_up_checkout_string = pop_up_by_text.replaceFirst("##", "Checkout");
    String banner_coe = "//*[normalize-space(text())='Check Out effettuato']";

    public By container_car = By.xpath(container_car_string);
    public By car_menu_elbu = By.xpath(container_car_string + menu_item_by_text.replaceFirst("##", "Elettrico - Business"));
    public By car_button_add_cart_gxti = By.xpath(container_car_string + car_button_add_cart_by_name.replaceFirst("##", "GiustaXte Impresa"));
    public By car_button_add_bnststca = By.xpath(container_car_string + car_button_add_cart_by_name.replaceFirst("##", "Bonus_Test Catalogo"));
    public By car_button_salva = By.xpath(container_car_string + button_salva);
    public By car_button_checkout = By.xpath(container_car_string + button_checkout);
    public By car_pop_up_checkout = By.xpath(car_pop_up_checkout_string);
    public By car_pop_up_checkout_button_conferma = By.xpath(car_pop_up_checkout_string + button_conferma);
    public By car_banner_coe = By.xpath(sdfc_box_by_title_text_2.replaceFirst("##", "Carrello") + banner_coe);

    //BOX CIG e CUP
    String container_cicu_string = sdfc_box_by_title_text.replaceFirst("##", "CIG e CUP");
    String label_f136 = label_by_text.replaceFirst("##", "Flag 136");

    public By container_cicu = By.xpath(container_cicu_string);
    public By cicu_input_flag_136 = By.xpath(container_cicu_string + label_f136 + input_in_any_parent + any_parent);
    public By cicu_pickuplist_no = By.xpath(container_cicu_string + any_with_text.replaceFirst("##", "NO"));
    public By cicu_button_conferma = By.xpath(container_cicu_string + button_conferma);


    //BOX Split Payment
    String container_sp_string = sdfc_box_by_title_text.replaceFirst("##", "Split Payment");
    String label_sp = label_by_text.replaceFirst("##", "Split Payment");

    public By container_sp = By.xpath(container_sp_string);
    public By sp_input_split_payment = By.xpath(container_sp_string + label_sp + input_in_any_parent /*+ any_parent*/);
    public By sp_pickuplist_no = By.xpath(container_sp_string + any_with_text.replaceFirst("##", "No"));
    public By sp_button_conferma = By.xpath(container_sp_string + button_conferma);

    //BOX SCONTI E BONUS
    String container_seb_string = sdfc_box_by_title_text.replaceFirst("##", "Sconti e Bonus");
    String label_cc = label_by_text.replaceFirst("##", "Codice Campagna");

    public By container_seb = By.xpath(container_seb_string);
    public By seb_input_codice_compagnia = By.xpath(container_seb_string + label_cc + input_in_any_parent);
    public By seb_input_codice_compagnia_populated = By.xpath(container_seb_string + label_cc + input_in_any_parent);
    public By seb_button_conferma = By.xpath(container_seb_string + button_conferma);

    //BOX METODO DI PAGAMENTO
    String container_mdp_string = sdfc_box_by_title_text.replaceFirst("##", "Metodo di pagamento");
    String radio_by_text = "//div[text()='##']/ancestor::tr//label";

    public By container_mdp = By.xpath(container_mdp_string);
    public By mdp_radio_bollettino_postale = By.xpath(container_mdp_string + radio_by_text.replaceFirst("##", "Bollettino Postale"));
    public By pop_up_button_ok = By.xpath("//button[text()='Ok']/ancestor::slot[@name='footer']//button");
    public By mdp_button_conferma = By.xpath(container_mdp_string + button_conferma);

    //BOX FATTURAZIONE ELETTRONICA
    String container_fe_string = sdfc_box_by_title_text.replaceFirst("##", "Fatturazione Elettronica");
    String label_cu = label_by_text.replaceFirst("##", "Codice Ufficio");
    String label_div = label_by_text.replaceFirst("##", "Data Inizio Validità");
    String label_dfv = label_by_text.replaceFirst("##", "Data Fine Validità");

    public By container_fe = By.xpath(container_fe_string);
    public By fe_pickuplist_canale_invio = By.xpath(container_fe_string + label_ci + input_in_any_parent + any_parent + disabled_value);
    public By fe_pickuplist_canale_invio_sdi = By.xpath(container_fe_string + any_by_text_in_any_parent.replaceFirst("##", "SDI"));
    public By fe_input_cu = By.xpath(container_fe_string + label_cu + input_in_any_parent + disabled_value);
    public By fe_valore_corto = By.xpath(container_fe_string + any_with_text.replaceFirst("##", "Il valore inserito è troppo corto"));
    public By fe_button_conferma = By.xpath(container_fe_string + button_conferma);
    public By fe_input_data_inizio_validita = By.xpath(container_fe_string + label_div + input_in_any_parent);
    public By fe_input_data_inizio_validita_disabled = By.xpath(container_fe_string + label_div + input_in_any_parent + disabled_value);
    public By fe_input_data_fine_validita = By.xpath(container_fe_string + label_dfv + input_in_any_parent);
    public By fe_input_data_fine_validita_disabled = By.xpath(container_fe_string + label_dfv + input_in_any_parent + disabled_value);

    //BOX INDIRIZZO DI FATTURAZIONE
    String container_idf_string = sdfc_box_by_title_text.replaceFirst("##", "Indirizzo di Fatturazione");
    String label_reg = label_by_text.replaceFirst("##", "Regione");
    String label_pro = label_by_text.replaceFirst("##", "*Provincia");
    String label_com = label_by_text.replaceFirst("##", "*Comune");
    String label_loc = label_by_text.replaceFirst("##", "Località");
    String label_ind = label_by_text.replaceFirst("##", "*Indirizzo");
    String label_nuc = label_by_text.replaceFirst("##", "*Numero Civico");
    String label_cap = label_by_text.replaceFirst("##", "CAP");

    public By container_idf = By.xpath(container_idf_string);
    public By idf_banner_indirizzo = By.xpath(container_idf_string + any_with_text.replaceFirst("##", "Indirizzo di Residenza/Sede Legale"));
    public By idf_button_conferma = By.xpath(container_idf_string + button_conferma);
    public By idf_tab_verifica = By.xpath(container_idf_string + any_with_text.replaceFirst("##", "Verifica Indirizzi Esistenti"));
    public By idf_first_checkbox_showed = By.xpath(container_idf_string + first_check_box_show);
    public By idf_button_importa = By.xpath(container_idf_string + button_importa);
    public By idf_input_regione = By.xpath(container_idf_string + label_reg + input_in_any_parent);
    public By idf_input_regione_disabled = By.xpath(container_idf_string + label_reg + input_in_any_parent + disabled_value);
    public By idf_input_provincia = By.xpath(container_idf_string + label_pro + input_in_any_parent);
    public By idf_input_comune = By.xpath(container_idf_string + label_com + input_in_any_parent);
    public By idf_input_localita = By.xpath(container_idf_string + label_loc + input_in_any_parent);
    public By idf_input_indirizzo = By.xpath(container_idf_string + label_ind + input_in_any_parent);
    public By idf_input_ncivico = By.xpath(container_idf_string + label_nuc + input_in_any_parent);
    public By idf_input_cap = By.xpath(container_idf_string + label_cap + input_in_any_parent);
    public By idf_input_cap_disabled = By.xpath(container_idf_string + label_cap + input_in_any_parent + disabled_value);
    public By idf_button_verifica = By.xpath(container_idf_string + button_verifica);
    public By idf_button_forza = By.xpath(container_idf_string + button_forza);
    public By idf_bannerChiaviFonetiche = By.xpath("//span[text()='CANDIDATO ESTRATTO CON LE CHIAVI FONETICHE']");

    public By textVerifica = By.xpath("//*[@id='spanFocusX']");
    public By inputValueForCup = By.xpath("//span[@class='slds-lookup__item-action slds-media slds-media--center']");

    public By idf_indirizzo_verificato = By.xpath(container_idf_string + any_with_text.replaceFirst("##", "Indirizzo verificato"));
    public By idf_indirizzo_non_verificato = By.xpath(container_idf_string + any_with_text.replaceFirst("##", "Indirizzo non verificato:"));
    public By idf_indirizzo_non_verificato_generic = By.xpath(container_idf_string + any_with_contains_text.replaceFirst("##", "Indirizzo non verificato:"));
    public By spanForzaIndirizzo = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']");

    //BOX COMMODITY
    String container_commodity_string = sdfc_box_by_title_text.replaceFirst("##", "Commodity");
    String label_cms = label_by_text.replaceFirst("##", "Categoria Merceologica SAP");
    String label_res = label_by_text.replaceFirst("##", "Residente");
    String label_ofi = label_by_text.replaceFirst("##", "Ordine Fittizio");

    String label_tit = label_by_text.replaceFirst("##", "Titolarita’");

    String label_tdi = label_by_text.replaceFirst("##", "Telefono Distributore");
    String label_asc = label_by_text.replaceFirst("##", "Ascensore");
    String label_dis = label_by_text.replaceFirst("##", "Disalimentabilita’");
    String label_can = label_by_text.replaceFirst("##", "Consumo Annuo");
    String pickuplist_no = any_by_text_in_any_parent.replaceFirst("##", "NO");
    String pickuplist_si = any_by_text_in_any_parent.replaceFirst("##", "SI");
    String pickuplist_al_se = any_by_text_in_any_parent.replaceFirst("##", "ALTRI SERVIZI");

    public By container_comm = By.xpath(container_commodity_string);
    public By comm_radio_elettrico = By.xpath(container_commodity_string + "//div[text()='ELETTRICO']/ancestor::tbody//label");
    public By comm_pickuplist_cms = By.xpath(container_commodity_string + label_cms + input_in_any_parent);
    public By comm_pickuplist_cms_al_se = By.xpath(container_commodity_string + label_cms + pickuplist_al_se);


    public By comm_pickuplist_residente = By.xpath(container_commodity_string + label_res + input_in_any_parent);
    public By comm_pickuplist_ofi = By.xpath(container_commodity_string + label_ofi + input_in_any_parent + disabled_value);


    public By comm_pickuplist_tit = By.xpath(container_commodity_string + label_tit + input_in_any_parent + disabled_value);


    public By comm_pickuplist_ofi_no = By.xpath(container_commodity_string + label_ofi + pickuplist_no);
    public By comm_input_tdi_dist = By.xpath(container_commodity_string + label_tdi + input_in_any_parent);
    public By comm_pickuplist_asc = By.xpath(container_commodity_string + label_asc + input_in_any_parent);
    public By comm_asc_pickuplist_no = By.xpath(container_commodity_string + label_asc + pickuplist_no);
    public By comm_pickuplist_dis = By.xpath(container_commodity_string + label_dis + input_in_any_parent);
    public By comm_dis_pickuplist_si = By.xpath(container_commodity_string + label_dis + pickuplist_si);
    public By comm_input_consumo_annuo = By.xpath(container_commodity_string + label_can + input_in_any_parent);
    public By comm_button_conferma_fornitura = By.xpath(container_commodity_string + button_conferma_fornitura);
    public By comm_cell_confermata_si = By.xpath(container_commodity_string + "//*[text()='ELETTRICO']/ancestor::tbody//*[text()='SI']");
    public By comm_span_sezioni_non_confermate = By.xpath("//span[contains(text(),'sezioni non confermate')]");
    public By comm_button_conferma = By.xpath(container_commodity_string + button_conferma);

    //BOX Indirizzo di Residenza/Sede Legale
    String container_irsl_string = sdfc_box_by_title_text.replaceFirst("##", "Indirizzo di Residenza/Sede Legale");
    public By container_irsl = By.xpath(container_irsl_string);
    public By irsl_indirizzo_verificato = By.xpath(container_irsl_string + any_with_text.replaceFirst("##", "Indirizzo verificato"));
    public By irsl_button_conferma = By.xpath(container_irsl_string + button_conferma);


    //BOX Referenti
    String container_referente_string = sdfc_box_by_title_text.replaceFirst("##", "Referente");

    public By container_referente = By.xpath(container_referente_string);
    public By container_referente_button_conferma = By.xpath(container_referente_string + button_conferma);

    //BOX Seleziona uso e fornitura
    String container_suf_string = sdfc_box_by_title_text.replaceFirst("##", "Selezione Uso Fornitura");
    String label_uf = label_by_text.replaceFirst("##", "Uso Fornitura");

    public By container_suf = By.xpath(container_suf_string);
    public By suf_pickuplist_uf = By.xpath(container_suf_string + label_uf + input_in_any_parent + any_parent + any_parent);
    //	public By suf_pickuplist_uf_uda = By.xpath(container_suf_string + any_with_text.replaceFirst("##", "Uso Diverso da Abitazione"));
    public By suf_pickuplist_uf_uda = By.xpath(container_suf_string + label_uf + any_by_text_in_any_parent.replaceFirst("##", "Uso Diverso da Abitazione"));
    public By suf_pickuplist_uf_ua = By.xpath(container_suf_string + label_uf + any_by_text_in_any_parent.replaceFirst("##", "Uso Abitativo"));
    public By suf_button_conferma = By.xpath(container_suf_string + button_conferma);

    //BOX Cvp
    String container_cvp_string = sdfc_box_by_title_text.replaceFirst("##", "CVP");
    String label_ecvpl = label_by_text.replaceFirst("##", "Esito CVP Light");
    String label_pincmindl = label_by_text.replaceFirst("##", "Presa in Carico MIND Light");
    String label_damo = label_by_text.replaceFirst("##", "Data morosità");
    String label_demicvp = label_by_text.replaceFirst("##", "Descr Microcausale CVP");
    String label_esvbl = label_by_text.replaceFirst("##", "Esito VBL");
    String label_idprcvp = label_by_text.replaceFirst("##", "Id Pratica CVP");
    String label_immo = label_by_text.replaceFirst("##", "Importo Morosità");
    String label_cacvp = label_by_text.replaceFirst("##", "Causale CVP");

    public By cvp_input_ecvpl = By.xpath(container_cvp_string + label_ecvpl + input_in_any_parent);
    public By cvp_input_pincmindl = By.xpath(container_cvp_string + label_pincmindl + input_in_any_parent);
    public By cvp_input_damo = By.xpath(container_cvp_string + label_damo + input_in_any_parent);
    public By cvp_input_demicvp = By.xpath(container_cvp_string + label_demicvp + input_in_any_parent);
    public By cvp_input_esvbl = By.xpath(container_cvp_string + label_esvbl + input_in_any_parent);
    public By cvp_input_idprcv = By.xpath(container_cvp_string + label_idprcvp + input_in_any_parent);
    public By cvp_input_immo = By.xpath(container_cvp_string + label_immo + input_in_any_parent);
    public By cvp_input_cacvp = By.xpath(container_cvp_string + label_cacvp + input_in_any_parent);

    public By container_cvp = By.xpath(container_cvp_string);
    public By cvp_button_conferma = By.xpath(container_cvp_string + button_conferma);





    public void sezioniNonConfermateTextIsValue(String text) throws Exception {
        try {
            verifyComponentExistence(new ByChained(sezioni_non_confermate, By.xpath(text_is_value.replaceFirst("##", text))));
        } catch (Exception e) {
            throw new Exception("The down counter of the not-confirmed section doesn't work , the difference beetween cofermation is not (-1).");
        }
    }


    public By pickuplistByLabelText(By parent, String pickup_label_text) {
        return new ByChained(parent, By.xpath(label_by_text.replaceFirst("##", pickup_label_text) + input_in_any_parent + not_disabled_value + any_parent));
    }

    public By boxByTitleText(By parent, String box_title_text) {
        return new ByChained(parent, By.xpath(sdfc_box_by_title_text.replaceFirst("##", box_title_text)));
    }

    public boolean selectPickUpList(Properties prop, String key, By pickup_input, By pickup_choice) throws Exception {
        if (!prop.getProperty(key).isEmpty()) {
            try {
                if (clickComponentIfExist(pickup_input)) {
                    verifyComponentExistence(pickup_choice);
                    clickComponent(pickup_choice);
                    return true;
                }
            } catch (Exception e) {
                throw new Exception("Error in selecting Pickuplist :" + key, e.fillInStackTrace());
            }
        }
        return false;
    }


}
