package com.nttdata.qa.enel.components.lightning;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.util.StringUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.components.colla.BaseComponent;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;
import org.openqa.selenium.JavascriptExecutor;


public class CompilaIndirizziComponent extends BaseComponent{
	public  final By spanVerificaIndirizzo = By.id("spanFocusYServiceAddressChange");
	final public By buttonVerificaAddress = By.xpath("//div[@id='DivmodalModifyAddressABC']//button[text()='Verifica']");
	WebDriver driver;
	SeleniumUtilities util;
	SpinnerManager spinner;
	//Logger //LOGGER = Logger.getLogger("");

	//	final By tabBillingAddress = new By.ByXPath("//div[@id='j_id0:formId:billingAddress']");
	//	final By tabResidentialAddress = new By.ByXPath("//div[@id='ResidentialAddressSection']");

	final public By verificaInd = new By.ByXPath("//button[@id='componentAddress1:verifyAddrBtn']");
	final public By verificaInd2 = new By.ByXPath("//button[@id='componentAddress2:verifyAddrBtn']");
	final public By verificaInd3 = new By.ByXPath("//button[@id='componentAddress3:verifyAddrBtn']");
	//    final public By clienteFornisceIndirizzoSelect = By.xpath("//label[contains(text(),'Cliente fornisce l’indirizzo')]/following::select");
	final public By clienteFornisceIndirizzoSelect = By.xpath("//label[contains(text(),'Cliente fornisce l’indirizzo')]/ancestor::div[@role='none']//input");
	final public By clienteFornisceIndirizzoSelectDisabilitato = By.xpath("//label[contains(text(),'Cliente fornisce l’indirizzo')]/ancestor::div[@role='none']//input[@class='slds-input readonly']");
	final public By indirizziEsistenti = new By.ByXPath("//a[text()='Verifica Indirizzi Esistenti']");
	final public By indirizziEsistentiTabBill = new By.ByXPath("//div[@id='j_id0:formId:billingAddress']//a[@role='tab' "
			+ "and text()='Verifica Indirizzi Esistenti']");

	final public By indirizziEsistentiTabResidenza = new By.ByXPath("//div[@id='ResidentialAddressSection']"
			+ "//a[@role='tab' and text()='Verifica Indirizzi Esistenti']");

	final public By checkBoxIndirizzoResidenza = new By.ByXPath("//div[@aria-labelledby='existingAddresses__item']"
			+ "//label[@class='slds-checkbox'][1]");

	//div[@aria-labelledby='existingAddresses__item']//label[@class='slds-checkbox'][1]

	final public By importaBtn = new By.ByXPath("//button[text()='Importa']");
	final public By verificaBtn =  new By.ByXPath("//button[text()='Verifica']");
	final public By verificaBtnBill = new By.ByXPath("//div[@aria-labelledby='newAddress__item']"
			+ "//button[text()='Verifica']");

	final public By verificaBtnRes = new By.ByXPath("//div[@id='ResidentialAddressSection']"
			+ "//button[text() = 'Verifica']");

	final public By verificaBtnAddresKit = new By.ByXPath("//div[@id='addressComponentSendKit']"
			+ "//button[text() = 'Verifica']");
	final public By verificaBtnSedeLegale=new By.ByXPath("//div[@id='addressComponentResidential']//button[text() = 'Verifica']");

	final public By confermaIndirizzoVerificatoBill = new By.ById("spanFocusYaddressComponentBilling");

	final public By spanIndirizzoConfermato =  spanVerificaIndirizzo;

	final public By confermaIndirizzoVerificatoRes = new By.ById("spanFocusYResidentialAddressSection");

	final public By confermaIndirizzoVerificatoKit = new By.ById("spanFocusYaddressComponentSendKit");
	final public By confermaIndirizzoVerificatoSedeLegale = new By.ById("spanFocusYaddressComponentResidential");
	final public By conferma = By.xpath("//button[text()='Conferma'] | //input[contains(@id,'confermaButtonTop')]");
	final public By confermaIndirizzoEsecuzioneLavori = By.xpath("//span[text()='Indirizzo Esecuzioni Lavori']/ancestor::div[@role='region']//button[text()='Conferma']");
	final public By conferma2 = By.xpath("//input[contains(@id,'confermaButtonTop')] | //button[text()='Conferma' and contains(@onclick,'processSave')]");

	final public By avantiIndirizzoVerificatoBill = new By.ById("billingAddressNextBtn");

	final public By avantiIndirizzoVerificatoRes = new By.ById("checkResAddressButton");

	final public By avantiIndirizzoVerificatoKit = new By.ById("envAddNext");

	final public By avantiIndirizzoVerificatoKitVoltura = new By.ById("nextKitShipping_Button");
	final public By avantiIndirizzoVerificatoVoltura = new By.ById("nextResidence_Button");
	final public By avantiIndirizzoVerificatoVolturaAccollo = By.xpath("//button[text()='Avanti' and @id='confirmModeSignButton']");

	public By avantiButton = By.xpath("//div[text()='Indirizzo di Fatturazione']/parent::div//button[@alt='Avanti' and contains(@id,'next')]");
	public By avantiButton2 = By.xpath("//button[@id='buttonNext']");
	public By avantiButton3 = By.xpath("//button[contains(@id,'next')]");
    public By closeButtonAlert = By.xpath("//div[@id='RedAlerts']//button");
    public By indirizzoForzatoRadioButton = By.xpath("//input[@id='sedeLegaleComponent:forceAddrChk']/..");
	final public By selezioneFornisceIndirizzoSINO = new By.ById("j_id0:formId:providedAddressId");
	final public By selezioneClienteFornisceIndirizzo = new By.ByXPath("//span[contains(text(),'Cliente fornisce') and contains(text(),'indirizzo')]/ancestor::div[1]//select");
	final public By selezioneDichiarazioneResidenza = new By.ByXPath("//label[text()='Cliente Dichiara Residenza']/parent::span//select");

	final public By regioneDomicilio = new By.ById("componentAddress1:regionInput");
	final public By capDomicilio = new By.ById("componentAddress1:zipCodeInput");
	final public By regioneDomicilio2 = new By.ById("componentAddress2:regionInput");
	final public By capResidenza = new By.ById("componentAddress2:zipCodeInput");
	final public By capSedeLegale = new By.ById("componentAddress3:zipCodeInput");
	final public By sedeLegale = new By.ByXPath("//input[@id='sedeLegaleComponent:zipCodeInput']");
	final public By sedeLegaleSpan = new By.ByXPath("//input[@id='sedeLegaleComponent:zipCodeInput']/../following::li//tbody");
	final public By street = new By.ById("precheckDetailAddressComponent:streetNameInput");
	final public By province = new By.ById("precheckDetailAddressComponent:provinceInput");
	final public By civico = new By.ById("precheckDetailAddressComponent:streetNumberInput");
	final public By city = new By.ById("precheckDetailAddressComponent:cityInput");
	final public By street2 = new By.ByXPath("//input[contains(@id,'streetNameInput') and @type='text']");
	final public By province2 = new By.ByXPath("//input[contains(@id,'provinceInput') and @type='text']");
	final public By civico2 = new By.ByXPath("//input[contains(@id,'streetNumberInput') and @type='text']");
	final public By city2 = new By.ByXPath("//input[contains(@id,'cityInput') and @type='text']");
	final public By addressCheckBtn = new By.ById("precheckDetailAddressComponent:verifyAddrBtn");

		private final String provinciaXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'provinceInput') and @type='text']";
		private final String comuneXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'cityInput') and @type='text']";
		private final String indirizzoXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'streetNameInput') and @type='text']";
		private final String civicoXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//div[contains(@id,'$')]//input[contains(@id,'streetNumberInput') and @type='text']";
//	private final String provinciaXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//input[contains(@id,'provinceInput') and @type='text']";
//	private final String comuneXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//input[contains(@id,'cityInput') and @type='text']";
//	private final String indirizzoXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//input[contains(@id,'streetNameInput') and @type='text']";
//	private final String civicoXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//input[contains(@id,'streetNumberInput') and @type='text']";

	private final String telefonoClienteXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//input[contains(@id,'telefonoCliente') and @type='text']";

	private final String verificaIndirizzoButtonXPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//button[contains(@id,'verifyAddrBtn')][1]";
	private final String verificaIndirizzoButtonXPath2 = "//button[contains(@id,'componentAddress2:verifyAddrBtn')]";
	private final String indirizzoVerificato1XPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//fieldset/label[text()='Domicilio']/..//div[contains(@id,'verifiedAddrTick')]";

	private final String indirizzoVerificato2XPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//fieldset/label[text()='Residenza']/..//div[contains(@id,'verifiedAddrTick')]";
	private final String indirizzoVerificato3XPath = "//h2[text()='#']/ancestor::div[@class='slds-card']//fieldset/label/..//div[contains(@id,'verifiedAddrTick')]";

	private final String indirizzoVerificatoSedeLegaleXpath = "//h2[text()='Sede Legale e Contatti']/ancestor::div[@class='slds-card']//div[contains(@id,'verifiedAddrTick')]";
	//private final By indirizzoVerificato2 = By.id("componentAddress2:verifiedAddrTick");
	private final By copiaIndirizzo = By.xpath("//input[@id='CopiaIndirizzo']/following-sibling::*");
	public final By copiaDaIndirizzoDiResidenzaLink = By.xpath("//a[@class='acceleretorLinkResidentialAddress' and contains(text(),'residenza')]");
	public final By copiaDaIndirizzoDiResidenzaLink2 = By.xpath("//a[@class='acceleretorLinkBillingAddress' and contains(text(),'residenza')]");
	public final By copiaDaIndirizzoDiFornituraLink = By.xpath("//a[contains(text(),'indirizzo di fornitura')]");
	public final By copiaDaIndirizzoSedeLegale = By.xpath("//a[contains(text(),'sede legale')]");
	public By inputPresso=By.xpath("//label[contains(text(),'Presso')]/../input");
	public By inputPressoSpan=By.xpath("//label[text()='Comune']/../following::li[@data-record='0']");
	public By forzaIndirizzoSedeLegele = By.xpath("//button[@id='sedeLegaleComponent:forceAddrBtn']");
	//  public By inputProvinciaIndFattur=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//label[contains(text(),'Provincia')]/../input");
	public By inputProvinciaIndFattur=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../input");
	public By selectVia = By.xpath("//select[@Id='sedeLegaleComponent:dugInput']");
	public By inputProvincia=By.xpath("//label[contains(text(),'Provincia')]/../input");
	public By inputProvinciaSpan=By.xpath("//label[contains(text(),'Provincia')]/../following::li[@data-record='0']");
	public By closeAlertButton = By.xpath("//button[@id='hmbBannerButton']");
	//  public By inputComuneIndFattur=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//label[contains(text(),'Comune')]/../input");
	public By inputComuneIndFattur=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../input");
	public By inputComune=By.xpath("//label[contains(text(),'Comune')]/../input");
	public By inputComuneSpan=By.xpath("//label[contains(text(),'Comune')]/../following::li[@data-record='0']");

	public By inputLocalita=By.xpath("//label[contains(text(),'Localit')]/../input");
	public By inputLocalitaSpan=By.xpath("//label[contains(text(),'Localit')]/../following::li[@data-record='0']");


	//  public By inputIndirizzoFatturazione=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//label[contains(text(),'Indirizzo')]/../input");
	public By inputIndirizzoFatturazione=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Indirizzo')]/../input");

	public By inputIndirizzo=By.xpath("//label[contains(text(),'Indirizzo')]/../input");
	public By inputIndirizzoSpan=By.xpath("//label[contains(text(),'Indirizzo')]/../following::li[@data-record='0']");

	// public By inputCivicoIndFatturazione= By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//label[contains(text(),'Numero Civico')]/../div/input[@name='civic']");
	public By inputCivicoIndFatturazione= By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Numero Civico')]/../div/input[@name='civic']");
	public By inputCivico=By.xpath("//label[contains(text(),'Numero Civico')]/../div/input[@name='civic']");
	public By inputCivicoSpan=By.xpath("//label[contains(text(),'Numero Civico')]/../following::li[@data-record='0']");
	public By inputCap2=By.xpath("//label[contains(text(),'CAP')]/ancestor::div/input");

	public By inputCap=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../input");
	public By inputCapEsecuzioneLavori = By.xpath("//label[contains(text(),'CAP')]/../input");
	public By capSpan=By.xpath("//label[contains(text(),'CAP')]/../following::li[@data-record='0']");

	public By inputCapRes=By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[text()='CAP']/../input");
    public By capSpanRes=By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']");
	
	public By indirizzoForzatoButtonEsecuzioneLavori = By.xpath("//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']");

	public By indirizzoForzatoButton = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"); 
	public By indirizzoForzatoButtonRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span/span[@class='slds-checkbox_faux']"); 
	public By forzaIndirizzoButton = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
    public By forzaIndirizzoButtonRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']");
	public By buttonIndirizzoNonForzato = By.xpath("//span[text()='Indirizzo Non forzato']/ancestor::span");
	public By forzaIndirizzoButtonInserimentoFornitura = By.xpath("//button[text()='Forza Indirizzo']");
	public By forzaIndirizzoEsecuzioneLavoriButton = By.xpath("//button[text()='Forza Indirizzo']");
	public By inputCivico2=By.xpath("//span[contains(text(),'Numero Civico')]/ancestor::div[1]/input");
	public By inputScala=By.xpath("//label[contains(text(),'Scala')]/../div/input[@name='stairNr']");
	public By inputPiano=By.xpath("//label[contains(text(),'Piano')]/../div/input[@name='floorNr']");
	public By inputInterno=By.xpath("//label[contains(text(),'Interno')]/../div/input[@name='apartmentNr']");
	public By buttonVerifica=By.xpath("//button[contains(text(),'Verifica')]");
	public By buttonVerificaIndirizzoFatturazione=By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[contains(text(),'Verifica')]");
	public By buttonVerificaVolturaAccollo=By.xpath("//div[@id='focusOnThisaddressComponentSendKit']/..//button[contains(text(),'Verifica')]");
	public By spanEsitoVerificaIndirizzo=By.xpath("//span[@id='spanFocusYaddressDefaultChannel']");
	public By buttonModificaIndizzoResidenza = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Modifica Indirizzo']");
	public By buttonModificaIndizzoResidenzaAbilitato = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//div[@class='false slds-p-around_small']/button[text()='Modifica Indirizzo']");
	public By buttonConfermaIndizzoResidenza = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By buttonConfermaIndizzoResidenzaDisabilitato = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Conferma' and @disabled='true']");
	//h2//span[text()='"+sezione+"']/ancestor::div[@role='main']//button[text()='Conferma']
	public By simulaAvantiButton = By.xpath("//button[@alt='Avanti' and contains(@onclick,'simulaclick')]");
	public By avantiComponentlightningButton = By.xpath("//button[text()='Avanti' and contains(@onclick,'clickButton')]");

	public By inputResidentialProvince = By.xpath("//input[@id='residentialAddress:provinceInput']");
	public By inputResidentialCity = By.id("residentialAddress:cityInput");
	public By inputResidentialStreet = By.id("residentialAddress:streetNameInput");
	public By inputResidentialStreetNumber = By.id("residentialAddress:streetNumberInput");

	public By TabverificaIndirizziEsistenti = By.xpath("//li[@title='Verifica Indirizzi Esistenti']/a");

	public By primoIndirizzoEsistente = By.xpath("//div[@aria-labelledby='existingAddresses__item']"
			+ "//label[@class='slds-checkbox'][1]");

	public By btnImporta = By.xpath("//button[text()='Importa']");

	public By btnConfermaCoordinateGeografiche=By.xpath("//span[text()='Coordinate geografiche con TP']/ancestor::div[2]//button");

	public By spanIndirizzoVerificato = By.xpath("//span[text()='Indirizzo verificato']");

	public final By buttonVerificaIndirizzo = By.xpath("//button[text()='Verifica Indirizzo']");

	public final By tabellaIstat = By.xpath("//table[@role='table' and(contains(@aria-label,'Tabella delle intestazioni'))]");
	public final By primoIstat=By.xpath("//table[@role='table']//tbody/tr[1]/td[1]//label/span[@class='slds-radio_faux']");
	public final By confermaIstat=By.xpath("//button[@name='Conferma Istat']");

	private final By provinciaInput = By.xpath("//label[@id='addressComponentBillingProvincia']/following-sibling::input");
	private final By provinciaDropDownMenu = By.xpath("//span[contains(text(), 'SALERNO')]/../../../../..");
	private final By comuneInput = By.xpath("//label[@id='addressComponentBillingComune']/following-sibling::input");
	private final By viaInput = By.xpath("//label[@id='addressComponentBillingIndirizzo']/following-sibling::input");
	private final By viaDropDownMenu = By.xpath("//span[contains(text(), 'VIA QUASIMODO')]/..");
	private final By civicoInput = By.xpath("//label[contains(text(), 'Numero Civico')]/following-sibling::div/input");
	private final By verifica = By.xpath("//div[@id='focusOnThisaddressComponentBilling']/following-sibling::div/div/button[contains(text(), 'Verifica')]");
	private final By avanti = By.xpath("//button[@id='nextBillingAddress_Button']");
	//    public final By confermaIndirizzoResidenzaSWAEVO = By.xpath("//div[@aria-label='Indirizzo di Residenza/Sede Legale']//button[text()='Conferma']");
	public final By confermaIndirizzoResidenzaSWAEVO = By.xpath("//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Conferma']");
	public final By verificaIndirizzoResidenzaSWAEVO = By.xpath("//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Verifica']");
	public final By verificaIndirizzoFatturazioneEVO = By.xpath("//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Verifica']");
	//    public final By confermaIndirizzoFatturazioneSWAEVO = By.xpath("//div[@aria-label='Indirizzo di Fatturazione']//button[text()='Conferma']");
	public final By confermaIndirizzoFatturazioneSWAEVO = By.xpath("//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Conferma']");
	//    public final By confermaIndirizzoSpedizionePlicoSWAEVO = By.xpath("//div[@aria-label='Spedizione plico']//button[text()='Conferma']");
	public final By confermaIndirizzoSpedizionePlicoSWAEVO = By.xpath("//span[text()='Spedizione plico']/ancestor::div[@role='region']//button[text()='Conferma']");
	public final By confermaModalitaFirmaDigitalSWAEVO = By.xpath("//*[text()='Modalità firma digital']/ancestor::div[@role='region']//button[text()='Conferma']");
	public final By confermaModalitaFirmaCanaleInvio = By.xpath("//*[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//button[text()='Conferma']");

	// messaggio di errore indirizzo non verificato

	//Modifiche xpath 30/09/2020
	public By indirizzoNonVerificatoObj = By.xpath("//div[@title='Indirizzo non verificato:']");

	//public By indirizzoNonVerificatoObjTesto = By.xpath("//div[@title='Indirizzo non verificato:']/span");
	public By indirizzoNonVerificatoObjTesto = By.xpath("//span[contains(text(),'Indirizzo non verificato.')]");

	public String indirizzoNonVerifTestoAtteso ="Indirizzo non verificato.CANDIDATO ESTRATTO CON LE CHIAVI FONETICHE";
	public By indirizzoNonVerificatoButton = By.xpath("//div[@title='Indirizzo non verificato:']//span[text()='Chiudi']");	

	public By capIndirizzoFatturazione = By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='main']//label[contains(text(),'Numero Civico')]");


	//messaggio di errore indirizzo di fatturazione non verificato
	public By indirizzoFattNonVerificatoObj = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//span[@id='spanFocusX']");
	//public By indirizzoFattNonVerificatoObjTesto = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='main']//span[@id='spanFocusX']");
	public By indirizzoFattNonVerificatoObjTesto = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//span[@id='spanFocusX']");

	public String indirizzoFattNonVerifTestoAtteso ="Indirizzo non verificato.CANDIDATO ESTRATTO CON LE CHIAVI FONETICHE";



	public By matricola=By.xpath("//label[text()='Matricola contatore distributore']/ancestor::div[@role='none']//input");
	public By checkBoxIndirizzoFatturazione=By.xpath("//label/span[text()='Inserimento dati a carico del cliente da web']/ancestor::div[1]//input[@type='checkbox']");
	public By buttonConfermaIndUltimaFat = By.xpath("//*[text()='Indirizzo Ultima Fattura']/ancestor::div[@role='region']//button[text()='Conferma']");
	public By indirizzoVerificato = By.xpath("//span[text()='Indirizzo verificato']");

	// Nuovi xpath per verifiche Ubiest
	public By insertProvinciaRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../input");
	public By insertProvinciaFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../input");
	public By verificaFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//button[text()='Verifica']");
	public By verificaRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//button[text()='Verifica']");
	public By insertCapRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../input");
	public By insertCapFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../input");
	public By insertComuneRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../input");
	public By insertComuneFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../input");
	//Flag Button
	public By buttonIndirizzoNonForzatoLavoriRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//div[@class='slds-visible slds-p-around--small']//span[text()='Indirizzo Non forzato']/ancestor::span");
	public By buttonIndirizzoNonForzatoLavoriFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//div[@class='slds-visible slds-p-around--small']//span[text()='Indirizzo Non forzato']/ancestor::span");
	public By buttonForzaIndirizzoRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//div[@id='DivmodalModifyAddressABC']//*[text()='Forza Indirizzo']");
	public By buttonForzaIndirizzoFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//div[@id='DivmodalModifyAddressABC']//*[text()='Forza Indirizzo']");
	public By buttonModicaIndirizzoRes = By.xpath("//h2//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='region']//div[@id='DivmodalModifyAddressABC']//*[text()='Modifica Indirizzo']");
	public By buttonModicaIndirizzoFatt = By.xpath("//h2//span[text()='Indirizzo di Fatturazione']/ancestor::div[@role='region']//div[@id='DivmodalModifyAddressABC']//*[text()='Modifica Indirizzo']");
	// Fibra
	public By inputProvinciaFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../input");
	public By inputProvinciaFibraSpan=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../following::li[@data-record='0']");
	public By inputComuneFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../input");
	public By inputComuneFibraSpan=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../following::li[@data-record='0']");
	public By inputIndirizzoFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Indirizzo')]/../input");
	public By inputIndirizzoFibraSpan=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Indirizzo')]/../following::li[@data-record='0']");
	public By inputCivicoFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//*[@name='civic']");
	public By buttonVerificaFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//button[text()='Verifica']");
	public By bannerOkFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//div[@id='coverageBannerIndirizzo di Fornitura Fibra']");
	public By bannerIndirizzoVerificatoFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//div/span[text()='Indirizzo verificato']");//R3 FR 22.07.2021
	public By bannerIndirizzoCopertoFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//div[@aria-label='Indirizzo coperto dalla fibra']");//R3 FR 22.07.2021
	
	
//	public final String sceltaMelitaRadioButton = "//h2[text()='Fibra']/ancestor::div[@class='slds-card']//span[text()='@SCELTA@']/parent::label";
	public final String sceltaMelitaRadioButton = "//*[@name='portabilityRadioGroup']/../label/span";
	public final String sceltaMelitaNoRadioButton = "//*[@name='portabilityRadioGroup' and @value='No']/../label/span";
	public By inputRadioButtonFibraNo=By.xpath(sceltaMelitaNoRadioButton); //R3 FR 22.07.2021
	public By inputRadioButtonFibraSi=By.xpath(sceltaMelitaNoRadioButton); //R3 FR 22.07.2021
	public By inputCellulareFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Cellulare')]/..//input");
	public By inputCellulareConfermaFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Conferma Cellulare')]/..//input");
	public By inputMailFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//input[@type='text' and contains(@id,'Email')]");//R3 FR 22.07.2021
	public By inputCodiceMigrazioneFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Codice di migrazione')]/../div/input");
	public By inputNumeroTelefonoFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//label[contains(text(),'Numero di telefono')]/../div/input");
	public By btnVerificaDatiFibra=By.xpath("//button[@name='Verifica dati']");
	public By checkConsensiFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//*[@data-label='ITA_IFM_Conditions_Accepted__c']/label");
	public By btnModificaDatiFibra=By.xpath("//button[@name='Modifica dati']");
	public By semaforoRosso=By.xpath("//*[@title='Esito verifica portabilità KO semaforo rosso']");
	public By btnConfermaDatiFibra=By.xpath("//h2//span[text()='Fibra']/ancestor::div[@role='region']//button[text()='Conferma']");
	
	public CompilaIndirizziComponent(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		this.util = new SeleniumUtilities(driver);
		this.spinner = new SpinnerManager(driver);
	}




	public void pressButton(By button) throws Exception {
		util.objectManager(button, util.scrollToVisibility, false);
		util.objectManager(button, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void clickCheckIndirizzoFatturazione(By button, String sezione ) throws Exception {
		util.objectManager(button, util.scrollToVisibility, false);
		util.objectManager(button, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();

	}

	//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='main']//span[text()='Indirizzo verificato']
	public void selezionaIndirizzoEsistenteSeNonSelezionato(String sezione) throws Exception {
		By verifica = By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Indirizzo verificato']");
		if(!util.verifyExistence(verifica, 10)) {
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/following::input[@type='checkbox' and contains(@class,'uiInputCheckbox')][1]/.."), util.scrollAndClick);
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Indirizzo di Residenza']/ancestor::div[@role='tabpanel'][1]//button[text()='Importa']"), util.scrollAndClick);
			spinner.checkSpinners();
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);
			if(!util.verifyExistence(verifica, 20)) throw new Exception("Dopo aver selezionato un indirizzo esistente e cliccato su Verifica, non risulta verificato l'indirizzo");
			spinner.checkSpinners();
		} else {
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Modifica Indirizzo']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(1);
		}

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void Confermaindirizzi(String sezione) throws Exception {

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}

	//span[text()='Indirizzo di Residenza/Sede Legale']/ancestor::div[@role='main']//span[text()='Indirizzo verificato']
	public void selezionaIndirizzoDomicilio(String sezione) throws Exception {
		By verif = By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Indirizzo verificato']");
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/ancestor::div[1]//span[text()='Domicilio']//ancestor::div[@class='slds-media__body']//div/label"), util.scrollAndClick);
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Indirizzo di Residenza']/ancestor::div[@role='tabpanel'][1]//button[text()='Importa']"), util.scrollAndClick);
			spinner.checkSpinners();
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);
			if(!util.verifyExistence(verif, 20)) throw new Exception("Dopo aver selezionato un indirizzo esistente e cliccato su Verifica, non risulta verificato l'indirizzo");
			spinner.checkSpinners();
			
			util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
			spinner.checkSpinners();
		}



	public void selezionaIndirizzoEsistenteSeNonSelezionatoForzatoNoForzatura(String sezione,String Cap, String citta) throws Exception{
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
		List<WebElement> elementName = driver.findElements(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/ancestor::div[1]//li[@class='slds-item']//li[@class='slds-item']//input[@type='checkbox' and contains(@class,'uiInputCheckbox')]/.."));
		elementName.get(0).click();
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Indirizzo di Residenza']/ancestor::div[@role='tabpanel'][1]//button[text()='Importa']"), util.scrollAndClick);
		spinner.checkSpinners();
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}








	public void selezionaIndirizzoEsistenteSeNonSelezionatoForzato(String sezione,String Cap, String citta) throws Exception {
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
		List<WebElement> elementName = driver.findElements(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/ancestor::div[1]//li[@class='slds-item']//li[@class='slds-item']//input[@type='checkbox' and contains(@class,'uiInputCheckbox')]/.."));
		elementName.get(0).click();
//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/following::input[@type='checkbox' and contains(@class,'uiInputCheckbox')][1]/ancestor::label/span[1]"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Indirizzo di Residenza']/ancestor::div[@role='tabpanel'][1]//button[text()='Importa']"), util.scrollAndClick);
		spinner.checkSpinners();
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);

		//Click sullo Switch indirizzo non forzato
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);

		//Inserimento CAP
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/ancestor::div/input"), util.scrollAndSendKeys, Cap);
		TimeUnit.SECONDS.sleep(5);
		//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);

		//Forza indirizzo
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']"), util.scrollAndClick);
		spinner.checkSpinners();

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	public void selezionaIndirizzoDomicilioForzato(String sezione,String Cap, String citta) throws Exception {
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
//		List<WebElement> elementName = driver.findElements(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/ancestor::div[1]//li[@class='slds-item']//li[@class='slds-item']//input[@type='checkbox' and contains(@class,'uiInputCheckbox')]/.."));
//		elementName.get(0).click();
//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/following::input[@type='checkbox' and contains(@class,'uiInputCheckbox')][1]/ancestor::label/span[1]"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/ancestor::div[1]//span[text()='Domicilio']//ancestor::div[@class='slds-media__body']//div/label"), util.scrollAndClick);
		spinner.checkSpinners();
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);

		//Click sullo Switch indirizzo non forzato
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);

		//Inserimento CAP
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/ancestor::div/input"), util.scrollAndSendKeys, Cap);
		TimeUnit.SECONDS.sleep(5);
		//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);

		//Forza indirizzo
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']"), util.scrollAndClick);
		spinner.checkSpinners();

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}


	public void selezionaIndirizzoEsistenteSeNonSelezionatoForzatoSezUltimaFattura(String sezione,String Cap, String citta) throws Exception {
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Recapito ultima fattura Cliente Uscente']/following::input[@type='checkbox'][1]/.."), util.scrollAndClick);
		//util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);
		//List<WebElement> elementName = driver.findElements(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/ancestor::div[1]//li[@class='slds-item']//li[@class='slds-item']//input[@type='checkbox' and contains(@class,'uiInputCheckbox')]/.."));
		//elementName.get(0).click();
//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/following::input[@type='checkbox' and contains(@class,'uiInputCheckbox')][1]/ancestor::label/span[1]"), util.scrollAndClick);
		//util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Indirizzo di Residenza']/ancestor::div[@role='tabpanel'][1]//button[text()='Importa']"), util.scrollAndClick);
		//spinner.checkSpinners();
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);
		//Click sullo Switch indirizzo non forzato
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);

		//Inserimento CAP
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/ancestor::div/input"), util.scrollAndSendKeys, Cap);
		TimeUnit.SECONDS.sleep(5);
		//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);

		//Forza indirizzo
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']"), util.scrollAndClick);
		spinner.checkSpinners();

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	public void selezionaIndirizzoEsistenteSeNonSelezionatoForzatoModFirma(String sezione,String Cap, String citta) throws Exception {
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Verifica Indirizzi Esistenti']/following::input[@type='checkbox' and contains(@class,'uiInputCheckbox')][1]/.."), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//a[text()='Indirizzo di Residenza']/ancestor::div[@role='tabpanel'][1]//button[text()='Importa']"), util.scrollAndClick);
		spinner.checkSpinners();
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Verifica']"), util.scrollAndClick);
		//Click sullo Switch indirizzo non forzato
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//span[text()='Indirizzo Non forzato']/ancestor::span"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(3);

		//Inserimento CAP
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/ancestor::div/input"), util.scrollAndSendKeys, Cap);
		TimeUnit.SECONDS.sleep(5);
		//		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li[@data-record='0']"), util.scrollAndClick);
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);

		//Forza indirizzo
		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Forza Indirizzo']"), util.scrollAndClick);
		spinner.checkSpinners();

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void confermaIndirizzoFibraDisabilitato(String sezione) throws Exception {
		WebElement confermaDatiFibra = util.waitAndGetElement(btnConfermaDatiFibra,true);
		if (confermaDatiFibra.isEnabled()) {
			System.out.println("Attenzione! Bottone Conferma Fibra Disabilitato - Test KO");
		} else
			System.out.println("*** Bottone Conferma Fibra Disabilitato - Test OK ***");
			
	}


	public void confermaIndirizzo(String sezione) throws Exception {

		util.objectManager(By.xpath("//h2//span[text()='"+sezione+"']/ancestor::div[@role='region']//button[text()='Conferma']"), util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void inserisciIndirizzo(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {

		WebElement prov = util.waitAndGetElement(inputProvincia,true);
		if (prov.isEnabled()) {
			util.objectManager(inputProvincia, util.scrollAndSendKeysAndTab,Provincia);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(inputProvinciaSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(inputComune, util.scrollAndSendKeysAndTab,Comune);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(inputComuneSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(inputIndirizzo, util.scrollAndSendKeys,Indirizzo);
			TimeUnit.SECONDS.sleep(7);
			util.objectManager(inputCivico, util.scrollAndSendKeys,Civico);
			TimeUnit.SECONDS.sleep(5);
		}
	}
	


	public void inserisciIndirizzoFibraOk(String Provincia, String Comune, String Indirizzo, String Civico, String Valore, String Cellulare, String CodMigrazione, String NumTelefono, String Consenso,String mail) throws Exception {
//R3 - 22.07.2021FR aggiunto parametro per compilazione mail
		WebElement prov = util.waitAndGetElement(inputProvinciaFibra,true);

			if (prov.isEnabled()) {
				util.objectManager(inputProvinciaFibra, util.scrollAndSendKeysAndTab,Provincia);
				TimeUnit.SECONDS.sleep(5);
				util.objectManager(inputProvinciaFibraSpan, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(2);
				util.objectManager(inputComuneFibra, util.scrollAndSendKeysAndTab,Comune);
				TimeUnit.SECONDS.sleep(5);
				util.objectManager(inputComuneFibraSpan, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(2);
				util.objectManager(inputIndirizzoFibra, util.scrollAndSendKeys,Indirizzo);
				TimeUnit.SECONDS.sleep(5);
				util.objectManager(inputIndirizzoFibraSpan, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(7);
				util.objectManager(inputCivicoFibra, util.scrollAndSendKeys,Civico);
				TimeUnit.SECONDS.sleep(5);
				// Verifica Copertura Fibra OK
				verificaFibraOK();				
				// Consensi a Melita
				if(Valore.contentEquals("SI")) {
					this.selezionaSceltaMelita(Valore); // 'SI' dati a Melita
					// Codice di migrazione
					TimeUnit.SECONDS.sleep(2);
					// Codice Migrazione corto - Errore
					util.objectManager(inputCodiceMigrazioneFibra, util.scrollAndSendKeys,"XXXXXX");
					// Verificare messaggio di errore: "Il valore inserito è troppo corto"
					
					// Codice Migrazione lunghezza corretta
					util.objectManager(inputCodiceMigrazioneFibra, util.scrollAndSendKeys,CodMigrazione);
					// Numero di telefono portabilita
					TimeUnit.SECONDS.sleep(2);
					util.objectManager(inputNumeroTelefonoFibra, util.scrollAndSendKeys,NumTelefono);
					// Bottone Verifica Dati Num Migrazione e Num Telefono
					util.objectManager(btnVerificaDatiFibra, util.scrollAndClick);
					// Chiusura messaggio di errore: "ESITO VERIFICA PORTABILITÀ - semaforo rosso"
					  // Esito verifica portabilità KO semaforo rosso
					util.exists(semaforoRosso, 20);
					System.out.println("Semaforo Rosso - Test OK");
					// Click bottone: MODIFICA DATI
					util.objectManager(btnModificaDatiFibra, util.scrollAndClick);
					System.out.println("Bottone Modifica Dati Fibra - Test OK");
					// Codice Migrazione editabile
					util.objectManager(inputCodiceMigrazioneFibra, util.scrollAndSendKeys,CodMigrazione);
					System.out.println("Codice Migrazione editabile - Test OK");
				} else {  
					this.selezionaSceltaMelita(Valore); // 'NO' dati a Melita
					TimeUnit.SECONDS.sleep(2);
					confermaTelefono(Cellulare);
					InserisciMailFibra(mail);//R3 FR 22.07.2021
					
					
					// gestire consenso=NO
					if(Consenso.contentEquals("SI")) {
						util.objectManager(checkConsensiFibra, util.scrollAndClick);
						confermaIndirizzo("Fibra");
					}
					else  {
						confermaIndirizzoFibraDisabilitato("Fibra");
						} 
					}
			} else {
				System.out.println("Attenzione! Campo 'Provincia Fibra' non abilitato! Test: KO");
				throw new Exception("Attenzione! Campo 'Provincia Fibra' non abilitato! Test: KO");		
			}
		}
	public void InserisciIndirizzoFibra(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {
		//R3 - 22.07.2021FR aggiunto parametro per compilazione mail
				WebElement prov = util.waitAndGetElement(inputProvinciaFibra,true);
					if (prov.isEnabled()) {
						util.objectManager(inputProvinciaFibra, util.scrollAndSendKeysAndTab,Provincia);
						TimeUnit.SECONDS.sleep(5);
						util.objectManager(inputProvinciaFibraSpan, util.scrollAndClick);
						TimeUnit.SECONDS.sleep(2);
						util.objectManager(inputComuneFibra, util.scrollAndSendKeysAndTab,Comune);
						TimeUnit.SECONDS.sleep(5);
						util.objectManager(inputComuneFibraSpan, util.scrollAndClick);
						TimeUnit.SECONDS.sleep(2);
						util.objectManager(inputIndirizzoFibra, util.scrollAndSendKeys,Indirizzo);
						TimeUnit.SECONDS.sleep(5);
						util.objectManager(inputIndirizzoFibraSpan, util.scrollAndClick);
						TimeUnit.SECONDS.sleep(7);
						util.objectManager(inputCivicoFibra, util.scrollAndSendKeys,Civico);
						TimeUnit.SECONDS.sleep(5);			
					} else {
						System.out.println("Attenzione! Campo provincia Fibra non abilitato! Test: KO");
						throw new Exception("Attenzione! Campo provincia Fibra non abilitato! Test: KO");		
					}
				}
	
	public void InserisciCdmFibra(String CodMigrazione) throws Exception {
		util.objectManager(inputCodiceMigrazioneFibra, util.scrollAndSendKeys, CodMigrazione);
		System.out.println("Compila indirizzo Fibra- Codice di migrazione inserito.");
	}
	
	public void InserisciNumeroTelFibra(String NumeroTelFibra) throws Exception {
		util.objectManager(inputCodiceMigrazioneFibra, util.scrollAndSendKeys, NumeroTelFibra);
		System.out.println("Compila indirizzo Fibra- Numero Telefonico Fibra inserito.");
	}


	public void inserisciIndirizzoFibraKo(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {

		WebElement prov = util.waitAndGetElement(inputProvinciaFibra,true);
		if (prov.isEnabled()) {
			util.objectManager(inputProvinciaFibra, util.scrollAndSendKeysAndTab,Provincia);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(inputProvinciaFibraSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(inputComuneFibra, util.scrollAndSendKeysAndTab,Comune);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(inputComuneFibraSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(inputIndirizzoFibra, util.scrollAndSendKeys,Indirizzo);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(inputIndirizzoFibraSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(7);
			util.objectManager(inputCivicoFibra, util.scrollAndSendKeys,Civico);
			TimeUnit.SECONDS.sleep(5);
			verificaFibraKO();
			
		} else {
			System.out.println("Attenzione! Campo provincia Fibra non abilitato!");
			throw new Exception("Attenzione! Campo provincia Fibra non abilitato!");		
		}
		
	}


	
		/**
		 * Metodo che verifica la validazione del numero di cellulare fibra 
		 * @param ExpectedValue messaggio atteso nella validazione , passare stringa vuota per evitare il controllo
		 * @return true se bisogna cliccare il bottone conferma e continuare con l'esecuzione dello script, false se lo scopo è verificare il testo di validazione del campo Cellulare fibra
		 * @throws Exception
		 */
		public boolean VerificaValidazioneNumeroCellulare(String ExpectedValue) throws Exception {
	
			boolean ClickButtonConfirm = false;
	
			if (ExpectedValue.contentEquals("")) {
	
				ClickButtonConfirm = true;
			} else {
				String XPath = "//h2//span[text()='Fibra']/ancestor::div[@role='region']//h6[text()=\"@ExpectedValue@\"]".replace("@ExpectedValue@",ExpectedValue);
				if (util.exists(By.xpath(XPath), 5)) {
					ClickButtonConfirm = false;
					System.out.println("Alert Verificato:"+ XPath);
				} else {
					throw new Exception("Validazione cellulare fibra - Il messsaggio di errore non coincide con l'atteso");
				}
	
			}
			return ClickButtonConfirm;
	
		}
		
		
		/**
		 * Metodo che verifica la validazione dell email  fibra 
		 * @param ExpectedValue messaggio atteso nella validazione , passare stringa vuota per evitare il controllo
		 * @return true se bisogna cliccare il bottone conferma e continuare con l'esecuzione dello script, false se lo scopo è verificare il testo di validazione del campo Email fibra
		 * @throws Exception
		 */
		public boolean VerificaValidazioneEmailFibra(String ExpectedValue) throws Exception {
	
			boolean ClickButtonConfirm = false;
	
			if (ExpectedValue.contentEquals("")) {
	
				ClickButtonConfirm = true;
			} else {
				String XPath = "//h2//span[text()='Fibra']/ancestor::div[@role='region']//h6[text()=\"@ExpectedValue@\"]".replace("@ExpectedValue@",ExpectedValue);
				if (util.exists(By.xpath(XPath), 5)) {
					ClickButtonConfirm = false;
					System.out.println("Alert Verificato:"+ XPath);
				} else {
					throw new Exception("Validazione email fibra - Il messsaggio di errore non coincide con l'atteso");
				}
	
			}
			return ClickButtonConfirm;
	
		}
	
	
	
	public void InserisciMailFibra(String mail) throws Exception{
		

		if(driver.findElement(inputMailFibra).isEnabled())
		{
			util.objectManager(inputMailFibra, util.scrollAndSendKeysAndTab,mail);
		
		}else{
//			throw new Exception("Campo Email Fibra disabilitato!");
			System.out.println("Campo Email Fibra disabilitato");
		}
	}
	
	public void GestioneConsensiFibra(String Consenso) throws Exception {

		util.objectManager(checkConsensiFibra, util.scrollAndClick);
		System.out.println("Inserito Consenso Fibra.");
	}
	

	public void confermaTelefono(String Cellulare) throws Exception{
		
//		WebElement el = driver.findElement(inputCellulareFibra);
//		String cellulare = el.getText();
		String valoreInputCellulareFibra=driver.findElement(inputCellulareFibra).getAttribute("value");
		System.out.println(valoreInputCellulareFibra);
		if(Cellulare.contentEquals("")) {
			System.out.println("Attenzione! Numero Cellulare passato in input  vuoto!");
			if(!valoreInputCellulareFibra.contentEquals(""))
			{
				Cellulare=valoreInputCellulareFibra;
			}
			else
			{
//				throw new Exception("Attenzione! Configurazione FlussoSII PK1 non effettuata correttamente.");
			}

			
		} 
//			Cellulare= "0039"+Cellulare;
		
		if(!valoreInputCellulareFibra.startsWith("0039"))
		{
			Cellulare= "0039"+Cellulare;
		}
		util.objectManager(inputCellulareConfermaFibra, util.scrollAndSendKeysAndTab,Cellulare);
		System.out.println("Inserito Numero Cellulare Fibra.");
		

	}
	public void verificaFibraOK() throws Exception{
		
		util.objectManager(buttonVerificaFibra, util.scrollAndClick);
		checkSpinnersSFDC();
		WebElement el = driver.findElement(bannerOkFibra);
		String testo = el.getText();
		
		if(!testo.contentEquals("INDIRIZZO COPERTO DALLA FIBRA")) {
			System.out.println("Attenzione Errore! Copertura Fibra non presente!");
		} else {
			System.out.println("Copertura Fibra OK.");
		}

	}

	public void verificaFibraKO() throws Exception{
		
		util.objectManager(buttonVerificaFibra, util.scrollAndClick);
		checkSpinnersSFDC();
		WebElement el = driver.findElement(bannerOkFibra);
		String testo = el.getText();
		
		if(!testo.contentEquals("INDIRIZZO COPERTO DALLA FIBRA")) {
			System.out.println("OK - Copertura Fibra non presente!");
			System.out.println("// *** FINE TEST ***");
		} else {
			System.out.println("Attenzione! Copertura Fibra presente mentre volevamo KO.");
//			throw new Exception("Attenzione! Configurazione FlussoSII PK1 non effettuata correttamente.");
		}

	}
	
	public void selezionaSceltaMelita(String valore) throws Exception{
		String melitaSceltaXpath;
		if(valore.equalsIgnoreCase("SI")) melitaSceltaXpath = this.sceltaMelitaRadioButton;
		else if(valore.equalsIgnoreCase("NO")) melitaSceltaXpath = this.sceltaMelitaNoRadioButton;
		else throw new Exception("Il valore da selezionare scelto per Melita e' diverso da SI/NO. Verificare dati di input");
		press(By.xpath(melitaSceltaXpath));
	}

	public void inserisciNonValidato(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {

		WebElement prov = util.waitAndGetElement(inputProvincia,true);
		if (prov.isEnabled()) {
			util.objectManager(inputProvincia, util.scrollAndSendKeysAndTab,Provincia);
			TimeUnit.SECONDS.sleep(5);
//			util.objectManager(inputProvinciaSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(inputComune, util.scrollAndSendKeysAndTab,Comune);
			TimeUnit.SECONDS.sleep(5);
//			util.objectManager(inputComuneSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(inputIndirizzo, util.scrollAndSendKeys,Indirizzo);
			TimeUnit.SECONDS.sleep(7);
			util.objectManager(inputCivico, util.scrollAndSendKeys,Civico);
			TimeUnit.SECONDS.sleep(5);


		}

	}
	
	

	public void inserisciIndirizzoDaForzare(String Provincia, String Comune, String Indirizzo, String Civico) throws Exception {

		WebElement prov = util.waitAndGetElement(inputProvincia,true);
		if (prov.isEnabled()) {
			util.objectManager(inputProvincia, util.scrollAndSendKeysAndTab,Provincia);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(inputComune, util.scrollAndSendKeysAndTab,Comune);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(inputIndirizzo, util.scrollAndSendKeys,Indirizzo);
			TimeUnit.SECONDS.sleep(3);
			util.objectManager(inputCivico, util.scrollAndSendKeys,Civico);
			TimeUnit.SECONDS.sleep(3);
		}

	}

	public void forzaIndirizzoPrimaAttivazione(String Cap, String citta) throws Exception {

		WebElement prov = util.waitAndGetElement(inputProvincia,true);
		if (prov.isEnabled()) {

			//Click sullo Switch indirizzo non forzato
			util.objectManager(buttonIndirizzoNonForzato, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);

			//Inserimento CAP
			util.objectManager(inputCap2, util.scrollAndSendKeys, Cap);
			TimeUnit.SECONDS.sleep(5);
			util.objectManager(By.xpath("//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);

			//Forza indirizzo
			util.objectManager(forzaIndirizzoButtonInserimentoFornitura, util.scrollAndClick);
			spinner.checkSpinners();

		}

	}

	public void forzaIndirizzo(String Cap) throws Exception {

		WebElement prov = util.waitAndGetElement(inputProvincia,true);
		if (prov.isEnabled()) {

			//Click sullo Switch indirizzo non forzato
			util.objectManager(buttonIndirizzoNonForzato, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);

			//Inserimento CAP
			util.objectManager(inputCap2, util.scrollAndSendKeys, Cap);
			TimeUnit.SECONDS.sleep(5);
			//			util.objectManager(By.xpath("//label[contains(text(),'CAP')]/../following::li//span[@class='slds-lookup__result-meta slds-text-body--small']/span[text()='"+citta+"']"), util.scrollAndClick);
			util.objectManager(capSpan, util.scrollAndClick);

			//Forza indirizzo
			util.objectManager(forzaIndirizzoButtonInserimentoFornitura, util.scrollAndClick);
			spinner.checkSpinners();

		}

	}

	public void inserisciCap(String cap) throws Exception {
		WebElement cod_Cap = util.waitAndGetElement(By.xpath("(//label[text()='CAP'])[2]/..//input"),true);
		util.objectManager(By.xpath("(//label[text()='CAP'])[2]/..//input"), util.scrollAndSendKeysAndTab,cap);
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(By.xpath("//label[contains(text(),'CAP')]/../following::li[@data-record='0']"), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
	}
	public void verificaIndirizzo(By button) throws Exception {
		if(util.exists(button, 5)&& driver.findElement(button).isDisplayed()){
			util.objectManager(button, util.scrollToVisibility, false);
			util.objectManager(button, util.scrollAndClick);
			spinner.checkSpinners();
		}
	}


	public void popolareCampo(String valore, By object) throws Exception{
		TimeUnit.SECONDS.sleep(1);
		WebElement element = util.waitAndGetElement(object,60,2);
		util.objectManager(object, util.sendKeys, valore);		
		TimeUnit.SECONDS.sleep(1);
		element.sendKeys(Keys.ENTER);
	}
	private boolean checkAlertPresence(WebElement alertBar) throws Exception{
		boolean exist = false;
		String style = alertBar.getAttribute("style");
		if(style.contentEquals("display: none;")) exist = false;
		else exist = true;
		return exist;
	}

	private boolean checkAlertMessage(String message) throws Exception {
		WebElement errors = util.waitAndGetElement(By.xpath("//span[@id='RedAlertsErrors']"));
		if(errors.getText().contains(message)) return true;
		else return false;
	}


	public void verificaObbligatorietaIndirizzoDomicilio(String frameName) throws Exception {
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.FrameSwitcher(By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		if(util.waitAndGetElement(regioneDomicilio,true).isEnabled()) throw new Exception ("Il campo Regione nell' indirizzo domicilio non risulta disabilitato");
		if(util.waitAndGetElement(capDomicilio,true).isEnabled()) throw new Exception ("Il campo CAP nell' indirizzo domicilio non risulta disabilitato");
		util.objectManager(verificaInd, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo indirizzo domicilio risulta obbligatorio, impossibile trovare la Red Alert");
		else {

			if(!checkAlertMessage("Il campo Provincia è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Provincia risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Comune è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Comune risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Indirizzo è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Indirizzo risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Numero Civico è obbligatorio")) throw new Exception("Nella sezione indirizzo domicilio Il campo Numero Civico risulta non essere obbligatorio");

			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NAPOLI");
			util.objectManager(By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NAPOLI");
			util.objectManager(By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"VIA ROMA NAPOLI");
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"5");
			util.objectManager(verificaInd, util.scrollAndClick);
			if(util.exists(verificaInd, 5)) util.objectManager(verificaInd, util.scrollAndClick);

//			WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));
//
//			if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");
//
//			WebElement capElement = util.waitAndGetElement(capDomicilio,true);
//			if(!org.apache.commons.lang3.StringUtils.isNumeric(capElement.getAttribute("value"))) throw new Exception("Nella sezione indirizzo del domicilio, dopo la verifica indirizzo non viene popolato in automatico il campo CAP");


			driver.switchTo().defaultContent();

		}


	}

	public void forzaIndirizzoSedeLegale(String frameName,String provincia, String comune, String indirizzo, String civico, String telefono) throws Exception{
		String domresid = "componentAddress3";
		String section = "Sede Legale e Contatti";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(telefonoClienteXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,telefono);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);

		driver.switchTo().frame(frameToSw);
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);

		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, il sistema non mostra alcun messaggio di errore");
		else {

			if(!checkAlertMessage("Indirizzo non riconosciuto")) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, al verifica indirizzo non viene visualizzato il messaggio di indirizzo non riconosciuto.");
			By closeBar = By.xpath("//div[@id='RedAlerts']//button");
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capSedeLegale, util.scrollAndSendKeys, "80014");
			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}


		}

		//		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificatoSedeLegaleXpath));
		//		//class <> slds-hide
		//		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo Sede Legale nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}

	public void forzaIndirizzoSedeLegaleUBIEST(String frameName,String provincia, String comune, String indirizzo, String civico, String telefono) throws Exception{
		String domresid = "componentAddress3";
		String section = "Sede Legale e Contatti";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(telefonoClienteXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,telefono);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);

		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);

		driver.switchTo().frame(frameToSw);

		TimeUnit.SECONDS.sleep(3);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capSedeLegale, util.scrollAndSendKeys, "80014");
//			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			driver.findElement(capSedeLegale).sendKeys(Keys.ARROW_DOWN);
			TimeUnit.SECONDS.sleep(1);			
			driver.findElement(capSedeLegale).sendKeys(Keys.ENTER);
			TimeUnit.SECONDS.sleep(1);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}
		this.driver.switchTo().defaultContent();

	}


	public void forzaindirizzoResidenza(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress2";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath2.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		//class <> slds-hide
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);

		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, il sistema non mostra alcun messaggio di errore");
		else {

			if(!checkAlertMessage("Indirizzo non riconosciuto")) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, al verifica indirizzo non viene visualizzato il messaggio di indirizzo non riconosciuto.");
			By closeBar = By.xpath("//div[@id='RedAlerts']//button");
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capResidenza, util.scrollAndSendKeys, "80014");
			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}


		}

		this.driver.switchTo().defaultContent();

	}

	public void forzaindirizzoResidenzaUBIEST(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress2";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath2.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		//class <> slds-hide
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capResidenza, util.scrollAndSendKeys, "80014");
			TimeUnit.SECONDS.sleep(1);			
//			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			driver.findElement(capResidenza).sendKeys(Keys.ARROW_DOWN);
			TimeUnit.SECONDS.sleep(1);			
			driver.findElement(capResidenza).sendKeys(Keys.ENTER);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}

		this.driver.switchTo().defaultContent();

	}

	public void forzaindirizzoDomicilio(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		//class <> slds-hide
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);

		if(!checkAlertPresence(alertBar)) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, il sistema non mostra alcun messaggio di errore");
		else {

			if(!checkAlertMessage("Indirizzo non riconosciuto")) throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, al verifica indirizzo non viene visualizzato il messaggio di indirizzo non riconosciuto.");
			By closeBar = By.xpath("//div[@id='RedAlerts']//button");
			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capDomicilio, util.scrollAndSendKeys, "80014");
			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");

			}


		}

		this.driver.switchTo().defaultContent();

	}

	public void forzaindirizzoDomicilioUBIEST(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']"));
		driver.switchTo().frame(frameToSw);
		//class <> slds-hide
//		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
//
//			By closeBar = By.xpath("//div[@id='RedAlerts']//button");
//			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath("//input[@id='"+domresid+":forceAddrChk']/ancestor::label"), util.scrollAndClick);
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
			util.objectManager(capDomicilio, util.scrollAndSendKeys, "80014");
			util.objectManager(By.xpath("//td[text()='80014']/ancestor::table//tr[2]/td"), util.scrollAndClick);
			util.objectManager(By.xpath("//button[@id='"+domresid+":forceAddrBtn']"), util.scrollAndClick);
			if(!util.exists(By.xpath("//button[@id='"+domresid+":editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
				throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");
			}
	
		this.driver.switchTo().defaultContent();

	}

	public void verificaObbligatorietaSedeLegale(String frameName) throws Exception {

		util.FrameSwitcher(By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		util.objectManager(conferma2, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo indirizzo domicilio risulta obbligatorio, impossibile trovare la Red Alert");
		else {

			if(!checkAlertMessage("Il campo telefono cliente è obbligatorio")) throw new Exception("Nella sezione indirizzo sede legale Il campo telefono cliente risulta non essere obbligatorio");


			util.objectManager(closeBar, util.scrollAndClick);
			String domresid = "componentAddress3";
			String section = "Sede Legale e Contatti";
			util.objectManager(By.xpath(telefonoClienteXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"0819988374");
			util.objectManager(conferma2, util.scrollAndClick);

			if(!checkAlertMessage("Inserire o Eseguire Validazione Indirizzo Sede Legale")) throw new Exception("Durante la crezione di un nuovo cliente pur non compilando i campi dell'indirizzo di sede legale il sistema permette la conferma senza verifica obbligatorietà dei campi.");
			util.objectManager(closeBar, util.scrollAndClick);

			if(util.waitAndGetElement(capSedeLegale,true).isEnabled()) throw new Exception ("Il campo CAP nell' indirizzo sede legale non risulta disabilitato");
			util.objectManager(verificaInd3, util.scrollAndClick);
			if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo indirizzo sede legale risulta obbligatorio, impossibile trovare la Red Alert");
			else {

				if(!checkAlertMessage("Il campo Provincia è obbligatorio")) throw new Exception("Nella sezione indirizzo sede legale Il campo Provincia risulta non essere obbligatorio");
				if(!checkAlertMessage("Il campo Comune è obbligatorio")) throw new Exception("Nella sezione indirizzo sede legale Il campo Comune risulta non essere obbligatorio");
				if(!checkAlertMessage("Il campo Indirizzo è obbligatorio")) throw new Exception("Nella sezione indirizzo sede legale Il campo Indirizzo risulta non essere obbligatorio");
				if(!checkAlertMessage("Il campo Numero Civico è obbligatorio")) throw new Exception("Nella sezione indirizzo sede legale Il campo Numero Civico risulta non essere obbligatorio");


				driver.switchTo().defaultContent();

			}



			driver.switchTo().defaultContent();

		}


	}




	public void verificaObbligatorietaIndirizzoResidenza(String frameName) throws Exception {
		String section = "Indirizzi";
		String domresid = "componentAddress2";
		util.FrameSwitcher(By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']"));
		WebElement alertBar = util.waitAndGetElement(By.xpath("//div[@id='RedAlerts']"),true);
		By closeBar = By.xpath("//div[@id='RedAlerts']//button");

		if(util.waitAndGetElement(regioneDomicilio2,true).isEnabled()) throw new Exception ("Il campo Regione nell' indirizzo residenza non risulta disabilitato");
		if(util.waitAndGetElement(capResidenza,true).isEnabled()) throw new Exception ("Il campo CAP nell' indirizzo residenza non risulta disabilitato");
		util.objectManager(verificaInd2, util.scrollAndClick);
		if(!checkAlertPresence(alertBar)) throw new Exception("Nessun campo indirizzo residenza risulta obbligatorio, impossibile trovare la Red Alert");
		else {

			if(!checkAlertMessage("Il campo Provincia è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Provincia risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Comune è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Comune risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Indirizzo è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Indirizzo risulta non essere obbligatorio");
			if(!checkAlertMessage("Il campo Numero Civico è obbligatorio")) throw new Exception("Nella sezione indirizzo residenza Il campo Numero Civico risulta non essere obbligatorio");

			util.objectManager(closeBar, util.scrollAndClick);
			util.objectManager(By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NAPOLI");
			util.objectManager(By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"NAPOLI");
			util.objectManager(By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"VIA ROMA NAPOLI");
			util.objectManager(By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,"5");
			util.objectManager(verificaInd2, util.scrollAndClick);
			if(util.exists(verificaInd2, 5)) util.objectManager(verificaInd2, util.scrollAndClick);

			WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato2XPath.replaceAll("#", section)));

			if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo residenza del nuovo cliente non e' verificato. Inserire un indirizzo corretto");

			WebElement capElement = util.waitAndGetElement(capResidenza,true);
			if(!org.apache.commons.lang3.StringUtils.isNumeric(capElement.getAttribute("value"))) throw new Exception("Nella sezione indirizzo di residenza, dopo la verifica indirizzo non viene popolato in automatico il campo CAP");

			driver.switchTo().defaultContent();

		}


	}

	public void selezionaSceltaIndirizzoCliente(String frame, String scelta) throws Exception {
		if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.frameManager(frame, selezioneFornisceIndirizzoSINO,util.select,scelta);
		util.frameManager(frame, avantiIndirizzoVerificatoRes,util.scrollToVisibility,true);
		util.frameManager(frame, avantiIndirizzoVerificatoRes,util.click);
	}

	public void selezionaSceltaIndirizzoCliente(String frame, By selectDichiaraResidenza,String scelta,By avantiIndirizzo) throws Exception {
		if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.frameManager(frame, selectDichiaraResidenza,util.select,scelta);
		util.frameManager(frame, avantiIndirizzo,util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame, spinner.loadingSpinner);

	}

	public void selezionaSceltaIndirizzoCliente(By selectDichiaraResidenza,String scelta) throws Exception {
		if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.objectManager(selectDichiaraResidenza, util.scrollAndSelect, scelta);

	}

	public void selezionaSceltaIndirizzoClientePerValore(By selectDichiaraResidenza,String value) throws Exception {
		//if(!scelta.equals("NO") && !scelta.equals("SI")) throw new Exception("Scelta del cliente diversa da SI/NO. Impossibile proseguire");
		util.objectManager(selectDichiaraResidenza, util.scrollAndSelectByValue, value);

	}

	public void importaIndirizzoEsistente() throws Exception{

		util.objectManager(indirizziEsistenti,util.scrollAndClick);

		util.objectManager(checkBoxIndirizzoResidenza,util.scrollAndClick);

		util.objectManager(importaBtn, util.scrollAndClick);

		spinner.checkSpinners();
		util.objectManager(verificaBtn, util.scrollAndClick);

		util.scrollToBottom();
		spinner.checkSpinners();
		util.objectManager(conferma,util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void fillCustomerData(Properties prop, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		//this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.provinciaInput, util.scrollAndSendKeys,prop.getProperty("PROVINCIA"));
		util.objectManager(this.comuneInput, util.scrollAndSendKeys,prop.getProperty("COMUNE"));
		util.objectManager(this.viaInput, util.scrollAndSendKeys,prop.getProperty("VIA"));
		util.objectManager(this.viaDropDownMenu, util.click);
		util.objectManager(this.civicoInput, util.scrollAndSendKeys,prop.getProperty("NUMERO_CIVICO"));
		util.objectManager(this.verifica, util.click);
		TimeUnit.SECONDS.sleep(5);
		spinner.checkSpinners();
		util.objectManager(this.avanti, util.click);
		spinner.checkSpinners();
	}

	public void fillComune(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.comuneInput, util.scrollAndSendKeys,s);
	}

	public void fillProvincia(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.provinciaInput, util.scrollAndSendKeys,s);
	}

	public void fillIndirizzo(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.viaInput, util.scrollAndSendKeys,s);
	}

	public void fillCivico(String s) throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.civicoInput, util.scrollAndSendKeys,s);
	}

	public void selectVia() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.viaDropDownMenu, util.click);
	}

	public void verificaClick() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.verifica, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void avantiClick() throws Exception{
		String frame = util.getFrameByIndex(1);
		util.frameManager(frame, this.avanti, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void fillComune(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		//this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.comuneInput, util.scrollAndSendKeys,s);
	}

	public void fillProvincia(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.provinciaInput, util.scrollAndSendKeys,s);
	}

	public void fillIndirizzo(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.viaInput, util.scrollAndSendKeys,s);
	}

	public void fillCivico(String s, String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.civicoInput, util.scrollAndSendKeys,s);
	}

	public void selectVia(String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.viaDropDownMenu, util.click);
	}

	public void verificaClick(String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.verifica, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void avantiClick(String f) throws Exception{
		WebElement frameToSw = this.driver.findElement(
				By.xpath("//iframe[@name='" +f +"'] | //frame[@name='" +f +"']")
				);
		this.driver.switchTo().defaultContent();
		this.driver.switchTo().frame(frameToSw);
		util.objectManager(this.avanti, util.click);
		SpinnerManager spinner = new SpinnerManager(driver);
		spinner.checkSpinners();
	}

	public void importaIndirizzoEsistenteBilling(String frame) throws Exception{

		util.frameManager(frame, indirizziEsistentiTabBill,util.scrollAndClick);

		util.frameManager(frame, checkBoxIndirizzoResidenza,util.scrollAndClick);

		util.frameManager(frame, importaBtn, util.scrollAndClick);

		spinner.waitForSpinnerByStyleDiplayNone(frame);

		util.frameManager(frame, verificaBtnBill, util.scrollAndClick);

		util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoBill,120);
		util.frameManager(frame, avantiIndirizzoVerificatoBill,util.scrollAndClick);
		spinner.checkSpinners(frame);
	}

	public void validaIndirizzo(String frame,By verificaBtn,By spanIndirizzoverificato,By buttonAvanti) throws Exception{

		util.frameManager(frame, verificaBtn, util.scrollAndClick);
		spinner.checkSpinners(frame);
		util.verifyExistenceInFrame(frame,spanIndirizzoverificato,120);
		util.frameManager(frame, buttonAvanti,util.scrollAndClick);
		spinner.checkSpinners(frame);

	}



	public void clienteFornisceIndirizzo(String value) throws Exception{
		//		util.objectManager(clienteFornisceIndirizzoSelect, util.select, value);
		util.objectManager(clienteFornisceIndirizzoSelect, util.scrollAndClick);
		By scelta=By.xpath("//label[text()='Cliente fornisce l’indirizzo?']/parent::div//span[text()='"+value+"']");
		util.objectManager(scelta, util.scrollAndClick);
	}

	public void importaIndirizzoEsistenteResidenziale(String frame) throws Exception{

		util.frameManager(frame, verificaBtnRes, util.scrollAndClick);
		util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoRes,120);
		util.scrollDownDocument();
		util.frameManager(frame, avantiIndirizzoVerificatoRes,util.scrollAndClick);
		spinner.checkSpinners();

	}

	public void copiaDaIndirizzo(String frame,By link) throws Exception{
		util.frameManager(frame, link, util.scrollAndClick);
		spinner.waitForSpinnerByStyleDiplayNone(frame);
	}


	public void importaIndirizzoEsistenteKit(String frame) throws Exception{

		util.frameManager(frame, verificaBtnAddresKit, util.scrollAndClick);
		util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoKit,120);
		util.frameManager(frame, avantiIndirizzoVerificatoKit,util.scrollAndClick);

	}

	public void importaIndirizzoVolturaKit(String frame) throws Exception{

		if(util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoKit,120)){
			util.frameManager(frame, avantiIndirizzoVerificatoKitVoltura,util.scrollAndClick);
		}
		else{
			util.frameManager(frame, verificaBtnAddresKit, util.scrollAndClick);
			util.verifyExistenceInFrame(frame,confermaIndirizzoVerificatoKit,120);
			util.frameManager(frame, avantiIndirizzoVerificatoKitVoltura,util.scrollAndClick);
		}

	}
	public void compileAddressIfAvailable(
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy,
			By addressCheckBtnBy) throws Exception
	{
		if(util.exists( addressCheckBtnBy, 5)) 
		{

			WebElement el = driver.findElement(addressCheckBtnBy);
			if(!el.getAttribute("class").contains("slds-hide")) 
			{
				compileAddressInfo(provincia,provinciaBy,city,cityBy,address,addressBy,civico,civicoBy,addressCheckBtnBy) ;
			}
		}		
		driver.switchTo().defaultContent();
		spinner.checkSpinners();

	}

	public void compileAddressForzatura (By addressCheckBtnBy) throws Exception {
		if(util.exists( addressCheckBtnBy, 5)) 
		{
			util.objectManager(addressCheckBtnBy, util.scrollAndClick);
			}
			
		//WebElement el = driver.findElement(addressCheckBtnBy);
			//if(!el.getAttribute("class").contains("slds-hide")) 
		//	{
			//	util.objectManager(addressCheckBtnBy, util.scrollAndClick);
			/*
				// controlla esiste messaggio errore
			if (util.exists(closeButtonAlert, 5)){
				this.clickCloseAlert();
				// premi indirizzo formato
				util.objectManager(indirizzoForzatoRadioButton, util.scrollAndClick);
				// inserisci VIA 
				this.selezionaIndirizzo("VIA");
				// inserisci CAP
				util.objectManager(sedeLegale, util.scrollAndSendKeys, "80014");
				util.objectManager(forzaIndirizzoSedeLegele, util.scrollAndClick);
				
				if(!util.exists(By.xpath("//button[@id='sedeLegaleComponent:editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
					throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");
				}
				
			} else {
				throw new Exception("Dopo aver inserito un indirizzo non riconosciuto, al verifica indirizzo non viene visualizzato il messaggio di indirizzo non riconosciuto.");
			}
*/	
		driver.switchTo().defaultContent();
	//	spinner.checkSpinners();
//	}
	//}
	}
	
	
	public void forzaIndirizzoSedeLegale (By addressCheckBtnBy) throws Exception {
		
		if(util.exists( addressCheckBtnBy, 5)) 
		{			
			WebElement el = driver.findElement(addressCheckBtnBy);
			if(!el.getAttribute("class").contains("slds-hide")) 
			{
				this.press(indirizzoForzatoRadioButton);
				// inserisci VIA 
				this.selezionaIndirizzo("VIA");
				// inserisci CAP
				util.objectManager(sedeLegale, util.scrollAndSendKeys, "80014");
				TimeUnit.SECONDS.sleep(2);
				util.objectManager(sedeLegaleSpan, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(2);
				
				
				util.objectManager(forzaIndirizzoSedeLegele, util.scrollAndClick);
				
				if(!util.exists(By.xpath("//button[@id='sedeLegaleComponent:editAddrBtn' and not(contains(@class,'slds-hide'))]"), 5)) {
					throw new Exception("Dopo aver forzato l'indirizzo, il sistema non accetta il nuovo indirizzo e non compare il pulsante Modifica Indirizzo");
				}
		driver.switchTo().defaultContent();
		spinner.checkSpinners();
	}
	}
	}
	
	
	public void closeAlert() throws Exception {
		util.getFrameActive();
		press(closeAlertButton);
	}
	
	public void press(By oggetto) throws Exception{
		util.objectManager(oggetto, util.scrollToVisibility, false);
		util.objectManager(oggetto, util.scrollAndClick);
		spinner.checkSpinners();
	}
	
	
	 public void selezionaIndirizzo(String value) throws Exception{
	    	util.objectManager( selectVia, util.scrollToVisibility,true);
	    	TimeUnit.SECONDS.sleep(7);
	    	util.objectManager( selectVia, util.select, value);
	    	spinner.checkSpinners();
	    	util.objectManager( selectVia, util.select, value);
	    	spinner.checkSpinners();
//	    	}
	    }
	public void compileAddressIfAvailable(String frame,
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy,
			By addressCheckBtnBy) throws Exception
	{
		if(util.verifyExistenceInFrame(frame, addressCheckBtnBy, 5)) 
		{
			WebElement frameToSw = driver.findElement(
					By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			WebElement el = driver.findElement(addressCheckBtnBy);
			if(!el.getAttribute("class").contains("slds-hide")) 
			{
				compileAddressInfo(provincia,provinciaBy,city,cityBy,address,addressBy,civico,civicoBy,addressCheckBtnBy) ;
			}
		}		
		driver.switchTo().defaultContent();
		spinner.checkSpinners(frame);

	}

	public void clearAddress(String frame) throws Exception
	{

		util.frameManager(frame, inputProvincia, util.scrollAndClick);
		util.frameManager(frame, inputProvincia, util.clear);
		util.frameManager(frame, inputProvincia, util.sendKeys, "");
		util.frameManager(frame, inputComune, util.scrollAndClick);
		util.frameManager(frame, inputComune, util.clear);		
		util.frameManager(frame, inputComune, util.sendKeys, "");		
		util.frameManager(frame, inputLocalita, util.scrollAndClick);
		util.frameManager(frame, inputLocalita, util.clear);
		util.frameManager(frame, inputLocalita, util.sendKeys, "");
		util.frameManager(frame, inputIndirizzo, util.scrollAndClick);
		util.frameManager(frame, inputIndirizzo, util.clear);
		util.frameManager(frame, inputIndirizzo, util.sendKeys, "");
		util.frameManager(frame, inputCivico, util.scrollAndClick);
		util.frameManager(frame, inputCivico, util.clear);
		util.frameManager(frame, inputCivico, util.sendKeys, "");	
		util.frameManager(frame, inputScala, util.scrollAndClick);
		util.frameManager(frame, inputScala, util.clear);
		util.frameManager(frame, inputScala, util.sendKeys, "");
		util.frameManager(frame, inputPiano, util.scrollAndClick);
		util.frameManager(frame, inputPiano, util.clear);
		util.frameManager(frame, inputPiano, util.sendKeys, "");
		util.frameManager(frame, inputInterno, util.scrollAndClick);
		util.frameManager(frame, inputInterno, util.clear);
		util.frameManager(frame, inputInterno, util.sendKeys, "");
	}

	/**
	 * Sbianca tutti i campi dell'indirizzo e ricompila
	 * Utilizza gli oggetti By della classe
	 * @param frame
	 * @param provincia
	 * @param city
	 * @param localita
	 * @param address
	 * @param civico
	 * @param scala
	 * @param piano
	 * @param interno
	 * @param addressCheckBtnBy
	 * @throws Exception
	 */
	public void compileAddressInfo(String frame,
			String provincia,
			String city,
			String localita,
			String address,
			String civico,
			String scala,
			String piano,
			String interno,
			By buttonVerificaInput) throws Exception
	{	
		//Compila anche il campo Località
		//Campo localita
		clearAddress(frame);

		//inserimento campi
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//campo provincia
		util.frameManager(frame, inputProvincia,util.sendKeysCharByChar, provincia);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Comune
		util.frameManager(frame, inputComune,util.sendKeysCharByChar,city);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Località
		util.frameManager(frame, inputLocalita,util.sendKeysCharByChar,localita);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Indirizzo
		util.frameManager(frame, inputIndirizzo,util.sendKeysCharByChar,address);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Civico
		util.frameManager(frame, inputCivico,util.sendKeysCharByChar,civico);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//Inserisco Scala
		util.frameManager(frame, inputScala,util.sendKeysCharByChar, scala);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//Inserisco Piano
		util.frameManager(frame, inputPiano,util.sendKeysCharByChar,piano);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//Inserisco Interno
		util.frameManager(frame, inputInterno,util.sendKeysCharByChar,interno);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);

		util.frameManager(frame, buttonVerificaInput, util.scrollAndClick);

		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void compileAddressInfo(String frame,
			String provincia,
			String city,
			String address,
			String civico,
			By buttonVerificaInput) throws Exception
	{	
		//Compila anche il campo Località
		//Campo localita
		clearAddress(frame);

		//inserimento campi
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);
		//campo provincia
		util.frameManager(frame, inputProvincia,util.sendKeysCharByChar, provincia);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Comune
		util.frameManager(frame, inputComune,util.sendKeysCharByChar,city);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Indirizzo
		util.frameManager(frame, inputIndirizzo,util.sendKeysCharByChar,address);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);
		//inserisco Civico
		util.frameManager(frame, inputCivico,util.sendKeysCharByChar,civico);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, By.xpath("//body"),util.scrollAndClick);

		util.frameManager(frame, buttonVerificaInput, util.scrollAndClick);

		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void compileAddressInfo(String frame,
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy,
			By addressCheckBtnBy) throws Exception{


		//LOGGER.log(Level.INFO ,"Entrato in compileAddressInfo");
		compileAddress(frame, provincia, provinciaBy, city, cityBy, address, addressBy, civico, civicoBy);

		util.frameManager(frame, addressCheckBtnBy, util.scrollAndClick);
		//		//System.out.println("cliccato address check");

		TimeUnit.SECONDS.sleep(5);
		spinner.waitForSpinnerByStyleDiplayNone(frame);

	}

	public void compileAddressInfo(String provincia,By provinciaBy,String city,By cityBy,String address,By addressBy,String civico,By civicoBy,By addressCheckBtnBy) throws Exception{
		compileAddress(provincia, provinciaBy, city, cityBy, address, addressBy, civico, civicoBy);
		util.objectManager(addressCheckBtnBy, util.scrollAndClick);
		//		//System.out.println("cliccato address check");
		spinner.checkSpinners();
	}


	public void compileAddress(String frame,
			String provincia,
			By provinciaBy,
			String city,
			By cityBy,
			String address,
			By addressBy,
			String civico,
			By civicoBy) throws Exception{


		//LOGGER.log(Level.INFO ,"Entrato in compileAddress ");
		util.frameManager(frame, provinciaBy, util.sendKeys,provincia);
		//LOGGER.log(Level.INFO ,"Inserita provincia");
		//		TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, civicoBy, util.scrollAndClick);
		//LOGGER.log(Level.INFO ,"Click Civico");
		//		TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, cityBy, util.sendKeys,city);
		//		TimeUnit.SECONDS.sleep(1);
		util.frameManager(frame, provinciaBy, util.scrollAndClick);
		//		TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, addressBy, util.sendKeys,address);	
		//		TimeUnit.SECONDS.sleep(1);
		util.frameManager(frame, provinciaBy, util.scrollAndClick);
		//TimeUnit.SECONDS.sleep(2);
		util.frameManager(frame, civicoBy, util.sendKeys,civico);
		//		TimeUnit.SECONDS.sleep(1);
		util.frameManager(frame, provinciaBy, util.scrollAndClick);
		//TimeUnit.SECONDS.sleep(2);

	}

	public void compileAddress(String provincia,By provinciaBy,String city,By cityBy,String address,By addressBy,String civico,By civicoBy) throws Exception{

		//		driver.findElement(By.xpath("//body")).click();
		TimeUnit.SECONDS.sleep(2);
		//		driver.findElement(cityBy).sendKeys(city);
		util.objectManager(provinciaBy,util.scrollAndSendKeys, city);
		util.objectManager(cityBy,util.scrollAndSendKeys, city);
		//		driver.findElement(By.xpath("//body")).click();
		util.objectManager(provinciaBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		//		driver.findElement(addressBy).sendKeys(address);
		util.objectManager(addressBy,util.scrollAndSendKeys, address);
		//		driver.findElement(By.xpath("//body")).click();
		//util.objectManager(provinciaBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);
		//		driver.findElement(civicoBy).sendKeys(civico);
		util.objectManager(civicoBy,util.scrollAndSendKeys, civico);
		//		driver.findElement(By.xpath("//body")).click();
		//util.objectManager(provinciaBy,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);

	}



	public void ForzaIndirizzoEsecuzioneLavoriNonVerificato(String Civico, String Cap) throws Exception {

		// premi indirizzo non verificato
		util.objectManager(this.indirizzoForzatoButtonEsecuzioneLavori, util.scrollToVisibility, false);
		util.objectManager(this.indirizzoForzatoButtonEsecuzioneLavori, util.scrollAndClick);

		// compila CAP o numero civico 
		util.objectManager(inputCivico, util.scrollAndSendKeys,Civico);
		TimeUnit.SECONDS.sleep(5);

		util.objectManager(inputCapEsecuzioneLavori, util.scrollAndSendKeysAndTab,Cap);
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(capSpan, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);


		// premi Forza indirizzo
		util.objectManager(forzaIndirizzoEsecuzioneLavoriButton, util.scrollToVisibility, false);
		util.objectManager(forzaIndirizzoEsecuzioneLavoriButton, util.scrollAndClick);


		TimeUnit.SECONDS.sleep(5);

	}




	public void ForzaIndirizzoNonVerificato(String Civico, String Cap) throws Exception {

		// premi indirizzo non verificato
		util.objectManager(this.indirizzoForzatoButton, util.scrollToVisibility, false);
		util.objectManager(this.indirizzoForzatoButton, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(2);

		// compila CAP o numero civico 
		util.objectManager(inputCivicoIndFatturazione, util.scrollAndSendKeys,Civico);
		TimeUnit.SECONDS.sleep(5);

		util.objectManager(inputCap, util.scrollAndSendKeysAndTab,Cap);
		TimeUnit.SECONDS.sleep(5);
		util.objectManager(capSpan, util.scrollAndClick);
		TimeUnit.SECONDS.sleep(5);


		// premi Forza indirizzo
		util.objectManager(forzaIndirizzoButton, util.scrollToVisibility, false);
		util.objectManager(forzaIndirizzoButton, util.scrollAndClick);


		TimeUnit.SECONDS.sleep(5);

	}

	
	public void ForzaIndirizzoFatturazione(String Cap) throws Exception {
		
		// premi indirizzo non verificato
			util.objectManager(this.indirizzoForzatoButton, util.scrollToVisibility, false);
			util.objectManager(this.indirizzoForzatoButton, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			
			// compila CAP
			
			util.objectManager(inputCap, util.scrollAndSendKeysAndTab,Cap);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(capSpan, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			
			// premi Forza indirizzo
			util.objectManager(forzaIndirizzoButton, util.scrollToVisibility, false);
			util.objectManager(forzaIndirizzoButton, util.scrollAndClick);
	
			TimeUnit.SECONDS.sleep(5);
	}
	
	public void ForzaIndirizzoResidenza(String Cap) throws Exception {
		
		// premi indirizzo non verificato
			util.objectManager(this.indirizzoForzatoButtonRes, util.scrollToVisibility, false);
			util.objectManager(this.indirizzoForzatoButtonRes, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			
			// compila CAP
			
			util.objectManager(inputCapRes, util.scrollAndSendKeysAndTab,Cap);
			TimeUnit.SECONDS.sleep(2);
			util.objectManager(capSpanRes, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(2);
			
			// premi Forza indirizzo
			util.objectManager(forzaIndirizzoButtonRes, util.scrollToVisibility, false);
			util.objectManager(forzaIndirizzoButtonRes, util.scrollAndClick);
	
			TimeUnit.SECONDS.sleep(5);
	}
	
	//Questo modulo sbianca tutti i campi dell'indirizzo pre-caricato e ne inserisce uno nuovo
	public void reinserisciIndirizzo(String frame,By presso,By provincia, By localita,By comune, By indirizzo, By civico,By interno, By piano, By scala,String vpresso,String vprovincia, String vlocalita,String vcomune,String vindirizzo,String vcivico,String vinterno,String vpiano,String vscala) throws Exception{
		//sbiancamento campi
		util.frameManager(frame, scala,util.clear);
		util.frameManager(frame, piano,util.clear);
		util.frameManager(frame, interno,util.clear);
		util.frameManager(frame, civico,util.clear);
		util.frameManager(frame, civico,util.clear);//occorre riperetere 2 volte il clear per sbiancare campo
		util.frameManager(frame, indirizzo,util.clear);
		util.frameManager(frame, comune,util.clear);
		util.frameManager(frame, localita,util.clear);
		util.frameManager(frame, provincia,util.clear);
		util.frameManager(frame, presso,util.clear);

		//inserimento campi
		//campo presso
		util.frameManager(frame, presso,util.sendKeysCharByChar,vpresso);
		TimeUnit.SECONDS.sleep(5);
		//campo provincia
		util.frameManager(frame, provincia,util.sendKeysCharByChar,vprovincia);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Comune
		util.frameManager(frame, comune,util.sendKeysCharByChar,vcomune);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Località
		util.frameManager(frame, localita,util.sendKeysCharByChar,vlocalita);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Indirizzo
		util.frameManager(frame, indirizzo,util.sendKeysCharByChar,vindirizzo);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//inserisco Civico
		util.frameManager(frame, civico,util.sendKeysCharByChar,vcivico);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//Inserisco Scala
		util.frameManager(frame, scala,util.sendKeysCharByChar,vscala);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//Inserisco Piano
		util.frameManager(frame, piano,util.sendKeysCharByChar,vpiano);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
		//Inserisco Interno
		util.frameManager(frame, interno,util.sendKeysCharByChar,vinterno);
		TimeUnit.SECONDS.sleep(5);
		util.frameManager(frame, presso,util.scrollAndClick);
	}

	public void compilaIndirizzi(String section,String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replaceAll("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato3XPath.replaceAll("#", section)));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo nella sezione "+section+" non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}

	public void compilaIndirizziResidenziale(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.findElement(copiaIndirizzo).click();
		el = util.waitAndGetElement(By.xpath(indirizzoVerificato2XPath.replaceAll("#", section)));
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo di Residenza nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}
	
	public void forzaCopiaIndirizzo(String frameName)
	{
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		this.driver.findElement(copiaIndirizzo).click();
		this.driver.switchTo().defaultContent();
	}

	public void compilaIndirizziResidenzialeDomicilio(String frameName,String provincia, String comune, String indirizzo, String civico) throws Exception{
		String section = "Indirizzi";
		String domresid = "componentAddress1";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
//		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificato1XPath.replaceAll("#", section)));
//		//class <> slds-hide
//		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo del Domicilio nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}



	public void compilaSedeLegale(String frameName,String provincia, String comune, String indirizzo, String civico, String telefono) throws Exception{
		String domresid = "componentAddress3";
		String section = "Sede Legale e Contatti";
		util.frameManager(frameName, By.xpath(provinciaXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,provincia);
		util.frameManager(frameName, By.xpath(comuneXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,comune);
		util.frameManager(frameName, By.xpath(indirizzoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,indirizzo);
		util.frameManager(frameName, By.xpath(civicoXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,civico);
		util.frameManager(frameName, By.xpath(telefonoClienteXPath.replaceAll("#", section).replace("$", domresid)),util.sendKeys,telefono);
		util.frameManager(frameName, By.xpath(verificaIndirizzoButtonXPath.replaceAll("#", section)), util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement el = util.waitAndGetElement(By.xpath(indirizzoVerificatoSedeLegaleXpath));
		//class <> slds-hide
		if(el.getAttribute("class").length()>0) throw new Exception("L'indirizzo Sede Legale nuovo cliente non e' verificato. Inserire un indirizzo corretto");
		this.driver.switchTo().defaultContent();

	}

	public void verificaIndirizzo(String frame,By verifica,By esitoVerificaIndirizzo) throws Exception{
		//Click su verifica e aspetta fin quando non compare l'esito della verifica
		util.frameManager(frame, verifica,util.scrollAndClick);
		TimeUnit.SECONDS.sleep(10);
		WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
		WebElement mess=null;
		mess=util.waitAndGetElement(esitoVerificaIndirizzo, 100, 2);
		if (mess==null){
			throw new Exception("L'indirizzo inserito risulta essere non validato");
		}
		driver.switchTo().defaultContent();
		TimeUnit.SECONDS.sleep(5);
	}


	public void avanti(String frame,By avantiButton) throws Exception{
		util.frameManager(frame, avantiButton, util.scrollAndClick);
		spinner.checkSpinners(frame);
	}

	public void avanti(By avantiButton) throws Exception{
		util.objectManager(avantiButton, util.scrollAndClick);
		spinner.checkSpinners();
	}

	public void verificaPopUpCoordinateGeografiche(By buttonConfirmPopUp) throws Exception{
		TimeUnit.SECONDS.sleep(5);

		if(util.exists(buttonConfirmPopUp, 30)&& driver.findElement(buttonConfirmPopUp).isDisplayed()){
			util.objectManager(buttonConfirmPopUp, util.scrollAndClick);
			spinner.checkSpinners();
			// Avanti su Punto su Mappa
			avanti(avantiButton2);
		}
		//		else{
		//			throw new Exception("Pop up di conferma Coordinate Geografiche non visualizzata");}

	}
	public void selezionaIstat(By tabellaIstat, By primoIstat, By buttonConfirm) throws Exception{
		if (util.exists(tabellaIstat, 15)){
			util.objectManager(primoIstat, util.scrollAndClick);
			util.objectManager(buttonConfirm, util.scrollAndClick);
			spinner.checkSpinners();
		}
	}

	public void compilaNuovoIndirizzoDiFornitura() throws Exception {

		compilaNuovoIndirizzoDiFornitura(spanVerificaIndirizzo);
	}

	public void compilaNuovoIndirizzoDiFornitura(By verificaBy) throws Exception {

		util.scrollAndClick.accept(
				util.waitAndGetElement(TabverificaIndirizziEsistenti));

		util.scrollAndClick.accept(
				util.waitAndGetElement( primoIndirizzoEsistente));


		util.scrollAndClick.accept(
				util.waitAndGetElement(btnImporta
						));

		util.waitAndGetElement(
				buttonVerificaAddress)
		.click();

		util.waitAndGetElement(
				verificaBy);
	}


	//	public void inserisciIndirizzoECivicoSeModificabile(String frame,By provinciaBy, String provincia, By comuneBy, String comune, By indirizzoBy, String indirizzo, By civicoBy, String civico) throws Exception{
	//		if(util.exists(frame, buttonVerificaIndirizzo, 5)) {
	//    		driver.switchTo().frame(frame);
	//    		WebElement el = driver.findElement(buttonVerificaIndirizzo);
	//    		if(!el.getAttribute("class").contains("slds-hide")) {
	//    			util.objectManager(provinciaBy, util.sendKeysAndEnter, provincia);
	//    			util.objectManager(comuneBy, util.sendKeysAndEnter, comune);
	//    			util.objectManager(indirizzoBy, util.sendKeysAndEnter, indirizzo);
	//    			util.objectManager(civicoBy, util.sendKeysAndEnter, civico);
	//    			spinner.checkSpinners();
	//    			el.click();
	//    		}
	//    		driver.switchTo().defaultContent();
	//    	    spinner.checkSpinners(frame);
	//    	}
	//	
	//
	//	}

	/**
	 * Verifica esistenza oggetto con descrizione "Indirizzo verificato", 
	 * se presente l'indirizzo è già stato inserito/verificato 
	 * @param frame
	 * @return true --> Indirizzo da Inserire - false --> Indirizzo già verificato
	 * @throws Exception
	 */
	public boolean verificaInserimentoIndirizzo(String frame) throws Exception
	{
		//Oggetto descrizione  "Indirizzo verificato"
		//Presente a video solo se l'indirizzo è già stato verificato
		By objectIndirizzoVerificato = By.xpath("//span[@id='spanFocusYaddressComponentResidential']");
		boolean returnValue = false;

		//		driver.switchTo().frame(frame);

		//	List<WebElement> el = driver.findElements(objectIndirizzoVerificato);
		//	if (el.size() == 1)
		if	(!util.exists(frame, objectIndirizzoVerificato, 5))
		{
			returnValue = true;
		}

		//driver.switchTo().defaultContent();

		return returnValue;

	}

	public By getButtonVerifica() {
		return buttonVerifica;
	}

	public void setButtonVerifica(By buttonVerifica) {
		this.buttonVerifica = buttonVerifica;
	}
	public void ConfermaModalitaFirmaDigital(By Conferma) throws Exception{

		if(util.exists(Conferma, 5)) {
			pressButton(Conferma);
		}
		Thread.currentThread().sleep(3000);
	}



	//	public void verificaMessaggioErroreIndirizzoFatturazioneNonVerificato(By obj, By objText,String popupTestoAtteso) throws Exception{
	//		if(!util.verifyExistence(obj, 60)) throw new Exception("Non è stata visualizzato il messaggio con testo: "+popupTestoAtteso);
	//		String textModal = util.waitAndGetElement(objText,false).getText();
	////		System.out.println(textModal);
	//		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo del messaggio non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
	//	}

	public void verificaMessaggioErroreIndirizzoFatturazioneNonVerificato(By objText,String popupTestoAtteso) throws Exception{
		if(!util.verifyExistence(objText, 60)) throw new Exception("Non è stata visualizzato il messaggio con testo: "+popupTestoAtteso);
		String textModal = util.waitAndGetElement(objText,false).getText();		
		if(textModal.compareTo(popupTestoAtteso)!=0) throw new Exception("Il testo del messaggio non è quello atteso. Testo visualizzato: "+textModal+" testo atteso:"+popupTestoAtteso);
	}

	public void verificaIndirizzoPrepopolatoSezModalitaFirma(String regioneValue, String provinciaValue, String comuneValue, String indirizzoValue, String civicoValue, String capValue, String localitaValue) throws Exception{

		String regione = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Regione')]/../input"),true).getAttribute("value");
		String comune = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Comune')]/../input"),true).getAttribute("value");
		String provincia = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Provincia')]/../input"),true).getAttribute("value");
		String localita = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Località')]/../input"),true).getAttribute("value");
		String indirizzo = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Indirizzo')]/../input"),true).getAttribute("value");
		String civico =   util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'Numero Civico')]"),false).getText(); 
		String cap = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='region']//label[contains(text(),'CAP')]/../input"),true).getAttribute("value");

		//if (regione.compareTo(regioneValue)!=0)  throw new Exception("il campo non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +regioneValue+ " Valore mostrato: " +regione);	
		if (comune.compareTo(comuneValue)!=0)  throw new Exception("il campo Comune non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +comuneValue+ " Valore mostrato: " +comune);	
		if (provincia.compareTo(provinciaValue)!=0)  throw new Exception("il campo Provincia non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +provinciaValue+ " Valore mostrato: " +provincia);	
		if (indirizzo.compareTo(indirizzoValue)!=0)  throw new Exception("il campo Indirizzo non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +indirizzoValue+ " Valore mostrato: " +indirizzo);	
		if (civico.compareTo(civicoValue)!=0)  throw new Exception("il campo Civico non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +civicoValue+ " Valore mostrato: " +civico);	
		if (cap.compareTo(capValue)!=0)  throw new Exception("il campo CAP non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +capValue+ " Valore mostrato: " +cap);	
		if (localita.compareTo(localitaValue)!=0)  throw new Exception("il campo localita non risulta prepopolato o il valore è diverso dal valore atteso. Valore atteso:" +localitaValue+ " Valore mostrato: " +localita);	


	}

	public void verificaEmailPrepopolataSezModalitaFirma() throws Exception{	
		String modalitaFirmaText = util.waitAndGetElement(By.xpath("//h2//span[text()='Modalità firma e Canale Invio']/ancestor::div[@role='main']//label[text()='Indirizzo Email']/following::input"),true).getAttribute("value");
		if(modalitaFirmaText.length()<2) throw new Exception("il campo email è vuoto");
	}



	public String salvaValoreIndirizzo(String path) throws Exception{
		String campoValue = util.waitAndGetElement(By.xpath(path),true).getAttribute("value");
		return campoValue;		
	}

	public String salvaValoreNumeroCivico(String path) throws Exception{
		String campoValue =  util.waitAndGetElement(By.xpath(path)).getText();
		return campoValue;		
	}


	public boolean isElementEnabled(By existentObject) throws Exception{

		if ((driver.findElement(existentObject).isEnabled()))
			return true;
		else {
			return false;
		}
	}
	
	public void verificaIndirizzo(By buttonVerifica,By esitoVerificaIndirizzo)throws Exception{
		if(!util.exists(esitoVerificaIndirizzo, 10)){
			util.objectManager(buttonVerifica, util.scrollAndClick);
		}
	}
	public void verificaIndirizzoNew(String Cap, By btnVerificaIndirizzo, By Provincia, By btnModificaIndirizzo, By btnForzaIndirizzo, 
			By btnIndirizzoNonForzatoLavori, By insertCap, By insertComune, By btnConferma) throws Exception {
		WebElement verificaButton = driver.findElement(btnConferma);
		if (!verificaButton.isEnabled()) {
			//if(!util.exists(btnVerificaIndirizzo, 10)){
			if(util.exists(btnVerificaIndirizzo, 10) && util.verifyVisibility(btnVerificaIndirizzo, 10)){
				util.objectManager(btnVerificaIndirizzo, util.scrollAndClick);
			} else {
//				WebElement prov = driver.findElement(insertProvinciaRes);
				WebElement prov = driver.findElement(Provincia);
				if (prov.isEnabled()) {
		//			util.objectManager(verifica,util.scrollAndClick);
					util.objectManager(btnVerificaIndirizzo,util.scrollAndClick);
		//			spinnerManager.checkSpinners();
					TimeUnit.SECONDS.sleep(6);
				}
				try {
					forzaIndirizzoSWAEVO(Cap, btnForzaIndirizzo, btnVerificaIndirizzo, btnIndirizzoNonForzatoLavori, insertCap, insertComune,btnModificaIndirizzo);
					util.waitAndGetElement(btnModificaIndirizzo, 20, 1); // verifica se esiste il bottone "Modifica Indirizzo"
				} catch (Exception e) {
					throw new Exception("L'indirizzo di fornitura non è stato validato");
				}
			}
		}
	}

	public void forzaIndirizzoSWAEVO(String Cap, By btnForzaIndirizzo, By btnVerificaIndirizzo, By btnIndirizzoNonForzatoLavori, By insertCap, By insertComune, By buttonModicaIndirizzoFatt) throws Exception{
		// Bottone Modifica Indirizzo
		WebElement verificaButton = driver.findElement(btnVerificaIndirizzo);
		if (verificaButton.isDisplayed()) {
			// Salva il CAP
			WebElement element1 = util.waitAndGetElement(insertCap,true);
			String leggiCap = element1.getAttribute("value");
			// Salva il nome del Comune
			element1 = util.waitAndGetElement(insertComune,true);
			String nomeCitta = element1.getAttribute("value");
			
			//Click sul Bottone Switch indirizzo non forzato
			util.objectManager(btnIndirizzoNonForzatoLavori, util.scrollAndClick);
			TimeUnit.SECONDS.sleep(3);
		
			//Inserimento CAP
			if(leggiCap.isEmpty()) {
				util.objectManager(insertCap, util.scrollAndSendKeys, Cap);
			} else {
				util.objectManager(insertCap, util.scrollAndSendKeys, leggiCap);
			}
			TimeUnit.SECONDS.sleep(10);
			// Verificare capSpan se funziona sempre su tutti i cap presenti nelle varie schermate
			pressJavascript(capSpan);
			//Forza indirizzo
			util.objectManager(btnForzaIndirizzo, util.scrollAndClick);
//			spinnerManager.checkSpinners();
			TimeUnit.SECONDS.sleep(6);
	//			// *** Se appare la schermata Seleziona ISTAT ***
	//			WebElement istat = util.waitAndGetElement(codiceIstat,true);
	//			if (istat.isDisplayed()) {
	//				util.objectManager(radioIstat, util.scrollAndClick);
	//				TimeUnit.SECONDS.sleep(5);
	//				util.objectManager(buttonConfermaIstat, util.scrollAndClick);
	//			}
			
		} else {
			WebElement verificaButtonModifica = driver.findElement(buttonModicaIndirizzoFatt);
			if (verificaButtonModifica.isDisplayed()) {
				//Click sul Bottone Modifica Indirizzo Fatturazione
				util.objectManager(buttonModicaIndirizzoFatt, util.scrollAndClick);
				TimeUnit.SECONDS.sleep(3);
				//Verifica indirizzo
				util.objectManager(btnVerificaIndirizzo, util.scrollAndClick);
			}
		}
	}

	public void pressJavascript(By oggetto) throws Exception{
		WebElement element = util.waitAndGetElement(oggetto);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}

}



