package com.nttdata.qa.enel.components.colla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class RESBollettaWebComponent {

	WebDriver driver;
	SeleniumUtilities util;
		
	public By homePageTitle = By.xpath("//div[@class='heading']/h1");
	public By homePageText = By.xpath("//div[@class='heading']/p[2]");	
	public By homePageSubTitle1 = By.xpath("//section[@id='lista-forniture']/div[@class='section-heading']/h2");
	public By homePageSubTitle2 = By.xpath("//section[@id='activity']/div[@class='section-heading']/h2");
	public By homePageSubText1 = By.xpath("//section[@id='lista-forniture']/div[@class='section-heading']/p[2]");
	public By homePageSubText2 = By.xpath("//section[@id='activity']/div[@class='section-heading']/p[2]");
	public By servizi = By.xpath("//a[@id='servizi']");	
	public By serviziPageTitle = By.xpath("//section[1]/div[@class='section-heading']/h2");
	public By serviziPageSubTitle1 = By.xpath("//section[2]/div[@class='section-heading']/h2");
	public By serviziPageSubTitle2 = By.xpath("//section[3]/div[@class='section-heading']/h2");
	public By bollettaWeb = By.xpath("//li[@class='tile']/a/div[1]/span[@class='dsc-icon-bolletta-web']/following::h3");  //h3[contains(text(), 'Bolletta Web')]
	public By autolettura = By.xpath("//section[@id='forniture-id0']/ul/li[1]/a/div[2]/h3");
	public By infoEnelEnergia = By.xpath("//section[@id='forniture-id0']/ul/li[2]/a/div[2]/h3");
	public By modificaIndirizzoFatturazione = By.xpath("//section[@id='forniture-id0']/ul/li[3]/a/div[2]/h3");
	public By modificaPotenzaEOTensione = By.xpath("//section[@id='forniture-id0']/ul/li[4]/a/div[2]/h3");
	public By modificaContattiEConsensi = By.xpath("//section[@id='forniture-id0']/ul/li[5]/a/div[2]/h3");	
	public By pagamentoOnline = By.xpath("//h3[contains(text(), 'Pagamento Online')]");
	public By addebitoDiretto = By.xpath("//h3[contains(text(), 'Addebito Diretto')]");
	public By rettificaBolletta = By.xpath("//h3[contains(text(), 'Rettifica Bolletta')]");
	public By rateizzazione = By.xpath("//h3[contains(text(), 'Rateizzazione')]");
	public By bollettadiSintesiodiDettaglio = By.xpath("//h3[contains(text(), 'Bolletta di Sintesi o di Dettaglio')]");
	public By duplicatoBolletta = By.xpath("//h3[contains(text(), 'Duplicato Bolletta')]");
	public By invioDocumenti = By.xpath("//h3[contains(text(), 'Invio Documenti')]");
	public By cambioPiano = By.xpath("//h3[contains(text(), 'Cambio Piano')]");
	public By modificaConsumoConcordato = By.xpath("//h3[contains(text(), 'Modifica Consumo Concordato')]");
	public By disattivazioneFornitura = By.xpath("//h3[contains(text(), 'Disattivazione Fornitura')]");
	public By pageTitleBW = By.xpath("//section[@id='landing-text']/div/div/h2");
	public By pageTextBW = By.xpath("//section[@id='landing-text']/div/div/p[2]");
	public By pageSubTextBW = By.xpath("//section[@id='landing-text']/div[2]/div/section/div//div/p");
	public By pageFooterTextBW = By.xpath("//section[@id='landing-text']/div[2]/div/section/div/p");
	public By avvialaRichiesta = By.xpath("//section[@id='landing-text']/div[2]/div/section/div/p/a");
	public By pageTitleDisattivaBW = By.xpath("//h1[@id='h1_bollettaweb']");
	public By pageTextDisattivaBW = By.xpath("//div[@id='sc']/div/div/h2");
	public By pageSubTextDisattivaBW = By.xpath("//div[@id='sc']/div/div/h2/p");
	public By checkSupply = By.xpath("//span[@id='0']");
	public By attenzione = By.xpath("//div[@id='modalAlert']/div/p");
	
	public static final String HomePageTitle = "Benvenuto nella tua area privata";
	public static final String HomePageText = "In questa sezione potrai gestire le tue forniture";
	
	public static final String HomePageSubTitle1 = "Le tue forniture";
	public static final String HomePageSubTitle2 = "Le tue attività";
	
	public static final String HomePageSubText1 = "Visualizza le informazioni principali delle tue forniture e consulta le tue bollette, le tue ricevute e i tuoi documenti.";
	public static final String HomePageSubText2 = "Qui troverai tutte le attività per poter gestire le tue forniture e scoprire i vantaggi.";
	
	public static final String ServiziPageTitle = "Servizi per le forniture";
	public static final String ServiziPageSubTitle1 = "Servizi per le bollette";
	public static final String ServiziPageSubTitle2 = "Servizi per il contratto";
	
	public static final String PageTitleBW = "Bolletta Web";
	public static final String PageTextBW = "Il servizio Bolletta Web consente di ricevere la bolletta direttamente tramite email e la notifica di emissione tramite SMS.";
	public static final String PageSubTextBW = "Il servizio Bolletta Web è attivo sulla fornitura:";
	public static final String PageFooterTextBW = "Se vuoi disattivare il servizio, avvia la richiesta";
	
	public static final String PageTitleDisattivaBW = "Disattiva Bolletta Web";
	public static final String PageTextDisattivaBW = "Seleziona una o più forniture su cui intendi revocare il servizio di Bolletta Web. Puoi effettuare questa operazione solo su una fornitura alla volta.";
	public static final String PageSubTextDisattivaBW = "Puoi effettuare questa operazione solo su una fornitura alla volta.";
	public static final String Attenzione = "Siamo spiacenti ma per l'offerta che hai sottoscritto sulla fornitura selezionata non è possibile disattivare la Bolletta Web.";
	
	
	public RESBollettaWebComponent(WebDriver driver) {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);	
	}
	
	public void verifyComponentExistence(By existingObject) throws Exception {
		if (!util.exists(existingObject, 15))
			throw new Exception("l'oggetto con xpath " + existingObject + " non esiste.");
	}
	
	public void clickComponent(By clickableObject) throws Exception {
		util.objectManager(clickableObject, util.scrollToVisibility, false);
		//Thread.sleep(1000);
		util.objectManager(clickableObject, util.scrollAndClick);
	}
	    
 public void comprareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();	
		if(!text.equals(weText))
			throw new Exception("Text mismatch. Text is not matching with the expected value");
	}
 
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}

		public void checkForSupplyServices(String property) throws Exception {
			
		
			if(! property.contains(driver.findElement(autolettura).getText()) && property.contains(driver.findElement(infoEnelEnergia).getText()) && property.contains(driver.findElement(modificaIndirizzoFatturazione).getText()) && property.contains(driver.findElement(modificaPotenzaEOTensione).getText())
					&& property.contains(driver.findElement(modificaContattiEConsensi).getText()) && property.contains(driver.findElement(bollettaWeb).getText()) && property.contains(driver.findElement(pagamentoOnline).getText())  && property.contains(driver.findElement(addebitoDiretto).getText())
					 && property.contains(driver.findElement(rettificaBolletta).getText()) && property.contains(driver.findElement(rateizzazione).getText()) && property.contains(driver.findElement(bollettadiSintesiodiDettaglio).getText()) && property.contains(driver.findElement(duplicatoBolletta).getText())
					 && property.contains(driver.findElement(invioDocumenti).getText()) && property.contains(driver.findElement(cambioPiano).getText()) && property.contains(driver.findElement(modificaConsumoConcordato).getText()) && property.contains(driver.findElement(disattivazioneFornitura).getText()))
							
				throw new Exception("In the page 'https://www-colla.enel.it/' Supply services is not matching");
		}
}
