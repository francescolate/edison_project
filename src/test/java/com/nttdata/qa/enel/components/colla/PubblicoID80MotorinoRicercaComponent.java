package com.nttdata.qa.enel.components.colla;

import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PubblicoID80MotorinoRicercaComponent {
		
	public By logoEnel = By.xpath("//img[@class='logoimg']");
	public By iconUser = By.xpath("//span[@class='icon-user']");
	public By homePageClose = By.xpath("//div[@id='fsa-close-button']");
	public By loginPage = By.xpath("//form[@id='formlogin']");
	public By username = By.id("txtLoginUsername");
	public By password = By.id("txtLoginPassword");
	public By buttonLoginAccedi = By.id("login-btn");
	public By buttonAccetta = By.xpath("//*[@id='truste-consent-button']");
	public By buttonAcceptCookie= By.xpath("//button[@id='accept-btn']");
	
	public By cheContratooQuery = By.xpath("//label[text()='Che contratto vuoi attivare?']");
//	public By cheContratooQueryDropdown = By.xpath("//label[text()='Che contratto vuoi attivare?']/following-sibling::select");
	public By cheContratooQueryDropdown = By.xpath("//label[text()='Che contratto vuoi attivare?']/following-sibling::span//span[@data-val]");
	public By cheContratooQueryDropdown_luceEgas = By.xpath("//label[text()='Che contratto vuoi attivare?']/following-sibling::span//span[@data-val='luce-e-gas']");
	
	public By doveQuery = By.xpath("//label[text()='Dove?']");
//	public By doveQueryDropdown = By.xpath("//label[text()='Dove?']/following-sibling::select");
	public By doveQueryDropdown = By.xpath("//label[text()='Dove?']/following-sibling::span//span[@data-val]");
	public By doveQueryDropdown_negozioUfficio = By.xpath("//label[text()='Dove?']/following-sibling::span//span[@data-val='negozio-ufficio']");
	
	public By perQualeQuery = By.xpath("//label[text()='Per quale necessità?']");
//	public By perQualeQueryDropdown = By.xpath("//label[text()='Per quale necessità?']/following-sibling::select");
	public By perQualeQueryDropdown = By.xpath("//label[text()='Per quale necessità?']/following-sibling::span//span[@data-val]");
	public By perQualeQueryDropdown_PA = By.xpath("//label[text()='Per quale necessità?']/following-sibling::span//span[@data-val='pa']");
	
	public By button_INIZIA_ORA = By.xpath("//span[text()='INIZIA ORA']/parent::a");
	
	public By luceEgasHeading = By.xpath("//div[@id='promo-offert_noresults']//span[text()='Luce e gas per le imprese']");
	public By luceEgasTitle = By.xpath("//div[@id='promo-offert_noresults']//span[text()='Luce e gas per la tua impresa']");
	public By luceEgasContent = By.xpath("//div[@id='promo-offert_noresults']//p");
	
	public By visualizzaLuceLink = By.xpath("//div[@id='promo-offert_noresults']//span[text()=' Visualizza offerte luce']");
	public By visualizzaGasLink = By.xpath("//div[@id='promo-offert_noresults']//span[text()=' Visualizza offerte gas']");
	public By contattaLink = By.xpath("//div[@id='promo-offert_noresults']//span[text()=' Contatta un consulente senza impegno']");
	
	public By pagePath = By.xpath("//ul[@class='breadcrumbs component']");
	public By pageTitle = By.xpath("//h1[@class='image-hero_title text--page-heading']");
	public By contattaciSubTitle = By.xpath("//h1[@class='image-hero_title text--page-heading']/following-sibling::p[contains(text(),'Inserisci i tuoi dati')]");
	
	public By creditsFooterLink = By.xpath("//ul[@class='footer-legal-links']/li//a[text()='Credits']");
	
	//Test Case 81
	public By WhatkindOfContract_Query = By.xpath("//label[text()='What kind of contract?']");
	public By Where_Query = By.xpath("//label[text()='Where?']");
	public By WhatFor_Query = By.xpath("//label[contains(text(),'What for?')]");
	
	public By powerANDgasHeading = By.xpath("//div[@id='promo-offert_noresults']//span[text()='POWER AND GAS TOGETHER FOR YOUR HOME']");
	public By powerANDgasTitle = By.xpath("//div[@id='promo-offert_noresults']//span[@class='titleNoResult']");
	public By powerANDgasDescription = By.xpath("//div[@id='promo-offert_noresults']//p");
	public By nearestEnelStoreLink = By.xpath("//div[@id='promo-offert_noresults']//a/span[text()=' Find the nearest Enel store!']");
	
	public By supportFooterLink = By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a[text()='Support']");
	
	WebDriver driver;
	SeleniumUtilities util;
	
	 public PubblicoID80MotorinoRicercaComponent(WebDriver driver) throws Exception {
			this.driver = driver;
			util = new SeleniumUtilities(this.driver);
			
		}

	 public void launchLink(String url) throws Exception {
			if(!Costanti.WP_BasicAuth_Username.equals(""))
				url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
			try {
				driver.manage().window().maximize();
				driver.navigate().to(url);
			} catch (Exception e) {
				throw new Exception("it's impossible the link " + url);

			}
		}
	 
	 public void VerifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String textfield_found="NO";
			
			WebElement element = driver.findElement(checkObject);
			
			String actualtext = element.getText();
			
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
			}

	/* public void verifyElementSelected(By checkObject) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String selected="NO";
			
			WebElement element = driver.findElement(checkObject);
			
			if (element.isSelected()){
				selected="YES";
			}			
			if (!element.isSelected()){
				selected = "NO";
				throw new Exception("The element is not selected");
			}
		}
		
	 public void verifyElementNotSelected(By NocheckboxObject) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(NocheckboxObject));
			
			String NotSelected="YES";
			
			WebElement element = driver.findElement(NocheckboxObject);
			
			if (!element.isSelected()){
				NotSelected="NO";
			}			
			if (element.isSelected()){
				NotSelected = "YES";
				throw new Exception("The checkbox is selected");
			}
		}
	 
	 public void checkPrePopulatedValueAndCompare(String xpath, String expectedValue) throws Exception{
	        
	        WebDriverWait wait = new WebDriverWait(this.driver, 50);
	            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	            String value = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
	        if(value.equals(""))throw new Exception("Field is not pre filled");
	        
	        String match = "FAIL";
	        if(value.contentEquals(expectedValue))
	        	match = "PASS";
	        if(match.contentEquals("FAIL"))
	        	throw new Exception("PrePopulatedValue and Expected values are not matching");
	    }*/
	 
		public void selectDropDownValue(By checkObject, String property) throws Exception
		{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			Select select = new Select(driver.findElement(checkObject));
			select.selectByValue(property);
					
		}
		
		
		
		public void verifyDefaultDropDownValue(By checkObject, String Value ) throws Exception
		{
			String textFound = "NO";
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			Select select = new Select(driver.findElement(checkObject));
			String actualDropDown = select.getFirstSelectedOption().getText();
			
			if (actualDropDown.contentEquals(Value))
				textFound="YES";
								
			if (textFound.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					
		}
		
		public void selectDropDown(By checkObject, By checkObject2) throws Exception
		{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
			clickComponent(checkObject);
			Thread.sleep(2000);
			clickComponent(checkObject2);
		}
		
		public void verifyDefaultValue(By checkObject, String Value ) throws Exception
		{
			String textFound = "NO";
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			
			String actualValue = driver.findElement(checkObject).getText();
			
			if (actualValue.contentEquals(Value))
				textFound="YES";
								
			if (textFound.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
					
		}
		
		public void clickDropDownValue(By dropdown,By input) throws Exception {
	         util.scrollToElement(driver.findElement(dropdown));
	         clickComponent(dropdown);
	         clickComponent(input);
	           
	        }
		

		public void clickComponent(By clickableObject) throws Exception {
			util.objectManager(clickableObject, util.scrollToVisibility, false);
			Thread.sleep(1000);
			util.objectManager(clickableObject, util.scrollAndClick);
			
		}
		
		public void switchToFrame(String frameName) throws Exception{
			WebDriverWait wait = new WebDriverWait(this.driver, 120);
			
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
					);
			this.driver.switchTo().frame(frameToSw);
		}
		public void clearText(By object){
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WebElement input = driver.findElement(object);
			input.clear();
		}
		
		
		public void verifyComponentExistence(By oggettoEsistente) throws Exception {
					if (!util.verifyExistence(oggettoEsistente, 30)) {
				throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
			}
			
		}
		
		public void verifyComponentNotExistence(By oggettoEsistente) throws Exception {
			if (util.verifyExistence(oggettoEsistente, 30)) {
		throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
	}
	
		}
		
		/*public void SwitchToWindow() throws Exception
		{
			Set<String> windows = driver.getWindowHandles();
			String currentHandle = driver.getWindowHandle();
			
			for (String winHandle : windows) {
				System.out.println(winHandle);
				System.out.println("switched to "+driver.getTitle()+"  Window");
				System.out.println(driver.getCurrentUrl());
				
		       // String pagetitle = driver.getTitle();
		        		driver.switchTo().window(winHandle);
		        System.out.println(winHandle);
		        System.out.println("After switch"+ driver.getCurrentUrl());
		        System.out.println(driver.getTitle());
			    
			 }
		}*/
		/*public void verifyComponentVisibility(By visibleObject) throws Exception {
			if (!util.verifyVisibility(visibleObject, 15)) {
				throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
			}
		}*/
		
		public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
			textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "");
			if (!util.checkElementText(objectWithText, textToCheck)) {
				WebElement problemElement = driver.findElement(objectWithText);
				String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
				if (elementText.length() < 1) {
					elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");
				}
				String message = "The text displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
				System.out.println(message);
				throw new Exception(message);
			}
		}
		public void verifyContent(By object,String text) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(object));
			String value = driver.findElement(object).getText();
			value = value.replaceAll("(\r\n|\n,)","");
			//System.out.println(value);
			if(!text.contentEquals(value))
				throw new Exception("Value is not matching with expected");
			
		}

			public void clickComponentIfExist(By existObject) throws Exception {
			if (util.exists(existObject, 15)) {
				util.objectManager(existObject, util.scrollToVisibility, false);
			    util.objectManager(existObject, util.scrollAndClick);
			}
		}
			public void waitForElementToDisplay (By checkObject) throws Exception{
				WebDriverWait wait = new WebDriverWait (driver, 50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
				
			}
			public void jsClickObject(By by) throws Exception {
				util.jsClickElement(by);
			}
				
		public void enterInput(By insertObject, String valore) throws Exception {
			util.objectManager(insertObject, util.sendKeys, valore);
		}
		
		public void clickTab(By Object) throws Exception {
			driver.findElement(Object).sendKeys(Keys.TAB);;
		}
		
		public static String generateRandomPOD(int n) {
		    int m = (int) Math.pow(10, n - 1);
		    m = m + new Random().nextInt(9 * m);
		    return String.valueOf(m);
		}
		
		public void scrollDown(By by) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			util.scrollToElement(we);
			util.scrollNaNdecchiaNdecchia();
		}
		
		public void verifyText(By checkObject, String Value) throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
			String textfield_found="NO";
			WebElement element = driver.findElement(checkObject);
			String actualtext = element.getText().replaceAll("(\r\n|\n)", "");;
			System.out.println(actualtext);
			System.out.println(Value);
			
			if (actualtext.contentEquals(Value))
			textfield_found="YES";
							
			if (textfield_found.contentEquals("NO"))
				throw new Exception("The expected text" + Value + " is not found:");
		}
		
		public void hanldeFullscreenMessage(By by) throws Exception{
			if(util.verifyExistence(by, 60)){
				util.objectManager(by, util.scrollToVisibility, false);
				util.objectManager(by, util.scrollAndClick);
			}
		}
		
		public void backBrowser(By checkObject) throws Exception {
		    try {
		    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		    	driver.navigate().back();
		    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		                
		    } catch (Exception e) {
		        throw new Exception("Previous Page not displayed");
		    }
		}
		public void comprareText(By by, String text, boolean nestedTags) throws Exception{
			WebElement we = util.waitAndGetElement(by);
			String weText = we.getAttribute("innerHTML");
			if(nestedTags)
				weText = normalizeInnerHTML(weText);
			else
				weText = we.getText();
			System.out.println("Normalized:\n"+weText);
			System.out.println(text);
			if(!text.equals(weText))
				throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
		}
		
		public String normalizeInnerHTML(String s){
			int less = s.indexOf("<"); int greater = s.indexOf(">");
			if(less==-1)
				return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
			else{
				String tag = s.substring(less, greater+1);
				String endTag = null;
				if(!tag.contains("<br>"))
					endTag = tag.replace("<", "</");
				if(endTag!=null)
					return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
				else
					return normalizeInnerHTML(s.replace(tag, ""));
			}
			
			
		}
		
		public void checkUrl(String url) throws Exception{
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			Thread.sleep(5000);
			String link=driver.getCurrentUrl();
			
			if (url.contains("https://"))
			{
				url = url.substring(8);
			}
			if (!link.contains(url))
				 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
			}
		
		public void checkURLAfterRedirection(String url) throws Exception{
			
			Thread.sleep(10000);
			
			String CurrentUrl = "";
			if(driver.getCurrentUrl().contains("enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it"))
			{
				CurrentUrl =	driver.getCurrentUrl().replace("enelsite:FrUkoq+swIpl!O0aKeB_@www-coll1.enel.it", "www-coll1.enel.it");
			}	 
			else
			{
				CurrentUrl = driver.getCurrentUrl();
			}
			
			
			System.out.println(CurrentUrl);
			System.out.println(url);	
			
			if(!CurrentUrl.contains(url))
				throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
		}
		
		public void pressJavascript(By clickObject) throws Exception {
			WebElement element = util.waitAndGetElement(clickObject);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
		
		public void verifyHeader() throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a")));
			
			String[] headerText = {"Luce e gas","Imprese","Storie","Futur-e","Media","Supporto"};
			List<WebElement> header = driver.findElements(By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a"));
			String footerText [] = new String[header.size()];
			 
			for (int i = 0; i < header.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(headerText[i])){
					throw new Exception("Footer values for "+i+" is not matching")	;		
					} 
			}
			
		}
		
		public void verifyFooterFixedText() throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='footer-copyright']/li")));
			
			String[] footerFixedText = {"Tutti i diritti riservati © Enel Italia S.p.a. © Enel Energia S.p.a.","Gruppo IVA Enel P.IVA 15844561009"};
			List<WebElement> footerFixed = driver.findElements(By.xpath("//ul[@class='footer-copyright']/li"));
			String footerText [] = new String[footerFixed.size()];
			 
			for (int i = 0; i < footerFixed.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//ul[@class='footer-copyright']/li)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(footerFixedText[i])){
					throw new Exception("Footer fixed values for "+i+" is not matching")	;		
					} 
			}
			
		}
		
		public void verifyFooterVisibility() throws Exception {
			
			verifyFooterFixedText();
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='footer-legal-links']/li//a")));
			
			String[] footerLinks = {"Informazioni Legali","Credits","Privacy","Cookie Policy","Preferenze Cookie","Remit"};
			List<WebElement> footer = driver.findElements(By.xpath("//ul[@class='footer-legal-links']/li//a"));
			String footerText [] = new String[footer.size()];
			 
			for (int i = 0; i < footer.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//ul[@class='footer-legal-links']/li//a)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(footerLinks[i])){
					throw new Exception("Footer values for "+i+" is not matching")	;		
					} 
			}
			
		}
		
		
		public void verifyHeader_powerAndGas() throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a")));
			
			String[] headerText = {"Power and gas","Business","ENELPREMIA WOW!","Stories","Futur-e","Media","Support"};
			List<WebElement> header = driver.findElements(By.xpath("//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a"));
			String footerText [] = new String[header.size()];
			 
			for (int i = 0; i < header.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//div[@class='dotcom-header__links dotcom-header__links-custom']/ul/li/a)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(headerText[i])){
					throw new Exception("Footer values for "+i+" is not matching")	;		
					} 
			}
			
		}
		
		public void verifyFooterFixedText_powerAndGas() throws Exception {
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='footer-copyright']/li")));
			
			String[] footerFixedText = {"Enel Italia S.p.a.","| © Enel Energia S.p.a. Gruppo IVA Enel VAT 15844561009"};
			List<WebElement> footerFixed = driver.findElements(By.xpath("//ul[@class='footer-copyright']/li"));
			String footerText [] = new String[footerFixed.size()];
			 
			for (int i = 0; i < footerFixed.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//ul[@class='footer-copyright']/li)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(footerFixedText[i])){
					throw new Exception("Footer fixed values for "+i+" is not matching")	;		
					} 
			}
			
		}
		
		public void verifyFooterVisibility_powerAndGas() throws Exception {
			
			verifyFooterFixedText_powerAndGas();
			
			WebDriverWait WaitVar = new WebDriverWait(driver, 10);
			WaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='footer-legal-links']/li//a")));
			
			String[] footerLinks = {"Legal disclaimers","Credits","Privacy","Cookie Policy","Cookie Preferences","Remit"};
			List<WebElement> footer = driver.findElements(By.xpath("//ul[@class='footer-legal-links']/li//a"));
			String footerText [] = new String[footer.size()];
			 
			for (int i = 0; i < footer.size(); i++) {
				
				footerText[i] = driver.findElement(By.xpath("(//ul[@class='footer-legal-links']/li//a)["+(i+1)+"]")).getText().replaceAll("\\r\\n|\\r|\\n", " ");
				
				if(!footerText[i].equalsIgnoreCase(footerLinks[i])){
					throw new Exception("Footer values for "+i+" is not matching")	;		
					} 
			}
			
		}

		public static final String LuceEgasContentText = "Attualmente non sono disponibili soluzioni combinate luce e gas per l'impresa. Ti consigliamo di visitare la sezione luce e gas dedicata alla tua attività o contattare un consulente dedicato.";	
		public static final String Query1DropdownValue = "Luce e Gas";
		public static final String Query2DropdownValue = "Negozio - Ufficio";
		public static final String Query3DropdownValue = "Prima Attivazione";
		public static final String QueryDropdownValue_Luce = "Luce";
		public static final String QueryDropdownValue_Gas = "Gas";
		public static final String QueryDropdownValue_VisualizzaTutte = "Visualizza tutte";
		public static final String QueryDropdownValue_Voltura = "Voltura";
		public static final String ContattaciPath = "Home / Form ricontatto commerciale";
		public static final String ContattaciTitle = "Contattaci";
		public static final String ContattaciSubTitle = "Inserisci i tuoi dati, sarai ricontattato da nostri consulenti SENZA alcun impegno";
		public static final String CreditsPath = "Home/supporto/Credits";
		public static final String CreditsTitle = "Credits";
		
		//Test Case 81
		public static final String PowerANDgasHeading = "POWER AND GAS TOGETHER FOR YOUR HOME";
		public static final String PowerANDgasTitle = "Find the right solution for your home with Enel Energia deals";
		public static final String PowerANDgasDescription = "If you did not find what you were looking for, you can visit the specific section or contact a dedicated consultant";
		
		//Test case 82
		
		public static final String SupportPath = "Home / support services";
		public static final String SupportTitle = "Manage your supply easily";
		
		
		
		
}
