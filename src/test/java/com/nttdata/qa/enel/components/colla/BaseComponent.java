package com.nttdata.qa.enel.components.colla;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;

import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.pagefactory.ByAll;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseComponent {

    public WebDriver driver;
    public SeleniumUtilities util;
    public SpinnerManager spinner;
    public int baseTimeoutInterval = 30;
    public int inexTimeoutInterval = 5;
    public int extTimeoutInterval = 15;
    public static String link_by_inner_and_href = "//a[contains(.,'#1') and contains(@href, '#2')]";
    public static String link_by_text_and_href = "//a[contains(text(),'#1') and contains(@href, '#2')]";
    public static String element_by_text_and_parent_id = "//*[contains(@id,'#2')]//*[contains(text(),'#1')]";
    public static String element_by_text_and_parent_class = "//*[contains(@class,'#2')]//*[contains(text(),'#1')]";
    public static String element_by_inner_and_parent_id = "//*[contains(@id,'#2')]//*[contains(.,'#1')]";
    public static String element_by_inner_and_parent_class = "//*[contains(@class,'#2')]//*[contains(.,'#1')]";
    public static String label_by_text = "//label[text()='##']";
    public static String any_parent = "/parent::*";
    public static String any_descendant = "/descendant::*";
    public static String input_in_any_parent = any_parent + "//input";
    public static String any_with_text = "//*[text()='##']";
    public static String any_with_not_text = "//*[not(text()='##')]";
    public static String any_with_contains_text = "//*[contains(text(),'##')]";
    public static String any_with_not_contains_text = "//*[not(contains(text(),'##'))]";
    public static String any_by_text_in_any_parent = any_parent + any_with_text;
    public static String any_by_not_text_in_any_parent = any_parent + any_with_not_text;
    public static String any_by_not_contains_text_in_any_parent = any_parent + any_with_not_contains_text;
    public static String first_ancestor_focusable = "/ancestor::*[@tabindex or @hidden != 'true' or @disabled != 'true']";
    public static String first_descendant_clickable = "//descendant::*[@tabindex != '-1' or @hidden != 'true' or @disabled != 'true']";
    public static String disabled_value = "[@disabled]";
    public static String not_disabled_value = "[not(@disabled)]";
    public static String not_tagname = "[not(name()='##')]";
    public static String text_is_not_value = "[text() != '##']";
    public static String text_is_value = "[text() == '##']";
    public static String first_check_box_show = "//input[@type='checkbox' and ancestor::div[contains(@class , 'show')]]" + any_parent;

    /**
     * @param driver
     */
    public BaseComponent(WebDriver driver) {
        this.driver = driver;
        util = new SeleniumUtilities(this.driver);
        spinner = new SpinnerManager(driver);
    }


    /**
     *
     */
    public BaseComponent() {
    }

    /**
     * @param fieldName
     * @throws Exception
     */
    public void verifyInputFieldExist(String fieldName) throws Exception {
        if (!util.verifyExistence(By.xpath("//label[contains(text(),'" + fieldName + "')]/following::input[1]"), baseTimeoutInterval))
            throw new Exception("Impossibile trovare il campo di input " + fieldName);
    }

    /**
     * @param fieldName
     * @throws Exception
     */
    public void verifyInputFieldIsNotEmpty(String fieldName) throws Exception {
        if (util.waitAndGetElement(By.xpath("//label[contains(text(),'" + fieldName + "')]/following::input[1]"), true).getAttribute("value").length() < 1)
            throw new Exception("Il campo " + fieldName + " non risulta prepopolato come atteso");
    }

    /**
     * @param fieldName
     * @throws Exception
     */
    public void verifyFieldIsNotEmpty(By fieldName) throws Exception {
        if (util.waitAndGetElement(fieldName, true).getAttribute("value").length() < 1)
            throw new Exception("Il campo " + fieldName + " non risulta prepopolato come atteso");
    }

    /**
     * @param fieldName
     * @throws Exception
     */
    
    public void verifyInputFieldIsEnabled(String fieldName) throws Exception {
        if (!util.verifyExistence(By.xpath("//label[contains(text(),'" + fieldName + "')]/following::input[1]"), baseTimeoutInterval))
            throw new Exception("Impossibile trovare il campo di input " + fieldName);
        //		if(!util.verifyExistence(By.xpath("//label[contains(text(),'"+fieldName+"')]/../following::input[@disabled]"), baseTimeoutInterval))
        if (util.verifyExistence(By.xpath("//label[contains(text(),'" + fieldName + "')]/parent::*//descendant::input[@disabled]"), baseTimeoutInterval))
            throw new Exception("Il campo  " + fieldName + " non risulta abilitato come atteso");
    }
    
    public void verifyInputFieldIsNotEnabled(String fieldName) throws Exception {
        if (!util.verifyExistence(By.xpath("//label[contains(text(),'" + fieldName + "')]/following::input[1]"), baseTimeoutInterval))
            throw new Exception("Impossibile trovare il campo di input " + fieldName);
        //		if(!util.verifyExistence(By.xpath("//label[contains(text(),'"+fieldName+"')]/../following::input[@disabled]"), baseTimeoutInterval))
        if (!util.verifyExistence(By.xpath("//label[contains(text(),'" + fieldName + "')]/parent::*//descendant::input[@disabled]"), baseTimeoutInterval))
            throw new Exception("Il campo  " + fieldName + " non risulta disabilitato come atteso");
    }

/*    public boolean verifyComponentExistence(By existingObject) throws Exception {
        if (!util.exists(existingObject, 15))
            return false;

        return true;
    }*/

    /**
     * @param fieldName
     * @throws Exception
     */
    public void verifyButtonIsNotEnabled(String fieldName) throws Exception {
        if (!util.verifyExistence(By.xpath("//button[contains(text(),'" + fieldName + "')]/following::button[1]"), baseTimeoutInterval))
            throw new Exception("Impossibile trovare il button " + fieldName);
        if (!util.verifyExistence(By.xpath("//button[contains(text(),'" + fieldName + "')]/parent::*//descendant::button[@disabled]"), baseTimeoutInterval))
            throw new Exception("Il button  " + fieldName + " non risulta disabilitato come atteso");
    }


    /**
     * @param labelField
     * @param value
     * @throws Exception
     */
    public void sendText(String labelField, String value) throws Exception {
        String by = "//label[contains(text(),'" + labelField + "')]/following::input";
        util.objectManager(By.xpath(by), util.scrollAndSendKeys, value);
    }

    /**
     * @param xpathSection
     * @throws Exception
     */
    public void verifySectionExistence(By xpathSection) throws Exception {
        if (!util.verifyExistence(xpathSection, 150)) {
            throw new Exception("Impossibile trovare la sezione identificata dal seguente xpath " + xpathSection);
        }
    }

    /**
     * @param element
     * @param textInput
     * @throws Exception
     */
    public void clearAndSendKeys(By element, String textInput) throws Exception {
        try {
            verifyComponentExistence(element);
            driver.findElement(element).clear();
            driver.findElement(element).sendKeys(textInput);
        } catch (Exception e) {
            e.printStackTrace();
            new Exception("Errore nel popolamento dell'input");
        }
    }
    
	public void clearText(By object){
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WebElement input = driver.findElement(object);
		input.clear();
	}

    /**
     * @param oggettoEsistente
     * @throws Exception
     */
    public void verifyComponentExistence(By oggettoEsistente) throws Exception {
        if (!util.verifyExistence(oggettoEsistente, extTimeoutInterval)) {
            throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
        }
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		 */
    }

    /**
     * @param oggettoEsistente
     * @throws Exception
     */
    public void verifyComponentNonExistence(By oggettoEsistente) throws Exception {
        if (util.verifyExistence(oggettoEsistente, extTimeoutInterval)) {
            throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
        }
		/*
		NB: util.exists mixes implicit and explicit waits, and this can lead to unpredictable behaviour according to documentation.
		It is preferable to use util.verifyExistance, which is based on the same condition presenceOfElementLocated 
		 */
    }

    /**
     * @param existingObject
     * @throws Exception
     */
    public void verifyComponentInexistence(By existingObject) throws Exception {
        if (util.exists(existingObject, inexTimeoutInterval))
            throw new Exception("l'oggetto con xpath " + existingObject + " esiste.");
    }

    /**
     * @param frameEsistente
     * @throws Exception
     */
    public void verifyFrameAvailableAndSwitchToIt(By frameEsistente) throws Exception {
    	driver.switchTo().defaultContent();
        if (!util.verifyFrameAvailableAndSwitchToIt(frameEsistente, baseTimeoutInterval)) {
            throw new Exception("il frame con xpath " + frameEsistente + " non disponibile allo switch.");
        }
    }

    /**
     * @param index
     * @throws Exception
     */
    public void switchToFrameByIndex(int index) throws Exception {
        String frame = util.getFrameByIndex(index);
        util.switchToFrame(frame);
    }

    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    
	public void switchToFrame(String frameName) throws Exception{
        WebDriverWait wait = new WebDriverWait(this.driver, 120);
        WebElement frameToSw = this.driver.findElement(
                By.xpath("//iframe[@name='" +frameName +"'] | //frame[@name='" +frameName +"']")
                );
        this.driver.switchTo().frame(frameToSw);
    }
    
    public void clickWithJS(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
        util.jsClickElement(oggettocliccabile);
    }


    public void simulateDownArrowKey(By element) throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(element).sendKeys(Keys.ARROW_DOWN);
    }

    public void simulateEnterKey(By element) throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(element).sendKeys(Keys.ENTER);
    }
    
    public void simulateTabKey(By element) throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(element).sendKeys(Keys.TAB);
    }


    /**
     * @param popupText
     * @param buttonLabel
     * @throws Exception
     */
    public void checkPopupTextAndPressButton(String popupText, String buttonLabel) throws Exception {
        //By modal = By.xpath("//div[contains(@id,'theModalCmp_')]//p");
        //By modal = By.xpath("//div[contains(@class,'slds-modal')]");
        By modal = By.xpath("//div[contains(@class,'slds-modal__container')]//p[text()='"+popupText+"']");
        By modalButton = By.xpath("//div[contains(@class,'slds-modal')]//button[text()='" + buttonLabel + "']");
        if (!util.verifyExistence(modal, 30))
            throw new Exception("Dopo aver confermato il processo non viene mostrata a video la modale con messaggio: " + popupText);
        String textModal = util.waitAndGetElement(modal, false).getText();
        System.out.println(textModal);
        if (!textModal.contentEquals(popupText))
            throw new Exception("Dopo aver confermato il processo viene mostrata una popup modale ma non contiene il testo : " + popupText);
        if (!util.verifyExistence(modalButton, 10))
            throw new Exception("Dopo aver confermato il processo, come atteso è mostrata a video una popup ma non contiene il pulsante " + buttonLabel);
        util.objectManager(modalButton, util.scrollAndClick);

    }

    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void clickComponent(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
        util.objectManager(oggettocliccabile, util.scrollAndClick);
    }
    
    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void jsClickComponent(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
        util.jsClickElement(oggettocliccabile);
    }
    
    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void jsScrollToElement(By by) throws Exception {
        WebElement we = util.waitAndGetElement(by);
        util.jsClickElement(by);
    }

    /**
     * @param oggettocliccabile
     * @return
     * @throws Exception
     */
    public boolean clickComponentIfNotDisabled(By oggettocliccabile) throws Exception {
        if (!checkIfElementIsDisabled(oggettocliccabile, true, true)) {
            util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
            util.objectManager(oggettocliccabile, util.scrollAndClick);
            return true;
        }
        return false;
    }

    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void doubleClickComponent(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
        util.objectManager(oggettocliccabile, util.scrollAndClick);
        util.objectManager(oggettocliccabile, util.click);
        //		spinner.checkMainCollaSpinner();
    }

    /**
     * @param oggettocliccabile
     * @return
     * @throws Exception
     */
    public boolean clickComponentIfExist(By oggettocliccabile) throws Exception {
        if (this.getElementExistance(oggettocliccabile)) {
            clickComponent(oggettocliccabile);
            return true;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    public void checkSpinnersSFDC() throws Exception {
        spinner.checkSpinners();
    }

    /**
     * @throws Exception
     */
    public void checkSpinnersSFDC2() throws Exception {
        int i = 0;
        int old = baseTimeoutInterval;
        baseTimeoutInterval = 1;
        By spinners_class_visible_0 = new ByChained(spinner.dotSpinner, By.xpath("/@class[contains(., 'slds-visible')]"));
        By spinners_class_visible_1 = new ByChained(spinner.newConfiguratorSpinner, By.xpath("/@class[contains(., 'slds-visible')]"));
        By spinners = new ByAll(spinner.bauSpinner, spinner.collaSpinner1, spinner.collaSpinner2
                , spinner.lightSpinner, spinner.loadingSpinner
                , spinner.overlaySpinner, spinner.tinySpinner, spinners_class_visible_0, spinners_class_visible_1);
        try {
            verifyComponentInvisibility(spinners);
        } catch (Exception e) {
            i = i + 1;
            System.out.println("check spinner i : " + i);
            if (i > 10) {
                throw new Exception("Check Spinner error");
            }
        }
        baseTimeoutInterval = old;
    }

    /**
     * @param parent
     * @throws Exception
     */
    public void checkSpinnersSFDC2(By parent) throws Exception {
        int i = 0;
        int old = baseTimeoutInterval;
        baseTimeoutInterval = 1;
        By spinners_class_visible_0 = new ByChained(spinner.dotSpinner, By.xpath("/@class[contains(., 'slds-visible')]"));
        By spinners_class_visible_1 = new ByChained(spinner.newConfiguratorSpinner, By.xpath("/@class[contains(., 'slds-visible')]"));
        By spinners = new ByChained(parent, new ByAll(spinner.bauSpinner, spinner.collaSpinner1, spinner.collaSpinner2
                , spinner.lightSpinner, spinner.loadingSpinner
                , spinner.overlaySpinner, spinner.tinySpinner, spinners_class_visible_0, spinners_class_visible_1));
        try {
            verifyComponentInvisibility(spinners);
        } catch (Exception e) {
            i = i + 1;
            System.out.println("check spinner i : " + i);
            if (i > 10) {
                throw new Exception("Check Spinner error in parent : " + xpathToString(parent));
            }
        }
        baseTimeoutInterval = old;
    }


    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void press(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollElement);
        util.objectManager(oggettocliccabile, util.click);
    }

    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void pressAndCheckSpinners(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollElement);
        util.objectManager(oggettocliccabile, util.click);
        spinner.checkSpinners();
    }


    /**
     * @param element
     * @return
     * @throws Exception
     */
    public boolean getElementExistance(By element) throws Exception {
        return util.verifyExistence(element, baseTimeoutInterval);
    }


    /**
     * This method will imitate the behaviour of an user trying to repeatedly click on an element
     * covered by another one intercepting clicks (for example a spinner overlay).
     * It will verify the element is clickable, then it will try interacting for a maximum number of attempts.
     * If unsuccessful it will throw an exception.
     *
     * @param timeToWait seconds to wait (explicitly) for the element to become clickable
     * @param locator    identifier (By) for the element
     * @param maxClicks  maximum number of clicking attempts. If exceeded an exception will be thrown
     * @throws Exception
     */
    public void waitAndKeepClickingOnElement(int timeToWait, By locator, int maxClicks) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, timeToWait);
        WebElement theElement = wait.until(ExpectedConditions.elementToBeClickable(locator));
        util.keepClickingElementUntilSuccess(theElement, maxClicks);
    }

    /**
     * @param visibleObject
     * @throws Exception
     */
    public void verifyComponentVisibility(By visibleObject) throws Exception {
        if (!util.verifyVisibility(visibleObject, baseTimeoutInterval)) {
            throw new Exception("The element located by " + visibleObject + " does not exist or is not visible.");
        }
    }

    /**
     * @param invisibleObject
     * @throws Exception
     */
    public void verifyComponentInvisibility(By invisibleObject) throws Exception {
        if (util.verifyVisibility(invisibleObject, baseTimeoutInterval)) {
            throw new Exception("The element located by " + invisibleObject + " is visible.");
        }
    }

    /**
     * @param element
     * @return
     */
    public boolean checkDisabled(By element) {
        try {
            boolean compare = xpathToString(element).contains(disabled_value);
            if (compare)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param objectWithText
     * @param textToCheck
     * @throws Exception
     */
    public void verifyComponentText(By objectWithText, String textToCheck) throws Exception {
        textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        WebElement problemElement = driver.findElement(objectWithText);
        String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        if (elementText.length() < 1) {
            elementText = problemElement.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        }
        if (!elementText.contains(textToCheck)) {
            String message = "The text displayed by element located by " + objectWithText + " is:\n'" + elementText + "'\nIt's different from:\n'" + textToCheck + "'";
            System.out.println(message);
            throw new Exception(message);
        }
    }

    /**
     * @param objectWithText
     * @param textToCheck
     * @throws Exception
     */
    public void verifyComponentTextIsNot(By objectWithText, String textToCheck) throws Exception {
        try {
            verifyComponentText(objectWithText, textToCheck);
        } catch (Exception e) {
            throw new Exception("The element located by " + objectWithText + " have the text = " + textToCheck + " .");
        }
    }

    /**
     * @param objectWithText
     * @param textToCheck
     * @param result
     * @throws Exception
     */
    public void verifyComponentText2(By objectWithText, String textToCheck, boolean result) throws Exception {
        //	verifyComponentExistence(objectWithText);
        if (!util.verifyText(objectWithText, textToCheck)) {
            if (result)
                throw new Exception("The element located by " + objectWithText + " doesn't have the text = " + textToCheck + " .");
            if (!result)
                throw new Exception("The element located by " + objectWithText + " have the text = " + textToCheck + " .");
        }
    }

    /**
     * @param objectWithText
     * @param textToCheck
     * @throws Exception
     */
    public void verifyComponentValue(By objectWithText, String textToCheck) throws Exception {
        textToCheck = textToCheck.replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        WebElement problemElement = driver.findElement(objectWithText);
        String elementText = problemElement.getText().replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        if (elementText.length() < 1) {
            elementText = problemElement.getAttribute("value").replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        }
        if (!elementText.contains(textToCheck)) {
            String message = "The value displayed by element located by " + objectWithText + " is:\n" + elementText + "\nIt's different from:\n" + textToCheck;
            System.out.println(message);
            throw new Exception(message);
        }
    }

    /**
     * @param objectWithText
     * @param textToCheck
     * @throws Exception
     */
    public void verifyVisibilityAndText(By objectWithText, String textToCheck) throws Exception {
        verifyComponentVisibility(objectWithText);
        verifyComponentText(objectWithText, textToCheck);
    }

    /**
     * @param locator
     * @throws Exception
     */
    public void verifyVisibilityThenClick(By locator) throws Exception {
        //A clickable element is necessarily visible
        if (util.verifyClickability(locator, baseTimeoutInterval)) {
            clickComponent(locator);
        } else {
            throw new Exception("The element located by " + locator + " does not exist or is not clickable.");
        }
    }

    /**
     * @param locator
     * @throws Exception
     */
    public void verifyVisibilityThenJsClick(By locator) throws Exception {
        //A clickable element is necessarily visible
        if (util.verifyClickability(locator, baseTimeoutInterval)) {
            util.jsClickElement(locator);
        } else {
            throw new Exception("The element located by " + locator + " does not exist or is not clickable.");
        }
    }


    /**
     * @param objectWithText
     * @param substrings
     * @throws Exception
     */
    public void verifyComponentTextContainsSubstrings(By objectWithText, String[] substrings) throws Exception {
        WebElement element = driver.findElement(objectWithText);
        String theText = element.getText().replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
        for (String substring : substrings) {
            substring = substring.replace("\r", "").replace("\n", "").replace("\r\n", "").toLowerCase();
            if (!theText.contains(substring.toLowerCase())) {
                String message = "The text displayed by element located by " + objectWithText + " is:\n" + theText + "\nIt does not contain substring:\n" + substring;
                throw new Exception(message);
            }
        }
    }

    /**
     * @param locators
     * @param strings
     * @throws Exception
     */
    public void verifyElementsArrayText(By[] locators, String[] strings) throws Exception {
        if (locators.length != strings.length) {
            throw new Exception("The numbers of strings to check does not match the number of elements");
        }
        for (int i = 0; i < locators.length; i++) {
            //			System.out.println("Checking string: " + strings[i]);
            verifyComponentVisibility(locators[i]);
            verifyComponentText(locators[i], strings[i]);
        }
    }

    /**
     * @param locators
     * @throws Exception
     */
    public void verifyElementsArrayVisibility(By[] locators) throws Exception {
        for (int i = 0; i < locators.length; i++) {
            verifyComponentVisibility(locators[i]);
        }
    }

    /**
     * This method checks if an element attribute has a specific value. It does not verify existence nor visibility
     * of the element, so this check has to be done before method invocation
     *
     * @param locator
     * @param attribute
     * @param value
     * @throws Exception
     */
    public void verifyAttributeValue(By locator, String attribute, String value) throws Exception {
        WebElement element = driver.findElement(locator);
        String attrValue = element.getAttribute(attribute);
        if (!attrValue.equals(value)) {
            throw new Exception("The element located by:\n" + locator + "\nhas value: " + attrValue + "\nfor attribute: " + attribute + "\ninstead of expected value: " + value);
        }
    }

    /**
     * @param theUrl
     * @throws Exception
     */
    public void confirmUrl(String theUrl) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, baseTimeoutInterval);
        if (!wait.until(ExpectedConditions.urlToBe(theUrl))) {
            String current = driver.getCurrentUrl();
            throw new Exception("Browser did not load required url: " + theUrl + "\nCurrent url: " + current);
        }
    }

    /**
     * @param locator
     * @return
     * @throws Exception
     */
    public String getElementTextString(By locator) throws Exception {
        verifyComponentVisibility(locator);
        WebElement element = driver.findElement(locator);
        return element.getText();
    }

    /**
     * @param locator
     * @return
     * @throws Exception
     */
    public String getElementInnerTextString(By locator) throws Exception {
        WebElement element = driver.findElement(locator);
        return element.getAttribute("innerText");
    }


    /**
     * This method checks the color of an element against a required color.
     * It does not check for the presence or visibility of the element, so these conditions have to be verified before
     * method call.
     *
     * @param locator
     * @param colorToCheck
     * @throws Exception
     */
    public void verifyElementColor(By locator, Color colorToCheck) throws Exception {
        Color elementColor = Color.fromString(driver.findElement(locator).getCssValue("color"));
        if (!elementColor.equals(colorToCheck)) {
            throw new Exception("The element located by:\n" + locator + "\nhas color: " + elementColor + "\ninstead of expected color: " + colorToCheck);
        }
    }

    /**
     * @param locator
     * @param textToInsert
     * @throws Exception
     */
    public void insertText(By locator, String textToInsert) throws Exception {
        util.objectManager(locator, util.sendKeys, textToInsert);
    }

    /**
     * @param locator
     * @param textToInsert
     * @throws Exception
     */
    public void insertTextAndReturn(By locator, String textToInsert) throws Exception {
        insertText(locator, textToInsert);
        WebElement textField = driver.findElement(locator);
        textField.sendKeys(Keys.ENTER);
    }

    /**
     * @param locator
     * @param textToInsert
     * @throws Exception
     */
    public void insertTextByChar(By locator, String textToInsert) throws Exception {
        if (util.verifyExistence(locator, baseTimeoutInterval)) {
            util.objectManager(locator, util.clickwithjse);
            util.objectManager(locator, util.sendKeysCharByChar, textToInsert);
        } else throw new Exception("Impossibile interagire col campo dove inserire il testo " + textToInsert);
    }

    /**
     * Simple method to verify strings inside a  table
     *
     * @param locators       Two dimensional array of element locators
     * @param stringsToCheck Two dimensional array of strings
     * @throws Exception
     */
    public void verifyTable(By[][] locators, String[][] stringsToCheck) throws Exception {
        for (int i = 0; i < stringsToCheck.length; i++) {
            verifyElementsArrayText(locators[i], stringsToCheck[i]);
        }
    }

    /**
     * @param label
     * @param value
     * @throws Exception
     */
    public void selezionaLigtheningValue(String label, String value) throws Exception {
        util.selectLighningValue(label, value);
        spinner.checkSpinners();
    }

    /**
     * @param label
     * @param value
     * @throws Exception
     */
    public void selezionaLigtheningValueInSrollView(String label, String value) throws Exception {
        util.selectLighningValueInSrollView(label, value);
        spinner.checkSpinners();
    }

    /**
     * @param label
     * @return
     * @throws Exception
     */
    public String getLigtheningValue(String label) throws Exception {
        return getValue("//label[text()='" + label + "']/..//input");
    }

    /**
     * @param section
     * @param label
     * @param value
     * @throws Exception
     */
    public void selezionaLigtheningValue(String section, String label, String value) throws Exception {
        util.selectLighningValue(section, label, value);
        spinner.checkSpinners();
    }

    /**
     * @param label
     * @param value
     * @throws Exception
     */
    public void selezionaValore(String label, String value) throws Exception {
        util.objectManager(By.xpath("//label[contains(text(),'" + label + "')]/following::select[1] | //span[contains(text(),'" + label + "')]/following::select[1]"), util.scrollAndSelect, value);
        spinner.checkSpinners();
    }


    /**
     * This method allow the simulation of click the element with JavaScript
     *
     * @param locators Two dimensional array of element locators
     * @throws Exception
     */
    public void clickComponentWithJse(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollElement);
        util.objectManager(oggettocliccabile, util.clickwithjse);
    }


    /**
     * This method allow the simulation of click the element with JavaScript and wait SFDC spinners
     *
     * @param locators Two dimensional array of element locators
     * @throws Exception
     */
    public void clickComponentWithJseAndCheckSpinners(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollElement);
        util.objectManager(oggettocliccabile, util.clickwithjse);
        spinner.checkSpinners();
    }


    /**
     * @param obj
     * @return
     */
    public int countElement(By obj) {
        return util.countElement(obj);
    }

    /**
     * @param obj
     * @return
     * @throws Exception
     */
    public boolean verifyClickability(By obj) throws Exception {
        return util.verifyClickability(obj, baseTimeoutInterval);
    }

    /**
     * @param xpath
     * @return
     */
    public String[] getAttributesName(String xpath) {
        return util.getAttributesNameWithJse(xpath);
    }


    /**
     * @param xpath
     * @return
     */
    public String getValue(String xpath) {
        return util.getValueWithJse(xpath);
    }


    /**
     * @param xpath
     * @return
     * @throws Exception
     */
    public String getValue(By xpath) throws Exception {
        return util.getValueWithJse(util.xpathToString(xpath));
    }


    /**
     * @param xpath
     * @return
     * @throws Exception
     */
    public String xpathToString(By xpath) throws Exception {
        return util.xpathToString(xpath);
    }


    /**
     * @param oggettocliccabile
     * @throws Exception
     */
    public void scrollComponent(By oggettocliccabile) throws Exception {
        util.objectManager(oggettocliccabile, util.scrollElement);
    }

    /**
     * @param element
     * @param n_ancestor
     * @throws Exception
     */
    public void clickAncestorClickable(String element, int n_ancestor) throws Exception {
        int i = 0;
        boolean ok = false;
        while (i < n_ancestor || ok == false) {
            try {
                ok = clickComponentIfExist(By.xpath(element));
                i = i + 1;
                element = element + any_parent;
            } catch (Exception e) {
                //				throw new Exception(e);
            }
        }
        if (!ok) {
            throw new Exception("Error : clickable element not found with xpath : " + element);
        }
    }

    /**
     * @param element
     * @param n_descendent
     * @throws Exception
     */
    public void clickDescendentClickable(String element, int n_descendent) throws Exception {
        int i = 0;
        boolean ok = false;
        while (i < n_descendent || ok == false) {
            try {
                ok = clickComponentIfExist(By.xpath(element));
                i = i + 1;
                element = element + any_descendant;
            } catch (Exception e) {
                throw new Exception("Error : clickable element not found with xpath : " + element, e.fillInStackTrace());
            }
            if (!ok) {
                throw new Exception("Error : clickable element not found with xpath : " + element);
            }
        }
    }


    //@SuppressWarnings("finally")
    //TODO DA TESTARE SE FUNZIONTE
    //		public void clickDescendentClickable(String element , int n_ancestor) throws Exception {
    //			int i = 0;
    //			boolean ok = false;
    //			while(i<n_ancestor) {
    //				try {
    //					clickComponent(By.xpath(element));
    //					ok = true;
    //					break;
    //				} catch (Exception e) {
    //					i = i +1;
    //					element = element + any_descendant;
    //				}
    //				if(!ok){
    //					throw new Exception("Error : clickable element not found with xpath : " + element);
    //				}
    //			}
    //
    //
    //		}

    /**
     * @param element
     * @param by_existence
     * @throws Exception
     */
    public void checkIfElementIsDisabled(By element, boolean by_existence) throws Exception {
        try {
            if (by_existence) {
                verifyComponentExistence(new ByChained(element, By.xpath(disabled_value)));
            } else {
                verifyComponentInexistence(new ByChained(element, By.xpath(not_disabled_value)));
            }
        } catch (Exception e) {
            throw new Exception("The element is disabled : " + element);
        }
    }

    /**
     * @param element
     * @param byExistenceOrInexistence
     * @param return_boolean
     * @return
     * @throws Exception
     */
    public boolean checkIfElementIsDisabled(By element, boolean byExistenceOrInexistence, boolean return_boolean) throws Exception {
        try {
            if (byExistenceOrInexistence) {
                verifyComponentExistence(new ByChained(element, By.xpath(disabled_value)));
            } else {
                verifyComponentInexistence(new ByChained(element, By.xpath(not_disabled_value)));
            }
        } catch (Exception e) {
            return true;
        }
        return false;
    }
    
    public boolean verifyExistence(By by, int timeout) throws Exception{
    	return util.verifyExistence(by, timeout);
    }
    
    public void compareText(By by, String text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		System.out.println(text);
		if(!text.equals(weText))
			throw new Exception("Text mismatch. The passed text is not equal to the one within bolletteHeaderText item.");
	}
	
	public void compareText(By by, String[] text, boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("Normalized:\n"+weText);
		for(String s : text){
			System.out.println(s);
			if(!weText.contains(s))
				throw new Exception("The webpage container does not contain the text '"+s+"'");
		}
	}
	
	public void compareTextDisabledInput(String xpath, String textToCheck) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String s = (String)js.executeScript("return (document.evaluate(\""+xpath+"\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value");
		if(!s.equals(textToCheck))
			throw new Exception("Il testo cercato:\n\t"+textToCheck+"\nè diverso da quello in pagina:\n\t"+s);
	}
	
	public String normalizeInnerHTML(String s){
		int less = s.indexOf("<"); int greater = s.indexOf(">");
		if(less==-1)
			return ((((s.replace("\n", "")).replace("\r", "")).replace("\t", "")).replace("&nbsp;", "")).trim().replaceAll(" +", " ");
		else{
			String tag = s.substring(less, greater+1);
			String endTag = null;
			if(!tag.contains("<br>"))
				endTag = tag.replace("<", "</");
			if(endTag!=null)
				return normalizeInnerHTML((s.replace(tag, "")).replace(endTag, ""));
			else
				return normalizeInnerHTML(s.replace(tag, ""));
		}
	}
	
	public void compareStringList(String[] propList, String[] valueList) throws Exception{
		for (int i=0; i<propList.length; i++){
			System.out.println(" propList --> " + propList[i]);
			System.out.println(" valueList --> " + valueList[i]);
			if (!propList[i].equals(valueList[i]))
				throw new Exception("The values don't match");		
		}		
		
	}
	
	public static String generateRandomPOD(int n) {
	    int m = (int) Math.pow(10, n - 1);
	    m = m + new Random().nextInt(9 * m);
	    return String.valueOf(m);
	}
	
	public void containsText(By by, String text) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		weText = we.getText();		
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	
	public void containsText(By by, String text, Boolean nestedTags) throws Exception{
		WebElement we = util.waitAndGetElement(by);
		String weText = we.getAttribute("innerHTML");
		if(nestedTags)
			weText = normalizeInnerHTML(weText);
		else
			weText = we.getText();
		System.out.println("weText -->" + weText);
		if (!weText.contains(text))
			throw new Exception("There isn't " + text);
	}
	

	
	public void launchLinkBasicAuth(String url) throws Exception {
		if(!Costanti.WP_BasicAuth_Username.equals(""))
			url = url.replace("https://", "https://"+Costanti.WP_BasicAuth_Username+":"+Costanti.WP_BasicAuth_Password+"@");
		try {
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e){
			throw new Exception("it's impossible the link " + url);
		}
	}
	
	public void launchLink(String url) throws Exception {
		try{
			driver.manage().window().maximize();
			driver.navigate().to(url);
		} catch (Exception e){
			throw new Exception("it's impossible the link " + url);
		}
	}
	
	public void checkURLAfterRedirection(String url) throws Exception{
		if(!url.equals(driver.getCurrentUrl()))
			throw new Exception("Redirection failed! The current URL '"+driver.getCurrentUrl()+"' is different from the one saved before '"+url+"'.");
	}
	
	public void backBrowser(By checkObject) throws Exception {
	    try {
	    	WebDriverWait WaitVar = new WebDriverWait(driver, 10);
	    	driver.navigate().back();
	    	WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkObject));           
	    } catch (Exception e) {
	        throw new Exception("Previous Page not displayed");
	    }
	}
	
	public void checkUrl(String url) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		String link=driver.getCurrentUrl();
		if (url.contains("https://"))
			url = url.substring(8);
		if (!link.contains(url))
			 throw new Exception("the url della pagina e' diverso da quello atteso: l'url della pagina e' "+link+";quello atteso e' "+url);
	}
	
	public void SwitchToWindow() throws Exception{
		Set<String> newWindows = driver.getWindowHandles();
		for (String winHandle : newWindows) {
	        driver.switchTo().window(winHandle);
	        break;
		 }
	}
	
	public void SwitchToWindow_2() throws Exception{
		Set<String> newWindows = driver.getWindowHandles();
		for (String winHandle : newWindows) {
	        driver.switchTo().window(winHandle);
		 }
	}
	
	public void waitForElementToDisplay (By checkObject) throws Exception{
		WebDriverWait wait = new WebDriverWait (driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(checkObject));
		
	}
	
	public void  isElementNotPresent(By existentObject ) throws Exception{
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		try{
			WaitVar.until(ExpectedConditions.invisibilityOfElementLocated(existentObject));
		} catch (Exception e) {
        	e.printStackTrace();
        }
     }
	/**
	 * Effettua il click su un elemento tentando per 5 volte
	 * @param locator
	 * @throws Exception
	 */
	public void clickElementWhitRetry(By locator) throws Exception
	{
		int numeroTentativi=0;
		while(true)
		{
			numeroTentativi++;
			try {
				util.objectManager(locator, util.scrollAndClick);
				break;
			}
			catch(Exception ex) {
				if(numeroTentativi>=5) throw ex;
				Thread.sleep(3000);
			}
		}
	}
}


