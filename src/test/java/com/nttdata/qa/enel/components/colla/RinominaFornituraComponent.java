package com.nttdata.qa.enel.components.colla;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nttdata.qa.enel.util.SeleniumUtilities;

public class RinominaFornituraComponent {
	WebDriver driver;
	SeleniumUtilities util;
	public By accessForniture = By.xpath("//div[@class='section-heading']/h2[text()='Le tue forniture']");
	public By fornituraAttiva = By.xpath("//div[@class='card forniture regolare']");
	public By detailsF = By.xpath("//div[@class='heading supply']//dl[@class='supply-details-heading']");
	public By rinominaFornituraButton = By.xpath("//a[@aria-label='rinomina il titolo della tua fornitura']");
    public By accessFornituraRenamePage=By.xpath("//div[@class='form-group-container simply-border']//h4[normalize-space('Inserisci il nuovo nome della fornitura')]");
	public By inputboxNomeFornitura=By.id("input_nomeFornitura");
    public By continuaButton=By.xpath("//button[@class='button_first']//span[text()='CONTINUA']");
    public By newName=By.xpath("//div[@class='details-container modifySupplyName odd']//span//dd[@id='aliasRiepilogo']");
    public By confermaButton=By.xpath("//button[@class='button_first']//span[text()='CONFERMA']");
    public By finishedOperation=By.xpath("//div[@class='step-box-container']//div[@class='right-main']//h4[text()='Operazione eseguita correttamente']");
    public By fineButton=By.xpath("//button[@class='button_first']//span[text()='FINE']");
    public By updatedName=By.xpath("//section[@data-context='info-dati-fatturazione']//h1[@id='fornitura01']");
    public By sezioneContrattoScaricaPDF=By.xpath("//section[@data-context='info-dati-fatturazione']//dt[text()='Contratto']/ancestor::span/dd/span[text()='Scarica Pdf']");
    public By iconaScaricaPdf=By.xpath("//section[@data-context='info-dati-fatturazione']//dt[text()='Contratto']/ancestor::span/dd/span[text()='Scarica Pdf']/ancestor::dd/a[contains(@href,'/bin/areaclienti/auth/contratti/download?url')]");
    public RinominaFornituraComponent(WebDriver driver) throws Exception {
		this.driver = driver;
		util = new SeleniumUtilities(this.driver);
	}

	public void verifyComponentExistence(By oggettoEsistente) throws Exception {
		if (!util.exists(oggettoEsistente, 15))
			throw new Exception("l'oggetto con xpath " + oggettoEsistente + " non esiste.");
	}

	public void clickComponent(By oggettocliccabile) throws Exception {
		util.objectManager(oggettocliccabile, util.scrollToVisibility, false);
		util.objectManager(oggettocliccabile, util.scrollAndClick);

	}
	
	public String createFornituraName() {
		LocalDate today = LocalDate.now();
		String giorno=today.toString();
		String time=new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime()).toString();
		String name="Test"+giorno+time;
		name=name.replace("-", "");
		name=name.replace(":", "");
		return name;
	}
	
	public void insertFornituraName(By inserisciOggetto, String valore) throws Exception {
		util.objectManager(inserisciOggetto, util.sendKeys, valore);
	}

	public void verifyExistenceAndClickFornituraAttiva(By checkOggetto, String state, String userNumber)
			throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkOggetto));
		String description = "";
		List<WebElement> checkFornitura = driver.findElements(checkOggetto);
		int dim = checkFornitura.size();

		int count = 0;
		String fornituraTrovata = "NO";

		for (WebElement li : checkFornitura) {
			int indiceFornitura = checkFornitura.indexOf(li);
			description = li.getText();
			if (description.contains(state) && description.contains(userNumber)) {
				count = indiceFornitura + 1;
				fornituraTrovata = "Y";
				break;
			}
			if (fornituraTrovata == "Y")
				break;
		}
		if (fornituraTrovata != "Y")
			throw new Exception(
					"nella sezione privata del cliente nell'area 'Forniture' non e' visibile la fornitura con stato: "
							+ state + " e numero utente: " + userNumber);
		else {
			WaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='card forniture regolare']["
					+ count + "]//div[@class='button-container']//a[contains(@onclick,'goToDettaglio')]")));
			driver.findElement(By.xpath("//div[@class='card forniture regolare'][" + count
					+ "]//div[@class='button-container']//a[contains(@onclick,'goToDettaglio')]")).click();
		}
	}

	public void verifyDetailsFornituta(By oggettoEsistente, String state, String userNumber, String details)
			throws Exception {
		String description = "";
		String stateDescription = driver.findElement(By.xpath("//div[@class='supply details-container']")).getText();
		List<WebElement> detailsFornitura = driver.findElements(oggettoEsistente);
		for (WebElement li : detailsFornitura) {
			description = li.getText();
			if (description.contains(userNumber) && description.contains(details)) {
				if (!stateDescription.contains(state))
					throw new Exception(
							"nell'area 'Forniture' sulla pagina dettaglio fornitura non e' visibile la fornitura con stato: "
									+ state);
			}

			else
				throw new Exception(
						"nell'area 'Forniture' sulla pagina dettaglio fornitura non e' visibile la fornitura con numero utente: "
								+ userNumber + "; indirizzo utente: " + details);
		}

	}
	
	public void verifyChangeNameFornitura(By oggettoEsistente, String newName, String page) throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(oggettoEsistente));
		String description="";
		WebElement detailsFornitura = driver.findElement(oggettoEsistente);
			description = detailsFornitura.getText();
			if (!description.contentEquals(newName)) 
			  throw new Exception("sulla pagina '"+page+"' non è visualizzabile il Nome Fornitura con il valore atteso: "+newName);
	}
	
	public void verifyExistenceAndClickFornitura(By checkOggetto, String state, String userNumber, String commodity)
			throws Exception {
		WebDriverWait WaitVar = new WebDriverWait(driver, 10);
		WaitVar.until(ExpectedConditions.visibilityOfElementLocated(checkOggetto));
		String description = "";
		List<WebElement> checkFornitura = util.waitAndGetElements(checkOggetto);
		int dim = checkFornitura.size();

		int count = 0;
		String fornituraTrovata = "NO";

		for (WebElement li : checkFornitura) {
			int indiceFornitura = checkFornitura.indexOf(li);
			description = li.getText();
			//System.out.println(description);
			if (description.contains(state) && description.contains(userNumber) && description.contains(commodity) ) {
				count = indiceFornitura + 1;
				fornituraTrovata = "Y";
				break;
			}
			if (fornituraTrovata == "Y")
				break;
		}
		if (fornituraTrovata != "Y")
			throw new Exception(
					"nella sezione privata del cliente nell'area 'Forniture' non e' visibile la fornitura con stato: "
							+ state + " e numero utente: " + userNumber);
		else {
			WaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='card forniture regolare']["
					+ count + "]//div[@class='button-container']//a[contains(@onclick,'goToDettaglio')]")));
			//driver.findElement(By.xpath("//div[@class='card forniture regolare'][" + count
				//	+ "]//div[@class='button-container']//a[contains(@onclick,'goToDettaglio')]")).click();
			util.objectManager(By.xpath("//div[@class='card forniture regolare'][" + count
					+ "]//div[@class='button-container']//a[contains(@onclick,'goToDettaglio')]"), util.scrollToVisibility, false);
			util.objectManager(By.xpath("//div[@class='card forniture regolare'][" + count
					+ "]//div[@class='button-container']//a[contains(@onclick,'goToDettaglio')]"), util.scrollAndClick);
		}
	}
	
	public void verificaDettaglioFornEClickSuMostraDiPiù(By oggettoEsistente, String commodity, String userNumber)
			throws Exception {
		String description = "";
		String titolo=util.waitAndGetElement(By.xpath("//section[@data-context='info-dati-fatturazione']//h1[@id='fornitura01']")).getText();
		List<WebElement> detailsFornitura = util.waitAndGetElements(oggettoEsistente);
		for (WebElement li : detailsFornitura) {
			description = li.getText();
			String details="L’indirizzo della tua fornitura "+commodity.toLowerCase()+" è:";
			
			if (description.contains(userNumber) && description.contains(details) && titolo.contentEquals(commodity)) {
				util.objectManager(By.xpath("//div[@class='open-supply']//a"), util.scrollToVisibility, false);
				util.objectManager(By.xpath("//div[@class='open-supply']//a"), util.scrollAndClick);
			}

			else
				throw new Exception(
						"nell'area 'Forniture' sulla pagina dettaglio fornitura non e' visibile la fornitura con titolo: "+commodity+"; numero utente: "
								+ userNumber + "; testo: " + details);
		}

	}

}
