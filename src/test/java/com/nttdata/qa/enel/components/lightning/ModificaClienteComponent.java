package com.nttdata.qa.enel.components.lightning;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;


import com.nttdata.qa.enel.util.CodiceFiscale;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.SpinnerManager;


public class ModificaClienteComponent {
    private WebDriver driver;
    private SeleniumUtilities util;
    String frameName;
    SpinnerManager spinner;
    public ModificaClienteComponent(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
    util=new SeleniumUtilities(driver);
    spinner = new SpinnerManager(driver);
    
  }
    
    private final By inputNome = By.xpath("//input[contains(@id,'FirstName')]");
    private final By inputCognome = By.xpath("//input[contains(@id,'LastName')]");
    private final By inputCF = By.xpath("//input[contains(@id,'accountCF')]");
    private final By inputTelefonoCasa = By.xpath("//input[contains(@name,'contactHomePhoneField')]");
    private final By inputTelefonoCliente = By.xpath("//input[contains(@name,'accountPhoneInputField')]");
    private final By selectDescrizioneTelefonoCasa = By.xpath("//select[contains(@name,'contactHomePhoneFieldDescription')]");
    private final By inputCellulare = By.xpath("//input[contains(@name,'contactMobilePhoneField')]");
    private final By selectDescrizioneCellulare = By.xpath("//select[contains(@name,'contactMobilePhoneFieldDescription')]");
    private final By inputEmail = By.xpath("//input[contains(@name,'contactEmailField')]");
    private final By selectDescrizioneEmail = By.xpath("//select[contains(@name,'contactEmailFieldDescription')]");
    private final By inputRagioneSociale = By.xpath("//input[contains(@name,'accountNameInputField')]");
    private final By inputRagioneSocialeBusiness = By.xpath("//input[contains(@name,'accountRagioneSocialeInputField')]");

    private final By selectModalitaAccettazione = By.xpath("//select[contains(@name,'caseMethodOfAgreementField')]");
    private final By modalControlloForniture = By.xpath("//*[text()='Controllo Forniture']");
    private final By modalButtonSi = By.xpath("//*[text()='Si' and @id='hmbCustomButtonSi'] ");

    public final By buttonConferma = By.xpath("//button[@id='confirmButton2']");
    public final By buttonVerificaIndirizzo = By.xpath("//button[text()='Verifica Indirizzo']");
    public final By buttonConfermaSelezioneReferente = By.xpath("//input[@value='Conferma Selezione Referente']");
    
    
    public void verificaControlloForniture(String frame) throws Exception{
    	if(util.exists(frame, modalControlloForniture, 5)) {
    		pressButton(frame, modalButtonSi);
    		spinner.checkSpinners();
    	}
    	
    }
    public void verificaControlloForniture() throws Exception{
    	if(util.exists( modalControlloForniture, 5)) {
    		pressButton( modalButtonSi);
    		spinner.checkSpinners();
    	}
    	
    }
    
    public void modificaRagioneSociale(String frame, String ragioneSociale) throws Exception{
    	util.frameManager(frame, inputRagioneSociale, util.clear);
    	spinner.checkSpinners(frame);
    	util.frameManager(frame, inputRagioneSociale, util.sendKeysAndEnter, ragioneSociale);
    	spinner.checkSpinners(frame);
    }
    
    public void modificaRagioneSocialeBusiness(String frame, String ragioneSociale) throws Exception{
    	util.frameManager(frame, inputRagioneSocialeBusiness, util.clear);
    	spinner.checkSpinners(frame);
    	util.frameManager(frame, inputRagioneSocialeBusiness, util.sendKeysAndEnter, ragioneSociale);
    	spinner.checkSpinners(frame);
    }
    
    public void modificaRagioneSocialeBusiness( String ragioneSociale) throws Exception{
    	util.objectManager( inputRagioneSocialeBusiness, util.clear);
    	spinner.checkSpinners();
    	util.objectManager( inputRagioneSocialeBusiness, util.sendKeysAndEnter, ragioneSociale);
    	spinner.checkSpinners();
    }
    
    public void modificaRagioneSociale( String ragioneSociale) throws Exception{
    	util.objectManager( inputRagioneSociale, util.clear);
    	spinner.checkSpinners();
    	util.objectManager( inputRagioneSociale, util.sendKeysAndEnter, ragioneSociale);
    	spinner.checkSpinners();
    }
    
    public void selezionaModalitaAccettazione(String frame,String modalita) throws Exception{
//    	if(util.exists(frame, selectModalitaAccettazione,5)) {
    	util.frameManager(frame, selectModalitaAccettazione, util.scrollToVisibility,true);
    	TimeUnit.SECONDS.sleep(7);
    	util.frameManager(frame, selectModalitaAccettazione, util.select, modalita);
    	spinner.checkSpinners(frame);
    	util.frameManager(frame, selectModalitaAccettazione, util.select, modalita);
    	spinner.checkSpinners(frame);
//    	}
    }
    public void selezionaModalitaAccettazione(String modalita) throws Exception{
//    	if(util.exists(frame, selectModalitaAccettazione,5)) {
    	util.getFrameActive();
    	util.objectManager( selectModalitaAccettazione, util.scrollToVisibility,true);
    	TimeUnit.SECONDS.sleep(7);
    	util.objectManager( selectModalitaAccettazione, util.select, modalita);
    	spinner.checkSpinners();
    	util.objectManager( selectModalitaAccettazione, util.select, modalita);
    	spinner.checkSpinners();
//    	}
    }
    
    public void pressButton(String frame,By button) throws Exception{
    	util.frameManager(frame, button, util.scrollAndClick);
    	spinner.checkSpinners(frame);
    }
    
    public void pressButton(By button) throws Exception{
    	util.objectManager( button, util.scrollAndClick);
    	spinner.checkSpinners();
    }
    
    public void pressButtonIfExist(String frame,By button) throws Exception{
    	if(util.exists(frame, button, 5)) {
    		WebElement frameToSw = driver.findElement(
    				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
    				);
    		driver.switchTo().frame(frameToSw);
    		WebElement el = driver.findElement(button);
    		if(!el.getAttribute("class").contains("slds-hide")) {
    			el.click();
    		}
    		driver.switchTo().defaultContent();
    	    spinner.checkSpinners(frame);
    	}
    }
    
    public void modificaDatiDiContatto(String frame,String telefono, String descrizioneTelefono, String cellulare, String descrizioneCellulare, String email, String descrizioneEmail) throws Exception{
    	util.frameManager(frame, inputTelefonoCasa, util.sendKeys, telefono);
    	util.frameManager(frame, inputCellulare, util.sendKeys, cellulare);
    	util.frameManager(frame, inputEmail, util.sendKeys, email);
    	util.frameManager(frame, selectDescrizioneTelefonoCasa, util.select, descrizioneTelefono);
    	util.frameManager(frame, selectDescrizioneCellulare, util.select, descrizioneCellulare);
    	util.frameManager(frame, selectDescrizioneEmail, util.select, descrizioneEmail);
    	
    }
    
    public void inserisciNome(String frame,String nome) throws Exception{
    	util.frameManager(frame, inputNome, util.sendKeys, nome);
    }
    
    public void inserisciCognome(String frame,String cognome) throws Exception{
    	util.frameManager(frame, inputCognome, util.sendKeys, cognome);
    }
    
    public void inserisciTelefonoCliente(String telefono) throws Exception{
    	util.objectManager( inputTelefonoCliente, util.sendKeys, telefono);
    	spinner.checkSpinners();
    }
    
    public void inserisciCf(String frame,String cf) throws Exception{
    	util.frameManager(frame, inputCF, util.clear);
    	spinner.checkSpinners();
    	util.frameManager(frame, inputCF, util.sendKeys, cf);
    	spinner.checkSpinners();
    }
    
    public void compilaDatiClienteResidenziale(String frame, String nome, String cognome) throws Exception{
    	inserisciNome(frame, nome);
    	inserisciCognome(frame, cognome);
    	String cf = new CodiceFiscale(nome,cognome,"Napoli","Marzo","1985","04","M").getCodiceFiscale();
    	inserisciCf(frame, cf);
    }
    
    
    public String modificaNomeEInserisci(String frame,String nome) throws Exception{
    	nome = cambiaVocali(nome);
    	util.frameManager(frame, inputNome, util.sendKeys, nome);
    	return nome;
    }
    
    public String recuperaNome(String frame) throws Exception{
    	WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
    	String nome = util.waitAndGetElement(inputNome).getAttribute("value");
    	driver.switchTo().defaultContent();
    	return nome;
    	
    }
    
    public String recuperaCF(String frame) throws Exception{
    	WebElement frameToSw = driver.findElement(
				By.xpath("//iframe[@name='" +frame +"'] | //frame[@name='" +frame +"']")
				);
		driver.switchTo().frame(frameToSw);
    	String nome = util.waitAndGetElement(inputCF).getAttribute("value");
    	driver.switchTo().defaultContent();
    	return nome;
    	
    }
    
    public String cambiaVocali(String field) throws Exception{
   
    	String nuovoCampo = "";
    	boolean sostituito = false;
    	for(int x = field.length()-1;x>=0;x--) {
    		if(!sostituito) {
    		switch(field.charAt(x)) {
	    		case 'A' :
	    			nuovoCampo = "E" + nuovoCampo; 
	    			sostituito = true;
	    			break;
	    		case 'E' :
	    			nuovoCampo =  "I" + nuovoCampo; 
	    			sostituito = true;
	    			break;
	    		case 'I' :
	    			nuovoCampo =  "O"+ nuovoCampo;
	    			sostituito = true;
	    			break;
	    		case 'O' :
	    			nuovoCampo = "U" + nuovoCampo; 
	    			sostituito = true;
	    			break;
	    		case 'U' :
	    			nuovoCampo = "A" + nuovoCampo; 
	    			sostituito = true;
	    			break;
	    		default : 
	    			nuovoCampo =  field.charAt(x) + nuovoCampo ;
	    			break;
    		}
    		}
    		else nuovoCampo =  field.charAt(x)+ nuovoCampo;
	
    	}
    	return nuovoCampo;
    	
    }
    
    
    
    
//    
//    
//    
//    @FindBy(xpath = "//div[@title='Nuovo' and (text() = 'Nuovo')]")
//    private WebElement nuovoClienteButton;
//    
//    public final By radioButtonResidenziale = By.xpath("//label[@for='ITA_IFM_Residential']/span[@class='slds-radio--faux']");
//    public final By radioButtonImpresaIndividuale = By.xpath("//label[@for='ITA_IFM_ImpresaIndividuale']/span[@class='slds-radio--faux']");
//    public final By radioButtonCondominio = By.xpath("//label[@for='ITA_IFM_Condominio']/span[@class='slds-radio--faux']");
//    public final By radioButtonBusiness = By.xpath("//label[@for='ITA_IFM_Business']/span[@class='slds-radio--faux']");
//    private final By confirmButton = By.xpath("//button[@id='modalNewAccountNext']");
//    
//   private final By cellulareField = By.xpath("//input[contains(@id,'valorecellulare')]");
//   private final By descrizioneCellulareField = By.xpath("//select[contains(@id,'descrizionecellulare')]");
//   
//
//   private final By inputSesso = By.xpath("//select[contains(@id,'sesso')]");
//   private final By inputDataNascita = By.xpath("//input[contains(@id,'datePicker')]");
//   private final By inputComuneNascita = By.xpath("//input[contains(@id,'lookupComuneDiNascita')]");
//   private final By inputPartitaIva = By.xpath("//input[contains(@id,'partitaIVA')]");
//   private final By inputRagioneSociale = By.xpath("//input[contains(@id,'nomeClienteRagioneSociale')]");
//   private final By inputAccount = By.xpath("//input[contains(@id,'codiceFiscaleAccount')]");
//   private final By selectTipoFormaGiuridica = By.xpath("//select[contains(@id,'TipoFormaGiuridica')]");
//   private final By selectRuoloReferente = By.xpath("//select[contains(@id,'ruolo')]");
//		   
//   private final By calcolaCodiceFiscaleButton = By.xpath("//button[text()='Calcola Codice Fiscale']");
//
//   private final By confermaButton = By.xpath("//input[contains(@id,'btnConferma')]");
//   
//   private final By confermaComuneNascita = By.xpath("//div[@id='lookupItemId']");  
//   
//	private By  tuttiIProcessi = new By.ByXPath("//div[@title = 'Tutti i processi'" + " and (text() = 'Tutti i processi')]");;
//	
//		   
//	private String compilaAnagraficaClienteResidenziale(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception{
//		util.frameManager(this.frameName, inputComuneNascita,util.sendKeys,comune);
//		TimeUnit.SECONDS.sleep(5);
//		util.frameManager(this.frameName, confermaComuneNascita, util.scrollAndClick);
//		util.frameManager(this.frameName, inputDataNascita,util.sendKeys,dataNascita + Keys.RETURN);
//		util.frameManager(this.frameName, inputNome,util.sendKeys,nome);
//		util.frameManager(this.frameName, inputCognome,util.sendKeys,cognome);
//		util.frameManager(this.frameName, inputSesso, util.select,sesso);
//		util.frameManager(this.frameName, calcolaCodiceFiscaleButton, util.scrollAndClick);
//		this.driver.switchTo().frame(this.frameName);
//		String cf = util.waitAndGetElement(inputCF).getAttribute("value");
//		this.driver.switchTo().defaultContent();
//		return cf;
//	}
//	
//	private void compilaAnagraficaReferente(String nome, String cognome, String sesso, String dataNascita, String comune) throws Exception{
//		this.compilaAnagraficaClienteResidenziale(nome,cognome,sesso,dataNascita,comune);
//	}
//	
//	private void compilaAnagraficaReferenteConRuolo(String nome, String cognome, String sesso, String dataNascita, String comune,String ruolo) throws Exception{
//		util.frameManager(this.frameName, selectRuoloReferente,util.select,ruolo);
//		this.compilaAnagraficaClienteResidenziale(nome,cognome,sesso,dataNascita,comune);
//	}
//	
//	public void compilaContatti(String frameName,String cellulare,String descrizioneCellulare) throws Exception {
//		util.frameManager(frameName, cellulareField, util.sendKeys,cellulare);
//		util.frameManager(frameName, descrizioneCellulareField, util.select,descrizioneCellulare);
//		
//	}
//	
//	public void confermaCreazione() throws Exception {
//		util.frameManager(this.frameName, confermaButton, util.scrollAndClick);
//	}
//	
//	public void verificaCreazione() throws Exception {
//		
//		try {
//			this.driver.switchTo().defaultContent();
//			util.waitAndGetElement(tuttiIProcessi);
//		} catch(Exception e) {
//			throw new Exception("Impossibile creare la nuova anagrafica. Verificare i dati inseriti");
//		}
//	}
//	
//	private void compilaAnagraficaImpresaIndividuale(String piva, String ragioneSociale) throws Exception{
//		   util.frameManager(this.frameName, inputPartitaIva, util.sendKeys,piva);
//		   util.frameManager(this.frameName, inputRagioneSociale, util.sendKeys,ragioneSociale);
//	}
//	
//	private void compilaAnagraficaCondominio(String cf,String piva, String ragioneSociale) throws Exception{
//		util.frameManager(this.frameName, inputAccount, util.sendKeys,cf);
//		this.compilaAnagraficaImpresaIndividuale(piva, ragioneSociale);
//	}
//	
//	private void compilaAnagraficaBusiness(String cf, String piva, String ragioneSociale, String tipoFormaGiuridica) throws Exception {
//		this.compilaAnagraficaCondominio(cf, piva, ragioneSociale);
//		util.frameManager(this.frameName, selectTipoFormaGiuridica, util.select,tipoFormaGiuridica);
//	}
//	
//	public String selezionaTipologiaCliente(By tipologia) throws Exception{
//		
//		TimeUnit.SECONDS.sleep(7);
//		nuovoClienteButton.click();
//		String frame = util.getFrameByIndex(0);
//		this.frameName = frame;
//		util.frameManager(frame, tipologia, util.scrollAndClick);
//		util.frameManager(frame, confirmButton, util.scrollAndClick);
//		return frame;
//		
//	}
//
//
//	public void creaClienteResidenziale(Properties prop) throws Exception {
//		String cf = this.compilaAnagraficaClienteResidenziale(prop.getProperty("NOME"),prop.getProperty("COGNOME"),prop.getProperty("SESSO"),prop.getProperty("DATA_NASCITA"),prop.getProperty("COMUNE_NASCITA"));
//		prop.setProperty("CODICE_FISCALE", cf);
//	
//	}
//	
//	public void creaClienteImpresaIndividuale(Properties prop) throws Exception {
//		this.compilaAnagraficaImpresaIndividuale(prop.getProperty("PARTITA_IVA"), prop.getProperty("RAGIONE_SOCIALE"));
//		this.compilaAnagraficaReferente(prop.getProperty("NOME_REFERENTE"),prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"));
//		
//	}
//
//	public void creaClienteCondominio(Properties prop) throws Exception {
//		this.compilaAnagraficaCondominio(prop.getProperty("CODICE_FISCALE"),prop.getProperty("PARTITA_IVA"), prop.getProperty("RAGIONE_SOCIALE"));
//		this.compilaAnagraficaReferente(prop.getProperty("NOME_REFERENTE"),prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"));
//	}
//	
//	public void creaClienteBusiness(Properties prop) throws Exception {
//		this.compilaAnagraficaBusiness(prop.getProperty("CODICE_FISCALE"),prop.getProperty("PARTITA_IVA"), prop.getProperty("RAGIONE_SOCIALE"),prop.getProperty("TIPO_FORMA_GIURIDICA"));
//		this.compilaAnagraficaReferenteConRuolo(prop.getProperty("NOME_REFERENTE"),prop.getProperty("COGNOME_REFERENTE"),prop.getProperty("SESSO_REFERENTE"),prop.getProperty("DATA_NASCITA_REFERENTE"),prop.getProperty("COMUNE_NASCITA_REFERENTE"),prop.getProperty("RUOLO_REFERENTE"));
//
//	}
	
}
