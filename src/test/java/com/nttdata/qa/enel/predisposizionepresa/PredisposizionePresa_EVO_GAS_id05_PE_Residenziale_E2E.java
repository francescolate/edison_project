package com.nttdata.qa.enel.predisposizionepresa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiPPGas;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoPredisposizionePresaEVOGas;
import com.nttdata.qa.enel.testqantt.Recupera_Oi_Order_senza_POD;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.SalvaOiRicerca;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SceltaProcessoPredisposizionePresaEVOGas;
import com.nttdata.qa.enel.testqantt.SetPropertyPredPresaGasId5_01;
import com.nttdata.qa.enel.testqantt.SetPropertyPredPresaGasId5_02;
import com.nttdata.qa.enel.testqantt.SetPropertyPredPresaGasId5_03;
import com.nttdata.qa.enel.testqantt.SetPropertyPredPresaGasId5_04;
import com.nttdata.qa.enel.testqantt.SetPropertyPredPresaGasId5_05;
import com.nttdata.qa.enel.testqantt.ValidazionePreventivoPP;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaPPDopoPreventivoEVO;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaPredisposizionePresaEVO;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_2OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPredisposizionePresa_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;


public class PredisposizionePresa_EVO_GAS_id05_PE_Residenziale_E2E {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
		prop.setProperty("TIPO_UTENZA","PE");
		prop.setProperty("ANNULLA_OFFERTA","N");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("PDR", "00881906559602");
		prop.setProperty("CODICE_FISCALE", "MTGPCH76D18Z357B");
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("INDIRIZZO_EMAIL", "");
		prop.setProperty("TIPO_OPERAZIONE", "PREDISPOSIZIONE_PRESA");
		
		prop.setProperty("TIPO", "ALLACCIO");
		prop.setProperty("STATO_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "INVIATO");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "PREVENTIVO RICHIESTO");
		// ORDINE
		prop.setProperty("STATO_ELEMENTO_ORDINE", "PREVENTIVO RICHIESTO");
		prop.setProperty("TIPO_ORDINE", "COMMODITY");
		prop.setProperty("STATO_POD_ORDINE", "N.D.");
		prop.setProperty("STATO_R2D_ORDINE", "PRESO IN CARICO");
		prop.setProperty("COMMODITY_ORDINE", "N.D.");
		prop.setProperty("STATO_SAP_SD_ORDINE", "BOZZA");
		prop.setProperty("STATO_SAP_ISU_ORDINE", "N.D.");
		prop.setProperty("STATO_SEMPRE_ORDINE", "N.D.");
		prop.setProperty("PRATICA_ORDINE", "LAVORI");
		prop.setProperty("STATO_PRODOTTO", "N.D.");

		//nuovi prop
		prop.setProperty("POTENZIALITA_1", "20000");
		prop.setProperty("NUMERO_PDR_TIPO_1", "1");
		prop.setProperty("NUMERO_PDR_TIPO_2", "");
		prop.setProperty("NUMERO_PDR_TIPO_3", "");

		prop.setProperty("FATTURAZIONE_ANTICIPATA", "NO");

		//Integrazione R2D GAS
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/login.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS","ITALGAS RETI SPA");
		                  
		prop.setProperty("PREVENTIVO_GAS","Y");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Allaccio");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");

		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "INVIATO");
//		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("OI_RICERCA", "");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\Doc.pdf");
		prop.setProperty("IMPORTO_PREVENTIVO", "12");
		prop.setProperty("CODICE_PREVENTIVO", "1234567");
		prop.setProperty("FLAG_136", "NO");
		prop.setProperty("SPLIT_PAYMENT", "NO");
		prop.setProperty("CIG", "1234567890");
		prop.setProperty("CUP", "123456789012345");
		prop.setProperty("TIPOLOGIA_PUBBL_AMMIN", "Locale");
		prop.setProperty("STEP_PP","0");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

	@After
	public void tearDown() throws Exception{
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	              }



	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		SceltaProcessoPredisposizionePresaEVOGas.main(args);
		ProcessoPredisposizionePresaEVOGas.main(args);
		//startare 3 minuti dopo l'emissione
		Thread.currentThread().sleep(180000); 

		VerificheRichiestaPredisposizionePresaEVO.main(args);

		// Recupera OI-Ordine
		Recupera_Oi_Order_senza_POD.main(args);
		SalvaOiRicerca.main(args);

		// Esitazione R2D GAS
		LoginR2D_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_GAS.main(args);  
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSPredisposizionePresa_1OK_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_CaricamentoEsiti_3OK_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_CaricamentoEsiti_5OK_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodFinali_GAS.main(args);
		
		//startare dopo 3 minuti per Stato SAP-SD
		Thread.currentThread().sleep(180000); 
		
		// Verifica Stato Richiesta (Preventivo Inviato - Fatto)
		SetPropertyPredPresaGasId5_01.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		
		VerificheRichiestaPPDopoPreventivoEVO.main(args);
	
		// Accedere SFDC cliccare su 'Altre Azioni' e successivamente cliccare sul "Gestione Preventivo"
		ValidazionePreventivoPP.main(args);

		SetPropertyPredPresaGasId5_02.main(args);

		VerificheRichiestaPPDopoPreventivoEVO.main(args);

		// Caricamento Documento e Validazione Documentale con Attività DMS
		CaricaEValidaDocumentiPPGas.main(args);

		// Lavorazione flusso N01 Esecuzione Lavori
		SetPropertyPredPresaGasId5_03.main(args);

//		// solo x test poi togliere - inizio
//		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
//		RicercaRichiesta.main(args);
//		// solo x test poi togliere - Fine

		// Entrare in Altre AZIONI ed eseguire INVIO e poi entrare in Gest. Prev. e dare l'OK
		ValidazionePreventivoPP.main(args);
		
		// Lavorazione flusso N01 Esecuzione Lavori
		SetPropertyPredPresaGasId5_04.main(args);
		
		VerificheRichiestaPPDopoPreventivoEVO.main(args);

		// Recupera OI-Ordine
		Recupera_Oi_Order_senza_POD.main(args);
		SalvaOiRicerca.main(args);

		// Esitazione R2D GAS
		LoginR2D_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		
		R2D_VerifichePodIniziali_GAS.main(args);  
		TimeUnit.SECONDS.sleep(5);
		
//		//R2D_InvioPSPortale_1OK_GAS.main(args);
//		//TimeUnit.SECONDS.sleep(5);

		// Viene effettuata la Forzatura per errore sul Template di R2D
		R2D_Forzatura_Esiti_2OK_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_3OK_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);
		
		R2D_CaricamentoEsiti_5OK_GAS.main(args); //OK Cla
		TimeUnit.SECONDS.sleep(5);

		R2D_VerifichePodFinali_GAS.main(args);

		//startare dopo 2 minuti per Stato SAP-SD
		Thread.currentThread().sleep(120000); 

		// Verifica Stato Richiesta (Preventivo Inviato - Fatto)
		SetPropertyPredPresaGasId5_05.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		
		VerificheRichiestaPPDopoPreventivoEVO.main(args);
		
	}

}



