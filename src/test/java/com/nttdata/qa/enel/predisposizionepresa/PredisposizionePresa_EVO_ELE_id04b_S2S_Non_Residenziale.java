package com.nttdata.qa.enel.predisposizionepresa;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazionePPEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoPredisposizionePresaEVOEle;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SceltaProcessoPredisposizionePresaEVOEle;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaAnnullaPredisposizionePresa;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaPredisposizionePresaEVO;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;


public class PredisposizionePresa_EVO_ELE_id04b_S2S_Non_Residenziale {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
	// Valorizzare anche TIPO_UTENZA con PE oppure S2S
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);
		prop.setProperty("TIPO_UTENZA","S2S");
		prop.setProperty("ANNULLA_OFFERTA","N");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
	// ***
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "02356590204");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "GLDGWJ85C04F839A");
		prop.setProperty("TIPOCLIENTE", "BUSINESS");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE","NO"); 
		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "PREDISPOSIZIONE_PRESA");
		// RICHIESTA
		prop.setProperty("TIPO", "ALLACCIO");
		prop.setProperty("STATO_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTO_STATO_RICHIESTA", "INVIATO");
		prop.setProperty("STATO_ELEMENTO_RICHIESTA", "PREVENTIVO RICHIESTO");
		prop.setProperty("STATO_POD", "N.D.");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		// ORDINE
		prop.setProperty("STATO_ELEMENTO_ORDINE", "PREVENTIVO RICHIESTO");
		prop.setProperty("TIPO_ORDINE", "COMMODITY");
		prop.setProperty("STATO_POD_ORDINE", "N.D.");
		prop.setProperty("STATO_R2D_ORDINE", "PRESO IN CARICO");
		prop.setProperty("COMMODITY_ORDINE", "N.D.");
		prop.setProperty("STATO_SAP_SD_ORDINE", "BOZZA");
		prop.setProperty("STATO_SAP_ISU_ORDINE", "N.D.");
		prop.setProperty("STATO_SEMPRE_ORDINE", "N.D.");
		prop.setProperty("PRATICA_ORDINE", "LAVORI");
		prop.setProperty("STATO_PRODOTTO", "N.D.");

		//nuovi prop
		prop.setProperty("USO_FORNITURA", "Uso domestico Residente");
		prop.setProperty("TENSIONE_RICHIESTA", "220");
		prop.setProperty("POTENZA_RICHIESTA", "4.5");
		prop.setProperty("FATTURAZIONE_ANTICIPATA", "NO");
		prop.setProperty("FLAG_136", "NO");
		prop.setProperty("SPLIT_PAYMENT", "NO");
		prop.setProperty("CIG", "1234567890");
		prop.setProperty("CUP", "123456789012345");
		prop.setProperty("TIPOLOGIA_PUBBL_AMMIN", "Locale");

		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazionePPEVO.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		SceltaProcessoPredisposizionePresaEVOEle.main(args);
		ProcessoPredisposizionePresaEVOEle.main(args);
		//startare 2 minuti dopo l'emissione
		Thread.currentThread().sleep(120000); 
		VerificheRichiestaPredisposizionePresaEVO.main(args);
		
		
	}
	
	@After
	public void tearDown() throws Exception{
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
            }


}



