package com.nttdata.qa.enel;


import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class CopiaDocumentiEVO_Id_4 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void inizioTest() throws Exception {
        this.prop = new Properties();
        prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
        prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
        prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
        prop.setProperty("NOME_UTENTE", "723535469");
        prop.setProperty("ID_DOCUMENTO", "0332118324");
        prop.setProperty("ID_DOCUMENTO_2", "0332118327");
        prop.setProperty("CANALE_INVIO", "POSTA");
        prop.setProperty("TIPOLOGIA_DOC", "KIT_CONTRUATTUALE");
        prop.setProperty("CAP", "00198");
        prop.setProperty("RUN_LOCALLY", "Y");
    }

    @Test
    public void eseguiTest() throws Exception {

//      prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load( new FileInputStream(nomeScenario));
/*
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID21.main(args);
		VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
		VerificaSelezioneFornitureCopiaDocumenti.main(args);
		SelezionaSezioneTipologiaDocumenti.main(args);
		VerificaKitContrattualePadreCopiaDocumenti.main(args);
		VerificaKitContrattualeCopiaDocumenti.main(args);		
		AzioniDettagliSpedizioneCopiaDocumenti.main(args);
		SalvaNumeroDocumento.main(args);	

		//verifica case: Stato - In Lavorazione, Sottostato - Inviato  
		VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);
		VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench.main(args);		
		//verifica presenza reconds document associati al Case
		RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
		VerificaCasePlicoCopiaDocumenti.main(args);
		//verifica presenza item con campo Link valorizzato
		RecuperaLinkItemCaseCopiaDocumenti.main(args);

        //Da eseguire dopo batch
        //verifica avanzamento document con Modello = Plico in Stato=Inviato
		RecuperaStatoDocumentModelloPlicoCopiaDocumentiPostBatch.main(args);
		VerificaStatoDocumentInviatoModelloPlicoCopiaDocumenti.main(args);
		//verifica case: Stato - Chiuso, Sottostato - Ricevuto  
		VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);
		VerificaStatoSottostatoDocumentoWorkbench.main(args);*/
    }

    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
