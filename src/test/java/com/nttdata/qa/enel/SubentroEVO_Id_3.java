package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class SubentroEVO_Id_3 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("CODICE_FISCALE", "CLZGRL86R14D883O");
		prop.setProperty("CAP", "00178");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("PROCESSO", "Avvio Subentro EVO");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "4");
		prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("LOCALITA_ISTAT", "ROMA");

		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("SEZIONE_ISTAT", "Y");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TIPO_FORNITURA", "ELETTRICO");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TESTO_POPUP",
				"Il Codice del Consumo prevede 14 giorni di tempo per consentire al cliente di esercitare il diritto ripensamento dopo la conclusione di un contratto. Scegliendo la “Richiesta di esecuzione anticipata del contratto”, il Cliente richiede espressamente che le procedure per dar corso all’attivazione vengano avviate subito, ossia prima che sia decorso il termine per il ripensamento. Anche nel caso di “Richiesta di esecuzione anticipata del contratto”, per il Cliente sarà sempre possibile esercitare il diritto di ripensamento.Qualora sia possibile annullare la richiesta di attivazione, il Cliente continuerà ad essere fornito dal precedente Fornitore oppure saranno attivati i Servizi di ultima istanza gas o il Servizio di maggior tutela per il settore elettrico. Qualora, invece, non sia possibile interrompere la richiesta di attivazione verso il distributore, il Cliente continuerà ad essere fornito da Enel Energia e potrà individuare successivamente un altro Fornitore o procedere alla richiesta di chiusura del Punto di fornitura, facendone espressa richiesta.In questo caso il Cliente sarà tenuto al pagamento dei corrispettivi previsti dal contratto fino all’avvenuta cessazione della fornitura.");
        prop.setProperty("TITOLO_POPUP", "Diritto di ripensamento: cosa avviene in caso di esecuzione anticipata.");
        prop.setProperty("CHECK_RIPENSAMENTO", "Y");
        
		return prop;
	}

	@Test
	public void eseguiTest() throws Exception {

		String args[] = { nomeScenario };
		prop.store(new FileOutputStream(nomeScenario), null);
		prop.load(new FileInputStream(nomeScenario));
 
		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID17.main(args);
        SezioneMercatoSubentro.main(args);
		InserimentoFornitureSubentroSingolaEleGas.main(args);
		SelezioneUsoFornitura.main(args);
		ConsensiEContattiSubentro.main(args);
		
		System.out.println("Fine test");

	}

	@After
	public void fineTest() throws Exception {
		 /*
		 prop.load(new FileInputStream(nomeScenario));
		 this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 ReportUtility.reportToServer(this.prop);
		 */
		 
	}
}
