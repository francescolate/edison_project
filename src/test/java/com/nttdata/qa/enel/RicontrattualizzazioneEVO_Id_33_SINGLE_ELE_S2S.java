package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import com.nttdata.qa.enel.util.ReportUtility;

public class RicontrattualizzazioneEVO_Id_33_SINGLE_ELE_S2S {
	
    Properties prop;
    String nomeScenario = this.getClass().getSimpleName() + ".properties";
        
    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");
        prop.setProperty("ATTIVABILITA", "Attivabile");
        prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("ASSICURAZIONE_SERVIZI", "Y");
        prop.setProperty("CAP", "00184");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO", "EMAIL");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("TIPO_OI_ORDER", "Commodity");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("RIGA_DA_ESTRARRE", "4");
        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
        prop.setProperty("FLAG_136", "NO");
        //prop.setProperty("ELIMINA_VAS", "Y");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

//      prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
      /*
        SetQueryRecuperaDatiRicontrattualizzazione119_2_bus.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyNAMECFPOD.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID22.main(args);
        ConfermaCheckList.main(args);
        CheckSezioneSelezioneFornituraRicontrattualizzazione.main(args);
        ConfermaRiepilogoOffertaRicontrattualizzazione.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);
        ConfiguraProdottoElettricoNonResidenziale.main(args);
        AssociazioneProdottiServizi.main(args);
        ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);
        SplitPaymentSwaEVO.main(args);//nuovo
        CigCugpRicontrattualizzazione.main(args);//nuovo
        IndirizziFatturazione.main(args);
        ConsensiEContatti.main(args);
        ModalitaFirmaECanaleDInvioVoltura.main(args);
        SalvaOffertaInBozzaRicontrattualizzazione.main(args);
        ModificaOffertaInBozzaAllaccioAttivazione.main(args);
        ConfermaSezioniMancantiRicontrattualizzazione.main(args);*/
        
    }

    @After
    public void fineTest() throws Exception {
		prop.load( new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

    }
}