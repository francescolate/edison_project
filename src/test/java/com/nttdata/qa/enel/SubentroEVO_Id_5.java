package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class SubentroEVO_Id_5 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("TIPO_DELEGA", "Nessuna delega");
        prop.setProperty("RIGA_DA_ESTRARRE", "2");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("CODICE_FISCALE", "BRBFDN64T17A794Y");
        prop.setProperty("CAP", "00178");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("TENSIONE_CONSEGNA", "380");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("COMMODITY", "ELE");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

       // prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        /*
        SetSubentroquery_subentro_id1.main(args);
        RecuperaDatiWorkbench.main(args);
        
        RecuperaPodNonEsistente.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);
        AnnullamentoSubentroQuote.main(args);
*/
    }

    @After
    public void fineTest() throws Exception {
    	
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
    }


}
