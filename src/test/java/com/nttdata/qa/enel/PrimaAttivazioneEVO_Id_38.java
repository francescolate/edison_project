package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.components.colla.FibraDiMelitaComponent;
import com.nttdata.qa.enel.components.lightning.ScontiEBonusComponent;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrelloAddFibra;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiDelibera40;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CommodityGasResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraGasPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaIndirizzoFibra;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneAppuntamentoPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ModificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOffertaDaTab;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Dual;
import com.nttdata.qa.enel.testqantt.VerificaOIOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.colla.FibraNoCover_Res;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Accertamento_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Istruttoria_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIntermedie_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrimaAttivazioneEVO_Id_38 {
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		
		//Property utenza Salesforce
		
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		
		//Property creazione cliente
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "F");
		prop.setProperty("DATA_NASCITA", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");	
		prop.setProperty("CAP", "27036");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");
		

		// Configurazione carrello
		prop.setProperty("PRODOTTO","NEW_E-Light Gas_WEB");
		prop.setProperty("OPZIONE_FIBRA","SI");
		
		//Configurazione mail nel VAS bolletta WEB 
		prop.setProperty("MOD_MAIL_CERTIFICATA","SI");
		prop.setProperty("MAIL_CERTIFICATA","testing.crm.automation@gmail.com");
		
		//Configurazione Fibra
		prop.setProperty("INDIRIZZO_FIBRA_OK","SI");
		prop.setProperty("CODICE_MIGRAZIONE", "MGG121234");
		prop.setProperty("TELEFONO_FIBRA", "06221234567");
		prop.setProperty("CONSENSO_DATI_FIBRA_OK", "SI");
		prop.setProperty("REGIONE_FIBRA", "LAZIO");
		prop.setProperty("PROVINCIA_FIBRA", "ROMA");
		prop.setProperty("CITTA_FIBRA", "ROMA");
		prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
		prop.setProperty("CIVICO_FIBRA", "196");
		prop.setProperty("EMAIL_FIBRA", "cli.res111@gmail.com");

		//Property prima  attivazione
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");	
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("SKIP_CONTATTI_CONSENSI", "Y");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO", "N");
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - NULL / DRAFT");
		prop.setProperty("STATO_RICHIESTA", "BOZZA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("RECUPERA_OI_RICHIESTA", "Y");
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY", "Y");	
	
		
		// Dati R2d GAS
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/217.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d);
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Attivazione in delibera 40");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		prop.setProperty("CP_GESTORE", "12345");
		prop.setProperty("ESITO_DELIBERA40_ISTRUTTORIA", "Istruttoria Positiva");
		prop.setProperty("ESITO_DELIBERA40_ACCERTAMENTO", "Accertamento Positivo");
		prop.setProperty("LETTURA_CONTATORE", "112233");
		prop.setProperty("ANNO_COSTRUZIONE_CONTATORE", "2019");
		prop.setProperty("ACCESSIBILITA_229", "1 (Accessibile)");
		prop.setProperty("COEFFICIENTE_C", "1");
		prop.setProperty("CLASSE_CONTATORE", "G0004");
		


	};

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		
//		//Creazione cliente
//		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
//		CreazioneNuovoCliente.main(args);
//		
//		//Scenario PA
//		
//		RecuperaPodNonEsistentePrimaAttivazioneEVO.main(args);
//		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
//		CercaClientePerNuovaInterazione.main(args);
//		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
//		Identificazione_Interlocutore_ID15.main(args);
//		SelezioneMercatoPrimaAttivazioneEVO.main(args);
//		InserimentoFornitureEVO.main(args);
//		CompilaDatiFornituraGasPrimaAttivazione.main(args);
//		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
//		CommodityGasResidenzialePrimaAttivazione.main(args);
//		ConfermaIndirizziPrimaAttivazione.main(args);
//		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
//		PagamentoBollettinoPostaleEVO.main(args);
//		ConfermaScontiBonusSWAEVO.main(args);
//		ConfiguraProdottoSWAEVOGasResidenziale.main(args);
//		GestioneCVPPrimaAttivazione.main(args);
//		ConsensiEContattiPrimaAttivazione.main(args);
//		CompilaIndirizzoFibra.main(args);
//		ModalitaFirmaPrimaAttivazione.main(args);
//		ConfermaPrimaAttivazione.main(args);  
//	
		//Recupera Offerta
		RecuperaStatusOffer.main(args);
		VerificaOffertaInBozza.main(args);

		RecuperaStatusCase_and_Offer.main(args);
		VerificaOIOffertaInBozza.main(args);
		SalvaIdOfferta.main(args);	

		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);

		//set chiave ricerca per R2D
		SetOIRicercaDaOIOrdine.main(args);

		//Esiti R2D delibera 40
		LoginR2D_GAS.main(args);
		R2D_VerifichePodIniziali_GAS.main(args);
		R2D_InvioPSPortale_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		R2D_CaricamentoEsitiDelibera40Istruttoria_GAS.main(args);
		R2D_CaricamentoEsitiDelibera40Accertamento_GAS.main(args);
		R2D_VerifichePodIntermedie_GAS.main(args);


		//Accesso a SFDC per caricare file
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOffertaDaTab.main(args);
		CaricaEValidaDocumentiDelibera40.main(args);
		GestioneAppuntamentoPrimaAttivazione.main(args);
	
		//Verifica offerta in stato chiusa
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args);
		
		//SECONDA INTEGRAZIONE CON R2D - ESITO 5OK
		LoginR2D_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);

		//Verifica ricezione esiti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);

		//Verifica chiusura richiesta
		VerificheAttivazioneRichiesta.main(args);
		

	};

//	@After
//	public void fineTest() throws Exception {
//		InputStream in = new FileInputStream(nomeScenario);
//		prop.load(in);
//		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//		ReportUtility.reportToServer(this.prop);
//
//	}
}