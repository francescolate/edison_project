package com.nttdata.qa.enel.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;



public class QANTTLogger {

	
	private File logFile;
	
	public QANTTLogger(Properties prop) throws Exception {		
		String logFileName = "TestObjectLog.txt";
		logFile = new File(prop.getProperty("OUTPUT_DIR") + File.separator + logFileName);
		File parent = logFile.getParentFile();
		if (!parent.exists() && !parent.mkdirs()) {
		    throw new IllegalStateException("Couldn't create dir: " + parent);
		}
		else {
			prop.setProperty("LOG_FILE", logFileName);
			this.write("Start");
		}
	
	
	}
	
	
	
	public void write(String message) throws Exception  {
	
		BufferedWriter writer = null;
		PrintWriter out = null;
		try {
			 writer = new BufferedWriter(new FileWriter(this.logFile.getAbsolutePath(),true));
			 out = new PrintWriter(writer);
			 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 //get current date time with Date()
			 Date date = new Date();
			 ////System.out.println(dateFormat.format(date));
			 out.println(dateFormat.format(date) + " INFO ---  "+message);
		}catch(Exception e) {
			throw new Exception("Couldn't write into log file");
		}finally {
			if(out !=null) out.close();
			if(writer !=null) writer.close();
		}
	}
	

}
