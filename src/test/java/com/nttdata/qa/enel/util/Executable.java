package com.nttdata.qa.enel.util;

public interface Executable {
	public void Esegui() throws Exception;
}
