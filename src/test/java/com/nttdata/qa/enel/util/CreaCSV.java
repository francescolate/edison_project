package com.nttdata.qa.enel.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JFrame;


public class CreaCSV {

	public static void main(String[] args) throws Exception {
		String[] data = null;
		//mi creo il properties
		StringBuilder sb = null;
		Properties prop = conf();
		//leggo l'header
		final JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(true);
		fc.setCurrentDirectory(new File("C:\\Temp"));
		final JFrame frame = new JFrame("JFileChooser Demo");
		int retVal = fc.showOpenDialog(frame);
		if (retVal == JFileChooser.APPROVE_OPTION) {
			File[] selectedfiles = fc.getSelectedFiles();
			sb = new StringBuilder();
			for (int i = 0; i < selectedfiles.length; i++) {
				sb.append(selectedfiles[i].getName() + "\n");
			}
			//  JOptionPane.showMessageDialog(frame, sb.toString());
		}
		BufferedReader csvReader = new BufferedReader(new FileReader(fc.getSelectedFiles()[0]));
		String row ="";
		while ((row  = csvReader.readLine()) != null) {
			data = row.split(";");



		}
		csvReader.close();





		// TODO Auto-generated method stub
		ArrayList<String> temp = new ArrayList<String>();



		for (String string : data) {
			temp.add(prop.getProperty(string, "\"\""));
		}

		FileWriter csvWriter = new FileWriter(fc.getSelectedFiles()[0].getAbsoluteFile());
		for (String string : data) {
			csvWriter.append(string);
			csvWriter.append(";");
		}
		csvWriter.append("\n");


		for (String rowData : temp) {
			csvWriter.append( rowData);
			csvWriter.append(";");
		}

		csvWriter.flush();
		csvWriter.close();
		System.exit(0);
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "00100");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "KO");
		
		prop.setProperty("WP_USERNAME", "r.32019.p109statocontiweb@gmail.com");
		prop.setProperty("WP_PASSWORD", "Password01");
		prop.setProperty("AREA_CLIENTI", "");
		prop.setProperty("ACCOUNT_TYPE", "BSN");
		prop.setProperty("QUERY_PROPERTIES", "QUERY");
		prop.setProperty("QUERY_REFERENCES", "query1_ACB_32_to_37");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");

		return prop;
	}

}
