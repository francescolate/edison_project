package com.nttdata.qa.enel.util;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Properties;

import javax.imageio.ImageIO;



public class CreateScreenshot {
	
	public static String screenShot(Properties prop) throws Exception {
		
		String fileName="";
		try {
			// Recover Path
			String servicePath = prop.getProperty("OUTPUT_DIR");
			fileName = servicePath+"/ERROR_FILE.png";
			
//			String browser = prop.getProperty("BROWSER", "");

			// Draw screen
			Toolkit tool = Toolkit.getDefaultToolkit();
			Dimension d = tool.getScreenSize();
			Rectangle rect = new Rectangle(d);
			Robot robot = new Robot();
			File f = new File(fileName);
			BufferedImage img = robot.createScreenCapture(rect);
			ImageIO.write(img, "png", f);
			img.flush();
			
		} 
		catch (Exception e) 
		{
			throw new Exception("Error! ScreenShot Image not created.");
		}
		return fileName;
	}
}
