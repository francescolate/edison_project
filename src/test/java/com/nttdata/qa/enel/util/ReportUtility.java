package com.nttdata.qa.enel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ReportUtility {
	
	public static final String columnNames[] = {"CdT","DATA EXECUTION","STATUS","DATI","NOTE","TIME EXECUTION","START DATE","END DATE"};
	public static final String columnNamesToServer[] = {"CdT","DATA EXECUTION","STATUS","DATI","NOTE","MACHINE","USER"};

	public static final String logColumnNames[] = {"Step","Dati eventuali","Status","Note"};
	public static String folder = "\\\\10.154.32.119\\Report\\ENEL_EXECUTION";
	public static String name = folder+"\\Prop.properties";
	public static Workbook createReportTemplate() throws Exception{
		Workbook workbook = ExcelUtility.createWorkbook();
		Sheet sheet = ExcelUtility.createSheet(workbook, "Report");
		CellStyle headerCellStyle=ExcelUtility.setCellStyleFont(workbook, 14, IndexedColors.RED.getIndex());
	
		ExcelUtility.addHeaderRow(sheet, columnNames, headerCellStyle);
		return workbook;
		
	}
	
	public static Workbook createReportTemplateToServer() throws Exception{
		Workbook workbook = ExcelUtility.createWorkbook();
		Sheet sheet = ExcelUtility.createSheet(workbook, "Report");
		CellStyle headerCellStyle=ExcelUtility.setCellStyleFont(workbook, 14, IndexedColors.RED.getIndex());
	
		ExcelUtility.addHeaderRow(sheet, columnNamesToServer, headerCellStyle);
		return workbook;
		
	}
	
	public static Workbook createLogTemplate() throws Exception{
		Workbook workbook = ExcelUtility.createWorkbook();
		Sheet sheet = ExcelUtility.createSheet(workbook, "Report");
		CellStyle headerCellStyle=ExcelUtility.setCellStyleFont(workbook, 14, IndexedColors.RED.getIndex());
	
		ExcelUtility.addHeaderRow(sheet, logColumnNames, headerCellStyle);
		return workbook;
		
	}
	
	
	public static void log(String filePath,String step, String dati, String status, String note) throws Exception{
		Workbook w = ExcelUtility.openWorkbook(new FileInputStream(filePath));
		writeLog(w, step, dati, status, note);
		ReportUtility.finalizeReport(w, filePath);

	}
	
	public static void report(Properties prop) throws Exception {
		String filePath = ReportUtility.createOrGetTodayReport("ENEL_EXECUTION");
		Workbook w = ExcelUtility.openWorkbook(new FileInputStream(filePath));
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		
		String startDate = prop.getProperty("START_TIME", "");
		if (startDate.isEmpty())
		{//2019-08-29 09:46:31
			simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ssy");
			startDate = simpleDateFormat.format(calendar.getTime()).toString();
			prop.setProperty("START_TIME", startDate);
		}
		
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date d1 = fmt.parse(prop.getProperty("START_TIME", startDate));
	    Date d2 = new Date();
        
		long millisDiff = d2.getTime() - d1.getTime();
		int seconds = (int) (millisDiff / 1000 % 60);
		int minutes = (int) (millisDiff / 60000 % 60);
		//int hours = (int) (millisDiff / 3600000 % 24);
        //int days = (int) (millisDiff / 86400000);
		 String timeDiff = minutes + " minuti e "+seconds+" secondi";
		ReportUtility.writeReport(w, prop.getProperty("TEST_NAME"), sDate, prop.getProperty("RETURN_VALUE"), getMeaningfulData(prop), prop.getProperty("ERROR_DESCRIPTION"),timeDiff,d1.toString(),d2.toString());
		ReportUtility.finalizeReport(w, filePath);
	}
	
	public static void reportForTSE(String testName, String result, String failureMessage) throws Exception {
		String filePath = ReportUtility.createOrGetTodayReport("ENEL_EXECUTION");
		Workbook w = ExcelUtility.openWorkbook(new FileInputStream(filePath));

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		ReportUtility.writeReportForTSE(w, testName, now.toString(), result, failureMessage);
		ReportUtility.finalizeReport(w, filePath);
	}
	
	public static void reportToServer(Properties prop) throws Exception {
		
		//Update Jira
		if (Costanti.setJiraStatus)
			SetJiraExecutionStatus.updateJira(prop);
		
		
		String user = System.getProperty("user.name");
		String machine = InetAddress.getLocalHost().getHostName();
		String testName = prop.getProperty("TEST_NAME","");
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		
		if(Costanti.reportToDB) {
			
			if(!testName.equals("")) {
				ReportUtility.ReportToServerDB( testName, sDate, prop.getProperty("RETURN_VALUE", "KO"), getMeaningfulData(prop), prop.getProperty("ERROR_DESCRIPTION"),machine, user);
			}
			
		}else {
			String filePath = ReportUtility.createOrGetTodayReportToServer();
			Workbook w = ExcelUtility.openWorkbook(new FileInputStream(filePath));
			
			//per la concorrenzialità controllo un property che fa da semaforo.
			
			waitYourTurn();
			myTurn();
			if(!testName.equals("")) {
				ReportUtility.writeReportToServer(w, testName, sDate, prop.getProperty("RETURN_VALUE", "KO"), getMeaningfulData(prop), prop.getProperty("ERROR_DESCRIPTION"),machine, user);
			}
			ReportUtility.finalizeReport(w, filePath);
			leave();
		}
	}
	
	public static void reportToServerbackup(Properties prop) throws Exception {
		String filePath = ReportUtility.createOrGetTodayReportToServerBackup();
		Workbook w = ExcelUtility.openWorkbook(new FileInputStream(filePath));
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		
		String user = System.getProperty("user.name");
		String machine = InetAddress.getLocalHost().getHostName();
		String testName = prop.getProperty("TEST_NAME","");
		
		
		ReportUtility.writeReportToServer(w, testName, sDate, prop.getProperty("RETURN_VALUE", "KO"), getMeaningfulData(prop), prop.getProperty("ERROR_DESCRIPTION"),machine, user);
		
		ReportUtility.finalizeReport(w, filePath);
		
	
	}
	
	
	public static void myTurn() throws Exception {
		// TODO Auto-generated method stub
		String stato = "";
		InputStream in = null;
		try {
			 in = new FileInputStream(name);
		}catch(Exception e) {
			File file = new File(name);
			file.createNewFile();
			in = new FileInputStream(name);
		}
        Properties prop = new Properties();
		prop.load(in);
		 prop.setProperty("SEMAFORO", "ROSSO");
		 prop.store(new FileOutputStream(name), null);
	}
	
	public static void leave() throws Exception {
		// TODO Auto-generated method stub
		
		String stato = "";
		InputStream in = null;
		try {
			 in = new FileInputStream(name);
		}catch(Exception e) {
			File file = new File(name);
			file.createNewFile();
			in = new FileInputStream(name);
		}
        Properties prop = new Properties();
		prop.load(in);
		 prop.setProperty("SEMAFORO", "VERDE");
		 prop.store(new FileOutputStream(name), null);
	}

	public static void waitYourTurn() throws Exception {
		int i=0;
		while((!semaforo())&&(i<10)) {
			Thread.currentThread().sleep(3000);
			System.out.println("Attesa semaforo scrittura file");
			i+=1;
		}
	
		
	}

	public static boolean semaforo() throws Exception {
		String stato = "";
		InputStream in = null;
		try {
			 in = new FileInputStream(name);
		}catch(Exception e) {
			File file = new File(name);
			file.createNewFile();
			in = new FileInputStream(name);
		}
        Properties prop = new Properties();
		prop.load(in);
		 stato = prop.getProperty("SEMAFORO", "VERDE");
		 if(stato.equals("VERDE")) {
			 return true;
		 }else {
			 return false;
		 }
	}
	
	public static String getMeaningfulData(Properties prop) throws Exception{
		String meaningfulData[] = {"CODICE_FISCALE","REFERENTE","PARTITA_IVA","CODICEFISCALE","CODICE_RICHIESTA","POD","POD_1","POD_2","POD_ELE","POD_GAS","NOFFERTA","NUMERO_RICHIESTA","NRICHIESTA","N.RICHIESTA","N. RICHIESTA","N RICHIESTA","ID_RICHIESTA_CRM_ELE","ID_RICHIESTA_CRM_GAS",
				"JSON_INPUT", "Authorization", "CHANNELKEY", "TID", "SID", "SOURCECHANNEL", "JsonObjectStatusResponse"};
		String dati = "";
		String separatore=" - ";
		for(String x : meaningfulData) {
			if(prop.containsKey(x)) {
				dati = dati + x + " : "+ prop.getProperty(x)+ separatore;
			}
		}
		if (dati.length() > 0)
		{
			dati=dati.substring(0, dati.lastIndexOf(separatore));
		}
		return dati;
	}
	
	
	public static String createOrGetTodayReport(String folderName) throws Exception{
		Workbook w = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		folder = System.getProperty("user.home")+"\\Desktop\\"+folderName + "_"+sDate;
		File dir = new File(folder);
        if(!dir.exists()) {
        	Files.createDirectory(Paths.get(folder));
        }
        String filePath = folder+"\\Report.xls";
        File file = new File(filePath);
        if(!file.exists()) {
        		w = createReportTemplate();
        		ReportUtility.finalizeReport(w, filePath);
        	}
  
        return filePath;
      
	}
	
	public static String createOrGetTodayReportToServer() throws Exception{
		Workbook w = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		
		File dir = new File(folder);
        if(!dir.exists()) {
        	new File(folder).mkdir();
        }
        String filePath = folder+"\\Automation_Suite_Report_Execution_R4_"+sDate+".xls";
        File file = new File(filePath);
        if(!file.exists()) {
        		w = createReportTemplateToServer();
        		ReportUtility.finalizeReport(w, filePath);
        	}
  
        return filePath;
      
	}
	
	public static String createOrGetTodayReportToServerBackup() throws Exception{
		Workbook w = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		
		File dir = new File(folder);
        if(!dir.exists()) {
        	new File(folder).mkdir();
        }
        String filePath = folder+"\\Automation_Suite_Report_Execution_R3_2020_backup.xls";
        File file = new File(filePath);
        if(!file.exists()) {
        		w = createReportTemplateToServer();
        		ReportUtility.finalizeReport(w, filePath);
        	}
  
        return filePath;
      
	}
	
	public static String createOrGetLog(String folderName, String filename) throws Exception{
		Workbook w = null;
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		String folder = "C:\\"+folderName + "_"+sDate;
		File dir = new File(folder);
        if(!dir.exists()) {
        	new File(folder).mkdir();
        }
        String filePath = folder+"\\"+filename+".xls";
        File file = new File(filePath);
        if(!file.exists()) {
        		w = createLogTemplate();
        		ReportUtility.finalizeReport(w, filePath);
        	}
  
        return filePath;
      
	}
	
	
	public static void writeReport(Workbook workbook, String cdt,String dataExec,String status,String dati,String note,int rowNum) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(rowNum);
		row.createCell(0).setCellValue(cdt);
	    row.createCell(1).setCellValue(dataExec);
	    row.createCell(2).setCellValue(status);
	    row.createCell(3).setCellValue(dati);
	    row.createCell(4).setCellValue(note);
	    ExcelUtility.autoSizeColumn(sheet, columnNames);
		
	}
	
	public static void writeReportForTSE(Workbook workbook, String cdt,String dataExec,String status, String failureMessage) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(sheet.getLastRowNum()+1);
		row.createCell(0).setCellValue(cdt);
	    row.createCell(1).setCellValue(dataExec);
	    row.createCell(2).setCellValue(status);
	    row.createCell(4).setCellValue(failureMessage);
	    ExcelUtility.autoSizeColumn(sheet, columnNames);
		
	}
	
	public static void writeReport(Workbook workbook, String cdt,String dataExec,String status,String dati,String note, String time, String start, String end) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(sheet.getLastRowNum()+1);
		row.createCell(0).setCellValue(cdt);
	    row.createCell(1).setCellValue(dataExec);
	    row.createCell(2).setCellValue(status);
	    row.createCell(3).setCellValue(dati);
	    row.createCell(4).setCellValue(note);
	    row.createCell(5).setCellValue(time);
	    row.createCell(6).setCellValue(start);
	    row.createCell(7).setCellValue(end);
	    ExcelUtility.autoSizeColumn(sheet, columnNames);
	}
	
	public static void writeReportToServer(Workbook workbook, String cdt,String dataExec,String status,String dati,String note, String machine, String user) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(sheet.getLastRowNum()+1);
		row.createCell(0).setCellValue(cdt);
	    row.createCell(1).setCellValue(dataExec);
	    row.createCell(2).setCellValue(status);
	    row.createCell(3).setCellValue(dati);
	    row.createCell(4).setCellValue(note);
	    row.createCell(5).setCellValue(machine);
	    row.createCell(6).setCellValue(user);
	    ExcelUtility.autoSizeColumn(sheet, columnNamesToServer);
	}
	
	public static void ReportToServerDB(String cdt,String dataExec,String status,String dati,String note, String machine, String user) throws Exception{

		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://10.151.201.147:5432/report?user=postgres&password=admin";
		Connection conn = DriverManager.getConnection(url);
		
		PreparedStatement st = conn.prepareStatement("INSERT INTO public.\""+Costanti.Postgresql_Table_Name+"\" (\"CDT\", \"DATI\", \"EXECUTION_DATE\", \"MACHINE\", \"NOTE\", \"STATUS\", \"USERS\") VALUES (?, ?, ?, ?, ?, ?, ?)");
		st.setString(1, cdt);
		st.setString(2, dati);
		st.setString(3, dataExec);
		st.setString(4, machine);
		st.setString(5, note);
		st.setString(6, status);
		st.setString(7, user);
		st.executeUpdate();
	
		conn.close(); 
		
	}
	
	public static void writeLog(Workbook workbook, String step,String dati,String status,String note) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(sheet.getLastRowNum()+1);
		row.createCell(0).setCellValue(step);
	    row.createCell(1).setCellValue(dati);
	    row.createCell(2).setCellValue(status);
	    row.createCell(3).setCellValue(note);
	    ExcelUtility.autoSizeColumn(sheet, columnNames);
	}
	
	public static void writeReportOk(Workbook workbook, String cdt,String dataExec,String dati,String note) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(sheet.getLastRowNum()+1);
		row.createCell(0).setCellValue(cdt);
	    row.createCell(1).setCellValue(dataExec);
	    row.createCell(2).setCellValue("Passed");
	    row.createCell(3).setCellValue(dati);
	    row.createCell(4).setCellValue(note);
	    ExcelUtility.autoSizeColumn(sheet, columnNames);
	}
	
	public static void writeReportKo(Workbook workbook, String cdt,String dataExec,String dati,String note) throws Exception{
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.createRow(sheet.getLastRowNum()+1);
		row.createCell(0).setCellValue(cdt);
	    row.createCell(1).setCellValue(dataExec);
	    row.createCell(2).setCellValue("Failed");
	    row.createCell(3).setCellValue(dati);
	    row.createCell(4).setCellValue(note);
	    ExcelUtility.autoSizeColumn(sheet, columnNames);
		
	}
	
	
	public static void finalizeReport(Workbook workbook, String filePath) throws Exception {
		ExcelUtility.writeWorkbook(workbook, filePath);
	}
	
	

	public static void main(String args[]) throws Exception{
	
	   
	    
	    
}

}
