package com.nttdata.qa.enel.util;
import java.math.BigInteger;
import java.util.Random;

/**
 * Provides methods for generating valid Italian IBANs (International Bank
 * Account Number).
 * <p>
 *
 * @author Vincenzo Gaeta
 * @version 1.0
 * @since 2021-03-04
 */
public class IBAN {

	/**
	 * Returns a properly generated Italian IBAN based on the input parameters.
	 * 
	 * @param abi           -> ABI code (bank code - 5 digits, numeric)
	 * @param cab           -> CAB code (branch code - 5 digits, numeric)
	 * @param accountNumber -> bank account number (12 digits, alphanumeric)
	 * @return The generated IBAN.
	 * @exception IllegalArgumentException On invalid input.
	 */
	private static String generate(String abi, String cab, String accountNumber) {
		// Input checks
		if (!abi.matches("[0-9]{5}"))
			throw new IllegalArgumentException("Invalid ABI code!");
		if (!cab.matches("[0-9]{5}"))
			throw new IllegalArgumentException("Invalid CAB code!");
		if (!accountNumber.matches("[0-9A-Za-z]{12}"))
			throw new IllegalArgumentException("Invalid account number!");
		// CIN calculation
		String rawBban = abi + cab + accountNumber.toUpperCase();
		int cinSum = 0;
		for (int i = 0; i < 22; i++) {
			int tmp = -1;
			if (rawBban.charAt(i) < 58) // Digits
				tmp = rawBban.charAt(i) - 48;
			else // Letters
				tmp = rawBban.charAt(i) - 65;
			if (i % 2 == 0) { // Even positions
				switch (tmp) {
				case 0:
					cinSum += 1;
					break;
				case 1:
					cinSum += 0;
					break;
				case 2:
					cinSum += 5;
					break;
				case 3:
					cinSum += 7;
					break;
				case 4:
					cinSum += 9;
					break;
				case 5:
					cinSum += 13;
					break;
				case 6:
					cinSum += 15;
					break;
				case 7:
					cinSum += 17;
					break;
				case 8:
					cinSum += 19;
					break;
				case 9:
					cinSum += 21;
					break;
				case 10:
					cinSum += 2;
					break;
				case 11:
					cinSum += 4;
					break;
				case 12:
					cinSum += 18;
					break;
				case 13:
					cinSum += 20;
					break;
				case 14:
					cinSum += 11;
					break;
				case 15:
					cinSum += 3;
					break;
				case 16:
					cinSum += 6;
					break;
				case 17:
					cinSum += 8;
					break;
				case 18:
					cinSum += 12;
					break;
				case 19:
					cinSum += 14;
					break;
				case 20:
					cinSum += 16;
					break;
				case 21:
					cinSum += 10;
					break;
				case 22:
					cinSum += 22;
					break;
				case 23:
					cinSum += 25;
					break;
				case 24:
					cinSum += 24;
					break;
				case 25:
					cinSum += 23;
					break;
				}
			} else // Odd positions
				cinSum += tmp;
		}
		char cin = (char) ((cinSum % 26) + 65);
		// BBAN
		String bban = cin + rawBban;
		// IBAN control code calculation
		String rawIban = bban + "IT00";
		String rawIbanConverted = "";
		for (int i = 0; i < 27; i++) {
			if (rawIban.charAt(i) >= 58)
				rawIbanConverted += (int) (rawIban.charAt(i) - 55);
			else
				rawIbanConverted += rawIban.charAt(i);
		}
		int ibanControlCode = 98 - (new BigInteger(rawIbanConverted).mod(new BigInteger("97"))).intValue();
		// IBAN
		String iban = "IT" + String.format("%02d", ibanControlCode) + bban;
		return iban;
	}

	/**
	 * Generates a random Italian IBAN.
	 * 
	 * @return The generated IBAN.
	 */
	public static String generate() {
		String abi = "03069"; // Intesa Sanpaolo
		String cab = "03491"; // Branch located in Naples (Centro Direzionale)
		String accountNumber = "1000"; // Account number (fixed prefix + random part)
		Random random = new Random();
		for (int i = 0; i < 8; i++)
			accountNumber += random.nextInt(10);
		String iban = generate(abi, cab, accountNumber);
		return iban;
	}

}