package com.nttdata.qa.enel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;


public class ExcelUtility {



	public static FileInputStream openFile(String path) throws Exception {
		FileInputStream file = new FileInputStream(new File(path));
		return file;
	}

	public static HSSFWorkbook openWorkbook(FileInputStream file) throws Exception {

		HSSFWorkbook workbook = null;
		workbook = new HSSFWorkbook(file);
		return workbook;

	}

	public static void closeFile(FileInputStream file) throws Exception {
		file.close();
	}

	public static HSSFSheet openSheet(HSSFWorkbook workbook, int sheetNum) {
		HSSFSheet sheet = workbook.getSheetAt(sheetNum);
		return sheet;

	}

	public static String getRowCellValueByColName(HSSFSheet sheet, int row, String colName) throws Exception {
		HSSFCell cell = sheet.getRow(row).getCell(getHeaderColNumByValue(sheet, colName));
		return castCellStringValue(cell);

	}

	public static int getHeaderColNumByValue(HSSFSheet sheet, String colName) throws Exception {
		Row row = sheet.getRow(0);
		// For each row, iterate through each columns
		Iterator<Cell> cellIterator = row.cellIterator();
		int colIndex = 0;
		boolean found = false;
		while (cellIterator.hasNext() && !found) {
			Cell cell = cellIterator.next();
			String cellValue = cell.getStringCellValue();
			if (cellValue.compareToIgnoreCase(colName) == 0) {
				found = true;
			}
			else colIndex++;
		}

		if (!found)
			throw new Exception("Column " + colName + " not found");
		return colIndex;
	}


	
	
	public static String castCellStringValue(Cell cell) throws Exception {
		String valore="";
		switch (cell.getCellType()) {
		//TODO NON ESISTE IL TIPO BOOLEAN --> Luca Capasso sei pregato di allinearci, Grazie
		// Se da errore Aggironare il POM e il progetto Maven con le utilme modifiche.
		case Cell.CELL_TYPE_BOOLEAN:
			if(cell.getBooleanCellValue()) valore = "true";
			else valore = "false";
			break;
		case Cell.CELL_TYPE_NUMERIC:
			valore = cell.getNumericCellValue()+"";
			break;
		case Cell.CELL_TYPE_STRING:
			valore=cell.getStringCellValue();
			break;
		}
		return valore;
		
	}
	

	public static void printExcel(String fileName) throws Exception{
		

			FileInputStream file = new FileInputStream(new File(fileName));

			// Get the workbook instance for XLS file
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			// Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheetAt(0);
			int rowCount = 0;

			System.out.println(sheet.getRow(0).getCell(0).toString());

			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Map testData = new HashMap<>();
				rowCount++;
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();
					if (rowCount == 1) {

					}

					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_BOOLEAN:
						System.out.print(cell.getBooleanCellValue() + "\t\t");
						break;
					case Cell.CELL_TYPE_NUMERIC:
						System.out.print(cell.getNumericCellValue() + "\t\t");
						break;
					case Cell.CELL_TYPE_STRING:
						System.out.print(cell.getStringCellValue() + "\t\t");
						break;
					}
				}
				System.out.println("");
			}
			file.close();

	}
	
    public static Sheet createSheet(Workbook workbook, String sheetName) throws Exception{
		Sheet sheet = workbook.createSheet(sheetName);
    	return sheet;
    }
   
   public static Workbook createWorkbook() throws Exception {
	   return new HSSFWorkbook(); 
   }
   
   public static CellStyle setCellStyleFont(Workbook workbook,int fontHeight,short indexedColors) throws Exception {
	    Font font = workbook.createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) fontHeight);
		font.setColor(indexedColors);
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(font);
		return headerCellStyle;
   }
   
   public static Row addHeaderRow(Sheet sheet,String [] columnNames,CellStyle headerCellStyle) throws Exception{
	    Row headerRow = sheet.createRow(0);
	    for(int i = 0; i < columnNames.length; i++) {
	        Cell cell = headerRow.createCell(i);
	        cell.setCellValue(columnNames[i]);
	        cell.setCellStyle(headerCellStyle);
	    }
	    return headerRow;
  
	   
   }
   
   public static void autoSizeColumn(Sheet sheet,String columns[]) throws Exception {
	    for(int i = 0; i < columns.length; i++) {
	        sheet.autoSizeColumn(i);
	    }
	   
   }
   
   public static void writeWorkbook(Workbook workbook,String filePath) throws Exception {
	   
	    FileOutputStream fileOut = new FileOutputStream(filePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();
	   
   }

	
	
}
