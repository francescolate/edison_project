/**
 * 
 */
package com.nttdata.qa.enel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Collections;
import java.util.Properties;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Command;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Response;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.remote.http.JsonHttpCommandCodec;
import org.openqa.selenium.remote.http.JsonHttpResponseCodec;

/**
 * @author Rosario.Chiantese
 *
 */
public class WebDriverManager {
	
	/**
	 * Kill old WebBriver Istance and create new WebDriver Istance 
	 * @param properties
	 * @return WebDriver
	 * @throws Exception
	 */
	public static RemoteWebDriver getNewWebDriver(Properties properties) throws Exception
	{
		RemoteWebDriver newDriverIstance = null;	
		
		String browserType = properties.getProperty("BROWSER", "CHROME");
		
		DesiredCapabilities capabilities = null;
		switch (browserType.toUpperCase()) {
		case "CHROME":
//			System.setProperty("webdriver.chrome.driver",
//		"C:/AgentJarLibs/chromedriver.exe");
//			System.setProperty("webdriver.chrome.driver", "classes/resources/chromedriver.exe");
			properties.setProperty("ClassPathWebDriver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/chromedriver.exe");
			
			//Kill dei processi Chrome e ChromeDriver
			WindowsProcessKiller.killChrome();
			
			System.setProperty("webdriver.chrome.driver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/chromedriver.exe");

			// driver setup
			ChromeOptions options = new ChromeOptions();
			
			options.addArguments("start-maximized");
			options.addArguments("test-type");
			if(properties.getProperty("USE_PERSONAL_BROWSER", "N").equals("Y"))
				options.addArguments("--user-data-dir=C:\\Users\\"+System.getProperty("user.name")+"\\AppData\\Local\\Google\\Chrome\\User Data");
			
//			System.out.println("C:\\Users\\"+System.getProperty("user.name")+"\\AppData\\Local\\Google\\Chrome\\User Data");
			options.addArguments("enable-automation"); // https://stackoverflow.com/a/43840128/1689770
			
			if(properties.getProperty("INCOGNITO_MODE", "").equals("Y"))
				options.addArguments("--incognito");
			
			options.addArguments("--disable-web-security");
            options.addArguments("--allow-running-insecure-content");
//			options.addArguments("--headless"); // only if you are ACTUALLY running headless
//			options.addArguments("--no-sandbox"); //https://stackoverflow.com/a/50725918/1689770
//			options.addArguments("--disable-infobars"); //https://stackoverflow.com/a/43840128/1689770
//			options.addArguments("--disable-dev-shm-usage"); //https://stackoverflow.com/a/50725918/1689770
//			options.addArguments("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
			options.addArguments("--disable-gpu"); //https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc
			newDriverIstance = new ChromeDriver(options);
			break;
		case "IE":
			properties.setProperty("ClassPathWebDriver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/IEDriverServer.exe");

			//Kill dei processi iexplore e IEDriverServer
			WindowsProcessKiller.killIE();
			System.setProperty("webdriver.ie.driver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/IEDriverServer.exe");
           // IEOptions options = new IEOptions();
			
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability("nativeEvents", false);
			capabilities.setCapability("unexpectedAlertBehaviour", "accept");
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability("disable-popup-blocking", true);
			capabilities.setCapability("enablePersistentHover", true);
			capabilities.setCapability("ignoreZoomSetting", true);
			
			newDriverIstance = new InternetExplorerDriver(capabilities);
			//newDriverIstance.navigate().to("www.google.it");
			break;
		case "FF":
			newDriverIstance = new FirefoxDriver();
			break;
		default:
			throw new Exception("Errore! browser Type Not Supported! ("+ browserType.toUpperCase() +")");	
		}
		
		//Recover Url and Session WebDriver 
		String WEBDRIVERURL = ((HttpCommandExecutor) newDriverIstance.getCommandExecutor()).getAddressOfRemoteServer().toString();	
		String WEBDRIVERSESSION = newDriverIstance.getSessionId().toString();
		
		//Save  Url and Session WebDriver in properties
		properties.setProperty("WEBDRIVERURL", WEBDRIVERURL);
		properties.setProperty("WEBDRIVERSESSION", WEBDRIVERSESSION);
//		Logger.getLogger("").log(Level.INFO, "WEB DRIVER URL:"+WEBDRIVERURL);
//		Logger.getLogger("").log(Level.INFO, "WEBDRIVERSESSION:"+WEBDRIVERSESSION);
		
		return newDriverIstance;
	}
	
	public static RemoteWebDriver getNewWebDriverNoKill(Properties properties) throws Exception
	{
		RemoteWebDriver newDriverIstance = null;	
		
		String browserType = properties.getProperty("BROWSER", "CHROME");
		
		DesiredCapabilities capabilities = null;
		switch (browserType.toUpperCase()) {
		case "CHROME":
//			System.setProperty("webdriver.chrome.driver",
//		"C:/AgentJarLibs/chromedriver.exe");
//			System.setProperty("webdriver.chrome.driver", "classes/resources/chromedriver.exe");
			properties.setProperty("ClassPathWebDriver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/chromedriver.exe");

			
			System.setProperty("webdriver.chrome.driver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/chromedriver.exe");

			// driver setup
			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized");
			options.addArguments("test-type");
			options.addArguments("enable-automation"); // https://stackoverflow.com/a/43840128/1689770
//			options.addArguments("--headless"); // only if you are ACTUALLY running headless
//			options.addArguments("--no-sandbox"); //https://stackoverflow.com/a/50725918/1689770
//			options.addArguments("--disable-infobars"); //https://stackoverflow.com/a/43840128/1689770
//			options.addArguments("--disable-dev-shm-usage"); //https://stackoverflow.com/a/50725918/1689770
//			options.addArguments("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
			options.addArguments("--disable-gpu"); //https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc
			newDriverIstance = new ChromeDriver(options);
			break;
		case "IE":
			properties.setProperty("ClassPathWebDriver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/IEDriverServer.exe");


			System.setProperty("webdriver.ie.driver", properties.getProperty("CLASS_PATH","classes") + File.separator + "resources/IEDriverServer.exe");
           // IEOptions options = new IEOptions();
			
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability("nativeEvents", false);
			capabilities.setCapability("unexpectedAlertBehaviour", "accept");
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability("disable-popup-blocking", true);
			capabilities.setCapability("enablePersistentHover", true);
			capabilities.setCapability("ignoreZoomSetting", true);
			
			newDriverIstance = new InternetExplorerDriver(capabilities);
			//newDriverIstance.navigate().to("www.google.it");
			break;
		case "FF":
			newDriverIstance = new FirefoxDriver();
			break;
		default:
			throw new Exception("Errore! browser Type Not Supported! ("+ browserType.toUpperCase() +")");	
		}
		
		//Recover Url and Session WebDriver 
//		String WEBDRIVERURL = ((HttpCommandExecutor) newDriverIstance.getCommandExecutor()).getAddressOfRemoteServer().toString();	
//		String WEBDRIVERSESSION = newDriverIstance.getSessionId().toString();
//		
//		//Save  Url and Session WebDriver in properties
//		properties.setProperty("WEBDRIVERURL", WEBDRIVERURL);
//		properties.setProperty("WEBDRIVERSESSION", WEBDRIVERSESSION);
//		Logger.getLogger("").log(Level.INFO, "WEB DRIVER URL:"+WEBDRIVERURL);
//		Logger.getLogger("").log(Level.INFO, "WEBDRIVERSESSION:"+WEBDRIVERSESSION);
		
		return newDriverIstance;
	}
	
	
	public static RemoteWebDriver getDriverInstance(Properties properties) throws Exception{
		
			SessionId session = new SessionId(properties.getProperty("WEBDRIVERSESSION"));
			URL url = new URL(properties.getProperty("WEBDRIVERURL"));
			CommandExecutor executor = new HttpCommandExecutor(url) {

				@Override
				public Response execute(Command command) throws IOException {
					Response response = null;
					if (command.getName() == "newSession") {
						response = new Response();
						response.setSessionId(session.toString());
						response.setStatus(0);
						response.setValue(Collections.<String, String>emptyMap());

						try {
							Field commandCodec = null;
							commandCodec = this.getClass().getSuperclass().getDeclaredField("commandCodec");
							commandCodec.setAccessible(true);
							//commandCodec.set(this, new W3CHttpCommandCodec());
							commandCodec.set(this, new JsonHttpCommandCodec());
							Field responseCodec = null;
							responseCodec = this.getClass().getSuperclass().getDeclaredField("responseCodec");
							responseCodec.setAccessible(true);
							//responseCodec.set(this, new W3CHttpResponseCodec());
							responseCodec.set(this, new JsonHttpResponseCodec());
						} catch (NoSuchFieldException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}

					} else {
						response = super.execute(command);
					}
					return response;
				}
			};

			//return new RemoteWebDriver(executor, new DesiredCapabilities());
			String browserType = properties.getProperty("BROWSER", "CHROME");
			
			
			
			DesiredCapabilities capabilities = null;
			switch (browserType.toUpperCase()) {
			case "CHROME":
				// driver setup
				capabilities = DesiredCapabilities.chrome();
				break;
			case "IE":
				// IEOptions options = new IEOptions();
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capabilities.setCapability("nativeEvents", false);
				capabilities.setCapability("unexpectedAlertBehaviour", "accept");
				capabilities.setCapability("ignoreProtectedModeSettings", true);
				capabilities.setCapability("disable-popup-blocking", true);
				capabilities.setCapability("enablePersistentHover", true);
				capabilities.setCapability("ignoreZoomSetting", true);
				
				break;
			case "FF":
				capabilities = DesiredCapabilities.firefox();
				break;
			default:
				throw new Exception("Errore! browser Type Not Supported! ("+ browserType.toUpperCase() +")");	
			}	
			
			//return new RemoteWebDriver(executor, DesiredCapabilities.chrome());
			return new RemoteWebDriver(executor, capabilities);
		}
	
	
	
	public static Properties getPropertiesIstance(String propertiesFile) throws Exception
	{
		Properties propertiesIstance = new Properties();
		FileInputStream inputStream = null;

		try {
			inputStream = new FileInputStream(propertiesFile);
			propertiesIstance.load(inputStream);	 		
		} 
		catch (Exception e) 
		{
			throw new Exception("Error! Properties File not found!"); 
		}
		
		return propertiesIstance;
	}
	
	
	} 
	
	


