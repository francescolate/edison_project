package com.nttdata.qa.enel.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Collections;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Command;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.Response;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.remote.http.JsonHttpCommandCodec;
import org.openqa.selenium.remote.http.JsonHttpResponseCodec;
import org.openqa.selenium.support.ui.Select;

public class SeleniumManageDriver {
	public static RemoteWebDriver createDriverFromSession(final SessionId sessionId, URL command_executor) {
		CommandExecutor executor = new HttpCommandExecutor(command_executor) {

			@Override
			public Response execute(Command command) throws IOException {
				Response response = null;
				if (command.getName() == "newSession") {
					response = new Response();
					response.setSessionId(sessionId.toString());
					response.setStatus(0);
					response.setValue(Collections.<String, String>emptyMap());

					try {
						Field commandCodec = null;
						commandCodec = this.getClass().getSuperclass().getDeclaredField("commandCodec");
						commandCodec.setAccessible(true);
						// commandCodec.set(this, new W3CHttpCommandCodec());
						commandCodec.set(this, new JsonHttpCommandCodec());
						Field responseCodec = null;
						responseCodec = this.getClass().getSuperclass().getDeclaredField("responseCodec");
						responseCodec.setAccessible(true);
						// responseCodec.set(this, new W3CHttpResponseCodec());
						responseCodec.set(this, new JsonHttpResponseCodec());
					} catch (NoSuchFieldException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}

				} else {
					response = super.execute(command);
				}
				return response;
			}
		};

		// return new RemoteWebDriver(executor, new DesiredCapabilities());
		return new RemoteWebDriver(executor, DesiredCapabilities.chrome());
	}

	public static void main(String[] args) throws Exception {

//		SessionId session = new SessionId("930eabc8184096acd52736a7d901850b");
//		URL url = new URL("http://localhost:44535");
//		RemoteWebDriver driver = getOpenBrowser(session, url);
		Properties prop = new Properties();
		String args2[] = { "prop.properties" };
		InputStream in = new FileInputStream("prop.properties");
		prop.load(in);
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_operatore_avanzato);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_operatore_avanzato);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_CLIENTE", "Residenziale");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("RIGA_DA_ESTRARRE", "2");
		prop.setProperty("QUERY",
				Costanti.recupera_cliente_senza_operazioni_in_corso_pod_non_telegestito
						.replaceAll("@FAMIGLIA_POD@", "IT002%A")
						.replaceAll("@TIPO_CLIENTE@", prop.getProperty("TIPO_CLIENTE")));
		prop.setProperty("EXPECTEDSTATUS", "Chiuso");
		prop.setProperty("EXPECTEDSTATUS", "In Lavorazione");
		// Property aggiuntivi per R2D
		prop.setProperty("EXPECTEDSTATUS_EMISSIONE_RICHIESTA", "In Lavorazione");
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE", "Y");
		prop.setProperty("LINK_R2D_ELE", "http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE", Costanti.utenza_r2d);
		prop.setProperty("PASSWORD_R2D_ELE", Costanti.password_r2d);
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE", "Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Richiesta Disdetta standard");
		prop.setProperty("EVENTO_3OK_ELE", "EIN - Esito Ammissibilità RC1");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito Richiesta RC1");
		// Property per verifiche post emissione
		prop.setProperty("EXPECTEDSTATUS_RICHIESTA_FINALE", "Espletato");
		prop.setProperty("EXPECTEDSTATUSR2D_RICHIESTA_FINALE", "OK");
		prop.setProperty("PRATICA", "Cessazione");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("NUMERO_RICHIESTA", "215103050");
		prop.setProperty("POD_ELE", "IT002E0004223A");

		SessionId session = new SessionId("1ded9e6c6e8756b299510b2ce5ca07e3");
		URL url = new URL("http://localhost:21321");
		RemoteWebDriver driver = getOpenBrowser(session, url);

		//driver.switchTo().defaultContent();

		// driver.switchTo().frame();

		// driver.switchTo().frame("vfFrameId_1582885258222");

		SeleniumUtilities utils = new SeleniumUtilities(driver);

		//utils.FrameSwitcher(By.xpath("//iframe[@title='accessibility title']"));

		WebElement el = utils.waitAndGetElement(By.xpath("//label[text()='Tipologia Cliente']/../select[contains(@id,'tipologiaCliente')]"));

		new Select(el).selectByIndex(1);

		driver.switchTo().defaultContent();
	}

	public static RemoteWebDriver getOpenBrowser(SessionId session, URL url) {

		// ChromeDriver driver = new ChromeDriver();
		// HttpCommandExecutor executor = (HttpCommandExecutor)
		// driver.getCommandExecutor();
		// URL url = executor.getAddressOfRemoteServer();
		// //LOGGER.log(Level.INFO,url);
		// SessionId session_id = driver.getSessionId();
		// //LOGGER.log(Level.INFO,session_id.toString());

		// URL url2 = new URL("http://localhost:14751");
		// SessionId session = new SessionId("290ddb6bd86f90f7a0ece1c620e72f9a");
		RemoteWebDriver driver2 = createDriverFromSession(session, url);
		return driver2;
	}

}
