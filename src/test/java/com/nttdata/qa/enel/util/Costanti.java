package com.nttdata.qa.enel.util;

import core.Decrypt;

import java.util.ArrayList;
import java.util.Arrays;

public class Costanti {

	public final static boolean setJiraStatus = true; 
	public static boolean reportToDB=false;
	public final static String Postgresql_Table_Name = "EXECUTION_2021_R3";
    public final static int TIMEOUT = 10;
    public final static int POLLING = 1;
    public final static int IMPLICIT_TIMEOUT = 60;
    public final static String TIPO_DELEGA = "Nessuna delega";
    public final static String prodotto_residenziale_ele_fibra = "Scegli Tu";
//    public final static String prodotto_residenziale_ele_fibra = "SCEGLI OGGI LUCE";
    public final static String prodotto_residenziale_ele = "SCEGLI OGGI LUCE 40";
    public final static String prodotto_residenziale_ele_elegibile = "Scegli Tu";
    public final static String subentro_id14_popupmercato = "Attenzione! Per Mercato Riferimento Prodotto = Salvaguardia è possibile inserire solo siti elettrici";
    public final static String prodotto_residenziale_gas = "NEW_Energia Sicura Gas_15 percento";
    public final static String prodotto_residenziale_gas_fatturabile = "Speciale Gas 60";    
    public final static String prodotto_residenziale_gas_fibra = "NEW_E-Light Gas_WEB";    
//    public final static String prodotto_non_residenziale_ele = "New_Soluzione Energia Impresa Business";
    public final static String prodotto_non_residenziale_ele = "New_Soluzione Energia Impresa Business";
    public final static String prodotto_non_residenziale_ele_elegibile = "New_Soluzione Energia Impresa Business";
//  public final static String prodotto_non_residenziale_gas = "New_Trend Sicuro Gas Impresa Business";
    public final static String prodotto_non_residenziale_gas = "SICURA GAS";
    public final static String utenza_salesforce_pe_attivazioni = "af09533@enelfreemarket.com.uat";
    public final static String password_salesforce_pe_attivazioni = Decrypt.decrypt("83LVnuD3PEtkGwjVc0pmVg==");
    public final static String utenza_salesforce_operatore_avanzato = "op_cli_avanzato_cfi_l@enelfreemarket.com.uat";
    public final static String password_salesforce_operatore_avanzato =  Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_salesforce_operatore_pe_user = "pe_user_cfi_l@crmtenelfreemarket.com.uat";
    public final static String password_salesforce_operatore_pe_user = Decrypt.decrypt("3MaInyncYAjIwfUpeme9zQ==");
    public final static String utenza_salesforce_operatore_pe_2 = "af09531@crmtenelfreemarket.com.uat";
    public final static String password_salesforce_operatore_pe_2 = Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_salesforce_pe = "af09533@enelfreemarket.com.uat";
    public final static String password_salesforce_pe = Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_salesforce_pe_manager = "pe_manager_cfi_l@enelfreemarket.com.uat";
    public final static String password_salesforce_pe_manager = Decrypt.decrypt("VRHhH8SUguMLVexYpdjbMw==");
    public final static String utenza_salesforce_s2s_manager = "s2s_manager_cfi_l@crmtenelfreemarket.com.uat";
    public final static String password_salesforce_s2s_manager = Decrypt.decrypt("xbRlkHTuG1FbyESPTGKlqg==");
    public final static String utenza_salesforce_penp_pag = "penp_pag_cfi@enelfreemarket.com.uat";
    public final static String password_salesforce_penp_pag = "UATSFDC2019";
    public final static String utenza_salesforce_penp_mag = "penp_mag_cfi@enelfreemarket.com.uat";
    public final static String password_salesforce_penp_mag = Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_salesforce_s2s_attivazioni = "s2s_manager_cfi_l@crmtenelfreemarket.com.uat";
    public final static String password_salesforce_s2s_attivazioni = Decrypt.decrypt("xbRlkHTuG1FbyESPTGKlqg==");
    public final static String utenza_salesforce_s2s_attivazioni_QC = "s2squalitycontroller_cfi_l@enelfreemarket.com.uat";
    public final static String password_salesforce_s2s_attivazioni_QC = Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_admin_salesforce = "domenico.luongo@enelfreemarket.com.uat";
    public final static String password_admin_salesforce = Decrypt.decrypt("ZQJAxMbR3Yy4TRU6DqLqUw==");
    public final static String utenza_forzatura_salesforce = "bo_fatt_primo_livello_cfi_l@enelfreemarket.com.uat";
    public final static String password_forzatura_salesforce = Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_Tp2_crm = "testing.crm.automation@gmail.com";
    public final static String password_Tp2_crm = Decrypt.decrypt("DiNBmaSJxGYQHMTYy3r8sg==");
    public final static String utenza_Tp8_web = "web.testing.automation@gmail.com";
    public final static String password_Tp8_web = Decrypt.decrypt("DiNBmaSJxGYQHMTYy3r8sg==");
    public final static String utenza_Tp2_mobile = "mobile.testing.automation@gmail.com";
    public final static String password_Tp2_mobile = Decrypt.decrypt("DiNBmaSJxGYQHMTYy3r8sg==");
    public final static String utenza_Tp2_mobile_a = "mobile.testing.a.utomation@gmail.com";
    public final static String password_Tp2_mobile_a = Decrypt.decrypt("DcoSN5wvldxa3Wu+Cu3tYw==");
    public final static String utenza_salesforce_s2s = "s2s_user_cfi_l@enelfreemarket.com.uat";
    public final static String password_salesforce_s2s = Decrypt.decrypt("xbRlkHTuG1FbyESPTGKlqg==");
    public final static String utenza_r2d = "AF45318@enelint.global";
    public final static String password_r2d = Decrypt.decrypt("WIpeqMCLDYHSuddkl+P2jw==");
    public final static String utenza_r2d_swa = "AF45318@enelint.global";
    public final static String password_r2d_swa = Decrypt.decrypt("WIpeqMCLDYHSuddkl+P2jw==");
    public final static String utenza_salesforce_bo = "bo_rcd_super_user_cfi_l@enelfreemarket.com.uat";
    public final static String password_salesforce_bo = Decrypt.decrypt("Zil9l43L6NmrtIazMMfrzQ==");
    public final static String utenza_salesforce_bo1 = "doc.complesso.tl.cfi@enelfreemarket.com.uat";
    public final static String password_salesforce_bo1 = Decrypt.decrypt("0/hCkHuX72BhKXnqI7B3Ew==");
    public final static String utenza_r2d_cla = "AE82870@enelint.global";
    public final static String password_r2d_cla = Decrypt.decrypt("/wRk3I0U+rchtCvFlB18rA==");
    public final static String salesforceLink = "https://enelcrmt--uat.cs88.my.salesforce.com";
    public final static String workbenchLink = "https://workbench.developerforce.com/query.php";
    public final static String workbenchUser = "domenico.luongo@enelfreemarket.com.uat"; // ae02967@crmtenelfreemarket.com.uat	Enel$2021 UTENZA AMMINISTRAELIO
    public final static String workbenchPwd =Decrypt.decrypt("ZQJAxMbR3Yy4TRU6DqLqUw==");// Decrypt.decrypt("QssdosJ8kEArmC+CfxK62A==");
    public final static String utenza_s2s_quality_controller = "s2squalitycontroller_cfi_l@enelfreemarket.com.uat";
    public final static String password_s2s_quality_controller = Decrypt.decrypt("MrEDrphwzNcFgQkFl0cqEg==");
    public final static String utenza_antichurn = "AE77777@enelfreemarket.com.uat";
    public final static String password_antichurn = Decrypt.decrypt("ls6vCzItMtATyUHT3KWcNw==");
    public final static String utenza_antichurn2 = "antichurn_fo_cfi@enelfreemarket.com.uat";
    public final static String password_antichurn2 = Decrypt.decrypt("Fg4yQcfut0FpjXuvmrjOCQ==");
    public final static String utenza_salesforce_operatore_pe_3 = "pe_manager_cfi_Autmation_Cavalleri@enelfreemarket.com.uat";
    public final static String password_salesforce_operatore_pe_3 = Decrypt.decrypt("YKWslIHhiXJjc1UndJy2sw==");
    public final static String api_url_getUUID = "https://uniqueid-coll.enel.com:8243/profile/account";
    public final static String api_url_accountExists = "https://uniqueid-coll.enel.com:8243/profile/account/exists";
    public final static String api_url_migrationExistence= "https://uniqueid-coll.enel.com:8243/migration/account";
    public final static String api_url_getToken = "https://uniqueid-coll.enel.com:8243/token";
    public final static String api_url_changePassword = "https://uniqueid-coll.enel.com:8243/provisioning/v2/account/%s/change-password";
    public final static String api_url_getOTP = "https://uniqueid-coll.enel.com:8243/backoffice/otp";
    public final static String api_url_accountRegistration = "https://uniqueid-coll.enel.com:8243/registration/account";
    public final static String api_url_getConfirmationLink = "https://uniqueid-coll.enel.com:8243/provisioning/validation-code/validate";
    public final static String api_url_removeTestFromTextExec_on_Jira = "https://jira.springlab.enel.com/rest/raven/1.0/api/testexec/$TEST_EXEC_KEY$$/test/$TEST_KEY$";
    public final static String api_url_deleteAccount = "https://uniqueid-coll.enel.com:8243/registration/account/";
    //public final static String authUser = "ENELINT\\AF09527";
    //public final static String authPassword = Decrypt.decrypt("vIEv514T6Uz4Ir/JoZ8icA==");
    public final static String JiraAuthUser = "franck.laterza94@gmail.com";
    public final static String JiraAuthPassword = "!francklatFL7";//Decrypt.decrypt("Da8inCQrp0QSd5TWqCCzRA=="); 
    //public final static String proxy = "proxy.risorse.enel";
    //public final static String proxyAddress = "proxy-euc1.enelint.global";
    //public final static int proxyPort = 8080;
    public final static String WP_BasicAuth_Username = "enelsite";
    public final static String WP_BasicAuth_Password = Decrypt.decrypt("rDnDqtijnO/Y3fy+pCz1YILDuDuCx2uMl39MVYlzVVY=");
    public final static String WP_Corporate_BasicAuth_Password = Decrypt.decrypt("OGi9kJw5WwLSWJ4VsPId3A==");
    public final static String prp_lnk = "LINK";
    public final static String workbenchSelectionEnv = "Sandbox";
    public final static String statusUbiest = "ON";//Valori possibili ON, OFF
    public final static ArrayList<String> lista_KO_SAP = new ArrayList<String>( Arrays.asList("1023","1042", "2064","1123","2089"));
    public final static ArrayList<String> lista_KO_SEMPRE = new ArrayList<String>( Arrays.asList("2403", "2400","GASWS017","GASTEC05","18")); 
    public final static String get_data_for_registration_res = "SELECT Account.NE__Fiscal_code__c , Account.ITA_IFM_First_name__c,Account.ITA_IFM_Last_name__c, \n" +
            "ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_POD__c,AccountId,NE__Status__c, RecordType.Name \n" +
            "FROM Asset \n" +
            "WHERE RecordType.Name = 'Commodity'  and NE__Status__c IN ('Active') and ITA_IFM_ConsumptionNumber__c !='' \n" +
            "and Account.NE__Fiscal_code__c !='' and Account.RecordType.Name = 'Residenziale' \n" +
            "and ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
            "LIMIT 100 OFFSET 50";
    public final static String get_data_for_registration_ress = "SELECT Account.NE__Fiscal_code__c , Account.ITA_IFM_First_name__c,Account.ITA_IFM_Last_name__c, \n" +
    		"ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_POD__c,AccountId,NE__Status__c, RecordType.Name \n" +
            "FROM Asset \n" +
    		"WHERE Account.NE__Fiscal_code__c !='' and NE__Status__c = 'Suspended' \n" +
            "and RecordType.Name = 'Commodity' and Account.RecordType.Name = 'Residenziale' \n" +
    		"and RecordType.Name NOT IN ('VAS')LIMIT 200";
    public final static String get_data_for_registration_bsn_1 = "SELECT Account.id, Account.NE__Fiscal_code__c , ITA_IFM_ConsumptionNumber__c, Account.ITA_IFM_Company__c\n" +
            "FROM Asset \n" +
            "WHERE RecordType.Name = 'Commodity'  and NE__Status__c IN ('Active') and ITA_IFM_ConsumptionNumber__c !='' \n" +
            "and Account.NE__Fiscal_code__c !='' and Account.RecordType.Name = 'Business' \n" +
            "and Account.ITA_IFM_CustomerType__c Like 'Azienda'\n" +
            "AND Account.CreatedDate > 2019-07-30T00:00:00.000+0000\n" +
            "LIMIT 200";
    public final static String get_data_for_registration_bsn_2 = "Select FirstName, LastName\n" +
            "From Contact\n" +
            "Where AccountId = '$ACCOUNT_ID$'";
    public final static String query_ACR_17 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c\r\n" +
            "FROM Asset\r\n" +
            "WHERE Account.NE__Fiscal_code__c !=''\r\n" +
            "and Account.RecordType.Name IN ('Residenziale')\r\n" +
            "and NE__Status__c = 'Active'\r\n" +
            "and CreatedDate > 2019-12-01T00:00:00Z\r\n" +
            "AND ProductCode = 'infoenelenergia' \r\n" +
            "AND ITA_IFM_ConsumptionNumber__c ='#NUMERO_CLIENTE#'";
    public final static String query_ACB_18 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c\r\n" +
            "FROM Asset\r\n" +
            "WHERE Account.NE__Fiscal_code__c !=''\r\n" +
            "and NE__Status__c = 'Active'\r\n" +
            "AND ProductCode = 'infoenelenergia' \r\n" +
            "AND ITA_IFM_ConsumptionNumber__c ='#NUMERO_CLIENTE#'";
    public final static String query1_ACB_32_to_37 = "select AccountId , ITA_IFM_CreatedDate__c , Priority, Status, Origin, Subject, ITA_IFM_SubStatus__c\r\n" +
            "from case \r\n" +
            "where AccountId = '0010Y00000FqA99QAF' and ITA_IFM_CreatedDate__c = TODAY and Subject = 'GESTIONE PRIVACY'\r\n" +
            "order by ITA_IFM_CreatedDate__c desc";
    public final static String query_pod_cessato_gas =

            "SELECT Id, Name, ITA_IFM_HistoricalFlag__c, ITA_IFM_POD__c, ITA_IFM_Commodity__c, ITA_IFM_PODStatus__c \n" +
                    "FROM NE__Service_Point__c \n" +
                    "where ITA_IFM_Commodity__c='Gas' \n" +
                    "and ITA_IFM_PODStatus__c='cessato' \n" +
                    "and ITA_IFM_HistoricalFlag__c=true \n" +
                    "limit 1";
    public final static String query2_ACB_32_33 = "Select ITA_IFM_Account__c,ITA_IFM_CreationDate__c,ITA_IFM_Phone__c, ITA_IFM_MarketingConsentPhone__c , ITA_IFM_ThirdPartyConsentPhone__c, ITA_IFM_Email__c,\r\n" +
            "ITA_IFM_MarketingConsentEmail__c , ITA_IFM_ThirdPartyConsentEmail__c \r\n" +
            "from ITA_IFM_Privacy__c\r\n" +
            "where ITA_IFM_Account__c ='0010Y00000FqA99QAF' and ITA_IFM_CreationDate__c = TODAY\r\n" +
            "order by ITA_IFM_CreationDate__c desc";
    public final static String query2_ACB_34_36_37 = "Select ITA_IFM_Account__c,ITA_IFM_CreationDate__c,ITA_IFM_Phone__c, ITA_IFM_MarketingConsentPhone__c , ITA_IFM_ThirdPartyConsentPhone__c, ITA_IFM_Email__c,\r\n" +
            "ITA_IFM_MarketingConsentEmail__c , ITA_IFM_ThirdPartyConsentEmail__c , ITA_IFM_ProfilingConsent__c\r\n" +
            "from ITA_IFM_Privacy__c\r\n" +
            "where ITA_IFM_Account__c ='0010Y00000FqA99QAF' and ITA_IFM_CreationDate__c = TODAY\r\n" +
            "order by ITA_IFM_CreationDate__c desc";
    public final static String recupera_cliente_non_residenziale = "SELECT Id, Name, NE__VAT__c, NE__Fiscal_code__c, Type, ITA_IFM_CustomerType__c \n" +
            "FROM Account where Type='NON RESIDENZIALE' LIMIT 50";
    
    public final static String get_AccountStatus_171 ="select Id,CaseNumber, AccountId, CreatedDate, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, ITA_IFM_AccountFirstName__c, ITA_IFM_AccountLastName__c, ITA_IFM_CommunicationEmailAddress__c\r\n" + 
    		"from Case\r\n" + 
    		"where AccountId = '0011l00000YKZuKAAX' and CreatedDate = TODAY and ITA_IFM_Process__c!= ''";
    
    public final static String get_AccountType_171 ="select Id,ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c\r\n" + 
    		"from ITA_IFM_Case_Items__c\r\n" + 
    		"where ITA_IFM_Account_ID__c ='0011l00000YKZuKAAX' and CreatedDate = TODAY";
    
    
    public final static String query_106 =
            "SELECT \n" +
                    "Account.Id, Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.ITA_IFM_Complete_Name__c, ITA_IFM_Commodity__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_Address__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "AND RecordType.Name = 'Commodity' \n" +
                    "AND ITA_IFM_Commodity__c = 'ELE' \n" +
                    "AND Account.RecordType.Name = 'Residenziale' \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "and NE__ProdId__r.Name != 'Bolletta Web' \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND CreatedDate > 2020-01-20T00:00:00.000+0000 \n" +
                    "LIMIT 10";
    public final static String query_107 =
            "SELECT \n" +
                    "Account.Id, Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.ITA_IFM_Complete_Name__c, ITA_IFM_Commodity__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_Address__c \n" +
                    "FROM Asset \n" +
                    "WHERE CreatedDate > 2019-01-01T00:00:00Z \n" +
                    "AND NE__Status__c = 'Active' \n" +
                    "AND RecordType.Name = 'Commodity' \n" +
                    "AND ITA_IFM_Commodity__c = 'GAS' \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' \n" +
                    "AND ITA_IFM_SAP_Contract__c != null \n" +
                    "AND ITA_IFM_Sap_Contractual_Account__c != null \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = '0301' \n" +
                    "AND Account.RecordType.Name != 'Residenziale' \n" +
                    "AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "LIMIT 1";

//	public final static String query_107 =
//			"SELECT \n"+
//			"Account.Id, Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.ITA_IFM_Complete_Name__c, ITA_IFM_Commodity__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_Address__c \n"+
//			"FROM Asset \n"+
//			"WHERE NE__Status__c = 'Active' \n"+
//			"AND RecordType.Name = 'Commodity' \n"+
//			"AND ITA_IFM_Commodity__c = 'GAS' \n"+
//			"AND Account.RecordType.Name <> 'Residenziale' \n"+
//			"AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' \n"+
//			"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = '0301' \n"+
//			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n"+
//			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"+
//			"AND CreatedDate > 2020-01-20T00:00:00.000+0000 \n"+
//			"LIMIT 10";
    public final static String query_111_1 =
            "SELECT \n" +
                    "Account.Id \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "AND RecordType.Name = 'Commodity' \n" +
                    "AND Account.RecordType.Name = 'Condominio' \n" +
                    "AND ITA_IFM_Commodity__c = 'ELE' \n" +
//                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND AccountId NOT IN ('0010Y00000FlTt2QAF','0010Y00000Fm0amQAB') "+                    
                    "AND Account.CreatedDate > 2019-03-01T00:00:00.000+0000 \n" +
                    "LIMIT 10";
    public final static String query_111_2 = "SELECT \n" +
            "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
            "FROM Asset \n" +
            "WHERE \n" +
            "RecordType.Name = 'Commodity' \n" +
            "AND AccountId IN %s \n" +
            "GROUP BY AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
            "HAVING count(Id) = 1";
    public final static String query_111_3 = "SELECT \n" +
            "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c , Account.NE__VAT__c, Account.ITA_IFM_Company__c , NE__Service_Point__r.ITA_IFM_POD__c \n" +
            "FROM Asset \n" +
            "WHERE \n" +
            "RecordType.Name = 'Commodity' \n" +
            "AND AccountId IN %s ";
    public final static String query_112_1 =
            "SELECT Account.Id \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "AND RecordType.Name = 'Commodity' \n" +
                    "AND Account.RecordType.Name = 'Residenziale' \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
                    "AND ITA_IFM_Commodity__c = 'GAS' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = '0301' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND Account.CreatedDate > 2018-11-01T00:00:00.000+0000 \n" +
                    "LIMIT 5";
    public final static String query_112_2 =
            "SELECT \n" +
                    "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
                    "FROM Asset \n" +
                    "WHERE \n" +
                    "RecordType.Name = 'Commodity' \n" +
                    "AND AccountId IN %s \n" +
                    "GROUP BY AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
                    "HAVING count(Id) = 1";
    public final static String query_112_3 =
            "SELECT \n" +
                    "AccountId, Account.Name, Account.NE__Fiscal_code__c , Account.NE__VAT__c, Account.ITA_IFM_Company__c , NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE \n" +
                    "RecordType.Name = 'Commodity' \n" +
                    "AND AccountId IN %s ";
    public final static String query_113_1 =
            "SELECT \n" +
                    "Account.Id \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "AND RecordType.Name = 'Commodity' \n" +
                    "AND Account.RecordType.Name = 'Impresa Individuale' \n" +
                    "AND ITA_IFM_Commodity__c = 'ELE' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND CreatedDate > 2019-07-01T00:00:00.000+0000\n" +
                    "LIMIT 3";
    public final static String query_113_2 =
            "SELECT \n" +
                    "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
                    "FROM Asset \n" +
                    "WHERE \n" +
                    "RecordType.Name = 'Commodity' \n" +
                    "AND AccountId IN %s \n" +
                    "GROUP BY AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
                    "HAVING count(Id) = 1\n" +
"			AND CreatedDate > 2018-07-01T00:00:00.000+0000 ";
    public final static String query_113_3 =
            "SELECT \n" +
                    "AccountId, Account.Name, Account.NE__Fiscal_code__c , Account.NE__VAT__c, Account.ITA_IFM_Company__c , NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE \n" +
                    "RecordType.Name = 'Commodity' \n" +
                    "AND AccountId IN %s ";
    public final static String query_113_4 =
            "SELECT "
                    + "Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, ITA_IFM_Pod_Pdr__c, ITA_IFM_Commodity__c, NE__Status__c, ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c "
                    + "FROM   Asset WHERE "
                    + "Account.ITA_IFM_CustomerType__c = 'Casa' AND NE__Status__c='active' "
                    + "AND ITA_IFM_Service_Use__c ='uso abitativo' AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  LIMIT 200 ";
    public final static String query_113_5 =
            "SELECT\r\n" +
                    "Account.NE__Fiscal_code__c,\r\n" +
                    "Account.Name,\r\n" +
                    "account.ITA_IFM_CustomerType__c,\r\n" +
                    "ITA_IFM_Pod_Pdr__c,\r\n" +
                    "ITA_IFM_Commodity__c,\r\n" +
                    "NE__Status__c,\r\n" +
                    "ITA_IFM_Resident__c\r\n" +
                    "FROM \r\n" +
                    " Asset\r\n" +
                    "WHERE\r\n" +
                    "Account.ITA_IFM_CustomerType__c != 'Casa'\r\n" +
                    "AND NE__Status__c IN ( 'suspended' , 'reduced')\r\n" +
                    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\r\n" +
                    "AND Account.NE__Fiscal_code__c = 'STRMNL57L01E044U' \r\n" +
                    "LIMIT 20\r\n" +
                    "";
    public final static String query_114_1 =
            "SELECT \n" +
                    "Account.Id \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "AND RecordType.Name = 'Commodity' \n" +
                    "AND Account.RecordType.Name = 'Business' \n" +
                    "AND ITA_IFM_Commodity__c = 'ELE' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND CreatedDate > 2019-09-01T00:00:00.000+0000 ";
    public final static String query_114_2 =
            "SELECT \n" +
                    "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
                    "FROM Asset \n" +
                    "WHERE \n" +
                    "RecordType.Name = 'Commodity' \n" +
                    "AND AccountId IN %s \n" +
                    "GROUP BY AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Company__c \n" +
                    "HAVING count(Id) = 1";
    public final static String query_114_3 =
            "SELECT \n" +
                    "AccountId, Account.Name, Account.NE__Fiscal_code__c , Account.NE__VAT__c, Account.ITA_IFM_Company__c , NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE \n" +
                    "RecordType.Name = 'Commodity' \n" +
                    "AND AccountId IN %s ";
    //R3 - FR 03.08.2021
    public final static String Query_id_132="SELECT ITA_IFM_Case__r.CaseNumber, ITA_IFM_Case__r.Status, ITA_IFM_Case__r.ITA_IFM_SubStatus__c, ITA_IFM_Subject__c, ITA_IFM_POD__c, Name, ITA_IFM_Status__c, \r\n"
    		+ "ITA_IFM_BPM_ID__c, ITA_IFM_SAP_Status__c, ITA_IFM_UDB_Status__c\r\n"
    		+ "FROM ITA_IFM_Case_Items__c\r\n"
    		+ "WHERE ITA_IFM_Case__r.CaseNumber = '@ID_RICHIESTA@'	";
    /**
     * R3 FR 04.08.2021
     * Attività associate alla richiesta  - sostituire @ID_RICHIESTA@ con id richiesta
     */
    public final static String Query_id_94_by_id_richiesta=
    		"select CreatedDate, id, ITA_IFM_Number__c, Name, ITA_IFM_Status__c, ITA_IFM_Tipo_Attivita__c, ITA_IFM_Specifica__c, ITA_IFM_Causale_Contatto__c, ITA_IFM_Descrizione__c, ITA_IFM_Commodity__c ,ITA_IFM_FromCompetence__c, ITA_IFM_Assegnato_A__c, ITA_IFM_Case__r.CaseNumber\r\n"
    		+ "FROM wrts_prcgvr__Activity__c\r\n"
    		+ "where ITA_IFM_Case__r.CaseNumber = '@ID_RICHIESTA@' and ITA_IFM_Specifica__c='Domiciliazione estera'";
    /**
     * R3 FR 04.08.2021
     * Attività associate all'offerta - sostituire @ID_IFFERTA@ con id offerta
     */
    public final static String Query_id_94_by_id_offerta=
    		"select CreatedDate, id, ITA_IFM_Number__c, Name, ITA_IFM_Status__c, ITA_IFM_Tipo_Attivita__c, ITA_IFM_Specifica__c, ITA_IFM_Causale_Contatto__c, ITA_IFM_Descrizione__c, ITA_IFM_Commodity__c ,ITA_IFM_FromCompetence__c, ITA_IFM_Assegnato_A__c, ITA_IFM_Case__r.CaseNumber\r\n"
    		+ "FROM wrts_prcgvr__Activity__c\r\n"
    		+ "where ITA_IFM_Quote__r.Name = '@ID_IFFERTA@'";
    
    public final static String query_id_104=
    		"SELECT Id, ITA_IFM_Commodity__c, NE__Status__c, NE__StartDate__c, NE__EndDate__c, Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_Address__c, ITA_IFM_Usage_Type__c, ITA_IFM_ConsumptionNumber__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_SAP_Primary_BP__c, \r\n"
    		+ "ITA_IFM_SAP_Contract__c, ITA_IFM_Sap_Contractual_Account__c, NE__BillingProf__r.NE__Payment__c, ITA_IFM_BackupMOP__c, NE__BillingProf__r.NE__Iban__c, ITA_IFM_PreviousOrder__c, NE__ProdId__r.Name\r\n"
    		+ "FROM Asset\r\n"
    		+ "WHERE RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Service_Point__r.ITA_IFM_POD__c = '@@POD@@'";
    
    public final static String query_id_54_1 = "SELECT Account.NE__Fiscal_code__c, count(id) \n" +
            "FROM Asset \n" +
            "WHERE Account.NE__Fiscal_code__c !='' \n" +
            "and Account.RecordType.Name = 'Business' \n" +
            "and NE__Status__c = 'Active' \n" +
            "and RecordType.Name = 'Commodity' \n" +
            "and CreatedDate > 2019-12-01T00:00:00Z \n" +
            "and RecordType.Name NOT IN ('VAS') \n" +
            "GROUP BY Account.NE__Fiscal_code__c \n" +
            "HAVING count(Id) > 1 \n" +
            "LIMIT 150 ";
    public final static String query_id_54_2 = "select id, Username, email, Account.NE__VAT__c, Account.Name \n"
            + "from user \n" +
            "where Account.NE__VAT__c in ('@NE__Fiscal_code__c@') ";
    public final static String get_BillingAddress_171 = "select \r\n" +
            "account.Name, \r\n" +
            "Account.NE__Fiscal_code__c,\r\n" +
            "Account.NE__VAT__c, \r\n" +
            "Account.RecordType.Name,\r\n" +
            "Account.ITA_IFM_Type_Legal_Form__c,\r\n" +
            "ITA_IFM_Commodity__c,\r\n" +
            "NE__Service_Point__r.ITA_IFM_POD__c,\r\n" +
            "ITA_IFM_ConsumptionNumber__c,\r\n" +
            "NE__ProdId__r.Name,\r\n" +
            "ITA_IFM_BillingAddress__c\r\n" +
            "From Asset \r\n" +
            "where ITA_IFM_ConsumptionNumber__c = '$CUSTOMER_NUMBER$'";
    public final static String query_200 =
            "SELECT id, NE__Fiscal_code__c \n" +
                    "FROM Account \n" +
                    "WHERE ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "and Id not IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'VAS' and NE__Status__c ='Active' and CreatedDate > 2020-01-01T00:00:00.000+0000) \n" +
                    "and Id IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'Commodity' and NE__Status__c ='Active' and ITA_IFM_Service_Use__c = 'Uso Abitativo' and CreatedDate > 2020-01-01T00:00:00.000+0000) LIMIT 10";


    
//    public final static String query_200_1=
//   		 "SELECT id \n"+
//   		 "FROM Account \n"+
//   		 "WHERE ITA_IFM_CustomerType__c = 'Casa' \n"+
//   		 "and Id not IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'VAS' and NE__Status__c ='Active' \n"+
//   		 "and CreatedDate > 2019-11-01T00:00:00.000+0000) \n"+
//   		 "and Id IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'Commodity' and NE__Status__c ='Active' and ITA_IFM_Service_Use__c = 'Uso Abitativo' \n"+
//   		 "and CreatedDate > 2019-11-01T00:00:00.000+0000) \n"+
//   		 "LIMIT 400";
//    
//
////   		 public final static String query_200_2=
////   		 "SELECT \n"+
////   		 "AccountId \n"+
////   		 "FROM Asset \n"+
////   		 "WHERE \n"+
////   		 "RecordType.Name = 'Commodity' \n"+
////   		 "and NE__Status__c ='Active' \n"+
////   		 "AND AccountId IN %s  \n"+
////   		 "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"+
////   		 "GROUP BY AccountId  \n"+
////   		 "HAVING count(Id) = 1";
//
//			 public final static String query_200_2=
//			 "SELECT \n"+
//			 "AccountId, Account.RecordType.Name,Account.NE__Fiscal_code__c,ITA_IFM_ConsumptionNumber__c \n"+
//			 "FROM Asset \n"+
//			 "WHERE \n"+
//			 "RecordType.Name = 'Commodity' \n"+
//			 "and AccountId IN %s  \n"+
//			 "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"+
//			 "GROUP BY AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Company__c, ITA_IFM_ConsumptionNumber__c  ";
//
////   		 public final static String query_200_3 =  
////   	    		 "SELECT \n"+
////   		         "ITA_IFM_ConsumptionNumber__c, \n"+
////   		         "Account.ITA_IFM_HolderContact__c, \n"+
////   	    		 "Account.NE__Fiscal_code__c, \n"+
////   	    		 "Account.Name, \n"+
////   	    		 "Account.ITA_IFM_Company__c, \n"+
////   	    		 "account.ITA_IFM_CustomerType__c, \n"+
////   	    		 "ITA_IFM_Pod_Pdr__c, \n"+
////   	    		 "ITA_IFM_Service_Use__c, \n"+
////   	    		 "ITA_IFM_Commodity__c, \n"+
////   	    		 "NE__Status__c, \n"+
////   	    		 "NE__Service_Point__r.ITA_IFM_POD__c, \n"+
////   	    		 "ITA_IFM_Resident__c, \n"+
////   	    		 "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n"+
////   	    		 "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n"+
////   	    		 "NE_BillingProf_Type__c \n"+
////   	    		 "FROM  \n"+
////   	    		 "Asset \n"+
////   	    		 "WHERE \n"+
////   	    		 "AccountId IN %s  \n"+
////   	    	     "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"+
////   	    		 "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n"+
////   	    	     "AND ITA_IFM_SAP_Contract__c != null \n"+
////   	    	     "AND ITA_IFM_Sap_Contractual_Account__c != null  \n"+
////   	    	     "AND Account.ITA_IFM_HolderContact__c != null \n"+
////   	    	     "LIMIT 3";
//
//   		 public final static String query_200_3 =  
//   	    		 "SELECT \n"+
//   		         "AccountId,name,ProductCode,Account.NE__Fiscal_code__c \n"+
//   		         "FROM  \n"+
//   	    		 "Asset \n"+
//   	    		 "WHERE \n"+
//   	    		 "AccountId IN %s  \n"+
//   	    	     "AND (NOT NE__ProdId__r.Name like '@PRODOTTO_DEFAULT@') ";
//
//      		 public final static String query_200_4 =  
//   	    		 "SELECT \n"+
//   		         "id, ITA_IFM_Account__r.RecordType.name, ITA_IFM_Account__r.ITA_IFM_Complete_Name__c,  \n"+
//   		         "ITA_IFM_Mobile_Phone__c, ITA_IFM_Account__r.NE__Fiscal_code__c,ITA_IFM_Email__c, ITA_IFM_Status__c  \n"+
//   		         "FROM  \n"+
//   	    		 " ITA_IFM_Data_Contact_Cert_History__c \n"+
//   	    		 "WHERE \n"+
//   	    		 "ITA_IFM_Status__c = 'CERTIFICATA'  \n"+
//   	    		 "and ITA_IFM_Account__r.NE__Fiscal_code__c IN %s  \n"+
//   	    	     "LIMIT 100";

   		 public final static String query_200_3 =  
   	    		 "SELECT \n"+
   		         "AccountId,name,ProductCode,Account.NE__Fiscal_code__c \n"+
   		         "FROM  \n"+
   	    		 "Asset \n"+
   	    		 "WHERE \n"+
   	    		 "AccountId IN %s  \n"+
   	    	     "AND (NOT NE__ProdId__r.Name like '@PRODOTTO_DEFAULT@') ";

      		 public final static String query_200_4 =  
   	    		 "SELECT \n"+
   		         "id, ITA_IFM_Account__r.RecordType.name, ITA_IFM_Account__r.ITA_IFM_Complete_Name__c,  \n"+
   		         "ITA_IFM_Mobile_Phone__c, ITA_IFM_Account__r.NE__Fiscal_code__c,ITA_IFM_Email__c, ITA_IFM_Status__c  \n"+
   		         "FROM  \n"+
   	    		 " ITA_IFM_Data_Contact_Cert_History__c \n"+
   	    		 "WHERE \n"+
   	    		 "ITA_IFM_Status__c = 'CERTIFICATA'  \n"+
   	    		 "and ITA_IFM_Account__r.NE__Fiscal_code__c IN %s  \n"+
   	    	     "LIMIT 100";
    
//	// nuova versione 18 novembre 2020
    public final static String query_200_1=
		 "SELECT id, NE__Fiscal_code__c \n"+
		 "FROM Account \n"+
		 "WHERE ITA_IFM_CustomerType__c = 'Casa' \n"+
		 "and Id IN (SELECT AccountId FROM Asset WHERE NE__Status__c ='Active' and recordtype.Name = 'Commodity' \n"+
		 "and ITA_IFM_Service_Use__c = 'Uso Abitativo' and ITA_IFM_SAP_Contract__c !=null \n"+
		 "and ITA_IFM_Sap_Contractual_Account__c !=null and CreatedDate > 2020-01-01T00:00:00.000+0000) \n"+
		 "and Id NOT IN (SELECT AccountId FROM Asset WHERE recordtype.Name ='VAS') \n"+
		 "LIMIT 100";
	    
	// nuova versione 18 novembre 2020
 	public final static String query_200_2 =  
    		 "SELECT \n"+
	         "id, ITA_IFM_Account__r.RecordType.name, ITA_IFM_Account__r.ITA_IFM_Complete_Name__c,  \n"+
	         "ITA_IFM_Mobile_Phone__c, ITA_IFM_Account__r.NE__Fiscal_code__c,ITA_IFM_Email__c, ITA_IFM_Status__c  \n"+
	         "FROM  \n"+
    		 " ITA_IFM_Data_Contact_Cert_History__c \n"+
    		 "WHERE \n"+
    		 "ITA_IFM_Status__c = 'CERTIFICATA'  \n"+
    		 "and ITA_IFM_Account__r.NE__Fiscal_code__c IN %s  \n"+
    	     "LIMIT 100";
    
    public final static String query_200_1a=
     		 "SELECT id \n"+
     		 "FROM Account \n"+
     		 "WHERE ITA_IFM_CustomerType__c = 'Casa' \n"+
     		 "and Id not IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'VAS' and NE__Status__c ='Active' \n"+
     		 "and CreatedDate > 2020-01-01T00:00:00.000+0000) \n"+
     		 "and Id IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'Commodity' and NE__Status__c ='Active' and ITA_IFM_Service_Use__c = 'Uso Abitativo' \n"+
     		 "and CreatedDate > 2020-01-01T00:00:00.000+0000) \n"+
     		 "LIMIT 200";

	 public final static String query_200_2a=
			 "SELECT \n"+
			 "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c, ITA_IFM_ConsumptionNumber__c \n"+
			 "FROM Asset \n"+
			 "WHERE \n"+
			 "RecordType.Name = 'Commodity' \n"+
			 "and AccountId IN %s  \n"+
			 "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n"+
			 "GROUP BY AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Company__c, ITA_IFM_ConsumptionNumber__c  ";

	 public final static String query_200_3a =  
   	    		 "SELECT \n"+
   		         "AccountId,name,ProductCode,Account.NE__Fiscal_code__c \n"+
   		         "FROM  \n"+
   	    		 "Asset \n"+
   	    		 "WHERE \n"+
   	    		 "AccountId IN %s  \n"+
   	    	     "AND (NOT NE__ProdId__r.Name like '@PRODOTTO_DEFAULT@') ";

	 public final static String query_200_4a =  
   	    		 "SELECT \n"+
   		         "id, ITA_IFM_Account__r.RecordType.name, ITA_IFM_Account__r.ITA_IFM_Complete_Name__c,  \n"+
   		         "ITA_IFM_Mobile_Phone__c, ITA_IFM_Account__r.NE__Fiscal_code__c,ITA_IFM_Email__c, ITA_IFM_Status__c  \n"+
   		         "FROM  \n"+
   	    		 " ITA_IFM_Data_Contact_Cert_History__c \n"+
   	    		 "WHERE \n"+
   	    		 "ITA_IFM_Status__c = 'CERTIFICATA'  \n"+
   	    		 "and ITA_IFM_Account__r.NE__Fiscal_code__c IN %s  \n"+
   	    	     "LIMIT 100";
	 
	 // Nuova query: "Query 1c" di Raffaella Agosto 2021 ** modifica 6/ottobre 2021
	 public final static String query_200_1c=
	     		 "SELECT id \n"+
	     		 "FROM Account \n"+
	     		 "WHERE ITA_IFM_CustomerType__c = 'Casa' \n"+
	     		 "and Id not IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'VAS' and NE__Status__c ='Active') \n"+
	     		 "and Id IN (SELECT AccountId FROM Asset WHERE recordtype.Name = 'Commodity' and NE__Status__c ='Active' and ITA_IFM_Service_Use__c = 'Uso Abitativo' and CreatedDate > 2020-01-01T00:00:00.000+0000) \n"+
	     		 "LIMIT 100";

	 // Nuova query: "Query 2c" di Raffaella Agosto 2021
	 public final static String query_200_2c=
			 "SELECT \n"+
			 "AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c, ITA_IFM_ConsumptionNumber__c, NE__Status__c, NE__ProdId__r.Name \n"+
			 "FROM Asset \n"+
			 "WHERE \n"+
			 "RecordType.Name = 'Commodity' \n"+
			 "and AccountId IN %s  \n"+
			 "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n"+
			 "AND ITA_IFM_Sap_Contractual_Account__c != '' \n"+
			 "AND ITA_IFM_SAP_Contract__c != '' \n"+
			 "AND (NOT NE__ProdId__r.Name like '*PRODOTTO_DEFAULT*') \n"+
			 "AND NE__Status__c = 'Active'  ";

	 
      		public final static String query_201_extra =
			 "SELECT Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.Name, ITA_IFM_Pod_Pdr__c\n"+
			 "FROM  Asset\n"+
			 "WHERE CreatedDate > 2020-01-01T00:00:00Z\n"+
			 "AND NE__Status__c = 'Active'\n"+
			 "AND RecordType.Name = 'Commodity'\n"+
			 "AND ITA_IFM_Commodity__c = 'ELE'\n"+
			 "AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione'\n"+
			 "AND ITA_IFM_SAP_Contract__c != null\n"+
			 "AND ITA_IFM_Sap_Contractual_Account__c != null\n"+
			 "AND NE_BillingProf_Type__c = 'RID'\n"+
			 "AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('5983','ACEA')\n"+
			 "AND Account.RecordType.Name = 'Residenziale'\n"+
			 "AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')\n"+ 
			 "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\n"+
			 "LIMIT 10";
      		 
      		public final static String query_201 =
      		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, ITA_IFM_Pod_Pdr__c, ITA_IFM_Resident__c \n" +
      		"FROM  Asset \n" +
      		"WHERE CreatedDate > 2020-01-01T00:00:00Z \n" +
      		"AND NE__Status__c = 'Active' \n" +
      		"AND RecordType.Name = 'Commodity' \n" +
      		"AND ITA_IFM_Commodity__c = 'ELE' \n" +
      		"AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' \n" +
      		"AND ITA_IFM_SAP_Contract__c != null \n" +
      		"AND ITA_IFM_Sap_Contractual_Account__c != null \n" +
      		"AND NE_BillingProf_Type__c = 'RID' \n" +
      		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('5983','ACEA') \n" +
      		"AND Account.RecordType.Name = 'Residenziale' \n" +
      		"AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \n" +
      		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
      		"LIMIT 1";
      		 
      		 
    public final static String query_202 =
    		"SELECT Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.Name, ITA_IFM_Pod_Pdr__c\n" +
    		"FROM  Asset\n" +
    		"WHERE CreatedDate > 2020-03-01T00:00:00Z\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND ITA_IFM_Commodity__c = 'GAS'\n" +
    		"AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione'\n" +
    		"AND ITA_IFM_SAP_Contract__c != null\n" +
    		"AND ITA_IFM_Sap_Contractual_Account__c != null\n" +
    		"AND NE_BillingProf_Type__c = 'RID'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('6149','0088','0807')\n" +
    		"AND Account.RecordType.Name = 'Business'\n" +
    		"AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"LIMIT 1";
    
    public final static String query_202_1 =
            "SELECT \n" +
                    "Id \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Impresa Individuale' \n" +
                    "AND NE__Status__c='active' \n" +
                    "and ITA_IFM_Service_Use__c='Uso Diverso da Abitazione' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA' \n" +
                    //"AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c  like 'LD RETI%' \n"+
                    //LD RETI S.R.L., 2i RETE GAS SPA, UNARETI SPA
                    "AND NE_BillingProf_Type__c='RID' \n" +
                    "AND recordtype.Name != 'VAS' \n" +
                    //"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"+
                    "LIMIT 10";
    public final static String query_202_2 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "Account.ITA_IFM_Company__c, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Service_Use__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Id IN %s \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n" +
                    "AND ITA_IFM_Pod_Pdr__c NOT IN ('15810000018100') \n" +
                    "AND ITA_IFM_SAP_Contract__c != null \n" +
                    "AND ITA_IFM_Sap_Contractual_Account__c != null  \n" +
                    "LIMIT 10";
    public final static String query_202_3 = "SELECT AccountId, Account.RecordType.Name, Account.NE__Fiscal_code__c, Account.Name, Account.ITA_IFM_CustomerType__c,\r\n" +
            "RecordType.Name, ITA_IFM_Pod_Pdr__c, ITA_IFM_Commodity__c, NE__Status__c, ITA_IFM_Resident__c, NE_BillingProf_Type__c,\r\n" +
            "NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\r\n" +
            "FROM  Asset\r\n" +
            "WHERE Account.ITA_IFM_CustomerType__c = 'Casa'\r\n" +
            "AND NE__Status__c = 'Active'\r\n" +
            "AND ITA_IFM_Service_Use__c = 'Uso Abitativo'\r\n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n" +
            "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n" +
            "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" +
            "LIMIT 30\r\n" +
            "";
    
    
    public final static String query_203 =
    "SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, ITA_IFM_Pod_Pdr__c, ITA_IFM_Resident__c \n" +
    "FROM  Asset \n" +
    "WHERE CreatedDate > 2020-01-01T00:00:00Z \n" +
    "AND NE__Status__c = 'Active' \n" +
    "AND RecordType.Name = 'Commodity' \n" +
    "AND ITA_IFM_Commodity__c = 'GAS' \n" +
    "AND ITA_IFM_SAP_Contract__c != null \n" +
    "AND ITA_IFM_Sap_Contractual_Account__c != null \n" +
    "AND NE_BillingProf_Type__c = 'RID' \n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('6149','0088','0807') \n" +
    "AND Account.RecordType.Name = 'Residenziale' \n" +
    "AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
    "LIMIT 1";
    
    public final static String query_203_items =

    		"SELECT Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.Name, ITA_IFM_Pod_Pdr__c\n" +
    		"FROM  Asset\n" +
    		"WHERE CreatedDate > 2020-01-01T00:00:00Z\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND ITA_IFM_Commodity__c = 'GAS'\n" +
    		"AND ITA_IFM_SAP_Contract__c != null\n" +
    		"AND ITA_IFM_Sap_Contractual_Account__c != null\n" +
    		"AND NE_BillingProf_Type__c = 'RID'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('6149','0088','0807')\n" +
    		"AND Account.RecordType.Name = 'Residenziale'\n" +
    		"AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')\n" + 
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"LIMIT 10";

    
    
    public final static String query_204_new =
    		"SELECT Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.Name, ITA_IFM_Pod_Pdr__c\n" +
    		"FROM  Asset\n" +
    		"WHERE CreatedDate > 2020-01-01T00:00:00Z\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND ITA_IFM_Commodity__c = 'ELE'\n" +
    		"AND ITA_IFM_Service_Use__c = 'Uso Abitativo'\n" +
    		"AND ITA_IFM_SAP_Contract__c != null\n" +
    		"AND ITA_IFM_Sap_Contractual_Account__c != null\n" +
    		"AND NE_BillingProf_Type__c = 'Bollettino Postale'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('5983','ACEA')\n" +
    		"AND Account.RecordType.Name = 'Residenziale'\n" +
    		"AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"LIMIT 10";
    
        
    public final static String query_204 =

            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='uso abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "and NE_BillingProf_Type__c='Bollettino Postale' \n" +
                    "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n" +
                    "AND ITA_IFM_SAP_Contract__c != null \n" +
                    "AND ITA_IFM_Sap_Contractual_Account__c != null  \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 30 ";
    public final static String query_204_2 =

            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='uso abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "and NE_BillingProf_Type__c='RID' \n" +
                    "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n" +
                    "AND ITA_IFM_SAP_Contract__c != null \n" +
                    "AND ITA_IFM_Sap_Contractual_Account__c != null  \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 30 ";
    public final static String query_204_3 =

            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='uso abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "and NE_BillingProf_Type__c='RID' \n" +
                    "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n" +
                    "AND ITA_IFM_SAP_Contract__c != null \n" +
                    "AND ITA_IFM_Sap_Contractual_Account__c != null  \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 30 ";
    public final static String query_204_4 =
            "SELECT \n" +
                    "id \n" +
                    "FROM   \n" +
                    "Asset  \n" +
                    "WHERE  \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa'  \n" +
                    "AND NE__Status__c='active'  \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
                    "and NE_BillingProf_Type__c='RID'   \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione'   \n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))  \n" +
                    "LIMIT 30";
    public final static String query_204_5 =

            "SELECT   \n" +
                    "Account.NE__Fiscal_code__c,  \n" +
                    "Account.Name,  \n" +
                    "ITA_IFM_Service_Use__c,  \n" +
                    "account.ITA_IFM_CustomerType__c,  \n" +
                    "ITA_IFM_Pod_Pdr__c,  \n" +
                    "ITA_IFM_Commodity__c,  \n" +
                    "NE__Status__c,  \n" +
                    " ITA_IFM_Resident__c  \n" +
                    "FROM    \n" +
                    " Asset   \n" +
                    "WHERE   \n" +
                    "id IN  %s \n" +
                    "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')    \n" +
                    "AND ITA_IFM_SAP_Contract__c != null   \n" +
                    "AND ITA_IFM_Sap_Contractual_Account__c != null   \n" +
                    "LIMIT 30 ";
    public final static String recupera_pod_acea_asset_attivi =
            "SELECT \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='uso abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "LIMIT 1";
    public final static String query_variazione_ind_fatturazione_ele_residenziale = "Select NE__Status__c, NE__StartDate__c, NE__EndDate__c,ITA_IFM_Service_Use__c, Account.Name,  Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c \n" +
            "From Asset  \n" +
            "where NE__Status__c = 'Active' \n" +
            "and Account.RecordType.Name = 'Residenziale' \n" +
            "AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))  \n" +
            "and NE__ProdId__r.Name != 'Bolletta Web' \n" +
            "limit 15";
    public final static String query_variazione_ind_fatturazione_ele_business = "Select NE__Status__c, NE__StartDate__c, NE__EndDate__c,ITA_IFM_Service_Use__c, Account.Name,  Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c \n" +
            "From Asset  \n" +
            "where NE__Status__c = 'Active' \n" +
            "and Account.RecordType.Name = 'Business' \n" +
            "AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' \n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
            "and NE__ProdId__r.Name != 'Bolletta Web' \n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))  \n" +
            "limit 1";
    public final static String query_variazione_ind_fatturaz_gas_business = "Select NE__Status__c, NE__StartDate__c, NE__EndDate__c,ITA_IFM_Service_Use__c, Account.Name,  Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c \n" +
            "From Asset  \n" +
            "where NE__Status__c = 'Active' \n" +
            "and Account.RecordType.Name = 'Business' \n" +
            "and ITA_IFM_Commodity__c = 'GAS' \n" +
            "and NE__ProdId__r.Name != 'Bolletta Web' \n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA' \n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))  \n" +
            "limit 1";
    public final static String query_variazione_ind_fatturaz_gas_residenziale = "Select NE__Status__c, NE__StartDate__c, NE__EndDate__c,ITA_IFM_Service_Use__c, Account.Name,  Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c \n" +
            "From Asset  \n" +
            "where NE__Status__c = 'Active' \n" +
            "and Account.RecordType.Name = 'Residenziale' \n" +
            "and ITA_IFM_Commodity__c = 'GAS' \n" +
            "and NE__ProdId__r.Name != 'Bolletta Web' \n" +
            //"and NE__Service_Point__r.ITA_IFM_POD__c LIKE '5453%' \n"+
            "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA' \n" +
            "limit 5";
    public final static String query_107_2 = "SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Address__c \n" +
            "From Asset  \n" +
            "WHERE CreatedDate > 2019-01-01T00:00:00Z \n" +
            "AND NE__Status__c = 'Active' \n" +
            "AND RecordType.Name = 'Commodity' \n" +
            "AND ITA_IFM_Commodity__c = 'GAS' \n" +
            "AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' \n" +
            "AND ITA_IFM_SAP_Contract__c != null \n" +
            "AND ITA_IFM_Sap_Contractual_Account__c != null \n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = '0301' \n" +
            "AND Account.RecordType.Name != 'Residenziale' \n" +
            "AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
            "limit 1";
    public final static String query_107_3 = "SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_Company__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Address__c \n" +
            "From Asset  \n" +
            "WHERE CreatedDate > 2019-01-01T00:00:00Z \n" +
            "AND NE__Status__c = 'Active' \n" +
            "AND RecordType.Name = 'Commodity' \n" +
            "AND ITA_IFM_Commodity__c = 'GAS' \n" +
            "AND ITA_IFM_SAP_Contract__c != null \n" +
            "AND ITA_IFM_Sap_Contractual_Account__c != null \n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = '0301' \n" +
            "AND Account.RecordType.Name = 'Residenziale' \n" +
            "AND Id IN (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
            "limit 4";
    public final static String query_variazione_ind_fatturaz_gas =
            "Select NE__Status__c, \n" +
                    "NE__StartDate__c, \n" +
                    "NE__EndDate__c,\n" +
                    "ITA_IFM_Service_Use__c, \n" +
                    "Account.Name,  \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.NE__VAT__c, \n" +
                    "Account.RecordType.Name, \n" +
                    "Account.ITA_IFM_Type_Legal_Form__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c, \n" +
                    "ITA_IFM_ConsumptionNumber__c, \n" +
                    "NE__ProdId__r.Name \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "AND Account.RecordType.Name = 'Residenziale' \n" +
                    "AND ITA_IFM_Commodity__c = 'GAS' \n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c LIKE '5453%', \n" +
                    "and NE__ProdId__r.Name != 'Bolletta Web' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('00881407930906') \n" +
                    "AND Account.CreatedDate > 2019-06-01T00:00:00.000+0000 \n" +
                    "LIMIT 10";
    public final static String msr_id1 =
//    		 "SELECT Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c,Account.Name,count(id) \n"+
//    				 "FROM Asset  \n"+
//    				 "WHERE Account.NE__Fiscal_code__c !=''  \n"+
//    				 "and Account.RecordType.Name = 'Residenziale'  \n"+
//    				 "and NE__Status__c = 'Active'  \n"+
//    				 "and RecordType.Name = 'Commodity'  \n"+
//    				 "and ITA_IFM_Commodity__c = 'ELE'  \n"+
//    				 "and CreatedDate > 2019-12-01T00:00:00Z  \n"+
//    				 "AND ITA_IFM_Service_Use__c = 'Uso Abitativo'  \n"+
//    				 "and NE__Service_Point__r.ITA_IFM_FlagResidence__c = false \n"+
//    				 "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))   \n"+
//    				 "AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA')  \n"+
//    	    	     "AND ITA_IFM_SAP_Contract__c != null \n"+
//    	    	     "AND ITA_IFM_Sap_Contractual_Account__c != null  \n"+
//    	    	     "AND Account.ITA_IFM_HolderContact__c != null \n"+
//    				 "GROUP BY Account.NE__Fiscal_code__c,Account.Name,NE__Service_Point__r.ITA_IFM_POD__c  \n"+
//    				 "HAVING count(Id) = 1  \n"+
//    				 "LIMIT 100";
            "SELECT Account.Id, Account.NE__Fiscal_code__c, Account.NE__E_mail__c, Account.ITA_IFM_HolderContactEmail__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\n" +
                    "    FROM Asset\n" +
                    "    WHERE RecordType.Name = 'Commodity'\n" +
                    "    and NE__Status__c = 'Active'\n" +
                    "    and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\n" +
                    "    and ITA_IFM_Commodity__c = 'ELE'\n" +
                    "    and Account.NE__Fiscal_code__c !=''\n" +
                    "    and Account.RecordType.Name = 'Residenziale'\n" +
                    "    and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\n" +
                    "    and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\n" +
                    "    and NE__Service_Point__r.ITA_IFM_FlagResidence__c = false\n" +
                    "    LIMIT 50";
//    public final static String msr_id2 =
//            "SELECT Account.NE__Fiscal_code__c Codice_Fiscale,NE__Service_Point__r.ITA_IFM_POD__c POD, count(id) N_Siti \n" +
//                    "FROM Asset \n" +
//                    "WHERE CreatedDate > 2019-12-01T00:00:00Z \n" +
//                    " AND CreatedDate < 2020-07-01T00:00:00Z \n" +
//                    "and NE__Status__c = 'Active' \n" +
//                    "and RecordType.Name = 'Commodity' \n" +
//                    "and ITA_IFM_Commodity__c = 'ELE' \n" +
//                    "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')  \n" +
//                    "and Account.RecordType.Name = 'Residenziale' \n" +
//                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))  \n" +
//                    "and accountID IN (SELECT NE__Account__c FROM NE__Service_Point__c WHERE ITA_IFM_Commodity_Use__c = 'Uso Abitativo' and ITA_IFM_FlagResidence__c = true) \n" +
//                    "and Account.NE__Fiscal_code__c != null \n" +
//                    "and ITA_IFM_SAP_Contract__c != null \n" +
//                    "and ITA_IFM_Sap_Contractual_Account__c != null \n" +
//                    "GROUP BY Account.NE__Fiscal_code__c,NE__Service_Point__r.ITA_IFM_POD__c \n" +
//                    "HAVING count(Id) = 1 \n" +
//                    "LIMIT 10";
    public final static String msr_id2 ="SELECT Account.NE__Fiscal_code__c Codice_Fiscale, count(id) N_Siti\r\n" + 
    		"FROM Asset\r\n" + 
    		"WHERE CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
    		"AND NE__Status__c = 'Active'\r\n" + 
    		"AND RecordType.Name = 'Commodity'\r\n" + 
    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA') " + 
    		"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
    		"AND ITA_IFM_Sap_Contractual_Account__c != null\r\n" + 
    		"AND Account.RecordType.Name = 'Residenziale'\r\n" + 
    		"AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \r\n" + 
    		"AND ITA_IFM_Resident__c = 'Y'\r\n" + 
    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n" + 
    		"GROUP BY Account.NE__Fiscal_code__c\r\n" + 
    		"HAVING count(Id) = 1\r\n" + 
    		"LIMIT 1";
    public final static String msr_id2_1 ="SELECT Account.RecordType.Name, Account.NE__Fiscal_code__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n" + 
    		"FROM Asset\r\n" + 
    		"WHERE NE__Status__c = 'Active'\r\n" + 
    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
    		"AND RecordType.Name = 'Commodity'\r\n" + 
    		"AND Account.NE__Fiscal_code__c = '@CF@'";
    public final static String msr_id2_2 =
            "Select\r\n" +
                    "NE__Status__c, \r\n" +
                    "ITA_IFM_Commodity__c,\r\n" +
                    "Account.Name, \r\n" +
                    "Account.NE__Fiscal_code__c ,\r\n" +
                    "Account.NE__VAT__c,\r\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c ,\r\n" +
                    "NE__Service_Point__r.ITA_IFM_ZIPCode__c\r\n" +
                    "From Asset \r\n" +
                    "Where \r\n" +
                    "Account.NE__Fiscal_code__c in (\r\n" +
                    "'BRLSDR79T18A509B',\r\n" +
                    "'BRTLNI49E13H735P',\r\n" +
                    "'BRTLNI49E13H735P',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTLNI90P43H501V',\r\n" +
                    "'BRTRBY71A42C543W',\r\n" +
                    "'CCCNNA35E70B354S',\r\n" +
                    "'CHPNGL57S25F839T',\r\n" +
                    "'CLEPRZ52L56H501C',\r\n" +
                    "'CLZGRL86R14D883O',\r\n" +
                    "'CNCMRA74S68A345V',\r\n" +
                    "'CNTNNI87M03F284V',\r\n" +
                    "'CNTNNI87M03F284V',\r\n" +
                    "'CNTNNI87M03F284V',\r\n" +
                    "'CRLDNL61L26E506Z',\r\n" +
                    "'CSSCLL90T41H501H',\r\n" +
                    "'CVDSMN69B45C933A',\r\n" +
                    "'DCLNNL62A52H501B',\r\n" +
                    "'DRBMGR09E57F284G',\r\n" +
                    "'DRBMGR09E57F284G',\r\n" +
                    "'DRBNLS87L68F284L',\r\n" +
                    "'DRSLRT62A02Z133C',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DTZRST33L18G482D',\r\n" +
                    "'DVCVNT71D68H501H',\r\n" +
                    "'DVCVNT71D68H501H',\r\n" +
                    "'FBRMRN80R41H502H',\r\n" +
                    "'FGLLRD56T46E974I',\r\n" +
                    "'FRNLRI88D63F284V',\r\n" +
                    "'GJRDND78E12Z100U',\r\n" +
                    "'GLDGCM00B15F839P',\r\n" +
                    "'GSTVNT70R16B157P',\r\n" +
                    "'LGNLGU36T16F205M',\r\n" +
                    "'LLVGNN49D07B448J',\r\n" +
                    "'LSINLT91E63I725F',\r\n" +
                    "'LSINLT91E63I725F',\r\n" +
                    "'LSINLT91E63I725F',\r\n" +
                    "'LSINLT91E63I725F',\r\n" +
                    "'LVGLNR86L43E958V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MCCFRC91P54H501V',\r\n" +
                    "'MDGGVR80C59H511K',\r\n" +
                    "'MDGGVR80C59H511K',\r\n" +
                    "'MDGGVR80C59H511K',\r\n" +
                    "'MDGGVR80C59H511K',\r\n" +
                    "'MGLNLS88C41B872B',\r\n" +
                    "'MGLNLS88C41B872B',\r\n" +
                    "'MGNLSN72A01F205T',\r\n" +
                    "'MLLMTR80A41H224T',\r\n" +
                    "'MLLMTR80A41H224T',\r\n" +
                    "'MROGNN80E01C662L',\r\n" +
                    "'MRSLFA52R31C743X',\r\n" +
                    "'MRZGLN48T46A944J',\r\n" +
                    "'MSTSMN88P28H501O',\r\n" +
                    "'MSTSMN88P28H501O',\r\n" +
                    "'NPLGNZ33R01E236F',\r\n" +
                    "'NRCRMB70E01H501U',\r\n" +
                    "'NXTMRA01S28H501C',\r\n" +
                    "'NXTMRA01S28H501C',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLLPNC02D04H501Q',\r\n" +
                    "'PLMMRS89R54G942A',\r\n" +
                    "'PLMMRS89R54G942A',\r\n" +
                    "'PNGPNG80H01H501Y',\r\n" +
                    "'PNGPNG80H01H501Y',\r\n" +
                    "'PPAGNN22C26A366L',\r\n" +
                    "'PPPTMS76D24F839J',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRCMRC89A01F205V',\r\n" +
                    "'PRTMNL54R65H501L',\r\n" +
                    "'PTTHRY85T19H224C',\r\n" +
                    "'PTTHRY85T19H224C',\r\n" +
                    "'PTTHRY85T19H224C',\r\n" +
                    "'RBRNNL88E61L628U',\r\n" +
                    "'RGIGDI88R53L483G',\r\n" +
                    "'RNLFRC88M55D912D',\r\n" +
                    "'RSSMRA39L17C774B',\r\n" +
                    "'RSTMCL74R28L219A',\r\n" +
                    "'RSTMCL74R28L219A',\r\n" +
                    "'RSTMCL74R28L219A',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBTMRS77E54B936W',\r\n" +
                    "'SBUCBN00M15F205W',\r\n" +
                    "'SCGCLD97H41F839F',\r\n" +
                    "'SCGVTR71B43F839C',\r\n" +
                    "'SCNBRS60H08H501C',\r\n" +
                    "'SCNBRS60H08H501C',\r\n" +
                    "'SCNBRS60H08H501C',\r\n" +
                    "'SCNBRS60H08H501C',\r\n" +
                    "'SDDCRN73H57I452B',\r\n" +
                    "'STRJOE70A01F205A',\r\n" +
                    "'STRJOE70A01F205A',\r\n" +
                    "'STRJOE70A01F205A',\r\n" +
                    "'STRJOE70A01F205A',\r\n" +
                    "'STRJOE70A01F205A',\r\n" +
                    "'STRJOE70A01F205A',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SVSGNR88C01B872W',\r\n" +
                    "'SWANRT80A01G791X',\r\n" +
                    "'TRTRGU89S58H501Z',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC40A07F083S',\r\n" +
                    "'TTTFNC76P27H501J',\r\n" +
                    "'TTTFNC76P27H501J',\r\n" +
                    "'TTTFNC76P27H501J',\r\n" +
                    "'TTTFNC76P27H501J',\r\n" +
                    "'VSLCNL80B63Z129P',\r\n" +
                    "'VSLCNL80B63Z129P'\r\n" +
                    ") and AccountId not IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n" +
                    "";
  /*  public final static String msr_id3 =
            "SELECT Account.NE__Fiscal_code__c Codice_Fiscale, count(id) N_Siti \n" +
                    "FROM Asset \n" +
                    "WHERE CreatedDate > 2019-12-01T00:00:00Z \n" +
                    " AND CreatedDate < 2020-07-01T00:00:00Z \n" +
                    "and NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and ITA_IFM_Commodity__c = 'ELE' \n" +
                    "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')  \n" +
                    "and Account.RecordType.Name = 'Residenziale' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))  \n" +
                    "and accountID IN (SELECT NE__Account__c FROM NE__Service_Point__c WHERE ITA_IFM_Commodity_Use__c = 'Uso Abitativo' and ITA_IFM_FlagResidence__c = false) \n" +
                    "and Account.NE__Fiscal_code__c != null \n" +
                    "and ITA_IFM_SAP_Contract__c != null \n" +
                    "and ITA_IFM_Sap_Contractual_Account__c != null \n" +
                    "GROUP BY Account.NE__Fiscal_code__c \n" +
                    "HAVING count(Id) > 1 \n" +
                    "LIMIT 10";*/
    public final static String msr_id3 ="SELECT Account.NE__Fiscal_code__c Codice_Fiscale, count(id) N_Siti\r\n" + 
    		"FROM Asset\r\n" + 
    		"WHERE CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
    		"AND NE__Status__c = 'Active'\r\n" + 
    		"AND RecordType.Name = 'Commodity'\r\n" + 
    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\r\n" + 
    		"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
    		"AND ITA_IFM_Sap_Contractual_Account__c != null\r\n" + 
    		"AND Account.RecordType.Name = 'Residenziale'\r\n" + 
    		"AND AccountId IN (SELECT NE__Account__c FROM NE__Service_Point__c WHERE ITA_IFM_Commodity_Use__c = 'Uso Abitativo' and ITA_IFM_FlagResidence__c = true)\r\n" + 
    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n" + 
    		"AND Account.NE__Fiscal_code__c NOT IN ('FRNGLI99A70A354Y') "+
    		"GROUP BY Account.NE__Fiscal_code__c\r\n" + 
    		"HAVING count(Id) > 1\r\n" + 
    		"LIMIT 1";
    public final static String msr_id3_1 ="SELECT Account.RecordType.Name, Account.NE__Fiscal_code__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c " + 
    		"FROM Asset\r\n" + 
    		"WHERE NE__Status__c = 'Active'\r\n" + 
    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
    		"AND RecordType.Name = 'Commodity'\r\n" + 
    		"AND Account.NE__Fiscal_code__c = '@CF@'";
    public final static String msr_id4 =
//            "SELECT Account.NE__Fiscal_code__c Codice_Fiscale, count(id) N_Siti \n" +
//                    "FROM Asset \n" +
//                    "WHERE CreatedDate > 2019-12-01T00:00:00Z \n" +
//                    " AND CreatedDate < 2020-07-01T00:00:00Z \n" +
//                    "and NE__Status__c = 'Active' \n" +
//                    "and RecordType.Name = 'Commodity' \n" +
//                    "and ITA_IFM_Commodity__c = 'ELE' \n" +
//                    "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')  \n" +
//                    "and Account.RecordType.Name = 'Residenziale' \n" +
//                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working') OR ITA_IFM_OperationStatus__c IN ('In Attesa'))  \n" +
//                    "and accountID IN (SELECT NE__Account__c FROM NE__Service_Point__c WHERE ITA_IFM_Commodity_Use__c = 'Uso Abitativo' and ITA_IFM_FlagResidence__c = true) \n" +
//                    "and Account.NE__Fiscal_code__c != null \n" +
//                    "and ITA_IFM_SAP_Contract__c != null \n" +
//                    "and ITA_IFM_Sap_Contractual_Account__c != null \n" +
//                    "GROUP BY Account.NE__Fiscal_code__c \n" +
//                    "HAVING count(Id) > 1 \n" +
//                    "LIMIT 10";
    		"SELECT Account.Id, Account.NE__Fiscal_code__c, Account.NE__E_mail__c, Account.ITA_IFM_HolderContactEmail__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\n" +
                    "FROM Asset\n" +
                    "WHERE RecordType.Name = 'Commodity'\n" +
                    "and NE__Status__c = 'Active'\n" +
                    "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA')\n" +
                    "and ITA_IFM_Commodity__c = 'ELE'\n" +
                    "and Account.NE__Fiscal_code__c !=''\n" +
                    "and Account.RecordType.Name = 'Residenziale'\n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Bozza'))\n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','bozza'))\n" +
                    "and NE__Service_Point__r.ITA_IFM_FlagResidence__c = false\n" +
                    "LIMIT 10\n";
    public final static String msr_id4_1 ="SELECT Account.RecordType.Name, Account.NE__Fiscal_code__c, NE__Status__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n" + 
    		"FROM Asset\r\n" + 
    		"WHERE NE__Status__c = 'Active'\r\n" + 
    		"AND ITA_IFM_Commodity__c = 'ELE'\r\n" + 
    		"AND RecordType.Name = 'Commodity'\r\n" + 
    		"AND Account.NE__Fiscal_code__c = '@CF@'";
    
    //  #Residenziale,Impresa Individuale,Condominio,Business
    public final static String recupera_cliente_senza_operazioni_in_corso = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and RecordType.Name = 'Commodity' \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Preventivo richiesto')) \n"
            + "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n"
            + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
            + "and Account.NE__Fiscal_code__c !='' \n"
            + "LIMIT 1";
    //  #Residenziale,Impresa Individuale,Condominio,Business
    public final static String recupera_cliente_senza_operazioni_in_corso2 = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and RecordType.Name = 'Commodity' \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Preventivo richiesto') and ITA_IFM_Status__c IN ('Working')) \n"
            + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
            + "and Account.NE__Fiscal_code__c !='' \n"
            + "and Account.NE__Fiscal_code__c not IN ('MRZRFL69H15B639M','FRRDLA72P55G230W','RFFPRI40D08L219T') LIMIT 1";
    public final static String recupera_CF_CopiaDocumenti_id14 =
            "SELECT  \n" +
                    "ITA_IFM_Account__c, ITA_IFM_Account__r.Name, ITA_IFM_Account__r.NE__Fiscal_code__c, ITA_IFM_Modello__c  \n" +
                    "FROM  \n" +
                    "ITA_IFM_Document__c  \n" +
                    "WHERE  \n" +
                    "CreatedDate  > 2019-06-01T00:00:00.000+0000  \n";
    public final static String recupera_dati_cliente_da_casenumber =
            "SELECT \n" +
                    "Account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c , Account.NE__Fiscal_code__c, Account.ITA_IFM_HolderContact__r.Name, Account.ITA_IFM_Company__c \n" +
                    "FROM Case \n" +
                    "WHERE \n" +
                    "CaseNumber = '%s' ";
    //  #Residenziale,Impresa Individuale,Condominio,Business
    public final static String recupera_cliente_senza_operazioni_in_corso4 = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and RecordType.Name = 'Commodity' \n"
            + "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Preventivo richiesto') or ITA_IFM_Status__c IN ('Working')) \n"
            + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
            + "and Account.NE__Fiscal_code__c not IN ('MRZRFL69H15B639M','FRRDLA72P55G230W','RFFPRI40D08L219T') LIMIT 10";
    //  #Residenziale,Impresa Individuale,Condominio,Business
    public final static String recupera_cliente_uso_diverso_da_abitazione_senza_operazioni_in_corso = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and RecordType.Name = 'Commodity' \n"
            + " and NE__Service_Point__r.ITA_IFM_Commodity_Use__c = 'Uso Diverso da Abitazione' \n"
            + "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Preventivo richiesto') and ITA_IFM_Status__c IN ('Working')) \n"
            + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
            + "and Account.NE__Fiscal_code__c not IN ('MRZRFL69H15B639M','FRRDLA72P55G230W','RFFPRI40D08L219T') LIMIT 10";
    //  #Residenziale,Impresa Individuale,Condominio,Business
    public final static String recupera_cliente_uso_abitativo_senza_operazioni_in_corso = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name, Account.RecordType.Name, NE__Service_Point__r.ITA_IFM_POD__c \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and RecordType.Name = 'Commodity' \n"
            + " and NE__Service_Point__r.ITA_IFM_Commodity_Use__c = 'Uso Abitativo' \n"
            + "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Preventivo richiesto') and ITA_IFM_Status__c IN ('Working')) \n"
            + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
            + "and Account.NE__Fiscal_code__c not IN ('MRZRFL69H15B639M','FRRDLA72P55G230W','RFFPRI40D08L219T') LIMIT 10";
    //  #Residenziale,Impresa Individuale,Condominio,Business
    public final static String recupera_cliente_senza_operazioni_in_corso_pod_non_telegestito = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and RecordType.Name = 'Commodity' \n"
            + "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n"
            + "and  NE__Service_Point__r.ITA_IFM_Distributor_Telemanagement__c = false \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa','Preventivo richiesto') and ITA_IFM_Status__c IN ('Working')) \n"
            + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
            + "and Account.NE__Fiscal_code__c !='' \n"
            + "and Account.NE__Fiscal_code__c not IN ('MRZRFL69H15B639M','FRRDLA72P55G230W','RFFPRI40D08L219T') LIMIT 10";
    // @  ELE, GAS
    // #  Residenziale, Impresa Individuale, Condominio
    public final static String recupera_pod_senza_operazioni_in_corso = "SELECT \n" +
            "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
            "FROM Asset \n" +
            "WHERE NE__Status__c = 'Active' \n" +
            "and RecordType.Name = 'Commodity' \n" +
            "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
            "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n" +
            "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n" +
            "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
            "and NE__Service_Point__r.ITA_IFM_City__c='NAPOLI' \n" +
            "and NE__Service_Point__r.ITA_IFM_ZIPCode__c='80129' \n" +
            "LIMIT 1";
    //@ ELE,GAS
    // #  Residenziale, Business
    public final static String recupera_pod_senza_operazioni_2 =
			/*
			"SELECT \n"+
			"Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n"+
			"FROM Asset \n"+
			"WHERE NE__Status__c = 'Active' \n"+
			"and RecordType.Name = 'Commodity' \n"+
			"and Account.NE__Fiscal_code__c !='' \n"+
			"and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c NOT IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa', 'Working') AND ITA_IFM_Status__c IN ('Closed')) \n"+
//			"and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n"+
			"and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n"+
			"and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002%A'"+
			"and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"+
			"and Account.ITA_IFM_Entitled_Market__c = 'Maggior Tutela' \n"+
			"and ITA_IFM_PreviousOrder__c not in ( 'ATTIVAZIONE VAS', 'VARIAZIONE VAS') \n"+
			"LIMIT 8";
			*/

            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa')) \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n" +
                    "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
                    "and Account.ITA_IFM_Entitled_Market__c = 'Maggior Tutela' \n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n" +
                    "and ITA_IFM_PreviousOrder__c not in ( 'ATTIVAZIONE VAS', 'VARIAZIONE VAS') \n" +
                    "LIMIT 1";
    //@ ELE,GAS
    // #  Residenziale, Business
    public final static String recupera_pod_senza_operazioni_3 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n" +
                    "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
                    "and Account.ITA_IFM_Entitled_Market__c = 'Maggior Tutela' \n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n" +
                    "and ITA_IFM_PreviousOrder__c not in ( 'ATTIVAZIONE VAS', 'VARIAZIONE VAS' ,null) \n" +
                    "LIMIT 2 ";
    //@ ELE,GAS
    // #  Residenziale, Business
    public final static String recupera_pod_con_operazioni_3 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n" +
                    "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
                    "LIMIT 1";
			/*
			"SELECT \n"+
			"Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n"+
			"FROM Asset \n"+
			"WHERE NE__Status__c = 'Active' \n"+
			"and RecordType.Name = 'Commodity' \n"+
			"and Account.NE__Fiscal_code__c !='' \n"+
			"and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c NOT IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa') AND ITA_IFM_Status__c IN ('Closed')) \n"+
			"and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n"+
			"and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"+
			"and Account.ITA_IFM_Entitled_Market__c = 'Maggior Tutela' \n"+
			"and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n"+
			"and ITA_IFM_PreviousOrder__c not in ( 'ATTIVAZIONE VAS', 'VARIAZIONE VAS' ,null) \n"+
			"LIMIT 20 ";
			 */
    public final static String recupera_pod_con_operazioni_bollettino_postale_ele =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa')) \n" +
                    //"and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n" +
                    "and ITA_IFM_Commodity__c = 'ELE' \n" +
                    "and Account.RecordType.Name = 'Residenziale' \n" +
                    "and NE_BillingProf_Type__c = 'Bollettino Postale' \n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002E%A' \n" +
                    "LIMIT 5";
    public final static String recupera_pod_con_operazioni_bollettino_postale_gas =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa')) \n" +
                    "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n" +
                    "and ITA_IFM_Commodity__c = 'GAS' \n" +
                    "and Account.RecordType.Name = 'Residenziale' \n" +
                    "and NE_BillingProf_Type__c = 'Bollettino Postale' \n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like '0526%9' \n" +
                    "LIMIT 5";
    public final static String recupera_pod_con_operazioni_bollettino_postale_rid =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa')) \n" +
                    "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n" +
                    "and ITA_IFM_Commodity__c = 'GAS' \n" +
                    "and Account.RecordType.Name = 'Residenziale' \n" +
                    "and NE_BillingProf_Type__c = 'RID' \n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like '0526%9' \n" +
                    "LIMIT 5";
    //@ ELE,GAS
    // #  Residenziale, Business
    public final static String recupera_pod_con_operazioni_in_working =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n" +
                    "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
                    "LIMIT 1";
    //@ RES, NON_RES
    public final static String recupera_pod_senza_operazioni_res_non_res = "Select\n" +
            "NE__StartDate__c,\n" +
            "ITA_IFM_StartDate_Forma_Contrattuale__c,\n" +
            "RecordType.name,\n" +
            "id,\n" +
            "NE__Status__c,\n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c,\n" +
            "ITA_IFM_Service_Use__c,\n" +
            "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
            "NE__Service_Point__c,\n" +
            "ITA_IFM_ConsumptionNumber__c,\n" +
            "NE__ProdId__r.Name,\n" +
            "ITA_IFM_External_Item_Name__c,\n" +
            "NE__BillingProf__c,\n" +
            "NE_BillingProf_Type__c,\n" +
            "parentId,\n" +
            "ITA_IFM_SAP_Account_BP__c, \n" +
            "ITA_IFM_SAP_Contract__c, \n" +
            "ITA_IFM_Sap_Contractual_Account__c\n" +
            "From Asset \n" +
            "where RecordType.Name = 'Commodity' \n" +
            "and NE__Status__c = 'Active'\n" +
            "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','In Attesa','Preventivo richiesto'))\n" +
            "and AccountId IN (select id from account where ITA_IFM_Account_Type__c = '@')\n" +
            "and ITA_IFM_Commodity__c ='ELE'\n" +
            "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002%A'" +
            "and ITA_IFM_SAP_Contract__c <> null\n" +
            "limit 10";
    public final static String recupera_pod_per_tipologia_bolletta = "SELECT \n"
            + "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" + "FROM Asset \n"
            + "WHERE NE__Status__c = 'Active' \n" + "and RecordType.Name = 'Commodity' \n"
            + "and ITA_IFM_Commodity__c = '@COMMODITY@' \n"
            + "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n"
            + "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n"
            + "and ITA_IFM_Invoice_Typology_Formula__c = '@TIPOLOGIA_BOLLETTA@' \n" + "LIMIT 15";
    /**
     * @TIPO_COMMODITY@ --> ELE,GAS
     * @TIPO_CLIENTE@ --> Business,Residenziale, Impresa Individuale
     */
    public final static String recupera_pod_senza_preventivi_pending =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n" +
                    "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
//			"and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c NOT IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa') AND ITA_IFM_Status__c NOT IN ('Closed'))\n"+
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa') )\n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n" +
                    "LIMIT 5 ";
    public final static String recupera_pod_senza_preventivi_pending2 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset \n" +
                    "WHERE NE__Status__c = 'Active' \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and Account.NE__Fiscal_code__c !='' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('Preventivo richiesto', 'Preventivo inviato', 'Richiesto lavoro', 'In Attesa')) \n" +
                    "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Closed')) \n" +
                    "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n" +
                    "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n" +
                    " and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n" +
                    "LIMIT 5";
    public final static String recupera_pod_attivi_con_mpd =
            "SELECT Account.NE__Fiscal_code__c, ITA_IFM_Pod_Pdr__c \n"
                    + "from Asset where \n"
                    + "NE_BillingProf_Type__c='@TIPOLOGIA_BOLLETTA@' \n"
                    + "and NE__Status__c='Active' \n"
                    + "and Account.NE__Fiscal_code__c !='' \n"
                    + "and ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n"
                    + "and Account.RecordType.Name = '@TIPO_CLIENTE@' \n"
                    + "LIMIT 1";
    /**
     * @TIPO_COMMODITY@ --> ELE,GAS
     * @TIPO_MERCATO@ --> Maggior Tutela, Salvaguardia
     */
    public final static String recupera_pod_attivi_rettifiche_mercato =
            "SELECT \n"
                    + "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n"
                    + "FROM Asset \n"
                    + " WHERE \n"
                    + " NE__Status__c IN ('Active') \n"
                    + " AND RecordType.Name = 'Commodity' \n"
                    + " AND NE__Service_Point__r.ITA_IFM_Commodity_Use__c = 'Uso Diverso da Abitazione' \n"
                    + " AND ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n"
                    + " AND Account.NE__VAT__c != null \n"
                    + " AND Account.NE__Fiscal_code__c != null  \n"
                    + " AND Account.ITA_IFM_Entitled_Market__c != null \n"
                    + " and Account.ITA_IFM_Entitled_Market__c = '@TIPO_MERCATO@' \n"
                    + " and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n"
                    + " and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"
                    + " LIMIT 1";
    public final static String recupera_pod_attivi_rettifiche_mercato2 =
            "SELECT \n"
                    + "Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c \n"
                    + "FROM Asset \n"
                    + " WHERE \n"
                    + " NE__Status__c IN ('Active') \n"
                    + " AND RecordType.Name = 'Commodity' \n"
                    + " AND NE__Service_Point__r.ITA_IFM_Commodity_Use__c = 'Uso Diverso da Abitazione' \n"
                    + " AND ITA_IFM_Commodity__c = '@TIPO_COMMODITY@' \n"
//					+" AND Account.NE__VAT__c != null \n"
//					+" AND Account.NE__Fiscal_code__c != null  \n"
//					+" AND Account.ITA_IFM_Entitled_Market__c != null \n"
                    + " and Account.ITA_IFM_Entitled_Market__c = '@TIPO_MERCATO@' \n"
                    + " and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c NOT IN ('In Attesa') AND ITA_IFM_Status__c NOT IN ('Working')) \n"
                    //+" and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c NOT IN ('Working')) \n"
                    + " and NE__Service_Point__r.ITA_IFM_POD__c like '@FAMIGLIA_POD@' \n"
                    + " LIMIT 15";
    public final static String recupera_cliente_residenziale_contatti_valorizzati =
            "SELECT Id, Name, NE__VAT__c, NE__Fiscal_code__c, Type, ITA_IFM_CustomerType__c,ITA_IFM_HolderContactEmail__c,ITA_IFM_HolderContactMobilePhone__c \n" +
                    "FROM Account  \n" +
                    "where Type='RESIDENZIALE' \n" +
                    "and  ITA_IFM_HolderContactEmail__c != null \n" +
                    "and ITA_IFM_HolderContactMobilePhone__c!= null  \n" +
                    "LIMIT 50";
    public final static String cliente_pubblica_amministrazione =
            "SELECT  \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset   \n" +
                    "WHERE RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'  \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c \n" +
                    "WHERE ITA_IFM_Status__c IN ('Working','In Attesa','Preventivo richiesto')) \n" +
                    "AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione'\n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002%A'" +
                    "AND ITA_IFM_SAP_Contract__c <> null \n" +
                    "and NE__Status__c = 'Active'\n" +
                    "LIMIT 2";
    public final static String cliente_pubblica_amministrazione_senzafepa =
            "SELECT  \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset   \n" +
                    "WHERE RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'  \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c \n" +
                    "WHERE ITA_IFM_Status__c IN ('Working','In Attesa','Preventivo richiesto')) \n" +
                    "AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione'\n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002%A'" +
                    "AND ITA_IFM_SAP_Contract__c <> null \n" +
                    "and NE__Status__c = 'Active'\n" +
                    "and ITA_IFM_FEPAIsActive__c != 'SI'" +
                    "LIMIT 2";
    public final static String cliente_pubblica_amministrazione_confepa =
            "SELECT  \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c \n" +
                    "FROM Asset   \n" +
                    "WHERE RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'  \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c \n" +
                    "WHERE ITA_IFM_Status__c IN ('Working','In Attesa','Preventivo richiesto')) \n" +
                    "AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione'\n" +
                    "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002%A'" +
                    "AND ITA_IFM_SAP_Contract__c <> null \n" +
                    "and NE__Status__c = 'Active'\n" +
                    "and ITA_IFM_FEPAIsActive__c = 'SI'" +
                    "LIMIT 2";
    public final static String aggiorna_data_ripensamento_SWA =
            "SELECT Id \n"
                    + "FROM NE__OrderItem__c \n"
                    + "WHERE \n"
                    + "name ='@ID_OFFERTA_CRM@' \n";
    public final static String aggiorna_cluster_SWA =
            "SELECT Id \n"
                    + "FROM ITA_IFM_Order_Management_Log__c \n"
                    + "WHERE \n"
                    + "ITA_IFM_OrderHeader__r.Name ='@ORH_ORDINE@' \n";
    public final static String recupera_cliente_non_res_bonus_attivi = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name \n, ITA_IFM_Pod_Pdr__c \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and ITA_IFM_Pod_Pdr__c LIKE 'IT002%A'"
            + "and Account.ITA_IFM_Account_Type__c= 'NON RES' \n"
            + "and RecordTypeId = '01224000000FLvoAAG' \n"
            + "LIMIT 1";
    public final static String recupera_cliente_res_bonus_attivi = "SELECT \n"
            + "Account.NE__Fiscal_code__c, Account.Name \n, ITA_IFM_Pod_Pdr__c \n" + "FROM Asset \n" + "WHERE NE__Status__c = 'Active' \n"
            + "and ITA_IFM_Pod_Pdr__c LIKE 'IT002%A'"
            + "and Account.ITA_IFM_Account_Type__c= 'RES' \n"
            + "and RecordTypeId = '01224000000FLvoAAG' \n"
            + "LIMIT 1";
    public final static String recupera_pod_senza_operazioni_res_non_res_fepa = "Select\n" +
            "NE__StartDate__c,\n" +
            "ITA_IFM_StartDate_Forma_Contrattuale__c,\n" +
            "RecordType.name,\n" +
            "id,\n" +
            "NE__Status__c,\n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c,\n" +
            "ITA_IFM_Service_Use__c,\n" +
            "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
            "NE__Service_Point__c,\n" +
            "ITA_IFM_ConsumptionNumber__c,\n" +
            "NE__ProdId__r.Name,\n" +
            "ITA_IFM_External_Item_Name__c,\n" +
            "NE__BillingProf__c,\n" +
            "NE_BillingProf_Type__c,\n" +
            "parentId,\n" +
            "ITA_IFM_SAP_Account_BP__c, \n" +
            "ITA_IFM_SAP_Contract__c, \n" +
            "ITA_IFM_Sap_Contractual_Account__c\n" +
            "From Asset \n" +
            "where RecordType.Name = 'Commodity' \n" +
            "and NE__Status__c = 'Active'\n" +
            "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working','In Attesa','Preventivo richiesto'))\n" +
            "and AccountId IN (select id from account where ITA_IFM_Account_Type__c = '@')\n" +
            "and ITA_IFM_Commodity__c ='ELE'\n" +
            "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002%A'" +
            "and ITA_IFM_SAP_Contract__c <> null\n" +
            "and ITA_IFM_FEPAIsActive__c = '#'" +
            "limit 10";
    public final static String recupera_pod_con_bonus_attivi = "Select \n" +
            "id, \n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c, \n" +
            "ITA_IFM_Commodity__c, \n" +
            "ITA_IFM_Pod_Pdr__c \n" +
            "From Asset \n" +
            "WHERE NE__Status__c = 'Active' \n" +
            "and NE__ProdId__r.Name <>'Placet BUS Gas Indicizzato' \n" +
            "and AccountId IN (select id from account where ITA_IFM_Account_Type__c = '@R')\n" +
            "and NE__ProdId__r.Name = 'Bonus 50 euro per 5 mesi' \n" +
            "limit 10";
    public final static String recupera_pod_non_res_con_bonus_attivi = "Select \n" +
            "id, \n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c, \n" +
            "ITA_IFM_Commodity__c, \n" +
            "ITA_IFM_Pod_Pdr__c \n" +
            "From Asset \n" +
            "WHERE NE__Status__c = 'Active' \n" +
            "and NE__ProdId__r.Name <>'Placet BUS Gas Indicizzato' \n" +
            "and AccountId IN (select id from account where ITA_IFM_Account_Type__c = '@R')\n" +
            "and NE__ProdId__r.Name = 'Bonus LuceOK'  \n" +
            "limit 10";
    public final static String recupera_pod_res_senza_bonus = "Select \n" +
            "id, \n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c, \n" +
            "ITA_IFM_Commodity__c, \n" +
            "ITA_IFM_Pod_Pdr__c \n" +
            "From Asset \n" +
            "WHERE NE__Status__c = 'Active' \n" +
            "and NE__ProdId__r.Name='SEMPLICE LUCE' \n" +
            "and AccountId IN (select id from account where ITA_IFM_Account_Type__c = 'RES' and ITA_IFM_HolderContactEmail__c <> NULL and NE__E_mail__c <> NULL)\n" +
            "limit 5";
    public final static String recupera_pod_non_res_senza_bonus = "Select \n" +
            "id, \n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c, \n" +
            "NE__ProdId__r.Name,\n" +
            "ITA_IFM_Commodity__c, \n" +
            "ITA_IFM_Pod_Pdr__c \n" +
            "From Asset \n" +
            "WHERE NE__Status__c = 'Active' \n" +
            "and Account.ITA_IFM_CustomerType__c <> 'Pubblica Amministrazione' \n" +
            "and NE__ProdId__r.Name='SEMPLICE LUCE' \n" +
            "and AccountId IN (select id from account where ITA_IFM_Account_Type__c = 'NON RES' and ITA_IFM_HolderContactEmail__c <> NULL and NE__E_mail__c <> NULL)\n" +
            "limit 3";
    public final static String get_business_account_info = "Select Account.Name, Account.NE__E_mail__c,\n" +
            "Account.ITA_IFM_PEC__c, Account.Phone, Account.NE__Fiscal_code__c\n" +
            "from Asset\n" +
            "Where AccountId = '@ACCOUNTID@'";
    public final static String get_email_from_accountid = "select ITA_IFM_HolderContactEmail__c\n" +
            "from account \n" +
            "where id = '@ACCOUNTID@'";
    public final static String get_email_from_cf = "select ITA_IFM_HolderContactEmail__c " +
            "from account \n" +
            "where Account.NE__Fiscal_code__c  = '@COD_FISCALE@'";
    public final static String get_cf_for_attiva_bolletta_web = "SELECT Account.NE__Fiscal_code__c, count(id)\n" +
            "FROM Asset\n" +
            "WHERE Account.NE__Fiscal_code__c !=''\n" +
            "and Account.RecordType.Name ='Residenziale'\n" +
            "and NE__Status__c = 'Active'\n" +
            "and RecordType.Name = 'Commodity'\n" +
            "and CreatedDate > 2019-12-01T00:00:00Z\n" +
            "and RecordType.Name NOT IN ('VAS')\n" +
            "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c\n" +
            "WHERE ITA_IFM_Status__c !='In Attesa'\n" +
            "and ITA_IFM_Status__c !='Working'\n" +
            "and ITA_IFM_OperationStatus__c != 'In attesa'\n" +
            "and ITA_IFM_OperationStatus__c != 'working')\n" +
            "GROUP BY Account.NE__Fiscal_code__c\n" +
            "HAVING count(Id) > 1\n" +
            "LIMIT 150 OFFSET 20";
    public final static String get_cf_for_modifica_bolletta_web = "SELECT Account.NE__Fiscal_code__c, count(id) \n" +
            "FROM Asset \n" +
            "WHERE Account.NE__Fiscal_code__c !='' \n" +
            " and Account.RecordType.Name ='Residenziale' \n" +
            " and NE__Status__c = 'Active' \n" +
            " and RecordType.Name = 'Commodity' \n" +
            " and CreatedDate > 2019-12-01T00:00:00Z \n" +
//			" and RecordType.Name NOT IN ('VAS') \n"+
            " and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c NOT IN ('In Attesa','Working')) \n" +
            "GROUP BY Account.NE__Fiscal_code__c \n" +
            "HAVING count(Id) = 1 \n" +
            "LIMIT 150 OFFSET 160";
    //74
    public final static String get_query_79 = "Select \n" +
            "account.Name, \n" +
            "Account.NE__Fiscal_code__c,\n" +
            "Account.NE__VAT__c, \n" +
            "Account.RecordType.Name,\n" +
            "Account.ITA_IFM_Type_Legal_Form__c,\n" +
            "ITA_IFM_Commodity__c," +
            "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
            "ITA_IFM_ConsumptionNumber__c,\n" +
            "NE__ProdId__r.Name,\n" +
            "ITA_IFM_BillingAddress__c \n" +
            "FROM \n" +
            "asset \n" +
            "WHERE \n" +
            "ITA_IFM_ConsumptionNumber__c = '$CUSTOMER_NUMBER$'";
    public final static String get_query_163a = "select \n" +
            "AccountId, \n" +
            "ITA_IFM_CreatedDate__c , \n" +
            "Priority, \n" +
            "Status,\n" +
            "Origin, \n" +
            "Subject, \n" +
            "ITA_IFM_SubStatus__c \n" +
            "from \n" +
            "case \n" +
            "where \n" +
            "AccountId = '0010Y00000FMWwtQAH' \n" +
            "and \n" +
            "ITA_IFM_CreatedDate__c > $TODAY$ \n" +
            "and \n" +
            "Subject = 'GESTIONE PRIVACY' \n" +
            "order by \n" +
            "ITA_IFM_CreatedDate__c \n" +
            "desc";


    public final static String get_query_163b = "Select \n" +
            "ITA_IFM_Account__c, \n" +
            "ITA_IFM_CreationDate__c, \n" +
            "ITA_IFM_Phone__c, \n" +
            "ITA_IFM_MarketingConsentPhone__c, \n" +
            "ITA_IFM_ThirdPartyConsentPhone__c, \n" +
            "ITA_IFM_Email__c, \n" +
            "ITA_IFM_MarketingConsentEmail__c ,\n" +
            "ITA_IFM_ThirdPartyConsentEmail__c \n" +
            "from \n" +
            "ITA_IFM_Privacy__c \n" +
            "where \n" +
            "ITA_IFM_Account__c ='$ACCOUNT_ID$'\n" +
            "and \n" +
            "ITA_IFM_CreationDate__c = TODAY \n" +
            "order by \n" +
            "ITA_IFM_CreationDate__c \n" +
            "desc";
    public final static String get_query_164a = "select \n" +
            "AccountId, \n" +
            "ITA_IFM_CreatedDate__c , \n" +
            "Priority, \n" +
            "Status,\n" +
            "Origin, \n" +
            "Subject, \n" +
            "ITA_IFM_SubStatus__c \n" +
            "from \n" +
            "case \n" +
            "where \n" +
            "AccountId = '0010Y00000G8JwnQAF' \n" +
            "and \n" +
            "ITA_IFM_CreatedDate__c > $TODAY$ \n" +
            "and \n" +
            "Subject = 'GESTIONE PRIVACY' \n" +
            "order by \n" +
            "ITA_IFM_CreatedDate__c \n" +
            "desc";
    public final static String get_query_164b = "Select \n" +
            "ITA_IFM_Account__c, \n" +
            "ITA_IFM_CreationDate__c, \n" +
            "ITA_IFM_Phone__c, \n" +
            "ITA_IFM_MarketingConsentPhone__c, \n" +
            "ITA_IFM_ThirdPartyConsentPhone__c, \n" +
            "ITA_IFM_Email__c, \n" +
            "ITA_IFM_MarketingConsentEmail__c ,\n" +
            "ITA_IFM_ThirdPartyConsentEmail__c \n" +
            "from \n" +
            "ITA_IFM_Privacy__c \n" +
            "where \n" +
            "ITA_IFM_Account__c ='$ACCOUNT_ID$'\n" +
            "and \n" +
            "ITA_IFM_CreationDate__c = TODAY \n" +
            "order by \n" +
            "ITA_IFM_CreationDate__c \n" +
            "desc";
    public final static String get_CF_email_230 = "SELECT  Account.NE__Fiscal_code__c,count(id) \n" +
            "FROM Asset\n" +
            "WHERE RecordType.Name = 'Commodity'\n " +
            "and NE__Status__c = 'Active'\n" +
            "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ENEL-D')\n" +
            "and Account.NE__Fiscal_code__c !='' \n" +
            "and Account.RecordType.Name = 'Residenziale' and ITA_IFM_Service_Use__c = 'Uso Abitativo' and ITA_IFM_Commodity__c = 'ELE'\n" +
            "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c !='In Attesa' and ITA_IFM_Status__c !='Working' \n" +
            "and ITA_IFM_OperationStatus__c != 'In attesa' \n" +
            "and ITA_IFM_OperationStatus__c != 'working') \n" +
            "LIMIT 50";
    public final static String recupera_pod_pa_split_payment = "Select \n" +
            "Account.NE__Fiscal_code__c,\n" +
//			"Account.Name,\n" +
//			"ITA_IFM_Service_Use__c,\n" +
//			"account.ITA_IFM_CustomerType__c, \n" +
//			"account.ITA_IFM_PEC__c, \n" +
            "ITA_IFM_Pod_Pdr__c\n" +
//			"ITA_IFM_Commodity__c, \n" +
//			"NE__Status__c, \n" +
//			"NE__Service_Point__r.ITA_IFM_SupplierId__c, \n"+
//			"ITA_IFM_Split_Payment_StartDate__c, \n"+
//			"ITA_IFM_Split_Payment_EndDate__c \n" +
            "FROM \n" +
            "asset \n" +
            "WHERE \n" +
            "ITA_IFM_SplitPayment__c='no' \n" +
            "and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \n" +
            "and ITA_IFM_FEPAIsActive__c='si' \n" +
            "and NE__Status__c='Active' \n" +
            "and ITA_IFM_Commodity__c='ELE'\n" +
            "and ITA_IFM_Pod_Pdr__c LIKE'%IT002_%' \n" +
            "and ITA_IFM_Service_Use__c <>'Uso Diverso da Abitazione' \n" +
            "and ITA_IFM_Service_Use__c <>'Uso Abitativo'\n" +
            "LIMIT 10";
    public final static String recupera_pod_pa_split_paymento_controlloCf = "Select \n" +
            "ITA_IFM_Fiscal_Code__c,\n" +
            "ITA_IFM_Categoria_Istat__c \n" +
            "FROM ITA_IFM_FEPA_IPA__c \n" +
            "where \n" +
            "ITA_IFM_Fiscal_Code__c IN(@)";
    public final static String recupera_pod_pa_split_paymento_controlloData = "Select \n" +
            "ITA_IFM_FiscalCode__c, \n" +
            " ITA_IFM_EffectiveDateSP__c \n" +
            " FROM ITA_IFM_SplitPaymentMEF__c \n" +
            "where \n" +
            " ITA_IFM_FiscalCode__c =@";
    public final static String recupera_pod_pa_split_payment_cessazione = "Select \n" +
            "Account.NE__Fiscal_code__c,\n" +
            "ITA_IFM_Commodity__c, \n" +
            "ITA_IFM_Pod_Pdr__c \n" +
            "FROM \n" +
            "asset \n" +
            "WHERE \n" +
            "ITA_IFM_SplitPayment__c='si' \n" +
            "and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \n" +
            "and ITA_IFM_FEPAIsActive__c='si' \n" +
            "and NE__Status__c='Active' \n" +
            "and ITA_IFM_Service_Use__c <>'Uso Diverso da Abitazione' \n" +
            "and ITA_IFM_Service_Use__c <>'Uso Abitativo'\n" +
            "LIMIT 10";

    //Nuove query
    //Stato e sottostato del Case + OI Quote
    public final static String recupera_stati_item_case_quote = "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber, ITA_IFM_Case__c,  ITA_IFM_Case__r.Status , ITA_IFM_Case__r.ITA_IFM_SubStatus__c,  \n" +
            "Name, NE__OrderId__r.RecordType.Name, ITA_IFM_POD__c, NE__Status__c,ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c ,ITA_IFM_RecordTypeName__c, CreatedDate \n" +
            "FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_Commodity__c='@TIPOLOGIA_POD@' and ITA_IFM_POD__c='@POD@' and  ITA_IFM_RecordTypeName__c='@TIPO_OI@' \n"  +
            "and NE__OrderId__r.RecordType.Name = 'Quote'";
    public final static String recupera_stati_item_case_quote_VAS = "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber, ITA_IFM_Case__c,  ITA_IFM_Case__r.Status , ITA_IFM_Case__r.ITA_IFM_SubStatus__c,  \n" +
            "Name, NE__OrderId__r.RecordType.Name, ITA_IFM_POD__c, NE__Status__c,ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c ,ITA_IFM_RecordTypeName__c, CreatedDate \n" +
            "FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_RecordTypeName__c='@TIPO_OI@' \n" +
            "and NE__OrderId__r.RecordType.Name = 'Quote'";
    //Stato e sottostato del Case + OI Ordine + BPM e stati di integrazione e metodo dipagamento
    public final static String recupera_stati_item_case_order = "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber,ITA_IFM_Case__c, ITA_IFM_Case__r.Status,ITA_IFM_Case__r.ITA_IFM_SubStatus__c, Name, \n" +
            "NE__OrderId__r.RecordType.Name,NE__Status__c, ITA_IFM_IDBPM__c, ITA_IFM_POD__c, ITA_IFM_R2D_Status__c,ITA_IFM_R2DOutcomeStatus__c,ITA_IFM_SAP_Status__c,ITA_IFM_SAP_StatusCode__c,ITA_IFM_SAP_StatusDescription__c, ITA_IFM_SEMPRE_Status__c,ITA_IFM_SEMPRE_StatusCode__c,ITA_IFM_SEMPRE_StatusDescription__c, ITA_IFM_UDB_Status__c, \n" +
            "ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c,ITA_IFM_RecordTypeName__c, NE__BillingProfId__r.NE__Payment__c,ITA_IFM_Temporary_Billing__c, CreatedDate \n" +
            "FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' \n and ITA_IFM_Commodity__c='@TIPOLOGIA_POD@' and ITA_IFM_POD__c='@POD@' and ITA_IFM_RecordTypeName__c='@TIPO_OI@' " +
            "and NE__OrderId__r.RecordType.Name = 'Order'";
    //Stato e sottostato del Case + OI Ordine + BPM + fase e stati di integrazione e metodo dipagamento
    public final static String recupera_stati_item_case_order1 = "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber,ITA_IFM_Case__c, ITA_IFM_Case__r.Status,ITA_IFM_Case__r.ITA_IFM_SubStatus__c, Name, \n" +
            "NE__OrderId__r.RecordType.Name,NE__Status__c, ITA_IFM_IDBPM__c, ITA_IFM_POD__c, ITA_IFM_R2D_Status__c,ITA_IFM_R2DOutcomeStatus__c,ITA_IFM_SAP_Status__c,ITA_IFM_SAP_StatusCode__c,ITA_IFM_SAP_StatusDescription__c, ITA_IFM_SEMPRE_Status__c,ITA_IFM_SEMPRE_StatusCode__c,ITA_IFM_SEMPRE_StatusDescription__c, ITA_IFM_UDB_Status__c, \n" +
            "ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c,ITA_IFM_RecordTypeName__c, NE__BillingProfId__r.NE__Payment__c,ITA_IFM_Temporary_Billing__c,ITA_IFM_Phase__c, CreatedDate \n" +
            "FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' \n and ITA_IFM_Commodity__c='@TIPOLOGIA_POD@' and ITA_IFM_POD__c='@POD@' and ITA_IFM_RecordTypeName__c='@TIPO_OI@' " +
            "and NE__OrderId__r.RecordType.Name = 'Order'";
    //Stato e sottostato del Case + OI Ordine + BPM e stati di integrazione e metodo dipagamento
    public final static String recupera_stati_item_case_order_vas = "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber,ITA_IFM_Case__c, ITA_IFM_Case__r.Status,ITA_IFM_Case__r.ITA_IFM_SubStatus__c, Name, \n" +
            "NE__OrderId__r.RecordType.Name,NE__Status__c, ITA_IFM_IDBPM__c, ITA_IFM_POD__c, ITA_IFM_R2D_Status__c,ITA_IFM_R2DOutcomeStatus__c,ITA_IFM_SAP_Status__c,ITA_IFM_SAP_StatusCode__c,ITA_IFM_SAP_StatusDescription__c, ITA_IFM_SEMPRE_Status__c,ITA_IFM_SEMPRE_StatusCode__c,ITA_IFM_SEMPRE_StatusDescription__c, ITA_IFM_UDB_Status__c, \n" +
            "ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c,ITA_IFM_RecordTypeName__c, NE__BillingProfId__r.NE__Payment__c,ITA_IFM_Temporary_Billing__c, CreatedDate \n" +
            "FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' \n and ITA_IFM_RecordTypeName__c='@TIPO_OI@' \n" +
            "and NE__OrderId__r.RecordType.Name = 'Order'";
    //Stato dell'asset
    public final static String recupera_stato_asset = "SELECT id, ITA_IFM_Commodity__c, NE__Status__c, NE__StartDate__c, NE__EndDate__c,NE__Service_Point__r.ITA_IFM_POD__c,NE__Service_Point__r.ITA_IFM_SupplierId__c, \n " +
            "NE__Service_Point__r.ITA_IFM_Commodity_Use__c, ITA_IFM_Usage_Type__c,Account.NE__VAT__c, Account.NE__Fiscal_code__c, ITA_IFM_SAP_Contract__c,ITA_IFM_Sap_Contractual_Account__c, \n" +
            "Account.ITA_IFM_SAP_Primary_BP__c,NE__BillingProf__r.NE__Payment__c, ITA_IFM_BackupMOP__c,NE__Service_Point__r.ITA_IFM_Address__c, ITA_IFM_ConsumptionNumber__c \n" +
            "FROM Asset \n" +
            "WHERE RecordType.Name = '@TIPO_OI@' \n" +
            "AND NE__Service_Point__r.ITA_IFM_POD__c = '@POD@' \n";
    
    public final static String recupera_case_stato_residenza = "select ITA_IFM_FiscalCode__c,ITA_IFM_Nome_Cliente__c,ITA_IFM_Descrizione_Tripletta__c,Status,ITA_IFM_CreatedDate__c,CaseNumber \n " +
            "FROM Case \n" +
            "WHERE \n" +
            "ITA_IFM_FiscalCode__c='@CODICE_FISCALE@' and ITA_IFM_Descrizione_Tripletta__c ='Modifica Stato Residenza' and ITA_IFM_CreatedDate__c >= @DATA@ ";
    
    public final static String recupera_stati_Moge = "SELECT ITA_IFM_Case__r.CaseNumber, ITA_IFM_Case__r.Status, ITA_IFM_Case__r.ITA_IFM_SubStatus__c, \n" +
    		"ITA_IFM_Subject__c, ITA_IFM_POD__c, Name, ITA_IFM_Status__c, ITA_IFM_BPM_ID__c,ITA_IFM_SEMPRE_STATUSCODE__C, ITA_IFM_R2D_Status__c, ITA_IFM_Fittizio_Order_flag__c, ITA_IFM_SAP_Status__c, ITA_IFM_SEMPRE_Status__c\n" +
    		"FROM ITA_IFM_Case_Items__c\n" +
    		"WHERE ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@'";
    //Recupero dati Id Documento
    public final static String recupera_stati_item_case_Copia_Documento = "SELECT Name, RecordType.Name, ITA_IFM_Status__c, ITA_IFM_Direzione__c, ITA_IFM_Modello__c, ITA_IFM_Delivery_Channel__c, \n" +
            "ITA_IFM_Case__r.CaseNumber, ITA_IFM_Account__r.Name, CreatedDate, ITA_IFM_LinkText__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_Modello__c='Plico' \n";
    //Recupero stato documento modello plico
    public final static String recupera_stato_modello_plico_Copia_Documento = "SELECT ITA_IFM_Status__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_Modello__c='Plico' \n";
    //Recupero Item Link Documento valorizzato
    public final static String recupera_item_valorizzato_case_Copia_Documento = "SELECT ITA_IFM_LinkText__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_LinkText__c <> NULL \n";
    //Recupero stato sottostato case copia documento
    public final static String recupera_stato_sottostato_case_Copia_Documento = "SELECT Subject, CaseNumber, Status, ITA_IFM_SubStatus__c\n" +
            "FROM Case \n" +
            "where CaseNumber = '@CASE@' \n";
    //Recupero Items Modello Documento valorizzato con Oggetto Email Generico
    public final static String recupera_item_modello_valorizzato_Oggetto_case_Copia_Documento = "SELECT ITA_IFM_Modello__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_Modello__c = 'Oggetto Email Generico' \n";
    //Recupero Items Modello Documento valorizzato con Oggetto Email Generico
    public final static String recupera_item_modello_valorizzato_Oggetto_Pec_case_Copia_Documento = "SELECT ITA_IFM_Modello__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_Modello__c = 'Oggetto Pec Generico' \n";
    
    //Recupero Items Modello Documento valorizzato con Corpo Email Generico
    public final static String recupera_item_modello_valorizzato_Corpo_case_Copia_Documento = "SELECT ITA_IFM_Modello__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_Modello__c = 'Corpo Email Generico' \n";
    //Recupero Items Modello Documento valorizzato con Corpo Pec Generico
    public final static String recupera_item_modello_valorizzato_Corpo_Pec_case_Copia_Documento = "SELECT ITA_IFM_Modello__c \n" +
            "FROM ITA_IFM_Document__c \n" +
            "where ITA_IFM_Case__r.CaseNumber  = '@CASE@' \n and ITA_IFM_Modello__c = 'Corpo Pec Generico' \n";
	//Recupero stato Offerta e Causale Cliente
    public final static String recupera_stato_offerta_causale_cliente = "select Name, NE__Status__c, ITA_IFM_Causale_Annullamento__c, ITA_IFM_FiscalCode__c, ITA_IFM_ActivationReason__c from NE__Quote__c where name ='@OFFERTA@'";
    
    
    
    
    public final static String recupera_stato_offerta_causale_cliente2 = "select Name, NE__Status__c, ITA_IFM_Causale_Annullamento__c, ITA_IFM_FiscalCode__c, ITA_IFM_ActivationReason__c from NE__Quote__c where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@'";

    
	/**
	 * Stato offerta
	 */
    public final static String recupera_stato_offerta = "SELECT ITA_IFM_ActivationReason__c, ITA_IFM_Case__r.CaseNumber,ITA_IFM_Quote_Number__c, Name, NE__Status__c,ITA_IFM_QualityCheck_VocalOutcome__c, ITA_IFM_QualityCheck_Outcome__c,CreatedDate, ITA_IFM_RosQC_TypeCall__c, ITA_IFM_Causal_SuspensionPrck__c " +
            " FROM NE__Quote__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@'";
    
    /**
	 * Recupera ID oirdine
	 */
    public final static String recupera_ID_Ordine = "SELECT Name " +
            " FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' \n" +
            "and NE__OrderId__r.RecordType.Name = 'Order' \n" +
            "and ITA_IFM_RecordTypeName__c = 'Commodity'";


	/**
	 * Recupera dati relativi alla richiesta di Touch Point
	 */
    public final static String recupera_richiesta_touch_point =
            "SELECT id, Name, ITA_IFM_Status__c, ITA_IFM_Account__c,ITA_IFM_Tipologia_Touch_Point__c, ITA_IFM_Email__c, \n" +
                    "ITA_IFM_Cellulare__c, \n" +
                    "ITA_IFM_Expired_Date__c, ITA_IFM_DaysExpiry__c,\n" +
                    "ITA_IFM_Channel1__c, ITA_IFM_DateReminder1__c, ITA_IFM_DaysReminder1__c, \n" +
                    "ITA_IFM_Channel2__c, ITA_IFM_DateReminder2__c,ITA_IFM_DaysReminder2__c, \n" +
                    "ITA_IFM_Channel3__c, ITA_IFM_DateReminder3__c,ITA_IFM_DaysReminder3__c, \n" +
                    "ITA_IFM_Channel4__c, ITA_IFM_DateReminder4__c, ITA_IFM_DaysReminder4__c, \n" +
                    "ITA_IFM_Channel5__c, ITA_IFM_DateReminder5__c,ITA_IFM_DaysReminder5__c \n" +
                    "FROM ITA_IFM_Touch_Point__c \n" +
                    "WHERE \n" +
                    "ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' \n" +
                    "AND ITA_IFM_Tipologia_Touch_Point__c <> null ";
    //Numero Contratto
    public final static String recupera_numero_contratto = "select ITA_IFM_Quote_Number__c  \n" +
            "FROM NE__Quote__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@'";
    //Conferma Dati Catastali
    public final static String conferma_dati_catastali =
            "SELECT \n" +
                    "id, Name, RecordType.Name, ITA_IFM_Commodity__c, NE__Status__c, NE__StartDate__c, NE__EndDate__c, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, ITA_IFM_Usage_Type__c, Account.NE__VAT__c, Account.NE__Fiscal_code__c, ITA_IFM_SAP_Contract__c, ITA_IFM_Sap_Contractual_Account__c, Account.ITA_IFM_SAP_Primary_BP__c, NE__BillingProf__r.NE__Payment__c, ITA_IFM_BackupMOP__c, NE__Service_Point__r.ITA_IFM_Address__c, ITA_IFM_ConsumptionNumber__c, ITA_IFM_Confirm_Data_Cadastral__c \n" +
                    "FROM Asset \n" +
                    "WHERE RecordType.Name = 'Commodity' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_POD__c = '@POD_CLIENTE@' ";

	public final static String recupera_CF_POD_per_Volture_id19 = "SELECT\n" + "Account.NE__Fiscal_code__c,\n"
			+ "Account.Name,\n" + "NE__Service_Point__r.ITA_IFM_POD__c,\n"
			+ "NE__Service_Point__r.ITA_IFM_SupplierId__c,\n" + "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\n"
			+ "Account.RecordType.Name\n" + "FROM Asset\n" + "WHERE CreatedDate > 2017-01-01T00:00:00.000+0000\n"
			+ "AND Account.RecordType.Name = 'Business'\n" + "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n"
			+ "AND RecordType.Name = 'Commodity'\n" + "AND NE__Status__c = 'Active'\n"
			+ "AND ITA_IFM_Service_Use__c ='Illuminazione pubblica'\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n"
			+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n"
			+ "LIMIT 5";
    public final static String recupera_CF_POD_per_Volture_id38 =  
    "SELECT \n" +
    "Account.NE__Fiscal_code__c,\n" +
    "Account.Name,\n" +
    "account.ITA_IFM_CustomerType__c,\n" +
    "ITA_IFM_Pod_Pdr__c,\n" +
    "ITA_IFM_Commodity__c,\n" +
    "NE__Status__c,\n" +
    "ITA_IFM_Resident__c,\n" +
    "NE__Service_Point__r.ITA_IFM_SupplierId__c,\n" +
    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\n" +
    "NE_BillingProf_Type__c \n" +
    "FROM  \n" +
     "Asset \n" +
    "WHERE \n" +
    "Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
    "AND NE__Status__c='active' \n" +
    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
    "LIMIT 1 \n";
//    
    
//    public final static String recupera_CF_POD_per_Volture_id38 =  
//    	    "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Resident__c\n" +
//    		"NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
//    		"FROM Asset\n" +
//    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
//    		"AND Account.RecordType.Name = 'Business'\n" +
//    		"AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
//    		"AND RecordType.Name = 'Commodity'\n" +
//    		"AND NE__Status__c = 'Active'\n" +
//    		"AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
//    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
//    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
//    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
//    		"LIMIT 40\n";
//    

    public final static String recupera_CF_POD_per_Volture_id26 = "SELECT\r\n" +
            "Account.NE__Fiscal_code__c,\r\n" +
            "Account.Name,\r\n" +
            "account.ITA_IFM_CustomerType__c,\r\n" +
            "ITA_IFM_Pod_Pdr__c,\r\n" +
            "ITA_IFM_Service_Use__c,\r\n" +
            "ITA_IFM_Commodity__c,\r\n" +
            "NE__Status__c,\r\n" +
            "ITA_IFM_Resident__c,\r\n" +
            "NE__Service_Point__r.ITA_IFM_SupplierId__c,\r\n" +
            "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\r\n" +
            "NE_BillingProf_Type__c\r\n" +
            "FROM \r\n" +
            " Asset\r\n" +
            "WHERE\r\n" +
            "Account.ITA_IFM_CustomerType__c = 'Casa'\r\n" +
            "AND NE__Status__c='active'\r\n" +
            "and ITA_IFM_Service_Use__c='Uso Abitativo'\r\n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\r\n" +
            "and CreatedDate > 2020-07-01T00:00:00.000+0000\r\n" +
            "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
            "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
            "AND ITA_IFM_SAP_Contract__c != ''"+
            "AND ITA_IFM_Sap_Contractual_Account__c != ''"+
            "Limit 5";
    public final static String recupera_CF_POD_per_Volture_id23 = 
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
    		"FROM Asset\n" +
    		"WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\n" +
    		"AND Account.RecordType.Name = 'Business'\n" +
    		"AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND ITA_IFM_Service_Use__c ='Uso Diverso da Abitazione'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"and ITA_IFM_SAP_Contract__c != ''"+
    		"and ITA_IFM_Sap_Contractual_Account__c != ''"+
    		"LIMIT 1";
    
    public final static String recupera_CF_POD_per_Volture_id27 = "SELECT\r\n" +
            "Account.NE__Fiscal_code__c,\r\n" +
            "Account.Name,\r\n" +
            "account.ITA_IFM_CustomerType__c,\r\n" +
            "ITA_IFM_Pod_Pdr__c,\r\n" +
            "ITA_IFM_Service_Use__c,\r\n" +
            "ITA_IFM_Commodity__c,\r\n" +
            "NE__Status__c,\r\n" +
            "ITA_IFM_Resident__c,\r\n" +
            "NE__Service_Point__r.ITA_IFM_SupplierId__c,\r\n" +
            "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\r\n" +
            "NE_BillingProf_Type__c\r\n" +
            "FROM \r\n" +
            " Asset\r\n" +
            "WHERE\r\n" +
            "Account.ITA_IFM_CustomerType__c != 'Casa'\r\n" +
            "AND NE__Status__c='active'\r\n" +
            "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n" +
//            "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\r\n" +
            //"and CreatedDate > 2019-07-01T00:00:00.000+0000\r\n" +
            "and ITA_IFM_Commodity__c = 'GAS' \n" +
            "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
            "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
            "Limit 1";
    
    public final static String recupera_CF_POD_per_Volture_id27Fui =
    		 "Select\r\n"
    		+ "NE__Status__c,"
    		+ "NE__StartDate__c,"
    		+ "NE__EndDate__c,"
    		+ "ITA_IFM_Service_Use__c,"
    		+ "Account.Name,"
    		+ "Account.NE__Fiscal_code__c,"
    		+ "Account.RecordType.Name,"
    		+ "ITA_IFM_Commodity__c,"
    		+ "NE__Service_Point__r.ITA_IFM_POD__c,"
    		+ "ITA_IFM_ConsumptionNumber__c,"	
    		+ "NE__ProdId__r.Name"
    		+ "From Asset\r\n"
    		+ "where NE__Status__c = 'Active'"
    		+ "and NE__ProdId__r.Name = 'OFFERTA FUI'"
    		+ "and ITA_IFM_Service_Use__c = 'Uso abitativo'"
    		+ "AND ITA_IFM_SAP_Contract__c != '' AND ITA_IFM_Sap_Contractual_Account__c != ''"
    		+ "Limit 20";
    		


    public final static String recupera_CF_POD_per_VoltureConAccolloid16=
    
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\n" +
    		"ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c\n" + 
    		"FROM  Asset\n" +
    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"AND Account.RecordType.Name = 'Business'\n" +
    		"AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    		"AND ITA_IFM_Commodity__c ='GAS'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\n" +
    		"LIMIT 5";
    
    public final static String recupera_CF_POD_per_Volture_id16=
    "SELECT \n" +
    "Account.NE__Fiscal_code__c, \n" +
    "Account.Name, \n" +
    "account.ITA_IFM_CustomerType__c, \n" +
    "ITA_IFM_Pod_Pdr__c, \n" +
    "ITA_IFM_Service_Use__c, \n" +
    "ITA_IFM_Commodity__c, \n" +
    "NE__Status__c, \n" +
    "ITA_IFM_Resident__c, \n" +
    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
    "NE_BillingProf_Type__c \n" +
    "FROM \n" +
     "Asset \n" +
    "WHERE \n" +
    "Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
    "AND NE__Status__c='active' \n" +
    "and ITA_IFM_Service_Use__c ='Uso diverso da Abitazione' \n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA' \n" +
    "and CreatedDate > 2019-07-01T00:00:00.000+0000 \n" +
    "GROUP BY Account.NE__Fiscal_code__c, Account.NE__VAT__c \n" +
    "HAVING count(Id) = 1 \n" +
    "Limit 30 \n";
    
    public final static String recupera_CF_POD_per_Volture_id29 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name,\n" +
                    "account.ITA_IFM_CustomerType__c,\n" +
                    "ITA_IFM_Pod_Pdr__c,\n" +
                    "ITA_IFM_Service_Use__c,\n" +
                    "ITA_IFM_Commodity__c,\n" +
                    "NE__Status__c,\n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
                    "AND NE__Status__c='active' \n" +
                    "and ITA_IFM_Service_Use__c='Uso Abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA' \n" +
                    "and CreatedDate > 2019-07-01T00:00:00.000+0000 \n" +
                    "Limit 150";
    public final static String recupera_CF_POD_per_Volture_id30 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Service_Use__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "and ITA_IFM_Service_Use__c='Uso Abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA' \n" +
                    "and CreatedDate > 2019-07-01T00:00:00.000+0000 \n" +
                    "Limit 150";

    public final static String recupera_CF_POD_per_Voltureid24 =
    		  " SELECT Account.NE__Fiscal_code__c,"
    		+ " Account.Name, account.ITA_IFM_CustomerType__c,"
    		+ " NE__Service_Point__r.ITA_IFM_POD__c,"
    		+ " ITA_IFM_Commodity__c, NE__Status__c,"
    		+ " ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c,"
    		+ " NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,"
    		+ " NE_BillingProf_Type__c,"
    		+ " ITA_IFM_Sap_Contractual_Account__c,"
    		+ " ITA_IFM_SAP_Contract__c"
    		+ " FROM Asset"
    		+ " WHERE CreatedDate > 2020-03-01T00:00:00.000+0000"
    		+ " AND Account.RecordType.Name = 'Residenziale'"
    		+ " AND Account.ITA_IFM_CustomerType__c = 'Casa'"
    		+ " AND RecordType.Name = 'Commodity' "
    		+ " AND NE__Status__c = 'Active' AND ITA_IFM_Service_Use__c ='Uso Abitativo'"
    		+ " AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'"
    		+ " AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))"
    		+ " AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))"
    		+ " AND ITA_IFM_Sap_Contractual_Account__c != ''"
    		+ " AND ITA_IFM_SAP_Contract__c != ''"
    		+ " LIMIT 10";
    

    public final static String recupera_CF_POD_per_Volture17 =
            "SELECT \n" +
                    "AccountId, \n" +
                    "Account.RecordType.Name, \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "Account.ITA_IFM_CustomerType__c,\n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE_BillingProf_Type__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c \n" +
                    "FROM \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c = 'Active' \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 50 \n";
    
    //FR 19.07.2021 - aggiunta condizione su created date recupera_CF_POD_per_Volture18
    public final static String recupera_CF_POD_per_Volture18 =
            "SELECT \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "Account.Name,\n" +
                    "account.ITA_IFM_CustomerType__c,\n" +
                    "ITA_IFM_Pod_Pdr__c,\n" +
                    "ITA_IFM_Commodity__c,\n" +
                    "NE__Status__c,\n" +
                    "ITA_IFM_Resident__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\n" +
                    "NE_BillingProf_Type__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_POD__C "+
                    "FROM \n" +
                    "Asset \n" +
                    "WHERE  CreatedDate > 2020-01-01T00:00:00.000+0000\n" +
                    "and Account.RecordType.Name = 'Business' \n" +
                    "AND Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 50 \n";
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid5 = 
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\n" +
                    "ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c \n" +
                    "FROM  Asset\n" +
                    "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\n" +
                    "AND Account.RecordType.Name = 'Residenziale'\n" +
                    "AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
                    "AND ITA_IFM_Commodity__c ='GAS'\n" +
                    "AND RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'\n" +
                    "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
                    "AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\n" +
                    "LIMIT 30";
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid19 = //05.07.2021 rimpiazzata query con query excel
		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\r\n"
		+ "ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c \r\n"
		+ "FROM  Asset\r\n"
		+ "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\r\n"
		+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
		+ "AND Account.ITA_IFM_CustomerType__c = 'Casa'\r\n"
		+ "AND ITA_IFM_Commodity__c ='GAS'\r\n"
		+ "AND RecordType.Name = 'Commodity'\r\n"
		+ "AND NE__Status__c = 'Active'\r\n"
		+ "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\r\n"
		+ "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\r\n"
		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
		+ "AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\r\n"
		+ "LIMIT 30";
    /*"SELECT \n" +
    "Account.NE__Fiscal_code__c,\n" +
    "Account.Name,\n" +
    "account.ITA_IFM_CustomerType__c,\n" +
    "ITA_IFM_Pod_Pdr__c,\n" +
    "ITA_IFM_Service_Use__c,\n" +
    "ITA_IFM_Commodity__c,\n" +
    "NE__Status__c,\n" +
    "ITA_IFM_Resident__c,\n" +
    "NE__Service_Point__r.ITA_IFM_SupplierId__c,\n" +
    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\n" +
    "NE_BillingProf_Type__c \n" +
    "FROM \n" +
     "Asset \n" +
    "WHERE \n" +
    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
    "AND NE__Status__c='active' \n" +
    "and ITA_IFM_Service_Use__c='Uso Abitativo' \n" +
    "AND ITA_IFM_Commodity__c ='GAS' \n" +
    "and CreatedDate > 2019-07-01T00:00:00.000+0000 \n" +
    "Limit 150 \n";*/
    
    public final static String recupera_CF_POD_per_VoltureSenzaAccolloid9 = 
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\n" +
"ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c\n" +
"FROM  Asset WHERE\n" +
"Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
"AND NE__Status__c='active'\n" +
"and ITA_IFM_Service_Use__c='Uso Abitativo'\n" +
"AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\n" +
"and CreatedDate > 2020-03-01T00:00:00.000+0000\n" +
"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
"AND ITA_IFM_SAP_Contract__c != '' AND ITA_IFM_Sap_Contractual_Account__c != ''"+
"Limit 10\n" ;

    public final static String recupera_CF_POD_per_VoltureSenzaAccolloid20 =
 
    		"SELECT \n" +
                    "AccountId,\n" +
                    "Account.RecordType.Name,\n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
    		"AND NE__Status__c = 'Active' \n" +
    		"AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c NOT IN ('ACEA', 'ENEL-D') \n" +
    		"AND ITA_IFM_Commodity__c = 'ELE' \n" +
    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
    		"LIMIT 10 ";
    	
    
    public final static String recupera_CF_POD_per_VoltureSenzaAccollo_id_4=
    		
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
    		"FROM Asset\n" +
    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"AND Account.RecordType.Name = 'Residenziale'\n" +
    		"AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"LIMIT 30";
    
  public final static String recupera_CF_POD_per_VoltureSenzaAccollo_id_31=
    		
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Commodity__c, NE__Status__c, ITA_IFM_Resident__c,\n" +
		  	"NE__Service_Point__r.ITA_IFM_SupplierId__c,  NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c \n" +
		  	"FROM Asset WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
		  	"AND Account.RecordType.Name = 'Residenziale'\n" +
		  	"AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
		  	"AND RecordType.Name = 'Commodity'\n" +
		  	"AND NE__Status__c = 'Active'\n" +
		  	"AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
		  	"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
		  	"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
		  	"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
		  	"LIMIT 30";
    
    
    public final static String recupera_CF_POD_per_Volture =
            "SELECT \n" +
                    "AccountId,\n" +
                    "Account.RecordType.Name,\n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='Uso Abitativo' \n" +
//                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
					"and ITA_IFM_Commodity__c = 'ELE' \n" +
					"and CreatedDate > 2020-07-01T00:00:00.000+0000\r\n" +
                   "and ITA_IFM_Pod_Pdr__c not in ('IT002E3027178A','IT004E33222110','IT002E0933493A','IT002E9402934A','IT004E33666211','IT018E00563851','IT002E2234567A') "+
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 1 ";
    public final static String recupera_CF_POD_per_Volture_33 =
            "SELECT \n" +
                    "AccountId,\n" +
                    "Account.RecordType.Name,\n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='Uso Abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
//                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
//                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "and ITA_IFM_Pod_Pdr__c = 'IT002E9144271A'"+
                    "LIMIT 3 ";
    
    public final static String recupera_CF_POD_per_VSA_33 =
    		  " SELECT Account.NE__Fiscal_code__c,"
    		+ " Account.Name, account.ITA_IFM_CustomerType__c,"
    		+ " NE__Service_Point__r.ITA_IFM_POD__c,"
    		+ " ITA_IFM_Commodity__c, NE__Status__c,"
    		+ " ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c,"
    		+ " NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,"
    		+ " NE_BillingProf_Type__c,"
    		+ " ITA_IFM_Sap_Contractual_Account__c,"
    		+ " ITA_IFM_SAP_Contract__c"
    		+ " FROM Asset"
    		+ " WHERE CreatedDate > 2020-03-01T00:00:00.000+0000"
    		+ " AND Account.RecordType.Name = 'Residenziale'"
    		+ " AND Account.ITA_IFM_CustomerType__c = 'Casa'"
    		+ " AND RecordType.Name = 'Commodity' "
    		+ " AND NE__Status__c = 'Active' AND ITA_IFM_Service_Use__c ='Uso Abitativo'"
    		+ " AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'"
    		+ " AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))"
    		+ " AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))"
    		+ " AND ITA_IFM_Sap_Contractual_Account__c != ''"
    		+ " AND ITA_IFM_SAP_Contract__c != ''"
    		+ " LIMIT 10";
    
    
    public final static String recupera_CF_POD_per_Voltureid8 =
            "SELECT \n" +
                    "AccountId,\n" +
                    "Account.RecordType.Name,\n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='Uso Abitativo' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "and CreatedDate > 2019-07-01T00:00:00.000+0000 \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "AND ITA_IFM_SAP_Contract__c != ''"+
                    "AND ITA_IFM_Sap_Contractual_Account__c != ''"+
                    "LIMIT 1";
    public final static String recupera_CF_POD_per_VSA_15 =
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\n" +
    		"ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c\n" + 
    		"FROM  Asset\n" +
    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"AND Account.RecordType.Name = 'Residenziale'\n" +
    		"AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
    		"AND ITA_IFM_Commodity__c ='GAS'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\n" +
    		"LIMIT 30";
    
    
    
    
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid3 =
            "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
                    "FROM Asset\n" +
                    "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
                    "AND Account.RecordType.Name = 'Business'\n" +
                    "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
                    "AND RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'\n" +
                    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
                    "LIMIT 10";
    public final static String recupera_CF_POD_per_Volture_id15=
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\r\n"
    		+ "ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c \r\n"
    		+ "FROM  Asset\r\n"
    		+ "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\r\n"
    		+ "AND Account.RecordType.Name = 'Residenziale'\r\n"
    		+ "AND Account.ITA_IFM_CustomerType__c = 'Casa'\r\n"
    		+ "AND ITA_IFM_Commodity__c ='GAS'\r\n"
    		+ "AND RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Status__c = 'Active'\r\n"
    		+ "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\r\n"
    		+ "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
    		+ "AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\r\n"
    		+ "LIMIT 30";
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid6 = 
    		
    		"SELECT Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,\n" + 
    		"NE__Service_Point__r.ITA_IFM_POD__c, CreatedDate \n" +
    		"FROM Asset\n" +
    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"and Account.RecordType.Name = 'Business' \n" +
    		"AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione'\n" + 
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"LIMIT 2";
    
    
public final static String recupera_CF_POD_per_VoltureConAccolloid7 = 
		  "SELECT Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, \r\n"
		+ "NE__Service_Point__r.ITA_IFM_POD__c, CreatedDate\r\n"
		+ "FROM Asset \r\n"
		+ "WHERE CreatedDate > 2020-01-01T00:00:00.000+0000\r\n"
		+ "and RecordType.Name = 'Commodity' \r\n"
		+ "and NE__Status__c = 'Active' \r\n"
		+ "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA') \r\n"
		+ "and Account.RecordType.Name = 'Business' \r\n"
		+ "and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \r\n"
		+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE \r\n"
		+ "ITA_IFM_OperationStatus__c IN ('In Attesa') OR ITA_IFM_Status__c IN ('Working') )\r\n"
		+ "LIMIT 10";
    		/*
    		"SELECT Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, \r\n"
    		+ "NE__Service_Point__r.ITA_IFM_POD__c, CreatedDate\r\n"
    		+ "FROM Asset \r\n"
    		+ "WHERE CreatedDate > 2020-01-01T00:00:00.000+0000\r\n"
    		+ "and RecordType.Name = 'Commodity' \r\n"
    		+ "and NE__Status__c = 'Active' \r\n"
    		+ "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA') \r\n"
    		+ "and Account.RecordType.Name = 'Business' \r\n"
    		+ "and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \r\n"
    		+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \r\n"
    		+ "LIMIT 2";*/
 
	 public final static String recupera_CF_per_VoltureConAccolloid7_entr = 
			   " SELECT\r\n"
			 + "id, RecordType.Name, NE__Fiscal_code__c, Account.ITA_IFM_Complete_Name__c, NE__VAT__c, Account.ITA_IFM_Company__c, NE__Status__c\r\n"
			 + "FROM Account\r\n"
			 + "where\r\n"
			 + "id NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
			 + "AND CreatedDate > 2020-01-01T00:00:00.000+0000 AND Account.RecordType.Name = 'Business' AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione'\r\n"
			 + " AND NE__Status__c!='Prospect'"
			 + "LIMIT 100\r\n";
    

    public final static String recupera_CF_POD_per_VoltureConAccolloid8 = 
    "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
            "FROM Asset\n" +
            "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
            "AND Account.RecordType.Name = 'Business'\n" +
            "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
            "AND RecordType.Name = 'Commodity'\n" +
            "AND NE__Status__c = 'Active'\n" +
            "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
            "LIMIT 10";
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid20 = //05.07.2021 rimpiazzata query con query TL
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\r\n"
    		+ "FROM Asset\r\n"
    		+ "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\r\n"
    		+ "AND Account.RecordType.Name = 'Business'\r\n"
    		+ "AND Account.ITA_IFM_CustomerType__c != 'Casa'\r\n"
    		+ "AND RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Status__c = 'Active'\r\n"
    		+ "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\r\n"
    		+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
    		+ "LIMIT 10";
    		/*
    "SELECT \n" +
    "Account.NE__Fiscal_code__c,\n" +
    "Account.Name,\n" +
    "account.ITA_IFM_CustomerType__c,\n" +
    "ITA_IFM_Pod_Pdr__c,\n" +
    "ITA_IFM_Commodity__c,\n" +
    "NE__Status__c,\n" +
    "ITA_IFM_Resident__c,\n" +
    "NE__Service_Point__r.ITA_IFM_SupplierId__c,\n" +
    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\n" +
    "NE_BillingProf_Type__c \n" +
    "FROM \n" +
    "Asset \n" +
    "WHERE \n" +
    "Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
    "AND NE__Status__c='Active' \n" +
    "AND ITA_IFM_Service_Use__c ='Uso diverso da abitazione' \n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
    "LIMIT 20 \n";*/
    
    //FR 22.06.2021 allineata query con versione agg. TL 
    public final static String recupera_CF_POD_per_VoltureConAccolloid14 =
    		"SELECT "
    		+ "Account.NE__Fiscal_code__c, "
    		+ "Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, "
    		+ "NE__Service_Point__r.ITA_IFM_SupplierId__c, "
    		+ "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, "
    		+ "Account.RecordType.Name \n"
    		+ "FROM Asset\r\n"
    		+ "WHERE CreatedDate > 2020-01-01T00:00:00.000+0000 \n"
    		+ "AND Account.RecordType.Name = 'Business' \n"
    		+ "AND Account.ITA_IFM_CustomerType__c != 'Casa' \n"
    		+ "AND RecordType.Name = 'Commodity' \n"
    		+ "AND NE__Status__c = 'Active' \n"
    		+ "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n"
    		+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n"
    		+ "LIMIT 10";
    		
    		/*
            "SELECT \n" +
                    "AccountId, \n" +
                    "Account.RecordType.Name, \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "Account.ITA_IFM_CustomerType__c, \n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE_BillingProf_Type__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
                    "AND NE__Status__c = 'Active' \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso diverso da Abitazione' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 30 \n";*/
    public final static String recupera_CF_POD_per_VoltureConAccolloid13 =
            "SELECT \n" +
                    "AccountId, \n" +
                    "Account.RecordType.Name, \n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "Account.ITA_IFM_CustomerType__c,\n" +
                    "RecordType.Name, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE_BillingProf_Type__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_POD__C \n" + //FR 2021.06.18 aggiunto campo pod 
                    "FROM Asset\r\n" + 
					"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000 \n" +
					"AND Account.RecordType.Name = 'Residenziale' \n" +
					"AND Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
					"AND RecordType.Name = 'Commodity' \n" +
					"AND NE__Status__c = 'Active' \n" +
					"AND ITA_IFM_Service_Use__c ='Uso Abitativo' \n" +
					"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
					"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
					"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
					"LIMIT 30"; 
					                    
                    
                    
                    
                    /*FR 2021.06.18 - sostituita con query presente in Excel
                    "FROM \n" +
                    "Asset \n" +
                    "WHERE Account.ITA_IFM_CustomerType__c = 'Casa' \n" +
                    "AND NE__Status__c = 'Active' \n" +
                    "AND ITA_IFM_Service_Use__c = 'Uso Abitativo' \n" +
                    "AND ITA_IFM_Commodity__c = 'ELE' \n" +
                    "AND CreatedDate > 2020-01-01T00:00:00.000+0000 \n" +
                   // "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 30 \n";*/

    
    public final static String recupera_CF_POD_per_VoltureConAccolloid23 = 
    	    "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
    		"FROM Asset\n" +
    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"AND Account.RecordType.Name = 'Residenziale'\n" +
    		"AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"LIMIT 30 \n";
    
  

    

    public final static String recupera_CF_POD_per_VoltureConAccolloid9 =
            "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
                    "FROM Asset\n" +
                    "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
                    "AND Account.RecordType.Name = 'Residenziale'\n" +
                    "AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
                    "AND RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'\n" +
                    "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
                    "LIMIT 10";

    /**
     Query recupera_CF_POD_per_VoltureConAccolloid4
     Utilizzata per voltura con accollo 4
     Modificata il 07.09.2021  - Query SFDC - CRM e WEB.xlsx, con il’ID 561
     */
    public final static String recupera_CF_POD_per_VoltureConAccolloid4 =
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\r\n"
    		+ "FROM Asset\r\n"
    		+ "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\r\n"
    		+ "AND Account.RecordType.Name = 'Business'\r\n"
    		+ "AND Account.ITA_IFM_CustomerType__c != 'Casa'\r\n"
    		+ "AND RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Status__c = 'Active'\r\n"
    		+ "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\r\n"
    		+ "AND NE__Service_Point__c IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "AND NE__Service_Point__c IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
    		+ "LIMIT 10";
    		/*"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\r\n"
    		+ "FROM Asset\r\n"
    		+ "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\r\n"
    		+ "AND Account.RecordType.Name = 'Business'\r\n"
    		+ "AND Account.ITA_IFM_CustomerType__c != 'Casa'\r\n"
    		+ "AND RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Status__c = 'Active'\r\n"
    		+ "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\r\n"
    		+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
    		+ "LIMIT 10";*/
    		/*
    		 * "SELECT Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c, CreatedDate \n" +
                    "FROM Asset \n" +
                    "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000 \n" +
                    "and RecordType.Name = 'Commodity' \n" +
                    "and NE__Status__c = 'Active' \n" +
                    "and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA') \n" +
                    "and Account.RecordType.Name = 'Business' \n" +
                    "and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
                    "and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 2";*/
    public final static String recupera_CF_POD_per_VoltureConAccolloid21 =
    		//FR 16/07/2021 - query Excel
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name ,\r\n"
    		+ "NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n"
    		+ "FROM Asset\r\n"
    		+ "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\r\n"
    		+ "AND Account.RecordType.Name != 'Residenziale'\r\n"
    		+ "AND Account.ITA_IFM_CustomerType__c != 'Casa'\r\n"
    		+ "AND RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Status__c = 'Active'\r\n"
    		+ "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\r\n"
    		+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n"
    		+ "AND ( Account.NE__Fiscal_code__c LIKE '8%' OR ( Account.NE__Fiscal_code__c  LIKE '9%'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
    		+ "LIMIT 10";
    		
    		/*
    		//FR 16/07/2021 - sostituita con query presente nell'excel
    		"SELECT \n" +
    		"Account.NE__Fiscal_code__c, \n" +
    		"Account.Name, \n" +
    		"account.ITA_IFM_CustomerType__c, \n" +
    		"ITA_IFM_Pod_Pdr__c, \n" +
    		"ITA_IFM_Commodity__c, \n" +
    		"NE__Status__c, \n" +
    		"ITA_IFM_Resident__c, \n" +
    		"NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
    		"NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
    		"NE_BillingProf_Type__c \n" +
    		"FROM  \n" +
    		 "Asset \n" +
    		"WHERE \n" +
    		"Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
    		"AND NE__Status__c='active' \n" +
    		"AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
    		"AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"LIMIT 10 \n";*/
    
    public final static String recupera_CF_POD_per_Volture2 =
            "SELECT \n" +
                    "AccountId,\n" +
                    "Account.NE__Fiscal_code__c, \n" +
                    "Account.Name, \n" +
                    "account.ITA_IFM_CustomerType__c, \n" +
                    "ITA_IFM_Pod_Pdr__c, \n" +
                    "ITA_IFM_Commodity__c, \n" +
                    "NE__Status__c, \n" +
                    "ITA_IFM_Resident__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
                    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
                    "NE_BillingProf_Type__c \n" +
                    "FROM  \n" +
                    "Asset \n" +
                    "WHERE \n" +
                    "Account.ITA_IFM_CustomerType__c = 'Azienda' \n" +
                    "AND NE__Status__c='active' \n" +
                    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 1";
    
    public final static String recupera_CF_POD_per_VSA6 =
    				"SELECT Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, \n" +
    "NE__Service_Point__r.ITA_IFM_POD__c, CreatedDate\n" + 
    "FROM Asset\n" +
    "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"and Account.RecordType.Name = 'Business' \n" +
    "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    "AND RecordType.Name = 'Commodity'\n" +
    "AND NE__Status__c = 'Active'\n" +
    "and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione'\n" + 
    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    "LIMIT 1";
    
    public final static String recupera_CF_POD_per_VSA2 ="SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
    		"FROM Asset\n" +
    		"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    		"AND Account.RecordType.Name = 'Residenziale'\n" +
    		"AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
    		"AND RecordType.Name = 'Commodity'\n" +
    		"AND NE__Status__c = 'Active'\n" +
    		"AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
    		"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    		"LIMIT 10";
    
    
    
    
    
    
    
    
    public final static String recupera_CF_POD_per_VoltureId10 =
    "SELECT \n" +
    "Account.NE__Fiscal_code__c, \n" +
    "Account.Name, \n" +
    "account.ITA_IFM_CustomerType__c, \n" +
    "ITA_IFM_Pod_Pdr__c, \n" +
    "ITA_IFM_Commodity__c, \n" +
    "NE__Status__c, \n" +
    "ITA_IFM_Resident__c, \n" +
    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
    "NE_BillingProf_Type__c \n" +
    "FROM  \n" +
     "Asset \n" +
    "WHERE \n" +
    "Account.ITA_IFM_CustomerType__c != 'Casa' \n" +
    "AND NE__Status__c='active' \n" +
    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione' \n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'  \n" +
    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))  \n" +
    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
    "LIMIT 20 \n";
    
    public final static String recupera_CF_POD_per_VoltureId7_ClienteUscente =
            "SELECT Account.RecordType.Name, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, NE__Service_Point__r.ITA_IFM_POD__c, CreatedDate \n" +
                    "FROM Asset  \n" +
                    "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000 \n" +
                    "AND RecordType.Name = 'Commodity'   \n" +
                    "AND NE__Status__c = 'Active' \n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA' \n" +
                    "AND Account.RecordType.Name = 'Business' \n" +
                    "AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa')) \n" +
                    "AND AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
                    "LIMIT 1";
    public final static String recupera_CF_POD_per_VoltureId7_ClienteEntrante =
            "SELECT id, \n" +
    "Account.NE__Fiscal_code__c, \n" +
    "Account.Name, \n" +
    "account.ITA_IFM_CustomerType__c, \n" +
    "ITA_IFM_Pod_Pdr__c, \n" +
    "ITA_IFM_Commodity__c, \n" +
    "NE__Status__c, \n" +
    "ITA_IFM_Resident__c, \n" +
    "NE__Service_Point__r.ITA_IFM_SupplierId__c, \n" +
    "NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, \n" +
    "NE_BillingProf_Type__c \n" +
    "FROM  \n" +
     "Asset \n" +
    "WHERE \n" +        
     "WHERE id NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
     "AND CreatedDate > 2020-01-01T00:00:00.000+0000 AND Account.RecordType.Name = 'Business' AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \n" +
     "LIMIT 5";
    public final static String recupera_POD_per_VoltureId6_7_ClienteUscente =
    		"SELECT \n" +
    		"Account.RecordType.Name,  \n" +
    		"Account.ITA_IFM_CustomerType__c,  \n" +
    		"Account.NE__Fiscal_code__c, \n" +
    		"Account.NE__VAT__c, \n" +
    		"NE__Service_Point__r.ITA_IFM_POD__c, \n" +
    		"CreatedDate \n" +
    		"FROM \n" +
    		"Asset \n" +
    		"WHERE\n" +
    		"RecordType.Name = 'Commodity' \n" +
    		"and NE__Status__c = 'Active' \n" +
    		"and NE__Service_Point__r.ITA_IFM_SupplierId__c IN ('ACEA') \n" +
    		"and Account.RecordType.Name = 'Business' \n" +
    		"and Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' \n" +
    		"and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    		"and AccountId NOT IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working')) \n" +
    		"LIMIT 2 \n";
    public final static String recupera_CF_PrimaAttivazioneId11 =
            "SELECT Id, Name, NE__VAT__c, NE__Fiscal_code__c, Type, ITA_IFM_CustomerType__c \n" +
                    "FROM Account where Type='Non Residenziale'  \n" +
                    "and ITA_IFM_CustomerType__c='Pubblica Amministrazione' \n" +
                    "LIMIT 5";
    public final static String query_recupero_stato_Case="select ITA_IFM_Status__c,ITA_IFM_SubStatus__c,ITA_IFM_FiscalCode__c from Case where CaseNumber='@RICHIESTA@' \n";
    		
    		
    public final static String query_subentro_id1 =
            "SELECT \n" +
                    "NE__Status__c, NE__StartDate__c, NE__EndDate__c, ITA_IFM_Service_Use__c, Account.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name \n" +
                    "From Asset \n" +
                    "where NE__Status__c = 'Active' \n" +
                    "and Account.RecordType.Name = 'Residenziale' \n" +
                    "and ITA_IFM_Commodity__c = 'ELE' \n" +
                    "limit 10";
    public final static String query_subentro_id2 =
            "SELECT \n" +
                    "NE__Status__c, NE__StartDate__c, NE__EndDate__c, ITA_IFM_Service_Use__c, Account.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name \n" +
                    "From Asset \n" +
                    "where NE__Status__c = 'Active' \n" +
                    "and Account.RecordType.Name = 'Residenziale' \n" +
                    "and ITA_IFM_Commodity__c != 'GAS' \n" +
                    "limit 10";
    public final static String query_subentro_id14 =
            "SELECT Account.NE__Fiscal_code__c, id\r\n" + 
            "FROM Asset\r\n" + 
            "WHERE Account.NE__Fiscal_code__c !=''\r\n" + 
            "and Account.RecordType.Name = 'Residenziale'\r\n" + 
            "LIMIT 50\r\n" +
            "";
    public final static String query_subentro_id4 =
            "SELECT \n" +
                    "NE__Status__c,\n" +
                    "NE__StartDate__c,\n" +
                    "NE__EndDate__c,\n" +
                    "Account.Name, \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "Account.NE__VAT__c, \n" +
                    "Account.RecordType.Name,\n" +
                    "ACCOUNT.NE__Status__c,\n" +
                    "Account.ITA_IFM_Type_Legal_Form__c,\n" +
                    "ITA_IFM_Commodity__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
                    "ITA_IFM_ConsumptionNumber__c,\n" +
                    "account.ITA_IFM_CustomerType__c\n" +
                    "From Asset \n" +
                    "where NE__Status__c = 'Active'\n" +
                    "and Account.RecordType.Name != 'Residenziale'\n" +
                    "and Account.ITA_IFM_Type_Legal_Form__c = '1'\n" +
                    "limit 10";
    
    public final static String query_subentro_id34 = 
    		"Select NE__Status__c,\n" +
    		"NE__StartDate__c,\n" +
    		"NE__EndDate__c,\n" +
    		"Account.Name, \n" +
    		"Account.NE__Fiscal_code__c,\n" +
    		"Account.NE__VAT__c, \n" +
    		"Account.RecordType.Name,\n" +
    		"ACCOUNT.NE__Status__c,\n" +
    		"Account.ITA_IFM_Type_Legal_Form__c,\n" +
    		"ITA_IFM_Commodity__c,\n" +
    		"NE__Service_Point__r.ITA_IFM_POD__c,\n" +
    		"ITA_IFM_ConsumptionNumber__c,\n" +
    		"account.ITA_IFM_CustomerType__c\n" +
    		"From Asset \n" +
    		"where NE__Status__c = 'Active'\n" +
    		"and Account.RecordType.Name != 'Residenziale'\n" +
    		"and Account.ITA_IFM_Type_Legal_Form__c = '8'\n" +
    		"limit 10";
    
    public final static String query_subentro_id37 = 
    		"select\n" +
    		"ACCOUNT.NE__FISCAL_CODE__C,\n" +
    		"Account.Name,\n" +
    		"Account.RecordType.Name\n" +
    		"from Asset\n" +
    		"where Account.RecordType.Name = 'Residenziale'\n" +
    		"limit 100";

    public final static String recupera_CF_POD_per_VoltureConAccolloid22 =
    		////FR 16/07/2021 - query recupera_CF_POD_per_VoltureConAccolloid22  aggiornata con query Excel
    		"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name ,\r\n"
    		+ "NE__Service_Point__r.ITA_IFM_FlagResidence__c\r\n"
    		+ "FROM Asset\r\n"
    		+ "WHERE CreatedDate > 2017-01-01T00:00:00.000+0000\r\n"
    		+ "AND Account.RecordType.Name != 'Residenziale'\r\n"
    		+ "AND Account.ITA_IFM_CustomerType__c != 'Casa'\r\n"
    		+ "AND RecordType.Name = 'Commodity'\r\n"
    		+ "AND NE__Status__c = 'Active'\r\n"
    		+ "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\r\n"
    		+ "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\r\n"
    		+ "AND ( (NOT Account.NE__Fiscal_code__c LIKE '8%') AND ( NOT Account.NE__Fiscal_code__c  LIKE '9%'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\r\n"
    		+ "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n"
    		+ "LIMIT 10";
    		/*
    		//FR 16/07/2021 -R2 query recupera_CF_POD_per_VoltureConAccolloid22  aggiornata con query Excel
            "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
                    "FROM Asset\n" +
                    "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\n" +
                    "AND Account.RecordType.Name = 'Business'\n" +
                    "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
                    "AND RecordType.Name = 'Commodity'\n" +
                    "AND NE__Status__c = 'Active'\n" +
                    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
                    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
                    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
                    "LIMIT 10\n";*/

    public final static String recupera_CF_POD_per_VoltureConAccolloid25 = 
			"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
			"FROM Asset\n" +
			"WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
			"AND Account.RecordType.Name = 'Residenziale'\n" +
			"AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
			"AND RecordType.Name = 'Commodity'\n" +
			"AND NE__Status__c = 'Active'\n" +
			"AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
			"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
			"LIMIT 10";
    public final static String recupera_CF_POD_per_VoltureConAccolloid32 =
    "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
    "FROM Asset\n" +
    "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    "AND Account.RecordType.Name = 'Residenziale'\n" +
    "AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
    "AND RecordType.Name = 'Commodity'\n" +
    "AND NE__Status__c = 'Active'\n" +
    "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
    "and NE__Order_Config__r.NE__Order_Header__r.ITA_IFM_Quote__r.ITA_IFM_OfferType__c = 'Dual energy'\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    "LIMIT 20";

    
    public final static String recupera_CF_POD_per_VoltureConAccolloid26 = 
    		
"SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
"FROM Asset\n" +
"WHERE CreatedDate > 2021-03-01T00:00:00.000+0000\n" +
"AND Account.RecordType.Name = 'Business'\n" +
"AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
"AND RecordType.Name = 'Commodity'\n" +
"AND NE__Status__c = 'Active'\n" +
"AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
"AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
"AND Account.NE__Fiscal_code__C NOT IN ('05220831001', '80034390585', '02438750586' ,'93026890017', '05865511009')\n" +
"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
"LIMIT 10" ;
    
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid28 = 
    "SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\n" +
    "ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c \n" +
    "FROM  Asset\n" +
    "WHERE CreatedDate > 2020-03-01T00:00:00.000+0000\n" +
    "AND Account.RecordType.Name = 'Business'\n" +
    "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    "AND ITA_IFM_Commodity__c ='GAS'\n" +
    "AND RecordType.Name = 'Commodity'\n" +
    "AND NE__Status__c = 'Active'\n" +
    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    "AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\n" +
    "AND ITA_IFM_Sap_Contractual_Account__c != ''"+
    "AND ITA_IFM_SAP_Contract__c != ''"+
    "LIMIT 10\n";
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid29 = 
    	    "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
    	    "FROM Asset\n" +
    	    "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\n" +
    	    "AND Account.RecordType.Name = 'Business'\n" +
    	    "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    	    "AND RecordType.Name = 'Commodity'\n" +
    	    "AND NE__Status__c = 'Active'\n" +
    	    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +    	   
    	    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    	    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    	    "LIMIT 1";
    
    public final static String recupera_CF_POD_per_VoltureConAccolloid33 =
    "SELECT Account.NE__Fiscal_code__c, Account.Name, account.ITA_IFM_CustomerType__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Service_Use__c,\n" +
    "ITA_IFM_Commodity__c, NE__Status__c,  ITA_IFM_Resident__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE_BillingProf_Type__c, NE__ProdId__r.ITA_IFM_Regime__c\n" + 
    "FROM  Asset\n" +
    "WHERE CreatedDate > 2018-01-01T00:00:00.000+0000\n" +
    "AND Account.RecordType.Name = 'Business'\n" +
    "AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
    "AND ITA_IFM_Commodity__c ='GAS'\n" +
    "AND RecordType.Name = 'Commodity'\n" +
    "AND NE__Status__c = 'Active'\n" +
    "AND ITA_IFM_Service_Use__c ='uso diverso da abitazione'\n" +
    "AND NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c ='ITALGAS SPA'\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
    "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
    "AND NE__ProdId__r.ITA_IFM_Regime__c  != 'Fui Default'\n" +
    "LIMIT 10";
    
    
    
    
    public final static String query_subentro_id26_27_28_29 =
            "Select NE__Status__c,\n" +
                    "NE__StartDate__c,\n" +
                    "NE__EndDate__c,\n" +
                    "ITA_IFM_Service_Use__c,\n" +
                    "Account.Name, \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "Account.NE__VAT__c, \n" +
                    "Account.RecordType.Name,\n" +
                    "Account.ITA_IFM_Type_Legal_Form__c,\n" +
                    "ITA_IFM_Commodity__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
                    "ITA_IFM_ConsumptionNumber__c,\n" +
                    "NE__ProdId__r.Name\n" +
                    "From Asset \n" +
                    "where NE__Status__c != ''\n" +
                    "and Account.RecordType.Name != 'Impresa Individuale'\n" +
                    "and Account.RecordType.Name != 'Condominio'\n" +
                    "and Account.RecordType.Name != 'Pubblica Amministrazione'\n" +
                    "and Account.RecordType.Name != 'Residenziale'\n" +
                    "Limit 10";

    public final static String query_subentro_id8 =
            "SELECT \n" +
                    "NE__Status__c, NE__StartDate__c, NE__EndDate__c, ITA_IFM_Service_Use__c, Account.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name \n" +
                    "From Asset \n" +
                    "where NE__Status__c != '' \n" +
                    "and Account.RecordType.Name != 'Residenziale' \n" +
                    "and Account.RecordType.Name != 'Impresa Individuale' \n" +
                    "and Account.RecordType.Name != 'Condominio' \n" +
                    " and Account.NE__Fiscal_code__c not in ('00850380288','01238040180','03081860169','93029520660','02451860247','01821530514','03428640365', '03242700270', '06944790721','01524030622','03063430015') "+
                    "limit 10";
    public final static String query_subentro_id10 =
            "SELECT \n" +
                    "NE__Status__c, NE__StartDate__c, NE__EndDate__c, ITA_IFM_Service_Use__c, Account.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, ITA_IFM_Commodity__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__ProdId__r.Name \n" +
                    "From Asset \n" +
                    "where NE__Status__c != '' \n" +
                    "and Account.RecordType.Name = 'Condominio' \n" +
                    "limit 10";
    public final static String query_subentro_id11 =
            "Select NE__Status__c,\r\n" +
                    "NE__StartDate__c,\r\n" +
                    "NE__EndDate__c,\r\n" +
                    "ITA_IFM_Service_Use__c,\r\n" +
                    "Account.Name, \r\n" +
                    "Account.NE__Fiscal_code__c,\r\n" +
                    "Account.NE__VAT__c, \r\n" +
                    "Account.RecordType.Name,\r\n" +
                    "Account.ITA_IFM_Type_Legal_Form__c,\r\n" +
                    "ITA_IFM_Commodity__c,\r\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c,\r\n" +
                    "ITA_IFM_ConsumptionNumber__c,\r\n" +
                    "NE__ProdId__r.Name\r\n" +
                    "From Asset \r\n" +
                    "where NE__Status__c != ''\r\n" +
                    "and Account.RecordType.Name = 'Impresa Individuale'\r\n" +
                    "Limit 10";
    public final static String query_subentro_id13 =
            "Select NE__Status__c,\r\n" +
                    "NE__StartDate__c,\r\n" +
                    "NE__EndDate__c,\r\n" +
                    "ITA_IFM_Service_Use__c,\r\n" +
                    "Account.Name, \r\n" +
                    "Account.NE__Fiscal_code__c,\r\n" +
                    "Account.NE__VAT__c, \r\n" +
                    "Account.RecordType.Name,\r\n" +
                    "Account.ITA_IFM_Type_Legal_Form__c,\r\n" +
                    "ITA_IFM_Commodity__c,\r\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c,\r\n" +
                    "ITA_IFM_ConsumptionNumber__c,\r\n" +
                    "NE__ProdId__r.Name\r\n" +
                    "From Asset \r\n" +
                    "where NE__Status__c != ''\r\n" +
                    "and Account.RecordType.Name != 'Impresa Individuale'\r\n" +
                    "and Account.RecordType.Name != 'Condominio'\r\n" +
                    "and Account.RecordType.Name != 'Pubblica Amministrazione'\r\n" +
                    "and Account.RecordType.Name != 'Residenziale'\r\n" +
                    "Limit 10";

    public final static String query_subentro_residenziale = "select\r\n" +
            "Account.NE__Fiscal_code__c,\r\n" +
            "Account.Name,\r\n" +
            "Account.RecordType.Name\r\n" +
            "from account\r\n" +
            "where RecordType.Name = 'Residenziale'\r\n" +
            "limit 10";

    public final static String query_subentro_20_21 = 
            "Select NE__Status__c,\r\n" +
                    "NE__StartDate__c,\r\n" +
                    "NE__EndDate__c,\r\n" +
                    "ITA_IFM_Service_Use__c,\r\n" +
                    "Account.Name, \r\n" +
                    "Account.NE__Fiscal_code__c,\r\n" +
                    "Account.NE__VAT__c, \r\n" +
                    "Account.RecordType.Name,\r\n" +
                    "Account.ITA_IFM_Type_Legal_Form__c,\r\n" +
                    "ITA_IFM_Commodity__c,\r\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c,\r\n" +
                    "ITA_IFM_ConsumptionNumber__c,\r\n" +
                    "NE__ProdId__r.Name\r\n" +
                    "From Asset \r\n" +
                    "where NE__Status__c = 'Active'\r\n" +
                    "and Account.RecordType.Name = 'Residenziale'\r\n" +
                    "Limit 10";

    public final static String query_subentro_TipoPotenza = 
       		"Select ITA_IFM_Account_ID__r.ITA_IFM_Complete_Name__c,\r\n" +
            		"ITA_IFM_Account_ID__r.NE__Fiscal_code__c,\r\n" + 
            		"ITA_IFM_Case__r.CaseNumber,\r\n" + 
            		"ITA_IFM_Case__r.Status,\r\n" + 
            		"ITA_IFM_Case__r.ITA_IFM_SubStatus__c,\r\n" + 
            		"CreatedDate, Name, ITA_IFM_Status__c,\r\n" + 
            		"ITA_IFM_Record_Type_Name__c,\r\n" + 
            		"ITA_IFM_Subject__c,\r\n" + 
            		"ITA_IFM_POD__c,\r\n" + 
            		"ITA_IFM_PowerType__c \r\n" +
            		"FROM ITA_IFM_Case_Items__c \r\n" +
            		"WHERE ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@'";
    
    
    public final static String query_subentro_annullamento_fibra =
    		

			"SELECT ITA_IFM_Case__r.Subject,"
			+ " ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c, "
			+ "ITA_IFM_Case__r.CaseNumber, "
			+ "ITA_IFM_Case__r.Status, "
			+ "ITA_IFM_Case__r.ITA_IFM_SubStatus__c, "
			+ "ITA_IFM_Case__c, "
			+ "CreatedDate, Name,NE__Status__c, "
			+ "ITA_IFM_RecordTypeName__c, "
			+ "ITA_IFM_Commodity__c, "
			+ "ITA_IFM_POD__c, "
			+ "ITA_IFM_Phase__c, "
			+ "ITA_IFM_IDBPM__c, "
			+ "ITA_IFM_SAP_Status__c, "
			+ "ITA_IFM_Partner_Status__c FROM NE__OrderItem__c where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@'";

    		
    
    public final static String recupera_CF_per_PrimaAttivazione_id25 =
            "Select Account.NE__Fiscal_code__c\r\n" +
                    "FROM Asset WHERE\r\n" +
                    "Account.Type='Non Residenziale'\r\n" +
                    "AND Account.ITA_IFM_CustomerType__c  = 'Pubblica Amministrazione'\r\n" +
                    "AND ITA_IFM_Service_Use__c ='Illuminazione pubblica'\r\n" +
                    "LIMIT 5";
    public final static String get_query_293 = "select CaseNumber, Status, ITA_IFM_Email__c, ITA_IFM_MMP_Reason__c, ITA_IFM_Operation_Type__c, ITA_IFM_PEC__c, ContactMobile,ContactPhone, Origin\n"
            + "from Case\n"
            + "where AccountId = '0010Y00000FWBDUQA5' and CreatedDate = TODAY and ITA_IFM_MMP_Reason__c = 'VARIAZIONE ANAGRAFICA'";
    public final static String get_query_297 = "select CaseNumber, Status, ITA_IFM_Email__c, ITA_IFM_MMP_Reason__c, ITA_IFM_Operation_Type__c, ITA_IFM_PEC__c, ContactMobile,ContactPhone, Origin\n"
            + "from Case\n"
            + "where AccountId = '0010Y00000FWBDUQA5' and CreatedDate = TODAY and ITA_IFM_MMP_Reason__c = 'VARIAZIONE ANAGRAFICA'";
    public final static String get_query_295 = "select CaseNumber, Status, ITA_IFM_Email__c, ITA_IFM_MMP_Reason__c, ITA_IFM_Operation_Type__c, ITA_IFM_PEC__c, ContactMobile,ContactPhone, Origin\n"
            + "from Case\n"
            + "where Account.NE__Fiscal_code__c = 'SCRGPP60E27M088Z' and CreatedDate = TODAY and ITA_IFM_MMP_Reason__c = 'VARIAZIONE ANAGRAFICA'";
    public final static String get_query_165a = "select \n" +
            "AccountId, \n" +
            "ITA_IFM_CreatedDate__c , \n" +
            "Priority, \n" +
            "Status,\n" +
            "Origin, \n" +
            "Subject, \n" +
            "ITA_IFM_SubStatus__c \n" +
            "from \n" +
            "case \n" +
            "where \n" +
            "AccountId = '$ACCOUNT_ID$' \n" +
            "and \n" +
            "ITA_IFM_CreatedDate__c = TODAY \n" +
            "and \n" +
            "Subject = 'GESTIONE PRIVACY' \n" +
            "order by \n" +
            "ITA_IFM_CreatedDate__c \n" +
            "desc";
    public final static String get_query_165b = "Select \n" +
            "ITA_IFM_Account__c, \n" +
            "ITA_IFM_CreationDate__c, \n" +
            "ITA_IFM_Phone__c, \n" +
            "ITA_IFM_MarketingConsentPhone__c, \n" +
            "ITA_IFM_ThirdPartyConsentPhone__c, \n" +
            "ITA_IFM_Email__c, \n" +
            "ITA_IFM_MarketingConsentEmail__c ,\n" +
            "ITA_IFM_ThirdPartyConsentEmail__c \n" +
            "from \n" +
            "ITA_IFM_Privacy__c \n" +
            "where \n" +
            "ITA_IFM_Account__c ='$ACCOUNT_ID$'\n" +
            "and \n" +
            "ITA_IFM_CreationDate__c = TODAY \n" +
            "order by \n" +
            "ITA_IFM_CreationDate__c \n" +
            "desc";
    public final static String recupera_oi_order = "SELECT ITA_IFM_Case__r.Subject, ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber, ITA_IFM_Case__c,  ITA_IFM_Case__r.Status , ITA_IFM_Case__r.ITA_IFM_SubStatus__c,  \n" +
            "Name, NE__OrderId__r.RecordType.Name, ITA_IFM_POD__c, NE__Status__c,ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c ,ITA_IFM_RecordTypeName__c, CreatedDate \n" +
            "FROM NE__OrderItem__c \n" +
            "where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_RecordTypeName__c='@TIPO_OI@'";
    public final static String Query_338_a = "select AccountId, ITA_IFM_ABI_Description__c,ITA_IFM_ActivityStatus__c , ITA_IFM_Bank_Account_Holder_FiscalCode__c,  ITA_IFM_Category__c, ITA_IFM_Causale__c, ITA_IFM_CAB_Description__c, \n"
            + "ITA_IFM_CommunicationEmailAddress__c, ITA_IFM_Descrizione_Tripletta__c,ITA_IFM_IBAN__c, ITA_IFM_MMP_Reason__c,ITA_IFM_MMP_Reason_SDD__c, ITA_IFM_Operation_Type__c\n"
            + "from Case \n"
            + "where AccountId= '0010Y00000F8ifXQAR' and ITA_IFM_MMP_Reason__c = 'VARIAZIONE RID' and CreatedDate = TODAY";
    public final static String Query_338_b = "select Name, ITA_IFM_Account_ID__c ,ITA_IFM_ABI_Description__c,ITA_IFM_Current_Method_Of_Payment__c,ITA_IFM_Method_Of_Payment_Previous__c, ITA_IFM_New_IBAN__c,ITA_IFM_New_Method_Of_Payment__c,ITA_IFM_New_Method_Of_Payment_Name__c,ITA_IFM_POD_PDR__c, ITA_IFM_R2D_Status__c, ITA_IFM_SAP_Status__c,ITA_IFM_Service_Use__c,ITA_IFM_Status__c, ITA_IFM_Subject__c,ITA_IFM_UDB_Status__c \n"
            + "from ITA_IFM_Case_Items__c \n"
            + "where ITA_IFM_Account_ID__c = '0010Y00000F8ifXQAR' and CreatedDate = TODAY and ITA_IFM_Operation_Type__c = 'Variazione RID'";
    public final static String Query_338_c = "select CreatedDate , ITA_IFM_CancelStatus__c,ITA_IFM_OperationStatus__c, ITA_IFM_Status__c, RecordType.Name \n"
    		+ "from ITA_IFM_ServiceRequest__c\n"
    		+ "where ITA_IFM_Account__c ='0010Y00000F8ifXQAR' and CreatedDate = TODAY and RecordType.Name = 'Variazione RID - Modifica'";
    public final static String Query_338_d = "SELECT Account.NE__Fiscal_code__c,AccountId, ITA_IFM_Pod_Pdr__c , ITA_IFM_Commodity__c,NE_BillingProf_Type__c, ITA_IFM_BillingAddress__c, ITA_IFM_ConsumptionNumber__c \n"
            + "FROM Asset  \n"
            + "WHERE NE__Status__c = 'Active' and Account.NE__Fiscal_code__c !='' \n"
            + "and Account.RecordType.Name = 'Residenziale' \n"
            + "and AccountId IN (SELECT ITA_IFM_Account__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c !='In Attesa' and ITA_IFM_Status__c !='Working' \n"
            + "and ITA_IFM_OperationStatus__c != 'In attesa' \n"
            + "and ITA_IFM_OperationStatus__c != 'working') and NE_BillingProf_Type__c = 'RID'\n"
            + "limit 5";
    public final static String Query_338_e = "select  NE__Iban__c, ITA_IFM_Agency__c,ITA_IFM_BankName__c,ITA_IFM_Fiscal_Code__c , NE__First_Name__c, NE__Last_Name__c \n"
            + "from NE__Billing_Profile__c \n"
            + "where ITA_IFM_Fiscal_Code__c = 'HRNMNK46H63Z112Q'";
    public static final String Query_277 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c\n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !=''\n"
            + "and Account.RecordType.Name IN ('Residenziale')\n"
            + "and NE__Status__c = 'Active'\n"
            + "and CreatedDate > 2019-12-01T00:00:00Z\n"
            + "AND ProductCode = 'infoenelenergia'\n "
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'\n";
    public static final String Query_275 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c\n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !=''\n"
            + "and Account.RecordType.Name IN ('Residenziale')\n"
            + "and NE__Status__c = 'Active'\n"
            + "AND ProductCode = 'infoenelenergia'\n "
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'\n";
    public static final String Query_318_Annullato = "SELECT Name, ITA_IFM_Subject__c, CreatedDate , ITA_IFM_Status__c, ITA_IFM_POD_PDR__c, ITA_IFM_R2D_Status__c, ITA_IFM_SAP_Status__c, ITA_IFM_SEMPRE_Status__c, ITA_IFM_SAPSD_Status__c, ITA_IFM_UDB_Status__c\n"
            + "FROM ITA_IFM_Case_Items__c \n"
            + "WHERE ITA_IFM_POD_PDR__c = '$POD$'\n"
            + "AND ITA_IFM_Subject__c = 'VARIAZIONE VAS'\n"
            + "AND CreatedDate = Today\n";
    public static final String Query_278 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c \n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !='' \n"
            + "and Account.RecordType.Name IN ('Business') \n"
            + "and NE__Status__c = 'Active' \n"
            + "and CreatedDate > 2019-12-01T00:00:00Z \n"
            + "AND ProductCode = 'infoenelenergia' \n"
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'";

    public final static String recupera_CF_POD_per_VoltureConAccolloid12 =
            "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, Account.RecordType.Name\n" +
                    "    FROM Asset\n" +
                    "    WHERE CreatedDate > 2017-01-01T00:00:00.000+0000\n" +
                    "    AND Account.RecordType.Name = 'Business'\n" +
                    "    AND Account.ITA_IFM_CustomerType__c != 'Casa'\n" +
                    "    AND RecordType.Name = 'Commodity'\n" +
                    "    AND NE__Status__c = 'Active'\n" +
                    "    AND ITA_IFM_Service_Use__c ='Illuminazione pubblica'\n" +
                    "    AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
                    "    AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
                    "    LIMIT 20";


    public final static String query_voltura_con_accollo_id17 = "SELECT Account.NE__Fiscal_code__c, Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c\n" +
            "FROM Asset\n" +
            "WHERE CreatedDate > 2019-01-01T00:00:00.000+0000\n" +
            "AND Account.RecordType.Name = 'Residenziale'\n" +
            "AND Account.ITA_IFM_CustomerType__c = 'Casa'\n" +
            "AND RecordType.Name = 'Commodity'\n" +
            "AND NE__Status__c = 'Active'\n" +
            "AND ITA_IFM_Service_Use__c ='Uso Abitativo'\n" +
            "AND NE__Service_Point__r.ITA_IFM_SupplierId__c = 'ACEA'\n" +
            "AND Account.NE__Fiscal_code__c not in ('CRSFNC89M49H769B','CRCNNL73E64F839H','STNJNA02R41H501L','CRSFNC89M49H769B','CVDSMN69B45C933A') \n"+
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_OperationStatus__c IN ('In Attesa'))\n" +
            "AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\n" +
            "LIMIT 200";


    public static final String Query_259 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c \n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !='' \n"
            + "and Account.RecordType.Name IN ('Residenziale') \n"
            + "and NE__Status__c = 'Active' \n"
            + "and CreatedDate > 2019-12-01T00:00:00Z \n"
            + "AND ProductCode = 'infoenelenergia' \n"
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'";
    public static final String Query_263a = "SELECT id, CaseNumber\n"
            + "FROM Case \n"
            + "WHERE accountid ='$Account ID$' \n"
            + "and CreatedDate = TODAY \n"
            + "order by CreatedDate desc";
    public static final String Query_263b = "SELECT id, ITA_IFM_Subject__c,ITA_IFM_Status__c,ITA_IFM_POD_PDR__c\n"
            + "FROM ITA_IFM_Case_Items__c\n"
            + "WHERE ITA_IFM_Account_ID__c ='$Account ID$' \n"
            + "and CreatedDate = TODAY \n"
            + "order by CreatedDate desc";
    public static final String Query_264a = "SELECT id, CaseNumber\n"
            + "FROM Case \n"
            + "WHERE accountid ='$Account ID$' \n"
            + "and CreatedDate = TODAY \n"
            + "order by CreatedDate desc";
    public static final String Query_264b = "SELECT id, ITA_IFM_Subject__c,ITA_IFM_Status__c,ITA_IFM_POD_PDR__c\n"
            + "FROM ITA_IFM_Case_Items__c\n"
            + "WHERE ITA_IFM_Account_ID__c ='$Account ID$' \n"
            + "and CreatedDate = TODAY \n"
            + "order by CreatedDate desc";
    public static final String Query_264 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c \n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !='' \n"
            + "and Account.RecordType.Name IN ('Business') \n"
            + "and NE__Status__c = 'Active' \n"
            + "and CreatedDate > 2019-12-01T00:00:00Z \n"
            + "AND ProductCode = 'infoenelenergia' \n"
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'";
    public static final String Query_276 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c \n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !='' \n"
            + "and Account.RecordType.Name IN ('Business') \n"
            + "and NE__Status__c = 'Active' \n"
            + "and CreatedDate > 2019-12-01T00:00:00Z \n"
            + "AND ProductCode = 'infoenelenergia'  \n"
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'";
    public static final String Query_261 = "SELECT id, Account.NE__Fiscal_code__c, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c \n"
            + "FROM Asset \n"
            + "WHERE Account.NE__Fiscal_code__c !='' \n"
            + "and Account.RecordType.Name IN ('Business') \n"
            + "and NE__Status__c = 'Active' \n"
            + "and CreatedDate > 2019-12-01T00:00:00Z \n"
            + "AND ProductCode = 'infoenelenergia'  \n"
            + "AND ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'";
    public static final String Query_174 = "SELECT account.Name, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.RecordType.Name,Account.ITA_IFM_Type_Legal_Form__c, \n"
            + "ITA_IFM_Commodity__c,NE__Service_Point__r.ITA_IFM_POD__c,ITA_IFM_ConsumptionNumber__c,NE__ProdId__r.Name,ITA_IFM_BillingAddress__c \n"
            + "FROM Asset \n"
            + "WHERE ITA_IFM_ConsumptionNumber__c ='$Numero cliente$'";
    public static String GMAIL_ADDRESS_FOR_REGISTRATION = "testingcrmautomation@gmail.com";
	public static String recupera_Dati_Ricontrattualizzazione_121="SELECT\r\n" + 
			"id, RecordType.Name, NE__Fiscal_code__c, Account.ITA_IFM_Complete_Name__c, NE__VAT__c, Account.ITA_IFM_Company__c, NE__Status__c, Account.Name \r\n" + 
			"FROM Account\r\n" + 
			"where\r\n" + 
			"CreatedDate > 2020-01-01T00:00:00.000+0000\r\n" + 
			"AND id NOT IN (SELECT AccountId FROM Asset)\r\n" + 
			"LIMIT 20";
	public static String recupera_Dati_Ricontrattualizzazione_122="SELECT\r\n" + 
			"Account.Id, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c,\r\n" + 
			"Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name,\r\n" + 
			"NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c,\r\n" + 
			"ITA_IFM_Commodity__c,NE__Status__c,\r\n" + 
			"NE__Service_Point__r.ITA_IFM_SupplierId__c,\r\n" + 
			"NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c,\r\n" + 
			"NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE\r\n" + 
			"NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa'\r\n" + 
			"AND Account.NE__Fiscal_code__c not in('RSSMRA39L17C774B') "+
			"LIMIT 10";
	public static String recupera_Dati_Ricontrattualizzazione_119_1="SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" + 
			"AND Account.NE__Fiscal_code__c != 'TTTFNC77H12B350F'\r\n" + 
			"AND Account.Name != 'TOTTI FRANCESCO'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			"AND ITA_IFM_Commodity__c='ELE' "+
			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2021-01-01T00:00:00Z\r\n" + 
			"LIMIT 20";
	public static String recupera_Dati_Ricontrattualizzazione_119_1_Bus ="SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA' and ITA_IFM_EndDate_Forma_Contrattuale__c > TODAY) \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione' "+
			"AND ITA_IFM_Commodity__c='ELE' "+
			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 10";
	public static String recupera_Dati_Ricontrattualizzazione_119_1_Bus_GAS ="SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione' "+
			"AND ITA_IFM_Commodity__c='GAS' "+
			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 10";
	public static String recupera_Dati_Ricontrattualizzazione_119_2_Bus ="SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%Solo XTe Impresa%')\r\n" + 
			//"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione' "+
			"AND ITA_IFM_Commodity__c='ELE' "+
			//"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 20";
	public static String recupera_Dati_Ricontrattualizzazione_119_Pubblica_Amministrazione_Bonifico ="SELECT Account.Id, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%Solo XTe Impresa%')\r\n" + 
			//"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c = 'Pubblica Amministrazione' "+
			"AND ITA_IFM_Commodity__c='ELE' "+
			"AND NE_BillingProf_Type__c='Bonifico' "+
			//"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 20";
	public static String recupera_Dati_Ricontrattualizzazione_119_3="SELECT Account.Id "+//, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND Account.NE__Fiscal_code__c != 'TTTFNC77H12B350F'\r\n" + 
			"AND Account.Name != 'TOTTI FRANCESCO'\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			//"AND ITA_IFM_Commodity__c='ELE' "+
			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 50";
	
	public static String recupera_Dati_Ricontrattualizzazione_119_3_2="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
            "AND Account.NE__Fiscal_code__c != 'TTTFNC77H12B350F'\r\n" + 
            "AND Account.Name != 'TOTTI FRANCESCO'\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			"AND ITA_IFM_Commodity__c='GAS' "+
			"AND Account.Id in %s "+
//			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 30";
	public static String recupera_Dati_Ricontrattualizzazione_119_Gas_Res="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
            "AND Account.NE__Fiscal_code__c != 'TTTFNC77H12B350F'\r\n" + 
            "AND Account.Name != 'TOTTI FRANCESCO'\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			"AND ITA_IFM_Commodity__c='GAS' "+
//			"AND Account.Id in %s "+
//			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 30";
	public static String recupera_Dati_Ricontrattualizzazione_119_Res_Gas_RID="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
            "AND Account.NE__Fiscal_code__c != 'TTTFNC77H12B350F'\r\n" + 
            "AND Account.Name != 'TOTTI FRANCESCO'\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			"AND ITA_IFM_Commodity__c='GAS' "+
			"AND NE_BillingProf_Type__c='RID' "+
//			"AND Account.Id in %s "+
//			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 30";
	
	public static String recupera_Dati_Ricontrattualizzazione_119_3_2_1="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND Account.NE__Fiscal_code__c != 'TTTFNC77H12B350F'\r\n" + 
			"AND Account.Name != 'TOTTI FRANCESCO'\r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			"AND ITA_IFM_Commodity__c='ELE' "+
			"AND Account.Id in %s "+
//			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 30";
	
	public static String recupera_Dati_Ricontrattualizzazione_119_3_3="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo' "+
			"AND ITA_IFM_Commodity__c='ELE' "+
			"AND Account.Id = '%s' "+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 10";
	
	public static String recupera_Dati_Ricontrattualizzazione_119_4="SELECT Account.Id "+//, Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
			"AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione' \r\n" + 
			"AND ITA_IFM_Commodity__c='GAS' "+
			"AND NE__Service_Point__r.ITA_IFM_POD__c NOT IN ('IT002E7890001A')"+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 50";
	
	
	public static String recupera_Dati_Ricontrattualizzazione_119_4_1="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
            "AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione'\r\n" +
			"AND ITA_IFM_Commodity__c='GAS' "+
			"AND Account.Id in %s "+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 30";
	
	public static String recupera_Dati_Ricontrattualizzazione_119_4_2="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
            "AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione'\r\n"+
			"AND ITA_IFM_Commodity__c='GAS' "+
			"AND Account.Id = '%s' "+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 10";
	
	public static String recupera_Dati_Ricontrattualizzazione_119_4_3="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND id in (select ITA_IFM_Asset_FK__c from ITA_IFM_Contract_Agreement__c where ITA_IFM_Contract_Agreement_Status__c = 'ATTIVA') \r\n" + 
			"AND ITA_IFM_SAP_Contract__c != null\r\n" + 
			"AND ITA_IFM_Del_153_FLG__c = FALSE\r\n" +
			"AND ITA_IFM_SAP_Contractual_Account__c != null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
//			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%')\r\n" + 
            "AND Account.RecordType.Name = 'Business' AND ITA_IFM_Service_Use__c = 'Uso Diverso da Abitazione' AND Account.ITA_IFM_CustomerType__c != 'Pubblica Amministrazione'\r\n"+
			"AND ITA_IFM_Commodity__c='ELE' "+
			"AND Account.Id = '%s' "+
			"AND CreatedDate > 2020-01-01T00:00:00Z\r\n" + 
			"LIMIT 10";
	
	public static String recupera_Dati_Ricontrattualizzazione_123="SELECT Account.Id , Account.RecordType.Name, Account.ITA_IFM_Type_Legal_Form__c, Account.ITA_IFM_CustomerType__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c,Account.Name, NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_ConsumptionNumber__c, NE__Service_Point__r.ITA_IFM_Concatenated_Supply_Address__c, NE__ProdId__r.ITA_IFM_Regime__c, ITA_IFM_Commodity__c,NE__Status__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_SupplierCompanyName__c, NE__ProdId__r.Name, NE__ProdId__r.NE__Description__c, NE__StartDate__c,NE_BillingProf_Type__c, NE__BillingProf__r.NE__Iban__c\r\n" + 
			"FROM Asset\r\n" + 
			"WHERE NE__Status__c = 'Active'\r\n" + 
			"AND RecordType.Name = 'Commodity'\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n" + 
			"AND ITA_IFM_Commodity__c='ELE'\r\n" + 
			"AND ITA_IFM_SAP_Contract__c = null\r\n" + 
			"AND ITA_IFM_SAP_Contractual_Account__c = null \r\n" + 
			"AND NE__Service_Point__c NOT IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_ServiceRequest__c WHERE ITA_IFM_Status__c IN ('Working'))\r\n" + 
			"AND NE__Service_Point__c IN (SELECT ITA_IFM_ServicePoint__c FROM ITA_IFM_Case_Items__c WHERE ITA_IFM_Phase__c IN ('SWAKO','SWA2MIND','SWAKOECCRIP','SWAQUALCHK','SWARICSUPPORTO','SWAORDER2ASSET','SUBCASEITEMOKPREST','VCAOKR2D','VSAOKR2D','PACASEITEMOKPREST','AACASEITEMOKPREST')) \r\n" + 
			"AND NE__ProdId__r.ITA_IFM_Regime__c = 'Mercato Libero'\r\n" + 
			"AND Account.ITA_IFM_CustomerType__c = 'Casa' AND ITA_IFM_Service_Use__c ='Uso abitativo'\r\n "+
			"AND (NOT NE__ProdId__r.Name like '%OFFERTA STANDARD%') \r\n "+
			"AND CreatedDate > 2020-01-01T00:00:00Z \r\n" + 
			"LIMIT 20";
	
    public final static String get_data_for_registration_bsn_38 = "SELECT Account.NE__Fiscal_code__c,Account.NE__VAT__c, Account.ITA_IFM_Last_name__c, ITA_IFM_ConsumptionNumber__c,  Contact.FirstName, Contact.LastName, NE__Service_Point__r.ITA_IFM_POD__c,AccountId,NE__Status__c,RecordType.Name \n"
    		+ "FROM Asset \n"
    		+ "WHERE RecordType.Name = 'Commodity' \n "
    		+ "and NE__Status__c IN ('Active') \n"
    		+ "and Account.NE__Fiscal_code__c !='' and Account.NE__VAT__c!='' and Account.RecordType.Name = 'Business' \n "
    		+ "and ITA_IFM_Service_Use__c != 'Uso Abitativo' and (NOT Contact.FirstName  like '%2-%') \n"
    		+ "LIMIT 10 \n";
    public final static String get_data_for_registration_res_334 = "SELECT\n"
    		+ "Account.NE__Fiscal_code__c,account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, NE__Service_Point__r.ITA_IFM_POD__c, AccountId, ITA_IFM_ConsumptionNumber__c\n"
    		+ "FROM Asset \n"
    		+ "WHERE RecordType.Name = 'Commodity' \n"
    		+ "and NE__Status__c = 'Active'\n"
    		+ "and Account.NE__Fiscal_code__c !=''\n"
    		+ "and Account.RecordType.Name = 'Residenziale'\n"
    		+ "LIMIT 50";
    public final static String get_data_for_registration_332 = "SELECT \n"
    		+ "Account.NE__Fiscal_code__c,account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, NE__Service_Point__r.ITA_IFM_POD__c, AccountId, ITA_IFM_ConsumptionNumber__c \n"
    		+ "FROM Asset \n"
    		+ "WHERE RecordType.Name = 'Commodity'  \n"
    		+ "and NE__Status__c = 'Active' \n"
    		+ "and Account.NE__Fiscal_code__c !='' \n"
    		+ "and Account.RecordType.Name = 'Residenziale' \n"
    		+ "LIMIT 5";
    public final static String get_data_for_registration_333 = "SELECT\n"
    		+ "Account.NE__Fiscal_code__c,account.ITA_IFM_VAT__c, account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c, NE__Service_Point__r.ITA_IFM_POD__c, AccountId, ITA_IFM_ConsumptionNumber__c\n"
    		+ "FROM Asset \n"
    		+ "WHERE RecordType.Name = 'Commodity' \n"
    		+ "and NE__Status__c = 'Active'\n"
    		+ "and Account.RecordType.Name = 'Business'\n"
    		+ "LIMIT 50";
    public final static String get_data_for_registration_204 = "SELECT NE__Fiscal_code__c, Account.ITA_IFM_First_name__c,Account.ITA_IFM_Last_name__c \n "+
            "from Account \n " +
            "WHERE NE__Fiscal_code__c !='' and RecordType.Name = 'Residenziale'\n"+
            "and Id not in ( select AccountId from Asset) \n"+
            "LIMIT 50";
    public final static String query_292 = "select CaseNumber, Status, ITA_IFM_Email__c, ITA_IFM_MMP_Reason__c, ITA_IFM_Operation_Type__c, ContactMobile,ContactPhone, Origin\n"
    		+ "from Case \n"
    		+ "where AccountId = '$AccountId$' and CreatedDate = TODAY and ITA_IFM_MMP_Reason__c = 'VARIAZIONE ANAGRAFICA'";
    public final static String get_Query_271 = "SELECT Account.NE__Fiscal_code__c,RecordType.Name,  NE__Service_Point__r.ITA_IFM_POD__c,AccountId,NE__Status__c, NE__EndDate__c,ITA_IFM_ConsumptionNumber__c,account.ITA_IFM_First_name__c, Account.ITA_IFM_Last_name__c \n"
    		+ "FROM Asset \n"
    		+ "WHERE RecordType.Name = 'Commodity' \n "
    		+ "and NE__Status__c IN ('Disconnected') \n"
    		+ "and Account.NE__Fiscal_code__c !='' and Account.RecordType.Name = 'Residenziale' \n "
    		+ "and NE__EndDate__c >    LAST_YEAR \n"
    		+ "LIMIT 100";

    public final static String query_115 = "SELECT ITA_IFM_Subject__c, ITA_IFM_Case__r.CaseNumber, ITA_IFM_Case__r.Status, ITA_IFM_Case__r.ITA_IFM_SubStatus__c, ITA_IFM_POD__c, Name, ITA_IFM_Status__c, ITA_IFM_R2D_Status__c,ITA_IFM_SAP_Status__c,ITA_IFM_SAP_StatusCode__c,ITA_IFM_SAP_StatusDescription__c, ITA_IFM_SEMPRE_Status__c,ITA_IFM_SEMPRE_StatusCode__c,ITA_IFM_SEMPRE_StatusDescription__c \r\n" + 
    		"FROM ITA_IFM_Case_Items__c\r\n" + 
    		"where ITA_IFM_Case__r.CaseNumber = '@NUMERO_RICHIESTA@'";
    
    public final static String query_105 ="select ITA_IFM_OrderHeader__r.ITA_IFM_Quote__r.ITA_IFM_Case__r.CaseNumber, ITA_IFM_OrderHeader__r.NE__OrderStatus__c,\r\n" + 
    		"id, ITA_IFM_OrderHeader__c, CreatedDate, ITA_IFM_Status__c, ITA_IFM_Change_Status__c, ITA_IFM_Change_Status_Code__c, ITA_IFM_Change_Status_Description__c, ITA_IFM_Request__c\r\n" + 
    		"from ITA_IFM_Order_Management_Log__c\r\n" + 
    		"where ITA_IFM_OrderHeader__r.ITA_IFM_Quote__r.ITA_IFM_Case__r.CaseNumber = '@NUMERO_RICHIESTA@'";
    
    public final static String query_104="SELECT Id, ITA_IFM_Commodity__c, NE__Status__c, NE__StartDate__c, NE__EndDate__c, Name, NE__Service_Point__r.ITA_IFM_POD__c, NE__Service_Point__r.ITA_IFM_Commodity_Use__c, NE__Service_Point__r.ITA_IFM_SupplierId__c, NE__Service_Point__r.ITA_IFM_Address__c, ITA_IFM_Usage_Type__c, ITA_IFM_ConsumptionNumber__c, Account.NE__Fiscal_code__c, Account.NE__VAT__c, Account.ITA_IFM_SAP_Primary_BP__c, \r\n" + 
    		"ITA_IFM_SAP_Contract__c, ITA_IFM_Sap_Contractual_Account__c, NE__BillingProf__r.NE__Payment__c, ITA_IFM_BackupMOP__c, NE__BillingProf__r.NE__Iban__c, ITA_IFM_PreviousOrder__c, NE__ProdId__r.Name\r\n" + 
    		"FROM Asset\r\n" + 
    		"WHERE RecordType.Name = 'Commodity'\r\n" + 
    		"AND NE__Service_Point__r.ITA_IFM_POD__c = '@POD@'";
    
    public final static String query_120="SELECT LastModifiedDate, ITA_IFM_StartDate_Forma_Contrattuale__c, ITA_IFM_EndDate_Forma_Contrattuale__c, ITA_IFM_Contract_Agreement_Status__c, ITA_IFM_Asset_FK__r.NE__Service_Point__r.ITA_IFM_POD__c, ITA_IFM_Asset_FK__r.NE__Status__c, ITA_IFM_ProdId__r.Name\r\n" + 
    		"FROM ITA_IFM_Contract_Agreement__c\r\n" + 
    		"WHERE ITA_IFM_Asset_FK__r.NE__Service_Point__r.ITA_IFM_POD__c = '@POD@' \r\n" + 
    		"ORDER BY ITA_IFM_StartDate_Forma_Contrattuale__c, ITA_IFM_EndDate_Forma_Contrattuale__c";
   
    public final static String get_query_260A = "select id ,CreatedDate,  ITA_IFM_Email__c, ITA_IFM_Mobile_Phone__c,  ITA_IFM_Status__c, ITA_IFM_Account__r.NE__Fiscal_code__c \n "
    		+ "from ITA_IFM_Data_Contact_Cert_History__c \n"
    		+ "where ITA_IFM_Account__c = '$Account_Id$'  and ITA_IFM_Email__c = '$email$' ";
    
    public final static String get_query_260B ="select id,CreatedDate,  CaseNumber, Status,ITA_IFM_SubStatus__c, ITA_IFM_Email__c, ITA_IFM_Operation_Type__c\n"
    		+ "from Case \n"
    		+ "where AccountId = '$Account_Id$'  and Subject = 'ATTIVAZIONE VAS' and ITA_IFM_Email__c !=''\n"
    		+ "order by CreatedDate desc";
    
    public final static String get_query_260C = "select ITA_IFM_AccountType__c,ITA_IFM_Subject__c , ITA_IFM_POD_PDR__c,ITA_IFM_R2D_Status__c, ITA_IFM_R2R_Status__c, ITA_IFM_SAP_Status__c, ITA_IFM_SAPFICA_Status__c,ITA_IFM_SAPMM_Status__c, ITA_IFM_SAPSD_Status__c, ITA_IFM_SEMPRE_Status__c, ITA_IFM_UDB_Status__c,ITA_IFM_NEC_Status__c \n"
    		+ "from ITA_IFM_Case_Items__c \n"
    		+ "where ITA_IFM_Case__c ='$Id$' and ITA_IFM_Subject__c = 'ATTIVAZIONE VAS' ";
    
    public final static String recupera_attivita_scarto_sap =
    		"select CreatedDate, id, ITA_IFM_Number__c, Name, ITA_IFM_Status__c, ITA_IFM_Tipo_Attivita__c, ITA_IFM_Specifica__c, ITA_IFM_Causale_Contatto__c, ITA_IFM_Descrizione__c, ITA_IFM_Commodity__c ,ITA_IFM_FromCompetence__c, ITA_IFM_Assegnato_A__c, ITA_IFM_Case__r.CaseNumber \n"+
    				"FROM wrts_prcgvr__Activity__c \n"+
    				"where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_Tipo_Attivita__c='Scarti SAP' \n";
    
    public final static String recupera_attivita_scarto_sap_commodity =
    		"select CreatedDate, id, ITA_IFM_Number__c, Name, ITA_IFM_Status__c, ITA_IFM_Tipo_Attivita__c, ITA_IFM_Specifica__c, ITA_IFM_Causale_Contatto__c, ITA_IFM_Descrizione__c, ITA_IFM_Commodity__c ,ITA_IFM_FromCompetence__c, ITA_IFM_Assegnato_A__c, ITA_IFM_Case__r.CaseNumber \n"+
    				"FROM wrts_prcgvr__Activity__c \n"+
    				"where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_Tipo_Attivita__c='Scarti SAP' and ITA_IFM_Commodity__c='@TIPOLOGIA_POD@'\n";
    
    public final static String recupera_attivita_scarto_sempre =
    		"select CreatedDate, id, ITA_IFM_Number__c, Name, ITA_IFM_Status__c, ITA_IFM_Tipo_Attivita__c, ITA_IFM_Specifica__c, ITA_IFM_Causale_Contatto__c, ITA_IFM_Descrizione__c, ITA_IFM_Commodity__c ,ITA_IFM_FromCompetence__c, ITA_IFM_Assegnato_A__c, ITA_IFM_Case__r.CaseNumber \n"+
    				"FROM wrts_prcgvr__Activity__c \n"+
    				"where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_Tipo_Attivita__c='Scarti SEMPRE' \n";
    
    public final static String recupera_attivita_scarto_sempre_commodity =
    		"select CreatedDate, id, ITA_IFM_Number__c, Name, ITA_IFM_Status__c, ITA_IFM_Tipo_Attivita__c, ITA_IFM_Specifica__c, ITA_IFM_Causale_Contatto__c, ITA_IFM_Descrizione__c, ITA_IFM_Commodity__c ,ITA_IFM_FromCompetence__c, ITA_IFM_Assegnato_A__c, ITA_IFM_Case__r.CaseNumber \n"+
    				"FROM wrts_prcgvr__Activity__c \n"+
    				"where ITA_IFM_Case__r.CaseNumber = '@RICHIESTA@' and ITA_IFM_Tipo_Attivita__c='Scarti SEMPRE' and ITA_IFM_Commodity__c='@TIPOLOGIA_POD@'\n";



    public final static String query_subentro_id29 =
            "Select NE__Status__c,\n" +
                    "NE__StartDate__c,\n" +
                    "NE__EndDate__c,\n" +
                    "ITA_IFM_Service_Use__c,\n" +
                    "Account.Name, \n" +
                    "Account.NE__Fiscal_code__c,\n" +
                    "Account.NE__VAT__c, \n" +
                    "Account.RecordType.Name,\n" +
                    "Account.ITA_IFM_Type_Legal_Form__c,\n" +
                    "ITA_IFM_Commodity__c,\n" +
                    "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
                    "ITA_IFM_ConsumptionNumber__c,\n" +
                    "NE__ProdId__r.Name\n" +
                    "From Asset \n" +
                    "where NE__Status__c != ''\n" +
                    "and Account.RecordType.Name != 'Impresa Individuale'\n" +
                    "and Account.RecordType.Name != 'Condominio'\n" +
                    "and Account.RecordType.Name != 'Pubblica Amministrazione'\n" +
                    "and Account.RecordType.Name != 'Residenziale'\n" +
                    "Limit 10";

    public final static String query_subentro_id16 =
            "Select NE__Status__c,\n" +
            "NE__StartDate__c,\n" +
            "NE__EndDate__c,\n" +
            "ITA_IFM_Service_Use__c,\n" +
            "Account.Name,\n" +
            "Account.NE__Fiscal_code__c,\n" +
            "Account.NE__VAT__c,\n" +
            "Account.RecordType.Name,\n" +
            "Account.ITA_IFM_Type_Legal_Form__c,\n" +
            "ITA_IFM_Commodity__c,\n" +
            "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
            "ITA_IFM_ConsumptionNumber__c,\n" +
            "NE__ProdId__r.Name\n" +
            "From Asset \n" +
            "where NE__Status__c != '' \n" +
            "and Account.RecordType.Name = 'Residenziale'\n" +
            "Limit 10";

    
    public final static String query_subentro_id30 = 
    		"Select\n" +
    		"NE__Status__c,\n" + 
    		"ITA_IFM_Commodity__c,\n" +
    	    "Account.Name,\n" + 
    		"Account.NE__Fiscal_code__c,\n" +
    		"NE__Service_Point__r.ITA_IFM_POD__c,\n" +
    		"NE__Service_Point__r.ITA_IFM_ZIPCode__c\n" +
    		"From Asset \n" +
    		"Where \n" +
    		"NE__Status__c != 'Disconnected' \n" +
    		"AND ITA_IFM_Commodity__c = 'GAS'\n" +
    		"Limit 20";
    
    
    /*query script subentro_id31*/
     
    //query per il cf
    public final static String ricerca_anagrafica =
            "select \n" +
            "Account.NE__Fiscal_code__c,\n" +
            "Account.Name, \n" +
            "Account.RecordType.Name\n" +
            "\n" +
            "from account\n" +
            "where RecordType.Name = 'Residenziale'\n" +
            "limit 10";

    /*Da cablare all'interno di un moduletto*/

    //query per il pod
    public final static String ricerca_pod =
            "Select\n" +
            "NE__Status__c, \n" +
            "ITA_IFM_Commodity__c,\n" +
            "Account.Name, \n" +
            "Account.NE__Fiscal_code__c,\n" +
            "NE__Service_Point__r.ITA_IFM_POD__c,\n" +
            "NE__Service_Point__r.ITA_IFM_ZIPCode__c\n" +
            "\n" +
            "From Asset \n" +
            "\n" +
            "Where \n" +
            "NE__Status__c != 'Disconnected' \n" +
            "AND ITA_IFM_Commodity__c = 'ELE'\n" +
            "and NE__Service_Point__r.ITA_IFM_POD__c like 'IT002E%'\n" +
            "limit 10";
    
    public final static String getQuery_98a = "select Id,CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, ITA_IFM_AccountFirstName__c,ITA_IFM_AccountLastName__c,Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c \n"
    		+ "from Case \n"
    		+ "where account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and ITA_IFM_Process__c = 'SWITCH ATTIVO'  and CreatedDate = TODAY";

    public final static String getQuery_98b = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c \n"
    		+ "from ITA_IFM_Case_Items__c \n"
    		+ "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";
    
    public final static String getQuery_98c = "select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c,ITA_IFM_BillingProfilePayment__c \n"
    		+ "from NE__Quote__c \n"
    		+ "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";
    
    public final static String getQuery_99a = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c \n"
    		+ "from ITA_IFM_Case_Items__c \n"
    		+ "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";

    public final static String getQuery_99b = "select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c \n"
    		+ "from NE__Quote__c \n"
    		+ "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";
    
    public final static String getQuery_101a  = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c \n"
    + "from ITA_IFM_Case_Items__c \n"
       + "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";
    
    public final static String getQuery_101b = "select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c \n"
   +" from NE__Quote__c \n"
   +" where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";
    

   public final static String getQuery_100a = "select Id,CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c \n"
+"from Case \n"
+"where account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";
   

   public final static String getQuery_100b = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c \n"
+"from ITA_IFM_Case_Items__c \n"
+"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";
   
   public final static String getQuery_100c = "select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c,ITA_IFM_BillingProfilePayment__c \n"
  +" from NE__Quote__c  \n"
   +"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";

   public final static String getQuery_102a = "select Id,CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c \n"
+"from Case \n"
+"where account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY and ITA_IFM_Process__c = 'SWITCH ATTIVO' ";
   

   public final static String getQuery_102b = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c \n"
+"from ITA_IFM_Case_Items__c \n"
+"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";
   
   public final static String getQuery_102c = "select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c,ITA_IFM_BillingProfilePayment__c \n"
  +" from NE__Quote__c  \n"
   +"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";
   
   
   public final static String getQuery_103a = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, \n"
		   +"from ITA_IFM_Case_Items__c \n"
		   +"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY  ";
		   
		     
 public final static String getQuery_103b =" select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, \n"
		    +" from NE__Quote__c \n"
		     + "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";

 public final static String getQuery_109a = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_NewOrderItem_Status__c \n"
		   +"from ITA_IFM_Case_Items__c \n"
		   +"where  ITA_IFM_FiscalCode__c = 'MSSBRN64T43G005E' and CreatedDate = TODAY  ";
		   
		     
public final static String getQuery_109b =" select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, \n"
		    +" from INE__Quote__c  \n"
		     + "where  ITA_IFM_FiscalCode__c = 'MSSBRN64T43G005E' and CreatedDate = TODAY ";


public final static String getQuery_104b = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c, ITA_IFM_POD__c \n"
+"from case \n"
+"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";

public final static String getQuery_104a = "select Id,CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c\n"
+" from case  \n"
+"where  account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";

     
public final static String getQuery_104c  ="select Id,CreatedDate, ITA_IFM_ActivationReason__c,ITA_IFM_BillingProfilePayment__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, ITA_IFM_BollettaWeb_Email__c, ITA_IFM_Email__c, ITA_IFM_Phone__c, ITA_IFM_MobilePhone__c\n"
+"from NE__Quote__c\n"
+"where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";


public final static String getQuery_104d ="select Id,CreatedDate, ITA_IFM_Agency__c, ITA_IFM_Fiscal_Code__c, ITA_IFM_Payment_New__c,NE__Payment__c, ITA_IFM_Type_Of_Payment__c, NE__Iban__c \n"
+" from NE__Billing_Profile__c\n"
+"where NE__Account__c = '0011l00000rQWK9AAO' and CreatedDate = TODAY";



public final static String getQuery_105a = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c\n"
		   +"from ITA_IFM_Case_Items__c \n"
		   +"where ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";
		     

public final static String getQuery_105b= "select Id,ITA_IFM_ActivationReason__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c\n"
+"from NE__Quote__c \n"
+" where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";


public final static String getQuery_116a= "select Id,CreatedDate , CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c"
+"from Case \n"
+" where  account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";

public final static String getQuery_116b= "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c, ITA_IFM_POD__c"
+"from ITA_IFM_Case_Items__c \n"
+" where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";

public final static String getQuery_116c= "select Id,CreatedDate, ITA_IFM_ActivationReason__c,ITA_IFM_BillingProfilePayment__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, ITA_IFM_BollettaWeb_Email__c, ITA_IFM_Email__c, ITA_IFM_Phone__c, ITA_IFM_MobilePhone__c"
+"from NE__Quote__c  \n"
+" where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";

public final static String getQuery_116d= "select Id,CreatedDate , CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c"
+"from Case \n"
+" where  account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";


public final static String getQuery_115a= "select Id,CreatedDate , CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c"
+"from Case \n"
+" where  account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' , CreatedDate = TODAY and  ITA_IFM_Process__c = 'Subentro' order by CreatedDate desc";

public final static String getQuery_115b= "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c, ITA_IFM_POD__c"
+"from ITA_IFM_Case_Items__c \n"
+" where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY order by CreatedDate desc";

public final static String getQuery_119a = "select Id,CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c\n"
		+ "from Case \n"
		+ "where account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and ITA_IFM_Process__c = 'SWITCH ATTIVO'  and CreatedDate = TODAY";


public final static String getQuery_119b = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c \n"
		+ "from ITA_IFM_Case_Items__c \n"
		+ "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";




public final static String getQuery_119c = "select Id,CreatedDate, ITA_IFM_ActivationReason__c,ITA_IFM_BillingProfilePayment__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, ITA_IFM_BollettaWeb_Email__c, ITA_IFM_Email__c, ITA_IFM_Phone__c, ITA_IFM_MobilePhone__c"

		+ "from NE__Quote__c \n"
		+ "where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY ";




public final static String getQuery_119d = "select Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c, ITA_IFM_POD__c"
		+ "from ITA_IFM_Case_Items__c \n"
		+ "where  ID= 'step ID column value 17'  and CreatedDate = TODAY";




public final static String getQuery_115c= "select Id,CreatedDate, ITA_IFM_ActivationReason__c,ITA_IFM_BillingProfilePayment__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, ITA_IFM_BollettaWeb_Email__c, ITA_IFM_Email__c, ITA_IFM_Phone__c, ITA_IFM_MobilePhone__c"
+"from NE__Quote__c  \n"
+" where  ITA_IFM_FiscalCode__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY order by CreatedDate desc";

public final static String getQuery_115d= "select Id,CreatedDate , CaseNumber, AccountId, Status, ITA_IFM_Process__c, ITA_IFM_ProdName__c, account.NE__Fiscal_code__c,   ITA_IFM_CommunicationEmailAddress__c, Origin,ITA_IFM_UserChannel__c,ITA_IFM_UserChannelCode__c,ITA_IFM_UserSubchannel__c,ITA_IFM_UserSubchannelCode__c"
+"from Case \n"
+" where  account.NE__Fiscal_code__c = 'PRKPTR72B03H502P' and CreatedDate = TODAY";

public final static String getQuery_119e = "Id,Name, ITA_IFM_Account_ID__c , ITA_IFM_AccountType__c, ITA_IFM_Commodity__c, ITA_IFM_FiscalCode__c, ITA_IFM_Status__c, ITA_IFM_POD__c"
		+ "from ITA_IFM_Case_Items__c \n"
		+ "where  ID= 'step ID column value 17'  and CreatedDate = TODAY";


public final static String getQuery_119f = "select Id,CreatedDate, ITA_IFM_ActivationReason__c,ITA_IFM_BillingProfilePayment__c, NE__Status__c,ITA_IFM_OfferType__c, Name, ITA_IFM_Quote_Number__c, ITA_IFM_Operation_Type__c,ITA_IFM_AccountType__c, ITA_IFM_BollettaWeb_Email__c, ITA_IFM_Email__c, ITA_IFM_Phone__c, ITA_IFM_MobilePhone__c\n"
		+ "from NE__Billing_Profile__c \n"
		+ "where id = 'colonna and ITA_IFM_Billing_Profile__c 'nello step precedente";



public final static String query_VCA_OrderItemdaOrderItemId=" SELECT ITA_IFM_Case__r.Subject, id,ITA_IFM_Case__r.ITA_IFM_Sub_Subject__c,ITA_IFM_Case__r.CaseNumber,ITA_IFM_Case__c, ITA_IFM_Case__r.Status,ITA_IFM_Case__r.ITA_IFM_SubStatus__c, Name, \r\n"
		+ "NE__OrderId__r.RecordType.Name,NE__Status__c, ITA_IFM_IDBPM__c, ITA_IFM_POD__c, ITA_IFM_R2D_Status__c,ITA_IFM_R2DOutcomeStatus__c,ITA_IFM_SAP_Status__c,ITA_IFM_SAP_StatusCode__c,ITA_IFM_SAP_StatusDescription__c, ITA_IFM_SEMPRE_Status__c,ITA_IFM_SEMPRE_StatusCode__c,ITA_IFM_SEMPRE_StatusDescription__c, ITA_IFM_UDB_Status__c, \r\n"
		+ "ITA_IFM_Commodity__c, ITA_IFM_Customer_Type_Calc__c,ITA_IFM_RecordTypeName__c, NE__BillingProfId__r.NE__Payment__c,ITA_IFM_Temporary_Billing__c,ITA_IFM_Case__r.ITA_IFM_Operation_Type__c\r\n"
		+ "FROM NE__OrderItem__c \r\n"
		+ "where \r\n"
		+ "ITA_IFM_RecordTypeName__c='Commodity' \r\n"
		+ "and NE__OrderId__r.RecordType.Name = 'Order' \r\n"
		+ "and name ='@ORDER_ITEM_ID@'\r\n "
		+ "AND ITA_IFM_POD__c='@POD@'"
		+ "order by createdDate";


  
}