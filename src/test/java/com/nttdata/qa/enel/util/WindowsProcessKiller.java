package com.nttdata.qa.enel.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class WindowsProcessKiller {

	// command used to get list of running task
	private static final String TASKLIST = "tasklist";
	// command used to kill a task
	private static final String KILL = "taskkill /IM ";

	public static boolean isProcessRunning(String serviceName) {

		try {
			Process pro = Runtime.getRuntime().exec(TASKLIST);
			BufferedReader reader = new BufferedReader(new InputStreamReader(pro.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				// //System.out.println(line);
				if (line.startsWith(serviceName)) {
					return true;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static void killProcess(String serviceName) {

		try {
			Runtime.getRuntime().exec(KILL + serviceName + " /F");
			//System.out.println(serviceName+" killed successfully!");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void killChrome() {
		boolean isRunning = WindowsProcessKiller.isProcessRunning("chrome.exe");
		if (isRunning) {
			WindowsProcessKiller.killProcess("chrome.exe");
		}
		
		isRunning = WindowsProcessKiller.isProcessRunning("chromedriver.exe");
		if (isRunning) {
			WindowsProcessKiller.killProcess("chromedriver.exe");
		}
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		public static void killIE() {
			boolean isRunning = WindowsProcessKiller.isProcessRunning("iexplore.exe");
			if (isRunning) {
				WindowsProcessKiller.killProcess("iexplore.exe");
			}
			
			isRunning = WindowsProcessKiller.isProcessRunning("IEDriverServer.exe");
			if (isRunning) {
				WindowsProcessKiller.killProcess("IEDriverServer.exe");
			}
			
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}



}

