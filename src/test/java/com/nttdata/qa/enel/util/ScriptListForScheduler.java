package com.nttdata.qa.enel.util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ScriptListForScheduler {

	public static void main(String[] args) throws Exception {
		
		String apiPath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\nttdata\\qa\\enel\\colla\\api";
		File apiFolder = new File(apiPath);
		File[] folderContent = apiFolder.listFiles();
		Path targetFile = Paths.get("C:\\Users\\gaetavi\\Desktop\\apiList.txt");
		String packageName = "com.nttdata.qa.enel.colla.api";
		
		if(Files.exists(targetFile)){
			Files.delete(targetFile);
			Files.createFile(targetFile);
		}else
			Files.createFile(targetFile);
		
		for(File f : folderContent){
			Files.write(targetFile, (packageName+"."+f.getName().replace(".java", "")+"\n").getBytes(), StandardOpenOption.APPEND);
		}
	}

}
