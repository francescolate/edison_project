package com.nttdata.qa.enel.util;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Call;
import okhttp3.FormBody;

/**
 * This class allows us to call WebPortal API (Heroku), letting us check or manipulate accounts data.
 * 
 * @author
 * NTT Automation Team
 * 
 * @version
 * 1.0
 */
public final class APIService {
	
	//It calls the API for getting the token to be used within other APIs.
	public static String getToken() throws Exception{
		Thread.currentThread().sleep(1000);
		OkHttpClient client = new OkHttpClient();
		RequestBody formBody = new FormBody.Builder().add("grant_type", "client_credentials").build(); 
	    Request request = new Request.Builder().url(Costanti.api_url_getToken).addHeader("Authorization", "Basic dURVZml3S01sZFM2MWNJZjU2dXg3dDVyYWtFYTpHZ2pySkxGZ1JIdkpCelUxZTM4UzUxdHRpM0Fh").addHeader("Content-Type", "application/x-www-form-urlencoded").post(formBody).build(); 
	    Response response = client.newCall(request).execute();
	    if (!response.isSuccessful())
	        throw new Exception("Unexpected code " + response);
	    HashMap<String,String> result = new ObjectMapper().readValue(response.body().string(), HashMap.class);
	    return result.get("access_token");
	}
	
	//It sends a REST GET request toward a URL, passing some parameters.
	private static JSONObject sendGetRequest(String apiURL, String param) throws Exception{
		Thread.currentThread().sleep(2000);
		OkHttpClient client = new OkHttpClient();
	    Request request = new Request.Builder().url(apiURL+param)
	    		.addHeader("Authorization", "Bearer "+getToken())
	    		.addHeader("TID","7f24e2c9-7cce-4c4f-a25a-c112cbdb6130")
	    		.addHeader("SID","8b8fdc68-ee96-11e9-81b4-2a2ae2culo19")
	    		.addHeader("SOURCE_CHANNEL","EE")
	    		.addHeader("ENEL_ID", "")
	    		.addHeader("TOUCHPOINT", "WEB")
	    		.addHeader("CLIENT_IP", "10.10.11.11")
	    		.addHeader("Content-Type", "application/json")
	    		.build();
	    Response response = client.newCall(request).execute();
	    System.out.println("response --> " + response);
	    String a = response.body().string();
	    System.out.println("aaa --> " + a);
	    JSONObject jsonObject = new JSONObject(a);
	    int code = (int)jsonObject.get("code");
	    if (code != 200)   // && code != 500)    => this comment has been added due to Registration issue. Some API Restfull requests were returning 500 as result code that was affecting the registration process. 2021-02-18
	        throw new Exception("Unexpected code " + response);
	    
	    if(code==200)
	    	return jsonObject;
	    else
	    	return null;
	}
	
	//It sends a REST GET request toward a URL, passing some parameters and headers.
		private static JSONObject sendGetRequest(String apiURL, String param, HashMap<String, String> headers) throws Exception{
			Thread.currentThread().sleep(2000);
			OkHttpClient client = new OkHttpClient();
		    Builder builder = new Request.Builder().url(apiURL+param);
		    for(Entry<String, String> entry : headers.entrySet())
		    	builder.addHeader(entry.getKey(), entry.getValue());
		    Request request = builder.build();
		    Response response = client.newCall(request).execute();
		    JSONObject jsonObject = new JSONObject(response.body().string());
		    int code = (int)jsonObject.get("code");
		    if (code != 200 && code != 500)
		        throw new Exception("Unexpected code " + response);
		    
		    if(code==200)
		    	return jsonObject;
		    else
		    	return null;
		}
	
	//It sends a REST POST request toward a URL, passing some parameters. These parameters are sent in JSON format.
	private static JSONObject sendPostRequest(String apiURL, String json) throws Exception{
		Thread.currentThread().sleep(2000);
		OkHttpClient client = new OkHttpClient();
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody body = RequestBody.create(json, JSON);
	    Request request = new Request.Builder().url(apiURL)
	    		.addHeader("Authorization", "Bearer "+getToken())
	    		.addHeader("TID","7f24e2c9-7cce-4c4f-a25a-c112cbdb6130")
	    		.addHeader("SID","7f24e2c9-7cce-4c4f-a25a-c112cbdb6131")
	    		.addHeader("SOURCE_CHANNEL","EE")
	    		.addHeader("ENEL_ID", "")
	    		.addHeader("TOUCHPOINT", "WEB")
	    		.addHeader("CLIENT_IP", "10.10.11.11")
	    		.addHeader("Content-Type", "application/json")
	    		.post(body)
	    		.build();
	    Response response = client.newCall(request).execute();
	    Thread.sleep(1000);
	    JSONObject jsonObject = new JSONObject(response.body().string());
	    int code = (int)jsonObject.get("code");
	    if (code != 200 && code != 500 && code != 20000)
	        throw new Exception("Unexpected code " + response);
	    
	    if(code==200 || code==20000)
	    	return jsonObject;
	    else
	    	return null;
	}
	
	//It sends a REST DELETE request toward a URL, passing some parameters.
	private static JSONObject sendDeleteRequest(String apiURL, String param) throws Exception{
		Thread.currentThread().sleep(2000);
		OkHttpClient client = new OkHttpClient();
	    Request request = new Request.Builder().url(apiURL+param)
	    		.addHeader("Authorization", "Bearer "+getToken())
	    		.addHeader("TID","7f24e2c9-7cce-4c4f-a25a-c112cbdb6130")
	    		.addHeader("SID","7f24e2c9-7cce-4c4f-a25a-c112cbdb6131")
	    		.addHeader("SOURCE_CHANNEL","EE")
	    		.addHeader("ENEL_ID", "")
	    		.addHeader("TOUCHPOINT", "WEB")
	    		.addHeader("CLIENT_IP", "10.10.11.11")
	    		.addHeader("Content-Type", "application/json")
	    		.delete()
	    		.build();
	    Response response = client.newCall(request).execute();
	    JSONObject jsonObject = new JSONObject(response.body().string());
	    return jsonObject;
	}
	
	/**
	 * This method calls the getUUID api (see the URL on Costanti.java file), returning the enelId value linked to the account specified with dataType.
	 * 
	 * @param apiType It's a string value identifying the retreiving-information-way. It can be set to "email" or to "cf"
	 * @param dataType It represents the value to be passed to the API. If apiType is set to "email", this value should be an email. Same for cf.
	 * 
	 * @return
	 * the UUID if the specified account is existing and enabled, null otherwise.
	 */
	public static String getUUID(String apiType, String dataType) throws Exception{
		String filter = null;
		if(apiType.equals("email"))
			filter = "?email="+dataType;
		else if(apiType.equals("cf"))
			filter = "?personalId="+dataType+"&personalIdCountry=IT";
		else //if(apiType.equals("businessId"))
			filter = "?businessId="+dataType+"&businessIdCountry=IT";
		JSONObject jsonObject = sendGetRequest(Costanti.api_url_getUUID, filter);
		if(jsonObject==null)
			return null;
	    jsonObject = jsonObject.getJSONObject("data");
	    List<String> list = new ArrayList<String>();
	    JSONArray jsonArray = jsonObject.getJSONArray("accounts");
	    for(int i = 0 ; i < jsonArray.length(); i++){
	         list.add(jsonArray.getJSONObject(i).getString("enelId"));
	         list.add(Boolean.toString(jsonArray.getJSONObject(i).getBoolean("enabled")));
	    }
	    if(list.size()>0 && list.get(1).equals("true"))
	    	return list.get(0);
	    else
	    	return null;
	}
	
	/**
	 * This method calls the getEmail api (see the URL on Costanti.java file), returning the email value linked to the account specified with dataType.
	 * 
	 * @param apiType It's a string value identifying the retreiving-information-way. It can be set to "email" or to "cf"
	 * @param dataType It represents the value to be passed to the API. If apiType is set to "email", this value should be an email. Same for cf.
	 * 
	 * @return
	 * the EMAIL if the specified account is existing and enabled, null otherwise.
	 */
	public static String getEmail(String apiType, String dataType) throws Exception{
		String filter = null;
		if(apiType.equals("email"))
			filter = "?email="+dataType;
		else if(apiType.equals("cf"))
			filter = "?personalId="+dataType+"&personalIdCountry=IT";
		else //if(apiType.equals("businessId"))
			filter = "?businessId="+dataType+"&businessIdCountry=IT";
		JSONObject jsonObject = sendGetRequest(Costanti.api_url_getUUID, filter);
		if(jsonObject==null)
			return null;
	    jsonObject = jsonObject.getJSONObject("data");
	    List<String> list = new ArrayList<String>();
	    JSONArray jsonArray = jsonObject.getJSONArray("accounts");
	    for(int i = 0 ; i < jsonArray.length(); i++){ 
	         list.add(jsonArray.getJSONObject(i).getString("email"));
	         list.add(Boolean.toString(jsonArray.getJSONObject(i).getBoolean("enabled")));
	    }
	    if(list.size()>0 && list.get(1).equals("true"))
	    	return list.get(0);
	    else
	    	return null;
	}
	
	/**
	 * This method calls the exists api (see the URL on Costanti.java file), returning true if the account exists, false otherwise.
	 * 
	 * @param apiType It's a string value identifying the retreiving-information-way. It can be set to "email" or to "cf"
	 * @param dataType It represents the value to be passed to the API. If apiType is set to "email", this value should be an email. Same for cf.
	 * 
	 * @return
	 * true if the given account info is associated to an is existing and enabled account, false otherwise.
	 */
	public static boolean accountExists(String apiType, String dataType) throws Exception{
		String filter = null;
		if(apiType.equals("email"))
			filter = "?email="+dataType;
		else if(apiType.equals("cf"))
			filter = "?personalId="+dataType+"&personalIdCountry=IT";
		else //if(apiType.equals("businessId"))
			filter = "?businessId="+dataType+"&businessIdCountry=IT";
			
		JSONObject jsonObject = sendGetRequest(Costanti.api_url_accountExists, filter);
		if(jsonObject==null)
			return false;
	    jsonObject = jsonObject.getJSONObject("data");
	    JSONArray jsonArray = jsonObject.getJSONArray("matches");
	    if(jsonArray.length()>0)
	    	return true;
	    else
	    	return false;
	}
	
	/**
	 * This method calls the migration checking api (see the URL on Costanti.java file), returning true if the account is present within migration table, false otherwise.
	 * 
	 * @param apiType It's a string value identifying the retreiving-information-way. It can be set to "email" or to "cf"
	 * @param dataType It represents the value to be passed to the API. If apiType is set to "email", this value should be an email. Same for cf.
	 * 
	 * @return
	 * true if the given account info is associated to an is existing and enabled account, false otherwise.
	 */
	public static boolean migrationExistence(String apiType, String dataType) throws Exception{
		String filter = null;
		if(apiType.equals("email"))
			filter = "?email="+dataType;
		else if(apiType.equals("cf"))
			filter = "?personalId="+dataType+"&personalIdCountry=IT";
		else //if(apiType.equals("businessId"))
			filter = "?businessId="+dataType+"&businessIdCountry=IT";
			
		JSONObject jsonObject = sendGetRequest(Costanti.api_url_migrationExistence, filter);
		if(jsonObject==null)
			return false;
	    jsonObject = jsonObject.getJSONObject("data");
	    JSONArray jsonArray = jsonObject.getJSONArray("users");
	    if(jsonArray.length()>0)
	    	return true;
	    else
	    	return false;
	}
	
	/**
	 * This method calls the changePassword api (see the URL on Costanti.java file), returning true if the password has been correctly changed, false otherwise.
	 * 
	 * @param apiType It's a string value identifying the retreiving-information-way. It can be set to "email" or to "cf"
	 * @param dataType It represents the value to be passed to the API. If apiType is set to "email", this value should be an email. Same for cf.
	 * @param newPassword	The new password that will substitute the old one.
	 * 
	 * @return
	 * true if the password has been successfully changed, false otherwise.
	 */
	public static boolean changePassword(String apiType, String dataType, String newPassword) throws Exception{
		String json = "{\"newPassword\": \""+newPassword+"\",\"metadata\": {\"language\": \"it_IT\",\"processChannel\": \"ENEL_ENERGIA\"}}";
		JSONObject jsonObject = sendPostRequest(String.format(Costanti.api_url_changePassword, getUUID(apiType, dataType)), json);
		if(jsonObject==null)
			return false;
	    jsonObject = jsonObject.getJSONObject("data");
	    return (boolean)jsonObject.get("success");
	}
	
	/**
	 * This method calls the getOTP api (see the URL on Costanti.java file), returning the OTP send to the specified mobile number.
	 * 
	 * @param email The email specified during the registration phase.
	 * @param mobileNumber The mobile number specified during the registration phase.
	 * 
	 * @return
	 * OTP sent to the specified mobile number
	 */
	public static String getOTP(String email, String mobileNumber) throws Exception{
		String params = "?username="+email+"&phoneNumber=%2B39"+mobileNumber;
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer "+getToken());
		headers.put("TID","7f24e2c9-7cce-4c4f-a25a-c112cbdb6130");
		headers.put("SID","8b8fdc68-ee96-11e9-81b4-2a2ae2culo19");
		headers.put("SOURCE_CHANNEL","EE");
		headers.put("OPERATOR", "AutomationTeam");
		headers.put("TOUCHPOINT", "WEB");
		headers.put("CLIENT_IP", "10.10.11.11");
		JSONObject jsonObject = sendGetRequest(Costanti.api_url_getOTP, params, headers);
		if(jsonObject==null)
			return null;
		jsonObject = jsonObject.getJSONObject("data");
		return (String)jsonObject.get("otpCode");
	}
	
	/**
	 * This method calls the accountRegistration api (see the URL on CommonConstants.java file), returning true if the account has been correctly created, false otherwise.
	 * 
	 * 
	 * @return
	 * true if the password has been successfully changed, false otherwise.
	 */
	public static boolean registerResidentialAccount(String email, String mobile, String firstName, String lastName, String cf) throws Exception{
		String json = "{\n"+
				 "\"email\": \""+email+"\",\n"+
				   "\"phoneNumber\": \"+39"+mobile+"\",\n"+
				   "\"password\": \"Password01\",\n"+
				   "\"firstName\": \""+firstName+"\",\n"+
				   "\"lastName\": \""+lastName+"\",\n"+
				   "\"countryCode\": \"IT\",\n"+
				   "\"attributes\": [\n"+
				   "    {\n"+
				   "        \"country\": \"IT\",\n"+
				   "        \"claims\": [\n"+
				   "            {\n"+
				   "                \"name\": \"personalId\",\n"+
				   "                \"value\": \""+cf+"\"\n"+
				   "            }\n"+
				   "        ]\n"+
				   "    }\n"+
				   "],\n"+
				   "\"consents\": [\n"+
				   "    {\n"+
				   "        \"id\": \"11\",\n"+
				   "        \"group\": \"12\",\n"+
				   "        \"language\": \"it_IT\",\n"+
				   "        \"channel\": \"ENEL_ENERGIA\",\n"+
				   "        \"given\": true\n"+
				   "    },\n"+
				   "    {\n"+
				   "        \"id\": \"10\",\n"+
				   "        \"group\": \"12\",\n"+
				   "        \"language\": \"it_IT\",\n"+
				   "        \"channel\": \"ENEL_ENERGIA\",\n"+
				   "        \"given\": true\n"+
				   "    }\n"+
				   "],\n"+
				   "\"metadata\": {\n"+
				   "    \"language\": \"it_IT\",\n"+
				   "    \"processChannel\": \"ENEL_ENERGIA\"\n"+
				   "}\n"+
				"}";
		
		JSONObject jsonObject = sendPostRequest(Costanti.api_url_accountRegistration, json);
		if(jsonObject==null)
			return false;
	    String returnValue = jsonObject.getString("message");
	    if(returnValue.equals("OK"))
	    	return true;
	    else
	    	return false;
	}
	
	//It sends a REST POST request Jira. These parameters are sent in JSON format.
	public static void sendPostRequestJira(String apiURL, String json) throws Exception{
		Thread.currentThread().sleep(2000);
		Proxy proxy=null;
		OkHttpClient client = null;
		Response response = null;
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		//Istanzio objectBody
		RequestBody body = RequestBody.create(json, JSON);	
		//Istanzio request

		//La vecchia POST Request utilizzava le credenziali di Zio Rosario per esportare il risultato di una esecuzione su Jira. Da ora in poi, si utilizza un'utenza applicativa specifica - 03/02/2021 VG
//		String auth = Base64.getEncoder().encodeToString((Costanti.authUser.substring(Costanti.authUser.indexOf("\\")+1) + ":" + Costanti.authPassword).getBytes());
		
		String userPass = Costanti.JiraAuthUser + ":" + Costanti.JiraAuthPassword;
		String auth = Base64.getEncoder().encodeToString(userPass.getBytes());
		
		Request request = new Request.Builder().url(apiURL)
	    		.addHeader("Authorization", "Basic "+ auth)
	    		.addHeader("Content-Type", "application/json")
	    		.post(body)
	    		.build();

	/*	try (Socket socket = new Socket(Costanti.proxyAddress, Costanti.proxyPort)) 
		{//Invio request con Proxy
			//System.setProperty("http.proxyHost", Costanti.proxy);
			//System.setProperty("http.proxyUser", Costanti.authUser);
			//System.setProperty("http.proxyPassword", Costanti.authPassword);
			//System.setProperty("http.proxyPort", Costanti.proxyPort+"");
			proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Costanti.proxyAddress, Costanti.proxyPort));
			client = new OkHttpClient.Builder()
					.readTimeout(60000, TimeUnit.MILLISECONDS)
					.proxy(proxy)
					.build();
			response = client.newCall(request).execute();	     
		} 
		catch (Exception ex) */
		{//Invio request senza Proxy
			client = new OkHttpClient.Builder()
					.readTimeout(60000, TimeUnit.MILLISECONDS)
					.build();
			response = client.newCall(request).execute();
		}
		
		if (response.code() != 200 )
	        throw new Exception("Unexpected code " + response);
	    
	}
	
	/**
	 * This method calls the accountRegistration api (see the URL on CommonConstants.java file), returning true if the account has been correctly created, false otherwise.
	 * 
	 * 
	 * @return
	 * true if the password has been successfully changed, false otherwise.
	 */
	public static boolean confirmAccount(String code) throws Exception{
		String json = "{\n"+
				"    \"code\": \""+code+"\",\n"+
				"    \"metadata\": {\n"+
				"        \"language\": \"it_IT\",\n"+
				"        \"processChannel\": \"ENEL_ENERGIA\"\n"+
				"    }\n"+
				"}";
		
		JSONObject jsonObject = sendPostRequest(Costanti.api_url_getConfirmationLink, json);
		if(jsonObject==null)
			return false;
	    jsonObject = jsonObject.getJSONObject("data");
	    System.out.println(jsonObject.get("success"));
	    return (Boolean)jsonObject.get("success");
	}
	
//	public static void removeTestFromTextExecutionJIRA(String textExecKey, List<String> testList) throws Exception{
//		OkHttpClient client = new OkHttpClient();
//	    Request request = new Request.Builder().url(Costanti.api_url_removeTestFromTextExec_on_Jira)
//	    		.addHeader("Authorization", "Basic "+Base64.toBase64String("AF09527:Enel$R32020".getBytes()))
//	    		.delete()
//	    		.build();
//	    Response response = client.newCall(request).execute();
//	    JSONObject jsonObject = new JSONObject(response.body().string());
//	}
	
	private static JSONObject sendPostRequest_2(String apiURL, String json, String TID, String SID) throws Exception{
		Thread.currentThread().sleep(2000);
		OkHttpClient client = new OkHttpClient();
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody body = RequestBody.create(json, JSON);
		Request request = new Request.Builder().url(apiURL)
				.addHeader("TID",TID)
				.addHeader("SID",SID)
				.addHeader("CHANNELKEY", "cfa3a38e-b820-4226-a66f-bd9c14cbbe0d")
				.addHeader("SOURCECHANNEL", "APP")
				.addHeader("Content-Type", "application/json")
				.post(body)
				.build();
		Response response = client.newCall(request).execute();
		JSONObject jsonObject = new JSONObject(response.body().string());
		
		JSONObject jsonObject_1 = jsonObject.getJSONObject("status");
		
		String code = (String)jsonObject_1.get("code");
		//int code = (int)jsonObject.get("code");
		//if (code != 200 && code != 500)
		
		if (!code.equals("0"))
			throw new Exception("Unexpected code " + response);

		if(code.equals("0")) // FORSE C0DE = 0 ?
			return jsonObject;
		else
			return null;
	}

	public static String APILoginStrong(String email, String Password, String TID, String SID) throws Exception{
		String json = "{\n"+
                              "\"gen\": {\n"+
                              "\"tid\": \""+ TID +"\",\n"+
                              "\"sid\": \""+ SID +"\",\n"+
                              "\"username\": \""+ email +"\",\n"+
                              "\"keys\": [\n"+
                                     "{\n"+
                                           "\"key\": \"APP_VER\",\n"+
                                           "\"value\": \"20.3.0\"\n"+
                                     "},\n"+
                                     "{\n"+
                                           "\"key\": \"OS\",\n"+
                                           "\"value\": \"ANDROID\"\n"+
                                     "},\n"+
                                     "{\n"+
                                           "\"key\": \"APP\",\n"+
                                           "\"value\": \"cfa3a38e-b820-4226-a66f-bd9c14cbbe0d\"\n"+
                                     "},\n"+
                                     "{\n"+
                                           "\"key\": \"ID_DISPOSITIVO\",\n"+
                                           "\"value\": \"MWS0216808002415\"\n"+
                                     "}\n"+
                              "]\n"+
                          "},\n"+
                            "\"data\": {\n"+
                                     "\"username\": \"" + email + "\",\n"+
                                     "\"password\": \"" + Password + "\",\n"+
                                     "\"migrationToken\": \"\",\n"+
                                     "\"oidcToken\": \"\",\n"+
                                     "\"pwd_crypt\": \"\",\n"+
                                     "\"resolution\": \"1080x1794\",\n"+
                                     "\"osVer\": \"7.0\",\n"+
                                     "\"idDispositivo\": \"MWS0216808002415\",\n"+
                                     "\"os\": \"ANDROID\",\n"+
                                     "\"context\": \"app_ee\",\n"+
                                     "\"device\": \"HUAWEI EVA-L09\"\n"+
                              "}\n"+
                          "}";

        JSONObject jsonObject = sendPostRequest_2("https://msa-stage.enel.it/msa/app/authentication/v3/loginstrong", json, TID, SID);
        if(jsonObject==null)
               return "KO_TokenLoginStrong";
        
        jsonObject = jsonObject.getJSONObject("data");
        
        String returnValue = jsonObject.getString("token");
        return returnValue; 
	}

	public static String registerResidentialAccount_2(String email, String mobile, String firstName, String lastName, String cf) throws Exception{
		String json = "{\n"+
				 "\"email\": \""+email+"\",\n"+
				   "\"phoneNumber\": \"+39"+mobile+"\",\n"+
				   "\"password\": \"Password01\",\n"+
				   "\"firstName\": \""+firstName+"\",\n"+
				   "\"lastName\": \""+lastName+"\",\n"+
				   "\"countryCode\": \"IT\",\n"+
				   "\"attributes\": [\n"+
				   "    {\n"+
				   "        \"country\": \"IT\",\n"+
				   "        \"claims\": [\n"+
				   "            {\n"+
				   "                \"name\": \"personalId\",\n"+
				   "                \"value\": \""+cf+"\"\n"+
				   "            }\n"+
				   "        ]\n"+
				   "    }\n"+
				   "],\n"+
				   "\"consents\": [\n"+
				   "    {\n"+
				   "        \"id\": \"11\",\n"+
				   "        \"group\": \"12\",\n"+
				   "        \"language\": \"it_IT\",\n"+
				   "        \"channel\": \"ENEL_ENERGIA\",\n"+
				   "        \"given\": true\n"+
				   "    },\n"+
				   "    {\n"+
				   "        \"id\": \"10\",\n"+
				   "        \"group\": \"12\",\n"+
				   "        \"language\": \"it_IT\",\n"+
				   "        \"channel\": \"ENEL_ENERGIA\",\n"+
				   "        \"given\": true\n"+
				   "    }\n"+
				   "],\n"+
				   "\"metadata\": {\n"+
				   "    \"language\": \"it_IT\",\n"+
				   "    \"processChannel\": \"ENEL_ENERGIA\"\n"+
				   "}\n"+
				"}";

		JSONObject jsonObject = sendPostRequest(Costanti.api_url_accountRegistration, json);
        if(jsonObject==null)
               return "KO_RegistrazioneAccount";

        jsonObject = jsonObject.getJSONObject("data");
        return (String)jsonObject.get("enelId");

	}
	
	/**
	 * This method calls the getEmail api (see the URL on CommonConstants.java file), returning the email value linked to the account specified with dataType.
	 * 
	 * @param apiType It's a string value identifying the retreiving-information-way. It can be set to "email" or to "cf"
	 * @param dataType It represents the value to be passed to the API. If apiType is set to "email", this value should be an email. Same for cf.
	 * 
	 * @return
	 * the EMAIL if the specified account is existing and enabled, null otherwise.
	 */
	public static boolean deleteAccount(String apiType, String dataType) throws Exception{
		String filter = getUUID(apiType, dataType);
		JSONObject jsonObject = sendDeleteRequest(Costanti.api_url_deleteAccount, filter);
		if(jsonObject==null || filter == null)
			return false;
		else{
			jsonObject = jsonObject.getJSONObject("data");
			return (Boolean)jsonObject.get("success");
		}
	}
}
