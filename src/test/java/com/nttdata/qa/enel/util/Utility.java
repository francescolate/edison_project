package com.nttdata.qa.enel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Utility {

	public static String getDocumentPdf(Properties prop) throws Exception {

		File currentDirFile = new File(".");
		String helper = currentDirFile.getAbsolutePath();
		String currentDir = helper.substring(0, helper.length() - 1);
		String pathDoc = "";
		if(prop.getProperty("PATH_DOCUMENTO")!=null) {
			if(!prop.getProperty("PATH_DOCUMENTO").equals("")) {
				
				File fl = new File(prop.getProperty("PATH_DOCUMENTO"));
				
				if(fl.exists()) {
					return prop.getProperty("PATH_DOCUMENTO");
				}else {
					pathDoc = currentDir+"src\\test\\java\\resources\\genericDoc.pdf";
					File doc = new File(pathDoc);
					if(doc.exists()) {
					
						return pathDoc;
					}else {
						throw new Exception("1 File Doc non trovato all'interno del progetto:"+pathDoc);
					}
					
					
				}
				
			}
			
		}else {
			pathDoc = currentDir+"resources\\genericDoc.pdf";
			File doc = new File(pathDoc);
			if(doc.exists()) {
				System.out.println(pathDoc);
				return pathDoc;
			}else {
				
				//throw new Exception("2 File Doc non trovato all'interno del progetto: "+pathDoc);
				doc.createNewFile();
				return pathDoc;
			}
		}
		return null;
		
	}
	
	public static String GetYesterday() {
	    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Calendar calendar = new GregorianCalendar();
	    calendar.add(Calendar.DAY_OF_MONTH, -1);
		fmt.setCalendar(calendar );
	    String dateFormatted = fmt.format(calendar.getTime());
	    dateFormatted = dateFormatted.replace(" ", "T")+".000Z";
	    return dateFormatted;
	}
	
	public static String getMobileNumber(){
		return "3" + new RandomStringGenerator.Builder().withinRange('0','9').build().generate(9);
	}
	public static String getMobileNumber2(){
		return "34" + new RandomStringGenerator.Builder().withinRange('0','9').build().generate(8);
	}
	
	public static String getPhoneNumber(){
		return "08" + new RandomStringGenerator.Builder().withinRange('0','9').build().generate(8);
	}
	
//	public static void main (String [] args) throws Exception {
//		Utility u = new Utility();
//		u.getDocumentPdf(new Properties());
//	}
	
	public static void Wait(int minutes)
	{
		try {
			System.out.println("Waiting for "+minutes+" minutes");
			int counter=0;
			while(counter<minutes)
			{
				counter++;
				Thread.sleep(60*1000);
				System.out.println((minutes-counter) +" minutes left");
			}
		} catch (InterruptedException e) {
			
		}
	}
	

	
   
    public static void ExecuteAndRetry(int numberOfRetry,Executable executable) throws Exception
	{
		
		boolean retry=true;
		int cycle=0;
		
		while(retry)
		{
			try {
				cycle++;
				executable.Esegui();
				retry=false;
			}
			catch(Exception e)
			{
				if(cycle>=numberOfRetry)
				{
					throw e;
				}
			}
		}
		
		
		
	}

	public static void takeSnapShotOnKo(Properties prop,String nomeScenario) throws Exception{
			System.out.println("Taking SnapShot");
			
			
			
			nomeScenario=nomeScenario.replace(".properties", "");
		
    		String numeroRichiesta=prop.getProperty("NUMERO_RICHIESTA","");
    		String esito=prop.getProperty("RETURN_VALUE");
    		//String dateTime=new SimpleDateFormat("yyyy-MM-dd_HHmmss").format(new Date());
    		String dateTime=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    		String nomeFile=nomeScenario+"_["+numeroRichiesta+"]_"+dateTime+"_"+esito;
    		
    
    	
    		//SNAPSHOT
    		WebDriver webdriver=WebDriverManager.getDriverInstance(prop);
    		
    		//PROPERTY
    		takeSnapShot(webdriver, "prop\\"+nomeFile+".jpg");
    		prop.store(new FileOutputStream("prop\\"+nomeFile+".properties"), null);
    		
    		System.out.println("SnapShoot Tken");
    	
    	
    }
   
	public static String takeSnapShot(Properties prop,String nomeScenario,String label) throws Exception{
    	
    		WebDriver webdriver=WebDriverManager.getDriverInstance(prop);
    		String path=nomeScenario.replace(".properties", "")+"_"+prop.getProperty("NUMERO_RICHIESTA")+"_"+label.replace(" ","_")+".jpg";
    		takeSnapShot(webdriver, path);
    		return path;
    	
    }
    
    
    public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

    	
	        //Convert web driver object to TakeScreenshot
	        TakesScreenshot scrShot =((TakesScreenshot)webdriver);
	        //Call getScreenshotAs method to create image file
	        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	        //Move image file to new destination
	        File DestFile=new File(fileWithPath);
	        //Copy file at destination
	        FileUtils.copyFile(SrcFile, DestFile);
	        System.out.println("SnapShot Taken - ");
    	
    }
	
}
