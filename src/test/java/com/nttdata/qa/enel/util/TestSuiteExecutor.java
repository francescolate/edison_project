package com.nttdata.qa.enel.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.util.List;
import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestSuiteExecutor {

	public static void main(String[] args) throws Exception {
		args = new String[1];
		args[0] = "C:\\Users\\gaetavi\\NTT Data\\Projects\\Enel\\WEB_MOBILE_SFDC_AUTOMATION 2019-2021\\Utils\\apiList_test.txt";		
		JUnitCore core = new JUnitCore();
		core.addListener(new TextListener(System.out));
		Class jUnitClass = null;
		List<String> scripts = null;
		try {
			System.out.println(args[0]);
			BufferedReader reader;
			reader = new BufferedReader(new FileReader(args[0]));
			String line = null;
			while ((line = reader.readLine())!=null) {
				System.out.println(line);
				String failureMessage = null;
				jUnitClass = Class.forName(line);
				Constructor constructor = jUnitClass.getConstructor();
				Object instance = constructor.newInstance();
				Result result = core.run(instance.getClass());
//				System.out.println(result.wasSuccessful());
				for(Failure failure : result.getFailures())
					failureMessage = failure.getMessage();
				ReportUtility.reportForTSE(line, result.wasSuccessful()?"PASSED":"FAILED", failureMessage);
			}
			reader.close();
		} catch (Exception e){ 
			e.printStackTrace();
		}
	}
	
	public static String getFailures(Result result){
		String failure = "";
		for(Failure f : result.getFailures()){
			failure = f.toString();
		}
		return failure;
	}

}
