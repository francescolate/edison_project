package com.nttdata.qa.enel.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListLabelsResponse;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;

public class GmailQuickStart {

	 private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
	    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	    private static final String TOKENS_DIRECTORY_PATH = "tokens";
		private static String []  a = { GmailScopes.GMAIL_LABELS, GmailScopes.GMAIL_COMPOSE,GmailScopes.GMAIL_INSERT, GmailScopes.GMAIL_MODIFY, GmailScopes.GMAIL_READONLY, GmailScopes.MAIL_GOOGLE_COM};

	    /**
	     * Global instance of the scopes required by this quickstart.
	     * If modifying these scopes, delete your previously saved tokens/ folder.
	     */
	    //private static final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_LABELS);
	    private static final Collection<String> SCOPES = new ArrayList<String>(Arrays.asList(a ));
	    		;
	    private static final String CREDENTIALS_FILE_PATH = "credentials.json";

	    /**
	     * Creates an authorized Credential object.
	     * @param HTTP_TRANSPORT The network HTTP Transport.
	     * @return An authorized Credential object.
	     * @throws IOException If the credentials.json file cannot be found.
	     */
	    
	    
	    @SuppressWarnings("unused")
	    
	    
	    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
            return new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest httpRequest) throws IOException {
                    requestInitializer.initialize(httpRequest);
                    httpRequest.setConnectTimeout(3 * 60000);  // 3 minutes connect timeout
                    httpRequest.setReadTimeout(3 * 60000);  // 3 minutes read timeout
                }
            };
        }
	    
	    
		private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, String cliente, String pathPropertiesClass) throws IOException {
	        // Load client secrets.
	    	
		
	    	String ResourseFolder = pathPropertiesClass+ File.separator +"";
	    	
	    	String finalPath = "";
	    	switch(cliente) {
	    	case "RESIDENZIALE":
	    		ResourseFolder =  ResourseFolder+ File.separator + "residenziale" + File.separator;
	    		break;
	    	case "BUSINESS":
	    		ResourseFolder =  ResourseFolder+ File.separator + "business" + File.separator;
	    		break;
	    	case "SENDER":
	    		ResourseFolder =  ResourseFolder+ File.separator + "sender" + File.separator;
	    		break;
	    	case "TP8":
	    	case "REG_FROM_MOBILE":
	    		ResourseFolder =  ResourseFolder+ File.separator + "TP8" + File.separator;
	    		break;
	    	}
	    	
	    	finalPath = ResourseFolder+CREDENTIALS_FILE_PATH;
	    	  File initialFile = new File(finalPath);
	    	  System.out.println(finalPath);
	    	   InputStream in = new FileInputStream(initialFile);

	    	   if (in == null) {
	            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
	        }
	        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

	        // Build flow and trigger user authorization request.

	        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
	        		HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
	                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(ResourseFolder+ File.separator +TOKENS_DIRECTORY_PATH)))
	                .setAccessType("offline")
	                .build();
	        
	        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8080).build();
	        
	        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	    }

	    public static Message getMessage(Gmail service, String userId, String messageId)
	    	      throws IOException {
	    	    Message message = service.users().messages().get(userId, messageId).execute();

	    	    System.out.println("Message snippet: " + message.getSnippet());

	    	    return message;
	    	  }
	    

	    public static List<Message> listMessagesMatchingQuery(Gmail service, String userId,
	    	      String query) throws IOException {
	    	    ListMessagesResponse response = service.users().messages().list(userId).execute();

	    	    List<Message> messages = new ArrayList<Message>();
	    	    while (response.getMessages() != null) {
	    	      messages.addAll(response.getMessages());
	    	      if (response.getNextPageToken() != null) {
	    	        String pageToken = response.getNextPageToken();
	    	        response = service.users().messages().list(userId).setQ(query)
	    	            .setPageToken(pageToken).execute();
	    	      } else {
	    	        break;
	    	      }
	    	    }

	    	    for (Message message : messages) {
	    	      System.out.println(message.toPrettyString());
	    	    }

	    	    return messages;
	    	  }

	    	  /**
	    	   * List all Messages of the user's mailbox with labelIds applied.
	    	   *
	    	   * @param service Authorized Gmail API instance.
	    	   * @param userId User's email address. The special value "me"
	    	   * can be used to indicate the authenticated user.
	    	   * @param labelIds Only return Messages with these labelIds applied.
	    	   * @throws IOException
	    	   */
	    	  public static List<Message> listMessagesWithLabels(Gmail service, String userId,
	    	      List<String> labelIds) throws IOException {
	    	    ListMessagesResponse response = service.users().messages().list(userId)
	    	        .setLabelIds(labelIds).execute();

	    	    List<Message> messages = new ArrayList<Message>();
	    	    while (response.getMessages() != null) {
	    	      messages.addAll(response.getMessages());
	    	      if (response.getNextPageToken() != null) {
	    	        String pageToken = response.getNextPageToken();
	    	        response = service.users().messages().list(userId).setLabelIds(labelIds)
	    	            .setPageToken(pageToken).execute();
	    	      } else {
	    	        break;
	    	      }
	    	    }

	    	    for (Message message : messages) {
	    	      System.out.println(message.toPrettyString());
	    	    }

	    	    return messages;
	    	  }
	    
	    public static String getSubject(String raw) {
	    	String temp ="";	  
	    	try {  
	    		temp = raw.substring(raw.indexOf("Subject:"), raw.indexOf("MIME")).replace("Subject: ", "").replaceAll ("\r\n|\r|\n", "");
//	    		System.out.println(temp);
	    	
	    	}catch(Exception e) {}
	    		//System.out.println(temp);
	    		  return temp;
	    	  }
	    
	    public static String getLinkInEmailSollecito(String raw, String type) {
	    	return ricercaSollecito(raw);
	    }
  
	    public static String getLinkInEmail(String raw, String type) {
	    	
	    	String temp = "";
	    	switch (type) {
			case "ELETTRICO":
				temp = ricerca(raw);
				break;
			case "ELE":
				temp = ricerca(raw);
				break;
			case "GAS":
				temp = ricerca(raw);
				break;
			case "MULTI":
				temp = ricerca(raw);
				break;
			case "VOLTURA_SENZA_ACCOLLO":
				temp = ricercaVoltura(raw);
				break;
			case "SUBENTRO":
				temp = ricercaSubentro(raw);
				break;
			case "PRIMA_ATTIVAZIONE":
				temp = ricercaPrimaAttivazione(raw);
				break;
			case "DUAL":
//				temp = ricercaDual(raw);
				temp = ricerca(raw);
				break;
			case "NEW_ACCOUNT":
				temp = ricercaNewAccount(raw);
				break;
			case "TP8":
				temp = ricercaTP8(raw);
				break;
			case "RECOVER_PASSWORD_OTP":
				temp = ricercaOtp(raw);
				break;
			case "REG_FROM_MOBILE":
				temp = ricercaNewAccount(raw);
				break;
			case "INFO_ENEL_ACTIVATION":
				temp = ricercaInfoActivation(raw);
				break;			
			case "ADDEBITO_DIRETTO":
				temp = riceracaAddebitoDiretto(raw);
				break;
			case "CONFERMA_OFFERTA":
			    temp = ricercaOfferta(raw);
			    break;
			case "COMPLETA_ATTIVAZIONE":
				    temp = ricercaattivazionecompleta(raw);
				    break;
			case "RECLAMO LUCE":
				temp = ricerareclamoluce(raw);
				break;
			case "RECLAMO LUCE 75":
				temp = ricerareclamoluce75(raw);
			    break;
			case "FORMSCRIVICI 97":
				temp = riceraformscrivici97(raw);
				break;
			case "OFFERTA VALORE LUCE PLUS":
				temp = riceraoffertavaloreluceplus(raw);
				break;
			case "COMPLETA_ADESIONE_SALVATA":
				temp = completaAdesioneSalvata(raw);
				break;
								
			case "OFFERTA VALORE GAS PLUS":
				temp = completaAdesioneSalvata(raw);
				break;
			case "ID_113_RES_SWA_ELE_OCR_Salva3":
				temp = riceraoffertavaloreluceplus113(raw);
				break;
			default:
			}
			return temp;
		}
	    
	    public static String ricerca(String raw) {
//	    	System.out.println(raw);
//	    	 String temp = raw.substring(raw.indexOf("<a"), raw.indexOf(">Clicca qui per iniziare"));
	    	String first = raw.substring(raw.indexOf("<a href=\"http://tp-coll1.enel.it")+9);
	    	String second = first.substring(0, first.indexOf("\""));
	    	
//	    	 String temp = raw.substring(raw.indexOf("<a href=\"")+8, raw.indexOf("\"  style=\"color:#00bce5;\">Clicca qui per iniziare</a>"));

	    	 System.out.println(second);
//		     temp = temp.substring(temp.indexOf("\"")+1, temp.indexOf("\" ")).replaceAll ("\r\n|\r|\n", "");
	    	return second;
	    }
	    
	    public static String ricercaVoltura(String raw) {
	    	System.out.println(raw);
	    	 String temp = raw.substring(raw.indexOf("<a"), raw.indexOf(">clicca qui"));
			 System.out.println(temp);
		     temp = temp.substring(temp.indexOf("\"")+1, temp.indexOf("\" style")).replaceAll ("\r\n|\r|\n", "");
	    	return temp;
	    }
	    
	    public static String ricercaPrimaAttivazione(String raw) {
	    	System.out.println(raw);
	    	 String temp = raw.substring(raw.indexOf("<a"), raw.indexOf(">Clicca qui"));
			 System.out.println(temp);
//		     temp = temp.substring(temp.indexOf("\"")+1, temp.indexOf("\" class")).replaceAll ("\r\n|\r|\n", ""); 15/07/2021 modificato per modulo tp2 prima attivazione
		     temp = temp.substring(temp.indexOf("\"")+1, temp.indexOf("\" style")).replaceAll ("\r\n|\r|\n", "");
	    	return temp;
	    }
	    public static String ricercaSubentro(String raw) {
	    	System.out.println(raw);
	    	 String temp = raw.substring(raw.indexOf("<a"), raw.indexOf(">Clicca qui"));
			 System.out.println(temp);
		     temp = temp.substring(temp.indexOf("\"")+1, temp.indexOf("\" ")).replaceAll ("\r\n|\r|\n", "");
	    	return temp;
	    }
	    
	    public static String ricercaSollecito(String raw) {
	    	System.out.println(raw);
	    	 String temp = raw.substring(raw.indexOf("<a"), raw.indexOf(">Clicca qui"));
			 System.out.println(temp);
		     temp = temp.substring(temp.indexOf("\"")+1, temp.indexOf("\" ")).replaceAll ("\r\n|\r|\n", "");
	    	return temp;
	    }

	    public static String ricercaTP8(String raw) {
		    String temp =  raw.substring(raw.indexOf("sufficiente fare click"), raw.indexOf("Ti ricordiamo che il"));
			temp = temp.substring(temp.indexOf("href=\"")+6, temp.indexOf("\"><img")).replaceAll ("\r\n|\r|\n", "");
		 	return temp;
		}
	    
	    public static String ricercaNewAccount(String raw) {

		    //String temp =  raw.substring(raw.indexOf("Per confermarlo "), raw.indexOf("target=\""));
		    String temp =  raw.substring(raw.indexOf("Per confermarlo "), raw.indexOf("\">clicca qui</a>"));
			//temp = temp.substring(temp.indexOf("href=\"")+6, temp.indexOf(" target=\"")).replaceAll ("\r\n|\r|\n", "");
			temp = temp.substring(temp.indexOf("href=\"")+6);
		 	return temp;
		}
	    
	    public static String  ricercaInfoActivation(String raw){
	    	
	    	//String temp = raw.substring(raw.indexOf("<a"),raw.indexOf(">Conferma Validità Mail[1]"));
	    	String endingLimit = "\" style=\"color:";
	        String startingLimit = "<a href=\"";
	        String infoEElink = raw.substring(raw.indexOf(startingLimit)+9, raw.indexOf(endingLimit));
//	    	System.out.println(infoEElink);
	    	return infoEElink.replace("&amp;","&");
	    }
	    public static String ricercaOtp(String raw){
	    	String temp = raw.substring(raw.indexOf("Il tuo codice Pin e': ")+22, raw.indexOf("Il tuo codice Pin e': ")+28);
	    	return temp;
	    }
	    public static String riceracaAddebitoDiretto(String raw){
	    	return raw;
	    }

	    public static String ricercaattivazionecompleta(String raw){
	    	String endingLimit = "\" style=\"color:";
	        String startingLimit = "<a href=\"";
	        String infoEElink = raw.substring(raw.indexOf(startingLimit)+9, raw.indexOf(endingLimit));
//	    	System.out.println(infoEElink);
	    	return infoEElink.replace("&amp;","&");
	  	}
	    public static String ricercaOfferta(String raw){
	    	String endingLimit =  "\" aria-label=\"Clicca per confermare";
	        String startingLimit = "<a href=\"";
	    	String temp = raw.substring(raw.indexOf(startingLimit)+9, raw.indexOf(endingLimit));
	    	System.out.println(temp);
	    	return temp;
	    }	   
	    
	    public static String ricerareclamoluce(String raw){
	    	if(raw.indexOf("Content-Disposition: attachment")==-1)
	    		return null;
	    	else
	    		return raw.substring(raw.indexOf("Gentile Cliente,")+0, raw.indexOf("01/02/2021")+10).replace("<br>", "").replace("<td>", "").replace("<tr>", "").replace("</td>", "").replace("</tr>", "").replace("=C3=B9", "ù").replace("=C3=A8", "è");
	    }
	    
	    public static String ricerareclamoluce75(String raw){
	    	if(raw.indexOf("Content-Disposition: attachment")==-1)
	    		return null;
	    	else
	    	return raw.substring(raw.indexOf("Gentile Cliente,")+0, raw.indexOf("Autolettura:")).replace("<br>", "").replace("<td>", "").replace("<tr>", "").replace("</td>", "").replace("</tr>", "").replace("=C3=B9", "ù").replace("=C3=A8", "è");
	    	
	    }
	    
	    public static String riceraoffertavaloreluceplus(String raw){
	    	
	    	String temp = raw.substring(raw.indexOf("Gentile PETER PARKER")+0, raw.indexOf("Enel SpA")+8).replace("&#39;", "'").replace("=C3=A0", "à").replace("=C3=A8", "è").replace("&quot;", "\"");
	    	System.out.println(temp);
	    	return temp;
	    }
	    
	    public static String completaAdesioneSalvata(String raw){
	    	String temp = raw.replace("&#39;", "'").replace("=C3=A0", "à").replace("=C3=A8", "è").replace("&amp;", "&").substring(raw.indexOf("https://")-4, raw.indexOf("https://")+188);
	    	//String temp = raw.substring(raw.indexOf("https://"), raw.indexOf("https://")+193).replace("&#39;", "'").replace("=C3=A0", "à").replace("=C3=A8", "è").replace("&quot;", "\"").replace("&amp;", "&");
	    	return temp;
	    }
	    
	    public static String riceraoffertavaloreluceplus113(String raw){
	    	
	    	String temp = raw.substring(raw.indexOf("Gentile")+0, raw.indexOf("sito")+12).replace("=C3=A8", "è").replace("&#39;", "'");
	    	System.out.println(temp);
	    	return temp;
	    }
	    
	    public static String riceraformscrivici97(String raw){
	    	
	    	String temp = raw.substring(raw.indexOf("Gentile Cliente,")+0, raw.indexOf("ACCETTATO")+9).replace("pi=C3=B9", "più").replace("=C3=A8", "è");
	    	System.out.println(temp);
	    	return temp;
	    }

	    public static Boolean searchToOfferta(String raw, String POD) {
			  
	    	 String stringToSearch = raw;

	    	    Pattern p = Pattern.compile(POD);   // the pattern to search for
	    	    Matcher m = p.matcher(stringToSearch);
	    	    Boolean x =  m.find();
	    	    // now try to find at least one match
	    	    if (x) {
//	    	      System.out.println("Found a match");
	    	    }else {
//	    	      System.out.println("Did not find a match");
	    	    }
	    	   
	    	    return x;
		  }
	    	  
	    
	    public static NetHttpTransport connectionSetting() {
			
	    
	    		 try (
	    				Socket s = new Socket(Costanti.proxy, Costanti.proxyPort)) {
	    			 System.setProperty("http.proxyHost", Costanti.proxy);
	    				System.setProperty("http.proxyUser", Costanti.authUser);
	    				System.setProperty("http.proxyPassword", Costanti.authPassword);
	    				System.setProperty("http.proxyPort", Costanti.proxyPort+"");
	    				 final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Costanti.proxy, Costanti.proxyPort));
	    		    	 return new NetHttpTransport.Builder().setProxy(proxy).build();
	    		 
	    		 } catch (IOException ex) {
			    	 return new NetHttpTransport.Builder().build();//.setProxy(proxy).build();
	    		    }
	    	
			
	    
	    	 
	        
	    }
	    
	    
	    public static String recuperaLink(Properties prop, String type, Boolean isSollecito) throws Exception {
	    	

	        // Build a new authorized API client service.
//	        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
	        //String  pathPropertiesClass = prop.getProperty("CLASS_PATH","classes");
	    	String  pathPropertiesClass = "/Users/silviadesantis/eclipse-workspace/EnelMobileTesting/classes/resources/TP8";
	        
	        NetHttpTransport httpTransport = connectionSetting();
	        
	        Gmail service = new Gmail.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport,type.equals("TP8")?"TP8":prop.getProperty("TIPOCLIENTE"), pathPropertiesClass))
	                .setApplicationName(APPLICATION_NAME)
	                .build();
	        
	        // Print the labels in the user's account.
	        String user = "me";
	        ListMessagesResponse response = service.users().messages().list(user).execute();

	        List<Message> messages = new ArrayList<Message>();
	        while (response.getMessages() != null) {
	          messages.addAll(response.getMessages());
	          if (response.getNextPageToken() != null) {
	            String pageToken = response.getNextPageToken();
	            response = service.users().messages().list(user)
	                .setPageToken(pageToken).execute();
	          } else {
	            break;
	          }
	        }
	        Boolean flag = true;
	        String Last = "";
	        String Subject = "";
	          String ricerca = "";
	        switch (type) {
			case "ELE":
				 Subject = "Attivazione dell'offerta luce";  
				 ricerca =  prop.getProperty("POD");
				break;
			case "GAS":
				 Subject = "Attivazione dell'offerta gas";
				 ricerca =  prop.getProperty("POD");
				break;
			case "DUAL":
				 Subject = "Attivazione dell'offerta luce e gas";
				 ricerca =  prop.getProperty("POD_ELE");
				break;
			case "MULTI":
				Subject = "Attivazione dell'offerta";
				 ricerca =  prop.getProperty("POD_1");
				break;
			case "VOLTURA_SENZA_ACCOLLO":
				Subject = "richiesta di VOLTURA SENZA ACCOLLO";
				 ricerca =  prop.getProperty("NUMERO_CONTRATTO");
				break;
			case "PRIMA_ATTIVAZIONE":
				Subject = "richiesta di PRIMA ATTIVAZIONE";
				 ricerca =  prop.getProperty("NUMERO_CONTRATTO");
				break;
			case "SUBENTRO":
				Subject = "richiesta di SUBENTRO";
				 ricerca =  prop.getProperty("NUMERO_CONTRATTO");
				break;
			case "TP8":
				 Subject = "Compilazione_dei_Dati_Catastali";
				 ricerca = prop.getProperty("ITA_IFM_QUOTE_NUMBER__C");
				break;
			case "NEW_ACCOUNT":
				 Subject = "Conferma Account Utente";
				 ricerca = prop.getProperty("WP_USERNAME");
				break;
			case "RECOVER_PASSWORD_OTP":
				Subject = "OTP di conferma";
				ricerca = prop.getProperty("EMAIL");
				break;
			case "REG_FROM_MOBILE":
				Subject = "Conferma Account Utente";
				ricerca = prop.getProperty("EMAIL");
				break;
			case "INFO_ENEL_ACTIVATION":
				Subject = "Enel_Energia_-_N°_Cliente__-_Conferma_Indirizzo_Email?";
				 ricerca = prop.getProperty("USERNAME");
				break;
			case "ADDEBITO_DIRETTO":
				Subject = "Enel_Energia_-_Comunicazioni";
				 ricerca = prop.getProperty("EMAIL_INPUT");
				 break;
			case "COMPLETA_ATTIVAZIONE":
			    Subject = "Enel_Energia_-_Completa_l'attivazione_del_servizio_Bolletta_Web";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "CONFERMA_OFFERTA":
			    Subject = "Enel_Energia_-_Conferma_la_tua_offerta";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "RECLAMO LUCE":
			    Subject = "Web: Richiesta di supporto";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "RECLAMO LUCE 75":
			    Subject = "Web: Richiesta di supporto";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "FORMSCRIVICI 94":
			    Subject = "Web: Richiesta di supporto";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "FORMSCRIVICI 95":
			    Subject = "Web: Richiesta di supporto";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "FORMSCRIVICI 96":
			    Subject = "Web: Richiesta di supporto";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "FORMSCRIVICI 97":
			    Subject = "Web: Richiesta di supporto";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "OFFERTA VALORE LUCE PLUS":
				Subject = "Enel_Energia_-_Conferma_la_tua_offerta?";
			    ricerca = prop.getProperty("EMAIL");
			case "OFFERTA VALORE GAS PLUS":
				Subject = "Enel_Energia_-_Conferma_la_tua_offerta?";
			    ricerca = prop.getProperty("EMAIL"); 
			    break;
			case "Offer Scegli Tu Ore Free":
				Subject = "Enel_Energia_personali in conformita ";
			    ricerca = prop.getProperty("EMAIL"); 
			    break; 
			   
			case "Offer Enel Energia PLACET Fissa Gas Business":
				Subject = "Enel_Energia_personali in conformita ";
			    ricerca = prop.getProperty("EMAIL"); 
			    break;
			case "COMPLETA_ADESIONE_SALVATA":
				Subject = "Enel Energia - Completa l'adesione salvata!";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "ID_113_RES_SWA_ELE_OCR_Salva3":
				Subject = "Enel Energia - Completa l'adesione salvata!";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "ID_112_RES_SWA_ELE_OCR_Salva2":
				Subject = "Enel Energia - Completa l'adesione salvata!";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			case "ID_117_RES_PRIMA_ATTIV_ELE_OCR":
				Subject = "Enel Energia - Completa l'adesione salvata!";
			    ricerca = prop.getProperty("EMAIL");
			    break;
			default:
				throw new Exception("Type non corretto");
			}
	        for (Message message : messages) {
	          Last = message.getId();
	          Gmail.Users.Messages.Get messagesGet = service.users().messages().get(user, Last);
	          messagesGet.setFormat("raw");
	          Message mess = messagesGet.execute();

	        //  String decodedString = new String(mess.decodeRaw()).replaceAll ("=\r\n|\r|\n", "").replace("=3D", "=").replace("=?utf-8?Q?", "").replace("=C2=B0", "°").replace("=E2=80=93","-").replace("=E2=80=99","'").replace("? ", "");

	          String decodedString = new String(mess.decodeRaw()).replaceAll ("=\r\n|\r|\n", "").replace("=3D", "=").replace("=?utf-8?Q?", "").replace("=C2=B0", "°").replace("=E2=80=93", "-").replace("=E2=80=99", "'").replace("attiv? azione", "attivazione");
//	          System.out.println(Subject);
	       

	          System.out.println(getSubject(decodedString));
	          if(getSubject(decodedString).contains(Subject)||getSubject(decodedString).contains(Subject.replace(" ","_"))) { //in alcuni casi nell'oggetto al posto degli spazi viene inserito un underscore
	        	  System.out.println(decodedString);
	        	  if(isSollecito) {
		        	  if(searchToOfferta(decodedString, prop.getProperty("POD"))&&searchToOfferta(decodedString, "Hai ancora pochi giorni")) {
		        		  String tm = getLinkInEmailSollecito(decodedString, type);
		                  System.out.println(tm);
		             
		                  flag = false;
		                  return tm;
		        	  }
		                  
	        	  }else {
	        		  if(searchToOfferta(decodedString,ricerca)) {
		        	  
		        		  String tm = getLinkInEmail(decodedString, type);
		             
		                  flag = false;
		                  return tm;
	        		  }
	        	  }
	          }else {
	        	  continue;
	          }
	        }
	    	if(flag) {
	    		throw new Exception ("Email con link per TP2 non trovata.");
	    	}
	    	return "";
	    }
	    
	    
	    public static String recuperaLink2(Properties prop, String mailSubject, Date mailDate, String verification) throws Exception {
	    	Boolean flag = true;
	    	Message selectedMessage = getGmailMessage(prop, mailSubject, mailDate, verification);
	    	if (selectedMessage != null) {
	    		String body = getMessageBody(selectedMessage);
	    		if (body != null) {
	    			String theLink = getInfoEnelEnergiaActivationLink(body);
	    			System.out.println("Il link è: " + theLink);
	    			flag = false;
	    			
	    			return theLink;
				}
			} else {
				throw new Exception ("Cannot find message");
			}
	    	if(flag) {
	    		throw new Exception ("Cannot find link in mail");
	    	}
	    	
	    	return "";
	    }
	    
	    public static String getMessageBody(Message theMessage) {
	    	MessagePartBody mpBody = null;
	    	String body = null;
	    	try {
	    		List<MessagePart> parts = theMessage.getPayload().getParts();
		    	for (MessagePart part:parts) {
		    		if (part.getMimeType().contains("text/html")) {
		    			mpBody = part.getBody();
		    			break;
					}
		    	}
			} catch (NullPointerException e) {
				MessagePart payload = theMessage.getPayload();
				if (payload.getMimeType().contains("text/html")) {
	    			mpBody = payload.getBody();
				}
			}
	    	if (mpBody != null) {
				body = new String(Base64.decodeBase64(mpBody.getData().getBytes()));
			}
	    	
	    	return body;
	    }
	    
	    private static String getInfoEnelEnergiaActivationLink(String messageBody) {
	    	int initialIdx = messageBody.indexOf("<a href=\"") + 9;
	    	int finalIdx = messageBody.indexOf("\" ", initialIdx);

	    	return messageBody.substring(initialIdx, finalIdx);
	    }	
	    
	    public static Message getGmailMessage(Properties prop, String mailSubject, Date mailDate, String verification) throws Exception {
	    	NetHttpTransport httpTransport = connectionSetting();
	    	String pathPropertiesClass = prop.getProperty("CLASS_PATH","classes");
	    	Gmail service = new Gmail.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport, prop.getProperty("TIPOCLIENTE"), pathPropertiesClass))
	    			.setApplicationName(APPLICATION_NAME)
	    			.build();
	    	// Print the labels in the user's account.
	    	String user = "me";
	    	ListMessagesResponse response = service.users().messages().list(user).execute();
	    	List<Message> messages = new ArrayList<Message>();
	    	while (response.getMessages() != null) {
	    		messages.addAll(response.getMessages());
	    		if (response.getNextPageToken() != null) {
	    			String pageToken = response.getNextPageToken();
	    			response = service.users().messages().list(user)
	    					.setPageToken(pageToken).execute();
	    		} else {
	    			break;
	    		}
	    	}
	    	Boolean flag = true;
	    	String Last = "";
	    	Message selectedMessage = null;
	    	Date selectedMessageDate = mailDate;
	    	for (Message message : messages) {
	    		Last = message.getId();
	    		Gmail.Users.Messages.Get messagesGet = service.users().messages().get(user, Last);
	    		messagesGet.setFormat("full");
	    		Message mess = messagesGet.execute();
	    		String snippet = mess.getSnippet();
	    		System.out.println("****SNIPPET****: " + snippet);
	    		if (mess.getPayload() != null) {
	    			List<MessagePartHeader> headers = mess.getPayload().getHeaders();
	    			String messageSubject = null;
	    			String messageDate = null;
	    			for (MessagePartHeader header:headers) {
	    				if (header.getName().contains("Subject")) {
							messageSubject = header.getValue();
						} else if (header.getName().contains("Date")) {
							messageDate = header.getValue();
						}
	    			}
	    			if (messageSubject != null && messageDate != null) {
	    				Date theDate;
	    				try {
	    					SimpleDateFormat dateFormat = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss Z (z)", Locale.US);
	    					theDate = dateFormat.parse(messageDate);
						} catch (Exception e) {
							try {
								SimpleDateFormat dateFormat = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss Z", Locale.US);
		    					theDate = dateFormat.parse(messageDate);
							} catch (Exception e2) {
								continue;
							}
						}
						boolean subjectIsCorrect = messageSubject.contains(mailSubject);
						//Even if selectedMessage is null, we initialized selectedMessageDate with mailDate
						boolean dateIsCorrect = theDate.after(selectedMessageDate);
						if (dateIsCorrect) {
							if (subjectIsCorrect && snippet.contains(verification)) {
								selectedMessage = mess;
								selectedMessageDate = theDate;
								System.out.println("Oggetto: " + messageSubject);
								System.out.println("Data: " + messageDate);
								System.out.println("Snippet: " + snippet);
							}
						} else {
							//Supposing we get the messages in descending order by date, let's exit the loop if the date is not correct
							break;
						}
					}
				}
	    	}
	    	if (selectedMessage == null) {
				throw new Exception("Could not find mail message");
			}
	    	
	    	return selectedMessage;
	    }
	    
	    public static MimeMessage createEmailWithAttachment(String to,
                String from,
                String subject,
                String bodyText,
                File file)
			throws MessagingException, IOException {
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			
			MimeMessage email = new MimeMessage(session);
			
			email.setFrom(new InternetAddress(from));
			
			String [] tos = to.split(",");
			
			Address[] recipientAddress = new InternetAddress[tos.length];
			int counter = 0;
			for (String recipients : tos) {
			    recipientAddress[counter] = new InternetAddress(recipients.trim());
			    counter++;
			}
			email.addRecipients(javax.mail.Message.RecipientType.TO,
					recipientAddress);
			email.setSubject(subject);
			
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(bodyText, "text/plain");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);
			
			mimeBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(file);
			
			mimeBodyPart.setDataHandler(new DataHandler(source));
			mimeBodyPart.setFileName(file.getName());
			
			multipart.addBodyPart(mimeBodyPart);
			email.setContent(multipart);
			
			return email;
			}
	    
	    public static Message createMessageWithEmail(MimeMessage emailContent)
	            throws MessagingException, IOException {
	        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	        emailContent.writeTo(buffer);
	        byte[] bytes = buffer.toByteArray();
	        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
	        Message message = new Message();
	        message.setRaw(encodedEmail);
	        return message;
	    }
	    
	    public static MimeMessage createEmail(String to,
                String from,
                String subject,
                String bodyText)
				throws MessagingException {
				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				
				MimeMessage email = new MimeMessage(session);
				email.setContent(bodyText, "text/html; charset=utf-8");
				email.setFrom(new InternetAddress(from));
				String [] tos = to.split(",");
				
				Address[] recipientAddress = new InternetAddress[tos.length];
				int counter = 0;
				for (String recipients : tos) {
				    recipientAddress[counter] = new InternetAddress(recipients.trim());
				    counter++;
				}
				email.addRecipients(javax.mail.Message.RecipientType.TO,
						recipientAddress);
				email.setSubject(subject);
				//email.setText(bodyText);
				return email;
				}
	    
	    public static Message sendMessage(Gmail service,
                String userId,
                MimeMessage emailContent)
				throws MessagingException, IOException {
				Message message = createMessageWithEmail(emailContent );		
				
				message = service.users().messages().send(userId, message).execute();
				
				System.out.println("Message id: " + message.getId());
				System.out.println(message.toPrettyString());
				return message;
				}
	    
	
	    
	    
	    
	    public static void InviaEmail(String recipient, String oggetto, String Testo, String pathPropertiesClass) throws Exception {
	    	final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
	    	 final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy.risorse.enel", 8080));
		        final NetHttpTransport httpTransport = new NetHttpTransport.Builder().setProxy(proxy).build();

	    	final String authUser = "ENELINT\\AF09527";
			final String authPassword = "Enel$1234";

			System.setProperty("http.proxyHost", "proxy.risorse.enel");
			System.setProperty("http.proxyUser", authUser);
			System.setProperty("http.proxyPassword", authPassword);
			
			System.setProperty("http.proxyPort", "8080");
	        Gmail service = new Gmail.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport, "SENDER", pathPropertiesClass))
	                .setApplicationName(APPLICATION_NAME)
	                .build();

	        // Print the labels in the user's account.
	        String user = "me";
			sendMessage(service, user, createEmail(recipient, "Sender.testing.automation@gmail.com", oggetto, Testo));

	    }
	    
}
