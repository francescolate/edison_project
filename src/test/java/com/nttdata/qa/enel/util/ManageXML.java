package com.nttdata.qa.enel.util;

import java.io.File;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ManageXML {
	
	
//	public static void main(String [] args) {
//		try {
//			String filePath="src/test/java/com/nttdata/qa/enel/testdata/ModificaTipologiaBollettaGAS.xml";
//			Properties prop = new Properties();
//			Element x = setPropertyFromXmlByElement(filePath, prop, "Sintesi e Dettaglio");
//			//System.out.println(prop.getProperty("POD_1"));
//			x.setAttribute("ID", "cippalippa");
//			Transformer xformer = TransformerFactory.newInstance().newTransformer();
//			xformer.transform(new DOMSource(x.getOwnerDocument()), new StreamResult(new File(filePath)));
//			
//			
//		}catch(Exception e) {e.printStackTrace();}
//		
//		
//	}

	public static Element setPropertyFromXmlByElement(String filePath, Properties prop,
			String idelement) throws Exception {

		File fXmlFile = new File(filePath);
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
		        .newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document doc = documentBuilder.parse(fXmlFile);
		NodeList list = doc.getElementsByTagName("dataInput");
		Stream<Node> nodeStream = IntStream.range(0, list.getLength())
                .mapToObj(list::item).filter(p-> p.getAttributes().item(0).getTextContent().equals(idelement));	
		Optional<Node> optional = nodeStream.findFirst();
		if(!optional.isPresent()) throw new Exception("Non ci sono dati per la tipologia "+idelement);
		Node c = optional.get();
		Element eElementx = (Element) c;
		NodeList propList = eElementx.getElementsByTagName("propValue");
		Stream<Node> nodeStream2 = IntStream.range(0, propList.getLength())
                .mapToObj(propList::item);
		
		nodeStream2.forEach(n->{
			Element elemento = (Element)n;
			String key = elemento.getElementsByTagName("key").item(0).getTextContent();
			String value = elemento.getElementsByTagName("value").item(0).getTextContent();
			prop.setProperty(key, value);
		});
	    


		return eElementx;
	}

}
