package com.nttdata.qa.enel.util;

public class PartitaIva {
	public PartitaIva() {
	}

	static String Piva = "";
	static int X = 0;
	static int Y = 0;
	static int Z = 0;
	static int T = 0;
	static int C = 0;

	public static int charToInt(char c) {
		switch (c) {
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		}
		return -1;
	}

	public static String ControllaPIVA(String pi) {
		int i = 0;
		if (pi.length() == 0)
			return "";
		if (pi.length() != 11) {
			return "La lunghezza della partita IVA non e' corretta: la partita IVA dovrebbe essere lunga esattamente 11 caratteri.\n";
		}

		for (i = 0; i < 11; i++) {
			if ((pi.charAt(i) < '0') || (pi.charAt(i) > '9')) {
				return "La partita IVA contiene dei caratteri non ammessi:\nla partita IVA dovrebbe contenere solo cifre.\n";
			}
		}
		int s = 0;
		for (i = 0; i <= 9; i += 2) {
			s += pi.charAt(i) - '0';
		}

		for (i = 1; i <= 9; i += 2) {
			int ci = 2 * (pi.charAt(i) - '0');

			if (ci > 9) {
				ci -= 9;
			}
			s += ci;
		}

		if ((10 - s % 10) % 10 != pi.charAt(10) - '0') {

			return "La partita IVA non e' valida:\n il codice di controllo non corrisponde.";
		}
		return "partita iva valida";
	}

	public static String getPartitaIVA() {
		int temp = (int) (Math.random() * 1.0E7D);
		String t = temp+"";
		if (t.length() < 7) {
			int l = 7 - t.length();
			for (int i = 0; i < l; i++) {
				t = "0" + t;
			}
		}
		Piva = t;
		temp = (int) (Math.random() * 100.0D) + 1;
		t = temp+"";
		if (t.length() < 3) {
			int l = 3 - t.length();
			for (int i = 0; i < l; i++) {
				t = "0" + t;
			}
		}
		Piva += t;
		X = charToInt(Piva.charAt(0)) + charToInt(Piva.charAt(2)) + charToInt(Piva.charAt(4))
				+ charToInt(Piva.charAt(6)) + charToInt(Piva.charAt(8));

		for (int i = 1; i <= 9; i += 2) {
			Y = 2 * (Piva.charAt(i) - '0');

			if (Y > 9)
				Y -= 9;
			X += Y;
		}

		T = X % 10;
		C = (10 - T) % 10;
		Piva += C;
		return Piva;
	}
	
	public static String getPartitaIVA(String firstValue) {
		int temp = (int) (Math.random() * 1.0E7D);
		String t = temp+"";
		t=t.replace(t.charAt(0), firstValue.charAt(0));
		if (t.length() < 7) {
			int l = 7 - t.length();
			for (int i = 0; i < l; i++) {
				t = firstValue + t;
			}
		}
		Piva = t;
		temp = (int) (Math.random() * 100.0D) + 1;
		t = temp+"";
		if (t.length() < 3) {
			int l = 3 - t.length();
			for (int i = 0; i < l; i++) {
				t = "0" + t;
			}
		}
		Piva += t;
		X = charToInt(Piva.charAt(0)) + charToInt(Piva.charAt(2)) + charToInt(Piva.charAt(4))
				+ charToInt(Piva.charAt(6)) + charToInt(Piva.charAt(8));

		for (int i = 1; i <= 9; i += 2) {
			Y = 2 * (Piva.charAt(i) - '0');

			if (Y > 9)
				Y -= 9;
			X += Y;
		}

		T = X % 10;
		C = (10 - T) % 10;
		Piva += C;
		return Piva;
	}
	
	
	
	public static void main(String [] args) {
		
		getPartitaIVA("8");
	}

}
