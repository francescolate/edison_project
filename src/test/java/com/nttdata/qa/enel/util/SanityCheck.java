package com.nttdata.qa.enel.util;

import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class SanityCheck {

	public static void main(String[] args) throws Exception {
		
		Properties properties = new Properties();
		JUnitCore core = new JUnitCore();
		core.addListener(new TextListener(System.out));
		Class jUnitClass = null;
		List<String> environments = null;
		try {
//			properties.load(new FileInputStream("SanityCheck.properties"));
			properties.load(new FileInputStream(args[0]));
			for(Entry<Object, Object> entry : properties.entrySet())
				if(entry.getKey().equals("WP_LINK"))
					environments =  Arrays.asList(entry.getValue().toString().split(";"));
				else
					System.setProperty((String)entry.getKey(), (String)entry.getValue());
			for(String env : environments){
				System.setProperty("WP_LINK", env);
				jUnitClass = Class.forName(properties.getProperty("SCRIPT_NAME"));
				Constructor constructor = jUnitClass.getConstructor();
				Object instance = constructor.newInstance();
				Result result = core.run(instance.getClass());
				if(!result.wasSuccessful()){
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
					LocalDateTime now = LocalDateTime.now();  
					GmailQuickStart.InviaEmail(System.getProperty("EMAIL_RECIPIENT"), System.getProperty("EMAIL_SUBJECT"), 
							System.getProperty("EMAIL_BODY").replace("$URL$", System.getProperty("WP_LINK"))
									.replace("$USERNAME$", System.getProperty("WP_USERNAME"))
									.replace("$PASSWORD$", System.getProperty("WP_PASSWORD"))
									.replace("$DATETIME$", dtf.format(now))
									.replace("$ERROR$" , getFailures(result)),
									properties.getProperty("CLASS_PATH","classes")
									
							);
				}
			}
		} catch (Exception e){ 
			e.printStackTrace();
		}
	}
	
	public static String getFailures(Result result){
		String failure = "";
		for(Failure f : result.getFailures()){
			failure = f.toString();
		}
		return failure;
	}

}
