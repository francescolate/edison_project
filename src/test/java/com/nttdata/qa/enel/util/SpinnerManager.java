package com.nttdata.qa.enel.util;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SpinnerManager {
	SeleniumUtilities util;
	//Logger LOGGER = Logger.getLogger("");
	final static String displayAttr = "display:none";
	final static String displayAttrSp = "display: none";

	WebDriver driver;
	public final By tinySpinner = new By.ByXPath("//div[@id='loading']");
	public final By bauSpinner = new By.ById("load_scrl");
	public final By overlaySpinner = new ByXPath("//*[@class='loadingBox overlay']");
	public final By dotSpinner = new By.ById("spinner");
//	public final By dotSpinner2 =new By.ByXPath("//lightning-spinner[@id='spinner' and contains(@class,'AllaccioAttivazione')]");
	public final By loadingSpinner = new By.ById("loadingSpinner");
	public final By lightSpinner = new By.ByXPath("//span[@id='spinners']/preceding-sibling::div[@role='dialog']");
	public final By newConfiguratorSpinner = new By.ByXPath("//div[contains(@class,'slds-spinner')]");
    public final By collaSpinner1 = By.xpath("//div[@role='alertdialog'  and @class='loader-container' and @style='display: block;']");
	public final By collaSpinner2=By.xpath("//div[@class='loader-container ng-isolate-scope' and @id='spinner']/div[@class='loader-wrapper']/span[@class='loader']");
	
	public SpinnerManager(WebDriver driver) {
		util = new SeleniumUtilities(driver);
		this.driver = driver;
	}

//	class DisplaynoneCondition implements ExpectedCondition<Boolean> {
//		By e;
//
//		public DisplaynoneCondition(By e) {
//			this.e = e;
//		}
//
//		@Override
//		public Boolean apply(WebDriver input) {
//			// TODO Auto-generated method stub
//			//System.out.println("Searching " + input);
//			WebElement ele = driver.findElement(this.e);
//			String attr = ele.getAttribute("style");
//			//System.out.println("Attr: " + attr);
//			if (attr.contains(displayAttr) || attr.contains(displayAttrSp)) {
//				//System.out.println("Spinner invisible");
//				return true;
//			}
//			return false;
//		}
//
//	}
	
	
	
	public void checkCollaSpinner() throws Exception{
		int tentativi = 200;
		
		while(util.exists(collaSpinner1, 5) && --tentativi > 0) {
	          TimeUnit.SECONDS.sleep(3);
		}
	}
	
	public void checkCollaSpinner1() throws Exception{
		int tentativi = 200;
		
		while(util.exists(collaSpinner2, 5) && --tentativi > 0) {
	          TimeUnit.SECONDS.sleep(3);
		}
	}
	
	public void checkMainCollaSpinner() throws Exception{
		int tentativi = 200;
		while(!util.exists(By.xpath("//div[@id='spinner' and @style='display: none;']"), 5) && --tentativi > 0) {
	          TimeUnit.SECONDS.sleep(3);
		}
	}
	
	public void checkCollaSpinner4() throws Exception{
		int tentativi = 200;
		
		while(util.exists(By.xpath("//span//div[@class='loader']"), 5) && --tentativi > 0) {
	          TimeUnit.SECONDS.sleep(3);
		}
	}

	public void waitForSpinnerByStyleDiplayNone(By by) {
		//		FluentWait<WebDriver> wait = new WebDriverWait(driver, 250).pollingEvery(5, TimeUnit.SECONDS);
		//		try {
		//			wait.until(new DisplaynoneCondition(by));
		//			TimeUnit.SECONDS.sleep(2);
		//		} catch (Exception e) {
		//			System.out.println("Stale reference for spinner, skipping");
		//			return;
		//		}
		try {checkSpinners();}catch(Exception e) {}


	}

	private boolean isAttributePresent(WebElement element, String attribute) throws Exception {
		Boolean result = false;
		try {
			String value = element.getAttribute(attribute);
			if (value != null) {
				result = true;
			}
		} catch (Exception e) {
		}

		return result;
	}

	private void waitDisplayNoneAttribute(By by) throws Exception {
		try {
			boolean found = false;
			FluentWait<WebDriver> wait2 = new WebDriverWait(driver, 60).pollingEvery(1, TimeUnit.SECONDS);
			WebElement spinner = driver.findElement(by);
			if (isAttributePresent(spinner, "style")) {
				//System.out.println("found style attribute");
				//logger.log(Level.INFO, "found style attribte");
				boolean none = false;
				String attr = "";
				int tentativi = 60;
				while (!none && tentativi-- > 0) {
					attr = spinner.getAttribute("style");
					if (attr.contains(displayAttr) || attr.contains(displayAttrSp)) {
						none = true;
						//System.out.println("found " + attr);
						//logger.log(Level.INFO, "found " + attr);
					}
					TimeUnit.SECONDS.sleep(1);
				}

			}
		} catch (Exception e) {
			//System.out.println("spinner finito");
		}

	}

	private void waitSldsAttribute(By by) throws Exception {
		try {
			boolean found = false;
			FluentWait<WebDriver> wait2 = new WebDriverWait(driver, 60).pollingEvery(1, TimeUnit.SECONDS);
			WebElement spinner = driver.findElement(by);
			if (isAttributePresent(spinner, "class")) {
				//System.out.println("found class attribute");
				//logger.log(Level.INFO, "found class attribute:"+spinner.getAttribute("class"));
				try {
					wait2.until(ExpectedConditions.attributeContains(spinner, "class", "slds-hide"));
					//logger.log(Level.INFO, "found slds-hide");
					//System.out.println("found slds-hide");
					found = true;
				}catch(Exception e) {
					//logger.log(Level.INFO, "not found slds-hide");
					//System.out.println("not found slds-hide");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("problema spinner");
		}

	}
	
	private void waitSldsAttribute(WebElement spinner) throws Exception {
		try {
			boolean found = false;
			FluentWait<WebDriver> wait2 = new WebDriverWait(driver, 60).pollingEvery(1, TimeUnit.SECONDS);
			if (isAttributePresent(spinner, "class")) {
				//System.out.println("found class attribute");
				try {
					wait2.until(ExpectedConditions.attributeContains(spinner, "class", "slds-hide"));
					//System.out.println("found slds-hide");
					found = true;
				}catch(Exception e) {
					//System.out.println("not found slds-hide");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("problema spinner");
		}

	}



	public void checkSpinners(String frame) throws Exception{
		try {
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
//			driver.switchTo().frame(frame);
			checkSpinners();
		}finally {
			driver.switchTo().defaultContent();
		}

	}

	public void checkSpinners() throws Exception {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			FluentWait<WebDriver> wait = new WebDriverWait(driver, 2).pollingEvery(1, TimeUnit.SECONDS);
			int i = 0;
			while (i++ < 2) {
				//System.out.println("ciclo "+i);
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(dotSpinner));
					List<WebElement> listDotSpinner=driver.findElements(dotSpinner);
					for(int j=0;j<listDotSpinner.size();j++){
					try {
						//ciclo per verificare tutti i dot Spinner
						//System.out.println("dotspinner"+(j+1));
						////logger.log(Level.INFO, "found dotSpinner:"+(j+1));
						waitSldsAttribute(listDotSpinner.get(j));
						continue;
					} catch (Exception e) {
						//System.out.println();
						////logger.log(Level.INFO, "no dot"+(j+1));
					}}
				}catch(Exception e) {
					
				}
			
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(tinySpinner));
					//System.out.println("found tiny");
					////logger.log(Level.INFO, "found tiny");
					waitDisplayNoneAttribute(tinySpinner);
					continue;
				} catch (Exception e) {
					////logger.log(Level.INFO, "no tiny spinner");
				}
				try {
					//questo spinner compare in pagina per pochissimi secondi e poi scompare
					//in altri casi invece resta fisso in pagina ma scompare con la proprieta sls-hide
					wait.until(ExpectedConditions.presenceOfElementLocated(lightSpinner));
					//System.out.println("found light");
					////logger.log(Level.INFO, "found light");
					waitSldsAttribute(lightSpinner);
					continue;
				} catch (Exception e) {
					////logger.log(Level.INFO, "no light spinner");
				}
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(loadingSpinner));
					//System.out.println("found loadingSpinner");
					////logger.log(Level.INFO, "found loadingSpinner");
					waitDisplayNoneAttribute(loadingSpinner);
					continue;
				} catch (Exception e) {
					////logger.log(Level.INFO, "no Loading spinner");
				}

				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(overlaySpinner));
					//System.out.println("found overlay");
					////logger.log(Level.INFO, "overlaySpinner");
					waitDisplayNoneAttribute(overlaySpinner);
					continue;
				} catch (Exception e) {
					////logger.log(Level.INFO, "no overlay");

				}
			

			}
		} finally {
			////logger.log(Level.INFO, "Fine Check Spinner");
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			TimeUnit.SECONDS.sleep(1);

		}
	}
	
	
	public void checkSpinnersWithBau(String frame) throws Exception{
		try {
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			checkSpinnersWithBau();
		}finally {
			driver.switchTo().defaultContent();
		}

	}

	public void checkSpinnersWithBau() throws Exception {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			FluentWait<WebDriver> wait = new WebDriverWait(driver, 1);
			int i = 0;
			while (i++ < 2) {
				//System.out.println("ciclo "+i);
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				List<WebElement> listDotSpinner=driver.findElements(dotSpinner);
				for(int j=0;j<listDotSpinner.size();j++){
				try {
					//ciclo per verificare tutti i dot Spinner
					//System.out.println("dotspinner"+(j+1));
					//logger.log(Level.INFO, "found dotSpinner");
					waitSldsAttribute(listDotSpinner.get(j));
				} catch (Exception e) {
					//System.out.println("no dot"+(j+1));
					////logger.log(Level.INFO, "no dot"+(j+1));
				}}
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(tinySpinner));
					//System.out.println("found tiny");
					////logger.log(Level.INFO, "found tiny");
					waitDisplayNoneAttribute(tinySpinner);
					continue;
				} catch (Exception e) {
					//logger.log(Level.INFO, "no tiny");
				}
				try {
					//questo spinner compare in pagina per pochissimi secondi e poi scompare
					//in altri casi invece resta fisso in pagina ma scompare con la proprieta sls-hide
					wait.until(ExpectedConditions.presenceOfElementLocated(lightSpinner));
					//System.out.println("found light");
					//logger.log(Level.INFO, "found light");
					waitSldsAttribute(lightSpinner);
					continue;
				} catch (Exception e) {
					//logger.log(Level.INFO, "no light");
				}
				try {
					
					wait.until(ExpectedConditions.presenceOfElementLocated(loadingSpinner));
					//System.out.println("found loadingSpinner");
					//logger.log(Level.INFO, "found loadingSpinner");
					waitDisplayNoneAttribute(loadingSpinner);
					continue;
				} catch (Exception e) {
					//logger.log(Level.INFO, "no loadingSpinner");
				}

				try {			
						wait.until(ExpectedConditions.presenceOfElementLocated(bauSpinner));
						//System.out.println("found bau");
						//logger.log(Level.INFO, "found bau");
						waitDisplayNoneAttribute(bauSpinner);
						continue;
				} catch (Exception e) {
					//logger.log(Level.INFO, "no bau spinner");
				}
				try {
					driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
					wait.until(ExpectedConditions.presenceOfElementLocated(overlaySpinner));
					//System.out.println("found overlay");
					//logger.log(Level.INFO, "overlaySpinner");
					waitDisplayNoneAttribute(overlaySpinner);
					continue;
				} catch (Exception e) {
					//logger.log(Level.INFO, "no overlay");

				}
			

			}
		} finally {
			//logger.log(Level.INFO, "Fine Check Spinner With Bau");
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			TimeUnit.SECONDS.sleep(1);

		}
	}
	
	
	public void checkBauSpinner(String frame) throws Exception{
		try {
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			checkBauSpinner();
		}finally {
			driver.switchTo().defaultContent();
		}

	}

	public void checkBauSpinner() throws Exception {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			FluentWait<WebDriver> wait = new WebDriverWait(driver, 1);
			int i = 0;
			while (i++ < 2) {
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(bauSpinner));
					//System.out.println("found bau");
					//logger.log(Level.INFO, "found bau");
					waitDisplayNoneAttribute(bauSpinner);
					continue;
				} catch (Exception e) {
					//logger.log(Level.INFO, "no bau spinner");
				}
			}
		} finally {
			//logger.log(Level.INFO, "Fine Check Bau Spinner");
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			TimeUnit.SECONDS.sleep(1);

		}
	}
	
	public void checkSpinnerNewConfigurator(String frame) throws Exception{
		try {
			WebElement frameToSw = this.driver.findElement(
					By.xpath("//iframe[@name='" +frame +"']")
					);
			driver.switchTo().frame(frameToSw);
			checkSpinnerNewConfigurator();
		}finally {
			driver.switchTo().defaultContent();
		}

	}
	
	
	public void checkSpinnerNewConfigurator() throws Exception {
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			FluentWait<WebDriver> wait = new WebDriverWait(driver, 1);
			int i = 0;
			while (i++ < 2) {
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(newConfiguratorSpinner));
					//System.out.println("found bau");
					//LOGGER.log(Level.INFO, "found newConfiguratorSpinner");
					waitSldsAttribute(newConfiguratorSpinner);
					continue;
				} catch (Exception e) {
					//LOGGER.log(Level.INFO, "no newConfiguratorSpinner");
				}
			}
		} finally {
			//LOGGER.log(Level.INFO, "Fine Check newConfiguratorSpinner");
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			TimeUnit.SECONDS.sleep(1);

		}
	}
	
	


	public void waitForSpinnerBySldsHide(By by) throws Exception {
		//		FluentWait<WebDriver> wait = new WebDriverWait(driver, 250).pollingEvery(5, TimeUnit.SECONDS);
		//
		//		WebElement spin = driver.findElement(by);
		//		System.out.println("Waiting for spinner, current attr : " + spin.getAttribute("class"));
		//		try {
		//			wait.until(ExpectedConditions.attributeContains(spin, "class", "slds-hide"));
		//		} catch (StaleElementReferenceException e) {
		//			System.out.println("Stale reference for spinner, skipping");
		//			return;
		//		}
		//		System.out.println("Spinner ok, current attr : " + spin.getAttribute("class"));
		try {checkSpinners();}catch(Exception e) {}
	}

	public void waitForSpinnerBySldsHide(String frame, By by) throws Exception {
		//		try {
		//			driver.switchTo().frame(frame);
		//			waitForSpinnerBySldsHide(by);
		//		} catch (Exception e) {
		//			System.out.println("Spinner slds hide not present. Skip");
		//		} finally {
		//			driver.switchTo().defaultContent();
		//		}
		try {checkSpinners(frame);}catch(Exception e) {}

	}

	public void waitForSpinnerByStyleDiplayNone(String frame) throws Exception {

		//		waitForSpinnerByStyleDiplayNone(frame, tinySpinner);
		try {checkSpinners(frame);}catch(Exception e) {}

	}

	public void waitForSpinnerByStyleDiplayNone(String frame, By by) throws Exception {
		//		try {
		//			this.driver.switchTo().frame(frame);
		//			System.out.println("Switched to frame " + frame);
		//			waitForSpinnerByStyleDiplayNone(by);
		//			this.driver.switchTo().defaultContent();
		//			TimeUnit.SECONDS.sleep(2);
		//			System.out.println("Returned to default content ");
		//		} catch (Exception e) {
		//			throw e;
		//		}
		try {checkSpinners(frame);}catch(Exception e) {}
	}

	// slds-modal slds-fade-in-open slds-spinner--large
}
