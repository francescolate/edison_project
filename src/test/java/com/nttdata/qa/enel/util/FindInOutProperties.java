package com.nttdata.qa.enel.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindInOutProperties {

	public static void main(String[] args) throws IOException {

		final File folder = new File(
				"C:\\Users\\galdieroro\\eclipse-workspace\\Enel\\Enel\\src\\test\\java\\com\\nttdata\\qa\\enel\\testqantt\\r2d\\");
		listFilesForFolder(folder);
		// TODO Auto-generated method stub

	}

	public static void listFilesForFolder(final File folder) throws IOException {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				// PER ORA IGNORA listFilesForFolder(fileEntry);
			} else {
//				System.out.println(fileEntry.getName());
//				System.out.println(fileEntry.getAbsolutePath());
//				System.out.println(fileEntry.getCanonicalPath());
				////System.out.println(fileEntry.getParent());

				////System.out.println(fileEntry.getAbsoluteFile());

				readFile(fileEntry.getParent() + "\\", fileEntry.getName());

			}
		}
	}

	public static void writeFile(String newfoldername, ArrayList<String> input, ArrayList<String> output,
			ArrayList<String> def) {
		String PATH = "C:\\Temp\\PROPERTIES\\R2D\\";
		String fileName = "stocazzo.txt";

		File directory = new File(PATH + newfoldername);
		if (!directory.exists()) {
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + newfoldername + "\\" + fileName, true));

			writer.newLine();
			writer.write("-----------------INPUT PROPERTIES-------------------");

			for (String x : input) {
				writer.newLine(); // Add new line
				writer.write(x);

			}

			writer.newLine();
			writer.write(" ");
			writer.newLine();
			writer.write("-----------------OUTPUT PROPERTIES-----------------");

			for (String x : output) {
				writer.newLine(); // Add new line
				writer.write(x);

			}

			writer.newLine();
			writer.write(" ");
			writer.newLine();
			writer.write("-----------------DEFAULT VALUES PROPERTIES-----------------");
			for (String x : def) {
				writer.newLine(); // Add new line
				writer.write(x);

			}

			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

	}

	public static void readFile(String path, String filename) {

		ArrayList<String> input = new ArrayList();
		ArrayList<String> output = new ArrayList();
		ArrayList<String> def = new ArrayList();

		try (FileReader reader = new FileReader(path + filename); BufferedReader br = new BufferedReader(reader)) {

			// read line by line
			String line;
			int x = 1;
			int x2 = 1;
			boolean flag = false;
			boolean flag2 = false;
			while ((line = br.readLine()) != null) {
				if (x == 2)
					flag = true;
				if (line.contains("getProperty")) {

					final Pattern pattern = Pattern.compile("getProperty\\(\"(.+?)\"", Pattern.DOTALL);
					final Matcher matcher = pattern.matcher(line);
					x = 1;
					String m = "";
					while (matcher.find()) {
						m = matcher.group(1);
						if (!m.equals("INTERACTIONFRAME") && !m.equals("RUN_LOCALLY")) {
							input.add(m);
						}

						x++;
					}

					final Pattern pattern2 = Pattern.compile("getProperty\\(\"\\w+\",\"(.+?)\"", Pattern.DOTALL);
					final Matcher matcher2 = pattern2.matcher(line);
					x2 = 1;
					String m2 = "";
					while (matcher2.find()) {
						if (!matcher2.group(0).contains("MODULE_ENABLED") && !matcher2.group(0).contains("RUN_LOCALLY")) {
							m2 = matcher2.group(1);
							
								flag2 = true;
								def.add(m2);
							

							x2++;
						}
					}

				}

				if (line.contains("setProperty")) {
					final Pattern pattern = Pattern.compile("setProperty\\(\"(.+?)\"", Pattern.DOTALL);
					final Matcher matcher = pattern.matcher(line);
					matcher.find();
					output.add(matcher.group(1));
				}

			}
			if (flag2)
				System.out.println(filename.replace(".java", ""));

			writeFile(filename.replace(".java", ""), input, output, def);

		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		}

	}
}
