package com.nttdata.qa.enel.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;



public class SetJiraExecutionStatus {

	public static void main(String[] args) throws Exception {
				
	}
	
	public static void updateJira(Properties prop) throws Exception
	{

		
		//Da HashMap Raf
//		String testName = className.replace(".properties", "");
	    String testKey = new JiraTestMap().getidTestJiraByClassName(prop.getProperty("TEST_NAME"));
	    String testExecutionKey = new JiraTestMap().getExecutionIdByClassName(prop.getProperty("TEST_NAME"));
	    
	    
	    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	    Date startDate = new Date(System.currentTimeMillis());
	    String startTestDate = startDate.toString();
	    String endTestDate = startDate.toString();
	    String comment = "Nuova Esecuzione";
	    ;
	    String jiraStatus = "PASS";
	    if (prop.getProperty("RETURN_VALUE", "KO").equalsIgnoreCase("KO"))
	    {
	    	jiraStatus = "ABORTED";
	    	comment = prop.getProperty("ERROR_DESCRIPTION", "");
	    }
	    	
	    
	    String dataFIleAttach = "data";
	    String fileNameAttach = prop.getProperty("TEST_NAME")+".properties";

	    dataFIleAttach =  Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(System.getProperty("user.dir")+File.separator+fileNameAttach)));

	    comment = comment.replaceAll(System.lineSeparator(), "");
	    comment = comment.replaceAll("(\\r|\\n)", "");
	    comment = comment.replaceAll("\n", "");
	    comment = comment.replaceAll("\r", "");
	    comment = comment.replaceAll("\t", "");
	    comment = comment.replaceAll("\"", "'");
	    comment = comment.replaceAll("\u00E2", "");
	    
	    
	    String JSON_INPUT="{\r\n\r\n    \"testExecutionKey\": \""+testExecutionKey+"\",\r\n\r\n    \"tests\" : [\r\n\r\n        {\r\n\r\n            \"testKey\" : \""+testKey+"\",\r\n\r\n            \"start\" : \""+startTestDate+"\",\r\n\r\n            \"finish\" : \""+endTestDate+"\",\r\n\r\n            \"comment\" : \""+comment+"\",\r\n\r\n            \"status\" : \""+jiraStatus+"\",\r\n\r\n                        \"evidences\" : [\r\n\r\n                        {\r\n\r\n                            \"data\": \""+dataFIleAttach+"\",\r\n\r\n                            \"filename\": \""+fileNameAttach+"\",\r\n\r\n                            \"contentType\": \"text/plain\"\r\n\r\n                        }]\r\n\r\n        }\r\n\r\n    ]\r\n\r\n}";

		
		//APIService.sendPostRequestJira("https://jira.springlab.enel.com/rest/raven/1.0/import/execution",JSON_INPUT);
		APIService.sendPostRequestJira("https://teamfrancesco.atlassian.net/rest/raven/1.0/import/execution",JSON_INPUT);

	}

}
