package com.nttdata.qa.enel.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;

import org.apache.poi.ss.usermodel.Workbook;

public class RecuperaDatiProperties {
	static JFrame f; 
	  
    static JProgressBar b; 
	public static void main(String[] args) throws Exception {
		
		ArrayList<String> arr = new ArrayList<String>();
		Files.walk(Paths.get("."))
		.filter(f ->  f.toString().contains(".properties"))
        .forEach(entry ->  arr.add(entry.getFileName().toString()));
//		System.out.println(Arrays.toString(arr.toArray()));
		   // create a frame 
        f = new JFrame("ProgressBar demo"); 
  
        // create a panel 
        JPanel p = new JPanel(); 
  
        // create a progressbar 
        b = new JProgressBar(); 
        p.setSize(350, 200); 
        // set initial value 
        b.setValue(0); 
  
        b.setStringPainted(true); 
  
        // add progressbar 
        p.add(b); 

        // add panel 
        f.add(p); 
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // set the size of the frame 
        f.setSize(500, 300); 
        f.setVisible(true); 
        int total = arr.size();
        
        //il totale total
        Double avanzamentoStatico =  100.0 / total ;
        
        Double avanzamento = 0.0;
        ReportUtility.waitYourTurn();
		ReportUtility.myTurn();
		Properties prop = new Properties();
		InputStream in = null;
		String filePath = ReportUtility.createOrGetTodayReportToServerBackup();
		Workbook w = ExcelUtility.openWorkbook(new FileInputStream(filePath));
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String sDate = simpleDateFormat.format(calendar.getTime()).toString();
		String user = System.getProperty("user.name");
		String machine = InetAddress.getLocalHost().getHostName();
		
		for (String string : arr) {
			System.out.println(string);
			try {
				 in = new FileInputStream(string);
			}catch (Exception e) {
				continue;
			}
			prop.load(in);

			if( prop.getProperty("START_TIME","").equals("")) {
				continue;
			}
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date dt=format.parse(prop.getProperty("START_TIME"));
			
			SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
//			System.out.println(format2.format(dt));
			ReportUtility.writeReportToServer(w,  string ,format2.format(dt), prop.getProperty("RETURN_VALUE", "KO"), ReportUtility.getMeaningfulData(prop), prop.getProperty("ERROR_DESCRIPTION"),machine, user);

	        prop.clear();
	        avanzamento = avanzamento + avanzamentoStatico;
	        int IntValue = (int) Math.round(avanzamento);
	        b.setValue(IntValue); 
		}
		
		ReportUtility.finalizeReport(w, filePath);
		ReportUtility.leave();
		System.exit(0);
	}
	
	 public static void fill() 
	    { 
	        int i = 0; 
	        try { 
	            while (i <= 100) { 
	                // fill the menu bar 
	                b.setValue(i + 10); 
	  
	                // delay the thread 
	                Thread.sleep(1000); 
	                i += 20; 
	            } 
	        } 
	        catch (Exception e) { 
	        } 
	    } 

}
