/**
 * 
 */
package com.nttdata.qa.enel.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

/**
 * @author Galdiero Roberto
 *
 */
public class ErrorManager {
	
	private static ErrorManager instance = null;

	// costruttore privato
	private ErrorManager() {
	}
	
	public static ErrorManager getInstance() {
		if (instance == null) {
			instance = new ErrorManager();
		}
		return instance;
	}

	public String getStackTrace(Exception e)
	{
	    StringWriter sWriter = new StringWriter();
	    PrintWriter pWriter = new PrintWriter(sWriter);
	    e.printStackTrace(pWriter);
	    return sWriter.toString();
	}
	
	/**
	 * Mostra una dialog Message di errore e interrompe l'esecuzione dell'Agent
	 * @param Exception
	 * @param Logger
	 */
	public void showFatal(Exception e, Logger logger)
	{
	    StringWriter sWriter = new StringWriter();
	    PrintWriter pWriter = new PrintWriter(sWriter);
	    e.printStackTrace(pWriter);
	    JOptionPane.showMessageDialog(null, sWriter.toString(), "RQA - RIMM Remote Agent", JOptionPane.ERROR_MESSAGE);

	  //  logger.log(Level.SEVERE,sWriter.toString());
	    System.exit(0);
//	    return sWriter.toString();
	}

	/**
	 * Mostra una dialog Message di errore 
	 * L'esecuzione del programma continua.
	 * @param Error Message
	 * @param Logger
	 */
	public void showError(String errorMessage, Logger logger)
	{   
		
	    JOptionPane.showMessageDialog(null, errorMessage, "RQA - RIMM Remote Agent", JOptionPane.ERROR_MESSAGE);
	   
	
	   // logger.log(Level.SEVERE,errorMessage);
	}
	
	/**
	 * Mostra una dialog Message di errore 
	 * L'utente puo decidere se interrompere l'esecuzione.
	 * @param Error Message
	 * @param Logger
	 */
	public void showErrorOption(String errorMessage, Logger logger)
	{   	

	
		//logger.log(Level.SEVERE,errorMessage);
		errorMessage += "\r\n do you want to try the connection again?";
		int dialogResult = JOptionPane.showConfirmDialog (null, errorMessage, "RQA - RIMM Remote Agent", JOptionPane.ERROR_MESSAGE);
	    
	    
//	    JOptionPane.showMessageDialog(null, errorMessage, "QRA - QANTT Remote Agent", JOptionPane.ERROR_MESSAGE);
	    if(dialogResult == JOptionPane.NO_OPTION)
	    {
	    	System.exit(0);
	    }
	   
	}


}
